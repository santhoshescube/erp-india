﻿namespace MyBooksERP
{
    partial class FrmUnearnedPolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label DescriptionLabel;
            System.Windows.Forms.Label Label2;
            System.Windows.Forms.Label Label1;
            System.Windows.Forms.Label Label4;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUnearnedPolicy));
            this.AbsentPolicyBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.lblHeader = new System.Windows.Forms.Label();
            this.txtPolicyName = new System.Windows.Forms.TextBox();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.errAbsentPolicy = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.cboCalculationBased = new System.Windows.Forms.ComboBox();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.chkRateOnly = new System.Windows.Forms.CheckBox();
            this.rdbActualMonth = new System.Windows.Forms.RadioButton();
            this.rdbCompanyBased = new System.Windows.Forms.RadioButton();
            this.lblHoursPerDay = new System.Windows.Forms.Label();
            this.txtHoursPerDay = new System.Windows.Forms.TextBox();
            this.lblRate = new System.Windows.Forms.Label();
            this.chkRatePerDay = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.dgvAbsentDetails = new DemoClsDataGridview.ClsDataGirdView();
            this.colAbsentAddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAbsentSelectedItem = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colAbsentParticulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpunearned = new System.Windows.Forms.GroupBox();
            DescriptionLabel = new System.Windows.Forms.Label();
            Label2 = new System.Windows.Forms.Label();
            Label1 = new System.Windows.Forms.Label();
            Label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.AbsentPolicyBindingNavigator)).BeginInit();
            this.AbsentPolicyBindingNavigator.SuspendLayout();
            this.ssStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errAbsentPolicy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAbsentDetails)).BeginInit();
            this.grpunearned.SuspendLayout();
            this.SuspendLayout();
            // 
            // DescriptionLabel
            // 
            DescriptionLabel.AutoSize = true;
            DescriptionLabel.Location = new System.Drawing.Point(6, 25);
            DescriptionLabel.Name = "DescriptionLabel";
            DescriptionLabel.Size = new System.Drawing.Size(66, 13);
            DescriptionLabel.TabIndex = 314;
            DescriptionLabel.Text = "Policy Name";
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Location = new System.Drawing.Point(6, 103);
            Label2.Name = "Label2";
            Label2.Size = new System.Drawing.Size(101, 13);
            Label2.TabIndex = 329;
            Label2.Text = "Exclude from Gross ";
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new System.Drawing.Point(6, 77);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(109, 13);
            Label1.TabIndex = 328;
            Label1.Text = "Calculation Based On";
            // 
            // Label4
            // 
            Label4.AutoSize = true;
            Label4.Location = new System.Drawing.Point(6, 49);
            Label4.Name = "Label4";
            Label4.Size = new System.Drawing.Size(81, 13);
            Label4.TabIndex = 330;
            Label4.Text = "Calculation Day";
            // 
            // AbsentPolicyBindingNavigator
            // 
            this.AbsentPolicyBindingNavigator.AddNewItem = null;
            this.AbsentPolicyBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.AbsentPolicyBindingNavigator.DeleteItem = null;
            this.AbsentPolicyBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorSaveItem,
            this.btnClear,
            this.btnPrint,
            this.btnEmail,
            this.ToolStripSeparator2,
            this.btnHelp});
            this.AbsentPolicyBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.AbsentPolicyBindingNavigator.MoveFirstItem = null;
            this.AbsentPolicyBindingNavigator.MoveLastItem = null;
            this.AbsentPolicyBindingNavigator.MoveNextItem = null;
            this.AbsentPolicyBindingNavigator.MovePreviousItem = null;
            this.AbsentPolicyBindingNavigator.Name = "AbsentPolicyBindingNavigator";
            this.AbsentPolicyBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.AbsentPolicyBindingNavigator.Size = new System.Drawing.Size(383, 25);
            this.AbsentPolicyBindingNavigator.TabIndex = 301;
            this.AbsentPolicyBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save Data";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Visible = false;
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Visible = false;
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 20);
            this.btnHelp.Text = "He&lp";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblHeader.Location = new System.Drawing.Point(16, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(66, 13);
            this.lblHeader.TabIndex = 315;
            this.lblHeader.Text = "Policy Info";
            // 
            // txtPolicyName
            // 
            this.txtPolicyName.BackColor = System.Drawing.SystemColors.Info;
            this.txtPolicyName.Location = new System.Drawing.Point(126, 21);
            this.txtPolicyName.MaxLength = 50;
            this.txtPolicyName.Name = "txtPolicyName";
            this.txtPolicyName.Size = new System.Drawing.Size(229, 20);
            this.txtPolicyName.TabIndex = 313;
            this.txtPolicyName.TextChanged += new System.EventHandler(this.txtPolicyName_TextChanged);
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 368);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(383, 22);
            this.ssStatus.TabIndex = 317;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // errAbsentPolicy
            // 
            this.errAbsentPolicy.ContainerControl = this;
            this.errAbsentPolicy.RightToLeft = true;
            // 
            // tmrClear
            // 
            this.tmrClear.Tick += new System.EventHandler(this.tmrClear_Tick);
            // 
            // cboCalculationBased
            // 
            this.cboCalculationBased.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCalculationBased.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCalculationBased.BackColor = System.Drawing.SystemColors.Info;
            this.cboCalculationBased.DropDownHeight = 134;
            this.cboCalculationBased.FormattingEnabled = true;
            this.cboCalculationBased.IntegralHeight = false;
            this.cboCalculationBased.Location = new System.Drawing.Point(126, 72);
            this.cboCalculationBased.MaxDropDownItems = 10;
            this.cboCalculationBased.Name = "cboCalculationBased";
            this.cboCalculationBased.Size = new System.Drawing.Size(231, 21);
            this.cboCalculationBased.TabIndex = 320;
            this.cboCalculationBased.SelectedIndexChanged += new System.EventHandler(this.cboCalculationBased_SelectedIndexChanged);
            this.cboCalculationBased.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCalculationBased_KeyDown);
            // 
            // txtRate
            // 
            this.txtRate.Location = new System.Drawing.Point(126, 271);
            this.txtRate.MaxLength = 9;
            this.txtRate.Name = "txtRate";
            this.txtRate.ShortcutsEnabled = false;
            this.txtRate.Size = new System.Drawing.Size(231, 20);
            this.txtRate.TabIndex = 325;
            this.txtRate.TextChanged += new System.EventHandler(this.Changestatus);
            this.txtRate.Validated += new System.EventHandler(this.ResetForm);
            this.txtRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // chkRateOnly
            // 
            this.chkRateOnly.AutoSize = true;
            this.chkRateOnly.Location = new System.Drawing.Point(126, 249);
            this.chkRateOnly.Name = "chkRateOnly";
            this.chkRateOnly.Size = new System.Drawing.Size(71, 17);
            this.chkRateOnly.TabIndex = 324;
            this.chkRateOnly.Text = "Rate only";
            this.chkRateOnly.UseVisualStyleBackColor = true;
            this.chkRateOnly.CheckedChanged += new System.EventHandler(this.chkRateOnly_CheckedChanged);
            // 
            // rdbActualMonth
            // 
            this.rdbActualMonth.AutoSize = true;
            this.rdbActualMonth.Location = new System.Drawing.Point(267, 47);
            this.rdbActualMonth.Name = "rdbActualMonth";
            this.rdbActualMonth.Size = new System.Drawing.Size(88, 17);
            this.rdbActualMonth.TabIndex = 319;
            this.rdbActualMonth.TabStop = true;
            this.rdbActualMonth.Text = "Actual Month";
            this.rdbActualMonth.UseVisualStyleBackColor = true;
            this.rdbActualMonth.CheckedChanged += new System.EventHandler(this.rdbActualMonth_CheckedChanged);
            // 
            // rdbCompanyBased
            // 
            this.rdbCompanyBased.AutoSize = true;
            this.rdbCompanyBased.Location = new System.Drawing.Point(126, 46);
            this.rdbCompanyBased.Name = "rdbCompanyBased";
            this.rdbCompanyBased.Size = new System.Drawing.Size(117, 17);
            this.rdbCompanyBased.TabIndex = 318;
            this.rdbCompanyBased.TabStop = true;
            this.rdbCompanyBased.Text = "Based on Company";
            this.rdbCompanyBased.UseVisualStyleBackColor = true;
            this.rdbCompanyBased.CheckedChanged += new System.EventHandler(this.rdbCompanyBased_CheckedChanged);
            // 
            // lblHoursPerDay
            // 
            this.lblHoursPerDay.AutoSize = true;
            this.lblHoursPerDay.Location = new System.Drawing.Point(6, 139);
            this.lblHoursPerDay.Name = "lblHoursPerDay";
            this.lblHoursPerDay.Size = new System.Drawing.Size(76, 13);
            this.lblHoursPerDay.TabIndex = 327;
            this.lblHoursPerDay.Text = "Hours Per Day";
            this.lblHoursPerDay.Visible = false;
            // 
            // txtHoursPerDay
            // 
            this.txtHoursPerDay.BackColor = System.Drawing.SystemColors.Window;
            this.txtHoursPerDay.Location = new System.Drawing.Point(6, 160);
            this.txtHoursPerDay.MaxLength = 5;
            this.txtHoursPerDay.Name = "txtHoursPerDay";
            this.txtHoursPerDay.ShortcutsEnabled = false;
            this.txtHoursPerDay.Size = new System.Drawing.Size(231, 20);
            this.txtHoursPerDay.TabIndex = 323;
            this.txtHoursPerDay.Visible = false;
            this.txtHoursPerDay.TextChanged += new System.EventHandler(this.ResetForm);
            this.txtHoursPerDay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // lblRate
            // 
            this.lblRate.AutoSize = true;
            this.lblRate.Location = new System.Drawing.Point(6, 276);
            this.lblRate.Name = "lblRate";
            this.lblRate.Size = new System.Drawing.Size(81, 13);
            this.lblRate.TabIndex = 326;
            this.lblRate.Text = "UnEarned Rate";
            // 
            // chkRatePerDay
            // 
            this.chkRatePerDay.AutoSize = true;
            this.chkRatePerDay.Location = new System.Drawing.Point(9, 190);
            this.chkRatePerDay.Name = "chkRatePerDay";
            this.chkRatePerDay.Size = new System.Drawing.Size(123, 17);
            this.chkRatePerDay.TabIndex = 322;
            this.chkRatePerDay.Text = "Rate Based on Hour";
            this.chkRatePerDay.UseVisualStyleBackColor = true;
            this.chkRatePerDay.Visible = false;
            this.chkRatePerDay.CheckedChanged += new System.EventHandler(this.chkRatePerDay_CheckedChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(301, 338);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 333;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(220, 338);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 332;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(7, 338);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 331;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dgvAbsentDetails
            // 
            this.dgvAbsentDetails.AddNewRow = false;
            this.dgvAbsentDetails.AllowUserToAddRows = false;
            this.dgvAbsentDetails.AllowUserToDeleteRows = false;
            this.dgvAbsentDetails.AllowUserToResizeColumns = false;
            this.dgvAbsentDetails.AllowUserToResizeRows = false;
            this.dgvAbsentDetails.AlphaNumericCols = new int[0];
            this.dgvAbsentDetails.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvAbsentDetails.CapsLockCols = new int[0];
            this.dgvAbsentDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAbsentDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colAbsentAddDedID,
            this.colAbsentSelectedItem,
            this.colAbsentParticulars});
            this.dgvAbsentDetails.DecimalCols = new int[0];
            this.dgvAbsentDetails.HasSlNo = false;
            this.dgvAbsentDetails.LastRowIndex = 0;
            this.dgvAbsentDetails.Location = new System.Drawing.Point(126, 101);
            this.dgvAbsentDetails.MultiSelect = false;
            this.dgvAbsentDetails.Name = "dgvAbsentDetails";
            this.dgvAbsentDetails.NegativeValueCols = new int[0];
            this.dgvAbsentDetails.NumericCols = new int[0];
            this.dgvAbsentDetails.RowHeadersWidth = 35;
            this.dgvAbsentDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvAbsentDetails.Size = new System.Drawing.Size(231, 142);
            this.dgvAbsentDetails.TabIndex = 321;
            this.dgvAbsentDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAbsentDetails_CellValueChanged);
            this.dgvAbsentDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvAbsentDetails_CellBeginEdit);
            // 
            // colAbsentAddDedID
            // 
            this.colAbsentAddDedID.HeaderText = "AddDedID";
            this.colAbsentAddDedID.Name = "colAbsentAddDedID";
            this.colAbsentAddDedID.Visible = false;
            // 
            // colAbsentSelectedItem
            // 
            this.colAbsentSelectedItem.HeaderText = "";
            this.colAbsentSelectedItem.Name = "colAbsentSelectedItem";
            this.colAbsentSelectedItem.Width = 30;
            // 
            // colAbsentParticulars
            // 
            this.colAbsentParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colAbsentParticulars.HeaderText = "Particulars";
            this.colAbsentParticulars.Name = "colAbsentParticulars";
            this.colAbsentParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // grpunearned
            // 
            this.grpunearned.Controls.Add(DescriptionLabel);
            this.grpunearned.Controls.Add(this.txtPolicyName);
            this.grpunearned.Controls.Add(Label4);
            this.grpunearned.Controls.Add(this.rdbCompanyBased);
            this.grpunearned.Controls.Add(this.rdbActualMonth);
            this.grpunearned.Controls.Add(this.lblHeader);
            this.grpunearned.Controls.Add(Label1);
            this.grpunearned.Controls.Add(this.cboCalculationBased);
            this.grpunearned.Controls.Add(Label2);
            this.grpunearned.Controls.Add(this.dgvAbsentDetails);
            this.grpunearned.Controls.Add(this.chkRatePerDay);
            this.grpunearned.Controls.Add(this.lblHoursPerDay);
            this.grpunearned.Controls.Add(this.txtHoursPerDay);
            this.grpunearned.Controls.Add(this.chkRateOnly);
            this.grpunearned.Controls.Add(this.lblRate);
            this.grpunearned.Controls.Add(this.txtRate);
            this.grpunearned.Location = new System.Drawing.Point(6, 33);
            this.grpunearned.Name = "grpunearned";
            this.grpunearned.Size = new System.Drawing.Size(370, 298);
            this.grpunearned.TabIndex = 334;
            this.grpunearned.TabStop = false;
            // 
            // FrmUnearnedPolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 390);
            this.Controls.Add(this.grpunearned);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.AbsentPolicyBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmUnearnedPolicy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Unearned Policy";
            this.Load += new System.EventHandler(this.FrmUnearnedPolicy_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmUnearnedPolicy_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmUnearnedPolicy_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.AbsentPolicyBindingNavigator)).EndInit();
            this.AbsentPolicyBindingNavigator.ResumeLayout(false);
            this.AbsentPolicyBindingNavigator.PerformLayout();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errAbsentPolicy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAbsentDetails)).EndInit();
            this.grpunearned.ResumeLayout(false);
            this.grpunearned.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator AbsentPolicyBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.Label lblHeader;
        internal System.Windows.Forms.TextBox txtPolicyName;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        private System.Windows.Forms.ErrorProvider errAbsentPolicy;
        private System.Windows.Forms.Timer tmrClear;
        private DemoClsDataGridview.ClsDataGirdView dgvAbsentDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAbsentAddDedID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colAbsentSelectedItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAbsentParticulars;
        internal System.Windows.Forms.ComboBox cboCalculationBased;
        internal System.Windows.Forms.TextBox txtRate;
        internal System.Windows.Forms.CheckBox chkRateOnly;
        internal System.Windows.Forms.RadioButton rdbActualMonth;
        internal System.Windows.Forms.RadioButton rdbCompanyBased;
        internal System.Windows.Forms.Label lblHoursPerDay;
        internal System.Windows.Forms.TextBox txtHoursPerDay;
        internal System.Windows.Forms.Label lblRate;
        internal System.Windows.Forms.CheckBox chkRatePerDay;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox grpunearned;
    }
}