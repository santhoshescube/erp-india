﻿namespace MyBooksERP
{
    partial class FrmSettlementPolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSettlementPolicy));
            this.GrpCSP = new System.Windows.Forms.GroupBox();
            this.tbSettlement = new System.Windows.Forms.TabControl();
            this.TabGratuity = new System.Windows.Forms.TabPage();
            this.TabGrauity = new System.Windows.Forms.TabControl();
            this.TabLessThanFive = new System.Windows.Forms.TabPage();
            this.grdGratuity = new System.Windows.Forms.DataGridView();
            this.ParameterID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.NoOfMonths = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TerminationDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GratuityID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TabGreaterThanFive = new System.Windows.Forms.TabPage();
            this.grdGratuityFive = new System.Windows.Forms.DataGridView();
            this.ParameterIDFive = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.NoOfMonthsFive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfDaysFive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TerminationDaysFive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GratuityIDFive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkIncludeLeave = new System.Windows.Forms.CheckBox();
            this.DetailsDataGridViewGratuity = new DemoClsDataGridview.ClsDataGirdView();
            this.txtAddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkParticular = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.txtParticulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblCalcGratuity = new System.Windows.Forms.Label();
            this.lblGrossGratuity = new System.Windows.Forms.Label();
            this.cboCalculationBasedGratuity = new System.Windows.Forms.ComboBox();
            this.txtPolicyName = new System.Windows.Forms.TextBox();
            this.CompanyIDLabel = new System.Windows.Forms.Label();
            this.txtIntText = new System.Windows.Forms.TextBox();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lblAmount = new System.Windows.Forms.Label();
            this.txtRateGratuity = new System.Windows.Forms.TextBox();
            this.chkRateOnlyGratuity = new System.Windows.Forms.CheckBox();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnBottomCancel = new System.Windows.Forms.Button();
            this.OKSaveButton = new System.Windows.Forms.Button();
            this.PolicyIDTextBox = new System.Windows.Forms.TextBox();
            this.TextboxNumeric = new System.Windows.Forms.TextBox();
            this.CompanySettlementPolicyBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BtnCancel = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.bnHelp = new System.Windows.Forms.ToolStripButton();
            this.StatusStripSettlement = new System.Windows.Forms.StatusStrip();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.ErrSettlement = new System.Windows.Forms.ErrorProvider(this.components);
            this.TmrSettlement = new System.Windows.Forms.Timer(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            Label2 = new System.Windows.Forms.Label();
            this.GrpCSP.SuspendLayout();
            this.tbSettlement.SuspendLayout();
            this.TabGratuity.SuspendLayout();
            this.TabGrauity.SuspendLayout();
            this.TabLessThanFive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuity)).BeginInit();
            this.TabGreaterThanFive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuityFive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailsDataGridViewGratuity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompanySettlementPolicyBindingNavigator)).BeginInit();
            this.CompanySettlementPolicyBindingNavigator.SuspendLayout();
            this.StatusStripSettlement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrSettlement)).BeginInit();
            this.SuspendLayout();
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label2.Location = new System.Drawing.Point(4, -2);
            Label2.Name = "Label2";
            Label2.Size = new System.Drawing.Size(51, 13);
            Label2.TabIndex = 0;
            Label2.Text = "General";
            // 
            // GrpCSP
            // 
            this.GrpCSP.Controls.Add(this.tbSettlement);
            this.GrpCSP.Controls.Add(this.txtPolicyName);
            this.GrpCSP.Controls.Add(this.CompanyIDLabel);
            this.GrpCSP.Controls.Add(Label2);
            this.GrpCSP.Location = new System.Drawing.Point(0, 28);
            this.GrpCSP.Name = "GrpCSP";
            this.GrpCSP.Size = new System.Drawing.Size(437, 395);
            this.GrpCSP.TabIndex = 1;
            this.GrpCSP.TabStop = false;
            // 
            // tbSettlement
            // 
            this.tbSettlement.Controls.Add(this.TabGratuity);
            this.tbSettlement.Location = new System.Drawing.Point(12, 41);
            this.tbSettlement.Name = "tbSettlement";
            this.tbSettlement.SelectedIndex = 0;
            this.tbSettlement.Size = new System.Drawing.Size(416, 348);
            this.tbSettlement.TabIndex = 11;
            // 
            // TabGratuity
            // 
            this.TabGratuity.Controls.Add(this.TabGrauity);
            this.TabGratuity.Controls.Add(this.chkIncludeLeave);
            this.TabGratuity.Controls.Add(this.DetailsDataGridViewGratuity);
            this.TabGratuity.Controls.Add(this.lblCalcGratuity);
            this.TabGratuity.Controls.Add(this.lblGrossGratuity);
            this.TabGratuity.Controls.Add(this.cboCalculationBasedGratuity);
            this.TabGratuity.Location = new System.Drawing.Point(4, 22);
            this.TabGratuity.Name = "TabGratuity";
            this.TabGratuity.Padding = new System.Windows.Forms.Padding(3);
            this.TabGratuity.Size = new System.Drawing.Size(408, 322);
            this.TabGratuity.TabIndex = 0;
            this.TabGratuity.Text = "Gratuity";
            this.TabGratuity.UseVisualStyleBackColor = true;
            // 
            // TabGrauity
            // 
            this.TabGrauity.Controls.Add(this.TabLessThanFive);
            this.TabGrauity.Controls.Add(this.TabGreaterThanFive);
            this.TabGrauity.Location = new System.Drawing.Point(3, 152);
            this.TabGrauity.Name = "TabGrauity";
            this.TabGrauity.SelectedIndex = 0;
            this.TabGrauity.Size = new System.Drawing.Size(408, 167);
            this.TabGrauity.TabIndex = 48;
            // 
            // TabLessThanFive
            // 
            this.TabLessThanFive.Controls.Add(this.grdGratuity);
            this.TabLessThanFive.Location = new System.Drawing.Point(4, 22);
            this.TabLessThanFive.Name = "TabLessThanFive";
            this.TabLessThanFive.Padding = new System.Windows.Forms.Padding(3);
            this.TabLessThanFive.Size = new System.Drawing.Size(400, 141);
            this.TabLessThanFive.TabIndex = 0;
            this.TabLessThanFive.Text = "Less Than Five";
            this.TabLessThanFive.UseVisualStyleBackColor = true;
            // 
            // grdGratuity
            // 
            this.grdGratuity.AllowUserToResizeColumns = false;
            this.grdGratuity.AllowUserToResizeRows = false;
            this.grdGratuity.BackgroundColor = System.Drawing.Color.White;
            this.grdGratuity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdGratuity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ParameterID,
            this.NoOfMonths,
            this.NoOfDays,
            this.TerminationDays,
            this.GratuityID});
            this.grdGratuity.Location = new System.Drawing.Point(-7, 3);
            this.grdGratuity.Name = "grdGratuity";
            this.grdGratuity.RowHeadersWidth = 25;
            this.grdGratuity.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdGratuity.Size = new System.Drawing.Size(415, 138);
            this.grdGratuity.TabIndex = 2;
            this.grdGratuity.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdGratuity_CellValueChanged);
            this.grdGratuity.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdGratuity_CellBeginEdit);
            this.grdGratuity.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.grdGratuity_UserDeletedRow);
            //this.grdGratuity.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdGratuity_CellValidating);
            this.grdGratuity.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdGratuity_CellEndEdit);
            this.grdGratuity.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grdGratuity_EditingControlShowing);
            this.grdGratuity.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdGratuity_DataError);
            // 
            // ParameterID
            // 
            this.ParameterID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ParameterID.HeaderText = "Parameter";
            this.ParameterID.Name = "ParameterID";
            this.ParameterID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ParameterID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ParameterID.Width = 120;
            // 
            // NoOfMonths
            // 
            this.NoOfMonths.HeaderText = "NoOfMonths";
            this.NoOfMonths.MaxInputLength = 5;
            this.NoOfMonths.Name = "NoOfMonths";
            // 
            // NoOfDays
            // 
            this.NoOfDays.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NoOfDays.HeaderText = "NoOfDays";
            this.NoOfDays.MaxInputLength = 5;
            this.NoOfDays.MinimumWidth = 80;
            this.NoOfDays.Name = "NoOfDays";
            // 
            // TerminationDays
            // 
            this.TerminationDays.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TerminationDays.HeaderText = "Termination Days";
            this.TerminationDays.MinimumWidth = 80;
            this.TerminationDays.Name = "TerminationDays";
            // 
            // GratuityID
            // 
            this.GratuityID.HeaderText = "GratuityID";
            this.GratuityID.Name = "GratuityID";
            this.GratuityID.Visible = false;
            this.GratuityID.Width = 5;
            // 
            // TabGreaterThanFive
            // 
            this.TabGreaterThanFive.Controls.Add(this.grdGratuityFive);
            this.TabGreaterThanFive.Location = new System.Drawing.Point(4, 22);
            this.TabGreaterThanFive.Name = "TabGreaterThanFive";
            this.TabGreaterThanFive.Padding = new System.Windows.Forms.Padding(3);
            this.TabGreaterThanFive.Size = new System.Drawing.Size(400, 141);
            this.TabGreaterThanFive.TabIndex = 1;
            this.TabGreaterThanFive.Text = "Greater Than Five";
            this.TabGreaterThanFive.UseVisualStyleBackColor = true;
            // 
            // grdGratuityFive
            // 
            this.grdGratuityFive.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.grdGratuityFive.AllowUserToResizeColumns = false;
            this.grdGratuityFive.AllowUserToResizeRows = false;
            this.grdGratuityFive.BackgroundColor = System.Drawing.Color.White;
            this.grdGratuityFive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdGratuityFive.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ParameterIDFive,
            this.NoOfMonthsFive,
            this.NoOfDaysFive,
            this.TerminationDaysFive,
            this.GratuityIDFive});
            this.grdGratuityFive.Location = new System.Drawing.Point(-1, 2);
            this.grdGratuityFive.Name = "grdGratuityFive";
            this.grdGratuityFive.RowHeadersWidth = 25;
            this.grdGratuityFive.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdGratuityFive.Size = new System.Drawing.Size(409, 139);
            this.grdGratuityFive.TabIndex = 3;
            this.grdGratuityFive.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdGratuityFive_CellBeginEdit);
            this.grdGratuityFive.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdGratuityFive_CellEndEdit);
            this.grdGratuityFive.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grdGratuityFive_EditingControlShowing);
            this.grdGratuityFive.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdGratuityFive_DataError);
            // 
            // ParameterIDFive
            // 
            this.ParameterIDFive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ParameterIDFive.HeaderText = "Parameter";
            this.ParameterIDFive.Name = "ParameterIDFive";
            this.ParameterIDFive.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ParameterIDFive.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ParameterIDFive.Width = 120;
            // 
            // NoOfMonthsFive
            // 
            this.NoOfMonthsFive.HeaderText = "NoOfMonths";
            this.NoOfMonthsFive.MaxInputLength = 5;
            this.NoOfMonthsFive.Name = "NoOfMonthsFive";
            // 
            // NoOfDaysFive
            // 
            this.NoOfDaysFive.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NoOfDaysFive.HeaderText = "NoOfDays";
            this.NoOfDaysFive.MaxInputLength = 5;
            this.NoOfDaysFive.MinimumWidth = 80;
            this.NoOfDaysFive.Name = "NoOfDaysFive";
            // 
            // TerminationDaysFive
            // 
            this.TerminationDaysFive.HeaderText = "Termination Days";
            this.TerminationDaysFive.MinimumWidth = 80;
            this.TerminationDaysFive.Name = "TerminationDaysFive";
            this.TerminationDaysFive.Width = 80;
            // 
            // GratuityIDFive
            // 
            this.GratuityIDFive.HeaderText = "GratuityID";
            this.GratuityIDFive.Name = "GratuityIDFive";
            this.GratuityIDFive.Visible = false;
            // 
            // chkIncludeLeave
            // 
            this.chkIncludeLeave.AutoSize = true;
            this.chkIncludeLeave.Location = new System.Drawing.Point(10, 117);
            this.chkIncludeLeave.Name = "chkIncludeLeave";
            this.chkIncludeLeave.Size = new System.Drawing.Size(130, 17);
            this.chkIncludeLeave.TabIndex = 47;
            this.chkIncludeLeave.Text = "Include Eligible Leave";
            this.chkIncludeLeave.UseVisualStyleBackColor = true;
            // 
            // DetailsDataGridViewGratuity
            // 
            this.DetailsDataGridViewGratuity.AddNewRow = false;
            this.DetailsDataGridViewGratuity.AllowUserToAddRows = false;
            this.DetailsDataGridViewGratuity.AllowUserToDeleteRows = false;
            this.DetailsDataGridViewGratuity.AllowUserToResizeColumns = false;
            this.DetailsDataGridViewGratuity.AllowUserToResizeRows = false;
            this.DetailsDataGridViewGratuity.AlphaNumericCols = new int[0];
            this.DetailsDataGridViewGratuity.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DetailsDataGridViewGratuity.CapsLockCols = new int[0];
            this.DetailsDataGridViewGratuity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DetailsDataGridViewGratuity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtAddDedID,
            this.chkParticular,
            this.txtParticulars});
            this.DetailsDataGridViewGratuity.DecimalCols = new int[0];
            this.DetailsDataGridViewGratuity.HasSlNo = false;
            this.DetailsDataGridViewGratuity.LastRowIndex = 0;
            this.DetailsDataGridViewGratuity.Location = new System.Drawing.Point(143, 37);
            this.DetailsDataGridViewGratuity.MultiSelect = false;
            this.DetailsDataGridViewGratuity.Name = "DetailsDataGridViewGratuity";
            this.DetailsDataGridViewGratuity.NegativeValueCols = new int[0];
            this.DetailsDataGridViewGratuity.NumericCols = new int[0];
            this.DetailsDataGridViewGratuity.RowHeadersWidth = 35;
            this.DetailsDataGridViewGratuity.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DetailsDataGridViewGratuity.Size = new System.Drawing.Size(253, 106);
            this.DetailsDataGridViewGratuity.TabIndex = 1;
            this.DetailsDataGridViewGratuity.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DetailsDataGridViewGratuity_CellBeginEdit);
            this.DetailsDataGridViewGratuity.CurrentCellDirtyStateChanged += new System.EventHandler(this.DetailsDataGridViewGratuity_CurrentCellDirtyStateChanged);
            this.DetailsDataGridViewGratuity.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DetailsDataGridViewGratuity_DataError);
            this.DetailsDataGridViewGratuity.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DetailsDataGridViewGratuity_CellContentClick);
            // 
            // txtAddDedID
            // 
            this.txtAddDedID.HeaderText = "txtAddDedID";
            this.txtAddDedID.Name = "txtAddDedID";
            this.txtAddDedID.Visible = false;
            // 
            // chkParticular
            // 
            this.chkParticular.HeaderText = "";
            this.chkParticular.Name = "chkParticular";
            this.chkParticular.Width = 30;
            // 
            // txtParticulars
            // 
            this.txtParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtParticulars.HeaderText = "Particulars";
            this.txtParticulars.Name = "txtParticulars";
            this.txtParticulars.ReadOnly = true;
            this.txtParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // lblCalcGratuity
            // 
            this.lblCalcGratuity.AutoSize = true;
            this.lblCalcGratuity.Location = new System.Drawing.Point(6, 11);
            this.lblCalcGratuity.Name = "lblCalcGratuity";
            this.lblCalcGratuity.Size = new System.Drawing.Size(107, 13);
            this.lblCalcGratuity.TabIndex = 45;
            this.lblCalcGratuity.Text = "Calculation Based on";
            // 
            // lblGrossGratuity
            // 
            this.lblGrossGratuity.AutoSize = true;
            this.lblGrossGratuity.Location = new System.Drawing.Point(6, 37);
            this.lblGrossGratuity.Name = "lblGrossGratuity";
            this.lblGrossGratuity.Size = new System.Drawing.Size(101, 13);
            this.lblGrossGratuity.TabIndex = 46;
            this.lblGrossGratuity.Text = "Exclude from Gross ";
            // 
            // cboCalculationBasedGratuity
            // 
            this.cboCalculationBasedGratuity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCalculationBasedGratuity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCalculationBasedGratuity.BackColor = System.Drawing.SystemColors.Info;
            this.cboCalculationBasedGratuity.DropDownHeight = 134;
            this.cboCalculationBasedGratuity.FormattingEnabled = true;
            this.cboCalculationBasedGratuity.IntegralHeight = false;
            this.cboCalculationBasedGratuity.Location = new System.Drawing.Point(143, 8);
            this.cboCalculationBasedGratuity.MaxDropDownItems = 10;
            this.cboCalculationBasedGratuity.Name = "cboCalculationBasedGratuity";
            this.cboCalculationBasedGratuity.Size = new System.Drawing.Size(253, 21);
            this.cboCalculationBasedGratuity.TabIndex = 0;
            this.cboCalculationBasedGratuity.SelectedIndexChanged += new System.EventHandler(this.cboCalculationBasedGratuity_SelectedIndexChanged);
            this.cboCalculationBasedGratuity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCalculationBasedGratuity_KeyPress);
            // 
            // txtPolicyName
            // 
            this.txtPolicyName.BackColor = System.Drawing.SystemColors.Info;
            this.txtPolicyName.Location = new System.Drawing.Point(121, 15);
            this.txtPolicyName.MaxLength = 30;
            this.txtPolicyName.Name = "txtPolicyName";
            this.txtPolicyName.Size = new System.Drawing.Size(274, 20);
            this.txtPolicyName.TabIndex = 0;
            this.txtPolicyName.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // CompanyIDLabel
            // 
            this.CompanyIDLabel.AutoSize = true;
            this.CompanyIDLabel.Location = new System.Drawing.Point(6, 18);
            this.CompanyIDLabel.Name = "CompanyIDLabel";
            this.CompanyIDLabel.Size = new System.Drawing.Size(60, 13);
            this.CompanyIDLabel.TabIndex = 21;
            this.CompanyIDLabel.Text = "Description";
            // 
            // txtIntText
            // 
            this.txtIntText.Location = new System.Drawing.Point(443, 445);
            this.txtIntText.Name = "txtIntText";
            this.txtIntText.Size = new System.Drawing.Size(100, 20);
            this.txtIntText.TabIndex = 11;
            this.txtIntText.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIntText.Visible = false;
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 53;
            this.LineShape1.X2 = 370;
            this.LineShape1.Y1 = 300;
            this.LineShape1.Y2 = 300;
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(106, 454);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(43, 13);
            this.lblAmount.TabIndex = 47;
            this.lblAmount.Text = "Amount";
            this.lblAmount.Visible = false;
            // 
            // txtRateGratuity
            // 
            this.txtRateGratuity.Location = new System.Drawing.Point(109, 476);
            this.txtRateGratuity.MaxLength = 9;
            this.txtRateGratuity.Name = "txtRateGratuity";
            this.txtRateGratuity.ShortcutsEnabled = false;
            this.txtRateGratuity.Size = new System.Drawing.Size(138, 20);
            this.txtRateGratuity.TabIndex = 3;
            this.txtRateGratuity.Visible = false;
            this.txtRateGratuity.TextChanged += new System.EventHandler(this.ChangeStatus);
            this.txtRateGratuity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRateGratuity_KeyPress);
            // 
            // chkRateOnlyGratuity
            // 
            this.chkRateOnlyGratuity.AutoSize = true;
            this.chkRateOnlyGratuity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRateOnlyGratuity.Location = new System.Drawing.Point(155, 478);
            this.chkRateOnlyGratuity.Name = "chkRateOnlyGratuity";
            this.chkRateOnlyGratuity.Size = new System.Drawing.Size(56, 17);
            this.chkRateOnlyGratuity.TabIndex = 38;
            this.chkRateOnlyGratuity.Text = "Fixed";
            this.chkRateOnlyGratuity.UseVisualStyleBackColor = true;
            this.chkRateOnlyGratuity.Visible = false;
            this.chkRateOnlyGratuity.Click += new System.EventHandler(this.ChangeStatus);
            this.chkRateOnlyGratuity.CheckedChanged += new System.EventHandler(this.chkRateOnlyGratuity_CheckedChanged);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(6, 429);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 0;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnBottomCancel
            // 
            this.BtnBottomCancel.Location = new System.Drawing.Point(362, 429);
            this.BtnBottomCancel.Name = "BtnBottomCancel";
            this.BtnBottomCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnBottomCancel.TabIndex = 2;
            this.BtnBottomCancel.Text = "&Cancel";
            this.BtnBottomCancel.UseVisualStyleBackColor = true;
            this.BtnBottomCancel.Click += new System.EventHandler(this.BtnBottomCancel_Click);
            // 
            // OKSaveButton
            // 
            this.OKSaveButton.Location = new System.Drawing.Point(281, 429);
            this.OKSaveButton.Name = "OKSaveButton";
            this.OKSaveButton.Size = new System.Drawing.Size(75, 23);
            this.OKSaveButton.TabIndex = 1;
            this.OKSaveButton.Text = "&Ok";
            this.OKSaveButton.UseVisualStyleBackColor = true;
            this.OKSaveButton.Click += new System.EventHandler(this.OKSaveButton_Click);
            // 
            // PolicyIDTextBox
            // 
            this.PolicyIDTextBox.Location = new System.Drawing.Point(541, 283);
            this.PolicyIDTextBox.Name = "PolicyIDTextBox";
            this.PolicyIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.PolicyIDTextBox.TabIndex = 12;
            this.PolicyIDTextBox.Visible = false;
            // 
            // TextboxNumeric
            // 
            this.TextboxNumeric.Location = new System.Drawing.Point(541, 250);
            this.TextboxNumeric.Name = "TextboxNumeric";
            this.TextboxNumeric.Size = new System.Drawing.Size(100, 20);
            this.TextboxNumeric.TabIndex = 11;
            this.TextboxNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TextboxNumeric.Visible = false;
            // 
            // CompanySettlementPolicyBindingNavigator
            // 
            this.CompanySettlementPolicyBindingNavigator.AddNewItem = null;
            this.CompanySettlementPolicyBindingNavigator.CountItem = null;
            this.CompanySettlementPolicyBindingNavigator.DeleteItem = null;
            this.CompanySettlementPolicyBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripSeparator4,
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorPositionItem,
            this.bnCountItem,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.ToolStripSeparator5,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BtnCancel,
            this.ToolStripSeparator6,
            this.bnHelp});
            this.CompanySettlementPolicyBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.CompanySettlementPolicyBindingNavigator.MoveFirstItem = null;
            this.CompanySettlementPolicyBindingNavigator.MoveLastItem = null;
            this.CompanySettlementPolicyBindingNavigator.MoveNextItem = null;
            this.CompanySettlementPolicyBindingNavigator.MovePreviousItem = null;
            this.CompanySettlementPolicyBindingNavigator.Name = "CompanySettlementPolicyBindingNavigator";
            this.CompanySettlementPolicyBindingNavigator.PositionItem = null;
            this.CompanySettlementPolicyBindingNavigator.Size = new System.Drawing.Size(441, 25);
            this.CompanySettlementPolicyBindingNavigator.TabIndex = 19;
            this.CompanySettlementPolicyBindingNavigator.Text = "BindingNavigator1";
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Enabled = false;
            this.BindingNavigatorPositionItem.MaxLength = 12;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.Size = new System.Drawing.Size(35, 22);
            this.bnCountItem.Text = "of {0}";
            this.bnCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save Data";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.CompanySettlementPolicyBindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnCancel.Image = ((System.Drawing.Image)(resources.GetObject("BtnCancel.Image")));
            this.BtnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(23, 22);
            this.BtnCancel.ToolTipText = "Clear";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // ToolStripSeparator6
            // 
            this.ToolStripSeparator6.Name = "ToolStripSeparator6";
            this.ToolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // bnHelp
            // 
            this.bnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnHelp.Image = ((System.Drawing.Image)(resources.GetObject("bnHelp.Image")));
            this.bnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnHelp.Name = "bnHelp";
            this.bnHelp.Size = new System.Drawing.Size(23, 22);
            this.bnHelp.Text = "He&lp";
            this.bnHelp.Click += new System.EventHandler(this.bnHelp_Click);
            // 
            // StatusStripSettlement
            // 
            this.StatusStripSettlement.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblstatus});
            this.StatusStripSettlement.Location = new System.Drawing.Point(0, 455);
            this.StatusStripSettlement.Name = "StatusStripSettlement";
            this.StatusStripSettlement.Size = new System.Drawing.Size(441, 22);
            this.StatusStripSettlement.TabIndex = 20;
            this.StatusStripSettlement.Text = "StatusStrip1";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // ErrSettlement
            // 
            this.ErrSettlement.ContainerControl = this;
            this.ErrSettlement.RightToLeft = true;
            // 
            // TmrSettlement
            // 
            this.TmrSettlement.Tick += new System.EventHandler(this.TmrSettlement_Tick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "NoOfMonths";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "NoOfDays";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "GratuityID";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // FrmSettlementPolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 477);
            this.Controls.Add(this.chkRateOnlyGratuity);
            this.Controls.Add(this.StatusStripSettlement);
            this.Controls.Add(this.lblAmount);
            this.Controls.Add(this.CompanySettlementPolicyBindingNavigator);
            this.Controls.Add(this.PolicyIDTextBox);
            this.Controls.Add(this.txtIntText);
            this.Controls.Add(this.TextboxNumeric);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BtnBottomCancel);
            this.Controls.Add(this.txtRateGratuity);
            this.Controls.Add(this.OKSaveButton);
            this.Controls.Add(this.GrpCSP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSettlementPolicy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settlement Policy";
            this.Load += new System.EventHandler(this.FrmSettlementPolicy_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSettlementPolicy_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmSettlementPolicy_KeyDown);
            this.GrpCSP.ResumeLayout(false);
            this.GrpCSP.PerformLayout();
            this.tbSettlement.ResumeLayout(false);
            this.TabGratuity.ResumeLayout(false);
            this.TabGratuity.PerformLayout();
            this.TabGrauity.ResumeLayout(false);
            this.TabLessThanFive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuity)).EndInit();
            this.TabGreaterThanFive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuityFive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailsDataGridViewGratuity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompanySettlementPolicyBindingNavigator)).EndInit();
            this.CompanySettlementPolicyBindingNavigator.ResumeLayout(false);
            this.CompanySettlementPolicyBindingNavigator.PerformLayout();
            this.StatusStripSettlement.ResumeLayout(false);
            this.StatusStripSettlement.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrSettlement)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.GroupBox GrpCSP;
        internal System.Windows.Forms.TabControl tbSettlement;
        internal System.Windows.Forms.TabPage TabGratuity;
        internal System.Windows.Forms.Label lblAmount;
        internal System.Windows.Forms.Label lblCalcGratuity;
        internal System.Windows.Forms.Label lblGrossGratuity;
        internal System.Windows.Forms.TextBox txtIntText;
        internal System.Windows.Forms.ComboBox cboCalculationBasedGratuity;
        internal System.Windows.Forms.TextBox txtRateGratuity;
        internal System.Windows.Forms.CheckBox chkRateOnlyGratuity;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.TextBox txtPolicyName;
        internal System.Windows.Forms.Label CompanyIDLabel;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Button BtnBottomCancel;
        internal System.Windows.Forms.Button OKSaveButton;
        internal System.Windows.Forms.TextBox PolicyIDTextBox;
        internal System.Windows.Forms.TextBox TextboxNumeric;
        internal System.Windows.Forms.BindingNavigator CompanySettlementPolicyBindingNavigator;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripLabel bnCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator6;
        internal System.Windows.Forms.ToolStripButton BtnCancel;
        private DemoClsDataGridview.ClsDataGirdView DetailsDataGridViewGratuity;
        private System.Windows.Forms.DataGridView grdGratuity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        internal System.Windows.Forms.StatusStrip StatusStripSettlement;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.ErrorProvider ErrSettlement;
        internal System.Windows.Forms.Timer TmrSettlement;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtAddDedID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkParticular;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtParticulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        internal System.Windows.Forms.ToolStripButton bnHelp;
        private System.Windows.Forms.CheckBox chkIncludeLeave;
        private System.Windows.Forms.TabControl TabGrauity;
        private System.Windows.Forms.TabPage TabLessThanFive;
        private System.Windows.Forms.TabPage TabGreaterThanFive;
        private System.Windows.Forms.DataGridView grdGratuityFive;
        private System.Windows.Forms.DataGridViewComboBoxColumn ParameterIDFive;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfMonthsFive;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfDaysFive;
        private System.Windows.Forms.DataGridViewTextBoxColumn TerminationDaysFive;
        private System.Windows.Forms.DataGridViewTextBoxColumn GratuityIDFive;
        private System.Windows.Forms.DataGridViewComboBoxColumn ParameterID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfMonths;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn TerminationDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn GratuityID;
    }
}