﻿namespace MyBooksERP
{
    partial class FrmDeductionPolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDeductionPolicy));
            this.chkRateOnly = new System.Windows.Forms.CheckBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.DTPWithEffect = new System.Windows.Forms.DateTimePicker();
            this.Label4 = new System.Windows.Forms.Label();
            this.CboParameter = new System.Windows.Forms.ComboBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.DeductionPolicyBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.ProjectsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BtnCancel = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripbtnHelp = new System.Windows.Forms.ToolStripButton();
            this.CboAdditions = new System.Windows.Forms.ComboBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.DeductionPolicyErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.Txtpolicyname = new System.Windows.Forms.TextBox();
            this.DeductionPolicyTimer = new System.Windows.Forms.Timer(this.components);
            this.lblAmount = new System.Windows.Forms.Label();
            this.BtnSave = new System.Windows.Forms.Button();
            this.ProjectCreationStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.Label1 = new System.Windows.Forms.Label();
            this.DeductionPolicyStatusStrip = new System.Windows.Forms.StatusStrip();
            this.BtnBottomCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.TabAddDedpolicy = new System.Windows.Forms.TabControl();
            this.tbpgeDeduction = new System.Windows.Forms.TabPage();
            this.dgvDeductionDetails = new System.Windows.Forms.DataGridView();
            this.DAddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DSelectValue = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DParticulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TxtEmpPer = new System.Windows.Forms.TextBox();
            this.TxtEmprPer = new System.Windows.Forms.TextBox();
            this.lblEmp = new System.Windows.Forms.Label();
            this.lblEmper = new System.Windows.Forms.Label();
            this.cboParticulars = new System.Windows.Forms.ComboBox();
            this.LblParticulars = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.TxtAmount = new System.Windows.Forms.TextBox();
            this.GrpDeductionPolicy = new System.Windows.Forms.GroupBox();
            this.gbSlab = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdbFixedAmount = new System.Windows.Forms.RadioButton();
            this.rdbParticulars = new System.Windows.Forms.RadioButton();
            this.rdbRateOnly = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFixedAmount = new System.Windows.Forms.TextBox();
            this.txtAdditionAmount = new System.Windows.Forms.TextBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DeductionPolicyBindingNavigator)).BeginInit();
            this.DeductionPolicyBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DeductionPolicyErrorProvider)).BeginInit();
            this.DeductionPolicyStatusStrip.SuspendLayout();
            this.TabAddDedpolicy.SuspendLayout();
            this.tbpgeDeduction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeductionDetails)).BeginInit();
            this.GrpDeductionPolicy.SuspendLayout();
            this.gbSlab.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkRateOnly
            // 
            this.chkRateOnly.AutoSize = true;
            this.chkRateOnly.Location = new System.Drawing.Point(631, 231);
            this.chkRateOnly.Name = "chkRateOnly";
            this.chkRateOnly.Size = new System.Drawing.Size(73, 17);
            this.chkRateOnly.TabIndex = 2;
            this.chkRateOnly.Text = "Rate Only";
            this.chkRateOnly.UseVisualStyleBackColor = true;
            this.chkRateOnly.CheckStateChanged += new System.EventHandler(this.chkRateOnly_CheckStateChanged);
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(321, 19);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(61, 13);
            this.Label7.TabIndex = 33;
            this.Label7.Text = "Effect From";
            // 
            // DTPWithEffect
            // 
            this.DTPWithEffect.CustomFormat = "MMM-yyyy";
            this.DTPWithEffect.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTPWithEffect.Location = new System.Drawing.Point(414, 16);
            this.DTPWithEffect.Name = "DTPWithEffect";
            this.DTPWithEffect.Size = new System.Drawing.Size(114, 20);
            this.DTPWithEffect.TabIndex = 1;
            this.DTPWithEffect.ValueChanged += new System.EventHandler(this.DTPWithEffect_ValueChanged);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(14, 40);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(85, 13);
            this.Label4.TabIndex = 16;
            this.Label4.Text = "Parameter Value";
            // 
            // CboParameter
            // 
            this.CboParameter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboParameter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboParameter.BackColor = System.Drawing.SystemColors.Info;
            this.CboParameter.DropDownHeight = 134;
            this.CboParameter.FormattingEnabled = true;
            this.CboParameter.IntegralHeight = false;
            this.CboParameter.Location = new System.Drawing.Point(107, 39);
            this.CboParameter.Name = "CboParameter";
            this.CboParameter.Size = new System.Drawing.Size(166, 21);
            this.CboParameter.TabIndex = 5;
            this.CboParameter.SelectedIndexChanged += new System.EventHandler(this.CboParameter_SelectedIndexChanged);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(6, 99);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(0, 13);
            this.Label3.TabIndex = 13;
            // 
            // DeductionPolicyBindingNavigator
            // 
            this.DeductionPolicyBindingNavigator.AddNewItem = null;
            this.DeductionPolicyBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.DeductionPolicyBindingNavigator.DeleteItem = null;
            this.DeductionPolicyBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorDeleteItem,
            this.ProjectsBindingNavigatorSaveItem,
            this.BtnCancel,
            this.ToolStripSeparator1,
            this.BtnPrint,
            this.BtnEmail,
            this.ToolStripSeparator2,
            this.ToolStripbtnHelp});
            this.DeductionPolicyBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.DeductionPolicyBindingNavigator.MoveFirstItem = null;
            this.DeductionPolicyBindingNavigator.MoveLastItem = null;
            this.DeductionPolicyBindingNavigator.MoveNextItem = null;
            this.DeductionPolicyBindingNavigator.MovePreviousItem = null;
            this.DeductionPolicyBindingNavigator.Name = "DeductionPolicyBindingNavigator";
            this.DeductionPolicyBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.DeductionPolicyBindingNavigator.Size = new System.Drawing.Size(600, 25);
            this.DeductionPolicyBindingNavigator.TabIndex = 35;
            this.DeductionPolicyBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Enabled = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // ProjectsBindingNavigatorSaveItem
            // 
            this.ProjectsBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ProjectsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("ProjectsBindingNavigatorSaveItem.Image")));
            this.ProjectsBindingNavigatorSaveItem.Name = "ProjectsBindingNavigatorSaveItem";
            this.ProjectsBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.ProjectsBindingNavigatorSaveItem.Text = "Save Data";
            this.ProjectsBindingNavigatorSaveItem.Click += new System.EventHandler(this.ProjectsBindingNavigatorSaveItem_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnCancel.Image = ((System.Drawing.Image)(resources.GetObject("BtnCancel.Image")));
            this.BtnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(23, 22);
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.ToolTipText = "Clear";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "ToolStripButton1";
            this.BtnPrint.ToolTipText = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "ToolStripButton2";
            this.BtnEmail.ToolTipText = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStripbtnHelp
            // 
            this.ToolStripbtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripbtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripbtnHelp.Image")));
            this.ToolStripbtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripbtnHelp.Name = "ToolStripbtnHelp";
            this.ToolStripbtnHelp.Size = new System.Drawing.Size(23, 22);
            this.ToolStripbtnHelp.Text = "He&lp";
            this.ToolStripbtnHelp.Click += new System.EventHandler(this.ToolStripbtnHelp_Click);
            // 
            // CboAdditions
            // 
            this.CboAdditions.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboAdditions.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboAdditions.BackColor = System.Drawing.SystemColors.Info;
            this.CboAdditions.DropDownHeight = 134;
            this.CboAdditions.FormattingEnabled = true;
            this.CboAdditions.IntegralHeight = false;
            this.CboAdditions.Location = new System.Drawing.Point(107, 16);
            this.CboAdditions.Name = "CboAdditions";
            this.CboAdditions.Size = new System.Drawing.Size(166, 21);
            this.CboAdditions.TabIndex = 4;
            this.CboAdditions.SelectedIndexChanged += new System.EventHandler(this.CboAdditions_SelectedIndexChanged);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(14, 17);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(86, 13);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "Parameter Name";
            // 
            // DeductionPolicyErrorProvider
            // 
            this.DeductionPolicyErrorProvider.ContainerControl = this;
            this.DeductionPolicyErrorProvider.RightToLeft = true;
            // 
            // Txtpolicyname
            // 
            this.Txtpolicyname.BackColor = System.Drawing.SystemColors.Info;
            this.Txtpolicyname.Location = new System.Drawing.Point(87, 16);
            this.Txtpolicyname.MaxLength = 50;
            this.Txtpolicyname.Name = "Txtpolicyname";
            this.Txtpolicyname.Size = new System.Drawing.Size(213, 20);
            this.Txtpolicyname.TabIndex = 0;
            this.Txtpolicyname.TextChanged += new System.EventHandler(this.Txtpolicyname_TextChanged);
            // 
            // DeductionPolicyTimer
            // 
            this.DeductionPolicyTimer.Interval = 2000;
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(134, 474);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(84, 13);
            this.lblAmount.TabIndex = 41;
            this.lblAmount.Text = "Addition Amount";
            this.lblAmount.Visible = false;
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(18, 472);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 15;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // ProjectCreationStatusLabel
            // 
            this.ProjectCreationStatusLabel.Name = "ProjectCreationStatusLabel";
            this.ProjectCreationStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(12, 19);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(66, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Policy Name";
            // 
            // DeductionPolicyStatusStrip
            // 
            this.DeductionPolicyStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProjectCreationStatusLabel});
            this.DeductionPolicyStatusStrip.Location = new System.Drawing.Point(0, 508);
            this.DeductionPolicyStatusStrip.Name = "DeductionPolicyStatusStrip";
            this.DeductionPolicyStatusStrip.Size = new System.Drawing.Size(600, 22);
            this.DeductionPolicyStatusStrip.TabIndex = 40;
            this.DeductionPolicyStatusStrip.Text = "StatusStrip1";
            // 
            // BtnBottomCancel
            // 
            this.BtnBottomCancel.Location = new System.Drawing.Point(513, 474);
            this.BtnBottomCancel.Name = "BtnBottomCancel";
            this.BtnBottomCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnBottomCancel.TabIndex = 17;
            this.BtnBottomCancel.Text = "&Cancel";
            this.BtnBottomCancel.UseVisualStyleBackColor = true;
            this.BtnBottomCancel.Click += new System.EventHandler(this.BtnBottomCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(432, 474);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 16;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // TabAddDedpolicy
            // 
            this.TabAddDedpolicy.Controls.Add(this.tbpgeDeduction);
            this.TabAddDedpolicy.Location = new System.Drawing.Point(12, 140);
            this.TabAddDedpolicy.Name = "TabAddDedpolicy";
            this.TabAddDedpolicy.SelectedIndex = 0;
            this.TabAddDedpolicy.Size = new System.Drawing.Size(571, 282);
            this.TabAddDedpolicy.TabIndex = 33;
            // 
            // tbpgeDeduction
            // 
            this.tbpgeDeduction.Controls.Add(this.dgvDeductionDetails);
            this.tbpgeDeduction.Controls.Add(this.TxtEmpPer);
            this.tbpgeDeduction.Controls.Add(this.TxtEmprPer);
            this.tbpgeDeduction.Controls.Add(this.lblEmp);
            this.tbpgeDeduction.Controls.Add(this.lblEmper);
            this.tbpgeDeduction.Location = new System.Drawing.Point(4, 22);
            this.tbpgeDeduction.Name = "tbpgeDeduction";
            this.tbpgeDeduction.Padding = new System.Windows.Forms.Padding(3);
            this.tbpgeDeduction.Size = new System.Drawing.Size(563, 256);
            this.tbpgeDeduction.TabIndex = 1;
            this.tbpgeDeduction.Text = "Deduct From";
            this.tbpgeDeduction.UseVisualStyleBackColor = true;
            // 
            // dgvDeductionDetails
            // 
            this.dgvDeductionDetails.AllowUserToAddRows = false;
            this.dgvDeductionDetails.AllowUserToDeleteRows = false;
            this.dgvDeductionDetails.AllowUserToResizeColumns = false;
            this.dgvDeductionDetails.AllowUserToResizeRows = false;
            this.dgvDeductionDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvDeductionDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeductionDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DAddDedID,
            this.DSelectValue,
            this.DParticulars});
            this.dgvDeductionDetails.Location = new System.Drawing.Point(6, 6);
            this.dgvDeductionDetails.Name = "dgvDeductionDetails";
            this.dgvDeductionDetails.RowHeadersWidth = 25;
            this.dgvDeductionDetails.Size = new System.Drawing.Size(554, 209);
            this.dgvDeductionDetails.TabIndex = 22;
            this.dgvDeductionDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDeductionDetails_CellValueChanged);
            this.dgvDeductionDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvDeductionDetails_CellBeginEdit);
            this.dgvDeductionDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvDeductionDetails_DataError);
            // 
            // DAddDedID
            // 
            this.DAddDedID.HeaderText = "AddDedID";
            this.DAddDedID.Name = "DAddDedID";
            this.DAddDedID.ReadOnly = true;
            this.DAddDedID.Visible = false;
            // 
            // DSelectValue
            // 
            this.DSelectValue.HeaderText = "";
            this.DSelectValue.Name = "DSelectValue";
            this.DSelectValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DSelectValue.Width = 30;
            // 
            // DParticulars
            // 
            this.DParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DParticulars.HeaderText = "Particulars";
            this.DParticulars.Name = "DParticulars";
            this.DParticulars.ReadOnly = true;
            this.DParticulars.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // TxtEmpPer
            // 
            this.TxtEmpPer.BackColor = System.Drawing.SystemColors.Info;
            this.TxtEmpPer.Location = new System.Drawing.Point(467, 223);
            this.TxtEmpPer.MaxLength = 6;
            this.TxtEmpPer.Name = "TxtEmpPer";
            this.TxtEmpPer.Size = new System.Drawing.Size(80, 20);
            this.TxtEmpPer.TabIndex = 9;
            this.TxtEmpPer.TextChanged += new System.EventHandler(this.TxtEmpPer_TextChanged);
            this.TxtEmpPer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtEmpPer_KeyPress);
            // 
            // TxtEmprPer
            // 
            this.TxtEmprPer.BackColor = System.Drawing.SystemColors.Info;
            this.TxtEmprPer.Location = new System.Drawing.Point(135, 225);
            this.TxtEmprPer.MaxLength = 6;
            this.TxtEmprPer.Name = "TxtEmprPer";
            this.TxtEmprPer.Size = new System.Drawing.Size(80, 20);
            this.TxtEmprPer.TabIndex = 8;
            this.TxtEmprPer.TextChanged += new System.EventHandler(this.TxtEmprPer_TextChanged);
            this.TxtEmprPer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtEmprPer_KeyPress);
            // 
            // lblEmp
            // 
            this.lblEmp.AutoSize = true;
            this.lblEmp.Location = new System.Drawing.Point(338, 226);
            this.lblEmp.Name = "lblEmp";
            this.lblEmp.Size = new System.Drawing.Size(126, 13);
            this.lblEmp.TabIndex = 20;
            this.lblEmp.Text = "Employee Contribution(%)";
            // 
            // lblEmper
            // 
            this.lblEmper.AutoSize = true;
            this.lblEmper.Location = new System.Drawing.Point(6, 228);
            this.lblEmper.Name = "lblEmper";
            this.lblEmper.Size = new System.Drawing.Size(123, 13);
            this.lblEmper.TabIndex = 21;
            this.lblEmper.Text = "Employer Contribution(%)";
            // 
            // cboParticulars
            // 
            this.cboParticulars.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboParticulars.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboParticulars.BackColor = System.Drawing.SystemColors.Info;
            this.cboParticulars.DropDownHeight = 134;
            this.cboParticulars.FormattingEnabled = true;
            this.cboParticulars.IntegralHeight = false;
            this.cboParticulars.Location = new System.Drawing.Point(87, 44);
            this.cboParticulars.Name = "cboParticulars";
            this.cboParticulars.Size = new System.Drawing.Size(213, 21);
            this.cboParticulars.TabIndex = 35;
            this.cboParticulars.SelectedIndexChanged += new System.EventHandler(this.cboParticulars_SelectedIndexChanged);
            // 
            // LblParticulars
            // 
            this.LblParticulars.AutoSize = true;
            this.LblParticulars.Location = new System.Drawing.Point(12, 48);
            this.LblParticulars.Name = "LblParticulars";
            this.LblParticulars.Size = new System.Drawing.Size(45, 13);
            this.LblParticulars.TabIndex = 34;
            this.LblParticulars.Text = "Addition";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(14, 63);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(43, 13);
            this.Label6.TabIndex = 17;
            this.Label6.Text = "Amount";
            // 
            // TxtAmount
            // 
            this.TxtAmount.BackColor = System.Drawing.SystemColors.Info;
            this.TxtAmount.Location = new System.Drawing.Point(107, 62);
            this.TxtAmount.MaxLength = 8;
            this.TxtAmount.Name = "TxtAmount";
            this.TxtAmount.Size = new System.Drawing.Size(166, 20);
            this.TxtAmount.TabIndex = 6;
            this.TxtAmount.TextChanged += new System.EventHandler(this.TxtAmount_TextChanged);
            this.TxtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtAmount_KeyPress);
            // 
            // GrpDeductionPolicy
            // 
            this.GrpDeductionPolicy.Controls.Add(this.gbSlab);
            this.GrpDeductionPolicy.Controls.Add(this.label8);
            this.GrpDeductionPolicy.Controls.Add(this.groupBox1);
            this.GrpDeductionPolicy.Controls.Add(this.label5);
            this.GrpDeductionPolicy.Controls.Add(this.cboParticulars);
            this.GrpDeductionPolicy.Controls.Add(this.LblParticulars);
            this.GrpDeductionPolicy.Controls.Add(this.txtFixedAmount);
            this.GrpDeductionPolicy.Controls.Add(this.TabAddDedpolicy);
            this.GrpDeductionPolicy.Controls.Add(this.Label3);
            this.GrpDeductionPolicy.Controls.Add(this.Txtpolicyname);
            this.GrpDeductionPolicy.Controls.Add(this.Label7);
            this.GrpDeductionPolicy.Controls.Add(this.Label1);
            this.GrpDeductionPolicy.Controls.Add(this.DTPWithEffect);
            this.GrpDeductionPolicy.Location = new System.Drawing.Point(0, 28);
            this.GrpDeductionPolicy.Name = "GrpDeductionPolicy";
            this.GrpDeductionPolicy.Size = new System.Drawing.Size(590, 433);
            this.GrpDeductionPolicy.TabIndex = 39;
            this.GrpDeductionPolicy.TabStop = false;
            // 
            // gbSlab
            // 
            this.gbSlab.Controls.Add(this.CboParameter);
            this.gbSlab.Controls.Add(this.Label2);
            this.gbSlab.Controls.Add(this.CboAdditions);
            this.gbSlab.Controls.Add(this.Label4);
            this.gbSlab.Controls.Add(this.TxtAmount);
            this.gbSlab.Controls.Add(this.Label6);
            this.gbSlab.Location = new System.Drawing.Point(307, 40);
            this.gbSlab.Name = "gbSlab";
            this.gbSlab.Size = new System.Drawing.Size(277, 91);
            this.gbSlab.TabIndex = 42;
            this.gbSlab.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 42;
            this.label8.Text = "Based On";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdbFixedAmount);
            this.groupBox1.Controls.Add(this.rdbParticulars);
            this.groupBox1.Controls.Add(this.rdbRateOnly);
            this.groupBox1.Location = new System.Drawing.Point(87, 66);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(213, 65);
            this.groupBox1.TabIndex = 41;
            this.groupBox1.TabStop = false;
            // 
            // rdbFixedAmount
            // 
            this.rdbFixedAmount.AutoSize = true;
            this.rdbFixedAmount.Location = new System.Drawing.Point(108, 39);
            this.rdbFixedAmount.Name = "rdbFixedAmount";
            this.rdbFixedAmount.Size = new System.Drawing.Size(89, 17);
            this.rdbFixedAmount.TabIndex = 38;
            this.rdbFixedAmount.TabStop = true;
            this.rdbFixedAmount.Text = "Fixed Amount";
            this.rdbFixedAmount.UseVisualStyleBackColor = true;
            this.rdbFixedAmount.Visible = false;
            this.rdbFixedAmount.CheckedChanged += new System.EventHandler(this.rdbFixedAmount_CheckedChanged);
            // 
            // rdbParticulars
            // 
            this.rdbParticulars.AutoSize = true;
            this.rdbParticulars.Location = new System.Drawing.Point(16, 16);
            this.rdbParticulars.Name = "rdbParticulars";
            this.rdbParticulars.Size = new System.Drawing.Size(74, 17);
            this.rdbParticulars.TabIndex = 36;
            this.rdbParticulars.TabStop = true;
            this.rdbParticulars.Text = "Particulars";
            this.rdbParticulars.UseVisualStyleBackColor = true;
            this.rdbParticulars.CheckedChanged += new System.EventHandler(this.rdbParticulars_CheckedChanged);
            // 
            // rdbRateOnly
            // 
            this.rdbRateOnly.AutoSize = true;
            this.rdbRateOnly.Location = new System.Drawing.Point(16, 39);
            this.rdbRateOnly.Name = "rdbRateOnly";
            this.rdbRateOnly.Size = new System.Drawing.Size(72, 17);
            this.rdbRateOnly.TabIndex = 37;
            this.rdbRateOnly.TabStop = true;
            this.rdbRateOnly.Text = "Rate Only";
            this.rdbRateOnly.UseVisualStyleBackColor = true;
            this.rdbRateOnly.CheckedChanged += new System.EventHandler(this.rdbRateOnly_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(470, -3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 40;
            this.label5.Text = "Fixed Amount";
            this.label5.Visible = false;
            // 
            // txtFixedAmount
            // 
            this.txtFixedAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtFixedAmount.Location = new System.Drawing.Point(547, -6);
            this.txtFixedAmount.MaxLength = 8;
            this.txtFixedAmount.Name = "txtFixedAmount";
            this.txtFixedAmount.Size = new System.Drawing.Size(10, 20);
            this.txtFixedAmount.TabIndex = 39;
            this.txtFixedAmount.Visible = false;
            this.txtFixedAmount.TextChanged += new System.EventHandler(this.txtFixedAmount_TextChanged);
            this.txtFixedAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFixedAmount_KeyPress);
            // 
            // txtAdditionAmount
            // 
            this.txtAdditionAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtAdditionAmount.Location = new System.Drawing.Point(226, 472);
            this.txtAdditionAmount.Name = "txtAdditionAmount";
            this.txtAdditionAmount.Size = new System.Drawing.Size(166, 20);
            this.txtAdditionAmount.TabIndex = 14;
            this.txtAdditionAmount.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "AddDedID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Particulars";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FrmDeductionPolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 530);
            this.Controls.Add(this.DeductionPolicyBindingNavigator);
            this.Controls.Add(this.lblAmount);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.DeductionPolicyStatusStrip);
            this.Controls.Add(this.BtnBottomCancel);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.GrpDeductionPolicy);
            this.Controls.Add(this.txtAdditionAmount);
            this.Controls.Add(this.chkRateOnly);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmDeductionPolicy";
            this.Text = "Deduction Policy";
            this.Load += new System.EventHandler(this.FrmDeductionPolicy_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmDeductionPolicy_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.DeductionPolicyBindingNavigator)).EndInit();
            this.DeductionPolicyBindingNavigator.ResumeLayout(false);
            this.DeductionPolicyBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DeductionPolicyErrorProvider)).EndInit();
            this.DeductionPolicyStatusStrip.ResumeLayout(false);
            this.DeductionPolicyStatusStrip.PerformLayout();
            this.TabAddDedpolicy.ResumeLayout(false);
            this.tbpgeDeduction.ResumeLayout(false);
            this.tbpgeDeduction.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeductionDetails)).EndInit();
            this.GrpDeductionPolicy.ResumeLayout(false);
            this.GrpDeductionPolicy.PerformLayout();
            this.gbSlab.ResumeLayout(false);
            this.gbSlab.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.CheckBox chkRateOnly;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.DateTimePicker DTPWithEffect;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.ComboBox CboParameter;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.BindingNavigator DeductionPolicyBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton ProjectsBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BtnCancel;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton ToolStripbtnHelp;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn DAddDedID;
        internal System.Windows.Forms.ComboBox CboAdditions;
        //internal System.Windows.Forms.DataGridViewCheckBoxColumn DSelectValue;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn DParticulars;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.ErrorProvider DeductionPolicyErrorProvider;
        internal System.Windows.Forms.Label lblAmount;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.StatusStrip DeductionPolicyStatusStrip;
        internal System.Windows.Forms.ToolStripStatusLabel ProjectCreationStatusLabel;
        internal System.Windows.Forms.Button BtnBottomCancel;
        internal System.Windows.Forms.Button BtnOk;
        internal System.Windows.Forms.GroupBox GrpDeductionPolicy;
        internal System.Windows.Forms.ComboBox cboParticulars;
        internal System.Windows.Forms.Label LblParticulars;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.TextBox TxtAmount;
        internal System.Windows.Forms.TabControl TabAddDedpolicy;
        internal System.Windows.Forms.TextBox Txtpolicyname;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox txtAdditionAmount;
        internal System.Windows.Forms.Timer DeductionPolicyTimer;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        internal System.Windows.Forms.TabPage tbpgeDeduction;
        private System.Windows.Forms.DataGridView dgvDeductionDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn DAddDedID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DSelectValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn DParticulars;
        internal System.Windows.Forms.TextBox TxtEmpPer;
        internal System.Windows.Forms.TextBox TxtEmprPer;
        internal System.Windows.Forms.Label lblEmp;
        internal System.Windows.Forms.Label lblEmper;
        internal System.Windows.Forms.RadioButton rdbFixedAmount;
        internal System.Windows.Forms.RadioButton rdbRateOnly;
        internal System.Windows.Forms.RadioButton rdbParticulars;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.TextBox txtFixedAmount;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbSlab;
        internal System.Windows.Forms.Label label8;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        //internal System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        //internal System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        //internal System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        //internal System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        //internal System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        //internal System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn6;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
    }
}