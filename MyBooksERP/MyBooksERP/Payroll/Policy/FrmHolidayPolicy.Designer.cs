﻿namespace MyBooksERP
{
    partial class FrmHolidayPolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label2;
            System.Windows.Forms.Label Label4;
            System.Windows.Forms.Label Label1;
            System.Windows.Forms.Label DescriptionLabel;
            System.Windows.Forms.Label Label5;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmHolidayPolicy));
            this.HolidayPolicyBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.chkHourBasedShift = new System.Windows.Forms.CheckBox();
            this.cboCalculationBased = new System.Windows.Forms.ComboBox();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.chkRateOnly = new System.Windows.Forms.CheckBox();
            this.rdbActualMonth = new System.Windows.Forms.RadioButton();
            this.rdbCompanyBased = new System.Windows.Forms.RadioButton();
            this.lblHoursPerDay = new System.Windows.Forms.Label();
            this.txtHoursPerDay = new System.Windows.Forms.TextBox();
            this.lblRate = new System.Windows.Forms.Label();
            this.chkRatePerDay = new System.Windows.Forms.CheckBox();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.SelectValue = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SelectValueS = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtPolicyName = new System.Windows.Forms.TextBox();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblHeader = new System.Windows.Forms.Label();
            this.errHolidayPolicy = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.dgvHolidayDetails = new DemoClsDataGridview.ClsDataGirdView();
            this.colHolidayAddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colHolidaySelectedItem = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colHolidayParticulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Particulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddDedIDS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParticularsS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCalculationPercent = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            Label2 = new System.Windows.Forms.Label();
            Label4 = new System.Windows.Forms.Label();
            Label1 = new System.Windows.Forms.Label();
            DescriptionLabel = new System.Windows.Forms.Label();
            Label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayPolicyBindingNavigator)).BeginInit();
            this.HolidayPolicyBindingNavigator.SuspendLayout();
            this.ssStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errHolidayPolicy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHolidayDetails)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Location = new System.Drawing.Point(10, 160);
            Label2.Name = "Label2";
            Label2.Size = new System.Drawing.Size(101, 13);
            Label2.TabIndex = 38;
            Label2.Text = "Exclude from Gross ";
            // 
            // Label4
            // 
            Label4.AutoSize = true;
            Label4.Location = new System.Drawing.Point(7, 54);
            Label4.Name = "Label4";
            Label4.Size = new System.Drawing.Size(81, 13);
            Label4.TabIndex = 17;
            Label4.Text = "Calculation Day";
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new System.Drawing.Point(10, 126);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(109, 13);
            Label1.TabIndex = 36;
            Label1.Text = "Calculation Based On";
            // 
            // DescriptionLabel
            // 
            DescriptionLabel.AutoSize = true;
            DescriptionLabel.Location = new System.Drawing.Point(10, 24);
            DescriptionLabel.Name = "DescriptionLabel";
            DescriptionLabel.Size = new System.Drawing.Size(66, 13);
            DescriptionLabel.TabIndex = 306;
            DescriptionLabel.Text = "Policy Name";
            // 
            // Label5
            // 
            Label5.AutoSize = true;
            Label5.Location = new System.Drawing.Point(10, 286);
            Label5.Name = "Label5";
            Label5.Size = new System.Drawing.Size(117, 13);
            Label5.TabIndex = 315;
            Label5.Text = "Calculation Percentage";
            // 
            // HolidayPolicyBindingNavigator
            // 
            this.HolidayPolicyBindingNavigator.AddNewItem = null;
            this.HolidayPolicyBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.HolidayPolicyBindingNavigator.DeleteItem = null;
            this.HolidayPolicyBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorSaveItem,
            this.btnClear,
            this.btnPrint,
            this.btnEmail,
            this.ToolStripSeparator2,
            this.btnHelp});
            this.HolidayPolicyBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.HolidayPolicyBindingNavigator.MoveFirstItem = null;
            this.HolidayPolicyBindingNavigator.MoveLastItem = null;
            this.HolidayPolicyBindingNavigator.MoveNextItem = null;
            this.HolidayPolicyBindingNavigator.MovePreviousItem = null;
            this.HolidayPolicyBindingNavigator.Name = "HolidayPolicyBindingNavigator";
            this.HolidayPolicyBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.HolidayPolicyBindingNavigator.Size = new System.Drawing.Size(416, 25);
            this.HolidayPolicyBindingNavigator.TabIndex = 300;
            this.HolidayPolicyBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save Data";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            //this.btnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Visible = false;
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Visible = false;
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.Text = "He&lp";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // chkHourBasedShift
            // 
            this.chkHourBasedShift.AutoSize = true;
            this.chkHourBasedShift.Location = new System.Drawing.Point(226, 318);
            this.chkHourBasedShift.Name = "chkHourBasedShift";
            this.chkHourBasedShift.Size = new System.Drawing.Size(143, 17);
            this.chkHourBasedShift.TabIndex = 41;
            this.chkHourBasedShift.Text = "Min Hour Based On Shift";
            this.chkHourBasedShift.UseVisualStyleBackColor = true;
            this.chkHourBasedShift.Visible = false;
            this.chkHourBasedShift.CheckedChanged += new System.EventHandler(this.chkHourBasedShift_CheckedChanged);
            // 
            // cboCalculationBased
            // 
            this.cboCalculationBased.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCalculationBased.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCalculationBased.BackColor = System.Drawing.SystemColors.Info;
            this.cboCalculationBased.DropDownHeight = 134;
            this.cboCalculationBased.FormattingEnabled = true;
            this.cboCalculationBased.IntegralHeight = false;
            this.cboCalculationBased.Location = new System.Drawing.Point(138, 123);
            this.cboCalculationBased.MaxDropDownItems = 10;
            this.cboCalculationBased.Name = "cboCalculationBased";
            this.cboCalculationBased.Size = new System.Drawing.Size(231, 21);
            this.cboCalculationBased.TabIndex = 2;
            this.cboCalculationBased.SelectedIndexChanged += new System.EventHandler(this.cboCalculationBased_SelectedIndexChanged);
            this.cboCalculationBased.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCalculationBased_KeyDown);
            // 
            // txtRate
            // 
            this.txtRate.Location = new System.Drawing.Point(155, 351);
            this.txtRate.MaxLength = 9;
            this.txtRate.Name = "txtRate";
            this.txtRate.ShortcutsEnabled = false;
            this.txtRate.Size = new System.Drawing.Size(210, 20);
            this.txtRate.TabIndex = 7;
            this.txtRate.TextChanged += new System.EventHandler(this.txtRate_TextChanged);
            this.txtRate.Validated += new System.EventHandler(this.ResetForm);
            this.txtRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // chkRateOnly
            // 
            this.chkRateOnly.AutoSize = true;
            this.chkRateOnly.Location = new System.Drawing.Point(138, 354);
            this.chkRateOnly.Name = "chkRateOnly";
            this.chkRateOnly.Size = new System.Drawing.Size(15, 14);
            this.chkRateOnly.TabIndex = 6;
            this.chkRateOnly.UseVisualStyleBackColor = true;
            this.chkRateOnly.CheckedChanged += new System.EventHandler(this.chkRateOnly_CheckedChanged);
            // 
            // rdbActualMonth
            // 
            this.rdbActualMonth.AutoSize = true;
            this.rdbActualMonth.Location = new System.Drawing.Point(276, 86);
            this.rdbActualMonth.Name = "rdbActualMonth";
            this.rdbActualMonth.Size = new System.Drawing.Size(88, 17);
            this.rdbActualMonth.TabIndex = 1;
            this.rdbActualMonth.Text = "Actual Month";
            this.rdbActualMonth.UseVisualStyleBackColor = true;
            this.rdbActualMonth.CheckedChanged += new System.EventHandler(this.ResetForm);
            // 
            // rdbCompanyBased
            // 
            this.rdbCompanyBased.AutoSize = true;
            this.rdbCompanyBased.Checked = true;
            this.rdbCompanyBased.Location = new System.Drawing.Point(138, 86);
            this.rdbCompanyBased.Name = "rdbCompanyBased";
            this.rdbCompanyBased.Size = new System.Drawing.Size(117, 17);
            this.rdbCompanyBased.TabIndex = 0;
            this.rdbCompanyBased.TabStop = true;
            this.rdbCompanyBased.Text = "Based on Company";
            this.rdbCompanyBased.UseVisualStyleBackColor = true;
            this.rdbCompanyBased.CheckedChanged += new System.EventHandler(this.rdbCompanyBased_CheckedChanged);
            // 
            // lblHoursPerDay
            // 
            this.lblHoursPerDay.AutoSize = true;
            this.lblHoursPerDay.Location = new System.Drawing.Point(10, 319);
            this.lblHoursPerDay.Name = "lblHoursPerDay";
            this.lblHoursPerDay.Size = new System.Drawing.Size(76, 13);
            this.lblHoursPerDay.TabIndex = 15;
            this.lblHoursPerDay.Text = "Hours Per Day";
            // 
            // txtHoursPerDay
            // 
            this.txtHoursPerDay.BackColor = System.Drawing.SystemColors.Window;
            this.txtHoursPerDay.Location = new System.Drawing.Point(155, 316);
            this.txtHoursPerDay.MaxLength = 5;
            this.txtHoursPerDay.Name = "txtHoursPerDay";
            this.txtHoursPerDay.ShortcutsEnabled = false;
            this.txtHoursPerDay.Size = new System.Drawing.Size(65, 20);
            this.txtHoursPerDay.TabIndex = 5;
            this.txtHoursPerDay.TextChanged += new System.EventHandler(this.ResetForm);
            this.txtHoursPerDay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtint_KeyPress);
            // 
            // lblRate
            // 
            this.lblRate.AutoSize = true;
            this.lblRate.Location = new System.Drawing.Point(10, 354);
            this.lblRate.Name = "lblRate";
            this.lblRate.Size = new System.Drawing.Size(121, 13);
            this.lblRate.TabIndex = 14;
            this.lblRate.Text = "Holiday Rate(Rate Only)";
            // 
            // chkRatePerDay
            // 
            this.chkRatePerDay.AutoSize = true;
            this.chkRatePerDay.Location = new System.Drawing.Point(138, 319);
            this.chkRatePerDay.Name = "chkRatePerDay";
            this.chkRatePerDay.Size = new System.Drawing.Size(15, 14);
            this.chkRatePerDay.TabIndex = 4;
            this.chkRatePerDay.UseVisualStyleBackColor = true;
            this.chkRatePerDay.CheckedChanged += new System.EventHandler(this.chkRatePerDay_CheckedChanged);
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 7;
            this.lineShape3.X2 = 390;
            this.lineShape3.Y1 = 57;
            this.lineShape3.Y2 = 57;
            // 
            // SelectValue
            // 
            this.SelectValue.HeaderText = "";
            this.SelectValue.Name = "SelectValue";
            this.SelectValue.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SelectValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SelectValue.Width = 30;
            // 
            // SelectValueS
            // 
            this.SelectValueS.HeaderText = "";
            this.SelectValueS.Name = "SelectValueS";
            this.SelectValueS.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SelectValueS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SelectValueS.Width = 30;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(334, 434);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(250, 434);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(8, 431);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtPolicyName
            // 
            this.txtPolicyName.BackColor = System.Drawing.SystemColors.Info;
            this.txtPolicyName.Location = new System.Drawing.Point(138, 21);
            this.txtPolicyName.MaxLength = 50;
            this.txtPolicyName.Name = "txtPolicyName";
            this.txtPolicyName.Size = new System.Drawing.Size(231, 20);
            this.txtPolicyName.TabIndex = 0;
            this.txtPolicyName.TextChanged += new System.EventHandler(this.txtPolicyName_TextChanged);
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 460);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(416, 22);
            this.ssStatus.TabIndex = 310;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblHeader.Location = new System.Drawing.Point(6, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(66, 13);
            this.lblHeader.TabIndex = 312;
            this.lblHeader.Text = "Policy Info";
            // 
            // errHolidayPolicy
            // 
            this.errHolidayPolicy.ContainerControl = this;
            this.errHolidayPolicy.RightToLeft = true;
            // 
            // tmrClear
            // 
            this.tmrClear.Tick += new System.EventHandler(this.tmrClear_Tick);
            // 
            // dgvHolidayDetails
            // 
            this.dgvHolidayDetails.AddNewRow = false;
            this.dgvHolidayDetails.AllowUserToAddRows = false;
            this.dgvHolidayDetails.AllowUserToDeleteRows = false;
            this.dgvHolidayDetails.AllowUserToResizeColumns = false;
            this.dgvHolidayDetails.AllowUserToResizeRows = false;
            this.dgvHolidayDetails.AlphaNumericCols = new int[0];
            this.dgvHolidayDetails.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvHolidayDetails.CapsLockCols = new int[0];
            this.dgvHolidayDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHolidayDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colHolidayAddDedID,
            this.colHolidaySelectedItem,
            this.colHolidayParticulars});
            this.dgvHolidayDetails.DecimalCols = new int[0];
            this.dgvHolidayDetails.HasSlNo = false;
            this.dgvHolidayDetails.LastRowIndex = 0;
            this.dgvHolidayDetails.Location = new System.Drawing.Point(138, 150);
            this.dgvHolidayDetails.MultiSelect = false;
            this.dgvHolidayDetails.Name = "dgvHolidayDetails";
            this.dgvHolidayDetails.NegativeValueCols = new int[0];
            this.dgvHolidayDetails.NumericCols = new int[0];
            this.dgvHolidayDetails.RowHeadersWidth = 35;
            this.dgvHolidayDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvHolidayDetails.Size = new System.Drawing.Size(231, 122);
            this.dgvHolidayDetails.TabIndex = 3;
            this.dgvHolidayDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvHolidayDetails_CellValueChanged_1);
            this.dgvHolidayDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvHolidayDetails_CellBeginEdit);
            // 
            // colHolidayAddDedID
            // 
            this.colHolidayAddDedID.HeaderText = "AddDedID";
            this.colHolidayAddDedID.Name = "colHolidayAddDedID";
            this.colHolidayAddDedID.Visible = false;
            // 
            // colHolidaySelectedItem
            // 
            this.colHolidaySelectedItem.HeaderText = "";
            this.colHolidaySelectedItem.Name = "colHolidaySelectedItem";
            this.colHolidaySelectedItem.Width = 30;
            // 
            // colHolidayParticulars
            // 
            this.colHolidayParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colHolidayParticulars.HeaderText = "Particulars";
            this.colHolidayParticulars.Name = "colHolidayParticulars";
            this.colHolidayParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AddDedID
            // 
            this.AddDedID.HeaderText = "AddDedID";
            this.AddDedID.Name = "AddDedID";
            this.AddDedID.ReadOnly = true;
            this.AddDedID.Visible = false;
            // 
            // Particulars
            // 
            this.Particulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Particulars.HeaderText = "Particulars";
            this.Particulars.Name = "Particulars";
            this.Particulars.ReadOnly = true;
            // 
            // AddDedIDS
            // 
            this.AddDedIDS.HeaderText = "AddDedID";
            this.AddDedIDS.Name = "AddDedIDS";
            this.AddDedIDS.ReadOnly = true;
            this.AddDedIDS.Visible = false;
            // 
            // ParticularsS
            // 
            this.ParticularsS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ParticularsS.HeaderText = "Particulars";
            this.ParticularsS.Name = "ParticularsS";
            this.ParticularsS.ReadOnly = true;
            // 
            // txtCalculationPercent
            // 
            this.txtCalculationPercent.BackColor = System.Drawing.SystemColors.Info;
            this.txtCalculationPercent.Location = new System.Drawing.Point(138, 283);
            this.txtCalculationPercent.MaxLength = 5;
            this.txtCalculationPercent.Name = "txtCalculationPercent";
            this.txtCalculationPercent.Size = new System.Drawing.Size(231, 20);
            this.txtCalculationPercent.TabIndex = 314;
            this.txtCalculationPercent.TextChanged += new System.EventHandler(this.txtCalculationPercent_TextChanged);
            this.txtCalculationPercent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdbCompanyBased);
            this.groupBox1.Controls.Add(Label4);
            this.groupBox1.Controls.Add(this.txtCalculationPercent);
            this.groupBox1.Controls.Add(this.lblHeader);
            this.groupBox1.Controls.Add(this.chkRateOnly);
            this.groupBox1.Controls.Add(Label5);
            this.groupBox1.Controls.Add(Label1);
            this.groupBox1.Controls.Add(this.chkHourBasedShift);
            this.groupBox1.Controls.Add(this.txtPolicyName);
            this.groupBox1.Controls.Add(this.rdbActualMonth);
            this.groupBox1.Controls.Add(DescriptionLabel);
            this.groupBox1.Controls.Add(this.dgvHolidayDetails);
            this.groupBox1.Controls.Add(this.txtRate);
            this.groupBox1.Controls.Add(this.chkRatePerDay);
            this.groupBox1.Controls.Add(this.cboCalculationBased);
            this.groupBox1.Controls.Add(this.lblRate);
            this.groupBox1.Controls.Add(this.lblHoursPerDay);
            this.groupBox1.Controls.Add(Label2);
            this.groupBox1.Controls.Add(this.txtHoursPerDay);
            this.groupBox1.Controls.Add(this.shapeContainer2);
            this.groupBox1.Location = new System.Drawing.Point(8, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(401, 386);
            this.groupBox1.TabIndex = 316;
            this.groupBox1.TabStop = false;
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3});
            this.shapeContainer2.Size = new System.Drawing.Size(395, 367);
            this.shapeContainer2.TabIndex = 316;
            this.shapeContainer2.TabStop = false;
            // 
            // FrmHolidayPolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 482);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.HolidayPolicyBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmHolidayPolicy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Holiday Policy";
            this.Load += new System.EventHandler(this.FrmHolidayPolicy_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmHolidayPolicy_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.HolidayPolicyBindingNavigator)).EndInit();
            this.HolidayPolicyBindingNavigator.ResumeLayout(false);
            this.HolidayPolicyBindingNavigator.PerformLayout();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errHolidayPolicy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHolidayDetails)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator HolidayPolicyBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.ComboBox cboCalculationBased;
        internal System.Windows.Forms.TextBox txtRate;
        internal System.Windows.Forms.CheckBox chkRateOnly;
        internal System.Windows.Forms.RadioButton rdbActualMonth;
        internal System.Windows.Forms.RadioButton rdbCompanyBased;
        internal System.Windows.Forms.Label lblHoursPerDay;
        internal System.Windows.Forms.TextBox txtHoursPerDay;
        internal System.Windows.Forms.Label lblRate;
        internal System.Windows.Forms.CheckBox chkRatePerDay;
        internal System.Windows.Forms.DataGridViewTextBoxColumn AddDedID;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn SelectValue;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Particulars;
        internal System.Windows.Forms.DataGridViewTextBoxColumn AddDedIDS;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn SelectValueS;
        internal System.Windows.Forms.DataGridViewTextBoxColumn ParticularsS;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.TextBox txtPolicyName;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.Label lblHeader;
        private DemoClsDataGridview.ClsDataGirdView dgvHolidayDetails;
        private System.Windows.Forms.ErrorProvider errHolidayPolicy;
        private System.Windows.Forms.Timer tmrClear;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        internal System.Windows.Forms.CheckBox chkHourBasedShift;
        private System.Windows.Forms.DataGridViewTextBoxColumn colHolidayAddDedID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colHolidaySelectedItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn colHolidayParticulars;
        internal System.Windows.Forms.TextBox txtCalculationPercent;
        private System.Windows.Forms.GroupBox groupBox1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
    }
}