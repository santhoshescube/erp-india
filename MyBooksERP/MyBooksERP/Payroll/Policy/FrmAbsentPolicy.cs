﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    /*****************************************************
   * Created By       : Arun
   * Creation Date    : 10 Apr 2012
   * Description      : Handle Absent Policy
   * 
   * Modified By      : Ranju Mathew
   * Creation Date    : 12 aug 2013
   * Description      : Tuning and performance improving
   * ***************************************************/
    public partial class FrmAbsentPolicy : Form
    {

        #region Declartions
        public int PintAbsentPolicyID = 0;   // From Refernce Form 
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission

        private string MstrCommonMessage;   
        private bool MblnIsEditMode = false;  //  To Find Whether Is add mode or Edit Mode
        private int MintRecordCnt = 0;     //Total Record Count
        private int MintCurrentRecCnt = 0; // Current Record Count
        private bool MblnChangeStatus = false;
        private bool MblbtnOk = false;

        private clsBLLAbsentPolicy MobjclsBLLAbsentPolicy = null;
        private clsMessage ObjUserMessage = null;
        string strBindingOf = "Of ";
        #endregion Declartions
        /// <summary>
        /// FORM ID=112
        /// Constructor
        /// </summary>
        #region Constructor
        public FrmAbsentPolicy()  
        {
            InitializeComponent();

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.AbsentPolicy, this);

        //    strBindingOf = "من ";
        //    dgvAbsentDetails.Columns["colAbsentParticulars"].HeaderText = "تفاصيل";
        //    dgvShortageDetails.Columns["colShortParticulars"].HeaderText = "تفاصيل";
        //}
        #endregion Constructor

        #region Properties
        private clsMessage UserMessage  // For Notification Message
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.AbsentPolicy);
                return this.ObjUserMessage;
            }
        }
        private clsBLLAbsentPolicy BLLAbsentPolicy
        {
            get
        {
            if (this.MobjclsBLLAbsentPolicy == null)
            this.MobjclsBLLAbsentPolicy = new clsBLLAbsentPolicy();

            return this.MobjclsBLLAbsentPolicy;
        }
        }
        #endregion Properties

        #region Methods

        private void SetPermissions()  // Function for setting permissions Add/Update/Delete/Email-Print
        {
           
        clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
        if (ClsCommonSettings.RoleID >3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.AbsentPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
        else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }

        private void ClearAllControls()   // Funtion to clear All Controls
        {
            this.txtPolicyName.Text = "";
            this.txtPolicyName.Tag = 0;
            this.chkAbsentPolicyOnly.Checked = false;
            this.chkMergeBoth.Checked = false;

            this.rdbCompanyBased.Checked = true;
            this.rdbActualMonth.Checked = false;
            //this.rdbWorkingDays.Checked = false;
            this.cboCalculationBased.SelectedIndex = -1;
            this.cboCalculationBased.Text = "";
            this.dgvAbsentDetails.Rows.Clear();
            this.chkRatePerDay.Checked = false;
            this.chkRateOnly.Checked = false;
            this.txtHoursPerDay.Text = "";
            this.txtRate.Text = "";
            this.rdbShortBasedOnCompany.Checked = true;
            this.cboShortCalculationBasedOn.SelectedIndex = -1;
            this.cboShortCalculationBasedOn.Text = "";
            this.dgvShortageDetails.Rows.Clear();
            this.txtShortHoursPerDay.Text = "";
            this.chkShortRateOnly.Checked = false;
            this.txtShortRatePerHour.Text = "";

            tbPolicy.SelectedTab = tabpgAbsent;


        }

        private void Changestatus(object sender, EventArgs e) //function for changing status
        {
            
        if (!MblnIsEditMode)
            {
                btnSave.Enabled = btnOk.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
        else
            {
                btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            errAbsentPolicy.Clear();
            MblnChangeStatus = true;

        }
        private bool LoadCombos(int intType) // For Loading the Combos 
        {
            bool blnRetvalue = false;
            DataTable datCombos = new DataTable();
        try
        {

            if (intType == 0 || intType == 1)//Calculation type
            {
                //if (ClsCommonSettings.IsArabicView)
                //    datCombos = BLLAbsentPolicy.FillCombos(new string[] { "CalculationID,CalculationArb AS Calculation", "PayCalculationReference", "CalculationID in(1,2)" });
                //else
                    datCombos = BLLAbsentPolicy.FillCombos(new string[] { "CalculationID,Calculation", "PayCalculationReference", "CalculationID in(1,2)" });
                cboCalculationBased.ValueMember = "CalculationID";
                cboCalculationBased.DisplayMember = "Calculation";
                cboCalculationBased.DataSource = datCombos;
                blnRetvalue = true;
            }

            if (intType == 0 || intType == 2)////Calculation type
            {
                //if (ClsCommonSettings.IsArabicView)
                //    datCombos = BLLAbsentPolicy.FillCombos(new string[] { "CalculationID,CalculationArb AS Calculation", "PayCalculationReference", "CalculationID in(1,2)" });
                //else
                    datCombos = BLLAbsentPolicy.FillCombos(new string[] { "CalculationID,Calculation", "PayCalculationReference", "CalculationID in(1,2)" });
                
                cboShortCalculationBasedOn.ValueMember = "CalculationID";
                cboShortCalculationBasedOn.DisplayMember = "Calculation";
                cboShortCalculationBasedOn.DataSource = datCombos;
                blnRetvalue = true;
            }

        }
        catch (Exception Ex)
        {
            ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            blnRetvalue = false;
        }
            return blnRetvalue;
        }
        private void FillAdditionDeductionsGrid(DataGridView Dgv, DataTable dtAddDedDetails) // To fill AdditionDeduction Details in Grid
        {
            Dgv.Rows.Clear();
            if (dtAddDedDetails != null)
            {
                if (dtAddDedDetails.Rows.Count > 0)
                {
                    for (int iCounter = 0; iCounter <= dtAddDedDetails.Rows.Count - 1; iCounter++)
                    {
                        Dgv.RowCount = Dgv.RowCount + 1;
                        Dgv.Rows[iCounter].Cells[0].Value = dtAddDedDetails.Rows[iCounter]["AddDedID"].ToInt32();//Addition deduction Policy
                        Dgv.Rows[iCounter].Cells[1].Value = dtAddDedDetails.Rows[iCounter]["Checked"].ToBoolean(); ;//ChkBox
                        //if (ClsCommonSettings.IsArabicView)
                        //    Dgv.Rows[iCounter].Cells[2].Value = dtAddDedDetails.Rows[iCounter]["DescriptionArb"].ToStringCustom();//Description
                        //else
                            Dgv.Rows[iCounter].Cells[2].Value = dtAddDedDetails.Rows[iCounter]["Description"].ToStringCustom();//Description
                    }
                }
            }
        }

        private void GetRecordCount()   // TO Get Record Count
        {
            MintRecordCnt = 0;
            MintRecordCnt = BLLAbsentPolicy.GetRecordCount();
            if (MintRecordCnt < 0)
                {
                    BindingNavigatorCountItem.Text = strBindingOf + "0";
                    MintRecordCnt = 0;
                }
        }
        private void RefernceDisplay() // To Display Details In Edit Mode
        {

            int intRowNum = 0;
            intRowNum = BLLAbsentPolicy.GetRowNumber(PintAbsentPolicyID);
            if (intRowNum > 0)
            {
                MintCurrentRecCnt = intRowNum;
                DisplayAbsentPolicyInfo();
            }
        }
        private void AddNewAbsentPolicy()  // Add Mode
        {
            MblnChangeStatus = false;
            MblnIsEditMode = false;
            lblstatus.Text = "";
            tmrClear.Enabled = true;
            errAbsentPolicy.Clear();

            ClearAllControls();  //Clear all controls in the form
            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();

            BindingNavigatorAddNewItem.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = false;
            btnPrint.Enabled = false;
            btnEmail.Enabled = false;
            btnOk.Enabled = false;
            btnSave.Enabled = false;
            BindingNavigatorSaveItem.Enabled = false;
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;

            txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtRate.Enabled = chkRateOnly.Checked ? true : false;

            txtHoursPerDay.BackColor = chkRatePerDay.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtHoursPerDay.Enabled = chkRatePerDay.Checked ? true : false;

            txtShortHoursPerDay.BackColor = chkShortRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtShortRatePerHour.Enabled = chkShortRateOnly.Checked ? true : false;
            txtShortHoursPerDay.BackColor = System.Drawing.SystemColors.Info;
            btnClear.Enabled = true;

        }
        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
                {
                    BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
                }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
                {
                    BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
                }
        }
        private void DisplayAbsentPolicyInfo() //To Display Detail Info
        {

            FillAbsentPolicyInfo();

            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            btnPrint.Enabled = MblnPrintEmailPermission;
            btnEmail.Enabled = MblnPrintEmailPermission;
            BindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            MblnIsEditMode = true;
            SetBindingNavigatorButtons();
            MblnChangeStatus = false;
            BindingNavigatorSaveItem.Enabled = MblnChangeStatus;
            btnOk.Enabled = MblnChangeStatus;
            btnSave.Enabled = MblnChangeStatus;

            txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtRate.Enabled = chkRateOnly.Checked ? true : false;

            txtHoursPerDay.BackColor = chkRatePerDay.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtHoursPerDay.Enabled = chkRatePerDay.Checked ? true : false;

            txtShortHoursPerDay.BackColor = chkShortRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtShortRatePerHour.Enabled = chkShortRateOnly.Checked ? true : false;
            txtShortHoursPerDay.BackColor = System.Drawing.SystemColors.Info;
            btnClear.Enabled = false;
        }
        private void FillAbsentPolicyInfo() // To fill AbsentPolicy
        {
            ClearAllControls();
            DataTable dtAbsentPolicy = BLLAbsentPolicy.DisplayAbsentPolicy(MintCurrentRecCnt);

            if (dtAbsentPolicy.Rows.Count > 0)
            {
                chkAbsentPolicyOnly.Checked = true;
                txtPolicyName.Text = dtAbsentPolicy.Rows[0]["AbsentPolicy"].ToStringCustom();
                txtPolicyName.Tag = dtAbsentPolicy.Rows[0]["AbsentPolicyID"].ToInt32();
                chkMergeBoth.Checked = dtAbsentPolicy.Rows[0]["IsMergeBoth"].ToBoolean() ? true : false;

                for (int iCounter = 0; iCounter <= dtAbsentPolicy.Rows.Count - 1; iCounter++)
                {

                    if (dtAbsentPolicy.Rows[iCounter]["AbsentType"].ToInt32() == (int)AbsentType.Absent)
                    {

                        cboCalculationBased.SelectedValue = dtAbsentPolicy.Rows[iCounter]["CalculationID"].ToInt32();

                        if (dtAbsentPolicy.Rows[iCounter]["CompanyBasedOn"].ToInt32() == 1)
                        {
                            rdbCompanyBased.Checked = true;
                        }
                        else 
                        {
                            rdbActualMonth.Checked = true;
                        }
                        chkRateOnly.Checked = dtAbsentPolicy.Rows[iCounter]["IsRateOnly"].ToBoolean();
                        txtRate.Text = dtAbsentPolicy.Rows[iCounter]["AbsentRate"].ToDecimal().ToStringCustom();

                        chkRatePerDay.Checked = dtAbsentPolicy.Rows[iCounter]["IsRateBasedOnHour"].ToBoolean();
                        txtHoursPerDay.Text = dtAbsentPolicy.Rows[iCounter]["HoursPerDay"].ToStringCustom();
                        if (dtAbsentPolicy.Rows[iCounter]["CalculationID"].ToInt32() == (int)CalculationType.GrossSalary)
                            {
                                DataTable DtAddDedRef = BLLAbsentPolicy.DisplayAddDedDetails(txtPolicyName.Tag.ToInt32(), (int)AbsentType.Absent);
                                FillAdditionDeductionsGrid(dgvAbsentDetails, DtAddDedRef);
                            }

                    }
                    else if (dtAbsentPolicy.Rows[iCounter]["AbsentType"].ToInt32() == (int)AbsentType.Shortage)
                    {
                        chkAbsentPolicyOnly.Checked = false;
                        cboShortCalculationBasedOn.SelectedValue = dtAbsentPolicy.Rows[iCounter]["CalculationID"].ToInt32();

                        if (dtAbsentPolicy.Rows[iCounter]["CompanyBasedOn"].ToInt32() == 1)
                        {
                            rdbShortBasedOnCompany.Checked = true;
                        }
                        else 
                        {
                            rdbShortActualMonth.Checked = true;
                        }
                        txtShortHoursPerDay.Text = dtAbsentPolicy.Rows[iCounter]["HoursPerDay"].ToStringCustom();
                        chkShortRateOnly.Checked = dtAbsentPolicy.Rows[iCounter]["IsRateOnly"].ToBoolean();
                        txtShortRatePerHour.Text = dtAbsentPolicy.Rows[iCounter]["AbsentRate"].ToDecimal().ToStringCustom();
                        if (dtAbsentPolicy.Rows[iCounter]["CalculationID"].ToInt32() == (int)CalculationType.GrossSalary)
                        {
                            DataTable DtAddDedRef = BLLAbsentPolicy.DisplayAddDedDetails(txtPolicyName.Tag.ToInt32(), (int)AbsentType.Shortage);
                            FillAdditionDeductionsGrid(dgvShortageDetails, DtAddDedRef);
                        }

                    }

                    //if (ClsCommonSettings.IsAmountRoundByZero)
                    //{
                    //    txtRate.Text = txtRate.Text.ToDecimal().ToString("F" + 0);
                    //    txtHoursPerDay.Text = txtHoursPerDay.Text.ToDecimal().ToString("F" + 0);
                    //    txtShortHoursPerDay.Text = txtShortHoursPerDay.Text.ToDecimal().ToString("F" + 0);
                    //    txtShortRatePerHour.Text = txtShortRatePerHour.Text.ToDecimal().ToString("F" + 0);
                    //}




                }

            }


        }
        private bool FormValidation() // For Validation
        {

            errAbsentPolicy.Clear();
            Control control = null;
            bool blnReturnValue = true;

            if (txtPolicyName.Text.Trim().Length == 0)     // Please enter Policy Name
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(7250);
                control = txtPolicyName;
                blnReturnValue = false;
            }
            else if (chkRateOnly.Checked == false && cboCalculationBased.SelectedValue.ToInt32() == 0) //Please select Calculation based for absent policy
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(7251);
                control = cboCalculationBased;
                blnReturnValue = false;
            }
            else if (chkRatePerDay.Checked == true && (txtHoursPerDay.Text.Trim().Length == 0 ? 0.ToDecimal() : txtHoursPerDay.Text.ToDecimal()) == 0.ToDecimal())
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(7265); //Please enter a 'Hour per Day'  between 1 and 24 for absent policy
                control = txtHoursPerDay;
                blnReturnValue = false;
            }
            else if (chkRatePerDay.Checked == true && (txtHoursPerDay.Text.Trim().Length == 0 ? 0.ToDecimal() : txtHoursPerDay.Text.ToDecimal()) > 24.ToDecimal())
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(7265); //Please enter a 'Hour per Day'  between 1 and 24 for absent policy
                control = txtHoursPerDay;
                blnReturnValue = false;
            }
            else if (chkRateOnly.Checked && (txtRate.Text.Trim().Length == 0 || txtRate.Text.ToDecimal()==0))
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(7254); //Please enter valid Rate for absent policy
                control = txtRate;
                blnReturnValue = false;
            }
            else if (txtRate.Text.Trim().Length > 0)
            {
                try
                {
                    decimal rate = Convert.ToDecimal(txtRate.Text);
                }
                catch
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(7254);  //Please enter valid Rate for absent policy
                    control = txtRate;
                    blnReturnValue = false;
                }
            }


            if (blnReturnValue == false)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrClear.Enabled = true;
                if (control != null)
                {
                    errAbsentPolicy.SetError(control, MstrCommonMessage);
                    control.Focus();
                    if (control.Parent.GetType().FullName == "System.Windows.Forms.TabPage")
                        tbPolicy.SelectedTab = (TabPage)control.Parent;

                }
                return blnReturnValue;
            }

            if (chkAbsentPolicyOnly.Checked == false)
            {
                if (chkShortRateOnly.Checked == false && cboShortCalculationBasedOn.SelectedValue.ToInt32() == 0)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(7263); //Could not delete the selected Policy details.
                    control = cboShortCalculationBasedOn;
                    blnReturnValue = false;
                }
                else if (chkShortRateOnly.Checked == false && ((txtShortHoursPerDay.Text.Trim().Length == 0 ? 0.ToDecimal() : txtShortHoursPerDay.Text.ToDecimal()) == 0.ToDecimal()))
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(7266); //Please enter a 'Hour per Day' between 1 and 24 for shortage policy
                    control = txtShortHoursPerDay;
                    blnReturnValue = false;
                }
                else if (chkShortRateOnly.Checked == false && (txtShortHoursPerDay.Text.Trim().Length == 0 ? 0.ToDecimal() : txtShortHoursPerDay.Text.ToDecimal()) > 24)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(7266); //Please enter a 'Hour per Day' between 1 and 24 for shortage policy
                    control = txtShortHoursPerDay;
                    blnReturnValue = false;
                }
                else if (chkShortRateOnly.Checked == true && (txtShortRatePerHour.Text.Trim().Length == 0 || txtShortRatePerHour.Text.ToDecimal()==0))
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(7264); //Please enter valid Rate for shortage policy
                    control = txtShortRatePerHour;
                    blnReturnValue = false;
                }
                else if (chkAbsentPolicyOnly.Checked == false && chkShortRateOnly.Checked == true && txtShortRatePerHour.Text.Trim().Length > 0)
                {
                    try
                    {
                        decimal rate = Convert.ToDecimal(txtShortRatePerHour.Text);
                    }
                    catch
                    {
                        MstrCommonMessage = UserMessage.GetMessageByCode(7264); //Please enter valid Rate for shortage policy
                        control = txtShortRatePerHour;
                        blnReturnValue = false;
                    }
                }
            }
            if (blnReturnValue)
            {
                if (BLLAbsentPolicy.CheckDuplicate(txtPolicyName.Tag.ToInt32(), txtPolicyName.Text.Trim()))//CheckDuplicate()
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(7270); //Duplicate policy name.Please change policy name
                    control = txtPolicyName;
                    blnReturnValue = false;
                }
            }
            if (blnReturnValue == false)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrClear.Enabled = true;
                if (control != null)
                {
                    errAbsentPolicy.SetError(control, MstrCommonMessage);
                    control.Focus();
                    if (control.Parent.GetType().FullName == "System.Windows.Forms.TabPage")
                    {
                        tbPolicy.SelectedTab = (TabPage)control.Parent;
                    }
                }
            }

            return blnReturnValue;
        }
        private bool DeleteValidation()
        {
            if (txtPolicyName.Tag.ToInt32() == 0)
            {
                UserMessage.ShowMessage(9015); // Sorry,No Data Found
                return false;

            }
            if (BLLAbsentPolicy.PolicyIDExists(txtPolicyName.Tag.ToInt32()))
            {
                UserMessage.ShowMessage(7258); //Details exists for this Absent Policy in the system.Please delete the existing details to proceed with Absent Policy deletion.
                return false;
            }
            if (UserMessage.ShowMessage(7269) == false) // Do you wish to Delete Absent policy information?
            {
                return false;
            }
            return true;

        }
        private void FillParameterMaster() // Fill Master Details To DTO 
        {
            BLLAbsentPolicy.DTOAbsentPolicy.AbsentPolicyID = txtPolicyName.Tag.ToInt32();
            BLLAbsentPolicy.DTOAbsentPolicy.AbsentPolicy = txtPolicyName.Text.Trim();
            BLLAbsentPolicy.DTOAbsentPolicy.IsMergeBoth = chkMergeBoth.Checked;

        }
        private void FillParameterDetails() //// Fill Det Details To DTO 
        {
            BLLAbsentPolicy.DTOAbsentPolicy.DTOAbsentPolicyDeteails = new List<clsDTOAbsentPolicyDeteails>();

            clsDTOAbsentPolicyDeteails ObjclsDTOAbsentPolicyDeteails = new clsDTOAbsentPolicyDeteails();
            ObjclsDTOAbsentPolicyDeteails.CalculationID = cboCalculationBased.SelectedValue.ToInt32();
            if (rdbCompanyBased.Checked == true)
            {
                ObjclsDTOAbsentPolicyDeteails.CompanyBasedOn = 1;  // CompanyBasedOn
            }
            else if (rdbActualMonth.Checked == true)
            {
                ObjclsDTOAbsentPolicyDeteails.CompanyBasedOn = 2;  // ActualMonth
            }
            else
            {
                ObjclsDTOAbsentPolicyDeteails.CompanyBasedOn = 3;
            }
            ObjclsDTOAbsentPolicyDeteails.IsRateOnly = chkRateOnly.Checked;
            ObjclsDTOAbsentPolicyDeteails.AbsentRate = chkRateOnly.Checked ? txtRate.Text.Trim().ToDecimal() : 0;

            ObjclsDTOAbsentPolicyDeteails.IsRateBasedOnHour = chkRatePerDay.Checked;
            ObjclsDTOAbsentPolicyDeteails.HoursPerDay = chkRatePerDay.Checked ? txtHoursPerDay.Text.Trim().ToDecimal() : 0;
            ObjclsDTOAbsentPolicyDeteails.AbsentType = (int)AbsentType.Absent;

            BLLAbsentPolicy.DTOAbsentPolicy.DTOAbsentPolicyDeteails.Add(ObjclsDTOAbsentPolicyDeteails);
            if (chkAbsentPolicyOnly.Checked == false)//Shortage
            {

                ObjclsDTOAbsentPolicyDeteails = new clsDTOAbsentPolicyDeteails();
                ObjclsDTOAbsentPolicyDeteails.CalculationID = cboShortCalculationBasedOn.SelectedValue.ToInt32();
                if (rdbShortBasedOnCompany.Checked == true)
                {
                    ObjclsDTOAbsentPolicyDeteails.CompanyBasedOn = 1;
                }
                else if (rdbShortActualMonth.Checked == true)
                {
                    ObjclsDTOAbsentPolicyDeteails.CompanyBasedOn = 2;
                }
                else
                {
                    ObjclsDTOAbsentPolicyDeteails.CompanyBasedOn = 3;
                }
                ObjclsDTOAbsentPolicyDeteails.IsRateOnly = chkShortRateOnly.Checked;
                ObjclsDTOAbsentPolicyDeteails.AbsentRate = chkShortRateOnly.Checked ? txtShortRatePerHour.Text.Trim().ToDecimal() : 0;
                ObjclsDTOAbsentPolicyDeteails.IsRateBasedOnHour = true;//Shortage always in hours
                ObjclsDTOAbsentPolicyDeteails.HoursPerDay = txtShortHoursPerDay.Text.Trim().ToDecimal();
                ObjclsDTOAbsentPolicyDeteails.AbsentType = (int)AbsentType.Shortage;
                BLLAbsentPolicy.DTOAbsentPolicy.DTOAbsentPolicyDeteails.Add(ObjclsDTOAbsentPolicyDeteails);
            }


        }
        private void FillParameterSalPolicyDetails() // fill AddtionDeduction Details
        {
            BLLAbsentPolicy.DTOAbsentPolicy.DTOSalaryPolicyDetailAbsent = new List<clsDTOSalaryPolicyDetailAbsent>();

            if (dgvAbsentDetails.Rows.Count > 0)
            {
                dgvAbsentDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                dgvAbsentDetails.CurrentCell = dgvAbsentDetails["colAbsentParticulars", 0];
                foreach (DataGridViewRow row in dgvAbsentDetails.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["colAbsentSelectedItem"].Value) == true)
                    {
                        clsDTOSalaryPolicyDetailAbsent objSalaryPolicyDetail = new clsDTOSalaryPolicyDetailAbsent();
                        objSalaryPolicyDetail.PolicyType = (int)PolicyType.Absent;
                        objSalaryPolicyDetail.AdditionDeductionID = row.Cells["colAbsentAddDedID"].Value.ToInt32();
                        BLLAbsentPolicy.DTOAbsentPolicy.DTOSalaryPolicyDetailAbsent.Add(objSalaryPolicyDetail);
                    }
                }
            }
            if (chkAbsentPolicyOnly.Checked == false)
            {
                if (dgvShortageDetails.Rows.Count > 0)
                {
                    dgvShortageDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    dgvShortageDetails.CurrentCell = dgvShortageDetails["colShortParticulars", 0];
                    foreach (DataGridViewRow rowItem in dgvShortageDetails.Rows)
                    {
                        if (Convert.ToBoolean(rowItem.Cells["colShortSelectedItem"].Value) == true)
                        {
                            clsDTOSalaryPolicyDetailAbsent ObjPolicyDet = new clsDTOSalaryPolicyDetailAbsent();
                            ObjPolicyDet.PolicyType = (int)PolicyType.Shortage;
                            ObjPolicyDet.AdditionDeductionID = rowItem.Cells["colShortAddDedID"].Value.ToInt32();
                            BLLAbsentPolicy.DTOAbsentPolicy.DTOSalaryPolicyDetailAbsent.Add(ObjPolicyDet);

                        }
                    }
                }
            }

        }
        private bool SaveAbsentPolicy() // Save Absent Policy Details
        {
            bool blnRetValue = false;

            if (FormValidation())
            {
                int intMessageCode = 0;
                if (txtPolicyName.Tag.ToInt32() > 0)
                    intMessageCode = 7268;
                else
                    intMessageCode = 7267;

                if (UserMessage.ShowMessage(intMessageCode))
                {
                    FillParameterMaster();
                    FillParameterDetails();
                    FillParameterSalPolicyDetails();
                    if (BLLAbsentPolicy.AbsentPolicyMasterSave())
                    {
                        if (!MblnIsEditMode)
                        {
                            lblstatus.Text = UserMessage.GetMessageByCode(2);
                            UserMessage.ShowMessage(2, null, null);
                        }
                        else
                        {
                            lblstatus.Text = UserMessage.GetMessageByCode(21);
                            UserMessage.ShowMessage(21, null, null);
                        }
                        blnRetValue = true;
                        txtPolicyName.Tag = BLLAbsentPolicy.DTOAbsentPolicy.AbsentPolicyID;
                        PintAbsentPolicyID = BLLAbsentPolicy.DTOAbsentPolicy.AbsentPolicyID;
                        if (!MblnIsEditMode)
                        {
                            MintCurrentRecCnt = MintCurrentRecCnt + 1;
                            BindingNavigatorMoveLastItem_Click(null, null);
                        }

                        //BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                        //btnEmail.Enabled = MblnPrintEmailPermission;
                        //btnPrint.Enabled = MblnPrintEmailPermission;
                        //BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                    }

                }
            }

            return blnRetValue;
        }

        private bool DeleteAbsentPolicy()  // Delete Absent Policy Reference
        {
            bool blnRetValue = false;
            if (DeleteValidation())
            {
                BLLAbsentPolicy.DTOAbsentPolicy.AbsentPolicyID = txtPolicyName.Tag.ToInt32();
                if (BLLAbsentPolicy.DeleteAbsentPolicy())
                {
                    AddNewAbsentPolicy();
                    blnRetValue = true;
                }

            }
            return blnRetValue;

        }
        private void ResetForm(object sender, System.EventArgs e) // To reset Froms
        {
            if (sender is CheckBox)
            {
                CheckBox chk = (CheckBox)sender;
                if (chk.Name == chkRateOnly.Name)
                {
                    txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
                    txtRate.Enabled = chkRateOnly.Checked ? true : false;

                    cboCalculationBased.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Window : System.Drawing.SystemColors.Info;
                    if (chkRateOnly.Checked)
                    {
                        cboCalculationBased.SelectedIndex = -1;
                    }
                    else
                    {
                        cboCalculationBased.SelectedIndex = 0;
                        txtRate.Text = "";
                    }
                    cboCalculationBased.Enabled = chkRateOnly.Checked ? false : true;
                   
                    txtHoursPerDay.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
                    if (chkRateOnly.Checked)
                        chkRatePerDay.Checked = false;

               txtHoursPerDay.Enabled = !txtRate.Enabled;
                }
                else if (chk.Name == chkRatePerDay.Name)
                {
                    if (chkRatePerDay.Checked)
                    {
                        chkRateOnly.Checked = false;
                    }
                    else
                        txtHoursPerDay.Text = "";

                }
                rdbCompanyBased.Enabled = (chkRateOnly.Checked ) ? false : true;
                rdbActualMonth.Enabled = (chkRateOnly.Checked) ? false : true;
                //rdbWorkingDays.Enabled = (chkRateOnly.Checked ) ? false : true;

                txtHoursPerDay.BackColor = chkRatePerDay.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
                txtHoursPerDay.Enabled = chkRatePerDay.Checked ? true : false;
      
            }
            Changestatus(null, null);



        }
        #endregion Methods

        #region Events

        private void FrmAbsentPolicy_Load(object sender, EventArgs e) // Load 
        {
            SetPermissions();
            LoadCombos(0);
            if (PintAbsentPolicyID > 0)  // Calling Absent Policy From Another Froms 
            {
                GetRecordCount();
                RefernceDisplay();
            }
            else
            {
                AddNewAbsentPolicy();
            }
        }

        private void FrmAbsentPolicy_FormClosing(object sender, FormClosingEventArgs e) // Form Close Event
        {
            if (btnSave.Enabled)
            {
                if (UserMessage.ShowMessage(8))
                {
                    e.Cancel = false;
                }
                else
                { e.Cancel = true; }
            }
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e) // Button Action Move First
        {
            GetRecordCount();
            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;
                DisplayAbsentPolicyInfo();
                lblstatus.Text = UserMessage.GetMessageByCode(9);
                tmrClear.Enabled = true;
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e) // Button Action Move Previous
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    DisplayAbsentPolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(10);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 1;
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e) // Button Action Move Next
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                }

                if (MintCurrentRecCnt > MintRecordCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                }
                else
                {
                    DisplayAbsentPolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(11);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 0;
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e) // Button Action Move Last
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    DisplayAbsentPolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(12);
                    tmrClear.Enabled = true;
                }
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e) // Add Button Click Event
        {
            AddNewAbsentPolicy();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e) // Delete Button Click Event
        {
            if (DeleteAbsentPolicy())
            {
                lblstatus.Text = UserMessage.GetMessageByCode(4);
                tmrClear.Enabled = true;
                UserMessage.ShowMessage(4);
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e) // Save Button Click Event
        {
            if (SaveAbsentPolicy())
            {
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false;
            }
        }

        private void btnClear_Click(object sender, EventArgs e) // Clear Button Click Event
        {
            AddNewAbsentPolicy();
        }


        private void btnSave_Click(object sender, EventArgs e) // Save Button Click Event
        {
            if (SaveAbsentPolicy())
            {
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false;
            }
        }

        private void btnOk_Click(object sender, EventArgs e) // Ok Button Click Event
        {
            if (SaveAbsentPolicy())
            {
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)// Cancel Button Click Event
        {
            this.Close();
        }



        private void rdbCompanyBased_CheckedChanged(object sender, EventArgs e) // CompanyBased CheckedChanged
        {
            ResetForm(sender, e);
        }

        private void chkRatePerDay_CheckedChanged(object sender, EventArgs e) //RatePerDay_CheckedChanged
        {
            ResetForm(sender, e);
        }

        private void chkRateOnly_CheckedChanged(object sender, EventArgs e) //RateOnly_CheckedChanged
        {
            ResetForm(sender, e);
        }

        private void rdbShortBasedOnCompany_CheckedChanged(object sender, EventArgs e)//ShortBasedOnCompany_CheckedChanged
        {
            Changestatus(null, null);
        }

        private void chkShortRateOnly_CheckedChanged(object sender, EventArgs e) //ShortRateOnly_CheckedChanged
        {
            Changestatus(null, null);
            if (chkShortRateOnly.Checked == true)
            {
                txtShortRatePerHour.BackColor = System.Drawing.SystemColors.Info;
                txtShortRatePerHour.Enabled = true;
                cboShortCalculationBasedOn.BackColor = System.Drawing.SystemColors.Window;
                cboShortCalculationBasedOn.SelectedIndex = -1;
                cboShortCalculationBasedOn.Enabled = false;
                rdbShortBasedOnCompany.Enabled = false;
                rdbShortActualMonth.Enabled = false;
                //rdbShortageWorkingDays.Enabled = false;
                txtShortHoursPerDay.BackColor = System.Drawing.SystemColors.Window;
                txtShortHoursPerDay.Enabled = false;
                txtShortHoursPerDay.Text = "";
            }
            else
            {
                txtShortRatePerHour.BackColor = System.Drawing.SystemColors.Window;
                txtShortRatePerHour.Enabled = false;
                txtShortRatePerHour.Text = "";
                cboShortCalculationBasedOn.BackColor = System.Drawing.SystemColors.Info;
                cboShortCalculationBasedOn.SelectedIndex = 0;
                cboShortCalculationBasedOn.Enabled = true;
                rdbShortBasedOnCompany.Enabled = true;
                rdbShortActualMonth.Enabled = true;
                //rdbShortageWorkingDays.Enabled = true;
                txtShortHoursPerDay.BackColor = System.Drawing.SystemColors.Info;
                txtShortHoursPerDay.Enabled = true;

            }
        }

        private void tmrClear_Tick(object sender, EventArgs e) 
        {
            tmrClear.Enabled = false;
            lblstatus.Text = "";
        }

        private void txtDecimal_KeyPress(object sender, KeyPressEventArgs e) // Key Press For TextDecimal
        {
            string strInvalidChars;

            strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            //if (ClsCommonSettings.IsAmountRoundByZero)
            //{
            //    strInvalidChars = strInvalidChars + ".";
            //}

            e.Handled = false;
            if (((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))))
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }

        private void txtint_KeyPress(object sender, KeyPressEventArgs e) // Key Press For txtint_KeyPress
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[].";
            e.Handled = false;
            if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }

        private void chkAbsentPolicyOnly_CheckedChanged(object sender, EventArgs e) // AbsentPolicyOnly_CheckedChanged
        {
            Changestatus(null, null);
            if (chkAbsentPolicyOnly.Checked == true)
            {
                tbPolicy.SelectedIndex = 0;
                chkMergeBoth.Checked = false;
                chkMergeBoth.Enabled = false;
                // tbPolicy.TabPages.RemoveAt(1);
            }
            else
            {
                chkMergeBoth.Enabled = true;
            }

        }

        private void chkMergeBoth_CheckedChanged(object sender, EventArgs e)//chkMergeBoth_CheckedChanged
        {
            Changestatus(null, null);
        }

        private void tbPolicy_SelectedIndexChanged(object sender, EventArgs e)//tbPolicy_SelectedIndexChanged
        {
            if (tbPolicy.SelectedIndex == 1 && chkAbsentPolicyOnly.Checked == true)
                tbPolicy.SelectedIndex = 0;

        }

        private void txtPolicyName_TextChanged(object sender, EventArgs e)// txtPolicyName_TextChanged
        {
            ResetForm(sender, e);
            Changestatus(null, null);
        }

        private void dgvAbsentDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch
            {
            }
        }

        private void dgvAbsentDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvAbsentDetails.IsCurrentCellDirty)
                {

                    dgvAbsentDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }

        private void dgvShortageDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvShortageDetails.IsCurrentCellDirty)
                {

                    dgvShortageDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }
        private void dgvShortageDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e) //dgvShortageDetails_CellValueChanged
        {
            Changestatus(null, null);
        }

        private void dgvAbsentDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e) //dgvAbsentDetails_CellValueChanged
        {
            Changestatus(null, null);
        }

        private void dgvShortageDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void cboCalculationBased_SelectedIndexChanged(object sender, EventArgs e) //cboCalculationBased_SelectedIndexChanged
        {
            //Absent Tab
            Changestatus(null, null);
            if (cboCalculationBased.SelectedValue.ToInt32() > 0)
            {
                dgvAbsentDetails.Enabled = true;

                if (cboCalculationBased.SelectedValue.ToInt32() == (int)CalculationType.GrossSalary)
                {
                    DataTable dtADDDedd = BLLAbsentPolicy.GetAdditionDeductions();
                    FillAdditionDeductionsGrid(dgvAbsentDetails, dtADDDedd);
                }
                else
                {
                    dgvAbsentDetails.Rows.Clear();
                    dgvAbsentDetails.Enabled = false;
                }
            }
            else
            {
                dgvAbsentDetails.Rows.Clear();
                dgvAbsentDetails.Enabled = false;
            }
        }

        private void cboShortCalculationBasedOn_SelectedIndexChanged(object sender, EventArgs e) //cboShortCalculationBasedOn_SelectedIndexChanged
        {
            //Shortage Tab
            Changestatus(null, null);
            if (cboShortCalculationBasedOn.SelectedValue.ToInt32() > 0)
            {
                dgvShortageDetails.Enabled = true;
                if (cboShortCalculationBasedOn.SelectedValue.ToInt32() == (int)CalculationType.GrossSalary)
                {
                    DataTable dtADDDedd = BLLAbsentPolicy.GetAdditionDeductions();
                    FillAdditionDeductionsGrid(dgvShortageDetails, dtADDDedd);
                }
                else
                {
                    dgvShortageDetails.Rows.Clear();
                    dgvShortageDetails.Enabled = false;
                }
            }
            else
            {
                dgvShortageDetails.Rows.Clear();
                dgvShortageDetails.Enabled = false;
            }

        }

        private void cboCalculationBased_KeyDown(object sender, KeyEventArgs e) //cboCalculationBased_KeyDown for Like Search
        {
            cboCalculationBased.DroppedDown = false;
            if (e.KeyData == Keys.Enter)
                cboCalculationBased_SelectedIndexChanged(sender, new EventArgs());

        }

        private void cboShortCalculationBasedOn_KeyDown(object sender, KeyEventArgs e) //cboShortCalculationBasedOn_KeyDown for Like Search
        {
            cboShortCalculationBasedOn.DroppedDown = false;
            if (e.KeyData == Keys.Enter)
                cboShortCalculationBasedOn_SelectedIndexChanged(sender, new EventArgs());
        }

        private void txtShortRatePerHour_TextChanged(object sender, EventArgs e) //txtShortRatePerHour_TextChanged 
        {
            Changestatus(null, null);
        }

        private void dgvAbsentDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e) //dgvAbsentDetails_CellBeginEdit
        {
            Changestatus(null, null);
        }

        private void txtShortHoursPerDay_TextChanged(object sender, EventArgs e) //txtShortHoursPerDay_TextChanged
        {
            Changestatus(null, null);
        }

        #endregion Events

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "SalaryStructure";
            objHelp.ShowDialog();
            objHelp = null;
        }

       










    }
}
