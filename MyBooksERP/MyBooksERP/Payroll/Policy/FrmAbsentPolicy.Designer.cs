﻿namespace MyBooksERP
{
    partial class FrmAbsentPolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label2;
            System.Windows.Forms.Label Label4;
            System.Windows.Forms.Label Label1;
            System.Windows.Forms.Label lblExcludebasedOnS;
            System.Windows.Forms.Label lblcalculationDayS;
            System.Windows.Forms.Label lblCalculationBasedOnS;
            System.Windows.Forms.Label DescriptionLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAbsentPolicy));
            this.AbsentPolicyBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.chkAbsentPolicyOnly = new System.Windows.Forms.CheckBox();
            this.chkMergeBoth = new System.Windows.Forms.CheckBox();
            this.tbPolicy = new System.Windows.Forms.TabControl();
            this.tabpgAbsent = new System.Windows.Forms.TabPage();
            this.dgvAbsentDetails = new DemoClsDataGridview.ClsDataGirdView();
            this.colAbsentAddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAbsentSelectedItem = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colAbsentParticulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboCalculationBased = new System.Windows.Forms.ComboBox();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.chkRateOnly = new System.Windows.Forms.CheckBox();
            this.rdbActualMonth = new System.Windows.Forms.RadioButton();
            this.rdbCompanyBased = new System.Windows.Forms.RadioButton();
            this.lblHoursPerDay = new System.Windows.Forms.Label();
            this.txtHoursPerDay = new System.Windows.Forms.TextBox();
            this.lblRate = new System.Windows.Forms.Label();
            this.chkRatePerDay = new System.Windows.Forms.CheckBox();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.tabpgShortage = new System.Windows.Forms.TabPage();
            this.dgvShortageDetails = new DemoClsDataGridview.ClsDataGirdView();
            this.colShortAddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colShortSelectedItem = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colShortParticulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboShortCalculationBasedOn = new System.Windows.Forms.ComboBox();
            this.txtShortRatePerHour = new System.Windows.Forms.TextBox();
            this.chkShortRateOnly = new System.Windows.Forms.CheckBox();
            this.rdbShortActualMonth = new System.Windows.Forms.RadioButton();
            this.rdbShortBasedOnCompany = new System.Windows.Forms.RadioButton();
            this.lblHoursPerHour = new System.Windows.Forms.Label();
            this.txtShortHoursPerDay = new System.Windows.Forms.TextBox();
            this.lblRatePerHourS = new System.Windows.Forms.Label();
            this.shapeContainer3 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.SelectValue = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SelectValueS = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtPolicyName = new System.Windows.Forms.TextBox();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lblHeader = new System.Windows.Forms.Label();
            this.errAbsentPolicy = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.AddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Particulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddDedIDS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParticularsS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            Label2 = new System.Windows.Forms.Label();
            Label4 = new System.Windows.Forms.Label();
            Label1 = new System.Windows.Forms.Label();
            lblExcludebasedOnS = new System.Windows.Forms.Label();
            lblcalculationDayS = new System.Windows.Forms.Label();
            lblCalculationBasedOnS = new System.Windows.Forms.Label();
            DescriptionLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.AbsentPolicyBindingNavigator)).BeginInit();
            this.AbsentPolicyBindingNavigator.SuspendLayout();
            this.tbPolicy.SuspendLayout();
            this.tabpgAbsent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAbsentDetails)).BeginInit();
            this.tabpgShortage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShortageDetails)).BeginInit();
            this.ssStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errAbsentPolicy)).BeginInit();
            this.SuspendLayout();
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Location = new System.Drawing.Point(14, 100);
            Label2.Name = "Label2";
            Label2.Size = new System.Drawing.Size(101, 13);
            Label2.TabIndex = 38;
            Label2.Text = "Exclude from Gross ";
            // 
            // Label4
            // 
            Label4.AutoSize = true;
            Label4.Location = new System.Drawing.Point(11, 11);
            Label4.Name = "Label4";
            Label4.Size = new System.Drawing.Size(81, 13);
            Label4.TabIndex = 17;
            Label4.Text = "Calculation Day";
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new System.Drawing.Point(14, 65);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(109, 13);
            Label1.TabIndex = 36;
            Label1.Text = "Calculation Based On";
            // 
            // lblExcludebasedOnS
            // 
            lblExcludebasedOnS.AutoSize = true;
            lblExcludebasedOnS.Location = new System.Drawing.Point(14, 101);
            lblExcludebasedOnS.Name = "lblExcludebasedOnS";
            lblExcludebasedOnS.Size = new System.Drawing.Size(101, 13);
            lblExcludebasedOnS.TabIndex = 51;
            lblExcludebasedOnS.Text = "Exclude from Gross ";
            // 
            // lblcalculationDayS
            // 
            lblcalculationDayS.AutoSize = true;
            lblcalculationDayS.Location = new System.Drawing.Point(14, 11);
            lblcalculationDayS.Name = "lblcalculationDayS";
            lblcalculationDayS.Size = new System.Drawing.Size(81, 13);
            lblcalculationDayS.TabIndex = 47;
            lblcalculationDayS.Text = "Calculation Day";
            // 
            // lblCalculationBasedOnS
            // 
            lblCalculationBasedOnS.AutoSize = true;
            lblCalculationBasedOnS.Location = new System.Drawing.Point(14, 66);
            lblCalculationBasedOnS.Name = "lblCalculationBasedOnS";
            lblCalculationBasedOnS.Size = new System.Drawing.Size(109, 13);
            lblCalculationBasedOnS.TabIndex = 49;
            lblCalculationBasedOnS.Text = "Calculation Based On";
            // 
            // DescriptionLabel
            // 
            DescriptionLabel.AutoSize = true;
            DescriptionLabel.Location = new System.Drawing.Point(2, 49);
            DescriptionLabel.Name = "DescriptionLabel";
            DescriptionLabel.Size = new System.Drawing.Size(66, 13);
            DescriptionLabel.TabIndex = 306;
            DescriptionLabel.Text = "Policy Name";
            // 
            // AbsentPolicyBindingNavigator
            // 
            this.AbsentPolicyBindingNavigator.AddNewItem = null;
            this.AbsentPolicyBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.AbsentPolicyBindingNavigator.DeleteItem = null;
            this.AbsentPolicyBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorSaveItem,
            this.btnClear,
            this.btnPrint,
            this.btnEmail,
            this.ToolStripSeparator2,
            this.btnHelp});
            this.AbsentPolicyBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.AbsentPolicyBindingNavigator.MoveFirstItem = null;
            this.AbsentPolicyBindingNavigator.MoveLastItem = null;
            this.AbsentPolicyBindingNavigator.MoveNextItem = null;
            this.AbsentPolicyBindingNavigator.MovePreviousItem = null;
            this.AbsentPolicyBindingNavigator.Name = "AbsentPolicyBindingNavigator";
            this.AbsentPolicyBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.AbsentPolicyBindingNavigator.Size = new System.Drawing.Size(404, 25);
            this.AbsentPolicyBindingNavigator.TabIndex = 300;
            this.AbsentPolicyBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save Data";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Visible = false;
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Visible = false;
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.Text = "He&lp";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // chkAbsentPolicyOnly
            // 
            this.chkAbsentPolicyOnly.AutoSize = true;
            this.chkAbsentPolicyOnly.Location = new System.Drawing.Point(6, 75);
            this.chkAbsentPolicyOnly.Name = "chkAbsentPolicyOnly";
            this.chkAbsentPolicyOnly.Size = new System.Drawing.Size(114, 17);
            this.chkAbsentPolicyOnly.TabIndex = 1;
            this.chkAbsentPolicyOnly.Text = "Absent Policy Only";
            this.chkAbsentPolicyOnly.UseVisualStyleBackColor = true;
            this.chkAbsentPolicyOnly.CheckedChanged += new System.EventHandler(this.chkAbsentPolicyOnly_CheckedChanged);
            // 
            // chkMergeBoth
            // 
            this.chkMergeBoth.AutoSize = true;
            this.chkMergeBoth.Location = new System.Drawing.Point(129, 75);
            this.chkMergeBoth.Name = "chkMergeBoth";
            this.chkMergeBoth.Size = new System.Drawing.Size(249, 17);
            this.chkMergeBoth.TabIndex = 2;
            this.chkMergeBoth.Text = "Merge Absent && Shortage Amount in Salary Slip";
            this.chkMergeBoth.UseVisualStyleBackColor = true;
            this.chkMergeBoth.CheckedChanged += new System.EventHandler(this.chkMergeBoth_CheckedChanged);
            // 
            // tbPolicy
            // 
            this.tbPolicy.Controls.Add(this.tabpgAbsent);
            this.tbPolicy.Controls.Add(this.tabpgShortage);
            this.tbPolicy.Location = new System.Drawing.Point(5, 98);
            this.tbPolicy.Name = "tbPolicy";
            this.tbPolicy.SelectedIndex = 0;
            this.tbPolicy.Size = new System.Drawing.Size(390, 358);
            this.tbPolicy.TabIndex = 3;
            this.tbPolicy.SelectedIndexChanged += new System.EventHandler(this.tbPolicy_SelectedIndexChanged);
            // 
            // tabpgAbsent
            // 
            this.tabpgAbsent.Controls.Add(this.dgvAbsentDetails);
            this.tabpgAbsent.Controls.Add(Label2);
            this.tabpgAbsent.Controls.Add(Label4);
            this.tabpgAbsent.Controls.Add(this.cboCalculationBased);
            this.tabpgAbsent.Controls.Add(this.txtRate);
            this.tabpgAbsent.Controls.Add(Label1);
            this.tabpgAbsent.Controls.Add(this.chkRateOnly);
            this.tabpgAbsent.Controls.Add(this.rdbActualMonth);
            this.tabpgAbsent.Controls.Add(this.rdbCompanyBased);
            this.tabpgAbsent.Controls.Add(this.lblHoursPerDay);
            this.tabpgAbsent.Controls.Add(this.txtHoursPerDay);
            this.tabpgAbsent.Controls.Add(this.lblRate);
            this.tabpgAbsent.Controls.Add(this.chkRatePerDay);
            this.tabpgAbsent.Controls.Add(this.shapeContainer2);
            this.tabpgAbsent.Location = new System.Drawing.Point(4, 22);
            this.tabpgAbsent.Name = "tabpgAbsent";
            this.tabpgAbsent.Padding = new System.Windows.Forms.Padding(3);
            this.tabpgAbsent.Size = new System.Drawing.Size(382, 332);
            this.tabpgAbsent.TabIndex = 0;
            this.tabpgAbsent.Text = "Absent";
            this.tabpgAbsent.UseVisualStyleBackColor = true;
            // 
            // dgvAbsentDetails
            // 
            this.dgvAbsentDetails.AddNewRow = false;
            this.dgvAbsentDetails.AllowUserToAddRows = false;
            this.dgvAbsentDetails.AllowUserToDeleteRows = false;
            this.dgvAbsentDetails.AllowUserToResizeColumns = false;
            this.dgvAbsentDetails.AllowUserToResizeRows = false;
            this.dgvAbsentDetails.AlphaNumericCols = new int[0];
            this.dgvAbsentDetails.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvAbsentDetails.CapsLockCols = new int[0];
            this.dgvAbsentDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAbsentDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colAbsentAddDedID,
            this.colAbsentSelectedItem,
            this.colAbsentParticulars});
            this.dgvAbsentDetails.DecimalCols = new int[0];
            this.dgvAbsentDetails.HasSlNo = false;
            this.dgvAbsentDetails.LastRowIndex = 0;
            this.dgvAbsentDetails.Location = new System.Drawing.Point(138, 89);
            this.dgvAbsentDetails.MultiSelect = false;
            this.dgvAbsentDetails.Name = "dgvAbsentDetails";
            this.dgvAbsentDetails.NegativeValueCols = new int[0];
            this.dgvAbsentDetails.NumericCols = new int[0];
            this.dgvAbsentDetails.RowHeadersWidth = 35;
            this.dgvAbsentDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvAbsentDetails.Size = new System.Drawing.Size(231, 122);
            this.dgvAbsentDetails.TabIndex = 3;
            this.dgvAbsentDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAbsentDetails_CellValueChanged);
            this.dgvAbsentDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvAbsentDetails_CellBeginEdit);
            this.dgvAbsentDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvAbsentDetails_CurrentCellDirtyStateChanged);
            this.dgvAbsentDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvAbsentDetails_DataError);
            // 
            // colAbsentAddDedID
            // 
            this.colAbsentAddDedID.HeaderText = "AddDedID";
            this.colAbsentAddDedID.Name = "colAbsentAddDedID";
            this.colAbsentAddDedID.Visible = false;
            // 
            // colAbsentSelectedItem
            // 
            this.colAbsentSelectedItem.HeaderText = "";
            this.colAbsentSelectedItem.Name = "colAbsentSelectedItem";
            this.colAbsentSelectedItem.Width = 30;
            // 
            // colAbsentParticulars
            // 
            this.colAbsentParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colAbsentParticulars.HeaderText = "Particulars";
            this.colAbsentParticulars.Name = "colAbsentParticulars";
            this.colAbsentParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cboCalculationBased
            // 
            this.cboCalculationBased.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCalculationBased.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCalculationBased.BackColor = System.Drawing.SystemColors.Info;
            this.cboCalculationBased.DropDownHeight = 134;
            this.cboCalculationBased.FormattingEnabled = true;
            this.cboCalculationBased.IntegralHeight = false;
            this.cboCalculationBased.Location = new System.Drawing.Point(138, 62);
            this.cboCalculationBased.MaxDropDownItems = 10;
            this.cboCalculationBased.Name = "cboCalculationBased";
            this.cboCalculationBased.Size = new System.Drawing.Size(231, 21);
            this.cboCalculationBased.TabIndex = 2;
            this.cboCalculationBased.SelectedIndexChanged += new System.EventHandler(this.cboCalculationBased_SelectedIndexChanged);
            this.cboCalculationBased.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCalculationBased_KeyDown);
            // 
            // txtRate
            // 
            this.txtRate.Location = new System.Drawing.Point(138, 302);
            this.txtRate.MaxLength = 9;
            this.txtRate.Name = "txtRate";
            this.txtRate.ShortcutsEnabled = false;
            this.txtRate.Size = new System.Drawing.Size(231, 20);
            this.txtRate.TabIndex = 7;
            this.txtRate.TextChanged += new System.EventHandler(this.Changestatus);
            this.txtRate.Validated += new System.EventHandler(this.ResetForm);
            this.txtRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // chkRateOnly
            // 
            this.chkRateOnly.AutoSize = true;
            this.chkRateOnly.Location = new System.Drawing.Point(138, 279);
            this.chkRateOnly.Name = "chkRateOnly";
            this.chkRateOnly.Size = new System.Drawing.Size(71, 17);
            this.chkRateOnly.TabIndex = 6;
            this.chkRateOnly.Text = "Rate only";
            this.chkRateOnly.UseVisualStyleBackColor = true;
            this.chkRateOnly.CheckedChanged += new System.EventHandler(this.chkRateOnly_CheckedChanged);
            // 
            // rdbActualMonth
            // 
            this.rdbActualMonth.AutoSize = true;
            this.rdbActualMonth.Location = new System.Drawing.Point(217, 35);
            this.rdbActualMonth.Name = "rdbActualMonth";
            this.rdbActualMonth.Size = new System.Drawing.Size(88, 17);
            this.rdbActualMonth.TabIndex = 1;
            this.rdbActualMonth.TabStop = true;
            this.rdbActualMonth.Text = "Actual Month";
            this.rdbActualMonth.UseVisualStyleBackColor = true;
            this.rdbActualMonth.CheckedChanged += new System.EventHandler(this.ResetForm);
            // 
            // rdbCompanyBased
            // 
            this.rdbCompanyBased.AutoSize = true;
            this.rdbCompanyBased.Location = new System.Drawing.Point(78, 35);
            this.rdbCompanyBased.Name = "rdbCompanyBased";
            this.rdbCompanyBased.Size = new System.Drawing.Size(117, 17);
            this.rdbCompanyBased.TabIndex = 0;
            this.rdbCompanyBased.TabStop = true;
            this.rdbCompanyBased.Text = "Based on Company";
            this.rdbCompanyBased.UseVisualStyleBackColor = true;
            this.rdbCompanyBased.CheckedChanged += new System.EventHandler(this.rdbCompanyBased_CheckedChanged);
            // 
            // lblHoursPerDay
            // 
            this.lblHoursPerDay.AutoSize = true;
            this.lblHoursPerDay.Location = new System.Drawing.Point(14, 259);
            this.lblHoursPerDay.Name = "lblHoursPerDay";
            this.lblHoursPerDay.Size = new System.Drawing.Size(76, 13);
            this.lblHoursPerDay.TabIndex = 15;
            this.lblHoursPerDay.Text = "Hours Per Day";
            // 
            // txtHoursPerDay
            // 
            this.txtHoursPerDay.BackColor = System.Drawing.SystemColors.Window;
            this.txtHoursPerDay.Location = new System.Drawing.Point(138, 252);
            this.txtHoursPerDay.MaxLength = 5;
            this.txtHoursPerDay.Name = "txtHoursPerDay";
            this.txtHoursPerDay.ShortcutsEnabled = false;
            this.txtHoursPerDay.Size = new System.Drawing.Size(231, 20);
            this.txtHoursPerDay.TabIndex = 5;
            this.txtHoursPerDay.TextChanged += new System.EventHandler(this.ResetForm);
            this.txtHoursPerDay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtint_KeyPress);
            // 
            // lblRate
            // 
            this.lblRate.AutoSize = true;
            this.lblRate.Location = new System.Drawing.Point(14, 305);
            this.lblRate.Name = "lblRate";
            this.lblRate.Size = new System.Drawing.Size(66, 13);
            this.lblRate.TabIndex = 14;
            this.lblRate.Text = "Absent Rate";
            // 
            // chkRatePerDay
            // 
            this.chkRatePerDay.AutoSize = true;
            this.chkRatePerDay.Location = new System.Drawing.Point(138, 229);
            this.chkRatePerDay.Name = "chkRatePerDay";
            this.chkRatePerDay.Size = new System.Drawing.Size(123, 17);
            this.chkRatePerDay.TabIndex = 4;
            this.chkRatePerDay.Text = "Rate Based on Hour";
            this.chkRatePerDay.UseVisualStyleBackColor = true;
            this.chkRatePerDay.CheckedChanged += new System.EventHandler(this.chkRatePerDay_CheckedChanged);
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3});
            this.shapeContainer2.Size = new System.Drawing.Size(376, 326);
            this.shapeContainer2.TabIndex = 41;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 9;
            this.lineShape3.X2 = 367;
            this.lineShape3.Y1 = 24;
            this.lineShape3.Y2 = 24;
            // 
            // tabpgShortage
            // 
            this.tabpgShortage.Controls.Add(this.dgvShortageDetails);
            this.tabpgShortage.Controls.Add(lblExcludebasedOnS);
            this.tabpgShortage.Controls.Add(lblcalculationDayS);
            this.tabpgShortage.Controls.Add(this.cboShortCalculationBasedOn);
            this.tabpgShortage.Controls.Add(this.txtShortRatePerHour);
            this.tabpgShortage.Controls.Add(lblCalculationBasedOnS);
            this.tabpgShortage.Controls.Add(this.chkShortRateOnly);
            this.tabpgShortage.Controls.Add(this.rdbShortActualMonth);
            this.tabpgShortage.Controls.Add(this.rdbShortBasedOnCompany);
            this.tabpgShortage.Controls.Add(this.lblHoursPerHour);
            this.tabpgShortage.Controls.Add(this.txtShortHoursPerDay);
            this.tabpgShortage.Controls.Add(this.lblRatePerHourS);
            this.tabpgShortage.Controls.Add(this.shapeContainer3);
            this.tabpgShortage.Location = new System.Drawing.Point(4, 22);
            this.tabpgShortage.Name = "tabpgShortage";
            this.tabpgShortage.Padding = new System.Windows.Forms.Padding(3);
            this.tabpgShortage.Size = new System.Drawing.Size(382, 332);
            this.tabpgShortage.TabIndex = 1;
            this.tabpgShortage.Text = "Shortage";
            this.tabpgShortage.UseVisualStyleBackColor = true;
            // 
            // dgvShortageDetails
            // 
            this.dgvShortageDetails.AddNewRow = false;
            this.dgvShortageDetails.AllowUserToAddRows = false;
            this.dgvShortageDetails.AllowUserToDeleteRows = false;
            this.dgvShortageDetails.AllowUserToResizeColumns = false;
            this.dgvShortageDetails.AllowUserToResizeRows = false;
            this.dgvShortageDetails.AlphaNumericCols = new int[0];
            this.dgvShortageDetails.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvShortageDetails.CapsLockCols = new int[0];
            this.dgvShortageDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShortageDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colShortAddDedID,
            this.colShortSelectedItem,
            this.colShortParticulars});
            this.dgvShortageDetails.DecimalCols = new int[0];
            this.dgvShortageDetails.HasSlNo = false;
            this.dgvShortageDetails.LastRowIndex = 0;
            this.dgvShortageDetails.Location = new System.Drawing.Point(138, 101);
            this.dgvShortageDetails.MultiSelect = false;
            this.dgvShortageDetails.Name = "dgvShortageDetails";
            this.dgvShortageDetails.NegativeValueCols = new int[0];
            this.dgvShortageDetails.NumericCols = new int[0];
            this.dgvShortageDetails.RowHeadersWidth = 35;
            this.dgvShortageDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvShortageDetails.Size = new System.Drawing.Size(231, 138);
            this.dgvShortageDetails.TabIndex = 3;
            this.dgvShortageDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvShortageDetails_CellValueChanged);
            this.dgvShortageDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvShortageDetails_CurrentCellDirtyStateChanged);
            this.dgvShortageDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvShortageDetails_DataError);
            // 
            // colShortAddDedID
            // 
            this.colShortAddDedID.HeaderText = "AddDedID";
            this.colShortAddDedID.Name = "colShortAddDedID";
            this.colShortAddDedID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colShortAddDedID.Visible = false;
            // 
            // colShortSelectedItem
            // 
            this.colShortSelectedItem.HeaderText = "";
            this.colShortSelectedItem.Name = "colShortSelectedItem";
            this.colShortSelectedItem.Width = 30;
            // 
            // colShortParticulars
            // 
            this.colShortParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colShortParticulars.HeaderText = "Particulars";
            this.colShortParticulars.Name = "colShortParticulars";
            this.colShortParticulars.ReadOnly = true;
            this.colShortParticulars.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colShortParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cboShortCalculationBasedOn
            // 
            this.cboShortCalculationBasedOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboShortCalculationBasedOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboShortCalculationBasedOn.BackColor = System.Drawing.SystemColors.Info;
            this.cboShortCalculationBasedOn.DropDownHeight = 134;
            this.cboShortCalculationBasedOn.FormattingEnabled = true;
            this.cboShortCalculationBasedOn.IntegralHeight = false;
            this.cboShortCalculationBasedOn.Location = new System.Drawing.Point(138, 63);
            this.cboShortCalculationBasedOn.MaxDropDownItems = 10;
            this.cboShortCalculationBasedOn.Name = "cboShortCalculationBasedOn";
            this.cboShortCalculationBasedOn.Size = new System.Drawing.Size(231, 21);
            this.cboShortCalculationBasedOn.TabIndex = 2;
            this.cboShortCalculationBasedOn.SelectedIndexChanged += new System.EventHandler(this.cboShortCalculationBasedOn_SelectedIndexChanged);
            this.cboShortCalculationBasedOn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboShortCalculationBasedOn_KeyDown);
            // 
            // txtShortRatePerHour
            // 
            this.txtShortRatePerHour.Location = new System.Drawing.Point(138, 303);
            this.txtShortRatePerHour.MaxLength = 9;
            this.txtShortRatePerHour.Name = "txtShortRatePerHour";
            this.txtShortRatePerHour.ShortcutsEnabled = false;
            this.txtShortRatePerHour.Size = new System.Drawing.Size(231, 20);
            this.txtShortRatePerHour.TabIndex = 6;
            this.txtShortRatePerHour.TextChanged += new System.EventHandler(this.txtShortRatePerHour_TextChanged);
            this.txtShortRatePerHour.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // chkShortRateOnly
            // 
            this.chkShortRateOnly.AutoSize = true;
            this.chkShortRateOnly.Location = new System.Drawing.Point(138, 280);
            this.chkShortRateOnly.Name = "chkShortRateOnly";
            this.chkShortRateOnly.Size = new System.Drawing.Size(71, 17);
            this.chkShortRateOnly.TabIndex = 5;
            this.chkShortRateOnly.Text = "Rate only";
            this.chkShortRateOnly.UseVisualStyleBackColor = true;
            this.chkShortRateOnly.CheckedChanged += new System.EventHandler(this.chkShortRateOnly_CheckedChanged);
            // 
            // rdbShortActualMonth
            // 
            this.rdbShortActualMonth.AutoSize = true;
            this.rdbShortActualMonth.Location = new System.Drawing.Point(217, 34);
            this.rdbShortActualMonth.Name = "rdbShortActualMonth";
            this.rdbShortActualMonth.Size = new System.Drawing.Size(88, 17);
            this.rdbShortActualMonth.TabIndex = 1;
            this.rdbShortActualMonth.Text = "Actual Month";
            this.rdbShortActualMonth.UseVisualStyleBackColor = true;
            this.rdbShortActualMonth.CheckedChanged += new System.EventHandler(this.Changestatus);
            // 
            // rdbShortBasedOnCompany
            // 
            this.rdbShortBasedOnCompany.AutoSize = true;
            this.rdbShortBasedOnCompany.Checked = true;
            this.rdbShortBasedOnCompany.Location = new System.Drawing.Point(90, 34);
            this.rdbShortBasedOnCompany.Name = "rdbShortBasedOnCompany";
            this.rdbShortBasedOnCompany.Size = new System.Drawing.Size(117, 17);
            this.rdbShortBasedOnCompany.TabIndex = 0;
            this.rdbShortBasedOnCompany.TabStop = true;
            this.rdbShortBasedOnCompany.Text = "Based on Company";
            this.rdbShortBasedOnCompany.UseVisualStyleBackColor = true;
            this.rdbShortBasedOnCompany.CheckedChanged += new System.EventHandler(this.rdbShortBasedOnCompany_CheckedChanged);
            // 
            // lblHoursPerHour
            // 
            this.lblHoursPerHour.AutoSize = true;
            this.lblHoursPerHour.Location = new System.Drawing.Point(14, 260);
            this.lblHoursPerHour.Name = "lblHoursPerHour";
            this.lblHoursPerHour.Size = new System.Drawing.Size(76, 13);
            this.lblHoursPerHour.TabIndex = 46;
            this.lblHoursPerHour.Text = "Hours Per Day";
            // 
            // txtShortHoursPerDay
            // 
            this.txtShortHoursPerDay.BackColor = System.Drawing.SystemColors.Info;
            this.txtShortHoursPerDay.Location = new System.Drawing.Point(138, 253);
            this.txtShortHoursPerDay.MaxLength = 5;
            this.txtShortHoursPerDay.Name = "txtShortHoursPerDay";
            this.txtShortHoursPerDay.ShortcutsEnabled = false;
            this.txtShortHoursPerDay.Size = new System.Drawing.Size(231, 20);
            this.txtShortHoursPerDay.TabIndex = 4;
            this.txtShortHoursPerDay.TextChanged += new System.EventHandler(this.txtShortHoursPerDay_TextChanged);
            this.txtShortHoursPerDay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtint_KeyPress);
            // 
            // lblRatePerHourS
            // 
            this.lblRatePerHourS.AutoSize = true;
            this.lblRatePerHourS.Location = new System.Drawing.Point(14, 306);
            this.lblRatePerHourS.Name = "lblRatePerHourS";
            this.lblRatePerHourS.Size = new System.Drawing.Size(75, 13);
            this.lblRatePerHourS.TabIndex = 45;
            this.lblRatePerHourS.Text = "Rate Per Hour";
            // 
            // shapeContainer3
            // 
            this.shapeContainer3.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer3.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer3.Name = "shapeContainer3";
            this.shapeContainer3.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2});
            this.shapeContainer3.Size = new System.Drawing.Size(376, 326);
            this.shapeContainer3.TabIndex = 53;
            this.shapeContainer3.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 9;
            this.lineShape2.X2 = 367;
            this.lineShape2.Y1 = 24;
            this.lineShape2.Y2 = 24;
            // 
            // SelectValue
            // 
            this.SelectValue.HeaderText = "";
            this.SelectValue.Name = "SelectValue";
            this.SelectValue.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SelectValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SelectValue.Width = 30;
            // 
            // SelectValueS
            // 
            this.SelectValueS.HeaderText = "";
            this.SelectValueS.Name = "SelectValueS";
            this.SelectValueS.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SelectValueS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SelectValueS.Width = 30;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(320, 462);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(239, 464);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(5, 464);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtPolicyName
            // 
            this.txtPolicyName.BackColor = System.Drawing.SystemColors.Info;
            this.txtPolicyName.Location = new System.Drawing.Point(129, 46);
            this.txtPolicyName.MaxLength = 50;
            this.txtPolicyName.Name = "txtPolicyName";
            this.txtPolicyName.Size = new System.Drawing.Size(229, 20);
            this.txtPolicyName.TabIndex = 0;
            this.txtPolicyName.TextChanged += new System.EventHandler(this.txtPolicyName_TextChanged);
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 500);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(404, 22);
            this.ssStatus.TabIndex = 310;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // LineShape1
            // 
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 31;
            this.LineShape1.X2 = 388;
            this.LineShape1.Y1 = 37;
            this.LineShape1.Y2 = 37;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(404, 522);
            this.shapeContainer1.TabIndex = 311;
            this.shapeContainer1.TabStop = false;
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblHeader.Location = new System.Drawing.Point(2, 28);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(66, 13);
            this.lblHeader.TabIndex = 312;
            this.lblHeader.Text = "Policy Info";
            // 
            // errAbsentPolicy
            // 
            this.errAbsentPolicy.ContainerControl = this;
            this.errAbsentPolicy.RightToLeft = true;
            // 
            // tmrClear
            // 
            this.tmrClear.Tick += new System.EventHandler(this.tmrClear_Tick);
            // 
            // AddDedID
            // 
            this.AddDedID.HeaderText = "AddDedID";
            this.AddDedID.Name = "AddDedID";
            this.AddDedID.ReadOnly = true;
            this.AddDedID.Visible = false;
            // 
            // Particulars
            // 
            this.Particulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Particulars.HeaderText = "Particulars";
            this.Particulars.Name = "Particulars";
            this.Particulars.ReadOnly = true;
            // 
            // AddDedIDS
            // 
            this.AddDedIDS.HeaderText = "AddDedID";
            this.AddDedIDS.Name = "AddDedIDS";
            this.AddDedIDS.ReadOnly = true;
            this.AddDedIDS.Visible = false;
            // 
            // ParticularsS
            // 
            this.ParticularsS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ParticularsS.HeaderText = "Particulars";
            this.ParticularsS.Name = "ParticularsS";
            this.ParticularsS.ReadOnly = true;
            // 
            // FrmAbsentPolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 522);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.chkAbsentPolicyOnly);
            this.Controls.Add(this.chkMergeBoth);
            this.Controls.Add(this.tbPolicy);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtPolicyName);
            this.Controls.Add(DescriptionLabel);
            this.Controls.Add(this.AbsentPolicyBindingNavigator);
            this.Controls.Add(this.shapeContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAbsentPolicy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AbsentPolicy";
            this.Load += new System.EventHandler(this.FrmAbsentPolicy_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAbsentPolicy_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.AbsentPolicyBindingNavigator)).EndInit();
            this.AbsentPolicyBindingNavigator.ResumeLayout(false);
            this.AbsentPolicyBindingNavigator.PerformLayout();
            this.tbPolicy.ResumeLayout(false);
            this.tabpgAbsent.ResumeLayout(false);
            this.tabpgAbsent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAbsentDetails)).EndInit();
            this.tabpgShortage.ResumeLayout(false);
            this.tabpgShortage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShortageDetails)).EndInit();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errAbsentPolicy)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator AbsentPolicyBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.CheckBox chkAbsentPolicyOnly;
        internal System.Windows.Forms.CheckBox chkMergeBoth;
        internal System.Windows.Forms.TabControl tbPolicy;
        internal System.Windows.Forms.TabPage tabpgAbsent;
        internal System.Windows.Forms.ComboBox cboCalculationBased;
        internal System.Windows.Forms.TextBox txtRate;
        internal System.Windows.Forms.CheckBox chkRateOnly;
        internal System.Windows.Forms.RadioButton rdbActualMonth;
        internal System.Windows.Forms.RadioButton rdbCompanyBased;
        internal System.Windows.Forms.Label lblHoursPerDay;
        internal System.Windows.Forms.TextBox txtHoursPerDay;
        internal System.Windows.Forms.Label lblRate;
        internal System.Windows.Forms.CheckBox chkRatePerDay;
        internal System.Windows.Forms.TabPage tabpgShortage;
        internal System.Windows.Forms.ComboBox cboShortCalculationBasedOn;
        internal System.Windows.Forms.TextBox txtShortRatePerHour;
        internal System.Windows.Forms.CheckBox chkShortRateOnly;
        internal System.Windows.Forms.RadioButton rdbShortActualMonth;
        internal System.Windows.Forms.RadioButton rdbShortBasedOnCompany;
        internal System.Windows.Forms.Label lblHoursPerHour;
        internal System.Windows.Forms.TextBox txtShortHoursPerDay;
        internal System.Windows.Forms.Label lblRatePerHourS;
        internal System.Windows.Forms.DataGridViewTextBoxColumn AddDedID;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn SelectValue;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Particulars;
        internal System.Windows.Forms.DataGridViewTextBoxColumn AddDedIDS;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn SelectValueS;
        internal System.Windows.Forms.DataGridViewTextBoxColumn ParticularsS;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.TextBox txtPolicyName;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        internal System.Windows.Forms.Label lblHeader;
        private DemoClsDataGridview.ClsDataGirdView dgvAbsentDetails;
        private DemoClsDataGridview.ClsDataGirdView dgvShortageDetails;
        private System.Windows.Forms.ErrorProvider errAbsentPolicy;
        private System.Windows.Forms.Timer tmrClear;
        private System.Windows.Forms.DataGridViewTextBoxColumn colShortAddDedID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colShortSelectedItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn colShortParticulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAbsentAddDedID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colAbsentSelectedItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAbsentParticulars;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
    }
}