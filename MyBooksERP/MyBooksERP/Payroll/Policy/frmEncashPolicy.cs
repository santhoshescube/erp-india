﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    /*****************************************************
   * Created By       : Arun
   * Creation Date    : 10 Apr 2012
   * Description      : Handle Encash Policy
   * ***************************************************/
    public partial class frmEncashPolicy : Form
    {

        #region Declartions
        public int PintEncashPolicyID = 0;   // From Refernce Form 
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission

        private string MstrCommonMessage;
        private bool MblnIsEditMode = false;
        private int MintRecordCnt = 0;
        private int MintCurrentRecCnt = 0;
        private bool MblnChangeStatus = false;
        private bool MblbtnOk = false;

        private clsBLLEncashPolicy MobjclsBLLEncashPolicy = null;
        private clsMessage ObjUserMessage = null;
        string strBindingOf = "Of ";
        #endregion Declartions
        /// <summary>
        /// FORM ID=112
        /// Constructor
        /// </summary>
        #region Constructor
        public frmEncashPolicy()
        {
            InitializeComponent();

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.EncashPolicy, this);

        //    strBindingOf = "من ";
        //    dgvParticular.Columns["colEncashParticulars"].HeaderText = "تفاصيل";
        //}
        #endregion Constructor
        #region Properties
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.EncashPolicy);
                return this.ObjUserMessage;
            }
        }
        private clsBLLEncashPolicy BLLEncashPolicy
        {
            get
            {
                if (this.MobjclsBLLEncashPolicy == null)
                    this.MobjclsBLLEncashPolicy = new clsBLLEncashPolicy();

                return this.MobjclsBLLEncashPolicy;
            }
        }
        #endregion Properties

        #region Methods

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.EncashPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }

        private void ClearAllControls()
        {
            this.txtDescription.Text = "";
            this.txtDescription.Tag = 0;
            this.rdbCompanyBased.Checked = true;
            this.rdbActualMonth.Checked = false;
            this.cboCalculationBased.SelectedIndex = -1;
            this.cboCalculationBased.Text = "";
            this.dgvParticular.Rows.Clear();
            this.chkRateOnly.Checked = false;
            this.chkRateOnly.Checked = false;
            this.txtRate.Text = "";
            this.txtRate.Text = "";
            //this.chkHourBasedShift.Checked = false;
            this.txtCalculationPercent.Text = "";
        }

        private void Changestatus()
        {
            //function for changing status


            if (!MblnIsEditMode)
            {
                btnOk.Enabled = MblnAddPermission;
                btnSave.Enabled = MblnAddPermission;
                bnSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                btnOk.Enabled = MblnUpdatePermission;
                btnSave.Enabled = MblnUpdatePermission;
                bnSaveItem.Enabled = MblnUpdatePermission;
            }
            errEncashPolicy.Clear();
            MblnChangeStatus = true;

        }
        private bool LoadCombos(int intType)
        {
            bool blnRetvalue = false;
            DataTable datCombos = new DataTable();
            try
            {

                if (intType == 0 || intType == 1)//CAlculation type
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    datCombos = BLLEncashPolicy.FillCombos(new string[] { "CalculationID,CalculationArb AS Calculation", "PayCalculationReference", "CalculationID in(1,2)" });
                    //else
                        datCombos = BLLEncashPolicy.FillCombos(new string[] { "CalculationID,Calculation", "PayCalculationReference", "CalculationID in(1,2)" });
                    cboCalculationBased.ValueMember = "CalculationID";
                    cboCalculationBased.DisplayMember = "Calculation";
                    cboCalculationBased.DataSource = datCombos;
                    blnRetvalue = true;
                }
            }
            catch (Exception Ex)
            {
                //ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
                blnRetvalue = false;
            }
            return blnRetvalue;
        }
        private void FillAdditionDeductionsGrid(DataGridView Dgv, DataTable dtAddDedDetails)
        {
            Dgv.Rows.Clear();
            if (dtAddDedDetails != null)
            {
                if (dtAddDedDetails.Rows.Count > 0)
                {
                    for (int iCounter = 0; iCounter <= dtAddDedDetails.Rows.Count - 1; iCounter++)
                    {
                        Dgv.RowCount = Dgv.RowCount + 1;
                        Dgv.Rows[iCounter].Cells[0].Value = dtAddDedDetails.Rows[iCounter]["AddDedID"].ToInt32();//Addition deduction Policy
                        Dgv.Rows[iCounter].Cells[1].Value = dtAddDedDetails.Rows[iCounter]["Checked"].ToBoolean(); ;//ChkBox
                        Dgv.Rows[iCounter].Cells[2].Value = dtAddDedDetails.Rows[iCounter]["Description"].ToStringCustom();//Description
                    }
                }
            }


        }

        private void GetRecordCount()
        {
            MintRecordCnt = 0;
            MintRecordCnt = BLLEncashPolicy.GetRecordCount();
            if (MintRecordCnt < 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                MintRecordCnt = 0;
            }

        }
        private void RefernceDisplay()
        {

            int intRowNum = 0;
            intRowNum = BLLEncashPolicy.GetRowNumber(PintEncashPolicyID);
            if (intRowNum > 0)
            {
                MintCurrentRecCnt = intRowNum;
                DisplayEncashPolicyInfo();
            }

        }
        private void AddNewEncashPolicy()
        {
            MblnChangeStatus = false;
            MblnIsEditMode = false;
            lblstatus.Text = "";
            bnCancel.Enabled = true;
            tmrClear.Enabled = true;
            errEncashPolicy.Clear();
            
            ClearAllControls();  //Clear all controls in the form
            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();

            bnAddNewItem.Enabled = false;
            bnDeleteItem.Enabled = false;
            bnPrint.Enabled = false;
            btnEmail.Enabled = false;
            btnOk.Enabled = false;
            btnSave.Enabled = false;
            bnSaveItem.Enabled = false;
            bnMoveFirstItem.Enabled = true;
            bnMoveLastItem.Enabled = true;
            bnMovePreviousItem.Enabled = true;
            bnMoveNextItem.Enabled = true;

            txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtRate.Enabled = chkRateOnly.Checked ? true : false;

            txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtRate.Enabled = chkRateOnly.Checked ? true : false;
            txtDescription.Select();
        }
        private void SetBindingNavigatorButtons()
        {
            bnMoveNextItem.Enabled = bnMoveFirstItem.Enabled = bnMoveLastItem.Enabled = true;
            bnMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                bnMoveNextItem.Enabled = bnMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                bnMoveFirstItem.Enabled = bnMovePreviousItem.Enabled = false;
            }
        }
        private void DisplayEncashPolicyInfo()
        {

            FillEncashPolicyInfo();
            bnCancel.Enabled = false;
            bnAddNewItem.Enabled = MblnAddPermission;
            bnDeleteItem.Enabled = MblnDeletePermission;
            bnPrint.Enabled = MblnPrintEmailPermission;
            btnEmail.Enabled = MblnPrintEmailPermission;
            BindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            MblnIsEditMode = true;
            SetBindingNavigatorButtons();
            MblnChangeStatus = false;
            bnSaveItem.Enabled = MblnChangeStatus;
            btnOk.Enabled = MblnChangeStatus;
            btnSave.Enabled = MblnChangeStatus;

            txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtRate.Enabled = chkRateOnly.Checked ? true : false;

            txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtRate.Enabled = chkRateOnly.Checked ? true : false;

        }
        private void FillEncashPolicyInfo()
        {
            ClearAllControls();
            DataTable dtEncashPolicy = BLLEncashPolicy.DisplayEncashPolicy(MintCurrentRecCnt);
            
            if (dtEncashPolicy.Rows.Count > 0)
            {
                    txtDescription.Text = dtEncashPolicy.Rows[0]["EncashPolicy"].ToStringCustom();
                    txtDescription.Tag = dtEncashPolicy.Rows[0]["EncashPolicyID"].ToInt32();

                    cboCalculationBased.SelectedValue = dtEncashPolicy.Rows[0]["CalculationID"].ToInt32();

                    if (dtEncashPolicy.Rows[0]["IsCompanyBasedOn"].ToInt32() == 1)
                    {
                        rdbCompanyBased.Checked = true;
                    }
                    else 
                    {
                        rdbActualMonth.Checked = true;
                    }
                    //else
                    //{

                    //    rdbWorkingDays.Checked = true;
                    //}
                    txtCalculationPercent.Text = dtEncashPolicy.Rows[0]["CalculationPercentage"].ToString(); 
                    //chkHourBasedShift.Checked = dtEncashPolicy.Rows[0]["IsHourBasedShift"].ToBoolean();
                    chkRateOnly.Checked = dtEncashPolicy.Rows[0]["IsRateOnly"].ToBoolean();
                    txtRate.Text = dtEncashPolicy.Rows[0]["EncashRate"].ToDecimal().ToStringCustom();
                    chkRateOnly.Checked = dtEncashPolicy.Rows[0]["IsRateBasedOnHour"].ToBoolean();
                    txtRate.Text = dtEncashPolicy.Rows[0]["HoursPerDay"].ToStringCustom();
                    if (dtEncashPolicy.Rows[0]["CalculationID"].ToInt32() == (int)CalculationType.GrossSalary)
                    {
                        DataTable DtAddDedRef = BLLEncashPolicy.DisplayAddDedDetails(txtDescription.Tag.ToInt32(),(int)PolicyType.Encash);
                        FillAdditionDeductionsGrid(dgvParticular, DtAddDedRef);
                    }
                    //if (ClsCommonSettings.IsAmountRoundByZero)
                    //{
                    //    txtRate.Text = txtRate.Text.ToDecimal().ToString("F" + 0);
                    //}
            }
          
       


        }
        private bool FormValidation()
        {

            errEncashPolicy.Clear();
            Control control = null;
            bool blnReturnValue = true;

            if (txtCalculationPercent.Text.Trim() == "")
            {
                txtCalculationPercent.Text = "0";
            }
          
            if (txtDescription.Text.Trim().Length == 0)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10000);
                control = txtDescription;
                blnReturnValue = false;
            }
            else if (chkRateOnly.Checked == false && cboCalculationBased.SelectedValue.ToInt32() == 0)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10001);
                control = cboCalculationBased;
                blnReturnValue = false;
            }
            //else if (chkHourBasedShift.Checked == false && chkRatePerDay.Checked == true && (txtHoursPerDay.Text.Trim().Length == 0 ? 0.ToDecimal() : txtHoursPerDay.Text.ToDecimal()) == 0.ToDecimal())
            //{
            //    MstrCommonMessage = UserMessage.GetMessageByCode(10008);
            //    control = txtHoursPerDay;
            //    blnReturnValue = false;
            //}
            //else if (chkRateOnly.Checked == true && (txtRate.Text.Trim().Length == 0 ? 0.ToDecimal() : txtRate.Text.ToDecimal()) > 24.ToDecimal())
            //{
            //    MstrCommonMessage = UserMessage.GetMessageByCode(10008,2);
            //    control = txtRate;
            //    blnReturnValue = false;
            //}
            else if (chkRateOnly.Checked && (txtRate.Text.Trim().Length == 0 || txtRate.Text.ToDecimal()==0))
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10005);
                control = txtRate;
                blnReturnValue = false;
            }
            else if (txtCalculationPercent.Text.Trim() == "" && chkRateOnly.Checked == false )
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10003);
                control = txtCalculationPercent;
                blnReturnValue = false;
            }
            else if (Convert.ToDecimal(txtCalculationPercent.Text.Trim()) <= 0 && chkRateOnly.Checked == false)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10003);
                control = txtCalculationPercent;
                blnReturnValue = false;
            }
            else if (txtRate.Text.Trim().Length > 0)
            {
                try
                {
                    decimal rate = Convert.ToDecimal(txtRate.Text);
                }
                catch
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(10002);
                    control = txtRate;
                    blnReturnValue = false;
                }
            }


            if (blnReturnValue == false)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrClear.Enabled = true;
                if (control != null)
                {
                    errEncashPolicy.SetError(control, MstrCommonMessage);
                    control.Focus();
                }
                return blnReturnValue;
            }
            if (blnReturnValue)
            {
                if (BLLEncashPolicy.CheckDuplicate(txtDescription.Tag.ToInt32(), txtDescription.Text.Trim()))//CheckDuplicate()
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(10009);
                    control = txtDescription;
                    blnReturnValue = false;
                }
            }
            if (blnReturnValue == false)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrClear.Enabled = true;
                if (control != null)
                {
                    errEncashPolicy.SetError(control, MstrCommonMessage);
                    control.Focus();
                }
            }

            return blnReturnValue;
        }
        private bool DeleteValidation()
        {
            if (txtDescription.Tag.ToInt32() == 0)
            {
                UserMessage.ShowMessage(10004);
                return false;

            }
            if (BLLEncashPolicy.PolicyIDExists(txtDescription.Tag.ToInt32()))
            {
                UserMessage.ShowMessage(10006);
                return false;
            }
            if (UserMessage.ShowMessage(13) == false)
            {
                return false;
            }
            return true;

        }
        private void FillParameterMaster()
        {
            BLLEncashPolicy.DTOEncashPolicy.EncashPolicyID = txtDescription.Tag.ToInt32();
            BLLEncashPolicy.DTOEncashPolicy.EncashPolicy = txtDescription.Text.Trim();

            BLLEncashPolicy.DTOEncashPolicy.CalculationID = cboCalculationBased.SelectedValue.ToInt32();
            if (rdbCompanyBased.Checked == true)
            {
                BLLEncashPolicy.DTOEncashPolicy.CompanyBasedOn = 1;
            }
            else if (rdbActualMonth.Checked == true)
            {
                BLLEncashPolicy.DTOEncashPolicy.CompanyBasedOn = 2;
            }
            else
            {
                BLLEncashPolicy.DTOEncashPolicy.CompanyBasedOn = 3;
            }

            if (txtCalculationPercent.Text.Trim() != "")
            {
                BLLEncashPolicy.DTOEncashPolicy.CalculationPercentage = txtCalculationPercent.Text.ToDecimal();
            }
           
            BLLEncashPolicy.DTOEncashPolicy.IsRateOnly = chkRateOnly.Checked;
            BLLEncashPolicy.DTOEncashPolicy.EncashRate = chkRateOnly.Checked ? txtRate.Text.Trim().ToDecimal() : 0;

            BLLEncashPolicy.DTOEncashPolicy.IsRateBasedOnHour = chkRateOnly.Checked;
            BLLEncashPolicy.DTOEncashPolicy.HoursPerDay = chkRateOnly.Checked ? txtRate.Text.Trim().ToDecimal() : 0;
            //BLLEncashPolicy.DTOEncashPolicy.IsHourBasedShift = chkHourBasedShift.Checked;

        }
      
        private void FillParameterSalPolicyDetails()
        {
            BLLEncashPolicy.DTOEncashPolicy.DTOSalaryPolicyDetailEncash = new List<clsDTOSalaryPolicyDetailEncash>();

            if (dgvParticular.Rows.Count > 0)
            {
                dgvParticular.CommitEdit(DataGridViewDataErrorContexts.Commit);
                dgvParticular.CurrentCell = dgvParticular["colEncashParticulars", 0];
                foreach (DataGridViewRow row in dgvParticular.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["colEncashSelectedItem"].Value) == true)
                    {
                        clsDTOSalaryPolicyDetailEncash objSalaryPolicyDetail = new clsDTOSalaryPolicyDetailEncash();
                        objSalaryPolicyDetail.PolicyType = (int)PolicyType.Encash;
                        objSalaryPolicyDetail.AdditionDeductionID = row.Cells["colEncashAddDedID"].Value.ToInt32();
                        BLLEncashPolicy.DTOEncashPolicy.DTOSalaryPolicyDetailEncash.Add(objSalaryPolicyDetail);
                    }
                }
            }
          

        }
        private bool SaveEncashPolicy()
        {
            bool blnRetValue = false;

            if (FormValidation())
            {
                int intMessageCode = 0;
                if (txtDescription.Tag.ToInt32() > 0)
                    intMessageCode = 3;
                else
                    intMessageCode = 1;

                if (UserMessage.ShowMessage(intMessageCode))
                {
                    FillParameterMaster();
                    FillParameterSalPolicyDetails();
                    if (BLLEncashPolicy.EncashPolicyMasterSave())
                    {
                        if (!MblnIsEditMode)
                        {
                            lblstatus.Text = UserMessage.GetMessageByCode(2);
                            UserMessage.ShowMessage(2, null, null);
                        }
                        else
                        {
                            lblstatus.Text = UserMessage.GetMessageByCode(21);
                            UserMessage.ShowMessage(21, null, null);
                        }
                        blnRetValue = true;
                        txtDescription.Tag = BLLEncashPolicy.DTOEncashPolicy.EncashPolicyID;
                        PintEncashPolicyID = BLLEncashPolicy.DTOEncashPolicy.EncashPolicyID;
                        if (!MblnIsEditMode)
                        {
                            MintCurrentRecCnt = MintCurrentRecCnt + 1;
                            BindingNavigatorMoveLastItem_Click(null, null);
                        }
                     
                        //bnAddNewItem.Enabled = MblnAddPermission;
                        //btnEmail.Enabled = MblnPrintEmailPermission;
                        //bnPrint.Enabled = MblnPrintEmailPermission;
                        //bnDeleteItem.Enabled = MblnDeletePermission;
                        //MblnIsEditMode = true;
                    }

                }
            }

            return blnRetValue;
        }

        private bool DeleteEncashPolicy()
        {
            bool blnRetValue = false;
            if (DeleteValidation())
            {
                BLLEncashPolicy.DTOEncashPolicy.EncashPolicyID = txtDescription.Tag.ToInt32();
                if (BLLEncashPolicy.DeleteEncashPolicy())
                {
                    AddNewEncashPolicy();
                    blnRetValue = true;
                }

            }
            return blnRetValue;

        }
        private void ResetForm(object sender, System.EventArgs e)
        {
            if (sender is CheckBox)
            {
                CheckBox chk = (CheckBox)sender;
                if (chk.Name == chkRateOnly.Name)
                {
                    txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
                    txtRate.Enabled = chkRateOnly.Checked ? true : false;

                    cboCalculationBased.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Window : System.Drawing.SystemColors.Info;
                    if (chkRateOnly.Checked)
                    {
                        cboCalculationBased.SelectedIndex = -1;
                        txtCalculationPercent.Enabled = false;
                        txtCalculationPercent.Text = "0";
                    }
                    else
                    {
                        txtCalculationPercent.Enabled = true;
                        txtCalculationPercent.Text = "0";
                        cboCalculationBased.SelectedIndex = 0;
                        txtRate.Text = "";
                    }
                    cboCalculationBased.Enabled = chkRateOnly.Checked ? false : true;
                    rdbCompanyBased.Enabled = chkRateOnly.Checked ? false : true;
                    rdbActualMonth.Enabled = chkRateOnly.Checked ? false : true;
                    txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
                    txtCalculationPercent.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Window : System.Drawing.SystemColors.Info;
                    //if (chkRateOnly.Checked)
                    //    chkRateOnly.Checked = false;
                    txtRate.Enabled = !txtRate.Enabled;
                }
                else if (chk.Name == chkRateOnly.Name)
                {
                    if (chkRateOnly.Checked)
                    {
                        txtCalculationPercent.Enabled = false;
                        txtCalculationPercent.Text = "0";
                        //chkRateOnly.Checked = false;
                    }
                    else
                    {
                        txtRate.Text = "";
                        txtCalculationPercent.Enabled = true;
                        txtCalculationPercent.Text = "0";
                    }

                }

                txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
                txtRate.Enabled = chkRateOnly.Checked ? true : false;
            }
            Changestatus();



        }
        #endregion Methods
        #region Events

        private void FrmEncashPolicy_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadCombos(0);
            tmrClear.Interval = ClsCommonSettings.TimerInterval;    
            if (PintEncashPolicyID > 0)
            {
                GetRecordCount();
                RefernceDisplay();
            }
            else
            {
                AddNewEncashPolicy();
            }
            txtDescription.Select();

        }

        private void FrmEncashPolicy_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnSave.Enabled)
            {
                if (UserMessage.ShowMessage(8,null,null))
                {
                    e.Cancel = false;
                }
                else
                { e.Cancel = true; }
            }
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;
                DisplayEncashPolicyInfo();
                lblstatus.Text = UserMessage.GetMessageByCode(9);
                tmrClear.Enabled = true;
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    DisplayEncashPolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(10);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 1;
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                }

                if (MintCurrentRecCnt > MintRecordCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                }
                else
                {
                    DisplayEncashPolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(11);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 0;
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    DisplayEncashPolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(12);
                    tmrClear.Enabled = true;
                }
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewEncashPolicy();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (DeleteEncashPolicy())
            {
                lblstatus.Text = UserMessage.GetMessageByCode(4);
                tmrClear.Enabled = true;
                UserMessage.ShowMessage(4,null,null);
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveEncashPolicy())
            {
                
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            AddNewEncashPolicy();
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveEncashPolicy())
            {
                //lblstatus.Text = UserMessage.GetMessageByCode(2,2);
                //UserMessage.ShowMessage(2,null,null,2);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveEncashPolicy())
            {
                btnSave.Enabled = false;
                MblbtnOk = true;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void rdbCompanyBased_CheckedChanged(object sender, EventArgs e)
        {
            ResetForm(sender, e);
        }

        private void chkRatePerDay_CheckedChanged(object sender, EventArgs e)
        {
            ResetForm(sender, e);
            //if (chkRateOnly.Checked == true)
            //{
            //    chkHourBasedShift.Checked = true;
            //}
        }

        private void chkRateOnly_CheckedChanged(object sender, EventArgs e)
        {
            ResetForm(sender, e);
        }

        private void rdbShortBasedOnCompany_CheckedChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

       
        private void tmrClear_Tick(object sender, EventArgs e)
        {
            tmrClear.Enabled = false;
            lblstatus.Text = "";
        }

        private void txtDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars;

            strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            //if (ClsCommonSettings.IsAmountRoundByZero)
            //{
            //    strInvalidChars = strInvalidChars + ".";
            //}

            e.Handled = false;
            if (((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))) )
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 )
            {
                e.Handled = true;
            }
        }

        private void txtint_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[].";
            e.Handled = false;
            if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }

      

      
      
        private void txtPolicyName_TextChanged(object sender, EventArgs e)
        {
            ResetForm(sender, e);
            Changestatus();
        }

        private void dgvEncashDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch
            {
            }
        }

        private void dgvEncashDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvParticular.IsCurrentCellDirty)
                {

                    dgvParticular.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }

      
        private void dgvShortageDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Changestatus();
        }

        private void dgvEncashDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Changestatus();
        }

        private void dgvShortageDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void cboCalculationBased_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Encash Tab
            Changestatus();
            if (cboCalculationBased.SelectedValue.ToInt32() > 0)
            {
                dgvParticular.Enabled = true;

                if (cboCalculationBased.SelectedValue.ToInt32() == (int)CalculationType.GrossSalary)
                {
                    DataTable dtADDDedd = BLLEncashPolicy.GetAdditionDeductions();
                    FillAdditionDeductionsGrid(dgvParticular, dtADDDedd);
                }
                else
                {
                    dgvParticular.Rows.Clear();
                    dgvParticular.Enabled = false;
                }
            }
            else
            {
                dgvParticular.Rows.Clear();
                dgvParticular.Enabled = false;
            }
        }

       

        private void cboCalculationBased_KeyDown(object sender, KeyEventArgs e)
        {
            cboCalculationBased.DroppedDown = false;
            if (e.KeyData == Keys.Enter)
                cboCalculationBased_SelectedIndexChanged(sender, new EventArgs());

        }

       
       
        #endregion Events

        private void chkHourBasedShift_CheckedChanged(object sender, EventArgs e)
        {
            //if (chkHourBasedShift.Checked == true)
            //{
            //    txtHoursPerDay.Text = "0";
            //    chkRatePerDay.Checked = false;
            //    txtHoursPerDay.Enabled = false;
            //}
  
            Changestatus();
        }

        private void txtRate_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtCalculationPercent_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void dgvParticular_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            Changestatus();
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "SalaryStructure";
            objHelp.ShowDialog();
            objHelp = null;
        }

 



    }
}
