﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using BLL;
using DTO;
using Microsoft.VisualBasic;
/* 
=================================================
   Author:		<Author,,LAxmi>
   Create date: <Create Date,,23 March 2011>
   Description:	<Description,,Shift policy Form>
-------------------------------------------------
   Modification By  : Ranju Mathew
   Creation Date    : 14 Aug 2013
   Description      : Tuning and performance improving-Shift Policy 
================================================
*/
namespace MyBooksERP
{
    public partial class FrmShiftPolicy : Form//Form
    {

        #region Declartions

        private bool MblnAddStatus;//Add/Update mode 

        // permissions

        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnAddUpdatePermission = false; // To set add/update permission
        private bool MblnPrintEmailPermission = false;//To Set PrintEmail Permission



        private string MstrMessageCommon; //Messagebox display
        private string MstrMessageCaption;              //Message caption

        private ArrayList MsarMessageArr; // Error Message display
        private ArrayList MaStatusMessage;// Status bar error message display

        private TabPage TempTab;//To store Dynamic Shift Tab
        private int MiShiftID = 0;// Display of details when clicking grid
        public int PiCompanyID = 0;// Reference From Work policy
        public int PiShiftID = 0; // Reference from Shift policy
        private int TotalRecordCnt, CurrentRecCnt;
        private int MintTimerInterval;                  // To set timer interval
        private int MintCompanyId;                      // current companyid

        private MessageBoxIcon MmessageIcon;            // to set the message icon

        private bool Glb24HourFormat ;

        ClsLogWriter mObjLogs;
        ClsNotification mObjNotification;
        clsBLLShiftPolicy MobjclsBLLShiftPolicy;
        #endregion Declartions

        #region Constructor
        public FrmShiftPolicy()
        {
            //Constructor
            InitializeComponent();
            MintCompanyId = ClsCommonSettings.LoginCompanyID;
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            Glb24HourFormat = ClsCommonSettings.Glb24HourFormat;
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            MobjclsBLLShiftPolicy = new clsBLLShiftPolicy();

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    //ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.ShiftPolicy, this);

        //    DgvShift.Columns["Column8"].HeaderText = "اسم التحول";
        //    DgvShift.Columns["Column9"].HeaderText = "من وقت";
        //    DgvShift.Columns["Column10"].HeaderText = "إلى وقت";
        //    DgvShift.Columns["Column11"].HeaderText = "مدة";
        //    DgvShift.Columns["Column12"].HeaderText = "نوع التحول";

        //    dgvDynamicShift.Columns["OrderNo"].HeaderText = "الأمر رقم";
        //    dgvDynamicShift.Columns["FromTime"].HeaderText = "من وقت";
        //    dgvDynamicShift.Columns["ToTime"].HeaderText = "إلى وقت";
        //    dgvDynamicShift.Columns["Duration"].HeaderText = "مدة";
        //    dgvDynamicShift.Columns["AllowedBreakTime"].HeaderText = "كسر التوقيت";
        //    dgvDynamicShift.Columns["MinWorkingHrs"].HeaderText = "ساعات العمل الحد الأدنى";
        //    dgvDynamicShift.Columns["BufferTime"].HeaderText = "العازلة التوقيت";
        //}
        #endregion Constructor

        #region Methods

        #region SetPermissions
        /// <summary>
        /// Set Permission For Add /Update /Delete /Email
        /// </summary>
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3 )
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.ShiftPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }
        #endregion SetPermissions

        #region SetDatetimePicker
        /// <summary>
        /// To Set Date Formate for FromTimeDateTimePicker /ToTimeDateTimePicker
        /// </summary>
        private void SetDatetimePicker()
        {
            try
            {
                if (Glb24HourFormat == true)
                {
                    FromTimeDateTimePicker.CustomFormat = "HH:mm";
                    ToTimeDateTimePicker.CustomFormat = "HH:mm";
                }
                else
                {
                    FromTimeDateTimePicker.CustomFormat = "hh:mm tt";
                    ToTimeDateTimePicker.CustomFormat = "hh:mm tt";
                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on SetDatetimePicker " + this.Name + " " + Ex.Message.ToString(), 1);
            }
        }
        #endregion SetDatetimePicker

        #region LoadCombos
        /// <summary>
        /// To Load Details In DropDown Box
        /// </summary>
        private void LoadCombos()
        {
            try
            {
                DataTable datCombos = new DataTable();
                //if (ClsCommonSettings.IsArabicView)
                //    datCombos = MobjclsBLLShiftPolicy.FillCombos(new string[] { "ShiftTypeID,ShiftTypeArb AS ShiftType", "PayShiftTypeReference", "", "ShiftTypeID", "ShiftType" });
                //else
                    datCombos = MobjclsBLLShiftPolicy.FillCombos(new string[] { "ShiftTypeID,ShiftType", "PayShiftTypeReference", "", "ShiftTypeID", "ShiftType" });
                cboShiftType.ValueMember = "ShiftTypeID";
                cboShiftType.DisplayMember = "ShiftType";
                cboShiftType.DataSource = datCombos;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on LoadCombos " + this.Name + " " + Ex.Message.ToString(), 1);
            }
        }
        #endregion LoadCombos

        #region LoadInitial
        /// <summary>
        /// Function To Assign /Enable values in Initial Time
        /// </summary>
        /// <param name="iCompanyID"></param>
        private void LoadInitial(int iCompanyID)
        {

            BindingNavigatorAddNewItem.Enabled = true;
            ErrorProviderShift.Clear();
            MblnAddStatus = false;
        }
        #endregion LoadInitial

        #region LoadMessage
        /// <summary>
        /// To LoadMessage From Notification Master
        /// </summary>
        private void LoadMessage()
        {
            // Loading Message
            try
            {
                MsarMessageArr = new ArrayList();
                MsarMessageArr = mObjNotification.FillMessageArray((int)FormID.ShiftPolicy, 2);
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on LoadMessage " + this.Name + " " + Ex.Message.ToString(), 1);
            }

        }
        #endregion LoadMessage

        #region FillShiftDetails
        /// <summary>
        /// To fill shift Details to the Grid
        /// </summary>
        /// <param name="iCompanyID"></param>
        private void FillShiftDetails()
        {
            try
            {
                DgvShift.Rows.Clear();
                MobjclsBLLShiftPolicy = new clsBLLShiftPolicy();
                DataTable dtshift = MobjclsBLLShiftPolicy.GetAllShifts().Tables[0];
                if (dtshift.Rows.Count > 0)
                {
                    btnPrint.Enabled = MblnPrintEmailPermission ;
                    if (Glb24HourFormat == true)
                    {
                        for (int i = 0; i < dtshift.Rows.Count; i++)
                        {
                            DgvShift.RowCount = DgvShift.RowCount + 1;
                            DgvShift.Rows[i].Cells[0].Value = dtshift.Rows[i]["ShiftID"].ToInt32();
                            DgvShift.Rows[i].Cells[1].Value = dtshift.Rows[i]["ShiftName"];
                            DgvShift.Rows[i].Cells[2].Value = dtshift.Rows[i]["FromTime"];
                            DgvShift.Rows[i].Cells[3].Value = dtshift.Rows[i]["ToTime"];
                            DgvShift.Rows[i].Cells[4].Value = dtshift.Rows[i]["Duration"];
                            DgvShift.Rows[i].Cells[5].Value = dtshift.Rows[i]["ShiftTypeID"];
                        }

                    }
                    else
                    {
                        for (int i = 0; i < dtshift.Rows.Count; i++)
                        {
                            DgvShift.RowCount = DgvShift.RowCount + 1;
                            DgvShift.Rows[i].Cells[0].Value = dtshift.Rows[i]["ShiftID"].ToInt32();
                            DgvShift.Rows[i].Cells[1].Value = dtshift.Rows[i]["ShiftName"];
                            DgvShift.Rows[i].Cells[2].Value = ConvertTimeTo12Hr(Convert.ToString(dtshift.Rows[i]["FromTime"]));
                            DgvShift.Rows[i].Cells[3].Value = ConvertTimeTo12Hr(Convert.ToString(dtshift.Rows[i]["ToTime"]));
                            DgvShift.Rows[i].Cells[4].Value = dtshift.Rows[i]["Duration"];
                            DgvShift.Rows[i].Cells[5].Value = dtshift.Rows[i]["ShiftTypeID"];
                        }
                    }
                }
                else
                    btnPrint.Enabled = MblnPrintEmailPermission;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on FillShiftDetails " + this.Name + " " + Ex.Message.ToString(), 1);
            }

        }
        #endregion FillShiftDetails

        #region ConvertTimeTo12Hr
        /// <summary>
        /// Function To Convert DateTime 12 Hour Format
        /// </summary>
        /// <param name="Time"></param>
        /// <returns></returns>
        public string ConvertTimeTo12Hr(string Time)
        {
            string ConvertedTime = "";
            try
            {

                dtpTimeFormat.CustomFormat = "hh:mm tt";
                dtpTimeFormat.Text = Time;
                ConvertedTime = dtpTimeFormat.Text;
                return ConvertedTime;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on ConvertTimeTo12Hr " + this.Name + " " + Ex.Message.ToString(), 1);
                return ConvertedTime;
            }
        }
        #endregion ConvertTimeTo12Hr

        #region NewShiftPolicy
        /// <summary>
        /// To Add New Shift
        /// </summary>
        private void NewShiftPolicy()
        {
            ClearAllControls();
            GetTimeDifference();
            SetEnableDisable(ControlState.Disable);

            btnPrint.Enabled=BindingNavigatorAddNewItem.Enabled = false;
            CancelToolStripButton.Enabled=MblnAddStatus = true;
            //lblstatus.Text = "Add new information";
            DescriptionTextBox.Focus();
            DescriptionTextBox.Select();
           

        }
        #endregion NewShiftPolicy

        #region SetEnableDisable
        /// <summary>
        /// // function for setting enable and disable
        /// </summary>
        /// <param name="state"></param>
        private void SetEnableDisable(ControlState state)
        {
            
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;

            if (state == ControlState.Enable)
                ShiftReferenceBindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = MblnAddUpdatePermission;// CancelToolStripButton.Enabled =
            else
                BindingNavigatorDeleteItem.Enabled = ShiftReferenceBindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = false; // CancelToolStripButton.Enabled =
        }
        #endregion SetEnableDisable

        #region Saveshift
        /// <summary>
        /// To Save shift Policy
        /// </summary>
        /// <returns></returns>
        private bool Saveshift()
        {
            MobjclsBLLShiftPolicy.clsDTOShiftPolicy.ShiftID = DescriptionTextBox.Tag.ToInt32();
            if (MobjclsBLLShiftPolicy.IsExists(14))
            {


                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9001, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "Shift Policy");

                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrComapny.Enabled = true;
                return false;
            }

            if (Formvalidation() == false) return false;
            if (DynamicShiftValidation() == false) return false;
            if (SplitShiftValidation() == false) return false;
            try
            {
                int iShiftID = 0;
                if (MblnAddStatus == true)
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 1, out MmessageIcon);
                else
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;

                FillParameters();
                iShiftID = MobjclsBLLShiftPolicy.Insert(MblnAddStatus);
                if (iShiftID > 0)
                {
                    MblnAddStatus = true;
                    lblstatus.Text = "Saved successfully";
                    TmrComapny.Enabled = true;
                    // DescriptionTextBox.Tag = iShiftID ;
                    mObjLogs.WriteLog("Saved successfully:Saveshift()  " + this.Name + "", 0);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on Saveshift " + this.Name + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }
        #endregion Saveshift

        #region Formvalidation
        /// <summary>
        /// For Form validation
        /// </summary>
        /// <returns></returns>
        private bool Formvalidation()
        {
            ErrorProviderShift.Clear();

            if (MobjclsBLLShiftPolicy.CheckDuplication(MblnAddStatus, new string[] { DescriptionTextBox.Text.Replace("'", "").Trim().ToLower() }, DescriptionTextBox.Tag.ToInt32(), 1))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 206, out MmessageIcon);
                ErrorProviderShift.SetError(DescriptionTextBox, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrComapny.Enabled = true;
                DescriptionTextBox.Focus();
                return false;
            }

            if (DescriptionTextBox.Text.Trim() == "")
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 202, out MmessageIcon);
                ErrorProviderShift.SetError(DescriptionTextBox, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrComapny.Enabled = true;
                DescriptionTextBox.Focus();
                return false;
            }
            if (cboShiftType.SelectedIndex.ToInt32() == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 203, out MmessageIcon);
                ErrorProviderShift.SetError(cboShiftType, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrComapny.Enabled = true;
                cboShiftType.Focus();
                return false;
            }

            if (Convert.ToDouble(txtDuration.Text.Replace(":", ".")) < Convert.ToDouble(dtpMinWorkingHours.Text.Replace(":", ".")))
            {
                MstrMessageCommon = "Please check the duration";
                ErrorProviderShift.SetError(dtpMinWorkingHours, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrComapny.Enabled = true;
                dtpMinWorkingHours.Focus();
                return false;
            }

            if ((txtNoOfTimings.Text.Trim() == "" || txtNoOfTimings.Text == "0") && (cboShiftType.SelectedValue.ToInt32() == 3 || cboShiftType.SelectedValue.ToInt32() == 4))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 204, out MmessageIcon);
                ErrorProviderShift.SetError(txtNoOfTimings, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrComapny.Enabled = true;
                txtNoOfTimings.Focus();
                return false;
            }

            if (txtDuration.Text.Trim() == "" && cboShiftType.SelectedValue.ToInt32() == 3)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 205, out MmessageIcon);
                ErrorProviderShift.SetError(txtDuration, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrComapny.Enabled = true;
                txtNoOfTimings.Focus();
                return false;
            }
           

            if (FromTimeDateTimePicker.Value.ToString("HH:mm") == ToTimeDateTimePicker.Value.ToString("HH:mm"))
            {
                MstrMessageCommon = "FromTime and ToTime should not be same."; //mObjNotification.GetErrorMessage(MsarMessageArr, 207, out MmessageIcon);
                ErrorProviderShift.SetError(ToTimeDateTimePicker, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrComapny.Enabled = true;
                ToTimeDateTimePicker.Focus();
                return false;
            }
            if (cboShiftType.SelectedValue.ToInt32() == 2)
            {
                if (txtDuration.Text == "00:00")
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 257, out MmessageIcon);
                    ErrorProviderShift.SetError(txtDuration, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    txtDuration.Focus();
                    return false;
                }
            }
            if (dtpMinWorkingHours.Text == "00:00")
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 258, out MmessageIcon);
                ErrorProviderShift.SetError(dtpMinWorkingHours, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrComapny.Enabled = true;
                dtpMinWorkingHours.Focus();
                return false;
            }
            if (txtWeekWrkHrs.Text != "")
            {
                if (txtWeekWrkHrs.Text.ToInt32() > 0)
                {
                    if (Convert.ToInt32(txtWeekWrkHrs.Text) > 168)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 262, out MmessageIcon);
                        ErrorProviderShift.SetError(txtWeekWrkHrs, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        txtWeekWrkHrs.Focus();
                        return false;
                    }
                }
            }
            if (ValidateShiftTime() == false) return false;
            return true;
        }
        #endregion Formvalidation

        #region ValidateShiftTime
        /// <summary>
        /// fuction for ValidateShiftTime
        /// </summary>
        /// <returns></returns>
        private bool ValidateShiftTime()
        {
            if (cboShiftType.SelectedValue.ToInt32() == 1)
            {
               
                if (GetDurationInMinutes(txtDuration.Text) != txtAllowedBreakTime.Text.ToInt32() + GetDurationInMinutes(dtpMinWorkingHours.Value.TimeOfDay.ToString()))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 260, out MmessageIcon);
                    ErrorProviderShift.SetError(txtDuration, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    txtDuration.Focus();
                    return false;
                }
               
            }
            return true;
        }
        #endregion ValidateShiftTime

        #region DynamicShiftValidation
        /// <summary>
        /// Function for validate DynamicShift
        /// </summary>
        /// <returns></returns>
        private bool DynamicShiftValidation()
        {
            int intICounter ;
            for  (intICounter=0;intICounter<dgvDynamicShift.Rows.Count;intICounter++)
            {
                if (cboShiftType.SelectedValue.ToInt32() == 3 )
                {

                    if (dgvDynamicShift.Rows[intICounter].Cells["MinWorkingHrs"].Value == DBNull.Value || dgvDynamicShift.Rows[intICounter].Cells["MinWorkingHrs"].Value == null )
                    {
                        tabMain.SelectedTab = tabDynamicShiftDetails;
                        dgvDynamicShift.CurrentCell = dgvDynamicShift["MinWorkingHrs", intICounter];
                        MessageBox.Show("Minimum working hours should not be zero.", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                       
                        return false;
                    }


                    if (dgvDynamicShift.Rows[intICounter].Cells["MinWorkingHrs"].Value.ToString()== "00:00" || dgvDynamicShift.Rows[intICounter].Cells["MinWorkingHrs"].Value.ToString() == "")
                    {
                        tabMain.SelectedTab =tabDynamicShiftDetails ;
                         dgvDynamicShift.CurrentCell = dgvDynamicShift["MinWorkingHrs", intICounter];
                         MessageBox.Show("Minimum working hours should not be zero.", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                         return false;
                    }
                  
                }
            }
            return true ;
        }
        #endregion DynamicShiftValidation


        #region SplitShiftValidation
        /// <summary>
        /// validation for SplitShift
        /// </summary>
        /// <returns></returns>
        private bool SplitShiftValidation()
        {
            bool bIsValid = true;
            int tsMaxDuration = 0;
            int tsTotalDuration = 0;
            string sSplitEndTime = "";
            string sSplitStartTime = "";

            bool SHIFTCHANGE = false;
            bool ShiftChange1 = false;

            DateTime sShiftFromTime = Convert.ToDateTime((FromTimeDateTimePicker.Value.ToString("HH:mm tt")));
            DateTime sShiftToTime = Convert.ToDateTime((ToTimeDateTimePicker.Value.ToString("HH:mm tt")));

            if (sShiftFromTime >= sShiftToTime)
                sShiftToTime = sShiftToTime.AddDays(1);
            for (int i = 0; i < dgvDynamicShift.Rows.Count; i++)
            {

                DateTime sFromTime = Convert.ToDateTime(Convert.ToDateTime(dgvDynamicShift.Rows[i].Cells["FromTime"].Value).ToString("HH:mm tt"));
                DateTime sToTime = Convert.ToDateTime(Convert.ToDateTime(dgvDynamicShift.Rows[i].Cells["ToTime"].Value).ToString("HH:mm tt"));
                if (i == 0)
                    sSplitStartTime = Convert.ToString(Convert.ToDateTime(dgvDynamicShift.Rows[i].Cells["FromTime"].Value).ToString("HH:mm tt"));
                else if (i == dgvDynamicShift.Rows.Count - 1)
                    sSplitEndTime = Convert.ToString(Convert.ToDateTime(dgvDynamicShift.Rows[i].Cells["ToTime"].Value).ToString("HH:mm tt"));
                ShiftChange1 = false;



                if(cboShiftType.SelectedValue.ToInt32() == 4)
                {
                    if (SHIFTCHANGE == true )
                    {
                        sToTime = sToTime.AddDays(1);
                        sFromTime = sFromTime.AddDays(1);
                        ShiftChange1 = true;
                    }
                    if (sFromTime >= sToTime && SHIFTCHANGE == false)
                    {
                        SHIFTCHANGE = true;
                        ShiftChange1 = true;
                        sToTime = sToTime.AddDays(1);
                    }
                }

                else if (cboShiftType.SelectedValue.ToInt32() == 3)
                {
                    if (sShiftFromTime.Date < sShiftToTime.Date && sFromTime >= Convert.ToDateTime(Convert.ToDateTime("12:00 AM").ToString("hh:mm tt")) && sFromTime < ToTimeDateTimePicker.Value)
                    {
                        sFromTime = sFromTime.AddDays(1);
                        sToTime = sToTime.AddDays(1);
                    }
                    else if (sFromTime >= sToTime)
                    {
                        sToTime = sToTime.AddDays(1);
                    }
                }
                //if (SHIFTCHANGE == true)
                //{
                //    sToTime = sToTime.AddDays(1);
                //    sFromTime = sFromTime.AddDays(1);
                //    ShiftChange1 = true;
                //}
                //if (sFromTime >= sToTime && SHIFTCHANGE == false)
                //{
                //    SHIFTCHANGE = true;
                //    ShiftChange1 = true;
                //    sToTime = sToTime.AddDays(1);
                //}
                tsTotalDuration = tsTotalDuration + GetDurationInMinutes(Convert.ToString(dgvDynamicShift.Rows[i].Cells["Duration"].Value));

                if (cboShiftType.SelectedValue.ToInt32() == 3)
                {
                    if (tsMaxDuration < GetDurationInMinutes(Convert.ToString(dgvDynamicShift.Rows[i].Cells["MinWorkingHrs"].Value)))
                        tsMaxDuration = GetDurationInMinutes(Convert.ToString(dgvDynamicShift.Rows[i].Cells["MinWorkingHrs"].Value));

                }
                if (sFromTime < sShiftFromTime || sFromTime > sShiftToTime || sToTime < sShiftFromTime || sToTime > sShiftToTime)
                {
                    bIsValid = false;
                    if (cboShiftType.SelectedValue.ToInt32() == 4 || cboShiftType.SelectedValue.ToInt32() == 3)
                    {
                        MessageBox.Show("Shift Overlap is not allowed In " + cboShiftType.Text + " shift", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    }

                }
                else if (cboShiftType.SelectedValue.ToInt32() == 4)
                {
                    if (i + 1 < dgvDynamicShift.Rows.Count)
                    {
                        DateTime sFromTime1 = Convert.ToDateTime(Convert.ToDateTime(dgvDynamicShift.Rows[i + 1].Cells["FromTime"].Value).ToString("HH:mm tt"));
                        DateTime sToTime1 = Convert.ToDateTime(Convert.ToDateTime(dgvDynamicShift.Rows[i + 1].Cells["ToTime"].Value).ToString("HH:mm tt"));

                        if (ShiftChange1 == true)
                        {
                            sToTime1 = sToTime1.AddDays(1);
                            sFromTime1 = sFromTime1.AddDays(1);
                        }
                        if (sToTime1 <= sFromTime1 && ShiftChange1 == false)
                        {
                            ShiftChange1 = true;
                            sToTime1 = sToTime1.AddDays(1);
                        }
                        if (sFromTime1 < sToTime)
                        {
                            bIsValid = false;
                            if (cboShiftType.SelectedValue.ToInt32() == 4)
                            {
                                MessageBox.Show("Shift Overlap is not allowed In " + cboShiftType.Text + "  shift", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                break;
                            }
                        }
                    }
                }
            }
            if (cboShiftType.SelectedValue.ToInt32() != 4 && cboShiftType.SelectedValue.ToInt32() != 3)
            {
                bIsValid = true;
            }
            if (bIsValid == true)
            {
                if (cboShiftType.SelectedValue.ToInt32() == 3)
                {
                    DateTime iWorkTime = Microsoft.VisualBasic.DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Minute, Convert.ToDouble(tsMaxDuration), DateTime.Today);
                    //string sWorkTime = iWorkTime.Minute.ToString() ;
                    // string sWorkTime = DateAdd(DateInterval.Minute, Convert.ToDouble(tsMaxDuration), DateTime.Today);
                    string sWorkTime = Convert.ToDateTime(iWorkTime).ToString("HH:mm");
                    dtpMinWorkingHours.Text = sWorkTime;
                }
                else if (cboShiftType.SelectedValue.ToInt32() == 4)
                {
                    if (tsTotalDuration < GetDurationInMinutes(dtpMinWorkingHours.Text))
                    {
                        bIsValid = false;
                        MessageBox.Show("Minimum working hours exceeds duration", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
            }
            return bIsValid;
        }
        #endregion SplitShiftValidation

        #region GetDurationInMinutes
        /// <summary>
        /// Function to Get Duration In Minutes
        /// </summary>
        /// <param name="sDuration"></param>
        /// <returns></returns>
        private int GetDurationInMinutes(string sDuration)
        {
            int iDurationInMinutes = 0;
            string[] sHourMinute;
            if (sDuration.Contains("."))
            {
                sHourMinute = sDuration.Split(Convert.ToChar("."));
                iDurationInMinutes = sHourMinute[0].ToInt32() * 60 + sHourMinute[1].ToInt32();
            }

            else if (sDuration.Contains(":"))
            {
                sHourMinute = sDuration.Split(Convert.ToChar(":"));
                iDurationInMinutes = sHourMinute[0].ToInt32() * 60 + sHourMinute[1].ToInt32();
            }

            else if (sDuration != "")
            {
                iDurationInMinutes = sDuration.ToInt32() * 60;
            }
            return iDurationInMinutes;

        }
        #endregion GetDurationInMinutes

        #region GetMinFromHour
        /// <summary>
        /// Function to GetMintFromHour
        /// </summary>
        /// <param name="decMint"></param>
        /// <returns></returns>
        private decimal GetMinFromHour(decimal decMint)
        {
            DataSet ds;
            ds = MobjclsBLLShiftPolicy.GetMinFromHour(decMint);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToDecimal(ds.Tables[0].Rows[0][0]);
            }
            return 0;
        }
        #endregion GetMinFromHour

        #region GetHourFromMin
        /// <summary>
        /// Function To GetHourFromMin
        /// </summary>
        /// <param name="decMint"></param>
        /// <returns></returns>
        private decimal GetHourFromMin(decimal decMint)
        {
            DataSet ds;
            ds = MobjclsBLLShiftPolicy.GetHourFromMin(decMint);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToDecimal(ds.Tables[0].Rows[0][0]);
            }
            return 0;
        }
        #endregion GetHourFromMin

        #region AddTimings
        /// <summary>
        /// Function to Add Timing
        /// </summary>
        private void AddTimings()
        {
            int intTotTimings = 0;
            Decimal decTotDuration = 0;
            Decimal decDuration = 0;
            Decimal TotalMin = 0;
            Decimal DivMin = 0;
            DateTime dteFrom;
            DateTime dteTo;
            string[] strTime;
            int intHour = 0;
            int intMinute = 0;
            String strDuration;
            char[] cSplitChar = { ':' };
            dgvDynamicShift.Rows.Clear();

            if (txtNoOfTimings.Text.Trim() == "")
                return;

            else if (Convert.ToDecimal(txtNoOfTimings.Text) == 0)
                return;

            if (txtNoOfTimings.Text.Trim() != "")
            {
                //datediff(mi,'12:30 PM','06:30 PM')

                intTotTimings = txtNoOfTimings.Text.Trim().ToInt32();
                decTotDuration = txtDuration.Text.Replace(":", ".").Trim().ToDecimal();
                TotalMin = GetMinFromHour(decTotDuration);
                DivMin = TotalMin / intTotTimings;
                decDuration = GetHourFromMin(DivMin);

                //decDuration = Math.Round((decTotDuration / intTotTimings), 2);
                dteFrom = FromTimeDateTimePicker.Value;
                dteTo = dteFrom.AddMinutes((double)DivMin);
                //dteTo = dteFrom.AddHours((double)decDuration);

                strTime = ((decDuration * 100).ToString("00:00")).Split(cSplitChar);

                intHour = strTime[0].ToInt32();
                intMinute = strTime[1].ToInt32();

                if (intMinute > 59)
                {
                    intHour += 1;
                    intMinute = intMinute - 60;
                }

                strDuration = intHour.ToString("00") + ":" + intMinute.ToString("00");

                if (cboShiftType.SelectedValue.ToInt32() != 4)
                    dtpMinWorkingHours.Text = strDuration;


                int j = 1;
                for (int i = 0; i < intTotTimings; ++i)
                {
                    dgvDynamicShift.RowCount = dgvDynamicShift.RowCount + 1;
                    dgvDynamicShift.Rows[i].Cells[0].Value = 0;
                    dgvDynamicShift.Rows[i].Cells[1].Value = j;
                    if (Glb24HourFormat == true)
                    {
                        FromTime.CustomFormat = "HH:mm";
                        ToTime.CustomFormat = "HH:mm";
                        dgvDynamicShift.Rows[i].Cells[2].Value = dteFrom.ToString("HH:mm");
                        dgvDynamicShift.Rows[i].Cells[3].Value = dteTo.ToString("HH:mm");
                    }
                    else
                    {
                        FromTime.CustomFormat = "hh:mm tt";
                        ToTime.CustomFormat = "hh:mm tt";
                        dgvDynamicShift.Rows[i].Cells[2].Value = ConvertTo12Hr(dteFrom.ToString());
                        dgvDynamicShift.Rows[i].Cells[3].Value = ConvertTo12Hr(dteTo.ToString());
                    }

                    if (cboShiftType.SelectedValue.ToInt32() == 4)
                    {
                        if (i == 0)
                            dgvDynamicShift.Rows[i].Cells[FromTime.Index].ReadOnly = true;
                        else if (i == intTotTimings - 1)
                            dgvDynamicShift.Rows[i].Cells[ToTime.Index].ReadOnly = true;

                    }

                    dgvDynamicShift.Rows[i].Cells[Duration.Index].Value = strDuration;
                    dgvDynamicShift.Rows[i].Cells[MinWorkingHrs.Index].Value = strDuration;
                    dgvDynamicShift.Rows[i].Cells[AllowedBreakTime.Index].Value = "0";

                    TimeSpan tsActualDuration = dteTo.Subtract(dteFrom);
                    string sActualDuration = "";
                    int iHrs = tsActualDuration.Hours;
                    int iMins = tsActualDuration.Minutes;

                    sActualDuration = iHrs.ToString() + ":" + iMins.ToString();
                    dgvDynamicShift.Rows[i].Cells["ActualDuration"].Value = sActualDuration;

                    dteFrom = dteTo;
                    dteTo = dteFrom.AddMinutes((double)DivMin);
                    j = j + 1;
                }
            }
        }
        #endregion AddTimings

        #region FillParameters
        /// <summary>
        ///  To FillParameters
        /// </summary>
        private void FillParameters()
        {
            try
            {
                if (MblnAddStatus == true)
                {
                    MobjclsBLLShiftPolicy.clsDTOShiftPolicy.ShiftID = 0;
                }
                else
                {
                    MobjclsBLLShiftPolicy.clsDTOShiftPolicy.ShiftID = DescriptionTextBox.Tag.ToInt32();
                }
                MobjclsBLLShiftPolicy.clsDTOShiftPolicy.Description = DescriptionTextBox.Text;
                MobjclsBLLShiftPolicy.clsDTOShiftPolicy.FromTime = FromTimeDateTimePicker.Value.ToString("HH:mm");
                MobjclsBLLShiftPolicy.clsDTOShiftPolicy.ToTime = ToTimeDateTimePicker.Value.ToString("HH:mm");
                MobjclsBLLShiftPolicy.clsDTOShiftPolicy.ShiftType = cboShiftType.SelectedValue.ToInt32();
                MobjclsBLLShiftPolicy.clsDTOShiftPolicy.Duration = Convert.ToString(txtDuration.Text);
                MobjclsBLLShiftPolicy.clsDTOShiftPolicy.WeekWrkHrs = txtWeekWrkHrs.Text.ToString();

                if (cboShiftType.SelectedValue.ToInt32() == 3 || cboShiftType.SelectedValue.ToInt32() == 4)
                    MobjclsBLLShiftPolicy.clsDTOShiftPolicy.NoOfTimings = txtNoOfTimings.Text.ToInt32();

                MobjclsBLLShiftPolicy.clsDTOShiftPolicy.MinWorkingHours = dtpMinWorkingHours.Text;
                if (cboShiftType.SelectedValue.ToInt32() != 3)
                    MobjclsBLLShiftPolicy.clsDTOShiftPolicy.AllowedBreakTime = txtAllowedBreakTime.Text.ToInt32();
                if (txtLateAfter.Text != "")
                    MobjclsBLLShiftPolicy.clsDTOShiftPolicy.LateAfter = txtLateAfter.Text.ToInt32();
                if (txtEarlyBefore.Text != "")
                    MobjclsBLLShiftPolicy.clsDTOShiftPolicy.EarlyBefore = txtEarlyBefore.Text.ToInt32();
                MobjclsBLLShiftPolicy.clsDTOShiftPolicy.ShiftOTPolicyID = 0;
                if (txtBufferTime.Text != "")
                    MobjclsBLLShiftPolicy.clsDTOShiftPolicy.intBuffer = txtBufferTime.Text.ToInt32();

                if (txtMinTimeForOT.Text != "")
                    MobjclsBLLShiftPolicy.clsDTOShiftPolicy.intMinimumOT = txtMinTimeForOT.Text.ToInt32();

                if (chkIsMinWorkOT.Checked)
                    MobjclsBLLShiftPolicy.clsDTOShiftPolicy.IsMinWorkOT = 1;
                else
                    MobjclsBLLShiftPolicy.clsDTOShiftPolicy.IsMinWorkOT = 0;

                if (chkISBufferForOT.Checked)
                    MobjclsBLLShiftPolicy.clsDTOShiftPolicy.intIsBufferForOT = 1;
                else
                    MobjclsBLLShiftPolicy.clsDTOShiftPolicy.intIsBufferForOT = 0;

                
                FillDynamicShiftPolicyParameters();

            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on FillParameters " + this.Name + " " + Ex.Message.ToString(), 1);
            }


        }
        #endregion FillParameters

        #region FillDynamicShiftPolicyParameters
        private void FillDynamicShiftPolicyParameters()
        {
            MobjclsBLLShiftPolicy.clsDTOShiftPolicy.lstclsDTOShiftPolicyDynamic = new System.Collections.Generic.List<DTO.clsDTOShiftPolicyDynamic>();

            try
            {

                for (int i = 0; i < dgvDynamicShift.Rows.Count; i++)
                {
                    clsDTOShiftPolicyDynamic objclsDTOShiftPolicyDynamic = new clsDTOShiftPolicyDynamic();
                    objclsDTOShiftPolicyDynamic.OrderNo = dgvDynamicShift["OrderNo", i].Value.ToInt32();
                    objclsDTOShiftPolicyDynamic.FromTime = Convert.ToDateTime(dgvDynamicShift["FromTime", i].Value).ToString("HH:mm");
                    objclsDTOShiftPolicyDynamic.ToTime = Convert.ToDateTime(dgvDynamicShift["ToTime", i].Value).ToString("HH:mm");

                    if (Convert.ToString(dgvDynamicShift["BufferTime", i].Value) == "" || Convert.ToString(dgvDynamicShift["BufferTime", i].Value) == ".")
                    {
                        dgvDynamicShift["BufferTime", i].Value = 0;
                    }
                    if (cboShiftType.SelectedValue.ToInt32() == 3)
                    {
                        int iTime = 0;

                        if (Int32.TryParse(Convert.ToString(dgvDynamicShift.Rows[i].Cells["AllowedBreakTime"].Value), out iTime)) { }

                        // Int32.TryParse(Convert.ToString(dgvDynamicShift.Rows[i].Cells["AllowedBreakTime"].Value) ,iTime);
                        objclsDTOShiftPolicyDynamic.AllowedBreakTime = iTime;
                        objclsDTOShiftPolicyDynamic.MinWorkingHours = Convert.ToString(dgvDynamicShift.Rows[i].Cells["MinWorkingHrs"].Value);
        
                    }
                    int iTimei = 0;
                    if (Int32.TryParse(Convert.ToString(dgvDynamicShift.Rows[i].Cells["BufferTime"].Value), out iTimei)) { }
                    objclsDTOShiftPolicyDynamic.BufferTime = iTimei;

                    if (Convert.ToString(dgvDynamicShift["Duration", i].Value) == "" || Convert.ToString(dgvDynamicShift["Duration", i].Value) == ".")
                    {
                        dgvDynamicShift["Duration", i].Value = 0;
                    }

                    objclsDTOShiftPolicyDynamic.Duration = Convert.ToString(dgvDynamicShift["Duration", i].Value);
                    objclsDTOShiftPolicyDynamic.DynamicDescription = "Timing" + Convert.ToString(i + 1);
                    objclsDTOShiftPolicyDynamic.ShiftOTPolicyID = dgvDynamicShift["cboShiftOTPolicyID", i].Value.ToInt32();
                    MobjclsBLLShiftPolicy.clsDTOShiftPolicy.lstclsDTOShiftPolicyDynamic.Add(objclsDTOShiftPolicyDynamic);

                }
            }
            catch (Exception )
            {
                //
            }
        }
        #endregion FillDynamicShiftPolicyParameters

        #region Convert24HrFormat
        private string Convert24HrFormat(string StrValue)
        {

            if (StrValue.IndexOf(".") != -1)
                StrValue = StrValue.Replace(".", ":");

            string ReturnTime = "";
            if (StrValue.IndexOf(":") != -1)
            {
                string LeftTime = "";
                string RightTime = "";
                string[] strar = new string[2];
                strar = StrValue.Split(':');
                LeftTime = strar[0];
                RightTime = strar[1];
                if (Microsoft.VisualBasic.Information.IsNumeric(RightTime))
                {
                    if ((RightTime.Length) > 2)
                        return Microsoft.VisualBasic.Strings.Left(StrValue, 2);

                }
            }
            if (StrValue.IndexOf(":") != -1)
            {
                string LeftTime = "";
                string RightTime = "";
                string Time1 = "";
                string time2 = "";
                string[] strar = new string[2];
                strar = StrValue.Split(':');
                LeftTime = strar[0];
                RightTime = strar[1];
                if (Microsoft.VisualBasic.Information.IsNumeric(LeftTime))
                {
                    if (LeftTime.ToInt32() > 23)
                        return StrValue;
                    else
                    {
                        if (LeftTime.Length == 2)
                            Time1 = LeftTime;
                        else if (LeftTime.Length == 1)
                            Time1 = "0" + LeftTime;

                    }
                }
                else
                    return "10p";

                if (Microsoft.VisualBasic.Information.IsNumeric(RightTime))
                    if (RightTime.ToInt32() > 59)
                        return StrValue;
                    else
                    {
                        if (RightTime.Length == 2)
                            time2 = RightTime;
                        else if (RightTime.Length == 1)
                            time2 = "0" + RightTime;

                    }
                else
                    return "10p";

                ReturnTime = Time1 + ":" + time2;
                return ReturnTime;
            }
            else
            {

                if (Microsoft.VisualBasic.Information.IsNumeric(StrValue))
                {
                    if (StrValue.ToInt32() > 23)
                        return StrValue;
                    else
                    {
                        if (StrValue.Length == 2)
                        {
                            ReturnTime = StrValue + ":" + "00";
                            return ReturnTime;
                        }
                        else if (StrValue.Length == 1)
                        {
                            ReturnTime = "0" + StrValue + ":" + "00";
                            return ReturnTime;
                        }
                        else
                            return StrValue;

                    }
                }
                else
                {
                    return StrValue;
                }




            }
        }
        #endregion Convert24HrFormat

        #region Convert24HrFormat

        private string ConvertTo12Hr(string Time)
        {
            dtpHourFormat12.CustomFormat = "hh:mm tt";
            string msTime = Time;
            string ConvertedTime = "";
            dtpHourFormat12.Value = Convert.ToDateTime(Time);
            ConvertedTime = dtpHourFormat12.Value.ToString();
            return ConvertedTime;
        }
        #endregion Convert24HrFormat

        #region Convert24HrFormat

        private string ConvertTo24Hr(string Time)
        {
            dtpHourFormat12.CustomFormat = "HH:mm";
            string msTime = Time;
            string ConvertedTime = "";
            dtpHourFormat12.Value = Convert.ToDateTime(Time);
            ConvertedTime = dtpHourFormat12.Value.ToString();
            return ConvertedTime;
        }
        #endregion Convert24HrFormat

        #region ChangeStatus
        private void ChangeStatus()
        {
            ErrorProviderShift.Clear();
            if (MblnAddStatus)
            {
                BtnOk.Enabled = MblnAddPermission;
                BtnSave.Enabled = MblnAddPermission;
                ShiftReferenceBindingNavigatorSaveItem.Enabled = MblnAddPermission;

            }
            else
            {
                BtnOk.Enabled = MblnUpdatePermission;
                BtnSave.Enabled = MblnUpdatePermission;
                ShiftReferenceBindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
        }
        #endregion ChangeStatus

        #region GridSetUp
        private void GridSetUp(int GridTypeID)
        {
            try
            {
                dgvDynamicShift.Rows.Clear();

                dgvDynamicShift.Columns["ShiftID"].Visible = false;
                dgvDynamicShift.Columns["OrderNo"].Visible = true;
                dgvDynamicShift.Columns["FromTime"].Visible = true;
                dgvDynamicShift.Columns["ToTime"].Visible = true;
                dgvDynamicShift.Columns["Duration"].Visible = true;
                // dgvDynamicShift.Columns["ActualDuration"].Visible = true;
                dgvDynamicShift.Columns["AllowedBreakTime"].Visible = true;
                dgvDynamicShift.Columns["MinWorkingHrs"].Visible = true;
                dgvDynamicShift.Columns["BufferTime"].Visible = true;
                dgvDynamicShift.Columns["cboShiftOTPolicyID"].Visible = true;

                dgvDynamicShift.Columns["OrderNo"].Width = 50;
                dgvDynamicShift.Columns["FromTime"].Width = 60;
                dgvDynamicShift.Columns["ToTime"].Width = 60;
                dgvDynamicShift.Columns["Duration"].Width = 60;

                if (cboShiftType.SelectedValue.ToInt32() == 3)
                {
                    dgvDynamicShift.Columns["AllowedBreakTime"].Visible = true;
                    dgvDynamicShift.Columns["MinWorkingHrs"].Visible = true;
                    dgvDynamicShift.Columns["BufferTime"].Visible = true;
                    dgvDynamicShift.Columns["cboShiftOTPolicyID"].Visible = false;
                    //dgvDynamicShift.Columns["Duration"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    dgvDynamicShift.Columns["BufferTime"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                }
                else if (cboShiftType.SelectedValue.ToInt32() == 4)
                {
                    dgvDynamicShift.Columns["cboShiftOTPolicyID"].Visible = false;
                    dgvDynamicShift.Columns["BufferTime"].Visible = true;
                    dgvDynamicShift.Columns["cboShiftOTPolicyID"].Visible = false;
                    dgvDynamicShift.Columns["AllowedBreakTime"].Visible = false;
                    dgvDynamicShift.Columns["MinWorkingHrs"].Visible = false;
                    dgvDynamicShift.Columns["OrderNo"].Width = 80;
                    dgvDynamicShift.Columns["FromTime"].Width = 80;
                    dgvDynamicShift.Columns["ToTime"].Width = 80;
                    dgvDynamicShift.Columns["Duration"].Width = 80;
                    dgvDynamicShift.Columns["BufferTime"].Width = 125;
                    //dgvDynamicShift.Columns["Duration"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    dgvDynamicShift.Columns["BufferTime"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
                dgvDynamicShift.AllowUserToAddRows = false;
                dgvDynamicShift.AllowUserToDeleteRows = false;
            }
            catch (Exception)
            {

            }
        }
        #endregion GridSetUp

        #region GridSetUp
        private void ClearAllControls()
        {
            DescriptionTextBox.Text = "";
            txtNoOfTimings.Text = "0";
            DescriptionTextBox.Tag = 0;
            txtAllowedBreakTime.Text = "0";
            txtEarlyBefore.Text = "0";
            txtLateAfter.Text = "0";
            txtBufferTime.Text = "0";
            txtMinTimeForOT.Text = "0";
            txtWeekWrkHrs.Text = "0";
            chkISBufferForOT.Checked = false;
            tabMain.SelectedTab = tabShiftDetails;
            dgvDynamicShift.Rows.Clear();
            if (cboShiftType.Items.Count > 0)
                cboShiftType.SelectedIndex = 0;
            //cboOTPolicy.SelectedIndex = -1;
            if (Glb24HourFormat == false)
            {
                FromTimeDateTimePicker.CustomFormat = "hh:mm tt";
                ToTimeDateTimePicker.CustomFormat = "hh:mm tt";
            }
            else
            {
                FromTimeDateTimePicker.CustomFormat = "HH:mm";
                ToTimeDateTimePicker.CustomFormat = "HH:mm";
            }
            FromTimeDateTimePicker.Value = ToTimeDateTimePicker.Value = DateTime.Now;
            
        }
        #endregion GridSetUp

        #region DisplayShiftInfo
        private void DisplayShiftInfo()
        {
            ClearAllControls();
            CancelToolStripButton.Enabled = false;
            btnPrint.Enabled=MblnPrintEmailPermission;
            lblstatus.Text = string.Empty;
            MobjclsBLLShiftPolicy = new clsBLLShiftPolicy();
            try
            {
                if (DgvShift.RowCount >= 1)
                {
                    if (PiShiftID > 0) MiShiftID = PiShiftID;
                    else
                        MiShiftID = DgvShift.CurrentRow.Cells[0].Value.ToInt32();
                    MintCompanyId = DgvShift.CurrentRow.Cells[6].Value.ToInt32();
                    MobjclsBLLShiftPolicy.clsDTOShiftPolicy.ShiftID = MiShiftID;

                    using (SqlDataReader sdrShift = MobjclsBLLShiftPolicy.GetShiftDetails())
                    {
                        if (sdrShift.Read())
                        {
                            DescriptionTextBox.Tag = sdrShift["ShiftID"].ToInt32();
                            DescriptionTextBox.Text = Convert.ToString(sdrShift["ShiftName"]);
                            FromTimeDateTimePicker.Text = Convert.ToString(sdrShift["FromTime"]);
                            ToTimeDateTimePicker.Text = Convert.ToString(sdrShift["ToTime"]);
                            cboShiftType.SelectedValue = sdrShift["ShiftTypeID"].ToInt32();
                            txtDuration.Text = Convert.ToString((sdrShift["Duration"])).Replace(".", ":");
                            txtNoOfTimings.Text = Convert.ToString(sdrShift["NoOfTimings"]);
                            if (sdrShift["MinWorkingHours"] != DBNull.Value)
                                dtpMinWorkingHours.Text = Convert.ToString(sdrShift["MinWorkingHours"]);

                            txtAllowedBreakTime.Text = Convert.ToString(sdrShift["AllowedBreakTime"]);
                            txtLateAfter.Text = Convert.ToString(sdrShift["LateAfter"]);
                            txtEarlyBefore.Text = Convert.ToString(sdrShift["EarlyBefore"]);
                            txtBufferTime.Text = Convert.ToString(sdrShift["Buffer"]);
                            txtMinTimeForOT.Text = Convert.ToString(sdrShift["MinimumOT"]);
                            txtWeekWrkHrs.Text = Convert.ToString(sdrShift["WeekWrkHrs"]);

                            if (sdrShift["IsMinimumWorkOT"] == DBNull.Value)
                                chkIsMinWorkOT.Checked = false;
                            else
                                chkIsMinWorkOT.Checked = Convert.ToBoolean(sdrShift["IsMinimumWorkOT"]);

                            if (sdrShift["IsBufferForOT"] == DBNull.Value)
                                chkISBufferForOT.Checked = false;
                            else
                                chkISBufferForOT.Checked = Convert.ToBoolean(sdrShift["IsBufferForOT"]);

                            MblnAddStatus = false;
                            SetEnableDisable(ControlState.Disable);
                            //BindingNavigatorAddNewItem.Enabled = true;
                            //BindingNavigatorDeleteItem.Enabled = true;
                        }
                        sdrShift.Close();
                    }

                    if (cboShiftType.SelectedValue.ToInt32() == 3 || cboShiftType.SelectedValue.ToInt32() == 4)
                    {
                        GridSetUp(cboShiftType.SelectedValue.ToInt32());
                        //int i = 0;
                        DataSet datShift;
                        datShift = MobjclsBLLShiftPolicy.GetDynamicShiftDetails();
                        if (datShift.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < datShift.Tables[0].Rows.Count; i++)
                            {
                                dgvDynamicShift.RowCount = dgvDynamicShift.RowCount + 1;
                                dgvDynamicShift["ShiftID", i].Value = datShift.Tables[0].Rows[i]["ShiftID"];
                                dgvDynamicShift["OrderNo", i].Value = datShift.Tables[0].Rows[i]["OrderNo"];

                                if (cboShiftType.SelectedValue.ToInt32() == 4)
                                {
                                    if (i == 0)
                                        dgvDynamicShift["FromTime", i].ReadOnly = true;
                                    else if (i == datShift.Tables[0].Rows.Count - 1)
                                        dgvDynamicShift["ToTime", i].ReadOnly = true;

                                }
                                if (Glb24HourFormat == true)
                                {
                                    FromTime.CustomFormat = "HH:mm";
                                    ToTime.CustomFormat = "HH:mm";
                                    dgvDynamicShift["FromTime", i].Value = datShift.Tables[0].Rows[i]["FromTime"];
                                    dgvDynamicShift["ToTime", i].Value = datShift.Tables[0].Rows[i]["ToTime"];
                                }
                                else
                                {
                                    FromTime.CustomFormat = "hh:mm tt";
                                    ToTime.CustomFormat = "hh:mm tt";
                                    dgvDynamicShift["FromTime", i].Value = ConvertTimeTo12Hr(Convert.ToString(datShift.Tables[0].Rows[i]["FromTime"]));
                                    dgvDynamicShift["ToTime", i].Value = ConvertTimeTo12Hr(Convert.ToString(datShift.Tables[0].Rows[i]["ToTime"]));
                                }

                                dgvDynamicShift["Duration", i].Value = datShift.Tables[0].Rows[i]["Duration"];
                                dgvDynamicShift["AllowedBreakTime", i].Value = datShift.Tables[0].Rows[i]["AllowedBreakTime"];
                                dgvDynamicShift["BufferTime", i].Value = datShift.Tables[0].Rows[i]["BufferTime"];
                                dgvDynamicShift["MinWorkingHrs", i].Value = datShift.Tables[0].Rows[i]["MinWorkingHours"];

                            }
                        }
                    }
                }
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on DisplayShiftInfo " + this.Name + " " + Ex.Message.ToString(), 1);
            }
        }
        #endregion DisplayShiftInfo

        #region GetTimeDifference
        private void GetTimeDifference()
        {
            DateTime date1;
            DateTime date2;
            try
            {
                date1 = Convert.ToDateTime(FromTimeDateTimePicker.Text);
                date2 = Convert.ToDateTime(ToTimeDateTimePicker.Text);

                if (date2 > date1)
                {
                    txtDuration.Text = date2.Subtract(date1).Hours.ToString() + "." + date2.Subtract(date1).Minutes.ToString();
                    dtpMinWorkingHours.Text = date2.Subtract(date1).Hours.ToString() + ":" + date2.Subtract(date1).Minutes.ToString();
                }
                else
                {

                    //int iHour;
                    //if (FromTimeDateTimePicker.Text == ToTimeDateTimePicker.Text)
                    //    iHour = (23 - date1.Hour) + date2.Hour;
                    //else
                    //    iHour = (24 - date1.Hour) + date2.Hour;

                    //txtDuration.Text = iHour + "." + date2.Subtract(date1).Minutes.ToString();
                    //dtpMinWorkingHours.Text = iHour + ":" + date2.Subtract(date1).Minutes.ToString();

                    int tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(date1, date2);
                    if (tmDiff < 0)
                    {
                        date2 = date2.AddDays(1);
                        tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(date1, date2);
                    }

                    //if (tmDiff == 1440)
                    //    tmDiff = tmDiff - 1;
                    string sWorkTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(tmDiff), DateTime.Today));
                    sWorkTime = Convert.ToDateTime(sWorkTime).ToString("HH:mm");
                    txtDuration.Text = sWorkTime;
                    dtpMinWorkingHours.Text = sWorkTime;

                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on GetTimeDifference " + this.Name + " " + Ex.Message.ToString(), 1);
            }
        }
        #endregion GetTimeDifference

        #region IsValidTime
        private bool IsValidTime(string sTime)
        {
            try
            {
                //'Sample  Format  "13:14"
                //DateTime TheDate ;
                //TheDate = sTime;
                string Format1 = "^*(1[0-2]|[1-9]):[0-5][0-9]*(a|p|A|P)(m|M)*$";
                System.Text.RegularExpressions.Regex TryValidateFormat1 = new System.Text.RegularExpressions.Regex(Format1);
                string Format2 = "([0-1][0-9]|2[0-3]):([0-5][0-9])";
                System.Text.RegularExpressions.Regex TryValidateFormat2 = new System.Text.RegularExpressions.Regex(Format2);
                return TryValidateFormat2.IsMatch(sTime) || TryValidateFormat1.IsMatch(sTime);
            }
            catch (Exception Ex)
            {
                //'objLog.WriteLog("Error on " & System.Reflection.MethodBase.GetCurrentMethod.Name & " : " & Me.Name & " " & ex.Message.ToString(), 1)
                return false;
            }
        }
        #endregion IsValidTime

        #region DeleteValidation
        private bool DeleteValidation()
        {
            //function for deleting validation
            try
            {
                MobjclsBLLShiftPolicy.clsDTOShiftPolicy.ShiftID = DescriptionTextBox.Tag.ToInt32();
                if (MobjclsBLLShiftPolicy.IsExists(10))
                {


                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9001, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Shift Policy");

                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    return false;
                }

                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, (int)CommonMessages.DeleteConfirm, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;
                else
                    return true;
            }
           
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on DeleteValidation " + this.Name + " " + Ex.Message.ToString(), 1);
                return false;
            }

        }
        #endregion DeleteValidation

        #endregion Methods

        #region Events

        private void ShiftPolicy_Load(object sender, EventArgs e) // Formload
        {
            if (PiCompanyID > 0) MintCompanyId = PiCompanyID;

            SetDatetimePicker();
            SetPermissions();
            LoadCombos();
            LoadInitial(MintCompanyId);
            LoadMessage();
            FillShiftDetails();
            NewShiftPolicy();

            DgvShift.ClearSelection();
            ErrorProviderShift.Clear();
            BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorDeleteItem.Enabled = false;

            if (PiShiftID > 0)
            {
                DisplayShiftInfo();
                if (DgvShift.Rows.Count > 0)
                {
                    for (int i = 0; i <= DgvShift.Rows.Count - 1; i++)
                    {
                        if (DgvShift.Rows[i].Cells[0].Value.ToInt32() == PiShiftID)
                        {
                            DgvShift.Focus();
                            DgvShift.CurrentCell = DgvShift[2, i];

                        }
                    }
                }
            }
            //tabMain.TabPages.RemoveAt(1);
            DescriptionTextBox.Select();
        }

        private void FromTimeDateTimePicker_ValueChanged(object sender, EventArgs e) //FromTimeDateTimePicker_ValueChanged
        {
            GetTimeDifference();
            ChangeStatus();

            AddTimings();
        }

        private void ToTimeDateTimePicker_ValueChanged(object sender, EventArgs e)   //ToTimeDateTimePicker_ValueChanged
        {
            GetTimeDifference();
            ChangeStatus();

            AddTimings();
        }

        private void CompanyMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            BtnSave_Click(sender, e);
        } //CompanyMasterBindingNavigatorSaveItem_Click

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e) //Button Click to AddNewItem
        {
            NewShiftPolicy();
        }

        private void BtnSave_Click(object sender, EventArgs e) // button click to save policy
        {

            if (Saveshift())
            {
                FillShiftDetails();
                NewShiftPolicy();             
                tabMain.SelectedTab = tabShiftDetails;
            }

        }

       

        private void DgvShift_SelectionChanged(object sender, EventArgs e)
        {

        }

        private void DescriptionTextBox_TextChanged(object sender, EventArgs e) // to change the Status
        {
            ChangeStatus();
        }

        private void cboShiftType_SelectedIndexChanged(object sender, EventArgs e) //CboBox Selected Index Change
        {
            txtAllowedBreakTime.Enabled = true;
            dtpMinWorkingHours.Enabled = true;
            txtLateAfter.Enabled = true;
            txtEarlyBefore.Enabled = true;

            txtEarlyBefore.Text = "0";
            txtLateAfter.Text = "0";
            txtAllowedBreakTime.Text = "0";
            txtNoOfTimings.Text = "";
            txtNoOfTimings.Enabled = false;
            if (cboShiftType.SelectedValue.ToInt32() == 2)
            {
                txtEarlyBefore.Enabled = false;
                txtLateAfter.Enabled = false;
                txtAllowedBreakTime.Enabled = false;
            }
            else if ((cboShiftType.SelectedValue.ToInt32() == 3 || cboShiftType.SelectedValue.ToInt32() == 4) && tabMain.TabPages.Count < 2)
            {
                GridSetUp(cboShiftType.SelectedValue.ToInt32());
                if (cboShiftType.SelectedValue.ToInt32() == 3)
                {
                    txtNoOfTimings.Text = "";
                    txtNoOfTimings.Enabled = true;
                    txtAllowedBreakTime.Enabled = false;
                    dtpMinWorkingHours.Enabled = false;

                    //if (ClsCommonSettings.IsArabicView)
                    //    TempTab.Text = "تفاصيل التحول الديناميكي";
                    //else
                        TempTab.Text = "Dynamic Shift Details";
                }
                else if (cboShiftType.SelectedValue.ToInt32() == 4)
                {
                    txtNoOfTimings.Text = "";
                    txtNoOfTimings.Enabled = true;

                    //if (ClsCommonSettings.IsArabicView)
                    //    TempTab.Text = "تفاصيل التحول الانقسام";
                    //else
                        TempTab.Text = "Split Shift Details";
                }
                tabMain.TabPages.Insert(1, TempTab);
            }

            else if (tabMain.TabPages.Count > 1 && (cboShiftType.SelectedValue.ToInt32() != 3 && cboShiftType.SelectedValue.ToInt32() != 4))
            {
                TempTab = tabMain.TabPages[1];
                tabMain.TabPages.RemoveAt(1);
            }

            else if (cboShiftType.SelectedValue.ToInt32() == 3 || cboShiftType.SelectedValue.ToInt32() == 4)
            {
                txtNoOfTimings.Text = "";
                txtNoOfTimings.Enabled = true;
                if (cboShiftType.SelectedValue.ToInt32() == 3)
                {
                    txtAllowedBreakTime.Enabled = false;
                    dtpMinWorkingHours.Enabled = false;

                    //if (ClsCommonSettings.IsArabicView)
                    //    TempTab.Text = "تفاصيل التحول الديناميكي";
                    //else
                        TempTab.Text = "Dynamic Shift Details";
                }
                else if (cboShiftType.SelectedValue.ToInt32() == 4)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    TempTab.Text = "تفاصيل التحول الانقسام";
                    //else
                        TempTab.Text = "Split Shift Details";
                }

                GridSetUp(cboShiftType.SelectedValue.ToInt32());
            }


            if (txtNoOfTimings.Text != "")
            {
                AddTimings();
                ChangeStatus();
            }

            GetTimeDifference();
            txtDuration.Enabled = false;

            if (cboShiftType.SelectedValue.ToInt32() == 1)
            {
                txtDuration.Enabled = false;
                txtNoOfTimings.Enabled = false;
                txtNoOfTimings.BackColor = SystemColors.Window;
                txtDuration.BackColor = SystemColors.Window;
            }
            else if (cboShiftType.SelectedValue.ToInt32() == 2)
            {

                txtNoOfTimings.Enabled = false;
                txtNoOfTimings.BackColor = SystemColors.Window;
                txtDuration.BackColor = SystemColors.Window;
            }
            else if (cboShiftType.SelectedValue.ToInt32() == 3 || cboShiftType.SelectedValue.ToInt32() == 4)
            {
                txtNoOfTimings.BackColor = SystemColors.Info;
                txtDuration.BackColor = SystemColors.Info;
                txtNoOfTimings.Enabled = true;
                //if (tabMain.TabPages.Count > 1)
                //{
                //    GridSetUp(Convert.ToInt32(cboShiftType.SelectedValue));
                //}
            }
            ErrorProviderShift.Clear();
            ChangeStatus();
        }

        private void txtMinWorkingHours_TextChanged(object sender, EventArgs e) //txtMinWorkingHours_TextChanged
        {
            ErrorProviderShift.Clear();
            ChangeStatus();
        }

        private void txtNoOfTimings_TextChanged(object sender, EventArgs e) //txtNoOfTimings_TextChanged
        {
            ErrorProviderShift.Clear();
            AddTimings();
            ChangeStatus();
        }

        private void BtnOk_Click(object sender, EventArgs e) // Button Click  for Save and Close
        {
            if (Saveshift())
            {
                BtnSave.Enabled = false;
                this.Close();
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e) //BtnCancel_Click
        {
            this.Close();
        }

        private void CancelToolStripButton_Click(object sender, EventArgs e) //CancelToolStripButton_Click
        {
            NewShiftPolicy();
        }

        private void DgvShift_CellClick(object sender, DataGridViewCellEventArgs e) // Grid( dgvShift cell Click) 
        {
            PiShiftID = 0;
            DisplayShiftInfo();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e) // BindingNavigatorDeleteItem_Click
        {
            try
            {
                if (DescriptionTextBox.Tag.ToInt32() == 0)
                    MessageBox.Show("Please select any information.", MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                else
                {
                    if (DeleteValidation())
                    {

                        //if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        //{
                        MobjclsBLLShiftPolicy.clsDTOShiftPolicy.ShiftID = DescriptionTextBox.Tag.ToInt32();
                        if (MobjclsBLLShiftPolicy.Delete())
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, (int)CommonMessages.Deleted, out MmessageIcon).Replace("#", "");
                            lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrFocus.Enabled = true;
                            MessageBox.Show(MstrMessageCommon, MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            NewShiftPolicy();
                            FillShiftDetails();
                        }
                        //}
                    }
                }
            }
            catch (SqlException Ex)
            {

                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9001, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "Shift Policy");

                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrComapny.Enabled = true;
              

            }
        }

        private void dgvDynamicShift_DataError(object sender, DataGridViewDataErrorEventArgs e) //dgvDynamicShift_DataError
        {
            try
            {
                //iii
            }
            catch (Exception)
            {
                //Console.WriteLine("{0} Exception caught.", e);
            }

        }

        private void dgvDynamicShift_CurrentCellDirtyStateChanged(object sender, EventArgs e) // dgvDynamicShift_CurrentCellDirtyStateChanged
        {
            dgvDynamicShift.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dgvDynamicShift_CellValidating(object sender, DataGridViewCellValidatingEventArgs e) //dgvDynamicShift_CellValidating
        {
            try
            {
                string date2 = string.Empty;
                DateTime D1;
                DateTime D2;
                if (e.ColumnIndex == 2 || e.ColumnIndex == 3)
                {

                    D1 = Convert.ToDateTime(dgvDynamicShift.Rows[e.RowIndex].Cells[2].Value);
                    D2 = Convert.ToDateTime(dgvDynamicShift.Rows[e.RowIndex].Cells[3].Value);
                    if (D2 > D1)
                    {
                        dgvDynamicShift.Rows[e.RowIndex].Cells["Duration"].Value = D2.Subtract(D1).Hours.ToString() + "." + D2.Subtract(D1).Minutes.ToString();
                    }
                    else
                    {
                        int iHour;
                        if (dgvDynamicShift.Rows[e.RowIndex].Cells[2].Value == dgvDynamicShift.Rows[e.RowIndex].Cells[3].Value)
                            iHour = (23 - D1.Hour) + D2.Hour;
                        else
                            iHour = (24 - D1.Hour) + D2.Hour;
                        dgvDynamicShift.Rows[e.RowIndex].Cells["Duration"].Value = iHour + "." + D1.Subtract(D2).Minutes.ToString();
                    }
                }
                else if (e.ColumnIndex == 6)
                {
                    if (dgvDynamicShift.CurrentRow.Cells[e.ColumnIndex].Value.ToString().Trim() == "")
                        return;
                    if (dgvDynamicShift.CurrentRow.Cells[e.ColumnIndex].Value != DBNull.Value)
                    {
                        dgvDynamicShift.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = Convert24HrFormat(dgvDynamicShift.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                        date2 = dgvDynamicShift.CurrentRow.Cells[e.ColumnIndex].Value.ToString();
                    }

                    if (IsValidTime(dgvDynamicShift.CurrentRow.Cells[e.ColumnIndex].Value.ToString()) == false)
                    {

                        MessageBox.Show("Invalid Time", MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        dgvDynamicShift.CurrentCell = dgvDynamicShift[e.ColumnIndex, e.RowIndex];
                        e.Cancel = true;

                    }


                }
            }
            catch (Exception)
            {
                //Console.WriteLine("{0} Exception caught.", e);
            }
        }

        private void dgvDynamicShift_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e) //dgvDynamicShift_EditingControlShowing 
        {
            this.dgvDynamicShift.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e) //EditingControl_KeyPress
        {
            if (dgvDynamicShift.CurrentCell.OwningColumn.Name == "BufferTime" || dgvDynamicShift.CurrentCell.OwningColumn.Name == "Duration")
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (!string.IsNullOrEmpty(dgvDynamicShift.EditingControl.Text) && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
        }

        private void checkBoxX1_CheckedChanged(object sender, EventArgs e) //checkBoxX1_CheckedChanged
        {
            ChangeStatus();
        }

        private void txtDuration_TextChanged(object sender, EventArgs e) //txtDuration_TextChanged
        {
            AddTimings();
            ChangeStatus();
        }

        private void btnPrint_Click(object sender, EventArgs e) //btnPrint_Click
        {
            try
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = "ShiftPolicy";
                ObjViewer.PiFormID = (int)FormID.ShiftPolicy;
                ObjViewer.PiRecId = MiShiftID;
                ObjViewer.ShowDialog();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnPrint_Click() " + ex.Message);

            }
        }

       

        private void txtBufferTime_TextChanged(object sender, EventArgs e) //txtBufferTime_TextChanged
        {
            ChangeStatus();
        }

        private void txtMinTimeForOT_TextChanged(object sender, EventArgs e) //txtMinTimeForOT_TextChanged
        {
            ChangeStatus();
        }

        private void txtEarlyBefore_TextChanged(object sender, EventArgs e) //txtEarlyBefore_TextChanged
        {
            ChangeStatus();
        }

        private void txtLateAfter_TextChanged(object sender, EventArgs e) //txtLateAfter_TextChanged
        {
            ChangeStatus();
        }

        private void txtAllowedBreakTime_TextChanged(object sender, EventArgs e) //txtAllowedBreakTime_TextChanged
        {
            ChangeStatus();
        }

        private void dtpMinWorkingHours_ValueChanged(object sender, EventArgs e) //dtpMinWorkingHours_ValueChanged
        {
            ChangeStatus();
        }

        private void dgvDynamicShift_CellClick(object sender, DataGridViewCellEventArgs e) //dgvDynamicShift_CellClick
        {
            ChangeStatus();
        }

        #endregion Events

        private void FrmShiftPolicy_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 8, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            try
            {
                FrmHelp objHelp = new FrmHelp();
                objHelp.strFormName = "ShiftPolicy";
                objHelp.ShowDialog();
                objHelp = null;
            }
            catch 
            {
               

            }
        }

        private void txtWeekWrkHrs_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

    }
}