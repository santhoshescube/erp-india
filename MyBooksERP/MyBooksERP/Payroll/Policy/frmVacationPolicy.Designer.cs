﻿namespace MyBooksERP
{
    partial class frmVacationPolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label LblPolicyDetails;
            System.Windows.Forms.Label lblGeneral;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVacationPolicy));
            this.bnHelp = new System.Windows.Forms.ToolStripButton();
            this.lblOnTimeRejoinDateBenefit = new System.Windows.Forms.Label();
            this.txtOnTimeRejoinBenefit = new System.Windows.Forms.TextBox();
            this.BtnBottomCancel = new System.Windows.Forms.Button();
            this.OKSaveButton = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.lblCalcVacation = new System.Windows.Forms.Label();
            this.cboCalculationBasedVacation = new System.Windows.Forms.ComboBox();
            this.CompanyVacationPolicyBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bnPreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bnPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.bnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.bnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusStripSettlement = new System.Windows.Forms.StatusStrip();
            this.tmrVacationPolicy = new System.Windows.Forms.Timer(this.components);
            this.lblGrossVacation = new System.Windows.Forms.Label();
            this.lblPolicyName = new System.Windows.Forms.Label();
            this.txtPolicyName = new System.Windows.Forms.TextBox();
            this.GrpCSP = new System.Windows.Forms.GroupBox();
            this.chkFullSalary = new System.Windows.Forms.CheckBox();
            this.chkLeaveOnly = new System.Windows.Forms.CheckBox();
            this.DetailsDataGridViewVacation = new DemoClsDataGridview.ClsDataGirdView();
            this.txtAddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkParticular = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.txtParticulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvVacation = new System.Windows.Forms.DataGridView();
            this.cboParameter = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.txtNoOfMonths = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNoOfDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNoOfTickets = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtTicketAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtGratuityID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            LblPolicyDetails = new System.Windows.Forms.Label();
            lblGeneral = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.CompanyVacationPolicyBindingNavigator)).BeginInit();
            this.CompanyVacationPolicyBindingNavigator.SuspendLayout();
            this.StatusStripSettlement.SuspendLayout();
            this.GrpCSP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DetailsDataGridViewVacation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVacation)).BeginInit();
            this.SuspendLayout();
            // 
            // LblPolicyDetails
            // 
            LblPolicyDetails.AutoSize = true;
            LblPolicyDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            LblPolicyDetails.Location = new System.Drawing.Point(2, 242);
            LblPolicyDetails.Name = "LblPolicyDetails";
            LblPolicyDetails.Size = new System.Drawing.Size(84, 13);
            LblPolicyDetails.TabIndex = 58;
            LblPolicyDetails.Text = "Policy Details";
            // 
            // lblGeneral
            // 
            lblGeneral.AutoSize = true;
            lblGeneral.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblGeneral.Location = new System.Drawing.Point(2, 0);
            lblGeneral.Name = "lblGeneral";
            lblGeneral.Size = new System.Drawing.Size(51, 13);
            lblGeneral.TabIndex = 19;
            lblGeneral.Text = "General";
            // 
            // bnHelp
            // 
            this.bnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnHelp.Image = ((System.Drawing.Image)(resources.GetObject("bnHelp.Image")));
            this.bnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnHelp.Name = "bnHelp";
            this.bnHelp.Size = new System.Drawing.Size(23, 22);
            this.bnHelp.Text = "He&lp";
            this.bnHelp.Visible = false;
            this.bnHelp.Click += new System.EventHandler(this.bnHelp_Click);
            // 
            // lblOnTimeRejoinDateBenefit
            // 
            this.lblOnTimeRejoinDateBenefit.AutoSize = true;
            this.lblOnTimeRejoinDateBenefit.Location = new System.Drawing.Point(9, 208);
            this.lblOnTimeRejoinDateBenefit.Name = "lblOnTimeRejoinDateBenefit";
            this.lblOnTimeRejoinDateBenefit.Size = new System.Drawing.Size(109, 13);
            this.lblOnTimeRejoinDateBenefit.TabIndex = 60;
            this.lblOnTimeRejoinDateBenefit.Text = "Ontime Rejoin Benefit";
            // 
            // txtOnTimeRejoinBenefit
            // 
            this.txtOnTimeRejoinBenefit.Location = new System.Drawing.Point(128, 205);
            this.txtOnTimeRejoinBenefit.MaxLength = 6;
            this.txtOnTimeRejoinBenefit.Name = "txtOnTimeRejoinBenefit";
            this.txtOnTimeRejoinBenefit.ShortcutsEnabled = false;
            this.txtOnTimeRejoinBenefit.Size = new System.Drawing.Size(85, 20);
            this.txtOnTimeRejoinBenefit.TabIndex = 5;
            this.txtOnTimeRejoinBenefit.TextChanged += new System.EventHandler(this.txtOnTimeRejoinBenefit_TextChanged);
            this.txtOnTimeRejoinBenefit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // BtnBottomCancel
            // 
            this.BtnBottomCancel.Location = new System.Drawing.Point(447, 429);
            this.BtnBottomCancel.Name = "BtnBottomCancel";
            this.BtnBottomCancel.Size = new System.Drawing.Size(60, 23);
            this.BtnBottomCancel.TabIndex = 2;
            this.BtnBottomCancel.Text = "&Cancel";
            this.BtnBottomCancel.UseVisualStyleBackColor = true;
            this.BtnBottomCancel.Click += new System.EventHandler(this.BtnBottomCancel_Click);
            // 
            // OKSaveButton
            // 
            this.OKSaveButton.Location = new System.Drawing.Point(381, 429);
            this.OKSaveButton.Name = "OKSaveButton";
            this.OKSaveButton.Size = new System.Drawing.Size(60, 23);
            this.OKSaveButton.TabIndex = 1;
            this.OKSaveButton.Text = "&Ok";
            this.OKSaveButton.UseVisualStyleBackColor = true;
            this.OKSaveButton.Click += new System.EventHandler(this.OKSaveButton_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(9, 429);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(60, 23);
            this.BtnSave.TabIndex = 0;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // lblCalcVacation
            // 
            this.lblCalcVacation.AutoSize = true;
            this.lblCalcVacation.Location = new System.Drawing.Point(9, 49);
            this.lblCalcVacation.Name = "lblCalcVacation";
            this.lblCalcVacation.Size = new System.Drawing.Size(107, 13);
            this.lblCalcVacation.TabIndex = 45;
            this.lblCalcVacation.Text = "Calculation Based on";
            // 
            // cboCalculationBasedVacation
            // 
            this.cboCalculationBasedVacation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCalculationBasedVacation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCalculationBasedVacation.BackColor = System.Drawing.SystemColors.Info;
            this.cboCalculationBasedVacation.DropDownHeight = 134;
            this.cboCalculationBasedVacation.FormattingEnabled = true;
            this.cboCalculationBasedVacation.IntegralHeight = false;
            this.cboCalculationBasedVacation.Location = new System.Drawing.Point(128, 41);
            this.cboCalculationBasedVacation.MaxDropDownItems = 10;
            this.cboCalculationBasedVacation.Name = "cboCalculationBasedVacation";
            this.cboCalculationBasedVacation.Size = new System.Drawing.Size(199, 21);
            this.cboCalculationBasedVacation.TabIndex = 1;
            this.cboCalculationBasedVacation.SelectedIndexChanged += new System.EventHandler(this.cboCalculationBasedVacation_SelectedIndexChanged);
            // 
            // CompanyVacationPolicyBindingNavigator
            // 
            this.CompanyVacationPolicyBindingNavigator.AddNewItem = null;
            this.CompanyVacationPolicyBindingNavigator.CountItem = null;
            this.CompanyVacationPolicyBindingNavigator.DeleteItem = null;
            this.CompanyVacationPolicyBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripSeparator4,
            this.bnMoveFirstItem,
            this.bnPreviousItem,
            this.bnPositionItem,
            this.bnCountItem,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.ToolStripSeparator5,
            this.btnAdd,
            this.BindingNavigatorSaveItem,
            this.bnDeleteItem,
            this.ToolStripSeparator6,
            this.bnClear,
            this.ToolStripSeparator7,
            this.bnPrint,
            this.bnEmail,
            this.ToolStripSeparator8,
            this.bnHelp});
            this.CompanyVacationPolicyBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.CompanyVacationPolicyBindingNavigator.MoveFirstItem = null;
            this.CompanyVacationPolicyBindingNavigator.MoveLastItem = null;
            this.CompanyVacationPolicyBindingNavigator.MoveNextItem = null;
            this.CompanyVacationPolicyBindingNavigator.MovePreviousItem = null;
            this.CompanyVacationPolicyBindingNavigator.Name = "CompanyVacationPolicyBindingNavigator";
            this.CompanyVacationPolicyBindingNavigator.PositionItem = null;
            this.CompanyVacationPolicyBindingNavigator.Size = new System.Drawing.Size(514, 25);
            this.CompanyVacationPolicyBindingNavigator.TabIndex = 18;
            this.CompanyVacationPolicyBindingNavigator.Text = "BindingNavigator1";
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveFirstItem
            // 
            this.bnMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            this.bnMoveFirstItem.Name = "bnMoveFirstItem";
            this.bnMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveFirstItem.Text = "Move first";
            this.bnMoveFirstItem.Click += new System.EventHandler(this.bnMoveFirstItem_Click);
            // 
            // bnPreviousItem
            // 
            this.bnPreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnPreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnPreviousItem.Image")));
            this.bnPreviousItem.Name = "bnPreviousItem";
            this.bnPreviousItem.RightToLeftAutoMirrorImage = true;
            this.bnPreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bnPreviousItem.Text = "Move previous";
            this.bnPreviousItem.Click += new System.EventHandler(this.bnPreviousItem_Click);
            // 
            // bnPositionItem
            // 
            this.bnPositionItem.AccessibleName = "Position";
            this.bnPositionItem.AutoSize = false;
            this.bnPositionItem.Enabled = false;
            this.bnPositionItem.MaxLength = 12;
            this.bnPositionItem.Name = "bnPositionItem";
            this.bnPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bnPositionItem.Text = "0";
            this.bnPositionItem.ToolTipText = "Current position";
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.Size = new System.Drawing.Size(35, 22);
            this.bnCountItem.Text = "of {0}";
            this.bnCountItem.ToolTipText = "Total number of items";
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Move next";
            this.bnMoveNextItem.Click += new System.EventHandler(this.bnMoveNextItem_Click);
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Move last";
            this.bnMoveLastItem.Click += new System.EventHandler(this.bnMoveLastItem_Click);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.RightToLeftAutoMirrorImage = true;
            this.btnAdd.Size = new System.Drawing.Size(23, 22);
            this.btnAdd.Text = "Add new";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save Data";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.CompanyVacationPolicyBindingNavigatorSaveItem_Click);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bnDeleteItem.Image")));
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.Text = "Delete";
            this.bnDeleteItem.Click += new System.EventHandler(this.bnDeleteItem_Click);
            // 
            // ToolStripSeparator6
            // 
            this.ToolStripSeparator6.Name = "ToolStripSeparator6";
            this.ToolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // bnClear
            // 
            this.bnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnClear.Image = ((System.Drawing.Image)(resources.GetObject("bnClear.Image")));
            this.bnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnClear.Name = "bnClear";
            this.bnClear.Size = new System.Drawing.Size(23, 22);
            this.bnClear.ToolTipText = "Clear";
            this.bnClear.Click += new System.EventHandler(this.bnClear_Click);
            // 
            // ToolStripSeparator7
            // 
            this.ToolStripSeparator7.Name = "ToolStripSeparator7";
            this.ToolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            this.ToolStripSeparator7.Visible = false;
            // 
            // bnPrint
            // 
            this.bnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnPrint.Image = ((System.Drawing.Image)(resources.GetObject("bnPrint.Image")));
            this.bnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(23, 22);
            this.bnPrint.ToolTipText = "Print";
            this.bnPrint.Visible = false;
            // 
            // bnEmail
            // 
            this.bnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnEmail.Image = ((System.Drawing.Image)(resources.GetObject("bnEmail.Image")));
            this.bnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Size = new System.Drawing.Size(23, 22);
            this.bnEmail.ToolTipText = "Email";
            this.bnEmail.Visible = false;
            // 
            // ToolStripSeparator8
            // 
            this.ToolStripSeparator8.Name = "ToolStripSeparator8";
            this.ToolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            this.ToolStripSeparator8.Visible = false;
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            this.lblStatus.ToolTipText = "Status";
            // 
            // StatusStripSettlement
            // 
            this.StatusStripSettlement.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.StatusStripSettlement.Location = new System.Drawing.Point(0, 460);
            this.StatusStripSettlement.Name = "StatusStripSettlement";
            this.StatusStripSettlement.Size = new System.Drawing.Size(514, 22);
            this.StatusStripSettlement.TabIndex = 17;
            this.StatusStripSettlement.Text = "StatusStrip1";
            // 
            // lblGrossVacation
            // 
            this.lblGrossVacation.AutoSize = true;
            this.lblGrossVacation.Location = new System.Drawing.Point(9, 78);
            this.lblGrossVacation.Name = "lblGrossVacation";
            this.lblGrossVacation.Size = new System.Drawing.Size(101, 13);
            this.lblGrossVacation.TabIndex = 51;
            this.lblGrossVacation.Text = "Exclude from Gross ";
            // 
            // lblPolicyName
            // 
            this.lblPolicyName.AutoSize = true;
            this.lblPolicyName.Location = new System.Drawing.Point(9, 23);
            this.lblPolicyName.Name = "lblPolicyName";
            this.lblPolicyName.Size = new System.Drawing.Size(66, 13);
            this.lblPolicyName.TabIndex = 21;
            this.lblPolicyName.Text = "Policy Name";
            // 
            // txtPolicyName
            // 
            this.txtPolicyName.BackColor = System.Drawing.SystemColors.Info;
            this.txtPolicyName.Location = new System.Drawing.Point(128, 15);
            this.txtPolicyName.MaxLength = 50;
            this.txtPolicyName.Name = "txtPolicyName";
            this.txtPolicyName.Size = new System.Drawing.Size(304, 20);
            this.txtPolicyName.TabIndex = 0;
            this.txtPolicyName.TextChanged += new System.EventHandler(this.txtPolicyName_TextChanged);
            // 
            // GrpCSP
            // 
            this.GrpCSP.Controls.Add(this.chkFullSalary);
            this.GrpCSP.Controls.Add(this.chkLeaveOnly);
            this.GrpCSP.Controls.Add(this.DetailsDataGridViewVacation);
            this.GrpCSP.Controls.Add(this.dgvVacation);
            this.GrpCSP.Controls.Add(this.lblOnTimeRejoinDateBenefit);
            this.GrpCSP.Controls.Add(this.txtOnTimeRejoinBenefit);
            this.GrpCSP.Controls.Add(LblPolicyDetails);
            this.GrpCSP.Controls.Add(this.lblCalcVacation);
            this.GrpCSP.Controls.Add(this.cboCalculationBasedVacation);
            this.GrpCSP.Controls.Add(this.lblGrossVacation);
            this.GrpCSP.Controls.Add(this.txtPolicyName);
            this.GrpCSP.Controls.Add(this.lblPolicyName);
            this.GrpCSP.Controls.Add(lblGeneral);
            this.GrpCSP.Controls.Add(this.ShapeContainer1);
            this.GrpCSP.Location = new System.Drawing.Point(9, 30);
            this.GrpCSP.Name = "GrpCSP";
            this.GrpCSP.Size = new System.Drawing.Size(498, 393);
            this.GrpCSP.TabIndex = 13;
            this.GrpCSP.TabStop = false;
            // 
            // chkFullSalary
            // 
            this.chkFullSalary.AutoSize = true;
            this.chkFullSalary.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkFullSalary.Location = new System.Drawing.Point(372, 208);
            this.chkFullSalary.Name = "chkFullSalary";
            this.chkFullSalary.Size = new System.Drawing.Size(107, 17);
            this.chkFullSalary.TabIndex = 63;
            this.chkFullSalary.Text = "Full Month Salary";
            this.chkFullSalary.UseVisualStyleBackColor = true;
            this.chkFullSalary.CheckedChanged += new System.EventHandler(this.chkFullSalary_CheckedChanged);
            // 
            // chkLeaveOnly
            // 
            this.chkLeaveOnly.AutoSize = true;
            this.chkLeaveOnly.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkLeaveOnly.Location = new System.Drawing.Point(247, 208);
            this.chkLeaveOnly.Name = "chkLeaveOnly";
            this.chkLeaveOnly.Size = new System.Drawing.Size(80, 17);
            this.chkLeaveOnly.TabIndex = 62;
            this.chkLeaveOnly.Text = "Leave Only";
            this.chkLeaveOnly.UseVisualStyleBackColor = true;
            this.chkLeaveOnly.CheckedChanged += new System.EventHandler(this.chkLeaveOnly_CheckedChanged);
            // 
            // DetailsDataGridViewVacation
            // 
            this.DetailsDataGridViewVacation.AddNewRow = false;
            this.DetailsDataGridViewVacation.AllowUserToAddRows = false;
            this.DetailsDataGridViewVacation.AllowUserToDeleteRows = false;
            this.DetailsDataGridViewVacation.AllowUserToResizeColumns = false;
            this.DetailsDataGridViewVacation.AllowUserToResizeRows = false;
            this.DetailsDataGridViewVacation.AlphaNumericCols = new int[0];
            this.DetailsDataGridViewVacation.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DetailsDataGridViewVacation.CapsLockCols = new int[0];
            this.DetailsDataGridViewVacation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DetailsDataGridViewVacation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtAddDedID,
            this.chkParticular,
            this.txtParticulars});
            this.DetailsDataGridViewVacation.DecimalCols = new int[0];
            this.DetailsDataGridViewVacation.HasSlNo = false;
            this.DetailsDataGridViewVacation.LastRowIndex = 0;
            this.DetailsDataGridViewVacation.Location = new System.Drawing.Point(128, 68);
            this.DetailsDataGridViewVacation.MultiSelect = false;
            this.DetailsDataGridViewVacation.Name = "DetailsDataGridViewVacation";
            this.DetailsDataGridViewVacation.NegativeValueCols = new int[0];
            this.DetailsDataGridViewVacation.NumericCols = new int[0];
            this.DetailsDataGridViewVacation.RowHeadersWidth = 35;
            this.DetailsDataGridViewVacation.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DetailsDataGridViewVacation.Size = new System.Drawing.Size(304, 122);
            this.DetailsDataGridViewVacation.TabIndex = 2;
            this.DetailsDataGridViewVacation.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DetailsDataGridViewVacation_CellValueChanged);
            this.DetailsDataGridViewVacation.CurrentCellDirtyStateChanged += new System.EventHandler(this.DetailsDataGridViewVacation_CurrentCellDirtyStateChanged);
            this.DetailsDataGridViewVacation.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DetailsDataGridViewVacation_DataError);
            // 
            // txtAddDedID
            // 
            this.txtAddDedID.HeaderText = "txtAddDedID";
            this.txtAddDedID.Name = "txtAddDedID";
            this.txtAddDedID.Visible = false;
            // 
            // chkParticular
            // 
            this.chkParticular.HeaderText = "";
            this.chkParticular.Name = "chkParticular";
            this.chkParticular.Width = 30;
            // 
            // txtParticulars
            // 
            this.txtParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtParticulars.HeaderText = "Particulars";
            this.txtParticulars.Name = "txtParticulars";
            this.txtParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvVacation
            // 
            this.dgvVacation.AllowUserToResizeColumns = false;
            this.dgvVacation.AllowUserToResizeRows = false;
            this.dgvVacation.BackgroundColor = System.Drawing.Color.White;
            this.dgvVacation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVacation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cboParameter,
            this.txtNoOfMonths,
            this.txtNoOfDays,
            this.txtNoOfTickets,
            this.txtTicketAmount,
            this.txtGratuityID});
            this.dgvVacation.Location = new System.Drawing.Point(4, 262);
            this.dgvVacation.Name = "dgvVacation";
            this.dgvVacation.RowHeadersWidth = 25;
            this.dgvVacation.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvVacation.Size = new System.Drawing.Size(488, 125);
            this.dgvVacation.TabIndex = 7;
            this.dgvVacation.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVacation_CellValueChanged);
            this.dgvVacation.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvVacation_UserDeletingRow);
            this.dgvVacation.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvVacation_CellBeginEdit);
            this.dgvVacation.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvVacation_UserDeletedRow);
            this.dgvVacation.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVacation_CellEndEdit);
            this.dgvVacation.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvVacation_EditingControlShowing);
            this.dgvVacation.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvVacation_CurrentCellDirtyStateChanged);
            this.dgvVacation.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvVacation_DataError);
            // 
            // cboParameter
            // 
            this.cboParameter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboParameter.HeaderText = "Parameter";
            this.cboParameter.Name = "cboParameter";
            this.cboParameter.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cboParameter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // txtNoOfMonths
            // 
            this.txtNoOfMonths.HeaderText = "No Of Months";
            this.txtNoOfMonths.MaxInputLength = 5;
            this.txtNoOfMonths.Name = "txtNoOfMonths";
            this.txtNoOfMonths.Width = 80;
            // 
            // txtNoOfDays
            // 
            this.txtNoOfDays.HeaderText = "Leave /Payable Days";
            this.txtNoOfDays.MaxInputLength = 10;
            this.txtNoOfDays.Name = "txtNoOfDays";
            // 
            // txtNoOfTickets
            // 
            this.txtNoOfTickets.HeaderText = "Tickets";
            this.txtNoOfTickets.MaxInputLength = 3;
            this.txtNoOfTickets.Name = "txtNoOfTickets";
            this.txtNoOfTickets.Width = 80;
            // 
            // txtTicketAmount
            // 
            this.txtTicketAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtTicketAmount.HeaderText = "Ticket Amount";
            this.txtTicketAmount.MaxInputLength = 10;
            this.txtTicketAmount.Name = "txtTicketAmount";
            // 
            // txtGratuityID
            // 
            this.txtGratuityID.HeaderText = "txtGratuityID";
            this.txtGratuityID.Name = "txtGratuityID";
            this.txtGratuityID.Visible = false;
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.ShapeContainer1.Size = new System.Drawing.Size(492, 374);
            this.ShapeContainer1.TabIndex = 20;
            this.ShapeContainer1.TabStop = false;
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 70;
            this.LineShape1.X2 = 486;
            this.LineShape1.Y1 = 233;
            this.LineShape1.Y2 = 233;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn1.Width = 30;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "AddDedID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Particulars";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "txtNoOfTickets";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "txtTicketAmount";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "txtGratuityID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // frmVacationPolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 482);
            this.Controls.Add(this.BtnBottomCancel);
            this.Controls.Add(this.OKSaveButton);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.CompanyVacationPolicyBindingNavigator);
            this.Controls.Add(this.StatusStripSettlement);
            this.Controls.Add(this.GrpCSP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmVacationPolicy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vacation Policy";
            this.Load += new System.EventHandler(this.frmVacationPolicy_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmVacationPolicy_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmVacationPolicy_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.CompanyVacationPolicyBindingNavigator)).EndInit();
            this.CompanyVacationPolicyBindingNavigator.ResumeLayout(false);
            this.CompanyVacationPolicyBindingNavigator.PerformLayout();
            this.StatusStripSettlement.ResumeLayout(false);
            this.StatusStripSettlement.PerformLayout();
            this.GrpCSP.ResumeLayout(false);
            this.GrpCSP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DetailsDataGridViewVacation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVacation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStripButton bnHelp;
        internal System.Windows.Forms.Label lblOnTimeRejoinDateBenefit;
        internal System.Windows.Forms.TextBox txtOnTimeRejoinBenefit;
        internal System.Windows.Forms.Button BtnBottomCancel;
        internal System.Windows.Forms.Button OKSaveButton;
        internal System.Windows.Forms.Button BtnSave;
        //internal System.Windows.Forms.DataGridViewComboBoxColumn cboParameter;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn txtNoOfMonths;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn txtNoOfDays;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn txtNoOfTickets;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn txtTicketAmount;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn txtGratuityID;
        internal System.Windows.Forms.Label lblCalcVacation;
        internal System.Windows.Forms.ComboBox cboCalculationBasedVacation;
        //internal System.Windows.Forms.DataGridViewCheckBoxColumn chkParticular;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn txtParticulars;
        internal System.Windows.Forms.BindingNavigator CompanyVacationPolicyBindingNavigator;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripButton bnMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton bnPreviousItem;
        internal System.Windows.Forms.ToolStripTextBox bnPositionItem;
        internal System.Windows.Forms.ToolStripLabel bnCountItem;
        internal System.Windows.Forms.ToolStripButton bnMoveNextItem;
        internal System.Windows.Forms.ToolStripButton bnMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripButton btnAdd;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator6;
        internal System.Windows.Forms.ToolStripButton bnClear;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator7;
        internal System.Windows.Forms.ToolStripButton bnPrint;
        internal System.Windows.Forms.ToolStripButton bnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator8;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.StatusStrip StatusStripSettlement;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn txtAddDedID;
        internal System.Windows.Forms.Timer tmrVacationPolicy;
        internal System.Windows.Forms.Label lblGrossVacation;
        internal System.Windows.Forms.Label lblPolicyName;
        internal System.Windows.Forms.TextBox txtPolicyName;
        internal System.Windows.Forms.GroupBox GrpCSP;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridView dgvVacation;
        private DemoClsDataGridview.ClsDataGirdView DetailsDataGridViewVacation;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtAddDedID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkParticular;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtParticulars;
        private System.Windows.Forms.DataGridViewComboBoxColumn cboParameter;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtNoOfMonths;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtNoOfDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtNoOfTickets;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtTicketAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtGratuityID;
        private System.Windows.Forms.CheckBox chkLeaveOnly;
        private System.Windows.Forms.CheckBox chkFullSalary;
    }
}