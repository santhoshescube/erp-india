﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

namespace MyBooksERP
{
    /*****************************************************
    * Created By       : Arun
    * Creation Date    : 5 Apr 2012
    * Description      : Handle Leave Policy
    * Modified by      : Siny
    * Modified date    : 13 Aug 2013
    * Description      : Tuning performance improving  
    * FormID           : 105 
    * ***************************************************/
    public partial class FrmLeavePolicy : Form
    {
        #region Declartions
        public int PintLeavePolicyID = 0;                     // From Refernce Form 
        public int PintCompanyID = 0;
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;               //To set Add Permission
        private bool MblnUpdatePermission = false;            //To set Update Permission
        private bool MblnDeletePermission = false;            //To set Delete Permission

        private bool MblnChangeStatus = false;
        private bool MblnPolicyExistance = false;             //Check if policy details exists in the system
        private string MstrCommonMessage;
        private bool MblnIsEditMode = false;
        private int MintRecordCnt = 0;
        private int MintCurrentRecCnt = 0;
        private bool bNav = false;
        private clsBLLLeavePolicy MobjclsBLLLeavePolicy = null;
        private clsMessage ObjUserMessage = null;
        ClsLogWriter MObjClsLogWriter;
        string strBindingOf = "Of ";
        #endregion Declartions

        #region Constructor
        public FrmLeavePolicy()
        {
            InitializeComponent();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);   // Application startup path

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.LeavePolicy, this);

        //    strBindingOf = "من ";
        //    dgvLeaveDetail.Columns["colLeaveType"].HeaderText = "ترك نوع";
        //    dgvLeaveDetail.Columns["colNoOfLeave"].HeaderText = "أيام";
        //    dgvLeaveDetail.Columns["colMonthLeave"].HeaderText = "شهريا";
        //    dgvLeaveDetail.Columns["colCarryForwardLeave"].HeaderText = "حمل إلى الأمام";
        //    dgvLeaveDetail.Columns["colEncashDays"].HeaderText = "يحققوا ربحا";
        //    dgvLeaveConsequence.Columns["colOrderNo"].HeaderText = "الأمر رقم";
        //    dgvLeaveConsequence.Columns["colLeaveTypeID"].HeaderText = "ترك نوع";
        //    dgvLeaveConsequence.Columns["colCalculationBasedID"].HeaderText = "حساب بناء";
        //    dgvLeaveConsequence.Columns["colDeductionPercentage"].HeaderText = "خصم نسبة";
        //    dgvLeaveConsequence.Columns["colApplicableDays"].HeaderText = "يحققوا ربحا";
        //}
        #endregion Constructor

        #region properties
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.LeavePolicy);
                return this.ObjUserMessage;
            }
        }
        private clsBLLLeavePolicy BLLLeavePolicy
        {
            get
            {
                if (this.MobjclsBLLLeavePolicy == null)
                    this.MobjclsBLLLeavePolicy = new clsBLLLeavePolicy();

                return this.MobjclsBLLLeavePolicy;
            }
        }
        #endregion properties
        
        #region Methods

        #region SetPermissions
        /// <summary>
        /// Set permissions for add,update,delete,print/email
        /// </summary>
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.LeavePolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }
        #endregion SetPermissions

        #region LoadCombos
        /// <summary>
        /// Loads the combos according to intType
        /// </summary>
        /// <param name="intType">
        /// intType=0 -> Fill all combos
        /// intType=2 -> Fills Leave details leave type combo(colLeaveType)
        /// intType=3 -> Fills Cosequence LeaveType combo(colLeaveTypeID)
        /// intType=4 -> Fills FinYear Combo
        /// intType=5 -> Fills Calculation basedon Combo in consequences(colCalculationBasedID)
        /// </param>
        /// <returns>success/failure</returns>
        private bool LoadCombos(int intType)
        {
            if (bNav == true)
            {
                intType = 0;
            }
            bool blnRetvalue = false;
            DataTable datCombos = new DataTable();
            try
            {
                if (intType == 0 || intType == 2)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    datCombos = BLLLeavePolicy.FillCombos(new string[] { "LeaveTypeID,LeaveTypeArb AS LeaveType", "PayLeaveTypeReference", "LeaveTypeID!=5" });
                    //else
                        datCombos = BLLLeavePolicy.FillCombos(new string[] { "LeaveTypeID,LeaveType", "PayLeaveTypeReference", "LeaveTypeID!=5" });
                    colLeaveType.ValueMember = "LeaveTypeID";
                    colLeaveType.DisplayMember = "LeaveType";
                    colLeaveType.DataSource = datCombos;
                    blnRetvalue = true;
                }
                if (intType == 0 || intType == 3)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    datCombos = BLLLeavePolicy.FillCombos(new string[] { "LeaveTypeID,LeaveTypeArb AS LeaveType", "PayLeaveTypeReference", "LeaveTypeID!=5" });
                    //else
                        datCombos = BLLLeavePolicy.FillCombos(new string[] { "LeaveTypeID,LeaveType", "PayLeaveTypeReference", "LeaveTypeID!=5" });
                    colLeaveTypeID.ValueMember = "LeaveTypeID";
                    colLeaveTypeID.DisplayMember = "LeaveType";
                    colLeaveTypeID.DataSource = datCombos;
                    blnRetvalue = true;
                }

                if (intType == 0 || intType == 4)
                {
                    CboFinYear.Items.Clear();
                    datCombos = BLLLeavePolicy.FillCombos(new string[] { "DISTINCT Convert(Varchar(12),FinYearStartDate,106) as FinYearStartDate", "CompanyMaster", "CompanyID=" + ClsCommonSettings.LoginCompanyID + " ORDER BY FinYearStartDate ASC" });
                    if (datCombos.Rows.Count > 0)
                    {
                        for (int iCounter = Convert.ToDateTime(datCombos.Rows[0][0]).Year; iCounter <= DateTime.Now.Year + 1; iCounter++)
                        {

                            CboFinYear.Items.Add(new clsListItem(datCombos.Rows[0][0], Convert.ToDateTime(datCombos.Rows[0][0]).ToString("dd-MMM-yyyy").Replace("-", " ")));
                            datCombos.Rows[0][0] = Convert.ToDateTime(datCombos.Rows[0][0]).AddYears(1).ToString("dd MMM yyyy");
                        }
                    }
                    datCombos = null;
                    blnRetvalue = true;
                }
                if (intType == 0 || intType == 5)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    datCombos = BLLLeavePolicy.FillCombos(new string[] { "CalculationID,CalculationArb AS Calculation", "PayCalculationReference", "" });
                    //else
                        datCombos = BLLLeavePolicy.FillCombos(new string[] { "CalculationID,Calculation", "PayCalculationReference", "" });
                    colCalculationBasedID.ValueMember = "CalculationID";
                    colCalculationBasedID.DisplayMember = "Calculation";
                    colCalculationBasedID.DataSource = datCombos;
                    blnRetvalue = true;
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
                blnRetvalue = false;
            }
            return blnRetvalue;
        }
        #endregion LoadCombos

        #region ClearAllControls
        /// <summary>
        /// Clear all the controls
        /// </summary>
        private void ClearAllControls()
        {
            CboFinYear.Enabled = true;
            this.txtPolicyName.Text = "";
            this.txtPolicyName.Tag = 0;
            this.CboFinYear.SelectedIndex = -1;
            this.CboFinYear.Text = "";
            this.dgvLeaveConsequence.Rows.Clear();
            this.dgvLeaveDetail.Rows.Clear();
            this.dgvLeaveDetail.RowCount = 1;
            dgvLeaveConsequence.RowCount = 1;
            tbLeave.SelectedTab = tbpgLeaveTypes;
            MblnPolicyExistance = false;
        }
        #endregion ClearAllControls

        #region GetRecordCount
        /// <summary>
        /// Get total record count and sets to property MintRecordCnt
        /// </summary>
        private void GetRecordCount()
        {
            MintRecordCnt = 0;
            MintRecordCnt = BLLLeavePolicy.GetRecordCount();
            if (MintRecordCnt < 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                MintRecordCnt = 0;
            }
        }
        #endregion GetRecordCount

        #region RefernceDisplay
        /// <summary>
        /// Display policy information with respect to the policyID provided(from other forms)
        /// </summary>
        private void RefernceDisplay()
        {
            int intRowNum = 0;
            intRowNum = BLLLeavePolicy.GetRowNumber(PintLeavePolicyID);
            if (intRowNum > 0)
            {
                MintCurrentRecCnt = intRowNum;
                DisplayLeavePolicyInfo();
            }
        }
        #endregion RefernceDisplay

        #region DisplayLeavePolicyInfo
        /// <summary>
        /// displaying policy info and setting enability of buttons
        /// </summary>
        private void DisplayLeavePolicyInfo()
        {
            try
            {
                FillLeavePolicyInfo();
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                btnPrint.Enabled = MblnPrintEmailPermission;
                btnEmail.Enabled = MblnPrintEmailPermission;
                BindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                if (MblnPolicyExistance)
                {
                    CboFinYear.Enabled=false;
                }
                else
                {
                    CboFinYear.Enabled=true;
                }
                
                
                MblnIsEditMode = true;
                SetBindingNavigatorButtons();
                MblnChangeStatus = false;
                btnSave.Enabled  = btnOk.Enabled = BindingNavigatorSaveItem.Enabled = MblnChangeStatus;
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
        }
        #endregion DisplayLeavePolicyInfo

        #region FillLeavePolicyInfo
        /// <summary>
        /// Fill information of a particular policy
        /// </summary>
        private void FillLeavePolicyInfo()
        {
            ClearAllControls();
            bNav = true;
            if (BLLLeavePolicy.DisplayLeavePolicyMaster(MintCurrentRecCnt))
            {
                txtPolicyName.Tag = BLLLeavePolicy.DTOLeavePolicy.LeavePolicyID;
                txtPolicyName.Text = BLLLeavePolicy.DTOLeavePolicy.LeavePolicyName;
                LoadCombos(4);//Fill Finacial Year Combo
                int index = -1;
                string strAppFrom = BLLLeavePolicy.DTOLeavePolicy.ApplicableFrom;
                string strAppFromTemp = "";
                for (int iCounter = 0; iCounter <= CboFinYear.Items.Count - 1; iCounter++)
                {
                   strAppFromTemp=Convert.ToString(((clsListItem)CboFinYear.Items[iCounter]).Value);

                   if (strAppFromTemp.Trim()== strAppFrom.Trim())
                    {
                        index = iCounter;
                        break;
                    }
                }
                CboFinYear.SelectedIndex = index;
                FillLeavePolicyDetailInfo();//Fill Details and Consequence
            }
            bNav = false;
        }
        #endregion FillLeavePolicyInfo

        #region FillLeavePolicyDetailInfo
        /// <summary>
        /// Fill Leave details and leave consequences grids
        /// </summary>
        private void FillLeavePolicyDetailInfo()
        {
            DataTable dtLeavePolicyDetail = MobjclsBLLLeavePolicy.DispalyPolicyDetail(Convert.ToInt32(txtPolicyName.Tag));
            dgvLeaveDetail.RowCount = 1;
           // MblnPolicyExistance = MobjclsBLLLeavePolicy.PolicyIDExists(txtPolicyName.Tag.ToInt32());
            if (dtLeavePolicyDetail != null && dtLeavePolicyDetail.Rows.Count > 0)
            {
                for (int iCounter = 0; iCounter <= dtLeavePolicyDetail.Rows.Count - 1; iCounter++)
                {
                    dgvLeaveDetail.RowCount = dgvLeaveDetail.RowCount + 1;
                    dgvLeaveDetail.Rows[iCounter].Cells["colSerialNo"].Value = dtLeavePolicyDetail.Rows[iCounter]["SerialNo"].ToInt32();
                    dgvLeaveDetail.Rows[iCounter].Cells["colLeaveType"].Value = dtLeavePolicyDetail.Rows[iCounter]["LeaveTypeID"].ToInt32();
                    dgvLeaveDetail.Rows[iCounter].Cells["colNoOfLeave"].Value = dtLeavePolicyDetail.Rows[iCounter]["NoOfLeave"].ToDecimal();
                    dgvLeaveDetail.Rows[iCounter].Cells["colMonthLeave"].Value = dtLeavePolicyDetail.Rows[iCounter]["MonthLeave"].ToDecimal();
                    if (dtLeavePolicyDetail.Rows[iCounter]["LeaveTypeID"].ToInt32() == 3)
                    {
                        dgvLeaveDetail.Rows[iCounter].Cells["colMonthLeave"].ReadOnly = true;
                        dgvLeaveDetail.Rows[iCounter].Cells["colCarryForwardLeave"].ReadOnly = true;
                        dgvLeaveDetail.Rows[iCounter].Cells["colEncashDays"].ReadOnly = true;
                    }
                    else
                    {
                        dgvLeaveDetail.Rows[iCounter].Cells["colMonthLeave"].ReadOnly = false;
                        dgvLeaveDetail.Rows[iCounter].Cells["colCarryForwardLeave"].ReadOnly = false;
                        dgvLeaveDetail.Rows[iCounter].Cells["colEncashDays"].ReadOnly = false;
                    }
                    dgvLeaveDetail.Rows[iCounter].Cells["colCarryForwardLeave"].Value = dtLeavePolicyDetail.Rows[iCounter]["CarryForwardLeave"].ToDecimal();
                    dgvLeaveDetail.Rows[iCounter].Cells["colEncashDays"].Value = dtLeavePolicyDetail.Rows[iCounter]["EncashDays"].ToDecimal();
                    dgvLeaveDetail.Rows[iCounter].Cells["colNoofleavesOriginal"].Value = dtLeavePolicyDetail.Rows[iCounter]["NoOfLeave"].ToDecimal();
                    dgvLeaveDetail.Rows[iCounter].Cells["LeaveTypeExistance"].Value = dtLeavePolicyDetail.Rows[iCounter]["LeaveTypeExistance"].ToBoolean();
                    dgvLeaveDetail.Rows[iCounter].Cells["colStatus"].Value = 0;
                    if (dtLeavePolicyDetail.Rows[iCounter]["LeaveTypeExistance"].ToBoolean())
                    {
                        MblnPolicyExistance = true;
                        dgvLeaveDetail.Rows[iCounter].ReadOnly = true;
                    }
                    else if (dtLeavePolicyDetail.Rows[iCounter]["LeaveTypeID"].ToInt32() != 3)
                        dgvLeaveDetail.Rows[iCounter].ReadOnly = false;

                }
            }
            dtLeavePolicyDetail = null;
            dtLeavePolicyDetail = BLLLeavePolicy.DispalyPolicyConsequenceDetail(txtPolicyName.Tag.ToInt32());
            dgvLeaveConsequence.RowCount = 1;
            if (dtLeavePolicyDetail != null && dtLeavePolicyDetail.Rows.Count > 0)
            {
                for (int iCounter = 0; iCounter <= dtLeavePolicyDetail.Rows.Count - 1; iCounter++)
                {
                    dgvLeaveConsequence.RowCount = dgvLeaveConsequence.RowCount + 1;
                    dgvLeaveConsequence.Rows[iCounter].Cells["colLeaveConsequenceID"].Value = dtLeavePolicyDetail.Rows[iCounter]["SerialNo"].ToInt32();
                    dgvLeaveConsequence.Rows[iCounter].Cells["colOrderNo"].Value = dtLeavePolicyDetail.Rows[iCounter]["OrderNo"].ToInt32();
                    dgvLeaveConsequence.Rows[iCounter].Cells["colLeaveTypeID"].Value = dtLeavePolicyDetail.Rows[iCounter]["LeaveTypeID"].ToInt32();
                    dgvLeaveConsequence.Rows[iCounter].Cells["colCalculationBasedID"].Value = dtLeavePolicyDetail.Rows[iCounter]["CalculationID"].ToInt32();
                    dgvLeaveConsequence.Rows[iCounter].Cells["colDeductionPercentage"].Value = dtLeavePolicyDetail.Rows[iCounter]["CalculationPercentage"].ToDecimal();
                    dgvLeaveConsequence.Rows[iCounter].Cells["colApplicableDays"].Value = dtLeavePolicyDetail.Rows[iCounter]["ApplicableDays"].ToDecimal();
                }
            }
            btnClear.Enabled = false;
        }
        #endregion FillLeavePolicyDetailInfo

        #region SaveLeavePolicy
        /// <summary>
        /// Save Leave policy master,details,consequences
        /// </summary>
        /// <returns></returns>
        private bool SaveLeavePolicy()
        {
            bool blnRetValue = false;
            try
            {
                if (FormValidation())
                {
                    int intMessageCode = 0;
                    if (txtPolicyName.Tag.ToInt32() > 0)
                        intMessageCode = 601;//Do you wish to update leave policy information?
                    else
                        intMessageCode = 600;//Do you wish to save leave policy information?

                    if (UserMessage.ShowMessage(intMessageCode))
                    {
                        FillParameterMaster();
                        FillParameterDetails();
                        FillParameterConsequenceDetails();
                        if (BLLLeavePolicy.LeavePolicyMasterSave())
                        {
                            blnRetValue = true;
                            txtPolicyName.Tag = MobjclsBLLLeavePolicy.DTOLeavePolicy.LeavePolicyID;
                            PintLeavePolicyID = MobjclsBLLLeavePolicy.DTOLeavePolicy.LeavePolicyID;
                            if (!MblnIsEditMode)
                            {
                                UserMessage.ShowMessage(2, null);
                                lblStatus.Text = UserMessage.GetMessageByCode(2);
                                MintCurrentRecCnt = MintCurrentRecCnt + 1;
                                BindingNavigatorMoveLastItem_Click(null, null);
                                lblStatus.Text = "";
                            }
                            else
                            {
                                UserMessage.ShowMessage(21, null);
                                lblStatus.Text = UserMessage.GetMessageByCode(21);
                                btnOk.Enabled = btnSave.Enabled = BindingNavigatorSaveItem.Enabled = false;
                            }
                            
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
                blnRetValue = false;
            }
            return blnRetValue;

        }
        #endregion SaveLeavePolicy

        #region FillParameterMaster
        /// <summary>
        /// Fill master dto with values
        /// </summary>
        private void FillParameterMaster()
        {
            BLLLeavePolicy.DTOLeavePolicy.LeavePolicyID = txtPolicyName.Tag.ToInt32();
            BLLLeavePolicy.DTOLeavePolicy.LeavePolicyName = txtPolicyName.Text.Trim();
            BLLLeavePolicy.DTOLeavePolicy.ApplicableFrom = ((clsListItem)CboFinYear.SelectedItem).Value.ToString();
            BLLLeavePolicy.DTOLeavePolicy.ApplicableFrom = Convert.ToDateTime(BLLLeavePolicy.DTOLeavePolicy.ApplicableFrom).ToString("dd MMM yyyy");
        }
        #endregion FillParameterMaster

        #region FillParameterDetails
        /// <summary>
        ///  Fill DTO parameters of policy details with values
        /// </summary>
        private void FillParameterDetails()
        {
            if (dgvLeaveDetail.Rows.Count > 0)
            {
                BLLLeavePolicy.DTOLeavePolicy.DTOLeavePolicyDetail = new List<clsDTOLeavePolicyDetail>();
                for (int intICounter = 0; intICounter < dgvLeaveDetail.Rows.Count - 1; intICounter++)
                {
                    clsDTOLeavePolicyDetail objLeavePolicyDetail = new clsDTOLeavePolicyDetail();
                    objLeavePolicyDetail.SerialNo = dgvLeaveDetail.Rows[intICounter].Cells["colSerialNo"].Value.ToInt32();
                    objLeavePolicyDetail.LeaveTypeID = dgvLeaveDetail.Rows[intICounter].Cells["colLeaveType"].Value.ToInt32();
                    objLeavePolicyDetail.NoOfLeave = dgvLeaveDetail.Rows[intICounter].Cells["colNoOfLeave"].Value.ToDecimal();
                    objLeavePolicyDetail.MonthLeave = dgvLeaveDetail.Rows[intICounter].Cells["colMonthLeave"].Value.ToDecimal();
                    objLeavePolicyDetail.CarryForwardLeave = dgvLeaveDetail.Rows[intICounter].Cells["colCarryForwardLeave"].Value.ToDecimal();
                    objLeavePolicyDetail.EncashDays = dgvLeaveDetail.Rows[intICounter].Cells["colEncashDays"].Value.ToDecimal();
                    BLLLeavePolicy.DTOLeavePolicy.DTOLeavePolicyDetail.Add(objLeavePolicyDetail);
                }
            }
        }
        #endregion FillParameterDetails

        #region FillParameterConsequenceDetails
        /// <summary>
        /// Fill DTO parameters of consequences with values
        /// </summary>
        private void FillParameterConsequenceDetails()
        {
            if (dgvLeaveConsequence.RowCount > 0)
            {
                BLLLeavePolicy.DTOLeavePolicy.DTOLeavePolicyConsequenceDetail = new List<clsDTOLeavePolicyConsequenceDetail>();
                for (int intICounter = 0; intICounter < dgvLeaveConsequence.Rows.Count - 1; intICounter++)
                {
                    clsDTOLeavePolicyConsequenceDetail objLeavePolicyConSeqDetail = new clsDTOLeavePolicyConsequenceDetail();
                    objLeavePolicyConSeqDetail.SerialNo = dgvLeaveConsequence.Rows[intICounter].Cells["colLeaveConsequenceID"].Value.ToInt32();
                    objLeavePolicyConSeqDetail.LeaveTypeID = dgvLeaveConsequence.Rows[intICounter].Cells["colLeaveTypeID"].Value.ToInt32();
                    objLeavePolicyConSeqDetail.CalculationID = dgvLeaveConsequence.Rows[intICounter].Cells["colCalculationBasedID"].Value.ToInt32();
                    objLeavePolicyConSeqDetail.CalculationPercentage = dgvLeaveConsequence.Rows[intICounter].Cells["colDeductionPercentage"].Value.ToDecimal();
                    objLeavePolicyConSeqDetail.ApplicableDays = dgvLeaveConsequence.Rows[intICounter].Cells["colApplicableDays"].Value.ToDecimal();
                    objLeavePolicyConSeqDetail.OrderNo = dgvLeaveConsequence.Rows[intICounter].Cells["colOrderNo"].Value.ToInt32();
                    BLLLeavePolicy.DTOLeavePolicy.DTOLeavePolicyConsequenceDetail.Add(objLeavePolicyConSeqDetail);
                }
            }
        }
        #endregion FillParameterConsequenceDetails

        #region DeleteLeavePolicy
        /// <summary>
        /// Delete Leave Policy according to LeavePolicyID
        /// </summary>
        /// <returns>success/failure as boolean</returns>
        private bool DeleteLeavePolicy()
        {
            bool blnRetValue = false;
            try
            {
                if (DeleteValidation(false))
                {
                    BLLLeavePolicy.DTOLeavePolicy.LeavePolicyID = txtPolicyName.Tag.ToInt32();
                    if (BLLLeavePolicy.DeleteLeavePolicy())
                    {
                        blnRetValue = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
                blnRetValue = false;
            }
            return blnRetValue;
        }
        #endregion DeleteLeavePolicy

        #region FormValidation
        /// <summary>
        /// Validations before saving policy
        /// </summary>
        /// <returns>success/failure as boolean</returns>
        private bool FormValidation()
        {
            int TempID = -1;           
            if (CboFinYear.SelectedIndex == -1)//select fin year
            {
                errProLeavePolicy.SetError(CboFinYear, UserMessage.GetMessageByCode(606));
                UserMessage.ShowMessage(606,null);
                lblStatus.Text = UserMessage.GetMessageByCode(606);
                tmrClear.Enabled = false;
                CboFinYear.Focus();
                return false;
            }
            if (txtPolicyName.Text.Trim() == "")//enter policy name
            {
                errProLeavePolicy.SetError(txtPolicyName, UserMessage.GetMessageByCode(604));
                UserMessage.ShowMessage(604);
                lblStatus.Text = UserMessage.GetMessageByCode(604);
                tmrClear.Enabled = false;
                txtPolicyName.Focus();
                return false;
            }
            if (CboFinYear.Text == "")//select an Applicable Date
            {
                errProLeavePolicy.SetError(CboFinYear, UserMessage.GetMessageByCode(606));
                UserMessage.ShowMessage(606,null);
                lblStatus.Text = UserMessage.GetMessageByCode(606);
                tmrClear.Enabled = false;
                CboFinYear.Focus();
                return false;
            }
            //Duplicate policy name
            if (BLLLeavePolicy.CheckDuplicatePolicyName(txtPolicyName.Tag.ToInt32(), txtPolicyName.Text.Trim()))
            {
                errProLeavePolicy.SetError(txtPolicyName, UserMessage.GetMessageByCode(607));
                UserMessage.ShowMessage(607,null);
                lblStatus.Text = UserMessage.GetMessageByCode(607);
                tmrClear.Enabled = false;
                txtPolicyName.Focus();
                return false;
            }
            //Salary processed without release
            if (txtPolicyName.Tag.ToInt32() > 0 && BLLLeavePolicy.CheckSalaryProcessed(txtPolicyName.Tag.ToInt32()))
            {

                UserMessage.ShowMessage(608,null);
                lblStatus.Text = UserMessage.GetMessageByCode(608);
                tmrClear.Enabled = false;
                txtPolicyName.Focus();
                tbLeave.SelectedTab = tbpgLeaveTypes;
                return false;

            }
            if (dgvLeaveDetail.RowCount > 1)
            {
                for (int i = 0; i < dgvLeaveDetail.RowCount - 1; i++)
                {
                    dgvLeaveDetail.CurrentCell = dgvLeaveDetail["colLeaveType", 0];

                    if (dgvLeaveDetail.Rows[i].Cells["colLeaveType"].Value.ToStringCustom() == string.Empty)
                    {
                        //Please select the 'Leave type'.
                        UserMessage.ShowMessage(609,null);
                        lblStatus.Text = UserMessage.GetMessageByCode(609);
                        tmrClear.Enabled = false;
                        dgvLeaveDetail.CurrentCell = dgvLeaveDetail["colLeaveType", i];
                        tbLeave.SelectedTab = tbpgLeaveTypes;
                        return false;
                    }
                    if (dgvLeaveDetail.Rows[i].Cells["colNoOfLeave"].Value.ToStringCustom() == string.Empty)
                    {
                        //Please enter the number of days.
                        UserMessage.ShowMessage(610,null);
                        lblStatus.Text = UserMessage.GetMessageByCode(610);
                        tmrClear.Enabled = false;
                        dgvLeaveDetail.CurrentCell = dgvLeaveDetail["colNoOfLeave", i];
                        tbLeave.SelectedTab = tbpgLeaveTypes;
                        return false;
                    }

                    if (dgvLeaveDetail.Rows[i].Cells["colMonthLeave"].Value.ToStringCustom() == string.Empty)
                        dgvLeaveDetail.Rows[i].Cells["colMonthLeave"].Value = 0;


                    if (dgvLeaveDetail.Rows[i].Cells["colNoOfLeave"].Value.ToStringCustom() == string.Empty)
                        dgvLeaveDetail.Rows[i].Cells["colNoOfLeave"].Value = 0;


                    if (dgvLeaveDetail.Rows[i].Cells["colNoofleavesOriginal"].Value.ToStringCustom() == string.Empty)
                        dgvLeaveDetail.Rows[i].Cells["colNoofleavesOriginal"].Value = 0;


                    if (dgvLeaveDetail.Rows[i].Cells["colStatus"].Value.ToStringCustom() == string.Empty)
                        dgvLeaveDetail.Rows[i].Cells["colStatus"].Value = 0;


                    if (dgvLeaveDetail.Rows[i].Cells["colCarryForwardLeave"].Value.ToStringCustom() == string.Empty)
                        dgvLeaveDetail.Rows[i].Cells["colCarryForwardLeave"].Value = 0;


                    if (dgvLeaveDetail.Rows[i].Cells["colNoOfLeave"].Value.ToDecimal() == 0)
                    {
                        //Please enter the number of leaves.
                        UserMessage.ShowMessage(611,null);
                        lblStatus.Text = UserMessage.GetMessageByCode(611);
                        tmrClear.Enabled = false;
                        dgvLeaveDetail.CurrentCell = dgvLeaveDetail["colNoOfLeave", i];
                        tbLeave.SelectedTab = tbpgLeaveTypes;
                        return false;
                    }
                    if (dgvLeaveDetail.Rows[i].Cells["colMonthLeave"].Value.ToDouble() > 31)
                    {
                        //Monthly Leave cannot be greater than 31.
                        UserMessage.ShowMessage(612,null);
                        lblStatus.Text = UserMessage.GetMessageByCode(612);
                        tmrClear.Enabled = false;
                        dgvLeaveDetail.CurrentCell = dgvLeaveDetail["colMonthLeave", i];
                        tbLeave.SelectedTab = tbpgLeaveTypes;
                        return false;
                    }


                    if (dgvLeaveDetail.Rows[i].Cells["colLeaveType"].Value.ToDouble() != 6 && dgvLeaveDetail.Rows[i].Cells["colMonthLeave"].Value.ToDouble() > dgvLeaveDetail.Rows[i].Cells["colNoOfLeave"].Value.ToDouble())
                    {
                        //Monthly cannot be greater than Days
                        UserMessage.ShowMessage(613,null);
                        lblStatus.Text = UserMessage.GetMessageByCode(613);
                        tmrClear.Enabled = false;
                        dgvLeaveDetail.CurrentCell = dgvLeaveDetail["colMonthLeave", i];
                        tbLeave.SelectedTab = tbpgLeaveTypes;
                        return false;
                    }


                    if (dgvLeaveDetail.Rows[i].Cells["colCarryForwardLeave"].Value.ToDouble() + dgvLeaveDetail.Rows[i].Cells["colEncashDays"].Value.ToDouble() > dgvLeaveDetail.Rows[i].Cells["colNoOfLeave"].Value.ToDouble())
                    {
                        //The sum of carryforward leaves and enchashable days cannot be greater than the total no of leaves avaliable.
                        UserMessage.ShowMessage(614,null);
                        dgvLeaveDetail.CurrentCell = dgvLeaveDetail["colCarryForwardLeave", i];
                        lblStatus.Text = UserMessage.GetMessageByCode(614);
                        tmrClear.Enabled = false;
                        tbLeave.SelectedTab = tbpgLeaveTypes;
                        return false;
                    }

                    //if (txtPolicyName.Tag.ToInt32() > 0)
                    //{
                    //    if (dgvLeaveDetail.Rows[i].Cells["colLeaveType"].Value.ToInt32() != 1 && dgvLeaveDetail.Rows[i].Cells["colStatus"].Value.ToInt32() != 0)
                    //    {

                    //        if (BLLLeavePolicy.CheckPolicyUpdation(txtPolicyName.Tag.ToInt32(), ClsCommonSettings.CompanyID, dgvLeaveDetail.Rows[i].Cells["colLeaveType"].Value.ToInt32(),
                    //                    dgvLeaveDetail.Rows[i].Cells["colEncashDays"].Value.ToDecimal(), dgvLeaveDetail.Rows[i].Cells["colCarryForwardLeave"].Value.ToDecimal(), dgvLeaveDetail.Rows[i].Cells["colNoOfLeave"].Value.ToDecimal(), dgvLeaveDetail.Rows[i].Cells["colMonthLeave"].Value.ToDecimal()))
                    //        {
                    //            //Policy updation not permitted
                    //            UserMessage.ShowMessage(615,null,2);
                    //            lblStatus.Text = UserMessage.GetMessageByCode(615, 2);
                    //            tmrClear.Enabled = false;
                    //            tbLeave.SelectedTab = tbpgLeaveTypes;
                    //            return false;
                    //        }
                    //    }
                    //}
                }
                TempID = -1;
                TempID = CheckDuplicateInGrid(dgvLeaveDetail, colLeaveType.Index);
                if (TempID != -1)
                {
                    //Duplicate Value In Leave Type
                    UserMessage.ShowMessage(618, null);
                    lblStatus.Text = UserMessage.GetMessageByCode(618);
                    tmrClear.Enabled = false;
                    dgvLeaveDetail.CurrentCell = dgvLeaveDetail[colLeaveType.Index, TempID];
                    dgvLeaveDetail.Focus();
                    tbLeave.SelectedTab = tbpgLeaveTypes;
                    return false;
                }
            }
            else
            {
                //Please select the ' Leave Detail'.
                UserMessage.ShowMessage(616,null);
                lblStatus.Text = UserMessage.GetMessageByCode(616);
                tmrClear.Enabled = false;
                tbLeave.SelectedTab = tbpgLeaveTypes;
                return false;
            }
            for (int j = 0; j <= dgvLeaveConsequence.RowCount - 2; j++)
            {
                dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence["colOrderNo", 0];
                if (dgvLeaveConsequence.Rows[j].Cells["colOrderNo"].Value.ToInt32() == 0)
                {
                    //Please select the ' Leave Detail'.
                    UserMessage.ShowMessage(616,null);
                    lblStatus.Text = UserMessage.GetMessageByCode(616);
                    tmrClear.Enabled = false;
                    dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence["colOrderNo", j];
                    tbLeave.SelectedTab = tbpgConsequenceTypes;
                    return false;
                }
                    if (dgvLeaveConsequence.Rows[j].Cells["colLeaveTypeID"].Value.ToStringCustom() == string.Empty)
                    {
                        dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[2, j];
                        MstrCommonMessage = UserMessage.GetMessageByCode(623);//select leave type
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[2].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblStatus.Text = MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[2].HeaderText.ToString();
                        tbLeave.SelectedTab = tbpgConsequenceTypes;
                        tmrClear.Enabled = false;
                        return false;
                    }
                    if (dgvLeaveConsequence.Rows[j].Cells["colCalculationBasedID"].Value.ToStringCustom() == string.Empty)
                    {
                        dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[3, j];
                        MstrCommonMessage = UserMessage.GetMessageByCode(623);//select calculation basedon
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[3].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblStatus.Text = MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[3].HeaderText.ToString();
                        tbLeave.SelectedTab = tbpgConsequenceTypes;
                        tmrClear.Enabled = false;
                        return false;
                    }
                    if (dgvLeaveConsequence.Rows[j].Cells["colApplicableDays"].Value.ToStringCustom() == string.Empty || dgvLeaveConsequence.Rows[j].Cells["colApplicableDays"].Value.ToStringCustom() == "0")
                    {
                        dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[5, j];
                        MstrCommonMessage = UserMessage.GetMessageByCode(628);//enter applicable days
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[5].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblStatus.Text = MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[5].HeaderText.ToString();
                        tbLeave.SelectedTab = tbpgConsequenceTypes;
                        tmrClear.Enabled = false;
                        return false;
                    }
            }

            TempID = CheckDuplicateInGrid(dgvLeaveConsequence, 1, 2);
            if (TempID != -1 && dgvLeaveConsequence.RowCount - 2 >= 0)
            {
                //Duplicate Value In Order
                MstrCommonMessage = UserMessage.GetMessageByCode(618);
                MessageBox.Show(MstrCommonMessage.Trim() + "  order", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblStatus.Text = UserMessage.GetMessageByCode(618);
                tmrClear.Enabled = false;
                dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[colOrderNo.Index, TempID];
                tbLeave.SelectedTab = tbpgConsequenceTypes;
                dgvLeaveConsequence.Focus();
                return false;
            }         
            int iRowIndex = -1;
            if (IsValidLeaveInConsequence(iRowIndex) == false && dgvLeaveConsequence.RowCount - 2 >= 0)
            {
                //Invalid leave type selection in  consequence
                UserMessage.ShowMessage(619,null);
                lblStatus.Text = UserMessage.GetMessageByCode(619);
                tmrClear.Enabled = false;
                tbLeave.SelectedTab = tbpgConsequenceTypes;
                if (iRowIndex != -1)
                    dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[colLeaveTypeID.Index, iRowIndex];
                tbLeave.SelectedTab = tbpgConsequenceTypes;
                return false;
            }
            if (IsValidNoOfDaysInConsequenceGrid(ref iRowIndex) == false && dgvLeaveConsequence.RowCount - 2 >= 0)
            {
                //Please Check Days and Applicable days in Leave Consequence
                UserMessage.ShowMessage(627);
                lblStatus.Text = UserMessage.GetMessageByCode(627);
                tmrClear.Enabled = false;
                tbLeave.SelectedTab = tbpgConsequenceTypes;
                if (iRowIndex != -1)
                    dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[colApplicableDays.Index, iRowIndex];

                return false;
            }
            return true;
        }
        #endregion FormValidation

        #region CheckDuplicateInGrid
        /// <summary>
        /// Check duplicate of leave type,order number in consequence grid
        /// </summary>
        /// <param name="Grd">consequence grid as DataGridView</param>
        /// <param name="ColIndex1">Order number column index as int</param>
        /// <param name="ColIndex2">Leave type column index as int</param>
        /// <returns></returns>
        private int CheckDuplicateInGrid(DataGridView Grd, int ColIndex1, int ColIndex2)
        {
            string SearchValue1 = "";
            int RowIndexTemp = 0;
            string SearchValue2 = "";
            int intRetValue = -1;
            try
            {

                foreach (DataGridViewRow rowValue in Grd.Rows)
                {
                    if (rowValue.Cells[ColIndex1].Value.ToStringCustom() != string.Empty)
                    {
                        SearchValue1 = rowValue.Cells[ColIndex1].Value.ToStringCustom();
                        SearchValue2 = rowValue.Cells[ColIndex2].Value.ToStringCustom();
                        RowIndexTemp = rowValue.Index;
                        foreach (DataGridViewRow row in Grd.Rows)
                        {
                            if (RowIndexTemp != row.Index)
                            {
                                if (row.Cells[ColIndex1].Value.ToStringCustom() != string.Empty)
                                {
                                    if (row.Cells[ColIndex1].Value.ToStringCustom() == SearchValue1 && row.Cells[ColIndex2].Value.ToStringCustom() == SearchValue2)
                                    {
                                        intRetValue = row.Index;
                                        return row.Index;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                intRetValue = -1;
            }
            return intRetValue;
        }
        #endregion CheckDuplicateInGrid

        #region CheckDuplicateInGrid
        /// <summary>
        /// Checking Leave type Duplication in the grid
        /// </summary>
        /// <param name="Grd">Grid in which duplicate should be checked as DataGridView</param>
        /// <param name="ColIndex">Column index of leave type as int</param>
        /// <returns>Row index if duplication exists else returns -1</returns>
        public int CheckDuplicateInGrid(DataGridView Grd, int ColIndex)
        {
            
            int SearchValue = 0;
            int RowIndexTemp = 0;
            int intRetValue = -1;
            try
            {
                intRetValue = -1;
                foreach (DataGridViewRow rowValue in Grd.Rows)
                {
                    if (rowValue.Cells[ColIndex].Value.ToInt32() != 0)
                    {
                        SearchValue = rowValue.Cells[ColIndex].Value.ToInt32();
                        RowIndexTemp = rowValue.Index;
                        foreach (DataGridViewRow row in Grd.Rows)
                        {
                            if (RowIndexTemp != row.Index)
                            {
                                if (row.Cells[ColIndex].Value.ToInt32() != 0)
                                {
                                    if (row.Cells[ColIndex].Value.ToInt32() == SearchValue)
                                    {
                                        intRetValue = row.Index;
                                        return row.Index;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                intRetValue = -1;
            }
            return intRetValue;
        }
        #endregion CheckDuplicateInGrid

        #region IsValidNoOfDaysInConsequenceGrid
        /// <summary>
        /// Checks if number of days entered in consequence grid is within the days in leave details grid
        /// </summary>
        /// <param name="iRowIndex">row index as int</param>
        /// <returns>valid/invalid</returns>
        private bool IsValidNoOfDaysInConsequenceGrid(ref int iRowIndex)
        {
            bool IsValidNoOfDaysInConsequence = false;
            try
            {
                iRowIndex = -1;
                decimal decApplicalbleDays = 0;
                decimal decNoOfdays = 0;
                int iLeaveType = 0;
                IsValidNoOfDaysInConsequence = true;
                foreach (DataGridViewRow rowValueDet in dgvLeaveDetail.Rows)
                {
                    if (rowValueDet.Cells["colLeaveType"].ToStringCustom() != string.Empty)
                    {
                        decNoOfdays = 0;
                        iLeaveType = rowValueDet.Cells["colLeaveType"].Value.ToInt32();
                        decNoOfdays = rowValueDet.Cells["colNoOfLeave"].Value.ToDecimal();
                        decApplicalbleDays = 0;
                        foreach (DataGridViewRow rowConseq in dgvLeaveConsequence.Rows)
                        {
                            if (rowConseq.Cells["colLeaveTypeID"].ToStringCustom() != string.Empty)
                            {
                                int iConseqLeaveType = rowConseq.Cells["colLeaveTypeID"].Value.ToInt32();
                                if (iConseqLeaveType != 0 && iLeaveType != 0)
                                {
                                    if (iLeaveType == iConseqLeaveType)
                                    {
                                        iRowIndex = rowConseq.Index;
                                        decApplicalbleDays += rowConseq.Cells["colApplicableDays"].Value.ToDecimal();
                                    }
                                }
                            }
                        }
                        if (decApplicalbleDays > decNoOfdays)
                        {
                            IsValidNoOfDaysInConsequence = false;
                            return IsValidNoOfDaysInConsequence;
                        }
                    }
                }
            }
            catch (Exception)
            {
                IsValidNoOfDaysInConsequence = false;
            }
            return IsValidNoOfDaysInConsequence;
        }
        #endregion IsValidNoOfDaysInConsequenceGrid

        #region IsValidLeaveInConsequence
        /// <summary>
        /// Checks if leavetype selected in consequence grid is selected in leave details grid 
        /// </summary>
        /// <param name="iRowIndex">row index as int</param>
        /// <returns>valid/invalid</returns>
        private bool IsValidLeaveInConsequence(int iRowIndex)
        {
            bool IsValidLeaveInConsequence = false;
            try
            {

                int flagMatch1 = 0;
                iRowIndex = -1;
                dgvLeaveDetail.EndEdit();
                int SearchValue1 = 0;
                int SearchValue2 = 0;
                int ColIndex1 = 2;
                int ColIndex2 = 2;

                IsValidLeaveInConsequence = false;
                foreach (DataGridViewRow rowValue in dgvLeaveConsequence.Rows)
                {
                    if (rowValue.Cells[ColIndex1].Value.ToStringCustom() != string.Empty)
                    {
                        SearchValue1 = rowValue.Cells[ColIndex1].Value.ToInt32();
                        flagMatch1 = 0;
                        foreach (DataGridViewRow row in dgvLeaveDetail.Rows)
                        {
                            SearchValue2 = rowValue.Cells[ColIndex2].Value.ToInt32();
                            if (row.Cells[ColIndex1].Value.ToStringCustom() != string.Empty)
                            {
                                if (row.Cells[ColIndex2].Value.ToStringCustom() != string.Empty)
                                {
                                    if (row.Cells[ColIndex2].Value.ToInt32() == SearchValue1)
                                    {
                                        flagMatch1 = 1;
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                    if (flagMatch1 != 1)
                    {
                        iRowIndex = rowValue.Index;
                        return false;
                    }
                }
                IsValidLeaveInConsequence = true;
            }
            catch (Exception)
            {
                IsValidLeaveInConsequence = false;
            }
            return IsValidLeaveInConsequence;
        }
        #endregion IsValidLeaveInConsequence

        #region DeleteValidation
        /// <summary>
        /// Validation done before deleting policy
        /// </summary>
        /// <returns>success/failure</returns>
        private bool DeleteValidation(bool IsRow)
        {
            if (txtPolicyName.Tag.ToInt32() == 0)//No Record to delete
            {
                UserMessage.ShowMessage(622, null);
                return false;
            }
            if (!IsRow && MblnPolicyExistance)//Details exists for this Leave Policy in the system.
            {
                UserMessage.ShowMessage(620, null);
                return false;
            }
            if (BLLLeavePolicy.CheckSalaryProcessed(txtPolicyName.Tag.ToInt32()))//Salary processed without release
            {
                UserMessage.ShowMessage(608, null);
                return false;
            }
            if (UserMessage.ShowMessage(13, null) == false)//Do you wish to delete this information?
            {
                return false;
            }
            return true;
        }
        #endregion DeleteValidation

        #region AddNewLeavePolicy
        /// <summary>
        /// Add a new policy
        /// </summary>
        private void AddNewLeavePolicy()
        {
            MblnIsEditMode = false;
            lblStatus.Text = "";
            tmrClear.Enabled = true;
            errProLeavePolicy.Clear();

            ClearAllControls();  //Clear all controls in the form
            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();

            BindingNavigatorAddNewItem.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = false;
            btnPrint.Enabled = btnEmail.Enabled = btnOk.Enabled = btnSave.Enabled = BindingNavigatorSaveItem.Enabled = false;
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = false;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = false;
            btnClear.Enabled = true;
        }
        #endregion AddNewLeavePolicy

        #region Changestatus
        /// <summary>
        /// change status when some changes are made
        /// </summary>
        private void Changestatus()
        {
            //function for changing status

            if (!MblnIsEditMode)
            {
                btnOk.Enabled = btnSave.Enabled = BindingNavigatorSaveItem.Enabled = btnClear.Enabled = MblnAddPermission;
            }
            else
            {
                btnOk.Enabled = btnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                btnClear.Enabled = false;
            }
            errProLeavePolicy.Clear();
            MblnChangeStatus = true;
        }
        #endregion Changestatus

        #region SetBindingNavigatorButtons
        /// <summary>
        /// Set the controls in binding navigator
        /// </summary>
        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }
        #endregion SetBindingNavigatorButtons
        #endregion Methods

        #region Events
        private void FrmLeavePolicy_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadCombos(0);
            tmrClear.Interval = ClsCommonSettings.TimerInterval;    
            if (PintLeavePolicyID > 0)//From employee form
            {
                GetRecordCount();
                RefernceDisplay();
            }
            else
            {
                AddNewLeavePolicy();
            }
        }
        private void FrmLeavePolicy_FormClosing(object sender, FormClosingEventArgs e)//FrmLeavePolicy FormClosing
        {
            if (MblnChangeStatus && btnOk.Enabled)
            {
                if (UserMessage.ShowMessage(8,null))
                {
                    e.Cancel = false;
                }
                else
                { e.Cancel = true; }
            }
        }
        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)//Move to first item
        {
            GetRecordCount();
            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;
                DisplayLeavePolicyInfo();
                lblStatus.Text = UserMessage.GetMessageByCode(9);
                tmrClear.Enabled = true;
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)//Move to prev item
        {
            GetRecordCount();       
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    DisplayLeavePolicyInfo();
                    lblStatus.Text = UserMessage.GetMessageByCode(10);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 1;
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)//Move to next item
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                }
                if (MintCurrentRecCnt > MintRecordCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                }
                else
                {
                    DisplayLeavePolicyInfo();
                    lblStatus.Text = UserMessage.GetMessageByCode(11);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 0;
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)//Move to last item
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    DisplayLeavePolicyInfo();
                    lblStatus.Text = UserMessage.GetMessageByCode(12);
                    tmrClear.Enabled = true;
                }
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)//Add new policy
        {
            AddNewLeavePolicy();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)//Delete policy
        {
            try
            {
                if (DeleteLeavePolicy())
                {
                    lblStatus.Text = UserMessage.GetMessageByCode(602);
                    UserMessage.ShowMessage(602, null);
                    AddNewLeavePolicy();
                }
            }
            catch (System.Data.SqlClient.SqlException Ex)
            {
                string MstrMessageCommon = "";
                if (Ex.Number == (int)SqlErrorCodes.ForeignKeyErrorNumber)
                {
                    MstrMessageCommon = "Details exists in the sysytem.Cannot delete";
                }
                else
                    MstrMessageCommon = "Error on BindingNavigatorDeleteItem_Click() " + Ex.Message.ToString();

                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1,Log.LogSeverity.Error);

            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)//Add/Update Policy
        {
            if (SaveLeavePolicy())
            {
                //lblStatus.Text = UserMessage.GetMessageByCode(2, 2);
                //UserMessage.ShowMessage(2, null, 2);
            }
        }
        private void tmrClear_Tick(object sender, EventArgs e)//Timer tick
        {
            tmrClear.Enabled = false;
            lblStatus.Text = "";
        }

        private void cboCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Changestatus();
            //if (Convert.ToInt32(cboCompany.SelectedValue) > 0)
            //{
                LoadCombos(4);//Fill Financial Year Combo
           // }
        }

        private void btnClear_Click(object sender, EventArgs e)//redirect to new mode
        {
            AddNewLeavePolicy();
        }

        private void btnPrint_Click(object sender, EventArgs e)//print details of a policy
        {
            try
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = txtPolicyName.Tag.ToInt32();
                ObjViewer.PiFormID = (int)FormID.LeavePolicy;
                ObjViewer.ShowDialog();
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }


        private void btnLeaveTypes_Click(object sender, EventArgs e)//show leave type form for addition/updation
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Leave Type", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "LeaveTypeID,IsPredefined,LeaveType,LeaveTypeArb", "PayLeaveTypeReference", "LeaveTypeID!=5");
                objCommon.ShowDialog();
                objCommon.Dispose();
                LoadCombos(2);
                LoadCombos(3);
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }

        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "LeavePolicy";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void btnSave_Click(object sender, EventArgs e)//Add/Update Policy
        {
            if (SaveLeavePolicy())
            {
               // UserMessage.ShowMessage(2, null, 2);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)//Add/Update Policy
        {
            if (SaveLeavePolicy())
            {
                //lblStatus.Text = UserMessage.GetMessageByCode(2, 2);
                //tmrClear.Enabled = true;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)//Close the form
        {
            this.Close();
        }

        private void txtPolicyName_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cboCompany.SelectedValue.ToInt32() > 0)
            //{
                bNav = false;
                LoadCombos(4); 
           // }
        }

        private void dgvLeaveDetail_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            Changestatus();
            try
            {
                if (txtPolicyName.Text.Trim() == "")
                {
                    UserMessage.ShowMessage(604, null);//Please enter a policyname
                    e.Cancel = true;
                    return;
                }
                if (CboFinYear.Text == "")
                {
                    UserMessage.ShowMessage(606, null);//Please select an Applicable Date
                    e.Cancel = true;
                    return;
                }
                int iRowIndex;
                int iColIndex;
                iRowIndex = e.RowIndex;
                iColIndex = e.ColumnIndex;
                int RowStart;
                RowStart = iRowIndex - 1;
                if (RowStart < 0)
                    RowStart = 0;


                

                    if (e.ColumnIndex==5) 
                    {

                    }

                for (int i = RowStart; i <= iRowIndex; i++)
                {
                    for (int j = 0; j <= dgvLeaveDetail.ColumnCount - 1; j++)
                    {
                        if (j < iColIndex && iRowIndex == i)
                        {
                            switch (j)
                            {
                                case 2:
                                    if (dgvLeaveDetail.Rows[i].Cells["colLeaveType"].Value.ToStringCustom() == string.Empty)
                                    {
                                        if (iColIndex != 4)
                                        {
                                            MstrCommonMessage = UserMessage.GetMessageByCode(623);//select leave type
                                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveDetail.Columns["colLeaveType"].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            dgvLeaveDetail.CurrentCell = dgvLeaveDetail["colLeaveType", i];
                                        }
                                        e.Cancel = true;
                                        return;
                                    }
                                    break;
                                case 3:
                                    if (dgvLeaveDetail.Rows[i].Cells["colNoOfLeave"].Value.ToStringCustom() == string.Empty)
                                    {
                                        if (iColIndex != 4)
                                        {
                                            MstrCommonMessage = UserMessage.GetMessageByCode(623);//select number of leaves
                                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveDetail.Columns["colNoOfLeave"].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            dgvLeaveDetail.CurrentCell = dgvLeaveDetail["colNoOfLeave", i];
                                        }
                                        e.Cancel = true;
                                        return;
                                    }
                                    break;

                            }
                        }
                        else if (i != iRowIndex)
                        {
                            switch (j)
                            {

                                case 2:

                                    if (dgvLeaveDetail.Rows[i].Cells["colLeaveType"].Value.ToStringCustom() == string.Empty)
                                    {
                                        if (iColIndex != 4)
                                        {
                                            MstrCommonMessage = UserMessage.GetMessageByCode(623);//select leave type
                                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveDetail.Columns["colLeaveType"].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            dgvLeaveDetail.CurrentCell = dgvLeaveDetail["colLeaveType", i];
                                        }
                                        e.Cancel = true;
                                        return;
                                    }
                                    break;
                                case 3:
                                    if (dgvLeaveDetail.Rows[i].Cells["colNoOfLeave"].Value.ToStringCustom() == string.Empty)
                                    {
                                        if (iColIndex != 4)
                                        {
                                            MstrCommonMessage = UserMessage.GetMessageByCode(623);//select number of leaves
                                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveDetail.Columns["colNoOfLeave"].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            dgvLeaveDetail.CurrentCell = dgvLeaveDetail["colNoOfLeave", i];
                                        }
                                        e.Cancel = true;
                                        return;
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void dgvLeaveDetail_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //disable all fields except 'Days' if leave type is maternity
                if (dgvLeaveDetail.CurrentRow.Cells["colLeaveType"].Value.ToInt32() == 3)
                {
                    dgvLeaveDetail.CurrentRow.Cells["colMonthLeave"].ReadOnly = true;
                    dgvLeaveDetail.CurrentRow.Cells["colCarryForwardLeave"].ReadOnly = true;
                    dgvLeaveDetail.CurrentRow.Cells["colEncashDays"].ReadOnly = true;
                    dgvLeaveDetail.CurrentRow.Cells["colMonthLeave"].Value = 0;
                    dgvLeaveDetail.CurrentRow.Cells["colCarryForwardLeave"].Value = 0;
                    dgvLeaveDetail.CurrentRow.Cells["colEncashDays"].Value = 0;
                }
                else
                {
                    dgvLeaveDetail.CurrentRow.Cells["colMonthLeave"].ReadOnly = false;
                    dgvLeaveDetail.CurrentRow.Cells["colCarryForwardLeave"].ReadOnly = false;
                    dgvLeaveDetail.CurrentRow.Cells["colEncashDays"].ReadOnly = false;
                }
                if (e.ColumnIndex == 3 && e.RowIndex > -1)
                {
                    if (dgvLeaveDetail.CurrentRow.Cells["colNoOfLeave"].Value.ToStringCustom() != string.Empty && dgvLeaveDetail.CurrentRow.Cells["colLeaveType"].Value.ToStringCustom() == string.Empty)
                    {
                        if (txtPolicyName.Tag.ToInt32() > 0)
                        {
                            int iLeavePolicyID = txtPolicyName.Tag.ToInt32();
                            int iLeaveType = dgvLeaveDetail.CurrentRow.Cells["colLeaveType"].Value.ToInt32();
                            int Leavediff = 0;
                            int NoLeaves = BLLLeavePolicy.GetNoOfDays(iLeavePolicyID, iLeaveType); //'geting no of leaves
                            Leavediff = NoLeaves - dgvLeaveDetail.CurrentRow.Cells["colNoOfLeave"].Value.ToInt32();



                            if (Leavediff > 0)
                            {
                                int iMinDiff = 0;
                                iMinDiff = BLLLeavePolicy.GetMinDiff(iLeavePolicyID, iLeaveType);//Calculate Maximum diiffernce between current and old value
                                if (iMinDiff == 0)
                                {
                                    MstrCommonMessage = UserMessage.GetMessageByCode(624);//Value Should not be less than old value
                                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    dgvLeaveDetail.CurrentRow.Cells["colNoOfLeave"].Value = NoLeaves;
                                }
                                else if (iMinDiff > 0)
                                {
                                    if (Leavediff > iMinDiff)
                                    {
                                        MstrCommonMessage = UserMessage.GetMessageByCode(625);//Maximum diiffernce between current and old value is iMinDiff
                                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + iMinDiff + "", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        dgvLeaveDetail.CurrentRow.Cells["colNoOfLeave"].Value = NoLeaves;
                                    }
                                }
                            }




                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }

        }     
       
        private void dgvLeaveDetail_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void dgvLeaveDetail_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (dgvLeaveDetail.CurrentCell != null)
                {
                    if (dgvLeaveDetail.CurrentCell.ColumnIndex == colLeaveType.Index)
                    {
                        ComboBox cb = (ComboBox)e.Control;
                        cb.KeyPress += new KeyPressEventHandler(cb_KeyPress);
                        cb.DropDownHeight = 120;
                        cb.DropDownStyle = ComboBoxStyle.DropDown;
                        cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        cb.AutoCompleteSource = AutoCompleteSource.ListItems;
                    }
                    if (dgvLeaveDetail.CurrentCell.ColumnIndex == colMonthLeave.Index || dgvLeaveDetail.CurrentCell.ColumnIndex == colEncashDays.Index ||
                        dgvLeaveDetail.CurrentCell.ColumnIndex == colCarryForwardLeave.Index || dgvLeaveDetail.CurrentCell.ColumnIndex == colNoOfLeave.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void dgvLeaveDetail_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                e.Handled = false;
                if (e.KeyData == Keys.Delete)//Deleting a row from grid
                {
                    if (dgvLeaveDetail.RowCount > 2)
                    {
                        if (dgvLeaveDetail.CurrentRow.Cells["colSerialNo"].Value.ToInt32() > 0)
                        {
                            if (!dgvLeaveDetail.CurrentRow.Cells["LeaveTypeExistance"].Value.ToBoolean())
                            {
                                if (DeleteValidation(true))
                                {
                                    BLLLeavePolicy.DeletLeavePolicyDetails(txtPolicyName.Tag.ToInt32(), dgvLeaveDetail.CurrentRow.Cells["colSerialNo"].Value.ToInt32());
                                }
                                else
                                {
                                   
                                    e.Handled = true;
                                }
                            }
                            else
                            {
                                UserMessage.ShowMessage(620, null);
                                e.Handled = true;
                            }
                        }
                    }
                    else
                    {
                        e.Handled = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void dgvLeaveDetail_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                dgvLeaveDetail.Rows[e.RowIndex].Cells["colStatus"].Value = 1;
            }

            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Checks if all the columns are filled in the prev row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvLeaveConsequence_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            Changestatus();
            try
            {
                if (txtPolicyName.Text.Trim() == "")
                {
                    UserMessage.ShowMessage(604, null);//Please enter a policyname
                    e.Cancel = true;
                    return;
                }
                if (CboFinYear.Text == "")
                {
                    UserMessage.ShowMessage(606, null);//Please select an Applicable Date.
                    e.Cancel = true;
                    return;
                }
                int iRowIndex;
                int iColIndex;
                iRowIndex = e.RowIndex;
                iColIndex = e.ColumnIndex;


                int RowStart;
                RowStart = iRowIndex - 1;

                if (RowStart < 0)
                    RowStart = 0;

                for (int i = RowStart; i <= iRowIndex; i++)
                {
                    for (int j = 0; j <= dgvLeaveConsequence.ColumnCount - 1; j++)
                    {
                        if (j < iColIndex && iRowIndex == i)
                        {
                            switch (j)
                            {
                                case 1:
                                    if (dgvLeaveConsequence.Rows[i].Cells["colOrderNo"].Value.ToStringCustom() == string.Empty)
                                    {
                                        dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[1, i];
                                        MstrCommonMessage = UserMessage.GetMessageByCode(623);//select order number
                                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[j].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        e.Cancel = true;
                                        return;

                                    }
                                    break;
                                case 2:
                                    if (dgvLeaveConsequence.Rows[i].Cells["colLeaveTypeID"].Value.ToStringCustom() == string.Empty)
                                    {
                                        dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[2, i];

                                        MstrCommonMessage = UserMessage.GetMessageByCode(623);//select leave type
                                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[j].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        e.Cancel = true;
                                        return;
                                    }
                                    break;
                                case 3:

                                    if (dgvLeaveConsequence.Rows[i].Cells["colCalculationBasedID"].Value.ToStringCustom() == string.Empty)
                                    {
                                        dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[3, i];

                                        MstrCommonMessage = UserMessage.GetMessageByCode(623);//select calculation based on
                                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[j].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        e.Cancel = true;
                                        return;
                                    }

                                    break;
                                //case 4:
                                    //if (dgvLeaveConsequence.Rows[i].Cells["colDeductionPercentage"].Value.ToStringCustom() == string.Empty)
                                    //{
                                    //    dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[4, i];
                                    //    MstrCommonMessage = UserMessage.GetMessageByCode(623, 2);//select deduction percentage
                                    //    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[j].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    //    e.Cancel = true;
                                    //    return;
                                    //}
                                   // break;
                                case 5:

                                    if (dgvLeaveConsequence.Rows[i].Cells["colApplicableDays"].Value.ToStringCustom() == string.Empty)
                                    {
                                        dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[5, i];
                                        MstrCommonMessage = UserMessage.GetMessageByCode(628);//enter applicable days
                                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[j].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        e.Cancel = true;
                                        return;
                                    }
                                    break;

                            }
                        }
                        else if (i != iRowIndex)
                        {
                            switch (j)
                            {
                                case 1:
                                    if (dgvLeaveConsequence.Rows[i].Cells["colOrderNo"].Value.ToStringCustom() == string.Empty)
                                    {
                                        dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[1, i];
                                        MstrCommonMessage = UserMessage.GetMessageByCode(623);
                                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[j].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        e.Cancel = true;
                                        return;

                                    }
                                    break;
                                case 2:
                                    if (dgvLeaveConsequence.Rows[i].Cells["colLeaveTypeID"].Value.ToStringCustom() == string.Empty)
                                    {
                                        dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[2, i];
                                        MstrCommonMessage = UserMessage.GetMessageByCode(623);
                                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[j].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        e.Cancel = true;
                                        return;
                                    }
                                    break;
                                case 3:
                                    if (dgvLeaveConsequence.Rows[i].Cells["colCalculationBasedID"].Value.ToStringCustom() == string.Empty)
                                    {
                                        dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[3, i];

                                        MstrCommonMessage = UserMessage.GetMessageByCode(623);
                                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[j].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        e.Cancel = true;
                                        return;
                                    }

                                    break;
                                //case 4:
                                //    if (dgvLeaveConsequence.Rows[i].Cells["colDeductionPercentage"].Value.ToStringCustom() == string.Empty)
                                //    {
                                //        dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[4, i];
                                //        MstrCommonMessage = UserMessage.GetMessageByCode(623, 2);
                                //        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[j].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //        e.Cancel = true;
                                //        return;
                                //    }
                                //    break;
                                case 5:
                                    if (dgvLeaveConsequence.Rows[i].Cells["colApplicableDays"].Value.ToStringCustom() == string.Empty)
                                    {
                                        dgvLeaveConsequence.CurrentCell = dgvLeaveConsequence[5, i];
                                        MstrCommonMessage = UserMessage.GetMessageByCode(628);
                                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " " + dgvLeaveConsequence.Columns[j].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        e.Cancel = true;
                                        return;
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void dgvLeaveConsequence_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == dgvLeaveConsequence.Columns["colDeductionPercentage"].Index)
                {
                    if (dgvLeaveConsequence.Rows[e.RowIndex].Cells["colDeductionPercentage"].Value.ToStringCustom() != string.Empty)
                    {

                        if (dgvLeaveConsequence.Rows[e.RowIndex].Cells["colDeductionPercentage"].Value.ToDecimal() > 0)
                        {
                            if (dgvLeaveConsequence.Rows[e.RowIndex].Cells["colDeductionPercentage"].Value.ToDecimal() >= 0 && dgvLeaveConsequence.Rows[e.RowIndex].Cells["colDeductionPercentage"].Value.ToDecimal() <= 500)
                            {
                            }
                            else
                            {
                                UserMessage.ShowMessage(626);//Please enter a value between 0 to 500
                                dgvLeaveConsequence.Rows[e.RowIndex].Cells["colDeductionPercentage"].Value = 0;
                            }
                        }
                        else
                        {
                            //UserMessage.ShowMessage(626, null, 2);
                            dgvLeaveConsequence.Rows[e.RowIndex].Cells["colDeductionPercentage"].Value = 0;
                        }
                    }
                    else
                    {
                        dgvLeaveConsequence.Rows[e.RowIndex].Cells["colDeductionPercentage"].Value = 0;
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

       

        private void dgvLeaveConsequence_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {

            }
        }

        private void dgvLeaveConsequence_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (dgvLeaveConsequence.CurrentCell != null)
                {
                    if (dgvLeaveConsequence.CurrentCell.ColumnIndex == colCalculationBasedID.Index || dgvLeaveConsequence.CurrentCell.ColumnIndex == colLeaveTypeID.Index)
                    {
                        ComboBox cb = (ComboBox)e.Control;
                        cb.KeyPress += new KeyPressEventHandler(cb_KeyPress);
                        cb.DropDownHeight = 120;
                        cb.DropDownStyle = ComboBoxStyle.DropDown;
                        cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        cb.AutoCompleteSource = AutoCompleteSource.ListItems;
                    }
                    if (dgvLeaveConsequence.CurrentCell.ColumnIndex == colDeductionPercentage.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                    if (dgvLeaveConsequence.CurrentCell.ColumnIndex == colOrderNo.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtInt_KeyPress);
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        void txtDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        }
        void txtInt_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}.{:?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0))
            {
                e.Handled = true;
            }
        }

        void cb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            cb.DroppedDown = false;
        }

        private void dgvLeaveConsequence_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                e.Handled = false;
                if (e.KeyData == Keys.Delete)
                {
                    if (dgvLeaveConsequence.CurrentRow.Cells["colLeaveConsequenceID"].Value.ToInt32() > 0)
                    {
                        if (dgvLeaveConsequence.CurrentRow != null && txtPolicyName.Tag.ToInt32()>0)
                        {
                            BLLLeavePolicy.DeletLeaveConseqDetails(txtPolicyName.Tag.ToInt32(), dgvLeaveConsequence.CurrentRow.Cells["colLeaveConsequenceID"].Value.ToInt32());
                        }
                        else
                        {
                            e.Handled = true;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }

        }

        private void dgvLeaveDetail_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvLeaveDetail.IsCurrentCellDirty)
                {
                    dgvLeaveDetail.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }

        }

        private void dgvLeaveConsequence_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvLeaveConsequence.IsCurrentCellDirty)
                {

                    dgvLeaveConsequence.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }


        private void CboFinYear_KeyDown(object sender, KeyEventArgs e)
        {
            CboFinYear.DroppedDown = false;
            if (e.KeyData == Keys.Enter)
                CboFinYear_SelectionChangeCommitted(sender, new EventArgs());
        }

        private void CboFinYear_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Changestatus();
        }

        /// <summary>
        /// Shortcut keys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmLeavePolicy_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        btnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());//Prev item
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());//Next item
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());//First item
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());//Last item
                        break;
                    case Keys.Control | Keys.P:
                        btnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.F1:
                        btnHelp_Click(sender, new EventArgs());//help
                        break;
                }
            }
            catch
            {
            }
        }

        #endregion Events

        private void dgvLeaveDetail_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
              try
              {
                  if (e.RowIndex.ToInt32() >=0)
                  {
                        if (e.ColumnIndex.ToInt32()==5) 
                        {
                            lblStatus.Text = UserMessage.GetMessageByCode(20014);
                            tmrClear.Enabled = true;
                        }
                  }
              }
             catch 
              {
             }
        }
    }
}
