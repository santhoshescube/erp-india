﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{

    /*****************************************************
    * Created By       : Alvin
    * Creation Date    : 10 Apr 2012
    * Description      : Handle Overtime Policy
    * ***************************************************/

    public partial class FrmOvertimePolicy : Form
    {
        private bool MblnPrintEmailPermission = false;     //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;       //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;  //To set Add Update Permission

        public bool MblnIsEditMode = false;

        private int MintRecordCnt = 0;
        private int MintCurrentRecCnt = 0;
        public int PintOTPolicyID = 0;
        public int PintOTPolicyIDForFocus = 0;

        private clsMessage ObjUserMessage = null;
        private clsBLLOvertimePolicy MobjclsBLLOvertimePolicy = null;
        private bool MblnIsFromOK = true;
        string strBindingOf = "Of ";

        public FrmOvertimePolicy()
        {
            InitializeComponent();

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.OvertimePolicy, this);

        //    strBindingOf = "من ";
        //    dgvParticulars.Columns["AdditionDeduction"].HeaderText = "تفاصيل";
        //}
        private clsMessage objUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.OvertimePolicy);

                return this.objUserMessage;
            }
        }


        private clsBLLOvertimePolicy BLLOvertimePolicy
        {
            get
            {
                if (this.MobjclsBLLOvertimePolicy == null)
                    this.MobjclsBLLOvertimePolicy = new clsBLLOvertimePolicy();

                return this.MobjclsBLLOvertimePolicy;
            }
        }



        #region Methods

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.OvertimePolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
               MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }

        private bool LoadCombos(int intType)
        {
            bool blnRetvalue = false;
            DataTable datCombos = new DataTable();
            try
            {
                if (intType == 0 || intType == 1)//CalculationBased Combo
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    datCombos = BLLOvertimePolicy.FillCombos(new string[] { "CalculationID,CalculationArb AS Calculation", "PayCalculationReference", "CalculationID in(1,2)" });
                    //else
                        datCombos = BLLOvertimePolicy.FillCombos(new string[] { "CalculationID,Calculation", "PayCalculationReference", "CalculationID in(1,2)" });
                    cboCalculationBased.ValueMember = "CalculationID";
                    cboCalculationBased.DisplayMember = "Calculation";
                    cboCalculationBased.DataSource = datCombos;
                    datCombos = null;
                    blnRetvalue = true;
                }

            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
                blnRetvalue = false;
            }
            return blnRetvalue;


        }

        private void LoadAdditionsList()
        {
            try
            {
                dgvParticulars.Rows.Clear();
                DataTable DtAdditionsList;
                DtAdditionsList = BLLOvertimePolicy.GetAdditionDeductionDetails();

                if (DtAdditionsList.Rows.Count > 0)
                {
                    for (int iCount = 0; iCount <= DtAdditionsList.Rows.Count - 1; iCount++)
                    {
                        dgvParticulars.RowCount = dgvParticulars.RowCount + 1;
                        dgvParticulars.Rows[iCount].Cells["AdditionDeductionID"].Value = DtAdditionsList.Rows[iCount]["AdditionDeductionID"].ToInt32();
                        dgvParticulars.Rows[iCount].Cells["AdditionDeduction"].Value = DtAdditionsList.Rows[iCount]["AdditionDeduction"].ToString();
                        dgvParticulars.Rows[iCount].Cells["SelectedVal"].Value = DtAdditionsList.Rows[iCount]["SelectedVal"].ToBoolean();

                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);

            }
        }
        private void LoadInitial()
        {
            rdbCompanyBased.Checked = true;
            txtOverTimeRate.Enabled = false;
            txtCalculationPercent.Enabled = false;
            
        }

        private void FillParametrOTPolicyMaster()
        {
            BLLOvertimePolicy.DTOOvertimePolicy.strOTPolicyName = txtOTPolicyName.Text.Trim();
            BLLOvertimePolicy.DTOOvertimePolicy.intCalculationID = cboCalculationBased.SelectedValue.ToInt32();
            BLLOvertimePolicy.DTOOvertimePolicy.decTotalWorkingHours = txtWorkingHoursOrYear.Text.ToDecimal();

            if (chkRateOnly.Checked == true)
                BLLOvertimePolicy.DTOOvertimePolicy.boolIsRateOnly = true;
            else
                BLLOvertimePolicy.DTOOvertimePolicy.boolIsRateOnly = false;

            if (rdbCompanyBased.Checked == true)
                BLLOvertimePolicy.DTOOvertimePolicy.intCompanyBasedOn = 1;
            else if (rdbActualMonth.Checked == true)
                BLLOvertimePolicy.DTOOvertimePolicy.intCompanyBasedOn = 2;
            //else if (rdbWorkingDays.Checked == true)
            //    BLLOvertimePolicy.DTOOvertimePolicy.intCompanyBasedOn = 3;

            BLLOvertimePolicy.DTOOvertimePolicy.decOTRate = txtOverTimeRate.Text.ToDecimal();
            BLLOvertimePolicy.DTOOvertimePolicy.CalculationPercentage = txtCalculationPercent.Text.ToDecimal();

        }
        private void FillParametrSalaryPolicyDetail()
        {
            if (dgvParticulars.Rows.Count > 0)
            {
                BLLOvertimePolicy.DTOOvertimePolicy.DTOSalaryPolicyDetail = new List<clsDTOSalaryPolicyDetail>();

                for (int intICounter = 0; intICounter <= dgvParticulars.Rows.Count - 1; intICounter++)
                {
                    clsDTOSalaryPolicyDetail objSalaryPolicyDetail = new clsDTOSalaryPolicyDetail();

                    if (dgvParticulars.Rows[intICounter].Cells["SelectedVal"].Value.ToBoolean())
                    {
                        objSalaryPolicyDetail.intAdditionDeductionID = dgvParticulars.Rows[intICounter].Cells["AdditionDeductionID"].Value.ToInt32();
                        BLLOvertimePolicy.DTOOvertimePolicy.DTOSalaryPolicyDetail.Add(objSalaryPolicyDetail);
                    }
                }


            }

        }

        private void AddNewOTPolicy()
        {
            MblnIsEditMode = false;
            tmrClear.Enabled = true;
            errProOTPolicy.Clear();
            ClearAllControls();

            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();


            lblStatus.Text = "";
            BindingNavigatorAddNewItem.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = false;
            btnPrint.Enabled = false;
            btnOk.Enabled = false;
            btnSave.Enabled = false;
            btnClear.Enabled = true;
            BindingNavigatorSaveItem.Enabled = false;
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = false;
            BindingNavigatorMoveNextItem.Enabled = false;
            BLLOvertimePolicy.DTOOvertimePolicy.intOTPolicyID = 0;
            //lblWorkingHoursOrYear.Visible = false;
            //txtWorkingHoursOrYear.Visible = false;
            MblnIsFromOK = true;

        }

        private bool SaveOvertimePolicy()
        {
            bool blnRetValue = false;
            if (FormValidation())
            {
                int intMessageCode = 0;
                if (MblnIsEditMode == false)
                    intMessageCode = 8017;
                else
                    intMessageCode = 8018;

                if (UserMessage.ShowMessage(intMessageCode))
                {
                    FillParametrOTPolicyMaster();
                    FillParametrSalaryPolicyDetail();

                    blnRetValue = BLLOvertimePolicy.OTPolicyMasterSave(MblnIsEditMode);
                    PintOTPolicyIDForFocus = BLLOvertimePolicy.DTOOvertimePolicy.intOTPolicyID;

                    if (blnRetValue == true)
                    {
                        blnRetValue = BLLOvertimePolicy.SalrayPolicyDetailsSave(MblnIsEditMode);
                    }
                    MblnIsEditMode = true;
                }
            }
            if (blnRetValue)
            {
                if (MblnIsEditMode)
                {
                    UserMessage.ShowMessage(21);
                    lblStatus.Text = UserMessage.GetMessageByCode(21);
                }
                else
                {
                    UserMessage.ShowMessage(2);
                    lblStatus.Text = UserMessage.GetMessageByCode(2);
                }
            }

            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorSaveItem.Enabled = false;
            btnSave.Enabled = false;
            btnOk.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            btnClear.Enabled = false;
           
            return blnRetValue;

        }

        private void ClearAllControls()
        {
            txtOTPolicyName.Text = "";
            cboCalculationBased.SelectedValue = -1;
            dgvParticulars.Rows.Clear();
            txtCalculationPercent.Text = "";
            txtWorkingHoursOrYear.Text = "";
            txtOverTimeRate.Text = "";
            chkCalculationPercentage.Checked = false;
            chkRateOnly.Checked = false;
            rdbCompanyBased.Checked = true;

        }

        private void GetRecordCount()
        {
            MintRecordCnt = 0;
            MintRecordCnt = BLLOvertimePolicy.GetRecordCount();
            if (MintRecordCnt < 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                MintRecordCnt = 0;
            }

        }

        private void DisplayOTPolicyInfo()
        {
            FillOTPolicyInfo();
            MblnIsEditMode = true;
            BindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            btnPrint.Enabled = MblnPrintEmailPermission;
            BindingNavigatorSaveItem.Enabled = false;
            btnSave.Enabled = false;
            btnOk.Enabled = false;
            btnClear.Enabled = false;
            SetBindingNavigatorButtons();
        }
        private void FillOTPolicyInfo()
        {
            ClearAllControls();

            if (BLLOvertimePolicy.DisplayOTPolicyMaster(MintCurrentRecCnt))
            {
                txtOTPolicyName.Text = BLLOvertimePolicy.DTOOvertimePolicy.strOTPolicyName;
                cboCalculationBased.SelectedValue = BLLOvertimePolicy.DTOOvertimePolicy.intCalculationID;

                if (BLLOvertimePolicy.DTOOvertimePolicy.CalculationPercentage == 0)
                {
                    chkCalculationPercentage.Checked = false;
                }
                else
                {
                    chkCalculationPercentage.Checked = true;
                    txtCalculationPercent.Text = BLLOvertimePolicy.DTOOvertimePolicy.CalculationPercentage.ToString();
                }
                //txtCalculationPercent.Text = BLLOvertimePolicy.DTOOvertimePolicy.CalculationPercentage.ToString();

                if (BLLOvertimePolicy.DTOOvertimePolicy.intCompanyBasedOn == 1)
                {
                    rdbCompanyBased.Checked = true;
                }
                else 
                {
                    rdbActualMonth.Checked = true;
                }
                //else if (BLLOvertimePolicy.DTOOvertimePolicy.intCompanyBasedOn == 3)
                //{
                //    rdbWorkingDays.Checked = true;
                //}

                if (BLLOvertimePolicy.DTOOvertimePolicy.boolIsRateOnly == false)
                    txtWorkingHoursOrYear.Text = BLLOvertimePolicy.DTOOvertimePolicy.decTotalWorkingHours.ToString();
                else
                    txtWorkingHoursOrYear.Text = "";


                if (BLLOvertimePolicy.DTOOvertimePolicy.boolIsRateOnly == true)
                {
                    chkRateOnly.Checked = true;
                    txtOverTimeRate.Text = BLLOvertimePolicy.DTOOvertimePolicy.decOTRate.ToString();

                }
                else
                {
                    chkRateOnly.Checked = false;
                    txtOverTimeRate.Text = "";
                }

                //if (ClsCommonSettings.IsAmountRoundByZero)
                //{
                //    txtOverTimeRate.Text = txtOverTimeRate.Text.ToDecimal().ToString("F" + 0);
                //}

                //txtOverTimeRate.Text = BLLOvertimePolicy.DTOOvertimePolicy.decOTRate.ToString();


                if (BLLOvertimePolicy.DTOOvertimePolicy.intCalculationID!=1)
                FillSalaryPolicyDetailInfo();

            }

        }

        private void FillSalaryPolicyDetailInfo()
        {
            DataTable dtSalayPolicyDetail = MobjclsBLLOvertimePolicy.DispalySalaryPolicyDetail(BLLOvertimePolicy.DTOOvertimePolicy.intOTPolicyID);

            dgvParticulars.Rows.Clear();
            if (dtSalayPolicyDetail.Rows.Count > 0)
            {
                for (int iCounter = 0; iCounter <= dtSalayPolicyDetail.Rows.Count - 1; iCounter++)
                {
                    dgvParticulars.RowCount = dgvParticulars.RowCount + 1;

                    dgvParticulars.Rows[iCounter].Cells["AdditionDeductionID"].Value = dtSalayPolicyDetail.Rows[iCounter]["AdditionDeductionID"].ToInt32();
                    dgvParticulars.Rows[iCounter].Cells["AdditionDeduction"].Value = dtSalayPolicyDetail.Rows[iCounter]["DescriptionStr"].ToString();
                    dgvParticulars.Rows[iCounter].Cells["SelectedVal"].Value = dtSalayPolicyDetail.Rows[iCounter]["SelectedVal"].ToBoolean();

                }
            }

        }

        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }

        private void Changestatus()
        {
            //function for changing status

            MblnIsFromOK = false;
            if (!MblnIsEditMode)
            {
                btnOk.Enabled = MblnAddPermission;
                btnSave.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                btnOk.Enabled = MblnUpdatePermission;
                btnSave.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            errProOTPolicy.Clear();
        }


        private bool DeleteOTPolicy()
        {
            bool blnRetValue = false;
            if (DeleteValidation())
            {
                if (UserMessage.ShowMessage(8026) == true)
                {
                    if (BLLOvertimePolicy.DeleteOTPolicy())
                    {
                        blnRetValue = true;
                    }
                }

            }
            return blnRetValue;

        }

        private bool DeleteValidation()
        {
            if (BLLOvertimePolicy.PolicyIDExists())
            {
                UserMessage.ShowMessage(8020);
                return false;
            }
            return true;

        }

        void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars;

            strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            //if (ClsCommonSettings.IsAmountRoundByZero)
            //{
            //    strInvalidChars = strInvalidChars + ".";
            //}

            e.Handled = false;
            if (((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))))
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 )
            {
                e.Handled = true;
            }
        }

        private bool FormValidation()
        {
            errProOTPolicy.Clear();

            if (txtOTPolicyName.Text.Trim() == "")
            {
                errProOTPolicy.SetError(txtOTPolicyName, UserMessage.GetMessageByCode(8000));
                UserMessage.ShowMessage(8000);
                txtOTPolicyName.Focus();
                return false;
            }

            if (chkRateOnly.Checked==false && cboCalculationBased.SelectedValue == null)
            {
                errProOTPolicy.SetError(cboCalculationBased, UserMessage.GetMessageByCode(8001));
                UserMessage.ShowMessage(8001);
                cboCalculationBased.Focus();
                return false;
            }

            if (chkCalculationPercentage.Checked == true && (txtCalculationPercent.Text.Trim() == "" || txtCalculationPercent.Text.ToDecimal()==0))
            {
                errProOTPolicy.SetError(txtCalculationPercent, UserMessage.GetMessageByCode(8006));
                UserMessage.ShowMessage(8006);
                txtCalculationPercent.Focus();
                return false;

            }

            if (chkCalculationPercentage.Checked == true && (txtWorkingHoursOrYear.Text.Trim() == "" || txtWorkingHoursOrYear.Text.ToDecimal()==0))
            {
                errProOTPolicy.SetError(txtWorkingHoursOrYear, UserMessage.GetMessageByCode(8002));
                UserMessage.ShowMessage(8002);
                txtWorkingHoursOrYear.Focus();
                return false;

            }

            if (txtWorkingHoursOrYear.Enabled   && txtWorkingHoursOrYear.Text.ToDecimal() > 24)
            {
                errProOTPolicy.SetError(txtWorkingHoursOrYear, UserMessage.GetMessageByCode(8002));
                UserMessage.ShowMessage(8002);
                txtWorkingHoursOrYear.Focus();
                return false;
            }

            //if (chkCalculationPercentage.Checked == false && txtWorkingHoursOrYear.Text.ToInt32() > 8760)
            //{
            //    errProOTPolicy.SetError(txtWorkingHoursOrYear, UserMessage.GetMessageByCode(8002));
            //    UserMessage.ShowMessage(8002);
            //    txtWorkingHoursOrYear.Focus();
            //    return false;
            //}

            if (chkRateOnly.Checked == false && (txtWorkingHoursOrYear.Text.Trim() == "" || txtWorkingHoursOrYear.Text.ToDecimal()==0))
            {
                errProOTPolicy.SetError(txtWorkingHoursOrYear, UserMessage.GetMessageByCode(8002));
                UserMessage.ShowMessage(8002);
                txtWorkingHoursOrYear.Focus();
                return false;
            }

            //if (chkRateOnly.Checked == true && cboCalculationBased.SelectedValue == null)
            //{
            //    errProOTPolicy.SetError(cboCalculationBased, UserMessage.GetMessageByCode(8001));
            //    UserMessage.ShowMessage(8001);
            //    cboCalculationBased.Focus();
            //    return false;
            //}

            if (chkRateOnly.Checked == true && (txtOverTimeRate.Text.Trim() == "" || txtOverTimeRate.Text.ToDecimal() ==0))
            {
                errProOTPolicy.SetError(txtOverTimeRate, UserMessage.GetMessageByCode(8004));
                UserMessage.ShowMessage(8004);
                txtOverTimeRate.Focus();
                return false;
            }
            if (MobjclsBLLOvertimePolicy.CheckDuplicate(BLLOvertimePolicy.DTOOvertimePolicy.intOTPolicyID, txtOTPolicyName.Text.Trim()))
            {
                errProOTPolicy.SetError(txtOTPolicyName, UserMessage.GetMessageByCode(8025));
                UserMessage.ShowMessage(8025);
                txtOverTimeRate.Focus();
                return false;
            }


            return true;

        }


        #endregion

        private void FrmOvertimePolicy_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadCombos(0);
            LoadInitial();

            if (PintOTPolicyID > 0)
            {
                MobjclsBLLOvertimePolicy.DTOOvertimePolicy.intOTPolicyID = PintOTPolicyID;
                MintRecordCnt = MobjclsBLLOvertimePolicy.GetRecordCount();
                MintCurrentRecCnt = MobjclsBLLOvertimePolicy.GetRowNumber();
                //MintCurrentRecCnt = MobjclsBLLOvertimePolicy.GetRecordCount();
                DisplayOTPolicyInfo();

            }
            else
            {
                AddNewOTPolicy();
            }
            txtOTPolicyName.Select();
            MblnIsFromOK = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboCalculationBased_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboCalculationBased.SelectedValue.ToInt32() == 2)
                {
                    dgvParticulars.Enabled = true;
                    LoadAdditionsList();
                }
                else
                {
                    dgvParticulars.Rows.Clear();
                    dgvParticulars.Enabled = false;
                }
                Changestatus();
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);

            }
        }

        private void chkCalculationPercentage_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCalculationPercentage.Checked == true)
            {
                //lblWorkingHoursOrYear.Visible = true;
                //txtWorkingHoursOrYear.Visible = true; 
                txtCalculationPercent.Enabled = true;
                txtCalculationPercent.BackColor = System.Drawing.SystemColors.Info;
                //if (ClsCommonSettings.IsArabicView)
                //    lblWorkingHoursOrYear.Text = " ساعات العمل / يوم";
                //else
                    lblWorkingHoursOrYear.Text = "Working Hours/Day";
                chkRateOnly.Checked = false;
            }
            else
            {
                //lblWorkingHoursOrYear.Visible = false;
                //txtWorkingHoursOrYear.Visible = false; 
                txtCalculationPercent.Text = "";
                txtCalculationPercent.BackColor = System.Drawing.SystemColors.Window;
                txtCalculationPercent.Enabled = false;
                //if (ClsCommonSettings.IsArabicView)
                //    lblWorkingHoursOrYear.Text = " ساعات العمل / يوم";
                //else
                    lblWorkingHoursOrYear.Text = "Working Hours/Day";

            }
            Changestatus();
        }

        private void chkRateOnly_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRateOnly.Checked == true)
            {
                chkCalculationPercentage.Checked = false;
                rdbCompanyBased.Enabled = false;
                rdbActualMonth.Enabled = false;
                //rdbWorkingDays.Enabled = false;
                txtWorkingHoursOrYear.Text = "";
                txtWorkingHoursOrYear.Enabled = false;
                txtOverTimeRate.Enabled = true;
                txtOverTimeRate.BackColor = System.Drawing.SystemColors.Info;
                txtWorkingHoursOrYear.BackColor = System.Drawing.SystemColors.Window;
                cboCalculationBased.SelectedIndex = -1;
                cboCalculationBased.Enabled = false;
                dgvParticulars.ReadOnly = true;
            }
            else
            {
                chkCalculationPercentage.Checked = true;
                rdbCompanyBased.Enabled = true;
                rdbActualMonth.Enabled = true;
                //rdbWorkingDays.Enabled = true;
                txtWorkingHoursOrYear.Enabled = true;
                txtOverTimeRate.Text = "";
                txtOverTimeRate.Enabled = false;
                txtOverTimeRate.BackColor = System.Drawing.SystemColors.Window;
                txtWorkingHoursOrYear.BackColor = System.Drawing.SystemColors.Info;
                txtWorkingHoursOrYear.Text = "";
                cboCalculationBased.Enabled = true;
                dgvParticulars.ReadOnly = false;

            }
            Changestatus();

        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            try
            {
                AddNewOTPolicy();
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);

            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveOvertimePolicy())
            {
              
                MblnIsFromOK = true;
            }

        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    DisplayOTPolicyInfo();
                    lblStatus.Text = UserMessage.GetMessageByCode(10);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 1;
            }
            MblnIsFromOK = true;
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;
                DisplayOTPolicyInfo();
                lblStatus.Text = UserMessage.GetMessageByCode(9);
                tmrClear.Enabled = true;

            }
            MblnIsFromOK = true;
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                }

                if (MintCurrentRecCnt > MintRecordCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                }
                else
                {
                    DisplayOTPolicyInfo();
                    lblStatus.Text = UserMessage.GetMessageByCode(11);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 0;
            }
            MblnIsFromOK = true;
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    DisplayOTPolicyInfo();
                    lblStatus.Text = UserMessage.GetMessageByCode(12);
                    tmrClear.Enabled = true;
                }
            }
            MblnIsFromOK = true;
        }

        private void txtOTPolicyName_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtCalculationPercent_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void rdbCompanyBased_CheckedChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtWorkingHoursOrYear_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtOverTimeRate_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveOvertimePolicy())
            {
                MblnIsFromOK = true;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveOvertimePolicy())
            {
                btnSave.Enabled = false;
                this.Close();
            }
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (DeleteOTPolicy())
            {
                UserMessage.ShowMessage(8023);
                lblStatus.Text = UserMessage.GetMessageByCode(8023);
                AddNewOTPolicy();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            AddNewOTPolicy();
        }

        private void dgvParticulars_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Changestatus();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = "OvertimePolicy";
                ObjViewer.PiRecId = BLLOvertimePolicy.DTOOvertimePolicy.intOTPolicyID;
                ObjViewer.PiFormID = (int)FormID.OvertimePolicy;
                ObjViewer.ShowDialog();
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);

            }


        }

        private void dgvParticulars_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvParticulars.IsCurrentCellDirty)
                {

                    dgvParticulars.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }

        private void dgvParticulars_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch
            {
            }
        }

        private void FrmOvertimePolicy_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnSave.Enabled)
            {

                if (UserMessage.ShowMessage(8) == true)
                {

                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void rdbActualMonth_CheckedChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void rdbWorkingDays_CheckedChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "SalaryStructure";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void FrmOvertimePolicy_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    
                    case Keys.Escape:
                        this.Close();
                        break;

                    case Keys.Control | Keys.Enter:
                        if (BindingNavigatorAddNewItem.Enabled) BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (BindingNavigatorDeleteItem.Enabled) BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (btnClear.Enabled) btnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        if (BindingNavigatorMovePreviousItem.Enabled) BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        if (BindingNavigatorMoveNextItem.Enabled) BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        if (BindingNavigatorMoveFirstItem.Enabled) BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        if (BindingNavigatorMoveLastItem.Enabled) BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (btnPrint.Enabled) btnPrint_Click(sender, new EventArgs());//Print
                        break;
                   
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
           
        }
    }
}
