﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.FileIO ;
namespace MyBooksERP
{
    public partial class FrmVacationExtension : Form
    {
        public String Remarks;
        public Double Days;
        public  Double VacationDays;
        public DateTime  ActualDateTime;
        public DateTime ToDateTime;

        public bool IsPaid;
        ArrayList MaMessageArr ;
        public bool IsFromOK = false;
        private bool MblnShowErrorMess;
        private string MstrMessageCommon; //Messagebox display
        private string MstrMessageCaption;
        private bool Glb24HourFormat;
        private int MintTimerInterval;                  // To set timer interval
        private int MintCompanyId;                      // current companyid
        private int MintUserId;

        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnAddUpdatePermission = false; // To set add/update permission
        private bool MblnPrintEmailPermission = false;//To Set PrintEmail Permission


        private MessageBoxIcon MmessageIcon;            // to set the message icon
        private ArrayList MsarMessageArr; // Error Message display
        private ArrayList MsarStatusMessage;// Status bar error message display
        ClsLogWriter mObjLogs;
        ClsNotification mObjNotification;

        public FrmVacationExtension()
        {
            InitializeComponent();
            MblnShowErrorMess = true;
            MintCompanyId = ClsCommonSettings.LoginCompanyID;
            MintUserId = ClsCommonSettings.UserID;
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            Glb24HourFormat = ClsCommonSettings.Glb24HourFormat;
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls(); 
            //}
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Remarks = "";
            Days = 0.0;
            this.Close();
        }
        #region SetArabicControls
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.VacationExtension, this);
        //}
        #endregion SetArabicControls
        private void btnOK_Click(object sender, EventArgs e)
        {
        IsFromOK = true;

        if (txtDays.Text.Trim() =="" || txtDays.Text.Trim() == ".") 
        {
            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr,7562, out MmessageIcon);
            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information); 
            txtDays.Focus();
            return;
        }

        if (rbLess.Checked==true && Convert.ToDouble(txtDays.Text.Trim()) > VacationDays )
        {
            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr,7569, out MmessageIcon);
            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            txtDays.Focus();
            return;
        }

        if (rbAdd.Checked==true && Convert.ToDouble(txtDays.Text.Trim()) > 180 )
        {
            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr,7570, out MmessageIcon);
            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            txtDays.Focus();
            return;
        }

        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr,7563, out MmessageIcon);
        if (rbLess.Checked==true) 
        {
            MstrMessageCommon = MstrMessageCommon.Replace("extend", "shorten");
        }


        if (dtpRejoinDate.Checked == true)
        {
            int  DayAd=0;
            if (rbLess.Checked == true)
            {
                DayAd = txtDays.Text.ToInt32() * -1;
            }
            else
            {
                DayAd = txtDays.Text.ToInt32();
            }

            if (dtpRejoinDate.Value.ToString("dd-MMM-yyyy").ToDateTime()   < (ToDateTime.AddDays(DayAd)))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7582, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                dtpRejoinDate.Focus();
                return;
            }
        }

        if( MessageBox.Show(MstrMessageCommon, MstrMessageCaption , MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No )
        {
           return;
        }

        Remarks = txtRemarks.Text;
        if (rbAdd.Checked ==true)
        {
            Days = Convert.ToDouble(txtDays.Text);
            IsPaid = true;
        }
        else
        {
            Days = Convert.ToDouble(txtDays.Text) * -1;
            IsPaid = false;
        }

        if (dtpRejoinDate.Checked == true)
        {
            ActualDateTime = dtpRejoinDate.Value.ToString("dd-MMM-yyyy").ToDateTime(); 
        }
        else
        {
            ActualDateTime = "01-Jan-1900".ToDateTime();
        }



        this.Close();
        }


        private void SetPermissions()
        {
            
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.VacationProcess, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            btnOK.Enabled = MblnAddPermission;
        }
        private void LoadMessage()
        {
            // Loading Message
            try
            {
                MsarMessageArr = new ArrayList();
                MsarMessageArr = mObjNotification.FillMessageArray((int)FormID.VacationEntry, 2);
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on LoadMessage " + this.Name + " " + Ex.Message.ToString(), 1);
            }

        }

        private void FrmVacationExtension_Load(object sender, EventArgs e)
        {
            LoadMessage();
            SetPermissions();
            txtRemarks.Text = Remarks;



    
                rbAdd.Enabled = true;
                rbLess.Enabled = true;
                txtDays.Enabled = true;
                dtpRejoinDate.Enabled = true;
                txtRemarks.Enabled = true;




                if (Days < 0) 
                {
                    rbLess.Checked = true;
                }
                else
                {
                    rbAdd.Checked = true;
                }

                if (ActualDateTime != "01-Jan-1900".ToDateTime())
                {
                    dtpRejoinDate.Checked = true;
                    dtpRejoinDate.Value = ActualDateTime;
                }
                else
                {

                    dtpRejoinDate.Value = "01-Jan-1900".ToDateTime();
                    dtpRejoinDate.Checked = false;
                }


            txtDays.Text = Math.Abs(Days).ToString();
        }

        private void FrmVacationExtension_FormClosing(object sender, FormClosingEventArgs e)
        {
             if (IsFromOK == false) 
             {
                   MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr,4214, out MmessageIcon);
                   if (MessageBox.Show(MstrMessageCommon, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                         e.Cancel = false;
                    }
                else
                     {
                        e.Cancel = true;
                     }
             }
        }

        private void FrmVacationExtension_Shown(object sender, EventArgs e)
        {
            txtDays.Focus();
        }

        private void txtDays_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~.!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        
        }

    }
}
