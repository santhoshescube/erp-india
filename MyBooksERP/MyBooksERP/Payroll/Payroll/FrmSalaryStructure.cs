﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmSalaryStructure : Form
    {
        #region Declarations
        private bool MblnPrintEmailPermission = false;     //To set Print Email Permission
        private bool MblnAddPermission = false;            //To set Add Permission
        private bool MblnUpdatePermission = false;         //To set Update Permission
        private bool MblnDeletePermission = false;        //To set Delete Permission
        public bool MblnIsEditMode = false;               //To check add status
        public long PintEmployeeID = 0;
        public int PintCompanyID = 0;
        private string MstrSearch = "";
        private int MintScale = 0;
        private int MintRecordCnt = 0;                    //Total record count
        private int MintCurrentRecCnt = 0;                //Current record
        private int MintRowNumber = -1;                   //To set row number
        private bool MblnIsFromOK = true;

        private clsBLLSalaryStructure MobjclsBLLSalaryStructure = null;
        private clsNavigatorBLL MobjclsNavigatorBLL = null;
        private clsMessage objUserMessage = null;
        string strBindingOf = "Of ";
        #endregion Declarations

        #region Constructor
        public FrmSalaryStructure()
        {
            InitializeComponent();
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls(); 
            //}
        }
        #endregion Constructor

        #region Properties
        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.SalaryStructure);
                return this.objUserMessage;
            }
        }

        private clsBLLSalaryStructure BLLSalaryStructure 
        {
            get
            {
                if (this.MobjclsBLLSalaryStructure == null)
                    this.MobjclsBLLSalaryStructure = new clsBLLSalaryStructure();
                return this.MobjclsBLLSalaryStructure;
            }
        }

        private clsNavigatorBLL  BLLNavigator
        {
            get
            {
                if (this.MobjclsNavigatorBLL == null)
                    this.MobjclsNavigatorBLL = new clsNavigatorBLL ();
                return this.MobjclsNavigatorBLL;
            }
        }
        #endregion Properties

        #region Methods

        private void SetPermissions()
        {
            DataTable dt = null;
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.SalaryStructure, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                dt = objClsBLLPermissionSettings.GetMenuPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID);
                if (dt.Rows.Count == 0)
                    btnAdditionDeduction.Enabled = false;
                else
                {   
                    // -------------------------------  Addition Deduction Permission -----------------
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AdditionDeduction).ToString();
                    btnAdditionDeduction.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);
                }
            }
            else
            {
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            }
        }

        #region LoadCombos
        /// <summary>
        /// Loads the combos according to intType
        /// </summary>
        /// <param name="intType">
        /// intType=0 -> Fill all combos
        /// intType=1 -> Fills Company Combo
        /// intType=2 -> Fills Employee
        /// intType=3 -> Fills Currency
        /// intType=4 -> Fills Payment Mode
        /// intType=5 -> Fills Payment Calculation
        /// intType=6 -> Fills Absent policy
        /// intType=7 -> Fills OT Policy
        /// intType=8 -> Fills Addition deduction
        /// intType=9 -> Fills Employee
        /// intType=10 -> Fills settlement policy
        /// intType=11 -> Fills holiday policy
        /// intType=12 -> Fills encash policy
        /// intType=13 -> Fills other remuneration
        /// </param>
        /// <returns>success/failure</returns>
        private bool LoadCombos(int intType)
        {
            bool blnRetvalue = false;
            DataTable datCombos = new DataTable();
            try
            {
                if (intType == 0 || intType == 1)//Company Combo
                {
                    datCombos = BLLSalaryStructure.FillCombos(new string[] { "CompanyID,CompanyName as Name", "CompanyMaster", "CompanyID IN(select CompanyID from UserCompanyDetails where UserID = "+ ClsCommonSettings.UserID + " and CompanyID = "+ ClsCommonSettings.LoginCompanyID  + ")" });
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "Name";
                    cboCompany.DataSource = datCombos;
                    datCombos = null;
                    blnRetvalue = true;
                }
                if (intType == 2)//Employee Combo
                {
                    datCombos = null;
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    if (!MblnIsEditMode)
                    //        datCombos = BLLSalaryStructure.FillCombos(new string[] { "EmployeeID,EmployeeFullNameArb + '-' + EmployeeNumber as EmployeeName", "EmployeeMaster Where  WorkStatusID>5 AND CompanyID = " + cboCompany.SelectedValue.ToInt32(), "" });
                    //    else
                    //        datCombos = BLLSalaryStructure.FillCombos(new string[] { "EmployeeID,EmployeeFullNameArb + '-' + EmployeeNumber as EmployeeName", "EmployeeMaster Where CompanyID = " + cboCompany.SelectedValue.ToInt32(), "" });

                    //}
                    //else
                    //{
                        if (!MblnIsEditMode)
                            datCombos = BLLSalaryStructure.FillCombos(new string[] { "EmployeeID,EmployeeFullName + '-' + EmployeeNumber as EmployeeName", "EmployeeMaster Where  WorkStatusID>5 AND CompanyID = " + cboCompany.SelectedValue.ToInt32(), "" });
                        else
                            datCombos = BLLSalaryStructure.FillCombos(new string[] { "EmployeeID,EmployeeFullName + '-' + EmployeeNumber as EmployeeName", "EmployeeMaster Where CompanyID = " + cboCompany.SelectedValue.ToInt32(), "" });

                    //} 
                    cboEmployee.ValueMember = "EmployeeID";
                    cboEmployee.DisplayMember = "EmployeeName";
                    cboEmployee.DataSource = datCombos;
                    blnRetvalue = true;
                }
                if (intType == 0 || intType == 3)
                {
                    cboCurrency.Text = "";
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    datCombos = BLLSalaryStructure.FillCombos(new string[] { "DISTINCT CR.CurrencyID,CR.CurrencyNameArb as Description", "CompanyMaster CD Left Join CurrencyReference CR on CR.CurrencyID = CD.CurrencyID where CD.CompanyID = " + cboCompany.SelectedValue.ToInt32(), "" });

                    //}
                    //else
                    //{
                        datCombos = BLLSalaryStructure.FillCombos(new string[] { "DISTINCT CR.CurrencyID,CR.CurrencyName as Description", "CompanyMaster CD Left Join CurrencyReference CR on CR.CurrencyID = CD.CurrencyID where CD.CompanyID = " + cboCompany.SelectedValue.ToInt32(), "" });
                    //}
                    cboCurrency.ValueMember = "CurrencyID";
                    cboCurrency.DisplayMember = "Description";
                    cboCurrency.DataSource = datCombos;
                    datCombos = null;
                    blnRetvalue = true;
                }
                if (intType == 0 || intType == 4)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    datCombos = BLLSalaryStructure.FillCombos(new string[] { "PaymentClassificationID,PaymentClassificationArb AS PaymentClassification", "PaymentClassificationReference Where PaymentClassificationID = 1", "" });

                    //}
                    //else
                    //{
                        datCombos = BLLSalaryStructure.FillCombos(new string[] { "PaymentClassificationID,PaymentClassification", "PaymentClassificationReference Where PaymentClassificationID = 1", "" });
                    //}
                    cboPayClassification.ValueMember = "PaymentClassificationID";
                    cboPayClassification.DisplayMember = "PaymentClassification";
                    cboPayClassification.DataSource = datCombos;
                    datCombos = null;
                    blnRetvalue = true;
                }
                if (intType == 0 || intType == 5)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    datCombos = BLLSalaryStructure.FillCombos(new string[] { "PayCalculationTypeID,PayCalculationTypeArb AS PayCalculationType", "PaymentCalculationTypeReference Where PayCalculationTypeID = 1", "" });

                    //}
                    //else
                    //{
                        datCombos = BLLSalaryStructure.FillCombos(new string[] { "PayCalculationTypeID,PayCalculationType", "PaymentCalculationTypeReference Where PayCalculationTypeID = 1", "" });
                    //}
                    cboPayCalculation.ValueMember = "PayCalculationTypeID";
                    cboPayCalculation.DisplayMember = "PayCalculationType";
                    cboPayCalculation.DataSource = datCombos;
                    datCombos = null;
                    blnRetvalue = true;
                }

                if (intType == 0 || intType == 6)//
                {
                    cboAbsentPolicy.Text = "";
                    datCombos = BLLSalaryStructure.FillCombos(new string[] { "AbsentPolicyID,AbsentPolicy", "PayAbsentPolicyMaster", "" });
                    cboAbsentPolicy.ValueMember = "AbsentPolicyID";
                    cboAbsentPolicy.DisplayMember = "AbsentPolicy";
                    cboAbsentPolicy.DataSource = datCombos;
                    datCombos = null;
                    blnRetvalue = true;
                }
                if (intType == 0 || intType == 7)//
                {
                    cboOvertimePolicy.Text = "";
                    datCombos = BLLSalaryStructure.FillCombos(new string[] { "OTPolicyID,OTPolicy", "PayOTPolicyMaster", "" });
                    cboOvertimePolicy.ValueMember = "OTPolicyID";
                    cboOvertimePolicy.DisplayMember = "OTPolicy";
                    cboOvertimePolicy.DataSource = datCombos;
                    datCombos = null;
                    blnRetvalue = true;
                }
                if(intType == 0 || intType == 8)//
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    datCombos = BLLSalaryStructure.FillCombos(new string[] { "AdditionDeductionID,AdditionDeductionArb + (CASE WHEN IsAddition = 1 THEN ' (+)' ELSE ' (-)' END) as Particulars", "PayAdditionDeductionReference where (IsPredefined = 0  Or AdditionDeductionID in(1))", "" });

                    //}
                    //else
                    //{
                        datCombos = BLLSalaryStructure.FillCombos(new string[] { "AdditionDeductionID,AdditionDeduction + (CASE WHEN IsAddition = 1 THEN ' (+)' ELSE ' (-)' END) as Particulars", "PayAdditionDeductionReference where (IsPredefined = 0  Or AdditionDeductionID in(1))", "" });
                    //}
                    dgvColParticulars.ValueMember = "AdditionDeductionID";
                    dgvColParticulars.DisplayMember = "Particulars";
                    dgvColParticulars.DataSource = datCombos;
                    datCombos = null;
                    blnRetvalue = true;
                }
                if (intType == 0 || intType == 9)
                {
                    datCombos = null;
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    if (!MblnIsEditMode)
                    //        datCombos = BLLSalaryStructure.FillCombos(new string[] { "EmployeeID,EmployeeFullNameArb + '-' + EmployeeNumber as EmployeeName", "EmployeeMaster Where CompanyID = " + cboCompany.SelectedValue.ToInt32() + "and EmployeeID not in(SELECT EmployeeID FROM PaySalaryStructure) AND WorkStatusID>5", "" });
                    //    else
                    //        datCombos = BLLSalaryStructure.FillCombos(new string[] { "EmployeeID,EmployeeFullNameArb + '-' + EmployeeNumber as EmployeeName", "EmployeeMaster Where CompanyID = " + cboCompany.SelectedValue.ToInt32() + "and EmployeeID not in(SELECT EmployeeID FROM PaySalaryStructure)", "" });

                    //}
                    //else
                    //{
                        if (!MblnIsEditMode)
                            datCombos = BLLSalaryStructure.FillCombos(new string[] { "EmployeeID,EmployeeFullName + '-' + EmployeeNumber as EmployeeName", "EmployeeMaster Where CompanyID = " + cboCompany.SelectedValue.ToInt32() + "and EmployeeID not in(SELECT EmployeeID FROM PaySalaryStructure) AND WorkStatusID>5", "" });
                        else
                            datCombos = BLLSalaryStructure.FillCombos(new string[] { "EmployeeID,EmployeeFullName + '-' + EmployeeNumber as EmployeeName", "EmployeeMaster Where CompanyID = " + cboCompany.SelectedValue.ToInt32() + "and EmployeeID not in(SELECT EmployeeID FROM PaySalaryStructure)", "" });
                    //}
             
                    cboEmployee.DataSource = null;
                    cboEmployee.ValueMember = "EmployeeID";
                    cboEmployee.DisplayMember = "EmployeeName";
                    cboEmployee.DataSource = datCombos;
                    blnRetvalue = true;
                }
                //if (intType == 0 || intType == 10)
                //{
                //    datCombos = null;
                //    datCombos = BLLSalaryStructure.FillCombos(new string[] { "SetPolicyID,DescriptionPolicy", "PaySettlementPolicy", "" });
                //    cboSettlement.DataSource = null;
                //    cboSettlement.ValueMember = "SetPolicyID";
                //    cboSettlement.DisplayMember = "DescriptionPolicy";
                //    cboSettlement.DataSource = datCombos;                    
                //    blnRetvalue = true;
                //}
                if (intType == 0 || intType == 11)
                {
                    cboHolidayPolicy.Text = "";
                    datCombos = null;
                    datCombos = BLLSalaryStructure.FillCombos(new string[] { "HolidayPolicyID,HolidayPolicy", "PayHolidayPolicyMaster", "" });
                    cboHolidayPolicy.DataSource = null;
                    cboHolidayPolicy.ValueMember = "HolidayPolicyID";
                    cboHolidayPolicy.DisplayMember = "HolidayPolicy";
                    cboHolidayPolicy.DataSource = datCombos;                  
                    blnRetvalue = true;
                }
                if (intType == 0 || intType == 12)
                {
                    cboEncashPolicy.Text = "";
                    datCombos = null;
                    datCombos = BLLSalaryStructure.FillCombos(new string[] { "EncashPolicyID,EncashPolicy", "PayEncashPolicyMaster", "" });
                    cboEncashPolicy.DataSource = null;
                    cboEncashPolicy.ValueMember = "EncashPolicyID";
                    cboEncashPolicy.DisplayMember = "EncashPolicy";
                    cboEncashPolicy.DataSource = datCombos;                   
                    blnRetvalue = true;
                }
                if (intType == 0 || intType == 13)//Filling OtherRemuneration
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    datCombos = BLLSalaryStructure.FillCombos(new string[] { "OtherRemunerationID,OtherRemunerationArb AS OtherRemuneration", "OtherRemunerationReference", "" });
                    //}
                    //else
                    //{
                        datCombos = BLLSalaryStructure.FillCombos(new string[] { "OtherRemunerationID,OtherRemuneration", "OtherRemunerationReference", "" });

                    //}
                    OtherRemunerationParticular.ValueMember = "OtherRemunerationID";
                    OtherRemunerationParticular.DisplayMember = "OtherRemuneration";
                    OtherRemunerationParticular.DataSource = datCombos;
                    datCombos = null;
                    blnRetvalue = true;
                }



                if (intType == 0 || intType == 14)//Filling Category Type
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    datCombos = BLLSalaryStructure.FillCombos(new string[] { "CategoryID,AmountCategoryArb as AmountCategory", "PayCategoryTypeReference", "" });
                    //}
                    //else
                    //{
                        datCombos = BLLSalaryStructure.FillCombos(new string[] { "CategoryID,AmountCategory", "PayCategoryTypeReference", "" });
                    //}
                    CboCategory.ValueMember = "CategoryID";
                    CboCategory.DisplayMember = "AmountCategory";
                    CboCategory.DataSource = datCombos;
                    datCombos = null;
                    //dgvParticulars.Rows[0].Cells["CboCategory"].Value = 1;
                    blnRetvalue = true;
                }


            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
                blnRetvalue = false;
            }
            return blnRetvalue;
        }
        #endregion LoadCombos

        #region SetArabicControls
            //private void SetArabicControls()
            //{
            //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            //    objDAL.SetArabicVersion((int)FormID.SalaryStructure, this);

            //    strBindingOf = "من ";
            //    txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            //    label12.Text = "شركة";

            //    dgvParticulars.Columns["dgvColParticulars"].HeaderText = "تفاصيل";
            //    dgvParticulars.Columns["CboCategory"].HeaderText = "الفئة";
            //    dgvParticulars.Columns["dgvColAmount"].HeaderText = "مبلغ";
            //    dgvParticulars.Columns["dgvColDeductionPolicy"].HeaderText = "سياسة خصم";
            //    dgvOtherRemuneration.Columns["OtherRemunerationParticular"].HeaderText = "تفاصيل";
            //    dgvOtherRemuneration.Columns["OtherRemunerationAmount"].HeaderText = "مبلغ";
            //}
        #endregion SetArabicControls

        #region CheckBasicPay
        /// <summary>
        /// Check if basic pay is selected
        /// </summary>
        /// <returns></returns>
        /// 

        private bool CheckBasicPay()
        {
            bool blnRetValue = false;
            try
            {
                foreach (DataGridViewRow rowValue in dgvParticulars.Rows)
                {
                    if (rowValue.Cells["dgvColParticulars"].Value.ToStringCustom() != string.Empty)
                    {
                        if (rowValue.Cells["dgvColParticulars"].Value.ToInt32() ==(int) CalculationType.BasicPay)
                        {
                            blnRetValue = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
                blnRetValue = false;
            }
            return blnRetValue;
        }
        #endregion CheckBasicPay

        #region CheckDuplicateAddDedID
        /// <summary>
        /// check if duplicate addition deduction is selected in grid
        /// </summary>
        /// <param name="ColIndex"></param>
        /// <returns></returns>
        private int CheckDuplicateAddDedID(int ColIndex)
        {
            string SearchValue = "";
            int RowIndexTemp = 0;
            int CheckDuplicateAddDedID = -1;
            try
            {
                foreach (DataGridViewRow rowValue in dgvParticulars.Rows)
                {
                    if (rowValue.Cells[ColIndex].Value.ToStringCustom() != string.Empty)
                    {
                        SearchValue = rowValue.Cells[ColIndex].Value.ToStringCustom();
                        RowIndexTemp = rowValue.Index;
                        foreach (DataGridViewRow row in dgvParticulars.Rows)
                        {
                            if (RowIndexTemp != row.Index)
                            {
                                if (row.Cells[ColIndex].Value.ToStringCustom() != string.Empty)
                                {
                                    if (row.Cells[ColIndex].Value.ToString() == SearchValue)
                                    {
                                        CheckDuplicateAddDedID = row.Index;
                                        dgvParticulars.CurrentCell = dgvParticulars[ColIndex, CheckDuplicateAddDedID];
                                        return CheckDuplicateAddDedID;
                                    }
                                }
                            }
                        }
                    }
                }
                return CheckDuplicateAddDedID;
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
                return CheckDuplicateAddDedID;
            }
        }
        #endregion CheckDuplicateAddDedID

        #region CheckDuplicateOtherRemunerationParticular
        /// <summary>
        /// check if duplicate particualr is selected
        /// </summary>
        /// <param name="ColIndex"></param>
        /// <returns></returns>
        private int CheckDuplicateOtherRemunerationParticular(int ColIndex)
        {
            string SearchValue = "";
            int RowIndexTemp = 0;
            int CheckDuplicateAddDedID = -1;
            try
            {
                foreach (DataGridViewRow rowValue in dgvOtherRemuneration.Rows)
                {
                    if (rowValue.Cells[ColIndex].Value.ToStringCustom() != string.Empty)
                    {
                        SearchValue = rowValue.Cells[ColIndex].Value.ToStringCustom();
                        RowIndexTemp = rowValue.Index;
                        foreach (DataGridViewRow row in dgvOtherRemuneration.Rows)
                        {
                            if (RowIndexTemp != row.Index)
                            {
                                if (row.Cells[ColIndex].Value.ToStringCustom() != string.Empty)
                                {
                                    if (row.Cells[ColIndex].Value.ToString() == SearchValue)
                                    {
                                        CheckDuplicateAddDedID = row.Index;
                                        dgvOtherRemuneration.CurrentCell = dgvOtherRemuneration[ColIndex, CheckDuplicateAddDedID];
                                        return CheckDuplicateAddDedID;
                                    }
                                }
                            }
                        }
                    }
                }
                return CheckDuplicateAddDedID;
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
                return CheckDuplicateAddDedID;
            }
        }
        #endregion CheckDuplicateOtherRemunerationParticular

        #region AddNewSalaryStructure
        /// <summary>
        /// Add new salary
        /// </summary>
        private void AddNewSalaryStructure()
        {
            MblnIsEditMode = false;
            MstrSearch = "";
            tbSalaryStructure.SelectedTab = General;
            LoadCombos(9);
            if (PintCompanyID > 0)
            {
                cboCompany.SelectedValue = PintCompanyID;
                cboEmployee.SelectedValue = PintEmployeeID;
            }
           
            tmrClear.Enabled = true;
            errProSalaryStructure.Clear();
            ClearAllControls();
            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();
            MintRowNumber = -1;
            lblStatus.Text = "";
            BindingNavigatorAddNewItem.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = false;
            btnPrint.Enabled = false;
            btnEmail.Enabled = false;
            btnOk.Enabled = false;
            btnSave.Enabled = false;
            BindingNavigatorSaveItem.Enabled = false;
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = false;
            BindingNavigatorMoveNextItem.Enabled = false;
            //cboCompany.Enabled = true;
            cboEmployee.Enabled = true;
           
            dgvParticulars.Rows.Add();
            dgvParticulars.Rows[0].Cells["dgvColParticulars"].Value = 1.ToInt32();
            //dgvParticulars.Rows[0].Cells["CboCategory"].Value = 1.ToInt32();
            dgvParticulars.Rows[0].Cells["CboCategory"].Value = 1;
            dgvOtherRemuneration.Rows.Clear();
            MblnIsFromOK = true;
        }
        #endregion AddNewSalaryStructure

        #region ClearAllControls
        /// <summary>
        /// Clear all the controls
        /// </summary>
        private void ClearAllControls()
        {
            cboAbsentPolicy.SelectedIndex = - 1;
            cboOvertimePolicy.SelectedIndex = -1;
            txtRemarks.Text = "";
            dgvParticulars.Rows.Clear();
            txtAddition.Text = "";
            txtDeduction.Text = "";
            txtNetAmount.Text = "";
            CboSalaryday.SelectedIndex = 0;
            txtRemarks.Text = "";
           // txtSearch.Text = "";
            cboEncashPolicy.SelectedIndex = -1;
            cboHolidayPolicy.SelectedIndex = -1;
            //cboSettlement.SelectedIndex = -1;
        }
        #endregion ClearAllControls

        void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars;
            
            strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            //if (ClsCommonSettings.IsAmountRoundByZero)
            //{
            //    strInvalidChars = strInvalidChars + ".";
            //}

            e.Handled = false;
            if (((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))) )
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }

        #region SaveSalaryStructure
        /// <summary>
        /// Save salary structure
        /// </summary>
        /// <returns></returns>
        private bool SaveSalaryStructure()
        {
            bool blnRetValue = false;
            if (FormValidation())
            {
                int intMessageCode = 0;
                if (MblnIsEditMode == false)
                    intMessageCode = 1712;
                else
                {
                    if (UpdateValidation())
                    {
                        intMessageCode = 1713;
                    }
                    else
                    {
                        return false;
                    }
                }
                if (UserMessage.ShowMessage(intMessageCode,null))
                {
                   FillParametrSalaryStructure();
                   FillParametrSalaryStructureDetail();
                   blnRetValue = BLLSalaryStructure.SalaryStructureSave(MblnIsEditMode);
                   if (blnRetValue == true)
                   {
                       blnRetValue = BLLSalaryStructure.SalaryStructureDetailSave(MblnIsEditMode);
                       if (blnRetValue == true)
                       {
                           blnRetValue = BLLSalaryStructure.SalaryStructureHistorySave();
                       }
                       else
                       {
                           blnRetValue = false;
                       }
                       if (!MblnIsEditMode)
                       {
                           UserMessage.ShowMessage(2);
                           lblStatus.Text = UserMessage.GetMessageByCode(2);
                       }
                       else
                       {
                           UserMessage.ShowMessage(21);
                           lblStatus.Text = UserMessage.GetMessageByCode(21);
                       }
                       DisplaySalaryStructureInfo();
                   }
                   else
                   {
                       blnRetValue = false;
                   }
                  
                    MblnIsEditMode = true;
                }
            }
            SetAutoCompleteList();
            return blnRetValue;
        }
        #endregion SaveSalaryStructure

        #region FillParametrSalaryStructure
        /// <summary>
        /// Fill master do parameters
        /// </summary>
        private void FillParametrSalaryStructure()
        {
            BLLSalaryStructure.DTOSalaryStructure.intEmployeeID = cboEmployee.SelectedValue.ToInt32();
            BLLSalaryStructure.DTOSalaryStructure.intPaymentClassificationID = cboPayClassification.SelectedValue.ToInt32();
            BLLSalaryStructure.DTOSalaryStructure.intPayCalculationTypeID = cboPayCalculation.SelectedValue.ToInt32();
            BLLSalaryStructure.DTOSalaryStructure.strRemarks  = txtRemarks.Text.ToString();
            BLLSalaryStructure.DTOSalaryStructure.intSalaryDay = CboSalaryday.Text.ToInt32();
            BLLSalaryStructure.DTOSalaryStructure.intCurrencyID = cboCurrency.SelectedValue.ToInt32();                
            BLLSalaryStructure.DTOSalaryStructure.intOTPolicyID = cboOvertimePolicy.SelectedValue.ToInt32();
            BLLSalaryStructure.DTOSalaryStructure.intAbsentPolicyID = cboAbsentPolicy.SelectedValue.ToInt32();
            BLLSalaryStructure.DTOSalaryStructure.decNetAmount = txtNetAmount.Text.ToDecimal();           
            //BLLSalaryStructure.DTOSalaryStructure.intSetPolicyID = cboSettlement.SelectedValue.ToInt32();
            BLLSalaryStructure.DTOSalaryStructure.intHolidayPolicyID = cboHolidayPolicy.SelectedValue.ToInt32();
            BLLSalaryStructure.DTOSalaryStructure.intEncashPolicyID = cboEncashPolicy.SelectedValue.ToInt32();
        }
        #endregion FillParametrSalaryStructure

        #region FillParametrSalaryStructureDetail
        /// <summary>
        /// Set detail dto parameters
        /// </summary>
        private void FillParametrSalaryStructureDetail()
        {
            if (dgvParticulars.Rows.Count > 0)
            {
                BLLSalaryStructure.DTOSalaryStructure.DTOSalaryStructureDetail = new List<clsDTOSalaryStructureDetail>();
                for (int intICounter = 0; intICounter < dgvParticulars.Rows.Count - 1; intICounter++)
                {
                    clsDTOSalaryStructureDetail objDTOSalaryStructureDetail = new clsDTOSalaryStructureDetail();
                    objDTOSalaryStructureDetail.intSerialNo = dgvParticulars.Rows[intICounter].Cells["dgvColSerialNo"].Value.ToInt32();
                    objDTOSalaryStructureDetail.intAdditionDeductionID = dgvParticulars.Rows[intICounter].Cells["dgvColParticulars"].Value.ToInt32();
                    if (dgvParticulars.Rows[intICounter].Cells["dgvColDeductionPolicy"].Tag.ToInt32() > 0)
                    {
                        objDTOSalaryStructureDetail.intAdditionDeductionPolicyID = dgvParticulars.Rows[intICounter].Cells["dgvColDeductionPolicy"].Tag.ToInt32();
                        objDTOSalaryStructureDetail.decAmount = 0;
                    }
                    else
                    {
                        objDTOSalaryStructureDetail.intAdditionDeductionPolicyID = null;
                        objDTOSalaryStructureDetail.decAmount = dgvParticulars.Rows[intICounter].Cells["dgvColAmount"].Value.ToDecimal();
                    }
                    objDTOSalaryStructureDetail.intCategoryID = dgvParticulars.Rows[intICounter].Cells["CboCategory"].Value.ToInt32(); 
                    BLLSalaryStructure.DTOSalaryStructure.DTOSalaryStructureDetail.Add(objDTOSalaryStructureDetail);
                }
            }
            if (dgvOtherRemuneration.Rows.Count > 0)
            {
                BLLSalaryStructure.DTOSalaryStructure.DTORemunerationDetail = new List<clsDTORemunerationDetail>();

                for (int intICounter = 0; intICounter < dgvOtherRemuneration.Rows.Count - 1; intICounter++)
                {
                    clsDTORemunerationDetail ObjRemunerationDetail = new clsDTORemunerationDetail();
                    ObjRemunerationDetail.OtherRemunerationDetailID = dgvOtherRemuneration.Rows[intICounter].Cells["OtherRemunerationParticular"].Tag.ToInt32();
                    ObjRemunerationDetail.AmountRemuneration = dgvOtherRemuneration.Rows[intICounter].Cells["OtherRemunerationAmount"].Value.ToDecimal();
                    //objDTOSalaryStructureDetail.intAdditionDeductionPolicyID = 0;
                    BLLSalaryStructure.DTOSalaryStructure.DTORemunerationDetail.Add(ObjRemunerationDetail);
                }
            }
        }
        #endregion FillParametrSalaryStructureDetail

        #region FormValidation
        /// <summary>
        /// Validations before saving
        /// </summary>
        /// <returns></returns>
        private bool FormValidation()
        {
            errProSalaryStructure.Clear();
            if (cboCompany.SelectedValue == null)
            {
                //Please select a Company or Branch.
                errProSalaryStructure.SetError(cboCompany, UserMessage.GetMessageByCode(1717));
                UserMessage.ShowMessage(1717);
                cboCompany.Focus();
                return false;
            }
            if (cboCompany.SelectedValue.ToInt32() == 0)
            {
                //The data entered in 'Company or Branch' field is invalid.
                errProSalaryStructure.SetError(cboCompany, UserMessage.GetMessageByCode(1718));
                UserMessage.ShowMessage(1718);
                cboCompany.Focus();
                return false;
            }
            if (cboEmployee.SelectedValue == null)
            {
                //Please select an Employee.
                errProSalaryStructure.SetError(cboEmployee, UserMessage.GetMessageByCode(1719));
                UserMessage.ShowMessage(1719);
                cboEmployee.Focus();
                return false;
            }

            if (cboEmployee.SelectedValue.ToInt32() == 0)
            {
                //The selected Employee is invalid.
                errProSalaryStructure.SetError(cboEmployee, UserMessage.GetMessageByCode(1720));
                UserMessage.ShowMessage(1720);
                cboEmployee.Focus();
                return false;
            }

            if (cboCurrency.SelectedValue == null)
            {
                //Please select a Currency.
                errProSalaryStructure.SetError(cboCurrency, UserMessage.GetMessageByCode(1721));
                UserMessage.ShowMessage(1721);
                cboCurrency.Focus();
                return false;
            }
            if (cboCurrency.SelectedValue.ToInt32() == 0)
            {
                //The selected Currency is invalid.
                errProSalaryStructure.SetError(cboCurrency, UserMessage.GetMessageByCode(1722));
                UserMessage.ShowMessage(1722);
                cboCurrency.Focus();
                return false;
            }
            if(!MobjclsBLLSalaryStructure.CheckIfEmployeeIsActive(cboEmployee.SelectedValue.ToInt32()))
            {
                //Settlement exists for the selected employee
                errProSalaryStructure.SetError(cboEmployee, UserMessage.GetMessageByCode(1731));
                UserMessage.ShowMessage(1731);
                cboEmployee.Focus();
                return false;
            }

            if (MblnIsEditMode && MobjclsBLLSalaryStructure.CheckIfEmployeeSalaryProcessed(cboEmployee.SelectedValue.ToInt32()))
            {
                //Sorry, cannot update this employee's 'Salary Structure'.  Salary Processed without release.
                UserMessage.ShowMessage(1732, null);
                return false;
            }

            if (dgvParticulars.RowCount >=0)
            {
                dgvParticulars.CurrentCell = dgvParticulars[dgvColParticulars.Index, 0];
            }

            if (dgvOtherRemuneration.RowCount >= 0)
            {
                dgvOtherRemuneration.CurrentCell = dgvOtherRemuneration[OtherRemunerationParticular.Index, 0];
            }

            if (CheckBasicPay() == false)
            {
                //Please select Basic Pay
                UserMessage.ShowMessage(1728,null);
                return false;
            }

            foreach (DataGridViewRow rowValue in dgvParticulars.Rows)
            {
                if (rowValue.Cells[dgvColParticulars.Index].Value.ToStringCustom() == string.Empty && rowValue.Cells[dgvColDeductionPolicy.Index].Value.ToStringCustom() != string.Empty)
                {
                    //Please select 'Particulars'.
                    UserMessage.ShowMessage(1658);
                    tbSalaryStructure.SelectedTab = General;
                    dgvParticulars.CurrentCell = rowValue.Cells[dgvColParticulars.Index];
                    return false;
                }

                if (Convert.ToInt32(rowValue.Cells[dgvColParticulars.Index].Value) == 1 && Convert.ToInt32(rowValue.Cells[CboCategory.Index].Value) != 1)
                {
                    //Please select 'Category'.
                    UserMessage.ShowMessage(20020);
                    tbSalaryStructure.SelectedTab = General;
                    dgvParticulars.CurrentCell = rowValue.Cells[CboCategory.Index];
                    return false;
                }

                if (rowValue.Cells[dgvColDeductionPolicy.Index].Tag.ToInt32() < 0 && rowValue.Cells[dgvColParticulars.Index].Value.ToStringCustom() == "1" && rowValue.Cells[dgvColAmount.Index].Value.ToDecimal() == 0)
                {
                    //Please Enter BasicPay Amount
                    UserMessage.ShowMessage(2216);
                    tbSalaryStructure.SelectedTab = General;
                    dgvParticulars.CurrentCell = rowValue.Cells[dgvColParticulars.Index];
                    return false;
                }
                if (rowValue.Cells[dgvColAmount.Index].Value.ToDecimal() == 0 && rowValue.Cells[dgvColDeductionPolicy.Index].Value.ToStringCustom() =="None")
                {
                    if (rowValue.Cells["dgvColIsAddition"].Value.ToInt32() == 1)//Please enter the Addition amount
                        UserMessage.ShowMessage(1659);
                    else//Please enter deduction amount or deduction policy
                        UserMessage.ShowMessage(1661);
                    dgvParticulars.CurrentCell = rowValue.Cells[dgvColAmount.Index];
                    tbSalaryStructure.SelectedTab = General;
                    return false;
                }
            }
            if (CheckDuplicateAddDedID(dgvColParticulars.Index) != -1)
            {
                //Please avoid duplicate values in particulars
                UserMessage.ShowMessage(1727);
                tbSalaryStructure.SelectedTab = General;
                return false;
            }
            foreach (DataGridViewRow rowValue in dgvOtherRemuneration.Rows)
            {
                if (rowValue.Cells[OtherRemunerationParticular.Index].Value.ToInt32() > 0 && rowValue.Cells[OtherRemunerationAmount.Index].Value.ToStringCustom() == string.Empty)
                {
                    //Please Enter Remuneration Amount
                    UserMessage.ShowMessage(2215);
                    tbSalaryStructure.SelectedTab = OtherRemuneration;
                    dgvOtherRemuneration.Focus();
                    return false;
                }
                if (rowValue.Cells[OtherRemunerationAmount.Index].Value.ToStringCustom() != string.Empty && rowValue.Cells[OtherRemunerationParticular.Index].Value.ToStringCustom() == string.Empty)
                {
                    //Please select 'Particulars'.
                    UserMessage.ShowMessage(1658);
                    tbSalaryStructure.SelectedTab = OtherRemuneration;
                    dgvOtherRemuneration.Focus();
                    return false;
                }
            }
            if (CheckDuplicateOtherRemunerationParticular(OtherRemunerationParticular.Index) != -1)
            {
                //Please avoid duplicate values in particulars
                UserMessage.ShowMessage(1727);
                tbSalaryStructure.SelectedTab = OtherRemuneration;
                return false;
            }
            if(txtNetAmount.Text.ToDecimal() < 0)
            {
                //Net Amount cannot be negative
                UserMessage.ShowMessage(1730);
                return false;
            }
            return true;
        }
        #endregion FormValidation

        #region GetPreviousEmployeeInformation
        /// <summary>
        /// Get information of prev employee
        /// </summary>
        /// <param name="blnFirstItem">check whether first item or not</param>
        private void GetPreviousEmployeeInformation(bool blnFirstItem)
        {            
                if (blnFirstItem)
                    MintRowNumber = 1;
                else
                    MintRowNumber--;
                EventArgs e = new EventArgs();
                // Calling searching event
                btnSearch_Click(btnSearch, e);
        }
        #endregion GetPreviousEmployeeInformation


        private void GetNextExecutiveInformation(bool bLastItem)
        {
            //GetRecordCount();
           
                if (bLastItem)
                {
                    MintRowNumber =  MintRecordCnt;
                }
                else if (Convert.ToInt32(BindingNavigatorPositionItem.Text) < MintRecordCnt)
                {
                    MintRowNumber++;
                }

                EventArgs e = new EventArgs();
                // Calling searching event
                btnSearch_Click(btnSearch, e);
        }
        private void DisplaySalaryStructureInfo()
        {
            tbSalaryStructure.SelectedTab = General;
            MblnIsEditMode = true;
            FillSalaryStructureMasterInfo();

           // cboCompany.Enabled = false;
            cboEmployee.Enabled = true;
            BindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            btnPrint.Enabled = MblnPrintEmailPermission;
            btnEmail.Enabled = MblnPrintEmailPermission;
            BindingNavigatorSaveItem.Enabled = false;
            btnSave.Enabled = false;
            btnOk.Enabled = false;
            SetBindingNavigatorButtons();
        }

        private void FillSalaryStructureMasterInfo()
        {
            ClearAllControls();
            if (BLLSalaryStructure.DisplaySalaryStructureMaster(MintCurrentRecCnt, MstrSearch, cboCompany.SelectedValue.ToInt32()))
            {
                cboCompany.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intCompanyID;
                LoadCombos(2);
                LoadCombos(3);
                cboEmployee.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intEmployeeID;
                cboPayClassification.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intPaymentClassificationID;
                cboPayCalculation.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intPayCalculationTypeID;
                txtRemarks.Text =BLLSalaryStructure.DTOSalaryStructure.strRemarks;
                CboSalaryday.Text = BLLSalaryStructure.DTOSalaryStructure.intSalaryDay.ToString();
                cboCurrency.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intCurrencyID; 
                cboOvertimePolicy.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intOTPolicyID;
                cboAbsentPolicy.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intAbsentPolicyID;
                //cboSettlement.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intSetPolicyID;

                cboHolidayPolicy.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intHolidayPolicyID;
                cboEncashPolicy.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intEncashPolicyID;    

                FillSalaryStructureDetailInfo();
           
            }

        }

        private void FillSalaryStructureDetailInfo()
        {
            DataTable dtSalayStructureDetail = MobjclsBLLSalaryStructure.DispalySalaryStructureDetail(BLLSalaryStructure.DTOSalaryStructure.intSalaryStructureID); 

            dgvParticulars.Rows.Clear();
            if (dtSalayStructureDetail.Rows.Count > 0)
            {
                for (int iCounter = 0; iCounter <= dtSalayStructureDetail.Rows.Count - 1; iCounter++)
                {
                    dgvParticulars.RowCount = dgvParticulars.RowCount + 1;

                    dgvParticulars.Rows[iCounter].Cells["dgvColParticulars"].Value = dtSalayStructureDetail.Rows[iCounter]["AdditionDeductionID"].ToInt32();

                    dgvParticulars.Rows[iCounter].Cells["dgvColAmount"].Value = dtSalayStructureDetail.Rows[iCounter]["Amount"].ToDecimal();

                    //if (ClsCommonSettings.IsAmountRoundByZero)
                    //{
                    //    MintScale = 0;
                    //    dgvParticulars.Rows[iCounter].Cells["dgvColAmount"].Value = dgvParticulars.Rows[iCounter].Cells["dgvColAmount"].Value.ToDecimal().ToString("F" + MintScale);
                    //}

                    dgvParticulars.Rows[iCounter].Cells["dgvColSerialNo"].Value = dtSalayStructureDetail.Rows[iCounter]["SerialNo"].ToInt32();
                    dgvParticulars.Rows[iCounter].Cells["dgvColIsAddition"].Value = dtSalayStructureDetail.Rows[iCounter]["IsAddition"].ToBoolean()==true?1:0;

                    FillDeductionPolicy(dgvParticulars.Rows[iCounter].Cells["dgvColParticulars"].Value.ToInt32(), iCounter);
                    if (dtSalayStructureDetail.Rows[iCounter]["AdditionDeductionPolicyId"] != DBNull.Value && dtSalayStructureDetail.Rows[iCounter]["AdditionDeductionPolicyId"].ToInt32() > 0)
                    {
                        dgvParticulars.Rows[iCounter].Cells["dgvColDeductionPolicy"].Value = dtSalayStructureDetail.Rows[iCounter]["AdditionDeductionPolicyID"].ToInt32();
                        dgvParticulars.Rows[iCounter].Cells["dgvColDeductionPolicy"].Tag = dtSalayStructureDetail.Rows[iCounter]["AdditionDeductionPolicyID"].ToInt32();
                        dgvParticulars.Rows[iCounter].Cells["dgvColDeductionPolicy"].Value = dgvParticulars.Rows[iCounter].Cells["dgvColDeductionPolicy"].FormattedValue;
                    }
                    else
                    {
                        dgvParticulars.Rows[iCounter].Cells["dgvColDeductionPolicy"].Value = -1;
                        dgvParticulars.Rows[iCounter].Cells["dgvColDeductionPolicy"].Tag = -1;
                        dgvParticulars.Rows[iCounter].Cells["dgvColDeductionPolicy"].Value = dgvParticulars.Rows[iCounter].Cells["dgvColDeductionPolicy"].FormattedValue;
                    }


                    dgvParticulars.Rows[iCounter].Cells["CboCategory"].Value = Convert.ToInt32(dtSalayStructureDetail.Rows[iCounter]["CategoryID"]);


                    

                }
            }
            FillRemunerationDetails();

        }

        private void FillRemunerationDetails()
        {
            DataTable dtRemunerationDetail = MobjclsBLLSalaryStructure.DispalyRemunerationDetail(BLLSalaryStructure.DTOSalaryStructure.intSalaryStructureID);

            dgvOtherRemuneration.Rows.Clear();
            if (dtRemunerationDetail.Rows.Count > 0)
            {
                for (int iCounter = 0; iCounter <= dtRemunerationDetail.Rows.Count - 1; iCounter++)
                {
                    dgvOtherRemuneration.RowCount = dgvOtherRemuneration.RowCount + 1;

                    dgvOtherRemuneration.Rows[iCounter].Cells["OtherRemunerationParticular"].Value = dtRemunerationDetail.Rows[iCounter]["OtherRemunerationID"].ToInt32();
                    dgvOtherRemuneration.Rows[iCounter].Cells["OtherRemunerationParticular"].Tag = dtRemunerationDetail.Rows[iCounter]["OtherRemunerationID"].ToInt32();
                    dgvOtherRemuneration.Rows[iCounter].Cells["OtherRemunerationParticular"].Value = dgvOtherRemuneration.Rows[iCounter].Cells["OtherRemunerationParticular"].FormattedValue;
                    dgvOtherRemuneration.Rows[iCounter].Cells["OtherRemunerationAmount"].Value = dtRemunerationDetail.Rows[iCounter]["Amount"].ToDecimal();
                    dgvOtherRemuneration.Rows[iCounter].Cells["OtherRemunerationDetailID"].Value = dtRemunerationDetail.Rows[iCounter]["OtherRemunerationDetailID"].ToInt32();

                    //if (ClsCommonSettings.IsAmountRoundByZero)
                    //{
                    //    dgvOtherRemuneration.Rows[iCounter].Cells["OtherRemunerationAmount"].Value = dgvOtherRemuneration.Rows[iCounter].Cells["OtherRemunerationAmount"].Value.ToDecimal().ToString("F" + 0); 
                    //}


                }
            }
        }


        private void SetSalaryStructureMasterInfo()
        {
           
                cboCompany.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intCompanyID;
                LoadCombos(2);
                cboEmployee.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intEmployeeID;
                cboPayClassification.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intPaymentClassificationID;
                cboPayCalculation.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intPayCalculationTypeID;
                txtRemarks.Text = BLLSalaryStructure.DTOSalaryStructure.strRemarks;
                CboSalaryday.Text = BLLSalaryStructure.DTOSalaryStructure.intSalaryDay.ToString();
                cboCurrency.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intCurrencyID;
                cboOvertimePolicy.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intOTPolicyID;
                cboAbsentPolicy.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intAbsentPolicyID;
                //cboSettlement.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intSetPolicyID;
                cboHolidayPolicy.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intHolidayPolicyID;
                cboEncashPolicy.SelectedValue = BLLSalaryStructure.DTOSalaryStructure.intEncashPolicyID;
 
                FillSalaryStructureDetailInfo();

        }


        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
            if (MblnIsEditMode)
            {
                CancelToolStripButton.Enabled = false;
            }
            else
                CancelToolStripButton.Enabled = MblnAddPermission;
        }

        private void GetRecordCount()
        {
            MintRecordCnt = 0;
            //if (!MblnSearch)
            //    txtSearch.Text = "";
            MintRecordCnt = BLLSalaryStructure.GetRecordCount(MstrSearch, cboCompany.SelectedValue.ToInt32());
            if (MintRecordCnt < 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                MintRecordCnt = 0;
            }

        }

        private void ChangeStatus()
        {
            //function for changing status
            MblnIsFromOK = false;
            if (!MblnIsEditMode)
            {
                btnOk.Enabled = MblnAddPermission;
                btnSave.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                btnOk.Enabled = MblnUpdatePermission;
                btnSave.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            errProSalaryStructure.Clear();
        }

        private bool DeleteValidation()
        {
            //Check for reference in salary advance,expense,payment,attendance,Loan,vacation
            if (BLLSalaryStructure.CheckSalaryReleased())
            {
                UserMessage.ShowMessage(1723);
                return false;
            }
            return true;

        }

        private bool UpdateValidation()
        {

            if (BLLSalaryStructure.Updatevalidation())
            {
                UserMessage.ShowMessage(1729);
                return false;
            }
            return true;

        }

        private bool DeleteSalaryStructure()
        {
            bool blnRetValue = false;
            if (DeleteValidation())
            {
                if (UserMessage.ShowMessage(1714) == true)
                {
                    if (BLLSalaryStructure.DeleteSalaryStructure())
                    {
                        blnRetValue = true;
                    }
                }
            }
            return blnRetValue;
        }

        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsUtilities.GetAutoCompleteListWithCompany(cboCompany.SelectedValue.ToInt32(), txtSearch.Text.Trim(), 0, "", "PaySalaryStructure");
            this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);
        }

        private  void  CalculateNetAmount()
        {
            txtAddition.Text = "";
            txtDeduction.Text = "";
            txtNetAmount.Text = "";

            Decimal     decAddAmount =0;
            Decimal     decDedAmount =0; 
            Decimal     decNetAmount =0;
            int iIsAdditition =0;
            
             if(dgvParticulars.RowCount >=2)
             {
                for(int iCount =0;iCount<=dgvParticulars.RowCount-2;iCount++)
                {
                   iIsAdditition=dgvParticulars.Rows[iCount].Cells["dgvColIsAddition"].Value.ToInt32();
                    decimal decAmount=0;
                    if (dgvParticulars.Rows[iCount].Cells["dgvColAmount"].Value.ToStringCustom() != "")
                    {
                        decAmount = dgvParticulars.Rows[iCount].Cells["dgvColAmount"].Value.ToDecimal();
                    }
                    //Calcualte deduction amount
                    if (dgvParticulars.Rows[iCount].Cells["dgvColDeductionPolicy"].Value.ToStringCustom().Trim() !="None")
                    {
                        decAmount = CalculateDeductionAmount(dgvParticulars.Rows[iCount].Cells["dgvColDeductionPolicy"].Tag.ToInt32());
                       // dgvParticulars.Rows[iCount].Cells["dgvColAmount"].Value = decAmount.ToStringCustom();
                    }

                    if (iIsAdditition==1)//Total additions
                    {
                        decAddAmount=decAddAmount+decAmount;
                    }
                    else//total deductions
                    {
                        decDedAmount= decDedAmount+decAmount;
                    }
                }
            }
            decNetAmount=decAddAmount-decDedAmount;
            txtAddition.Text=decAddAmount.ToString();
            txtDeduction.Text=decDedAmount.ToString();
            txtNetAmount.Text=decNetAmount.ToString();
        }

        #region CalculateDeductionAmount
        /// <summary>
        /// Calculate deduction amount if deduction policy is selected
        /// </summary>
        /// <param name="iDeductionPolicyID">DeductionpolicyId</param>
        /// <returns>deduction amount</returns>
        public decimal CalculateDeductionAmount(int iDeductionPolicyID)
        {
            decimal decCalculatedSalary = 0,decAmountLimit,decEmployerPart,decEmployeePart,decFixedAmount,decBasicPay=0,decGross=0;
            int iRateOnly,iAdditionDeductionID, iAdditionID, iParameterID, iCanContinue=0;
            //string[] strAdditionIds = null;
            foreach (DataGridViewRow rowValue in dgvParticulars.Rows)
            {
                if (rowValue.Cells["dgvColIsAddition"].Value.ToInt32() == 1)
                {
                    decGross += rowValue.Cells[dgvColAmount.Index].Value.ToDecimal();
                    //strAdditionIds[i] = rowValue.Cells[dgvColParticulars.Index].Value.ToStringCustom().Trim();
                    //i++;
                    if (rowValue.Cells[dgvColDeductionPolicy.Index].Tag.ToInt32() < 0 && rowValue.Cells[dgvColParticulars.Index].Value.ToStringCustom() == "1")
                    {
                        decBasicPay += rowValue.Cells[dgvColAmount.Index].Value.ToDecimal();
                    }
                }
            }

            DataSet dsDetails = MobjclsBLLSalaryStructure.GetDeductionPolicyDetails(iDeductionPolicyID);
            DataTable dtPolicyDetails = dsDetails.Tables[0];
            //AdditionIDs of the deduction policy
            string[] strAdditionIDs = dsDetails.Tables[1].Rows[0][0].ToStringCustom().Split(',');
            if (dtPolicyDetails.Rows.Count > 0)
            {
                iRateOnly = dtPolicyDetails.Rows[0]["RateOnly"].ToInt32();
                iAdditionID = dtPolicyDetails.Rows[0]["AdditionID"].ToInt32();
                iAdditionDeductionID = dtPolicyDetails.Rows[0]["AdditionDeductionID"].ToInt32();
                iParameterID = dtPolicyDetails.Rows[0]["ParameterID"].ToInt32();
                decAmountLimit = dtPolicyDetails.Rows[0]["AmountLimit"].ToDecimal();
                decEmployerPart = dtPolicyDetails.Rows[0]["EmployerPart"].ToDecimal();
                decEmployeePart = dtPolicyDetails.Rows[0]["EmployeePart"].ToDecimal();
                decFixedAmount = dtPolicyDetails.Rows[0]["FixedAmount"].ToDecimal();

                // IF RateOnly  = 1 THEN FIXED RATE ELSE RATE CALCULATION BASED ON THE PARAMETERS SPECIFIED
                if (iRateOnly == 1)
                {
                    decCalculatedSalary = decEmployeePart;
                    iCanContinue = 1;
                }
                else
                {
                    //CALCULATION BASED ON THE ADDITIONID VALUE EITHER BASIC PAY AND GROSS SALARY
                    //PARAMETER VALUE :18 (CALCULATION BASED ON GROSS SALARY)
					//	              :1  (CALCULATION BASED ON BASIC PAY)
                    decCalculatedSalary = 0;
                    if (iAdditionID == 1)//If basic pay or gross
                    {
                        decCalculatedSalary = decBasicPay;
                    }
                    else
                    {
                        decCalculatedSalary = decGross;
                    }
                    iCanContinue = 2;
                    //CHECKING WHETHER WE NEED TO CALCULATE THE DEDUCTION AMOUNT BASED ON THE PARAMETER VALUE
                    if (iParameterID == 1)//CALCULATED SALARY LESS THAN THE  AMOUNT LIMIT
                    {
                        if(decCalculatedSalary<decAmountLimit)
                           iCanContinue = 1;
                    }
                    else if (iParameterID == 2)
                    {
                        if (decCalculatedSalary > decAmountLimit)//CALCULATED SALARY GREATER THAN THE  AMOUNT LIMIT
                            iCanContinue = 1;
                    }
                    else if (iParameterID == 3)//CALCULATED SALARY  EQUAL TO THE  AMOUNT LIMIT
                    {
                        if (decCalculatedSalary == decAmountLimit)
                            iCanContinue = 1;
                    }
                    else if (iParameterID == 4)//CALCULATED SALARY LESS THAN OR  EQUAL TO THE  AMOUNT LIMIT
                    {
                        if (decCalculatedSalary <= decAmountLimit)
                            iCanContinue = 1;
                    }
                    else if (iParameterID == 5)//CALCULATED SALARY GREATER THAN OR  EQUAL TO THE  AMOUNT LIMIT
                    {
                        if (decCalculatedSalary >= decAmountLimit)
                            iCanContinue = 1;
                    }

                    if (iCanContinue == 1)
                    {
                        //CALCULATE THE SUM OF ALL THE PARAMETERS
                        decCalculatedSalary=0;
                        foreach (DataGridViewRow rowValue in dgvParticulars.Rows)
                        {
                            //CHECKING IF THE ADDITION IDS SPECIFIED IN THE DEDUCTION POLICY IS SELECTED BY USER.
                            //IF YES ,THEIR AMOUNT IS SUMMED UP
                            if (rowValue.Cells["dgvColIsAddition"].Value.ToInt32() == 1 && strAdditionIDs.Contains(rowValue.Cells[dgvColParticulars.Index].Value.ToStringCustom().Trim()))
                            {
                                decCalculatedSalary += rowValue.Cells[dgvColAmount.Index].Value.ToDecimal();
                            }
                        }
                        //THE EMPLOYEE DEDUCTION AMOUNT IS THE EMPLOYEE CONTRIBUTION PERCENTAGE OF THE CALCULATED SALARY
                        decCalculatedSalary = ((decEmployeePart / 100) * decCalculatedSalary);
                    }
                }
            }
            //RETURN CalculatedSalary AS DeductionAmount
            if (iCanContinue == 1)
            {
                return decCalculatedSalary;
            }
            else
                return 0;
        }
        #endregion CalculateDeductionAmount

        private void ClearSearchCount(object sender, EventArgs e)
        {
            MintRowNumber = -1;
        }

        private void Search()
        {

            int TotalRows;
           
            if(txtSearch.Text =="")
            {
                MintRowNumber = -1;
                UserMessage.ShowMessage(1724);
                txtSearch.Focus();
                return ;
            }           

            //Searching an Employee according to the Employeename/employee number
            if (MintRowNumber <= 0)
            {
                MintRowNumber = 1;
            }
            if (BLLSalaryStructure.SearchEmployee(txtSearch.Text.Trim(), MintRowNumber, out TotalRows,cboCompany.SelectedValue.ToInt32()))
            {
                BindingNavigatorPositionItem.Text = MintRowNumber.ToString();
                BindingNavigatorCountItem.Text = strBindingOf + TotalRows.ToString();
                
                MintRecordCnt = TotalRows;
                //MblnAddStatus = false;

                SetSalaryStructureMasterInfo();  // Displaying employee information
                BindingNavigatorPositionItem.Text = Convert.ToString(MintRowNumber);
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                
                BindingNavigatorMoveNextItem.Enabled = true;
                BindingNavigatorMoveLastItem.Enabled = true;
                BindingNavigatorMovePreviousItem.Enabled = true;
                BindingNavigatorMoveFirstItem.Enabled = true;
                MblnIsEditMode = true;

                int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
                int iRecordCnt = MintRecordCnt;  // Total count of the records

                if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                //LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                btnPrint.Enabled = MblnPrintEmailPermission;
                btnEmail.Enabled = MblnPrintEmailPermission;
                //BtnClear.Enabled = MblnUpdatePermission;
                btnSave.Enabled = false;
                btnOk.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
            }
            else
            {
                UserMessage.ShowMessage(1726);
                MintRowNumber = -1;
            }
        }

        #endregion



        private void btnAbsentPolicy_Click(object sender, EventArgs e)
        {
            int iAbsentPolicyID = cboAbsentPolicy.SelectedValue.ToInt32();
            using (FrmAbsentPolicy objFrmAbsentPolicy = new FrmAbsentPolicy())
            {
                objFrmAbsentPolicy.PintAbsentPolicyID = iAbsentPolicyID;
                objFrmAbsentPolicy.ShowDialog();
                LoadCombos(6);
                if (objFrmAbsentPolicy.PintAbsentPolicyID != 0)
                   cboAbsentPolicy.SelectedValue = objFrmAbsentPolicy.PintAbsentPolicyID;
                else
                    cboAbsentPolicy.SelectedValue = iAbsentPolicyID;
            }            
        }

        private void btnOvertimePolicy_Click(object sender, EventArgs e)
        {
            try
            {
                int iOTPolicyID = Convert.ToInt32(cboOvertimePolicy.SelectedValue);
                using (FrmOvertimePolicy objFrmOTPolicy = new FrmOvertimePolicy()) 
                {
                    objFrmOTPolicy.PintOTPolicyID = iOTPolicyID; 
                    objFrmOTPolicy.ShowDialog();
                    LoadCombos(7);
                    if (objFrmOTPolicy.PintOTPolicyIDForFocus != 0)

                        cboOvertimePolicy.SelectedValue = objFrmOTPolicy.PintOTPolicyIDForFocus;
                    else
                        cboOvertimePolicy.SelectedValue = objFrmOTPolicy.PintOTPolicyID;
                }
            }
              catch (Exception Ex)
            {
                  ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void FrmSalaryStructure_Load(object sender, EventArgs e)
        {


            SetPermissions();
            LoadCombos(0);
            if (ClsCommonSettings.GlbSalaryDayIsEditable)
                CboSalaryday.Enabled = true;
            else
                CboSalaryday.Enabled = false;
            cboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;
            MobjclsBLLSalaryStructure.DTOSalaryStructure.intEmployeeID = PintEmployeeID.ToInt32();
            if (BLLSalaryStructure.EmployeeIDExists())
            {
                if (PintEmployeeID > 0)
                {
                    if (PintCompanyID > 0)
                    {
                        cboCompany.SelectedValue = PintCompanyID;
                    }
                    MintRecordCnt = MobjclsBLLSalaryStructure.GetRecordCount(txtSearch.Text.Trim(),cboCompany.SelectedValue.ToInt32());
                    MobjclsBLLSalaryStructure.DTOSalaryStructure.intEmployeeID = PintEmployeeID.ToInt32();
                    MintCurrentRecCnt = MobjclsBLLSalaryStructure.GetRowNumber( cboCompany.SelectedValue.ToInt32(),txtSearch.Text.Trim());
                    DisplaySalaryStructureInfo();
                }
               
            }
            else
            {
                AddNewSalaryStructure();
               
            }
            setButtonPermissions();
            cboCompany.Enabled = true;
            SetAutoCompleteList();
            MblnIsFromOK = true;
        }

        private void setButtonPermissions()
        {
            DataTable dt = null;
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                dt = MobjclsBLLSalaryStructure.GetPolicyPermissions(ClsCommonSettings.RoleID);
                if (dt.Rows.Count == 0)
                {
                    btnAbsentPolicy.Enabled  = btnEncashPolicy.Enabled = btnOvertimePolicy.Enabled = btnHolidayPolicy.Enabled = btnDeductionPolicy.Enabled = false;
                }
                else
                {
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AbsentPolicy).ToString();
                    if (dt.DefaultView.Count >0) 
                    {
                        btnAbsentPolicy.Enabled = (dt.DefaultView.ToTable().Rows[0]["IsView"].ToBoolean()== true);
                    }
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SettlementPolicy).ToString();

                    //if (dt.DefaultView.Count > 0)
                    //{
                    //    btnSettlement.Enabled = (dt.DefaultView.ToTable().Rows[0]["IsView"].ToBoolean() == true);
                    //}
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.OvertimePolicy).ToString();

                    if (dt.DefaultView.Count > 0)
                    {
                        btnOvertimePolicy.Enabled = (dt.DefaultView.ToTable().Rows[0]["IsView"].ToBoolean() == true);
                    }
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.EncashPolicy).ToString();

                    if (dt.DefaultView.Count > 0)
                    {
                        btnEncashPolicy.Enabled = (dt.DefaultView.ToTable().Rows[0]["IsView"].ToBoolean() == true);
                    }
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.HolidayPolicy).ToString();

                    if (dt.DefaultView.Count > 0)
                    {
                        btnHolidayPolicy.Enabled = (dt.DefaultView.ToTable().Rows[0]["IsView"].ToBoolean() == true);
                    }
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.DeductionPolicy).ToString();

                    if (dt.DefaultView.Count > 0)
                    {
                        btnDeductionPolicy.Enabled = (dt.DefaultView.ToTable().Rows[0]["IsView"].ToBoolean() == true);
                    }
                }
            }
            else
            {
                btnAbsentPolicy.Enabled =  btnEncashPolicy.Enabled = btnOvertimePolicy.Enabled = btnHolidayPolicy.Enabled = btnDeductionPolicy.Enabled = true;
            }
        }

       

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            try
            {
                txtSearch.Text = string.Empty;
                AddNewSalaryStructure();
                CancelToolStripButton.Enabled = MblnAddPermission;
            }
            catch (Exception Ex)
            {
                  ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveSalaryStructure())
                {
                    //UserMessage.ShowMessage(2,null,2);
                    //lblStatus.Text = UserMessage.GetMessageByCode(2,2);
                    MblnIsFromOK = true;
                }
            }
            catch (Exception Ex)
            {
                  ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    DisplaySalaryStructureInfo();
                    lblStatus.Text = UserMessage.GetMessageByCode(10);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 1;
            }
            MblnIsFromOK = true;
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {

            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = 1;
                DisplaySalaryStructureInfo();
                lblStatus.Text = UserMessage.GetMessageByCode(9);
                tmrClear.Enabled = true;
                MblnIsFromOK = true;
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                }
                if (MintCurrentRecCnt > MintRecordCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                }
                else
                {
                    DisplaySalaryStructureInfo();
                    lblStatus.Text = UserMessage.GetMessageByCode(11);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 0;
            }
            MblnIsFromOK = true;
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            //if (MblnSearch)
            //{
            //    GetNextExecutiveInformation(true);
            //}
            //else
            //{
                if (MintRecordCnt > 0)
                {
                    if (MintRecordCnt != MintCurrentRecCnt)
                    {
                        MintCurrentRecCnt = MintRecordCnt;
                        DisplaySalaryStructureInfo();
                        lblStatus.Text = UserMessage.GetMessageByCode(12);
                        tmrClear.Enabled = true;
                    }
                }
           // }
            MblnIsFromOK = true;
        }

        private void cboAbsentPolicy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboOvertimePolicy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dgvParticulars_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            ChangeStatus();
            if (e.ColumnIndex != -1 && e.RowIndex != -1)
            {
                if (e.ColumnIndex == dgvColParticulars.Index)
                {
                    if (dgvParticulars.CurrentCell.Value.ToInt32() != 0)
                    {
                        int iIsAddition = BLLSalaryStructure.GetAdditionOrDeduction(dgvParticulars.CurrentCell.Value.ToInt32()).ToInt32();
                        dgvParticulars.CurrentRow.Cells["dgvColIsAddition"].Value = iIsAddition;
                        int intAdditionDeductionID = dgvParticulars.CurrentRow.Cells["dgvColParticulars"].Value.ToInt32();
                        if (intAdditionDeductionID > 0)
                        {
                            DataTable dtTemp = MobjclsBLLSalaryStructure.FillCombos(new string[] { "AdditionDeductionPolicyId", "PaySalaryStructureDetail", "SalaryStructureID = " + MobjclsBLLSalaryStructure.DTOSalaryStructure.intSalaryStructureID + " AND AdditionDeductionID = " + dgvParticulars.Rows[e.RowIndex].Cells["dgvColParticulars"].Value.ToInt32() + " AND AdditionDeductionPolicyId IS NOT NULL" });
                            FillDeductionPolicy(intAdditionDeductionID, e.RowIndex);
                            if (dtTemp != null && dtTemp.Rows.Count > 0)
                            {
                                dgvParticulars.Rows[e.RowIndex].Cells["dgvColDeductionPolicy"].Value = dtTemp.Rows[0]["AdditionDeductionPolicyId"];
                                dgvParticulars.Rows[e.RowIndex].Cells["dgvColDeductionPolicy"].Tag = dtTemp.Rows[0]["AdditionDeductionPolicyId"];
                                dgvParticulars.Rows[e.RowIndex].Cells["dgvColDeductionPolicy"].Value = dgvParticulars.Rows[e.RowIndex].Cells["dgvColDeductionPolicy"].FormattedValue;
                            }
                            else
                            {
                                dgvParticulars.Rows[e.RowIndex].Cells["dgvColDeductionPolicy"].Value = -1;
                                dgvParticulars.Rows[e.RowIndex].Cells["dgvColDeductionPolicy"].Tag = -1;
                                dgvParticulars.Rows[e.RowIndex].Cells["dgvColDeductionPolicy"].Value = dgvParticulars.Rows[e.RowIndex].Cells["dgvColDeductionPolicy"].FormattedValue;
                            }
                        }
                    }
                }
                else if (e.ColumnIndex == dgvColDeductionPolicy.Index)
                {
                    int iIsAddition = BLLSalaryStructure.GetAdditionOrDeduction(dgvParticulars.Rows[e.RowIndex].Cells["dgvColParticulars"].Value.ToInt32()).ToInt32();
                    if (iIsAddition == 0 && dgvParticulars.Rows[e.RowIndex].Cells["dgvColParticulars"].Value.ToInt32() != 0 && dgvParticulars.Rows[e.RowIndex].Cells["dgvColDeductionPolicy"].Tag.ToInt32() > 0)
                    {
                        dgvParticulars.Rows[e.RowIndex].Cells["dgvColAmount"].ReadOnly = true;
                        dgvParticulars.Rows[e.RowIndex].Cells["dgvColAmount"].Value = string.Empty;
                    }
                    else
                        dgvParticulars.Rows[e.RowIndex].Cells["dgvColAmount"].ReadOnly = false;
                }
                if (dgvParticulars.RowCount >= 1)
                {
                    CalculateNetAmount();
                }
            }
        }

        private void dgvParticulars_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                int iCol= 0;
                if (e.ColumnIndex != dgvColParticulars.Index && dgvParticulars.CurrentRow != null)
                {
                    if (dgvParticulars.CurrentRow.Cells[dgvColParticulars.Index].Value.ToStringCustom() == string.Empty)
                    {
                        UserMessage.ShowMessage(1658);
                        dgvParticulars.Focus();
                        return;
                    }
                    if (dgvParticulars.CurrentCell.ColumnIndex != 1)
                    {
                        if (dgvParticulars.CurrentRow.Cells[CboCategory.Index].Value.ToStringCustom() == string.Empty)
                        {
                            iCol = dgvParticulars.CurrentCell.ColumnIndex;
                            UserMessage.ShowMessage(9110);
                            dgvParticulars.CurrentCell = dgvParticulars[iCol, 1];
                            return;
                        }
                    }

                    if (e.ColumnIndex == dgvColDeductionPolicy.Index)
                    {
                        if (dgvParticulars.Rows[e.RowIndex].Cells["dgvColParticulars"].Value != null)
                        {
                            int intAdditionDeductionID = dgvParticulars.Rows[e.RowIndex].Cells["dgvColParticulars"].Value.ToInt32();
                            int tag = 0;
                            if (dgvParticulars.Rows[e.RowIndex].Cells["dgvColParticulars"].Tag != null)
                            {
                                tag = dgvParticulars.Rows[e.RowIndex].Cells["dgvColParticulars"].Tag.ToInt32();
                            }
                            FillDeductionPolicy(intAdditionDeductionID, e.RowIndex);
                            if (tag != 0)
                                dgvParticulars.Rows[e.RowIndex].Cells["dgvColParticulars"].Tag = tag;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void FillDeductionPolicy(int intAdditionDeductionID, int intRowIndex)
        {
            DataTable datCombos = BLLSalaryStructure.FillCombos(new string[] { "AdditionDeductionPolicyId,PolicyName", "PayAdditionDeductionPolicyMaster", "AdditionDeductionID = " + intAdditionDeductionID });
            dgvColDeductionPolicy.ValueMember = "AdditionDeductionPolicyId";
            dgvColDeductionPolicy.DisplayMember = "PolicyName";
            if (datCombos != null)
            {
                DataRow dr = datCombos.NewRow();
                dr["AdditionDeductionPolicyId"] = "-1";
                dr["PolicyName"] = "None";
                datCombos.Rows.InsertAt(dr, 0);
            }
            dgvColDeductionPolicy.DataSource = datCombos;
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MobjclsBLLSalaryStructure.CheckIfEmployeeIsActive(cboEmployee.SelectedValue.ToInt32()))
                {
                    if (DeleteSalaryStructure())
                    {
                        SetAutoCompleteList();
                        UserMessage.ShowMessage(1716);
                        lblStatus.Text = UserMessage.GetMessageByCode(1716);
                        AddNewSalaryStructure();
                    }
                }
                else
                {
                    //Settlement exists for the selected employee
                    errProSalaryStructure.SetError(cboEmployee, UserMessage.GetMessageByCode(1731));
                    UserMessage.ShowMessage(1731);
                    cboEmployee.Focus();
                    return;
                }

            }
            catch (System.Data.SqlClient.SqlException Ex)
            {
                string MstrMessageCommon = "";
                if (Ex.Number == (int)SqlErrorCodes.ForeignKeyErrorNumber)
                {
                    MstrMessageCommon = "Details exists in the sysytem.Cannot delete";
                }
                else
                    MstrMessageCommon = "Error on BindingNavigatorDeleteItem_Click() " + Ex.Message.ToString();

                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1,Log.LogSeverity.Error);

            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            MstrSearch = txtSearch.Text.Trim();
            GetRecordCount(); // get record count
            if (MintRecordCnt == 0) // No records
            {
                MstrSearch = "";
                UserMessage.ShowMessage(40);
                txtSearch.Text = string.Empty;
                return;
            }
            else if (MintRecordCnt > 0)
                BindingNavigatorMoveFirstItem_Click(sender, e); // if record found in search then automatically navigate to first item
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtSearch.Text = string.Empty;
            SetAutoCompleteList();
            //if (!MblnIsEditMode)
            //{
                LoadCombos(9);
                LoadCombos(3);
                GetRecordCount();
                BindingNavigatorAddNewItem_Click(null, null);
            //}
            //else
            //{
            //    //if (MintRecordCnt == 0) // No records
            //    //{
            //    //    UserMessage.ShowMessage(40, null, 2);
            //    //    return;
            //    //}
            //    //else if (MintRecordCnt > 0)
            //        BindingNavigatorMoveFirstItem_Click(sender, e); // if record found in search then automatically navigate to first item
            //}
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveSalaryStructure())
                {
                    //UserMessage.ShowMessage(2,null,2);
                    //lblStatus.Text = UserMessage.GetMessageByCode(2,2);
                    MblnIsFromOK = true;
                }
            }
            catch (Exception Ex)
            {
                  ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveSalaryStructure())
                {
                    //UserMessage.ShowMessage(2,null,2);
                    //lblStatus.Text = UserMessage.GetMessageByCode(2,2);
                    MblnIsFromOK = true;
                    this.Close();
                }
            }
            catch (Exception Ex)
            {
                  ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void CancelToolStripButton_Click(object sender, EventArgs e)
        {
            AddNewSalaryStructure();
        }

        private void dgvParticulars_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgvParticulars.CurrentCell.ColumnIndex == dgvColAmount.Index)
            {
                TextBox txt = (TextBox)(e.Control);
                txt.KeyPress += new KeyPressEventHandler(txt_KeyPress);
            }
            else if (dgvParticulars.CurrentCell.ColumnIndex == dgvColParticulars.Index)
            {
                // Avoid Black Color For dgvColParticulars
                ComboBox cboParticulars = e.Control as ComboBox;
                dgvColParticulars_SelectedIndexChanged(cboParticulars, e);
            }
        }


        private void dgvColParticulars_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Avoid Black Color For dgvColParticulars
            DataGridViewComboBoxEditingControl cboParticulars = (DataGridViewComboBoxEditingControl)sender;
            cboParticulars.SelectedIndexChanged -= new EventHandler(dgvColParticulars_SelectedIndexChanged);
            cboParticulars.DropDown += new EventHandler(dgvColParticulars_DropDown);
            cboParticulars.GotFocus += new EventHandler(dgvColParticulars_DropDown);
        }

        private void dgvColParticulars_DropDown(object sender, EventArgs e)
        {
            DataGridViewComboBoxEditingControl cboParticulars = (DataGridViewComboBoxEditingControl)sender;
            cboParticulars.BackColor = Color.White;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = "Salary Structure";
                ObjViewer.PiRecId = BLLSalaryStructure.DTOSalaryStructure.intSalaryStructureID;
                ObjViewer.PiFormID = (int)FormID.SalaryStructure;
                ObjViewer.ShowDialog();
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboCompany_KeyDown(object sender, KeyEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void txtRemarks_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void CboSalaryday_SelectedValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dgvParticulars_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                ChangeStatus();
            }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            //Email
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Salary Structure";
                    ObjEmailPopUp.EmailFormType = EmailFormID.SalaryStructure;
                    ObjEmailPopUp.EmailSource = MobjclsBLLSalaryStructure.DisplaySalaryStructure(BLLSalaryStructure.DTOSalaryStructure.intSalaryStructureID);
                   // BLLNavigator.Navigator.parentID = BLLSalaryStructure.DTOSalaryStructure.intEmployeeID;
                   // ObjEmailPopUp.EmailSource = MobjclsNavigatorBLL.GetSalaryStructureReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void cboEmployee_KeyDown(object sender, KeyEventArgs e)
        {
            cboEmployee.DroppedDown = false;
        }

        private void cboCurrency_KeyDown(object sender, KeyEventArgs e)
        {
            cboCurrency.DroppedDown = false;
        }

        private void CboSalaryday_KeyDown(object sender, KeyEventArgs e)
        {
            CboSalaryday.DroppedDown = false;
        }

        private void cboAbsentPolicy_KeyDown(object sender, KeyEventArgs e)
        {
            cboAbsentPolicy.DroppedDown = false;
        }

        private void cboOvertimePolicy_KeyDown(object sender, KeyEventArgs e)
        {
            cboOvertimePolicy.DroppedDown = false;
        }

        private void cboOvertimePolicy_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboAbsentPolicy_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void btnAdditionDeduction_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmAdditionDeduction(), true);
            LoadCombos(8);
        }

        private void dgvParticulars_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                return;
            }
            catch
            {
                return;
            }
        }

        private void dgvParticulars_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if(dgvParticulars.IsCurrentCellDirty)
                {
                    if (dgvParticulars.CurrentCell != null)
                        dgvParticulars.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }

        private void FrmSalaryStructure_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!MblnIsFromOK)
            {
                if (UserMessage.ShowMessage(8) == true)
                {
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

      

        private void btnHolidayPolicy_Click(object sender, EventArgs e)
        {
            int iHolidayPolicy = cboHolidayPolicy.SelectedValue.ToInt32();
            using (FrmHolidayPolicy objFrmHolidayPolicy = new FrmHolidayPolicy())
            {
                objFrmHolidayPolicy.PintHolidayPolicyID = iHolidayPolicy;
                objFrmHolidayPolicy.ShowDialog();
                LoadCombos(11);
                if (objFrmHolidayPolicy.PintHolidayPolicyID != 0)
                    cboHolidayPolicy.SelectedValue = objFrmHolidayPolicy.PintHolidayPolicyID;
                else
                    cboHolidayPolicy.SelectedValue = iHolidayPolicy;
            }
        }

        private void btnEncashPolicy_Click(object sender, EventArgs e)
        {
            int iEncashPolicy = cboEncashPolicy.SelectedValue.ToInt32();
            using (frmEncashPolicy objFrmEncashPolicy = new frmEncashPolicy())
            {
                objFrmEncashPolicy.PintEncashPolicyID = iEncashPolicy;
                objFrmEncashPolicy.ShowDialog();
                LoadCombos(12);
                if (objFrmEncashPolicy.PintEncashPolicyID != 0)
                    cboEncashPolicy.SelectedValue = objFrmEncashPolicy.PintEncashPolicyID;
                else
                    cboEncashPolicy.SelectedValue = iEncashPolicy;
            }
        }

        private void cboHolidayPolicy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboEncashPolicy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void btnDeductionPolicy_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmDeductionPolicy(), true);
        }

        private void dgvParticulars_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                dgvParticulars.EndEdit();
                if (e.ColumnIndex == dgvColDeductionPolicy.Index)
                {
                    dgvParticulars.Rows[e.RowIndex].Cells["dgvColDeductionPolicy"].Tag = dgvParticulars.Rows[e.RowIndex].Cells["dgvColDeductionPolicy"].Value;
                    dgvParticulars.Rows[e.RowIndex].Cells["dgvColDeductionPolicy"].Value = dgvParticulars.Rows[e.RowIndex].Cells["dgvColDeductionPolicy"].FormattedValue;
                }
            }
        }

        private void dgvParticulars_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (dgvParticulars.RowCount >= 1)
            {
                CalculateNetAmount();
            }
        }

        private void OtherRemunerationGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                ChangeStatus();
            }
        }

        private void OtherRemunerationGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            ChangeStatus();
        }

        private void dgvOtherRemuneration_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (e.ColumnIndex == OtherRemunerationParticular.Index)
                {
                    if (dgvOtherRemuneration.Rows[e.RowIndex].Cells["OtherRemunerationParticular"].Value != null)
                    {
                        int intOtherRenumerationID = dgvOtherRemuneration.Rows[e.RowIndex].Cells["OtherRemunerationParticular"].Value.ToInt32();
                        int tag = 0;
                        if (dgvOtherRemuneration.Rows[e.RowIndex].Cells["OtherRemunerationParticular"].Tag != null)
                        {
                            tag = dgvOtherRemuneration.Rows[e.RowIndex].Cells["OtherRemunerationParticular"].Tag.ToInt32();
                        }
                        LoadCombos(13);
                        if (tag != 0)
                            dgvOtherRemuneration.Rows[e.RowIndex].Cells["OtherRemunerationParticular"].Tag = tag;
                    }
                }
            }
            ChangeStatus();
        }

        private void btnOtherRemuneration_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("OtherRemunerationReference", new int[] { 1, 0, 1 }, "OtherRemunerationID,OtherRemuneration,OtherRemunerationArb", "OtherRemunerationReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                LoadCombos(13);
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void dgvOtherRemuneration_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                dgvOtherRemuneration.EndEdit();
                if (e.ColumnIndex == OtherRemunerationParticular.Index)
                {
                    dgvOtherRemuneration.Rows[e.RowIndex].Cells["OtherRemunerationParticular"].Tag = dgvOtherRemuneration.Rows[e.RowIndex].Cells["OtherRemunerationParticular"].Value;
                    dgvOtherRemuneration.Rows[e.RowIndex].Cells["OtherRemunerationParticular"].Value = dgvOtherRemuneration.Rows[e.RowIndex].Cells["OtherRemunerationParticular"].FormattedValue;
                }
            }
        }

        private void cboSettlement_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }



        private void cboSettlement_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        /// <summary>
        /// Shortcut keys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmSalaryStructure_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        CancelToolStripButton_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());//Prev item
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());//Next item
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());//First item
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());//Last item
                        break;
                    case Keys.Control | Keys.P:
                        btnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        btnEmail_Click(sender, new EventArgs());//Email
                        break;
                    case Keys.F1:
                        BtnHelp_Click(sender, new EventArgs());//help
                        break;
                }
            }
            catch
            {
            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "SalaryStructure";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
                btnSearch_Click(null, null);

        }

        private void cboHolidayPolicy_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboEncashPolicy_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dgvOtherRemuneration_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgvOtherRemuneration.CurrentCell.ColumnIndex == OtherRemunerationAmount.Index)
            {
                TextBox txt = (TextBox)(e.Control);
                txt.KeyPress += new KeyPressEventHandler(txt_KeyPress);
            }
        }
       
    }
}
