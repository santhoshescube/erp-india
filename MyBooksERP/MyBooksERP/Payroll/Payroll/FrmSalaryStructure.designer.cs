﻿namespace MyBooksERP
{
    partial class FrmSalaryStructure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label9;
            System.Windows.Forms.Label Label8;
            System.Windows.Forms.Label Label7;
            System.Windows.Forms.Label Label6;
            System.Windows.Forms.Label Label5;
            System.Windows.Forms.Label Label3;
            System.Windows.Forms.Label Label10;
            System.Windows.Forms.Label IncentiveTypeIDLabel;
            System.Windows.Forms.Label EmployeeIDLabel;
            System.Windows.Forms.Label PaymentClassificationIDLabel;
            System.Windows.Forms.Label PayCalculationTypeIDLabel;
            System.Windows.Forms.Label BasicPayLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSalaryStructure));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.EmployeeSalaryHeadBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.CancelToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAdditionDeduction = new System.Windows.Forms.ToolStripButton();
            this.btnDeductionPolicy = new System.Windows.Forms.ToolStripButton();
            this.btnOtherRemuneration = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.Label4 = new System.Windows.Forms.Label();
            this.GrpMain = new System.Windows.Forms.GroupBox();
            this.tbSalaryStructure = new System.Windows.Forms.TabControl();
            this.General = new System.Windows.Forms.TabPage();
            this.dgvParticulars = new DemoClsDataGridview.ClsDataGirdView();
            this.dgvColParticulars = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.CboCategory = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvColAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColSerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColIsAddition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColDeductionPolicy = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.OtherRemuneration = new System.Windows.Forms.TabPage();
            this.dgvOtherRemuneration = new DemoClsDataGridview.ClsDataGirdView();
            this.OtherRemunerationParticular = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.OtherRemunerationAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OtherRemunerationDetailID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnEncashPolicy = new System.Windows.Forms.Button();
            this.btnHolidayPolicy = new System.Windows.Forms.Button();
            this.btnOvertimePolicy = new System.Windows.Forms.Button();
            this.cboHolidayPolicy = new System.Windows.Forms.ComboBox();
            this.cboEncashPolicy = new System.Windows.Forms.ComboBox();
            this.btnAbsentPolicy = new System.Windows.Forms.Button();
            this.cboOvertimePolicy = new System.Windows.Forms.ComboBox();
            this.cboAbsentPolicy = new System.Windows.Forms.ComboBox();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.CboSalaryday = new System.Windows.Forms.ComboBox();
            this.lblsalaryday = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtNetAmount = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.txtDeduction = new System.Windows.Forms.TextBox();
            this.txtAddition = new System.Windows.Forms.TextBox();
            this.cboPayCalculation = new System.Windows.Forms.ComboBox();
            this.cboPayClassification = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.BtnIncentive = new System.Windows.Forms.Button();
            this.BasicPayTextBox = new System.Windows.Forms.TextBox();
            this.CboIncentiveType = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.StatusStrip2 = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.errProSalaryStructure = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.label12 = new System.Windows.Forms.Label();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            this.controlContainerItem1 = new DevComponents.DotNetBar.ControlContainerItem();
            Label9 = new System.Windows.Forms.Label();
            Label8 = new System.Windows.Forms.Label();
            Label7 = new System.Windows.Forms.Label();
            Label6 = new System.Windows.Forms.Label();
            Label5 = new System.Windows.Forms.Label();
            Label3 = new System.Windows.Forms.Label();
            Label10 = new System.Windows.Forms.Label();
            IncentiveTypeIDLabel = new System.Windows.Forms.Label();
            EmployeeIDLabel = new System.Windows.Forms.Label();
            PaymentClassificationIDLabel = new System.Windows.Forms.Label();
            PayCalculationTypeIDLabel = new System.Windows.Forms.Label();
            BasicPayLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSalaryHeadBindingNavigator)).BeginInit();
            this.EmployeeSalaryHeadBindingNavigator.SuspendLayout();
            this.GrpMain.SuspendLayout();
            this.tbSalaryStructure.SuspendLayout();
            this.General.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParticulars)).BeginInit();
            this.OtherRemuneration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOtherRemuneration)).BeginInit();
            this.StatusStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errProSalaryStructure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label9
            // 
            Label9.AutoSize = true;
            Label9.Location = new System.Drawing.Point(215, 271);
            Label9.Name = "Label9";
            Label9.Size = new System.Drawing.Size(56, 13);
            Label9.TabIndex = 1041;
            Label9.Text = "Deduction";
            // 
            // Label8
            // 
            Label8.AutoSize = true;
            Label8.Location = new System.Drawing.Point(65, 271);
            Label8.Name = "Label8";
            Label8.Size = new System.Drawing.Size(45, 13);
            Label8.TabIndex = 1040;
            Label8.Text = "Addition";
            // 
            // Label7
            // 
            Label7.AutoSize = true;
            Label7.Location = new System.Drawing.Point(288, 305);
            Label7.Name = "Label7";
            Label7.Size = new System.Drawing.Size(80, 13);
            Label7.TabIndex = 1035;
            Label7.Text = "Overtime Policy";
            // 
            // Label6
            // 
            Label6.AutoSize = true;
            Label6.Location = new System.Drawing.Point(14, 332);
            Label6.Name = "Label6";
            Label6.Size = new System.Drawing.Size(73, 13);
            Label6.TabIndex = 1033;
            Label6.Text = "Holiday Policy";
            // 
            // Label5
            // 
            Label5.AutoSize = true;
            Label5.Location = new System.Drawing.Point(288, 332);
            Label5.Name = "Label5";
            Label5.Size = new System.Drawing.Size(74, 13);
            Label5.TabIndex = 1031;
            Label5.Text = "Encash Policy";
            // 
            // Label3
            // 
            Label3.AutoSize = true;
            Label3.Location = new System.Drawing.Point(14, 305);
            Label3.Name = "Label3";
            Label3.Size = new System.Drawing.Size(71, 13);
            Label3.TabIndex = 1029;
            Label3.Text = "Absent Policy";
            // 
            // Label10
            // 
            Label10.AutoSize = true;
            Label10.Location = new System.Drawing.Point(14, 48);
            Label10.Name = "Label10";
            Label10.Size = new System.Drawing.Size(49, 13);
            Label10.TabIndex = 1022;
            Label10.Text = "Currency";
            // 
            // IncentiveTypeIDLabel
            // 
            IncentiveTypeIDLabel.AutoSize = true;
            IncentiveTypeIDLabel.Location = new System.Drawing.Point(803, 229);
            IncentiveTypeIDLabel.Name = "IncentiveTypeIDLabel";
            IncentiveTypeIDLabel.Size = new System.Drawing.Size(81, 13);
            IncentiveTypeIDLabel.TabIndex = 1009;
            IncentiveTypeIDLabel.Text = "Incentive Type ";
            IncentiveTypeIDLabel.Visible = false;
            // 
            // EmployeeIDLabel
            // 
            EmployeeIDLabel.AutoSize = true;
            EmployeeIDLabel.Location = new System.Drawing.Point(14, 22);
            EmployeeIDLabel.Name = "EmployeeIDLabel";
            EmployeeIDLabel.Size = new System.Drawing.Size(56, 13);
            EmployeeIDLabel.TabIndex = 2;
            EmployeeIDLabel.Text = "Employee ";
            // 
            // PaymentClassificationIDLabel
            // 
            PaymentClassificationIDLabel.AutoSize = true;
            PaymentClassificationIDLabel.Location = new System.Drawing.Point(347, 22);
            PaymentClassificationIDLabel.Name = "PaymentClassificationIDLabel";
            PaymentClassificationIDLabel.Size = new System.Drawing.Size(78, 13);
            PaymentClassificationIDLabel.TabIndex = 4;
            PaymentClassificationIDLabel.Text = "Payment Mode";
            // 
            // PayCalculationTypeIDLabel
            // 
            PayCalculationTypeIDLabel.AutoSize = true;
            PayCalculationTypeIDLabel.Location = new System.Drawing.Point(347, 48);
            PayCalculationTypeIDLabel.Name = "PayCalculationTypeIDLabel";
            PayCalculationTypeIDLabel.Size = new System.Drawing.Size(109, 13);
            PayCalculationTypeIDLabel.TabIndex = 6;
            PayCalculationTypeIDLabel.Text = "Payment  Calculation ";
            // 
            // BasicPayLabel
            // 
            BasicPayLabel.AutoSize = true;
            BasicPayLabel.Location = new System.Drawing.Point(700, 266);
            BasicPayLabel.Name = "BasicPayLabel";
            BasicPayLabel.Size = new System.Drawing.Size(54, 13);
            BasicPayLabel.TabIndex = 8;
            BasicPayLabel.Text = "Basic Pay";
            BasicPayLabel.Visible = false;
            // 
            // EmployeeSalaryHeadBindingNavigator
            // 
            this.EmployeeSalaryHeadBindingNavigator.AddNewItem = null;
            this.EmployeeSalaryHeadBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.EmployeeSalaryHeadBindingNavigator.DeleteItem = null;
            this.EmployeeSalaryHeadBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.CancelToolStripButton,
            this.ToolStripSeparator1,
            this.btnAdditionDeduction,
            this.btnDeductionPolicy,
            this.btnOtherRemuneration,
            this.toolStripSeparator3,
            this.btnPrint,
            this.btnEmail,
            this.ToolStripSeparator2,
            this.BtnHelp});
            this.EmployeeSalaryHeadBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.EmployeeSalaryHeadBindingNavigator.MoveFirstItem = null;
            this.EmployeeSalaryHeadBindingNavigator.MoveLastItem = null;
            this.EmployeeSalaryHeadBindingNavigator.MoveNextItem = null;
            this.EmployeeSalaryHeadBindingNavigator.MovePreviousItem = null;
            this.EmployeeSalaryHeadBindingNavigator.Name = "EmployeeSalaryHeadBindingNavigator";
            this.EmployeeSalaryHeadBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.EmployeeSalaryHeadBindingNavigator.Size = new System.Drawing.Size(597, 25);
            this.EmployeeSalaryHeadBindingNavigator.TabIndex = 1;
            this.EmployeeSalaryHeadBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save Data";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // CancelToolStripButton
            // 
            this.CancelToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CancelToolStripButton.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.CancelToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CancelToolStripButton.Name = "CancelToolStripButton";
            this.CancelToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.CancelToolStripButton.Text = "Clear";
            this.CancelToolStripButton.ToolTipText = "Clear";
            this.CancelToolStripButton.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnAdditionDeduction
            // 
            this.btnAdditionDeduction.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdditionDeduction.Image = global::MyBooksERP.Properties.Resources.Addition_Deduction2;
            this.btnAdditionDeduction.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdditionDeduction.Name = "btnAdditionDeduction";
            this.btnAdditionDeduction.Size = new System.Drawing.Size(23, 22);
            this.btnAdditionDeduction.Text = "Addition/Deduction";
            this.btnAdditionDeduction.Click += new System.EventHandler(this.btnAdditionDeduction_Click);
            // 
            // btnDeductionPolicy
            // 
            this.btnDeductionPolicy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDeductionPolicy.Image = global::MyBooksERP.Properties.Resources.Deduction_Policy;
            this.btnDeductionPolicy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDeductionPolicy.Name = "btnDeductionPolicy";
            this.btnDeductionPolicy.Size = new System.Drawing.Size(23, 22);
            this.btnDeductionPolicy.Text = "Deduction Policy";
            this.btnDeductionPolicy.Click += new System.EventHandler(this.btnDeductionPolicy_Click);
            // 
            // btnOtherRemuneration
            // 
            this.btnOtherRemuneration.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnOtherRemuneration.Image = ((System.Drawing.Image)(resources.GetObject("btnOtherRemuneration.Image")));
            this.btnOtherRemuneration.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOtherRemuneration.Name = "btnOtherRemuneration";
            this.btnOtherRemuneration.Size = new System.Drawing.Size(23, 22);
            this.btnOtherRemuneration.Text = "Other Remuneration";
            this.btnOtherRemuneration.Click += new System.EventHandler(this.btnOtherRemuneration_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Location = new System.Drawing.Point(4, -1);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(114, 13);
            this.Label4.TabIndex = 0;
            this.Label4.Text = "Salary Information";
            // 
            // GrpMain
            // 
            this.GrpMain.Controls.Add(this.tbSalaryStructure);
            this.GrpMain.Controls.Add(this.btnEncashPolicy);
            this.GrpMain.Controls.Add(this.btnHolidayPolicy);
            this.GrpMain.Controls.Add(Label9);
            this.GrpMain.Controls.Add(Label8);
            this.GrpMain.Controls.Add(this.btnOvertimePolicy);
            this.GrpMain.Controls.Add(this.cboHolidayPolicy);
            this.GrpMain.Controls.Add(this.cboEncashPolicy);
            this.GrpMain.Controls.Add(this.btnAbsentPolicy);
            this.GrpMain.Controls.Add(this.cboOvertimePolicy);
            this.GrpMain.Controls.Add(Label5);
            this.GrpMain.Controls.Add(Label6);
            this.GrpMain.Controls.Add(Label7);
            this.GrpMain.Controls.Add(this.cboAbsentPolicy);
            this.GrpMain.Controls.Add(Label3);
            this.GrpMain.Controls.Add(this.Label4);
            this.GrpMain.Controls.Add(Label10);
            this.GrpMain.Controls.Add(this.cboCurrency);
            this.GrpMain.Controls.Add(this.CboSalaryday);
            this.GrpMain.Controls.Add(this.lblsalaryday);
            this.GrpMain.Controls.Add(this.txtRemarks);
            this.GrpMain.Controls.Add(this.txtNetAmount);
            this.GrpMain.Controls.Add(this.Label2);
            this.GrpMain.Controls.Add(this.txtDeduction);
            this.GrpMain.Controls.Add(this.txtAddition);
            this.GrpMain.Controls.Add(this.cboPayCalculation);
            this.GrpMain.Controls.Add(this.cboPayClassification);
            this.GrpMain.Controls.Add(this.Label1);
            this.GrpMain.Controls.Add(this.cboEmployee);
            this.GrpMain.Controls.Add(EmployeeIDLabel);
            this.GrpMain.Controls.Add(PaymentClassificationIDLabel);
            this.GrpMain.Controls.Add(PayCalculationTypeIDLabel);
            this.GrpMain.Location = new System.Drawing.Point(8, 57);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.Size = new System.Drawing.Size(580, 432);
            this.GrpMain.TabIndex = 8;
            this.GrpMain.TabStop = false;
            // 
            // tbSalaryStructure
            // 
            this.tbSalaryStructure.Controls.Add(this.General);
            this.tbSalaryStructure.Controls.Add(this.OtherRemuneration);
            this.tbSalaryStructure.Location = new System.Drawing.Point(4, 101);
            this.tbSalaryStructure.Name = "tbSalaryStructure";
            this.tbSalaryStructure.SelectedIndex = 0;
            this.tbSalaryStructure.Size = new System.Drawing.Size(564, 162);
            this.tbSalaryStructure.TabIndex = 1045;
            // 
            // General
            // 
            this.General.Controls.Add(this.dgvParticulars);
            this.General.Location = new System.Drawing.Point(4, 22);
            this.General.Name = "General";
            this.General.Padding = new System.Windows.Forms.Padding(3);
            this.General.Size = new System.Drawing.Size(556, 136);
            this.General.TabIndex = 0;
            this.General.Text = "General";
            this.General.UseVisualStyleBackColor = true;
            // 
            // dgvParticulars
            // 
            this.dgvParticulars.AddNewRow = false;
            this.dgvParticulars.AlphaNumericCols = new int[0];
            this.dgvParticulars.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvParticulars.CapsLockCols = new int[0];
            this.dgvParticulars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvParticulars.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColParticulars,
            this.CboCategory,
            this.dgvColAmount,
            this.dgvColSerialNo,
            this.dgvColIsAddition,
            this.dgvColDeductionPolicy});
            this.dgvParticulars.DecimalCols = new int[0];
            this.dgvParticulars.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvParticulars.HasSlNo = false;
            this.dgvParticulars.LastRowIndex = 0;
            this.dgvParticulars.Location = new System.Drawing.Point(3, 3);
            this.dgvParticulars.Name = "dgvParticulars";
            this.dgvParticulars.NegativeValueCols = new int[0];
            this.dgvParticulars.NumericCols = new int[0];
            this.dgvParticulars.Size = new System.Drawing.Size(550, 130);
            this.dgvParticulars.TabIndex = 7;
            this.dgvParticulars.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvParticulars_CellValueChanged);
            this.dgvParticulars.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvParticulars_CellBeginEdit);
            this.dgvParticulars.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvParticulars_CellEndEdit);
            this.dgvParticulars.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvParticulars_EditingControlShowing);
            this.dgvParticulars.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvParticulars_CurrentCellDirtyStateChanged);
            this.dgvParticulars.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvParticulars_DataError);
            this.dgvParticulars.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvParticulars_KeyDown);
            this.dgvParticulars.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvParticulars_RowsRemoved);
            // 
            // dgvColParticulars
            // 
            this.dgvColParticulars.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColParticulars.HeaderText = "Particulars";
            this.dgvColParticulars.Name = "dgvColParticulars";
            this.dgvColParticulars.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvColParticulars.Width = 200;
            // 
            // CboCategory
            // 
            this.CboCategory.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.CboCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CboCategory.HeaderText = "Category";
            this.CboCategory.Name = "CboCategory";
            this.CboCategory.Width = 77;
            // 
            // dgvColAmount
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomRight;
            this.dgvColAmount.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvColAmount.HeaderText = "Amount";
            this.dgvColAmount.MaxInputLength = 8;
            this.dgvColAmount.Name = "dgvColAmount";
            this.dgvColAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvColAmount.Width = 80;
            // 
            // dgvColSerialNo
            // 
            this.dgvColSerialNo.HeaderText = "SerialNo";
            this.dgvColSerialNo.Name = "dgvColSerialNo";
            this.dgvColSerialNo.Visible = false;
            // 
            // dgvColIsAddition
            // 
            this.dgvColIsAddition.HeaderText = "IsAddition";
            this.dgvColIsAddition.Name = "dgvColIsAddition";
            this.dgvColIsAddition.Visible = false;
            // 
            // dgvColDeductionPolicy
            // 
            this.dgvColDeductionPolicy.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvColDeductionPolicy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColDeductionPolicy.HeaderText = "Deduction Policy";
            this.dgvColDeductionPolicy.Name = "dgvColDeductionPolicy";
            // 
            // OtherRemuneration
            // 
            this.OtherRemuneration.Controls.Add(this.dgvOtherRemuneration);
            this.OtherRemuneration.Location = new System.Drawing.Point(4, 22);
            this.OtherRemuneration.Name = "OtherRemuneration";
            this.OtherRemuneration.Padding = new System.Windows.Forms.Padding(3);
            this.OtherRemuneration.Size = new System.Drawing.Size(556, 136);
            this.OtherRemuneration.TabIndex = 1;
            this.OtherRemuneration.Text = "Other Remuneration";
            this.OtherRemuneration.UseVisualStyleBackColor = true;
            // 
            // dgvOtherRemuneration
            // 
            this.dgvOtherRemuneration.AddNewRow = false;
            this.dgvOtherRemuneration.AlphaNumericCols = new int[0];
            this.dgvOtherRemuneration.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvOtherRemuneration.CapsLockCols = new int[0];
            this.dgvOtherRemuneration.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOtherRemuneration.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.OtherRemunerationParticular,
            this.OtherRemunerationAmount,
            this.OtherRemunerationDetailID});
            this.dgvOtherRemuneration.DecimalCols = new int[] {
        1};
            this.dgvOtherRemuneration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvOtherRemuneration.HasSlNo = false;
            this.dgvOtherRemuneration.LastRowIndex = 0;
            this.dgvOtherRemuneration.Location = new System.Drawing.Point(3, 3);
            this.dgvOtherRemuneration.Name = "dgvOtherRemuneration";
            this.dgvOtherRemuneration.NegativeValueCols = new int[0];
            this.dgvOtherRemuneration.NumericCols = new int[0];
            this.dgvOtherRemuneration.Size = new System.Drawing.Size(550, 130);
            this.dgvOtherRemuneration.TabIndex = 8;
            this.dgvOtherRemuneration.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.OtherRemunerationGridView_CellValueChanged);
            this.dgvOtherRemuneration.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvOtherRemuneration_CellBeginEdit);
            this.dgvOtherRemuneration.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOtherRemuneration_CellEndEdit);
            this.dgvOtherRemuneration.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvOtherRemuneration_EditingControlShowing);
            this.dgvOtherRemuneration.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OtherRemunerationGridView_KeyDown);
            // 
            // OtherRemunerationParticular
            // 
            this.OtherRemunerationParticular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OtherRemunerationParticular.HeaderText = "Particulars";
            this.OtherRemunerationParticular.Name = "OtherRemunerationParticular";
            this.OtherRemunerationParticular.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.OtherRemunerationParticular.Width = 200;
            // 
            // OtherRemunerationAmount
            // 
            this.OtherRemunerationAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomRight;
            this.OtherRemunerationAmount.DefaultCellStyle = dataGridViewCellStyle6;
            this.OtherRemunerationAmount.HeaderText = "Amount";
            this.OtherRemunerationAmount.MaxInputLength = 8;
            this.OtherRemunerationAmount.Name = "OtherRemunerationAmount";
            this.OtherRemunerationAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // OtherRemunerationDetailID
            // 
            this.OtherRemunerationDetailID.HeaderText = "DetailID";
            this.OtherRemunerationDetailID.Name = "OtherRemunerationDetailID";
            this.OtherRemunerationDetailID.Visible = false;
            // 
            // btnEncashPolicy
            // 
            this.btnEncashPolicy.Location = new System.Drawing.Point(536, 332);
            this.btnEncashPolicy.Name = "btnEncashPolicy";
            this.btnEncashPolicy.Size = new System.Drawing.Size(32, 23);
            this.btnEncashPolicy.TabIndex = 18;
            this.btnEncashPolicy.Text = "...";
            this.btnEncashPolicy.UseVisualStyleBackColor = true;
            this.btnEncashPolicy.Click += new System.EventHandler(this.btnEncashPolicy_Click);
            // 
            // btnHolidayPolicy
            // 
            this.btnHolidayPolicy.Location = new System.Drawing.Point(239, 330);
            this.btnHolidayPolicy.Name = "btnHolidayPolicy";
            this.btnHolidayPolicy.Size = new System.Drawing.Size(32, 23);
            this.btnHolidayPolicy.TabIndex = 16;
            this.btnHolidayPolicy.Text = "...";
            this.btnHolidayPolicy.UseVisualStyleBackColor = true;
            this.btnHolidayPolicy.Click += new System.EventHandler(this.btnHolidayPolicy_Click);
            // 
            // btnOvertimePolicy
            // 
            this.btnOvertimePolicy.Location = new System.Drawing.Point(536, 304);
            this.btnOvertimePolicy.Name = "btnOvertimePolicy";
            this.btnOvertimePolicy.Size = new System.Drawing.Size(32, 23);
            this.btnOvertimePolicy.TabIndex = 14;
            this.btnOvertimePolicy.Text = "...";
            this.btnOvertimePolicy.UseVisualStyleBackColor = true;
            this.btnOvertimePolicy.Click += new System.EventHandler(this.btnOvertimePolicy_Click);
            // 
            // cboHolidayPolicy
            // 
            this.cboHolidayPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboHolidayPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboHolidayPolicy.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cboHolidayPolicy.DropDownHeight = 134;
            this.cboHolidayPolicy.FormattingEnabled = true;
            this.cboHolidayPolicy.IntegralHeight = false;
            this.cboHolidayPolicy.Location = new System.Drawing.Point(91, 332);
            this.cboHolidayPolicy.Name = "cboHolidayPolicy";
            this.cboHolidayPolicy.Size = new System.Drawing.Size(145, 21);
            this.cboHolidayPolicy.TabIndex = 15;
            this.cboHolidayPolicy.SelectedIndexChanged += new System.EventHandler(this.cboHolidayPolicy_SelectedIndexChanged);
            this.cboHolidayPolicy.TextChanged += new System.EventHandler(this.cboHolidayPolicy_TextChanged);
            // 
            // cboEncashPolicy
            // 
            this.cboEncashPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEncashPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEncashPolicy.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cboEncashPolicy.DropDownHeight = 134;
            this.cboEncashPolicy.FormattingEnabled = true;
            this.cboEncashPolicy.IntegralHeight = false;
            this.cboEncashPolicy.Location = new System.Drawing.Point(381, 332);
            this.cboEncashPolicy.Name = "cboEncashPolicy";
            this.cboEncashPolicy.Size = new System.Drawing.Size(149, 21);
            this.cboEncashPolicy.TabIndex = 17;
            this.cboEncashPolicy.SelectedIndexChanged += new System.EventHandler(this.cboEncashPolicy_SelectedIndexChanged);
            this.cboEncashPolicy.TextChanged += new System.EventHandler(this.cboEncashPolicy_TextChanged);
            // 
            // btnAbsentPolicy
            // 
            this.btnAbsentPolicy.Location = new System.Drawing.Point(239, 304);
            this.btnAbsentPolicy.Name = "btnAbsentPolicy";
            this.btnAbsentPolicy.Size = new System.Drawing.Size(32, 23);
            this.btnAbsentPolicy.TabIndex = 12;
            this.btnAbsentPolicy.Text = "...";
            this.btnAbsentPolicy.UseVisualStyleBackColor = true;
            this.btnAbsentPolicy.Click += new System.EventHandler(this.btnAbsentPolicy_Click);
            // 
            // cboOvertimePolicy
            // 
            this.cboOvertimePolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOvertimePolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOvertimePolicy.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cboOvertimePolicy.DropDownHeight = 134;
            this.cboOvertimePolicy.FormattingEnabled = true;
            this.cboOvertimePolicy.IntegralHeight = false;
            this.cboOvertimePolicy.Location = new System.Drawing.Point(381, 304);
            this.cboOvertimePolicy.Name = "cboOvertimePolicy";
            this.cboOvertimePolicy.Size = new System.Drawing.Size(149, 21);
            this.cboOvertimePolicy.TabIndex = 13;
            this.cboOvertimePolicy.SelectedIndexChanged += new System.EventHandler(this.cboOvertimePolicy_SelectedIndexChanged);
            this.cboOvertimePolicy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboOvertimePolicy_KeyDown);
            this.cboOvertimePolicy.TextChanged += new System.EventHandler(this.cboOvertimePolicy_TextChanged);
            // 
            // cboAbsentPolicy
            // 
            this.cboAbsentPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAbsentPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAbsentPolicy.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cboAbsentPolicy.DropDownHeight = 134;
            this.cboAbsentPolicy.FormattingEnabled = true;
            this.cboAbsentPolicy.IntegralHeight = false;
            this.cboAbsentPolicy.Location = new System.Drawing.Point(91, 304);
            this.cboAbsentPolicy.Name = "cboAbsentPolicy";
            this.cboAbsentPolicy.Size = new System.Drawing.Size(145, 21);
            this.cboAbsentPolicy.TabIndex = 11;
            this.cboAbsentPolicy.SelectedIndexChanged += new System.EventHandler(this.cboAbsentPolicy_SelectedIndexChanged);
            this.cboAbsentPolicy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboAbsentPolicy_KeyDown);
            this.cboAbsentPolicy.TextChanged += new System.EventHandler(this.cboAbsentPolicy_TextChanged);
            // 
            // cboCurrency
            // 
            this.cboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCurrency.BackColor = System.Drawing.SystemColors.Info;
            this.cboCurrency.DropDownHeight = 134;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.IntegralHeight = false;
            this.cboCurrency.Location = new System.Drawing.Point(83, 45);
            this.cboCurrency.MaxDropDownItems = 10;
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(253, 21);
            this.cboCurrency.TabIndex = 2;
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            this.cboCurrency.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCurrency_KeyDown);
            // 
            // CboSalaryday
            // 
            this.CboSalaryday.BackColor = System.Drawing.SystemColors.Info;
            this.CboSalaryday.DropDownHeight = 134;
            this.CboSalaryday.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboSalaryday.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CboSalaryday.FormattingEnabled = true;
            this.CboSalaryday.IntegralHeight = false;
            this.CboSalaryday.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28"});
            this.CboSalaryday.Location = new System.Drawing.Point(119, 72);
            this.CboSalaryday.Name = "CboSalaryday";
            this.CboSalaryday.Size = new System.Drawing.Size(78, 21);
            this.CboSalaryday.TabIndex = 4;
            this.CboSalaryday.SelectedValueChanged += new System.EventHandler(this.CboSalaryday_SelectedValueChanged);
            this.CboSalaryday.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboSalaryday_KeyDown);
            // 
            // lblsalaryday
            // 
            this.lblsalaryday.AutoSize = true;
            this.lblsalaryday.Location = new System.Drawing.Point(14, 75);
            this.lblsalaryday.Name = "lblsalaryday";
            this.lblsalaryday.Size = new System.Drawing.Size(99, 13);
            this.lblsalaryday.TabIndex = 1021;
            this.lblsalaryday.Text = "Salary Process Day";
            // 
            // txtRemarks
            // 
            this.txtRemarks.BackColor = System.Drawing.SystemColors.Window;
            this.txtRemarks.Location = new System.Drawing.Point(17, 375);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(551, 49);
            this.txtRemarks.TabIndex = 19;
            this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
            // 
            // txtNetAmount
            // 
            this.txtNetAmount.Enabled = false;
            this.txtNetAmount.Location = new System.Drawing.Point(447, 267);
            this.txtNetAmount.Name = "txtNetAmount";
            this.txtNetAmount.Size = new System.Drawing.Size(121, 20);
            this.txtNetAmount.TabIndex = 10;
            this.txtNetAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(378, 271);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(63, 13);
            this.Label2.TabIndex = 1005;
            this.Label2.Text = "Net Amount";
            // 
            // txtDeduction
            // 
            this.txtDeduction.Enabled = false;
            this.txtDeduction.Location = new System.Drawing.Point(275, 267);
            this.txtDeduction.Name = "txtDeduction";
            this.txtDeduction.Size = new System.Drawing.Size(93, 20);
            this.txtDeduction.TabIndex = 9;
            this.txtDeduction.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAddition
            // 
            this.txtAddition.Enabled = false;
            this.txtAddition.Location = new System.Drawing.Point(116, 267);
            this.txtAddition.Name = "txtAddition";
            this.txtAddition.Size = new System.Drawing.Size(93, 20);
            this.txtAddition.TabIndex = 8;
            this.txtAddition.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboPayCalculation
            // 
            this.cboPayCalculation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPayCalculation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPayCalculation.BackColor = System.Drawing.SystemColors.Info;
            this.cboPayCalculation.DropDownHeight = 134;
            this.cboPayCalculation.Enabled = false;
            this.cboPayCalculation.FormattingEnabled = true;
            this.cboPayCalculation.IntegralHeight = false;
            this.cboPayCalculation.Location = new System.Drawing.Point(462, 45);
            this.cboPayCalculation.Name = "cboPayCalculation";
            this.cboPayCalculation.Size = new System.Drawing.Size(106, 21);
            this.cboPayCalculation.TabIndex = 3;
            // 
            // cboPayClassification
            // 
            this.cboPayClassification.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPayClassification.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPayClassification.BackColor = System.Drawing.SystemColors.Info;
            this.cboPayClassification.DropDownHeight = 134;
            this.cboPayClassification.Enabled = false;
            this.cboPayClassification.FormattingEnabled = true;
            this.cboPayClassification.IntegralHeight = false;
            this.cboPayClassification.Location = new System.Drawing.Point(462, 19);
            this.cboPayClassification.Name = "cboPayClassification";
            this.cboPayClassification.Size = new System.Drawing.Size(106, 21);
            this.cboPayClassification.TabIndex = 1;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(14, 358);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(58, 13);
            this.Label1.TabIndex = 1000;
            this.Label1.Text = "Remarks";
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(83, 19);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(253, 21);
            this.cboEmployee.TabIndex = 0;
            this.cboEmployee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboEmployee_KeyDown);
            // 
            // BtnIncentive
            // 
            this.BtnIncentive.Location = new System.Drawing.Point(890, 146);
            this.BtnIncentive.Name = "BtnIncentive";
            this.BtnIncentive.Size = new System.Drawing.Size(32, 25);
            this.BtnIncentive.TabIndex = 7;
            this.BtnIncentive.Text = "...";
            this.BtnIncentive.UseVisualStyleBackColor = true;
            this.BtnIncentive.Visible = false;
            // 
            // BasicPayTextBox
            // 
            this.BasicPayTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.BasicPayTextBox.Location = new System.Drawing.Point(787, 262);
            this.BasicPayTextBox.MaxLength = 10;
            this.BasicPayTextBox.Name = "BasicPayTextBox";
            this.BasicPayTextBox.Size = new System.Drawing.Size(178, 20);
            this.BasicPayTextBox.TabIndex = 4;
            this.BasicPayTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.BasicPayTextBox.Visible = false;
            // 
            // CboIncentiveType
            // 
            this.CboIncentiveType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboIncentiveType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboIncentiveType.DropDownHeight = 134;
            this.CboIncentiveType.FormattingEnabled = true;
            this.CboIncentiveType.IntegralHeight = false;
            this.CboIncentiveType.Location = new System.Drawing.Point(706, 150);
            this.CboIncentiveType.Name = "CboIncentiveType";
            this.CboIncentiveType.Size = new System.Drawing.Size(178, 21);
            this.CboIncentiveType.TabIndex = 6;
            this.CboIncentiveType.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(514, 493);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(8, 493);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(433, 493);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // StatusStrip2
            // 
            this.StatusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel2,
            this.lblStatus});
            this.StatusStrip2.Location = new System.Drawing.Point(0, 521);
            this.StatusStrip2.Name = "StatusStrip2";
            this.StatusStrip2.Size = new System.Drawing.Size(597, 22);
            this.StatusStrip2.TabIndex = 12;
            this.StatusStrip2.Text = "StatusStrip2";
            // 
            // ToolStripStatusLabel2
            // 
            this.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2";
            this.ToolStripStatusLabel2.Size = new System.Drawing.Size(0, 17);
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // errProSalaryStructure
            // 
            this.errProSalaryStructure.ContainerControl = this;
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.label12);
            this.bSearch.Controls.Add(this.cboCompany);
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(597, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 1010;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(23, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 15);
            this.label12.TabIndex = 3;
            this.label12.Text = "Company";
            // 
            // cboCompany
            // 
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 17;
            this.cboCompany.Location = new System.Drawing.Point(91, 2);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(172, 23);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 2;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCompany_KeyDown);
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(269, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(295, 23);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Stretch = true;
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyBooksERP.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // controlContainerItem1
            // 
            this.controlContainerItem1.AllowItemResize = false;
            this.controlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem1.Name = "controlContainerItem1";
            // 
            // FrmSalaryStructure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 543);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.StatusStrip2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.GrpMain);
            this.Controls.Add(this.EmployeeSalaryHeadBindingNavigator);
            this.Controls.Add(this.CboIncentiveType);
            this.Controls.Add(this.BtnIncentive);
            this.Controls.Add(IncentiveTypeIDLabel);
            this.Controls.Add(this.BasicPayTextBox);
            this.Controls.Add(BasicPayLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSalaryStructure";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Salary Structure";
            this.Load += new System.EventHandler(this.FrmSalaryStructure_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSalaryStructure_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmSalaryStructure_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSalaryHeadBindingNavigator)).EndInit();
            this.EmployeeSalaryHeadBindingNavigator.ResumeLayout(false);
            this.EmployeeSalaryHeadBindingNavigator.PerformLayout();
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            this.tbSalaryStructure.ResumeLayout(false);
            this.General.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvParticulars)).EndInit();
            this.OtherRemuneration.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOtherRemuneration)).EndInit();
            this.StatusStrip2.ResumeLayout(false);
            this.StatusStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errProSalaryStructure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.bSearch.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator EmployeeSalaryHeadBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton CancelToolStripButton;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.GroupBox GrpMain;
        internal System.Windows.Forms.Button btnOvertimePolicy;
        internal System.Windows.Forms.Button btnEncashPolicy;
        internal System.Windows.Forms.Button btnHolidayPolicy;
        internal System.Windows.Forms.Button btnAbsentPolicy;
        internal System.Windows.Forms.ComboBox cboOvertimePolicy;
        internal System.Windows.Forms.ComboBox cboHolidayPolicy;
        internal System.Windows.Forms.ComboBox cboEncashPolicy;
        internal System.Windows.Forms.ComboBox cboAbsentPolicy;
        internal System.Windows.Forms.ComboBox cboCurrency;
        internal System.Windows.Forms.ComboBox CboSalaryday;
        internal System.Windows.Forms.Label lblsalaryday;
        internal System.Windows.Forms.Button BtnIncentive;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.TextBox txtNetAmount;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox txtDeduction;
        internal System.Windows.Forms.TextBox txtAddition;
        internal System.Windows.Forms.ComboBox cboPayCalculation;
        internal System.Windows.Forms.ComboBox cboPayClassification;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal System.Windows.Forms.TextBox BasicPayTextBox;
        internal System.Windows.Forms.ComboBox CboIncentiveType;
        private DemoClsDataGridview.ClsDataGirdView dgvParticulars;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.StatusStrip StatusStrip2;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel2;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.ToolStripButton btnAdditionDeduction;
        private System.Windows.Forms.ErrorProvider errProSalaryStructure;
        private System.Windows.Forms.Timer tmrClear;
        internal System.Windows.Forms.ToolStripButton btnDeductionPolicy;
        private System.Windows.Forms.TabControl tbSalaryStructure;
        private System.Windows.Forms.TabPage General;
        private System.Windows.Forms.TabPage OtherRemuneration;
        private DemoClsDataGridview.ClsDataGirdView dgvOtherRemuneration;
        private System.Windows.Forms.DataGridViewComboBoxColumn OtherRemunerationParticular;
        private System.Windows.Forms.DataGridViewTextBoxColumn OtherRemunerationAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn OtherRemunerationDetailID;
        internal System.Windows.Forms.ToolStripButton btnOtherRemuneration;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private DevComponents.DotNetBar.Bar bSearch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColParticulars;
        private System.Windows.Forms.DataGridViewComboBoxColumn CboCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColSerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColIsAddition;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColDeductionPolicy;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem1;
        private System.Windows.Forms.Label label12;
    }
}