﻿namespace MyBooksERP
{
    partial class FrmSettlementEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label12;
            System.Windows.Forms.Label Label4;
            System.Windows.Forms.Label LblDeduction;
            System.Windows.Forms.Label LblEarning;
            System.Windows.Forms.Label Label6;
            System.Windows.Forms.Label Label5;
            System.Windows.Forms.Label DateLabel;
            System.Windows.Forms.Label NetAmountLabel;
            System.Windows.Forms.Label Label1;
            System.Windows.Forms.Label lblCheqNo;
            System.Windows.Forms.Label lblCheqDate;
            System.Windows.Forms.Label label16;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSettlementEntry));
            System.Windows.Forms.Label lblAccount;
            this.txtAbsentDays = new System.Windows.Forms.TextBox();
            this.chkAbsent = new System.Windows.Forms.CheckBox();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.txtNofDaysExperience = new System.Windows.Forms.TextBox();
            this.lblTakenLeavePayDays = new System.Windows.Forms.Label();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.EmployeeSettlementBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.lblEligibleLeavePayDays = new System.Windows.Forms.Label();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Label11 = new System.Windows.Forms.Label();
            this.EmployeeSettlementBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BtnCancel = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnPrintRpt = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.Label10 = new System.Windows.Forms.Label();
            this.LblEmployeeSettlement = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusStripEmployeeSettlement = new System.Windows.Forms.StatusStrip();
            this.OKSaveButton = new System.Windows.Forms.Button();
            this.TmrEmployeeSettlement = new System.Windows.Forms.Timer(this.components);
            this.ErrEmployeeSettlement = new System.Windows.Forms.ErrorProvider(this.components);
            this.txtLeavePayDays = new System.Windows.Forms.TextBox();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.lblSettlementPolicy = new System.Windows.Forms.Label();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.DtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblNoofMonths = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.EmployeeSettlementBindingSourcerpt = new System.Windows.Forms.BindingSource(this.components);
            this.RemarksTextBox1 = new System.Windows.Forms.TextBox();
            this.chkIsAddition = new System.Windows.Forms.CheckBox();
            this.TxtParticulars = new System.Windows.Forms.TextBox();
            this.BtnBottomCancel = new System.Windows.Forms.Button();
            this.AddBtn = new System.Windows.Forms.Button();
            this.CboEmployee = new System.Windows.Forms.ComboBox();
            this.GrpEmployeeSettlement = new System.Windows.Forms.GroupBox();
            this.SettlementTab = new System.Windows.Forms.TabControl();
            this.SettlementTabEmpInfo = new System.Windows.Forms.TabPage();
            this.DocumentReturnCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtExcludeHolidays = new System.Windows.Forms.TextBox();
            this.txtExcludeLeaveDays = new System.Windows.Forms.TextBox();
            this.chkExcludeLeaveDays = new System.Windows.Forms.CheckBox();
            this.txtEligibleDays = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkExcludeHolidays = new System.Windows.Forms.CheckBox();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboTransactionType = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dtpChequeDate = new System.Windows.Forms.DateTimePicker();
            this.txtChequeNo = new System.Windows.Forms.TextBox();
            this.BtnProcess = new System.Windows.Forms.Button();
            this.lblToDate = new System.Windows.Forms.Label();
            this.LblType = new System.Windows.Forms.Label();
            this.DtpToDate = new System.Windows.Forms.DateTimePicker();
            this.DateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.CboType = new System.Windows.Forms.ComboBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.SettlementTabParticulars = new System.Windows.Forms.TabPage();
            this.EmployeeSettlementDetailDataGridView = new System.Windows.Forms.DataGridView();
            this.EmployeeSettlementDetailID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Particulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Credit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Debit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RepaymentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoanID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsAddition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalaryPaymentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LblCurrency = new System.Windows.Forms.Label();
            this.NetAmountTextBox = new System.Windows.Forms.TextBox();
            this.TxtTotalDeduction = new System.Windows.Forms.TextBox();
            this.TxtTotalEarning = new System.Windows.Forms.TextBox();
            this.SettlementTabHistory = new System.Windows.Forms.TabPage();
            this.DtGrdEmpHistory = new System.Windows.Forms.DataGridView();
            this.ColDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFromDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColToDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GratuityTab = new System.Windows.Forms.TabPage();
            this.DtGrdGratuity = new System.Windows.Forms.DataGridView();
            this.ColYears = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCalDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColGRTYDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColGratuityAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LblEmployee = new System.Windows.Forms.Label();
            this.TextBoxNumeric = new System.Windows.Forms.TextBox();
            this.EmployeeSettlementDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.BtnSave = new System.Windows.Forms.Button();
            this.lblPayBack = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            this.cboAccount = new System.Windows.Forms.ComboBox();
            Label12 = new System.Windows.Forms.Label();
            Label4 = new System.Windows.Forms.Label();
            LblDeduction = new System.Windows.Forms.Label();
            LblEarning = new System.Windows.Forms.Label();
            Label6 = new System.Windows.Forms.Label();
            Label5 = new System.Windows.Forms.Label();
            DateLabel = new System.Windows.Forms.Label();
            NetAmountLabel = new System.Windows.Forms.Label();
            Label1 = new System.Windows.Forms.Label();
            lblCheqNo = new System.Windows.Forms.Label();
            lblCheqDate = new System.Windows.Forms.Label();
            label16 = new System.Windows.Forms.Label();
            lblAccount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementBindingNavigator)).BeginInit();
            this.EmployeeSettlementBindingNavigator.SuspendLayout();
            this.StatusStripEmployeeSettlement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrEmployeeSettlement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementBindingSourcerpt)).BeginInit();
            this.GrpEmployeeSettlement.SuspendLayout();
            this.SettlementTab.SuspendLayout();
            this.SettlementTabEmpInfo.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SettlementTabParticulars.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementDetailDataGridView)).BeginInit();
            this.SettlementTabHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtGrdEmpHistory)).BeginInit();
            this.GratuityTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtGrdGratuity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label12
            // 
            Label12.AutoSize = true;
            Label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label12.Location = new System.Drawing.Point(237, 53);
            Label12.Name = "Label12";
            Label12.Size = new System.Drawing.Size(67, 13);
            Label12.TabIndex = 7;
            Label12.Text = "Leave Pay";
            // 
            // Label4
            // 
            Label4.AutoSize = true;
            Label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label4.Location = new System.Drawing.Point(5, 53);
            Label4.Name = "Label4";
            Label4.Size = new System.Drawing.Size(51, 13);
            Label4.TabIndex = 0;
            Label4.Text = "Gratuity";
            // 
            // LblDeduction
            // 
            LblDeduction.AutoSize = true;
            LblDeduction.BackColor = System.Drawing.Color.Transparent;
            LblDeduction.FlatStyle = System.Windows.Forms.FlatStyle.System;
            LblDeduction.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Italic);
            LblDeduction.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            LblDeduction.Location = new System.Drawing.Point(352, 232);
            LblDeduction.Name = "LblDeduction";
            LblDeduction.Size = new System.Drawing.Size(42, 9);
            LblDeduction.TabIndex = 134;
            LblDeduction.Text = "Debit Total";
            // 
            // LblEarning
            // 
            LblEarning.AutoSize = true;
            LblEarning.BackColor = System.Drawing.Color.Transparent;
            LblEarning.FlatStyle = System.Windows.Forms.FlatStyle.System;
            LblEarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Italic);
            LblEarning.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            LblEarning.Location = new System.Drawing.Point(199, 232);
            LblEarning.Name = "LblEarning";
            LblEarning.Size = new System.Drawing.Size(45, 9);
            LblEarning.TabIndex = 132;
            LblEarning.Text = "Credit Total";
            // 
            // Label6
            // 
            Label6.AutoSize = true;
            Label6.Location = new System.Drawing.Point(6, 7);
            Label6.Name = "Label6";
            Label6.Size = new System.Drawing.Size(51, 13);
            Label6.TabIndex = 0;
            Label6.Text = "Particular";
            // 
            // Label5
            // 
            Label5.AutoSize = true;
            Label5.Location = new System.Drawing.Point(179, 7);
            Label5.Name = "Label5";
            Label5.Size = new System.Drawing.Size(43, 13);
            Label5.TabIndex = 187;
            Label5.Text = "Amount";
            // 
            // DateLabel
            // 
            DateLabel.AutoSize = true;
            DateLabel.Location = new System.Drawing.Point(13, 79);
            DateLabel.Name = "DateLabel";
            DateLabel.Size = new System.Drawing.Size(71, 13);
            DateLabel.TabIndex = 5;
            DateLabel.Text = "Process Date";
            DateLabel.Visible = false;
            // 
            // NetAmountLabel
            // 
            NetAmountLabel.AutoSize = true;
            NetAmountLabel.Location = new System.Drawing.Point(281, 254);
            NetAmountLabel.Name = "NetAmountLabel";
            NetAmountLabel.Size = new System.Drawing.Size(63, 13);
            NetAmountLabel.TabIndex = 8;
            NetAmountLabel.Text = "Net Amount";
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label1.Location = new System.Drawing.Point(6, 3);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(87, 13);
            Label1.TabIndex = 0;
            Label1.Text = "Employee Info";
            // 
            // lblCheqNo
            // 
            lblCheqNo.AutoSize = true;
            lblCheqNo.Location = new System.Drawing.Point(5, 40);
            lblCheqNo.Name = "lblCheqNo";
            lblCheqNo.Size = new System.Drawing.Size(46, 13);
            lblCheqNo.TabIndex = 2;
            lblCheqNo.Text = "Chq. No";
            // 
            // lblCheqDate
            // 
            lblCheqDate.AutoSize = true;
            lblCheqDate.Location = new System.Drawing.Point(5, 66);
            lblCheqDate.Name = "lblCheqDate";
            lblCheqDate.Size = new System.Drawing.Size(55, 13);
            lblCheqDate.TabIndex = 4;
            lblCheqDate.Text = "Chq. Date";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label16.Location = new System.Drawing.Point(4, 9);
            label16.Name = "label16";
            label16.Size = new System.Drawing.Size(70, 13);
            label16.TabIndex = 26;
            label16.Text = "Experience";
            // 
            // txtAbsentDays
            // 
            this.txtAbsentDays.Enabled = false;
            this.txtAbsentDays.Location = new System.Drawing.Point(366, 124);
            this.txtAbsentDays.MaxLength = 9;
            this.txtAbsentDays.Name = "txtAbsentDays";
            this.txtAbsentDays.Size = new System.Drawing.Size(116, 20);
            this.txtAbsentDays.TabIndex = 11;
            this.txtAbsentDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAbsentDays.TextChanged += new System.EventHandler(this.txtAbsentDays_TextChanged);
            // 
            // chkAbsent
            // 
            this.chkAbsent.AutoSize = true;
            this.chkAbsent.Location = new System.Drawing.Point(248, 126);
            this.chkAbsent.Name = "chkAbsent";
            this.chkAbsent.Size = new System.Drawing.Size(103, 17);
            this.chkAbsent.TabIndex = 10;
            this.chkAbsent.Text = "Consider Absent";
            this.chkAbsent.UseVisualStyleBackColor = true;
            this.chkAbsent.CheckedChanged += new System.EventHandler(this.chkAbsent_CheckedChanged);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(245, 155);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(73, 13);
            this.Label9.TabIndex = 12;
            this.Label9.Text = "Balance Days";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(245, 31);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(58, 13);
            this.Label13.TabIndex = 3;
            this.Label13.Text = "Total Days";
            // 
            // txtNofDaysExperience
            // 
            this.txtNofDaysExperience.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNofDaysExperience.Location = new System.Drawing.Point(317, 31);
            this.txtNofDaysExperience.MaxLength = 9;
            this.txtNofDaysExperience.Name = "txtNofDaysExperience";
            this.txtNofDaysExperience.Size = new System.Drawing.Size(106, 13);
            this.txtNofDaysExperience.TabIndex = 4;
            this.txtNofDaysExperience.Text = "NIL";
            this.txtNofDaysExperience.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // lblTakenLeavePayDays
            // 
            this.lblTakenLeavePayDays.AutoSize = true;
            this.lblTakenLeavePayDays.Location = new System.Drawing.Point(363, 102);
            this.lblTakenLeavePayDays.Name = "lblTakenLeavePayDays";
            this.lblTakenLeavePayDays.Size = new System.Drawing.Size(24, 13);
            this.lblTakenLeavePayDays.TabIndex = 6;
            this.lblTakenLeavePayDays.Text = "NIL";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // EmployeeSettlementBindingNavigatorSaveItem
            // 
            this.EmployeeSettlementBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.EmployeeSettlementBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("EmployeeSettlementBindingNavigatorSaveItem.Image")));
            this.EmployeeSettlementBindingNavigatorSaveItem.Name = "EmployeeSettlementBindingNavigatorSaveItem";
            this.EmployeeSettlementBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.EmployeeSettlementBindingNavigatorSaveItem.Text = "Save Data";
            this.EmployeeSettlementBindingNavigatorSaveItem.Click += new System.EventHandler(this.EmployeeSettlementBindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // lblEligibleLeavePayDays
            // 
            this.lblEligibleLeavePayDays.AutoSize = true;
            this.lblEligibleLeavePayDays.Location = new System.Drawing.Point(363, 76);
            this.lblEligibleLeavePayDays.Name = "lblEligibleLeavePayDays";
            this.lblEligibleLeavePayDays.Size = new System.Drawing.Size(24, 13);
            this.lblEligibleLeavePayDays.TabIndex = 9;
            this.lblEligibleLeavePayDays.Text = "NIL";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(245, 102);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(65, 13);
            this.Label11.TabIndex = 5;
            this.Label11.Text = "Taken Days";
            // 
            // EmployeeSettlementBindingNavigator
            // 
            this.EmployeeSettlementBindingNavigator.AddNewItem = null;
            this.EmployeeSettlementBindingNavigator.CountItem = null;
            this.EmployeeSettlementBindingNavigator.DeleteItem = null;
            this.EmployeeSettlementBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.EmployeeSettlementBindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BtnCancel,
            this.ToolStripSeparator1,
            this.BtnPrint,
            this.BtnPrintRpt,
            this.btnEmail,
            this.ToolStripSeparator3,
            this.BtnHelp});
            this.EmployeeSettlementBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.EmployeeSettlementBindingNavigator.MoveFirstItem = null;
            this.EmployeeSettlementBindingNavigator.MoveLastItem = null;
            this.EmployeeSettlementBindingNavigator.MoveNextItem = null;
            this.EmployeeSettlementBindingNavigator.MovePreviousItem = null;
            this.EmployeeSettlementBindingNavigator.Name = "EmployeeSettlementBindingNavigator";
            this.EmployeeSettlementBindingNavigator.PositionItem = null;
            this.EmployeeSettlementBindingNavigator.Size = new System.Drawing.Size(544, 25);
            this.EmployeeSettlementBindingNavigator.TabIndex = 0;
            this.EmployeeSettlementBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnCancel.Image = ((System.Drawing.Image)(resources.GetObject("BtnCancel.Image")));
            this.BtnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(23, 22);
            this.BtnCancel.ToolTipText = "Clear";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.ToolTipText = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnPrintRpt
            // 
            this.BtnPrintRpt.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrintRpt.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrintRpt.Image")));
            this.BtnPrintRpt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrintRpt.Name = "BtnPrintRpt";
            this.BtnPrintRpt.Size = new System.Drawing.Size(23, 22);
            this.BtnPrintRpt.ToolTipText = "Print";
            this.BtnPrintRpt.Visible = false;
            this.BtnPrintRpt.Click += new System.EventHandler(this.BtnPrintRpt_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = ((System.Drawing.Image)(resources.GetObject("btnEmail.Image")));
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.ToolTipText = "Email";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(245, 76);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(67, 13);
            this.Label10.TabIndex = 8;
            this.Label10.Text = "Eligible Days";
            // 
            // LblEmployeeSettlement
            // 
            this.LblEmployeeSettlement.Name = "LblEmployeeSettlement";
            this.LblEmployeeSettlement.Size = new System.Drawing.Size(0, 17);
            // 
            // StatusStripEmployeeSettlement
            // 
            this.StatusStripEmployeeSettlement.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblEmployeeSettlement});
            this.StatusStripEmployeeSettlement.Location = new System.Drawing.Point(0, 542);
            this.StatusStripEmployeeSettlement.Name = "StatusStripEmployeeSettlement";
            this.StatusStripEmployeeSettlement.Size = new System.Drawing.Size(544, 22);
            this.StatusStripEmployeeSettlement.TabIndex = 172;
            this.StatusStripEmployeeSettlement.Text = "StatusStrip1";
            // 
            // OKSaveButton
            // 
            this.OKSaveButton.Location = new System.Drawing.Point(381, 516);
            this.OKSaveButton.Name = "OKSaveButton";
            this.OKSaveButton.Size = new System.Drawing.Size(75, 23);
            this.OKSaveButton.TabIndex = 3;
            this.OKSaveButton.Text = "&Confirm";
            this.OKSaveButton.UseVisualStyleBackColor = true;
            this.OKSaveButton.Click += new System.EventHandler(this.OKSaveButton_Click);
            // 
            // TmrEmployeeSettlement
            // 
            this.TmrEmployeeSettlement.Interval = 1000;
            // 
            // ErrEmployeeSettlement
            // 
            this.ErrEmployeeSettlement.ContainerControl = this;
            this.ErrEmployeeSettlement.RightToLeft = true;
            // 
            // txtLeavePayDays
            // 
            this.txtLeavePayDays.Location = new System.Drawing.Point(366, 152);
            this.txtLeavePayDays.MaxLength = 9;
            this.txtLeavePayDays.Name = "txtLeavePayDays";
            this.txtLeavePayDays.Size = new System.Drawing.Size(116, 20);
            this.txtLeavePayDays.TabIndex = 13;
            this.txtLeavePayDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLeavePayDays.TextChanged += new System.EventHandler(this.txtLeavePayDays_TextChanged);
            this.txtLeavePayDays.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(186, 22);
            this.txtAmount.MaxLength = 13;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(160, 20);
            this.txtAmount.TabIndex = 2;
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // lblSettlementPolicy
            // 
            this.lblSettlementPolicy.AutoSize = true;
            this.lblSettlementPolicy.Location = new System.Drawing.Point(357, 15);
            this.lblSettlementPolicy.Name = "lblSettlementPolicy";
            this.lblSettlementPolicy.Size = new System.Drawing.Size(24, 13);
            this.lblSettlementPolicy.TabIndex = 10;
            this.lblSettlementPolicy.Text = "NIL";
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Location = new System.Drawing.Point(13, 106);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(66, 13);
            this.lblFromDate.TabIndex = 7;
            this.lblFromDate.Text = "Joining Date";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(16, 31);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(69, 13);
            this.Label8.TabIndex = 1;
            this.Label8.Text = "Total Months";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(246, 15);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(105, 13);
            this.Label3.TabIndex = 9;
            this.Label3.Text = "Settlement Policy";
            // 
            // DtpFromDate
            // 
            this.DtpFromDate.CustomFormat = "dd-MMM-yyyy";
            this.DtpFromDate.Enabled = false;
            this.DtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFromDate.Location = new System.Drawing.Point(106, 102);
            this.DtpFromDate.Name = "DtpFromDate";
            this.DtpFromDate.Size = new System.Drawing.Size(106, 20);
            this.DtpFromDate.TabIndex = 8;
            this.DtpFromDate.ValueChanged += new System.EventHandler(this.DtpFromDate_ValueChanged);
            // 
            // lblNoofMonths
            // 
            this.lblNoofMonths.AutoSize = true;
            this.lblNoofMonths.Location = new System.Drawing.Point(94, 31);
            this.lblNoofMonths.Name = "lblNoofMonths";
            this.lblNoofMonths.Size = new System.Drawing.Size(24, 13);
            this.lblNoofMonths.TabIndex = 2;
            this.lblNoofMonths.Text = "NIL";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(6, 280);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(49, 13);
            this.Label7.TabIndex = 191;
            this.Label7.Text = "Remarks";
            // 
            // EmployeeSettlementBindingSourcerpt
            // 
            this.EmployeeSettlementBindingSourcerpt.DataMember = "EmployeeSettlement";
            // 
            // RemarksTextBox1
            // 
            this.RemarksTextBox1.Location = new System.Drawing.Point(61, 277);
            this.RemarksTextBox1.MaxLength = 500;
            this.RemarksTextBox1.Multiline = true;
            this.RemarksTextBox1.Name = "RemarksTextBox1";
            this.RemarksTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.RemarksTextBox1.Size = new System.Drawing.Size(436, 70);
            this.RemarksTextBox1.TabIndex = 10;
            this.RemarksTextBox1.TextChanged += new System.EventHandler(this.RemarksTextBox1_TextChanged);
            // 
            // chkIsAddition
            // 
            this.chkIsAddition.AutoSize = true;
            this.chkIsAddition.Location = new System.Drawing.Point(369, 25);
            this.chkIsAddition.Name = "chkIsAddition";
            this.chkIsAddition.Size = new System.Drawing.Size(70, 17);
            this.chkIsAddition.TabIndex = 3;
            this.chkIsAddition.Text = "IsEarning";
            this.chkIsAddition.UseVisualStyleBackColor = true;
            // 
            // TxtParticulars
            // 
            this.TxtParticulars.Location = new System.Drawing.Point(13, 22);
            this.TxtParticulars.MaxLength = 50;
            this.TxtParticulars.Name = "TxtParticulars";
            this.TxtParticulars.Size = new System.Drawing.Size(160, 20);
            this.TxtParticulars.TabIndex = 1;
            // 
            // BtnBottomCancel
            // 
            this.BtnBottomCancel.Location = new System.Drawing.Point(462, 516);
            this.BtnBottomCancel.Name = "BtnBottomCancel";
            this.BtnBottomCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnBottomCancel.TabIndex = 4;
            this.BtnBottomCancel.Text = "&Cancel";
            this.BtnBottomCancel.UseVisualStyleBackColor = true;
            this.BtnBottomCancel.Click += new System.EventHandler(this.BtnBottomCancel_Click);
            // 
            // AddBtn
            // 
            this.AddBtn.Location = new System.Drawing.Point(454, 20);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(43, 24);
            this.AddBtn.TabIndex = 4;
            this.AddBtn.Text = "Add";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // CboEmployee
            // 
            this.CboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.CboEmployee.DropDownHeight = 134;
            this.CboEmployee.FormattingEnabled = true;
            this.CboEmployee.IntegralHeight = false;
            this.CboEmployee.Location = new System.Drawing.Point(105, 13);
            this.CboEmployee.Name = "CboEmployee";
            this.CboEmployee.Size = new System.Drawing.Size(346, 21);
            this.CboEmployee.TabIndex = 1;
            this.CboEmployee.SelectedIndexChanged += new System.EventHandler(this.CboEmployee_SelectedIndexChanged);
            this.CboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboEmployee_KeyPress);
            // 
            // GrpEmployeeSettlement
            // 
            this.GrpEmployeeSettlement.Controls.Add(this.SettlementTab);
            this.GrpEmployeeSettlement.Controls.Add(this.LblEmployee);
            this.GrpEmployeeSettlement.Controls.Add(this.CboEmployee);
            this.GrpEmployeeSettlement.Location = new System.Drawing.Point(7, 56);
            this.GrpEmployeeSettlement.Name = "GrpEmployeeSettlement";
            this.GrpEmployeeSettlement.Size = new System.Drawing.Size(530, 454);
            this.GrpEmployeeSettlement.TabIndex = 1;
            this.GrpEmployeeSettlement.TabStop = false;
            // 
            // SettlementTab
            // 
            this.SettlementTab.Controls.Add(this.SettlementTabEmpInfo);
            this.SettlementTab.Controls.Add(this.SettlementTabParticulars);
            this.SettlementTab.Controls.Add(this.SettlementTabHistory);
            this.SettlementTab.Controls.Add(this.GratuityTab);
            this.SettlementTab.Location = new System.Drawing.Point(8, 45);
            this.SettlementTab.Name = "SettlementTab";
            this.SettlementTab.SelectedIndex = 0;
            this.SettlementTab.Size = new System.Drawing.Size(515, 403);
            this.SettlementTab.TabIndex = 2;
            // 
            // SettlementTabEmpInfo
            // 
            this.SettlementTabEmpInfo.Controls.Add(this.DocumentReturnCheckBox);
            this.SettlementTabEmpInfo.Controls.Add(this.groupBox2);
            this.SettlementTabEmpInfo.Controls.Add(this.groupBox1);
            this.SettlementTabEmpInfo.Controls.Add(this.DtpFromDate);
            this.SettlementTabEmpInfo.Controls.Add(this.lblFromDate);
            this.SettlementTabEmpInfo.Controls.Add(Label1);
            this.SettlementTabEmpInfo.Controls.Add(DateLabel);
            this.SettlementTabEmpInfo.Controls.Add(this.BtnProcess);
            this.SettlementTabEmpInfo.Controls.Add(this.lblToDate);
            this.SettlementTabEmpInfo.Controls.Add(this.LblType);
            this.SettlementTabEmpInfo.Controls.Add(this.DtpToDate);
            this.SettlementTabEmpInfo.Controls.Add(this.DateDateTimePicker);
            this.SettlementTabEmpInfo.Controls.Add(this.CboType);
            this.SettlementTabEmpInfo.Controls.Add(this.Label3);
            this.SettlementTabEmpInfo.Controls.Add(this.lblSettlementPolicy);
            this.SettlementTabEmpInfo.Controls.Add(this.shapeContainer1);
            this.SettlementTabEmpInfo.Location = new System.Drawing.Point(4, 22);
            this.SettlementTabEmpInfo.Name = "SettlementTabEmpInfo";
            this.SettlementTabEmpInfo.Padding = new System.Windows.Forms.Padding(3);
            this.SettlementTabEmpInfo.Size = new System.Drawing.Size(507, 377);
            this.SettlementTabEmpInfo.TabIndex = 0;
            this.SettlementTabEmpInfo.Text = "Employee Info";
            this.SettlementTabEmpInfo.UseVisualStyleBackColor = true;
            // 
            // DocumentReturnCheckBox
            // 
            this.DocumentReturnCheckBox.Location = new System.Drawing.Point(9, 350);
            this.DocumentReturnCheckBox.Name = "DocumentReturnCheckBox";
            this.DocumentReturnCheckBox.Size = new System.Drawing.Size(112, 18);
            this.DocumentReturnCheckBox.TabIndex = 0;
            this.DocumentReturnCheckBox.Text = "Document Return";
            this.DocumentReturnCheckBox.UseVisualStyleBackColor = true;
            this.DocumentReturnCheckBox.CheckedChanged += new System.EventHandler(this.DocumentReturnCheckBox_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtNofDaysExperience);
            this.groupBox2.Controls.Add(this.Label13);
            this.groupBox2.Controls.Add(this.lblNoofMonths);
            this.groupBox2.Controls.Add(this.Label8);
            this.groupBox2.Controls.Add(label16);
            this.groupBox2.Controls.Add(this.txtExcludeHolidays);
            this.groupBox2.Controls.Add(this.txtExcludeLeaveDays);
            this.groupBox2.Controls.Add(this.chkExcludeLeaveDays);
            this.groupBox2.Controls.Add(this.txtEligibleDays);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.chkAbsent);
            this.groupBox2.Controls.Add(this.txtAbsentDays);
            this.groupBox2.Controls.Add(this.txtLeavePayDays);
            this.groupBox2.Controls.Add(this.Label9);
            this.groupBox2.Controls.Add(this.chkExcludeHolidays);
            this.groupBox2.Controls.Add(this.Label10);
            this.groupBox2.Controls.Add(this.lblEligibleLeavePayDays);
            this.groupBox2.Controls.Add(this.lblTakenLeavePayDays);
            this.groupBox2.Controls.Add(this.Label11);
            this.groupBox2.Controls.Add(Label12);
            this.groupBox2.Controls.Add(Label4);
            this.groupBox2.Controls.Add(this.shapeContainer2);
            this.groupBox2.Location = new System.Drawing.Point(9, 155);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(489, 185);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            // 
            // txtExcludeHolidays
            // 
            this.txtExcludeHolidays.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtExcludeHolidays.Location = new System.Drawing.Point(142, 103);
            this.txtExcludeHolidays.MaxLength = 9;
            this.txtExcludeHolidays.Name = "txtExcludeHolidays";
            this.txtExcludeHolidays.Size = new System.Drawing.Size(65, 13);
            this.txtExcludeHolidays.TabIndex = 23;
            this.txtExcludeHolidays.Text = "NIL";
            // 
            // txtExcludeLeaveDays
            // 
            this.txtExcludeLeaveDays.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtExcludeLeaveDays.Location = new System.Drawing.Point(142, 76);
            this.txtExcludeLeaveDays.MaxLength = 9;
            this.txtExcludeLeaveDays.Name = "txtExcludeLeaveDays";
            this.txtExcludeLeaveDays.Size = new System.Drawing.Size(65, 13);
            this.txtExcludeLeaveDays.TabIndex = 22;
            this.txtExcludeLeaveDays.Text = "NIL";
            // 
            // chkExcludeLeaveDays
            // 
            this.chkExcludeLeaveDays.AutoSize = true;
            this.chkExcludeLeaveDays.Location = new System.Drawing.Point(19, 75);
            this.chkExcludeLeaveDays.Name = "chkExcludeLeaveDays";
            this.chkExcludeLeaveDays.Size = new System.Drawing.Size(124, 17);
            this.chkExcludeLeaveDays.TabIndex = 18;
            this.chkExcludeLeaveDays.Text = "Exclude Leave Days";
            this.chkExcludeLeaveDays.UseVisualStyleBackColor = true;
            this.chkExcludeLeaveDays.CheckedChanged += new System.EventHandler(this.chkExcludeLeaveDays_CheckedChanged);
            // 
            // txtEligibleDays
            // 
            this.txtEligibleDays.Location = new System.Drawing.Point(97, 124);
            this.txtEligibleDays.MaxLength = 9;
            this.txtEligibleDays.Name = "txtEligibleDays";
            this.txtEligibleDays.Size = new System.Drawing.Size(106, 20);
            this.txtEligibleDays.TabIndex = 21;
            this.txtEligibleDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Eligible Days";
            // 
            // chkExcludeHolidays
            // 
            this.chkExcludeHolidays.AutoSize = true;
            this.chkExcludeHolidays.Location = new System.Drawing.Point(19, 102);
            this.chkExcludeHolidays.Name = "chkExcludeHolidays";
            this.chkExcludeHolidays.Size = new System.Drawing.Size(107, 17);
            this.chkExcludeHolidays.TabIndex = 19;
            this.chkExcludeHolidays.Text = "Exclude Holidays";
            this.chkExcludeHolidays.UseVisualStyleBackColor = true;
            this.chkExcludeHolidays.CheckedChanged += new System.EventHandler(this.chkExcludeHolidays_CheckedChanged);
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape4,
            this.lineShape3,
            this.lineShape2});
            this.shapeContainer2.Size = new System.Drawing.Size(483, 166);
            this.shapeContainer2.TabIndex = 26;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape4
            // 
            this.lineShape4.BorderColor = System.Drawing.Color.LightGray;
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 24;
            this.lineShape4.X2 = 477;
            this.lineShape4.Y1 = 1;
            this.lineShape4.Y2 = 1;
            // 
            // lineShape3
            // 
            this.lineShape3.BorderColor = System.Drawing.Color.LightGray;
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 279;
            this.lineShape3.X2 = 477;
            this.lineShape3.Y1 = 45;
            this.lineShape3.Y2 = 45;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.Color.LightGray;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 48;
            this.lineShape2.X2 = 222;
            this.lineShape2.Y1 = 45;
            this.lineShape2.Y2 = 45;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(lblAccount);
            this.groupBox1.Controls.Add(this.cboAccount);
            this.groupBox1.Controls.Add(this.cboTransactionType);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.dtpChequeDate);
            this.groupBox1.Controls.Add(lblCheqNo);
            this.groupBox1.Controls.Add(lblCheqDate);
            this.groupBox1.Controls.Add(this.txtChequeNo);
            this.groupBox1.Location = new System.Drawing.Point(249, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(249, 118);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // cboTransactionType
            // 
            this.cboTransactionType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTransactionType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTransactionType.BackColor = System.Drawing.SystemColors.Info;
            this.cboTransactionType.DropDownHeight = 134;
            this.cboTransactionType.FormattingEnabled = true;
            this.cboTransactionType.IntegralHeight = false;
            this.cboTransactionType.Location = new System.Drawing.Point(77, 11);
            this.cboTransactionType.Name = "cboTransactionType";
            this.cboTransactionType.Size = new System.Drawing.Size(165, 21);
            this.cboTransactionType.TabIndex = 1;
            this.cboTransactionType.SelectedIndexChanged += new System.EventHandler(this.cboTransactionType_SelectedIndexChanged);
            this.cboTransactionType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboTransactionType_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Trans: Type";
            // 
            // dtpChequeDate
            // 
            this.dtpChequeDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpChequeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpChequeDate.Location = new System.Drawing.Point(77, 64);
            this.dtpChequeDate.Name = "dtpChequeDate";
            this.dtpChequeDate.Size = new System.Drawing.Size(106, 20);
            this.dtpChequeDate.TabIndex = 5;
            this.dtpChequeDate.ValueChanged += new System.EventHandler(this.dtpChequeDate_ValueChanged);
            // 
            // txtChequeNo
            // 
            this.txtChequeNo.Location = new System.Drawing.Point(77, 38);
            this.txtChequeNo.MaxLength = 99;
            this.txtChequeNo.Name = "txtChequeNo";
            this.txtChequeNo.Size = new System.Drawing.Size(165, 20);
            this.txtChequeNo.TabIndex = 3;
            this.txtChequeNo.TextChanged += new System.EventHandler(this.txtChequeNo_TextChanged);
            // 
            // BtnProcess
            // 
            this.BtnProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProcess.Location = new System.Drawing.Point(418, 346);
            this.BtnProcess.Name = "BtnProcess";
            this.BtnProcess.Size = new System.Drawing.Size(80, 25);
            this.BtnProcess.TabIndex = 14;
            this.BtnProcess.Text = "Process";
            this.BtnProcess.UseVisualStyleBackColor = true;
            this.BtnProcess.Click += new System.EventHandler(this.BtnProcess_Click);
            // 
            // lblToDate
            // 
            this.lblToDate.AutoSize = true;
            this.lblToDate.Location = new System.Drawing.Point(13, 53);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(89, 13);
            this.lblToDate.TabIndex = 3;
            this.lblToDate.Text = "Absconding Date";
            // 
            // LblType
            // 
            this.LblType.AutoSize = true;
            this.LblType.Location = new System.Drawing.Point(13, 26);
            this.LblType.Name = "LblType";
            this.LblType.Size = new System.Drawing.Size(84, 13);
            this.LblType.TabIndex = 1;
            this.LblType.Text = "Settlement Type";
            // 
            // DtpToDate
            // 
            this.DtpToDate.CustomFormat = "dd-MMM-yyyy";
            this.DtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpToDate.Location = new System.Drawing.Point(106, 50);
            this.DtpToDate.Name = "DtpToDate";
            this.DtpToDate.Size = new System.Drawing.Size(106, 20);
            this.DtpToDate.TabIndex = 4;
            this.DtpToDate.ValueChanged += new System.EventHandler(this.DtpToDate_ValueChanged);
            // 
            // DateDateTimePicker
            // 
            this.DateDateTimePicker.CustomFormat = "dd-MMM-yyyy";
            this.DateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateDateTimePicker.Location = new System.Drawing.Point(106, 76);
            this.DateDateTimePicker.Name = "DateDateTimePicker";
            this.DateDateTimePicker.Size = new System.Drawing.Size(106, 20);
            this.DateDateTimePicker.TabIndex = 6;
            this.DateDateTimePicker.Visible = false;
            this.DateDateTimePicker.ValueChanged += new System.EventHandler(this.DateDateTimePicker_ValueChanged);
            // 
            // CboType
            // 
            this.CboType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboType.BackColor = System.Drawing.SystemColors.Info;
            this.CboType.DropDownHeight = 134;
            this.CboType.FormattingEnabled = true;
            this.CboType.IntegralHeight = false;
            this.CboType.Location = new System.Drawing.Point(106, 23);
            this.CboType.Name = "CboType";
            this.CboType.Size = new System.Drawing.Size(106, 21);
            this.CboType.TabIndex = 2;
            this.CboType.SelectedIndexChanged += new System.EventHandler(this.CboType_SelectedIndexChanged);
            this.CboType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboType_KeyPress);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(501, 371);
            this.shapeContainer1.TabIndex = 212;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 65;
            this.lineShape1.X2 = 495;
            this.lineShape1.Y1 = 8;
            this.lineShape1.Y2 = 8;
            // 
            // SettlementTabParticulars
            // 
            this.SettlementTabParticulars.Controls.Add(this.EmployeeSettlementDetailDataGridView);
            this.SettlementTabParticulars.Controls.Add(this.RemarksTextBox1);
            this.SettlementTabParticulars.Controls.Add(this.Label7);
            this.SettlementTabParticulars.Controls.Add(LblDeduction);
            this.SettlementTabParticulars.Controls.Add(LblEarning);
            this.SettlementTabParticulars.Controls.Add(Label6);
            this.SettlementTabParticulars.Controls.Add(this.LblCurrency);
            this.SettlementTabParticulars.Controls.Add(Label5);
            this.SettlementTabParticulars.Controls.Add(this.AddBtn);
            this.SettlementTabParticulars.Controls.Add(NetAmountLabel);
            this.SettlementTabParticulars.Controls.Add(this.txtAmount);
            this.SettlementTabParticulars.Controls.Add(this.NetAmountTextBox);
            this.SettlementTabParticulars.Controls.Add(this.TxtParticulars);
            this.SettlementTabParticulars.Controls.Add(this.chkIsAddition);
            this.SettlementTabParticulars.Controls.Add(this.TxtTotalDeduction);
            this.SettlementTabParticulars.Controls.Add(this.TxtTotalEarning);
            this.SettlementTabParticulars.Location = new System.Drawing.Point(4, 22);
            this.SettlementTabParticulars.Name = "SettlementTabParticulars";
            this.SettlementTabParticulars.Padding = new System.Windows.Forms.Padding(3);
            this.SettlementTabParticulars.Size = new System.Drawing.Size(507, 377);
            this.SettlementTabParticulars.TabIndex = 1;
            this.SettlementTabParticulars.Text = "Particulars";
            this.SettlementTabParticulars.UseVisualStyleBackColor = true;
            // 
            // EmployeeSettlementDetailDataGridView
            // 
            this.EmployeeSettlementDetailDataGridView.AllowUserToAddRows = false;
            this.EmployeeSettlementDetailDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.EmployeeSettlementDetailDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EmployeeSettlementDetailDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EmployeeSettlementDetailID,
            this.Particulars,
            this.Credit,
            this.Debit,
            this.RepaymentID,
            this.LoanID,
            this.IsAddition,
            this.Flag,
            this.SalaryPaymentID});
            this.EmployeeSettlementDetailDataGridView.Location = new System.Drawing.Point(9, 50);
            this.EmployeeSettlementDetailDataGridView.MultiSelect = false;
            this.EmployeeSettlementDetailDataGridView.Name = "EmployeeSettlementDetailDataGridView";
            this.EmployeeSettlementDetailDataGridView.RowHeadersWidth = 25;
            this.EmployeeSettlementDetailDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.EmployeeSettlementDetailDataGridView.Size = new System.Drawing.Size(488, 169);
            this.EmployeeSettlementDetailDataGridView.TabIndex = 5;
            this.EmployeeSettlementDetailDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.EmployeeSettlementDetailDataGridView_CellValueChanged);
            this.EmployeeSettlementDetailDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.EmployeeSettlementDetailDataGridView_CellBeginEdit);
            this.EmployeeSettlementDetailDataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.EmployeeSettlementDetailDataGridView_EditingControlShowing);
            this.EmployeeSettlementDetailDataGridView.CurrentCellDirtyStateChanged += new System.EventHandler(this.EmployeeSettlementDetailDataGridView_CurrentCellDirtyStateChanged);
            this.EmployeeSettlementDetailDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.EmployeeSettlementDetailDataGridView_DataError);
            this.EmployeeSettlementDetailDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EmployeeSettlementDetailDataGridView_KeyDown);
            this.EmployeeSettlementDetailDataGridView.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.EmployeeSettlementDetailDataGridView_RowsRemoved);
            // 
            // EmployeeSettlementDetailID
            // 
            this.EmployeeSettlementDetailID.HeaderText = "EmployeeSettlementDetailID";
            this.EmployeeSettlementDetailID.Name = "EmployeeSettlementDetailID";
            this.EmployeeSettlementDetailID.Visible = false;
            // 
            // Particulars
            // 
            this.Particulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Particulars.HeaderText = "Particulars";
            this.Particulars.Name = "Particulars";
            this.Particulars.ReadOnly = true;
            // 
            // Credit
            // 
            this.Credit.HeaderText = "Credit";
            this.Credit.MaxInputLength = 13;
            this.Credit.Name = "Credit";
            // 
            // Debit
            // 
            this.Debit.HeaderText = "Debit";
            this.Debit.MaxInputLength = 13;
            this.Debit.Name = "Debit";
            // 
            // RepaymentID
            // 
            this.RepaymentID.HeaderText = "RepaymentID";
            this.RepaymentID.Name = "RepaymentID";
            this.RepaymentID.Visible = false;
            // 
            // LoanID
            // 
            this.LoanID.HeaderText = "LoanID";
            this.LoanID.Name = "LoanID";
            this.LoanID.Visible = false;
            // 
            // IsAddition
            // 
            this.IsAddition.HeaderText = "IsAddition";
            this.IsAddition.Name = "IsAddition";
            this.IsAddition.Visible = false;
            // 
            // Flag
            // 
            this.Flag.HeaderText = "Flag";
            this.Flag.Name = "Flag";
            this.Flag.Visible = false;
            // 
            // SalaryPaymentID
            // 
            this.SalaryPaymentID.HeaderText = "SalaryPaymentID";
            this.SalaryPaymentID.Name = "SalaryPaymentID";
            this.SalaryPaymentID.Visible = false;
            // 
            // LblCurrency
            // 
            this.LblCurrency.AutoSize = true;
            this.LblCurrency.Location = new System.Drawing.Point(350, 253);
            this.LblCurrency.Name = "LblCurrency";
            this.LblCurrency.Size = new System.Drawing.Size(0, 13);
            this.LblCurrency.TabIndex = 9;
            // 
            // NetAmountTextBox
            // 
            this.NetAmountTextBox.Enabled = false;
            this.NetAmountTextBox.Location = new System.Drawing.Point(350, 251);
            this.NetAmountTextBox.Name = "NetAmountTextBox";
            this.NetAmountTextBox.Size = new System.Drawing.Size(147, 20);
            this.NetAmountTextBox.TabIndex = 22;
            this.NetAmountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtTotalDeduction
            // 
            this.TxtTotalDeduction.Enabled = false;
            this.TxtTotalDeduction.Location = new System.Drawing.Point(350, 225);
            this.TxtTotalDeduction.Name = "TxtTotalDeduction";
            this.TxtTotalDeduction.Size = new System.Drawing.Size(147, 20);
            this.TxtTotalDeduction.TabIndex = 7;
            this.TxtTotalDeduction.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtTotalEarning
            // 
            this.TxtTotalEarning.Enabled = false;
            this.TxtTotalEarning.Location = new System.Drawing.Point(197, 225);
            this.TxtTotalEarning.Name = "TxtTotalEarning";
            this.TxtTotalEarning.Size = new System.Drawing.Size(147, 20);
            this.TxtTotalEarning.TabIndex = 6;
            this.TxtTotalEarning.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SettlementTabHistory
            // 
            this.SettlementTabHistory.Controls.Add(this.DtGrdEmpHistory);
            this.SettlementTabHistory.Location = new System.Drawing.Point(4, 22);
            this.SettlementTabHistory.Name = "SettlementTabHistory";
            this.SettlementTabHistory.Padding = new System.Windows.Forms.Padding(3);
            this.SettlementTabHistory.Size = new System.Drawing.Size(507, 377);
            this.SettlementTabHistory.TabIndex = 2;
            this.SettlementTabHistory.Text = "History";
            this.SettlementTabHistory.UseVisualStyleBackColor = true;
            // 
            // DtGrdEmpHistory
            // 
            this.DtGrdEmpHistory.AllowUserToAddRows = false;
            this.DtGrdEmpHistory.BackgroundColor = System.Drawing.Color.White;
            this.DtGrdEmpHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtGrdEmpHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColDescription,
            this.ColFromDate,
            this.ColToDate,
            this.ColAmount});
            this.DtGrdEmpHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DtGrdEmpHistory.Location = new System.Drawing.Point(3, 3);
            this.DtGrdEmpHistory.Name = "DtGrdEmpHistory";
            this.DtGrdEmpHistory.ReadOnly = true;
            this.DtGrdEmpHistory.RowHeadersVisible = false;
            this.DtGrdEmpHistory.Size = new System.Drawing.Size(501, 371);
            this.DtGrdEmpHistory.TabIndex = 24;
            // 
            // ColDescription
            // 
            this.ColDescription.HeaderText = "Description";
            this.ColDescription.Name = "ColDescription";
            this.ColDescription.ReadOnly = true;
            this.ColDescription.Width = 175;
            // 
            // ColFromDate
            // 
            this.ColFromDate.HeaderText = "From Date";
            this.ColFromDate.Name = "ColFromDate";
            this.ColFromDate.ReadOnly = true;
            this.ColFromDate.Width = 90;
            // 
            // ColToDate
            // 
            this.ColToDate.HeaderText = "To Date";
            this.ColToDate.Name = "ColToDate";
            this.ColToDate.ReadOnly = true;
            this.ColToDate.Width = 90;
            // 
            // ColAmount
            // 
            this.ColAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColAmount.HeaderText = "Amount";
            this.ColAmount.Name = "ColAmount";
            this.ColAmount.ReadOnly = true;
            // 
            // GratuityTab
            // 
            this.GratuityTab.Controls.Add(this.DtGrdGratuity);
            this.GratuityTab.Location = new System.Drawing.Point(4, 22);
            this.GratuityTab.Name = "GratuityTab";
            this.GratuityTab.Padding = new System.Windows.Forms.Padding(3);
            this.GratuityTab.Size = new System.Drawing.Size(507, 377);
            this.GratuityTab.TabIndex = 3;
            this.GratuityTab.Text = "Gratuity";
            this.GratuityTab.UseVisualStyleBackColor = true;
            // 
            // DtGrdGratuity
            // 
            this.DtGrdGratuity.AllowUserToAddRows = false;
            this.DtGrdGratuity.BackgroundColor = System.Drawing.Color.White;
            this.DtGrdGratuity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtGrdGratuity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColYears,
            this.ColCalDays,
            this.ColGRTYDays,
            this.ColGratuityAmt});
            this.DtGrdGratuity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DtGrdGratuity.Location = new System.Drawing.Point(3, 3);
            this.DtGrdGratuity.Name = "DtGrdGratuity";
            this.DtGrdGratuity.ReadOnly = true;
            this.DtGrdGratuity.RowHeadersVisible = false;
            this.DtGrdGratuity.Size = new System.Drawing.Size(501, 371);
            this.DtGrdGratuity.TabIndex = 25;
            // 
            // ColYears
            // 
            this.ColYears.HeaderText = "Years";
            this.ColYears.Name = "ColYears";
            this.ColYears.ReadOnly = true;
            this.ColYears.Width = 250;
            // 
            // ColCalDays
            // 
            this.ColCalDays.HeaderText = "Cal Days";
            this.ColCalDays.Name = "ColCalDays";
            this.ColCalDays.ReadOnly = true;
            this.ColCalDays.Width = 80;
            // 
            // ColGRTYDays
            // 
            this.ColGRTYDays.HeaderText = "Gratuity Days";
            this.ColGRTYDays.Name = "ColGRTYDays";
            this.ColGRTYDays.ReadOnly = true;
            this.ColGRTYDays.Width = 80;
            // 
            // ColGratuityAmt
            // 
            this.ColGratuityAmt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColGratuityAmt.HeaderText = "Gratuity Amt";
            this.ColGratuityAmt.Name = "ColGratuityAmt";
            this.ColGratuityAmt.ReadOnly = true;
            // 
            // LblEmployee
            // 
            this.LblEmployee.AutoSize = true;
            this.LblEmployee.Location = new System.Drawing.Point(26, 16);
            this.LblEmployee.Name = "LblEmployee";
            this.LblEmployee.Size = new System.Drawing.Size(53, 13);
            this.LblEmployee.TabIndex = 0;
            this.LblEmployee.Text = "Employee";
            // 
            // TextBoxNumeric
            // 
            this.TextBoxNumeric.Location = new System.Drawing.Point(1000, 272);
            this.TextBoxNumeric.Name = "TextBoxNumeric";
            this.TextBoxNumeric.Size = new System.Drawing.Size(110, 20);
            this.TextBoxNumeric.TabIndex = 190;
            this.TextBoxNumeric.Visible = false;
            // 
            // EmployeeSettlementDetailBindingSource
            // 
            this.EmployeeSettlementDetailBindingSource.DataMember = "EmployeeSettlementDetail";
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(7, 516);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 2;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // lblPayBack
            // 
            this.lblPayBack.AutoSize = true;
            this.lblPayBack.Location = new System.Drawing.Point(118, 531);
            this.lblPayBack.Name = "lblPayBack";
            this.lblPayBack.Size = new System.Drawing.Size(0, 13);
            this.lblPayBack.TabIndex = 173;
            this.lblPayBack.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "EmployeeSettlementDetailID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Particulars";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Credit";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Debit";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "RepaymentID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "LoanID";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "IsAddition";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Flag";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "SalaryPaymentID";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(544, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 191;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(311, 23);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyBooksERP.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblAccount
            // 
            lblAccount.AutoSize = true;
            lblAccount.Location = new System.Drawing.Point(5, 92);
            lblAccount.Name = "lblAccount";
            lblAccount.Size = new System.Drawing.Size(47, 13);
            lblAccount.TabIndex = 1045;
            lblAccount.Text = "Account";
            // 
            // cboAccount
            // 
            this.cboAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAccount.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cboAccount.DropDownHeight = 134;
            this.cboAccount.FormattingEnabled = true;
            this.cboAccount.IntegralHeight = false;
            this.cboAccount.Location = new System.Drawing.Point(77, 91);
            this.cboAccount.Name = "cboAccount";
            this.cboAccount.Size = new System.Drawing.Size(159, 21);
            this.cboAccount.TabIndex = 1044;
            // 
            // FrmSettlementEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 564);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.EmployeeSettlementBindingNavigator);
            this.Controls.Add(this.OKSaveButton);
            this.Controls.Add(this.StatusStripEmployeeSettlement);
            this.Controls.Add(this.BtnBottomCancel);
            this.Controls.Add(this.GrpEmployeeSettlement);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.lblPayBack);
            this.Controls.Add(this.TextBoxNumeric);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSettlementEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settlement Process";
            this.Load += new System.EventHandler(this.FrmSettlementEntry_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSettlementEntry_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmSettlementEntry_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementBindingNavigator)).EndInit();
            this.EmployeeSettlementBindingNavigator.ResumeLayout(false);
            this.EmployeeSettlementBindingNavigator.PerformLayout();
            this.StatusStripEmployeeSettlement.ResumeLayout(false);
            this.StatusStripEmployeeSettlement.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrEmployeeSettlement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementBindingSourcerpt)).EndInit();
            this.GrpEmployeeSettlement.ResumeLayout(false);
            this.GrpEmployeeSettlement.PerformLayout();
            this.SettlementTab.ResumeLayout(false);
            this.SettlementTabEmpInfo.ResumeLayout(false);
            this.SettlementTabEmpInfo.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.SettlementTabParticulars.ResumeLayout(false);
            this.SettlementTabParticulars.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementDetailDataGridView)).EndInit();
            this.SettlementTabHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DtGrdEmpHistory)).EndInit();
            this.GratuityTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DtGrdGratuity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox txtAbsentDays;
        internal System.Windows.Forms.CheckBox chkAbsent;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.TextBox txtNofDaysExperience;
        internal System.Windows.Forms.Label lblTakenLeavePayDays;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton EmployeeSettlementBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.Label lblEligibleLeavePayDays;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.BindingNavigator EmployeeSettlementBindingNavigator;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BtnCancel;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.ToolStripStatusLabel LblEmployeeSettlement;
        internal System.Windows.Forms.StatusStrip StatusStripEmployeeSettlement;
        internal System.Windows.Forms.Button OKSaveButton;
        internal System.Windows.Forms.Timer TmrEmployeeSettlement;
        internal System.Windows.Forms.ErrorProvider ErrEmployeeSettlement;
        internal System.Windows.Forms.Button BtnBottomCancel;
        internal System.Windows.Forms.GroupBox GrpEmployeeSettlement;
        internal System.Windows.Forms.TextBox txtLeavePayDays;
        internal System.Windows.Forms.TextBox txtAmount;
        internal System.Windows.Forms.Label lblSettlementPolicy;
        internal System.Windows.Forms.Label lblFromDate;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.DateTimePicker DtpFromDate;
        internal System.Windows.Forms.Label lblNoofMonths;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.TextBox RemarksTextBox1;
        internal System.Windows.Forms.CheckBox chkIsAddition;
        internal System.Windows.Forms.TextBox TxtParticulars;
        internal System.Windows.Forms.Button AddBtn;
        internal System.Windows.Forms.ComboBox CboEmployee;
        internal System.Windows.Forms.ComboBox CboType;
        internal System.Windows.Forms.DateTimePicker DateDateTimePicker;
        internal System.Windows.Forms.DateTimePicker DtpToDate;
        internal System.Windows.Forms.Label LblType;
        internal System.Windows.Forms.Label LblEmployee;
        internal System.Windows.Forms.Label lblToDate;
        internal System.Windows.Forms.Label LblCurrency;
        internal System.Windows.Forms.CheckBox DocumentReturnCheckBox;
        internal System.Windows.Forms.Button BtnProcess;
        internal System.Windows.Forms.TextBox NetAmountTextBox;
        internal System.Windows.Forms.TextBox TxtTotalDeduction;
        internal System.Windows.Forms.TextBox TxtTotalEarning;
        internal System.Windows.Forms.TextBox TextBoxNumeric;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Label lblPayBack;
        internal System.Windows.Forms.BindingSource EmployeeSettlementBindingSourcerpt;
        internal System.Windows.Forms.BindingSource EmployeeSettlementDetailBindingSource;
        private System.Windows.Forms.DataGridView EmployeeSettlementDetailDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.TabControl SettlementTab;
        private System.Windows.Forms.TabPage SettlementTabEmpInfo;
        private System.Windows.Forms.TabPage SettlementTabParticulars;
        private System.Windows.Forms.TabPage SettlementTabHistory;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        internal System.Windows.Forms.ComboBox cboTransactionType;
        private System.Windows.Forms.Label label14;
        internal System.Windows.Forms.DateTimePicker dtpChequeDate;
        private System.Windows.Forms.TextBox txtChequeNo;
        private System.Windows.Forms.DataGridView DtGrdEmpHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFromDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColToDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAmount;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeSettlementDetailID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Particulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn Credit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Debit;
        private System.Windows.Forms.DataGridViewTextBoxColumn RepaymentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoanID;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsAddition;
        private System.Windows.Forms.DataGridViewTextBoxColumn Flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalaryPaymentID;
        internal System.Windows.Forms.CheckBox chkExcludeHolidays;
        internal System.Windows.Forms.CheckBox chkExcludeLeaveDays;
        internal System.Windows.Forms.TextBox txtEligibleDays;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.TextBox txtExcludeHolidays;
        internal System.Windows.Forms.TextBox txtExcludeLeaveDays;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private System.Windows.Forms.TabPage GratuityTab;
        private System.Windows.Forms.DataGridView DtGrdGratuity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColYears;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCalDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColGRTYDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColGratuityAmt;
        internal System.Windows.Forms.ToolStripButton BtnPrintRpt;
        internal System.Windows.Forms.ComboBox cboAccount;
    }
}