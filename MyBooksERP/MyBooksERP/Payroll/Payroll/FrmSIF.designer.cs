﻿namespace MyBooksERP
{
    partial class FrmSIF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSIF));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.rtbVisaCompany = new System.Windows.Forms.RadioButton();
            this.rtbWorkingCompany = new System.Windows.Forms.RadioButton();
            this.cboBank = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DtpCurrentMonth = new System.Windows.Forms.DateTimePicker();
            this.saveFileDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboCompany);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.rtbVisaCompany);
            this.groupBox1.Controls.Add(this.rtbWorkingCompany);
            this.groupBox1.Controls.Add(this.cboBank);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.DtpCurrentMonth);
            this.groupBox1.Location = new System.Drawing.Point(-3, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(397, 151);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Salary Information File";
            // 
            // cboCompany
            // 
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.Location = new System.Drawing.Point(116, 55);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(257, 21);
            this.cboCompany.TabIndex = 14;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "WPS Based On";
            // 
            // rtbVisaCompany
            // 
            this.rtbVisaCompany.AutoSize = true;
            this.rtbVisaCompany.Location = new System.Drawing.Point(282, 22);
            this.rtbVisaCompany.Name = "rtbVisaCompany";
            this.rtbVisaCompany.Size = new System.Drawing.Size(92, 17);
            this.rtbVisaCompany.TabIndex = 12;
            this.rtbVisaCompany.TabStop = true;
            this.rtbVisaCompany.Text = "Visa Company";
            this.rtbVisaCompany.UseVisualStyleBackColor = true;
            this.rtbVisaCompany.CheckedChanged += new System.EventHandler(this.rtbVisaCompany_CheckedChanged);
            // 
            // rtbWorkingCompany
            // 
            this.rtbWorkingCompany.AutoSize = true;
            this.rtbWorkingCompany.Location = new System.Drawing.Point(116, 22);
            this.rtbWorkingCompany.Name = "rtbWorkingCompany";
            this.rtbWorkingCompany.Size = new System.Drawing.Size(112, 17);
            this.rtbWorkingCompany.TabIndex = 11;
            this.rtbWorkingCompany.TabStop = true;
            this.rtbWorkingCompany.Text = "Working Company";
            this.rtbWorkingCompany.UseVisualStyleBackColor = true;
            // 
            // cboBank
            // 
            this.cboBank.FormattingEnabled = true;
            this.cboBank.Location = new System.Drawing.Point(116, 82);
            this.cboBank.Name = "cboBank";
            this.cboBank.Size = new System.Drawing.Size(257, 21);
            this.cboBank.TabIndex = 10;
            this.cboBank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboBank_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Bank";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Company";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Salary Day";
            // 
            // DtpCurrentMonth
            // 
            this.DtpCurrentMonth.AllowDrop = true;
            this.DtpCurrentMonth.CustomFormat = "dd MMM yyyy";
            this.DtpCurrentMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpCurrentMonth.Location = new System.Drawing.Point(116, 115);
            this.DtpCurrentMonth.Name = "DtpCurrentMonth";
            this.DtpCurrentMonth.Size = new System.Drawing.Size(103, 20);
            this.DtpCurrentMonth.TabIndex = 4;
            this.DtpCurrentMonth.ValueChanged += new System.EventHandler(this.DtpCurrentMonth_ValueChanged);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(317, 168);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(77, 25);
            this.btnGenerate.TabIndex = 7;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // FrmSIF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 203);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSIF";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SIF";
            this.Load += new System.EventHandler(this.FrmSIF_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.DateTimePicker DtpCurrentMonth;
        private System.Windows.Forms.FolderBrowserDialog saveFileDialog1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboBank;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rtbVisaCompany;
        private System.Windows.Forms.RadioButton rtbWorkingCompany;
        private System.Windows.Forms.ComboBox cboCompany;
    }
}