﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MyBooksERP;


public partial class FrmSalayStructureList : Form
{
    private string ColumnName = "";
    private object  CurrentCellValue { get; set; }
    private List<TempSalaryMaster> SalaryDayList { get; set; }
    private List<clsDTOSalaryStructure> SalaryStructureMasterList { get; set; }
    private List<clsDTOSalaryStructureDetail> SalaryStructureDetails { get; set; }
    private List<AdditionDeductionColumn> AdditionDeductionColumns { get; set; }

    public FrmSalayStructureList()
    {
        InitializeComponent();
        this.AdditionDeductionColumns = new List<AdditionDeductionColumn>();

    }

    private void FillAllCompanies()
    {
        cboCompany.DisplayMember = "Name";
        cboCompany.ValueMember = "CompanyID";
        cboCompany.DataSource = new clsBLLSalaryStructure().FillCombos(new string[] { "CompanyID,CompanyName as Name", "CompanyMaster", "" });
    }
  
    private void FillAllEmployees()
    {
        cboEmployee.DisplayMember = "EmployeeFullName";
        cboEmployee.ValueMember = "EmployeeID";
        cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(),0, true,cboDepartment.SelectedValue.ToInt32());
       
    }

    private void FillAllDepartments()
    {
        cboDepartment.DisplayMember = "Department";
        cboDepartment.ValueMember = "DepartmentID";
        cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
    }

    private void GetDetails()
    {
        try
        {
            string AdditionDeductionIds;
            SalaryDayList = new List<TempSalaryMaster>();
            AdditionDeductionColumns = new List<AdditionDeductionColumn>();

            progressBarX1.Value = 0;
            chkSelectAll.Enabled = false;

            DataSet ds = clsBLLSalaryStructure.GetAllEmployeesSalaryStructure(cboCompany.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32());

            dgvSalaryDetails.Columns.Clear();

            if (ds.Tables.Count == 2)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    SalaryDayList.Add(new TempSalaryMaster()
                    {
                        EmployeeID = dr["EmployeeID"].ToInt32(),
                        PayCalculationTypeID = dr["PayCalculationTypeID"].ToInt32(),
                        PaymentClassificationID = dr["PaymentClassificationID"].ToInt32(),
                        SalaryDay = dr["SalaryDay"].ToInt32(),
                        CurrencyID = dr["CurrencyID"].ToInt32(),
                        SalaryStructureID = dr["SalaryStructureID"].ToInt32()

                    });
                }


                dgvSalaryDetails.DataSource = ds.Tables[1];


                if (ds.Tables[1].Rows.Count > 0)
                {
                    chkSelectAll.Enabled = true ;
                    //Creating the checkbox column
                    DataGridViewCheckBoxColumn column = new DataGridViewCheckBoxColumn()
                    {
                        HeaderText = "Select",
                        Name = "clmSelect"

                    };

                  
                    dgvSalaryDetails.Columns.Insert(0, column);

                    //Addition Deduction column count = Total No of columns - CheckBox column-EmployeeID Column - EmployeeName Column
                    //So Addition Deduction column count = Total No of columns - 3

                    foreach (DataGridViewColumn c in dgvSalaryDetails.Columns)
                    {
                        c.SortMode = DataGridViewColumnSortMode.NotSortable;
                        ColumnName = c.HeaderText;
                        if (c.Index > 2)
                        {
                            c.HeaderText = ColumnName.Split('@')[0];
                            AdditionDeductionIds = ColumnName.Split('@')[1];
                            c.Tag = AdditionDeductionIds.Split('^')[0];

                            this.AdditionDeductionColumns.Add(new AdditionDeductionColumn()
                            {
                                ColumnIndex = c.Index,
                                IsAddition = Convert.ToBoolean(AdditionDeductionIds.Split('^')[1].ToInt32())
                            });


                           
                        }
                    }
                }
                else
                    MessageBox.Show("No result found", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);    


                dgvSalaryDetails.Columns[1].Name = "clmEmployeeID";
                dgvSalaryDetails.Columns[2].Name = "clmEmployeeName";

                dgvSalaryDetails.Columns["clmEmployeeName"].ReadOnly = true;
                dgvSalaryDetails.Columns["clmSelect"].Width = 40;
                dgvSalaryDetails.Columns["clmSelect"].Resizable = DataGridViewTriState.False;

                dgvSalaryDetails.Columns[1].Visible = false;
                dgvSalaryDetails.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;



            }
            else
                MessageBox.Show("No result found", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);    


        }
        catch (Exception ex)
        {

        }
    }

    private void btnShow_Click(object sender, EventArgs e)
    {
        GetDetails();
    }

    private bool Validate()
    {
        MyBooksERP.clsBLLSalaryStructure objSalaryStructureBll = new MyBooksERP.clsBLLSalaryStructure();

        progressBarX1.Value = 0;

        bool BasicPayExists = false;
        foreach (DataGridViewColumn c in dgvSalaryDetails.Columns)
        {
            //Checking if basic pay column exists
            if (c.Tag.ToInt32() == 1)
            {
                c.Name = "clmBasicPay";
                BasicPayExists = true;
                break;
            }
        }
        foreach (DataGridViewRow dr in dgvSalaryDetails.Rows)
        {
            if (Convert.ToBoolean(dr.Cells["clmSelect"].Value))
            {
                if (objSalaryStructureBll.CheckIfEmployeeSalaryProcessed(dr.Cells["clmEmployeeID"].Value.ToInt32()))
                {
                    dgvSalaryDetails.CurrentCell = dr.Cells["clmEmployeeName"];
                    dgvSalaryDetails.CurrentCell.Selected = true;

                    MessageBox.Show("Sorry, cannot update this employee's 'Salary Structure'.  Salary Processed without release", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                if (BasicPayExists && dr.Cells["clmBasicPay"].Value.ToDecimal() == 0)
                {
                    dgvSalaryDetails.CurrentCell = dr.Cells["clmBasicPay"];
                    dgvSalaryDetails.CurrentCell.Selected = true;

                    MessageBox.Show("Please enter basic pay", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
        }
        return true;
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {
            MessageBox.Show("Failed to update salary structure", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
    }

    private void Save()
    {
        if (MessageBox.Show("Are you sure you want to update ?", ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo , MessageBoxIcon.Question) == DialogResult.Yes)
        {
            if (Validate())
            {
                if (ConstructSalaryDetails())
                {
                    clsBLLSalaryStructure objSalaryStructure = new clsBLLSalaryStructure();
                    progressBarX1.Minimum = 0;
                    progressBarX1.Maximum = SalaryStructureMasterList.Count;
                    progressBarX1.Step = 1;

                    for (int i = 0; i < SalaryStructureMasterList.Count; i++)
                    {

                        objSalaryStructure.DTOSalaryStructure = SalaryStructureMasterList[i];
                        objSalaryStructure.SalaryStructureSave(true);
                        objSalaryStructure.SalaryStructureDetailSave(true);
                        objSalaryStructure.SalaryStructureHistorySave();

                        progressBarX1.PerformStep();
                    }
                }

                MessageBox.Show("Updated successfully", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }

    private bool ConstructSalaryDetails()
    {
        int Iterator = 0, EmployeeId = 0;
        decimal Addition = 0, Deduction = 0;

        SalaryStructureMasterList = new List<clsDTOSalaryStructure>();
        SalaryStructureDetails = new List<clsDTOSalaryStructureDetail>();

        foreach (DataGridViewRow dr in dgvSalaryDetails.Rows)
        {
            if (Convert.ToBoolean(dr.Cells["clmSelect"].Value))
            {
                Iterator = 1;
                Addition = 0;
                Deduction = 0;

                EmployeeId = dr.Cells["clmEmployeeID"].Value.ToInt32();

                var Master = SalaryDayList.Where(t => t.EmployeeID == EmployeeId).Single();
                SalaryStructureDetails = new List<clsDTOSalaryStructureDetail>();

                foreach (DataGridViewColumn c in dgvSalaryDetails.Columns)
                {
                    //Leave Select Column(0) ,EmployeeID Column(1) And Employee Name Column(2)
                    if (c.Index > 2)
                    {
                        if (dr.Cells[c.Index].Value != null && dr.Cells[c.Index].Value != DBNull.Value)
                        {
                            SalaryStructureDetails.Add(new clsDTOSalaryStructureDetail()
                                {
                                    intAdditionDeductionID = c.Tag.ToInt32(),
                                    intAdditionDeductionPolicyID = null,
                                    intSerialNo = Iterator,
                                    decAmount = dr.Cells[c.Index].Value.ToDecimal(),
                                    intCategoryID = 1
                                });
                        }

                        var c1 = AdditionDeductionColumns.Where(t => t.ColumnIndex == c.Index);

                        var Result = AdditionDeductionColumns.Where(t => t.ColumnIndex == c.Index).Single();

                        if (Result.IsAddition)
                            Addition = Addition + dr.Cells[c.Index].Value.ToDecimal();
                        else
                            Deduction = Deduction + dr.Cells[c.Index].Value.ToDecimal();


                        Iterator = Iterator + 1;
                    }



                    if (c.Index == dgvSalaryDetails.Columns.Count - 1)
                    {
                        if (Addition - Deduction >= 0)
                        {
                            SalaryStructureMasterList.Add(new clsDTOSalaryStructure()
                                     {
                                         intEmployeeID = EmployeeId,
                                         intPayCalculationTypeID = Master.PayCalculationTypeID,
                                         intPaymentClassificationID = Master.PaymentClassificationID,
                                         strRemarks = string.Empty,
                                         intSalaryDay = Master.SalaryDay,
                                         intOTPolicyID = 0,
                                         intAbsentPolicyID = 0,
                                         intSetPolicyID = 0,
                                         intHolidayPolicyID = 0,
                                         intEncashPolicyID = 0,
                                         intCurrencyID = Master.CurrencyID,
                                         decNetAmount = Addition - Deduction,
                                         intSalaryStructureID = Master.SalaryStructureID,
                                         DTOSalaryStructureDetail = SalaryStructureDetails,
                                         DTORemunerationDetail = new List<clsDTORemunerationDetail>()
                                     });
                        }
                        else
                        {
                            MessageBox.Show("Net amount must be greater than zero", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                            dgvSalaryDetails.CurrentCell = dr.Cells["clmEmployeeName"];
                            dgvSalaryDetails.CurrentCell.Selected = true;
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
        this.Close();
    }

    public class TempSalaryMaster
    {
        public int EmployeeID { get; set; }

        public int SalaryDay { get; set; }

        public int PaymentClassificationID { get; set; }

        public int PayCalculationTypeID { get; set; }

        public int CurrencyID { get; set; }

        public int SalaryStructureID { get; set; }

    }

    public class AdditionDeductionColumn
    {
        public int ColumnIndex { get; set; }

        public bool IsAddition { get; set; }
    }

    private void FrmSalayStructureList_Load(object sender, EventArgs e)
    {
        Initialise();

    }

    private void Initialise()
    {
        FillAllCompanies();
        FillAllDepartments();
        chkSelectAll.Enabled = false;
    }

    private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }

    private void dgvSalaryDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
    {
        if (dgvSalaryDetails.CurrentCell.ColumnIndex >2)
        {
            TextBox txt = (TextBox)(e.Control);
            txt.KeyPress += new KeyPressEventHandler(txt_KeyPress);
        }

    }

    void txt_KeyPress(object sender, KeyPressEventArgs e)
    {
        //Copied from salary structure existing form
        string strInvalidChars;

        strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
        //if (ClsCommonSettings.IsAmountRoundByZero)
        //{
        //    strInvalidChars = strInvalidChars + ".";
        //}

        e.Handled = false;
        if (((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))) )
        {
            e.Handled = true;
        }
        else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
        {
            e.Handled = true;
        }
    }

    private void ComboKeyPress(object sender, KeyPressEventArgs e)
    {
        ((ComboBox)sender).DroppedDown = false;
    }

    private void cboCompany_SelectionChangeCommitted(object sender, EventArgs e)
    {
        FillAllEmployees(); 
    }

    private void dgvSalaryDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
    {
        if (e.ColumnIndex > 2)
        {
            if (Convert.ToBoolean(dgvSalaryDetails.Rows[e.RowIndex].Cells["clmSelect"].Value))
                CurrentCellValue = dgvSalaryDetails.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
            else
            {
                MessageBox.Show("Please select the employee", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

    }

    private void dgvSalaryDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
    {
        if (e.ColumnIndex > 2 && Convert.ToString(CurrentCellValue) != Convert.ToString(dgvSalaryDetails.Rows[e.RowIndex].Cells[e.ColumnIndex].Value))
            dgvSalaryDetails.Rows[e.RowIndex].Cells[e.ColumnIndex].Style = new DataGridViewCellStyle() { BackColor = Color.Yellow };
    }

    private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        foreach (DataGridViewRow dr in dgvSalaryDetails.Rows)
        {
            dr.Cells["clmSelect"].Value = chkSelectAll.Checked;
        }

    }
}

