﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
/* Design By        : Tijo 
 * Designed Date    : 26 Jun 2013
 * Developed By     : Tijo
 * Develop St.Date  : 26 Jun 2013
 * Develop End Date : 28 Jun 2013
 * Purpose          : Salary Procee and Release
*/
namespace MyBooksERP
{
    public partial class FrmSalaryProcessRelease : DevComponents.DotNetBar.Office2007Form
    {
        private bool MbFirst = false;
        private string NavClicked = "";
        private string RecordID1 = "0";
        private string RecordID2 = "0";
        private int CurrentPage = 1;
        private int TotalPage = 1;
        private DataSet dsTemp = new DataSet();
        private string MstrCommonMessage;
        
        public string strProcessDate;
        public string FillDate;
        public bool FlagSelect;
        public int PBankNameIDSalRel;
        public int PCompanyID;
        public int PProcessYear;

        public int PDepartmentID;
        public int PDesignationID;

        public int intSalaryProcessForm;

        clsConnection mObjCon = new clsConnection();
        clsBLLSalaryProcessAndRelease mobjclsBLLSalaryProcessAndRelease = new clsBLLSalaryProcessAndRelease();
        clsBLLSalaryProcess mobjProcess = new clsBLLSalaryProcess();
        private clsMessage ObjUserMessage = null;

        bool norecord;
        int pageRows = 25;
        bool FlgBlk = true;
        bool MChangeStatus; // Check state of the page
        bool MViewPermission;
        bool MAddPermission;
        bool MUpdatePermission;
        bool MDeletePermission;
        bool blnSalaryDayIsEditable = false;
        
        int CurrentMonth;
        int CurrentYear;
        int MCompanyID;
        int MEmployeeID;        
        int MPaymentClassificationID;
        int BranchIndicator;
        int TemplateID;
        int TransactionTypeIDRel;
        int MCurrencyID;
        int intCompanyID = ClsCommonSettings.LoginCompanyID;
        
        string MFromDate;
        string MToDate;
        string mdtFrom = "";
        string mDtTo = "";
        string mLOP = "";
        string mHLOP = "";
        string mLLOP = "";
        string mSType = "";
        string MstrMessageCaption = ClsCommonSettings.MessageCaption;
        string GetHostName = "";

        DateTime LoadDate;

        DataTable DtProcessEmployee = new DataTable();
        DataTable datEmployeeSalaryDetails = new DataTable();
        DataTable datTempSalary = new DataTable();
              
        TreeNode TvRoot;
        TreeNode TvChild;
        TreeNode TvChildChild;
        
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.SalaryRelease);
                return this.ObjUserMessage;
            }
        }

        public FrmSalaryProcessRelease()
        {
            InitializeComponent();
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        enum NavButton
        {
            First = 1,
            Next = 2,
            Previous = 3,
            Last = 4
        }

        private void FrmSalaryProcessRelease_Load(object sender, EventArgs e)
        {
            if (intSalaryProcessForm == 1)
            {
                expPnlSalaryProcess.Visible = true;
                expPnlSalaryRelease.Visible = false;
                trvSalaryProcessRelease.Visible = false;
                pnlMentioned.Visible = true;
                btnProcess.Text = "&Process";
                btnProcess.Image = Properties.Resources.Salary_processing;
                pnlLeft.Width = 250;
                //this.ToolStripDes.SetToolTip(this.Label16, "Bank");
            }            
            else if (intSalaryProcessForm == 2)
            {
                getProcessedDetails();
                expPnlSalaryProcess.Visible = false;
                expPnlSalaryRelease.Visible = true;
                trvSalaryProcessRelease.Visible = true;
                btnProcess.Text = "&Release";
                btnProcess.Image = Properties.Resources.Salary_release;
                btnProcess.Enabled = true;
                pnlMentioned.Visible = false;
                pnlLeft.Width = 150;

            }


            cboCountItem.Items.Add("All");
            cboCountItem.Items.Add("25");
            cboCountItem.Items.Add("50");
            cboCountItem.Items.Add("100");
            cboCountItem.Text = "25";
            SetPermissions();
            ClearControls();
            LoadCombos();

            if (intSalaryProcessForm == 1)
            {
                dgvSalaryDetails.DataSource = getNewTableForProcess();
                datTempSalary = (DataTable)dgvSalaryDetails.DataSource;
                GridColumnDisabled();
            }
            else
            {
                GridSetUp();
            }
          
            GetHostName = System.Net.Dns.GetHostName().Trim();
        }

        private void expPnlSalaryRelease_ExpandedChanged(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {
            if (expPnlSalaryRelease.Expanded)
            {
                btnProcess.Text = "&Release";
                btnProcess.Image = Properties.Resources.Salary_release;
            }
        }
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    if (intSalaryProcessForm == 1)
        //    {
        //        objDAL.SetArabicVersion((int)FormID.SalaryProcess, this);
        //    }
        //    else
        //    {
        //        objDAL.SetArabicVersion((int)FormID.SalaryRelease, this);
        //    }
        //}
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SetPermissions()
        {
            if (ClsCommonSettings.RoleID <= 3)
                MViewPermission = MAddPermission = MUpdatePermission = MDeletePermission = true;
            else
            {
                clsBLLPermissionSettings objPermission = new clsBLLPermissionSettings();
                int intMenuID = 0;
                if (intSalaryProcessForm == 1)
                    intMenuID = (int)eMenuID.SalaryProcess;
                else
                    intMenuID = (int)eMenuID.SalaryRelease;

                objPermission.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (int)eModuleID.Payroll, intMenuID,
                        out MViewPermission, out MAddPermission, out MUpdatePermission, out MDeletePermission);
            }
        }

        private void ClearControls()
        {
            if (!datEmployeeSalaryDetails.Columns.Contains("EmployeeID"))
                datEmployeeSalaryDetails.Columns.Add("EmployeeID");

            if (!datEmployeeSalaryDetails.Columns.Contains("CompanyID"))
                datEmployeeSalaryDetails.Columns.Add("CompanyID");

            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(12, 0); // Get Current Month & Year
            CurrentMonth = datTemp.Rows[0]["MonthNo"].ToInt32();
            CurrentYear = datTemp.Rows[0]["YearNo"].ToInt32();
            LoadDate = ("01/" + GetCurrentMonth(CurrentMonth) + "/" + CurrentYear + "").ToDateTime();

            dtpCurrentMonth.Value = LoadDate;
            dtpFromDate.Value = LoadDate;
            dtpToDate.Value = LoadDate;

            dtpFromDate.Enabled = false;
            dtpToDate.Enabled = false;

            MCompanyID = -1;
            MEmployeeID = -1;
            MFromDate = dtpFromDate.Text.Trim();
            MToDate = dtpToDate.Text.Trim();

            if (intSalaryProcessForm == 1)
                btnProcess.Enabled = false;
            else if (intSalaryProcessForm == 2)
                btnProcess.Enabled = true;

            chkAdvanceSearch.Checked = false;
            getAdvanceSearch();
            getSalaryDayIsEditable();
            btnPrint.Enabled = btnEmail.Enabled = MViewPermission;
        }

        private bool getSalaryDayIsEditable()
        {
            blnSalaryDayIsEditable = false;

            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos(new string[] { "UPPER(ConfigurationValue) AS ConfigurationValue", "" +
                "ConfigurationMaster", "ConfigurationItem = 'SalaryDayIsEditable'" });
            
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    if (datTemp.Rows[0][0].ToString() == "YES")
                        blnSalaryDayIsEditable = true;
                }
            }

            dtpCurrentMonth.Enabled = !blnSalaryDayIsEditable;
            dtpFromDate.Enabled = blnSalaryDayIsEditable;
            dtpToDate.Enabled = false;
            dtpToDate.Value = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, dtpFromDate.Value.Date));

            return blnSalaryDayIsEditable;
        }

        private void getAdvanceSearch()
        {
            lblDept.Visible = chkAdvanceSearch.Checked;
            cboDepartment.Visible = chkAdvanceSearch.Checked;
            lblDesg.Visible = chkAdvanceSearch.Checked;
            cboDesignation.Visible = chkAdvanceSearch.Checked;

            if (chkAdvanceSearch.Checked == false)
                btnShow.Location = new Point(165, cboDepartment.Location.Y);
            else
                btnShow.Location = new Point(165, (cboDesignation.Location.Y + 30));

            cboDepartment.SelectedIndex = -1;
            cboDesignation.SelectedIndex = -1;
        }

        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }

            return Months;
        }

        private void BtnAddAdditionDeduction_Click(object sender, EventArgs e)
        {
            if (strProcessDate != null)
            {
                using (FrmAdditionDeduction objFrmAdditionDeduction = new FrmAdditionDeduction())
                {
                    objFrmAdditionDeduction.strProcessDate = strProcessDate;
                    objFrmAdditionDeduction.ShowDialog();
                    FillSalaryInfogrid(strProcessDate);
                }
            }
        }

        private void LoadCombos()
        {
            try
            {
                DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(1, ClsCommonSettings.LoginCompanyID); // Get Company
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = datTemp;

                datTemp = null;
                datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(2, 0); // Get Department
                cboDepartment.ValueMember = "DepartmentID";
                cboDepartment.DisplayMember = "Department";
                cboDepartment.DataSource = datTemp;
                cboDepartment.SelectedIndex = -1;

                datTemp = null;
                datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(2, 0); // Get Department
                cboDepartmentFil.ValueMember = "DepartmentID";
                cboDepartmentFil.DisplayMember = "Department";
                cboDepartmentFil.DataSource = datTemp;
                cboDepartmentFil.SelectedIndex = -1;


                datTemp = null;
                datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(3, 0); // Get Designation
                cboDesignation.ValueMember = "DesignationID";
                cboDesignation.DisplayMember = "Designation";
                cboDesignation.DataSource = datTemp;
                cboDesignation.SelectedIndex = -1;



                datTemp = null;
                datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(3, 0); // Get Designation
                cboDesignationFil.ValueMember = "DesignationID";
                cboDesignationFil.DisplayMember = "Designation";
                cboDesignationFil.DataSource = datTemp;
                cboDesignationFil.SelectedIndex = -1;

                //datTemp = null;
                //datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(4, 0); // Get Process Type
                //cboTransactionType.ValueMember = "ProcessTypeID";
                //cboTransactionType.DisplayMember = "ProcessType";
                //cboTransactionType.DataSource = datTemp;

                datTemp = null;
                datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(5, 0); // Get TransactionType
                cboFilterTransactionType.ValueMember = "TransactionTypeID";
                cboFilterTransactionType.DisplayMember = "TransactionType";
                cboFilterTransactionType.DataSource = datTemp;

                datTemp = null;
                datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(6, ClsCommonSettings.LoginCompanyID); // Get Company & Branch
                cboFilterCompany.ValueMember = "CompanyID";
                cboFilterCompany.DisplayMember = "CompanyName";
                cboFilterCompany.DataSource = datTemp;
                cboFilterCompany.SelectedValue = intCompanyID;

                datTemp = null;
                datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(11, 0); // Get Payment Classification
                cboPaymentClassification.ValueMember = "PaymentClassificationID";
                cboPaymentClassification.DisplayMember = "PaymentClassification";
                cboPaymentClassification.DataSource = datTemp;
                cboPaymentClassification.SelectedValue = 1;

                cboCompany.SelectedValue = intCompanyID;
            }
            catch{}
        }

        private DataTable getNewTableForProcess()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("SelectAll");
            datTemp.Columns.Add("EmployeeID");
            datTemp.Columns.Add("EmployeeName");
            datTemp.Columns.Add("CompanyID");
            datTemp.Columns.Add("CompanyName");
            datTemp.Columns.Add("DepartmentID");
            datTemp.Columns.Add("Department");
            datTemp.Columns.Add("DesignationID");
            datTemp.Columns.Add("Designation");
            datTemp.Columns.Add("EmploymentTypeID");
            datTemp.Columns.Add("EmploymentType");
            datTemp.Columns.Add("TransactionTypeID");
            datTemp.Columns.Add("TransactionType");
            datTemp.Columns.Add("PaymentClassificationID");
            datTemp.Columns.Add("PaymentClassification");
            datTemp.Columns.Add("BasicPay");
            datTemp.Columns.Add("Currency");
            datTemp.Columns.Add("Reason");
            datTemp.Columns.Add("Released");
            return datTemp;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedIndex != -1)
            {
                //DataTable datTempBranch = new DataTable();
                //datTempBranch.Columns.Add("BranchID");
                //datTempBranch.Columns.Add("BranchName");

                //if (cboCompany.SelectedIndex >= 0)
                //    datTempBranch = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(7, cboCompany.SelectedValue.ToInt32()); // Get Branch

                //cboBranch.ValueMember = "BranchID";
                //cboBranch.DisplayMember = "BranchName";
                //cboBranch.DataSource = datTempBranch;
                //cboBranch.SelectedIndex = 0;

                cboDepartment.SelectedIndex = -1;
                cboDesignation.SelectedIndex = -1;
                cboDepartment.Text = "";
                cboDesignation.Text = "";
            }
        }

        private void cboFilterCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable datTempCurrency = new DataTable();
            datTempCurrency.Columns.Add("CurrencyID");
            datTempCurrency.Columns.Add("Currency");

            DataTable datTempBank = new DataTable();
            datTempBank.Columns.Add("BankID");
            datTempBank.Columns.Add("Bank");

            if (cboFilterCompany.SelectedIndex >= 0)
            {
                datTempCurrency = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(9, cboFilterCompany.SelectedValue.ToInt32()); // Get Currency
                datTempBank = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(24, cboFilterCompany.SelectedValue.ToInt32()); // Get Bank
            }
            
            //cboFilterCurrency.ValueMember = "CurrencyID";
            //cboFilterCurrency.DisplayMember = "Currency";
            //cboFilterCurrency.DataSource = datTempCurrency;
            //if (datTempCurrency.Rows.Count > 0)
            //    cboFilterCurrency.SelectedIndex = 0;

            cboFilterBank.ValueMember = "BankID";
            cboFilterBank.DisplayMember = "Bank";
            cboFilterBank.DataSource = datTempBank;
            cboFilterBank.SelectedIndex = -1;

            cboFilterTransactionType.SelectedIndex = -1;
            //cboFilterCurrency.Text = "";
            cboFilterBank.Text = "";
            cboFilterTransactionType.Text = "";

            PCompanyID = intCompanyID;
            FillSalaryInfogrid(strProcessDate);
        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany_SelectedIndexChanged(sender, e);
            ComboBox_KeyPress(sender, e);
        }
        private void PointSelectAll()
        {
            //try
            //{
            //    if (ClsCommonSettings.IsArabicView)
            //    {
            //        int X1 = dgvSalaryDetails.Location.X.ToInt32();
            //        int Y1 = dgvSalaryDetails.Location.Y.ToInt32();

            //        X1 = X1 + dgvSalaryDetails.Width -23;
            //        chkSelectAll.Location = new Point(X1, Y1);
            //    }
            //}
            //catch
            //{

            //}
        }
        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {

                PointSelectAll();
                if (MAddPermission == false)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(9131);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (FormValidationProcess())
                {
                    if (SaveDetails())
                        Application.DoEvents();
                }

                btnProcess.Enabled = true;
            }
            catch
            {
                btnProcess.Enabled = false;
            }
        }

        private bool FormValidationProcess()
        {
            try
            {
                DateTime MAXDate = new DateTime();
                int CompanyID = 0;

                errSalaryProcessRelease.Clear();
                if (blnSalaryDayIsEditable)
                    MAXDate = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, dtpFromDate.Value.Date));
                else
                    MAXDate = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, LoadDate));
                
                if (MAXDate < dtpToDate.Value.Date)
                    dtpToDate.Value = MAXDate;                

                if (cboPaymentClassification.SelectedIndex == -1)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(8153);
                    errSalaryProcessRelease.SetError(cboPaymentClassification, MstrCommonMessage.Replace("#", "").Trim());
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);                    
                    lblStatus.Text = MstrCommonMessage.Replace("#", "").Trim();
                    tmrSalryProcessRelease.Enabled = true;
                    cboPaymentClassification.Focus();
                    return false;
                }
                else
                    errSalaryProcessRelease.SetError(cboPaymentClassification, "");

                if (dtpFromDate.Value.Date > dtpToDate.Value.Date)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(1807);
                    errSalaryProcessRelease.SetError(dtpFromDate, MstrCommonMessage.Replace("#", "").Trim());
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblStatus.Text = MstrCommonMessage.Replace("#", "").Trim();
                    tmrSalryProcessRelease.Enabled = true;
                    dtpFromDate.Focus();
                    return false;
                }
                else
                    errSalaryProcessRelease.SetError(dtpFromDate, "");                

                if (chkPartial.Checked)
                {
                    if (DateCheckingForPartial() == false)
                    {
                        MstrCommonMessage = UserMessage.GetMessageByCode(1807);
                        errSalaryProcessRelease.SetError(dtpToDate, MstrCommonMessage.Replace("#", "").Trim());
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblStatus.Text = MstrCommonMessage.Replace("#", "").Trim();
                        tmrSalryProcessRelease.Enabled = true;
                        dtpToDate.Focus();
                        return false;
                    }
                }

                if (cboCompany.SelectedIndex != -1)
                {
                    intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                    //if (cboBranch.SelectedValue.ToInt32() > 0)
                    //{
                    //    CompanyID = Convert.ToInt32(cboBranch.SelectedValue);
                    //    BranchIndicator = chkIncludeCompany.Checked ? -1 : -2;

                    //    if (BranchIndicator == -2)
                    //        intCompanyID = Convert.ToInt32(cboBranch.SelectedValue);
                    //}
                    //else if (cboBranch.SelectedValue.ToInt32() == -2)
                    //{
                    //    CompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                    //    BranchIndicator = -2;
                    //}
                    //else if (cboBranch.SelectedValue.ToInt32() == -1)
                    //{
                    //    CompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                    //    BranchIndicator = chkIncludeCompany.Checked ? 0 : -3;
                    //}
                }
                else
                {
                    CompanyID = 0;
                    MstrCommonMessage = UserMessage.GetMessageByCode(14);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                MCompanyID = CompanyID;
                MEmployeeID = 0;
                MFromDate = dtpFromDate.Text.Trim();
                MToDate = dtpToDate.Text.Trim();
                MPaymentClassificationID = cboPaymentClassification.SelectedValue.ToInt32();
                GridList(); // Showing Processed and Non Processed Data in the grid

                if (dgvSalaryDetails.RowCount > 0)
                {
                    Application.DoEvents();
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool DateCheckingForPartial()
        {
            long DateDiffCnt = 0;
            DateDiffCnt = 0;

            if (cboPaymentClassification.SelectedIndex != -1)
            {
                switch (cboPaymentClassification.SelectedValue.ToInt32())
                {
                    case 1:
                        DateDiffCnt = DateAndTime.DateDiff(DateInterval.Day, dtpFromDate.Value.Date, dtpToDate.Value.Date, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
                        if (DateDiffCnt >= 1)
                            return false;
                        else
                            return true;
                    case 2:
                        DateDiffCnt = DateAndTime.DateDiff(DateInterval.Weekday, dtpFromDate.Value.Date, dtpToDate.Value.Date, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
                        if (DateDiffCnt >= 1)
                            return false;
                        else
                            return true;
                    case 3:
                        DateDiffCnt = DateAndTime.DateDiff(DateInterval.Weekday, dtpFromDate.Value.Date, dtpToDate.Value.Date, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
                        if (DateDiffCnt * 2 >= 1)
                            return false;
                        else
                            return true;
                    case 4:
                        long DateDiffCnt2 = 0;
                        long DateDiffCnt1 = 0;

                        DateDiffCnt2 = DateAndTime.DateDiff(DateInterval.Day, dtpFromDate.Value.Date, dtpToDate.Value.Date, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
                        DateDiffCnt1 = DateAndTime.DateDiff(DateInterval.Day, dtpFromDate.Value.Date, DateAndTime.DateAdd(DateInterval.Month, 1, dtpFromDate.Value.Date), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);

                        if (DateDiffCnt1 < DateDiffCnt2)
                            DateDiffCnt = 1;

                        if (DateDiffCnt >= 1)
                            return false;
                        else
                            return true;
                    default:
                        return false;
                }
            }
            else
                return false;
        }

        private void GridList()
        {
            int intTempDept = 0;
            int intTempDesg = 0;
            int BranchID = 0;
            if (cboDepartment.SelectedIndex >= 0)
                intTempDept = cboDepartment.SelectedValue.ToInt32();

            if (cboDesignation.SelectedIndex >= 0)
                intTempDesg = cboDesignation.SelectedValue.ToInt32();
            BranchIndicator = -2;

            if (cboCompany.SelectedIndex != -1)
            {
                MCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            }
            else
            {
                MCompanyID = ClsCommonSettings.LoginCompanyID;  
            }

            //if (cboBranch.SelectedIndex != -1)
            //{
            //    BranchID = Convert.ToInt32(cboBranch.SelectedValue);
            //}
            //else
            //{
            //    BranchID = ClsCommonSettings.LoginCompanyID;
            //}

            //if (cboBranch.SelectedValue.ToInt32() > 0)
            //{
            //    BranchIndicator = chkIncludeCompany.Checked ? -1 : -2;
            //}
            //else if (cboBranch.SelectedValue.ToInt32() == -2)
            //{
            //    BranchIndicator = -2;
            //}
            //else if (cboBranch.SelectedValue.ToInt32() == -1)
            //{
            BranchIndicator = -3;
            //}

            DataSet ds = new DataSet();
            ds = mobjclsBLLSalaryProcessAndRelease.GetProcessEmployee(0, MCompanyID, MFromDate, MToDate, GetHostName, ClsCommonSettings.UserID, BranchIndicator, 
                intTempDept, intTempDesg,  BranchID);

            if (ds.Tables.Count > 0)
            {
                dgvSalaryDetails.DataSource = ds.Tables[0];
                datTempSalary = (DataTable)dgvSalaryDetails.DataSource; // For Paging

                if (ds.Tables[0].Rows.Count > 0)
                {
                    fillDataGrid_dtgBrowse();
                    GridColumnColor();
                }
            }
            dgvSalaryDetails.ClearSelection();
        }

        private void GridColumnColor()
        {
            GridColumnDisabled();

            for (int i = 0; i <= dgvSalaryDetails.Rows.Count - 1; i++)
            {
                if (intSalaryProcessForm == 1)
                {
                    if (dgvSalaryDetails.Columns.Contains("Reason"))
                    {
                        if (dgvSalaryDetails.Rows[i].Cells["Reason"].Value.ToString() != "" &&
                            dgvSalaryDetails.Rows[i].Cells["Released"].Value.ToString() != "1")
                        {
                            dgvSalaryDetails.Rows[i].DefaultCellStyle.ForeColor = System.Drawing.Color.Firebrick;
                            dgvSalaryDetails.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.WhiteSmoke;
                            dgvSalaryDetails.Rows[i].ReadOnly = true;
                        }
                        else if (dgvSalaryDetails.Rows[i].Cells["Reason"].Value.ToString() == "" &&
                            dgvSalaryDetails.Rows[i].Cells["Released"].Value.ToString() == "1")
                        {
                            dgvSalaryDetails.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.LightSteelBlue;
                            dgvSalaryDetails.Rows[i].ReadOnly = true;
                        }
                    }
                }
                else if (intSalaryProcessForm == 2)
                {
                    if (dgvSalaryDetails.Rows[i].Cells["SelectAll"].Value.ToString() == "True" &&
                        dgvSalaryDetails.Rows[i].Cells["Status"].Value.ToString() == "True")
                    {
                        dgvSalaryDetails.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.LightSteelBlue;
                        dgvSalaryDetails.Rows[i].ReadOnly = true;
                    }


                    if (dgvSalaryDetails.Rows[i].Cells["IsVacationSettle"].Value != null)
                    {

                        if (dgvSalaryDetails.Rows[i].Cells["IsVacationSettle"].Value.ToString() != "" &&
                               dgvSalaryDetails.Rows[i].Cells["IsVacationSettle"].Value.ToString() == "1")
                        {
                            // &&  dgvSalaryDetails.Rows[i].Cells["Released"].Value.ToString() == "0"
                            dgvSalaryDetails.Rows[i].DefaultCellStyle.ForeColor = System.Drawing.Color.Brown;
                            dgvSalaryDetails.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.FloralWhite; 
                            dgvSalaryDetails.Rows[i].ReadOnly = true;
                        }
                    }


                }
            }
        }

        private void GridColumnDisabled()
        {
            try
            {
                if (dgvSalaryDetails.Columns.Contains("SelectAll"))
                {
                    dgvSalaryDetails.Columns["SelectAll"].Visible = true;
                    dgvSalaryDetails.Columns["SelectAll"].Width = 30;
                    dgvSalaryDetails.Columns["SelectAll"].HeaderText = "";
                }

                if (dgvSalaryDetails.Columns.Contains("EmployeeID"))
                    dgvSalaryDetails.Columns["EmployeeID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("CompanyID"))
                    dgvSalaryDetails.Columns["CompanyID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("DepartmentID"))
                    dgvSalaryDetails.Columns["DepartmentID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("DesignationID"))
                    dgvSalaryDetails.Columns["DesignationID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("EmploymentTypeID"))
                    dgvSalaryDetails.Columns["EmploymentTypeID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("TransactionTypeID"))
                    dgvSalaryDetails.Columns["TransactionTypeID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("PaymentClassificationID"))
                    dgvSalaryDetails.Columns["PaymentClassificationID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("Status"))
                    dgvSalaryDetails.Columns["Status"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("VendorID"))
                    dgvSalaryDetails.Columns["VendorID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("EmpVenFlag"))
                    dgvSalaryDetails.Columns["EmpVenFlag"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("PaymentID"))
                    dgvSalaryDetails.Columns["PaymentID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("IsPartial"))
                    dgvSalaryDetails.Columns["IsPartial"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("RecordID"))
                    dgvSalaryDetails.Columns["RecordID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("ProcessYear"))
                    dgvSalaryDetails.Columns["ProcessYear"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("ProcessMonth"))
                    dgvSalaryDetails.Columns["ProcessMonth"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("ProcessDate"))
                    dgvSalaryDetails.Columns["ProcessDate"].Visible = false;
                
                //if (dgvSalaryDetails.Columns.Contains("EmployeeNo"))
                //    dgvSalaryDetails.Columns["EmployeeNo"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("VendorName"))
                    dgvSalaryDetails.Columns["VendorName"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("Released"))
                    dgvSalaryDetails.Columns["Released"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("EmpCurrencyID"))
                    dgvSalaryDetails.Columns["EmpCurrencyID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("IsVacationSettle"))
                    dgvSalaryDetails.Columns["IsVacationSettle"].Visible = false;

                //if (intSalaryProcessForm == 2)
                //{
                //    if (dgvSalaryDetails.Columns.Contains("NetAmount"))
                //    {
                //        dgvSalaryDetails.Columns["NetAmount"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                //    }
                //}
                //    if (dgvSalaryDetails.Columns.Contains("EmployeeName"))
                //    {
                //        dgvSalaryDetails.Columns["EmployeeName"].Frozen = false;
                //        dgvSalaryDetails.Columns["EmployeeName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                //    }
                //}


            }
            catch { }
        }

        private bool SaveDetails()
        {
            try
            {
                int ParaPaymentClassificationID = 0;
                int TypeIndex = 0;
                string ParaProcessDateFrom = "";
                string ParaProcessDateTo = "";

                if (cboPaymentClassification.SelectedIndex != -1)
                    ParaPaymentClassificationID = cboPaymentClassification.SelectedValue.ToInt32();
                else
                    ParaPaymentClassificationID = 0;

                //if (cboTransactionType.SelectedIndex >= 0)
                //    TypeIndex = cboTransactionType.SelectedValue.ToInt32();
                //else
                //    TypeIndex = 0;

                ParaProcessDateFrom = dtpFromDate.Text;
                ParaProcessDateTo = dtpToDate.Text;

                //if (cboTransactionType.SelectedIndex != -1)
                //    TypeIndex = Convert.ToInt32(cboTransactionType.SelectedValue);

                //mobjclsBLLSalaryProcessAndRelease.DeletePayment(ParaProcessDateFrom, GetHostName, ParaProcessDateTo, cboCompany.SelectedValue.ToInt32());

                TypeIndex = 1;
                if (datEmployeeSalaryDetails.Rows.Count > 0)
                {
                    lblStatus.Text = "Salary Processing Initiated";
                    tmrSalryProcessRelease.Enabled = true;

                    tsProgressBar.Visible = true;
                    tsProgressBar.Minimum = 0;
                    tsProgressBar.Maximum = 100;

                    for (int i = 0; i <= datEmployeeSalaryDetails.Rows.Count - 1; i++)
                    {
                        mobjclsBLLSalaryProcessAndRelease.SalaryProcess(Convert.ToInt32(datEmployeeSalaryDetails.Rows[i]["EmployeeID"].ToString()), 
                            Convert.ToInt32(datEmployeeSalaryDetails.Rows[i]["CompanyID"].ToString()), ParaProcessDateFrom, ParaProcessDateTo, 
                            chkPartial.Checked, TypeIndex);
                        tsProgressBar.Value = Convert.ToInt32((i * 100) / datEmployeeSalaryDetails.Rows.Count);
                    }

                    tsProgressBar.Value = 100;
                    tsProgressBar.Visible = false;
                    tsProgressBar.Value = 0;

                    lblStatus.Text = "Salary Processing Successfully Completed";
                    tmrSalryProcessRelease.Enabled = true;
                }

                if (intSalaryProcessForm == 2)
                {
                    cboFilterCompany.SelectedIndex = -1;
                    cboFilterCompany.SelectedValue = MCompanyID;
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private void chkPartial_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPartial.Checked)
                dtpToDate.Enabled = false;
            else
            {
                dtpToDate.Value = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, dtpFromDate.Value.Date));
                dtpToDate.Enabled = false;
            }

            if (blnSalaryDayIsEditable)
                dtpToDate.Enabled = false ;
            else
            {
                if (chkPartial.Checked)
                    dtpToDate.Enabled = false;
                else
                    dtpToDate.Enabled = false;
            }
        }

        private void dtpCurrentMonth_ValueChanged(object sender, EventArgs e)
        {
            MChangeStatus = true;
            btnProcess.Enabled = MChangeStatus;
            dtpCurrentMonth.Value = ("01/" + GetCurrentMonth(dtpCurrentMonth.Value.Month) + "/" + (dtpCurrentMonth.Value.Year) + "").ToDateTime();
            dtpFromDate.Value = ("01/" + GetCurrentMonth(dtpCurrentMonth.Value.Month) + "/" + (dtpCurrentMonth.Value.Year) + "").ToDateTime();
            dtpToDate.Value = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, dtpFromDate.Value.Date));
            dtpToDate.Enabled = false;
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            dtpToDate.Value = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, dtpFromDate.Value.Date));
        }

        private void dgvSalaryDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                dgvSalaryDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
            catch
            {
            }
        }

        private void dgvSalaryDetails_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (e.ColumnIndex >= 0)
                    {
                        if (intSalaryProcessForm == 1)
                        {
                            if (dgvSalaryDetails.Rows[e.RowIndex].Cells["Reason"].Value.ToString() != "" &&
                                dgvSalaryDetails.Rows[e.RowIndex].Cells["Released"].Value.ToString() != "1")
                                lblStatus.Text = "Not able to process this Employee. Reason : " +
                                    dgvSalaryDetails.Rows[e.RowIndex].Cells["Reason"].Value.ToString();
                            else if (dgvSalaryDetails.Rows[e.RowIndex].Cells["Reason"].Value.ToString() == "" &&
                                dgvSalaryDetails.Rows[e.RowIndex].Cells["Released"].Value.ToString() == "1")
                                lblStatus.Text = "This Month Salary of this Employee is Processed and Released";
                            else
                            {
                                lblStatus.Text = "";

                                for (int iCounter = 0; iCounter <= datTempSalary.Rows.Count - 1; iCounter++)
                                {
                                    if (datTempSalary.Rows[iCounter]["EmployeeID"].ToInt32() == dgvSalaryDetails.Rows[e.RowIndex].Cells["EmployeeID"].Value.ToInt32())
                                    {
                                        datTempSalary.Rows[iCounter]["SelectAll"] = true;
                                        break;
                                    }
                                }
                            }
                        }
                        else if (intSalaryProcessForm == 2)
                        {

                            if (dgvSalaryDetails.Rows[e.RowIndex].Cells["SelectAll"].Value.ToString() == "True" &&
                                dgvSalaryDetails.Rows[e.RowIndex].ReadOnly == true)
                                lblStatus.Text = "This Month Salary of this Employee is Processed and Released";
                            else
                            {
                                lblStatus.Text = "";

                                for (int iCounter = 0; iCounter <= datTempSalary.Rows.Count - 1; iCounter++)
                                {
                                    if (datTempSalary.Rows[iCounter]["EmployeeID"].ToInt32() == dgvSalaryDetails.Rows[e.RowIndex].Cells["EmployeeID"].Value.ToInt32())
                                    {
                                        datTempSalary.Rows[iCounter]["SelectAll"] = true;
                                        break;
                                    }
                                }
                            }

                            //ColorNode();
                            //trvSalaryProcessRelease.SelectedNode.ForeColor = System.Drawing.Color.White;
                            //trvSalaryProcessRelease.SelectedNode.BackColor = System.Drawing.Color.Navy; 



                        }
                    }
                }
                //-------


                //TreeNode tr = new TreeNode();
                //tr = trvSalaryProcessRelease.SelectedNode;      
                //if (tr != null)
                //{
                //    trvSalaryProcessRelease.LabelEdit = true;
                //    tr.BeginEdit();
                //    trvSalaryProcessRelease.SelectedNode = tr;
                //}
            }
            catch
            {
            }

        }

        private void dgvSalaryDetails_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                GridColumnColor();
            }
            catch
            {
            }
        }

        private void dgvSalaryDetails_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (intSalaryProcessForm == 2)
                {
                    if (ValidationFormDisplay())
                        DispalyForm();
                }
            }
            catch
            {
            }
        }

        private bool ValidationFormDisplay()
        {
            if (dgvSalaryDetails.RowCount - 1 < 0)
                return false;

            if (cboFilterCompany.SelectedIndex == -1)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(9100);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            return true;
        }

        private void DispalyForm()
        {
            int iRowIndex = dgvSalaryDetails.CurrentRow.Index;
            int PaymentID = dgvSalaryDetails.Rows[iRowIndex].Cells["PaymentID"].Value.ToInt32();
            int PaymentModeID = dgvSalaryDetails.Rows[iRowIndex].Cells["TransactionTypeID"].Value.ToInt32();

            mobjclsBLLSalaryProcessAndRelease.DeleteTempEmployeeIDForPaymentRelease(GetHostName);

            if (PaymentID > 0)
            {
                DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos(new string[] { "PaymentID", "PayEmployeePayment", "" +
                    "PaymentID = " + PaymentID + ""});

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        mobjclsBLLSalaryProcessAndRelease.InsertTempEmployeeIDForPaymentRelease(GetHostName, datTemp.Rows[0]["PaymentID"].ToInt64());
                }

                using (FrmSalaryPayment FrmObj = new FrmSalaryPayment())
                {
                    if (CheckReleased(PaymentID))
                        FrmObj.PFormActiveFlag = false;
                    else
                        FrmObj.PFormActiveFlag = true;

                    FrmObj.PCompanyName = cboFilterCompany.Text;
                    FrmObj.PintPaymentID = PaymentID;
                    FrmObj.PintPaymentModeID = PaymentModeID;
                    FrmObj.PiCurrencyIDFilter = MCurrencyID;
                    FrmObj.PConformActiveFlag = false;
                    FrmObj.PDoubleClick = true; 
                    FrmObj.PCompanyID = cboFilterCompany.SelectedValue.ToInt32();
                    FrmObj.blnIsEditMode = false;
                    FrmObj.ShowDialog();
                }
            }
        }

        private bool CheckReleased(int PaymentID)
        {
            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos(new string[] {"PaymentID", "PayEmployeePayment", "" +
                "PaymentID = isnull(" + PaymentID + ",0) and isnull(Released,0) = 1"});

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            fillSalaryProcessAndReleaseTable();

            if (intSalaryProcessForm == 1) // Salary Process
            {
                if (SaveDetails())
                {
                    tmrSalryProcessRelease.Enabled = true;
                    Application.DoEvents();
                    MChangeStatus = false;

                    pnlMentioned.Visible = false;
                    expPnlSalaryRelease.Expanded = true;
                    expPnlSalaryRelease.Visible = true;
                    expPnlSalaryProcess.Visible = false;
                    trvSalaryProcessRelease.Visible = true;
                    btnBack.Visible = true;

                    getProcessedDetails();
                    intSalaryProcessForm = 2;
                    this.Text = "Salary Release " + this.Text.Replace("Salary Process ", "");
                    btnProcess.Text = "&Release";
                    btnProcess.Image = Properties.Resources.Salary_release;
                    pnlLeft.Width = 150;
                    GridColumnColor();
                }
            }
            else if (intSalaryProcessForm == 2) // Salary Release
            {
                //if (MUpdatePermission == false)
                //{
                //    MstrCommonMessage = UserMessage.GetMessageByCode(9131);
                //    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    return;
                //}
                if (MAddPermission == false)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(9131);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (FormValidationRelease())
                    EmployeePaymentRelease();
            }
        }

        private void fillSalaryProcessAndReleaseTable()
        {
            datEmployeeSalaryDetails.Rows.Clear();

            for (int i = 0; i <= datTempSalary.Rows.Count - 1; i++)
            {
                if (Convert.ToBoolean(datTempSalary.Rows[i]["SelectAll"]))
                {
                    DataRow dr = datEmployeeSalaryDetails.NewRow();

                    if (datTempSalary.Columns.Contains("EmployeeID"))
                        dr["EmployeeID"] = datTempSalary.Rows[i]["EmployeeID"].ToString();

                    if (datTempSalary.Columns.Contains("CompanyID"))
                        dr["CompanyID"] = datTempSalary.Rows[i]["CompanyID"].ToString();

                    datEmployeeSalaryDetails.Rows.Add(dr);
                }
            }
        }

        private void getProcessedDetails()
        {
            try
            {
                FillDate = mObjCon.GetSysDate();
                FillProcessYear();
                DataSet ds = new DataSet();
                ds = dsTemp;
                SetSalaryHead();

                if (dsTemp.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        fillDataGrid_dtgBrowse();
                        lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                    }
                    else
                    {
                        dgvSalaryDetails.DataSource = null;
                        datTempSalary = null;
                        ItemContainer5.Enabled = false;
                    }
                }
                else
                {
                    dgvSalaryDetails.DataSource = null;
                    datTempSalary = null;
                    ItemContainer5.Enabled = false;
                    lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                }

                FillSalaryInfogrid(FillDate.ToString());
                Application.DoEvents();
                cboFilterCompany.SelectedIndex = -1;
                if (cboCompany.SelectedIndex != -1)
                {
                    cboFilterCompany.SelectedValue = cboCompany.SelectedValue.ToInt32();
                }
                else
                {
                    cboFilterCompany.SelectedValue = intCompanyID;
                }
              
                MbFirst = false;
            }
            catch { }
        }

        private void FillProcessYear()
        {
            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(14, 0); // Get Processed Years
            cboFilterProcessYear.ValueMember = "ProcessYearID";
            cboFilterProcessYear.DisplayMember = "ProcessYear";
            cboFilterProcessYear.DataSource = datTemp;

            if (cboFilterProcessYear.Items.Count > 0)
                cboFilterProcessYear.SelectedIndex = 0;
        }

        private bool SetSalaryHead()
        {
            try
            {
                if (trvSalaryProcessRelease.GetNodeCount(false) > 0)
                {
                    trvSalaryProcessRelease.Focus();
                    TreeNode tr = new TreeNode();
                    tr = GetNode(FillDate, trvSalaryProcessRelease.Nodes);

                    if (tr != null)
                    {
                        trvSalaryProcessRelease.LabelEdit = true;
                        tr.BeginEdit();
                        trvSalaryProcessRelease.SelectedNode = tr;
                        trvSalaryProcessRelease.LabelEdit = false;
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private TreeNode GetNode(string text, TreeNodeCollection parentcollection)
        {
            TreeNode ret = new TreeNode();

            foreach (TreeNode Child in parentcollection)
            {
                if (Child.Text.Trim() == text)
                    ret = Child;
                else if (Child.GetNodeCount(false) > 0)
                    ret = GetNode(text, Child.Nodes);

                if (ret != null)
                    break;
            }
            return ret;
        }

        public bool FillSalaryInfogrid(string sProcessDate)
        {
            try
            {
                MbFirst = true;
                FlagSelect = false;
                PProcessYear = cboFilterProcessYear.SelectedValue.ToInt32();

                if (cboFilterCompany.SelectedIndex >= 0)
                    PCompanyID = cboFilterCompany.SelectedValue.ToInt32();

                if (TransactionTypeIDRel == -1)
                    TransactionTypeIDRel = 0;

                dgvSalaryDetails.DataSource = null;
                datTempSalary = null;
                dsTemp = mobjclsBLLSalaryProcessAndRelease.FillSalaryInfogrid(sProcessDate, TemplateID, PBankNameIDSalRel, TransactionTypeIDRel, 
                    PProcessYear, PCompanyID, MCurrencyID,PDesignationID,PDepartmentID);

                dgvSalaryDetails.DataSource = dsTemp.Tables[0];
                datTempSalary = (DataTable)dgvSalaryDetails.DataSource;

                if (datTempSalary != null)
                {
                    if (datTempSalary.Rows.Count > 0)
                        GridColumnColor();
                }

                GridColumnDisabled();
                CurrentPage = 1;
                fillDataGrid_dtgBrowse();
                MbFirst = false;
                dgvSalaryDetails.ClearSelection();                
                return true;
            }
            catch
            {
                return true;
            }
        }
    
        private void GridSetUp()
        {
            try
            {
                FlgBlk = false;
                int I = 0;
                int IntScale = 0;

                if (dgvSalaryDetails.Columns.Count > 1)
                {
                    if (dgvSalaryDetails.Columns.Contains("SelectAll"))
                    {
                        dgvSalaryDetails.Columns["SelectAll"].Width = 30;
                        dgvSalaryDetails.Columns["SelectAll"].HeaderText = "";
                    }

                    if (dgvSalaryDetails.Columns.Contains("EmployeeName"))
                    {
                        dgvSalaryDetails.Columns["EmployeeName"].Width = 150;
                        dgvSalaryDetails.Columns["EmployeeName"].Frozen = true;
                    }

                    if (dgvSalaryDetails.Columns.Contains("VendorName"))
                        dgvSalaryDetails.Columns["VendorName"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("PaymentMode"))
                        dgvSalaryDetails.Columns["PaymentMode"].Visible = false;                    

                    if (dgvSalaryDetails.Columns.Contains("PeriodFrom"))
                        dgvSalaryDetails.Columns["PeriodFrom"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("PeriodTo"))
                        dgvSalaryDetails.Columns["PeriodTo"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("BankName"))
                        dgvSalaryDetails.Columns["BankName"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("TransactionType"))
                        dgvSalaryDetails.Columns["TransactionType"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("PaymentDate"))
                        dgvSalaryDetails.Columns["PaymentDate"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("BasicPay"))
                        dgvSalaryDetails.Columns["BasicPay"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("Additions"))
                        dgvSalaryDetails.Columns["Additions"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("Deductions"))
                        dgvSalaryDetails.Columns["Deductions"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("NetAmount"))
                        dgvSalaryDetails.Columns["NetAmount"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("status"))
                        dgvSalaryDetails.Columns["status"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("VendorID"))
                        dgvSalaryDetails.Columns["VendorID"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("EmpVenFlag"))
                        dgvSalaryDetails.Columns["EmpVenFlag"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("PaymentID"))
                        dgvSalaryDetails.Columns["PaymentID"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("EmployeeID"))
                        dgvSalaryDetails.Columns["EmployeeID"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("PaymentClassificationID"))
                        dgvSalaryDetails.Columns["PaymentClassificationID"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("ProcessYear"))
                        dgvSalaryDetails.Columns["ProcessYear"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("ProcessMonth"))
                        dgvSalaryDetails.Columns["ProcessMonth"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("ProcessDate"))
                        dgvSalaryDetails.Columns["ProcessDate"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("TransactionTypeID"))
                        dgvSalaryDetails.Columns["TransactionTypeID"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("IsPartial"))
                        dgvSalaryDetails.Columns["IsPartial"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("RecordId"))
                        dgvSalaryDetails.Columns["RecordId"].Visible = false;
                    
                    //dgvSalaryDetails.Columns[23].ReadOnly = true;

                    for (I = 0; I <= dgvSalaryDetails.ColumnCount - 1; I++)
                    {
                        dgvSalaryDetails.Columns[I].SortMode = DataGridViewColumnSortMode.NotSortable;

                        if (I != 0)
                        {
                            dgvSalaryDetails.Columns[I].ReadOnly = true;
                            dgvSalaryDetails.Columns[I].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                        }
                        else
                            dgvSalaryDetails.Columns[I].ReadOnly = false;
                    }
                }


                IntScale = 2;

                DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos( new string[] {"isnull(Y.Scale,2) as Scale", "" +
                    "CurrencyReference AS Y INNER JOIN CompanyMaster AS C ON C.CurrencyId = Y.CurrencyID", "C.CompanyID = " + PCompanyID + ""});

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        IntScale = datTemp.Rows[0]["Scale"].ToInt32();
                }

                if (dgvSalaryDetails.Columns.Contains("BasicPay"))
                {
                    dgvSalaryDetails.Columns["BasicPay"].DefaultCellStyle.Format = "n" + IntScale.ToString();
                    dgvSalaryDetails.Columns["BasicPay"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }

                if (dgvSalaryDetails.Columns.Contains("Additions"))
                {
                    dgvSalaryDetails.Columns["Additions"].DefaultCellStyle.Format = "n" + IntScale.ToString();
                    dgvSalaryDetails.Columns["Additions"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }

                if (dgvSalaryDetails.Columns.Contains("Deductions"))
                {
                    dgvSalaryDetails.Columns["Deductions"].DefaultCellStyle.Format = "n" + IntScale.ToString();
                    dgvSalaryDetails.Columns["Deductions"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }

                if (dgvSalaryDetails.Columns.Contains("NetAmount"))
                {
                    dgvSalaryDetails.Columns["NetAmount"].DefaultCellStyle.Format = "n" + IntScale.ToString();
                    dgvSalaryDetails.Columns["NetAmount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }

                if (dgvSalaryDetails.Columns.Contains("NetAmount"))
                    dgvSalaryDetails.Columns["NetAmount"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
  

                PointSelectAll(); 
            }
            catch { }
        }

        private void cboFilterProcessYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboFilterProcessYear.SelectedIndex != -1)
            {
                TemplateID = 0;
                FillTreeDetails();
                PProcessYear = cboFilterProcessYear.SelectedValue.ToInt32();
                FillSalaryInfogrid(strProcessDate);
            }
        }

        private void FillTreeDetails()
        {
            try
            {
                string YearName = "";
                string MonthName = "";
                string ProcessDate = "";
                int IntYear = 0;

                if (cboFilterProcessYear.SelectedIndex >= 0)
                    IntYear = Convert.ToInt32(cboFilterProcessYear.SelectedValue);
                else
                    IntYear = 0;

                DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.getEmployeePaymentDetail(TemplateID, IntYear);
                trvSalaryProcessRelease.Nodes.Clear();

                for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                {
                    if (datTemp.Rows[iCounter]["ProcessYear"].ToString() != YearName)
                    {
                        TvRoot = trvSalaryProcessRelease.Nodes.Add(datTemp.Rows[iCounter]["ProcessYear"].ToString());
                        TvRoot.Tag = datTemp.Rows[iCounter]["ProcessYear"].ToString();
                    }

                    if (datTemp.Rows[iCounter]["ProcessMonth"].ToString() != MonthName || datTemp.Rows[iCounter]["ProcessYear"].ToString() != YearName)
                    {
                        TvChild = TvRoot.Nodes.Add(datTemp.Rows[iCounter]["ProcessMonth"].ToString());
                        TvChild.Tag = datTemp.Rows[iCounter]["ProcessMonth"].ToString();
                    }

                    if (datTemp.Rows[iCounter]["ProcessDate"].ToString() != ProcessDate)
                    {
                        TvChildChild = TvChild.Nodes.Add(datTemp.Rows[iCounter]["ProcessDate"].ToString().Trim());
                        TvChildChild.Tag = datTemp.Rows[iCounter]["ProcessDate"].ToString().Trim() + "@";
                    }

                    MonthName = datTemp.Rows[iCounter]["ProcessMonth"].ToString();
                    YearName = datTemp.Rows[iCounter]["ProcessYear"].ToString();
                    ProcessDate = datTemp.Rows[iCounter]["ProcessDate"].ToString();
                }

                trvSalaryProcessRelease.ExpandAll();
                FillDate = "01 Jan 1900";
                dgvSalaryDetails.DataSource = null;
                datTempSalary = null;
            }
            catch
            {
            }
        }


        private void ColorNode()
        {
            foreach (TreeNode RNode in trvSalaryProcessRelease.Nodes)
            {
                RNode.BackColor = System.Drawing.Color.White;
                RNode.ForeColor = System.Drawing.Color.Black;

                foreach (TreeNode RChild1 in RNode.Nodes)
                {
                    RChild1.BackColor = System.Drawing.Color.White;
                    RChild1.ForeColor = System.Drawing.Color.Black;

                    foreach (TreeNode RChild2 in RChild1.Nodes)
                    {
                        RChild2.BackColor = System.Drawing.Color.White;
                        RChild2.ForeColor = System.Drawing.Color.Black;
                    }

                }

            }
        }
        private void trvSalaryProcessRelease_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                FillGridColCombo();
                //TreeNode selectedNode = trvSalaryProcessRelease.HitTest(e.Location).Node;  
          
                if (e.Button != System.Windows.Forms.MouseButtons.Right)
                {
                    string sTag = e.Node.Tag.ToString();

                    if (sTag.IndexOf("@") != -1)
                    {
                        //ClearData();
                        FillDate = sTag.Replace("@", "").Trim();
                        FillDate = FillDate.Replace("@", "");
                        FillSalaryInfogrid(FillDate);
                        //dgvSalaryDetails.Columns[24].Visible = false;
                        strProcessDate = FillDate;
                    }
                }
                else if (e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    string sTag = "";

                    if (trvSalaryProcessRelease !=  null)
                    {
                        if (trvSalaryProcessRelease.SelectedNode != null)
                        {
                            if (Strings.InStr(trvSalaryProcessRelease.SelectedNode.Tag.ToString().Trim(), "\\", CompareMethod.Text) != 0)
                                sTag = trvSalaryProcessRelease.SelectedNode.Tag.ToString().Remove(trvSalaryProcessRelease.SelectedNode.Tag.ToString().LastIndexOf("\\")).ToString().Trim();
                            else
                                sTag = trvSalaryProcessRelease.SelectedNode.Tag.ToString().Trim();
                        }

                        if (sTag.IndexOf("@") != -1 && dgvSalaryDetails.Rows.Count > 0)
                            this.DeleteContextMenuStrip.Show(this.trvSalaryProcessRelease, this.trvSalaryProcessRelease.PointToClient(Cursor.Position));
                    }
                }

                //if (trvSalaryProcessRelease.SelectedNode != null)
                //{
                //    ColorNode();
                //    trvSalaryProcessRelease.SelectedNode.ForeColor = System.Drawing.Color.White;
                //    trvSalaryProcessRelease.SelectedNode.BackColor = System.Drawing.Color.Navy;
                //    ColorNode();
                //    trvSalaryProcessRelease.SelectedNode.ForeColor = System.Drawing.Color.White;
                //    trvSalaryProcessRelease.SelectedNode.BackColor = System.Drawing.Color.Navy;
                //}


            }
            catch { }
        }

        private void ClearData()
        {
            cboDepartmentFil.SelectedIndex = -1;
            cboDesignationFil.SelectedIndex = -1;
            cboDepartmentFil.Text = "";
            cboDesignationFil.Text = "";
            cboFilterTransactionType.SelectedIndex = -1;
            //cboFilterCurrency.SelectedIndex = -1;
            cboFilterBank.SelectedIndex = -1;
            TemplateID = 0;
            PBankNameIDSalRel = 0;
            TransactionTypeIDRel = 0;
            PProcessYear = Convert.ToDateTime(mObjCon.GetSysDate()).Year;
            PCompanyID = 0;
            MCurrencyID = 0;
        }

        private bool FormValidationRelease()
        {
            string ComputerName = "";

            if (cboFilterCompany.SelectedIndex == -1)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(9100);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            //if (cboFilterCurrency.SelectedIndex == -1)
            //{
            //    MstrCommonMessage = UserMessage.GetMessageByCode(1832);
            //    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return false;
            //}

            if (cboFilterTransactionType.SelectedIndex == -1)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(1831);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            if (SetTempTableValue() == false)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(1803);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            //--------------------------------------------------------Alteration Required------------------------------------------------------
            //--------------------------------------------------------Alteration Required------------------------------------------------------
            //--------------------------------------------------------Alteration Required------------------------------------------------------
            //if (CheckPaymentRelease() == false)
            //{
            //    MstrCommonMessage = UserMessage.GetMessageByCode(1820);
            //    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    FillSalaryInfogrid(FillDate);
            //    return false;
            //}
            //--------------------------------------------------------Alteration Required------------------------------------------------------
            //--------------------------------------------------------Alteration Required------------------------------------------------------
            //--------------------------------------------------------Alteration Required------------------------------------------------------

            if (checkPaymentIDUse() == false)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(1821);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                FillSalaryInfogrid(FillDate);
                return false;
            }

            long IPaymentID = 0;

            for (int iCounter = 0; iCounter <= dgvSalaryDetails.RowCount - 1; iCounter++)
            {
                if (dgvSalaryDetails.Rows[iCounter].Cells["SelectAll"].Value.ToBoolean() == true &&
                        dgvSalaryDetails.Rows[iCounter].Cells["Status"].Value.ToBoolean() == false)
                {
                    IPaymentID = dgvSalaryDetails.Rows[iCounter].Cells["PaymentID"].Value.ToInt64();
                    if (IsPartialChk(IPaymentID) == true)
                    {
                        MstrCommonMessage = UserMessage.GetMessageByCode(1814);
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }

            ComputerName = GetHostName;
            return true;
        }

        private bool SetTempTableValue()
        {
            string ComputerName = GetHostName;
            string ProcessDate = "";
            int EmployeeID = 0;
            long PaymentID = 0;
            FlagSelect = false;

            mobjclsBLLSalaryProcessAndRelease.DeleteTempEmployeeIDForPaymentRelease(ComputerName);

            for (int II = 0; II <= dgvSalaryDetails.RowCount - 1; II++)
            {
                if (dgvSalaryDetails.Rows[II].Cells["PaymentID"].Value != null)
                {
                    if (dgvSalaryDetails.Rows[II].Cells["SelectAll"].Value.ToBoolean() == true &&
                        dgvSalaryDetails.Rows[II].Cells["Status"].Value.ToBoolean() == false)
                    {
                        PaymentID = dgvSalaryDetails.Rows[II].Cells["PaymentID"].Value.ToInt64();
                        ProcessDate = dgvSalaryDetails.Rows[II].Cells["ProcessDate"].Value.ToStringCustom();
                        EmployeeID = dgvSalaryDetails.Rows[II].Cells["EmployeeID"].Value.ToInt32();
                        FlagSelect = true;
                        mobjclsBLLSalaryProcessAndRelease.InsertTempEmployeeIDForPaymentRelease(EmployeeID, ProcessDate, ComputerName, PaymentID);
                    }
                }
            }

            if (FlagSelect)
                return true;
            else
                return false;
        }

        private bool CheckPaymentRelease()
        {
            string sPaymentIds = "";

            for (int i = 0; i<= dgvSalaryDetails.RowCount - 1; i++)
            {
                if (dgvSalaryDetails.Rows[i].Cells["PaymentID"].Value != null)
                {
                    if (dgvSalaryDetails.Rows[i].Cells["SelectAll"].Value.ToBoolean() == true &&
                        dgvSalaryDetails.Rows[i].Cells["Status"].Value.ToBoolean() == false)
                        sPaymentIds += dgvSalaryDetails.Rows[i].Cells["PaymentID"].Value.ToStringCustom() + ",";
                }
            }

            if (sPaymentIds != "")
            {
                sPaymentIds = sPaymentIds.Substring(0, sPaymentIds.Length - 1);
                DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos(new string[] { "PaymentID", "PayEmployeePayment", "" +
                    "Released = 1 AND PaymentID IN (" + sPaymentIds + ")"});

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        return false;
                    else
                        return true;
                }
                else
                    return true;
            }
            else
                return false;
        }

        private bool checkPaymentIDUse()
        {
            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.checkPaymentIDUse(GetHostName);

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    return false;
                else
                    return true;
            }
            else
                return true;
        }

        private bool IsPartialChk(long PaymentID)
        {
            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.CheckPaymentIsPartial(PaymentID);

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        private void EmployeePaymentRelease()
        {
            if (cboFilterCompany.SelectedIndex >= 0)
            {
                string ComputerName = GetHostName;
                int PaymentModeID = 0;
                PCompanyID = Convert.ToInt32(cboFilterCompany.SelectedValue);

                if (cboFilterTransactionType.SelectedIndex >= 0)
                    PaymentModeID = Convert.ToInt32(cboFilterTransactionType.SelectedValue);

                using (FrmSalaryPayment ObjFrmSalaryPayment = new FrmSalaryPayment())
                {
                    ObjFrmSalaryPayment.PCompanyName = cboFilterCompany.Text;
                    ObjFrmSalaryPayment.PintPaymentModeID = PaymentModeID;
                    ObjFrmSalaryPayment.TransactionTypeIDView = TransactionTypeIDRel;
                    ObjFrmSalaryPayment.PFormActiveFlag = true;
                    ObjFrmSalaryPayment.PConformActiveFlag = true;
                    ObjFrmSalaryPayment.PCompanyID = PCompanyID;
                    ObjFrmSalaryPayment.PProcessDate = FillDate;
                    ObjFrmSalaryPayment.PiCurrencyIDFilter = MCurrencyID;
                    ObjFrmSalaryPayment.blnIsEditMode = true;
                    ObjFrmSalaryPayment.ShowDialog();
                }

                FillTreeDetails();

                if (FillDate == "")
                    FillDate = "01 Jan 1900";

                FillSalaryInfogrid(FillDate);
            }
        }

        private void tmrSalryProcessRelease_Tick(object sender, EventArgs e)
        {
            tmrSalryProcessRelease.Enabled = false;
            lblStatus.Text = "";
        }



        private void cboFilterTransactionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboFilterBank.Enabled = true;

            if (cboFilterTransactionType.SelectedIndex >= 0)
            {
                TransactionTypeIDRel = cboFilterTransactionType.SelectedValue.ToInt32();

                if (cboFilterTransactionType.SelectedValue.ToInt32() != (int)PaymentTransactionType.Bank)
                {
                    cboFilterBank.Enabled = false;
                    cboFilterBank.SelectedIndex = -1;
                    PBankNameIDSalRel = 0;
                }
            }
            else if (cboFilterTransactionType.SelectedIndex == -1)
            {
                TransactionTypeIDRel = -1;
                cboFilterBank.Enabled = false;
                cboFilterBank.SelectedIndex = -1;
                PBankNameIDSalRel = 0;
            }

            FillSalaryInfogrid(strProcessDate);
        }

        private void cboFilterBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboFilterBank.SelectedIndex >= 0)
                PBankNameIDSalRel = cboFilterBank.SelectedValue.ToInt32();
            else if (cboFilterBank.SelectedIndex == -1)
                PBankNameIDSalRel = 0;

            FillSalaryInfogrid(strProcessDate);
        }

        private void btnFilterClear_Click(object sender, EventArgs e)
        {
            cboFilterCompany.SelectedIndex = -1;
            cboFilterCompany.SelectedValue = intCompanyID;
            ClearData();
        }

        private void ToolStripMenuItemDelete_Click(object sender, EventArgs e)
        {
            int BankNameIDSalRel = PBankNameIDSalRel;
            int ProcessYear = PProcessYear;
            string sProcessDate = FillDate;
            int TempTemplateID = TemplateID;
            int TransactionTypeID = TransactionTypeIDRel;
            
            Application.DoEvents();

            if (cboFilterCompany.SelectedIndex == -1)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(9100);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            MstrCommonMessage = UserMessage.GetMessageByCode(13);
            if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Cancel)
                return;

            PCompanyID = cboFilterCompany.SelectedValue.ToInt32();

            if (mobjclsBLLSalaryProcessAndRelease.DeletePayment(FillDate, GetHostName, PCompanyID) == 0)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(1812);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(1811);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                FillDate = sProcessDate;
                TemplateID = TempTemplateID;
                PBankNameIDSalRel = BankNameIDSalRel;
                TransactionTypeIDRel = TransactionTypeID;
                PProcessYear = ProcessYear;
                FillTreeDetails();
                FillSalaryInfogrid(FillDate);
            }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (MViewPermission == false)
                    return;

                if ((dgvSalaryDetails.RowCount - 1) < 0)
                    return;

                if (dgvSalaryDetails.CurrentRow.Index < 0)
                    return;

                ClsWebform objClsWebform = new ClsWebform();
                using (FrmEmailPopup objemail = new FrmEmailPopup())
                {
                    objemail.MsSubject = "Payment";
                    objemail.EmailFormType = EmailFormID.SalaryPayment;
                    objemail.MiRecordID = Convert.ToInt32(dgvSalaryDetails.Rows[dgvSalaryDetails.CurrentRow.Index].Cells["PaymentID"].Value);
                    objemail.ShowDialog();
                }
            }
            catch
            {
            }

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (MViewPermission == false)
                return;

            FillDate = FillDate.Replace("@", "").Trim();

            if (FillDate == "01 Jan 1900")
                return;

            if (FillDate.Trim().Length < 7)
                return;

            FrmReportviewer ObjViewer = new FrmReportviewer();
            ObjViewer.PsFormName = "Salary Release";
            ObjViewer.sDate = FillDate.ToString().Trim();
            ObjViewer.PiFormID = (int)FormID.SalaryRelease;
            ObjViewer.PintCompany = Convert.ToInt32(cboCompany.SelectedValue);
            ObjViewer.ShowDialog();
        }

        private void btnSalarySlip_Click(object sender, EventArgs e)
        {
            //Salary Slip Employee Currency
            string sPaymentID;
            int iStype = 0; 

            if (dgvSalaryDetails.RowCount == 0)
                return;

            int iRowIndex = dgvSalaryDetails.CurrentRow.Index;
            
            if (iRowIndex > -1)
            {
                FrmPayRoll obj = new FrmPayRoll();
                obj.PstrType = "salslip";
                obj.EmpVenFlag = "1";
                sPaymentID = "";

                obj.CompanyID = GetCompanyID(Convert.ToInt32(dgvSalaryDetails.CurrentRow.Cells["PaymentID"].Value));

                dgvSalaryDetails.Columns["PeriodFrom"].Width = 125;
                dgvSalaryDetails.Columns["PeriodTo"].Width = 125;

                mdtFrom = dgvSalaryDetails.Rows[iRowIndex].Cells["PeriodFrom"].Value.ToString();
                mDtTo = dgvSalaryDetails.Rows[iRowIndex].Cells["PeriodTo"].Value.ToString();
                SalaryType(iStype);

                String sYear = "";
                sYear = sYear + DateAndTime.Year(Convert.ToDateTime(dgvSalaryDetails.CurrentRow.Cells["PeriodFrom"].Value));
                String sMonth = "";

                sMonth = Strings.Format(Convert.ToDateTime(dgvSalaryDetails.CurrentRow.Cells["PeriodFrom"].Value), "MMMM");

                obj.PiSelRowCount = dgvSalaryDetails.SelectedRows.Count;
                obj.piYear = Convert.ToInt32(sYear);
                obj.psMonth = sMonth;
                mLLOP = "";
                mHLOP = "";

                sPaymentID = "";
                foreach (DataGridViewRow drPAyId in dgvSalaryDetails.SelectedRows)
                    sPaymentID = sPaymentID + drPAyId.Cells["PaymentID"].Value + ",";

                if (sPaymentID.Trim().Length > 0)
                {
                    if (Strings.Len(sPaymentID) > 5000)
                        sPaymentID = sPaymentID.Substring(0, 5000);

                    sPaymentID = sPaymentID.Substring(0, sPaymentID.LastIndexOf(","));
                    obj.CompanyPayFlag = 0;
                    obj.pPayID = sPaymentID;
                    obj.pLOP = mHLOP;
                    obj.pLblLOP = mLLOP;
                    obj.pMonthlyMonth = iStype;
                    obj.pSType = "";
                    obj.pFormName = "Pay Slip";
                    obj.ShowDialog();
                }
                obj.Dispose();
            }
        }

        private int GetCompanyID(int intPaymentID)
        {
            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos(new string[] { "CompanyID", "PayEmployeePayment", "PaymentID=" + intPaymentID + ""});

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    return Convert.ToInt32(datTemp.Rows[0]["CompanyID"]);
                else
                    return 0;
            }
            else
                return 0;
        }

        private void SalaryType(int sType)
        {
            string mLOP = "";
            switch (sType)
            {
                case 1:
                    mSType = "SALARY SLIP DAILY-";
                    break;
                case 2:
                    mSType = "WEEKLY-" + mdtFrom + "-" + mDtTo;
                    mLLOP = "LOP";
                    mHLOP = Convert.ToString(mLOP);
                    break;
                case 3:
                    mSType = "FORTNIGHT-" + mdtFrom + "-" + mDtTo;
                    mLLOP = "LOP";
                    mHLOP = Convert.ToString(mLOP);
                    break;
                case 4:
                    mSType = "PAYSLIP FOR THE MONTH OF " + Strings.UCase(Strings.Format(Convert.ToDateTime(dgvSalaryDetails.CurrentRow.Cells["PeriodFrom"].Value), "MMMM")) + " " + DateAndTime.Year(Convert.ToDateTime(dgvSalaryDetails.CurrentRow.Cells["PeriodFrom"].Value));
                    mLLOP = "LOP";
                    mHLOP = Convert.ToString(mLOP);
                    break;
            }
        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            if (dgvSalaryDetails.Rows.Count < 1)
                return;

            if (trvSalaryProcessRelease.SelectedNode.Tag == null)
                return;

            if (cboFilterCompany.SelectedIndex == -1)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(9100);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }



            if (cboFilterTransactionType.SelectedIndex == -1)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(1831);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //using (FrmBankTransfer mobj = new FrmBankTransfer())
            //{
            //    mobj.PiCompanyID = Convert.ToInt32(cboFilterCompany.SelectedValue);
            //    mobj.PstrCompanyName = cboFilterCompany.Text.ToString();  
            //    //mobj.PiCurrencyID = Convert.ToInt32(cboFilterCurrency.SelectedValue);
            //    mobj.PstrType = "bankpayment";
            //    mobj.MachineName = GetHostName;
            //    mobj.pFormName = this.Text;
            //    //mobj.PiCurrencyIDFilter = Convert.ToInt32(cboFilterCurrency.SelectedValue);
            //    mobj.PdtProcessedDate = Convert.ToString(trvSalaryProcessRelease.SelectedNode.Text.Trim());
            //    mobj.ShowDialog();
            //}
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (dgvSalaryDetails.Rows.Count > 0)
            {
                for (int i = 0; i <= dgvSalaryDetails.Rows.Count - 1; i++)
                {
                    if (dgvSalaryDetails.Rows[i].ReadOnly == false)
                    {
                        dgvSalaryDetails.Rows[i].Cells["SelectAll"].Value = chkSelectAll.Checked;
                        datTempSalary.Rows[i]["SelectAll"] = chkSelectAll.Checked;
                    }
                }
            }
        }

        private void FillGridColCombo()
        {
            if (intSalaryProcessForm == 2)
            {
                cboRepColForSearch.Items.Clear();

                if (dgvSalaryDetails.ColumnCount > 0)
                {
                    cboRepColForSearch.Items.Add(dgvSalaryDetails.Columns[1].HeaderText);
                    cboRepColForSearch.Items.Add(dgvSalaryDetails.Columns[2].HeaderText);
                    cboRepColForSearch.Items.Add(dgvSalaryDetails.Columns[4].HeaderText);
                    cboRepColForSearch.Items.Add(dgvSalaryDetails.Columns[5].HeaderText);
                    cboRepColForSearch.Items.Add(dgvSalaryDetails.Columns[6].HeaderText);
                    cboRepColForSearch.Items.Add(dgvSalaryDetails.Columns[7].HeaderText);
                    cboRepColForSearch.Items.Add(dgvSalaryDetails.Columns[8].HeaderText);
                    cboRepColForSearch.Items.Add(dgvSalaryDetails.Columns[9].HeaderText);
                    cboRepColForSearch.Items.Add(dgvSalaryDetails.Columns[10].HeaderText);
                    cboRepColForSearch.Items.Add(dgvSalaryDetails.Columns[11].HeaderText);
                    //cboRepColForSearch.Items.Add(dgvSalaryDetails.Columns[12].HeaderText);
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (MViewPermission == false)
                return;

            int i;

            if (txtSearchFor.Text.ToString().Trim() != "")
            {
                switch (Convert.ToInt32(cboRepColForSearch.SelectedIndex))
                {
                    case 0:
                        i = 0;
                        foreach (DataGridViewRow row in dgvSalaryDetails.Rows)
                        {
                            if (row.Cells[1].Value.ToString().Contains(txtSearchFor.Text.ToString()))
                                dgvSalaryDetails.Rows[i].Selected = true;
                            else
                                dgvSalaryDetails.Rows[i].Selected = false;
                            i = i + 1;
                        }
                        break;
                    case 1:
                        i = 0;
                        foreach (DataGridViewRow row in dgvSalaryDetails.Rows)
                        {
                            if (row.Cells[2].Value.ToString().Contains(txtSearchFor.Text.ToString()))
                                dgvSalaryDetails.Rows[i].Selected = true;
                            else
                                dgvSalaryDetails.Rows[i].Selected = false;
                            i = i + 1;
                        }
                        break;
                    case 2:
                        i = 0;
                        foreach (DataGridViewRow row in dgvSalaryDetails.Rows)
                        {
                            if (row.Cells[4].Value.ToString().Trim().Contains(txtSearchFor.Text.ToString().Trim()))
                                dgvSalaryDetails.Rows[i].Selected = true;
                            else
                                dgvSalaryDetails.Rows[i].Selected = false;
                            i = i + 1;
                        }
                        break;
                    case 3:
                        i = 0;
                        foreach (DataGridViewRow row in dgvSalaryDetails.Rows)
                        {
                            if (row.Cells[5].Value.ToString().Contains(txtSearchFor.Text.ToString()))
                                dgvSalaryDetails.Rows[i].Selected = true;
                            else
                                dgvSalaryDetails.Rows[i].Selected = false;
                            i = i + 1;
                        }
                        break;
                    case 4:
                        i = 0;
                        foreach (DataGridViewRow row in dgvSalaryDetails.Rows)
                        {
                            if (row.Cells[6].Value.ToString().Contains(txtSearchFor.Text.ToString()))
                                dgvSalaryDetails.Rows[i].Selected = true;
                            else
                                dgvSalaryDetails.Rows[i].Selected = false;
                            i = i + 1;
                        }
                        break;
                    case 5:
                        i = 0;
                        foreach (DataGridViewRow row in dgvSalaryDetails.Rows)
                        {
                            if (row.Cells[7].Value.ToString().Contains(txtSearchFor.Text.ToString()))
                                dgvSalaryDetails.Rows[i].Selected = true;
                            else
                                dgvSalaryDetails.Rows[i].Selected = false;
                            i = i + 1;
                        }
                        break;
                    case 6:
                        i = 0;
                        foreach (DataGridViewRow row in dgvSalaryDetails.Rows)
                        {
                            if (row.Cells[8].Value.ToString().Contains(txtSearchFor.Text.ToStringCustom()))
                                dgvSalaryDetails.Rows[i].Selected = true;
                            else
                                dgvSalaryDetails.Rows[i].Selected = false;
                            i = i + 1;
                        }
                        break;
                    case 7:
                        i = 0;
                        foreach (DataGridViewRow row in dgvSalaryDetails.Rows)
                        {
                            if (row.Cells[1].Value.ToString().Contains(txtSearchFor.Text.ToStringCustom()))
                                dgvSalaryDetails.Rows[i].Selected = true;
                            else
                                dgvSalaryDetails.Rows[i].Selected = false;
                            i = i + 1;
                        }
                        break;
                    case 8:
                        i = 0;
                        foreach (DataGridViewRow row in dgvSalaryDetails.Rows)
                        {
                            if (row.Cells[1].Value.ToString().Contains(txtSearchFor.Text.ToStringCustom()))
                                dgvSalaryDetails.Rows[i].Selected = true;
                            else
                                dgvSalaryDetails.Rows[i].Selected = false;
                            i = i + 1;
                        }
                        break;
                }
            }
        }

        private void chkAdvanceSearch_CheckedChanged(object sender, EventArgs e)
        {
            getAdvanceSearch();
        }

        private void ComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            intSalaryProcessForm = 1;
            //dgvSalaryDetails.DataSource = null;
            pnlMentioned.Visible = true;
            expPnlSalaryRelease.Visible = false;
            trvSalaryProcessRelease.Visible = false;
            expPnlSalaryProcess.Visible = true;
            this.Text = "Salary Process " + this.Text.Replace("Salary Release ", "");
            btnProcess.Text = "&Process";
            btnProcess.Image = Properties.Resources.Salary_processing;
       
            pnlLeft.Width = 250;

            dgvSalaryDetails.DataSource = getNewTableForProcess();
            datTempSalary = (DataTable)dgvSalaryDetails.DataSource;
            GridColumnDisabled();
            btnBack.Visible = false;
            btnShow_Click(sender, e);
            chkSelectAll.Checked = true;
            chkSelectAll.Checked = false;
        }

        private void btnFirstGridCollection_Click(object sender, EventArgs e)
        {
            try
            {
                FlgBlk = false;
                NavClicked = NavButton.First.ToString();
                fillDataGrid_dtgBrowse();
                lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                FlgBlk = true;
            }
            catch { }
            Cursor = Cursors.Default;
        }

        private void btnPreviousGridCollection_Click(object sender, EventArgs e)
        {
            try
            {
                FlgBlk = false;
                NavClicked = NavButton.Previous.ToString();
                fillDataGrid_dtgBrowse();
                lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                FlgBlk = true;
            }
            catch { }
            Cursor = Cursors.Default;
        }

        private void btnNextGridCollection_Click(object sender, EventArgs e)
        {
            try
            {
                FlgBlk = false;
                NavClicked = NavButton.Next.ToString();
                fillDataGrid_dtgBrowse();
                lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                FlgBlk = true;
            }
            catch { }
            Cursor = Cursors.Default;
        }

        private void btnLastGridCollection_Click(object sender, EventArgs e)
        {
            try
            {
                FlgBlk = false;
                NavClicked = NavButton.Last.ToString();
                fillDataGrid_dtgBrowse();
                lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                FlgBlk = true;
            }
            catch { }
            Cursor = Cursors.Default;
        }

        private void fillDataGrid_dtgBrowse()
        {
            try
            {
                DataTable dt = datTempSalary;

                if (dt == null)
                    return;

                DeterminePageBoundaries();
                int iGrdrowcount = (dt.Rows.Count);

                if (iGrdrowcount >= 24)
                    ItemPanelMiddileBottom.Visible = true;

                if (norecord == false)
                {
                    dgvSalaryDetails.Enabled = true;
                    dt.DefaultView.RowFilter = "RecordID >= " + RecordID1 + " and RecordID <= " + RecordID2;
                    dgvSalaryDetails.DataSource = dt.DefaultView.ToTable();
                    GridColumnDisabled();
                    lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                }
                GridSetUp();
                GridColumnColor();
            }
            catch { }
        }

        private void DeterminePageBoundaries()
        {
            try
            {
                DataTable datPages = datTempSalary;
                int TotalRowCount = datPages.Rows.Count;
                if (TotalRowCount == 0)
                {
                    //MsgBox("No records to Display.")
                    norecord = true;
                    //Exit Sub
                }
                else
                    norecord = false;
                //This is the maximum rows to display on a page. So we want paging to be implemented at 100 rows a page 

                int pages = 0;

                //if the rows per page are less than the total row count do the following: 
                if (pageRows < TotalRowCount)
                {
                    //if the Modulus returns > 0  there should be another page. 
                    if ((TotalRowCount % pageRows) > 0)
                        pages = ((TotalRowCount / pageRows) - ((TotalRowCount % pageRows) / pageRows) + 1);
                    else
                    {
                        //There is nothing left after the Modulus, so the pageRows divide exactly... 
                        //...into TotalRowCount leaving no rest, thus no extra page needs to be addd. 
                        pages = TotalRowCount / pageRows;
                    }
                }
                else
                {
                    //if the rows per page are more than the total row count, we will obviously only ever have 1 page 
                    pages = 1;
                }
                TotalPage = pages;

                //We now need to determine the LowerBoundary and UpperBoundary in order to correctly... 
                //...determine the Correct RecordID//s to use in the bindingSource1.Filter property. 
                int LowerBoundary = 0;
                int UpperBoundary = 0;

                //We now need to know what button was clicked, if any (First, Last, Next, Previous) 
                switch (NavClicked)
                {
                    case "First":
                        {
                            //First clicked, the Current Page will always be 1 
                            CurrentPage = 1;
                            //The LowerBoundary will thus be ((50 * 1) - (50 - 1)) = 1 
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            //if the rows per page are less than the total row count do the following: 
                            if (pageRows < TotalRowCount)
                                UpperBoundary = (pageRows * CurrentPage);
                            else
                            {
                                //if the rows per page are more than the total row count do the following: 
                                //There is only one page, so get the total row count as an UpperBoundary 
                                UpperBoundary = TotalRowCount;
                            }

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                    case "Last":
                        {
                            //Last clicked, the CurrentPage will always be = to the variable pages 
                            CurrentPage = pages;
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));
                            //The UpperBoundary will always be the sum total of all the rows 
                            UpperBoundary = TotalRowCount;
                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                    case "Next":
                        {
                            //Next clicked 
                            if (CurrentPage != pages)
                            {
                                //if we arent on the last page already, add another page 
                                CurrentPage += 1;
                            }

                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            if (CurrentPage == pages)
                            {
                                //if we are on the last page, the UpperBoundary will always be the sum total of all the rows 
                                UpperBoundary = TotalRowCount;
                            }
                            else
                            {
                                //else if we have a pageRow of 50 and we are on page 3, the UpperBoundary = 150 
                                UpperBoundary = (pageRows * CurrentPage);
                            }

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }

                            break;
                        }
                    case "Previous":
                        {
                            //Previous clicked 
                            if (CurrentPage != 1)
                            {
                                //if we aren//t on the first page already, subtract 1 from the CurrentPage 
                                CurrentPage -= 1;
                            }
                            //Get the LowerBoundary 
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            if (pageRows < TotalRowCount)
                                UpperBoundary = (pageRows * CurrentPage);
                            else
                                UpperBoundary = TotalRowCount;

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                    case "1": //25
                    case "2": //50
                    case "3": //100
                        {
                            //First clicked, the Current Page will always be 1 
                            CurrentPage = 1;
                            //The LowerBoundary will thus be ((50 * 1) - (50 - 1)) = 1 
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            //if the rows per page are less than the total row count do the following: 
                            if (pageRows < TotalRowCount)
                                UpperBoundary = (pageRows * CurrentPage);
                            else
                            {
                                //if the rows per page are more than the total row count do the following: 
                                //There is only one page, so get the total row count as an UpperBoundary 
                                UpperBoundary = TotalRowCount;
                            }

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                    case "0": // All
                        {
                            //First clicked, the Current Page will always be 1 
                            CurrentPage = 1;
                            //The LowerBoundary will thus be ((50 * 1) - (50 - 1)) = 1 
                            LowerBoundary = 1;
                            UpperBoundary = TotalRowCount;

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }

                            break;
                        }
                    default:
                        {
                            //No button was clicked. 
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            //if the rows per page are less than the total row count do the following: 
                            if (pageRows < TotalRowCount)
                                UpperBoundary = (pageRows * CurrentPage);
                            else
                            {
                                //if the rows per page are more than the total row count do the following: 
                                //Therefore there is only one page, so get the total row count as an UpperBoundary 
                                UpperBoundary = TotalRowCount;
                            }

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                }
            }
            catch { }
        }

        private void cboCountItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            FlgBlk = false;

            if (cboCountItem.SelectedIndex > 0)
                pageRows = Convert.ToInt32(cboCountItem.Text);
            else
                pageRows = datTempSalary.Rows.Count;

            NavClicked = cboCountItem.SelectedIndex.ToString();
            fillDataGrid_dtgBrowse();
            lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
            FlgBlk = true;
        }

        private void BtnSingleRow_Click(object sender, EventArgs e)
        {
            try
            {
                bool bTempFlg = false;
                if (intSalaryProcessForm == 2)
                {
                    bTempFlg = mobjclsBLLSalaryProcessAndRelease.DeleteSingleRow(Convert.ToDouble(dgvSalaryDetails.CurrentRow.Cells["PaymentID"].Value));
                    if (bTempFlg == true)
                    {
                        dgvSalaryDetails.Rows.Remove(dgvSalaryDetails.CurrentRow);
                    }
                    else
                    {
                        MstrCommonMessage = UserMessage.GetMessageByCode(9017);
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
            }
            catch { }
        }

        private void btnSIF_Click(object sender, EventArgs e)
        {
            try
            {
                if (intSalaryProcessForm == 2)
                {
                    using (FrmSIF objsif = new FrmSIF())
                    {
                        //objsif.PCompanyName = cboFilterCompany.Text;
                        //objsif.PCompanyID = Convert.ToInt32(cboFilterCompany.SelectedValue);
                        objsif.ShowDialog();
                    }
                }
            }
            catch
            {
            }
        }

        private void btnSalarySlipCompany_Click(object sender, EventArgs e)
        {

            string sPaymentID;
            int iStype = 0;

            if (dgvSalaryDetails.RowCount == 0)
                return;

            int iRowIndex = dgvSalaryDetails.CurrentRow.Index;

            if (iRowIndex > -1)
            {
                FrmPayRoll obj = new FrmPayRoll();
                obj.PstrType = "salslip";
                obj.EmpVenFlag = "1";
                sPaymentID = "";

                obj.CompanyID = GetCompanyID(Convert.ToInt32(dgvSalaryDetails.CurrentRow.Cells["PaymentID"].Value));

                dgvSalaryDetails.Columns["PeriodFrom"].Width = 125;
                dgvSalaryDetails.Columns["PeriodTo"].Width = 125;

                mdtFrom = dgvSalaryDetails.Rows[iRowIndex].Cells["PeriodFrom"].Value.ToString();
                mDtTo = dgvSalaryDetails.Rows[iRowIndex].Cells["PeriodTo"].Value.ToString();
                SalaryType(iStype);

                String sYear = "";
                sYear = sYear + DateAndTime.Year(Convert.ToDateTime(dgvSalaryDetails.CurrentRow.Cells["PeriodFrom"].Value));
                String sMonth = "";

                sMonth = Strings.Format(Convert.ToDateTime(dgvSalaryDetails.CurrentRow.Cells["PeriodFrom"].Value), "MMMM");

                obj.PiSelRowCount = dgvSalaryDetails.SelectedRows.Count;
                obj.piYear = Convert.ToInt32(sYear);
                obj.psMonth = sMonth;
                mLLOP = "";
                mHLOP = "";

                sPaymentID = "";
                foreach (DataGridViewRow drPAyId in dgvSalaryDetails.SelectedRows)
                    sPaymentID = sPaymentID + drPAyId.Cells["PaymentID"].Value + ",";

                if (sPaymentID.Trim().Length > 0)
                {
                    if (Strings.Len(sPaymentID) > 5000)
                        sPaymentID = sPaymentID.Substring(0, 5000);

                    sPaymentID = sPaymentID.Substring(0, sPaymentID.LastIndexOf(","));
                    obj.pPayID = sPaymentID;
                    obj.CompanyPayFlag = 1;
                    obj.pLOP = mHLOP;
                    obj.pLblLOP = mLLOP;
                    obj.pMonthlyMonth = iStype;
                    obj.pSType = "";
                    obj.pFormName = "Pay Slip";
                    obj.ShowDialog();
                }
                obj.Dispose();
            }



        }

        private void dgvSalaryDetails_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (intSalaryProcessForm == 2)
                {
                    if (e.Button == MouseButtons.Right)
                    {
                        if (e.RowIndex >= 0)
                        {
                            if (CheckReleased(Convert.ToInt32(dgvSalaryDetails.Rows[e.RowIndex].Cells["PaymentID"].Value)) == false)
                                this.DeleteSingleRowContextMenuStrip.Show(this.dgvSalaryDetails, this.dgvSalaryDetails.PointToClient(Cursor.Position));
                        }
                    }
                }
            }
            catch { }
        }

        private void btnAddPariculars_Click(object sender, EventArgs e)
        {

            if (strProcessDate != null)
            {
                using (FrmAddParicularsToAll objFrmAddAdditionDeduction = new FrmAddParicularsToAll())
                {
                    objFrmAddAdditionDeduction.strProcessDate = strProcessDate;
                    objFrmAddAdditionDeduction.intCompanyID = cboFilterCompany.SelectedValue.ToInt32();
                    objFrmAddAdditionDeduction.ShowDialog();
                }
            }
        }

        private void cboDepartmentFil_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboDepartmentFil.SelectedIndex >= 0)
                PDepartmentID = cboDepartmentFil.SelectedValue.ToInt32();
            else if (cboDepartmentFil.SelectedIndex == -1)
                PDepartmentID = 0;
            FillSalaryInfogrid(strProcessDate);

        }

        private void cboDesignationFil_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboDesignationFil.SelectedIndex >= 0)
                PDesignationID = cboDesignationFil.SelectedValue.ToInt32();
            else if (cboDesignationFil.SelectedIndex == -1)
                PDesignationID = 0;
            FillSalaryInfogrid(strProcessDate);

        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            if (intSalaryProcessForm == 1)
            {

                objHelp.strFormName = "Salary Process";
            }
            else
                objHelp.strFormName = "Salary Release";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void FrmSalaryProcessRelease_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                        if (intSalaryProcessForm == 1)
                        {

                            objHelp.strFormName = "Salary Process";
                        }
                        else
                            objHelp.strFormName = "Salary Release";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.P:
                        btnPrint_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.M:
                        btnEmail_Click(sender, new EventArgs());
                        break;
                }
            }
            catch (Exception)
            {
            }
        }


        

        private void dgvSalaryDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void expandableSplitter1_ExpandedChanged(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {
            PointSelectAll();
        }

        private void FrmSalaryProcessRelease_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void FrmSalaryProcessRelease_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void FrmSalaryProcessRelease_Deactivate(object sender, EventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

  
        

       

    }
}