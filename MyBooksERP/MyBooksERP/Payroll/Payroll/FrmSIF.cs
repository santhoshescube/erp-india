﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmSIF : Form
    {
        clsBLLSalaryProcessAndRelease MobjclsBLLSalaryProcessAndRelease;
        private clsMessage ObjUserMessage = null;
        public int PCompanyID;
        //public string PCompanyName;
        string MstrCommonMessage;
        string MstrMessageCaption = ClsCommonSettings.MessageCaption;
        public FrmSIF()
        {
            InitializeComponent();
            MobjclsBLLSalaryProcessAndRelease = new clsBLLSalaryProcessAndRelease();
            
           
        }
       
        private void DtpCurrentMonth_ValueChanged(object sender, EventArgs e)
        {
            int iMonth = DtpCurrentMonth.Value.Date.Month;
            int iYear = DtpCurrentMonth.Value.Date.Year;
        }
        private string GetCurrentMonthStr(int MonthVal)
        {
            string Months;
            Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;

            }


            return Months;
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            string strPath = "";
            int IsVisa =0 ;

            if (cboBank.SelectedIndex==-1)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(15);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }


            if (cboCompany.SelectedIndex == -1)
            {
                return;
            }
            else
            {
                PCompanyID = cboCompany.SelectedValue.ToInt32();   
            }
            if (rtbVisaCompany.Checked)
            {
                IsVisa = 1;
            }
            else
            {
                rtbVisaCompany.Checked = false; 
                IsVisa = 0;
            }

            saveFileDialog1.ShowDialog();
            strPath = saveFileDialog1.SelectedPath.ToString();  

            if (strPath !="")
            {
                MobjclsBLLSalaryProcessAndRelease.InsertIntoFileTables(DtpCurrentMonth.Value.ToString("dd-MMM-yyyy"), strPath, PCompanyID, cboBank.SelectedValue.ToInt32(), IsVisa);
            }

            this.Close(); 

        }
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.SalaryRelease);
                return this.ObjUserMessage;
            }
        }
        private void FrmSIF_Load(object sender, EventArgs e)
        {
            try
            {
                rtbVisaCompany.Checked = true;
                rtbWorkingCompany.Checked = true;
                LoadCombo(0);
            }
            catch
            {
            }
        }
        private void LoadCombo(int TypeID)
        {
            try
            {

                //rtbWorkingCompany.Checked = true;
                if (TypeID == 0 || TypeID == 1)
                {
                    DataTable datTempBank = new DataTable();
                    datTempBank.Columns.Add("BankID");
                    datTempBank.Columns.Add("Bank");

                    if (PCompanyID >= 0)
                    {
                        if (rtbWorkingCompany.Checked)
                        {
                            datTempBank = MobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(26, PCompanyID); // Get Bank
                        }
                        else
                        {
                            datTempBank = MobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(26, 0); // Get Bank
                        }

                    }
                    cboBank.ValueMember = "BankID";
                    cboBank.DisplayMember = "Bank";
                    cboBank.DataSource = datTempBank;
                    cboBank.Text = "";
                    cboBank.SelectedIndex = -1;
                }
                if (TypeID == 0 || TypeID == 2)
                {
                    //DataTable datTemp = MobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(1, 0); // Get Company
                    //cboCompany.ValueMember = "CompanyID";
                    //cboCompany.DisplayMember = "CompanyName";
                    //cboCompany.DataSource = datTemp;


                    DataTable datTemp = MobjclsBLLSalaryProcessAndRelease.FillCombos(new string[] {"CompanyID,CompanyName", "CompanyMaster" ,""}); 
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datTemp;


                }

            }
            catch
            {
            }
        }

        private void cboBank_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                cboBank.DroppedDown = false; 
            }
            catch
            {
            }
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboCompany.SelectedIndex != -1)
                {
                    PCompanyID = Convert.ToInt32(cboCompany.SelectedValue.ToInt32());    
                }

                LoadCombo(1); LoadCombo(1);
            }
            catch
            {
            }
        }

        private void rtbVisaCompany_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                LoadCombo(1);
            }
            catch
            {
            }
        }

       

        
    }
}
