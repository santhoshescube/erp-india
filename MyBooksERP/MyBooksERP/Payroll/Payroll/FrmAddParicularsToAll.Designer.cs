﻿namespace MyBooksERP
{
    partial class FrmAddParicularsToAll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAddParicularsToAll));
            this.ProjectCreationStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.DeductionPolicyStatusStrip = new System.Windows.Forms.StatusStrip();
            this.btnSave = new System.Windows.Forms.Button();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.txtPercentage = new System.Windows.Forms.TextBox();
            this.cmsApplyToAll = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItemCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.rbtnCarryforward = new System.Windows.Forms.RadioButton();
            this.lblDepartment = new System.Windows.Forms.Label();
            this.cboDepartment = new System.Windows.Forms.ComboBox();
            this.btnAddDed = new System.Windows.Forms.Button();
            this.cboAdditionsDeductions = new System.Windows.Forms.ComboBox();
            this.rbtDeduction = new System.Windows.Forms.RadioButton();
            this.rbtAddition = new System.Windows.Forms.RadioButton();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.dgvEmployeeDetails = new System.Windows.Forms.DataGridView();
            this.errAddParicularsToAll = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.clmnEmployeeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnTotalAdditions = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnTotalDeduction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnNetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnEmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnAddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnPaymentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnCurrencyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeductionPolicyStatusStrip.SuspendLayout();
            this.Panel1.SuspendLayout();
            this.cmsApplyToAll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployeeDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errAddParicularsToAll)).BeginInit();
            this.SuspendLayout();
            // 
            // ProjectCreationStatusLabel
            // 
            this.ProjectCreationStatusLabel.Name = "ProjectCreationStatusLabel";
            this.ProjectCreationStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 17);
            this.lblStatus.Text = "Status";
            // 
            // DeductionPolicyStatusStrip
            // 
            this.DeductionPolicyStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.ProjectCreationStatusLabel});
            this.DeductionPolicyStatusStrip.Location = new System.Drawing.Point(0, 474);
            this.DeductionPolicyStatusStrip.Name = "DeductionPolicyStatusStrip";
            this.DeductionPolicyStatusStrip.Size = new System.Drawing.Size(664, 22);
            this.DeductionPolicyStatusStrip.TabIndex = 1016;
            this.DeductionPolicyStatusStrip.Text = "StatusStrip1";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(10, 442);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1015;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // Panel1
            // 
            this.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel1.Controls.Add(this.txtPercentage);
            this.Panel1.Controls.Add(this.Label2);
            this.Panel1.Controls.Add(this.Label1);
            this.Panel1.Controls.Add(this.rbtnCarryforward);
            this.Panel1.Controls.Add(this.lblDepartment);
            this.Panel1.Controls.Add(this.cboDepartment);
            this.Panel1.Controls.Add(this.btnAddDed);
            this.Panel1.Controls.Add(this.cboAdditionsDeductions);
            this.Panel1.Controls.Add(this.rbtDeduction);
            this.Panel1.Controls.Add(this.rbtAddition);
            this.Panel1.Location = new System.Drawing.Point(10, 4);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(640, 96);
            this.Panel1.TabIndex = 1012;
            // 
            // txtPercentage
            // 
            this.txtPercentage.ContextMenuStrip = this.cmsApplyToAll;
            this.txtPercentage.Location = new System.Drawing.Point(546, 11);
            this.txtPercentage.MaxLength = 3;
            this.txtPercentage.Name = "txtPercentage";
            this.txtPercentage.Size = new System.Drawing.Size(78, 20);
            this.txtPercentage.TabIndex = 10;
            this.txtPercentage.TextChanged += new System.EventHandler(this.txtPercentage_TextChanged);
            this.txtPercentage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPercentage_KeyPress);
            // 
            // cmsApplyToAll
            // 
            this.cmsApplyToAll.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemCopy});
            this.cmsApplyToAll.Name = "CopyDetailsContextMenuStrip";
            this.cmsApplyToAll.Size = new System.Drawing.Size(140, 26);
            // 
            // ToolStripMenuItemCopy
            // 
            this.ToolStripMenuItemCopy.Name = "ToolStripMenuItemCopy";
            this.ToolStripMenuItemCopy.Size = new System.Drawing.Size(139, 22);
            this.ToolStripMenuItemCopy.Text = "Apply To All";
            this.ToolStripMenuItemCopy.Click += new System.EventHandler(this.ToolStripMenuItemCopy_Click);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(400, 16);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(140, 13);
            this.Label2.TabIndex = 9;
            this.Label2.Text = "Percentage of gross amount";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(8, 59);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(56, 13);
            this.Label1.TabIndex = 8;
            this.Label1.Text = "Particulars";
            // 
            // rbtnCarryforward
            // 
            this.rbtnCarryforward.AutoSize = true;
            this.rbtnCarryforward.Location = new System.Drawing.Point(196, 14);
            this.rbtnCarryforward.Name = "rbtnCarryforward";
            this.rbtnCarryforward.Size = new System.Drawing.Size(129, 17);
            this.rbtnCarryforward.TabIndex = 7;
            this.rbtnCarryforward.TabStop = true;
            this.rbtnCarryforward.Text = "Carry-Forward Amount";
            this.rbtnCarryforward.UseVisualStyleBackColor = true;
            this.rbtnCarryforward.CheckedChanged += new System.EventHandler(this.rbtnCarryforward_CheckedChanged);
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            this.lblDepartment.Location = new System.Drawing.Point(340, 59);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(62, 13);
            this.lblDepartment.TabIndex = 6;
            this.lblDepartment.Text = "Department";
            // 
            // cboDepartment
            // 
            this.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDepartment.FormattingEnabled = true;
            this.cboDepartment.Location = new System.Drawing.Point(425, 56);
            this.cboDepartment.Name = "cboDepartment";
            this.cboDepartment.Size = new System.Drawing.Size(199, 21);
            this.cboDepartment.TabIndex = 5;
            this.cboDepartment.SelectedIndexChanged += new System.EventHandler(this.cboDepartment_SelectedIndexChanged);
            // 
            // btnAddDed
            // 
            this.btnAddDed.Location = new System.Drawing.Point(286, 54);
            this.btnAddDed.Name = "btnAddDed";
            this.btnAddDed.Size = new System.Drawing.Size(32, 23);
            this.btnAddDed.TabIndex = 4;
            this.btnAddDed.Text = "...";
            this.btnAddDed.UseVisualStyleBackColor = true;
            this.btnAddDed.Click += new System.EventHandler(this.btnAddDed_Click);
            // 
            // cboAdditionsDeductions
            // 
            this.cboAdditionsDeductions.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAdditionsDeductions.FormattingEnabled = true;
            this.cboAdditionsDeductions.Location = new System.Drawing.Point(81, 56);
            this.cboAdditionsDeductions.Name = "cboAdditionsDeductions";
            this.cboAdditionsDeductions.Size = new System.Drawing.Size(199, 21);
            this.cboAdditionsDeductions.TabIndex = 1;
            this.cboAdditionsDeductions.SelectedIndexChanged += new System.EventHandler(this.cboAdditionsDeductions_SelectedIndexChanged);
            // 
            // rbtDeduction
            // 
            this.rbtDeduction.AutoSize = true;
            this.rbtDeduction.Location = new System.Drawing.Point(98, 14);
            this.rbtDeduction.Name = "rbtDeduction";
            this.rbtDeduction.Size = new System.Drawing.Size(74, 17);
            this.rbtDeduction.TabIndex = 3;
            this.rbtDeduction.TabStop = true;
            this.rbtDeduction.Text = "Deduction";
            this.rbtDeduction.UseVisualStyleBackColor = true;
            this.rbtDeduction.CheckedChanged += new System.EventHandler(this.rbtDeduction_CheckedChanged);
            // 
            // rbtAddition
            // 
            this.rbtAddition.AutoSize = true;
            this.rbtAddition.Checked = true;
            this.rbtAddition.Location = new System.Drawing.Point(11, 14);
            this.rbtAddition.Name = "rbtAddition";
            this.rbtAddition.Size = new System.Drawing.Size(63, 17);
            this.rbtAddition.TabIndex = 2;
            this.rbtAddition.TabStop = true;
            this.rbtAddition.Text = "Addition";
            this.rbtAddition.UseVisualStyleBackColor = true;
            this.rbtAddition.CheckedChanged += new System.EventHandler(this.rbtAddition_CheckedChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(575, 442);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1014;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(494, 442);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1013;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // dgvEmployeeDetails
            // 
            this.dgvEmployeeDetails.AllowUserToAddRows = false;
            this.dgvEmployeeDetails.AllowUserToResizeRows = false;
            this.dgvEmployeeDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvEmployeeDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployeeDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmnEmployeeName,
            this.clmnTotalAdditions,
            this.clmnTotalDeduction,
            this.clmnNetAmount,
            this.clmnAmount,
            this.clmnEmployeeID,
            this.clmnAddDedID,
            this.clmnPaymentID,
            this.clmnCurrencyID});
            this.dgvEmployeeDetails.Location = new System.Drawing.Point(10, 106);
            this.dgvEmployeeDetails.Name = "dgvEmployeeDetails";
            this.dgvEmployeeDetails.Size = new System.Drawing.Size(640, 319);
            this.dgvEmployeeDetails.TabIndex = 1017;
            this.dgvEmployeeDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEmployeeDetails_CellValueChanged);
            this.dgvEmployeeDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvEmployeeDetails_CellBeginEdit);
            this.dgvEmployeeDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvEmployeeDetails_EditingControlShowing);
            this.dgvEmployeeDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvEmployeeDetails_CurrentCellDirtyStateChanged);
            // 
            // errAddParicularsToAll
            // 
            this.errAddParicularsToAll.ContainerControl = this;
            this.errAddParicularsToAll.RightToLeft = true;
            // 
            // clmnEmployeeName
            // 
            this.clmnEmployeeName.HeaderText = "Employee Name";
            this.clmnEmployeeName.Name = "clmnEmployeeName";
            this.clmnEmployeeName.Width = 245;
            // 
            // clmnTotalAdditions
            // 
            this.clmnTotalAdditions.HeaderText = "Total Additions";
            this.clmnTotalAdditions.Name = "clmnTotalAdditions";
            this.clmnTotalAdditions.ReadOnly = true;
            this.clmnTotalAdditions.Width = 90;
            // 
            // clmnTotalDeduction
            // 
            this.clmnTotalDeduction.HeaderText = "Total Deduction";
            this.clmnTotalDeduction.Name = "clmnTotalDeduction";
            // 
            // clmnNetAmount
            // 
            this.clmnNetAmount.HeaderText = "Net Amount";
            this.clmnNetAmount.Name = "clmnNetAmount";
            // 
            // clmnAmount
            // 
            this.clmnAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmnAmount.ContextMenuStrip = this.cmsApplyToAll;
            this.clmnAmount.HeaderText = "Amount";
            this.clmnAmount.MaxInputLength = 12;
            this.clmnAmount.Name = "clmnAmount";
            // 
            // clmnEmployeeID
            // 
            this.clmnEmployeeID.HeaderText = "clmnEmployeeID";
            this.clmnEmployeeID.Name = "clmnEmployeeID";
            this.clmnEmployeeID.Visible = false;
            // 
            // clmnAddDedID
            // 
            this.clmnAddDedID.HeaderText = "clmnAddDedID";
            this.clmnAddDedID.Name = "clmnAddDedID";
            this.clmnAddDedID.Visible = false;
            // 
            // clmnPaymentID
            // 
            this.clmnPaymentID.HeaderText = "clmnPaymentID";
            this.clmnPaymentID.Name = "clmnPaymentID";
            this.clmnPaymentID.Visible = false;
            // 
            // clmnCurrencyID
            // 
            this.clmnCurrencyID.HeaderText = "clmnCurrencyID";
            this.clmnCurrencyID.Name = "clmnCurrencyID";
            this.clmnCurrencyID.Visible = false;
            // 
            // FrmAddParicularsToAll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 496);
            this.Controls.Add(this.dgvEmployeeDetails);
            this.Controls.Add(this.DeductionPolicyStatusStrip);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAddParicularsToAll";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Pariculars To All";
            this.Load += new System.EventHandler(this.FrmAddParicularsToAll_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAddParicularsToAll_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmAddParicularsToAll_KeyDown);
            this.DeductionPolicyStatusStrip.ResumeLayout(false);
            this.DeductionPolicyStatusStrip.PerformLayout();
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.cmsApplyToAll.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployeeDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errAddParicularsToAll)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

   
        internal System.Windows.Forms.ToolStripStatusLabel ProjectCreationStatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.StatusStrip DeductionPolicyStatusStrip;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.TextBox txtPercentage;
        internal System.Windows.Forms.ContextMenuStrip cmsApplyToAll;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemCopy;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.RadioButton rbtnCarryforward;
        internal System.Windows.Forms.Label lblDepartment;
        internal System.Windows.Forms.ComboBox cboDepartment;
        internal System.Windows.Forms.Button btnAddDed;
        internal System.Windows.Forms.ComboBox cboAdditionsDeductions;
        internal System.Windows.Forms.RadioButton rbtDeduction;
        internal System.Windows.Forms.RadioButton rbtAddition;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.DataGridView dgvEmployeeDetails;
        private System.Windows.Forms.ErrorProvider errAddParicularsToAll;
        private System.Windows.Forms.Timer tmrClear;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnEmployeeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnTotalAdditions;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnTotalDeduction;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnNetAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnEmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnAddDedID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnPaymentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnCurrencyID;
    }
}