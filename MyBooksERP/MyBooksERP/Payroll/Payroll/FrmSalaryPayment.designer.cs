﻿namespace MyBooksERP
{
    partial class FrmSalaryPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label PaymentIDLabel;
            System.Windows.Forms.Label Label12;
            System.Windows.Forms.Label Label11;
            System.Windows.Forms.Label Label2;
            System.Windows.Forms.Label Label1;
            System.Windows.Forms.Label Label5;
            System.Windows.Forms.Label Label4;
            System.Windows.Forms.Label Label7;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSalaryPayment));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.WebBrowser1 = new System.Windows.Forms.WebBrowser();
            this.DebitAmtTextBox = new System.Windows.Forms.TextBox();
            this.pnlAmount = new System.Windows.Forms.Panel();
            this.GrdAmount = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Particular = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreditAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DebitAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Addition1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddDedIDD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BindingNavigatorRelease = new System.Windows.Forms.BindingNavigator(this.components);
            this.WordReportButton = new System.Windows.Forms.ToolStripButton();
            this.CreditAmtTextBox = new System.Windows.Forms.TextBox();
            this.GroupBox4 = new System.Windows.Forms.GroupBox();
            this.dtpToday = new System.Windows.Forms.DateTimePicker();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.CreditGroupTextBox = new System.Windows.Forms.TextBox();
            this.DebitGroupTextBox = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.TotalTextBox = new System.Windows.Forms.TextBox();
            this.BindingNavigatorSplit = new System.Windows.Forms.BindingNavigator(this.components);
            this.TabPageAmountSplit = new System.Windows.Forms.TabPage();
            this.Label14 = new System.Windows.Forms.Label();
            this.NewAddDedButton = new System.Windows.Forms.Button();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.Label13 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.lblTransactionType = new System.Windows.Forms.Label();
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.EmployeeLabel = new System.Windows.Forms.Label();
            this.TabPageRelease = new System.Windows.Forms.TabPage();
            this.BtnConfirm = new System.Windows.Forms.Button();
            this.CancelCButton = new System.Windows.Forms.Button();
            this.AddBtn = new System.Windows.Forms.Button();
            this.TabControlSal = new System.Windows.Forms.TabControl();
            this.TabPageSal = new System.Windows.Forms.TabPage();
            this.EmployeePaymentBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAccSettings = new System.Windows.Forms.ToolStripButton();
            this.EmployeePaymentBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BtEmail = new System.Windows.Forms.ToolStripButton();
            this.HelpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.EmployeePaymentDetailDataGridView = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.PaymentDetailID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PaymentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoanId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Addition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmpVenFlag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADDFLAG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Particulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Earnings = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Deductions = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsEditable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShortDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmpCurrencyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetAmtTextBox = new System.Windows.Forms.TextBox();
            this.CreditTextBox = new System.Windows.Forms.TextBox();
            this.RemarksTextBox = new System.Windows.Forms.TextBox();
            this.DebitTextBox = new System.Windows.Forms.TextBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.AmountTextBox = new System.Windows.Forms.TextBox();
            this.AdditionDeductionComboBox = new System.Windows.Forms.ComboBox();
            this.TimerForRpt = new System.Windows.Forms.Timer(this.components);
            this.ErrorProviderRel = new System.Windows.Forms.ErrorProvider(this.components);
            this.TextboxNumeric = new System.Windows.Forms.TextBox();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.ErrorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.PaymentIDTextBox = new System.Windows.Forms.TextBox();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            PaymentIDLabel = new System.Windows.Forms.Label();
            Label12 = new System.Windows.Forms.Label();
            Label11 = new System.Windows.Forms.Label();
            Label2 = new System.Windows.Forms.Label();
            Label1 = new System.Windows.Forms.Label();
            Label5 = new System.Windows.Forms.Label();
            Label4 = new System.Windows.Forms.Label();
            Label7 = new System.Windows.Forms.Label();
            this.Panel3.SuspendLayout();
            this.pnlAmount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigatorRelease)).BeginInit();
            this.BindingNavigatorRelease.SuspendLayout();
            this.GroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigatorSplit)).BeginInit();
            this.BindingNavigatorSplit.SuspendLayout();
            this.TabPageAmountSplit.SuspendLayout();
            this.Panel1.SuspendLayout();
            this.TabPageRelease.SuspendLayout();
            this.TabControlSal.SuspendLayout();
            this.TabPageSal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeePaymentBindingNavigator)).BeginInit();
            this.EmployeePaymentBindingNavigator.SuspendLayout();
            this.Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeePaymentDetailDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderRel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider1)).BeginInit();
            this.StatusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // PaymentIDLabel
            // 
            PaymentIDLabel.AutoSize = true;
            PaymentIDLabel.Location = new System.Drawing.Point(787, 34);
            PaymentIDLabel.Name = "PaymentIDLabel";
            PaymentIDLabel.Size = new System.Drawing.Size(65, 13);
            PaymentIDLabel.TabIndex = 136;
            PaymentIDLabel.Text = "Payment ID:";
            // 
            // Label12
            // 
            Label12.AutoSize = true;
            Label12.Location = new System.Drawing.Point(290, 292);
            Label12.Name = "Label12";
            Label12.Size = new System.Drawing.Size(31, 13);
            Label12.TabIndex = 152;
            Label12.Text = "Total";
            // 
            // Label11
            // 
            Label11.AutoSize = true;
            Label11.Location = new System.Drawing.Point(6, 12);
            Label11.Name = "Label11";
            Label11.Size = new System.Drawing.Size(74, 13);
            Label11.TabIndex = 155;
            Label11.Text = "Payment Date";
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Location = new System.Drawing.Point(7, 34);
            Label2.Name = "Label2";
            Label2.Size = new System.Drawing.Size(49, 13);
            Label2.TabIndex = 166;
            Label2.Text = "Currency";
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new System.Drawing.Point(421, 9);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(90, 13);
            Label1.TabIndex = 165;
            Label1.Text = "Transaction Type";
            // 
            // Label5
            // 
            Label5.AutoSize = true;
            Label5.Location = new System.Drawing.Point(349, 11);
            Label5.Name = "Label5";
            Label5.Size = new System.Drawing.Size(43, 13);
            Label5.TabIndex = 144;
            Label5.Text = "Amount";
            // 
            // Label4
            // 
            Label4.AutoSize = true;
            Label4.Location = new System.Drawing.Point(7, 11);
            Label4.Name = "Label4";
            Label4.Size = new System.Drawing.Size(99, 13);
            Label4.TabIndex = 141;
            Label4.Text = "Addition/Deduction";
            // 
            // Label7
            // 
            Label7.AutoSize = true;
            Label7.Location = new System.Drawing.Point(198, 312);
            Label7.Name = "Label7";
            Label7.Size = new System.Drawing.Size(31, 13);
            Label7.TabIndex = 149;
            Label7.Text = "Total";
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.ToolTipText = "Print Salary Details";
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.Transparent;
            this.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel3.Controls.Add(this.WebBrowser1);
            this.Panel3.Location = new System.Drawing.Point(3, 31);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(603, 403);
            this.Panel3.TabIndex = 166;
            // 
            // WebBrowser1
            // 
            this.WebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WebBrowser1.Location = new System.Drawing.Point(0, 0);
            this.WebBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.WebBrowser1.Name = "WebBrowser1";
            this.WebBrowser1.Size = new System.Drawing.Size(601, 401);
            this.WebBrowser1.TabIndex = 1;
            // 
            // DebitAmtTextBox
            // 
            this.DebitAmtTextBox.Location = new System.Drawing.Point(457, 288);
            this.DebitAmtTextBox.Name = "DebitAmtTextBox";
            this.DebitAmtTextBox.ReadOnly = true;
            this.DebitAmtTextBox.Size = new System.Drawing.Size(131, 20);
            this.DebitAmtTextBox.TabIndex = 150;
            this.DebitAmtTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // pnlAmount
            // 
            this.pnlAmount.Controls.Add(this.GrdAmount);
            this.pnlAmount.Controls.Add(this.BindingNavigatorRelease);
            this.pnlAmount.Controls.Add(Label12);
            this.pnlAmount.Controls.Add(this.DebitAmtTextBox);
            this.pnlAmount.Controls.Add(this.CreditAmtTextBox);
            this.pnlAmount.Controls.Add(this.GroupBox4);
            this.pnlAmount.Location = new System.Drawing.Point(2, 3);
            this.pnlAmount.Name = "pnlAmount";
            this.pnlAmount.Size = new System.Drawing.Size(606, 404);
            this.pnlAmount.TabIndex = 153;
            // 
            // GrdAmount
            // 
            this.GrdAmount.AllowUserToAddRows = false;
            this.GrdAmount.AllowUserToResizeRows = false;
            this.GrdAmount.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdAmount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GrdAmount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrdAmount.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Particular,
            this.CreditAmt,
            this.DebitAmt,
            this.Addition1,
            this.AddDedIDD});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GrdAmount.DefaultCellStyle = dataGridViewCellStyle2;
            this.GrdAmount.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.GrdAmount.Location = new System.Drawing.Point(3, 3);
            this.GrdAmount.Name = "GrdAmount";
            this.GrdAmount.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdAmount.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.GrdAmount.RowHeadersVisible = false;
            this.GrdAmount.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GrdAmount.Size = new System.Drawing.Size(600, 282);
            this.GrdAmount.TabIndex = 169;
            // 
            // Particular
            // 
            this.Particular.HeaderText = "Particular";
            this.Particular.Name = "Particular";
            this.Particular.ReadOnly = true;
            this.Particular.Width = 350;
            // 
            // CreditAmt
            // 
            this.CreditAmt.HeaderText = "CreditAmt";
            this.CreditAmt.Name = "CreditAmt";
            this.CreditAmt.ReadOnly = true;
            this.CreditAmt.Width = 126;
            // 
            // DebitAmt
            // 
            this.DebitAmt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DebitAmt.HeaderText = "DebitAmt";
            this.DebitAmt.Name = "DebitAmt";
            this.DebitAmt.ReadOnly = true;
            // 
            // Addition1
            // 
            this.Addition1.HeaderText = "Addition1";
            this.Addition1.Name = "Addition1";
            this.Addition1.ReadOnly = true;
            this.Addition1.Visible = false;
            // 
            // AddDedIDD
            // 
            this.AddDedIDD.HeaderText = "AddDedIDD";
            this.AddDedIDD.Name = "AddDedIDD";
            this.AddDedIDD.ReadOnly = true;
            this.AddDedIDD.Visible = false;
            // 
            // BindingNavigatorRelease
            // 
            this.BindingNavigatorRelease.AddNewItem = null;
            this.BindingNavigatorRelease.CountItem = null;
            this.BindingNavigatorRelease.DeleteItem = null;
            this.BindingNavigatorRelease.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.WordReportButton});
            this.BindingNavigatorRelease.Location = new System.Drawing.Point(0, 0);
            this.BindingNavigatorRelease.MoveFirstItem = null;
            this.BindingNavigatorRelease.MoveLastItem = null;
            this.BindingNavigatorRelease.MoveNextItem = null;
            this.BindingNavigatorRelease.MovePreviousItem = null;
            this.BindingNavigatorRelease.Name = "BindingNavigatorRelease";
            this.BindingNavigatorRelease.PositionItem = null;
            this.BindingNavigatorRelease.Size = new System.Drawing.Size(610, 25);
            this.BindingNavigatorRelease.TabIndex = 168;
            this.BindingNavigatorRelease.Text = "BindingNavigator1";
            this.BindingNavigatorRelease.Visible = false;
            // 
            // WordReportButton
            // 
            this.WordReportButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.WordReportButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.WordReportButton.Name = "WordReportButton";
            this.WordReportButton.Size = new System.Drawing.Size(23, 22);
            this.WordReportButton.Text = "Bank Payment";
            // 
            // CreditAmtTextBox
            // 
            this.CreditAmtTextBox.Location = new System.Drawing.Point(336, 288);
            this.CreditAmtTextBox.Name = "CreditAmtTextBox";
            this.CreditAmtTextBox.ReadOnly = true;
            this.CreditAmtTextBox.Size = new System.Drawing.Size(116, 20);
            this.CreditAmtTextBox.TabIndex = 151;
            this.CreditAmtTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // GroupBox4
            // 
            this.GroupBox4.Controls.Add(Label11);
            this.GroupBox4.Controls.Add(this.dtpToday);
            this.GroupBox4.Controls.Add(this.Label9);
            this.GroupBox4.Controls.Add(this.Label8);
            this.GroupBox4.Controls.Add(this.CreditGroupTextBox);
            this.GroupBox4.Controls.Add(this.DebitGroupTextBox);
            this.GroupBox4.Controls.Add(this.Label3);
            this.GroupBox4.Controls.Add(this.TotalTextBox);
            this.GroupBox4.Location = new System.Drawing.Point(-5, 314);
            this.GroupBox4.Name = "GroupBox4";
            this.GroupBox4.Size = new System.Drawing.Size(603, 90);
            this.GroupBox4.TabIndex = 135;
            this.GroupBox4.TabStop = false;
            // 
            // dtpToday
            // 
            this.dtpToday.CustomFormat = "dd-MMM-yyyy";
            this.dtpToday.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToday.Location = new System.Drawing.Point(117, 10);
            this.dtpToday.Name = "dtpToday";
            this.dtpToday.Size = new System.Drawing.Size(113, 20);
            this.dtpToday.TabIndex = 144;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(367, 15);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(73, 13);
            this.Label9.TabIndex = 152;
            this.Label9.Text = "Gross Amount";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(367, 39);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(61, 13);
            this.Label8.TabIndex = 151;
            this.Label8.Text = "Deductions";
            // 
            // CreditGroupTextBox
            // 
            this.CreditGroupTextBox.Location = new System.Drawing.Point(460, 12);
            this.CreditGroupTextBox.Name = "CreditGroupTextBox";
            this.CreditGroupTextBox.ReadOnly = true;
            this.CreditGroupTextBox.Size = new System.Drawing.Size(133, 20);
            this.CreditGroupTextBox.TabIndex = 3;
            this.CreditGroupTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // DebitGroupTextBox
            // 
            this.DebitGroupTextBox.Location = new System.Drawing.Point(460, 36);
            this.DebitGroupTextBox.Name = "DebitGroupTextBox";
            this.DebitGroupTextBox.ReadOnly = true;
            this.DebitGroupTextBox.Size = new System.Drawing.Size(133, 20);
            this.DebitGroupTextBox.TabIndex = 4;
            this.DebitGroupTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(367, 63);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(63, 13);
            this.Label3.TabIndex = 140;
            this.Label3.Text = "Net Amount";
            // 
            // TotalTextBox
            // 
            this.TotalTextBox.Location = new System.Drawing.Point(460, 60);
            this.TotalTextBox.Name = "TotalTextBox";
            this.TotalTextBox.ReadOnly = true;
            this.TotalTextBox.Size = new System.Drawing.Size(133, 20);
            this.TotalTextBox.TabIndex = 5;
            this.TotalTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // BindingNavigatorSplit
            // 
            this.BindingNavigatorSplit.AddNewItem = null;
            this.BindingNavigatorSplit.CountItem = null;
            this.BindingNavigatorSplit.DeleteItem = null;
            this.BindingNavigatorSplit.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPrint});
            this.BindingNavigatorSplit.Location = new System.Drawing.Point(3, 3);
            this.BindingNavigatorSplit.MoveFirstItem = null;
            this.BindingNavigatorSplit.MoveLastItem = null;
            this.BindingNavigatorSplit.MoveNextItem = null;
            this.BindingNavigatorSplit.MovePreviousItem = null;
            this.BindingNavigatorSplit.Name = "BindingNavigatorSplit";
            this.BindingNavigatorSplit.PositionItem = null;
            this.BindingNavigatorSplit.Size = new System.Drawing.Size(600, 25);
            this.BindingNavigatorSplit.TabIndex = 168;
            this.BindingNavigatorSplit.Text = "BindingNavigator1";
            this.BindingNavigatorSplit.Visible = false;
            // 
            // TabPageAmountSplit
            // 
            this.TabPageAmountSplit.Controls.Add(this.BindingNavigatorSplit);
            this.TabPageAmountSplit.Controls.Add(this.Panel3);
            this.TabPageAmountSplit.Location = new System.Drawing.Point(4, 22);
            this.TabPageAmountSplit.Name = "TabPageAmountSplit";
            this.TabPageAmountSplit.Padding = new System.Windows.Forms.Padding(3);
            this.TabPageAmountSplit.Size = new System.Drawing.Size(605, 441);
            this.TabPageAmountSplit.TabIndex = 2;
            this.TabPageAmountSplit.Text = "Split";
            this.TabPageAmountSplit.UseVisualStyleBackColor = true;
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(96, 34);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(10, 13);
            this.Label14.TabIndex = 173;
            this.Label14.Text = ":";
            // 
            // NewAddDedButton
            // 
            this.NewAddDedButton.Location = new System.Drawing.Point(279, 7);
            this.NewAddDedButton.Name = "NewAddDedButton";
            this.NewAddDedButton.Size = new System.Drawing.Size(32, 22);
            this.NewAddDedButton.TabIndex = 152;
            this.NewAddDedButton.Text = "...";
            this.NewAddDedButton.UseVisualStyleBackColor = true;
            this.NewAddDedButton.Click += new System.EventHandler(this.NewAddDedButton_Click);
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.Transparent;
            this.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel1.Controls.Add(this.Label14);
            this.Panel1.Controls.Add(this.Label13);
            this.Panel1.Controls.Add(this.Label6);
            this.Panel1.Controls.Add(this.lblCurrency);
            this.Panel1.Controls.Add(this.lblTransactionType);
            this.Panel1.Controls.Add(this.lblEmployeeName);
            this.Panel1.Controls.Add(this.EmployeeLabel);
            this.Panel1.Controls.Add(Label2);
            this.Panel1.Controls.Add(Label1);
            this.Panel1.Location = new System.Drawing.Point(3, 30);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(597, 58);
            this.Panel1.TabIndex = 165;
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(517, 9);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(10, 13);
            this.Label13.TabIndex = 172;
            this.Label13.Text = ":";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(96, 9);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(10, 13);
            this.Label6.TabIndex = 171;
            this.Label6.Text = ":";
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Location = new System.Drawing.Point(108, 34);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(49, 13);
            this.lblCurrency.TabIndex = 170;
            this.lblCurrency.Text = "Currency";
            // 
            // lblTransactionType
            // 
            this.lblTransactionType.AutoSize = true;
            this.lblTransactionType.Location = new System.Drawing.Point(525, 9);
            this.lblTransactionType.Name = "lblTransactionType";
            this.lblTransactionType.Size = new System.Drawing.Size(31, 13);
            this.lblTransactionType.TabIndex = 169;
            this.lblTransactionType.Text = "Type";
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.AutoSize = true;
            this.lblEmployeeName.Location = new System.Drawing.Point(108, 9);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(35, 13);
            this.lblEmployeeName.TabIndex = 168;
            this.lblEmployeeName.Text = "Name";
            // 
            // EmployeeLabel
            // 
            this.EmployeeLabel.AutoSize = true;
            this.EmployeeLabel.Location = new System.Drawing.Point(7, 9);
            this.EmployeeLabel.Name = "EmployeeLabel";
            this.EmployeeLabel.Size = new System.Drawing.Size(53, 13);
            this.EmployeeLabel.TabIndex = 167;
            this.EmployeeLabel.Text = "Employee";
            // 
            // TabPageRelease
            // 
            this.TabPageRelease.Controls.Add(this.pnlAmount);
            this.TabPageRelease.Controls.Add(this.BtnConfirm);
            this.TabPageRelease.Controls.Add(this.CancelCButton);
            this.TabPageRelease.Location = new System.Drawing.Point(4, 22);
            this.TabPageRelease.Name = "TabPageRelease";
            this.TabPageRelease.Padding = new System.Windows.Forms.Padding(3);
            this.TabPageRelease.Size = new System.Drawing.Size(605, 441);
            this.TabPageRelease.TabIndex = 1;
            this.TabPageRelease.Text = "Release";
            this.TabPageRelease.UseVisualStyleBackColor = true;
            // 
            // BtnConfirm
            // 
            this.BtnConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnConfirm.Location = new System.Drawing.Point(428, 409);
            this.BtnConfirm.Name = "BtnConfirm";
            this.BtnConfirm.Size = new System.Drawing.Size(79, 23);
            this.BtnConfirm.TabIndex = 136;
            this.BtnConfirm.Text = "&Confirm All";
            this.BtnConfirm.UseVisualStyleBackColor = true;
            this.BtnConfirm.Click += new System.EventHandler(this.BtnConfirm_Click);
            // 
            // CancelCButton
            // 
            this.CancelCButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelCButton.Location = new System.Drawing.Point(514, 409);
            this.CancelCButton.Name = "CancelCButton";
            this.CancelCButton.Size = new System.Drawing.Size(79, 23);
            this.CancelCButton.TabIndex = 137;
            this.CancelCButton.Text = "&Cancel";
            this.CancelCButton.UseVisualStyleBackColor = true;
            this.CancelCButton.Click += new System.EventHandler(this.CancelCButton_Click);
            // 
            // AddBtn
            // 
            this.AddBtn.Location = new System.Drawing.Point(512, 8);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(41, 22);
            this.AddBtn.TabIndex = 2;
            this.AddBtn.Text = "Add";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // TabControlSal
            // 
            this.TabControlSal.Controls.Add(this.TabPageSal);
            this.TabControlSal.Controls.Add(this.TabPageAmountSplit);
            this.TabControlSal.Controls.Add(this.TabPageRelease);
            this.TabControlSal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControlSal.Location = new System.Drawing.Point(0, 0);
            this.TabControlSal.Name = "TabControlSal";
            this.TabControlSal.SelectedIndex = 0;
            this.TabControlSal.Size = new System.Drawing.Size(613, 467);
            this.TabControlSal.TabIndex = 140;
            // 
            // TabPageSal
            // 
            this.TabPageSal.Controls.Add(this.EmployeePaymentBindingNavigator);
            this.TabPageSal.Controls.Add(this.Panel2);
            this.TabPageSal.Controls.Add(this.Panel1);
            this.TabPageSal.Location = new System.Drawing.Point(4, 22);
            this.TabPageSal.Name = "TabPageSal";
            this.TabPageSal.Padding = new System.Windows.Forms.Padding(3);
            this.TabPageSal.Size = new System.Drawing.Size(605, 441);
            this.TabPageSal.TabIndex = 0;
            this.TabPageSal.Text = "Salary";
            this.TabPageSal.UseVisualStyleBackColor = true;
            // 
            // EmployeePaymentBindingNavigator
            // 
            this.EmployeePaymentBindingNavigator.AddNewItem = null;
            this.EmployeePaymentBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.EmployeePaymentBindingNavigator.DeleteItem = null;
            this.EmployeePaymentBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.btnAccSettings,
            this.EmployeePaymentBindingNavigatorSaveItem,
            this.BtEmail,
            this.HelpToolStripButton});
            this.EmployeePaymentBindingNavigator.Location = new System.Drawing.Point(3, 3);
            this.EmployeePaymentBindingNavigator.MoveFirstItem = null;
            this.EmployeePaymentBindingNavigator.MoveLastItem = null;
            this.EmployeePaymentBindingNavigator.MoveNextItem = null;
            this.EmployeePaymentBindingNavigator.MovePreviousItem = null;
            this.EmployeePaymentBindingNavigator.Name = "EmployeePaymentBindingNavigator";
            this.EmployeePaymentBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.EmployeePaymentBindingNavigator.Size = new System.Drawing.Size(599, 25);
            this.EmployeePaymentBindingNavigator.TabIndex = 167;
            this.EmployeePaymentBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(36, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnAccSettings
            // 
            this.btnAccSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAccSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAccSettings.Name = "btnAccSettings";
            this.btnAccSettings.Size = new System.Drawing.Size(23, 22);
            this.btnAccSettings.Text = "Account Settings";
            this.btnAccSettings.Visible = false;
            // 
            // EmployeePaymentBindingNavigatorSaveItem
            // 
            this.EmployeePaymentBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.EmployeePaymentBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("EmployeePaymentBindingNavigatorSaveItem.Image")));
            this.EmployeePaymentBindingNavigatorSaveItem.Name = "EmployeePaymentBindingNavigatorSaveItem";
            this.EmployeePaymentBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.EmployeePaymentBindingNavigatorSaveItem.Text = "Save Data";
            this.EmployeePaymentBindingNavigatorSaveItem.Visible = false;
            // 
            // BtEmail
            // 
            this.BtEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtEmail.Image")));
            this.BtEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtEmail.Name = "BtEmail";
            this.BtEmail.Size = new System.Drawing.Size(23, 22);
            this.BtEmail.Text = "Email";
            this.BtEmail.Click += new System.EventHandler(this.BtEmail_Click);
            // 
            // HelpToolStripButton
            // 
            this.HelpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.HelpToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("HelpToolStripButton.Image")));
            this.HelpToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.HelpToolStripButton.Name = "HelpToolStripButton";
            this.HelpToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.HelpToolStripButton.Text = "He&lp";
            this.HelpToolStripButton.Click += new System.EventHandler(this.HelpToolStripButton_Click);
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.Color.Transparent;
            this.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel2.Controls.Add(this.EmployeePaymentDetailDataGridView);
            this.Panel2.Controls.Add(this.NewAddDedButton);
            this.Panel2.Controls.Add(this.AddBtn);
            this.Panel2.Controls.Add(this.NetAmtTextBox);
            this.Panel2.Controls.Add(this.CreditTextBox);
            this.Panel2.Controls.Add(this.RemarksTextBox);
            this.Panel2.Controls.Add(Label5);
            this.Panel2.Controls.Add(Label4);
            this.Panel2.Controls.Add(this.DebitTextBox);
            this.Panel2.Controls.Add(this.Label10);
            this.Panel2.Controls.Add(this.AmountTextBox);
            this.Panel2.Controls.Add(this.AdditionDeductionComboBox);
            this.Panel2.Controls.Add(Label7);
            this.Panel2.Location = new System.Drawing.Point(3, 93);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(597, 343);
            this.Panel2.TabIndex = 166;
            // 
            // EmployeePaymentDetailDataGridView
            // 
            this.EmployeePaymentDetailDataGridView.AllowUserToDeleteRows = false;
            this.EmployeePaymentDetailDataGridView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.EmployeePaymentDetailDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.EmployeePaymentDetailDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EmployeePaymentDetailDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PaymentDetailID,
            this.PaymentID,
            this.LoanId,
            this.Addition,
            this.AddDedID,
            this.EmpVenFlag,
            this.ADDFLAG,
            this.Particulars,
            this.Earnings,
            this.Deductions,
            this.Remarks,
            this.IsEditable,
            this.ShortDescription,
            this.EmpCurrencyID});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.EmployeePaymentDetailDataGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.EmployeePaymentDetailDataGridView.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.EmployeePaymentDetailDataGridView.Location = new System.Drawing.Point(10, 37);
            this.EmployeePaymentDetailDataGridView.MultiSelect = false;
            this.EmployeePaymentDetailDataGridView.Name = "EmployeePaymentDetailDataGridView";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.EmployeePaymentDetailDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.EmployeePaymentDetailDataGridView.RowHeadersWidth = 25;
            this.EmployeePaymentDetailDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.EmployeePaymentDetailDataGridView.Size = new System.Drawing.Size(575, 267);
            this.EmployeePaymentDetailDataGridView.TabIndex = 153;
            this.EmployeePaymentDetailDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.EmployeePaymentDetailDataGridView_CellValueChanged);
            this.EmployeePaymentDetailDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.EmployeePaymentDetailDataGridView_CellBeginEdit);
            this.EmployeePaymentDetailDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.EmployeePaymentDetailDataGridView_CellValidating);
            this.EmployeePaymentDetailDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.EmployeePaymentDetailDataGridView_CellEndEdit);
            this.EmployeePaymentDetailDataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.EmployeePaymentDetailDataGridView_EditingControlShowing);
            this.EmployeePaymentDetailDataGridView.CurrentCellDirtyStateChanged += new System.EventHandler(this.EmployeePaymentDetailDataGridView_CurrentCellDirtyStateChanged);
            this.EmployeePaymentDetailDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.EmployeePaymentDetailDataGridView_DataError);
            this.EmployeePaymentDetailDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EmployeePaymentDetailDataGridView_KeyDown);
            // 
            // PaymentDetailID
            // 
            this.PaymentDetailID.HeaderText = "PaymentDetailID";
            this.PaymentDetailID.Name = "PaymentDetailID";
            this.PaymentDetailID.Visible = false;
            // 
            // PaymentID
            // 
            this.PaymentID.HeaderText = "PaymentID";
            this.PaymentID.Name = "PaymentID";
            this.PaymentID.Visible = false;
            // 
            // LoanId
            // 
            this.LoanId.HeaderText = "LoanId";
            this.LoanId.Name = "LoanId";
            this.LoanId.Visible = false;
            // 
            // Addition
            // 
            this.Addition.HeaderText = "Addition";
            this.Addition.Name = "Addition";
            this.Addition.Visible = false;
            // 
            // AddDedID
            // 
            this.AddDedID.HeaderText = "AddDedID";
            this.AddDedID.Name = "AddDedID";
            this.AddDedID.Visible = false;
            // 
            // EmpVenFlag
            // 
            this.EmpVenFlag.HeaderText = "EmpVenFlag";
            this.EmpVenFlag.Name = "EmpVenFlag";
            this.EmpVenFlag.Visible = false;
            // 
            // ADDFLAG
            // 
            this.ADDFLAG.HeaderText = "ADDFLAG";
            this.ADDFLAG.Name = "ADDFLAG";
            this.ADDFLAG.Visible = false;
            // 
            // Particulars
            // 
            this.Particulars.HeaderText = "Particulars";
            this.Particulars.Name = "Particulars";
            this.Particulars.Width = 200;
            // 
            // Earnings
            // 
            this.Earnings.HeaderText = "Earnings";
            this.Earnings.MaxInputLength = 15;
            this.Earnings.Name = "Earnings";
            this.Earnings.Width = 120;
            // 
            // Deductions
            // 
            this.Deductions.HeaderText = "Deductions";
            this.Deductions.MaxInputLength = 15;
            this.Deductions.Name = "Deductions";
            this.Deductions.Width = 120;
            // 
            // Remarks
            // 
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.Name = "Remarks";
            this.Remarks.Visible = false;
            // 
            // IsEditable
            // 
            this.IsEditable.HeaderText = "IsEditable";
            this.IsEditable.Name = "IsEditable";
            this.IsEditable.Visible = false;
            // 
            // ShortDescription
            // 
            this.ShortDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ShortDescription.HeaderText = "Currency";
            this.ShortDescription.Name = "ShortDescription";
            this.ShortDescription.ReadOnly = true;
            // 
            // EmpCurrencyID
            // 
            this.EmpCurrencyID.HeaderText = "EmpCurrencyID";
            this.EmpCurrencyID.Name = "EmpCurrencyID";
            this.EmpCurrencyID.Visible = false;
            // 
            // NetAmtTextBox
            // 
            this.NetAmtTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NetAmtTextBox.Location = new System.Drawing.Point(447, 309);
            this.NetAmtTextBox.Name = "NetAmtTextBox";
            this.NetAmtTextBox.ReadOnly = true;
            this.NetAmtTextBox.Size = new System.Drawing.Size(138, 20);
            this.NetAmtTextBox.TabIndex = 6;
            this.NetAmtTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CreditTextBox
            // 
            this.CreditTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CreditTextBox.Location = new System.Drawing.Point(247, 310);
            this.CreditTextBox.Name = "CreditTextBox";
            this.CreditTextBox.ReadOnly = true;
            this.CreditTextBox.Size = new System.Drawing.Size(100, 20);
            this.CreditTextBox.TabIndex = 5;
            this.CreditTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RemarksTextBox
            // 
            this.RemarksTextBox.Location = new System.Drawing.Point(62, 309);
            this.RemarksTextBox.MaxLength = 200;
            this.RemarksTextBox.Multiline = true;
            this.RemarksTextBox.Name = "RemarksTextBox";
            this.RemarksTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.RemarksTextBox.Size = new System.Drawing.Size(71, 20);
            this.RemarksTextBox.TabIndex = 151;
            this.RemarksTextBox.Visible = false;
            // 
            // DebitTextBox
            // 
            this.DebitTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DebitTextBox.Location = new System.Drawing.Point(353, 310);
            this.DebitTextBox.Name = "DebitTextBox";
            this.DebitTextBox.ReadOnly = true;
            this.DebitTextBox.Size = new System.Drawing.Size(89, 20);
            this.DebitTextBox.TabIndex = 4;
            this.DebitTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(7, 312);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(49, 13);
            this.Label10.TabIndex = 150;
            this.Label10.Text = "Remarks";
            this.Label10.Visible = false;
            // 
            // AmountTextBox
            // 
            this.AmountTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AmountTextBox.Location = new System.Drawing.Point(398, 9);
            this.AmountTextBox.MaxLength = 9;
            this.AmountTextBox.Name = "AmountTextBox";
            this.AmountTextBox.Size = new System.Drawing.Size(109, 20);
            this.AmountTextBox.TabIndex = 1;
            // 
            // AdditionDeductionComboBox
            // 
            this.AdditionDeductionComboBox.FormattingEnabled = true;
            this.AdditionDeductionComboBox.Location = new System.Drawing.Point(112, 8);
            this.AdditionDeductionComboBox.Name = "AdditionDeductionComboBox";
            this.AdditionDeductionComboBox.Size = new System.Drawing.Size(161, 21);
            this.AdditionDeductionComboBox.TabIndex = 0;
            // 
            // TimerForRpt
            // 
            this.TimerForRpt.Interval = 1000;
            // 
            // ErrorProviderRel
            // 
            this.ErrorProviderRel.ContainerControl = this;
            this.ErrorProviderRel.RightToLeft = true;
            // 
            // TextboxNumeric
            // 
            this.TextboxNumeric.Location = new System.Drawing.Point(856, 5);
            this.TextboxNumeric.Name = "TextboxNumeric";
            this.TextboxNumeric.Size = new System.Drawing.Size(102, 20);
            this.TextboxNumeric.TabIndex = 139;
            // 
            // ErrorProvider1
            // 
            this.ErrorProvider1.ContainerControl = this;
            // 
            // PaymentIDTextBox
            // 
            this.PaymentIDTextBox.Location = new System.Drawing.Point(858, 31);
            this.PaymentIDTextBox.Name = "PaymentIDTextBox";
            this.PaymentIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.PaymentIDTextBox.TabIndex = 137;
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(38, 17);
            this.StatusLabel.Text = "Status";
            this.StatusLabel.ToolTipText = "Status";
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.lblstatus});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 467);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(613, 22);
            this.StatusStrip1.TabIndex = 138;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "PaymentDetailID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "PaymentID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "LoanId";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Addition";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "AddDedID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "EmpVenFlag";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "ADDFLAG";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Particulars";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 200;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Earnings";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Width = 120;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "Deductions";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 120;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "Remarks";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "IsEditable";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn13.HeaderText = "Currency";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "EmpCurrencyID";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "Particular";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Width = 350;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "CreditAmt";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.Width = 126;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn17.HeaderText = "DebitAmt";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "Addition1";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.Visible = false;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.HeaderText = "AddDedIDD";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.Visible = false;
            // 
            // FrmSalaryPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 489);
            this.Controls.Add(PaymentIDLabel);
            this.Controls.Add(this.TabControlSal);
            this.Controls.Add(this.TextboxNumeric);
            this.Controls.Add(this.PaymentIDTextBox);
            this.Controls.Add(this.StatusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSalaryPayment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Salary Payment";
            this.Load += new System.EventHandler(this.FrmSalaryPayment_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSalaryPayment_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmSalaryPayment_KeyDown);
            this.Panel3.ResumeLayout(false);
            this.pnlAmount.ResumeLayout(false);
            this.pnlAmount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigatorRelease)).EndInit();
            this.BindingNavigatorRelease.ResumeLayout(false);
            this.BindingNavigatorRelease.PerformLayout();
            this.GroupBox4.ResumeLayout(false);
            this.GroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigatorSplit)).EndInit();
            this.BindingNavigatorSplit.ResumeLayout(false);
            this.BindingNavigatorSplit.PerformLayout();
            this.TabPageAmountSplit.ResumeLayout(false);
            this.TabPageAmountSplit.PerformLayout();
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.TabPageRelease.ResumeLayout(false);
            this.TabControlSal.ResumeLayout(false);
            this.TabPageSal.ResumeLayout(false);
            this.TabPageSal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeePaymentBindingNavigator)).EndInit();
            this.EmployeePaymentBindingNavigator.ResumeLayout(false);
            this.EmployeePaymentBindingNavigator.PerformLayout();
            this.Panel2.ResumeLayout(false);
            this.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeePaymentDetailDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderRel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider1)).EndInit();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.WebBrowser WebBrowser1;
        internal System.Windows.Forms.TextBox DebitAmtTextBox;
        internal System.Windows.Forms.Panel pnlAmount;
        internal System.Windows.Forms.BindingNavigator BindingNavigatorRelease;
        internal System.Windows.Forms.ToolStripButton WordReportButton;
        internal System.Windows.Forms.TextBox CreditAmtTextBox;
        internal System.Windows.Forms.GroupBox GroupBox4;
        internal System.Windows.Forms.DateTimePicker dtpToday;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox CreditGroupTextBox;
        internal System.Windows.Forms.TextBox DebitGroupTextBox;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox TotalTextBox;
        internal System.Windows.Forms.BindingNavigator BindingNavigatorSplit;
        internal System.Windows.Forms.TabPage TabPageAmountSplit;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.Button NewAddDedButton;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label lblCurrency;
        internal System.Windows.Forms.Label lblTransactionType;
        internal System.Windows.Forms.Label lblEmployeeName;
        internal System.Windows.Forms.Label EmployeeLabel;
        internal System.Windows.Forms.TabPage TabPageRelease;
        internal System.Windows.Forms.Button BtnConfirm;
        internal System.Windows.Forms.Button CancelCButton;
        internal System.Windows.Forms.Button AddBtn;

        internal System.Windows.Forms.TabControl TabControlSal;
        internal System.Windows.Forms.TabPage TabPageSal;
        internal System.Windows.Forms.BindingNavigator EmployeePaymentBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton EmployeePaymentBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BtEmail;
        internal System.Windows.Forms.ToolStripButton HelpToolStripButton;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.TextBox NetAmtTextBox;
        internal System.Windows.Forms.TextBox CreditTextBox;
        internal System.Windows.Forms.TextBox RemarksTextBox;
        internal System.Windows.Forms.TextBox DebitTextBox;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.TextBox AmountTextBox;
        internal System.Windows.Forms.ComboBox AdditionDeductionComboBox;
        internal System.Windows.Forms.Timer TimerForRpt;
        internal System.Windows.Forms.ErrorProvider ErrorProviderRel;
        internal System.Windows.Forms.TextBox TextboxNumeric;
        internal System.Windows.Forms.TextBox PaymentIDTextBox;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.Timer Timer1;
        internal System.Windows.Forms.ErrorProvider ErrorProvider1;
        internal DevComponents.DotNetBar.Controls.DataGridViewX GrdAmount;
        internal DevComponents.DotNetBar.Controls.DataGridViewX EmployeePaymentDetailDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Particular;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreditAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn DebitAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn Addition1;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddDedIDD;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn PaymentDetailID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PaymentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoanId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Addition;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddDedID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmpVenFlag;
        private System.Windows.Forms.DataGridViewTextBoxColumn ADDFLAG;
        private System.Windows.Forms.DataGridViewTextBoxColumn Particulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn Earnings;
        private System.Windows.Forms.DataGridViewTextBoxColumn Deductions;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsEditable;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShortDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmpCurrencyID;
        private System.Windows.Forms.ToolStripButton btnAccSettings;
    }
}