﻿namespace MyBooksERP
{
    partial class FrmAdditionDeduction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label DescriptionLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAdditionDeduction));
            this.AddDedBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.CancelToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtAddDedInArabic = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.dgvAdditionDeduction = new System.Windows.Forms.DataGridView();
            this.btnSave = new System.Windows.Forms.Button();
            this.PnlAddDed = new System.Windows.Forms.Panel();
            this.RdoDeduction = new System.Windows.Forms.RadioButton();
            this.RdoAddition = new System.Windows.Forms.RadioButton();
            this.txtAdditionDeduction = new System.Windows.Forms.TextBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.LblAdded = new System.Windows.Forms.StatusStrip();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.errAdditionDeduction = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblInArabic = new System.Windows.Forms.Label();
            DescriptionLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.AddDedBindingNavigator)).BeginInit();
            this.AddDedBindingNavigator.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdditionDeduction)).BeginInit();
            this.PnlAddDed.SuspendLayout();
            this.LblAdded.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errAdditionDeduction)).BeginInit();
            this.SuspendLayout();
            // 
            // DescriptionLabel
            // 
            DescriptionLabel.AutoSize = true;
            DescriptionLabel.Location = new System.Drawing.Point(4, 16);
            DescriptionLabel.Name = "DescriptionLabel";
            DescriptionLabel.Size = new System.Drawing.Size(99, 13);
            DescriptionLabel.TabIndex = 84;
            DescriptionLabel.Text = "Addition/Deduction";
            // 
            // AddDedBindingNavigator
            // 
            this.AddDedBindingNavigator.AddNewItem = null;
            this.AddDedBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.AddDedBindingNavigator.CountItem = null;
            this.AddDedBindingNavigator.DeleteItem = null;
            this.AddDedBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.CancelToolStripButton,
            this.ToolStripSeparator1,
            this.btnPrint,
            this.btnEmail,
            this.BtnHelp});
            this.AddDedBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.AddDedBindingNavigator.MoveFirstItem = null;
            this.AddDedBindingNavigator.MoveLastItem = null;
            this.AddDedBindingNavigator.MoveNextItem = null;
            this.AddDedBindingNavigator.MovePreviousItem = null;
            this.AddDedBindingNavigator.Name = "AddDedBindingNavigator";
            this.AddDedBindingNavigator.PositionItem = null;
            this.AddDedBindingNavigator.Size = new System.Drawing.Size(373, 25);
            this.AddDedBindingNavigator.TabIndex = 1;
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            this.BindingNavigatorSeparator2.Visible = false;
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New Company";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Remove";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // CancelToolStripButton
            // 
            this.CancelToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelToolStripButton.Image")));
            this.CancelToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CancelToolStripButton.Name = "CancelToolStripButton";
            this.CancelToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.CancelToolStripButton.ToolTipText = "Clear";
            this.CancelToolStripButton.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            this.ToolStripSeparator1.Visible = false;
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Visible = false;
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Visible = false;
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblInArabic);
            this.groupBox1.Controls.Add(this.txtAddDedInArabic);
            this.groupBox1.Controls.Add(this.Label3);
            this.groupBox1.Controls.Add(this.dgvAdditionDeduction);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.PnlAddDed);
            this.groupBox1.Controls.Add(DescriptionLabel);
            this.groupBox1.Controls.Add(this.txtAdditionDeduction);
            this.groupBox1.Controls.Add(this.shapeContainer1);
            this.groupBox1.Location = new System.Drawing.Point(4, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(365, 341);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // txtAddDedInArabic
            // 
            this.txtAddDedInArabic.BackColor = System.Drawing.SystemColors.Info;
            this.txtAddDedInArabic.Location = new System.Drawing.Point(126, 40);
            this.txtAddDedInArabic.MaxLength = 50;
            this.txtAddDedInArabic.Name = "txtAddDedInArabic";
            this.txtAddDedInArabic.Size = new System.Drawing.Size(233, 20);
            this.txtAddDedInArabic.TabIndex = 88;
            this.txtAddDedInArabic.TextChanged += new System.EventHandler(this.txtAdditionDeduction_TextChanged);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Label3.Location = new System.Drawing.Point(3, 107);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(168, 13);
            this.Label3.TabIndex = 86;
            this.Label3.Text = "Addition && Deduction Details";
            // 
            // dgvAdditionDeduction
            // 
            this.dgvAdditionDeduction.AllowUserToAddRows = false;
            this.dgvAdditionDeduction.AllowUserToDeleteRows = false;
            this.dgvAdditionDeduction.AllowUserToResizeColumns = false;
            this.dgvAdditionDeduction.AllowUserToResizeRows = false;
            this.dgvAdditionDeduction.BackgroundColor = System.Drawing.Color.White;
            this.dgvAdditionDeduction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAdditionDeduction.Location = new System.Drawing.Point(7, 131);
            this.dgvAdditionDeduction.MultiSelect = false;
            this.dgvAdditionDeduction.Name = "dgvAdditionDeduction";
            this.dgvAdditionDeduction.ReadOnly = true;
            this.dgvAdditionDeduction.RowHeadersVisible = false;
            this.dgvAdditionDeduction.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAdditionDeduction.Size = new System.Drawing.Size(353, 196);
            this.dgvAdditionDeduction.TabIndex = 85;
            this.dgvAdditionDeduction.Leave += new System.EventHandler(this.dgvAdditionDeduction_Leave);
            this.dgvAdditionDeduction.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAdditionDeduction_CellClick);
            this.dgvAdditionDeduction.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvAdditionDeduction_DataError);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(284, 69);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 83;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // PnlAddDed
            // 
            this.PnlAddDed.Controls.Add(this.RdoDeduction);
            this.PnlAddDed.Controls.Add(this.RdoAddition);
            this.PnlAddDed.Location = new System.Drawing.Point(126, 69);
            this.PnlAddDed.Name = "PnlAddDed";
            this.PnlAddDed.Size = new System.Drawing.Size(152, 27);
            this.PnlAddDed.TabIndex = 82;
            // 
            // RdoDeduction
            // 
            this.RdoDeduction.AutoSize = true;
            this.RdoDeduction.Location = new System.Drawing.Point(72, 4);
            this.RdoDeduction.Name = "RdoDeduction";
            this.RdoDeduction.Size = new System.Drawing.Size(74, 17);
            this.RdoDeduction.TabIndex = 1;
            this.RdoDeduction.TabStop = true;
            this.RdoDeduction.Text = "Deduction";
            this.RdoDeduction.UseVisualStyleBackColor = true;
            // 
            // RdoAddition
            // 
            this.RdoAddition.AutoSize = true;
            this.RdoAddition.Checked = true;
            this.RdoAddition.Location = new System.Drawing.Point(3, 3);
            this.RdoAddition.Name = "RdoAddition";
            this.RdoAddition.Size = new System.Drawing.Size(63, 17);
            this.RdoAddition.TabIndex = 0;
            this.RdoAddition.TabStop = true;
            this.RdoAddition.Text = "Addition";
            this.RdoAddition.UseVisualStyleBackColor = true;
            this.RdoAddition.CheckedChanged += new System.EventHandler(this.RdoAddition_CheckedChanged);
            // 
            // txtAdditionDeduction
            // 
            this.txtAdditionDeduction.BackColor = System.Drawing.SystemColors.Info;
            this.txtAdditionDeduction.Location = new System.Drawing.Point(126, 14);
            this.txtAdditionDeduction.MaxLength = 50;
            this.txtAdditionDeduction.Name = "txtAdditionDeduction";
            this.txtAdditionDeduction.Size = new System.Drawing.Size(233, 20);
            this.txtAdditionDeduction.TabIndex = 81;
            this.txtAdditionDeduction.TextChanged += new System.EventHandler(this.txtAdditionDeduction_TextChanged);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(359, 322);
            this.shapeContainer1.TabIndex = 87;
            this.shapeContainer1.TabStop = false;
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 111;
            this.LineShape1.X2 = 352;
            this.LineShape1.Y1 = 100;
            this.LineShape1.Y2 = 100;
            // 
            // LblAdded
            // 
            this.LblAdded.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblstatus});
            this.LblAdded.Location = new System.Drawing.Point(0, 398);
            this.LblAdded.Name = "LblAdded";
            this.LblAdded.Size = new System.Drawing.Size(373, 22);
            this.LblAdded.TabIndex = 5;
            this.LblAdded.Text = "StatusStrip1";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(292, 369);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 7;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(211, 369);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // errAdditionDeduction
            // 
            this.errAdditionDeduction.ContainerControl = this;
            // 
            // lblInArabic
            // 
            this.lblInArabic.AutoSize = true;
            this.lblInArabic.Location = new System.Drawing.Point(8, 47);
            this.lblInArabic.Name = "lblInArabic";
            this.lblInArabic.Size = new System.Drawing.Size(49, 13);
            this.lblInArabic.TabIndex = 90;
            this.lblInArabic.Text = "In Arabic";
            // 
            // FrmAdditionDeduction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 420);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.LblAdded);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.AddDedBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAdditionDeduction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Addition/Deduction";
            this.Load += new System.EventHandler(this.FrmAdditionDeduction_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAdditionDeduction_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmAdditionDeduction_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.AddDedBindingNavigator)).EndInit();
            this.AddDedBindingNavigator.ResumeLayout(false);
            this.AddDedBindingNavigator.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdditionDeduction)).EndInit();
            this.PnlAddDed.ResumeLayout(false);
            this.PnlAddDed.PerformLayout();
            this.LblAdded.ResumeLayout(false);
            this.LblAdded.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errAdditionDeduction)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator AddDedBindingNavigator;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton CancelToolStripButton;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.StatusStrip LblAdded;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Panel PnlAddDed;
        internal System.Windows.Forms.RadioButton RdoDeduction;
        internal System.Windows.Forms.RadioButton RdoAddition;
        internal System.Windows.Forms.TextBox txtAdditionDeduction;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.DataGridView dgvAdditionDeduction;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Timer tmrClear;
        private System.Windows.Forms.ErrorProvider errAdditionDeduction;
        internal System.Windows.Forms.TextBox txtAddDedInArabic;
        private System.Windows.Forms.Label lblInArabic;
    }
}