﻿namespace MyBooksERP
{
    partial class FrmVacationExtension
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbAdd = new System.Windows.Forms.RadioButton();
            this.rbLess = new System.Windows.Forms.RadioButton();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtDays = new System.Windows.Forms.TextBox();
            this.lblDays = new System.Windows.Forms.Label();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpRejoinDate = new System.Windows.Forms.DateTimePicker();
            this.TextboxNumeric = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbAdd
            // 
            this.rbAdd.AutoSize = true;
            this.rbAdd.Checked = true;
            this.rbAdd.Location = new System.Drawing.Point(12, 14);
            this.rbAdd.Name = "rbAdd";
            this.rbAdd.Size = new System.Drawing.Size(103, 17);
            this.rbAdd.TabIndex = 1;
            this.rbAdd.TabStop = true;
            this.rbAdd.Text = "Extend Vacation";
            this.rbAdd.UseVisualStyleBackColor = true;
            // 
            // rbLess
            // 
            this.rbLess.AutoSize = true;
            this.rbLess.Location = new System.Drawing.Point(122, 14);
            this.rbLess.Name = "rbLess";
            this.rbLess.Size = new System.Drawing.Size(107, 17);
            this.rbLess.TabIndex = 2;
            this.rbLess.Text = "Shorten Vacation";
            this.rbLess.UseVisualStyleBackColor = true;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(9, 71);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 232;
            this.lblRemarks.Text = "Remarks";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(328, 148);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(76, 71);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRemarks.Size = new System.Drawing.Size(307, 54);
            this.txtRemarks.TabIndex = 4;
            // 
            // txtDays
            // 
            this.txtDays.Location = new System.Drawing.Point(76, 45);
            this.txtDays.MaxLength = 9;
            this.txtDays.Name = "txtDays";
            this.txtDays.Size = new System.Drawing.Size(85, 20);
            this.txtDays.TabIndex = 3;
            this.txtDays.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDays_KeyPress);
            // 
            // lblDays
            // 
            this.lblDays.AutoSize = true;
            this.lblDays.Location = new System.Drawing.Point(9, 48);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(62, 13);
            this.lblDays.TabIndex = 233;
            this.lblDays.Text = "No Of Days";
            // 
            // Panel1
            // 
            this.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel1.Controls.Add(this.label1);
            this.Panel1.Controls.Add(this.dtpRejoinDate);
            this.Panel1.Controls.Add(this.rbAdd);
            this.Panel1.Controls.Add(this.rbLess);
            this.Panel1.Controls.Add(this.lblRemarks);
            this.Panel1.Controls.Add(this.txtRemarks);
            this.Panel1.Controls.Add(this.txtDays);
            this.Panel1.Controls.Add(this.lblDays);
            this.Panel1.Controls.Add(this.TextboxNumeric);
            this.Panel1.Location = new System.Drawing.Point(8, 8);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(395, 134);
            this.Panel1.TabIndex = 3;
            this.Panel1.TabStop = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(167, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 1011;
            this.label1.Text = "Actual Joining Date";
            // 
            // dtpRejoinDate
            // 
            this.dtpRejoinDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpRejoinDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRejoinDate.Location = new System.Drawing.Point(274, 45);
            this.dtpRejoinDate.Name = "dtpRejoinDate";
            this.dtpRejoinDate.ShowCheckBox = true;
            this.dtpRejoinDate.Size = new System.Drawing.Size(109, 20);
            this.dtpRejoinDate.TabIndex = 1010;
            // 
            // TextboxNumeric
            // 
            this.TextboxNumeric.Location = new System.Drawing.Point(249, 88);
            this.TextboxNumeric.Name = "TextboxNumeric";
            this.TextboxNumeric.Size = new System.Drawing.Size(29, 20);
            this.TextboxNumeric.TabIndex = 1009;
            this.TextboxNumeric.Visible = false;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(247, 148);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "&Ok";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // FrmVacationExtension
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 179);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmVacationExtension";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vacation Extension or Rejoin Confirmation";
            this.Load += new System.EventHandler(this.FrmVacationExtension_Load);
            this.Shown += new System.EventHandler(this.FrmVacationExtension_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmVacationExtension_FormClosing);
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.RadioButton rbAdd;
        internal System.Windows.Forms.RadioButton rbLess;
        internal System.Windows.Forms.Label lblRemarks;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.TextBox txtDays;
        internal System.Windows.Forms.Label lblDays;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.TextBox TextboxNumeric;
        internal System.Windows.Forms.Button btnOK;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.DateTimePicker dtpRejoinDate;
    }
}