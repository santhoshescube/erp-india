﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    /*****************************************************
   * Created By       : Laxmi
   * Creation Date    : 11 Apr 2012
   * Description      : Handle Addition deduction
   * Modified By      : Siny
   * Modified Date    : 29 Aug 2013
   * Description      : Fine tuning and Performance improving 
   * FormID           : 115
   * ***************************************************/

    public partial class FrmAdditionDeduction : Form
    {

        #region Declarations
        public int PintAdditionDeductionID = 0;   // From Refernce Form 
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private string MstrMessageCaption;              //Message caption
        private string MstrCommonMessage;
        private bool MblnIsEditMode = false;
        private bool MblnChangeStatus = false;
        private bool MblbtnOk = false;
        private bool MblnIsPredeined = false;
        private clsBLLAdditionDeduction MobjclsBLLAdditionDeduction = null;
        private clsMessage ObjUserMessage = null;
        public string strProcessDate = "";
        #endregion

        #region Constructor
        public FrmAdditionDeduction()
        {
            InitializeComponent();
            MstrMessageCaption = ClsCommonSettings.MessageCaption;

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls(); 
            //}

        }
        #endregion Constructor

        #region Properties
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.AdditionDeduction);
                return this.ObjUserMessage;
            }
        }

        private clsBLLAdditionDeduction BLLAdditionDeduction
        {
            get
            {
                if (this.MobjclsBLLAdditionDeduction == null)
                    this.MobjclsBLLAdditionDeduction = new clsBLLAdditionDeduction();

                return this.MobjclsBLLAdditionDeduction;
            }
        }
        #endregion Properties

        #region Methods

        #region SetPermissions
        /// <summary>
        /// Function for setting permissions
        /// </summary>
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.AdditionDeduction, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }
        #endregion SetPermissions

        #region ClearAllControls
        private void ClearAllControls()
        {
            this.txtAdditionDeduction.Text = "";
            this.txtAdditionDeduction.Tag = 0;
            this.txtAddDedInArabic.Text = "";
            this.txtAddDedInArabic.Tag = 0;
            PnlAddDed.Enabled = true;
            MblnIsPredeined = false;
        }
        #endregion ClearAllControls

        #region Changestatus
        /// <summary>
        /// function for changing status
        /// </summary>
        private void Changestatus()
        {
            if (!MblnIsEditMode)
            {
                btnOk.Enabled = MblnAddPermission;
                btnSave.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                btnOk.Enabled = MblnUpdatePermission;
                btnSave.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            errAdditionDeduction.Clear();
            MblnChangeStatus = true;
        }
        #endregion Changestatus

        #region FillAdditionDeductions
        private void FillAdditionDeductions()
        {
            dgvAdditionDeduction.DataSource = null;
            DataTable dtAdditionDeduction = BLLAdditionDeduction.DisplayAdditionDeduction();
            if (dtAdditionDeduction != null)
            {
                if (dtAdditionDeduction.Rows.Count > 0)
                {
                    dgvAdditionDeduction.DataSource = dtAdditionDeduction;
                    dgvAdditionDeduction.Columns[0].Visible = false;
                    dgvAdditionDeduction.Columns[1].Width = dgvAdditionDeduction.Columns[4].Width = 270;
                    dgvAdditionDeduction.Columns[2].Visible = false;
                    dgvAdditionDeduction.Columns[3].Visible = false;
                    dgvAdditionDeduction.Columns[1].AutoSizeMode = dgvAdditionDeduction.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    dgvAdditionDeduction.Columns["Particulars"].HeaderText = "تفاصيل";
                    //    dgvAdditionDeduction.Columns["ParticularsArb"].HeaderText = "باللغة العربية";
                    //}
                    //else
                        dgvAdditionDeduction.Columns["ParticularsArb"].HeaderText = "In Arabic";


                    //if (ClsCommonSettings.IsArabicViewEnabled==false)
                    //{
                        dgvAdditionDeduction.Columns["ParticularsArb"].Visible = false; 
                    //}



                }
            }
        }
        #endregion FillAdditionDeductions

        #region AddNewAdditionDeduction
        /// <summary>
        /// Add new item
        /// </summary>
        private void AddNewAdditionDeduction()
        {
            MblnChangeStatus = false;
            MblnIsEditMode = false;
            lblstatus.Text = "";
            tmrClear.Enabled = true;
            errAdditionDeduction.Clear();

            ClearAllControls();  //Clear all controls in the form
            FillAdditionDeductions();
            btnPrint.Enabled = false;
            btnEmail.Enabled = false;
            btnOk.Enabled = false;
            btnSave.Enabled = false;
            BindingNavigatorSaveItem.Enabled = false;
            BindingNavigatorAddNewItem.Enabled = false;
            CancelToolStripButton.Enabled = MblnAddPermission;
            txtAdditionDeduction.Focus();
        }
        #endregion AddNewAdditionDeduction

        #region DisplayAdditionDeduction
        /// <summary>
        /// Display addition deduction
        /// </summary>
        private void DisplayAdditionDeduction()
        {

            FillAdditionDeduction();
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            btnPrint.Enabled = MblnPrintEmailPermission;
            btnEmail.Enabled = MblnPrintEmailPermission;
            CancelToolStripButton.Enabled = false;
            MblnIsEditMode = true;
            MblnChangeStatus = false;
        }
        #endregion DisplayAdditionDeduction

        #region FillAdditionDeduction
        /// <summary>
        /// Fill AdditionDeduction details on click
        /// </summary>
        private void FillAdditionDeduction()
        {
            ClearAllControls();

            if (dgvAdditionDeduction.RowCount >= 1)
            {
                txtAdditionDeduction.Tag = Convert.ToInt32(dgvAdditionDeduction.CurrentRow.Cells[0].Value);

                if (Convert.ToInt32(txtAdditionDeduction.Tag) > 0)
                {
                    RdoAddition.Checked = Convert.ToBoolean(dgvAdditionDeduction.Rows[dgvAdditionDeduction.CurrentRow.Index].Cells[2].Value);
                    RdoDeduction.Checked = !(RdoAddition.Checked);
                    this.txtAdditionDeduction.Text = dgvAdditionDeduction.Rows[dgvAdditionDeduction.CurrentRow.Index].Cells[1].Value.ToString().Replace("(+)", "").Replace("(-)", "");
                    this.txtAddDedInArabic.Text = dgvAdditionDeduction.Rows[dgvAdditionDeduction.CurrentRow.Index].Cells[4].Value.ToString().Replace("(+)", "").Replace("(-)", "");
                    //PnlAddDed.Enabled = !(MobjclsBLLAdditionDeduction.AddDedIDExistInSalary(Convert.ToInt32(txtAdditionDeduction.Tag)));
                   int iRetValue = MobjclsBLLAdditionDeduction.AddDedIDExists(txtAdditionDeduction.Tag.ToInt32());
                    PnlAddDed.Enabled = iRetValue == 0? true  :false ;

                    if (iRetValue == 1)
                        
                       lblstatus.Text = UserMessage.GetMessageByCode(8102);
                    else if (iRetValue == -1)                  
                        lblstatus.Text = UserMessage.GetMessageByCode(8106);               
                    else if (iRetValue == -2)                   
                         lblstatus.Text = UserMessage.GetMessageByCode(8103);              
                    else if (iRetValue == -3)
                        lblstatus.Text = UserMessage.GetMessageByCode(8105);

                    tmrClear.Enabled = true;


                    MblnIsPredeined = Convert.ToBoolean(dgvAdditionDeduction.Rows[dgvAdditionDeduction.CurrentRow.Index].Cells[3].Value);
                    if (MblnIsPredeined)
                    {
                        BindingNavigatorSaveItem.Enabled = BindingNavigatorDeleteItem.Enabled = btnOk.Enabled = btnSave.Enabled = !(MblnIsPredeined);
                        lblstatus.Text = UserMessage.GetMessageByCode(8104);
                        tmrClear.Enabled = true;
                    }
                    else
                    {
                        BindingNavigatorSaveItem.Enabled = BindingNavigatorDeleteItem.Enabled = btnOk.Enabled = btnSave.Enabled = MblnIsPredeined;
                        lblstatus.Text = "";
                        tmrClear.Enabled = true;
                    }
                }
            }
        }


        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.AdditionDeduction, this);
        //}

        #endregion FillAdditionDeduction

        #region FormValidation
        /// <summary>
        /// Validation before save
        /// </summary>
        /// <returns></returns>
        private bool FormValidation()
        {
            errAdditionDeduction.Clear();
            Control control = null;
            bool blnReturnValue = true;

            //Please enter Addition/Deduction
            if (txtAdditionDeduction.Text.Trim().Length == 0)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(8100);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                control = txtAdditionDeduction;
                blnReturnValue = false;
            }

            //if (ClsCommonSettings.IsArabicViewEnabled)
            //{
            //    if (txtAddDedInArabic.Text.Trim().Length == 0)
            //    {
            //        MstrCommonMessage = UserMessage.GetMessageByCode(8100);
            //        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        control = txtAddDedInArabic;
            //        blnReturnValue = false;
            //    }
            //}

            // Duplication
            if (MobjclsBLLAdditionDeduction.CheckDuplication(!(MblnIsEditMode), new string[] { txtAdditionDeduction.Text.Replace("'", "").Trim(),
                txtAddDedInArabic.Text.Replace("'", "").Trim()}, txtAdditionDeduction.Tag.ToInt32()))
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(8101);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                control = txtAdditionDeduction;
                blnReturnValue = false;
            }

            return blnReturnValue;
        }
        #endregion FormValidation

        #region DeleteValidation
        /// <summary>
        /// Validation before delete
        /// </summary>
        /// <returns>Valid/not</returns>
        private bool DeleteValidation()
        {
            int iRetValue = 0;
            if (txtAdditionDeduction.Tag.ToInt32() == 0)
            {
                UserMessage.ShowMessage(9015);
                return false;
            }
            //Checking for reference
            iRetValue = MobjclsBLLAdditionDeduction.AddDedIDExists(txtAdditionDeduction.Tag.ToInt32());
            if (iRetValue == 1)
            {
                UserMessage.ShowMessage(8102);
                return false;
            }
            else if (iRetValue == -1)
            {
                UserMessage.ShowMessage(8106);
                return false;
            }
            else if (iRetValue == -2)
            {
                UserMessage.ShowMessage(8103);
                return false;
            }
            else if (iRetValue == -3)
            {
                UserMessage.ShowMessage(8105);
                return false;
            }
            if (UserMessage.ShowMessage(13) == false)
            {
                return false;
            }
            return true;

        }
        #endregion DeleteValidation

        #region FillParameterMaster
        /// <summary>
        /// Fill master parameters before save
        /// </summary>
        private void FillParameterMaster()
        {
            bool bAddition = false;
            if (this.RdoAddition.Checked) bAddition = true;
            MobjclsBLLAdditionDeduction.PobjclsDTOAdditionDeduction.AdditionDeductionID = txtAdditionDeduction.Tag.ToInt32();
            MobjclsBLLAdditionDeduction.PobjclsDTOAdditionDeduction.AdditionDeduction = txtAdditionDeduction.Text.Trim();
            MobjclsBLLAdditionDeduction.PobjclsDTOAdditionDeduction.AdditionDeductionArb = txtAddDedInArabic.Text.Trim();
            MobjclsBLLAdditionDeduction.PobjclsDTOAdditionDeduction.IsAddition = bAddition;
        }
        #endregion FillParameterMaster

        #region SaveAdditionDeduction
        /// <summary>
        /// Save
        /// </summary>
        /// <returns>success/failure</returns>
        private bool SaveAdditionDeduction()
        {
            try
            {
                bool blnRetValue = false;
                if (FormValidation())
                {
                    int intMessageCode = 0;
                    if (txtAdditionDeduction.Tag.ToInt32() > 0)
                        intMessageCode = 3;
                    else
                        intMessageCode = 1;
                    if (UserMessage.ShowMessage(intMessageCode))
                    {
                        FillParameterMaster();
                        if (MobjclsBLLAdditionDeduction.AddDedSave())
                        {
                            AddNewAdditionDeduction();
                            blnRetValue = true;
                            txtAdditionDeduction.Tag = MobjclsBLLAdditionDeduction.PobjclsDTOAdditionDeduction.AdditionDeductionID;
                            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                            btnEmail.Enabled = MblnPrintEmailPermission;
                            btnPrint.Enabled = MblnPrintEmailPermission;
                            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                        }
                    }
                }
                return blnRetValue;
            }
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1,Log.LogSeverity.Error);
                return false;
            }
        }
        #endregion SaveAdditionDeduction

        #region DeleteAdditionDeduction
        /// <summary>
        /// Delete
        /// </summary>
        /// <returns>success/failure</returns>
        private bool DeleteAdditionDeduction()
        {
            bool blnRetValue = false;
            if (DeleteValidation())
            {
                MobjclsBLLAdditionDeduction.PobjclsDTOAdditionDeduction.AdditionDeductionID = txtAdditionDeduction.Tag.ToInt32();
                if (MobjclsBLLAdditionDeduction.DeleteAdditionDeduction())
                {
                    AddNewAdditionDeduction();
                    blnRetValue = true;
                }
            }
            return blnRetValue;
        }
        #endregion DeleteAdditionDeduction

        # endregion

        #region Events

        private void FrmAdditionDeduction_Load(object sender, EventArgs e)
        {
            SetPermissions();

            //if (ClsCommonSettings.IsArabicViewEnabled  == false)
            //{
            //    lblInArabic.Visible = false;
            //    txtAddDedInArabic.Visible = false;
            //}
            //else
            //{
                lblInArabic.Visible = true ;
                txtAddDedInArabic.Visible = true;
            //}


            if (PintAdditionDeductionID > 0)
            {
                // RefernceDisplay();
            }
            else
            {
                AddNewAdditionDeduction();
            }
        }

        private void FrmAdditionDeduction_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnSave.Enabled)
            {
                if (UserMessage.ShowMessage(8))
                {
                    e.Cancel = false;
                }
                else
                { e.Cancel = true; }
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewAdditionDeduction();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (DeleteAdditionDeduction())
                {
                    lblstatus.Text = UserMessage.GetMessageByCode(4);
                    tmrClear.Enabled = true;
                    // UserMessage.ShowMessage(4);
                }
            }
            catch (System.Data.SqlClient.SqlException Ex)
            {
                if (Ex.Number == (int)SqlErrorCodes.ForeignKeyErrorNumber)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    MstrCommonMessage = "التفاصيل موجودة في النظام. لا يمكن حذف";
                    //else
                        MstrCommonMessage = "Details exists.Cannot delete";
                }
                else
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    MstrCommonMessage = "التفاصيل موجودة في النظام. لا يمكن حذف";
                    //else
                        MstrCommonMessage = "Details exists.Cannot delete";
                }

                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1,Log.LogSeverity.Error);
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveAdditionDeduction())
            {
                lblstatus.Text = UserMessage.GetMessageByCode(2);
                tmrClear.Enabled = true;
                //UserMessage.ShowMessage(2);
            }
        }

        private void CancelToolStripButton_Click(object sender, EventArgs e)
        {
            AddNewAdditionDeduction();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveAdditionDeduction())
            {
                lblstatus.Text = UserMessage.GetMessageByCode(2);
                tmrClear.Enabled = true;
                //UserMessage.ShowMessage(2);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveAdditionDeduction())
            {
                btnSave.Enabled = false;
                MblbtnOk = true;
                this.Close();
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtAdditionDeduction_TextChanged(object sender, EventArgs e)
        {
            //function for changing status
            if (!MblnIsEditMode)
            {
                btnOk.Enabled = MblnAddPermission;
                btnSave.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                btnOk.Enabled = MblnUpdatePermission;
                btnSave.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            if (MblnIsPredeined) btnOk.Enabled = btnSave.Enabled = BindingNavigatorSaveItem.Enabled = false;
            errAdditionDeduction.Clear();
            MblnChangeStatus = true;
        }

        private void dgvAdditionDeduction_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
        }

        private void dgvAdditionDeduction_Leave(object sender, EventArgs e)
        {
            if (dgvAdditionDeduction.RowCount > 0) dgvAdditionDeduction.ClearSelection();
        }

        private void dgvAdditionDeduction_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DisplayAdditionDeduction();
        }

        private void RdoAddition_CheckedChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void FrmAdditionDeduction_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        CancelToolStripButton_Click(sender, new EventArgs());//Clear
                        break;
                }
            }
            catch
            {
            }
        }
        #endregion

       
    }

}