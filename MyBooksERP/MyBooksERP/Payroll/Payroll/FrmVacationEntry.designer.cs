﻿namespace MyBooksERP
{
    partial class FrmVacationEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lblGivenAmount;
            System.Windows.Forms.Label Label2;
            System.Windows.Forms.Label lblNetAmount;
            System.Windows.Forms.Label lblDeductions;
            System.Windows.Forms.Label lblAdditions;
            System.Windows.Forms.Label lblProcessDate;
            System.Windows.Forms.Label lblAmount;
            System.Windows.Forms.Label lblParticulars;
            System.Windows.Forms.Label lblCheqNo;
            System.Windows.Forms.Label lblAccount;
            System.Windows.Forms.Label lblChequeDate;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label Label12;
            System.Windows.Forms.Label label1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVacationEntry));
            this.lblRejoinDaate = new System.Windows.Forms.Label();
            this.dtpRejoinDate = new System.Windows.Forms.DateTimePicker();
            this.chkEncashOnly = new System.Windows.Forms.CheckBox();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.lblTotalEligibleLeavePayDays = new System.Windows.Forms.Label();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tabVacationProcess = new System.Windows.Forms.TabControl();
            this.tabEmployeeInfo = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtEncashComboOffDay = new System.Windows.Forms.TextBox();
            this.lblTicketAmount = new System.Windows.Forms.Label();
            this.lblIssuedTickets = new System.Windows.Forms.Label();
            this.txtNoOfTicketsIssued = new System.Windows.Forms.TextBox();
            this.txtTicketAmount = new System.Windows.Forms.TextBox();
            this.lblAbsentDays = new System.Windows.Forms.Label();
            this.lblAbsentDaysText = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtChequeNo = new System.Windows.Forms.TextBox();
            this.cboAccount = new System.Windows.Forms.ComboBox();
            this.cboTransactionType = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpChequeDate = new System.Windows.Forms.DateTimePicker();
            this.lblVacationDays = new System.Windows.Forms.Label();
            this.lblExperienceDays = new System.Windows.Forms.Label();
            this.lblTotalEligibleLeavePayDaysText = new System.Windows.Forms.Label();
            this.txtEligibleLeavePayDays = new System.Windows.Forms.TextBox();
            this.dtpProcessDate = new System.Windows.Forms.DateTimePicker();
            this.lblEligibleLeavePayDays = new System.Windows.Forms.Label();
            this.lblExperienceDaysText = new System.Windows.Forms.Label();
            this.dtpJoiningDate = new System.Windows.Forms.DateTimePicker();
            this.lblNoOfDays = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.lblEligibleLeavesText = new System.Windows.Forms.Label();
            this.lblIsPaid = new System.Windows.Forms.Label();
            this.lblEligibleLeaves = new System.Windows.Forms.Label();
            this.lblAdditionLeaves = new System.Windows.Forms.Label();
            this.lblAdditionalLeavesText = new System.Windows.Forms.Label();
            this.shapeContainer3 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkProcessSalaryForCurrentMonth = new System.Windows.Forms.CheckBox();
            this.chkConsiderAbsentDays = new System.Windows.Forms.CheckBox();
            this.lblActualRejoinDate = new System.Windows.Forms.Label();
            this.lblActualRejoinDateDec = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.lblToDate = new System.Windows.Forms.Label();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.btnProcess = new System.Windows.Forms.Button();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.tabParticulars = new System.Windows.Forms.TabPage();
            this.TextboxNumeric1 = new System.Windows.Forms.TextBox();
            this.TextboxNumeric = new System.Windows.Forms.TextBox();
            this.txtGivenAmount = new System.Windows.Forms.TextBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtNetAmount = new System.Windows.Forms.TextBox();
            this.txtTotalAddition = new System.Windows.Forms.TextBox();
            this.txtTotalDeduction = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.dgvVacationDetails = new System.Windows.Forms.DataGridView();
            this.dgvColVacationDetailID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColParticulars = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvColAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColIsAddition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColNoOfDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColReleaseLater = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgvColTempAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAdditionDeductions = new System.Windows.Forms.Button();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.cboParticulars = new System.Windows.Forms.ComboBox();
            this.shapeContainer4 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.tabHistory = new System.Windows.Forms.TabPage();
            this.dgvEmpHistory = new System.Windows.Forms.DataGridView();
            this.lblEmployee = new System.Windows.Forms.Label();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.errorProviderVacation = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrVacation = new System.Windows.Forms.Timer(this.components);
            this.btnSave = new System.Windows.Forms.Button();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.ssVacation = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnEmail = new System.Windows.Forms.ToolStripButton();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.bnPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorVacationProcess = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bnClearItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bnVacationExtension = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            lblGivenAmount = new System.Windows.Forms.Label();
            Label2 = new System.Windows.Forms.Label();
            lblNetAmount = new System.Windows.Forms.Label();
            lblDeductions = new System.Windows.Forms.Label();
            lblAdditions = new System.Windows.Forms.Label();
            lblProcessDate = new System.Windows.Forms.Label();
            lblAmount = new System.Windows.Forms.Label();
            lblParticulars = new System.Windows.Forms.Label();
            lblCheqNo = new System.Windows.Forms.Label();
            lblAccount = new System.Windows.Forms.Label();
            lblChequeDate = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            Label12 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            this.pnlMain.SuspendLayout();
            this.tabVacationProcess.SuspendLayout();
            this.tabEmployeeInfo.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabParticulars.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVacationDetails)).BeginInit();
            this.tabHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmpHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderVacation)).BeginInit();
            this.ssVacation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigatorVacationProcess)).BeginInit();
            this.BindingNavigatorVacationProcess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblGivenAmount
            // 
            lblGivenAmount.AutoSize = true;
            lblGivenAmount.Location = new System.Drawing.Point(353, 265);
            lblGivenAmount.Name = "lblGivenAmount";
            lblGivenAmount.Size = new System.Drawing.Size(74, 13);
            lblGivenAmount.TabIndex = 15;
            lblGivenAmount.Text = "Given Amount";
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label2.Location = new System.Drawing.Point(-2, 16);
            Label2.Name = "Label2";
            Label2.Size = new System.Drawing.Size(147, 13);
            Label2.TabIndex = 0;
            Label2.Text = "Vacation Settlement Info";
            // 
            // lblNetAmount
            // 
            lblNetAmount.AutoSize = true;
            lblNetAmount.Location = new System.Drawing.Point(191, 265);
            lblNetAmount.Name = "lblNetAmount";
            lblNetAmount.Size = new System.Drawing.Size(63, 13);
            lblNetAmount.TabIndex = 13;
            lblNetAmount.Text = "Net Amount";
            // 
            // lblDeductions
            // 
            lblDeductions.AutoSize = true;
            lblDeductions.Location = new System.Drawing.Point(353, 238);
            lblDeductions.Name = "lblDeductions";
            lblDeductions.Size = new System.Drawing.Size(61, 13);
            lblDeductions.TabIndex = 11;
            lblDeductions.Text = "Deductions";
            // 
            // lblAdditions
            // 
            lblAdditions.AutoSize = true;
            lblAdditions.Location = new System.Drawing.Point(191, 238);
            lblAdditions.Name = "lblAdditions";
            lblAdditions.Size = new System.Drawing.Size(50, 13);
            lblAdditions.TabIndex = 9;
            lblAdditions.Text = "Additions";
            // 
            // lblProcessDate
            // 
            lblProcessDate.AutoSize = true;
            lblProcessDate.Location = new System.Drawing.Point(132, 52);
            lblProcessDate.Name = "lblProcessDate";
            lblProcessDate.Size = new System.Drawing.Size(71, 13);
            lblProcessDate.TabIndex = 6;
            lblProcessDate.Text = "Process Date";
            lblProcessDate.Visible = false;
            // 
            // lblAmount
            // 
            lblAmount.AutoSize = true;
            lblAmount.Location = new System.Drawing.Point(272, 45);
            lblAmount.Name = "lblAmount";
            lblAmount.Size = new System.Drawing.Size(43, 13);
            lblAmount.TabIndex = 3;
            lblAmount.Text = "Amount";
            // 
            // lblParticulars
            // 
            lblParticulars.AutoSize = true;
            lblParticulars.Location = new System.Drawing.Point(5, 45);
            lblParticulars.Name = "lblParticulars";
            lblParticulars.Size = new System.Drawing.Size(51, 13);
            lblParticulars.TabIndex = 208;
            lblParticulars.Text = "Particular";
            // 
            // lblCheqNo
            // 
            lblCheqNo.AutoSize = true;
            lblCheqNo.Location = new System.Drawing.Point(4, 45);
            lblCheqNo.Name = "lblCheqNo";
            lblCheqNo.Size = new System.Drawing.Size(64, 13);
            lblCheqNo.TabIndex = 2;
            lblCheqNo.Text = "Cheque No ";
            // 
            // lblAccount
            // 
            lblAccount.AutoSize = true;
            lblAccount.Location = new System.Drawing.Point(4, 97);
            lblAccount.Name = "lblAccount";
            lblAccount.Size = new System.Drawing.Size(47, 13);
            lblAccount.TabIndex = 1043;
            lblAccount.Text = "Account";
            lblAccount.Click += new System.EventHandler(this.lblAccount_Click);
            // 
            // lblChequeDate
            // 
            lblChequeDate.AutoSize = true;
            lblChequeDate.Location = new System.Drawing.Point(4, 71);
            lblChequeDate.Name = "lblChequeDate";
            lblChequeDate.Size = new System.Drawing.Size(70, 13);
            lblChequeDate.TabIndex = 4;
            lblChequeDate.Text = "Cheque Date";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label6.Location = new System.Drawing.Point(6, 13);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(83, 13);
            label6.TabIndex = 1;
            label6.Text = "Vacation Info";
            // 
            // Label12
            // 
            Label12.AutoSize = true;
            Label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label12.Location = new System.Drawing.Point(2, 79);
            Label12.Name = "Label12";
            Label12.Size = new System.Drawing.Size(67, 13);
            Label12.TabIndex = 12;
            Label12.Text = "Leave Pay";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(6, 202);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(86, 13);
            label1.TabIndex = 19;
            label1.Text = "Ticket Details";
            // 
            // lblRejoinDaate
            // 
            this.lblRejoinDaate.AutoSize = true;
            this.lblRejoinDaate.Location = new System.Drawing.Point(12, 47);
            this.lblRejoinDaate.Name = "lblRejoinDaate";
            this.lblRejoinDaate.Size = new System.Drawing.Size(63, 13);
            this.lblRejoinDaate.TabIndex = 4;
            this.lblRejoinDaate.Text = "Rejoin Date";
            // 
            // dtpRejoinDate
            // 
            this.dtpRejoinDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpRejoinDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRejoinDate.Location = new System.Drawing.Point(81, 41);
            this.dtpRejoinDate.Name = "dtpRejoinDate";
            this.dtpRejoinDate.Size = new System.Drawing.Size(109, 20);
            this.dtpRejoinDate.TabIndex = 5;
            this.dtpRejoinDate.ValueChanged += new System.EventHandler(this.dtpRejoinDate_ValueChanged);
            // 
            // chkEncashOnly
            // 
            this.chkEncashOnly.AutoSize = true;
            this.chkEncashOnly.Location = new System.Drawing.Point(15, 77);
            this.chkEncashOnly.Name = "chkEncashOnly";
            this.chkEncashOnly.Size = new System.Drawing.Size(86, 17);
            this.chkEncashOnly.TabIndex = 0;
            this.chkEncashOnly.Text = "Encash Only";
            this.chkEncashOnly.UseVisualStyleBackColor = true;
            this.chkEncashOnly.CheckedChanged += new System.EventHandler(this.chkEncashOnly_CheckedChanged);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // lblTotalEligibleLeavePayDays
            // 
            this.lblTotalEligibleLeavePayDays.AutoSize = true;
            this.lblTotalEligibleLeavePayDays.Location = new System.Drawing.Point(12, 99);
            this.lblTotalEligibleLeavePayDays.Name = "lblTotalEligibleLeavePayDays";
            this.lblTotalEligibleLeavePayDays.Size = new System.Drawing.Size(148, 13);
            this.lblTotalEligibleLeavePayDays.TabIndex = 13;
            this.lblTotalEligibleLeavePayDays.Text = "Total Eligible Leave Pay Days";
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.tabVacationProcess);
            this.pnlMain.Controls.Add(this.lblEmployee);
            this.pnlMain.Controls.Add(this.cboEmployee);
            this.pnlMain.Location = new System.Drawing.Point(4, 56);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(544, 490);
            this.pnlMain.TabIndex = 222;
            // 
            // tabVacationProcess
            // 
            this.tabVacationProcess.Controls.Add(this.tabEmployeeInfo);
            this.tabVacationProcess.Controls.Add(this.tabParticulars);
            this.tabVacationProcess.Controls.Add(this.tabHistory);
            this.tabVacationProcess.Location = new System.Drawing.Point(8, 40);
            this.tabVacationProcess.Name = "tabVacationProcess";
            this.tabVacationProcess.SelectedIndex = 0;
            this.tabVacationProcess.Size = new System.Drawing.Size(530, 445);
            this.tabVacationProcess.TabIndex = 1;
            // 
            // tabEmployeeInfo
            // 
            this.tabEmployeeInfo.Controls.Add(this.groupBox3);
            this.tabEmployeeInfo.Controls.Add(this.groupBox2);
            this.tabEmployeeInfo.Controls.Add(this.btnProcess);
            this.tabEmployeeInfo.Controls.Add(label6);
            this.tabEmployeeInfo.Controls.Add(this.shapeContainer2);
            this.tabEmployeeInfo.Location = new System.Drawing.Point(4, 22);
            this.tabEmployeeInfo.Name = "tabEmployeeInfo";
            this.tabEmployeeInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabEmployeeInfo.Size = new System.Drawing.Size(522, 419);
            this.tabEmployeeInfo.TabIndex = 0;
            this.tabEmployeeInfo.Text = "Vacation Info";
            this.tabEmployeeInfo.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtEncashComboOffDay);
            this.groupBox3.Controls.Add(label1);
            this.groupBox3.Controls.Add(this.lblTicketAmount);
            this.groupBox3.Controls.Add(this.lblIssuedTickets);
            this.groupBox3.Controls.Add(this.txtNoOfTicketsIssued);
            this.groupBox3.Controls.Add(this.txtTicketAmount);
            this.groupBox3.Controls.Add(this.lblAbsentDays);
            this.groupBox3.Controls.Add(this.lblAbsentDaysText);
            this.groupBox3.Controls.Add(Label12);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(lblProcessDate);
            this.groupBox3.Controls.Add(this.lblVacationDays);
            this.groupBox3.Controls.Add(this.lblTotalEligibleLeavePayDays);
            this.groupBox3.Controls.Add(this.lblExperienceDays);
            this.groupBox3.Controls.Add(this.lblTotalEligibleLeavePayDaysText);
            this.groupBox3.Controls.Add(this.txtEligibleLeavePayDays);
            this.groupBox3.Controls.Add(this.dtpProcessDate);
            this.groupBox3.Controls.Add(this.lblEligibleLeavePayDays);
            this.groupBox3.Controls.Add(this.lblExperienceDaysText);
            this.groupBox3.Controls.Add(this.dtpJoiningDate);
            this.groupBox3.Controls.Add(this.lblNoOfDays);
            this.groupBox3.Controls.Add(this.Label3);
            this.groupBox3.Controls.Add(this.lblEligibleLeavesText);
            this.groupBox3.Controls.Add(this.lblIsPaid);
            this.groupBox3.Controls.Add(this.lblEligibleLeaves);
            this.groupBox3.Controls.Add(this.lblAdditionLeaves);
            this.groupBox3.Controls.Add(this.lblAdditionalLeavesText);
            this.groupBox3.Controls.Add(this.shapeContainer3);
            this.groupBox3.Location = new System.Drawing.Point(15, 134);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(495, 252);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            // 
            // txtEncashComboOffDay
            // 
            this.txtEncashComboOffDay.Location = new System.Drawing.Point(123, 170);
            this.txtEncashComboOffDay.Name = "txtEncashComboOffDay";
            this.txtEncashComboOffDay.Size = new System.Drawing.Size(96, 20);
            this.txtEncashComboOffDay.TabIndex = 1053;
            this.txtEncashComboOffDay.Visible = false;
            // 
            // lblTicketAmount
            // 
            this.lblTicketAmount.AutoSize = true;
            this.lblTicketAmount.Location = new System.Drawing.Point(276, 228);
            this.lblTicketAmount.Name = "lblTicketAmount";
            this.lblTicketAmount.Size = new System.Drawing.Size(81, 13);
            this.lblTicketAmount.TabIndex = 22;
            this.lblTicketAmount.Text = "Ticket Expense";
            // 
            // lblIssuedTickets
            // 
            this.lblIssuedTickets.AutoSize = true;
            this.lblIssuedTickets.Location = new System.Drawing.Point(12, 228);
            this.lblIssuedTickets.Name = "lblIssuedTickets";
            this.lblIssuedTickets.Size = new System.Drawing.Size(107, 13);
            this.lblIssuedTickets.TabIndex = 20;
            this.lblIssuedTickets.Text = "No Of Tickets Issued";
            // 
            // txtNoOfTicketsIssued
            // 
            this.txtNoOfTicketsIssued.Location = new System.Drawing.Point(122, 225);
            this.txtNoOfTicketsIssued.MaxLength = 9;
            this.txtNoOfTicketsIssued.Name = "txtNoOfTicketsIssued";
            this.txtNoOfTicketsIssued.Size = new System.Drawing.Size(112, 20);
            this.txtNoOfTicketsIssued.TabIndex = 21;
            this.txtNoOfTicketsIssued.TextChanged += new System.EventHandler(this.txtNoOfTicketsIssued_TextChanged);
            this.txtNoOfTicketsIssued.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoOfTicketsIssued_KeyPress);
            // 
            // txtTicketAmount
            // 
            this.txtTicketAmount.Location = new System.Drawing.Point(360, 225);
            this.txtTicketAmount.MaxLength = 9;
            this.txtTicketAmount.Name = "txtTicketAmount";
            this.txtTicketAmount.Size = new System.Drawing.Size(109, 20);
            this.txtTicketAmount.TabIndex = 23;
            this.txtTicketAmount.TextChanged += new System.EventHandler(this.txtTicketAmount_TextChanged);
            this.txtTicketAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTicketAmount_KeyPress);
            // 
            // lblAbsentDays
            // 
            this.lblAbsentDays.AutoSize = true;
            this.lblAbsentDays.Location = new System.Drawing.Point(12, 147);
            this.lblAbsentDays.Name = "lblAbsentDays";
            this.lblAbsentDays.Size = new System.Drawing.Size(67, 13);
            this.lblAbsentDays.TabIndex = 16;
            this.lblAbsentDays.Text = "Absent Days";
            // 
            // lblAbsentDaysText
            // 
            this.lblAbsentDaysText.AutoSize = true;
            this.lblAbsentDaysText.Location = new System.Drawing.Point(119, 147);
            this.lblAbsentDaysText.Name = "lblAbsentDaysText";
            this.lblAbsentDaysText.Size = new System.Drawing.Size(13, 13);
            this.lblAbsentDaysText.TabIndex = 17;
            this.lblAbsentDaysText.Text = "0";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(lblAccount);
            this.groupBox4.Controls.Add(this.txtChequeNo);
            this.groupBox4.Controls.Add(this.cboAccount);
            this.groupBox4.Controls.Add(this.cboTransactionType);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(lblChequeDate);
            this.groupBox4.Controls.Add(lblCheqNo);
            this.groupBox4.Controls.Add(this.dtpChequeDate);
            this.groupBox4.Location = new System.Drawing.Point(237, 84);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(251, 121);
            this.groupBox4.TabIndex = 1050;
            this.groupBox4.TabStop = false;
            // 
            // txtChequeNo
            // 
            this.txtChequeNo.Location = new System.Drawing.Point(73, 44);
            this.txtChequeNo.MaxLength = 99;
            this.txtChequeNo.Name = "txtChequeNo";
            this.txtChequeNo.Size = new System.Drawing.Size(157, 20);
            this.txtChequeNo.TabIndex = 3;
            this.txtChequeNo.TextChanged += new System.EventHandler(this.txtChequeNo_TextChanged);
            // 
            // cboAccount
            // 
            this.cboAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAccount.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cboAccount.DropDownHeight = 134;
            this.cboAccount.FormattingEnabled = true;
            this.cboAccount.IntegralHeight = false;
            this.cboAccount.Location = new System.Drawing.Point(73, 96);
            this.cboAccount.Name = "cboAccount";
            this.cboAccount.Size = new System.Drawing.Size(159, 21);
            this.cboAccount.TabIndex = 1042;
            this.cboAccount.SelectedIndexChanged += new System.EventHandler(this.cboAccount_SelectedIndexChanged);
            this.cboAccount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboAccount_KeyDown);
            // 
            // cboTransactionType
            // 
            this.cboTransactionType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTransactionType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTransactionType.BackColor = System.Drawing.SystemColors.Info;
            this.cboTransactionType.DropDownHeight = 134;
            this.cboTransactionType.FormattingEnabled = true;
            this.cboTransactionType.IntegralHeight = false;
            this.cboTransactionType.Location = new System.Drawing.Point(73, 16);
            this.cboTransactionType.Name = "cboTransactionType";
            this.cboTransactionType.Size = new System.Drawing.Size(158, 21);
            this.cboTransactionType.TabIndex = 1;
            this.cboTransactionType.SelectedIndexChanged += new System.EventHandler(this.cboTransactionType_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Trans: Type";
            // 
            // dtpChequeDate
            // 
            this.dtpChequeDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpChequeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpChequeDate.Location = new System.Drawing.Point(73, 70);
            this.dtpChequeDate.Name = "dtpChequeDate";
            this.dtpChequeDate.Size = new System.Drawing.Size(85, 20);
            this.dtpChequeDate.TabIndex = 5;
            this.dtpChequeDate.ValueChanged += new System.EventHandler(this.dtpChequeDate_ValueChanged);
            // 
            // lblVacationDays
            // 
            this.lblVacationDays.AutoSize = true;
            this.lblVacationDays.Location = new System.Drawing.Point(12, 54);
            this.lblVacationDays.Name = "lblVacationDays";
            this.lblVacationDays.Size = new System.Drawing.Size(76, 13);
            this.lblVacationDays.TabIndex = 2;
            this.lblVacationDays.Text = "Vacation Days";
            // 
            // lblExperienceDays
            // 
            this.lblExperienceDays.AutoSize = true;
            this.lblExperienceDays.Location = new System.Drawing.Point(328, 47);
            this.lblExperienceDays.Name = "lblExperienceDays";
            this.lblExperienceDays.Size = new System.Drawing.Size(87, 13);
            this.lblExperienceDays.TabIndex = 10;
            this.lblExperienceDays.Text = "Experience Days";
            // 
            // lblTotalEligibleLeavePayDaysText
            // 
            this.lblTotalEligibleLeavePayDaysText.AutoSize = true;
            this.lblTotalEligibleLeavePayDaysText.Location = new System.Drawing.Point(185, 99);
            this.lblTotalEligibleLeavePayDaysText.Name = "lblTotalEligibleLeavePayDaysText";
            this.lblTotalEligibleLeavePayDaysText.Size = new System.Drawing.Size(13, 13);
            this.lblTotalEligibleLeavePayDaysText.TabIndex = 18;
            this.lblTotalEligibleLeavePayDaysText.Text = "0";
            // 
            // txtEligibleLeavePayDays
            // 
            this.txtEligibleLeavePayDays.Location = new System.Drawing.Point(122, 120);
            this.txtEligibleLeavePayDays.MaxLength = 6;
            this.txtEligibleLeavePayDays.Name = "txtEligibleLeavePayDays";
            this.txtEligibleLeavePayDays.Size = new System.Drawing.Size(44, 20);
            this.txtEligibleLeavePayDays.TabIndex = 15;
            this.txtEligibleLeavePayDays.TextChanged += new System.EventHandler(this.txtEligibleLeavePayDays_TextChanged);
            this.txtEligibleLeavePayDays.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEligibleLeavePayDays_KeyPress);
            // 
            // dtpProcessDate
            // 
            this.dtpProcessDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpProcessDate.Enabled = false;
            this.dtpProcessDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpProcessDate.Location = new System.Drawing.Point(206, 48);
            this.dtpProcessDate.Name = "dtpProcessDate";
            this.dtpProcessDate.Size = new System.Drawing.Size(100, 20);
            this.dtpProcessDate.TabIndex = 7;
            this.dtpProcessDate.TabStop = false;
            this.dtpProcessDate.Visible = false;
            this.dtpProcessDate.ValueChanged += new System.EventHandler(this.dtpProcessDate_ValueChanged);
            // 
            // lblEligibleLeavePayDays
            // 
            this.lblEligibleLeavePayDays.AutoSize = true;
            this.lblEligibleLeavePayDays.Location = new System.Drawing.Point(12, 124);
            this.lblEligibleLeavePayDays.Name = "lblEligibleLeavePayDays";
            this.lblEligibleLeavePayDays.Size = new System.Drawing.Size(85, 13);
            this.lblEligibleLeavePayDays.TabIndex = 14;
            this.lblEligibleLeavePayDays.Text = "Leave Pay Days";
            // 
            // lblExperienceDaysText
            // 
            this.lblExperienceDaysText.AutoSize = true;
            this.lblExperienceDaysText.Location = new System.Drawing.Point(419, 47);
            this.lblExperienceDaysText.Name = "lblExperienceDaysText";
            this.lblExperienceDaysText.Size = new System.Drawing.Size(13, 13);
            this.lblExperienceDaysText.TabIndex = 11;
            this.lblExperienceDaysText.Text = "0";
            // 
            // dtpJoiningDate
            // 
            this.dtpJoiningDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpJoiningDate.Enabled = false;
            this.dtpJoiningDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpJoiningDate.Location = new System.Drawing.Point(207, 19);
            this.dtpJoiningDate.Name = "dtpJoiningDate";
            this.dtpJoiningDate.Size = new System.Drawing.Size(100, 20);
            this.dtpJoiningDate.TabIndex = 5;
            this.dtpJoiningDate.TabStop = false;
            // 
            // lblNoOfDays
            // 
            this.lblNoOfDays.AutoSize = true;
            this.lblNoOfDays.Location = new System.Drawing.Point(88, 54);
            this.lblNoOfDays.Name = "lblNoOfDays";
            this.lblNoOfDays.Size = new System.Drawing.Size(13, 13);
            this.lblNoOfDays.TabIndex = 3;
            this.lblNoOfDays.Text = "0";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(132, 23);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(66, 13);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "Joining Date";
            // 
            // lblEligibleLeavesText
            // 
            this.lblEligibleLeavesText.AutoSize = true;
            this.lblEligibleLeavesText.Location = new System.Drawing.Point(88, 25);
            this.lblEligibleLeavesText.Name = "lblEligibleLeavesText";
            this.lblEligibleLeavesText.Size = new System.Drawing.Size(13, 13);
            this.lblEligibleLeavesText.TabIndex = 1;
            this.lblEligibleLeavesText.Text = "0";
            // 
            // lblIsPaid
            // 
            this.lblIsPaid.AutoSize = true;
            this.lblIsPaid.Location = new System.Drawing.Point(162, 109);
            this.lblIsPaid.Name = "lblIsPaid";
            this.lblIsPaid.Size = new System.Drawing.Size(0, 13);
            this.lblIsPaid.TabIndex = 1022;
            this.lblIsPaid.Visible = false;
            // 
            // lblEligibleLeaves
            // 
            this.lblEligibleLeaves.AutoSize = true;
            this.lblEligibleLeaves.Location = new System.Drawing.Point(12, 25);
            this.lblEligibleLeaves.Name = "lblEligibleLeaves";
            this.lblEligibleLeaves.Size = new System.Drawing.Size(78, 13);
            this.lblEligibleLeaves.TabIndex = 0;
            this.lblEligibleLeaves.Text = "Eligible Leaves";
            // 
            // lblAdditionLeaves
            // 
            this.lblAdditionLeaves.AutoSize = true;
            this.lblAdditionLeaves.Location = new System.Drawing.Point(328, 19);
            this.lblAdditionLeaves.Name = "lblAdditionLeaves";
            this.lblAdditionLeaves.Size = new System.Drawing.Size(79, 13);
            this.lblAdditionLeaves.TabIndex = 8;
            this.lblAdditionLeaves.Text = "Extended Days";
            // 
            // lblAdditionalLeavesText
            // 
            this.lblAdditionalLeavesText.AutoSize = true;
            this.lblAdditionalLeavesText.Location = new System.Drawing.Point(419, 19);
            this.lblAdditionalLeavesText.Name = "lblAdditionalLeavesText";
            this.lblAdditionalLeavesText.Size = new System.Drawing.Size(13, 13);
            this.lblAdditionalLeavesText.TabIndex = 9;
            this.lblAdditionalLeavesText.Text = "0";
            // 
            // shapeContainer3
            // 
            this.shapeContainer3.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer3.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer3.Name = "shapeContainer3";
            this.shapeContainer3.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape4});
            this.shapeContainer3.Size = new System.Drawing.Size(489, 233);
            this.shapeContainer3.TabIndex = 1052;
            this.shapeContainer3.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 63;
            this.lineShape2.X2 = 486;
            this.lineShape2.Y1 = 198;
            this.lineShape2.Y2 = 198;
            // 
            // lineShape4
            // 
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 33;
            this.lineShape4.X2 = 199;
            this.lineShape4.Y1 = 71;
            this.lineShape4.Y2 = 71;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkProcessSalaryForCurrentMonth);
            this.groupBox2.Controls.Add(this.chkConsiderAbsentDays);
            this.groupBox2.Controls.Add(this.lblActualRejoinDate);
            this.groupBox2.Controls.Add(this.lblActualRejoinDateDec);
            this.groupBox2.Controls.Add(this.chkEncashOnly);
            this.groupBox2.Controls.Add(this.dtpToDate);
            this.groupBox2.Controls.Add(this.dtpRejoinDate);
            this.groupBox2.Controls.Add(this.lblRejoinDaate);
            this.groupBox2.Controls.Add(this.lblFromDate);
            this.groupBox2.Controls.Add(this.lblToDate);
            this.groupBox2.Controls.Add(this.dtpFromDate);
            this.groupBox2.Location = new System.Drawing.Point(16, 28);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(494, 100);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // chkProcessSalaryForCurrentMonth
            // 
            this.chkProcessSalaryForCurrentMonth.AutoSize = true;
            this.chkProcessSalaryForCurrentMonth.Location = new System.Drawing.Point(236, 77);
            this.chkProcessSalaryForCurrentMonth.Name = "chkProcessSalaryForCurrentMonth";
            this.chkProcessSalaryForCurrentMonth.Size = new System.Drawing.Size(249, 17);
            this.chkProcessSalaryForCurrentMonth.TabIndex = 2;
            this.chkProcessSalaryForCurrentMonth.Text = "Process Salary For CurrentMonth Worked Days";
            this.chkProcessSalaryForCurrentMonth.UseVisualStyleBackColor = true;
            this.chkProcessSalaryForCurrentMonth.CheckedChanged += new System.EventHandler(this.chkProcessSalaryForCurrentMonth_CheckedChanged);
            // 
            // chkConsiderAbsentDays
            // 
            this.chkConsiderAbsentDays.AutoSize = true;
            this.chkConsiderAbsentDays.Location = new System.Drawing.Point(103, 77);
            this.chkConsiderAbsentDays.Name = "chkConsiderAbsentDays";
            this.chkConsiderAbsentDays.Size = new System.Drawing.Size(130, 17);
            this.chkConsiderAbsentDays.TabIndex = 1;
            this.chkConsiderAbsentDays.Text = "Consider Absent Days";
            this.chkConsiderAbsentDays.UseVisualStyleBackColor = true;
            this.chkConsiderAbsentDays.CheckedChanged += new System.EventHandler(this.chkConsiderAbsentDays_CheckedChanged);
            // 
            // lblActualRejoinDate
            // 
            this.lblActualRejoinDate.AutoSize = true;
            this.lblActualRejoinDate.Location = new System.Drawing.Point(356, 48);
            this.lblActualRejoinDate.Name = "lblActualRejoinDate";
            this.lblActualRejoinDate.Size = new System.Drawing.Size(0, 13);
            this.lblActualRejoinDate.TabIndex = 7;
            // 
            // lblActualRejoinDateDec
            // 
            this.lblActualRejoinDateDec.AutoSize = true;
            this.lblActualRejoinDateDec.Location = new System.Drawing.Point(264, 47);
            this.lblActualRejoinDateDec.Name = "lblActualRejoinDateDec";
            this.lblActualRejoinDateDec.Size = new System.Drawing.Size(96, 13);
            this.lblActualRejoinDateDec.TabIndex = 6;
            this.lblActualRejoinDateDec.Text = "Actual Rejoin Date";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(359, 14);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(109, 20);
            this.dtpToDate.TabIndex = 3;
            this.dtpToDate.ValueChanged += new System.EventHandler(this.dtpToDate_ValueChanged);
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Location = new System.Drawing.Point(12, 17);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(56, 13);
            this.lblFromDate.TabIndex = 0;
            this.lblFromDate.Text = "From Date";
            // 
            // lblToDate
            // 
            this.lblToDate.AutoSize = true;
            this.lblToDate.Location = new System.Drawing.Point(264, 17);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(46, 13);
            this.lblToDate.TabIndex = 2;
            this.lblToDate.Text = "To Date";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(81, 14);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(109, 20);
            this.dtpFromDate.TabIndex = 1;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            // 
            // btnProcess
            // 
            this.btnProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.Location = new System.Drawing.Point(425, 392);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(85, 25);
            this.btnProcess.TabIndex = 5;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3});
            this.shapeContainer2.Size = new System.Drawing.Size(516, 413);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 84;
            this.lineShape3.X2 = 503;
            this.lineShape3.Y1 = 18;
            this.lineShape3.Y2 = 18;
            // 
            // tabParticulars
            // 
            this.tabParticulars.Controls.Add(lblDeductions);
            this.tabParticulars.Controls.Add(this.TextboxNumeric1);
            this.tabParticulars.Controls.Add(lblGivenAmount);
            this.tabParticulars.Controls.Add(this.TextboxNumeric);
            this.tabParticulars.Controls.Add(this.txtGivenAmount);
            this.tabParticulars.Controls.Add(this.lblRemarks);
            this.tabParticulars.Controls.Add(this.txtRemarks);
            this.tabParticulars.Controls.Add(lblNetAmount);
            this.tabParticulars.Controls.Add(lblAdditions);
            this.tabParticulars.Controls.Add(this.txtNetAmount);
            this.tabParticulars.Controls.Add(this.txtTotalAddition);
            this.tabParticulars.Controls.Add(this.txtTotalDeduction);
            this.tabParticulars.Controls.Add(this.Label4);
            this.tabParticulars.Controls.Add(this.lblCurrency);
            this.tabParticulars.Controls.Add(this.dgvVacationDetails);
            this.tabParticulars.Controls.Add(lblParticulars);
            this.tabParticulars.Controls.Add(this.btnAdditionDeductions);
            this.tabParticulars.Controls.Add(this.txtAmount);
            this.tabParticulars.Controls.Add(lblAmount);
            this.tabParticulars.Controls.Add(this.btnAdd);
            this.tabParticulars.Controls.Add(this.cboParticulars);
            this.tabParticulars.Controls.Add(Label2);
            this.tabParticulars.Controls.Add(this.shapeContainer4);
            this.tabParticulars.Location = new System.Drawing.Point(4, 22);
            this.tabParticulars.Name = "tabParticulars";
            this.tabParticulars.Padding = new System.Windows.Forms.Padding(3);
            this.tabParticulars.Size = new System.Drawing.Size(522, 419);
            this.tabParticulars.TabIndex = 1;
            this.tabParticulars.Text = "Particulars";
            this.tabParticulars.UseVisualStyleBackColor = true;
            // 
            // TextboxNumeric1
            // 
            this.TextboxNumeric1.Location = new System.Drawing.Point(304, 339);
            this.TextboxNumeric1.Name = "TextboxNumeric1";
            this.TextboxNumeric1.Size = new System.Drawing.Size(29, 20);
            this.TextboxNumeric1.TabIndex = 1009;
            this.TextboxNumeric1.Visible = false;
            // 
            // TextboxNumeric
            // 
            this.TextboxNumeric.Location = new System.Drawing.Point(385, 339);
            this.TextboxNumeric.Name = "TextboxNumeric";
            this.TextboxNumeric.Size = new System.Drawing.Size(29, 20);
            this.TextboxNumeric.TabIndex = 1008;
            this.TextboxNumeric.Visible = false;
            // 
            // txtGivenAmount
            // 
            this.txtGivenAmount.Enabled = false;
            this.txtGivenAmount.Location = new System.Drawing.Point(430, 262);
            this.txtGivenAmount.Name = "txtGivenAmount";
            this.txtGivenAmount.Size = new System.Drawing.Size(85, 20);
            this.txtGivenAmount.TabIndex = 16;
            this.txtGivenAmount.TabStop = false;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(4, 299);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 17;
            this.lblRemarks.Text = "Remarks";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(68, 302);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRemarks.Size = new System.Drawing.Size(448, 78);
            this.txtRemarks.TabIndex = 18;
            this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
            // 
            // txtNetAmount
            // 
            this.txtNetAmount.Enabled = false;
            this.txtNetAmount.Location = new System.Drawing.Point(256, 262);
            this.txtNetAmount.Name = "txtNetAmount";
            this.txtNetAmount.Size = new System.Drawing.Size(89, 20);
            this.txtNetAmount.TabIndex = 14;
            this.txtNetAmount.TabStop = false;
            // 
            // txtTotalAddition
            // 
            this.txtTotalAddition.Enabled = false;
            this.txtTotalAddition.Location = new System.Drawing.Point(256, 235);
            this.txtTotalAddition.Name = "txtTotalAddition";
            this.txtTotalAddition.Size = new System.Drawing.Size(89, 20);
            this.txtTotalAddition.TabIndex = 10;
            this.txtTotalAddition.TabStop = false;
            // 
            // txtTotalDeduction
            // 
            this.txtTotalDeduction.Enabled = false;
            this.txtTotalDeduction.Location = new System.Drawing.Point(429, 235);
            this.txtTotalDeduction.Name = "txtTotalDeduction";
            this.txtTotalDeduction.Size = new System.Drawing.Size(85, 20);
            this.txtTotalDeduction.TabIndex = 12;
            this.txtTotalDeduction.TabStop = false;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(4, 238);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(49, 13);
            this.Label4.TabIndex = 7;
            this.Label4.Text = "Currency";
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Location = new System.Drawing.Point(69, 238);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(26, 13);
            this.lblCurrency.TabIndex = 8;
            this.lblCurrency.Text = "INR";
            // 
            // dgvVacationDetails
            // 
            this.dgvVacationDetails.AllowUserToAddRows = false;
            this.dgvVacationDetails.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvVacationDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVacationDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColVacationDetailID,
            this.dgvColParticulars,
            this.dgvColAmount,
            this.dgvColIsAddition,
            this.dgvColNoOfDays,
            this.dgvColReleaseLater,
            this.dgvColTempAmount});
            this.dgvVacationDetails.Location = new System.Drawing.Point(6, 77);
            this.dgvVacationDetails.MultiSelect = false;
            this.dgvVacationDetails.Name = "dgvVacationDetails";
            this.dgvVacationDetails.RowHeadersWidth = 35;
            this.dgvVacationDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvVacationDetails.Size = new System.Drawing.Size(510, 152);
            this.dgvVacationDetails.TabIndex = 6;
            this.dgvVacationDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVacationDetails_CellValueChanged);
            this.dgvVacationDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvVacationDetails_CellBeginEdit);
            this.dgvVacationDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVacationDetails_CellEndEdit);
            this.dgvVacationDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvVacationDetails_EditingControlShowing);
            this.dgvVacationDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvVacationDetails_CurrentCellDirtyStateChanged);
            this.dgvVacationDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvVacationDetails_DataError);
            this.dgvVacationDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvVacationDetails_KeyDown);
            this.dgvVacationDetails.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvVacationDetails_RowsRemoved);
            // 
            // dgvColVacationDetailID
            // 
            this.dgvColVacationDetailID.HeaderText = "VacationDetailID";
            this.dgvColVacationDetailID.Name = "dgvColVacationDetailID";
            this.dgvColVacationDetailID.Visible = false;
            // 
            // dgvColParticulars
            // 
            this.dgvColParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvColParticulars.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColParticulars.HeaderText = "Particular";
            this.dgvColParticulars.Name = "dgvColParticulars";
            this.dgvColParticulars.ReadOnly = true;
            this.dgvColParticulars.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dgvColAmount
            // 
            this.dgvColAmount.HeaderText = "Amount";
            this.dgvColAmount.MaxInputLength = 8;
            this.dgvColAmount.Name = "dgvColAmount";
            // 
            // dgvColIsAddition
            // 
            this.dgvColIsAddition.HeaderText = "IsAddition";
            this.dgvColIsAddition.Name = "dgvColIsAddition";
            this.dgvColIsAddition.Visible = false;
            // 
            // dgvColNoOfDays
            // 
            this.dgvColNoOfDays.HeaderText = "NoOfDays";
            this.dgvColNoOfDays.Name = "dgvColNoOfDays";
            // 
            // dgvColReleaseLater
            // 
            this.dgvColReleaseLater.HeaderText = "Release Later";
            this.dgvColReleaseLater.Name = "dgvColReleaseLater";
            this.dgvColReleaseLater.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvColReleaseLater.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dgvColReleaseLater.Width = 80;
            // 
            // dgvColTempAmount
            // 
            this.dgvColTempAmount.HeaderText = "Amount";
            this.dgvColTempAmount.MaxInputLength = 20;
            this.dgvColTempAmount.Name = "dgvColTempAmount";
            this.dgvColTempAmount.Visible = false;
            // 
            // btnAdditionDeductions
            // 
            this.btnAdditionDeductions.Location = new System.Drawing.Point(232, 39);
            this.btnAdditionDeductions.Name = "btnAdditionDeductions";
            this.btnAdditionDeductions.Size = new System.Drawing.Size(30, 24);
            this.btnAdditionDeductions.TabIndex = 2;
            this.btnAdditionDeductions.Text = "...";
            this.btnAdditionDeductions.UseVisualStyleBackColor = true;
            this.btnAdditionDeductions.Click += new System.EventHandler(this.btnAdditionDeductions_Click);
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(351, 42);
            this.txtAmount.MaxLength = 8;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(108, 20);
            this.txtAmount.TabIndex = 4;
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(465, 39);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(50, 24);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cboParticulars
            // 
            this.cboParticulars.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboParticulars.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboParticulars.BackColor = System.Drawing.Color.White;
            this.cboParticulars.DropDownHeight = 134;
            this.cboParticulars.FormattingEnabled = true;
            this.cboParticulars.IntegralHeight = false;
            this.cboParticulars.Location = new System.Drawing.Point(74, 42);
            this.cboParticulars.Name = "cboParticulars";
            this.cboParticulars.Size = new System.Drawing.Size(156, 21);
            this.cboParticulars.TabIndex = 1;
            // 
            // shapeContainer4
            // 
            this.shapeContainer4.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer4.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer4.Name = "shapeContainer4";
            this.shapeContainer4.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.shapeContainer4.Size = new System.Drawing.Size(516, 413);
            this.shapeContainer4.TabIndex = 0;
            this.shapeContainer4.TabStop = false;
            // 
            // LineShape1
            // 
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 10;
            this.LineShape1.X2 = 509;
            this.LineShape1.Y1 = 22;
            this.LineShape1.Y2 = 22;
            // 
            // tabHistory
            // 
            this.tabHistory.Controls.Add(this.dgvEmpHistory);
            this.tabHistory.Location = new System.Drawing.Point(4, 22);
            this.tabHistory.Name = "tabHistory";
            this.tabHistory.Size = new System.Drawing.Size(522, 419);
            this.tabHistory.TabIndex = 2;
            this.tabHistory.Text = "Vacation History";
            this.tabHistory.UseVisualStyleBackColor = true;
            // 
            // dgvEmpHistory
            // 
            this.dgvEmpHistory.AllowUserToAddRows = false;
            this.dgvEmpHistory.AllowUserToDeleteRows = false;
            this.dgvEmpHistory.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvEmpHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmpHistory.Location = new System.Drawing.Point(3, 3);
            this.dgvEmpHistory.Name = "dgvEmpHistory";
            this.dgvEmpHistory.ReadOnly = true;
            this.dgvEmpHistory.RowHeadersVisible = false;
            this.dgvEmpHistory.Size = new System.Drawing.Size(516, 396);
            this.dgvEmpHistory.TabIndex = 1052;
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            this.lblEmployee.Location = new System.Drawing.Point(9, 16);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(53, 13);
            this.lblEmployee.TabIndex = 176;
            this.lblEmployee.Text = "Employee";
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(78, 13);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(366, 21);
            this.cboEmployee.TabIndex = 0;
            this.cboEmployee.SelectedIndexChanged += new System.EventHandler(this.cboEmployee_SelectedIndexChanged);
            this.cboEmployee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboEmployee_KeyDown);
            // 
            // errorProviderVacation
            // 
            this.errorProviderVacation.ContainerControl = this;
            this.errorProviderVacation.RightToLeft = true;
            // 
            // tmrVacation
            // 
            this.tmrVacation.Interval = 2000;
            this.tmrVacation.Tick += new System.EventHandler(this.tmrVacation_Tick);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(4, 552);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // ssVacation
            // 
            this.ssVacation.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.lblstatus});
            this.ssVacation.Location = new System.Drawing.Point(0, 586);
            this.ssVacation.Name = "ssVacation";
            this.ssVacation.Size = new System.Drawing.Size(551, 22);
            this.ssVacation.TabIndex = 227;
            this.ssVacation.Text = "StatusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(467, 552);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(386, 552);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "C&onfirm ";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnEmail
            // 
            this.bnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnEmail.Image = ((System.Drawing.Image)(resources.GetObject("bnEmail.Image")));
            this.bnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Size = new System.Drawing.Size(23, 22);
            this.bnEmail.ToolTipText = "Email";
            this.bnEmail.Click += new System.EventHandler(this.bnEmail_Click);
            // 
            // bnPrint
            // 
            this.bnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnPrint.Image = ((System.Drawing.Image)(resources.GetObject("bnPrint.Image")));
            this.bnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(23, 22);
            this.bnPrint.ToolTipText = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // bnPositionItem
            // 
            this.bnPositionItem.AccessibleName = "Position";
            this.bnPositionItem.AutoSize = false;
            this.bnPositionItem.Name = "bnPositionItem";
            this.bnPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bnPositionItem.Text = "0";
            this.bnPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewItem.Text = "Add new";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorVacationProcess
            // 
            this.BindingNavigatorVacationProcess.AddNewItem = null;
            this.BindingNavigatorVacationProcess.CountItem = null;
            this.BindingNavigatorVacationProcess.DeleteItem = null;
            this.BindingNavigatorVacationProcess.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.bnPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnDeleteItem,
            this.bnClearItem,
            this.ToolStripSeparator3,
            this.bnVacationExtension,
            this.ToolStripSeparator1,
            this.bnPrint,
            this.bnEmail,
            this.ToolStripSeparator2,
            this.BtnHelp});
            this.BindingNavigatorVacationProcess.Location = new System.Drawing.Point(0, 0);
            this.BindingNavigatorVacationProcess.MoveFirstItem = null;
            this.BindingNavigatorVacationProcess.MoveLastItem = null;
            this.BindingNavigatorVacationProcess.MoveNextItem = null;
            this.BindingNavigatorVacationProcess.MovePreviousItem = null;
            this.BindingNavigatorVacationProcess.Name = "BindingNavigatorVacationProcess";
            this.BindingNavigatorVacationProcess.PositionItem = null;
            this.BindingNavigatorVacationProcess.Size = new System.Drawing.Size(551, 25);
            this.BindingNavigatorVacationProcess.TabIndex = 223;
            this.BindingNavigatorVacationProcess.Text = "BindingNavigator1";
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bnSaveItem.Text = "Save Data";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bnDeleteItem.Image")));
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.Text = "Delete";
            this.bnDeleteItem.Click += new System.EventHandler(this.bnDeleteItem_Click);
            // 
            // bnClearItem
            // 
            this.bnClearItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnClearItem.Image = ((System.Drawing.Image)(resources.GetObject("bnClearItem.Image")));
            this.bnClearItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnClearItem.Name = "bnClearItem";
            this.bnClearItem.Size = new System.Drawing.Size(23, 22);
            this.bnClearItem.ToolTipText = "Clear";
            this.bnClearItem.Click += new System.EventHandler(this.bnClearItem_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // bnVacationExtension
            // 
            this.bnVacationExtension.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnVacationExtension.Image = global::MyBooksERP.Properties.Resources.VacationEs;
            this.bnVacationExtension.Name = "bnVacationExtension";
            this.bnVacationExtension.RightToLeftAutoMirrorImage = true;
            this.bnVacationExtension.Size = new System.Drawing.Size(23, 22);
            this.bnVacationExtension.Text = "0";
            this.bnVacationExtension.ToolTipText = "Vacation Extension / Confirm Rejoin Date";
            this.bnVacationExtension.Click += new System.EventHandler(this.bnVacationExtension_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(551, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 1044;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(311, 23);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyBooksERP.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "VacationDetailID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 20;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "IsAddition";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "NoOfDays";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "ReleaseLater";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 20;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            this.dataGridViewTextBoxColumn6.Width = 175;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "From Date";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 90;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "To Date";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 90;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn9.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // FrmVacationEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 608);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ssVacation);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.BindingNavigatorVacationProcess);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(567, 644);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(567, 644);
            this.Name = "FrmVacationEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vacation Process";
            this.Load += new System.EventHandler(this.FrmVacationEntry_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmVacationEntry_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmVacationEntry_KeyDown);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.tabVacationProcess.ResumeLayout(false);
            this.tabEmployeeInfo.ResumeLayout(false);
            this.tabEmployeeInfo.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabParticulars.ResumeLayout(false);
            this.tabParticulars.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVacationDetails)).EndInit();
            this.tabHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmpHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderVacation)).EndInit();
            this.ssVacation.ResumeLayout(false);
            this.ssVacation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigatorVacationProcess)).EndInit();
            this.BindingNavigatorVacationProcess.ResumeLayout(false);
            this.BindingNavigatorVacationProcess.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        //internal System.Windows.Forms.DataGridViewTextBoxColumn dgvColAmount;
        internal System.Windows.Forms.Label lblRejoinDaate;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dgvColIsAddition;
        internal System.Windows.Forms.DateTimePicker dtpRejoinDate;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dgvColNoOfDays;
        //internal System.Windows.Forms.DataGridViewComboBoxColumn dgvColParticulars;
        internal System.Windows.Forms.CheckBox chkEncashOnly;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dgvColVacationDetailID;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.Label lblTotalEligibleLeavePayDays;
        internal System.Windows.Forms.Panel pnlMain;
        internal System.Windows.Forms.Label lblIsPaid;
        internal System.Windows.Forms.Label lblTotalEligibleLeavePayDaysText;
        internal System.Windows.Forms.TextBox txtEligibleLeavePayDays;
        internal System.Windows.Forms.Label lblEligibleLeavePayDays;
        internal System.Windows.Forms.CheckBox chkConsiderAbsentDays;
        internal System.Windows.Forms.Label lblAbsentDays;
        internal System.Windows.Forms.Label lblAbsentDaysText;
        internal System.Windows.Forms.Label lblExperienceDays;
        internal System.Windows.Forms.Label lblExperienceDaysText;
        internal System.Windows.Forms.TextBox txtGivenAmount;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox txtTicketAmount;
        internal System.Windows.Forms.Label lblTicketAmount;
        internal System.Windows.Forms.Label lblCurrency;
        internal System.Windows.Forms.DateTimePicker dtpJoiningDate;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.CheckBox chkProcessSalaryForCurrentMonth;
        internal System.Windows.Forms.ComboBox cboParticulars;
        internal System.Windows.Forms.Label lblAdditionLeaves;
        internal System.Windows.Forms.Label lblAdditionalLeavesText;
        internal System.Windows.Forms.TextBox txtNoOfTicketsIssued;
        internal System.Windows.Forms.Label lblIssuedTickets;
        internal System.Windows.Forms.Label lblEligibleLeaves;
        internal System.Windows.Forms.Label lblEligibleLeavesText;
        internal System.Windows.Forms.Label lblVacationDays;
        internal System.Windows.Forms.TextBox txtNetAmount;
        internal System.Windows.Forms.TextBox txtTotalDeduction;
        internal System.Windows.Forms.TextBox txtTotalAddition;
        internal System.Windows.Forms.Label lblEmployee;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal System.Windows.Forms.Label lblFromDate;
        internal System.Windows.Forms.Label lblToDate;
        internal System.Windows.Forms.DateTimePicker dtpFromDate;
        internal System.Windows.Forms.DateTimePicker dtpToDate;
        internal System.Windows.Forms.Label lblNoOfDays;
        internal System.Windows.Forms.DateTimePicker dtpProcessDate;
        internal System.Windows.Forms.Button btnAdd;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.Label lblRemarks;
        internal System.Windows.Forms.TextBox txtAmount;
        internal System.Windows.Forms.Button btnProcess;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.TextBox TextboxNumeric;
        internal System.Windows.Forms.TextBox TextboxNumeric1;
        //internal System.Windows.Forms.DataGridViewCheckBoxColumn dgvColReleaseLater;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dgvColTempAmount;
        internal System.Windows.Forms.ErrorProvider errorProviderVacation;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.StatusStrip ssVacation;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOK;
        internal System.Windows.Forms.BindingNavigator BindingNavigatorVacationProcess;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox bnPositionItem;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton bnAddNewItem;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        internal System.Windows.Forms.ToolStripButton bnClearItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton bnVacationExtension;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton bnPrint;
        internal System.Windows.Forms.ToolStripButton bnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.Timer tmrVacation;
                                                    
        //private DemoClsDataGridview.ClsDataGirdView DetailsDataGridViewVacation;
        //private System.Windows.Forms.DataGridViewTextBoxColumn colAbsentAddDedID;
        //private System.Windows.Forms.DataGridViewCheckBoxColumn colAbsentSelectedItem;
        //private System.Windows.Forms.DataGridViewTextBoxColumn colAbsentParticulars;
        private System.Windows.Forms.DataGridView dgvVacationDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.TextBox txtChequeNo;
        internal System.Windows.Forms.DateTimePicker dtpChequeDate;
        internal System.Windows.Forms.ComboBox cboAccount;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.ComboBox cboTransactionType;
        internal System.Windows.Forms.Button btnAdditionDeductions;
        private System.Windows.Forms.TabControl tabVacationProcess;
        private System.Windows.Forms.TabPage tabEmployeeInfo;
        private System.Windows.Forms.TabPage tabParticulars;
        private System.Windows.Forms.TabPage tabHistory;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer4;
        private System.Windows.Forms.DataGridView dgvEmpHistory;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        internal System.Windows.Forms.Label lblActualRejoinDateDec;
        internal System.Windows.Forms.Label lblActualRejoinDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColVacationDetailID;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColParticulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColIsAddition;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColNoOfDays;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dgvColReleaseLater;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColTempAmount;
        private System.Windows.Forms.TextBox txtEncashComboOffDay;
        
    }
}