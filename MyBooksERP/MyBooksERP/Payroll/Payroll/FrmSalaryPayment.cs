﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using Microsoft.VisualBasic;
using System.IO;
using System.Xml.Linq;


namespace MyBooksERP
{
    public partial class FrmSalaryPayment : DevComponents.DotNetBar.Office2007Form
    {


        //private bool MblnChangeStatus;                //Check state of the page
        private bool MblnAddStatus;                     //Add/Update mode 
        private bool MblnPrintEmailPermission = false;  //To set Print Email Permission
        private bool MblnAddPermission = false;         //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnShowErrorMess;                 //set to Show Error Message or not

        private int MintRecordCnt;                      //Contains Total Record count
        private int MintTimerInterval;                  //To set timer interval
        private int MCurrencyDecimal = 0;
        public int PintPaymentID = 0;
        private int MintUserId;                         //current userid
        private string MstrMessageCaption;              //Message caption
        private string MstrMessageCommon;               //to set the message
        private MessageBoxIcon MmessageIcon;            // to set the message icon
        public bool LoadFlg = false;


        public string PCompanyName = "";
        public int PintPaymentModeID = 0;
        public int TransactionTypeIDView = 0;
        public Boolean PFormActiveFlag;
        public Boolean PDoubleClick=false;
        public Boolean PConformActiveFlag;
        public int PCompanyID = 0;
        public string PProcessDate = "";
        public int PiCurrencyIDFilter = 0;

        private ArrayList MsarMessageArr;               // Error Message display
        private ArrayList MsarStatusMessage;            // To set status message

        ClsLogWriter mObjLogs;
        ClsNotification mObjNotification;
        clsBLLSalaryPayment MobjClsBLLSalaryPayment;

        public int CurrentRecCnt = 0;

        public bool blnIsEditMode = false;

        public FrmSalaryPayment()
        {
            LoadFlg = false;
            InitializeComponent();
            LoadFlg = true;
            MblnShowErrorMess = true;
            MintUserId = ClsCommonSettings.UserID;
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            MobjClsBLLSalaryPayment = new clsBLLSalaryPayment();
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }
        private void SetArabicControls()
        {
            //ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            //objDAL.SetArabicVersion((int)FormID.SalaryPayment, this);
        }
        private bool LoadCombos(int intType)
        {
            try
            {
                DataTable datCombos = new DataTable();

                if (intType == 0 || intType == 1)
                {
                    datCombos = MobjClsBLLSalaryPayment.FillCombos("select AdditionDeductionID,AdditionDeduction from PayAdditionDeductionReference where (isnull(IsPredefined,0)=0) order by AdditionDeduction");
                    AdditionDeductionComboBox.ValueMember = "AdditionDeductionID";
                    AdditionDeductionComboBox.DisplayMember = "AdditionDeduction";
                    AdditionDeductionComboBox.DataSource = datCombos;
                    AdditionDeductionComboBox.SelectedIndex = -1;
                }
                return true;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }


        private void SetPermissions()
        {
            //Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.SalaryPayment, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }

        private void LoadInitial()
        {

            SetPermissions();
            //BtnUpdate.Enabled = MblnUpdatePermission;
            BtnConfirm.Enabled = MblnAddPermission;
            BtEmail.Enabled = MblnPrintEmailPermission;
            AddBtn.Enabled = MblnAddPermission;

            ErrorProvider1.Clear();
            EmployeePaymentDetailDataGridView.AlternatingRowsDefaultCellStyle.BackColor = SystemColors.ControlLight;


            LoadCombos(0);
            LoadMessage();
            dtpToday.Value = System.DateTime.Now.Date;
            MCurrencyDecimal = MobjClsBLLSalaryPayment.GetCurrencyScale();


            FillParameter();
            BtnConfirm.Enabled = MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intConformActiveFlag;

            

            RecCount();
            int RecCnt = MobjClsBLLSalaryPayment.RecCountNavigate();
            MintRecordCnt = RecCnt;
            DisplayPaymentDetails(Convert.ToString(RecCnt));

            GridHeadSet();
            LoadReportBody();


            TabControlSal.SelectedTab = TabPageSal;
        }


        private void GridHeadSet()
        {
            Boolean bEditableGrid = MobjClsBLLSalaryPayment.SalarySlipIsEditable();


            if (bEditableGrid == true && PFormActiveFlag == true)
            {

                //BtnUpdate.Enabled = MblnUpdatePermission;
                Panel2.Enabled = true;
            }
            else
            {
                Panel2.Enabled = false;
            }

            if (PFormActiveFlag == true)
            {
                BtnConfirm.Enabled = true;
                GroupBox4.Enabled = true;
            }
            else
            {
                BtnConfirm.Enabled = false;
                GroupBox4.Enabled = false;
            }
            if (PDoubleClick == true)
            {
                BtnConfirm.Enabled = false;
            }
            int i;

            EmployeePaymentDetailDataGridView.Columns[0].Visible = false;
            EmployeePaymentDetailDataGridView.Columns[1].Visible = false;
            EmployeePaymentDetailDataGridView.Columns[2].Visible = false;
            EmployeePaymentDetailDataGridView.Columns[3].Visible = false;
            EmployeePaymentDetailDataGridView.Columns[4].Visible = false;
            EmployeePaymentDetailDataGridView.Columns[5].Visible = false;
            EmployeePaymentDetailDataGridView.Columns[6].Visible = false;
            EmployeePaymentDetailDataGridView.Columns[7].ReadOnly = true;

            if (bEditableGrid == true && PFormActiveFlag == true)
            {
                EmployeePaymentDetailDataGridView.Columns[9].ReadOnly = false;
                EmployeePaymentDetailDataGridView.Columns[8].ReadOnly = false;
            }
            else
            {
                EmployeePaymentDetailDataGridView.Columns[9].ReadOnly = true;
                EmployeePaymentDetailDataGridView.Columns[8].ReadOnly = true;
            }


            EmployeePaymentDetailDataGridView.AllowUserToAddRows = false;
            EmployeePaymentDetailDataGridView.AllowUserToDeleteRows = true;
            EmployeePaymentDetailDataGridView.AllowUserToResizeColumns = false;



            for (i = 0; EmployeePaymentDetailDataGridView.ColumnCount - 1 >= i; ++i)
            {

                EmployeePaymentDetailDataGridView.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }



        }

        private void FillParameter()
        {
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intCompanyID = PCompanyID;
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intPaymentID = 0;
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intEmployeeID = 0;
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.strGetHostName = GetHostName().ToString().Trim();
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intUserId = Convert.ToInt32(MintUserId);
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intTransactionTypeID = TransactionTypeIDView;
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intFormActiveFlag = PFormActiveFlag;
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intConformActiveFlag = PConformActiveFlag;
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.strPProcessDate = PProcessDate;
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intPiCurrencyIDFilter = PiCurrencyIDFilter;
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.strPaymentDate = dtpToday.Value.Date.ToString("dd-MMM-yyyy");
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.strRowCount = Convert.ToString(RecCount());

        }

        private void DisplayPaymentDetails(String strRowCount)
        {

            if ((MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intConformActiveFlag = true) || (PintPaymentID < 1))
            {
                MobjClsBLLSalaryPayment.clsDTOSalaryPayment.strRowCount = strRowCount;
                PintPaymentID = MobjClsBLLSalaryPayment.GetPaymentID();
            }

            FillEmployeePaymentDetail(PintPaymentID);
            RecCalculate();

        }


        public string GetHostName()
        {
            return System.Net.Dns.GetHostName();
        }

        private void FillEmployeePaymentDetail(int iPaymentID)
        {
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intPaymentID = iPaymentID;

            int intCnt = 0;

            EmployeePaymentDetailDataGridView.Rows.Clear();
            DataTable DtSetPayment;
            DtSetPayment = MobjClsBLLSalaryPayment.GetSalaryPaymentDetail();



            if (DtSetPayment.Rows.Count > 0)
            {
                for (intCnt = 0; intCnt < DtSetPayment.Rows.Count; intCnt++)
                {

                    lblEmployeeName.Text = Convert.ToString(DtSetPayment.Rows[intCnt]["Name"]);
                    lblCurrency.Text = Convert.ToString(DtSetPayment.Rows[intCnt]["Currency"]);
                    lblTransactionType.Text = Convert.ToString(DtSetPayment.Rows[intCnt]["TransactionType"]);
                    dtpToday.Value = Convert.ToDateTime(Convert.ToDateTime(DtSetPayment.Rows[intCnt]["PaymentDate"]).ToString("dd-MMM-yyyy"));
                    EmployeePaymentDetailDataGridView.RowCount = EmployeePaymentDetailDataGridView.RowCount + 1;

                    EmployeePaymentDetailDataGridView.Rows[intCnt].Cells[0].Value = DtSetPayment.Rows[intCnt]["PaymentDetailID"];
                    EmployeePaymentDetailDataGridView.Rows[intCnt].Cells[1].Value = DtSetPayment.Rows[intCnt]["PaymentID"];
                    EmployeePaymentDetailDataGridView.Rows[intCnt].Cells[3].Value = DtSetPayment.Rows[intCnt]["IsAddition"];
                    EmployeePaymentDetailDataGridView.Rows[intCnt].Cells[4].Value = DtSetPayment.Rows[intCnt]["AdditionDeductionID"];
                    EmployeePaymentDetailDataGridView.Rows[intCnt].Cells[5].Value = DtSetPayment.Rows[intCnt]["EmpVenFlag"];
                    EmployeePaymentDetailDataGridView.Rows[intCnt].Cells[6].Value = DtSetPayment.Rows[intCnt]["ADDFLAG"];
                    EmployeePaymentDetailDataGridView.Rows[intCnt].Cells[7].Value = DtSetPayment.Rows[intCnt]["Particulars"];
                    EmployeePaymentDetailDataGridView.Rows[intCnt].Cells["ShortDescription"].Value = DtSetPayment.Rows[intCnt]["ShortDescription"];

                    EmployeePaymentDetailDataGridView.Rows[intCnt].Cells["Deductions"].ReadOnly = true;
                    EmployeePaymentDetailDataGridView.Rows[intCnt].Cells["Earnings"].ReadOnly = true;

                    if (Convert.ToBoolean(EmployeePaymentDetailDataGridView.Rows[intCnt].Cells[3].Value + "") == false)
                    {
                        EmployeePaymentDetailDataGridView.Rows[intCnt].Cells["Deductions"].Value = DtSetPayment.Rows[intCnt]["Amount"];
                    }
                    else
                    {
                        EmployeePaymentDetailDataGridView.Rows[intCnt].Cells["Earnings"].Value = DtSetPayment.Rows[intCnt]["Amount"];
                    }


                    //if (ClsCommonSettings.IsAmountRoundByZero)
                    //{
                    //    EmployeePaymentDetailDataGridView.Rows[intCnt].Cells["Deductions"].Value = EmployeePaymentDetailDataGridView.Rows[intCnt].Cells["Deductions"].Value.ToDecimal().ToString("F" + 0);
                    //    EmployeePaymentDetailDataGridView.Rows[intCnt].Cells["Earnings"].Value = EmployeePaymentDetailDataGridView.Rows[intCnt].Cells["Earnings"].Value.ToDecimal().ToString("F" + 0);
                    //}


                    EmployeePaymentDetailDataGridView.Rows[intCnt].Cells[10].Value = DtSetPayment.Rows[intCnt]["Remarks"];
                    EmployeePaymentDetailDataGridView.Rows[intCnt].Cells[11].Value = DtSetPayment.Rows[intCnt]["IsEditable"];

                }
            }

            EmployeePaymentDetailDataGridView.ClearSelection();
            AmountTextBox.Text = "";
        }

        private void LoadMessage()
        {
            try
            {
                // Loading Message
                MsarMessageArr = new ArrayList();
                MsarStatusMessage = new ArrayList();
                MsarMessageArr = mObjNotification.FillMessageArray((int)FormID.SalaryPayment, 2);
                MsarStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.SalaryPayment, 2);
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on LoadMessage() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadMessage()" + ex.Message.ToString());
            }
        }

        private void NetAmount()
        {
            DataTable DT;
            TotalTextBox.Text = "0";
            DT = MobjClsBLLSalaryPayment.NetAmount();
            if (DT.Rows.Count > 0)
            {
                TotalTextBox.Text = Convert.ToString(DT.Rows[0]["NetAmount"]);
                //if (ClsCommonSettings.IsAmountRoundByZero)
                //{
                //    TotalTextBox.Text = TotalTextBox.Text.ToDecimal().ToString("F" + 0);
                //}
                DebitGroupTextBox.Text = Convert.ToString(DT.Rows[0]["DedAmt"]);
                CreditGroupTextBox.Text = Convert.ToString(DT.Rows[0]["AddAmt"]);
                CreditAmtTextBox.Text = CreditGroupTextBox.Text;
                DebitAmtTextBox.Text = DebitGroupTextBox.Text; 
            }

        }

        private int RecCount()
        {
            int Recordt = 0;
            CurrentRecCnt = 0;
            Recordt = MobjClsBLLSalaryPayment.RecCountNavigate();
            if (Recordt > 0)
            {
                Recordt = Convert.ToInt32(Recordt);
                CurrentRecCnt = Recordt;
                BindingNavigatorCountItem.Text = "of " + Convert.ToString(Recordt) + "";
                BindingNavigatorPositionItem.Text = Convert.ToString(Recordt);
            }
            else
            {
                BindingNavigatorCountItem.Text = "of 0";
                CurrentRecCnt = 0;
                Recordt = 0;
            }

            return Recordt;
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {

            EmployeePaymentBindingNavigatorSaveItem.Enabled = MblnAddStatus;
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) - 1).ToString();

            if (Convert.ToInt32(BindingNavigatorPositionItem.Text) <= 0)
            {
                BindingNavigatorPositionItem.Text = "1";
            }
            if (MintRecordCnt > 1)
            {
                BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";
            }
            else
            {
                BindingNavigatorCountItem.Text = "of 1";
            }
            DisplayPaymentDetails(BindingNavigatorPositionItem.Text);
            AdditionDeductionComboBox.SelectedIndex = -1;
            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 10, out MmessageIcon);
            lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            Timer1.Enabled = true;
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {

            EmployeePaymentBindingNavigatorSaveItem.Enabled = MblnAddStatus;

            BindingNavigatorPositionItem.Text = "1";
            if (MintRecordCnt > 1)
            {
                BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";
            }
            else
            {
                BindingNavigatorCountItem.Text = "of 1";
            }

            DisplayPaymentDetails(BindingNavigatorPositionItem.Text);
            AdditionDeductionComboBox.SelectedIndex = -1;
            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9, out MmessageIcon);
            lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            Timer1.Enabled = true;

        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            EmployeePaymentBindingNavigatorSaveItem.Enabled = MblnAddStatus;
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) + 1).ToString();

            if (MintRecordCnt < Convert.ToInt32(BindingNavigatorPositionItem.Text))
            {
                if (MintRecordCnt > 1)
                {
                    BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();
                }
                else
                {
                    BindingNavigatorPositionItem.Text = "1";
                }
            }

            if (MintRecordCnt > 1)
            {
                BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";
            }
            else
            {
                BindingNavigatorCountItem.Text = "of 1";
            }

            DisplayPaymentDetails(BindingNavigatorPositionItem.Text);
            AdditionDeductionComboBox.SelectedIndex = -1;
            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 11, out MmessageIcon);
            lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            Timer1.Enabled = true;
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            EmployeePaymentBindingNavigatorSaveItem.Enabled = MblnAddStatus;
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();
            if (MintRecordCnt > 1)
            {
                BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";
            }
            else
            {
                BindingNavigatorCountItem.Text = "of 1";
            }

            DisplayPaymentDetails(BindingNavigatorPositionItem.Text);

            AdditionDeductionComboBox.SelectedIndex = -1;
            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 12, out MmessageIcon);
            lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            Timer1.Enabled = true;
        }


        #region "Load"

        private void FrmSalaryPayment_Load(object sender, EventArgs e)
        {
            LoadInitial();
            ParticularsSelect();
        }
        #endregion



        private void ParticularsSelect()
        {
            GrdAmount.RowCount = 0;
            int intCnt = 0;
            DataSet DTSet;
            DTSet = MobjClsBLLSalaryPayment.ParticularsSelect();
            intCnt = 0;
            GrdAmount.RowCount = 0;
            for (intCnt = 0; intCnt < DTSet.Tables[0].Rows.Count; ++intCnt)
            {
                GrdAmount.RowCount = GrdAmount.RowCount + 1;
                GrdAmount.Rows[intCnt].Cells["Particular"].Value = DTSet.Tables[0].Rows[intCnt]["AdditionDeduction"];
                GrdAmount.Rows[intCnt].Cells["Addition1"].Value = DTSet.Tables[0].Rows[intCnt]["IsAddition"];

                if (Convert.ToDouble(GrdAmount.Rows[intCnt].Cells["Addition1"].Value) == 1)
                {
                    GrdAmount.Rows[intCnt].Cells["CreditAmt"].Value = DTSet.Tables[0].Rows[intCnt]["Amount"];
                }
                else
                {
                    GrdAmount.Rows[intCnt].Cells["DebitAmt"].Value = DTSet.Tables[0].Rows[intCnt]["Amount"];
                }
                GrdAmount.Rows[intCnt].Cells["AddDedIDD"].Value = DTSet.Tables[0].Rows[intCnt]["AdditionDeductionID"];
            }
            GrdAmount.ClearSelection();
        }



        private void BtnConfirm_Click(object sender, EventArgs e)
        {
            String MachineName;

            MachineName = GetHostName();
            if (FormValidation() == false)
            {
                return;
            }

            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 1, out MmessageIcon);
            MstrMessageCommon = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            if (MessageBox.Show(MstrMessageCommon, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            else
            {
                MobjClsBLLSalaryPayment.clsDTOSalaryPayment.dblNetAmount = Convert.ToDouble(TotalTextBox.Text);
                MobjClsBLLSalaryPayment.ConfirmAll();
                MobjClsBLLSalaryPayment.ParticularsUpdate();
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                MessageBox.Show(MstrMessageCommon, MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }


        }

        private bool FormValidation()
        {

            DateTime BookStart; bool IsAct = false;
            IsAct = MobjClsBLLSalaryPayment.IsAccSet();
            FillParameter();


            if (IsAct == false)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 1853, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                return false;
            }


            if (MobjClsBLLSalaryPayment.GetBookStartDate().Trim() == "")
            {
                BookStart = System.DateTime.Now.Date;
            }
            else
            {
                BookStart = Convert.ToDateTime(MobjClsBLLSalaryPayment.GetBookStartDate());
            }

            if (dtpToday.Value.Date < BookStart)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 1854, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                return false;
            }

            if (dtpToday.Value.Date >  ClsCommonSettings.GetServerDate())
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9006, out MmessageIcon).Replace("*","");
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                return false;
            }


            return true;
        }

        private void LoadReportBody()
        {


            string htmStr = "<html><head><style>" +
            "td {font-family:arial;font-size:12px;border:none;color:black;padding:5px}" +
            "a.link{font-family:arial;color:Blue;text-decoration: none}" +
            "a.link:hover{font-family:arial;color: #3399CC;text-decoration: none}" +
            "</style></head><body bgcolor=White topmargin=0 oncontextmenu=return false ondragstart=return false onselectstart=return false><div align=center>" +
            "<table border=0 cellspading=0 cellspacing=0 width=100%>" +
            "<tr ><td style='border:solid 1 black' colspan=3 align=left height=30px><font size=3><b>" + PCompanyName + "</b></td> </tr>" +
            "<tr ><td style='border-left:solid 1 black;border-bottom:solid 1 black;border-right:solid 1 black'  colspan=3 align=left height=30px><font size=3><b>Salary Information</b></font></td></tr><tr><td colspan=2>" +
            "<tr ><td style='border-left:solid 1 black;border-bottom:solid 1 black' align=left><b>Employee Name</b></td>" +
                                "<td style='border-left:solid 1 black;border-bottom:solid 1 black' align=left><b>Payment Mode</b></td> <td style='border-left:solid 1 black;border-bottom:solid 1 black;border-right:solid 1 black' align=left><b>Net Amount</b></td> </tr>";


            DataTable sdr;
            String htminner;

            sdr = MobjClsBLLSalaryPayment.LoadReportBody();

            int i = 0;
            while (sdr.Rows.Count > i)
            {

                if ((int)sdr.Rows[i]["Type"] == 2)
                {
                    htminner = " <tr> " +
                              " <td style='border-left:solid 1 black;border-bottom:solid 1 black' align='left'><span style='display:none'>'dummy'</span></td> " +
                              " <td style='border-left:solid 1 black;border-bottom:solid 1 black' align='left'><b>" + sdr.Rows[i]["PaymentMode"] + " </b></td> " +
                              " <td style='border-left:solid 1 black;border-bottom:solid 1 black;border-right:solid 1 black' align='left'><b> " + sdr.Rows[i]["NetAmount"] + " </b></td> " +
                              " </tr> ";
                }
                else
                {
                    htminner = " <tr> " +
                              " <td style='border-left:solid 1 black;border-bottom:solid 1 black' align='left'> " + sdr.Rows[i]["EmployeeFullName"] + " </td> " +
                              " <td style='border-left:solid 1 black;border-bottom:solid 1 black' align='left'> " + sdr.Rows[i]["PaymentMode"] + " </td> " +
                              " <td style='border-left:solid 1 black;border-bottom:solid 1 black;border-right:solid 1 black' align='left'> " + sdr.Rows[i]["NetAmount"] + "</td> " +
                              "</tr> ";

                }
                i = i + 1;
                htmStr += htminner.ToString();
            }

            WebBrowser1.DocumentText = htmStr;
        }


        private void RecCalculate()
        {
            NetAmt();
            NetAmount();
        }

        private void NetAmt()
        {

            try
            {
                if (LoadFlg == true)
                {

                    Decimal AddAmt = 0;
                    Decimal DedAmt = 0;
                    DebitTextBox.Text = "0";
                    CreditTextBox.Text = "0";
                    NetAmtTextBox.Text = "0";

                    for (int I = 0; EmployeePaymentDetailDataGridView.RowCount - 1 >= I; ++I)
                    {
                        if (Convert.ToDecimal(EmployeePaymentDetailDataGridView.Rows[I].Cells[3].Value) == 1)
                        {
                            AddAmt = AddAmt + Convert.ToDecimal(EmployeePaymentDetailDataGridView.Rows[I].Cells["Earnings"].Value);
                        }
                        else
                        {
                            DedAmt = DedAmt + Convert.ToDecimal(EmployeePaymentDetailDataGridView.Rows[I].Cells["Deductions"].Value);
                        }
                    }

                    DebitTextBox.Text = Convert.ToString(DedAmt);
                    CreditTextBox.Text = Convert.ToString(AddAmt);
                    NetAmtTextBox.Text = Convert.ToString(AddAmt - DedAmt);
                    //if (ClsCommonSettings.IsAmountRoundByZero)
                    //{
                    //    NetAmtTextBox.Text = NetAmtTextBox.Text.ToDecimal().ToString("F" + 0);
                        
                    //}


                }

            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on form FrmSalaryPayment :NetAmt " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmSalaryPayment :NetAmt() " + Ex.Message.ToString());
            }

        }

        private void FrmSalaryPayment_FormClosing(object sender, FormClosingEventArgs e)
        {
            MobjClsBLLSalaryPayment.DelTempTable();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            try
            {

                int iRowIndex; int CurrencyID; double Amt;

                if (AdditionDeductionComboBox.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 1850, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                if (AmountTextBox.Text.ToString().Trim() == "")
                {

                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 1851, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                if (Information.IsNumeric(AmountTextBox.Text) == false)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 1851, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }

                if (Convert.ToDouble(AmountTextBox.Text) == 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 1850, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                int IsAddDed = 0;
                MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intAddDedID = Convert.ToInt32(AdditionDeductionComboBox.SelectedValue);
                IsAddDed = Convert.ToInt32(MobjClsBLLSalaryPayment.IsAdditionFun());
                int i = 0;
                foreach (DataGridViewRow row in EmployeePaymentDetailDataGridView.Rows)
                {
                    if (Convert.ToDecimal(row.Cells[4].Value) == Convert.ToDecimal(AdditionDeductionComboBox.SelectedValue))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 1852, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                }

                EmployeePaymentDetailDataGridView.RowCount = EmployeePaymentDetailDataGridView.RowCount + 1;
                iRowIndex = EmployeePaymentDetailDataGridView.RowCount - 1;

                Amt = 0;
                EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[0].Value = -1;
                EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[1].Value = PintPaymentID;
                EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[2].Value = -1;
                EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[3].Value = IsAddDed;
                EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[4].Value = AdditionDeductionComboBox.SelectedValue;
                EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[5].Value = 1;
                EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[6].Value = 0;
                EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[7].Value = Convert.ToString(AdditionDeductionComboBox.Text.ToString().Trim());

                if (Convert.ToDecimal(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[3].Value) == 0)
                {
                    EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[9].Value = Convert.ToDouble(AmountTextBox.Text);
                    EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[8].ReadOnly = true;
                    Amt = Amt * (-1);
                }
                else
                {
                    EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[9].ReadOnly = true;
                    EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[8].Value = Convert.ToDouble(AmountTextBox.Text);
                    Amt = Convert.ToDouble(AmountTextBox.Text.Trim());
                }

                EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[10].Value = "added at salary release";
                EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[11].Value = 1;

                if (iRowIndex > 0)
                {
                    EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[12].Value = EmployeePaymentDetailDataGridView.Rows[iRowIndex - 1].Cells[12].Value;
                }
                AdditionDeductionComboBox.SelectedIndex = -1;
                AmountTextBox.Text = "";
                //if (ClsCommonSettings.IsAmountRoundByZero)
                //{
                //    EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells["Deductions"].Value = EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells["Deductions"].Value.ToDecimal().ToString("F" + 0);
                //    EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells["Earnings"].Value = EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells["Earnings"].Value.ToDecimal().ToString("F" + 0);
                //}

                SaveUpdate();
                //LoadReportBody();


                NetAmt();
            }
            catch
            {
            }

        }

        private void SaveUpdate()
        {
            try
            {
                UpdatePaymentReleaseDetails();
                MobjClsBLLSalaryPayment.SavePayment();
                //FillEmployeePaymentDetail(PintPaymentID);
                RecCalculate();
                LoadReportBody();
                ParticularsSelect();
            }
            catch
            {
            }
        }

        //private void BtnUpdate_Click(object sender, EventArgs e)
        //{
        //    //MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3, out MmessageIcon);
        //    //MstrMessageCommon = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
        //    //if (MessageBox.Show(MstrMessageCommon, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
        //    //{
        //    //    UpdatePaymentReleaseDetails();
        //    //    MobjClsBLLSalaryPayment.SavePayment();
        //    //    FillEmployeePaymentDetail(PintPaymentID);
        //    //    RecCalculate();
        //    //    ParticularsSelect();
        //    //    LoadReportBody();
        //    //    //MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 21, out MmessageIcon);
        //    //    //MstrMessageCommon = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
        //    //    //if (MessageBox.Show(MstrMessageCommon + ". Do you wish to release?", MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
        //    //    //    TabControlSal.SelectedTab = TabPageRelease;
        //    //}
        //}


        private void UpdatePaymentReleaseDetails()
        {

            EmployeePaymentDetailDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
            EmployeePaymentDetailDataGridView.EndEdit();
            int iRowIndex = 0;

            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.lstSalaryPaymentList = new List<clsDTOSalaryPaymentList>();

            for (iRowIndex = 0; iRowIndex < EmployeePaymentDetailDataGridView.RowCount; ++iRowIndex)
            {
                clsDTOSalaryPaymentList objclsDTOSalaryPayment = new clsDTOSalaryPaymentList();
                objclsDTOSalaryPayment.intCompanyID = PCompanyID;
                objclsDTOSalaryPayment.intPaymentID = Convert.ToDouble(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[1].Value);
                objclsDTOSalaryPayment.intPaymentDetailID = Convert.ToInt32(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[0].Value);
                objclsDTOSalaryPayment.intAddDedID = Convert.ToInt32(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[4].Value);
                if (Convert.ToInt32(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[3].Value) == 1)
                {
                    objclsDTOSalaryPayment.dblAmount = Convert.ToDouble(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[8].Value);
                }
                else
                {
                    objclsDTOSalaryPayment.dblAmount = Convert.ToDouble(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[9].Value);
                }
                objclsDTOSalaryPayment.strRemarks = Convert.ToString(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[10].Value);
                objclsDTOSalaryPayment.intLoanID = Convert.ToInt32(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[2].Value);
                objclsDTOSalaryPayment.intEmpVenFlag = Convert.ToInt32(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[5].Value);

                objclsDTOSalaryPayment.intAddFlag = Convert.ToInt32(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[6].Value);
                objclsDTOSalaryPayment.intIsEditable = Convert.ToInt32(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[11].Value);
                objclsDTOSalaryPayment.intCurrencyId = Convert.ToInt32(PiCurrencyIDFilter);
                MobjClsBLLSalaryPayment.clsDTOSalaryPayment.lstSalaryPaymentList.Add(objclsDTOSalaryPayment);
            }

        }

        private void EmployeePaymentDetailDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void EmployeePaymentDetailDataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (Convert.ToInt32((((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex)) == 8 || Convert.ToInt32((((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex)) == 9)
                {
                    this.EmployeePaymentDetailDataGridView.EditingControl.KeyPress += new KeyPressEventHandler(NumericTXT_KeyPress);
                }
                else
                {
                    this.EmployeePaymentDetailDataGridView.EditingControl.KeyPress -= new KeyPressEventHandler(NumericTXT_KeyPress);
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on EmployeePaymentDetailDataGridView_EditingControlShowing " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }

        void NumericTXT_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                bool nonNumericEntered = true;
                if ((Strings.AscW(e.KeyChar) >= 48 && Strings.AscW(e.KeyChar) <= 57) || Strings.AscW(e.KeyChar) == 8 || Strings.AscW(e.KeyChar) == 46)
                {
                    nonNumericEntered = false;
                }

                if (Strings.AscW(e.KeyChar) == 46 )
                {
                    nonNumericEntered = true;
                }


                if (nonNumericEntered == true)
                {
                    e.Handled = true;
                }
                else
                {
                    e.Handled = false;
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on NumericTXT_KeyPress " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }

        private void BtEmail_Click(object sender, EventArgs e)
        {
            ClsWebform objClsWebform = new ClsWebform();
            using (FrmEmailPopup objemail = new FrmEmailPopup())
            {
                objemail.MsSubject = "Payment";
                objemail.EmailFormType = EmailFormID.SalaryPayment;
                objemail.MiRecordID = PintPaymentID;
                objemail.ShowDialog();

            }


        }

        private void EmployeePaymentDetailDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {

            try
            {

                Boolean bEditableGrid = MobjClsBLLSalaryPayment.SalarySlipIsEditable();
                if (bEditableGrid)
                {
                    if (EmployeePaymentDetailDataGridView.Rows[e.RowIndex].Cells[3].Value != null)
                    {
                        EmployeePaymentDetailDataGridView.Rows[e.RowIndex].Cells["Deductions"].ReadOnly = true;
                        EmployeePaymentDetailDataGridView.Rows[e.RowIndex].Cells["Earnings"].ReadOnly = true;

                        if (Convert.ToInt32(EmployeePaymentDetailDataGridView.Rows[e.RowIndex].Cells[3].Value) == 1)
                        {
                            EmployeePaymentDetailDataGridView.Rows[e.RowIndex].Cells["Earnings"].ReadOnly = false;
                            if (e.ColumnIndex == 9)
                            {
                                e.Cancel = true;
                            }
                        }
                        else
                        {
                            EmployeePaymentDetailDataGridView.Rows[e.RowIndex].Cells["Deductions"].ReadOnly = false;
                            if (e.ColumnIndex == 8)
                            {
                                e.Cancel = true;
                            }

                        }
                    }

                }
                else
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9128, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    MessageBox.Show(MstrMessageCommon, MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
               
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on EmployeePaymentDetailDataGridView_CellBeginEdit " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }

        private void CancelCButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NewAddDedButton_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmAdditionDeduction(), true);
            LoadCombos(1);
        }

        private void EmployeePaymentDetailDataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            int iRowIndex;
            string sSqlQuery;
            Boolean bEditableGrid = MobjClsBLLSalaryPayment.SalarySlipIsEditable();
            iRowIndex = EmployeePaymentDetailDataGridView.CurrentRow.Index;
            if (e.KeyCode == Keys.Delete)
            {
                if (EmployeePaymentDetailDataGridView.SelectedRows.Count > 0)
                {
                    if (EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[11].Value != null)
                    {
                        if (bEditableGrid == true && Convert.ToInt32(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells["AddDedID"].Value) != 1)
                        {

                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 13, out MmessageIcon);

                            if (MessageBox.Show(MstrMessageCommon, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {

                                MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intPaymentDetailID = Convert.ToInt32(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells[0].Value);
                                MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intPaymentID = Convert.ToInt32(EmployeePaymentDetailDataGridView.Rows[iRowIndex].Cells["PaymentID"].Value);
                                MobjClsBLLSalaryPayment.DeleteRow();
                                EmployeePaymentDetailDataGridView.Rows.RemoveAt(EmployeePaymentDetailDataGridView.CurrentRow.Index);
                                RecCalculate();
                            }
                            else
                            {
                                e.Handled = true;
                                return;
                            }

                        }
                        else
                        {
                            e.Handled = true;
                            return;
                        }
                    }
                }
            }
        }

        private void EmployeePaymentDetailDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                if (e.RowIndex > -1)
                {
                    switch (e.ColumnIndex)
                    {
                        case 8:
                        case 9:
                            if (Convert.ToString(EmployeePaymentDetailDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value) == ".")
                            {
                                EmployeePaymentDetailDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "0.";

                            }
                            break;
                    }
                }
            }
            catch
            {
            }
        }

        private void EmployeePaymentDetailDataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (EmployeePaymentDetailDataGridView.IsCurrentCellDirty)
                {
                    if (EmployeePaymentDetailDataGridView.CurrentCell != null)
                        EmployeePaymentDetailDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }

            }
            catch
            {
            }
        }

        private void EmployeePaymentDetailDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {

                NetAmt();

            }
            catch
            {

            }
        }

        private void HelpToolStripButton_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Salary Release";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void FrmSalaryPayment_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                            objHelp.strFormName = "Salary Release";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void EmployeePaymentDetailDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 9 || e.ColumnIndex == 8)
                {
                    SaveUpdate();
                }
            }
            catch
            {
            }
        }


    }
}