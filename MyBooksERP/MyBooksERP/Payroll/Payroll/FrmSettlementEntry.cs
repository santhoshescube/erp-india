﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using BLL;
using DTO;
namespace MyBooksERP
{
    public partial class FrmSettlementEntry  : Form
    {
        bool MbChangeStatus;
        bool MbAddStatus ;
        int MiCompanyID ;
        int miCurrencyId ;
        bool MbPrintEmailPermission =true;
        bool MbAddPermission =true;
        bool MbUpdatePermission  =true;
        bool MbDeletePermission =true;
        string MsMessageCommon = "";
        bool  bProcess = false;
        bool  bClickProcess  =false;
        bool  bLoad  =false;
        public  int RecordCnt;int CurrentRecCnt ;
        public  int Pscale;
        bool  DisplayFlg  =false;
        bool  NavFlg  = false;
        bool  blnConfirm  =false;
        DataTable datTempSalary;
        Double dblTempSalary ;
        public bool IsFormLoad;
        private string MstrMessageCommon; //Messagebox display
        private string MstrMessageCaption;              //Message caption
        private ArrayList MsarMessageArr; // Error Message display

        public int PiCompanyID = 0;// Reference From Work policy
        private int TotalRecordCnt;
        private int MintTimerInterval;                  // To set timer interval
        private int MintCompanyId;                      // current companyid
        private int MintUserId;
        private bool mblnSearchStatus = false;

        private MessageBoxIcon MmessageIcon;            // to set the message icon
        private bool Glb24HourFormat;
        private bool isGridRowPresent=true;
        private DateTime SysDate; 
        ClsNotification mObjNotification;
        clsBLLSettlementProcess MobjclsBLLSettlementProcess;
        string strBindingOf = "Of ";
        /* 
================================================
   Author:		<Author, Lince P Thomas>
   Create date: <Create Date,,8 Jan 2010>
   Description:	<Description,,Company Form>
 *
 *  Modified :	   <Saju>
    Modified date: <Modified Date,19 August 2013>
    Description:	<Description,Settlement Process Form>
================================================
*/
        public FrmSettlementEntry ()
        {
            MobjclsBLLSettlementProcess = new clsBLLSettlementProcess();
            InitializeComponent();
            MintCompanyId = ClsCommonSettings.CompanyID;
            MintUserId = ClsCommonSettings.UserID;
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            Glb24HourFormat = ClsCommonSettings.Glb24HourFormat;
            SysDate= ClsCommonSettings.GetServerDate();
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls(); 
            //}
        }
        #region SetArabicControls
        private void SetArabicControls()
        {
            //ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            //objDAL.SetArabicVersion((int)FormID.SettlementProcess, this);

            strBindingOf = "من ";
            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            EmployeeSettlementBindingNavigatorSaveItem.ToolTipText = "حفظ البيانات";
            GratuityTab.Text = "بقشيش";
            DtGrdGratuity.Columns["ColYears"].HeaderText = "عام";
            DtGrdGratuity.Columns["ColCalDays"].HeaderText = "مجموع أيام";
            DtGrdGratuity.Columns["ColGRTYDays"].HeaderText = "بقشيش أيام";
            DtGrdGratuity.Columns["ColGratuityAmt"].HeaderText = "بقشيش مبلغ";            
        }
        #endregion SetArabicControls
        private void FrmSettlementEntry_Load(object sender, EventArgs e)
        {
            IsFormLoad = false;
            LoadMessage();
            LoadCombos(0);
            LoadInitial();
            SetPermissions();
            BindingNavigatorAddNewItem_Click(sender, e); 
            TmrEmployeeSettlement.Interval = MintTimerInterval;
            DocumentReturnCheckBox.Checked = false;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
            BindingNavigatorPositionItem.Text = RecordCnt.ToString() ;
            MbChangeStatus = false;
            BtnProcess.Enabled = MbChangeStatus;
            OKSaveButton.Enabled = MbChangeStatus;
            BtnSave.Enabled = MbChangeStatus;
            EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbChangeStatus;
            BindingNavigatorDeleteItem.Enabled = MbChangeStatus;
            IsFormLoad = true;
        }

        private void LoadCombos(int intType)
        {
            DataTable datCombos = new DataTable();

            if (intType == 0 || intType == 1)////CAlculation type
            {
                //if (ClsCommonSettings.IsArabicView)
                //    datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { "workstatusID,workstatusArb AS workstatus", "WorkStatusReference", " workstatusID <6" });
                //else
                datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { "workstatusID,workstatus", "WorkStatusReference", " workstatusID <6" });
                CboType.ValueMember = "workstatusID";
                CboType.DisplayMember = "workstatus";
                CboType.DataSource = datCombos;
            }

            if (intType == 0 || intType == 1)////CAlculation type
            {
                CboEmployee.DataSource = null;
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullNameArb + ' - ' + EM.EmployeeNumber as Name", "EmployeeMaster AS EM INNER JOIN PaySalaryStructure AS ESH ON EM.EmployeeID = ESH.EmployeeID INNER JOIN UserCompanyDetails AS U ON EM.CompanyID = U.CompanyID", "EM.EmployeeID > 0 and U.UserID ="+ ClsCommonSettings.UserID +" order by Name" });
                    //}
                    //else
                    //{
                        datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName + ' - ' + EM.EmployeeNumber as Name", "EmployeeMaster AS EM INNER JOIN PaySalaryStructure AS ESH ON EM.EmployeeID = ESH.EmployeeID INNER JOIN UserCompanyDetails AS U ON EM.CompanyID = U.CompanyID", "EM.EmployeeID > 0 and U.UserID =" + ClsCommonSettings.UserID + " and EM.CompanyID="+ ClsCommonSettings.LoginCompanyID +"  order by Name" });
                    //}
                CboEmployee.ValueMember = "EmployeeID";
                CboEmployee.DisplayMember = "Name";
                CboEmployee.DataSource = datCombos;
            }

            if ( intType == 5)////CAlculation type
            {
                CboEmployee.DataSource = null;

                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullNameArb + ' - ' + EM.EmployeeNumber as Name", "EmployeeMaster EM inner join PaySalaryStructure ESH on EM.EmployeeID=ESH.EmployeeID inner join UserCompanyDetails U ON EM.CompanyID=U.CompanyID AND U.UserID =" + ClsCommonSettings.UserID + "", "  WorkStatusID>=6 order by Name" });
                    //}
                    //else
                    //{
                datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName + ' - ' + EM.EmployeeNumber as Name", "EmployeeMaster EM inner join PaySalaryStructure ESH on EM.EmployeeID=ESH.EmployeeID inner join UserCompanyDetails U ON EM.CompanyID=U.CompanyID AND U.UserID =" + ClsCommonSettings.UserID + " and U.CompanyID=" + ClsCommonSettings .LoginCompanyID + " ", "  WorkStatusID>=6 order by Name" });
                    //}

                CboEmployee.ValueMember = "EmployeeID";
                CboEmployee.DisplayMember = "Name";
                CboEmployee.DataSource = datCombos;
            }

            if (intType == 0 || intType == 3)//Transaction type
            {
                //if (ClsCommonSettings.IsArabicView)
                //    datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { "TransactionTypeID,TransactionTypeArb AS TransactionType", "TransactionTypeReference", "TransactionTypeID NOT IN(" + (int)PaymentTransactionType.Wps + ")" });
                //else
                    datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { "TransactionTypeID,TransactionType", "TransactionTypeReference", "" });
                cboTransactionType.ValueMember = "TransactionTypeID";
                cboTransactionType.DisplayMember = "TransactionType";
                cboTransactionType.DataSource = datCombos;
                datCombos = null;
            }
            if (intType == 0 || intType == 4)//Accounts
            {
                int iEmpID = CboEmployee.SelectedValue.ToInt32();
                if (iEmpID != 0)
                {
                    datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { "AM.AccountID,AM.AccountName", " AccAccountMaster  AM  INNER JOIN BankBranchReference BBR ON BBR.AccountID=AM.AccountID  INNER JOIN CompanyBankAccountDetails CBA ON CBA.BankBranchID=BBR.BankBranchID  INNER  JOIN EmployeeMaster EM ON EM.CompanyID=CBA.CompanyID  AND EM.EmployeeID=" + iEmpID.ToStringCustom(), "" });
                }
                else
                {
                    datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { "AM.AccountID,AM.AccountName", " AccAccountMaster  AM  INNER JOIN BankBranchReference BBR ON BBR.AccountID=AM.AccountID  INNER JOIN CompanyBankAccountDetails CBA ON CBA.BankBranchID=BBR.BankBranchID  INNER  JOIN EmployeeMaster EM ON EM.CompanyID=CBA.CompanyID", "" });
                }
                cboAccount.ValueMember = "AccountID";
                cboAccount.DisplayMember = "AccountName";
                cboAccount.DataSource = datCombos;
                datCombos = null;
            }
        }

        private void LoadInitial()
        {
            try
            {
                CboEmployee.SelectedIndex = -1;
                txtNofDaysExperience.Text = "0";
                lblNoofMonths.Text = "0";
                txtLeavePayDays.Text = "0";
                //if (ClsCommonSettings.IsArabicView)
                //    lblSettlementPolicy.Text = "لا شيء";
                //else
                    lblSettlementPolicy.Text = "NIL";
                lblEligibleLeavePayDays.Text = "0";
                lblTakenLeavePayDays.Text = "0";
                txtLeavePayDays.Text = "0";
                TxtTotalEarning.Text = "";
                TxtTotalDeduction.Text = "";
                NetAmountTextBox.Text = "";
            }
            catch { }
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID , (Int32)eModuleID.Payroll, (Int32)eMenuID.SettlementProcess, out MbPrintEmailPermission, out MbAddPermission, out MbUpdatePermission, out MbDeletePermission);
            }
            else            
            MbAddPermission = MbPrintEmailPermission = MbUpdatePermission = MbDeletePermission = true;
        }

        private void LoadMessage()
        {
            try
            {
                MsarMessageArr = new ArrayList();
                MsarMessageArr = mObjNotification.FillMessageArray((int)FormID.SettlementProcess , 2);
            }
            catch { }
        }

        private void RecCountNavigate()
        {
            RecordCnt = 0;
            RecordCnt = MobjclsBLLSettlementProcess.GetRecordCount();

            if (RecordCnt > 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
                BindingNavigatorPositionItem.Text = Convert.ToString(RecordCnt);
            }
            else
            {
                CurrentRecCnt = 0;
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                RecordCnt = 0;
            }
        }
        private int IsValidSet(int SetTypeID, int EmpID)
        {
            try
            {
                int IntRet;
                //if (ClsCommonSettings.IsHrPowerEnabled)
                //{
                    if (MbAddStatus == true)
                    {
                        IntRet=MobjclsBLLSettlementProcess.IsValidSet(SetTypeID, EmpID);
                        return IntRet;
                    }
                //}
                return 0;
            }
            catch
            {
                return 0;
            }
        }
        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            try
            {
                NavFlg = true;
                MbAddStatus = false;
                if (!mblnSearchStatus)
                    txtSearch.Text = string.Empty;
                RecCountNavigate();

                if (RecordCnt > 0)
                    CurrentRecCnt = 1;
                else
                    CurrentRecCnt = 0;

                CboEmployee.SelectedIndex = -1;
                BindingNavigatorPositionItem.Text = CurrentRecCnt.ToString();
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.RowNum = CurrentRecCnt;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AddEditMode = 0;
                DisplayInfo(0);
                TmrEmployeeSettlement.Enabled = true;

                this.BindingNavigatorAddNewItem.Enabled = MbAddPermission;
                this.BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
                BtnPrint.Enabled = btnEmail.Enabled = MbPrintEmailPermission;
                OKSaveButton.Enabled = MbUpdatePermission;

                if (CurrentRecCnt == 1)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
                else
                {
                    BindingNavigatorMoveFirstItem.Enabled = true;
                    BindingNavigatorMovePreviousItem.Enabled = true;
                }

                if (CurrentRecCnt == RecordCnt)
                {
                    BindingNavigatorMoveLastItem.Enabled = false;
                    BindingNavigatorMoveNextItem.Enabled = false;
                }
                else
                {
                    BindingNavigatorMoveLastItem.Enabled = true;
                    BindingNavigatorMoveNextItem.Enabled = true;
                }

                MbAddStatus = false;
                MbChangeStatus = false;
                BtnSave.Enabled = MbChangeStatus;
                EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbChangeStatus;

                ((Control)this.SettlementTabEmpInfo).Enabled = true;
                ((Control)this.SettlementTabParticulars).Enabled = true;
                ((Control)this.SettlementTabHistory).Enabled = true;

                if (Convert.ToInt32(LblEmployee.Tag) == 1)// '' When Closed Settlement
                {
                    ((Control)this.SettlementTabEmpInfo).Enabled = false;
                    ((Control)this.SettlementTabParticulars).Enabled = false;
                    ((Control)this.SettlementTabHistory).Enabled = false;

                    BindingNavigatorDeleteItem.Enabled = OKSaveButton.Enabled = false;
                }

                SettlementTab.SelectedTab = SettlementTabEmpInfo;
                EnableDisablePartially();
                NavFlg = false;
            }
            catch
            {
                NavFlg = false;
            }

            CboEmployee.Enabled = false;
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            try
            {
                NavFlg = true;
                MbAddStatus = false;
                if (!mblnSearchStatus)
                    txtSearch.Text = string.Empty;
                isGridRowPresent = false;
                RecCountNavigate();

                if (RecordCnt > 0)
                {
                    CurrentRecCnt = CurrentRecCnt - 1;

                    if (CurrentRecCnt <= 0)
                        CurrentRecCnt = 1;
                }
                else
                    CurrentRecCnt = 1;

                CboEmployee.SelectedIndex = -1;
                BindingNavigatorPositionItem.Text = CurrentRecCnt.ToString();
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.RowNum = CurrentRecCnt;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AddEditMode = 0;
                DisplayInfo(0);
                TmrEmployeeSettlement.Enabled = true;

                if (CurrentRecCnt == 1)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
                else
                {
                    BindingNavigatorMoveFirstItem.Enabled = true;
                    BindingNavigatorMovePreviousItem.Enabled = true;
                }

                if (CurrentRecCnt == RecordCnt)
                {
                    BindingNavigatorMoveLastItem.Enabled = false;
                    BindingNavigatorMoveNextItem.Enabled = false;
                }
                else
                {
                    BindingNavigatorMoveLastItem.Enabled = true;
                    BindingNavigatorMoveNextItem.Enabled = true;
                }

                this.BindingNavigatorAddNewItem.Enabled = MbAddPermission;
                this.BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
                BtnPrint.Enabled = btnEmail.Enabled = MbPrintEmailPermission;
                OKSaveButton.Enabled = MbUpdatePermission;// For Defult Confirmation

                MbAddStatus = false;
                MbChangeStatus = false;
                BtnSave.Enabled = MbChangeStatus;
                EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbChangeStatus;

                ((Control)this.SettlementTabEmpInfo).Enabled = true;
                ((Control)this.SettlementTabParticulars).Enabled = true;
                ((Control)this.SettlementTabHistory).Enabled = true;

                if (Convert.ToInt32(LblEmployee.Tag) == 1)// When Closed Settlement
                {
                    ((Control)this.SettlementTabEmpInfo).Enabled = false;
                    ((Control)this.SettlementTabParticulars).Enabled = false;
                    ((Control)this.SettlementTabHistory).Enabled = false;
                    BindingNavigatorDeleteItem.Enabled = OKSaveButton.Enabled = false;
                }

                SettlementTab.SelectedTab = SettlementTabEmpInfo;
                EnableDisablePartially();
                NavFlg = false;
            }
            catch
            {
                NavFlg = false;
            }

            CboEmployee.Enabled = false;
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            try
            {
                NavFlg = true;
                MbAddStatus = false;
                if (!mblnSearchStatus)
                    txtSearch.Text = string.Empty;
                RecCountNavigate();

                if (RecordCnt > 0)
                {
                    CurrentRecCnt = CurrentRecCnt + 1;

                    if (CurrentRecCnt >= RecordCnt)
                        CurrentRecCnt = RecordCnt;
                }
                else
                {
                    CurrentRecCnt = 0;
                }

                CboEmployee.SelectedIndex = -1;
                BindingNavigatorPositionItem.Text = CurrentRecCnt.ToString();
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.RowNum = CurrentRecCnt;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AddEditMode = 0;
                DisplayInfo(0);
                TmrEmployeeSettlement.Enabled = true;

                this.BindingNavigatorAddNewItem.Enabled = MbAddPermission;
                this.BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
                BtnPrint.Enabled = btnEmail.Enabled = MbPrintEmailPermission;
                OKSaveButton.Enabled = MbUpdatePermission;

                if (CurrentRecCnt == 1)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
                else
                {
                    BindingNavigatorMoveFirstItem.Enabled = true;
                    BindingNavigatorMovePreviousItem.Enabled = true;
                }

                if (CurrentRecCnt == RecordCnt)
                {
                    BindingNavigatorMoveLastItem.Enabled = false;
                    BindingNavigatorMoveNextItem.Enabled = false;
                }
                else
                {
                    BindingNavigatorMoveLastItem.Enabled = true;
                    BindingNavigatorMoveNextItem.Enabled = true;
                }

                MbAddStatus = false;
                MbChangeStatus = false;
                BtnSave.Enabled = MbChangeStatus;
                EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbChangeStatus;

                ((Control)this.SettlementTabEmpInfo).Enabled = true;
                ((Control)this.SettlementTabParticulars).Enabled = true;
                ((Control)this.SettlementTabHistory).Enabled = true;

                if (Convert.ToInt32(LblEmployee.Tag) == 1)// When Closed Settlement
                {
                    ((Control)this.SettlementTabEmpInfo).Enabled = false;
                    ((Control)this.SettlementTabParticulars).Enabled = false;
                    ((Control)this.SettlementTabHistory).Enabled = false;
                    BindingNavigatorDeleteItem.Enabled = OKSaveButton.Enabled = false;
                }

                SettlementTab.SelectedTab = SettlementTabEmpInfo;
                EnableDisablePartially();
                NavFlg = false;
            }
            catch
            {
                NavFlg = false;
            }

            CboEmployee.Enabled = false;
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            try
            {
                NavFlg = true;
                MbAddStatus = false;
                if (!mblnSearchStatus)
                    txtSearch.Text = string.Empty;
                RecCountNavigate();

                if (RecordCnt > 0)
                    CurrentRecCnt = RecordCnt;
                else
                    CurrentRecCnt = 0;

                CboEmployee.SelectedIndex = -1;
                BindingNavigatorPositionItem.Text = CurrentRecCnt.ToString();
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.RowNum = CurrentRecCnt;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AddEditMode = 0;
                DisplayInfo(0);
                TmrEmployeeSettlement.Enabled = true;

                this.BindingNavigatorAddNewItem.Enabled = MbAddPermission;
                this.BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
                BtnPrint.Enabled = btnEmail.Enabled = MbPrintEmailPermission;
                OKSaveButton.Enabled = MbUpdatePermission;// For Defult Confirmation

                if (CurrentRecCnt == 1)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
                else
                {
                    BindingNavigatorMoveFirstItem.Enabled = true;
                    BindingNavigatorMovePreviousItem.Enabled = true;
                }

                if (CurrentRecCnt == RecordCnt)
                {
                    BindingNavigatorMoveLastItem.Enabled = false;
                    BindingNavigatorMoveNextItem.Enabled = false;
                }
                else
                {
                    BindingNavigatorMoveLastItem.Enabled = true;
                    BindingNavigatorMoveNextItem.Enabled = true;
                }

                MbAddStatus = false;
                MbChangeStatus = false;
                BtnSave.Enabled = MbChangeStatus;
                EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbChangeStatus;

                ((Control)this.SettlementTabEmpInfo).Enabled = true;
                ((Control)this.SettlementTabParticulars).Enabled = true;
                ((Control)this.SettlementTabHistory).Enabled = true;

                if (Convert.ToInt32(LblEmployee.Tag) == 1) // When Closed Settlement
                {
                    ((Control)this.SettlementTabEmpInfo).Enabled = false;
                    ((Control)this.SettlementTabParticulars).Enabled = false;
                    ((Control)this.SettlementTabHistory).Enabled = false;
                    BindingNavigatorDeleteItem.Enabled = OKSaveButton.Enabled = false;
                }

                SettlementTab.SelectedTab = SettlementTabEmpInfo;
                EnableDisablePartially();
                NavFlg = false;
            }
            catch
            {
                NavFlg = false;
            }

            CboEmployee.Enabled = false;
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            try 
            {
                isGridRowPresent = true;

                ((Control)this.SettlementTabEmpInfo).Enabled = true;
                ((Control)this.SettlementTabParticulars).Enabled = true;
                ((Control)this.SettlementTabHistory).Enabled = true;
                ((Control)this.GratuityTab).Enabled = true;
                MbAddStatus = true;
                BindingNavigatorAddNewItem.Enabled = false;     
                LblType.Tag = "0";
                LblEmployee.Tag = "0";
                CboEmployee.Focus();
                SetAutoCompleteList();
                CboEmployee.Enabled = true;
                DtpToDate.Value = ClsCommonSettings.GetServerDate();
                DateDateTimePicker.Value = ClsCommonSettings.GetServerDate();    
                bProcess = false;
                CboType.Enabled = true;
                DtpToDate.Enabled = true;
                DocumentReturnCheckBox.Checked = false;
                txtLeavePayDays.Enabled = true;
                Clear();
                CboEmployee.SelectedIndex = -1;
                txtNofDaysExperience.Text = "0";
                lblNoofMonths.Text = "0";
                txtLeavePayDays.Text = "0";
                //if (ClsCommonSettings.IsArabicView)
                //    lblSettlementPolicy.Text = "لا شيء";
                //else
                    lblSettlementPolicy.Text = "NIL";
                lblEligibleLeavePayDays.Text = "0";
                lblTakenLeavePayDays.Text = "0";
                txtLeavePayDays.Text = "0";
                BtnCancel.Enabled = true;

                MbChangeStatus = false;
                BtnProcess.Enabled = MbChangeStatus;
                OKSaveButton.Enabled = MbChangeStatus;
                BindingNavigatorDeleteItem.Enabled = MbChangeStatus;
                BtnSave.Enabled = MbChangeStatus;
                EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbChangeStatus;
                BtnPrint.Enabled = MbChangeStatus;
                btnEmail.Enabled = MbChangeStatus;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AddEditMode = 1;
                txtSearch.Text = "";
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.strSearchKey = "";
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Closed = false;

                RecCount();
                RecordCnt = RecordCnt + 1;
                CurrentRecCnt = CurrentRecCnt + 1;
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
                BindingNavigatorPositionItem.Text = RecordCnt.ToString(); 
                EmployeeSettlementDetailDataGridView.RowCount = 1;

                if (RecordCnt > 1 )
                {
                    BindingNavigatorMoveFirstItem.Enabled = true;
                    BindingNavigatorMovePreviousItem.Enabled = true;
                }

                SettlementTab.SelectedTab = SettlementTabEmpInfo;
                chkAbsent.Checked = false;
                IsFormLoad = false;
                LoadCombos(5);
                IsFormLoad = true;
                Clear();           
            } 
            catch { }
        }

        private void Clear()
        {
            try
            {
                bClickProcess = false;
                bProcess = false;
                LblEmployeeSettlement.Text = "";
                LblEmployee.Tag = "0";
                LblType.Tag = "0";
                TxtParticulars.Text = "";
                txtAmount.Text = "";
                CboEmployee.SelectedIndex = -1;
                CboType.SelectedIndex = -1;
                lblPayBack.Text = "";
                LblCurrency.Text = "";
                TxtTotalEarning.Text = "";
                TxtTotalDeduction.Text = "";
                NetAmountTextBox.Text = "";
                ErrEmployeeSettlement.Clear();
                EmployeeSettlementDetailDataGridView.Rows.Clear();
                DtGrdGratuity.Rows.Clear();    
                RemarksTextBox1.Text = "";
                cboTransactionType.SelectedIndex =-1;
                txtChequeNo.Text = "";
                LblType.Tag =0;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Closed = false;
                DisplayFlg = false;
                CboEmployee.Enabled = true;
                lblSettlementPolicy.Text = "";
                lblNoofMonths.Text = "";
                txtNofDaysExperience.Text = "";
                txtLeavePayDays.Text = "";
                lblEligibleLeavePayDays.Text = "";
                lblTakenLeavePayDays.Text = "";
                lblEligibleLeavePayDays.Text = "";
                txtAbsentDays.Text = "";
                DtGrdEmpHistory.Rows.Clear();
            }
            catch { }
       }

       private void ClearPartially()
       {
            try
            {
                TxtParticulars.Text = "";
                txtAmount.Text = "";
                TxtTotalEarning.Text = "";
                TxtTotalDeduction.Text = "";
                NetAmountTextBox.Text = "";
                ErrEmployeeSettlement.Clear();
                EmployeeSettlementDetailDataGridView.Rows.Clear();
                EmployeeSettlementDetailDataGridView.RowCount = 1;
            }
            catch { }
        }

        private void EmployeeSettlementBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try 
            {
                if (MbChangeStatus == true )
                {
                    if( FormValidation() == true )
                    {
                        blnConfirm = false;
                        Fillparameters(1); 

                        if (MobjclsBLLSettlementProcess.SaveEmployeeSettlement() == true)
                        {
                            LblType.Tag = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID;
                            DisplayInfo(1);
                            EnableDisablePartially();
                            LblEmployeeSettlement.Text = "";
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LblEmployeeSettlement.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                               
                            MbAddStatus = false;
                            BindingNavigatorAddNewItem.Enabled = MbAddPermission;
                            BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
                            BtnPrint.Enabled = btnEmail.Enabled = MbPrintEmailPermission;
                        }
                    }
                }
            }
            catch { }
        }

        private void EnableDisablePartially()
        {
            DtpToDate.Enabled = CboType.Enabled = CboEmployee.Enabled = txtLeavePayDays.Enabled = false;
        }

        private bool FormValidation()
        {                
            ErrEmployeeSettlement.Clear();

            if (MbAddStatus == false)
            {
                if (Convert.ToInt32(LblEmployee.Tag) > 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 671, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrEmployeeSettlement.Enabled = true;
                    return false;
                }
            }  

            if (String.IsNullOrEmpty(CboEmployee.Text) == true )
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9124, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                ErrEmployeeSettlement.SetError(CboEmployee, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                TmrEmployeeSettlement.Enabled = true;
                CboEmployee.Focus();
                return false;
            }

            if (CboEmployee.SelectedIndex == -1 )
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 16, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                ErrEmployeeSettlement.SetError(CboEmployee, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                TmrEmployeeSettlement.Enabled = true;
                CboEmployee.Focus();
                return false;
            }
            else
                ErrEmployeeSettlement.SetError(CboEmployee, "");

            if (CboType.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 656, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                ErrEmployeeSettlement.SetError(CboType, MstrMessageCommon);
                LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                CboEmployee.Focus();
                TmrEmployeeSettlement.Enabled = true;
                return false;
            }
            else
                ErrEmployeeSettlement.SetError(CboType, "");

            if (cboTransactionType.SelectedValue.ToInt32() <= 0)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 681, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                ErrEmployeeSettlement.SetError(cboTransactionType, MstrMessageCommon);
                LblEmployeeSettlement.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                TmrEmployeeSettlement.Enabled = true;
                cboTransactionType.Focus();
                return false;
            }

            if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentTransactionType.Bank)
            {
                if (txtChequeNo.Text.ToStringCustom() == string.Empty)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3088, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LblEmployeeSettlement.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                    TmrEmployeeSettlement.Enabled = true;
                    return false;
                }
            }

            MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Date = DateDateTimePicker.Value.Date.ToString("dd-MMM-yyy");






            //if (MobjclsBLLSettlementProcess.IsTransferDateGreater()==1)
            //{
            //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 672, out MmessageIcon);
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    ErrEmployeeSettlement.SetError(DateDateTimePicker, MsMessageCommon.Replace("#", "").Trim());
            //    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
            //    TmrEmployeeSettlement.Enabled = true;
            //    DateDateTimePicker.Focus();
            //    return false;
            //}

            //if (SysDate < DtpToDate.Value.Date)
            //{
            //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3074, out MmessageIcon);
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            //    TmrEmployeeSettlement.Enabled = true;
            //    DtpToDate.Focus();
            //    return false;
            //}

            //if (SysDate < DateDateTimePicker.Value.Date)
            //{
            //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3074, out MmessageIcon);
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            //    TmrEmployeeSettlement.Enabled = true;
            //    DateDateTimePicker.Focus();
            //    return false;
            //}

            if (DtpToDate.Value.Date < DtpFromDate.Value.Date)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 19, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmrEmployeeSettlement.Enabled = true;
                DtpFromDate.Focus();
                return false;
            }

            if (MobjclsBLLSettlementProcess.IsDateOfJoinGreater()==1 )
            {
                if (DtpToDate.Value.Date < Convert.ToDateTime(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.dateofjoining))
                {
                   MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3081, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ErrEmployeeSettlement.SetError(DtpToDate, MsMessageCommon.Replace("#", "").Trim());
                    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrEmployeeSettlement.Enabled = true;
                    DtpToDate.Focus();
                    return false;
                }
            }

            if (MbAddStatus)
            {
                if (MobjclsBLLSettlementProcess.IsEmployeeSettled() == 1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 651, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            }

            if (bClickProcess == false && MbAddStatus == true)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 684, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                TmrEmployeeSettlement.Enabled = true;
                BtnProcess.Focus();
                return false;
            }

            TxtTotalEarning.Text = (TotalEarningSettlementAmt()).ToString();
            TxtTotalDeduction.Text = (TotalDedSettlementAmt()).ToString();                
            NetAmountTextBox.Text =Convert.ToString(TotalEarningSettlementAmt() - TotalDedSettlementAmt()); 
                
            if ((TotalEarningSettlementAmt() - TotalDedSettlementAmt()) < 0 )
            {
                 MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 657, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblPayBack.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim() + " " + NetAmountTextBox.Text + "";
            }
            else
                lblPayBack.Text = "";

            return true;
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (Convert.ToDouble(LblType.Tag) != 0 && Convert.ToInt32(LblEmployee.Tag) == 0 )
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 683, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (MobjclsBLLSettlementProcess.DeleteSettlementDetail() == true)
                        BindingNavigatorAddNewItem_Click(sender, e);
                }
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            BindingNavigatorAddNewItem_Click(sender, e);
        }

        private void CboEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            GetSettlementPolicyAndExperience();
            CboEmployee.DroppedDown = false;
        }

        private void DtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void CboType_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboType.DroppedDown = false;
            //if (ClsCommonSettings.IsArabicView)
            //    lblToDate.Text = "إلى تاريخ";
            //else
                lblToDate.Text = "To Date";
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                    btnSearch_Click(null, null);
            }
            catch { }
        }

        private void CboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErrEmployeeSettlement.Clear();
            MbChangeStatus = true;

            if (MbAddStatus)
                BtnProcess.Enabled = MbAddPermission;
            else
            {
                ChangeStatus();
                BtnProcess.Enabled = false;
            }

            //if (ClsCommonSettings.IsArabicView)
            //    lblToDate.Text = CboType.Text + " تاريخ";
            //else
                lblToDate.Text = CboType.Text + " Date";
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Closed == true)
                return;

            if (MbUpdatePermission == false)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 17, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            bProcess = true;

            if (CboEmployee.SelectedIndex  != -1 )
            {
                if (MbAddStatus == false )
                {
                    if (Convert.ToInt32(LblEmployee.Tag) > 0)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 668, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        TmrEmployeeSettlement.Enabled = true;
                        return;
                    }
                }

                if (cboTransactionType.SelectedValue.ToInt32() <= 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 681, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LblEmployeeSettlement.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                    TmrEmployeeSettlement.Enabled = true;
                    cboTransactionType.Focus();
                    return;
                }

                if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentTransactionType.Bank)
                {
         
                    if (txtChequeNo.Text.ToStringCustom() == string.Empty)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3088, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LblEmployeeSettlement.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                        TmrEmployeeSettlement.Enabled = true;
                        txtChequeNo.Focus();
                        return;
                    }
                }
                if (MobjclsBLLSettlementProcess.IsSettlementAccountExists(CboEmployee.SelectedValue.ToInt32()) == false)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 682, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                BtnSave.Enabled = OKSaveButton.Enabled = EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbUpdatePermission;
                Fillparameters(0);

                if (MobjclsBLLSettlementProcess.IsTransferDateGreater()==1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 672, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ErrEmployeeSettlement.SetError(DtpToDate, MsMessageCommon.Replace("#", "").Trim());
                    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrEmployeeSettlement.Enabled = true;
                    DtpToDate.Focus();
                    return;
                }

                if (MobjclsBLLSettlementProcess.IsDateOfJoinGreater()==1)
                {
                    if (DtpToDate.Value.Date < Convert.ToDateTime(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.dateofjoining))
                    {
                       MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3081, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ErrEmployeeSettlement.SetError(DtpToDate, MsMessageCommon.Replace("#", "").Trim());
                        LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        TmrEmployeeSettlement.Enabled = true;
                        DtpToDate.Focus();
                        return;
                    }

                    if (DtpToDate.Value.Date < Convert.ToDateTime(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.dateofjoining))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3081, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ErrEmployeeSettlement.SetError(DtpToDate, MsMessageCommon.Replace("#", "").Trim());
                        LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        TmrEmployeeSettlement.Enabled = true;
                        DtpToDate.Focus();
                        return;
                    }
                }

                if (MobjclsBLLSettlementProcess.IsEmployeeSettled()==1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 651, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (CboType.SelectedIndex==-1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 656, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ErrEmployeeSettlement.SetError(CboType, MstrMessageCommon);
                    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    CboEmployee.Focus();
                    TmrEmployeeSettlement.Enabled = true;
                    return;
                }
                else
                    ErrEmployeeSettlement.SetError(CboType, "");

                if (CboType.SelectedValue.ToInt32() == 3)
                {
                    if (dtpChequeDate.Value < System.DateTime.Now.Date)// Validation for previous cheque date.
                    {
                        int intDiffDays;
                        intDiffDays = Convert.ToInt32(System.DateTime.Now.Date.DayOfYear - dtpChequeDate.Value.Date.DayOfYear);
                        int intYearDiff = System.DateTime.Now.Year - dtpChequeDate.Value.Year;

                        if (System.DateTime.Now.Year != dtpChequeDate.Value.Year)
                            if (System.DateTime.Now.Year > dtpChequeDate.Value.Year)
                                intDiffDays = 365 * intYearDiff;

                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3161, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("*", Convert.ToString(intDiffDays));
                        TmrEmployeeSettlement.Enabled = true;

                        if (MessageBox.Show(MstrMessageCommon.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                            return ;
                    }
                }

                //if (ClsCommonSettings.IsHrPowerEnabled)
                //{
                //    if (MbAddStatus == true)
                //    {
                //        int RetVal=IsValidSet(CboType.SelectedValue.ToInt32(), CboEmployee.SelectedValue.ToInt32());
                //        if (RetVal == 0)
                //        {
                //            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 20013, out MmessageIcon);
                //            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //            LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                //            TmrEmployeeSettlement.Enabled = true;
                //            return; 
                //        }
                //    }
                //}

                //if (SysDate < DtpToDate.Value.Date )
                //{
                //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3074, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                //    TmrEmployeeSettlement.Enabled = true;
                //    DtpToDate.Focus();
                //    return;
                //}
                     
                //if (SysDate < DateDateTimePicker.Value.Date )
                //{
                //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3074, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                //    TmrEmployeeSettlement.Enabled = true;
                //    DateDateTimePicker.Focus();
                //    return;
                //}

                if (DtpToDate.Value.Date < DtpFromDate.Value.Date)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 19, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmrEmployeeSettlement.Enabled = true;
                    DtpFromDate.Focus();
                    return;
                }

                if (MobjclsBLLSettlementProcess.IsPaymentReleaseCheckingPrevious() == 1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 674, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    BtnProcess.Focus();
                    return;
                }
                
                if ( MobjclsBLLSettlementProcess.IsPaymentReleaseChecking()  == 1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 678, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DtpToDate.Focus();
                    return;
                }
        
                if (MobjclsBLLSettlementProcess.IsAttendanceExisting()  == 1)
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 685, out MmessageIcon);
        
                Fillparameters(0);
                FillProcessDetails();
                BtnCancel.Enabled = false;

                if (EmployeeSettlementDetailDataGridView.RowCount >= 0)
                    EmployeeSettlementDetailDataGridView.ClearSelection();

                if ((TotalEarningSettlementAmt() - TotalDedSettlementAmt()) < 0)
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 657, out MmessageIcon);
                else
                    lblPayBack.Text = "";
            }

            bProcess = true;
            bClickProcess = true;
            EmployeeSettlementBindingNavigatorSaveItem_Click(sender, e);
            EmployeeSettlementBindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnProcess.Enabled = false;
        }



        private void DateSetts()
        {
            try
            {
                if (SysDate < DtpToDate.Value.Date)
                {
                    MobjclsBLLSettlementProcess.EmployeeSetAttTransaction(CboEmployee.SelectedValue.ToInt32(), DtpToDate.Value.ToString("dd-MMM-yyyy"));
                }
            }
            catch
            {
            }

        }

        private void  FillProcessDetails()
        {
            try
            {


                DateSetts();



                datTempSalary = MobjclsBLLSettlementProcess.SettlementProcess();
                EmployeeSettlementDetailDataGridView.Rows.Clear();
                DtGrdGratuity.Rows.Clear(); 

                if (datTempSalary != null)
                {
                    if (datTempSalary.Rows.Count > 0)
                    {
                        EmployeeSettlementDetailDataGridView.Rows.Add();
                        EmployeeSettlementDetailDataGridView.Rows[0].Cells["Particulars"].Value = "Salary (" + Convert.ToDateTime(datTempSalary.Rows[0]["PeriodFrom"].ToString()).ToString("dd MMM yyyy") + " - " + Convert.ToDateTime(datTempSalary.Rows[0]["PeriodTo"].ToString()).ToString("dd MMM yyyy") + ")";

                        if (dblTempSalary >= 0)
                        {
                            EmployeeSettlementDetailDataGridView.Rows[0].Cells["IsAddition"].Value = 1;
                            EmployeeSettlementDetailDataGridView.Rows[0].Cells["Credit"].Value = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.PaymentAmount;
                            EmployeeSettlementDetailDataGridView.Rows[0].Cells["Debit"].Value = 0;
                        }
                        else
                        {
                            EmployeeSettlementDetailDataGridView.Rows[0].Cells["IsAddition"].Value = 0;
                            EmployeeSettlementDetailDataGridView.Rows[0].Cells["Credit"].Value = 0;
                            EmployeeSettlementDetailDataGridView.Rows[0].Cells["Debit"].Value = Math.Abs(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.PaymentAmount);
                        }

                        EmployeeSettlementDetailDataGridView.Rows[0].Cells["LoanID"].Value = 0;
                        EmployeeSettlementDetailDataGridView.Rows[0].Cells["Credit"].ReadOnly = true;
                        EmployeeSettlementDetailDataGridView.Rows[0].Cells["Debit"].ReadOnly = true;
                        EmployeeSettlementDetailDataGridView.Rows[0].Cells["Flag"].Value = 1;
                        EmployeeSettlementDetailDataGridView.Rows[0].Cells["SalaryPaymentID"].Value = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.PaymentID;
                    }
                }

                DataSet DTSet;
                DTSet = MobjclsBLLSettlementProcess.SettlementProcessDetail();

                if (DTSet.Tables[0].Rows.Count > 0)
                {
                    int iRow;

                    for (int i = 0; DTSet.Tables[0].Rows.Count - 1 >= i; ++i)
                    {
                        EmployeeSettlementDetailDataGridView.RowCount = EmployeeSettlementDetailDataGridView.RowCount + 1;
                        iRow = EmployeeSettlementDetailDataGridView.RowCount - 1;

                        if (iRow <= 0)
                            iRow = 0;

                        EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Particulars"].Value = Convert.ToString(DTSet.Tables[0].Rows[i]["Particulars"]);
                        EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["IsAddition"].Value = Convert.ToDouble(DTSet.Tables[0].Rows[i]["IsAddition"]);
                        EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Credit"].Value = Convert.ToDouble(DTSet.Tables[0].Rows[i]["Credit"]);
                        EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["LoanID"].Value = Convert.ToDouble(DTSet.Tables[0].Rows[i]["LoanID"]);

                        if (DTSet.Tables[0].Rows[i]["IsAddition"].ToInt32() == 1)
                        {
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Credit"].Value = Convert.ToDouble(DTSet.Tables[0].Rows[i]["Credit"]);
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Debit"].Value = 0;
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Credit"].ReadOnly = false;
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Debit"].ReadOnly = true;
                        }
                        else
                        {
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Debit"].Value = Convert.ToDouble(DTSet.Tables[0].Rows[i]["Debit"]);
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Credit"].Value = 0;
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Credit"].ReadOnly = true;
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Debit"].ReadOnly = false;
                        }

                        EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Flag"].Value = 1;
                        EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["SalaryPaymentID"].Value = "0";
                    }
                }


                if (DTSet.Tables[1].Rows.Count > 0)
                {
                    int iRow;

                    for (int i = 0; DTSet.Tables[1].Rows.Count - 1 >= i; ++i)
                    {
                        DtGrdGratuity.RowCount = DtGrdGratuity.RowCount + 1;
                        iRow = DtGrdGratuity.RowCount - 1;

                        if (iRow <= 0)
                            iRow = 0;

                        DtGrdGratuity.Rows[iRow].Cells["ColYears"].Value = Convert.ToString(DTSet.Tables[1].Rows[i]["Years"]);
                        DtGrdGratuity.Rows[iRow].Cells["ColCalDays"].Value = Convert.ToDouble(DTSet.Tables[1].Rows[i]["CalculationDays"]);
                        DtGrdGratuity.Rows[iRow].Cells["ColGRTYDays"].Value = Convert.ToDouble(DTSet.Tables[1].Rows[i]["GratuityDays"]);
                        DtGrdGratuity.Rows[iRow].Cells["ColGratuityAmt"].Value = Convert.ToDouble(DTSet.Tables[1].Rows[i]["GratuityAmount"]);
                    }
                }

            }
            catch
            { }


        }

        private void DtpToDate_ValueChanged(object sender, EventArgs e)
        {
            ErrEmployeeSettlement.Clear();
            ChangeStatus();
            ClearPartially();
            GetSettlementPolicyAndExperience();

            if (CboEmployee.SelectedIndex >= 0 && CboType.SelectedIndex >= 0 && MbAddStatus == true)
                BtnProcess.Enabled = MbAddPermission;

            CalculateLeavePayDays();
        }

        private void ChangeStatus()
        {
            LblEmployeeSettlement.Text = "";

            if (MbAddStatus == true)
                MbChangeStatus = MbAddPermission; 
            else
                MbChangeStatus = MbUpdatePermission;

            try
            {
                if (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Closed == false)
                {
                    OKSaveButton.Enabled = MbUpdatePermission;
                    BtnSave.Enabled = MbChangeStatus;
                    EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbChangeStatus;
                }
            }
            catch { }
        }
  
        private void DateDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            if (bLoad == false && bProcess == false )
                lblPayBack.Text = "";

            ErrEmployeeSettlement.Clear();
            ChangeStatus();
            MbChangeStatus = true;
        }

        private void txtAbsentDays_TextChanged(object sender, EventArgs e)
        {
            if (txtAbsentDays.Text == "" )
                txtAbsentDays.Text = "0";
        }

        private void txtLeavePayDays_TextChanged(object sender, EventArgs e)
        {
            if (txtLeavePayDays.Text == "" )
                txtLeavePayDays.Text = "0";
        }

        private bool ColDuplicateChk(string ParaString) 
        {
            try
            {
                for (int i = 0; EmployeeSettlementDetailDataGridView.RowCount > i;i++)
                {
                    if (EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value != null)
                    {
                        if (EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value.ToString() != "")
                        {
                            if (Convert.ToString(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value).Trim().ToUpper()   == ParaString.Trim().ToUpper()  )
                                return true;
                        }
                    }
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            try 
            {
                if ( txtAmount.Text.Trim() == "" || txtAmount.Text.Trim() == ".")
                    return;

                if (TxtParticulars.Text.Trim() == "" || Convert.ToDouble(txtAmount.Text) == 0)
                    return; 

                if (ColDuplicateChk(Convert.ToString(TxtParticulars.Text)) == true)
                    return;
            
                ChangeStatus();
                int i = 0;
                i = EmployeeSettlementDetailDataGridView.RowCount;                
                EmployeeSettlementDetailDataGridView.RowCount = EmployeeSettlementDetailDataGridView.RowCount + 1;
                EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value= Convert.ToString(TxtParticulars.Text);

                if ( chkIsAddition.Checked == true)
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["IsAddition"].Value = 1;
                else
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["IsAddition"].Value = 0;

                if (Convert.ToInt32(EmployeeSettlementDetailDataGridView.Rows[i].Cells["IsAddition"].Value) == 1)
                {
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value= Convert.ToDouble(txtAmount.Text);
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value= 0;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].ReadOnly = false;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].ReadOnly = true;
                }
                else
                {
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].ReadOnly = true;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].ReadOnly = false;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value= Convert.ToDouble(txtAmount.Text);
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value= 0;
                }

                EmployeeSettlementDetailDataGridView.Rows[i].Cells["Flag"].Value= 1;
                TxtTotalEarning.Text = (TotalEarningSettlementAmt()).ToString();
                TxtTotalDeduction.Text = (TotalDedSettlementAmt()).ToString();
                NetAmountTextBox.Text = (TotalEarningSettlementAmt() - TotalDedSettlementAmt()).ToString();          
            }
            catch { }
        }

        private void DocumentReturnCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            MbChangeStatus = true;

            if (MbAddStatus)
                BtnSave.Enabled = EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbAddPermission;
            else
                OKSaveButton.Enabled = BtnSave.Enabled = EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbUpdatePermission;
        }

        private void OKSaveButton_Click(object sender, EventArgs e)
        {
            try 
            {
                if (MbUpdatePermission == false)
                    return;

                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AddEditMode = 0;
                if (FormValidation() == true)
                {
                    blnConfirm = true;
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 680, out MmessageIcon);

                    if (MessageBox.Show(MsMessageCommon, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;

                    blnConfirm = true;
                    Fillparameters(1);

                    if (MobjclsBLLSettlementProcess.SaveEmployeeSettlement() == true)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        TmrEmployeeSettlement.Enabled = true;
                        MbChangeStatus = false;
                        DisplayInfo(0);

                        OKSaveButton.Enabled = BtnSave.Enabled = EmployeeSettlementBindingNavigatorSaveItem.Enabled = false;
                        BindingNavigatorDeleteItem.Enabled = false;
                        BindingNavigatorAddNewItem.Enabled = MbAddPermission;
                        BtnCancel.Enabled = false;
                    }

                    EnableDisablePartially();
                }
            }
            catch { }
        }

        private double  TotalEarningSettlementAmt()
        {
            try
            {
                double dEmployeeSettlementAmt = 0;
                int i;

                if (EmployeeSettlementDetailDataGridView.RowCount > 0)
                {
                    for( i = 0;EmployeeSettlementDetailDataGridView.Rows.Count - 1>=i;++i)
                    {
                        dEmployeeSettlementAmt = dEmployeeSettlementAmt + 
                            (EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value==null ? 0 : 
                            Convert.ToDouble(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value));
                    }
                }

                return dEmployeeSettlementAmt;
            }
            catch
            {
                return 0;
            }
        }

        private double TotalDedSettlementAmt()
        {
            try
            {
                double dblEmployeeDedSettlementAmt = 0;

                if (EmployeeSettlementDetailDataGridView.RowCount > 0)
                {
                    int i;

                    for (i = 0;EmployeeSettlementDetailDataGridView.Rows.Count - 1>=i;++i)
                    {
                        dblEmployeeDedSettlementAmt = dblEmployeeDedSettlementAmt +
                            ((EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value) == null ? 0 : 
                            Convert.ToDouble(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value));
                    }
                }

                return dblEmployeeDedSettlementAmt;
            }
            catch 
            {
                return 0;
            }
        }

        private string fillremarks() 
        {
            try
            {
                string  SetMessage;
                //if (ClsCommonSettings.IsArabicView)
                //    SetMessage = "عامل تسوية";
                //else
                    SetMessage = "Employee Settlement ";
                NetAmountTextBox.Text = NetAmountTextBox.Text.Replace("(", "").Replace(")", "");

                if (Convert.ToDouble((NetAmountTextBox.Text.Trim() == "" ?  "0" : NetAmountTextBox.Text)) > 0)
                {
                    if (Convert.ToDouble((TxtTotalEarning.Text.Trim() == "" ? "0" : TxtTotalEarning.Text)) - Convert.ToDouble((TxtTotalDeduction.Text == "" ? "0" : TxtTotalDeduction.Text.Trim())) > 0)
                        return SetMessage;
                    else
                        return CboEmployee.Text.Trim();
                }

                return RemarksTextBox1.Text.Trim();
            }
            catch
            {
                return RemarksTextBox1.Text.Trim();
            }
        }

        private void CboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int miCurrencyIdt = 0; int MiCompanyIDt = 0; string LblCurrencyt = ""; string DtpFromDatet = ""; int TransactionTypeIDt = 0;
                if (IsFormLoad == true)
                {
                    lblPayBack.Text = "";
                    DateTime TempDateTime = new DateTime(1900, 1, 1, 0, 0, 0);
                    bLoad = true;
                    ChangeStatus();

                    if (CboEmployee.SelectedIndex != -1)
                    {
                        if (MbAddStatus == true)
                        {
                            if (CboEmployee.SelectedIndex >= 0)
                                MobjclsBLLSettlementProcess.GetSalaryReleaseDate(Convert.ToInt32(CboEmployee.SelectedValue), ref TempDateTime);
                        }
                    }


                    MobjclsBLLSettlementProcess.DisplayCurrencyEmployee(Convert.ToInt32(CboEmployee.SelectedValue), ref  miCurrencyIdt, ref  MiCompanyIDt, ref  LblCurrencyt, ref  DtpFromDatet, ref TransactionTypeIDt);
                    miCurrencyId = miCurrencyIdt;
                    MiCompanyID = MiCompanyIDt;
                    LblCurrency.Text = LblCurrencyt;

                    if (DtpFromDatet != "")
                    {
                        if (Convert.ToDateTime(DtpFromDatet) > Convert.ToDateTime("01-Jan-1900"))
                            DtpFromDate.Value = DtpFromDatet.ToDateTime();
                    }

                    GetSettlementPolicyAndExperience();
                    EmployeeHistoryDetail();

                    if (MbAddStatus == true)
                        cboTransactionType.SelectedValue = TransactionTypeIDt;

                    if (CboEmployee.SelectedIndex >= 0 && CboType.SelectedIndex >= 0 && MbAddStatus == true)
                        BtnProcess.Enabled = MbAddPermission;
                }
                else if (CboEmployee.SelectedIndex >-1)
                {
                    ///4 getting currencyid
                    MobjclsBLLSettlementProcess.DisplayCurrencyEmployee(Convert.ToInt32(CboEmployee.SelectedValue), ref  miCurrencyIdt, ref  MiCompanyIDt, ref  LblCurrencyt, ref  DtpFromDatet, ref TransactionTypeIDt);
                    miCurrencyId = miCurrencyIdt;
                    MiCompanyID = MiCompanyIDt;
                    LblCurrency.Text = LblCurrencyt;
                }
            }
            catch { }
        }

        private void GetSettlementPolicyAndExperience()
        {
            string SetPolicyDescStr = ""; string lblNoofMonthsStr = ""; string txtNofDaysExperienceStr = ""; string txtLeavePayDaysStr = "";
            string lblTakenLeavePayDaysStr = ""; string lblEligibleLeavePayDaysStr = ""; string txtAbsentDaysStr = "";

            try
            {
                if (CboEmployee.SelectedIndex > -1)
                {
                    MobjclsBLLSettlementProcess.GetSettlementPolicyAndExperience(Convert.ToInt32(CboEmployee.SelectedValue),
                        DtpFromDate.Value.ToString("dd-MMM-yyyy"), DtpToDate.Value.ToString("dd-MMM-yyyy"), ref SetPolicyDescStr, ref lblNoofMonthsStr,
                        ref txtNofDaysExperienceStr, ref txtLeavePayDaysStr, ref lblTakenLeavePayDaysStr, ref lblEligibleLeavePayDaysStr, ref txtAbsentDaysStr);

                    lblSettlementPolicy.Text = SetPolicyDescStr;
                    lblNoofMonths.Text = lblNoofMonthsStr;
                    txtNofDaysExperience.Text = Math.Abs(txtNofDaysExperienceStr.ToDecimal()).ToString();
                    txtLeavePayDays.Text = txtLeavePayDaysStr;
                    lblEligibleLeavePayDays.Text = lblEligibleLeavePayDaysStr;
                    lblTakenLeavePayDays.Text = lblTakenLeavePayDaysStr;
                    lblEligibleLeavePayDays.Text = lblEligibleLeavePayDaysStr;
                    txtAbsentDays.Text = txtAbsentDaysStr;
                    GetEmployeeLeaveHolidays();
                }
            }
            catch { }
        }

        private void EmployeeHistoryDetail()
        {
            try
            {
                DataTable DT;
                if (CboEmployee.SelectedIndex != -1)
                    MobjclsBLLSettlementProcess.clsDTOSettlementProcess.EmployeeID = Convert.ToInt32(CboEmployee.SelectedValue);

                DT = MobjclsBLLSettlementProcess.EmployeeHistoryDetail();

                if (DT.Rows.Count > 0)
                {
                    DtGrdEmpHistory.Rows.Clear();
                    DtGrdEmpHistory.RowCount = 0;

                    for (int i = 0; DT.Rows.Count > i; ++i)
                    {
                        DtGrdEmpHistory.RowCount = DtGrdEmpHistory.RowCount + 1;
                        DtGrdEmpHistory.Rows[i].Cells[0].Value = DT.Rows[i]["Descriptio"];
                        DtGrdEmpHistory.Rows[i].Cells[1].Value = DT.Rows[i]["PeriodFrom"];

                        if (DT.Rows[i]["PeriodTo"] != System.DBNull.Value)
                            DtGrdEmpHistory.Rows[i].Cells[2].Value = DT.Rows[i]["PeriodTo"];

                        DtGrdEmpHistory.Rows[i].Cells[3].Value = DT.Rows[i]["NetAmount"];
                    }
                }
            }
            catch { }
        }

        private void DisplayInfo(int flg)
        {
            MobjclsBLLSettlementProcess.clsDTOSettlementProcess.strSearchKey = txtSearch.Text.Trim();
            if (flg == 0)
            {
                IsFormLoad = false;
                LoadCombos(1);
                MobjclsBLLSettlementProcess.DisplayInfo();

                if (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID > 0)
                {
                    CboEmployee.Focus();
                    LblEmployee.Tag = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Closed;
                    LblType.Tag = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID;
                    CboEmployee.SelectedValue = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.EmployeeID;
                    CboType.SelectedValue = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementType;
                    DtpFromDate.Value = Convert.ToDateTime(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.FromDate);
                    DtpToDate.Value = Convert.ToDateTime(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ToDate);
                    DateDateTimePicker.Value =Convert.ToDateTime( MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Date);
                    DocumentReturnCheckBox.Checked = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.DocumentReturn;
                    RemarksTextBox1.Text = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Remarks;
                    cboTransactionType.SelectedValue = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.TransactionTypeID;
                    txtChequeNo.Text = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ChequeNumber;
                    dtpChequeDate.Value = Convert.ToDateTime(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ChequeDate);
                    txtNofDaysExperience.Text = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ExperienceDays.ToString();
                    cboAccount.SelectedValue = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AccountID.ToInt32();     
                    string SetPolicyDescStr = ""; string lblNoofMonthsStr = ""; string txtNofDaysExperienceStr = ""; string txtLeavePayDaysStr = "";
                    string lblTakenLeavePayDaysStr = ""; string lblEligibleLeavePayDaysStr = ""; string txtAbsentDaysStr = "";

                    MobjclsBLLSettlementProcess.GetSettlementPolicyAndExperience(Convert.ToInt32(CboEmployee.SelectedValue),
                        DtpFromDate.Value.ToString("dd-MMM-yyyy"), DtpToDate.Value.ToString("dd-MMM-yyyy"), ref SetPolicyDescStr, ref lblNoofMonthsStr,
                        ref txtNofDaysExperienceStr, ref txtLeavePayDaysStr, ref lblTakenLeavePayDaysStr, ref lblEligibleLeavePayDaysStr, ref txtAbsentDaysStr);

                    lblNoofMonths.Text = lblNoofMonthsStr;                    
                    txtLeavePayDays.Text = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.LeavePayDays.ToString(); ;
                    chkAbsent.Checked = (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ConsiderAbsentDays.ToBoolean() == false? false : true);
                    txtAbsentDays.Text = txtAbsentDaysStr;
                    CalculateLeavePayDays();
                    txtLeavePayDays.Text = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.LeavePayDays.ToString(); ;

                }
            }

            if (flg == 1)
                MobjclsBLLSettlementProcess.FillDetails(Convert.ToInt32(LblType.Tag), Convert.ToInt32(CboEmployee.SelectedValue));

            EmployeeSettlementDetailDataGridView.Rows.Clear();
            int i=0;

            if (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOSettlementDetails != null)
            {
                foreach (clsDTOSettlementDetails objclsDTOSettlementDetails in MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOSettlementDetails)
                {
                    EmployeeSettlementDetailDataGridView.RowCount = EmployeeSettlementDetailDataGridView.RowCount + 1;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["EmployeeSettlementDetailID"].Value = objclsDTOSettlementDetails.EmployeeSettlementDetailID;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value = objclsDTOSettlementDetails.Particulars;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["IsAddition"].Value = objclsDTOSettlementDetails.IsAddition;

                    if (objclsDTOSettlementDetails.IsAddition == true)
                    {
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value = objclsDTOSettlementDetails.Amount;
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value = 0;
                    }
                    else
                    {
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value = 0;
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value = objclsDTOSettlementDetails.Amount;
                    }

                    if (objclsDTOSettlementDetails.IsAddition == true)
                    {
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].ReadOnly = false;
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].ReadOnly = true;
                    }
                    else
                    {
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].ReadOnly = true;
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].ReadOnly = false;
                    }

                    //if (ClsCommonSettings.IsAmountRoundByZero)
                    //{
                    //    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value = EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value.ToDecimal().ToString("F" + 0);
                    //    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value = EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value.ToDecimal().ToString("F" + 0);
                    //}

                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["LoanID"].Value = objclsDTOSettlementDetails.LoanID;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Flag"].Value = 1;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["SalaryPaymentID"].Value = objclsDTOSettlementDetails.SalaryPaymentID;
                    i = ++i;
                }


                //-----------------------------------------------------------------------------------------------
                DtGrdGratuity.Rows.Clear();
                i = 0;

                if (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOGratuityDetails != null)
                {
                    foreach (clsDTOGratuityDetails objclsDTOGratuityDetails in MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOGratuityDetails)
                    {
                        DtGrdGratuity.RowCount = DtGrdGratuity.RowCount + 1;
                        DtGrdGratuity.Rows[i].Cells["ColYears"].Value = objclsDTOGratuityDetails.ColYears;
                        DtGrdGratuity.Rows[i].Cells["ColCalDays"].Value = objclsDTOGratuityDetails.ColCalDays;
                        DtGrdGratuity.Rows[i].Cells["ColGRTYDays"].Value = objclsDTOGratuityDetails.ColGRTYDays;
                        DtGrdGratuity.Rows[i].Cells["ColGratuityAmt"].Value = objclsDTOGratuityDetails.ColGratuityAmt;
                        i = i + 1;
                    }
                }
                //-----------------------------------------------------------------------------------------------
            }
            IsFormLoad = true;
            TxtTotalEarning.Text = (TotalEarningSettlementAmt()).ToString();
            TxtTotalDeduction.Text = (TotalDedSettlementAmt()).ToString();
            NetAmountTextBox.Text = Convert.ToString(Convert.ToDecimal(TxtTotalEarning.Text) - Convert.ToDecimal(TxtTotalDeduction.Text));
            BtnCancel.Enabled = false;
        }

        private void RecCount()
        {
            RecordCnt = MobjclsBLLSettlementProcess.GetRecordCount(); 

            if (RecordCnt > 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
                BindingNavigatorPositionItem.Text = Convert.ToString(RecordCnt);
                CurrentRecCnt = RecordCnt;
            }
            else
            {
                CurrentRecCnt = 0;
                BindingNavigatorCountItem.Text = strBindingOf + "0";
            }

            BindingNavigatorMoveNextItem.Enabled = false;
            BindingNavigatorMoveLastItem.Enabled = false;

            if (RecordCnt >= 1)
            {
                BindingNavigatorMovePreviousItem.Enabled = true;
                BindingNavigatorMoveFirstItem.Enabled = true;
            }
            else
            {
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
            }
        }

        private void Fillparameters(int TypeIDD)
        {
            try
            {
                if (MbAddStatus == true)
                    MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AddEditMode = 1;
                else
                    MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AddEditMode = 2;                

                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID = Convert.ToInt32(LblType.Tag);
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.EmployeeID = Convert.ToInt32(CboEmployee.SelectedValue);
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Date = DateDateTimePicker.Value.Date.ToString("dd-MMM-yyy");
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.DocumentReturn = DocumentReturnCheckBox.Checked;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AssetReturn = DocumentReturnCheckBox.Checked;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Remarks = fillremarks();
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.NetAmount = Convert.ToDecimal(TotalEarningSettlementAmt().ToDecimal()  - TotalDedSettlementAmt().ToDecimal() );
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.CurrencyID = miCurrencyId;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementType = Convert.ToInt32(CboType.SelectedValue);
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.FromDate = DtpFromDate.Value.Date.ToString("dd-MMM-yyyy") ;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ToDate = DtpToDate.Value.Date.ToString("dd-MMM-yyyy");
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Closed = blnConfirm;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.TransactionTypeID=Convert.ToInt32(cboTransactionType.SelectedValue);  
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ChequeNumber=txtChequeNo.Text;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ChequeDate = dtpChequeDate.Value.Date.ToString("dd-MMM-yyyy");
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.CreatedBy = ClsCommonSettings.UserID;
                MiCompanyID = MobjclsBLLSettlementProcess.GetEmpCompanyID( MobjclsBLLSettlementProcess.clsDTOSettlementProcess.EmployeeID);

                if (MiCompanyID <= 0)
                    MiCompanyID = ClsCommonSettings.CompanyID;

                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.CompanyID = MiCompanyID;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.PaymentID = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.PaymentIdTemp  ;   
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ExperienceDays  =  Convert.ToDecimal(txtNofDaysExperience.Text);
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.EligibleLeavePayDays=  Convert.ToDecimal(lblEligibleLeavePayDays.Text.ToDecimal())  ;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.TakenLeavePayDays = Convert.ToDecimal(lblTakenLeavePayDays.Text.ToDecimal());
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.VacationDays = Convert.ToDecimal(txtLeavePayDays.Text.ToDecimal());
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.LeavePayDays = Convert.ToDecimal(txtLeavePayDays.Text.ToDecimal());
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AbsentDays = Convert.ToDecimal((txtAbsentDays.Text =="" ? "0" : txtAbsentDays.Text));
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ConsiderAbsentDays = chkAbsent.Checked;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AccountID = cboAccount.SelectedValue.ToInt32();   
  
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.blnIsExcludeHolidays = chkExcludeHolidays.Checked;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.decExcludeHolidays = txtExcludeHolidays.Text.ToDecimal();
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.blnIsExcludeLeaveDays = chkExcludeLeaveDays.Checked;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.decExcludeLeaveDays = txtExcludeLeaveDays.Text.ToDecimal();
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.decEligibleDays = txtEligibleDays.Text.ToDecimal();

                if (TypeIDD == 1)
                {
                    MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOSettlementDetails = new System.Collections.Generic.List<clsDTOSettlementDetails>();
                    
                    for (int i = 0; Convert.ToInt32(EmployeeSettlementDetailDataGridView.Rows.Count) - 1 >= i; ++i)
                    {
                        clsDTOSettlementDetails objclsDTOSettlementDetails = new clsDTOSettlementDetails();

                        if (EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value != null)
                        {
                            if (EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value.ToString() != "")
                            {
                                objclsDTOSettlementDetails.EmployeeSettlementDetailID = Convert.ToInt32(EmployeeSettlementDetailDataGridView.Rows[i].Cells["EmployeeSettlementDetailID"].Value);
                                objclsDTOSettlementDetails.Particulars = Convert.ToString(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value);

                                if (Convert.ToInt32(EmployeeSettlementDetailDataGridView.Rows[i].Cells["IsAddition"].Value) == 1)
                                {
                                    objclsDTOSettlementDetails.IsAddition = true;
                                    objclsDTOSettlementDetails.AmountCredit = Convert.ToDecimal(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value);
                                    objclsDTOSettlementDetails.Amount = Convert.ToDecimal(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value);
                                    objclsDTOSettlementDetails.AmountDebit =0;
                                }
                                else
                                {
                                    objclsDTOSettlementDetails.IsAddition = false;
                                    objclsDTOSettlementDetails.AmountDebit = Convert.ToDecimal(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value);
                                    objclsDTOSettlementDetails.Amount = Convert.ToDecimal(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value);
                                    objclsDTOSettlementDetails.AmountCredit = 0;
                                }

                                objclsDTOSettlementDetails.LoanID = Convert.ToInt32(EmployeeSettlementDetailDataGridView.Rows[i].Cells["LoanID"].Value);
                                objclsDTOSettlementDetails.SalaryPaymentID = Convert.ToInt32(EmployeeSettlementDetailDataGridView.Rows[i].Cells["SalaryPaymentID"].Value);
                                objclsDTOSettlementDetails.flag = Convert.ToInt32(EmployeeSettlementDetailDataGridView.Rows[i].Cells["flag"].Value);
                                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOSettlementDetails.Add(objclsDTOSettlementDetails);
                            }
                        }
                    }
                }



                if (TypeIDD == 1)
                {
                    MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOGratuityDetails = new System.Collections.Generic.List<clsDTOGratuityDetails>();
                    for (int i = 0; Convert.ToInt32(DtGrdGratuity.Rows.Count) - 1 >= i; ++i)
                    {
                        clsDTOGratuityDetails objclsDTOGratuityDetails = new clsDTOGratuityDetails();
                        objclsDTOGratuityDetails.ColYears = Convert.ToString(DtGrdGratuity.Rows[i].Cells["ColYears"].Value);
                        objclsDTOGratuityDetails.ColCalDays = Convert.ToDecimal(DtGrdGratuity.Rows[i].Cells["ColCalDays"].Value);
                        objclsDTOGratuityDetails.ColGRTYDays = Convert.ToDecimal(DtGrdGratuity.Rows[i].Cells["ColGRTYDays"].Value);
                        objclsDTOGratuityDetails.ColGratuityAmt = Convert.ToDecimal(DtGrdGratuity.Rows[i].Cells["ColGratuityAmt"].Value);
                        MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOGratuityDetails.Add(objclsDTOGratuityDetails);
                    }
                }



            }
            catch { }
        }

        private void cboTransactionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTransactionType();
            ChangeStatus();
            if (cboTransactionType.SelectedValue.ToInt32() > 0)
            {
                if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentTransactionType.Bank && CboEmployee.SelectedValue.ToInt32() > 0)
                    LoadCombos(3);//Bank
            }

        }

        private void SetTransactionType()
        {
            if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentTransactionType.Bank)
            {
                txtChequeNo.Enabled = true;
                dtpChequeDate.Enabled = true;
                txtChequeNo.BackColor = System.Drawing.SystemColors.Info;
            }
            else
            {
                txtChequeNo.Enabled = false;
                dtpChequeDate.Enabled = false;
                txtChequeNo.Text = "";
                dtpChequeDate.Value = DateTime.Now;
                txtChequeNo.BackColor = System.Drawing.SystemColors.Window;
            }
        }

        private void cboAccount_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtChequeNo_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboTransactionType_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboTransactionType.DroppedDown = false; 
        }

        private void EmployeeSettlementDetailDataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (EmployeeSettlementDetailDataGridView.CurrentCell != null)
                {
                    if (EmployeeSettlementDetailDataGridView.CurrentCell.ColumnIndex == Credit.Index || EmployeeSettlementDetailDataGridView.CurrentCell.ColumnIndex == Debit.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
            }
            catch { }
        }

        void txtDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;


            //if (ClsCommonSettings.IsAmountRoundByZero)
            //{
            //    strInvalidChars=strInvalidChars + ".";
            //}

            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
                e.Handled = true;
        }
        
        void cb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            cb.DroppedDown = false;
        }

        private void RemarksTextBox1_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dtpChequeDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void EmployeeSettlementDetailDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            ChangeStatus();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            EmployeeSettlementBindingNavigatorSaveItem_Click(sender, e); 
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {

                BtnPrintRpt_Click(sender, e);
                //FrmReportviewer ObjViewer = new FrmReportviewer();
                //ObjViewer.PsFormName = "Employee Settlement";
                //ObjViewer.PiRecId = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID;
                //ObjViewer.PiFormID = (int)FormID.SettlementProcess;
                //ObjViewer.No = 0;

                //if (datTempSalary != null)
                //{
                //    if (datTempSalary.Rows.Count > 0)
                //    {
                //        ObjViewer.dteFromDate = Convert.ToDateTime(datTempSalary.Rows[0]["PeriodFrom"]);
                //        ObjViewer.dteToDate = Convert.ToDateTime(datTempSalary.Rows[0]["PeriodTo"]);
                //        ObjViewer.No = 1;
                //    }
                //}

                //ObjViewer.ShowDialog();
            }
            catch { }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
           try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "EmployeeSettlement";
                    ObjEmailPopUp.EmailFormType = EmailFormID.SettlementProcess;
                    ObjEmailPopUp.EmailSource = MobjclsBLLSettlementProcess.EmailDetail(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID, MobjclsBLLSettlementProcess.clsDTOSettlementProcess.EmployeeID);
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch { }
        }

        private void BtnBottomCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CalculateLeavePayDays()
        {
            if (CboEmployee.SelectedIndex >= 0)
            {
                int intEmpID = CboEmployee.SelectedValue.ToInt32();
                CboEmployee.SelectedIndex = -1;
                CboEmployee.SelectedValue = intEmpID;

                if (chkAbsent.Checked == true)
                {
                    decimal decLeavePayDays = (Convert.ToDecimal(txtLeavePayDays.Text) - Convert.ToDecimal(txtAbsentDays.Text));

                    if (decLeavePayDays > 0)
                        txtLeavePayDays.Text = decLeavePayDays.ToString();
                    else
                        txtLeavePayDays.Text = "0";
                }
            }
            else
                txtLeavePayDays.Text = "0";
        }

        private void chkAbsent_CheckedChanged(object sender, EventArgs e)
        {
            CalculateLeavePayDays();
            ChangeStatus();
        }

        private void RecordCount()
        {
            MobjclsBLLSettlementProcess.clsDTOSettlementProcess.strSearchKey = txtSearch.Text.Trim(); // Is any search text filteration will be taken that also
            TotalRecordCnt = MobjclsBLLSettlementProcess.GetRecordCount();
            CurrentRecCnt = TotalRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();

            if (TotalRecordCnt == 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                CurrentRecCnt = 0;
                TotalRecordCnt = 0;
            }

            BindingEnableDisable();
        }

        private void BindingEnableDisable() // Navigator visibility settings
        {
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;

            if (TotalRecordCnt == 0)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == Convert.ToDouble(TotalRecordCnt))
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                else if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == 1)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
            }
        }

         private void SetAutoCompleteList()
         {
             this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
             DataTable dt = clsUtilities.GetAutoCompleteList(txtSearch.Text.Trim(), 0, "", "PayEmployeeSettlement");
             this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);
         }

         private void btnSearch_Click(object sender, EventArgs e)
         {
             if (txtSearch.Text == string.Empty)
                 mblnSearchStatus = false;
             else
                 mblnSearchStatus = true;

             if (this.txtSearch.Text.Trim() == string.Empty)
                 return;

             RecordCount(); // get record count

             if (TotalRecordCnt == 0) // No records
             {
                 MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 40, out MmessageIcon);
                 MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                 TmrEmployeeSettlement.Enabled = true;
                 txtSearch.Text = string.Empty;
                 BindingNavigatorAddNewItem_Click(sender, e);
             }
             else if (TotalRecordCnt > 0)
                 BindingNavigatorMoveFirstItem_Click(sender, e); // if record found in search then automatically navigate to first item
         }

         private void FrmSettlementEntry_KeyDown(object sender, KeyEventArgs e)
         {
             try
             {
                 switch (e.KeyData)
                 {
                     case Keys.F1:
                         FrmHelp objHelp = new FrmHelp();
                         objHelp.strFormName = "Settlement process";
                         objHelp.ShowDialog();
                         objHelp = null;
                         break;
                     case Keys.Escape:
                         this.Close();
                         break;
                     case Keys.Control | Keys.Enter:
                         BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                         break;
                     case Keys.Alt | Keys.R:
                         BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                         break;
                     case Keys.Control | Keys.E:
                         BtnCancel_Click(sender, new EventArgs());//Clear
                         break;
                     case Keys.Control | Keys.Left:
                         BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                         break;
                     case Keys.Control | Keys.Right:
                         BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                         break;
                     case Keys.Control | Keys.Up:
                         BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                         break;
                     case Keys.Control | Keys.Down:
                         BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                         break;
                     case Keys.Control | Keys.P:
                         BtnPrint_Click(sender, new EventArgs());
                         break;
                     case Keys.Control | Keys.M:
                         btnEmail_Click(sender, new EventArgs());
                         break;
                 }
             }
             catch { }
         }

         private void EmployeeSettlementDetailDataGridView_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
         {
             ChangeStatus();
         }

         private void EmployeeSettlementDetailDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
         {
             try
             {
                 if (e.ColumnIndex > -1 && e.RowIndex > -1)
                 {
                     if (EmployeeSettlementDetailDataGridView.Rows[e.RowIndex].Cells[SalaryPaymentID.Index].Value.ToInt32() > 0)
                     {
                         e.Cancel = true;
                         return;
                     }
                 }
             }
             catch { }
         }

         private void EmployeeSettlementDetailDataGridView_KeyDown(object sender, KeyEventArgs e)
         {
             try
             {
                 if (e.KeyData == Keys.Delete && EmployeeSettlementDetailDataGridView.CurrentCell != null)
                 {
                     if (EmployeeSettlementDetailDataGridView.CurrentRow.Cells[SalaryPaymentID.Index].Value.ToInt32() > 0)
                     {
                         if (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID > 0)
                         {
                             MobjclsBLLSettlementProcess.DeletePaymentDetails();
                         }
                     }

                 }
             }
             catch { }
         }

         private void BtnHelp_Click(object sender, EventArgs e)
         {
             try
             {
                 FrmHelp objHelp = new FrmHelp();
                 objHelp.strFormName = "Settlement process";
                 objHelp.ShowDialog();
                 objHelp = null;
             }
             catch { }
         }

         private void FrmSettlementEntry_FormClosing(object sender, FormClosingEventArgs e)
         {
             if (MbChangeStatus)
             {
                 // Checking the changes are not saved and shows warning to the user
                 MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 8, out MmessageIcon);
                 if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo,
                     MessageBoxIcon.Question) == DialogResult.Yes)
                     e.Cancel = false;
                 else
                     e.Cancel = true;
             }
         }

         private void txtSearch_TextChanged(object sender, EventArgs e)
         {
             mblnSearchStatus = false;
         }

         private void EmployeeSettlementDetailDataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
         {
             try
             {
                 if (EmployeeSettlementDetailDataGridView.IsCurrentCellDirty)
                     EmployeeSettlementDetailDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
             }
             catch { }
         }

         private void EmployeeSettlementDetailDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
         {
             try { } 
             catch { }
         }

        private void GetEmployeeLeaveHolidays()
        {
            DataTable datTemp = MobjclsBLLSettlementProcess.GetEmployeeLeaveHolidays(CboEmployee.SelectedValue.ToInt32(),
            DtpFromDate.Value.ToString("dd-MMM-yyyy"), DtpToDate.Value.ToString("dd-MMM-yyyy"));

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    if (datTemp.Rows[0]["Days"].ToDecimal() > 0) // Leave Days
                    {
                        if (datTemp.Rows[0]["Days"].ToString().Contains("."))
                        {
                            if ((datTemp.Rows[0]["Days"].ToString().Split('.').Last()).ToDecimal() > 0)
                                txtExcludeLeaveDays.Text = datTemp.Rows[0]["Days"].ToString();
                            else
                                txtExcludeLeaveDays.Text = datTemp.Rows[0]["Days"].ToString().Split('.').First();
                        }
                        else
                            txtExcludeLeaveDays.Text = datTemp.Rows[0]["Days"].ToString();
                    }
                    else
                    {
                        //if (ClsCommonSettings.IsArabicView)
                        //    txtExcludeLeaveDays.Text = "لا شيء";
                        //else
                            txtExcludeLeaveDays.Text = "NIL";
                    }

                    if (datTemp.Rows[1]["Days"].ToDecimal() > 0) // Holidays Days
                    {
                        if (datTemp.Rows[1]["Days"].ToString().Contains("."))
                        {
                            if ((datTemp.Rows[1]["Days"].ToString().Split('.').Last()).ToDecimal() > 0)
                                txtExcludeHolidays.Text = datTemp.Rows[1]["Days"].ToString();
                            else
                                txtExcludeHolidays.Text = datTemp.Rows[1]["Days"].ToString().Split('.').First();
                        }
                        else
                            txtExcludeHolidays.Text = datTemp.Rows[1]["Days"].ToString();
                    }
                    else
                    {
                        //if (ClsCommonSettings.IsArabicView)
                        //    txtExcludeHolidays.Text = "لا شيء";
                        //else
                            txtExcludeHolidays.Text = "NIL";                        
                    }

                    calculateEligibleDays();
                }
            }
        }

        private void chkExcludeLeaveDays_CheckedChanged(object sender, EventArgs e)
        {
            calculateEligibleDays();
        }

        private void chkExcludeHolidays_CheckedChanged(object sender, EventArgs e)
        {
            calculateEligibleDays();
        }

        private void calculateEligibleDays()
        {
            txtEligibleDays.Text = txtNofDaysExperience.Text;

            if (chkExcludeLeaveDays.Checked)
            {
                if (txtExcludeLeaveDays.Text.ToDecimal() > 0)
                    txtEligibleDays.Text = (txtEligibleDays.Text.ToDecimal() - txtExcludeLeaveDays.Text.ToDecimal()).ToString();
            }

            if (chkExcludeHolidays.Checked)
            {
                if (txtExcludeHolidays.Text.ToDecimal() > 0)
                    txtEligibleDays.Text = (txtEligibleDays.Text.ToDecimal() - txtExcludeHolidays.Text.ToDecimal()).ToString();
            }
        }

        private void BtnPrintRpt_Click(object sender, EventArgs e)
        {
            try
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = "Employee Settlement";
                ObjViewer.PiRecId = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID;
                ObjViewer.PiFormID = (int)FormID.SettlementProcess;
                ObjViewer.OperationTypeID = 1000; 
                ObjViewer.No = 0;

                if (datTempSalary != null)
                {
                    if (datTempSalary.Rows.Count > 0)
                    {
                        ObjViewer.dteFromDate = Convert.ToDateTime(datTempSalary.Rows[0]["PeriodFrom"]);
                        ObjViewer.dteToDate = Convert.ToDateTime(datTempSalary.Rows[0]["PeriodTo"]);
                        ObjViewer.No = 1;
                    }
                }

                ObjViewer.ShowDialog();
            }
            catch { }
        }
    }
}