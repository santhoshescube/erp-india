﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
/*****************************************************
    * Created By       : Sachin K S
    * Creation Date    : 12-08-2013
    * Description      : To Add Addtion and Deduction Particulars 
    * ***************************************************/
namespace MyBooksERP
{
    public partial class FrmAddParicularsToAll : DevComponents.DotNetBar.Office2007Form
    {
        #region Declarations

        public FrmMain objFrmMain;
        private string MstrMessageCommon;                      //  variable for assigning message

        clsBLLAddParicularsToAll MobjClsBLLAddParicularsToAll;
        ClsLogWriter MObjLogs;                                 //  Object for Class Clslogs
        ClsNotification mObjNotification;                      //  Object for Class ClsNotification
        clsBLLCommonUtility MobjclsBLLCommonUtility;           //  Object for Class clsBLLCommonUtility 

        private int MintTimerInterval;                         // To set timer interval

        //To set permissions
        //bool MblnPrintEmailPermission = false;
        //bool MblnAddPermission = false;
        //bool MblnUpdatePermission = false;
        //bool MblnDeletePermission = false;

        // Error Message display
        private ArrayList MaMessageArr;                 
        private ArrayList MaStatusMessage;
        private string MsMessageCaption;
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;

        private bool MblnChangeStatus = false; //To Specify Change Status
        int MintCompanyId;
        int MintUserId;
        public string strProcessDate;     //to get Salary Process Date
        public int intCompanyID;


        #endregion
        public FrmAddParicularsToAll()
        {
            //Constructor
            InitializeComponent();
            MobjClsBLLAddParicularsToAll = new clsBLLAddParicularsToAll();
            MObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MobjclsBLLCommonUtility = new clsBLLCommonUtility();
            MintCompanyId = ClsCommonSettings.LoginCompanyID; 
            MintUserId = ClsCommonSettings.UserID;
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MsMessageCaption = ClsCommonSettings.MessageCaption;
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        #region Events
        private void FrmAddParicularsToAll_Load(object sender, EventArgs e)
        {
            LoadMessage(); // Method for loading messages 
            LoadCombos(0); // Method for loading comboboxes 
            FillEmployeeDetails();
            txtPercentage.Enabled = false;
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    lblStatus.Text = "المبلغ في 'شركة العملات'";
            //    this.Text = "إضافة إضافات / الخصومات";
            //}
            //else
            //{
                lblStatus.Text = "Amount in 'Company Currency'";
                this.Text = "Add Additions/Deductions ";
            //}
        }

        private void rbtDeduction_CheckedChanged(object sender, EventArgs e)
        {
            LoadCombos(1);
            FillEmployeeDetails();
        }

        private void rbtAddition_CheckedChanged(object sender, EventArgs e)
        {
            LoadCombos(1);
            FillEmployeeDetails();
        }

        private void rbtnCarryforward_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnCarryforward.Checked)
            {
                txtPercentage.Enabled = true;
                cboAdditionsDeductions.SelectedValue = 12;
                cboAdditionsDeductions.Enabled = btnAddDed.Enabled = false;
            }
            else
            {
                txtPercentage.Text = "";
                cboAdditionsDeductions.Enabled = btnAddDed.Enabled = true;
                txtPercentage.Enabled = false;
            }
        }



        private void cboAdditionsDeductions_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillEmployeeDetails();
            ChangeStatus();
        }

        private void txtPercentage_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtPercentage_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                MObjLogs.WriteLog("Error on txtPercentage_KeyPress " + this.Name + ex.Message.ToString(), 1);

            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SaveEmployeeDetails();

        }

        private void dgvEmployeeDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            decimal decValue = 0;
            try
            {
                if (dgvEmployeeDetails.Rows.Count > 0)
                {
                    decValue = dgvEmployeeDetails[e.ColumnIndex, e.RowIndex].Value.ToDecimal();
                }
                if (decValue < 0)
                {
                    dgvEmployeeDetails[e.ColumnIndex, e.RowIndex].Value = 0;
                }
            }
            catch (Exception ex)
            {
                MObjLogs.WriteLog("Error on dgvEmployeeDetails_CellValueChanged " + this.Name + ex.Message.ToString(), 1);
            }

        }

        private void ToolStripMenuItemCopy_Click(object sender, EventArgs e)
        {
            try
            {
                bool MbCopyApplyFlag = true;
                int iRowIndexCur;
                if (MbCopyApplyFlag == true)
                {
                    dgvEmployeeDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    iRowIndexCur = dgvEmployeeDetails.CurrentRow.Index;
                    DataGridViewRow row = (DataGridViewRow)dgvEmployeeDetails.Rows[iRowIndexCur];
                    if (rbtnCarryforward.Checked = true && txtPercentage.Text.ToDecimal() > 0)
                    {
                        cboAdditionsDeductions.SelectedValue = 12;
                        for (int iRow = 0; iRow <= dgvEmployeeDetails.Rows.Count - 1; iRow++)
                        {
                            dgvEmployeeDetails.Rows[iRow].Cells[clmnAmount.Index].Value = dgvEmployeeDetails.Rows[iRow].Cells[clmnTotalAdditions.Index].Value.ToDecimal() * (txtPercentage.Text.ToDecimal() / 100);
                        }
                    }
                    else
                    {
                        for (int iRow = row.Index; iRow < dgvEmployeeDetails.Rows.Count; iRow++)
                        {
                            if (iRowIndexCur != iRow)
                            {
                                dgvEmployeeDetails.Rows[iRow].Cells[clmnAmount.Index].Value = dgvEmployeeDetails.Rows[iRowIndexCur].Cells[clmnAmount.Index].Value;
                            }
                        }
                    }
                    iRowIndexCur = -1;
                    dgvEmployeeDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    MbCopyApplyFlag = false;
                }
            }
            catch (Exception ex)
            {
                MObjLogs.WriteLog("Error on ToolStripMenuItemCopy_Click " + this.Name + ex.Message.ToString(), 1);
            }
        }

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillEmployeeDetails();
            ChangeStatus();
        }

        private void dgvEmployeeDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    if (e.ColumnIndex != clmnAmount.Index)
                    {
                        e.Cancel = true;
                        return;
                    }
                    else
                        ChangeStatus();
                }
            }
            catch (Exception ex)
            {
                MObjLogs.WriteLog("Error on dgvVacationDetails_CellBeginEdit " + this.Name + ex.Message.ToString(), 1);
            }
        }

        private void btnAddDed_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmAdditionDeduction(), true);
            LoadCombos(1);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            SaveEmployeeDetails();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvEmployeeDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            this.dgvEmployeeDetails.EditingControl.KeyPress += new KeyPressEventHandler(EditingControlConsequence_KeyPress);
        }

        void EditingControlConsequence_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvEmployeeDetails.CurrentCell.OwningColumn.Name == "clmnAmount")
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (!string.IsNullOrEmpty(dgvEmployeeDetails.EditingControl.Text) && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
        }

        private void dgvEmployeeDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvEmployeeDetails.IsCurrentCellDirty)
            {
                if (dgvEmployeeDetails.CurrentCell != null)
                    dgvEmployeeDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void FrmAddParicularsToAll_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:

                        break;
                    case Keys.Escape:
                        this.Close();
                        break;

                }
            }
            catch (Exception)
            {
            }
        }
        /// <summary>
        /// show confirmation message before closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmAddParicularsToAll_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MblnChangeStatus)
            {
                // Checking the changes are not saved and shows warning to the user
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        #endregion Events

        #region Methods
        #region LoadCombos
        /// <summary>
        /// Load the combos-AdditionDeductionReference,DepartmentReference
        /// </summary>
        /// <param name="intType">
        /// intType=0->Load all combos
        /// intType=1->Loads AdditionDeductionReference combo only
        /// intType=2->Loads DepartmentReference combo only
        /// </param>
        /// <returns>success/failure</returns>
        private void LoadCombos(int intType)
        {
            DataTable datCombos = new DataTable();

            if (intType == 0 || intType == 1)
            {
                int intValue = rbtAddition.Checked ? 1 : 0;

                if (intValue == 1)
                {
                    datCombos = MobjClsBLLAddParicularsToAll.FillCombos(new string[] { "AdditionDeductionID, AdditionDeduction", "PayAdditionDeductionReference", "IsPredefined=0  AND IsAddition = " + intValue + " OR AdditionDeductionID IN(21,24)" });
                }
                else
                    datCombos = MobjClsBLLAddParicularsToAll.FillCombos(new string[] { "AdditionDeductionID, AdditionDeduction", "PayAdditionDeductionReference", "IsPredefined=0  AND IsAddition = " + intValue + " OR AdditionDeductionID IN(8,12)" });

                cboAdditionsDeductions.ValueMember = "AdditionDeductionID";
                cboAdditionsDeductions.DisplayMember = "AdditionDeduction";
                cboAdditionsDeductions.DataSource = datCombos;
            }
            if (intType == 0 || intType == 2)
            {
                datCombos = MobjClsBLLAddParicularsToAll.FillCombos(new string[] { "DepartmentID,Department", "DepartmentReference", "" });
                cboDepartment.ValueMember = "DepartmentID";
                cboDepartment.DisplayMember = "Department";
                DataRow dtRow;
                dtRow = datCombos.NewRow();
                dtRow["DepartmentID"] = -1;
                dtRow["Department"] = "ALL";
                datCombos.Rows.InsertAt(dtRow, 0);
                cboDepartment.DataSource = datCombos;
            }
            MblnChangeStatus = false;
        }
        #endregion LoadCombos

        #region LoadMessage

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.AddParicularsToAll, this);
        //}


        /// <summary>
        /// Method to fill the message array according to form
        /// </summary>
        private void LoadMessage()
        {
            // Loading Message
            try
            {
                MaMessageArr = new ArrayList();
                MaStatusMessage = new ArrayList();
                MaMessageArr = mObjNotification.FillMessageArray((int)FormID.AddParicularsToAll, ClsCommonSettings.ProductID);
                MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.AddParicularsToAll, ClsCommonSettings.ProductID);
            }
            catch (Exception Ex)
            {
                MObjLogs.WriteLog("Error on dgvVacationDetails_CellBeginEdit " + this.Name + Ex.Message.ToString(), 1);
            }

        }
        #endregion LoadMessage

        #region FillEmployeeDetails
        /// <summary>
        /// Method to Fill Employee Payment Details
        /// </summary>
        private void FillEmployeeDetails()
        {
            DataTable dt = MobjClsBLLAddParicularsToAll.GetEmployeeDetails(cboAdditionsDeductions.SelectedValue.ToInt32(), strProcessDate,intCompanyID, cboDepartment.SelectedValue.ToInt32());
            dgvEmployeeDetails.Rows.Clear();
            dgvEmployeeDetails.DataSource = null;
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgvEmployeeDetails.RowCount = dgvEmployeeDetails.RowCount + 1;

                    dgvEmployeeDetails.Rows[i].Cells[clmnPaymentID.Index].Value = dt.Rows[i]["PaymentID"];
                    dgvEmployeeDetails.Rows[i].Cells[clmnCurrencyID.Index].Value = dt.Rows[i]["CurrencyID"];
                    dgvEmployeeDetails.Rows[i].Cells[clmnEmployeeName.Index].Value = dt.Rows[i]["EmployeeName"];
                    dgvEmployeeDetails.Rows[i].Cells[clmnTotalAdditions.Index].Value = dt.Rows[i]["TotalAdditions"];
                    dgvEmployeeDetails.Rows[i].Cells[clmnTotalDeduction.Index].Value = dt.Rows[i]["TotalDeduction"];
                    dgvEmployeeDetails.Rows[i].Cells[clmnNetAmount.Index].Value = dt.Rows[i]["TotalAdditions"].ToDecimal() - dt.Rows[i]["TotalDeduction"].ToDecimal();
                    dgvEmployeeDetails.Rows[i].Cells[clmnAmount.Index].Value = dt.Rows[i]["Amount"];

                }
            }
        }
        #endregion FillEmployeeDetails

        #region SaveEmployeeDetails
        /// <summary>
        /// Method to save Employee Payment Details
        /// </summary>
        private bool SaveEmployeeDetails()
        {
            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);
            if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                return false;

            if (SaveDetails())
            {
                FillEmployeeDetails();
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    lblStatus.Text = "حفظ بنجاح.";
                //}
                //else
                //{
                    lblStatus.Text = "Saved successfully.";
                //}

                return true;
            }
            return false;
        }
        #endregion SaveEmployeeDetails

        #region SaveDetails
        /// <summary>
        /// Method to save Employee Payment Details and Validations 
        /// </summary>
        private bool SaveDetails()
        {
            if (EmployeeDetailsvalidation())
            {
                FillDetails();
                if (MobjClsBLLAddParicularsToAll.SaveEmployeeDetails())
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
        #endregion SaveDetails

        #region FillDetails
        /// <summary>
        /// Method to Fill Employee Payment Details 
        /// </summary>
        private void FillDetails()
        {
            MobjClsBLLAddParicularsToAll.PobjClsDTOAddParicularsToAll.DTOAddParicularsToAllDetail = new System.Collections.Generic.List<clsDTOAddParicularsToAllDetail>();
            for (int i = 0; i < dgvEmployeeDetails.Rows.Count; i++)
            {
                clsDTOAddParicularsToAllDetail objclsDTOAddParicularsToAll = new clsDTOAddParicularsToAllDetail();
                objclsDTOAddParicularsToAll.PaymentID = dgvEmployeeDetails[clmnPaymentID.Index, i].Value.ToInt32();
                objclsDTOAddParicularsToAll.CurrencyID = dgvEmployeeDetails[clmnCurrencyID.Index, i].Value.ToInt32();
                objclsDTOAddParicularsToAll.Amount = Convert.ToDecimal(dgvEmployeeDetails[clmnAmount.Index, i].Value);

                int intValue = rbtAddition.Checked ? 1 : 0;
                objclsDTOAddParicularsToAll.IsAddition = intValue;
                objclsDTOAddParicularsToAll.AddDedID = cboAdditionsDeductions.SelectedValue.ToInt32();
                MobjClsBLLAddParicularsToAll.PobjClsDTOAddParicularsToAll.DTOAddParicularsToAllDetail.Add(objclsDTOAddParicularsToAll);
            }
        }
        #endregion FillDetails

        #region ChangeStatus
        /// <summary>
        /// Method to change buttons Enability
        /// </summary>
        private void ChangeStatus()
        {
            errAddParicularsToAll.Clear();
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    lblStatus.Text = "المبلغ في 'شركة العملات'";
            //}
            //else
            //{
                lblStatus.Text = "Amount in 'Company Currency'";
            //}
            MblnChangeStatus = true;
            btnOk.Enabled = true;
            btnSave.Enabled = true;
        }
        #endregion ChangeStatus

        #region EmployeeDetailsvalidation
        /// <summary>
        /// Method to do Employee Details validation
        /// </summary>
        private bool EmployeeDetailsvalidation()
        {    
            try
            {
                errAddParicularsToAll.Clear();
                lblStatus.Text = "";
                if (cboAdditionsDeductions.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9991, out MmessageIcon);
                    errAddParicularsToAll.SetError(cboAdditionsDeductions, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrClear.Enabled = true;
                    cboAdditionsDeductions.Focus();
                    return false;
                }
                else if (cboDepartment.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 17, out MmessageIcon);
                    errAddParicularsToAll.SetError(cboDepartment, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrClear.Enabled = true;
                    cboDepartment.Focus();
                    return false;
                }
                else if (dgvEmployeeDetails.Rows.Count == 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9992, out MmessageIcon);
                    errAddParicularsToAll.SetError(dgvEmployeeDetails, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrClear.Enabled = true;
                    dgvEmployeeDetails.Focus();
                    return false;
                }
                else
                    return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in EmployeeDetailsvalidation() " + ex.Message);
                MObjLogs.WriteLog("Error in EmployeeDetailsvalidation() " + ex.Message, 2);
                return false;
            }

        }
        #endregion EmployeeDetailsvalidation
        #endregion Methods
    }
}
