﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using Microsoft.Reporting.WinForms;
namespace MyBooksERP
{
    /*****************************************************
     * Created By    : Arun OK
     * Creation Date : 02 May 2013
     * Description   : Handles Employee Leave Structure
     * FormID        : 151
     *****************************************************/


    public partial class FrmEmployeeLeaveStructure : Form
    {
        #region Declaration

        bool blnDisplay = false;

        ArrayList MaMessageArr;
        public bool PblnShowReport = false;
        private string MstrMessageCaption;              //Message caption
        private string MstrMessageCommon;               //to set the message
        private MessageBoxIcon MmessageIcon;            // to set the message icon
        private ArrayList MaStatusMessageArr;
        public long pintEmployeeID;
        public int pintCompanyID;
        private string pstrCurrentFinYear = string.Empty;

        //------------permission-------------------'
        bool blnAddPermission, blnUpdatePermission, blnDeletePermission, blnAddUpdatePermission, blnPrintEmailPermission = true;
        //-------------------------------------------

        ClsLogWriter objLogs;
        ClsNotification objNotification;
        ClsCommonUtility objCommonUtility;
        clsBLLEmployeeLeaveStructure ObjBLLEmployeeLeaveStructure;
        clsDTOEmployeeLeaveStructure objDTOEmployeeLeaveStructure;
        List<clsDTOEmployeeLeaveStructure> lstEmployeeLeaveStructure;

        #endregion

        #region Methods

        /// <summary>
        /// Load error messages
        /// </summary>
        private void LoadMessage()
        {
            objNotification = new ClsNotification();
            MaMessageArr = new ArrayList();
            MaStatusMessageArr = new ArrayList();
            MaMessageArr = objNotification.FillMessageArray((int)FormID.EmployeeLeaveStructure, 2);
            MaStatusMessageArr = objNotification.FillStatusMessageArray((int)FormID.EmployeeLeaveStructure, 2);
        }
        
        /// <summary>
        /// Load combos
        /// </summary>
        /// <param name="intMode"></param>
        private void loadCombos(int intMode)
        {
            DataTable dtDataSource = null;
            if (intMode == 0 || intMode == 1)
            {
                dtDataSource = objCommonUtility.FillCombos(" SELECT DISTINCT LS.CompanyID, CM.CompanyName FROM PayEmployeeLeaveSummary LS INNER JOIN CompanyMAster CM ON LS.CompanyId=CM.CompanyId Where CM.CompanyID In(select CompanyID from UserCompanyDetails where UserID =" + ClsCommonSettings.UserID + " AND  CompanyID =" + ClsCommonSettings.LoginCompanyID + ")");
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                if (dtDataSource.Rows.Count > 0)
                    cboCompany.DataSource = dtDataSource;

                if (pintCompanyID > 0)
                {
                    cboCompany.SelectedValue = pintCompanyID;
                }
                cboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;
            }
            if (intMode == 0 || intMode == 2)
            {
                if (cboCompany.SelectedIndex != -1)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    dtDataSource = objCommonUtility.FillCombos("SELECT E.EmployeeID, ISNULL(E.EmployeeFullNameArb, '') + ' - ' + cast(E.EmployeeNumber as varchar(30)) AS FirstNAme FROM  EmployeeMaster AS E WHERE isnull(E.LeavePolicyID,0)<>0 AND WorkStatusID >5 AND E.CompanyID= " + (cboCompany.SelectedValue) + " ORDER BY E.FirstName");
                    //else
                        dtDataSource = objCommonUtility.FillCombos("SELECT E.EmployeeID, ISNULL(E.EmployeeFullName, '') + ' - ' + cast(E.EmployeeNumber as varchar(30)) AS FirstNAme FROM  EmployeeMaster AS E WHERE isnull(E.LeavePolicyID,0)<>0 AND WorkStatusID >5 AND E.CompanyID= " + (cboCompany.SelectedValue) + " ORDER BY E.FirstName");
                    cboEmployee.ValueMember = "EmployeeID";
                    cboEmployee.DisplayMember = "FirstNAme";
                    if (dtDataSource.Rows.Count > 0)
                    {
                        cboEmployee.DataSource = dtDataSource;
                        if (pintEmployeeID > 0)
                        {
                            cboEmployee.SelectedValue = pintEmployeeID;
                        }
                        else
                        {
                            cboEmployee.SelectedIndex = -1;
                        }
                    }
                }
            }
            if (intMode == 3)
            {
                if (cboEmployee.SelectedIndex != -1)
                {
                    dtDataSource = objCommonUtility.FillCombos("exec dbo.spPayEmployeeLeaveSummary @CompanyID=" + (cboCompany.SelectedValue) + " ,@EmployeeID=" + (cboEmployee.SelectedValue) + ",@type=10");
                    cboPeriod.DisplayMember = "FinYearPeriod";
                    cboPeriod.ValueMember = "FinYearStartDate";

                    if (dtDataSource.Rows.Count > 0)
                    {
                        cboPeriod.DataSource = dtDataSource;
                        GetCurrentFinYear(dtDataSource);
                        if (pstrCurrentFinYear != "")
                        {
                            cboPeriod.SelectedValue = pstrCurrentFinYear;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get current financial year
        /// </summary>
        /// <param name="dtDataSource"></param>
        private void GetCurrentFinYear(DataTable dtDataSource)
        {
            pstrCurrentFinYear = "";
            for (int i = dtDataSource.Rows.Count - 1; i >= 0; i--)
            {
                if (Convert.ToString(dtDataSource.Rows[i]["FinYearStartDate"]).Contains(DateTime.Now.Year.ToString()))
                {
                    pstrCurrentFinYear = Convert.ToString(dtDataSource.Rows[i]["FinYearStartDate"]);
                    break;
                }
            }
        }

        /// <summary>
        /// Set permissions
        /// </summary>
        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.LeaveStructure, out blnPrintEmailPermission, out blnAddPermission, out blnUpdatePermission, out blnDeletePermission);

            }    
            else
                blnAddPermission = blnPrintEmailPermission = blnUpdatePermission = blnDeletePermission = true;

            if (blnAddPermission || blnUpdatePermission)
            {
                blnAddUpdatePermission = true;
            }
            else
            {
                blnAddUpdatePermission = false;
            }

            this.GrpMain.Enabled = true;
            BtnPrint.Enabled = btnEmail.Enabled = blnPrintEmailPermission;
        }

        /// <summary>
        /// Clear all controls
        /// </summary>
        private void ClearAllControls()
        {
            DgvEmployeeLeaveStructure.Rows.Clear();
            cboCompany.SelectedIndex = -1;
            cboEmployee.SelectedIndex = -1;
            if (pstrCurrentFinYear != "")
                cboPeriod.SelectedValue = pstrCurrentFinYear;
            else
                cboPeriod.SelectedIndex = -1;
            pintCompanyID = 0;
            pintEmployeeID = 0;
        }

        #region ValidateLeaveStructure
        /// <summary>
        /// validation before saving
        /// </summary>
        /// <returns></returns>
        private bool ValidateLeaveStructure()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                MstrMessageCommon = objNotification.GetErrorMessage(MaMessageArr, 10100, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (cboEmployee.SelectedIndex == -1)
            {
                MstrMessageCommon = objNotification.GetErrorMessage(MaMessageArr, 10101, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (cboPeriod.SelectedIndex == -1)
            {
                MstrMessageCommon = objNotification.GetErrorMessage(MaMessageArr, 10102, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            for (int i = 0; i <= DgvEmployeeLeaveStructure.Rows.Count - 1; i++)
            {
                if (!DgvEmployeeLeaveStructure.Rows[i].Cells["Balance_Leaves"].Value.IsDecimal() || !DgvEmployeeLeaveStructure.Rows[i].Cells["CarryForward_Days"].Value.IsDecimal() || !DgvEmployeeLeaveStructure.Rows[i].Cells["Extended_Leaves"].Value.IsDecimal() || !DgvEmployeeLeaveStructure.Rows[i].Cells["Taken_Leaves"].Value.IsDecimal())
                {
                    MstrMessageCommon = objNotification.GetErrorMessage(MaMessageArr, 10103, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                //if (DgvEmployeeLeaveStructure.Rows[i].Cells["Balance_Leaves"].Value.ToDecimal() < DgvEmployeeLeaveStructure.Rows[i].Cells["Month_Leave"].Value.ToDecimal())
                //{
                //    MstrMessageCommon = objNotification.GetErrorMessage(MaMessageArr, 613, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    return false;
                //}
            }
            return true;
        }
        #endregion ValidateLeaveStructure

        #region CalculateCarryForwardDays
        /// <summary>
        /// Internally delete inserts and recalculates entries in leave summary table
        /// </summary>
        /// <returns></returns>
        private bool CalculateCarryForwardDays()
        {
            string[] str = null;
            if ((cboCompany.SelectedIndex != -1 || cboEmployee.SelectedIndex != -1 || cboPeriod.SelectedIndex != -1) && ((cboCompany.SelectedValue.ToInt32() != 0 || cboEmployee.SelectedValue.ToInt32() != 0)))
            {
                if (cboPeriod.Text.Trim().Length > 0)
                    str = cboPeriod.Text.Split(Convert.ToChar("-"));

                int intEmployeeID = objDTOEmployeeLeaveStructure.intEmployeeID = cboEmployee.SelectedValue.ToInt32();
                int intCompanyID = cboCompany.SelectedValue.ToInt32();
                if (str != null && intEmployeeID > 0 && intCompanyID > 0)
                {
                    if (ObjBLLEmployeeLeaveStructure.LeaveSummaryFromLeaveEntry(str, intEmployeeID, intCompanyID))
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            else
                return false;
        }
        #endregion CalculateCarryForwardDays


        #region Display
        /// <summary>
        /// Displays Leave structure
        /// </summary>
        private void Display()
        {
            try
            {
                blnDisplay = false;

                if ((cboCompany.SelectedIndex != -1) && (cboEmployee.SelectedIndex != -1) && (cboPeriod.SelectedIndex != -1))
                    if ((cboCompany.SelectedValue.ToInt32() != 0) && (cboEmployee.SelectedValue.ToInt32() != 0))
                    {
                        string[] str = null;
                        string fromDate;
                        if (cboPeriod.Text.Trim().Length > 0)
                            str = cboPeriod.Text.Split(Convert.ToChar("-"));
                        if (str != null)
                        {
                            fromDate = str[0].ToString();
                            int intEmployeeID = cboEmployee.SelectedValue.ToInt32();
                            int intCompanyID = cboCompany.SelectedValue.ToInt32();
                            int intLeavePolicyID = 0;


                            DataTable dtDisplayData = ObjBLLEmployeeLeaveStructure.GetDisplayData(fromDate, intEmployeeID, intCompanyID, out intLeavePolicyID);
                            if (dtDisplayData.Rows.Count > 0)
                            {
                                decimal decTakenLeaves = 0;
                                DgvEmployeeLeaveStructure.Rows.Clear();
                                for (int iCounter = 0; iCounter <= dtDisplayData.Rows.Count - 1; iCounter++)
                                {
                                    DgvEmployeeLeaveStructure.RowCount = DgvEmployeeLeaveStructure.RowCount + 1;
                                    DgvEmployeeLeaveStructure.Rows[iCounter].Cells["LeavePolicy_ID"].Value = (dtDisplayData.Rows[iCounter]["LeavePolicyID"]).ToInt32();
                                    DgvEmployeeLeaveStructure.Rows[iCounter].Cells["Leave_Type"].Value = (dtDisplayData.Rows[iCounter]["LeaveType"]).ToString();
                                    DgvEmployeeLeaveStructure.Rows[iCounter].Cells["leaveType_ID"].Value = (dtDisplayData.Rows[iCounter]["leaveTypeID"]).ToInt32();
                                    if (dtDisplayData.Rows[iCounter]["BalanceLeaves"].ToDecimal() <= 0)
                                        DgvEmployeeLeaveStructure.Rows[iCounter].Cells["Balance_Leaves"].Value = "0";
                                    else
                                        DgvEmployeeLeaveStructure.Rows[iCounter].Cells["Balance_Leaves"].Value = (dtDisplayData.Rows[iCounter]["BalanceLeaves"]).ToDecimal().ToString("0.0");
                                    if (dtDisplayData.Rows[iCounter]["CarryForwardDays"].ToDecimal() <= 0)
                                        DgvEmployeeLeaveStructure.Rows[iCounter].Cells["CarryForward_Days"].Value = "0";
                                    else
                                        DgvEmployeeLeaveStructure.Rows[iCounter].Cells["CarryForward_Days"].Value = (dtDisplayData.Rows[iCounter]["CarryForwardDays"]).ToDecimal().ToString("0.0");
                                    if (dtDisplayData.Rows[iCounter]["TakenLeaves"].ToDecimal() <= 0)
                                        DgvEmployeeLeaveStructure.Rows[iCounter].Cells["Taken_Leaves"].Value = "0";
                                    else
                                        DgvEmployeeLeaveStructure.Rows[iCounter].Cells["Taken_Leaves"].Value = (dtDisplayData.Rows[iCounter]["TakenLeaves"]).ToDecimal().ToString("0.0");
                                    if (dtDisplayData.Rows[iCounter]["Exceeded"].ToDecimal() <= 0)
                                        DgvEmployeeLeaveStructure.Rows[iCounter].Cells["Extended_Leaves"].Value = "0";
                                    else
                                        DgvEmployeeLeaveStructure.Rows[iCounter].Cells["Extended_Leaves"].Value = (dtDisplayData.Rows[iCounter]["Exceeded"]).ToDecimal().ToString("0.0");
                                    decTakenLeaves = ((dtDisplayData.Rows[iCounter]["BalanceLeaves"]).ToDecimal() + (dtDisplayData.Rows[iCounter]["CarryForwardDays"]).ToDecimal() + (dtDisplayData.Rows[iCounter]["Exceeded"]).ToDecimal()) - (dtDisplayData.Rows[iCounter]["TakenLeaves"]).ToDecimal();
                                    if (decTakenLeaves <= 0)
                                    {
                                        DgvEmployeeLeaveStructure.Rows[iCounter].Cells["Balance_Leave"].Value = "0";
                                    }
                                    else
                                        DgvEmployeeLeaveStructure.Rows[iCounter].Cells["Balance_Leave"].Value = decTakenLeaves.ToString("0.0");
                                    if (dtDisplayData.Rows[iCounter]["MonthLeaves"].ToDecimal() <= 0)
                                        DgvEmployeeLeaveStructure.Rows[iCounter].Cells["Month_Leave"].Value = "0";
                                    else
                                        DgvEmployeeLeaveStructure.Rows[iCounter].Cells["Month_Leave"].Value = (dtDisplayData.Rows[iCounter]["MonthLeaves"]).ToDecimal().ToString("0.0");
                                    if (dtDisplayData.Rows[iCounter]["EncashLeaves"].ToDecimal() <= 0)
                                        DgvEmployeeLeaveStructure.Rows[iCounter].Cells["Encash_Leaves"].Value = "0";
                                    else
                                        DgvEmployeeLeaveStructure.Rows[iCounter].Cells["Encash_Leaves"].Value = (dtDisplayData.Rows[iCounter]["EncashLeaves"]).ToDecimal().ToString("0.0");
                                    if ((DgvEmployeeLeaveStructure.Rows[iCounter].Cells["LeavePolicy_ID"].Value).ToInt32() != intLeavePolicyID)
                                    {
                                        DgvEmployeeLeaveStructure.Rows[iCounter].ReadOnly = true;
                                        DgvEmployeeLeaveStructure.Rows[iCounter].DefaultCellStyle.BackColor = SystemColors.ControlLight;
                                    }
                                    blnDisplay = true;
                                    SetPermissions();
                                }
                                BtnPrint.Enabled = btnEmail.Enabled = true;
                            }
                            else
                            {
                                DgvEmployeeLeaveStructure.Rows.Clear();
                                BtnPrint.Enabled = btnEmail.Enabled = false;
                            }
                        }
                    }
                BtnSave.Enabled = LeaveStructureBindingNavigatorSaveItem.Enabled = OKSaveButton.Enabled = false;
            }
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1,Log.LogSeverity.Error);

            }
        }
        #endregion Display

        #region SaveLeaveStructure
        /// <summary>
        /// Saves Leave Structure
        /// </summary>
        /// <returns></returns>
        private bool SaveLeaveStructure()
        {
            try
            {
                if (ValidateLeaveStructure())
                {
                    int intJCounter = 0;
                    for (intJCounter = 0; intJCounter <= DgvEmployeeLeaveStructure.Rows.Count - 1; intJCounter++)
                    {
                        if (((Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows[intJCounter].Cells["Balance_Leaves"].Value) + Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows[intJCounter].Cells["CarryForward_Days"].Value) + Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows[intJCounter].Cells["Extended_Leaves"].Value)) - Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows[intJCounter].Cells["Taken_Leaves"].Value)) < 0)
                        {
                            MstrMessageCommon = new ClsNotification().GetErrorMessage(MaMessageArr, 3165, out MmessageIcon);
                            lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrLeavePolicy.Enabled = true;
                            return false;
                        }
                    }

                    int intICounter = 0;
                    string[] str;

                    if (cboPeriod.Text.Trim().Length > 0)
                        str = cboPeriod.Text.Split(Convert.ToChar("-"));
                    else
                        return false;
                    lstEmployeeLeaveStructure = new List<clsDTOEmployeeLeaveStructure>();
                    for (intICounter = 0; intICounter <= DgvEmployeeLeaveStructure.Rows.Count - 1; intICounter++)
                    {
                        objDTOEmployeeLeaveStructure = new clsDTOEmployeeLeaveStructure();
                        objDTOEmployeeLeaveStructure.intEmployeeID = Convert.ToInt32(cboEmployee.SelectedValue);
                        objDTOEmployeeLeaveStructure.strFinYearStartDate = str[0];
                        objDTOEmployeeLeaveStructure.intLeaveTypeID = Convert.ToInt32(DgvEmployeeLeaveStructure.Rows[intICounter].Cells["leaveType_ID"].Value);
                        objDTOEmployeeLeaveStructure.BalanceLeaves = Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows[intICounter].Cells["Balance_Leaves"].Value);
                        objDTOEmployeeLeaveStructure.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                        objDTOEmployeeLeaveStructure.intLeavePolicyID = Convert.ToInt32(DgvEmployeeLeaveStructure.Rows[intICounter].Cells["LeavePolicy_ID"].Value);
                        objDTOEmployeeLeaveStructure.CarryForwardDays = Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows[intICounter].Cells["CarryForward_Days"].Value);
                        objDTOEmployeeLeaveStructure.TakenLeaves = Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows[intICounter].Cells["Taken_Leaves"].Value);
                        objDTOEmployeeLeaveStructure.Exceeded = Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows[intICounter].Cells["Extended_Leaves"].Value);
                        objDTOEmployeeLeaveStructure.MonthLeaves = Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows[intICounter].Cells["Month_Leave"].Value);
                        objDTOEmployeeLeaveStructure.EncashLeaves = Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows[intICounter].Cells["Encash_Leaves"].Value);

                        lstEmployeeLeaveStructure.Add(objDTOEmployeeLeaveStructure);
                    }

                    if (ObjBLLEmployeeLeaveStructure.SaveLeaveStructure(lstEmployeeLeaveStructure))
                    {
                        BtnSave.Enabled = LeaveStructureBindingNavigatorSaveItem.Enabled = OKSaveButton.Enabled = false;
                        if (CalculateCarryForwardDays())
                        {
                            Display();
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1,Log.LogSeverity.Error);
                return false;
            }
        }
        #endregion SaveLeaveStructure

        #region ChangeStatus
        private void ChangeStatus()
        {
            BtnSave.Enabled = LeaveStructureBindingNavigatorSaveItem.Enabled = OKSaveButton.Enabled = blnAddUpdatePermission;
        }
        #endregion ChangeStatus

        #endregion

        #region Events

        public FrmEmployeeLeaveStructure()
        {
            InitializeComponent();

            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            objLogs = new ClsLogWriter(Application.StartupPath);
            objNotification = new ClsNotification();
            objCommonUtility = new ClsCommonUtility();
            ObjBLLEmployeeLeaveStructure = new clsBLLEmployeeLeaveStructure();
            objDTOEmployeeLeaveStructure = new clsDTOEmployeeLeaveStructure();
            //lstEmployeeLeaveStructure = new List<clsDTOEmployeeLeaveStructure>();
            MmessageIcon = MessageBoxIcon.Information;
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.EmployeeLeaveStructure, this);

        //    DgvEmployeeLeaveStructure.Columns["Leave_Type"].HeaderText = "ترك نوع";
        //    DgvEmployeeLeaveStructure.Columns["Balance_Leaves"].HeaderText = "أيام";
        //    DgvEmployeeLeaveStructure.Columns["CarryForward_Days"].HeaderText = "حمل إلى الأمام";
        //    DgvEmployeeLeaveStructure.Columns["Taken_Leaves"].HeaderText = "أوراق تؤخذ";
        //    DgvEmployeeLeaveStructure.Columns["Extended_Leaves"].HeaderText = "تمديد أيام";
        //    DgvEmployeeLeaveStructure.Columns["Balance_Leave"].HeaderText = "المتبقي من الاجازة";
        //    DgvEmployeeLeaveStructure.Columns["Month_Leave"].HeaderText = "شهريا تؤخذ";
        //    DgvEmployeeLeaveStructure.Columns["Encash_Leaves"].HeaderText = "يحققوا تؤخذ";
        //}

        private void LeaveStructureBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            //if (DgvEmployeeLeaveStructure.CurrentRow.Cells["Balance_Leaves"].Value.IsDecimal() && DgvEmployeeLeaveStructure.CurrentRow.Cells["CarryForward_Days"].Value.IsDecimal() && DgvEmployeeLeaveStructure.CurrentRow.Cells["Extended_Leaves"].Value.IsDecimal() && DgvEmployeeLeaveStructure.CurrentRow.Cells["Taken_Leaves"].Value.IsDecimal())
            if (SaveLeaveStructure())
            {
                MstrMessageCommon = new ClsNotification().GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrLeavePolicy.Enabled = true;
                OKSaveButton.Enabled = false;
                BtnSave.Enabled = false;
            }
        }

        private void FrmEmployeeLeaveStructure_Load(object sender, EventArgs e)
        {
            loadCombos(0);
            if (pintCompanyID > 0)
            {
                cboCompany.SelectedValue = pintCompanyID;
            }
            else
            {
                cboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;
            }

            if (pintEmployeeID > 0)
            {
                cboEmployee.SelectedValue = pintEmployeeID;
            }

            cboCompany.Enabled = true;
            CalculateCarryForwardDays();
            Display();
            LoadMessage();
            SetPermissions();
            blnDisplay = false;
            TmrLeavePolicy.Interval = ClsCommonSettings.TimerInterval;
            TmrLeavePolicy.Enabled = false;
            if (DgvEmployeeLeaveStructure.Rows.Count > 0)
                BtnPrint.Enabled = btnEmail.Enabled = true;
            else
                BtnPrint.Enabled = btnEmail.Enabled = false;

            BtnSave.Enabled = OKSaveButton.Enabled = LeaveStructureBindingNavigatorSaveItem.Enabled = false;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedIndex != -1)
                loadCombos(2);
            else
            {
                DgvEmployeeLeaveStructure.Rows.Clear();
                DgvEmployeeLeaveStructure.RowCount = 1;
            }

            DgvEmployeeLeaveStructure.Rows.Clear();
            cboEmployee.SelectedIndex = -1;
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (cboCompany.SelectedIndex == -1)
            {
                return; 
            }

            if (cboEmployee.SelectedIndex != -1)
                loadCombos(3);

            int intStatus = 0;
            objDTOEmployeeLeaveStructure.intEmployeeID = cboEmployee.SelectedValue.ToInt32();
            DataTable dt = ObjBLLEmployeeLeaveStructure.GetEmployeeWorkStatusID(cboEmployee.SelectedValue.ToInt32());
            if (dt.Rows.Count > 0)
            {
                intStatus = Convert.ToInt32(dt.Rows[0]["WorkStatus"]);
            }

            if (intStatus == 1 || intStatus == 2 || intStatus == 3 || intStatus == 4 || intStatus == 5 || intStatus == 9)
                DgvEmployeeLeaveStructure.Enabled = false;
            else
                DgvEmployeeLeaveStructure.Enabled = true;

            DgvEmployeeLeaveStructure.Rows.Clear();


            CalculateCarryForwardDays();
            Display();
            //cboPeriod.SelectedIndex = -1;
        }

        private void cboCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboCompany.SelectedIndex == -1)
            {
                DgvEmployeeLeaveStructure.Rows.Clear();
                DgvEmployeeLeaveStructure.RowCount = 1;
            }
        }

        private void cboCompany_Leave(object sender, EventArgs e)
        {
            if (cboCompany.Text.Trim() == "")
            {
                cboCompany.SelectedIndex = -1;
                DgvEmployeeLeaveStructure.Rows.Clear();
                DgvEmployeeLeaveStructure.RowCount = 1;
            }
        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void cboEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboEmployee.DroppedDown = false;
        }

        private void cboPeriod_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboPeriod.DroppedDown = false;
        }

        private void cboPeriod_Leave(object sender, EventArgs e)
        {
            if (cboPeriod.Text.Trim() == "")
            {
                cboPeriod.SelectedIndex = -1;
                DgvEmployeeLeaveStructure.Rows.Clear();
                DgvEmployeeLeaveStructure.RowCount = 1;
            }
        }

        private void cboEmployee_Leave(object sender, EventArgs e)
        {
            if (cboEmployee.Text.Trim() == "")
            {
                cboEmployee.SelectedIndex = -1;
                DgvEmployeeLeaveStructure.Rows.Clear();
                DgvEmployeeLeaveStructure.RowCount = 1;
            }
        }

        private void CancelToolStripButton_Click(object sender, EventArgs e)
        {
            ClearAllControls();
        }

        private void DgvEmployeeLeaveStructure_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.RowIndex >= 0)
            //{
            //    if (DgvEmployeeLeaveStructure.Rows[e.RowIndex].Cells["LeaveType_ID"].Value != null)
            //        if (DgvEmployeeLeaveStructure.Rows[e.RowIndex].Cells["LeaveType_ID"].Value.ToString() == "1")
            //            DgvEmployeeLeaveStructure.Rows[e.RowIndex].Cells["Month_Leave"].ReadOnly = true;
            //    else
            //            DgvEmployeeLeaveStructure.Rows[e.RowIndex].Cells["Month_Leave"].ReadOnly = false;
            //}
        }

        private void DgvEmployeeLeaveStructure_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (DgvEmployeeLeaveStructure.IsCurrentCellDirty)
                if (DgvEmployeeLeaveStructure.CurrentCell != null)
                    DgvEmployeeLeaveStructure.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void DgvEmployeeLeaveStructure_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void DgvEmployeeLeaveStructure_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            DgvEmployeeLeaveStructure.CommitEdit(DataGridViewDataErrorContexts.Commit);
            if (DgvEmployeeLeaveStructure.CurrentRow.Index != 0)
            {
                if (DgvEmployeeLeaveStructure.CurrentRow.Cells["Balance_Leaves"].Value.IsNull() && DgvEmployeeLeaveStructure.CurrentRow.Cells["CarryForward_Days"].Value.IsNull() && DgvEmployeeLeaveStructure.CurrentRow.Cells["Extended_Leaves"].Value.IsNull() && DgvEmployeeLeaveStructure.CurrentRow.Cells["Taken_Leaves"].Value.IsNull())
                    if (DgvEmployeeLeaveStructure.CurrentRow.Cells["Balance_Leaves"].Value.IsDecimal() && DgvEmployeeLeaveStructure.CurrentRow.Cells["CarryForward_Days"].Value.IsDecimal() && DgvEmployeeLeaveStructure.CurrentRow.Cells["Extended_Leaves"].Value.IsDecimal() && DgvEmployeeLeaveStructure.CurrentRow.Cells["Taken_Leaves"].Value.IsDecimal())
                    {
                        decimal decTakenLeaves = 0;
                        decTakenLeaves = (Convert.ToDecimal(DgvEmployeeLeaveStructure.CurrentRow.Cells["Balance_Leaves"].Value) + Convert.ToDecimal(DgvEmployeeLeaveStructure.CurrentRow.Cells["CarryForward_Days"].Value) + Convert.ToDecimal(DgvEmployeeLeaveStructure.CurrentRow.Cells["Extended_Leaves"].Value)) - Convert.ToDecimal(DgvEmployeeLeaveStructure.CurrentRow.Cells["Taken_Leaves"].Value);

                        if (decTakenLeaves < 0 && blnDisplay)
                        {
                            e.Cancel = true;
                            TmrLeavePolicy.Enabled = true;
                            MstrMessageCommon = new ClsNotification().GetErrorMessage(MaMessageArr, 851, out MmessageIcon);
                            lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            DgvEmployeeLeaveStructure.CommitEdit(DataGridViewDataErrorContexts.Commit);
                            e.Cancel = false;
                        }
                        DgvEmployeeLeaveStructure.CurrentRow.Cells["Balance_Leave"].Value = decTakenLeaves;
                    }

            }
        }

        private void cboPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (blnComboFill == false)
            //{
            cboPeriod.DroppedDown = false;
            CalculateCarryForwardDays();
            Display();
            // }
        }



        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = "Leave Structure";
                if (cboEmployee.SelectedIndex != -1)
                    ObjViewer.PintExecutive = cboEmployee.SelectedValue.ToInt32();
                if (cboCompany.SelectedIndex != -1)
                    ObjViewer.PintCompany = cboCompany.SelectedValue.ToInt32();
                if (cboPeriod.Text != "")
                    ObjViewer.PsPeriod = cboPeriod.Text;
                ObjViewer.PsExecutive = cboEmployee.Text;
                ObjViewer.PiFormID = (int)FormID.EmployeeLeaveStructure;
                ObjViewer.ShowDialog();

            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void CancelCButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OKSaveButton_Click(object sender, EventArgs e)
        {
            if (SaveLeaveStructure())
            {
                MstrMessageCommon = new ClsNotification().GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrLeavePolicy.Enabled = true;
                OKSaveButton.Enabled = false;
                BtnSave.Enabled = false;
                this.Close();
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            LeaveStructureBindingNavigatorSaveItem_Click(sender, e);
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "LeaveStructure";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void DgvEmployeeLeaveStructure_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 3 && e.RowIndex >= 0)
            {
                if (!DgvEmployeeLeaveStructure.CurrentCell.Value.IsNull())
                {
                    if (!DgvEmployeeLeaveStructure.CurrentCell.Value.IsDecimal())
                        DgvEmployeeLeaveStructure.CurrentCell.Value = 0;
                }
                else
                    DgvEmployeeLeaveStructure.CurrentCell.Value = 0;
            }
            ChangeStatus();
        }

        #endregion

        private void btnEmail_Click(object sender, EventArgs e)
        {
            if (cboCompany.Text == "" || cboPeriod.Text == "" || cboEmployee.Text == "")
                return;
            string[] str = null;
            string fromDate;
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    if (cboPeriod.Text.Trim().Length > 0)
                        str = cboPeriod.Text.Split(Convert.ToChar("-"));
                    if (str != null)
                    {
                        fromDate = str[0].ToString();
                        ObjEmailPopUp.MsSubject = "Leave Structure";
                        ObjEmailPopUp.EmailFormType = EmailFormID.LeaveStructure;
                        ObjEmailPopUp.EmailSource = ObjBLLEmployeeLeaveStructure.GetLeaveStructureEmail(cboEmployee.SelectedValue.ToInt32(), cboCompany.SelectedValue.ToInt32(), fromDate, cboPeriod.Text);
                        ObjEmailPopUp.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }
        /// <summary>
        /// Shortcut keys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmLeaveStructure_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.P:
                        BtnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        btnEmail_Click(sender, new EventArgs());//Email
                        break;
                    case Keys.F1:
                        BtnHelp_Click(sender, new EventArgs());//help
                        break;
                }
            }
            catch
            {
            }
        }

        private void DgvEmployeeLeaveStructure_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (DgvEmployeeLeaveStructure.CurrentCell != null)
                {
                    if (DgvEmployeeLeaveStructure.CurrentCell.ColumnIndex == Balance_Leaves.Index || DgvEmployeeLeaveStructure.CurrentCell.ColumnIndex == CarryForward_Days.Index ||
                        DgvEmployeeLeaveStructure.CurrentCell.ColumnIndex == Taken_Leaves.Index || DgvEmployeeLeaveStructure.CurrentCell.ColumnIndex == Extended_Leaves.Index ||
                        DgvEmployeeLeaveStructure.CurrentCell.ColumnIndex == Month_Leave.Index || DgvEmployeeLeaveStructure.CurrentCell.ColumnIndex == Encash_Leaves.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress -= new KeyPressEventHandler(txtDecimal_KeyPress);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        void txtDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        }

        private void FrmEmployeeLeaveStructure_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                MstrMessageCommon = objNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);
                // MessageBox.Show(MstrMessageCommon.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

    }
}
