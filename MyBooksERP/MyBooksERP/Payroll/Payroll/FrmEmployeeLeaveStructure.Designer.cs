﻿namespace MyBooksERP
{
    partial class FrmEmployeeLeaveStructure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label EmployeeName;
            System.Windows.Forms.Label lblCompanyName;
            System.Windows.Forms.Label lblPeriod;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmployeeLeaveStructure));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PolicyMasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.LeaveStructureBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.TextboxNumeric = new System.Windows.Forms.TextBox();
            this.CancelCButton = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.OKSaveButton = new System.Windows.Forms.Button();
            this.EncashLeaves = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.MonthLeave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.Balance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboPeriod = new System.Windows.Forms.ComboBox();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.GrpMain = new System.Windows.Forms.GroupBox();
            this.DgvEmployeeLeaveStructure = new ClsDataGirdViewX();
            this.LeavePolicy_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.leaveType_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Leave_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Balance_Leaves = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CarryForward_Days = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Taken_Leaves = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Extended_Leaves = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Balance_Leave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Month_Leave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Encash_Leaves = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LeavePolicyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.leaveTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LeaveType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BalanceLeaves = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CarryForwardDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TakenLeaves = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Extended = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TmrLeavePolicy = new System.Windows.Forms.Timer(this.components);
            EmployeeName = new System.Windows.Forms.Label();
            lblCompanyName = new System.Windows.Forms.Label();
            lblPeriod = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PolicyMasterBindingNavigator)).BeginInit();
            this.PolicyMasterBindingNavigator.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            this.GrpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvEmployeeLeaveStructure)).BeginInit();
            this.SuspendLayout();
            // 
            // EmployeeName
            // 
            EmployeeName.AutoSize = true;
            EmployeeName.Location = new System.Drawing.Point(13, 46);
            EmployeeName.Name = "EmployeeName";
            EmployeeName.Size = new System.Drawing.Size(53, 13);
            EmployeeName.TabIndex = 202;
            EmployeeName.Text = "Employee";
            // 
            // lblCompanyName
            // 
            lblCompanyName.AutoSize = true;
            lblCompanyName.Location = new System.Drawing.Point(13, 20);
            lblCompanyName.Name = "lblCompanyName";
            lblCompanyName.Size = new System.Drawing.Size(51, 13);
            lblCompanyName.TabIndex = 206;
            lblCompanyName.Text = "Company";
            // 
            // lblPeriod
            // 
            lblPeriod.AutoSize = true;
            lblPeriod.Location = new System.Drawing.Point(363, 46);
            lblPeriod.Name = "lblPeriod";
            lblPeriod.Size = new System.Drawing.Size(37, 13);
            lblPeriod.TabIndex = 204;
            lblPeriod.Text = "Period";
            // 
            // PolicyMasterBindingNavigator
            // 
            this.PolicyMasterBindingNavigator.AddNewItem = null;
            this.PolicyMasterBindingNavigator.CountItem = null;
            this.PolicyMasterBindingNavigator.DeleteItem = null;
            this.PolicyMasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorSeparator2,
            this.LeaveStructureBindingNavigatorSaveItem,
            this.toolStripSeparator1,
            this.BtnPrint,
            this.btnEmail,
            this.ToolStripSeparator2,
            this.BtnHelp});
            this.PolicyMasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.PolicyMasterBindingNavigator.MoveFirstItem = null;
            this.PolicyMasterBindingNavigator.MoveLastItem = null;
            this.PolicyMasterBindingNavigator.MoveNextItem = null;
            this.PolicyMasterBindingNavigator.MovePreviousItem = null;
            this.PolicyMasterBindingNavigator.Name = "PolicyMasterBindingNavigator";
            this.PolicyMasterBindingNavigator.PositionItem = null;
            this.PolicyMasterBindingNavigator.Size = new System.Drawing.Size(652, 25);
            this.PolicyMasterBindingNavigator.TabIndex = 330;
            this.PolicyMasterBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // LeaveStructureBindingNavigatorSaveItem
            // 
            this.LeaveStructureBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.LeaveStructureBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("LeaveStructureBindingNavigatorSaveItem.Image")));
            this.LeaveStructureBindingNavigatorSaveItem.Name = "LeaveStructureBindingNavigatorSaveItem";
            this.LeaveStructureBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.LeaveStructureBindingNavigatorSaveItem.Text = "Save Data";
            this.LeaveStructureBindingNavigatorSaveItem.Click += new System.EventHandler(this.LeaveStructureBindingNavigatorSaveItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Print";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 17);
            this.StatusLabel.ToolTipText = "Status";
            // 
            // TextboxNumeric
            // 
            this.TextboxNumeric.Location = new System.Drawing.Point(242, 178);
            this.TextboxNumeric.Name = "TextboxNumeric";
            this.TextboxNumeric.Size = new System.Drawing.Size(100, 20);
            this.TextboxNumeric.TabIndex = 325;
            this.TextboxNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TextboxNumeric.Visible = false;
            // 
            // CancelCButton
            // 
            this.CancelCButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelCButton.Location = new System.Drawing.Point(570, 342);
            this.CancelCButton.Name = "CancelCButton";
            this.CancelCButton.Size = new System.Drawing.Size(75, 23);
            this.CancelCButton.TabIndex = 328;
            this.CancelCButton.Text = "&Cancel";
            this.CancelCButton.UseVisualStyleBackColor = true;
            this.CancelCButton.Click += new System.EventHandler(this.CancelCButton_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSave.Location = new System.Drawing.Point(7, 342);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 326;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // OKSaveButton
            // 
            this.OKSaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OKSaveButton.Enabled = false;
            this.OKSaveButton.Location = new System.Drawing.Point(489, 342);
            this.OKSaveButton.Name = "OKSaveButton";
            this.OKSaveButton.Size = new System.Drawing.Size(75, 23);
            this.OKSaveButton.TabIndex = 327;
            this.OKSaveButton.Text = "&OK";
            this.OKSaveButton.UseVisualStyleBackColor = true;
            this.OKSaveButton.Click += new System.EventHandler(this.OKSaveButton_Click);
            // 
            // EncashLeaves
            // 
            this.EncashLeaves.HeaderText = "Encash Leaves";
            this.EncashLeaves.Name = "EncashLeaves";
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.lblstatus});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 370);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(652, 22);
            this.StatusStrip1.TabIndex = 329;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // MonthLeave
            // 
            this.MonthLeave.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MonthLeave.HeaderText = "Month Leave";
            this.MonthLeave.MaxInputLength = 6;
            this.MonthLeave.Name = "MonthLeave";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.Location = new System.Drawing.Point(86, 16);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(235, 21);
            this.cboCompany.TabIndex = 0;
            this.cboCompany.SelectionChangeCommitted += new System.EventHandler(this.cboCompany_SelectionChangeCommitted);
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.Leave += new System.EventHandler(this.cboCompany_Leave);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCompany_KeyPress);
            // 
            // Balance
            // 
            this.Balance.HeaderText = "Balance";
            this.Balance.Name = "Balance";
            this.Balance.ReadOnly = true;
            // 
            // cboPeriod
            // 
            this.cboPeriod.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPeriod.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPeriod.BackColor = System.Drawing.SystemColors.Info;
            this.cboPeriod.DropDownHeight = 134;
            this.cboPeriod.FormattingEnabled = true;
            this.cboPeriod.IntegralHeight = false;
            this.cboPeriod.Location = new System.Drawing.Point(406, 42);
            this.cboPeriod.Name = "cboPeriod";
            this.cboPeriod.Size = new System.Drawing.Size(220, 21);
            this.cboPeriod.TabIndex = 2;
            this.cboPeriod.SelectedIndexChanged += new System.EventHandler(this.cboPeriod_SelectedIndexChanged);
            this.cboPeriod.Leave += new System.EventHandler(this.cboPeriod_Leave);
            this.cboPeriod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboPeriod_KeyPress);
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(86, 43);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(235, 21);
            this.cboEmployee.TabIndex = 1;
            this.cboEmployee.SelectedIndexChanged += new System.EventHandler(this.cboEmployee_SelectedIndexChanged);
            this.cboEmployee.Leave += new System.EventHandler(this.cboEmployee_Leave);
            this.cboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboEmployee_KeyPress);
            // 
            // GrpMain
            // 
            this.GrpMain.Controls.Add(this.DgvEmployeeLeaveStructure);
            this.GrpMain.Controls.Add(this.cboCompany);
            this.GrpMain.Controls.Add(lblCompanyName);
            this.GrpMain.Controls.Add(this.cboPeriod);
            this.GrpMain.Controls.Add(lblPeriod);
            this.GrpMain.Controls.Add(this.cboEmployee);
            this.GrpMain.Controls.Add(EmployeeName);
            this.GrpMain.Controls.Add(this.TextboxNumeric);
            this.GrpMain.Location = new System.Drawing.Point(6, 24);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.Size = new System.Drawing.Size(640, 312);
            this.GrpMain.TabIndex = 325;
            this.GrpMain.TabStop = false;
            // 
            // DgvEmployeeLeaveStructure
            // 
            this.DgvEmployeeLeaveStructure.AddNewRow = false;
            this.DgvEmployeeLeaveStructure.AllowUserToAddRows = false;
            this.DgvEmployeeLeaveStructure.AlphaNumericCols = new int[0];
            this.DgvEmployeeLeaveStructure.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvEmployeeLeaveStructure.BackgroundColor = System.Drawing.Color.White;
            this.DgvEmployeeLeaveStructure.CapsLockCols = new int[0];
            this.DgvEmployeeLeaveStructure.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvEmployeeLeaveStructure.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LeavePolicy_ID,
            this.leaveType_ID,
            this.Leave_Type,
            this.Balance_Leaves,
            this.CarryForward_Days,
            this.Taken_Leaves,
            this.Extended_Leaves,
            this.Balance_Leave,
            this.Month_Leave,
            this.Encash_Leaves});
            this.DgvEmployeeLeaveStructure.DecimalCols = new int[] {
        1,
        2,
        3,
        4,
        5,
        6,
        7};
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvEmployeeLeaveStructure.DefaultCellStyle = dataGridViewCellStyle1;
            this.DgvEmployeeLeaveStructure.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvEmployeeLeaveStructure.HasSlNo = false;
            this.DgvEmployeeLeaveStructure.LastRowIndex = 0;
            this.DgvEmployeeLeaveStructure.Location = new System.Drawing.Point(15, 81);
            this.DgvEmployeeLeaveStructure.Name = "DgvEmployeeLeaveStructure";
            this.DgvEmployeeLeaveStructure.NegativeValueCols = new int[0];
            this.DgvEmployeeLeaveStructure.NumericCols = new int[0];
            this.DgvEmployeeLeaveStructure.Size = new System.Drawing.Size(611, 212);
            this.DgvEmployeeLeaveStructure.TabIndex = 326;
            this.DgvEmployeeLeaveStructure.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DgvEmployeeLeaveStructure_CellValidating);
            this.DgvEmployeeLeaveStructure.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvEmployeeLeaveStructure_CellEndEdit);
            this.DgvEmployeeLeaveStructure.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DgvEmployeeLeaveStructure_EditingControlShowing);
            this.DgvEmployeeLeaveStructure.CurrentCellDirtyStateChanged += new System.EventHandler(this.DgvEmployeeLeaveStructure_CurrentCellDirtyStateChanged);
            this.DgvEmployeeLeaveStructure.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DgvEmployeeLeaveStructure_DataError);
            this.DgvEmployeeLeaveStructure.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvEmployeeLeaveStructure_CellEnter);
            // 
            // LeavePolicy_ID
            // 
            this.LeavePolicy_ID.HeaderText = "LeavePolicy_ID";
            this.LeavePolicy_ID.Name = "LeavePolicy_ID";
            this.LeavePolicy_ID.Visible = false;
            // 
            // leaveType_ID
            // 
            this.leaveType_ID.HeaderText = "leaveType_ID";
            this.leaveType_ID.Name = "leaveType_ID";
            this.leaveType_ID.Visible = false;
            // 
            // Leave_Type
            // 
            this.Leave_Type.HeaderText = "Leave Type";
            this.Leave_Type.Name = "Leave_Type";
            this.Leave_Type.ReadOnly = true;
            // 
            // Balance_Leaves
            // 
            this.Balance_Leaves.HeaderText = "Days";
            this.Balance_Leaves.MaxInputLength = 3;
            this.Balance_Leaves.Name = "Balance_Leaves";
            // 
            // CarryForward_Days
            // 
            this.CarryForward_Days.HeaderText = "Carry Forward";
            this.CarryForward_Days.MaxInputLength = 4;
            this.CarryForward_Days.Name = "CarryForward_Days";
            // 
            // Taken_Leaves
            // 
            this.Taken_Leaves.HeaderText = "Taken Leaves";
            this.Taken_Leaves.MaxInputLength = 3;
            this.Taken_Leaves.Name = "Taken_Leaves";
            this.Taken_Leaves.ReadOnly = true;
            // 
            // Extended_Leaves
            // 
            this.Extended_Leaves.HeaderText = "Extended";
            this.Extended_Leaves.MaxInputLength = 4;
            this.Extended_Leaves.Name = "Extended_Leaves";
            this.Extended_Leaves.ReadOnly = true;
            // 
            // Balance_Leave
            // 
            this.Balance_Leave.HeaderText = "Balance";
            this.Balance_Leave.MaxInputLength = 4;
            this.Balance_Leave.Name = "Balance_Leave";
            this.Balance_Leave.ReadOnly = true;
            // 
            // Month_Leave
            // 
            this.Month_Leave.HeaderText = "Month Leave";
            this.Month_Leave.MaxInputLength = 4;
            this.Month_Leave.Name = "Month_Leave";
            // 
            // Encash_Leaves
            // 
            this.Encash_Leaves.HeaderText = "Encash Leaves";
            this.Encash_Leaves.MaxInputLength = 4;
            this.Encash_Leaves.Name = "Encash_Leaves";
            // 
            // LeavePolicyID
            // 
            this.LeavePolicyID.HeaderText = "LeavePolicyID";
            this.LeavePolicyID.Name = "LeavePolicyID";
            this.LeavePolicyID.Visible = false;
            // 
            // leaveTypeID
            // 
            this.leaveTypeID.HeaderText = "leaveTypeID";
            this.leaveTypeID.Name = "leaveTypeID";
            this.leaveTypeID.Visible = false;
            // 
            // LeaveType
            // 
            this.LeaveType.HeaderText = "LeaveType";
            this.LeaveType.Name = "LeaveType";
            this.LeaveType.ReadOnly = true;
            // 
            // BalanceLeaves
            // 
            this.BalanceLeaves.HeaderText = "Days";
            this.BalanceLeaves.MaxInputLength = 3;
            this.BalanceLeaves.Name = "BalanceLeaves";
            this.BalanceLeaves.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CarryForwardDays
            // 
            this.CarryForwardDays.HeaderText = "Carry Forward";
            this.CarryForwardDays.MaxInputLength = 5;
            this.CarryForwardDays.Name = "CarryForwardDays";
            this.CarryForwardDays.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // TakenLeaves
            // 
            this.TakenLeaves.HeaderText = "Taken Leaves";
            this.TakenLeaves.MaxInputLength = 3;
            this.TakenLeaves.Name = "TakenLeaves";
            this.TakenLeaves.ReadOnly = true;
            this.TakenLeaves.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Extended
            // 
            this.Extended.HeaderText = "Extended";
            this.Extended.Name = "Extended";
            this.Extended.ReadOnly = true;
            // 
            // TmrLeavePolicy
            // 
            this.TmrLeavePolicy.Interval = 1000;
            // 
            // FrmEmployeeLeaveStructure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 392);
            this.Controls.Add(this.PolicyMasterBindingNavigator);
            this.Controls.Add(this.CancelCButton);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.OKSaveButton);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.GrpMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEmployeeLeaveStructure";
            this.Text = "Leave Structure";
            this.Load += new System.EventHandler(this.FrmEmployeeLeaveStructure_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmEmployeeLeaveStructure_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmLeaveStructure_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.PolicyMasterBindingNavigator)).EndInit();
            this.PolicyMasterBindingNavigator.ResumeLayout(false);
            this.PolicyMasterBindingNavigator.PerformLayout();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvEmployeeLeaveStructure)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator PolicyMasterBindingNavigator;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton LeaveStructureBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        internal System.Windows.Forms.TextBox TextboxNumeric;
        internal System.Windows.Forms.Button CancelCButton;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Button OKSaveButton;
        internal System.Windows.Forms.DataGridViewTextBoxColumn EncashLeaves;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn MonthLeave;
        internal System.Windows.Forms.ComboBox cboCompany;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Balance;
        internal System.Windows.Forms.ComboBox cboPeriod;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal System.Windows.Forms.GroupBox GrpMain;
        internal System.Windows.Forms.DataGridViewTextBoxColumn LeavePolicyID;
        internal System.Windows.Forms.DataGridViewTextBoxColumn leaveTypeID;
        internal System.Windows.Forms.DataGridViewTextBoxColumn LeaveType;
        internal System.Windows.Forms.DataGridViewTextBoxColumn BalanceLeaves;
        internal System.Windows.Forms.DataGridViewTextBoxColumn CarryForwardDays;
        internal System.Windows.Forms.DataGridViewTextBoxColumn TakenLeaves;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Extended;
        internal System.Windows.Forms.Timer TmrLeavePolicy;
        private ClsDataGirdViewX DgvEmployeeLeaveStructure;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.DataGridViewTextBoxColumn LeavePolicy_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn leaveType_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Leave_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Balance_Leaves;
        private System.Windows.Forms.DataGridViewTextBoxColumn CarryForward_Days;
        private System.Windows.Forms.DataGridViewTextBoxColumn Taken_Leaves;
        private System.Windows.Forms.DataGridViewTextBoxColumn Extended_Leaves;
        private System.Windows.Forms.DataGridViewTextBoxColumn Balance_Leave;
        private System.Windows.Forms.DataGridViewTextBoxColumn Month_Leave;
        private System.Windows.Forms.DataGridViewTextBoxColumn Encash_Leaves;
    }
}