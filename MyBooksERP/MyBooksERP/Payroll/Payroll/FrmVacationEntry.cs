﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using BLL;
using DTO;
using System.Globalization;
namespace MyBooksERP
{
    /// <summary>
    ///  Modified By        : Sachin
    ///  Modified Date      : 8/14/2013
    ///  Purpose            : Performance tuning
    /// </summary>
    public partial class FrmVacationEntry : Form
    {
        #region Declarations
        private bool MbAddStatus;
        private bool MblnSearch;
        private int CurrentRecCnt;
        private int RecordCnt;

        private bool MbChangeStatus;                    // Check state of the page
        private bool MblnAddPermission = false;         // To Set Add Permission
        private bool MblnUpdatePermission = false;      // To Set Update Permission
        private bool MblnDeletePermission = false;      // To Set Delete Permission
        private bool MblnPrintEmailPermission = false;  // To Set PrintEmail Permission
        private bool MblnExtenstionPermission = false;
        //private bool MblnIsHrPowerEnabled = false;      //To set if HRPower is enabled

        private bool blnAddvalue = false;

        private string MstrMessageCommon;               // Messagebox display
        private string MstrMessageCaption;              // Message caption
        private ArrayList MsarMessageArr;               // Error Message display
        private ArrayList MsarStatusMessage;            // Status bar error message display

        public int PiCompanyID = 0;                     // Reference From Work policy
        public int PiShiftID = 0;                       // Reference from Shift policy
        private int MintTimerInterval;                  // To set timer interval
        private int MintCompanyId;                      // current companyid
        private int MintUserId;
        bool mblnIsEmployeeFixedCategory = false;
        private int NavigateFlg = 0;
        private MessageBoxIcon MmessageIcon;            // to set the message icon
        private bool Glb24HourFormat;
        bool MblnProcess = false;
        bool mblnIsFromProcess = false;
        private bool mblnSearchStatus = false;

        ClsLogWriter mObjLogs;
        ClsNotification mObjNotification;
        clsBLLVacationEntry MobjclsBLLVacationEntry;
        string strBindingOf = "Of ";
        #endregion Declarations
        #region Constructor
        public FrmVacationEntry()
        {
            //Constructor
            InitializeComponent();
            MintCompanyId = ClsCommonSettings.LoginCompanyID;
            MintUserId = ClsCommonSettings.UserID;
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            Glb24HourFormat = ClsCommonSettings.Glb24HourFormat;
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            MobjclsBLLVacationEntry = new clsBLLVacationEntry();
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls(); 
            //}
        }
        #endregion Constructor
        #region Events
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboParticulars.SelectedValue == System.DBNull.Value || cboParticulars.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7566, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cboParticulars.Focus();
                    return;
                }

                if (txtAmount.Text.Trim() == "" || txtAmount.Text.Trim() == ".")
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7567, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAmount.Focus();
                    return;
                }

                if (CheckGridDuplication(Convert.ToInt32(cboParticulars.SelectedValue)) == true)
                {
                    if (InsertParticulars(cboParticulars.SelectedValue.ToInt32(),txtAmount.Text.ToDecimal()))
                    {
                        dgvVacationDetails.RowCount = dgvVacationDetails.RowCount + 1;
                        dgvVacationDetails[dgvColParticulars.Index, dgvVacationDetails.RowCount - 1].Value = cboParticulars.SelectedValue;
                        dgvVacationDetails[dgvColAmount.Index, dgvVacationDetails.RowCount - 1].Value = txtAmount.Text;
                        dgvVacationDetails[dgvColTempAmount.Index, dgvVacationDetails.RowCount - 1].Value = txtAmount.Text;
                        //CalculateNetAmount();

                        ChangeStatus();

                        cboParticulars.SelectedIndex = -1;
                        txtAmount.Clear();
                        cboParticulars.Focus();
                    }
                    MobjclsBLLVacationEntry.UpdateNetAmount(txtNetAmount.Text.ToDecimal(), txtGivenAmount.Text.ToDecimal());

                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on LoadMessage " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }

        private bool InsertParticulars(int intAdditionDeductionID, decimal decAmount)
        {
            if (MobjclsBLLVacationEntry.InsertParticulars(intAdditionDeductionID, decAmount) == true)
            {
                if (MbAddStatus == true)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon).ToString().Replace("*", "Vacation Detail");
                    lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                }
                else
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 21, out MmessageIcon);
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 21, out MmessageIcon).ToString().Replace("*", "Vacation Detail");
                    lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 21, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                }

                //DisplayVacation(CurrentRecCnt);
                tmrVacation.Enabled = true;
            }
            return true;
        }

        private void FrmVacationEntry_Load(object sender, EventArgs e)
        {
            //if (ClsCommonSettings.IsHrPowerEnabled)
            //{
            //    MblnIsHrPowerEnabled = true;
            //    dtpFromDate.Enabled = dtpToDate.Enabled = false;
            //}
            //else
            //{
                //MblnIsHrPowerEnabled = false;
                dtpFromDate.Enabled = dtpToDate.Enabled = true;
            //}

            tmrVacation.Interval = MintTimerInterval;
            MbAddStatus = true;
            LoadCombos(0);
            LoadVacationFormMessage();
            SetPermissions();
            AddNew();
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {

            lblExperienceDaysText.Text = "0";
            lblAbsentDaysText.Text = "0";
            lblTotalEligibleLeavePayDaysText.Text = "0";

            mblnIsEmployeeFixedCategory = false;

            this.FillDetail();
            //if (MblnIsHrPowerEnabled)
            //{
            //    //datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "DISTINCT EM.EmployeeID,ISNULL(EM.FirstName,'') + '' + ISNULL(EM.LastName,'') + ' - ' +  ISNULL(CAST(EM.EmployeeNumber AS VARCHAR(30)),'') AS Name ", "EmployeeMaster EM inner join PaySalaryStructure ESH on EM.EmployeeID=ESH.EmployeeID ", " EM.WorkStatusID >= 6 And ISNULL(EM.VacationPolicyID,0) <> 0 ORDER BY Name", "EmployeeID", "Name" });
            //    DataTable datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "LeaveFromDate,LeaveToDate", "HRLeaveRequest", "EmployeeID=" + cboEmployee.SelectedValue.ToInt32() + " AND IsVacation=1 AND VacationID IS NULL AND StatusID in (5,7) AND ApprovedBy IS NOT NULL", "", "" });
            //    if (datCombos != null && datCombos.Rows.Count > 0)
            //    {
            //        dtpFromDate.Value = datCombos.Rows[0]["LeaveFromDate"].ToDateTime();
            //        dtpToDate.Value = datCombos.Rows[0]["LeaveToDate"].ToDateTime();
            //       // dtpFromDate.Enabled = dtpToDate.Enabled = false;
            //    }
            //}
            EmployeeHistoryDetail();

        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            MbAddStatus = false;
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;
            if (MblnSearch == false)
            {
                RecCount();
            }

            if (RecordCnt > 0)
            {
                CurrentRecCnt = 1;
            }
            else
            {
                CurrentRecCnt = 0;
            }


            DisplayVacation(CurrentRecCnt);
            MblnProcess = false;
            bnPositionItem.Text = Convert.ToString(CurrentRecCnt);

            if (CurrentRecCnt == 1)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
            }
            else
            {
                BindingNavigatorMoveFirstItem.Enabled = true;
                BindingNavigatorMovePreviousItem.Enabled = true;
            }

            if (CurrentRecCnt == RecordCnt)
            {
                BindingNavigatorMoveLastItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
            }
            else
            {
                BindingNavigatorMoveLastItem.Enabled = true;
                BindingNavigatorMoveNextItem.Enabled = true;
            }

            //if (ClsCommonSettings.IsArabicView)
            //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9, out MmessageIcon).ToString().Replace("*", "معلومات اجازة");
            //else
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9, out MmessageIcon).ToString().Replace("*", "Vacation Detail");
            lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 9, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {

            MbAddStatus = false;
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty; 
            if (MblnSearch == false) { RecCountNavigate(); }
            if (RecordCnt > 0)
            {
                CurrentRecCnt = CurrentRecCnt - 1;
                if (CurrentRecCnt <= 0)
                {
                    CurrentRecCnt = 1;
                }
            }
            else
            {
                CurrentRecCnt = 1;
            }


            DisplayVacation(CurrentRecCnt);
            MblnProcess = false;
            bnPositionItem.Text = Convert.ToString(CurrentRecCnt);

            if (CurrentRecCnt == 1)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
            }
            else
            {
                BindingNavigatorMoveFirstItem.Enabled = true;
                BindingNavigatorMovePreviousItem.Enabled = true;
            }

            if (CurrentRecCnt == RecordCnt)
            {
                BindingNavigatorMoveLastItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
            }
            else
            {
                BindingNavigatorMoveLastItem.Enabled = true;
                BindingNavigatorMoveNextItem.Enabled = true;
            }
            
            //if (ClsCommonSettings.IsArabicView)
            //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 10, out MmessageIcon).ToString().Replace("*", "معلومات اجازة");
            //else
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 10, out MmessageIcon).ToString().Replace("*", "Vacation Detail");
            lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 10, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            MbAddStatus = false;
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty; 
            if (MblnSearch == false) { RecCountNavigate(); }
            if (RecordCnt > 0)
            {
                CurrentRecCnt = CurrentRecCnt + 1;
                if (CurrentRecCnt >= RecordCnt)
                {
                    CurrentRecCnt = RecordCnt;
                }
            }
            else
            {
                CurrentRecCnt = 0;
            }


            DisplayVacation(CurrentRecCnt);
            MblnProcess = false;
            bnPositionItem.Text = Convert.ToString(CurrentRecCnt);

            if (CurrentRecCnt == 1)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
            }
            else
            {
                BindingNavigatorMoveFirstItem.Enabled = true;
                BindingNavigatorMovePreviousItem.Enabled = true;
            }

            if (CurrentRecCnt == RecordCnt)
            {
                BindingNavigatorMoveLastItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
            }
            else
            {
                BindingNavigatorMoveLastItem.Enabled = true;
                BindingNavigatorMoveNextItem.Enabled = true;
            }
            
            //if (ClsCommonSettings.IsArabicView)
            //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 11, out MmessageIcon).ToString().Replace("*", "معلومات اجازة");
            //else
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 11, out MmessageIcon).ToString().Replace("*", "Vacation Detail");
            lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 11, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            MbAddStatus = false;
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty; 
            if (MblnSearch == false) { RecCountNavigate(); }
            if (RecordCnt > 0)
            {
                CurrentRecCnt = RecordCnt;
            }
            else
            {
                CurrentRecCnt = 0;
            }

            DisplayVacation(CurrentRecCnt);
            MblnProcess = false;
            bnPositionItem.Text = Convert.ToString(CurrentRecCnt);

            if (CurrentRecCnt == 1)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
            }
            else
            {
                BindingNavigatorMoveFirstItem.Enabled = true;
                BindingNavigatorMovePreviousItem.Enabled = true;
            }

            if (CurrentRecCnt == RecordCnt)
            {
                BindingNavigatorMoveLastItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
            }
            else
            {
                BindingNavigatorMoveLastItem.Enabled = true;
                BindingNavigatorMoveNextItem.Enabled = true;
            }
            
            //if (ClsCommonSettings.IsArabicView)
            //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 12, out MmessageIcon).ToString().Replace("*", "معلومات اجازة");
            //else
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 12, out MmessageIcon).ToString().Replace("*", "Vacation Detail");
            lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 12, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);
        }
        private void btnProcess_Click(object sender, EventArgs e)
        {
            ChangeStatus();
            FillParameters();
             
            if (MblnProcess == false)
            {
                if (cboEmployee.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7574, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    errorProviderVacation.SetError(cboEmployee, MstrMessageCommon.Replace("#", "").Trim());
                    lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                    tmrVacation.Enabled = true;
                    cboEmployee.Focus();
                    return;
                }

                if (chkEncashOnly.Checked == true)
                {
                    chkProcessSalaryForCurrentMonth.Checked = false;
                    dtpToDate.Value = dtpFromDate.Value;
                    lblNoOfDays.Text = "0";
                }
                if (cboEmployee.SelectedIndex != -1)
                {
                    if (MobjclsBLLVacationEntry.IsVacationPolicyExists() == false)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7551, out MmessageIcon);
                        if (MessageBox.Show(MstrMessageCommon, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return;
                        }
                    }


                    if (MobjclsBLLVacationEntry.IsEmployeeInService() == true)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7560, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                        return;
                    }

                    if (MobjclsBLLVacationEntry.IsPendingVacationProcessed() == true)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7571, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                        return;
                    }


                    /// checking vacqation Already processed in this period
                    if (MobjclsBLLVacationEntry.IsVacationAlreadyProcessed(dtpFromDate.Value.ToString("dd-MMM-yyyy"), dtpToDate.Value.ToString("dd-MMM-yyyy")) == true)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7561, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                        return;
                    }


                    //Check whether salary is processed
                    if (MobjclsBLLVacationEntry.IsSalaryIsProcessedOrClosed() == true)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7564, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                        return;
                    }
                }

                if (dtpFromDate.Value.Date < Convert.ToDateTime(MobjclsBLLVacationEntry.GetDateofJoining()))
                {
                    //Date should not be less than the joining date.
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3081, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    errorProviderVacation.SetError(dtpFromDate, MstrMessageCommon.Replace("#", "").Trim());
                    lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                    tmrVacation.Enabled = true;
                    dtpFromDate.Focus();
                    return;
                }


                if (dtpToDate.Value.Date < dtpFromDate.Value.Date)
                {
                    //From date should not be greater than To Date
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 19, out MmessageIcon);
                    errorProviderVacation.SetError(dtpFromDate, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrVacation.Enabled = true;
                    dtpFromDate.Focus();
                    return;
                }



                if (dtpRejoinDate.Value.Date <= dtpToDate.Value.Date)
                {
                    //Rejoin date should be greater than vacation to date
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7582, out MmessageIcon);
                    errorProviderVacation.SetError(dtpRejoinDate, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrVacation.Enabled = true;
                    dtpRejoinDate.Focus();
                    return;
                }

                if (MobjclsBLLVacationEntry.SalaryStructureCheck() == false)
                {
                    //This Employee has no salary structure
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9129, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                    tmrVacation.Enabled = true;
                    return;
                }


                if (FormValidation() == false)
                {
                    MblnProcess = false;
                    return;
                }
                else
                {
                    MblnProcess = true;
                    mblnIsFromProcess = true;
                }

                FillProcessDetails();
                FillParameters();

                //Sets voucher entry as false
                MobjclsBLLVacationEntry.clsDTOVacationEntry.NeedVoucherEntry = false;

                SaveVacation();
                tmrVacation.Enabled = true;
                //bnAddNewItem_Click(sender, e);
                //BindingNavigatorMovePreviousItem_Click(sender, e);
                //tabVacationProcess.SelectedTab = tabParticulars;

                bnSaveItem.Enabled = false;
                btnSave.Enabled = false;
                mblnIsFromProcess = false;
                MblnProcess = false;
            }

        }
        private void dgvVacationDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on LoadMessage " + this.Name +  Ex.Message.ToString(), 1);
            }

        }

        private void txtEligibleLeavePayDays_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
            this.FillDetail();
        }

        private void dtpProcessDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
            dtpRejoinDate.Value = dtpToDate.Value.AddDays(1);
            this.FillDetail();
        }

        private void chkEncashOnly_CheckedChanged(object sender, EventArgs e)
        {
            ChangeStatus();
            this.FillDetail();

            if (chkEncashOnly.Checked)
            {
                dtpToDate.Enabled = false;
                chkProcessSalaryForCurrentMonth.Checked = false;
            }
            else
            {

                    dtpToDate.Enabled = true;
              
            }
        }

        private void chkConsiderAbsentDays_CheckedChanged(object sender, EventArgs e)
        {
            ChangeStatus();
            this.FillDetail();
        }

        private void chkProcessSalaryForCurrentMonth_CheckedChanged(object sender, EventArgs e)
        {
            ChangeStatus();
            this.FillDetail();

            if (chkProcessSalaryForCurrentMonth.Checked)
                chkEncashOnly.Checked = false;

        }

        void txtInt_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}.{:?></,`-=\\[]'";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0))
            {
                e.Handled = true;
            }
        }
        void txtDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]'";
            e.Handled = false;

            //if (ClsCommonSettings.IsAmountRoundByZero)
            //{
            //    strInvalidChars = strInvalidChars + ".";
            //}

            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        }
        void cb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            cb.DroppedDown = false;
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
                e.Handled = false;

                //if (ClsCommonSettings.IsAmountRoundByZero)
                //{
                //    strInvalidChars = strInvalidChars + ".";
                //}


                if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
                {
                    e.Handled = true;
                }
            }
            catch
            {
            }

        }

        private void txtTicketAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
                e.Handled = false;
                if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
                {
                    e.Handled = true;
                }
            }
            catch
            {
            }
        }

        private void txtEligibleLeavePayDays_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
                e.Handled = false;
                if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
                {
                    e.Handled = true;
                }
            }
            catch
            {
            }
        }

        private void txtNoOfTicketsIssued_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
                e.Handled = false;
                if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
                {
                    e.Handled = true;
                }
            }
            catch
            {
            }
        }

        private void bnVacationExtension_Click(object sender, EventArgs e)
        {
            using (FrmVacationExtension ObjSet = new FrmVacationExtension())
            {
                ObjSet.Days = Convert.ToDouble(lblAdditionalLeavesText.Text);

                if (lblAdditionLeaves.Text == "Shortened Days" || lblAdditionLeaves.Text == "يوم تقصير")
                {
                    ObjSet.Days = -1 * ObjSet.Days;
                }


                ObjSet.VacationDays = Convert.ToDouble(lblNoOfDays.Text);
                ObjSet.Remarks = txtRemarks.Text;


                if (lblIsPaid.Text.Trim() == "Paid" || lblIsPaid.Text.Trim() == "مدفوع")
                {
                    ObjSet.IsPaid = true;
                }
                else
                {
                    ObjSet.IsPaid = false;
                }

                if (lblActualRejoinDate.Text.Trim() != "")
                {
                    ObjSet.ActualDateTime = lblActualRejoinDate.Text.Trim().ToDateTime();
                }
                else
                {
                    ObjSet.ActualDateTime = "01-Jan-1900".ToDateTime(); 
                }

                ObjSet.ToDateTime = dtpToDate.Value.ToDateTime(); 

                ObjSet.ShowDialog();




                if (ObjSet.IsFromOK == true)
                {
                    DateTime dtToDate = dtpToDate.Value;
                    String strNoOfDays = lblNoOfDays.Text;
                    String strAdditionalDays = lblAdditionalLeavesText.Text;
                    String strAdditionStatus = "";

                    if (ObjSet.Days < 0)
                    {

                        MobjclsBLLVacationEntry.clsDTOVacationEntry.ToDate = Convert.ToString(MobjclsBLLVacationEntry.clsDTOVacationEntry.ToDate.ToDateTime().Subtract(new TimeSpan(Convert.ToInt32(Math.Abs(ObjSet.Days)), 0, 0, 0)));
                        //if (ClsCommonSettings.IsArabicView)
                        //    strAdditionStatus = "عطلة تقصير ل " + Convert.ToString(Math.Abs(ObjSet.Days)) + " أيام وإعادة الانضمام التسجيل";
                        //else
                            strAdditionStatus = "Vacation Shortened to " + Convert.ToString(Math.Abs(ObjSet.Days)) + " days and the Rejoin Date";
                    }
                    else
                    {
                        MobjclsBLLVacationEntry.clsDTOVacationEntry.IsExtension = true;
                        MobjclsBLLVacationEntry.clsDTOVacationEntry.FromDate = MobjclsBLLVacationEntry.clsDTOVacationEntry.ToDate.ToDateTime().AddDays(1).ToString("dd-MMM-yyyy");
                        MobjclsBLLVacationEntry.clsDTOVacationEntry.ToDate = MobjclsBLLVacationEntry.clsDTOVacationEntry.ToDate.ToDateTime().AddDays(Convert.ToDouble(ObjSet.Days)).ToString("dd-MMM-yyyy");
                        //if (ClsCommonSettings.IsArabicView)
                        //    strAdditionStatus = " عطلة الموسعة ل " + Convert.ToString(Math.Abs(ObjSet.Days)) + " أيام وإعادة الانضمام التسجيل";
                        //else
                            strAdditionStatus = "Vacation Extended to " + Convert.ToString(Math.Abs(ObjSet.Days)) + " days and the Rejoin Date";
                    }

                    

                    MobjclsBLLVacationEntry.clsDTOVacationEntry.PaidLeave = true;
                    ObjSet.IsPaid = true;
                    dtpToDate.Value = Convert.ToDateTime(MobjclsBLLVacationEntry.clsDTOVacationEntry.ToDate);
                    lblNoOfDays.Text = strNoOfDays;
                    lblAdditionalLeavesText.Text = strAdditionalDays;
                    MobjclsBLLVacationEntry.clsDTOVacationEntry.ActualDateTime = ObjSet.ActualDateTime; 
                    bool tempDuc;
                    tempDuc = MobjclsBLLVacationEntry.chkdupicate(MbAddStatus);
                    if (tempDuc == true)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7557, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        tmrVacation.Enabled = true;
                        dtpFromDate.Focus();
                        dtpToDate.Value = dtToDate;
                        MobjclsBLLVacationEntry.clsDTOVacationEntry.ToDate = dtToDate.ToString("dd-MMM-yyyy");
                        return;
                    }

                    MobjclsBLLVacationEntry.clsDTOVacationEntry.OvertakenLeaves = Convert.ToDecimal(ObjSet.Days);
                    MobjclsBLLVacationEntry.clsDTOVacationEntry.Remarks = ObjSet.Remarks;

                    //MessageBox.Show(MobjclsBLLVacationEntry.clsDTOVacationEntry.ActualDateTime.ToString("dd-MMM-yyyy"));    

                    MobjclsBLLVacationEntry.UpdateOverTakenLeaves();

                    //MessageBox.Show(MobjclsBLLVacationEntry.clsDTOVacationEntry.ActualDateTime.ToString());    


                    DisplayVacation(CurrentRecCnt);

                }
            } ////END USING
        }


        private void btnOK_Click(object sender, EventArgs e)
        {

            if (MblnProcess == false && MobjclsBLLVacationEntry.clsDTOVacationEntry.IsClosed == false && MbAddStatus == true)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7554, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7568, out MmessageIcon);

            if (MessageBox.Show(MstrMessageCommon, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            MobjclsBLLVacationEntry.clsDTOVacationEntry.IsClosed = true;
            //Sets voucher entry as true
            MobjclsBLLVacationEntry.clsDTOVacationEntry.NeedVoucherEntry = true;

            if (SaveVacation() == true)
            {
                MbChangeStatus = false;
            }
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }
        private void dgvVacationDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgvVacationDetails.CurrentCell.OwningColumn.Name == "dgvColNoOfDays" || dgvVacationDetails.CurrentCell.OwningColumn.Name == "dgvColAmount")
            {
                TextBox txt = (TextBox)(e.Control);
                txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
            }
            else
            {
                dgvVacationDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgvVacationDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex > -1 && e.RowIndex > -1)
                {
                    if (e.ColumnIndex == dgvColParticulars.Index)
                    {
                        if (Convert.ToInt32(dgvVacationDetails.Rows[e.RowIndex].Cells[dgvColParticulars.Index].Value) == 15 || Convert.ToInt32(dgvVacationDetails.Rows[e.RowIndex].Cells[dgvColParticulars.Index].Value) == 20)
                        {
                            dgvVacationDetails.Rows[e.RowIndex].Cells[dgvColIsAddition.Index].Value = true;
                        }
                        else
                        {

                            dgvVacationDetails.Rows[e.RowIndex].Cells[dgvColIsAddition.Index].Value = MobjclsBLLVacationEntry.IsAddition(Convert.ToInt32(dgvVacationDetails.Rows[e.RowIndex].Cells[dgvColParticulars.Index].Value));
                        }
                    }
                    else if (e.ColumnIndex == dgvColAmount.Index)
                    {
                        CalculateNetAmount();
                    }
                    else if (e.ColumnIndex == dgvColReleaseLater.Index)
                    {
                        CalculateNetAmount();
                    }
                    ChangeStatus();
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on dgvVacationDetails_CellValueChanged " + this.Name + ex.Message.ToString(), 1);
            }
        }

        private void tmrVacation_Tick(object sender, EventArgs e)
        {
            lblstatus.Text = "";
            errorProviderVacation.Clear();
            tmrVacation.Enabled = false;
        }

        private void cboEmployee_KeyDown(object sender, KeyEventArgs e)
        {
            cboEmployee.DroppedDown = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bnSaveItem_Click(sender, e);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void cboTransactionType_SelectedIndexChanged(object sender, EventArgs e)
        {

            SetTransactionType();
            if (cboTransactionType.SelectedValue.ToInt32() > 0)
            {
                if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentTransactionType.Bank && cboEmployee.SelectedValue.ToInt32() > 0)
                {
                    LoadCombos(3);//Bank
                }
            }
            ChangeStatus();
        }

        private void cboAccount_KeyDown(object sender, KeyEventArgs e)
        {
            cboAccount.DroppedDown = false;
        }

        private void dgvVacationDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgvVacationDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dtpRejoinDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtChequeNo_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dtpChequeDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtRemarks_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void btnAdditionDeductions_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmAdditionDeduction(), true);
            LoadCombos(2);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == string.Empty)
                mblnSearchStatus = false;
            else
                mblnSearchStatus = true;

            if (this.txtSearch.Text.Trim() == string.Empty)
                return;

            RecCount(); // get record count

            if (RecordCnt == 0) // No records
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 40, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                tmrVacation.Enabled = true;
                txtSearch.Text = string.Empty;
                bnAddNewItem_Click(sender, e);
            }
            else if (RecordCnt > 0)
                BindingNavigatorMoveFirstItem_Click(sender, e); // if record found in search then automatically navigate to first item
        }

        private void dgvVacationDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                if (e.ColumnIndex > -1 && e.RowIndex > -1)
                {

                    if (Convert.ToInt32(dgvVacationDetails.Rows[e.RowIndex].Cells[dgvColParticulars.Index].Value) == 20 || Convert.ToInt32(dgvVacationDetails.Rows[e.RowIndex].Cells[dgvColParticulars.Index].Value) == 23)
                    {
                        e.Cancel = true;
                        return;
                    }
                    else if (e.ColumnIndex == dgvColNoOfDays.Index)
                    {
                        e.Cancel = true;
                        return;
                    }

                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on dgvVacationDetails_CellBeginEdit " + this.Name + ex.Message.ToString(), 1);
            }
        }

        private void dgvVacationDetails_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Delete && dgvVacationDetails.CurrentRow !=null)
                {
                    if (dgvVacationDetails.CurrentRow.Cells[dgvColParticulars.Index].Value.ToInt32() == 20 || dgvVacationDetails.CurrentRow.Cells[dgvColParticulars.Index].Value.ToInt32() == 23)
                    {
                        if (MobjclsBLLVacationEntry.clsDTOVacationEntry.VacationID > 0)
                        {
                            MobjclsBLLVacationEntry.DeletePaymentDetails();
                        }
                    }
                    MobjclsBLLVacationEntry.DeleteVacationDetailParticular(dgvVacationDetails.CurrentRow.Cells[dgvColParticulars.Index].Value.ToInt32());
                    MobjclsBLLVacationEntry.UpdateNetAmount(txtNetAmount.Text.ToDecimal(), txtGivenAmount.Text.ToDecimal());
                }
            }
            catch
            {
            }
        }

        private void dgvVacationDetails_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            CalculateNetAmount();
            ChangeStatus();
        }

        private void txtNoOfTicketsIssued_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtTicketAmount_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void FrmVacationEntry_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MbChangeStatus)
            {
                // Checking the changes are not saved and shows warning to the user
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 8, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void FrmVacationEntry_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                        objHelp.strFormName = "Vacation process";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    //case Keys.Control | Keys.S:
                    //    btnSave_Click(sender,new EventArgs());//save
                    //  break;
                    //case Keys.Control | Keys.O:
                    //    btnOk_Click(sender, new EventArgs());//OK
                    //    break; 
                    //case Keys.Control | Keys.L:
                    //    btnCancel_Click(sender, new EventArgs());//Cancel
                    //    break;
                    case Keys.Control | Keys.Enter:
                        bnAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        bnDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        btnCancel_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        bnPrint_Click(sender, new EventArgs());//Cancel
                        break;
                    case Keys.Control | Keys.M:
                        bnEmail_Click(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception)
            {
                mObjLogs.WriteLog("Error on FrmVacationEntry_KeyDown " + this.Name + " ", 1);
            }

        }
        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void bnSaveItem_Click(object sender, EventArgs e)
        {
            if (dgvVacationDetails.CurrentRow != null)
            {
                if (dgvVacationDetails.CurrentRow.Index >= 0 && dgvVacationDetails.CurrentCell.ColumnIndex == dgvColReleaseLater.Index)
                {

                    dgvVacationDetails.CurrentCell = dgvVacationDetails.Rows[dgvVacationDetails.CurrentRow.Index].Cells[dgvColParticulars.Index];
                    dgvVacationDetails.CurrentCell = dgvVacationDetails.Rows[dgvVacationDetails.CurrentRow.Index].Cells[dgvColReleaseLater.Index];
                }

            }

            //Sets voucher entry as false
            MobjclsBLLVacationEntry.clsDTOVacationEntry.NeedVoucherEntry = false;

            SaveVacation();
            MblnProcess = false;
        }
        private void bnDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 13, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }


                FillParameters();
                if (MobjclsBLLVacationEntry.clsDTOVacationEntry.VacationID > 0)
                {
                    if (MobjclsBLLVacationEntry.IsVacationAlreadyProcessed(dtpFromDate.Value.ToString("dd-MMM-yyyy"), dtpToDate.Value.ToString("dd-MMM-yyyy")) == true)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7556, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    if (MobjclsBLLVacationEntry.IsVacationProcessSalaryProcessed(dtpFromDate.Value.ToString("dd-MMM-yyyy"), dtpToDate.Value.ToString("dd-MMM-yyyy")) == true)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7584, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                }
                MobjclsBLLVacationEntry.DeletePaymentDetails();//To delete Payment Releated Details
                MobjclsBLLVacationEntry.DeleteVacationMaster();
                AddNew();
            }
            catch (System.Data.SqlClient.SqlException Ex)
            {
                string MstrMessageCommon = "";
                if (Ex.Number == (int)SqlErrorCodes.ForeignKeyErrorNumber)
                {
                //    if (ClsCommonSettings.IsArabicView)
                //        MstrMessageCommon = " التفاصيل موجودة في النظام. لا يمكن حذف";
                //    else
                        MstrMessageCommon = "Details exists in the system.Cannot delete";
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                    mObjLogs.WriteLog("Error on FrmVacationEntry_KeyDown " + this.Name + Ex.Message.ToString(), 1);
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on FrmVacationEntry_KeyDown " + this.Name + ex.Message.ToString(), 1);
            }
        }

        private void bnClearItem_Click(object sender, EventArgs e)
        {
            ClearSearchControls();
            ClearControls();
        }

        private void bnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "EmployeeVacation";
                    ObjEmailPopUp.EmailFormType = EmailFormID.VacationProcess;
                    ObjEmailPopUp.EmailSource = MobjclsBLLVacationEntry.EmailDetail();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch { }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            try
            {
                FrmHelp objHelp = new FrmHelp();
                objHelp.strFormName = "Vacation process";
                objHelp.ShowDialog();
                objHelp = null;
            }
            catch { }

        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnSearch_Click(null, null);
                }
            }
            catch { }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            mblnSearchStatus = false;
        }
        #endregion Events
        #region Methods
        #region Record Count Navigate
        /// <summary>
        /// To set navigation of Records
        /// </summary>
        private void RecCountNavigate()
        {

            RecordCnt = 0;
            RecordCnt = MobjclsBLLVacationEntry.GetRecCount();
            if (RecordCnt > 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
                bnPositionItem.Text = Convert.ToString(RecordCnt);
            }
            else
            {
                CurrentRecCnt = 0;
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                RecordCnt = 0;
            }

        }
        #endregion Record Count Navigate
        #region RecCount
        /// <summary>
        ///  To set navigation of Records
        /// </summary>
        private void RecCount()
        {
            MobjclsBLLVacationEntry.clsDTOVacationEntry.strSearchKey = txtSearch.Text.Trim(); // Is any search text filteration will be taken that also
            RecordCnt = MobjclsBLLVacationEntry.GetRecCount();

            if (RecordCnt > 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
                bnPositionItem.Text = Convert.ToString(RecordCnt);
                CurrentRecCnt = RecordCnt;
            }
            else
            {
                CurrentRecCnt = 0;
                BindingNavigatorCountItem.Text = strBindingOf + "0";
            }

            BindingNavigatorMoveNextItem.Enabled = false;
            BindingNavigatorMoveLastItem.Enabled = false;
            if (RecordCnt >= 1)
            {
                BindingNavigatorMovePreviousItem.Enabled = true;
                BindingNavigatorMoveFirstItem.Enabled = true;
            }
            else
            {
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
            }
        }
        #endregion RecCount
        #region DisplayVacation
        /// <summary>
        /// To Display Vacation process
        /// </summary>
        /// <param name="RowNum"></param>
        /// <returns>bool</returns>
        private bool DisplayVacation(int RowNum)
        {
            tabVacationProcess.SelectedTab = tabParticulars;
            bool BlnRetValue = false;
            MbAddStatus = false;
            blnAddvalue = true;
            btnAdd.Enabled = true;
            MobjclsBLLVacationEntry.clsDTOVacationEntry.strSearchKey = txtSearch.Text.Trim();
            DataSet DtVacation;

            if (MblnSearch == true)
            {
                int iSearchTotalCount = 0;
                string sSearchText = txtSearch.Text;
                if (BlnRetValue == true)
                {
                    LoadCombos(0);
                    ClearControls();
                }
                if (iSearchTotalCount > 0 && RowNum == 1)
                {
                    RecordCnt = iSearchTotalCount;
                    BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
                    bnPositionItem.Text = Convert.ToString(CurrentRecCnt);
                    setBindingNavigatorbuttons();
                }
            }
            else
            {
                MobjclsBLLVacationEntry.clsDTOVacationEntry.RowNum = RowNum;
                BlnRetValue = MobjclsBLLVacationEntry.GetVacationMaster(RowNum);
            }


            if (BlnRetValue == true)
            {

                NavigateFlg = 1;
                DtVacation = MobjclsBLLVacationEntry.GetVacationDetails();
                LoadCombos(1);
                lblActualRejoinDate.Text = "";
                cboEmployee.SelectedValue = MobjclsBLLVacationEntry.clsDTOVacationEntry.EmployeeID;
                dtpFromDate.Value = Convert.ToDateTime(MobjclsBLLVacationEntry.clsDTOVacationEntry.FromDate);
                dtpToDate.Value = Convert.ToDateTime(MobjclsBLLVacationEntry.clsDTOVacationEntry.ToDate);

                chkProcessSalaryForCurrentMonth.Checked = MobjclsBLLVacationEntry.clsDTOVacationEntry.IsProcessSalaryForCurrentMonth;
                txtNoOfTicketsIssued.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.IssuedTickets.ToString();
                txtTicketAmount.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.TicketExpense.ToString();
                txtNetAmount.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.NetAmount.ToString();
                txtGivenAmount.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.GivenAmount.ToString();
                dtpRejoinDate.Value = Convert.ToDateTime(MobjclsBLLVacationEntry.clsDTOVacationEntry.RejoinDate);

                cboAccount.SelectedValue = MobjclsBLLVacationEntry.clsDTOVacationEntry.AccountID;
                cboTransactionType.SelectedValue = MobjclsBLLVacationEntry.clsDTOVacationEntry.TransactionTypeID;
                if (MobjclsBLLVacationEntry.clsDTOVacationEntry.ChequeNumber != null)
                {
                    txtChequeNo.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.ChequeNumber.ToString();
                }
                if (Microsoft.VisualBasic.Information.IsDate(MobjclsBLLVacationEntry.clsDTOVacationEntry.ChequeDate) == true)
                {
                    if (Microsoft.VisualBasic.DateAndTime.DateValue(MobjclsBLLVacationEntry.clsDTOVacationEntry.ChequeDate.ToString()) > Convert.ToDateTime("01-Jan-1900"))
                    {
                        dtpChequeDate.Value = MobjclsBLLVacationEntry.clsDTOVacationEntry.ChequeDate;
                    }
                }

                if (MobjclsBLLVacationEntry.clsDTOVacationEntry.OvertakenLeaves > 0)
                {
                    if (Convert.ToBoolean(MobjclsBLLVacationEntry.clsDTOVacationEntry.PaidLeave) == true)
                    {
                        //if (ClsCommonSettings.IsArabicView)
                        //    lblIsPaid.Text = "مدفوع";
                        //else
                            lblIsPaid.Text = "Paid";
                    }
                    else
                    {
                        //if (ClsCommonSettings.IsArabicView)
                        //    lblIsPaid.Text = "غير مدفوع";
                        //else
                            lblIsPaid.Text = "Unpaid";
                    }
                }
                else
                {
                    lblIsPaid.Text = "";
                }

                if (MobjclsBLLVacationEntry.clsDTOVacationEntry.OvertakenLeaves < 0)
                {
                    lblAdditionalLeavesText.Text = Math.Abs(MobjclsBLLVacationEntry.clsDTOVacationEntry.OvertakenLeaves).ToString();
                    //if (ClsCommonSettings.IsArabicView)
                    //    lblAdditionLeaves.Text = "يوم تقصير";
                    //else
                        lblAdditionLeaves.Text = "Shortened Days";
                }
                else
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    lblAdditionLeaves.Text = "تمديد أيام";
                    //else
                        lblAdditionLeaves.Text = "Extended Days";
                    lblAdditionalLeavesText.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.OvertakenLeaves.ToString();
                }

                if (MobjclsBLLVacationEntry.clsDTOVacationEntry.ActualDateTime != "01-Jan-1900".ToDateTime())
                {
                    lblActualRejoinDate.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.ActualDateTime.ToString("dd-MMM-yyyy");
                }
                else
                {
                    lblActualRejoinDate.Text = "";
                }
                chkConsiderAbsentDays.Checked = MobjclsBLLVacationEntry.clsDTOVacationEntry.IsConsiderAbsentDays;
                lblAbsentDaysText.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.AbsentDays.ToString();
                lblExperienceDaysText.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.ExperienceDays.ToString();
                txtEligibleLeavePayDays.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.EligibleLeavePayDays.ToString();
                txtEncashComboOffDay.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.EncashComboOffDay.ToDecimal().ToString();  
                chkEncashOnly.Checked = MobjclsBLLVacationEntry.clsDTOVacationEntry.IsEncashOnly;
                dgvVacationDetails.Rows.Clear();
                NavigateFlg = 0;
                int intRowCount = 0;
                if (DtVacation.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in DtVacation.Tables[0].Rows)
                    {
                        dgvVacationDetails.RowCount = dgvVacationDetails.RowCount + 1;
                        intRowCount = dgvVacationDetails.RowCount - 1;
                        dgvVacationDetails.Rows[intRowCount].Cells[dgvColParticulars.Index].Value = Convert.ToInt32(dr["AdditionDeductionID"]);
                        dgvVacationDetails.Rows[intRowCount].Cells[dgvColAmount.Index].Value = dr["Amount"];
                        dgvVacationDetails.Rows[intRowCount].Cells[dgvColTempAmount.Index].Value = dr["Amount"];
                        dgvVacationDetails.Rows[intRowCount].Cells[dgvColNoOfDays.Index].Value = dr["NoOfDays"];
                        dgvVacationDetails.Rows[intRowCount].Cells[dgvColReleaseLater.Index].Value = dr["IsReleaseLater"];


                        //if (ClsCommonSettings.IsAmountRoundByZero)
                        //{
                        //    dgvVacationDetails.Rows[intRowCount].Cells[dgvColAmount.Index].Value = dgvVacationDetails.Rows[intRowCount].Cells[dgvColAmount.Index].Value.ToDecimal().ToString("F" + 0);
                        //    dgvVacationDetails.Rows[intRowCount].Cells[dgvColTempAmount.Index].Value = dgvVacationDetails.Rows[intRowCount].Cells[dgvColTempAmount.Index].Value.ToDecimal().ToString("F" + 0);
                        //}


                    }
                }

                txtRemarks.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.Remarks.ToString();
                //MblnProcess = true;
                btnProcess.Enabled = false;
                cboEmployee.Enabled = false;
                this.bnAddNewItem.Enabled = MblnAddPermission;
                bnPrint.Enabled = MblnPrintEmailPermission;
                bnEmail.Enabled = MblnPrintEmailPermission;

                MbChangeStatus = false;
                btnOK.Enabled = MblnUpdatePermission;
                btnSave.Enabled = MbChangeStatus;
                this.bnSaveItem.Enabled = MbChangeStatus;


                bnClearItem.Enabled = false;

            }

            this.FillDetail();


            if (MobjclsBLLVacationEntry.clsDTOVacationEntry.IsClosed == true)
            {
                ((Control)this.tabEmployeeInfo).Enabled = false;
                ((Control)this.tabParticulars).Enabled = false;
                ((Control)this.tabHistory).Enabled = false;
                bnDeleteItem.Enabled = false;
                btnOK.Enabled = false;

                //if (ClsCommonSettings.IsHrPowerEnabled)
                //{
                //    bnVacationExtension.Visible = false;
                //}
                //else
                //{
                //    bnVacationExtension.Visible = true;
                //    bnVacationExtension.Enabled = true;
                //}


                bnVacationExtension.Enabled = MblnExtenstionPermission;
                bnSaveItem.Enabled = false;
                btnSave.Enabled = false;

            }
            else
            {
                ((Control)this.tabEmployeeInfo).Enabled = true;
                ((Control)this.tabParticulars).Enabled = true;
                ((Control)this.tabHistory).Enabled = true;
                this.bnDeleteItem.Enabled = MblnDeletePermission;
                this.bnVacationExtension.Enabled = false;
                btnProcess.Enabled = btnSave.Enabled = bnSaveItem.Enabled = false;
            }
            tabVacationProcess.SelectedTab = tabEmployeeInfo;

            return BlnRetValue;
        }
        #endregion DisplayVacation
        #region LoadCombos
        /// <summary>
        /// Load the combos-Employee,AdditionDeductionReference,TransactionTypeReference,
        /// </summary>
        /// <param name="intType">
        /// intType=0->Load all combos
        /// intType=1->Loads Employee combo only
        /// intType=2->Loads AdditionDeductionReference combo only
        /// intType=2->Loads TransactionTypeReference combo only
        /// </param>
        /// <returns>success/failure</returns>
        private void LoadCombos(int intType)
        {
            DataTable datCombos = new DataTable();

            if (intType == 0 || intType == 1)
            {
                if (MbAddStatus == true)
                {
                    cboEmployee.Text = "";
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    if (!MblnIsHrPowerEnabled)
                    //        datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "DISTINCT EM.EmployeeID,ISNULL(EM.FirstNameArb,'') + '' + ISNULL(EM.LastNameArb,'') + ' - ' +  ISNULL(CAST(EM.EmployeeNumber AS VARCHAR(30)),'') AS Name ", "EmployeeMaster EM inner join PaySalaryStructure ESH on EM.EmployeeID=ESH.EmployeeID ", " EM.CompanyID IN(select CompanyId from dbo.UserCompanyDetails where UserID="+ ClsCommonSettings.UserID +")  and EM.WorkStatusID >= 6 And ISNULL(EM.VacationPolicyID,0) <> 0 ORDER BY Name", "EmployeeID", "Name" });
                    //    else
                    //        datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "DISTINCT EM.EmployeeID,ISNULL(EM.FirstNameArb,'') + '' + ISNULL(EM.LastNameArb,'') + ' - ' +  ISNULL(CAST(EM.EmployeeNumber AS VARCHAR(30)),'') AS Name ", "EmployeeMaster EM inner join PaySalaryStructure ESH on EM.EmployeeID=ESH.EmployeeID INNER JOIN HRLeaveRequest LR ON  EM.EmployeeID=LR.EmployeeID AND LR.StatusId in (5,7) AND LR.IsVacation=1 AND LR.VacationID IS NULL AND LR.ApprovedBy IS NOT NULL", " EM.CompanyID IN(select CompanyId from dbo.UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and EM.WorkStatusID >= 6 And ISNULL(EM.VacationPolicyID,0) <> 0 ORDER BY Name", "EmployeeID", "Name" });
                    //}
                    //else
                    //{
                        //if (!MblnIsHrPowerEnabled)
                            datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "DISTINCT EM.EmployeeID,ISNULL(EM.FirstName,'') + '' + ISNULL(EM.LastName,'') + ' - ' +  ISNULL(CAST(EM.EmployeeNumber AS VARCHAR(30)),'') AS Name ", "EmployeeMaster EM inner join PaySalaryStructure ESH on EM.EmployeeID=ESH.EmployeeID ", " EM.CompanyID IN(select CompanyID from dbo.UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and  EM.WorkStatusID >= 6 And ISNULL(EM.VacationPolicyID,0) <> 0 and EM.CompanyID ="+ ClsCommonSettings.LoginCompanyID +" ORDER BY Name", "EmployeeID", "Name" });
                    //    else
                    //        datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "DISTINCT EM.EmployeeID,ISNULL(EM.FirstName,'') + '' + ISNULL(EM.LastName,'') + ' - ' +  ISNULL(CAST(EM.EmployeeNumber AS VARCHAR(30)),'') AS Name ", "EmployeeMaster EM inner join PaySalaryStructure ESH on EM.EmployeeID=ESH.EmployeeID INNER JOIN HRLeaveRequest LR ON  EM.EmployeeID=LR.EmployeeID AND LR.StatusId in (5,7) AND LR.IsVacation=1 AND LR.VacationID IS NULL AND LR.ApprovedBy IS NOT NULL", " EM.CompanyID IN(select CompanyId from dbo.UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and EM.WorkStatusID >= 6 And ISNULL(EM.VacationPolicyID,0) <> 0 ORDER BY Name", "EmployeeID", "Name" });
                    ////}
                    cboEmployee.ValueMember = "EmployeeID";
                    cboEmployee.DisplayMember = "Name";
                    cboEmployee.DataSource = datCombos;
                }
                else
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "DISTINCT EmployeeID,ISNULL(FirstNameArb,'') + ' ' + ISNULL(LastNameArb,'') + ' - ' +  ISNULL(CAST(EmployeeNumber AS VARCHAR(30)),'') AS Name ", "EmployeeMaster", "", "EmployeeID", "Name" });
                    //}
                    //else
                    //{
                    datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "DISTINCT EmployeeID,ISNULL(FirstName,'') + ' ' + ISNULL(LastName,'') + ' - ' +  ISNULL(CAST(EmployeeNumber AS VARCHAR(30)),'') AS Name ", "EmployeeMaster", "CompanyID =" + ClsCommonSettings.LoginCompanyID + " ", "EmployeeID", "Name" });
                    //}
                    cboEmployee.ValueMember = "EmployeeID";
                    cboEmployee.DisplayMember = "Name";
                    cboEmployee.DataSource = datCombos;
                }
            }

            if (intType == 0 || intType == 2)
            {
                datCombos = null;
                //if (ClsCommonSettings.IsArabicView)
                //    datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "AdditionDeductionID, AdditionDeductionArb + (CASE WHEN IsAddition = 1 THEN ' (+)' ELSE ' (-)' END) AS AdditionDeduction ", "PayAdditionDeductionReference", "(IsPredefined = 0) AND (AdditionDeductionID <> 1)", "AdditionDeductionID", "AdditionDeduction" });
                //else
                    datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "AdditionDeductionID, AdditionDeduction + (CASE WHEN IsAddition = 1 THEN ' (+)' ELSE ' (-)' END) AS AdditionDeduction ", "PayAdditionDeductionReference", "(IsPredefined = 0) AND (AdditionDeductionID <> 1)", "AdditionDeductionID", "AdditionDeduction" });
                cboParticulars.ValueMember = "AdditionDeductionID";
                cboParticulars.DisplayMember = "AdditionDeduction";
                cboParticulars.DataSource = datCombos;
            }

            if (intType == 0 || intType == 2)
            {
                datCombos = null;
                //if (ClsCommonSettings.IsArabicView)
                //    datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "AdditionDeductionID, AdditionDeductionArb  + (CASE WHEN IsAddition = 1 Or AdditionDeductionID In (15,20) THEN ' (+)' ELSE ' (-)' END) AS AdditionDeduction", "PayAdditionDeductionReference", " (AdditionDeductionID <> 1)", "AdditionDeductionID", "AdditionDeduction" });
                //else
                    datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "AdditionDeductionID, AdditionDeduction  + (CASE WHEN IsAddition = 1 Or AdditionDeductionID In (15,20) THEN ' (+)' ELSE ' (-)' END) AS AdditionDeduction", "PayAdditionDeductionReference", " (AdditionDeductionID <> 1)", "AdditionDeductionID", "AdditionDeduction" });
                dgvColParticulars.ValueMember = "AdditionDeductionID";
                dgvColParticulars.DisplayMember = "AdditionDeduction";
                dgvColParticulars.DataSource = datCombos;
            }


            if (intType == 0 || intType == 3)//Transaction type
            {
                //if (ClsCommonSettings.IsArabicView)
                //    datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "TransactionTypeID,TransactionTypeArb AS TransactionType", "TransactionTypeReference", "TransactionTypeID NOT IN(" + (int)PaymentTransactionType.Wps + ")" });
                //else
                    datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "TransactionTypeID,TransactionType", "TransactionTypeReference", "TransactionTypeID  IN(1,2)" });
                cboTransactionType.ValueMember = "TransactionTypeID";
                cboTransactionType.DisplayMember = "TransactionType";
                cboTransactionType.DataSource = datCombos;
                datCombos = null;
            }
            if (intType == 0 || intType == 4)//Accounts
            {
                int iEmpID = cboEmployee.SelectedValue.ToInt32();
                if (iEmpID != 0)
                {
                    datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "AM.AccountID,AM.AccountName", " AccAccountMaster  AM  INNER JOIN BankBranchReference BBR ON BBR.AccountID=AM.AccountID  INNER JOIN CompanyBankAccountDetails CBA ON CBA.BankBranchID=BBR.BankBranchID  INNER  JOIN EmployeeMaster EM ON EM.CompanyID=CBA.CompanyID  AND EM.EmployeeID=" + iEmpID.ToStringCustom(), "" });
                }
                else
                {
                    datCombos = MobjclsBLLVacationEntry.FillCombos(new string[] { "AM.AccountID,AM.AccountName", " AccAccountMaster  AM  INNER JOIN BankBranchReference BBR ON BBR.AccountID=AM.AccountID  INNER JOIN CompanyBankAccountDetails CBA ON CBA.BankBranchID=BBR.BankBranchID  INNER  JOIN EmployeeMaster EM ON EM.CompanyID=CBA.CompanyID", "" });
                }
                cboAccount.ValueMember = "AccountID";
                cboAccount.DisplayMember = "AccountName";
                cboAccount.DataSource = datCombos;
                datCombos = null;
            }

        }
        #endregion LoadCombos
        #region SetPermissions
        /// <summary>
        /// To set Permissions
        /// </summary>
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID , (Int32)eModuleID.Payroll, (Int32)eMenuID.VacationProcess, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                DataTable datTemp = MobjclsBLLVacationEntry.FillCombos(new string[] { "IsView", "RoleDetails", "RoleID = " + ClsCommonSettings.RoleID +" AND MenuID = "+(int)eMenuID.VacationProcess });
                if (datTemp != null && datTemp.Rows.Count > 0)
                    MblnExtenstionPermission = Convert.ToBoolean(datTemp.Rows[0]["IsView"]);
                else
                    MblnExtenstionPermission = false;
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission =MblnExtenstionPermission= true;
        }
        #endregion SetPermissions
        #region LoadMessage
        /// <summary>
        /// Method to fill the message array according to form
        /// </summary>
        private void LoadMessage()
        {
            // Loading Message
            try
            {
                MsarMessageArr = new ArrayList();
                MsarMessageArr = mObjNotification.FillMessageArray((int)FormID.VacationEntry, ClsCommonSettings.ProductID);
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on LoadMessage " + this.Name + " " + Ex.Message.ToString(), 1);
            }

        }
        #endregion LoadMessage
        #region ClearSearchControls
        /// <summary>
        /// To clear search controls
        /// </summary> 
        private void ClearSearchControls()
        {
            txtSearch.Text = "";
            //cboSearchType.SelectedIndex = -1;
            MblnSearch = false;
        }
        #endregion ClearSearchControls
        #region setBindingNavigatorbuttons
        /// <summary>
        /// Setting binding navigator buttons Enability
        /// </summary>
        private void setBindingNavigatorbuttons()
        {
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            if (CurrentRecCnt == RecordCnt)
            {
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }

            if (CurrentRecCnt == 1)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }
        #endregion setBindingNavigatorbuttons
        #region ClearControls
        /// <summary>
        /// To Clear Controls used in form
        /// </summary>
        private void ClearControls()
        {
            cboEmployee.SelectedIndex = -1;
            mblnIsEmployeeFixedCategory = false;
            lblNoOfDays.Text = "1";
            dtpFromDate.Value = System.DateTime.Now.Date;
            dtpToDate.Value = System.DateTime.Now.Date;
            dtpRejoinDate.Value = dtpToDate.Value.AddDays(1);
            dtpProcessDate.Value = System.DateTime.Now.Date;
            dtpJoiningDate.Value = System.DateTime.Now.Date;
            chkProcessSalaryForCurrentMonth.Checked = true;
            txtAmount.Text = "";
            cboParticulars.SelectedIndex = -1;
            txtNoOfTicketsIssued.Text = "";
            txtTotalAddition.Text = "";
            txtTotalDeduction.Text = "";
            txtNetAmount.Text = "";
            txtGivenAmount.Text = "";
            txtRemarks.Text = "";
            lblEligibleLeavesText.Text = "0";
            txtTicketAmount.Text = "";
            lblExperienceDaysText.Text = "0";
            lblAbsentDaysText.Text = "0";
            lblTotalEligibleLeavePayDaysText.Text = "0";
            txtEligibleLeavePayDays.Text = "1";
            chkConsiderAbsentDays.Checked = false;
            lblAdditionalLeavesText.Text = "0";
            lblActualRejoinDate.Text = "";
            //if (ClsCommonSettings.IsArabicView)
            //    lblAdditionLeaves.Text = "تمديد أيام";
            //else
                lblAdditionLeaves.Text = "Extended Days";

            lblCurrency.Text = "";
            ((Control)this.tabEmployeeInfo).Enabled = true;
            ((Control)this.tabParticulars).Enabled = true;
            ((Control)this.tabHistory).Enabled = true;
            chkEncashOnly.Checked = false;
            dgvVacationDetails.Rows.Clear();
            cboAccount.SelectedIndex = -1;
            cboTransactionType.SelectedIndex = -1;
            txtChequeNo.Text = "";
            dtpChequeDate.Value = System.DateTime.Now.Date;

            //if (ClsCommonSettings.IsHrPowerEnabled)
            //{
            //    bnVacationExtension.Visible = false;  

            //}


        }
        #endregion ClearControls
        #region ChangeStatus
        /// <summary>
        /// To set Enablity of buttons
        /// </summary>
        private void ChangeStatus()
        {
            if (MbAddStatus == true)
            {
                MbChangeStatus = MblnAddPermission;
            }
            else
            {
                MbChangeStatus = MblnUpdatePermission;
            }

            btnOK.Enabled = MbChangeStatus;
            btnSave.Enabled = MbChangeStatus;
            bnSaveItem.Enabled = MbChangeStatus;
            btnProcess.Enabled = MbChangeStatus;
        }
        #endregion ChangeStatus
        #region FillProcessDetails
        /// <summary>
        /// To Fill Vacation Process Details
        /// </summary>
        private void FillProcessDetails()
        {
            int i = 0;

            DataTable dtVacation;
            dtVacation = MobjclsBLLVacationEntry.FillProcessDetails();

            if (dtVacation != null && dtVacation.Rows.Count > 0)
            {
                MobjclsBLLVacationEntry.clsDTOVacationEntry.RePaymentID = dtVacation.Rows[0]["PaymentID"].ToInt32();
            }
            dgvVacationDetails.Rows.Clear();
            foreach (DataRow dr in dtVacation.Rows)
            {
                if (dr["NoOfTickets"] != System.DBNull.Value && Convert.ToInt32(dr["NoOfTickets"]) != 0)
                {
                    txtNoOfTicketsIssued.Text = Convert.ToString(dr["NoOfTickets"]);
                }
                else
                {

                    if (Convert.ToDecimal(dr["Amount"]) > 0 && Convert.ToInt32(dr["AdditionDeductionID"]) != 20)
                    {
                        dgvVacationDetails.RowCount = dgvVacationDetails.RowCount + 1;
                        i = dgvVacationDetails.RowCount - 1;

                        dgvVacationDetails.Rows[i].Cells[dgvColParticulars.Index].Value = Convert.ToInt32(dr["AdditionDeductionID"]);
                        dgvVacationDetails.Rows[i].Cells[dgvColAmount.Index].Value = Convert.ToDecimal(dr["Amount"]);
                        dgvVacationDetails.Rows[i].Cells[dgvColNoOfDays.Index].Value = Convert.ToDouble(dr["NoOfDays"]);
                        i = i + 1;
                    }
                    else if (Convert.ToInt32(dr["AdditionDeductionID"]) == 20 && Convert.ToDecimal(dr["Amount"]) != 0)
                    {
                        dgvVacationDetails.RowCount = dgvVacationDetails.RowCount + 1;
                        i = dgvVacationDetails.RowCount - 1;

                        dgvVacationDetails.Rows[i].Cells[dgvColParticulars.Index].Value = Convert.ToInt32(dr["AdditionDeductionID"]);
                        dgvVacationDetails.Rows[i].Cells[dgvColAmount.Index].Value = Convert.ToDecimal(dr["Amount"]);
                        dgvVacationDetails.Rows[i].Cells[dgvColNoOfDays.Index].Value = Convert.ToDouble(dr["NoOfDays"]);
                        i = i + 1;
                    }
                }
            }
        }
        #endregion FillProcessDetails
        #region FormValidation
        /// <summary>
        /// Vacation Process Validations
        /// </summary>
        /// <returns>bool</returns>
        private bool FormValidation()
        {
            errorProviderVacation.Clear();

            FillParameters();
            if (cboEmployee.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7574, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                errorProviderVacation.SetError(cboEmployee, MstrMessageCommon.Replace("#", "").Trim());

                //if (ClsCommonSettings.IsArabicView)
                //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7574, out MmessageIcon).ToString().Replace("*", "معلومات اجازة");
                //else
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7574, out MmessageIcon).ToString().Replace("*", "Vacation Detail");
                lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 7574, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);

                tmrVacation.Enabled = true;
                cboEmployee.Focus();
                return false;
            }

            if (dtpProcessDate.Value.Date < Convert.ToDateTime(MobjclsBLLVacationEntry.GetDateofJoining()))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3081, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                errorProviderVacation.SetError(dtpProcessDate, MstrMessageCommon.Replace("#", "").Trim());
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                tmrVacation.Enabled = true;
                dtpProcessDate.Focus();
                return false;
            }

            if (MobjclsBLLVacationEntry.IsSalaryIsProcessedOrClosed() == true)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7564, out MmessageIcon); // 7556);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                tmrVacation.Enabled = true;
                return false;
            }

            if (Convert.ToString(txtEligibleLeavePayDays.Text).Trim() == "")
            {
                txtEligibleLeavePayDays.Text = "0";
            }


            if (cboTransactionType.SelectedValue.ToInt32() <= 0)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9116, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                errorProviderVacation.SetError(cboTransactionType, MstrMessageCommon.Replace("#", "").Trim());
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                tmrVacation.Enabled = true;
                cboTransactionType.Focus();
                return false;
            }

            if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentTransactionType.Bank)
            {
                if (txtChequeNo.Text.ToStringCustom() == string.Empty)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3088, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    errorProviderVacation.SetError(txtChequeNo, MstrMessageCommon.Replace("#", "").Trim());
                    lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                    tmrVacation.Enabled = true;
                    txtChequeNo.Focus();
                    return false;
                }
            }

            if ((mblnIsEmployeeFixedCategory == false && Convert.ToDouble(txtEligibleLeavePayDays.Text) > Convert.ToDouble(lblTotalEligibleLeavePayDaysText.Text)))
            {

                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7578, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                    tmrVacation.Enabled = true;
                    return false; ;
                }
            }

            //'check No of Leaves available in case of vacation



            if (MobjclsBLLVacationEntry.IsSalVacationAccountExists(cboEmployee.SelectedValue.ToInt32()) == false)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7583, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                StatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                tmrVacation.Enabled = true;
                return false;
            }





            foreach (DataGridViewRow dr in dgvVacationDetails.Rows)
            {
                if (Convert.ToString(dr.Cells[dgvColAmount.Index].Value).Trim() == "" || Convert.ToString(dr.Cells[dgvColAmount.Index].Value) == ".")
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7567, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 7567, out MmessageIcon);
                    tmrVacation.Enabled = true;
                    dgvVacationDetails.Focus();
                    return false;
                }
                else if (Convert.ToDecimal(dr.Cells[dgvColAmount.Index].Value) == 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7567, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information); // 7567, true), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 7567, out MmessageIcon); // 7567, false);
                    tmrVacation.Enabled = true;
                    dgvVacationDetails.Focus();
                    //dgvVacationDetails.CurrentCell = dgvVacationDetails(dgvColAmount.Index, dr.Index);
                    return false;
                }
                if (Convert.ToString(dr.Cells[dgvColNoOfDays.Index].Value).Trim() == ".")
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7572, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information); // 7572, true), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 7572, out MmessageIcon); // 7572, false);
                    tmrVacation.Enabled = true;
                    dgvVacationDetails.Focus();
                    //dgvVacationDetails.CurrentCell = dgvVacationDetails(dgvColNoOfDays.Index, dr.Index);
                    return false;
                }
            }
            if ((MobjclsBLLVacationEntry.CheckLeaveExists()))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7585, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                
                //if (ClsCommonSettings.IsArabicView)
                //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7585, out MmessageIcon).ToString().Replace("*", "معلومات اجازة");
                //else
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7585, out MmessageIcon).ToString().Replace("*", "Vacation Detail");
                lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 7585, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrVacation.Enabled = true;
                dtpFromDate.Focus();
                return false;
            }
            // Check Employee Salary is Processed between this period
            if (MobjclsBLLVacationEntry.CheckEmployeeSalaryIsProcessed(cboEmployee.SelectedValue.ToInt64(),
                (dtpFromDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime()) > 0)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7586, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                
                //if (ClsCommonSettings.IsArabicView)
                //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7586, out MmessageIcon).ToString().Replace("*", "معلومات اجازة");
                //else
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7586, out MmessageIcon).ToString().Replace("*", "Vacation Detail");

                lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 7586, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);

                tmrVacation.Enabled = true;
                dtpFromDate.Focus();
                return false;
            }
            // Check Employee vacation is Processed after this period
            if (MobjclsBLLVacationEntry.CheckEmployeeVacationProcessedAfterDate())
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7587, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                //if (ClsCommonSettings.IsArabicView)
                //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7587, out MmessageIcon).ToString().Replace("*", "معلومات اجازة");
                //else
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7587, out MmessageIcon).ToString().Replace("*", "Vacation Detail");

                lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 7587, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);

                tmrVacation.Enabled = true;
                dtpFromDate.Focus();
                return false;
            }

            return true;
        }
        #endregion FormValidation
        #region AddNew
        /// <summary>
        /// To Add new Vacation Process
        /// </summary>
        private void AddNew()
        {
            ClearSearchControls();
            bnClearItem.Enabled = true;
            mblnSearchStatus = false;

            MblnProcess = false;
            btnProcess.Enabled = true;
            mblnIsFromProcess = false;
            MbAddStatus = true;
            LoadCombos(0);
            cboEmployee.Enabled = true;
            bnVacationExtension.Enabled = false;
            lblIsPaid.Text = "";
            ((Control)this.tabEmployeeInfo).Enabled = true;
            ((Control)this.tabParticulars).Enabled = true;
            ((Control)this.tabHistory).Enabled = true;
            MobjclsBLLVacationEntry.clsDTOVacationEntry.VacationID = 0;
            ClearControls();
            MobjclsBLLVacationEntry = new clsBLLVacationEntry();
            RecCount();
            RecordCnt = RecordCnt + 1;
            CurrentRecCnt = CurrentRecCnt + 1;

            if (CurrentRecCnt == 1)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
            }
            else
            {
                BindingNavigatorMoveFirstItem.Enabled = true;
                BindingNavigatorMovePreviousItem.Enabled = true;
            }

            if (CurrentRecCnt == RecordCnt)
            {
                BindingNavigatorMoveLastItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;

            }
            else
            {
                BindingNavigatorMoveLastItem.Enabled = true;
                BindingNavigatorMoveNextItem.Enabled = true;
            }

            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
            bnPositionItem.Text = Convert.ToString(RecordCnt);
            MbChangeStatus = false;
            bnAddNewItem.Enabled = false;
            btnOK.Enabled = MbChangeStatus;
            btnSave.Enabled = MbChangeStatus;
            this.bnSaveItem.Enabled = MbChangeStatus;
            this.bnDeleteItem.Enabled = false;
            bnPrint.Enabled = false;
            bnEmail.Enabled = false;
            tabVacationProcess.SelectedTab = tabEmployeeInfo;

            //if (ClsCommonSettings.IsArabicView)
            //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 655, out MmessageIcon).ToString().Replace("*", "معلومات اجازة");
            //else
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 655, out MmessageIcon).ToString().Replace("*", "Vacation Detail");

            lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 655, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            tmrVacation.Enabled = true;
            cboEmployee.Focus();
            MobjclsBLLVacationEntry.clsDTOVacationEntry.IsClosed = false;
            dgvEmpHistory.DataSource = null;
            btnAdd.Enabled = false;

            SetAutoCompleteList();
        }
        #endregion AddNew
        #region SetAutoCompleteList
        /// <summary>
        /// To Get AutoComplete list for Search
        /// </summary>
        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsUtilities.GetAutoCompleteList(txtSearch.Text.Trim(), 0, "", "PayVacationMaster");
            this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);
        }
        #endregion SetAutoCompleteList
        #region SaveVacation
        /// <summary>
        /// Save Vacation Process
        /// </summary>
        /// <returns></returns>
        private bool SaveVacation()
        {
            if (MblnProcess == false && MobjclsBLLVacationEntry.clsDTOVacationEntry.IsClosed == false && MbAddStatus == true)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7554, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (mblnIsFromProcess == false)
            {
                if (FormValidation() == false)
                {
                    return false;
                }

                if (MbAddStatus == true)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 1, out MmessageIcon);
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        return false;
                    }
                }

                else
                {
                    if (MobjclsBLLVacationEntry.clsDTOVacationEntry.IsClosed == false)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3, out MmessageIcon);
                        if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return false;
                        }
                    }
                }
            }

            if (MobjclsBLLVacationEntry.SaveVacationMaster() == true)
            {
                if (MbAddStatus == true)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon); 
                    //if (ClsCommonSettings.IsArabicView)
                    //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon).ToString().Replace("*", "معلومات اجازة");
                    //else
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon).ToString().Replace("*", "Vacation Detail");
                    lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                }
                else
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 21, out MmessageIcon);

                    //if (ClsCommonSettings.IsArabicView)
                    //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 21, out MmessageIcon).ToString().Replace("*", "معلومات اجازة");
                    //else
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 21, out MmessageIcon).ToString().Replace("*", "Vacation Detail");
                    lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 21, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                }

                DisplayVacation(CurrentRecCnt);
                tmrVacation.Enabled = true;
                //lblstatus.Text = "";

                MbAddStatus = false;
            }
            return true;
        }
        #endregion SaveVacation
        #region CalculateNetAmount
        /// <summary>
        /// To calculate Net Amount
        /// </summary>
        private void CalculateNetAmount()
        {
            txtTotalAddition.Text = "";
            txtTotalDeduction.Text = "";
            txtNetAmount.Text = "";
            txtGivenAmount.Text = "";

            string SAddDed;

            decimal dAddition = 0.0m; decimal dDeduction = 0.0m; decimal dNetAmount = 0.0m;
            decimal dAdditionGiven = 0.0m; decimal dDeductionGiven = 0.0m; 

            try
            {

                if (dgvVacationDetails.RowCount > 0)
                {
                    for (int i = 0; dgvVacationDetails.RowCount - 1 >= i; ++i)
                    {
                        int iAdddedID = 0; decimal dAmount = 0.0m;

                        iAdddedID = Convert.ToInt32(dgvVacationDetails.Rows[i].Cells[dgvColParticulars.Index].Value);
                        if (Convert.ToString(dgvVacationDetails.Rows[i].Cells[dgvColAmount.Index].Value) != "" && Convert.ToString(dgvVacationDetails.Rows[i].Cells[dgvColAmount.Index].Value) != ".")
                        {
                            dAmount = Convert.ToDecimal(dgvVacationDetails.Rows[i].Cells[dgvColAmount.Index].Value);
                        }
                        if (dgvVacationDetails.Rows[i].Cells[dgvColIsAddition.Index].Value != null)
                        {
                            SAddDed = dgvVacationDetails.Rows[i].Cells[dgvColIsAddition.Index].Value.ToString();
                        }
                        else
                        {
                            SAddDed = "False";
                        }

                        if (SAddDed == "1" || SAddDed.ToString() == "True")
                        {
                            dAddition = dAddition + dAmount;
                        }
                        else if (SAddDed == "0" || SAddDed.ToString() == "False")
                        {
                            dDeduction = dDeduction + dAmount;
                        }

                        if (Convert.ToBoolean(dgvVacationDetails.Rows[i].Cells[dgvColReleaseLater.Index].Value) == false)
                        {
                            if (SAddDed == "1" || SAddDed.ToString() == "True")
                            {
                                dAdditionGiven = dAdditionGiven + dAmount;
                            }
                            else if (SAddDed == "0" || SAddDed.ToString() == "False")
                            {
                                dDeductionGiven = dDeductionGiven + dAmount;
                            }
                        }

                    }
                }

                txtTotalAddition.Text = dAddition.Format(2);
                txtTotalDeduction.Text = dDeduction.Format(2);
                dNetAmount = dAddition - dDeduction;
                txtNetAmount.Text = dNetAmount.Format(2);
                txtGivenAmount.Text = (dAdditionGiven - dDeductionGiven).Format(2);
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on CalculateNetAmount " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }
        #endregion CalculateNetAmount
        #region FillParameters
        /// <summary>
        /// To Fill Parameters
        /// </summary>
        private void FillParameters()
        {
            int ComID = 0;



            MobjclsBLLVacationEntry.clsDTOVacationEntry.EmployeeID = Convert.ToInt32(cboEmployee.SelectedValue);

            ComID = MobjclsBLLVacationEntry.GetEmpCompanyID();
            if (ComID != 0)
            {
                MintCompanyId = ComID;
            }
            MobjclsBLLVacationEntry.clsDTOVacationEntry.CompanyID = MintCompanyId;
            MobjclsBLLVacationEntry.clsDTOVacationEntry.IsProcessSalaryForCurrentMonth = chkProcessSalaryForCurrentMonth.Checked;

            if (txtNetAmount.Text.Trim() != "")
            {
                MobjclsBLLVacationEntry.clsDTOVacationEntry.NetAmount = Convert.ToDecimal(txtNetAmount.Text);
            }
            else
            {
                MobjclsBLLVacationEntry.clsDTOVacationEntry.NetAmount = 0;
            }

            if (txtGivenAmount.Text.Trim() != "")
            {
                MobjclsBLLVacationEntry.clsDTOVacationEntry.GivenAmount = Convert.ToDecimal(txtGivenAmount.Text);
            }
            else
            {
                MobjclsBLLVacationEntry.clsDTOVacationEntry.GivenAmount = 0;
            }

            MobjclsBLLVacationEntry.clsDTOVacationEntry.FromDate = dtpFromDate.Value.Date.ToString("dd-MMM-yyyy");
            MobjclsBLLVacationEntry.clsDTOVacationEntry.ToDate = dtpToDate.Value.Date.ToString("dd-MMM-yyyy");
            if ((chkEncashOnly.Checked == false))
            {
                MobjclsBLLVacationEntry.clsDTOVacationEntry.TakenLeaves = Convert.ToDecimal(lblNoOfDays.Text);
            }
            else
            {
                MobjclsBLLVacationEntry.clsDTOVacationEntry.TakenLeaves = 0;
            }
            if (txtNoOfTicketsIssued.Text.Trim() != "")
            {
                MobjclsBLLVacationEntry.clsDTOVacationEntry.IssuedTickets = Convert.ToInt32(txtNoOfTicketsIssued.Text);
            }
            if (txtTicketAmount.Text.Trim() != "" && txtTicketAmount.Text.Trim() != ".")
            {
                MobjclsBLLVacationEntry.clsDTOVacationEntry.TicketExpense = Convert.ToDecimal(txtTicketAmount.Text);
            }
            MobjclsBLLVacationEntry.clsDTOVacationEntry.Remarks = txtRemarks.Text.Trim();
            MobjclsBLLVacationEntry.clsDTOVacationEntry.CreatedBy = MintUserId;


            MobjclsBLLVacationEntry.clsDTOVacationEntry.ExperienceDays = lblExperienceDaysText.Text.ToDecimal();
            MobjclsBLLVacationEntry.clsDTOVacationEntry.IsConsiderAbsentDays = chkConsiderAbsentDays.Checked;
            MobjclsBLLVacationEntry.clsDTOVacationEntry.AbsentDays = lblAbsentDaysText.Text.ToDecimal();
            MobjclsBLLVacationEntry.clsDTOVacationEntry.EncashComboOffDay = txtEncashComboOffDay.Text.ToDecimal();

            if (txtEligibleLeavePayDays.Text == "")
            {
                txtEligibleLeavePayDays.Text = "0";
            }
            MobjclsBLLVacationEntry.clsDTOVacationEntry.EligibleLeavePayDays = Convert.ToDecimal(txtEligibleLeavePayDays.Text);
            MobjclsBLLVacationEntry.clsDTOVacationEntry.EncashComboOffDay = txtEncashComboOffDay.Text.ToDecimal() ;
            MobjclsBLLVacationEntry.clsDTOVacationEntry.RejoinDate = dtpRejoinDate.Value.ToString("dd-MMM-yyyy");
            MobjclsBLLVacationEntry.clsDTOVacationEntry.IsEncashOnly = chkEncashOnly.Checked;


            MobjclsBLLVacationEntry.clsDTOVacationEntry.AccountID = Convert.ToInt32(cboAccount.SelectedValue);
            MobjclsBLLVacationEntry.clsDTOVacationEntry.TransactionTypeID = Convert.ToInt32(cboTransactionType.SelectedValue);
            MobjclsBLLVacationEntry.clsDTOVacationEntry.ChequeNumber = txtChequeNo.Text.Trim();
            MobjclsBLLVacationEntry.clsDTOVacationEntry.ChequeDate = dtpChequeDate.Value;


            MobjclsBLLVacationEntry.clsDTOVacationEntry.OvertakenLeaves = Convert.ToDecimal(lblAdditionalLeavesText.Text);

            if (lblActualRejoinDate.Text.Trim() != "")
            {
                MobjclsBLLVacationEntry.clsDTOVacationEntry.ActualDateTime = lblActualRejoinDate.Text.Trim().ToDateTime();
            }
            else
            {
                MobjclsBLLVacationEntry.clsDTOVacationEntry.ActualDateTime = "01-Jan-1900".ToDateTime(); 
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            bool blnVacationSalaryExists = false;

            MobjclsBLLVacationEntry.clsDTOVacationEntry.lstclsDTOVacationEntryDetail = new System.Collections.Generic.List<DTO.clsDTOVacationEntryDetail>();

            int intOrder = 0;
            foreach (DataGridViewRow dr in dgvVacationDetails.Rows)
            {
                if (dr.Cells[dgvColParticulars.Index].Value != System.DBNull.Value)
                {
                    if (dr.Cells[dgvColParticulars.Index].Value != System.DBNull.Value)
                    {
                        clsDTOVacationEntryDetail objclsDTOVacationEntryDetail = new clsDTOVacationEntryDetail();
                        objclsDTOVacationEntryDetail.SerialNo = ++intOrder;
                        objclsDTOVacationEntryDetail.AdditionDeductionID = Convert.ToInt32(dr.Cells[dgvColParticulars.Index].Value);
                        objclsDTOVacationEntryDetail.Amount = Convert.ToDecimal(dr.Cells[dgvColAmount.Index].Value);
                        if (Convert.ToString(dr.Cells[dgvColNoOfDays.Index].Value) != "")
                        {
                            objclsDTOVacationEntryDetail.NoOfDays = Convert.ToDecimal(dr.Cells[dgvColNoOfDays.Index].Value);
                        }
                        else
                        {
                            objclsDTOVacationEntryDetail.NoOfDays = 0;
                        }
                        if (dr.Cells[dgvColReleaseLater.Index].Value != System.DBNull.Value)
                        {
                            objclsDTOVacationEntryDetail.IsReleaseLater = Convert.ToBoolean(dr.Cells[dgvColReleaseLater.Index].Value);
                        }
                        else
                        {
                            objclsDTOVacationEntryDetail.IsReleaseLater = false;
                        }
                        if (objclsDTOVacationEntryDetail.AdditionDeductionID == 15 || objclsDTOVacationEntryDetail.AdditionDeductionID == 19)
                        {
                            blnVacationSalaryExists = true;
                        }
                        MobjclsBLLVacationEntry.clsDTOVacationEntry.lstclsDTOVacationEntryDetail.Add(objclsDTOVacationEntryDetail);
                    }
                }
            }
        }
        #endregion FillParameters
        #region CheckGridDuplication
        /// <summary>
        /// To check Duplication of Particulars
        /// </summary>
        /// <param name="intAddDedID"></param>
        /// <returns>bool</returns>
        private bool CheckGridDuplication(int intAddDedID)
        {
            foreach (DataGridViewRow dr in dgvVacationDetails.Rows)
            {
                if (Convert.ToInt32(dr.Cells[dgvColParticulars.Index].Value) == intAddDedID)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7565, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
            return true;
        }
        #endregion CheckGridDuplication
        #region LoadVacationFormMessage
        /// <summary>
        /// To load Vacation Form Message
        /// </summary>
        private void LoadVacationFormMessage()
        {
            try
            {
                // Loading Message
                MsarMessageArr = new ArrayList();
                MsarStatusMessage = new ArrayList();
                MsarMessageArr = mObjNotification.FillMessageArray((int)FormID.VacationEntry, 2);
                MsarStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.VacationEntry, 2);
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on LoadMessage() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadMessage()" + ex.Message.ToString());
            }

        }
        #endregion LoadVacationFormMessage
        #region EmployeeHistoryDetail
        /// <summary>
        /// To Get Vacation History of Paticular Employee
        /// </summary>
        private void EmployeeHistoryDetail()
        {
            try
            {
                DataTable dt;
                dt = MobjclsBLLVacationEntry.EmployeeHistoryDetail();
                dgvEmpHistory.DataSource = null;
                if (dt.Rows.Count > 0)
                {
                    dgvEmpHistory.DataSource = dt;
                    dgvEmpHistory.Columns["Remarks"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
            catch
            {
            }

        }
        #endregion EmployeeHistoryDetail
        #region FillDetail
        /// <summary>
        /// To Fill Vacation Process Details
        /// </summary>
        private void FillDetail()
        {
            if (cboEmployee.SelectedIndex != -1 && NavigateFlg == 0)
            {
                FillParameters();
                MobjclsBLLVacationEntry.EmpDetail();

                if (MobjclsBLLVacationEntry.clsDTOVacationEntry.AbsentDays > 0 && chkConsiderAbsentDays.Checked == false)
                {
                    MobjclsBLLVacationEntry.clsDTOVacationEntry.ExperienceDays = MobjclsBLLVacationEntry.clsDTOVacationEntry.ExperienceDays - MobjclsBLLVacationEntry.clsDTOVacationEntry.AbsentDays;
                }
                if (blnAddvalue && chkConsiderAbsentDays.Checked == true)
                {
                    MobjclsBLLVacationEntry.clsDTOVacationEntry.ExperienceDays = MobjclsBLLVacationEntry.clsDTOVacationEntry.ExperienceDays - MobjclsBLLVacationEntry.clsDTOVacationEntry.AbsentDays;
                }
                if (blnAddvalue && chkConsiderAbsentDays.Checked == false)
                {
                    MobjclsBLLVacationEntry.clsDTOVacationEntry.ExperienceDays = MobjclsBLLVacationEntry.clsDTOVacationEntry.ExperienceDays + MobjclsBLLVacationEntry.clsDTOVacationEntry.AbsentDays;
                }
                if (!blnAddvalue)
                {

                    if (chkConsiderAbsentDays.Checked == true)
                    {
                        MobjclsBLLVacationEntry.clsDTOVacationEntry.ExperienceDays = (MobjclsBLLVacationEntry.clsDTOVacationEntry.ExperienceDays - MobjclsBLLVacationEntry.clsDTOVacationEntry.AbsentDays);
                        blnAddvalue = false;
                    }
                    else
                    {
                        MobjclsBLLVacationEntry.clsDTOVacationEntry.ExperienceDays = (MobjclsBLLVacationEntry.clsDTOVacationEntry.ExperienceDays + MobjclsBLLVacationEntry.clsDTOVacationEntry.AbsentDays);

                    }
                }
                blnAddvalue = false;

                lblNoOfDays.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.NoOfDays.ToString();
                mblnIsEmployeeFixedCategory = MobjclsBLLVacationEntry.clsDTOVacationEntry.IsRateOnly;
                dtpJoiningDate.Value = MobjclsBLLVacationEntry.clsDTOVacationEntry.JoiningDate;
                lblExperienceDaysText.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.ExperienceDays.ToString();
                lblAbsentDaysText.Text = "0";
                lblAbsentDaysText.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.AbsentDays.ToString();
                lblCurrency.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.ShortDescription;
                lblTotalEligibleLeavePayDaysText.Text = MobjclsBLLVacationEntry.GetEligibleLeavePayDays(true);
                lblEligibleLeavesText.Text = MobjclsBLLVacationEntry.GetEligibleLeavePayDays(false);
                txtEncashComboOffDay.Text = "";
                if (mblnIsEmployeeFixedCategory == true)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    lblEligibleLeavesText.Text = "ثابت";
                    //else
                        lblEligibleLeavesText.Text = "Fixed";
                }
                if (lblTotalEligibleLeavePayDaysText.Text.Trim() == "")
                {
                    lblTotalEligibleLeavePayDaysText.Text = "0";
                }


                if (Convert.ToDouble(lblTotalEligibleLeavePayDaysText.Text) > 0)
                {
                  
                        if (MbAddStatus == true)
                        {
                            if (chkEncashOnly.Checked)
                            {
                                txtEligibleLeavePayDays.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.EligibleLeavePayDays.ToString();
                            }
                            else
                            txtEligibleLeavePayDays.Text = lblNoOfDays.Text.ToString();
                        }
                        else
                            txtEligibleLeavePayDays.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.EligibleLeavePayDays.ToString();
                }
                else
                {
                    if (MbAddStatus == true)
                    {
                        txtEligibleLeavePayDays.Text = "0";
                    }
                    else
                    {
                        txtEligibleLeavePayDays.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.EligibleLeavePayDays.ToString();
                    }
                }
                if (MbAddStatus == true)
                {
                    cboTransactionType.SelectedValue = MobjclsBLLVacationEntry.clsDTOVacationEntry.TransactionTypeID;
                    //if (cboEmployee.SelectedIndex != -1)
                    //{
                    //    txtEncashComboOffDay.Text = MobjclsBLLVacationEntry.GetEligibleCombo(true);
                    //}
                }
                else
                {
                    txtEncashComboOffDay.Text = MobjclsBLLVacationEntry.clsDTOVacationEntry.EncashComboOffDay.ToString(); 
                }


            


            }
        }
        #endregion FillDetail
        #region LoadReport
        /// <summary>
        /// To load Vacation Pay Slip
        /// </summary>
        private void LoadReport()
        {
            try
            {
                //function for loading report
                if (Convert.ToInt32(MobjclsBLLVacationEntry.clsDTOVacationEntry.VacationID) > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = Convert.ToInt32(MobjclsBLLVacationEntry.clsDTOVacationEntry.VacationID);
                    ObjViewer.PiFormID = (int)FormID.VacationEntry;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on LoadReport() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport()" + ex.Message.ToString());
            }
        }
        #endregion LoadReport
        #region SetTransactionType
        /// <summary>
        /// To set Transaction Typoe
        /// </summary>
        private void SetTransactionType()
        {
            if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentTransactionType.Bank)
            {
                cboAccount.Enabled = true;
                txtChequeNo.Enabled = true;
                dtpChequeDate.Enabled = true;
                cboAccount.BackColor = System.Drawing.SystemColors.Info;
                txtChequeNo.BackColor = System.Drawing.SystemColors.Info;

            }
            else
            {
                cboAccount.Enabled = false;
                txtChequeNo.Enabled = false;
                dtpChequeDate.Enabled = false;
                cboAccount.SelectedIndex = -1;
                txtChequeNo.Text = "";
                dtpChequeDate.Value = DateTime.Now;
                cboAccount.BackColor = System.Drawing.SystemColors.Window;
                txtChequeNo.BackColor = System.Drawing.SystemColors.Window;
            }
        }
        #endregion SetTransactionType

        private void dgvVacationDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == dgvColAmount.Index)
                {
                    MobjclsBLLVacationEntry.UpdateParticularAmount(dgvVacationDetails.Rows[e.RowIndex].Cells[dgvColAmount.Index].Value.ToDecimal(), dgvVacationDetails.Rows[e.RowIndex].Cells[dgvColParticulars.Index].Value.ToInt32());
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 21, out MmessageIcon);

                    //if (ClsCommonSettings.IsArabicView)
                    //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 21, out MmessageIcon).ToString().Replace("*", "معلومات اجازة");
                    //else
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 21, out MmessageIcon).ToString().Replace("*", "Vacation Detail");
                    lblstatus.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 21, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    MobjclsBLLVacationEntry.UpdateNetAmount(txtNetAmount.Text.ToDecimal(), txtGivenAmount.Text.ToDecimal());

                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on dgvVacationDetails_CellEndEdit() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on dgvVacationDetails_CellEndEdit()" + ex.Message.ToString());
            }
        }

        #endregion Methods

        #region SetArabicControls
        //private void SetArabicControls()
        //{
        //    //ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.VacationEntry, this);

        //    strBindingOf = "من ";
        //    txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
        //}
        #endregion SetArabicControls

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void lblAccount_Click(object sender, EventArgs e)
        {

        }

       

        

    }
}
