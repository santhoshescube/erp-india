﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;



/* 
=================================================
Description :	     <Document Transactions>
Modified By :		 <Laxmi>
Modified Date :      <13 Apr 2012>
================================================
*/

namespace MyBooksERP
{
    public partial class frmDocumentMaster : Form
    {
        /// <summary>
        /// FormID 17
        /// </summary>
        /// 

        #region Declarations

        //public FrmPayrollMain objFrmPayrollMain;
        //public FrmDocumentsMain objFrmDocumentsMain;
        public FrmMain objTradingMain;

        public int FlagWhere=0; //1=from Document Main

        public int PintDocumentID; // DocumentID  for Receipt and Issue
        public int PiMode;        // receipt/issue
        
        public int PintOperationType; // OperationtypeID from refernce forms
        public long PlngOperationID; // ReferenceID from refernce forms

        public int PintOperationStatus; 
        public string PstrOperationNo;
        public string sStatus;
        public bool MblnEditable = true;
        bool MblnIsfromExistingForm = false;    // true when calling from existing form for recipt and issue

        bool blnIsEditMode = false;

        int MintRecordCnt, MintCurrentRecCnt = 0;
        string MstrCommonMessage;

        // permissions
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnAddUpdatePermission = false; // To set add/update permission
        private bool MblnPrintEmailPermission = false;//To Set PrintEmail Permission

        private clsBLLDocumentMaster objClsBLLDocumentMaster = null;
       
        private clsMessage ObjUserMessage = null;

        #endregion

        #region Constructor
        public frmDocumentMaster()
        {
            InitializeComponent();
            PiMode = (int)OperationStatusType.DocReceipt; // Default Mode is Receipt
            
        }
        #endregion

        #region Properties
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.Documents);
                return this.ObjUserMessage;
            }
        }

        private clsBLLDocumentMaster MobjClsBLLDocumentMaster
        {
            get
            {
                if (this.objClsBLLDocumentMaster == null)
                    this.objClsBLLDocumentMaster = new clsBLLDocumentMaster();

                return this.objClsBLLDocumentMaster;
            }
        }

        #endregion

        #region Methods

        private bool LoadCombo(int intType)
        {
            try
            {
                DataTable datCombos = null;

                if (intType == 0 || intType == 1) // Document Type
                {
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "DocumentTypeID, DocumentType", "DocumentTypeReference", "" });
                    cboDocumentType.DisplayMember = "DocumentType";
                    cboDocumentType.ValueMember = "DocumentTypeID";
                    cboDocumentType.DataSource = datCombos;
                }

                if (intType == 0 || intType == 2) // Operation Types
                {

                    /*------DOCUMENTS Attach--------------
                    1	PurchaseIndent----
                    2	PurchaseQuotation----
                    3	PurchaseOrder-----
                    4	GRN------
                    5	PurchaseInvoice----
                    6	DebitNote------
                    7	SalesQuotation----
                    8	SalesOrder-----
                    9	SalesInvoice-----
                    10	POS------
                    11	CreditNote------
                    14	Customers----
                    15	Suppliers----
                    16	Employee----
                    28	SiteVisit--------*/
                    string strCondition = "OperationTypeID In(" + (int)OperationType.PurchaseQuotation + "," +
                                                       (int)OperationType.PurchaseOrder + "," +
                                                       (int)OperationType.PurchaseInvoice + "," +
                                                       (int)OperationType.SalesQuotation + "," + (int)OperationType.SalesOrder + "," +
                                                       (int)OperationType.SalesInvoice + "," +
                                                       (int)OperationType.Customers + "," +
                                                        (int)OperationType.Suppliers + "," + (int)OperationType.Employee + ")";
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", strCondition });
                    cboOperationType.DisplayMember = "OperationType";
                    cboOperationType.ValueMember = "OperationTypeID";
                    cboOperationType.DataSource = datCombos;
                }
                if (intType == 0 || intType == 3) // Document Receipt staus
                {
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "DocumentReceiptID, DocumentReceipt", "DocDocumentReceiptReference", "" });
                    cboDocumentStatus.DisplayMember = "DocumentReceipt";
                    cboDocumentStatus.ValueMember = "DocumentReceiptID";
                    cboDocumentStatus.DataSource = datCombos;
                }
                if (intType == 0 || intType == 4) // Issue Reason Reference
                {
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "IssueReasonID, IssueReason", "DocDocumentIssueReasonReference", "" });
                    CboIssueReason.DisplayMember = "IssueReason";
                    CboIssueReason.ValueMember = "IssueReasonID";
                    CboIssueReason.DataSource = datCombos;
                }

                if (intType == 0 || intType == 5) // Bin Reference
                {
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "BinID, BinName", "DocBinReference", "" });
                    cboBinNumber.DisplayMember = "BinName";
                    cboBinNumber.ValueMember = "BinID";
                    cboBinNumber.DataSource = datCombos;
                }

                if (intType == 0 || intType == 6) // Users
                {
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "EmployeeID, EmployeeNumber+' - '+FirstName AS Name", "EmployeeMaster", "" });
                    cboCustodian.DisplayMember = "Name";
                    cboCustodian.ValueMember = "EmployeeID";
                    cboCustodian.DataSource = datCombos;
                }
                return true;
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                return false;
            }
        }

        private void GetRecordCount()
        {
            try
            {
                MintRecordCnt = 0;
                if (PintDocumentID > 0)
                    MintRecordCnt = 1;
                else
                MintRecordCnt = MobjClsBLLDocumentMaster.GetDocumentCount(PintOperationType, PlngOperationID);
                if (MintRecordCnt < 0)
                {
                    bnPositionItem.Text = "of 0";
                    MintRecordCnt = 0;
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void ClearAll()
        {
            txtDocumentNumber.Clear();
            cboDocumentType.Enabled = cboOperationType.Enabled = cboReferenceNumber.Enabled = btnDocumentType.Enabled = true;
            panel1.Enabled = panel2.Enabled = bnDeleteItem.Enabled = bnAddNewItem.Enabled = receiptIssueToolStripMenuItem.Enabled = bnCancel.Enabled = true;

            cboDocumentType.SelectedIndex = -1;
            cboOperationType.SelectedIndex = -1;
            cboReferenceNumber.SelectedIndex = -1;
            cboCustodian.SelectedIndex = -1;
            // if (cboCustodian.Items.Count > 0) cboCustodian.SelectedValue = ClsCommonSettings.UserID;
            cboDocumentStatus.SelectedIndex = -1;
            cboBinNumber.SelectedIndex = -1;
            CboIssueReason.SelectedIndex = -1;
            txtDocumentNumber.Tag = "0";
            txtRemarks.Clear();
            dtpTransactionDate.Value = DateTime.Today;
            dtpExpectedReturnDate.Value = DateTime.Today;
            chkExpiryDate.Checked = true;
            dtpExpiryDate.Enabled = true;
            dtpExpiryDate.Value = DateTime.Now.AddDays(1);
            SetLabel();
        }

        private bool SaveDocumentInfo()
        {
            int iDocID = 0;
            int intMessageCode = 0;
            bool blnRetValue = false;

            if (FormValidation())
            {

                if (txtDocumentNumber.Tag.ToInt32() > 0 && PintDocumentID == 0)
                    intMessageCode = 3;
                else
                    intMessageCode = 1;

                if (UserMessage.ShowMessage(intMessageCode))
                {
                    FillParameters();
                    iDocID = MobjClsBLLDocumentMaster.SaveDocumentInfo();
                    if (iDocID > 0)
                    {
                        MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intDocumentID = iDocID;
                        txtDocumentNumber.Tag = iDocID;
                        btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = false;
                        bnAddNewItem.Enabled = MblnAddPermission;//Add Permission
                        bnPrint.Enabled = true;
                        bnMoreActions.Enabled = true;
                        SetDocStatus();
                        lblstatus.Text = UserMessage.GetMessageByCode(2);
                        tmDocuments.Enabled = true;
                        blnRetValue = true;
                        MblnEditable = true;
                    }

                }
            }
            return blnRetValue;
        }

        private void FillParameters()
        {
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intOrderNo = Convert.ToInt32(txtRemarks.Tag);
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intDocumentID = Convert.ToInt32(txtDocumentNumber.Tag);
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intDocumentTypeID = Convert.ToInt32(cboDocumentType.SelectedValue);
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intOperationTypeID = Convert.ToInt32(cboOperationType.SelectedValue);
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.lngReferenceID = Convert.ToInt32(cboReferenceNumber.SelectedValue);
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strReferenceNumber = txtDocumentNumber.Text.Trim();
            if (chkExpiryDate.Checked)
                MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strExpiryDate = dtpExpiryDate.Value.ToString("dd-MMM-yyyy");
            else
                MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strExpiryDate = "";
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.blnIsReceipted = true;
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intCreatedBy = ClsCommonSettings.UserID;
            if (PiMode == (int)OperationStatusType.DocReceipt)
                MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intStatusID = (int)OperationStatusType.DocReceipt;
            else
                MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intStatusID = (int)OperationStatusType.DocIssue;

            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strReceiptIssueDate = dtpTransactionDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intCustodian = Convert.ToInt32(cboCustodian.SelectedValue);
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intDocumentStatusID = Convert.ToInt32(cboDocumentStatus.SelectedValue);
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intBinID = Convert.ToInt32(cboBinNumber.SelectedValue);
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intIssueReasonID = Convert.ToInt32(CboIssueReason.SelectedValue);
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strExpectedReturnDate = dtpExpectedReturnDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strRemarks = txtRemarks.Text.Trim();
        }

        private void DisplayAndSetDocumentInfo()
        {
            ClearAll();

            DisplayDocument();

            bnAddNewItem.Enabled = MblnAddPermission;
            bnDeleteItem.Enabled = MblnDeletePermission;
            bnMoreActions.Enabled = true;
            bnPrint.Enabled = MblnPrintEmailPermission;
            btnSave.Enabled = false;
            btnOk.Enabled = false;
            bnSaveItem.Enabled = false;
            SetDocStatus();
            bnPositionItem.Text = MintCurrentRecCnt.ToString();
            bnCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";
            blnIsEditMode = true;

            int iOutStatusID = 0;
            // Check editable mode

            if (cboOperationType.SelectedValue.ToInt32() != (int)OperationType.Employee && cboOperationType.SelectedValue.ToInt32() != (int)OperationType.Customers &&
                cboOperationType.SelectedValue.ToInt32() != (int)OperationType.Suppliers)
            {
                if (PintOperationType > 0 && PlngOperationID > 0 && !(MblnIsfromExistingForm))
                {
                    panel1.Enabled = panel2.Enabled = bnDeleteItem.Enabled = bnAddNewItem.Enabled = receiptIssueToolStripMenuItem.Enabled = bnCancel.Enabled = MblnEditable;

                }
                else
                {
                    MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intDocumentID = Convert.ToInt32(txtDocumentNumber.Tag);
                    iOutStatusID = MobjClsBLLDocumentMaster.GetCurStatus();
                    if (iOutStatusID == 1)
                    {
                        panel1.Enabled = panel2.Enabled = true;
                        if (MblnDeletePermission) bnDeleteItem.Enabled = true;
                        if (MblnAddPermission) bnAddNewItem.Enabled = true;
                        receiptIssueToolStripMenuItem.Enabled = bnCancel.Enabled = true;
                        MblnEditable = true;
                    }
                    else
                    {
                        panel1.Enabled = panel2.Enabled = bnDeleteItem.Enabled = bnAddNewItem.Enabled = receiptIssueToolStripMenuItem.Enabled = bnCancel.Enabled = false;
                        MblnEditable = false;
                    }
                }
            }
            else
            {
                MblnEditable = true;
            }
            if (PintOperationType == 0 && PlngOperationID == 0) bnAddNewItem.Enabled = MblnAddPermission;

        }

        private void DisplayDocument()
        {
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intRowNumber = MintCurrentRecCnt;
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intDocumentID = PintDocumentID;
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intOperationTypeID = PintOperationType;
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.lngReferenceID = PlngOperationID;

            if (MobjClsBLLDocumentMaster.GetDocumentInfo())
            {
                txtDocumentNumber.Tag = MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intDocumentID.ToInt32();
                cboDocumentType.Tag = MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intRowNumber.ToInt32();
                txtRemarks.Tag = MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intOrderNo.ToInt32();


                if (!(MblnIsfromExistingForm)) PiMode = (MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intStatusID);

                SetLabel();

                cboDocumentType.SelectedValue = Convert.ToInt32(MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intDocumentTypeID);
                cboOperationType.SelectedValue = Convert.ToInt32(MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intOperationTypeID);
                PlngOperationID = Convert.ToInt32(MobjClsBLLDocumentMaster.objClsDTODocumentMaster.lngReferenceID);
                OperationTypeChanged(cboOperationType, new EventArgs());
                cboReferenceNumber.SelectedValue = Convert.ToInt32(MobjClsBLLDocumentMaster.objClsDTODocumentMaster.lngReferenceID);
                txtDocumentNumber.Text = MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strReferenceNumber.ToString();
                dtpTransactionDate.Value = Convert.ToDateTime(MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strReceiptIssueDate);
                cboCustodian.SelectedValue = Convert.ToInt32(MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intCustodian);
                cboReferenceNumber.SelectedValue = Convert.ToInt32(MobjClsBLLDocumentMaster.objClsDTODocumentMaster.lngReferenceID);
                cboDocumentStatus.SelectedValue = Convert.ToInt32(MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intDocumentStatusID);
                CboIssueReason.SelectedValue = Convert.ToInt32(MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intIssueReasonID);
                cboBinNumber.SelectedValue = Convert.ToInt32(MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intBinID);

                txtRemarks.Text = MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strRemarks;
                cboDocumentType.Enabled = cboOperationType.Enabled = cboReferenceNumber.Enabled = btnDocumentType.Enabled = false;
                if (PintOperationType == 0) PlngOperationID = 0;
                if (MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strExpectedReturnDate != "")
                    dtpExpectedReturnDate.Value = Convert.ToDateTime(MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strExpectedReturnDate);
                //if (MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strExpiryDate !="")
                if (MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strExpiryDate != string.Empty)
                {
                    chkExpiryDate.Checked = true;
                    dtpExpiryDate.Value = MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strExpiryDate.ToDateTime();
                }
                else
                {
                    chkExpiryDate.Checked = false;
                    dtpExpiryDate.Enabled = false;
                }

            }
        }
        /// <summary>
        /// Set label based on Receipt and Issue
        /// </summary>
        private void SetLabel()
        {
            if (PiMode == (int)OperationStatusType.DocReceipt)  // Receipt
            {
                this.Text = "Documents - Receipt";
                lblReceiptIssue.Text = "Receipt Info"; // Heading
                lblTransactionDate.Text = "Receipt Date";
                lblReceivedBy.Text = "Received By";
                lblDocumentStatus.Text = "Document Status";
                CboIssueReason.Visible = false;
                cboDocumentStatus.Visible = true;
                lblExpRetDate.Visible = false;
                dtpExpectedReturnDate.Visible = false;
                dtpExpiryDate.Enabled = true;
                BtnNewDocumentStatus.Visible = true;
                BtnIssueReason.Visible = false;
            }
            else
            {
                this.Text = "Documents - Issue";
                lblReceiptIssue.Text = "Issue Info"; // Heading
                lblTransactionDate.Text = "Issue Date";
                lblReceivedBy.Text = "Issued By";
                lblDocumentStatus.Text = "Reason for Issue";
                dtpExpectedReturnDate.Visible = true;
                CboIssueReason.Visible = true;
                cboDocumentStatus.Visible = false;
                lblExpRetDate.Visible = true;
                dtpExpectedReturnDate.Visible = true;
                dtpExpiryDate.Enabled = false;
                BtnNewDocumentStatus.Visible = false;
                BtnIssueReason.Visible = true;
            }
        }

        private void SetDocStatus()
        {
            try
            {
                MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intDocumentID = txtDocumentNumber.Tag.ToInt32();
                if (MobjClsBLLDocumentMaster.GetStatusAndOrderNo())
                {
                    if (MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intStatusID == (int)OperationStatusType.DocReceipt) receiptIssueToolStripMenuItem.Text = "Issue";
                    else
                        receiptIssueToolStripMenuItem.Text = "Receipt";

                    txtRemarks.Tag = MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intOrderNo.ToInt32();
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }

        }

        private bool FormValidation()
        {
            errDocuments.Clear();

            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strReferenceNumber = txtDocumentNumber.Text.Trim();

            if (Convert.ToInt32(cboOperationType.SelectedValue) == 0)
            {
                errDocuments.SetError(cboOperationType, UserMessage.GetMessageByCode(1052));
                UserMessage.ShowMessage(1052);
                cboOperationType.Focus();
                lblstatus.Text = UserMessage.GetMessageByCode(1052);
                tmDocuments.Enabled = false;
                return false;


            }
            else if (Convert.ToInt32(cboReferenceNumber.SelectedValue) == 0)
            {
                //Please select Reference Number

                errDocuments.SetError(cboReferenceNumber, UserMessage.GetMessageByCode(1053));
                UserMessage.ShowMessage(1053);
                cboReferenceNumber.Focus();
                lblstatus.Text = UserMessage.GetMessageByCode(1053);
                tmDocuments.Enabled = false;
                return false;
            }
            else if (Convert.ToInt32(cboDocumentType.SelectedValue) == 0)
            {
                //Please select Document Type

                errDocuments.SetError(cboDocumentType, UserMessage.GetMessageByCode(1051));
                UserMessage.ShowMessage(1051);
                cboDocumentType.Focus();
                lblstatus.Text = UserMessage.GetMessageByCode(1051);
                tmDocuments.Enabled = false;
                return false;

            }
            else if (txtDocumentNumber.Text.Trim().Length == 0)
            {
                //Please select Document Number

                errDocuments.SetError(txtDocumentNumber, UserMessage.GetMessageByCode(1054));
                UserMessage.ShowMessage(1054);
                txtDocumentNumber.Focus();
                lblstatus.Text = UserMessage.GetMessageByCode(1054);
                tmDocuments.Enabled = false;
                return false;


            }
            else if (Convert.ToInt32(cboCustodian.SelectedValue) == 0)
            {
                //Please select Document Number

                if (PiMode == (int)OperationStatusType.DocReceipt)
                {
                    errDocuments.SetError(cboCustodian, UserMessage.GetMessageByCode(1063));
                    UserMessage.ShowMessage(1063);
                    cboCustodian.Focus();
                    lblstatus.Text = UserMessage.GetMessageByCode(1063);
                    tmDocuments.Enabled = false;
                    return false;
                }
                else
                {
                    errDocuments.SetError(cboCustodian, UserMessage.GetMessageByCode(1064));
                    UserMessage.ShowMessage(1064);
                    cboCustodian.Focus();
                    lblstatus.Text = UserMessage.GetMessageByCode(1064);
                    tmDocuments.Enabled = false;
                    return false;
                }

            }
            if (PiMode == (int)OperationStatusType.DocReceipt)
            {
                if (Convert.ToInt32(cboDocumentStatus.SelectedValue) == 0)
                {
                    //Please select Document Status

                    errDocuments.SetError(cboDocumentStatus, UserMessage.GetMessageByCode(1065));
                    UserMessage.ShowMessage(1065);
                    cboDocumentStatus.Focus();
                    lblstatus.Text = UserMessage.GetMessageByCode(1065);
                    tmDocuments.Enabled = false;
                    return false;

                }
                if (Convert.ToInt32(cboBinNumber.SelectedValue) == 0)
                {
                    //Please select Bin Number


                    errDocuments.SetError(cboBinNumber, UserMessage.GetMessageByCode(1066));
                    UserMessage.ShowMessage(1066);
                    cboBinNumber.Focus();
                    lblstatus.Text = UserMessage.GetMessageByCode(1066);
                    tmDocuments.Enabled = false;
                    return false;

                }
            }
            else
            {
                if (Convert.ToInt32(CboIssueReason.SelectedValue) == 0)
                {
                    //Please select Issue Reason

                    errDocuments.SetError(CboIssueReason, UserMessage.GetMessageByCode(1067));
                    UserMessage.ShowMessage(1067);
                    CboIssueReason.Focus();
                    lblstatus.Text = UserMessage.GetMessageByCode(1067);
                    tmDocuments.Enabled = false;
                    return false;

                }
            }
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intDocumentID = txtDocumentNumber.Tag.ToInt32();
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.strReferenceNumber = txtDocumentNumber.Text.Trim().ToUpper();
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intOperationTypeID = Convert.ToInt32(cboOperationType.SelectedValue);
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intDocumentTypeID = Convert.ToInt32(cboDocumentType.SelectedValue);
            MobjClsBLLDocumentMaster.objClsDTODocumentMaster.lngReferenceID = Convert.ToInt64(cboReferenceNumber.SelectedValue);
            if (MobjClsBLLDocumentMaster.CheckDocumentExists())
            {
                MessageBox.Show("Document number exists.", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtDocumentNumber.Focus();
                lblstatus.Text = "Document number exists.";
                tmDocuments.Enabled = false;
                return false;
            }

            if (chkExpiryDate.Checked)
            {
                if (MobjClsBLLDocumentMaster.CheckAlertSetting() <= 0)
                {
                    if (MessageBox.Show("Alert setting is not done,alerts will not be generated for this document.Do you wish to set Alert settings?  ", ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (!(bnAlert.Enabled))
                        {
                            MessageBox.Show("You have no permission to view Alert Settings.", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }

                        using (FrmAlertSettings objDoc = new FrmAlertSettings())
                        {
                            objDoc.ModuleID = (int)eModuleID.Payroll; ;
                            objDoc.ShowDialog();
                        }

                        //Program.objMain.ShowDialogForm(new FrmAlertSettings(), true);
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }

            return true;
        }

        private void AddNewDocument()
        {
            try
            {
               // PintDocumentID = 0;
                ClearAll();

                GetRecordCount();
                MintCurrentRecCnt = MintRecordCnt + 1;
                bnCountItem.Text = " of " + (MintRecordCnt + 1).ToString();
                bnPositionItem.Text = (MintRecordCnt + 1).ToString();
                cboOperationType.Enabled = true;
                btnOk.Enabled = false;
                btnSave.Enabled = false;
                bnAddNewItem.Enabled = false;
                bnDeleteItem.Enabled = false;
                bnPrint.Enabled = false;
                bnSaveItem.Enabled = false;
                bnMoreActions.Enabled = false;
                cboOperationType.Focus();
                panel1.Enabled = panel2.Enabled = true;

                receiptIssueToolStripMenuItem.Enabled = bnCancel.Enabled = true;
                //MblnEditable = true;

                LoadDocuments();
                blnIsEditMode = false;
                
                if (PintOperationType > 0)
                {
                    cboOperationType.SelectedValue = PintOperationType;
                    cboOperationType.Enabled = false;
                }


            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void LoadReport()
        {
            if (txtDocumentNumber.Tag.ToInt32() > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = txtDocumentNumber.Tag.ToInt32();
                ObjViewer.PiFormID = (int)FormID.Documents;
                ObjViewer.PiStatus = PiMode;
                ObjViewer.ShowDialog();
            }
        }

        private void SetPermissions()
        {
            DataTable dt = null;
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Document, (Int32)eMenuID.DocumentMaster, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                // -----------------------------------------  Alert Seting Permission -----------------------
                dt = objClsBLLPermissionSettings.GetMenuPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID);
                if (dt.Rows.Count == 0)
                    bnAlert.Enabled = false;
                else
                {
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AlertSettings).ToString();
                    bnAlert.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);
                }

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        #endregion

        #region Events
        private void FormLoad(object sender, EventArgs e)
        {
            SetLocation();
            SetPermissions();
            LoadCombo(0);
            AddNewDocument();
            dtpTransactionDate.MaxDate = ClsCommonSettings.GetServerDate();
           
            if (PintOperationType > 0 && PlngOperationID > 0 && !(MblnIsfromExistingForm))
                MoveLastItem(bnMoveLastItem, new EventArgs());
            else if (PintDocumentID > 0)
            {
                MintCurrentRecCnt = 0;
                MintRecordCnt = 1;
                MoveLastItem(bnMoveLastItem, new EventArgs());
                PintDocumentID = 0;
            }
            
        }
        private void SetLocation()
        {
            if (MblnIsfromExistingForm)
                this.Location = new Point(this.Location.X + 15, this.Location.Y + 20);
        
        }
        private void LoadDocuments()
        {
            if (MblnIsfromExistingForm == true)
            {
                if (PiMode == (int)OperationStatusType.DocReceipt) PiMode = (int)OperationStatusType.DocIssue;
                else PiMode = (int)OperationStatusType.DocReceipt;
                DisplayDocument();
                SetDocStatus();
                txtRemarks.Text = string.Empty;
                cboDocumentStatus.SelectedIndex = -1;
                CboIssueReason.SelectedIndex = -1;
                dtpTransactionDate.Value = DateTime.Today;
                dtpExpectedReturnDate.Value = DateTime.Today;
                txtRemarks.Tag = 0;

                bnMoveFirstItem.Visible = bnMovePreviousItem.Visible = bnMoveNextItem.Visible = bnMoveLastItem.Visible = bnAddNewItem.Visible = bnDeleteItem.Enabled = bnCancel.Visible = bnMoreActions.Visible = bnPositionItem.Visible = bnCountItem.Visible = bindingNavigatorSeparator.Visible = bindingNavigatorSeparator1.Visible = false;
                btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = false;
                btnDocumentType.Enabled = false;
                cboBinNumber.Enabled = BtnBinReference.Enabled = false;
            }
            else
            {
                bnMoveFirstItem.Visible = bnMovePreviousItem.Visible = bnMoveNextItem.Visible = bnMoveLastItem.Visible = bnAddNewItem.Visible = bnDeleteItem.Enabled = bnCancel.Visible = bnMoreActions.Visible = bnPositionItem.Visible = bnCountItem.Visible = bindingNavigatorSeparator.Visible = bindingNavigatorSeparator1.Visible = true;
                btnDocumentType.Enabled = true;
                cboBinNumber.Enabled = BtnBinReference.Enabled = true;
            }

            if (PintOperationType > 0 && PlngOperationID > 0)
            {
                cboOperationType.SelectedValue = PintOperationType;
                OperationTypeChanged(cboOperationType, new EventArgs());
                cboReferenceNumber.SelectedValue = PlngOperationID;
                cboOperationType.Enabled = cboReferenceNumber.Enabled = false;

            }
        }

        private void MoveFirstItem(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;
                DisplayAndSetDocumentInfo();
                lblstatus.Text = UserMessage.GetMessageByCode(9);
                tmDocuments.Enabled = true;
            }
        }

        private void MovePreviousItem(object sender, EventArgs e)
        {
            try
            {
                GetRecordCount();
                if (MintRecordCnt > 0)
                {
                    MintCurrentRecCnt = MintCurrentRecCnt - 1;
                    if (MintCurrentRecCnt <= 0)
                    {
                        MintCurrentRecCnt = 1;
                    }
                    else
                    {
                        DisplayAndSetDocumentInfo();
                        lblstatus.Text = UserMessage.GetMessageByCode(10);
                        tmDocuments.Enabled = true;
                    }
                }
                else
                {
                    MintCurrentRecCnt = 1;
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void MoveNextItem(object sender, EventArgs e)
        {
            try
            {
                GetRecordCount();
                if (MintRecordCnt > 0)
                {
                    if (MintRecordCnt == 1)
                    {
                        MintCurrentRecCnt = 1;
                    }
                    else
                    {
                        MintCurrentRecCnt = MintCurrentRecCnt + 1;
                    }

                    if (MintCurrentRecCnt > MintRecordCnt)
                    {
                        MintCurrentRecCnt = MintRecordCnt;
                    }
                    else
                    {
                        DisplayAndSetDocumentInfo();
                        lblstatus.Text = UserMessage.GetMessageByCode(11);
                        tmDocuments.Enabled = true;
                    }
                }
                else
                {
                    MintCurrentRecCnt = 0;
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void MoveLastItem(object sender, EventArgs e)
        {
            try
            {
                GetRecordCount();
                if (MintRecordCnt > 0)
                {
                    if (MintRecordCnt != MintCurrentRecCnt)
                    {
                        MintCurrentRecCnt = MintRecordCnt;
                        DisplayAndSetDocumentInfo();
                        lblstatus.Text = UserMessage.GetMessageByCode(12);
                        tmDocuments.Enabled = true;
                    }
                }
                else
                {
                    if (PintOperationType > 0 && PlngOperationID > 0)
                    {
                        panel1.Enabled = panel2.Enabled = receiptIssueToolStripMenuItem.Enabled = bnCancel.Enabled = MblnEditable;
                        if (MblnDeletePermission) bnDeleteItem.Enabled = MblnEditable;
                        if (MblnAddPermission) bnAddNewItem.Enabled = MblnEditable;
                        bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnEditable;
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void DeleteItem(object sender, EventArgs e)
        {
            try
            {
                //MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 1068, out MmessageIcon);

                if (Convert.ToInt32(txtDocumentNumber.Tag) > 0)
                {

                    MstrCommonMessage = UserMessage.GetMessageByCode(1068);
                    if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                   DialogResult.Yes)
                    {
                        MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intDocumentID = txtDocumentNumber.Tag.ToInt32();
                        MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intOperationTypeID = Convert.ToInt32(cboOperationType.SelectedValue);
                        MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intDocumentTypeID = Convert.ToInt32(cboDocumentType.SelectedValue);
                        MobjClsBLLDocumentMaster.objClsDTODocumentMaster.lngReferenceID = Convert.ToInt64(cboReferenceNumber.SelectedValue);
                        if (MobjClsBLLDocumentMaster.DeleteDocumentInfo())
                        {
                            MobjClsBLLDocumentMaster.DeleteAlert(txtDocumentNumber.Tag.ToInt32(), (int)AlertSettingsTypes.DocumentExpiry);                            
                            bnAddNewItem_Click(sender, e);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void Cancel(object sender, EventArgs e)
        {
            bnAddNewItem_Click(sender, e);
        }

        private void ChangeStatus(object sender, EventArgs e)
        {
           
            errDocuments.Clear();
            if (!blnIsEditMode)
            {
                btnOk.Enabled = MblnAddPermission;
                btnSave.Enabled = MblnAddPermission;
                bnSaveItem.Enabled = MblnAddPermission;

            }
            else
            {
                btnOk.Enabled = MblnUpdatePermission;
                btnSave.Enabled = MblnUpdatePermission;
                bnSaveItem.Enabled = MblnUpdatePermission;
            }
        }

        private void Close(object sender, EventArgs e)
        {
            Close();
        }

        private void TimerTick(object sender, EventArgs e)
        {
            // lblstatus.Text = "";
            tmDocuments.Enabled = false;
        }

        private void Email(object sender, EventArgs e)
        {
            //using (FrmEmailPopup objEmailPopUp = new FrmEmailPopup())
            //{
            //    objEmailPopUp.MsSubject = "Document Details";
            //    objEmailPopUp.EmailFormType = EmailFormID.Documents;
            //    objEmailPopUp.EmailSource = MobjClsBLLDocumentMaster.GetDocumentReport();
            //    objEmailPopUp.ShowDialog();
            //}
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (sender.GetType() == typeof(ComboBox))
            {
                ComboBox cbo = (ComboBox)sender;
                cbo.DroppedDown = false;
            }
        }

        private void OperationTypeChanged(object sender, EventArgs e)
        {
            DataTable datCombos = null;
            switch (Convert.ToInt32(cboOperationType.SelectedValue))
            {
                case (int)OperationType.CreditNote:
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "SalesReturnID as ReferenceID, SalesReturnNo as ReferenceNumber", "InvSalesReturnMaster", "" });
                    break;
                case (int)OperationType.DebitNote:
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "PurchaseReturnID as ReferenceID, PurchaseReturnNo as ReferenceNumber", "InvPurchaseReturnMaster", "" });
                    break;
                case (int)OperationType.GRN:
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "GRNID as ReferenceID, GRNNo as ReferenceNumber, StatusID", "InvGRNMaster", (PlngOperationID > 0 ? "" : " StatusID = " + (int)OperationStatusType.GRNOpened) });
                    break;
                case (int)OperationType.POS:
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "POSID as ReferenceID, POSNo as ReferenceNumber, StatusID", "InvPOSMaster", (PlngOperationID > 0 ? "" : " StatusID = " + (int)OperationStatusType.SPOSOpen) });
                    break;
                case (int)OperationType.PurchaseIndent:
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "PurchaseIndentID as ReferenceID, PurchaseIndentNo as ReferenceNumber, StatusID", "InvPurchaseIndentMaster", (PlngOperationID > 0 ? "" : "  StatusID = " + (int)OperationStatusType.PIndentOpened) });
                    break;
                case (int)OperationType.PurchaseInvoice:
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "PurchaseInvoiceID as ReferenceID, PurchaseInvoiceNo as ReferenceNumber, StatusID", "InvPurchaseInvoiceMaster", (PlngOperationID > 0 ? "" : "  StatusID = " + (int)OperationStatusType.PInvoiceOpened) });
                    break;
                case (int)OperationType.PurchaseOrder:
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "PurchaseOrderID as ReferenceID, PurchaseOrderNo as ReferenceNumber, StatusID", "InvPurchaseOrderMaster", (PlngOperationID > 0 ? "" : "  StatusID In( " + (int)OperationStatusType.POrderOpened + "," + (int)OperationStatusType.POrderRejected + ")") });
                    break;
                case (int)OperationType.PurchaseQuotation:
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "PurchaseQuotationID as ReferenceID, PurchaseQuotationNo as ReferenceNumber, StatusID", "InvPurchaseQuotationMaster", (PlngOperationID > 0 ? "" : "  StatusID In( " + (int)OperationStatusType.PQuotationOpened + "," + (int)OperationStatusType.PQuotationRejected + ")") });
                    break;
                case (int)OperationType.SalesInvoice:
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "SalesInvoiceID as ReferenceID, SalesInvoiceNo as ReferenceNumber, StatusID", "InvSalesInvoiceMaster", (PlngOperationID > 0 ? "" : "  StatusID = " + (int)OperationStatusType.SInvoiceOpen) });
                    break;
                case (int)OperationType.SalesOrder:
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "SalesOrderID as ReferenceID, SalesOrderNo as ReferenceNumber, StatusID", "InvSalesOrderMaster", (PlngOperationID > 0 ? "" : "  StatusID In ( " + (int)OperationStatusType.SOrderOpen + "," + (int)OperationStatusType.SOrderRejected + ")") });
                    break;
                case (int)OperationType.SalesQuotation:
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "SalesQuotationID as ReferenceID, SalesQuotationNo as ReferenceNumber, StatusID", "InvSalesQuotationMaster", (PlngOperationID > 0 ? "" : "  StatusID In( " + (int)OperationStatusType.SQuotationOpen + "," + (int)OperationStatusType.SQuotationRejected + ")") });
                    break;
                case (int)OperationType.Customers:
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "VendorID as ReferenceID, VendorCode + '-' +  VendorName as ReferenceNumber", "InvVendorInformation", "" });
                    break;
                case (int)OperationType.Suppliers:
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "VendorID as ReferenceID, VendorCode + '-' +  VendorName as ReferenceNumber", "InvVendorInformation", "" });
                    break;
                case (int)OperationType.Employee:
                    datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "EmployeeID as ReferenceID, FirstName + '-' + EmployeeNumber  as ReferenceNumber", "EmployeeMaster", "" });
                    break;
            }
            cboReferenceNumber.DisplayMember = "ReferenceNumber";
            cboReferenceNumber.ValueMember = "ReferenceID";
            cboReferenceNumber.DataSource = datCombos;
            cboReferenceNumber.SelectedValue = PlngOperationID;
        }

        private void mnuItemAttachDocuments_Click(object sender, EventArgs e)
        {
            using (FrmScanning objScanning = new FrmScanning())
            {
                objScanning.MintDocumentTypeid = Convert.ToInt32(cboDocumentType.SelectedValue);
                objScanning.MlngReferenceID = MobjClsBLLDocumentMaster.objClsDTODocumentMaster.lngReferenceID;
                objScanning.MintOperationTypeID = MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intOperationTypeID;
                objScanning.MstrReferenceNo = txtDocumentNumber.Text.Trim();
                objScanning.MintVendorID = MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intVendorID;
                objScanning.PblnEditable = MblnEditable;
                objScanning.ShowDialog();
            }
        }

        private void FormKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                switch (e.KeyCode)
                {
                    case Keys.Left:
                        MovePreviousItem(sender, e);
                        break;
                    case Keys.Right:
                        MoveNextItem(sender, e);
                        break;
                    case Keys.Up:
                        MoveLastItem(sender, e);
                        break;
                    case Keys.Down:
                        MoveFirstItem(sender, e);
                        break;
                }
            }
        }

        private void ClosingForm(object sender, FormClosingEventArgs e)
        {
            if (btnOk.Enabled)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(8);//MobjClsNotification.GetErrorMessage(datMessages, 8, out MmsgMessageIcon).Replace("#", "").Trim();
                if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.Yes)
                {
                    // MobjClsNotification = null;
                    // MobjClsLogWriter = null;
                    objClsBLLDocumentMaster = null;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void txtDocumentNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (e.KeyChar == (char)Keys.Space);
        }

        private void txtDocumentNumber_TextChanged(object sender, EventArgs e)
        {
            if (txtDocumentNumber.Text.Contains(" "))
            {
                txtDocumentNumber.Text = txtDocumentNumber.Text.Replace(" ", string.Empty);
            }

            errDocuments.Clear();
            btnSave.Enabled = true;
            btnOk.Enabled = true;
            bnSaveItem.Enabled = true;
        }

        private void bnSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveDocumentInfo())
            {

                LoadReport();
            }

        }

        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            PiMode = (int)OperationStatusType.DocReceipt;
            MblnIsfromExistingForm = false;
            bnMoveFirstItem.Visible = bnMovePreviousItem.Visible = bnMoveNextItem.Visible = bnMoveLastItem.Visible = bnAddNewItem.Visible = bnDeleteItem.Enabled = bnCancel.Visible = bnMoreActions.Visible = bnPositionItem.Visible = bnCountItem.Visible = bindingNavigatorSeparator.Visible = bindingNavigatorSeparator1.Visible = true;
            btnDocumentType.Enabled = true;
            cboBinNumber.Enabled = BtnBinReference.Enabled = true;
            AddNewDocument();
            //LoadDocuments();

        }

        private void receiptIssueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (frmDocumentMaster objDoc = new frmDocumentMaster())
            {
                objDoc.PintDocumentID = txtDocumentNumber.Tag.ToInt32();
                objDoc.MblnIsfromExistingForm = true;
                objDoc.PiMode = PiMode;
                objDoc.PintOperationType = PintOperationType;
                objDoc.PlngOperationID = PlngOperationID;
                objDoc.ShowDialog();
                DisplayDocument();
                SetDocStatus();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveDocumentInfo()) LoadReport();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveDocumentInfo())
            {
                LoadReport();
                this.Close();
            }

        }

        private void BtnNewDocumentStatus_Click(object sender, EventArgs e)
        {
            try
            {
                int iDocStatus = Convert.ToInt32(cboDocumentStatus.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Document Status",
                    new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "DocumentReceiptID,IsPredefined, DocumentReceipt", "DocDocumentReceiptReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                LoadCombo(3);
                if (objCommon.NewID != 0)
                    cboDocumentStatus.SelectedValue = objCommon.NewID;
                else
                    cboDocumentStatus.SelectedValue = iDocStatus;
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void BtnBinReference_Click(object sender, EventArgs e)
        {
            try
            {
                int iBinID = Convert.ToInt32(cboBinNumber.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Bin",
                    new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "BinID, BinName", "DocBinReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                LoadCombo(5);
                if (objCommon.NewID != 0)
                    cboBinNumber.SelectedValue = objCommon.NewID;
                else
                    cboBinNumber.SelectedValue = iBinID;
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void BtnIssueReason_Click(object sender, EventArgs e)
        {
            try
            {
                int iIssueReason = Convert.ToInt32(cboBinNumber.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Issue Reason",
                    new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "IssueReasonID, IsPredefined, IssueReason", "DocDocumentIssueReasonReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                LoadCombo(4);
                if (objCommon.NewID != 0)
                    cboBinNumber.SelectedValue = objCommon.NewID;
                else
                    cboBinNumber.SelectedValue = iIssueReason;
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                LoadReport();
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void btnDocumentType_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Document Type", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "DocumentTypeID,IsPredefined, DocumentType As [Document Type]", "DocumentTypeReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                int intDocumentTypeID = Convert.ToInt32(cboDocumentType.SelectedValue);
                LoadCombo(1);
                if (objCommon.NewID != 0)
                    cboDocumentType.SelectedValue = objCommon.NewID;
                else
                    cboDocumentType.SelectedValue = intDocumentTypeID;
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void bnAlert_Click(object sender, EventArgs e)
        {

            // Program.objMain.ShowDialogForm(new FrmAlertSettings(), true);
            using (FrmAlertSettings objDoc = new FrmAlertSettings())
            {
                objDoc.ModuleID = (int)eModuleID.Payroll;
                objDoc.ShowDialog();

            }
        }
        private void chkExpiryDate_CheckedChanged(object sender, EventArgs e)
        {
            ChangeStatus(sender, new EventArgs());
            if (chkExpiryDate.Checked)
                dtpExpiryDate.Enabled = true;
            else
                dtpExpiryDate.Enabled = false;
        }
        #endregion






    }
}