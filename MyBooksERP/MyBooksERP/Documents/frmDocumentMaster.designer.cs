﻿namespace MyBooksERP
{
    partial class frmDocumentMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label5;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDocumentMaster));
            this.tmDocuments = new System.Windows.Forms.Timer(this.components);
            this.DocumentsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bnMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bnMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bnPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bnCancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnAlert = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoreActions = new System.Windows.Forms.ToolStripDropDownButton();
            this.mnuItemAttachDocuments = new System.Windows.Forms.ToolStripMenuItem();
            this.receiptIssueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.bnHelp = new System.Windows.Forms.ToolStripButton();
            this.errDocuments = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkExpiryDate = new System.Windows.Forms.CheckBox();
            this.dtpExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboOperationType = new System.Windows.Forms.ComboBox();
            this.cboReferenceNumber = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDocumentType = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDocumentNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cboDocumentType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnActions = new DevComponents.DotNetBar.ButtonItem();
            this.btnInstallments = new DevComponents.DotNetBar.ButtonItem();
            this.btnExpense = new DevComponents.DotNetBar.ButtonItem();
            this.btnApprove = new DevComponents.DotNetBar.ButtonItem();
            this.btnReject = new DevComponents.DotNetBar.ButtonItem();
            this.btnDocuments = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem5 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem6 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem8 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem9 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem10 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem11 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem12 = new DevComponents.DotNetBar.ButtonItem();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblExpRetDate = new System.Windows.Forms.Label();
            this.lblReceiptIssue = new System.Windows.Forms.Label();
            this.lblDocumentStatus = new System.Windows.Forms.Label();
            this.dtpExpectedReturnDate = new System.Windows.Forms.DateTimePicker();
            this.BtnNewDocumentStatus = new System.Windows.Forms.Button();
            this.cboDocumentStatus = new System.Windows.Forms.ComboBox();
            this.lblBinNumber = new System.Windows.Forms.Label();
            this.BtnBinReference = new System.Windows.Forms.Button();
            this.cboBinNumber = new System.Windows.Forms.ComboBox();
            this.lblReceivedBy = new System.Windows.Forms.Label();
            this.CboIssueReason = new System.Windows.Forms.ComboBox();
            this.BtnIssueReason = new System.Windows.Forms.Button();
            this.cboCustodian = new System.Windows.Forms.ComboBox();
            this.lblTransactionDate = new System.Windows.Forms.Label();
            this.dtpTransactionDate = new System.Windows.Forms.DateTimePicker();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentsBindingNavigator)).BeginInit();
            this.DocumentsBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errDocuments)).BeginInit();
            this.panel1.SuspendLayout();
            this.ssStatus.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.Location = new System.Drawing.Point(7, 147);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(63, 13);
            label5.TabIndex = 141;
            label5.Text = "Other info";
            // 
            // DocumentsBindingNavigator
            // 
            this.DocumentsBindingNavigator.AddNewItem = null;
            this.DocumentsBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.DocumentsBindingNavigator.CountItem = this.bnCountItem;
            this.DocumentsBindingNavigator.DeleteItem = null;
            this.DocumentsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnMoveFirstItem,
            this.bnMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bnPositionItem,
            this.bnCountItem,
            this.bindingNavigatorSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnDeleteItem,
            this.bnCancel,
            this.toolStripSeparator1,
            this.bnAlert,
            this.toolStripSeparator2,
            this.bnMoreActions,
            this.toolStripSeparator3,
            this.bnPrint,
            this.bnHelp});
            this.DocumentsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.DocumentsBindingNavigator.MoveFirstItem = null;
            this.DocumentsBindingNavigator.MoveLastItem = null;
            this.DocumentsBindingNavigator.MoveNextItem = null;
            this.DocumentsBindingNavigator.MovePreviousItem = null;
            this.DocumentsBindingNavigator.Name = "DocumentsBindingNavigator";
            this.DocumentsBindingNavigator.PositionItem = this.bnPositionItem;
            this.DocumentsBindingNavigator.Size = new System.Drawing.Size(454, 25);
            this.DocumentsBindingNavigator.TabIndex = 4;
            this.DocumentsBindingNavigator.Text = "bindingNavigator1";
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.Size = new System.Drawing.Size(35, 22);
            this.bnCountItem.Text = "of {0}";
            this.bnCountItem.ToolTipText = "Total number of items";
            // 
            // bnMoveFirstItem
            // 
            this.bnMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            this.bnMoveFirstItem.Name = "bnMoveFirstItem";
            this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveFirstItem.Text = "Move first";
            this.bnMoveFirstItem.Click += new System.EventHandler(this.MoveFirstItem);
            // 
            // bnMovePreviousItem
            // 
            this.bnMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem.Image")));
            this.bnMovePreviousItem.Name = "bnMovePreviousItem";
            this.bnMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bnMovePreviousItem.Text = "Move previous";
            this.bnMovePreviousItem.Click += new System.EventHandler(this.MovePreviousItem);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPositionItem
            // 
            this.bnPositionItem.AccessibleName = "Position";
            this.bnPositionItem.AutoSize = false;
            this.bnPositionItem.Name = "bnPositionItem";
            this.bnPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bnPositionItem.Text = "0";
            this.bnPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Move next";
            this.bnMoveNextItem.Click += new System.EventHandler(this.MoveNextItem);
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Move last";
            this.bnMoveLastItem.Click += new System.EventHandler(this.MoveLastItem);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewItem.ToolTipText = "Add new Receipt ";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bnSaveItem.ToolTipText = "Save Item";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bnDeleteItem.Image")));
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.ToolTipText = "Delete Item";
            this.bnDeleteItem.Click += new System.EventHandler(this.DeleteItem);
            // 
            // bnCancel
            // 
            this.bnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnCancel.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.bnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnCancel.Name = "bnCancel";
            this.bnCancel.Size = new System.Drawing.Size(23, 22);
            this.bnCancel.Text = "Clear";
            this.bnCancel.ToolTipText = "Clear";
            this.bnCancel.Click += new System.EventHandler(this.Cancel);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnAlert
            // 
            this.bnAlert.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnAlert.Image = global::MyBooksERP.Properties.Resources.Alert_Role_Settings1;
            this.bnAlert.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnAlert.Name = "bnAlert";
            this.bnAlert.Size = new System.Drawing.Size(23, 22);
            this.bnAlert.Text = "AlertSettings";
            this.bnAlert.ToolTipText = "Alert Settings";
            this.bnAlert.Click += new System.EventHandler(this.bnAlert_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoreActions
            // 
            this.bnMoreActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemAttachDocuments,
            this.receiptIssueToolStripMenuItem});
            this.bnMoreActions.Image = global::MyBooksERP.Properties.Resources.Add1;
            this.bnMoreActions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnMoreActions.Name = "bnMoreActions";
            this.bnMoreActions.Size = new System.Drawing.Size(76, 22);
            this.bnMoreActions.Text = "Actions";
            this.bnMoreActions.ToolTipText = "Actions";
            // 
            // mnuItemAttachDocuments
            // 
            this.mnuItemAttachDocuments.Image = global::MyBooksERP.Properties.Resources.DocumentType;
            this.mnuItemAttachDocuments.Name = "mnuItemAttachDocuments";
            this.mnuItemAttachDocuments.Size = new System.Drawing.Size(135, 22);
            this.mnuItemAttachDocuments.Text = "Documents";
            this.mnuItemAttachDocuments.Click += new System.EventHandler(this.mnuItemAttachDocuments_Click);
            // 
            // receiptIssueToolStripMenuItem
            // 
            this.receiptIssueToolStripMenuItem.Image = global::MyBooksERP.Properties.Resources.Receipts;
            this.receiptIssueToolStripMenuItem.Name = "receiptIssueToolStripMenuItem";
            this.receiptIssueToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.receiptIssueToolStripMenuItem.Text = "Receipt";
            this.receiptIssueToolStripMenuItem.Click += new System.EventHandler(this.receiptIssueToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            this.toolStripSeparator3.Visible = false;
            // 
            // bnPrint
            // 
            this.bnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.bnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(23, 22);
            this.bnPrint.ToolTipText = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // bnHelp
            // 
            this.bnHelp.Enabled = false;
            this.bnHelp.Image = ((System.Drawing.Image)(resources.GetObject("bnHelp.Image")));
            this.bnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnHelp.Name = "bnHelp";
            this.bnHelp.Size = new System.Drawing.Size(23, 20);
            this.bnHelp.ToolTipText = "Help";
            // 
            // errDocuments
            // 
            this.errDocuments.ContainerControl = this;
            this.errDocuments.RightToLeft = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.chkExpiryDate);
            this.panel1.Controls.Add(this.dtpExpiryDate);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cboOperationType);
            this.panel1.Controls.Add(this.cboReferenceNumber);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnDocumentType);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtDocumentNumber);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cboDocumentType);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.shapeContainer2);
            this.panel1.Location = new System.Drawing.Point(3, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(445, 158);
            this.panel1.TabIndex = 99;
            // 
            // chkExpiryDate
            // 
            this.chkExpiryDate.AutoSize = true;
            this.chkExpiryDate.Checked = true;
            this.chkExpiryDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkExpiryDate.Location = new System.Drawing.Point(149, 129);
            this.chkExpiryDate.Name = "chkExpiryDate";
            this.chkExpiryDate.Size = new System.Drawing.Size(15, 14);
            this.chkExpiryDate.TabIndex = 128;
            this.chkExpiryDate.UseVisualStyleBackColor = true;
            this.chkExpiryDate.CheckedChanged += new System.EventHandler(this.chkExpiryDate_CheckedChanged);
            // 
            // dtpExpiryDate
            // 
            this.dtpExpiryDate.CalendarForeColor = System.Drawing.Color.Red;
            this.dtpExpiryDate.CalendarMonthBackground = System.Drawing.Color.Yellow;
            this.dtpExpiryDate.CalendarTitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.dtpExpiryDate.CalendarTitleForeColor = System.Drawing.Color.Navy;
            this.dtpExpiryDate.CalendarTrailingForeColor = System.Drawing.Color.Fuchsia;
            this.dtpExpiryDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpiryDate.Location = new System.Drawing.Point(146, 126);
            this.dtpExpiryDate.MaxDate = new System.DateTime(2099, 12, 31, 0, 0, 0, 0);
            this.dtpExpiryDate.Name = "dtpExpiryDate";
            this.dtpExpiryDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dtpExpiryDate.ShowCheckBox = true;
            this.dtpExpiryDate.Size = new System.Drawing.Size(117, 20);
            this.dtpExpiryDate.TabIndex = 5;
            this.dtpExpiryDate.Value = new System.DateTime(2013, 3, 14, 0, 0, 0, 0);
            this.dtpExpiryDate.ValueChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 127;
            this.label6.Text = "Expiry Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Operation Type";
            // 
            // cboOperationType
            // 
            this.cboOperationType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOperationType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOperationType.BackColor = System.Drawing.SystemColors.Info;
            this.cboOperationType.DropDownHeight = 134;
            this.cboOperationType.FormattingEnabled = true;
            this.cboOperationType.IntegralHeight = false;
            this.cboOperationType.Items.AddRange(new object[] {
            "Employee"});
            this.cboOperationType.Location = new System.Drawing.Point(146, 17);
            this.cboOperationType.Name = "cboOperationType";
            this.cboOperationType.Size = new System.Drawing.Size(284, 21);
            this.cboOperationType.TabIndex = 0;
            this.cboOperationType.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.cboOperationType.SelectedValueChanged += new System.EventHandler(this.OperationTypeChanged);
            this.cboOperationType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyDown);
            // 
            // cboReferenceNumber
            // 
            this.cboReferenceNumber.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboReferenceNumber.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboReferenceNumber.BackColor = System.Drawing.SystemColors.Info;
            this.cboReferenceNumber.DropDownHeight = 134;
            this.cboReferenceNumber.FormattingEnabled = true;
            this.cboReferenceNumber.IntegralHeight = false;
            this.cboReferenceNumber.Location = new System.Drawing.Point(146, 44);
            this.cboReferenceNumber.Name = "cboReferenceNumber";
            this.cboReferenceNumber.Size = new System.Drawing.Size(284, 21);
            this.cboReferenceNumber.TabIndex = 1;
            this.cboReferenceNumber.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.cboReferenceNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Reference Number";
            // 
            // btnDocumentType
            // 
            this.btnDocumentType.Location = new System.Drawing.Point(404, 71);
            this.btnDocumentType.Name = "btnDocumentType";
            this.btnDocumentType.Size = new System.Drawing.Size(26, 23);
            this.btnDocumentType.TabIndex = 3;
            this.btnDocumentType.Text = "...";
            this.btnDocumentType.UseVisualStyleBackColor = true;
            this.btnDocumentType.Click += new System.EventHandler(this.btnDocumentType_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, -2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 13);
            this.label8.TabIndex = 60;
            this.label8.Text = "Document Info";
            // 
            // txtDocumentNumber
            // 
            this.txtDocumentNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtDocumentNumber.Location = new System.Drawing.Point(146, 99);
            this.txtDocumentNumber.MaxLength = 30;
            this.txtDocumentNumber.Name = "txtDocumentNumber";
            this.txtDocumentNumber.Size = new System.Drawing.Size(284, 20);
            this.txtDocumentNumber.TabIndex = 4;
            this.txtDocumentNumber.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Document Number";
            // 
            // cboDocumentType
            // 
            this.cboDocumentType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDocumentType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDocumentType.BackColor = System.Drawing.SystemColors.Info;
            this.cboDocumentType.DropDownHeight = 134;
            this.cboDocumentType.FormattingEnabled = true;
            this.cboDocumentType.IntegralHeight = false;
            this.cboDocumentType.Items.AddRange(new object[] {
            "Passport"});
            this.cboDocumentType.Location = new System.Drawing.Point(146, 71);
            this.cboDocumentType.Name = "cboDocumentType";
            this.cboDocumentType.Size = new System.Drawing.Size(252, 21);
            this.cboDocumentType.TabIndex = 2;
            this.cboDocumentType.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.cboDocumentType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Document Type";
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(445, 158);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 55;
            this.lineShape1.X2 = 429;
            this.lineShape1.Y1 = 6;
            this.lineShape1.Y2 = 6;
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(21, 163);
            this.txtRemarks.MaxLength = 900;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(409, 49);
            this.txtRemarks.TabIndex = 15;
            this.txtRemarks.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(291, 425);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 16;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(372, 425);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.Close);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 422);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnActions
            // 
            this.btnActions.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnActions.Image = ((System.Drawing.Image)(resources.GetObject("btnActions.Image")));
            this.btnActions.Name = "btnActions";
            this.btnActions.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnInstallments,
            this.btnExpense,
            this.btnApprove,
            this.btnReject,
            this.btnDocuments});
            this.btnActions.Text = "Actions";
            this.btnActions.Tooltip = "Actions";
            // 
            // btnInstallments
            // 
            this.btnInstallments.Name = "btnInstallments";
            this.btnInstallments.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlH);
            this.btnInstallments.Text = "Installments";
            this.btnInstallments.Tooltip = "Installments";
            // 
            // btnExpense
            // 
            this.btnExpense.Image = ((System.Drawing.Image)(resources.GetObject("btnExpense.Image")));
            this.btnExpense.Name = "btnExpense";
            this.btnExpense.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.btnExpense.Text = "Expense";
            this.btnExpense.Tooltip = "Expense";
            // 
            // btnApprove
            // 
            this.btnApprove.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btnApprove.Text = "Approve";
            this.btnApprove.Tooltip = "Approve";
            // 
            // btnReject
            // 
            this.btnReject.Name = "btnReject";
            this.btnReject.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlR);
            this.btnReject.Text = "Reject";
            // 
            // btnDocuments
            // 
            this.btnDocuments.Name = "btnDocuments";
            this.btnDocuments.Text = "Documents";
            this.btnDocuments.Tooltip = "Documents";
            // 
            // buttonItem1
            // 
            this.buttonItem1.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem1.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem1.Image")));
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem2,
            this.buttonItem3,
            this.buttonItem4,
            this.buttonItem5,
            this.buttonItem6});
            this.buttonItem1.Text = "Actions";
            this.buttonItem1.Tooltip = "Actions";
            // 
            // buttonItem2
            // 
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlH);
            this.buttonItem2.Text = "Installments";
            this.buttonItem2.Tooltip = "Installments";
            // 
            // buttonItem3
            // 
            this.buttonItem3.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem3.Image")));
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.buttonItem3.Text = "Expense";
            this.buttonItem3.Tooltip = "Expense";
            // 
            // buttonItem4
            // 
            this.buttonItem4.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.buttonItem4.Text = "Approve";
            this.buttonItem4.Tooltip = "Approve";
            // 
            // buttonItem5
            // 
            this.buttonItem5.Name = "buttonItem5";
            this.buttonItem5.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlR);
            this.buttonItem5.Text = "Reject";
            // 
            // buttonItem6
            // 
            this.buttonItem6.Name = "buttonItem6";
            this.buttonItem6.Text = "Documents";
            this.buttonItem6.Tooltip = "Documents";
            // 
            // buttonItem7
            // 
            this.buttonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem7.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem7.Image")));
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem8,
            this.buttonItem9,
            this.buttonItem10,
            this.buttonItem11,
            this.buttonItem12});
            this.buttonItem7.Text = "Actions";
            this.buttonItem7.Tooltip = "Actions";
            // 
            // buttonItem8
            // 
            this.buttonItem8.Name = "buttonItem8";
            this.buttonItem8.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlH);
            this.buttonItem8.Text = "Installments";
            this.buttonItem8.Tooltip = "Installments";
            // 
            // buttonItem9
            // 
            this.buttonItem9.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem9.Image")));
            this.buttonItem9.Name = "buttonItem9";
            this.buttonItem9.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.buttonItem9.Text = "Expense";
            this.buttonItem9.Tooltip = "Expense";
            // 
            // buttonItem10
            // 
            this.buttonItem10.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem10.Name = "buttonItem10";
            this.buttonItem10.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.buttonItem10.Text = "Approve";
            this.buttonItem10.Tooltip = "Approve";
            // 
            // buttonItem11
            // 
            this.buttonItem11.Name = "buttonItem11";
            this.buttonItem11.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlR);
            this.buttonItem11.Text = "Reject";
            // 
            // buttonItem12
            // 
            this.buttonItem12.Name = "buttonItem12";
            this.buttonItem12.Text = "Documents";
            this.buttonItem12.Tooltip = "Documents";
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel1,
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 451);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(454, 22);
            this.ssStatus.TabIndex = 311;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent;
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(48, 17);
            this.ToolStripStatusLabel1.Text = "Status : ";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblExpRetDate);
            this.panel2.Controls.Add(this.lblReceiptIssue);
            this.panel2.Controls.Add(label5);
            this.panel2.Controls.Add(this.lblDocumentStatus);
            this.panel2.Controls.Add(this.dtpExpectedReturnDate);
            this.panel2.Controls.Add(this.BtnNewDocumentStatus);
            this.panel2.Controls.Add(this.cboDocumentStatus);
            this.panel2.Controls.Add(this.lblBinNumber);
            this.panel2.Controls.Add(this.BtnBinReference);
            this.panel2.Controls.Add(this.cboBinNumber);
            this.panel2.Controls.Add(this.lblReceivedBy);
            this.panel2.Controls.Add(this.CboIssueReason);
            this.panel2.Controls.Add(this.BtnIssueReason);
            this.panel2.Controls.Add(this.cboCustodian);
            this.panel2.Controls.Add(this.lblTransactionDate);
            this.panel2.Controls.Add(this.dtpTransactionDate);
            this.panel2.Controls.Add(this.txtRemarks);
            this.panel2.Controls.Add(this.shapeContainer1);
            this.panel2.Location = new System.Drawing.Point(3, 195);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(445, 222);
            this.panel2.TabIndex = 313;
            // 
            // lblExpRetDate
            // 
            this.lblExpRetDate.AutoSize = true;
            this.lblExpRetDate.Location = new System.Drawing.Point(20, 126);
            this.lblExpRetDate.Name = "lblExpRetDate";
            this.lblExpRetDate.Size = new System.Drawing.Size(113, 13);
            this.lblExpRetDate.TabIndex = 144;
            this.lblExpRetDate.Text = "Expected Return Date";
            // 
            // lblReceiptIssue
            // 
            this.lblReceiptIssue.AutoSize = true;
            this.lblReceiptIssue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceiptIssue.Location = new System.Drawing.Point(7, -2);
            this.lblReceiptIssue.Name = "lblReceiptIssue";
            this.lblReceiptIssue.Size = new System.Drawing.Size(76, 13);
            this.lblReceiptIssue.TabIndex = 143;
            this.lblReceiptIssue.Text = "Receipt Info";
            // 
            // lblDocumentStatus
            // 
            this.lblDocumentStatus.AutoSize = true;
            this.lblDocumentStatus.Location = new System.Drawing.Point(20, 74);
            this.lblDocumentStatus.Name = "lblDocumentStatus";
            this.lblDocumentStatus.Size = new System.Drawing.Size(89, 13);
            this.lblDocumentStatus.TabIndex = 140;
            this.lblDocumentStatus.Text = "Document Status";
            // 
            // dtpExpectedReturnDate
            // 
            this.dtpExpectedReturnDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpExpectedReturnDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpectedReturnDate.Location = new System.Drawing.Point(146, 126);
            this.dtpExpectedReturnDate.Name = "dtpExpectedReturnDate";
            this.dtpExpectedReturnDate.Size = new System.Drawing.Size(99, 20);
            this.dtpExpectedReturnDate.TabIndex = 14;
            this.dtpExpectedReturnDate.ValueChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // BtnNewDocumentStatus
            // 
            this.BtnNewDocumentStatus.Location = new System.Drawing.Point(404, 69);
            this.BtnNewDocumentStatus.Name = "BtnNewDocumentStatus";
            this.BtnNewDocumentStatus.Size = new System.Drawing.Size(26, 23);
            this.BtnNewDocumentStatus.TabIndex = 10;
            this.BtnNewDocumentStatus.Text = "...";
            this.BtnNewDocumentStatus.UseVisualStyleBackColor = true;
            this.BtnNewDocumentStatus.Click += new System.EventHandler(this.BtnNewDocumentStatus_Click);
            // 
            // cboDocumentStatus
            // 
            this.cboDocumentStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDocumentStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDocumentStatus.BackColor = System.Drawing.SystemColors.Info;
            this.cboDocumentStatus.DropDownHeight = 134;
            this.cboDocumentStatus.FormattingEnabled = true;
            this.cboDocumentStatus.IntegralHeight = false;
            this.cboDocumentStatus.Location = new System.Drawing.Point(146, 69);
            this.cboDocumentStatus.Name = "cboDocumentStatus";
            this.cboDocumentStatus.Size = new System.Drawing.Size(252, 21);
            this.cboDocumentStatus.TabIndex = 8;
            this.cboDocumentStatus.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.cboDocumentStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyDown);
            // 
            // lblBinNumber
            // 
            this.lblBinNumber.AutoSize = true;
            this.lblBinNumber.Location = new System.Drawing.Point(20, 100);
            this.lblBinNumber.Name = "lblBinNumber";
            this.lblBinNumber.Size = new System.Drawing.Size(62, 13);
            this.lblBinNumber.TabIndex = 136;
            this.lblBinNumber.Text = "Bin Number";
            // 
            // BtnBinReference
            // 
            this.BtnBinReference.Location = new System.Drawing.Point(404, 99);
            this.BtnBinReference.Name = "BtnBinReference";
            this.BtnBinReference.Size = new System.Drawing.Size(26, 23);
            this.BtnBinReference.TabIndex = 13;
            this.BtnBinReference.Text = "...";
            this.BtnBinReference.UseVisualStyleBackColor = true;
            this.BtnBinReference.Click += new System.EventHandler(this.BtnBinReference_Click);
            // 
            // cboBinNumber
            // 
            this.cboBinNumber.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBinNumber.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBinNumber.BackColor = System.Drawing.SystemColors.Info;
            this.cboBinNumber.DropDownHeight = 134;
            this.cboBinNumber.FormattingEnabled = true;
            this.cboBinNumber.IntegralHeight = false;
            this.cboBinNumber.Location = new System.Drawing.Point(146, 99);
            this.cboBinNumber.Name = "cboBinNumber";
            this.cboBinNumber.Size = new System.Drawing.Size(252, 21);
            this.cboBinNumber.TabIndex = 12;
            this.cboBinNumber.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.cboBinNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyDown);
            // 
            // lblReceivedBy
            // 
            this.lblReceivedBy.AutoSize = true;
            this.lblReceivedBy.Location = new System.Drawing.Point(20, 46);
            this.lblReceivedBy.Name = "lblReceivedBy";
            this.lblReceivedBy.Size = new System.Drawing.Size(68, 13);
            this.lblReceivedBy.TabIndex = 130;
            this.lblReceivedBy.Text = "Received By";
            // 
            // CboIssueReason
            // 
            this.CboIssueReason.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboIssueReason.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboIssueReason.BackColor = System.Drawing.SystemColors.Info;
            this.CboIssueReason.DropDownHeight = 134;
            this.CboIssueReason.FormattingEnabled = true;
            this.CboIssueReason.IntegralHeight = false;
            this.CboIssueReason.Location = new System.Drawing.Point(146, 69);
            this.CboIssueReason.Name = "CboIssueReason";
            this.CboIssueReason.Size = new System.Drawing.Size(252, 21);
            this.CboIssueReason.TabIndex = 9;
            this.CboIssueReason.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.CboIssueReason.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyDown);
            // 
            // BtnIssueReason
            // 
            this.BtnIssueReason.Location = new System.Drawing.Point(404, 69);
            this.BtnIssueReason.Name = "BtnIssueReason";
            this.BtnIssueReason.Size = new System.Drawing.Size(26, 23);
            this.BtnIssueReason.TabIndex = 11;
            this.BtnIssueReason.Text = "...";
            this.BtnIssueReason.UseVisualStyleBackColor = true;
            this.BtnIssueReason.Click += new System.EventHandler(this.BtnIssueReason_Click);
            // 
            // cboCustodian
            // 
            this.cboCustodian.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCustodian.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCustodian.BackColor = System.Drawing.SystemColors.Info;
            this.cboCustodian.DropDownHeight = 134;
            this.cboCustodian.FormattingEnabled = true;
            this.cboCustodian.IntegralHeight = false;
            this.cboCustodian.Location = new System.Drawing.Point(146, 42);
            this.cboCustodian.Name = "cboCustodian";
            this.cboCustodian.Size = new System.Drawing.Size(286, 21);
            this.cboCustodian.TabIndex = 7;
            this.cboCustodian.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.cboCustodian.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyDown);
            // 
            // lblTransactionDate
            // 
            this.lblTransactionDate.AutoSize = true;
            this.lblTransactionDate.Location = new System.Drawing.Point(20, 22);
            this.lblTransactionDate.Name = "lblTransactionDate";
            this.lblTransactionDate.Size = new System.Drawing.Size(70, 13);
            this.lblTransactionDate.TabIndex = 125;
            this.lblTransactionDate.Text = "Receipt Date";
            // 
            // dtpTransactionDate
            // 
            this.dtpTransactionDate.CalendarForeColor = System.Drawing.Color.Red;
            this.dtpTransactionDate.CalendarMonthBackground = System.Drawing.Color.Yellow;
            this.dtpTransactionDate.CalendarTitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.dtpTransactionDate.CalendarTitleForeColor = System.Drawing.Color.Navy;
            this.dtpTransactionDate.CalendarTrailingForeColor = System.Drawing.Color.Fuchsia;
            this.dtpTransactionDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpTransactionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransactionDate.Location = new System.Drawing.Point(146, 16);
            this.dtpTransactionDate.Name = "dtpTransactionDate";
            this.dtpTransactionDate.Size = new System.Drawing.Size(107, 20);
            this.dtpTransactionDate.TabIndex = 6;
            this.dtpTransactionDate.ValueChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2});
            this.shapeContainer1.Size = new System.Drawing.Size(445, 222);
            this.shapeContainer1.TabIndex = 142;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 38;
            this.lineShape2.X2 = 426;
            this.lineShape2.Y1 = 153;
            this.lineShape2.Y2 = 153;
            // 
            // frmDocumentMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(454, 473);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.DocumentsBindingNavigator);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDocumentMaster";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Documents";
            this.Load += new System.EventHandler(this.FormLoad);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClosingForm);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormKeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.DocumentsBindingNavigator)).EndInit();
            this.DocumentsBindingNavigator.ResumeLayout(false);
            this.DocumentsBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errDocuments)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer tmDocuments;
        private System.Windows.Forms.BindingNavigator DocumentsBindingNavigator;
        private System.Windows.Forms.ToolStripLabel bnCountItem;
        private System.Windows.Forms.ToolStripButton bnMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bnMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bnPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bnMoveNextItem;
        private System.Windows.Forms.ToolStripButton bnMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton bnAddNewItem;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        private System.Windows.Forms.ToolStripButton bnPrint;
        private System.Windows.Forms.ToolStripButton bnAlert;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton bnHelp;
        private System.Windows.Forms.ErrorProvider errDocuments;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboReferenceNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboOperationType;
        private System.Windows.Forms.ComboBox cboDocumentType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.TextBox txtDocumentNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private DevComponents.DotNetBar.ButtonItem btnActions;
        private DevComponents.DotNetBar.ButtonItem btnInstallments;
        private DevComponents.DotNetBar.ButtonItem btnExpense;
        private DevComponents.DotNetBar.ButtonItem btnApprove;
        private DevComponents.DotNetBar.ButtonItem btnReject;
        private DevComponents.DotNetBar.ButtonItem btnDocuments;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonItem buttonItem5;
        private DevComponents.DotNetBar.ButtonItem buttonItem6;
        private DevComponents.DotNetBar.ButtonItem buttonItem7;
        private DevComponents.DotNetBar.ButtonItem buttonItem8;
        private DevComponents.DotNetBar.ButtonItem buttonItem9;
        private DevComponents.DotNetBar.ButtonItem buttonItem10;
        private DevComponents.DotNetBar.ButtonItem buttonItem11;
        private DevComponents.DotNetBar.ButtonItem buttonItem12;
        private System.Windows.Forms.ToolStripDropDownButton bnMoreActions;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem mnuItemAttachDocuments;
        internal System.Windows.Forms.Button btnDocumentType;
        internal System.Windows.Forms.ToolStripButton bnCancel;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label lblReceivedBy;
        internal System.Windows.Forms.ComboBox CboIssueReason;
        internal System.Windows.Forms.Button BtnIssueReason;
        internal System.Windows.Forms.ComboBox cboCustodian;
        internal System.Windows.Forms.Label lblTransactionDate;
        internal System.Windows.Forms.Label lblDocumentStatus;
        internal System.Windows.Forms.DateTimePicker dtpExpectedReturnDate;
        internal System.Windows.Forms.Button BtnNewDocumentStatus;
        internal System.Windows.Forms.ComboBox cboDocumentStatus;
        internal System.Windows.Forms.Label lblBinNumber;
        internal System.Windows.Forms.Button BtnBinReference;
        internal System.Windows.Forms.ComboBox cboBinNumber;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private System.Windows.Forms.Label lblReceiptIssue;
        internal System.Windows.Forms.Label lblExpRetDate;
        internal System.Windows.Forms.DateTimePicker dtpTransactionDate;
        private System.Windows.Forms.ToolStripMenuItem receiptIssueToolStripMenuItem;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.DateTimePicker dtpExpiryDate;
        private System.Windows.Forms.CheckBox chkExpiryDate;
    }
}