﻿namespace MyBooksERP
{
    partial class frmStockRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStockRegister));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DockSite7 = new DevComponents.DotNetBar.DockSite();
            this.Bar1 = new DevComponents.DotNetBar.Bar();
            this.PrintStripButton = new DevComponents.DotNetBar.ButtonItem();
            this.CancelToolStripButton = new DevComponents.DotNetBar.ButtonItem();
            this.LabelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.cmbCompanyFilter = new DevComponents.DotNetBar.ComboBoxItem();
            this.HelpToolStripButton = new DevComponents.DotNetBar.ButtonItem();
            this.CustomizeItem1 = new DevComponents.DotNetBar.CustomizeItem();
            this.pnlLeft = new DevComponents.DotNetBar.PanelEx();
            this.wbDocs = new DevComponents.DotNetBar.Controls.WarningBox();
            this.gpSearch = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.cboDocumentType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.LabelX1 = new DevComponents.DotNetBar.LabelX();
            this.cboOperationType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.gpView = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.rbtnAll = new System.Windows.Forms.RadioButton();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.BtnIssue = new System.Windows.Forms.RadioButton();
            this.BtnReceipt = new System.Windows.Forms.RadioButton();
            this.LabelX4 = new DevComponents.DotNetBar.LabelX();
            this.expSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.GrpPanelDocMap = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dgvDocSummary = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Img = new System.Windows.Forms.DataGridViewImageColumn();
            this.clmdelete = new System.Windows.Forms.DataGridViewImageColumn();
            this.expandableSplitter1 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.pnlRight = new DevComponents.DotNetBar.PanelEx();
            this.dgvDocGrid = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.clmImage = new System.Windows.Forms.DataGridViewImageColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Custodian = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Print = new System.Windows.Forms.DataGridViewImageColumn();
            this.BarNavigation = new DevComponents.DotNetBar.Bar();
            this.ToolStripStatusLabel = new DevComponents.DotNetBar.LabelItem();
            this.GridNavigation = new DevComponents.DotNetBar.ItemContainer();
            this.cboPageCount = new DevComponents.DotNetBar.ComboBoxItem();
            this.BtnFirst = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrevious = new DevComponents.DotNetBar.ButtonItem();
            this.lblPageCount = new DevComponents.DotNetBar.LabelItem();
            this.BtnNext = new DevComponents.DotNetBar.ButtonItem();
            this.BtnLast = new DevComponents.DotNetBar.ButtonItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.In = new System.Windows.Forms.ToolStripMenuItem();
            this.Out = new System.Windows.Forms.ToolStripMenuItem();
            this.BindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DockSite7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Bar1)).BeginInit();
            this.pnlLeft.SuspendLayout();
            this.gpSearch.SuspendLayout();
            this.gpView.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.GrpPanelDocMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocSummary)).BeginInit();
            this.pnlRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarNavigation)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // DockSite7
            // 
            this.DockSite7.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite7.Controls.Add(this.Bar1);
            this.DockSite7.Dock = System.Windows.Forms.DockStyle.Top;
            this.DockSite7.Location = new System.Drawing.Point(0, 0);
            this.DockSite7.Name = "DockSite7";
            this.DockSite7.Size = new System.Drawing.Size(1225, 28);
            this.DockSite7.TabIndex = 77;
            this.DockSite7.TabStop = false;
            // 
            // Bar1
            // 
            this.Bar1.AccessibleDescription = "DotNetBar Bar (Bar1)";
            this.Bar1.AccessibleName = "DotNetBar Bar";
            this.Bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.Bar1.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.Bar1.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.Bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.PrintStripButton,
            this.CancelToolStripButton,
            this.LabelItem1,
            this.cmbCompanyFilter,
            this.HelpToolStripButton,
            this.CustomizeItem1});
            this.Bar1.Location = new System.Drawing.Point(0, 0);
            this.Bar1.Name = "Bar1";
            this.Bar1.Size = new System.Drawing.Size(384, 28);
            this.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.Bar1.TabIndex = 0;
            this.Bar1.TabStop = false;
            this.Bar1.Text = "Bar1";
            // 
            // PrintStripButton
            // 
            this.PrintStripButton.Name = "PrintStripButton";
            this.PrintStripButton.Text = "Print";
            this.PrintStripButton.Tooltip = "Print";
            this.PrintStripButton.Visible = false;
            // 
            // CancelToolStripButton
            // 
            this.CancelToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelToolStripButton.Image")));
            this.CancelToolStripButton.Name = "CancelToolStripButton";
            this.CancelToolStripButton.Text = "Clear Filter";
            this.CancelToolStripButton.Tooltip = "Remove Filter";
            this.CancelToolStripButton.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // LabelItem1
            // 
            this.LabelItem1.BeginGroup = true;
            this.LabelItem1.Name = "LabelItem1";
            this.LabelItem1.PaddingBottom = 2;
            this.LabelItem1.PaddingLeft = 10;
            this.LabelItem1.PaddingRight = 2;
            this.LabelItem1.Text = "<b> Show by</b>";
            this.LabelItem1.Visible = false;
            // 
            // cmbCompanyFilter
            // 
            this.cmbCompanyFilter.ComboWidth = 200;
            this.cmbCompanyFilter.DropDownHeight = 106;
            this.cmbCompanyFilter.ItemHeight = 17;
            this.cmbCompanyFilter.Name = "cmbCompanyFilter";
            this.cmbCompanyFilter.Stretch = true;
            this.cmbCompanyFilter.Visible = false;
            this.cmbCompanyFilter.WatermarkColor = System.Drawing.SystemColors.ControlDarkDark;
            this.cmbCompanyFilter.WatermarkText = "All <b>Companies</b> && <b>Branches</b>";
            // 
            // HelpToolStripButton
            // 
            this.HelpToolStripButton.BeginGroup = true;
            this.HelpToolStripButton.Name = "HelpToolStripButton";
            this.HelpToolStripButton.Text = "Help";
            this.HelpToolStripButton.Tooltip = "Help";
            // 
            // CustomizeItem1
            // 
            this.CustomizeItem1.CustomizeItemVisible = false;
            this.CustomizeItem1.Name = "CustomizeItem1";
            this.CustomizeItem1.Text = "&Add or Remove Buttons";
            this.CustomizeItem1.Tooltip = "Bar Options";
            // 
            // pnlLeft
            // 
            this.pnlLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlLeft.Controls.Add(this.wbDocs);
            this.pnlLeft.Controls.Add(this.gpSearch);
            this.pnlLeft.Controls.Add(this.gpView);
            this.pnlLeft.Controls.Add(this.LabelX4);
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 28);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(195, 503);
            this.pnlLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlLeft.Style.GradientAngle = 90;
            this.pnlLeft.TabIndex = 78;
            // 
            // wbDocs
            // 
            this.wbDocs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wbDocs.AutoScroll = true;
            this.wbDocs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.wbDocs.CloseButtonVisible = false;
            this.wbDocs.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wbDocs.Location = new System.Drawing.Point(8, 327);
            this.wbDocs.Name = "wbDocs";
            this.wbDocs.OptionsButtonVisible = false;
            this.wbDocs.OptionsText = "More Tips!";
            this.wbDocs.Size = new System.Drawing.Size(179, 167);
            this.wbDocs.TabIndex = 34;
            this.wbDocs.Text = resources.GetString("wbDocs.Text");
            // 
            // gpSearch
            // 
            this.gpSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gpSearch.CanvasColor = System.Drawing.SystemColors.Control;
            this.gpSearch.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.gpSearch.Controls.Add(this.labelX2);
            this.gpSearch.Controls.Add(this.cboDocumentType);
            this.gpSearch.Controls.Add(this.LabelX1);
            this.gpSearch.Controls.Add(this.cboOperationType);
            this.gpSearch.Location = new System.Drawing.Point(8, 139);
            this.gpSearch.Name = "gpSearch";
            this.gpSearch.Size = new System.Drawing.Size(179, 179);
            // 
            // 
            // 
            this.gpSearch.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.gpSearch.Style.BackColorGradientAngle = 90;
            this.gpSearch.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.gpSearch.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpSearch.Style.BorderBottomWidth = 1;
            this.gpSearch.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.gpSearch.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpSearch.Style.BorderLeftWidth = 1;
            this.gpSearch.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpSearch.Style.BorderRightWidth = 1;
            this.gpSearch.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpSearch.Style.BorderTopWidth = 1;
            this.gpSearch.Style.Class = "";
            this.gpSearch.Style.CornerDiameter = 4;
            this.gpSearch.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.gpSearch.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.gpSearch.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.gpSearch.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.gpSearch.StyleMouseDown.Class = "";
            this.gpSearch.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gpSearch.StyleMouseOver.Class = "";
            this.gpSearch.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gpSearch.TabIndex = 33;
            this.gpSearch.Text = "Search";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.Class = "";
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(3, 55);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(90, 15);
            this.labelX2.TabIndex = 28;
            this.labelX2.Text = "Document Type";
            // 
            // cboDocumentType
            // 
            this.cboDocumentType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboDocumentType.DisplayMember = "Text";
            this.cboDocumentType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDocumentType.FormattingEnabled = true;
            this.cboDocumentType.ItemHeight = 14;
            this.cboDocumentType.Location = new System.Drawing.Point(3, 78);
            this.cboDocumentType.Name = "cboDocumentType";
            this.cboDocumentType.Size = new System.Drawing.Size(167, 20);
            this.cboDocumentType.TabIndex = 27;
            this.cboDocumentType.SelectedIndexChanged += new System.EventHandler(this.rbtnAll_CheckedChanged);
            // 
            // LabelX1
            // 
            // 
            // 
            // 
            this.LabelX1.BackgroundStyle.Class = "";
            this.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX1.Location = new System.Drawing.Point(3, 3);
            this.LabelX1.Name = "LabelX1";
            this.LabelX1.Size = new System.Drawing.Size(36, 15);
            this.LabelX1.TabIndex = 26;
            this.LabelX1.Text = "Type";
            // 
            // cboOperationType
            // 
            this.cboOperationType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboOperationType.DisplayMember = "Text";
            this.cboOperationType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboOperationType.FormattingEnabled = true;
            this.cboOperationType.ItemHeight = 14;
            this.cboOperationType.Location = new System.Drawing.Point(3, 25);
            this.cboOperationType.Name = "cboOperationType";
            this.cboOperationType.Size = new System.Drawing.Size(167, 20);
            this.cboOperationType.TabIndex = 25;
            this.cboOperationType.SelectedIndexChanged += new System.EventHandler(this.rbtnAll_CheckedChanged);
            // 
            // gpView
            // 
            this.gpView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gpView.CanvasColor = System.Drawing.SystemColors.Control;
            this.gpView.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.gpView.Controls.Add(this.rbtnAll);
            this.gpView.Controls.Add(this.BtnIssue);
            this.gpView.Controls.Add(this.BtnReceipt);
            this.gpView.Location = new System.Drawing.Point(8, 29);
            this.gpView.Name = "gpView";
            this.gpView.Size = new System.Drawing.Size(179, 100);
            // 
            // 
            // 
            this.gpView.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.gpView.Style.BackColorGradientAngle = 90;
            this.gpView.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.gpView.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpView.Style.BorderBottomWidth = 1;
            this.gpView.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.gpView.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpView.Style.BorderLeftWidth = 1;
            this.gpView.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpView.Style.BorderRightWidth = 1;
            this.gpView.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpView.Style.BorderTopWidth = 1;
            this.gpView.Style.Class = "";
            this.gpView.Style.CornerDiameter = 4;
            this.gpView.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.gpView.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.gpView.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.gpView.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.gpView.StyleMouseDown.Class = "";
            this.gpView.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gpView.StyleMouseOver.Class = "";
            this.gpView.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gpView.TabIndex = 32;
            this.gpView.Text = "View";
            // 
            // rbtnAll
            // 
            this.rbtnAll.AutoSize = true;
            this.rbtnAll.Checked = true;
            this.rbtnAll.ImageIndex = 3;
            this.rbtnAll.ImageList = this.ImageList1;
            this.rbtnAll.Location = new System.Drawing.Point(12, 3);
            this.rbtnAll.Name = "rbtnAll";
            this.rbtnAll.Size = new System.Drawing.Size(97, 17);
            this.rbtnAll.TabIndex = 22;
            this.rbtnAll.TabStop = true;
            this.rbtnAll.Text = "Stock Book";
            this.rbtnAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbtnAll.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rbtnAll.UseVisualStyleBackColor = true;
            this.rbtnAll.CheckedChanged += new System.EventHandler(this.rbtnAll_CheckedChanged);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "DocumentIn.PNG");
            this.ImageList1.Images.SetKeyName(1, "DocumentOut.PNG");
            this.ImageList1.Images.SetKeyName(2, "Add.png");
            this.ImageList1.Images.SetKeyName(3, "Stock Book.png");
            // 
            // BtnIssue
            // 
            this.BtnIssue.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnIssue.ImageIndex = 1;
            this.BtnIssue.ImageList = this.ImageList1;
            this.BtnIssue.Location = new System.Drawing.Point(12, 52);
            this.BtnIssue.Name = "BtnIssue";
            this.BtnIssue.Size = new System.Drawing.Size(186, 28);
            this.BtnIssue.TabIndex = 20;
            this.BtnIssue.Text = "Docs Issued";
            this.BtnIssue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnIssue.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnIssue.UseVisualStyleBackColor = true;
            this.BtnIssue.CheckedChanged += new System.EventHandler(this.rbtnAll_CheckedChanged);
            // 
            // BtnReceipt
            // 
            this.BtnReceipt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnReceipt.ImageIndex = 0;
            this.BtnReceipt.ImageList = this.ImageList1;
            this.BtnReceipt.Location = new System.Drawing.Point(12, 26);
            this.BtnReceipt.Name = "BtnReceipt";
            this.BtnReceipt.Size = new System.Drawing.Size(186, 28);
            this.BtnReceipt.TabIndex = 21;
            this.BtnReceipt.Text = "Docs Available";
            this.BtnReceipt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnReceipt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnReceipt.UseVisualStyleBackColor = true;
            this.BtnReceipt.CheckedChanged += new System.EventHandler(this.rbtnAll_CheckedChanged);
            // 
            // LabelX4
            // 
            // 
            // 
            // 
            this.LabelX4.BackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarCaptionBackground2;
            this.LabelX4.BackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarCaptionBackground;
            this.LabelX4.BackgroundStyle.Class = "";
            this.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX4.Dock = System.Windows.Forms.DockStyle.Top;
            this.LabelX4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelX4.ForeColor = System.Drawing.Color.White;
            this.LabelX4.Location = new System.Drawing.Point(0, 0);
            this.LabelX4.Name = "LabelX4";
            this.LabelX4.Size = new System.Drawing.Size(195, 20);
            this.LabelX4.TabIndex = 31;
            this.LabelX4.Text = " Documents";
            // 
            // expSplitterLeft
            // 
            this.expSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expSplitterLeft.ExpandableControl = this.pnlLeft;
            this.expSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expSplitterLeft.Location = new System.Drawing.Point(195, 28);
            this.expSplitterLeft.Name = "expSplitterLeft";
            this.expSplitterLeft.Size = new System.Drawing.Size(2, 503);
            this.expSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expSplitterLeft.TabIndex = 81;
            this.expSplitterLeft.TabStop = false;
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottom.Controls.Add(this.GrpPanelDocMap);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(197, 358);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1028, 173);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 86;
            // 
            // GrpPanelDocMap
            // 
            this.GrpPanelDocMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.GrpPanelDocMap.CanvasColor = System.Drawing.SystemColors.Control;
            this.GrpPanelDocMap.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.GrpPanelDocMap.Controls.Add(this.dgvDocSummary);
            this.GrpPanelDocMap.Location = new System.Drawing.Point(7, 9);
            this.GrpPanelDocMap.Name = "GrpPanelDocMap";
            this.GrpPanelDocMap.Size = new System.Drawing.Size(1015, 155);
            // 
            // 
            // 
            this.GrpPanelDocMap.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.GrpPanelDocMap.Style.BackColorGradientAngle = 90;
            this.GrpPanelDocMap.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.GrpPanelDocMap.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpPanelDocMap.Style.BorderBottomWidth = 1;
            this.GrpPanelDocMap.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.GrpPanelDocMap.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpPanelDocMap.Style.BorderLeftWidth = 1;
            this.GrpPanelDocMap.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpPanelDocMap.Style.BorderRightWidth = 1;
            this.GrpPanelDocMap.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpPanelDocMap.Style.BorderTopWidth = 1;
            this.GrpPanelDocMap.Style.Class = "";
            this.GrpPanelDocMap.Style.CornerDiameter = 4;
            this.GrpPanelDocMap.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.GrpPanelDocMap.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.GrpPanelDocMap.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.GrpPanelDocMap.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.GrpPanelDocMap.StyleMouseDown.Class = "";
            this.GrpPanelDocMap.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.GrpPanelDocMap.StyleMouseOver.Class = "";
            this.GrpPanelDocMap.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.GrpPanelDocMap.TabIndex = 2;
            this.GrpPanelDocMap.Text = "Document Map";
            // 
            // dgvDocSummary
            // 
            this.dgvDocSummary.AllowUserToAddRows = false;
            this.dgvDocSummary.AllowUserToDeleteRows = false;
            this.dgvDocSummary.AllowUserToResizeRows = false;
            this.dgvDocSummary.BackgroundColor = System.Drawing.Color.White;
            this.dgvDocSummary.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvDocSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocSummary.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Img,
            this.clmdelete});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDocSummary.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvDocSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDocSummary.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvDocSummary.Location = new System.Drawing.Point(0, 0);
            this.dgvDocSummary.Name = "dgvDocSummary";
            this.dgvDocSummary.ReadOnly = true;
            this.dgvDocSummary.RowHeadersVisible = false;
            this.dgvDocSummary.Size = new System.Drawing.Size(1009, 134);
            this.dgvDocSummary.TabIndex = 0;
            this.dgvDocSummary.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvDocSummary_CellMouseClick);
            this.dgvDocSummary.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvDocSummary_DataError);
            // 
            // Img
            // 
            this.Img.DataPropertyName = "Img";
            this.Img.HeaderText = "";
            this.Img.Name = "Img";
            this.Img.ReadOnly = true;
            this.Img.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Img.ToolTipText = "Status";
            this.Img.Visible = false;
            this.Img.Width = 20;
            // 
            // clmdelete
            // 
            this.clmdelete.DataPropertyName = "clmdelete";
            this.clmdelete.Description = "Delete";
            this.clmdelete.HeaderText = "";
            this.clmdelete.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.clmdelete.Name = "clmdelete";
            this.clmdelete.ReadOnly = true;
            this.clmdelete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clmdelete.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.clmdelete.ToolTipText = "Delete";
            this.clmdelete.Width = 30;
            // 
            // expandableSplitter1
            // 
            this.expandableSplitter1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter1.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.expandableSplitter1.ExpandableControl = this.pnlBottom;
            this.expandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitter1.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.Location = new System.Drawing.Point(197, 356);
            this.expandableSplitter1.Name = "expandableSplitter1";
            this.expandableSplitter1.Size = new System.Drawing.Size(1028, 2);
            this.expandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitter1.TabIndex = 87;
            this.expandableSplitter1.TabStop = false;
            // 
            // pnlRight
            // 
            this.pnlRight.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlRight.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlRight.Controls.Add(this.dgvDocGrid);
            this.pnlRight.Controls.Add(this.BarNavigation);
            this.pnlRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRight.Location = new System.Drawing.Point(197, 28);
            this.pnlRight.Name = "pnlRight";
            this.pnlRight.Size = new System.Drawing.Size(1028, 328);
            this.pnlRight.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlRight.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlRight.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlRight.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlRight.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlRight.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlRight.Style.GradientAngle = 90;
            this.pnlRight.TabIndex = 88;
            // 
            // dgvDocGrid
            // 
            this.dgvDocGrid.AllowUserToAddRows = false;
            this.dgvDocGrid.AllowUserToDeleteRows = false;
            this.dgvDocGrid.AllowUserToResizeRows = false;
            this.dgvDocGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDocGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDocGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvDocGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmImage,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Custodian,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column7,
            this.DocumentID,
            this.StatusID,
            this.RecordID,
            this.Print});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDocGrid.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvDocGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDocGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvDocGrid.Location = new System.Drawing.Point(0, 0);
            this.dgvDocGrid.MultiSelect = false;
            this.dgvDocGrid.Name = "dgvDocGrid";
            this.dgvDocGrid.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDocGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvDocGrid.RowHeadersVisible = false;
            this.dgvDocGrid.Size = new System.Drawing.Size(1028, 294);
            this.dgvDocGrid.TabIndex = 33;
            this.dgvDocGrid.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvDocGrid_CellMouseClick);
            this.dgvDocGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvDocGrid_DataError);
            // 
            // clmImage
            // 
            this.clmImage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.clmImage.DataPropertyName = "clmImage";
            this.clmImage.Description = "In";
            this.clmImage.HeaderText = "";
            this.clmImage.Name = "clmImage";
            this.clmImage.ReadOnly = true;
            this.clmImage.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clmImage.ToolTipText = "Status";
            this.clmImage.Width = 25;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Type";
            this.Column2.FillWeight = 99.59214F;
            this.Column2.HeaderText = "Type";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Reference";
            this.Column3.FillWeight = 99.59214F;
            this.Column3.HeaderText = "Reference";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "DocumentType";
            this.Column4.FillWeight = 130.4792F;
            this.Column4.HeaderText = "Document Type";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "DocumentNumber";
            this.Column5.FillWeight = 99.59214F;
            this.Column5.HeaderText = "Document Number";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Date";
            this.Column6.FillWeight = 99.59214F;
            this.Column6.HeaderText = "Date";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Custodian
            // 
            this.Custodian.DataPropertyName = "Custodian";
            this.Custodian.FillWeight = 99.59214F;
            this.Custodian.HeaderText = "Custodian";
            this.Custodian.Name = "Custodian";
            this.Custodian.ReadOnly = true;
            this.Custodian.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "DocumentStatus";
            this.Column8.FillWeight = 122.4704F;
            this.Column8.HeaderText = "Document Status";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "BinDetails";
            this.Column9.FillWeight = 99.59214F;
            this.Column9.HeaderText = "Bin Details";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "IssueReason";
            this.Column10.FillWeight = 99.59214F;
            this.Column10.HeaderText = "Issue Reason";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "ReturnDate";
            this.Column11.FillWeight = 99.59214F;
            this.Column11.HeaderText = "Return Date";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Remarks";
            this.Column7.FillWeight = 99.59214F;
            this.Column7.HeaderText = "Remarks";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // DocumentID
            // 
            this.DocumentID.DataPropertyName = "DocumentID";
            this.DocumentID.HeaderText = "DocumentID";
            this.DocumentID.Name = "DocumentID";
            this.DocumentID.ReadOnly = true;
            this.DocumentID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DocumentID.Visible = false;
            // 
            // StatusID
            // 
            this.StatusID.DataPropertyName = "StatusID";
            this.StatusID.HeaderText = "StatusID";
            this.StatusID.Name = "StatusID";
            this.StatusID.ReadOnly = true;
            this.StatusID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.StatusID.Visible = false;
            // 
            // RecordID
            // 
            this.RecordID.DataPropertyName = "RecordID";
            this.RecordID.HeaderText = "RecordID";
            this.RecordID.Name = "RecordID";
            this.RecordID.ReadOnly = true;
            this.RecordID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RecordID.Visible = false;
            // 
            // Print
            // 
            this.Print.FillWeight = 97.52271F;
            this.Print.HeaderText = "";
            this.Print.Image = global::MyBooksERP.Properties.Resources.Print;
            this.Print.Name = "Print";
            this.Print.ReadOnly = true;
            this.Print.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // BarNavigation
            // 
            this.BarNavigation.AccessibleDescription = "DotNetBar Bar (BarNavigation)";
            this.BarNavigation.AccessibleName = "DotNetBar Bar";
            this.BarNavigation.AccessibleRole = System.Windows.Forms.AccessibleRole.StatusBar;
            this.BarNavigation.BackColor = System.Drawing.Color.White;
            this.BarNavigation.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.BarNavigation.CanCustomize = false;
            this.BarNavigation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BarNavigation.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.BarNavigation.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.BarNavigation.DockTabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Right;
            this.BarNavigation.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.ResizeHandle;
            this.BarNavigation.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ToolStripStatusLabel,
            this.GridNavigation});
            this.BarNavigation.Location = new System.Drawing.Point(0, 294);
            this.BarNavigation.Name = "BarNavigation";
            this.BarNavigation.Size = new System.Drawing.Size(1028, 34);
            this.BarNavigation.Stretch = true;
            this.BarNavigation.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.BarNavigation.TabIndex = 32;
            this.BarNavigation.TabStop = false;
            this.BarNavigation.Text = "Navigation";
            // 
            // ToolStripStatusLabel
            // 
            this.ToolStripStatusLabel.CanCustomize = false;
            this.ToolStripStatusLabel.Name = "ToolStripStatusLabel";
            this.ToolStripStatusLabel.PaddingLeft = 5;
            this.ToolStripStatusLabel.SingleLineColor = System.Drawing.SystemColors.MenuHighlight;
            this.ToolStripStatusLabel.Stretch = true;
            // 
            // GridNavigation
            // 
            // 
            // 
            // 
            this.GridNavigation.BackgroundStyle.Class = "";
            this.GridNavigation.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.GridNavigation.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.GridNavigation.Name = "GridNavigation";
            this.GridNavigation.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.cboPageCount,
            this.BtnFirst,
            this.BtnPrevious,
            this.lblPageCount,
            this.BtnNext,
            this.BtnLast});
            this.GridNavigation.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // cboPageCount
            // 
            this.cboPageCount.DropDownHeight = 106;
            this.cboPageCount.ItemHeight = 17;
            this.cboPageCount.Name = "cboPageCount";
            this.cboPageCount.SelectedIndexChanged += new System.EventHandler(this.cboPageCount_SelectedIndexChanged);
            // 
            // BtnFirst
            // 
            this.BtnFirst.Image = global::MyBooksERP.Properties.Resources.ArrowLeftStart1;
            this.BtnFirst.Name = "BtnFirst";
            this.BtnFirst.Text = "Move First";
            this.BtnFirst.Tooltip = "Move First";
            this.BtnFirst.Click += new System.EventHandler(this.BtnFirst_Click);
            // 
            // BtnPrevious
            // 
            this.BtnPrevious.Image = global::MyBooksERP.Properties.Resources.ArrowLeft1;
            this.BtnPrevious.Name = "BtnPrevious";
            this.BtnPrevious.Text = "Move Previous";
            this.BtnPrevious.Tooltip = "Move Previous";
            this.BtnPrevious.Click += new System.EventHandler(this.BtnPrevious_Click);
            // 
            // lblPageCount
            // 
            this.lblPageCount.BeginGroup = true;
            this.lblPageCount.Name = "lblPageCount";
            this.lblPageCount.PaddingLeft = 3;
            this.lblPageCount.PaddingRight = 3;
            this.lblPageCount.Text = "{0} of {0}";
            // 
            // BtnNext
            // 
            this.BtnNext.BeginGroup = true;
            this.BtnNext.Image = global::MyBooksERP.Properties.Resources.ArrowRight1;
            this.BtnNext.Name = "BtnNext";
            this.BtnNext.Text = "Next";
            this.BtnNext.Tooltip = "Move Next";
            this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // BtnLast
            // 
            this.BtnLast.Image = global::MyBooksERP.Properties.Resources.ArrowRightStart1;
            this.BtnLast.Name = "BtnLast";
            this.BtnLast.Text = "Last";
            this.BtnLast.Tooltip = "Move Last";
            this.BtnLast.Click += new System.EventHandler(this.BtnLast_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.In,
            this.Out});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(95, 48);
            // 
            // In
            // 
            this.In.Image = global::MyBooksERP.Properties.Resources.DocumentIn;
            this.In.Name = "In";
            this.In.Size = new System.Drawing.Size(94, 22);
            this.In.Text = "In";
            // 
            // Out
            // 
            this.Out.Image = global::MyBooksERP.Properties.Resources.DocumentOut;
            this.Out.Name = "Out";
            this.Out.Size = new System.Drawing.Size(94, 22);
            this.Out.Text = "Out";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Type";
            this.dataGridViewTextBoxColumn1.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Type";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 90;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Reference";
            this.dataGridViewTextBoxColumn2.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Reference";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 90;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "DocumentType";
            this.dataGridViewTextBoxColumn3.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Document Type";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 90;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "DocumentNumber";
            this.dataGridViewTextBoxColumn4.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Document Number";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 90;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Date";
            this.dataGridViewTextBoxColumn5.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn5.HeaderText = "Date";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 90;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Custodian";
            this.dataGridViewTextBoxColumn6.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn6.HeaderText = "Custodian";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 90;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "DocumentStatus";
            this.dataGridViewTextBoxColumn7.FillWeight = 139.5349F;
            this.dataGridViewTextBoxColumn7.HeaderText = "DocumentStatus";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 90;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "BinDetails";
            this.dataGridViewTextBoxColumn8.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn8.HeaderText = "Bin Details";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 90;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "IssueReason";
            this.dataGridViewTextBoxColumn9.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn9.HeaderText = "IssueReason";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 90;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "ReturnDate";
            this.dataGridViewTextBoxColumn10.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn10.HeaderText = "ReturnDate";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Width = 90;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Remarks";
            this.dataGridViewTextBoxColumn11.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn11.HeaderText = "Remarks";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 79;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "DocumentID";
            this.dataGridViewTextBoxColumn12.HeaderText = "DocumentID";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Visible = false;
            this.dataGridViewTextBoxColumn12.Width = 13;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "StatusID";
            this.dataGridViewTextBoxColumn13.HeaderText = "StatusID";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Visible = false;
            this.dataGridViewTextBoxColumn13.Width = 13;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "RecordID";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Width = 75;
            // 
            // frmStockRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1225, 531);
            this.Controls.Add(this.pnlRight);
            this.Controls.Add(this.expandableSplitter1);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.expSplitterLeft);
            this.Controls.Add(this.pnlLeft);
            this.Controls.Add(this.DockSite7);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmStockRegister";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Register";
            this.Load += new System.EventHandler(this.frmStockRegister_Load);
            this.Shown += new System.EventHandler(this.frmStockRegister_Shown);
            this.DockSite7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Bar1)).EndInit();
            this.pnlLeft.ResumeLayout(false);
            this.gpSearch.ResumeLayout(false);
            this.gpView.ResumeLayout(false);
            this.gpView.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            this.GrpPanelDocMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocSummary)).EndInit();
            this.pnlRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarNavigation)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.DockSite DockSite7;
        internal DevComponents.DotNetBar.Bar Bar1;
        internal DevComponents.DotNetBar.ButtonItem PrintStripButton;
        internal DevComponents.DotNetBar.ButtonItem CancelToolStripButton;
        internal DevComponents.DotNetBar.LabelItem LabelItem1;
        internal DevComponents.DotNetBar.ComboBoxItem cmbCompanyFilter;
        internal DevComponents.DotNetBar.ButtonItem HelpToolStripButton;
        internal DevComponents.DotNetBar.CustomizeItem CustomizeItem1;
        private DevComponents.DotNetBar.PanelEx pnlLeft;
        internal DevComponents.DotNetBar.ExpandableSplitter expSplitterLeft;
        private DevComponents.DotNetBar.PanelEx pnlBottom;
        internal DevComponents.DotNetBar.ExpandableSplitter expandableSplitter1;
        private DevComponents.DotNetBar.PanelEx pnlRight;
        internal DevComponents.DotNetBar.Bar BarNavigation;
        internal DevComponents.DotNetBar.LabelItem ToolStripStatusLabel;
        internal DevComponents.DotNetBar.ItemContainer GridNavigation;
        internal DevComponents.DotNetBar.ComboBoxItem cboPageCount;
        internal DevComponents.DotNetBar.ButtonItem BtnFirst;
        internal DevComponents.DotNetBar.ButtonItem BtnPrevious;
        internal DevComponents.DotNetBar.LabelItem lblPageCount;
        internal DevComponents.DotNetBar.ButtonItem BtnNext;
        internal DevComponents.DotNetBar.ButtonItem BtnLast;
        internal DevComponents.DotNetBar.Controls.GroupPanel GrpPanelDocMap;
        internal DevComponents.DotNetBar.Controls.DataGridViewX dgvDocSummary;
        internal DevComponents.DotNetBar.Controls.DataGridViewX dgvDocGrid;
        internal DevComponents.DotNetBar.Controls.WarningBox wbDocs;
        internal DevComponents.DotNetBar.Controls.GroupPanel gpSearch;
        internal DevComponents.DotNetBar.Controls.GroupPanel gpView;
        internal System.Windows.Forms.RadioButton rbtnAll;
        internal System.Windows.Forms.RadioButton BtnIssue;
        internal System.Windows.Forms.RadioButton BtnReceipt;
        internal DevComponents.DotNetBar.LabelX LabelX4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        internal System.Windows.Forms.ImageList ImageList1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem In;
        private System.Windows.Forms.ToolStripMenuItem Out;
        internal DevComponents.DotNetBar.LabelX LabelX1;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboOperationType;
        internal DevComponents.DotNetBar.LabelX labelX2;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboDocumentType;
        private System.Windows.Forms.DataGridViewImageColumn Img;
        private System.Windows.Forms.DataGridViewImageColumn clmdelete;
        internal System.Windows.Forms.BindingSource BindingSource1;
        private System.Windows.Forms.DataGridViewImageColumn clmImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Custodian;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusID;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordID;
        private System.Windows.Forms.DataGridViewImageColumn Print;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
    }
}