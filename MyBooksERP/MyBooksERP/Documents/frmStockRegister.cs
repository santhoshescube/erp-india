﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

/* *******************************************

Author      :		 <Laxmi>
Create date :        <13 Apr 2012>
Description :	     <Stock Register>
*******************************************
*/
namespace MyBooksERP
{
    public partial class frmStockRegister : DevComponents.DotNetBar.Office2007Form
    {

        #region Declarations

            string MstrCommonMessage;

            DataTable  dsTemp; // bind all documnts for grid page navigation 
            Boolean blnNoRecord;//  Record exists or not
            int iPageRows = 25;   // default pge setting
            String sNavClicked = "";// enum -First/Previous/Next/Last
            String RecordID1 = "0"; // Lower boundary 
            String RecordID2 = "0";// upper boundry
            int iCurrentPage = 1;// For Page label show
            int iTotalPage = 1;//For Page label show
            
            bool FlgBlk;

            // permissions
            private bool MblnViewPermission = false;
            private bool MblnAddPermission = false;//To Set Add Permission
            private bool MblnUpdatePermission = false;//To Set Update Permission
            private bool MblnDeletePermission = false;//To Set Delete Permission
            private bool MblnAddUpdatePermission = false; // To set add/update permission
            private bool MblnPrintEmailPermission = false;//To Set PrintEmail Permission

            private clsBLLDocumentMaster MobjClsBLLDocumentMaster =null;
            private clsMessage ObjUserMessage = null;

        #endregion
      
        #region Constructor
                public frmStockRegister()
                {
                    InitializeComponent();
                }
            #endregion

        #region Properties
                private clsMessage UserMessage
                {
                    get
                    {
                        if (this.ObjUserMessage == null)
                            this.ObjUserMessage = new clsMessage(FormID.Documents);
                        return this.ObjUserMessage;
                    }
                }

                private clsBLLDocumentMaster BLLDocumentMaster
                {
                    get
                    {
                        if (this.MobjClsBLLDocumentMaster == null)
                            this.MobjClsBLLDocumentMaster = new clsBLLDocumentMaster();
                        return this.MobjClsBLLDocumentMaster;
                    }
                }
        #endregion

        #region Events
                private void frmStockRegister_Load(object sender, EventArgs e)
                {
                    //SetPermissions();
                    LoadCombo();
                    cboOperationType.SelectedIndex = -1;
                    cboDocumentType.SelectedIndex = -1;
                   
                }

                private void dgvDocGrid_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
                {
                    if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                    {
                        ShowDocumentDetails(dgvDocGrid.Rows[e.RowIndex].Cells["DocumentID"].Value.ToInt32(),e.RowIndex);
                        if (e.ColumnIndex == 1)
                        {
                            LoadReport(dgvDocGrid.Rows[e.RowIndex].Cells["DocumentID"].Value.ToInt32(), dgvDocGrid.Rows[e.RowIndex].Cells["StatusID"].Value.ToInt32());
                        }
                    }
                }

                private void dgvDocGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
                {
                    try
                    {
                    }
                    catch (Exception ex)
                    {
                    }
                }

                private void dgvDocSummary_DataError(object sender, DataGridViewDataErrorEventArgs e)
                {
                    try { }
                    catch (Exception ex) { }
                }

                private void rbtnAll_CheckedChanged(object sender, EventArgs e)
                {
                    FilldgvDocGrid();
                    pnlBottom.Visible = false;
                }

                private void CancelToolStripButton_Click(object sender, EventArgs e)
                {
                    cboDocumentType.SelectedIndex = -1;
                    cboOperationType.SelectedIndex = -1;

                }

                private void frmStockRegister_Shown(object sender, EventArgs e)
                {
                    rbtnAll_CheckedChanged(sender, e);
                }

                private void dgvDocSummary_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
                {
                    if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                    {
                        if (e.ColumnIndex == 1 && dgvDocSummary.CurrentRow.Cells["clmdelete"].Style.BackColor == Color.White)
                        {
                            MstrCommonMessage = UserMessage.GetMessageByCode(13);
                            if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                           DialogResult.Yes)
                            {
                                BLLDocumentMaster.objClsDTODocumentMaster.intDocumentID = dgvDocSummary.Rows[e.RowIndex].Cells["DocumentID"].Value.ToInt32();
                                BLLDocumentMaster.objClsDTODocumentMaster.intStatusID = dgvDocSummary.Rows[e.RowIndex].Cells["StatusID"].Value.ToInt32();
                                BLLDocumentMaster.objClsDTODocumentMaster.intOrderNo = dgvDocSummary.Rows[e.RowIndex].Cells["OrderNo"].Value.ToInt32();
                                BLLDocumentMaster.DeleteSingleDocument();

                                ShowDocumentDetails(dgvDocSummary.Rows[e.RowIndex].Cells["DocumentID"].Value.ToInt32(), e.RowIndex);
                            }
                        }
                    }
                }

                private void cboPageCount_SelectedIndexChanged(object sender, EventArgs e)
                {
                    FlgBlk = false;
                    if (cboPageCount.ComboBoxEx.Text != "All")
                    {
                        iPageRows = Convert.ToInt32(cboPageCount.ComboBoxEx.Text);
                    }
                    else
                    {
                        iPageRows = dsTemp.Rows.Count;
                    }
                    sNavClicked = cboPageCount.ComboBoxEx.Text;
                    if (dsTemp !=null)
                    fillDataGrid_dtgBrowse(dsTemp);
                    lblPageCount.Text = "Page " + iCurrentPage + " of " + iTotalPage;
                    FlgBlk = true;
                }

                private void BtnFirst_Click(object sender, EventArgs e)
                {
                    FlgBlk = false;
                    sNavClicked = NavButton.First.ToString();
                    fillDataGrid_dtgBrowse(dsTemp);
                    lblPageCount.Text = "Page " + iCurrentPage + " of " + iTotalPage;
                    FlgBlk = true;
                }

                private void BtnPrevious_Click(object sender, EventArgs e)
                {
                    FlgBlk = false;
                    sNavClicked = NavButton.Previous.ToString();
                    fillDataGrid_dtgBrowse(dsTemp);
                    lblPageCount.Text = "Page " + iCurrentPage + " of " + iTotalPage;
                    FlgBlk = true;
                }

                private void BtnNext_Click(object sender, EventArgs e)
                {
                    FlgBlk = false;
                    sNavClicked = NavButton.Next.ToString();
                    fillDataGrid_dtgBrowse(dsTemp);
                    lblPageCount.Text = "Page " + iCurrentPage + " of " + iTotalPage;
                    FlgBlk = true;
                }

                private void BtnLast_Click(object sender, EventArgs e)
                {
                    FlgBlk = false;
                    sNavClicked = NavButton.Last.ToString();
                    fillDataGrid_dtgBrowse(dsTemp);
                    lblPageCount.Text = "Page " + iCurrentPage + " of " + iTotalPage;
                    FlgBlk = true;
                }


        #endregion

        #region Methods
                //private void SetPermissions()
                //{
                //    clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
                //    if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                //    {
                //        objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.StockRegister, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                //    }
                //    else
                //        MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
                //}

                private void FilldgvDocGrid()
                {
                    dgvDocGrid.DataSource = null;
                    dgvDocSummary.DataSource = null;
                    dgvDocSummary.Columns["Img"].Visible = false ;
                    if (rbtnAll.Checked) BLLDocumentMaster.objClsDTODocumentMaster.intStatusID = 0;
                    else
                        BLLDocumentMaster.objClsDTODocumentMaster.intStatusID = BtnReceipt.Checked == true ? (int)OperationStatusType.DocReceipt : (int)OperationStatusType.DocIssue;
                    BLLDocumentMaster.objClsDTODocumentMaster.intOperationTypeID =Convert.ToInt32(cboOperationType.SelectedValue);
                    BLLDocumentMaster.objClsDTODocumentMaster.intDocumentTypeID = Convert.ToInt32(cboDocumentType.SelectedValue);

                    dsTemp  = BLLDocumentMaster.GetStockDocuments();
                 
                    // Page navigation
                    if (dsTemp.Rows.Count > 0)
                    {
                        int iCount = dsTemp.Rows.Count;
                        if (iCount > 25) BarNavigation.Visible = true;
                        else
                            BarNavigation.Visible = false ;
                    }
                 
                   fillDataGrid_dtgBrowse(dsTemp);
                }

                private void fillDataGrid_dtgBrowse(DataTable  dt)
                {
                 
                    BindingSource1.DataSource = null;

                    if (dt.Rows.Count > 0)
                    {
                        BindingSource1.DataSource = dt;
                    }
                    else
                    {
                        BindingSource1.Clear();
                        BindingSource1.DataSource = null;
                    }

                    DeterminePageBoundaries(dt);
                    int iGrdrowcount = dt.Rows.Count;

                    if (blnNoRecord == false)
                    {
                        dgvDocGrid.Enabled = true;
                        BindingSource1.Filter = "RecordID >= " + RecordID1 + " and RecordID <= " + RecordID2;
                        dgvDocGrid.DataSource = null;
                        dgvDocGrid.DataSource = BindingSource1;
                        dgvDocGrid.Columns["RecordID"].Visible = false;
                        lblPageCount.Text = "Page " + iCurrentPage + " of " + iTotalPage;
                    }

                    if (dgvDocGrid.Rows.Count > 0)
                    {
                       
                        // Grid image settings
                            for (int i = 0; i < dgvDocGrid.RowCount; i++)
                            {
                                if (Convert.ToInt32(dgvDocGrid.Rows[i].Cells["StatusID"].Value) == (int)OperationStatusType.DocReceipt)
                                {
                                    dgvDocGrid.Rows[i].Cells["clmImage"].Value = global::MyBooksERP.Properties.Resources.DocumentIn;//ImageList1.Images[0];// In
                                    dgvDocGrid.Rows[i].Cells["clmImage"].ToolTipText = "In";
                                }
                                else
                                {
                                    dgvDocGrid.Rows[i].Cells["clmImage"].Value = global::MyBooksERP.Properties.Resources.DocumentOut;
                                    dgvDocGrid.Rows[i].Cells["clmImage"].ToolTipText = "Out";
                                }
                            }

                        dgvDocGrid.Columns["DocumentID"].Visible = false;
                        dgvDocGrid.Columns["StatusID"].Visible = false;
                        dgvDocGrid.Columns["RecordID"].Visible = false;
                        dgvDocGrid.Columns["Print"].HeaderText = string.Empty;
                        Print.DisplayIndex = 12;
                        if (dgvDocGrid.Columns.Count > 0)
                        {
                            for (int i = 0; i < dgvDocGrid.Columns.Count; i++)
                            {
                                dgvDocGrid.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                            }
                        }
                        dgvDocGrid.ClearSelection();
                    }
                }

                private void DeterminePageBoundaries(DataTable  dsPages)
                {

                    int TotalRowCount = dsPages.Rows.Count;
                    if (TotalRowCount == 0)
                    {
                        //MsgBox("No records to Display.")
                        blnNoRecord = true;
                        //Exit Sub
                    }
                    else
                    {
                        blnNoRecord = false;
                    }
                    //This is the maximum rows to display on a page. So we want paging to be implemented at 100 rows a page 

                    int pages = 0;

                    //if the rows per page are less than the total row count do the following: 
                    if (iPageRows < TotalRowCount)
                    {
                        //if the Modulus returns > 0  there should be another page. 
                        if ((TotalRowCount % iPageRows) > 0)
                        {
                            pages = ((TotalRowCount / iPageRows) - ((TotalRowCount % iPageRows) / iPageRows) + 1);
                        }
                        else
                        {
                            //There is nothing left after the Modulus, so the iPageRows divide exactly... 
                            //...into TotalRowCount leaving no rest, thus no extra page needs to be addd. 
                            pages = TotalRowCount / iPageRows;
                        }
                    }
                    else
                    {
                        //if the rows per page are more than the total row count, we will obviously only ever have 1 page 
                        pages = 1;
                    }
                    iTotalPage = pages;

                    //We now need to determine the LowerBoundary and UpperBoundary in order to correctly... 
                    //...determine the Correct RecordID//s to use in the bindingSource1.Filter property. 
                    int LowerBoundary = 0;
                    int UpperBoundary = 0;

                    //We now need to know what button was clicked, if any (First, Last, Next, Previous) 
                    switch (sNavClicked)
                    {
                        case "First":
                            {
                                //First clicked, the Current Page will always be 1 
                                iCurrentPage = 1;
                                //The LowerBoundary will thus be ((50 * 1) - (50 - 1)) = 1 
                                LowerBoundary = ((iPageRows * iCurrentPage) - (iPageRows - 1));

                                //if the rows per page are less than the total row count do the following: 
                                if (iPageRows < TotalRowCount)
                                {
                                    UpperBoundary = (iPageRows * iCurrentPage);
                                }
                                else
                                {
                                    //if the rows per page are more than the total row count do the following: 
                                    //There is only one page, so get the total row count as an UpperBoundary 
                                    UpperBoundary = TotalRowCount;
                                }

                                //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                                //...for this specific page. 
                                //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                                if (dsPages.Rows.Count > 0)
                                {
                                    RecordID1 = dsPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                    RecordID2 = dsPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                                }
                                break;
                            }
                        case "Last":
                            {
                                //Last clicked, the iCurrentPage will always be = to the variable pages 
                                iCurrentPage = pages;
                                LowerBoundary = ((iPageRows * iCurrentPage) - (iPageRows - 1));
                                //The UpperBoundary will always be the sum total of all the rows 
                                UpperBoundary = TotalRowCount;
                                //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                                //...for this specific page. 
                                //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                                if (dsPages.Rows.Count > 0)
                                {
                                    RecordID1 = dsPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                    RecordID2 = dsPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                                }
                                break;
                            }
                        case "Next":
                            {
                                //Next clicked 
                                if (iCurrentPage != pages)
                                {
                                    //if we arent on the last page already, add another page 
                                    iCurrentPage += 1;
                                }

                                LowerBoundary = ((iPageRows * iCurrentPage) - (iPageRows - 1));

                                if (iCurrentPage == pages)
                                {
                                    //if we are on the last page, the UpperBoundary will always be the sum total of all the rows 
                                    UpperBoundary = TotalRowCount;
                                }
                                else
                                {
                                    //else if we have a pageRow of 50 and we are on page 3, the UpperBoundary = 150 
                                    UpperBoundary = (iPageRows * iCurrentPage);
                                }

                                //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                                //...for this specific page. 
                                //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                                if (dsPages.Rows.Count > 0)
                                {
                                    RecordID1 = dsPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                    RecordID2 = dsPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                                }

                                break;
                            }
                        case "Previous":
                            {
                                //Previous clicked 
                                if (iCurrentPage != 1)
                                {
                                    //if we aren//t on the first page already, subtract 1 from the iCurrentPage 
                                    iCurrentPage -= 1;
                                }
                                //Get the LowerBoundary 
                                LowerBoundary = ((iPageRows * iCurrentPage) - (iPageRows - 1));

                                if (iPageRows < TotalRowCount)
                                {
                                    UpperBoundary = (iPageRows * iCurrentPage);
                                }
                                else
                                {
                                    UpperBoundary = TotalRowCount;
                                }

                                //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                                //...for this specific page. 
                                //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                                if (dsPages.Rows.Count > 0)
                                {
                                    RecordID1 = dsPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                    RecordID2 = dsPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                                }
                                break;
                            }
                        case "25":
                        case "50":
                        case "100":
                            {
                                //First clicked, the Current Page will always be 1 
                                iCurrentPage = 1;
                                //The LowerBoundary will thus be ((50 * 1) - (50 - 1)) = 1 
                                LowerBoundary = ((iPageRows * iCurrentPage) - (iPageRows - 1));

                                //if the rows per page are less than the total row count do the following: 
                                if (iPageRows < TotalRowCount)
                                {
                                    UpperBoundary = (iPageRows * iCurrentPage);
                                }
                                else
                                {
                                    //if the rows per page are more than the total row count do the following: 
                                    //There is only one page, so get the total row count as an UpperBoundary 
                                    UpperBoundary = TotalRowCount;
                                }

                                //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                                //...for this specific page. 
                                //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                                if (dsPages.Rows.Count > 0)
                                {
                                    RecordID1 = dsPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                    RecordID2 = dsPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                                }
                                break;
                            }
                        case "All":
                            {
                                //First clicked, the Current Page will always be 1 
                                iCurrentPage = 1;
                                //The LowerBoundary will thus be ((50 * 1) - (50 - 1)) = 1 
                                LowerBoundary = 1;
                                UpperBoundary = TotalRowCount;

                                //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                                //...for this specific page. 
                                //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                                if (dsPages.Rows.Count > 0)
                                {
                                    RecordID1 = dsPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                    RecordID2 = dsPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                                }

                                break;
                            }
                        default:
                            {
                                //No button was clicked. 
                                LowerBoundary = ((iPageRows * iCurrentPage) - (iPageRows - 1));

                                //if the rows per page are less than the total row count do the following: 
                                if (iPageRows < TotalRowCount)
                                {
                                    UpperBoundary = (iPageRows * iCurrentPage);
                                }
                                else
                                {
                                    //if the rows per page are more than the total row count do the following: 
                                    //Therefore there is only one page, so get the total row count as an UpperBoundary 
                                    UpperBoundary = TotalRowCount;
                                }

                                //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                                //...for this specific page. 
                                //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                                if (dsPages.Rows.Count > 0)
                                {
                                    RecordID1 = dsPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                    RecordID2 = dsPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                                }
                                break;
                            }
                    }
                }

                private void ShowDocumentDetails(int iDocumentID, int iRowindex)
                {
                    pnlBottom.Visible = true ;
                    dgvDocSummary.DataSource = null;

                    BLLDocumentMaster.objClsDTODocumentMaster.intDocumentID = iDocumentID;
                    DataTable dtDocDetails = BLLDocumentMaster.GetStockDocumentDetails();

                    if (dtDocDetails.Rows.Count > 0)
                    {
                        GrpPanelDocMap.Text = "Document Map - " + dgvDocGrid.Rows[iRowindex].Cells["Type"].Value + "(" + dgvDocGrid.Rows[iRowindex].Cells["DocumentNumber"].Value + ")";

                        dgvDocSummary.DataSource = dtDocDetails;
                        dgvDocSummary.Columns["DocumentID"].Visible = false;
                        dgvDocSummary.Columns["StatusID"].Visible = false;
                        dgvDocSummary.Columns["OrderNo"].Visible = false;
                        //dgvDocSummary.Columns["Remarks"].AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
                        dgvDocSummary.Columns["Img"].Visible =true ;
                        // Grid image settings
                        if (dgvDocSummary.RowCount > 0)
                        {
                            for (int i = 0; i < dgvDocSummary.RowCount; i++)
                            {
                                if (Convert.ToInt32(dgvDocSummary.Rows[i].Cells["StatusID"].Value) == (int)OperationStatusType.DocReceipt)
                                {
                                    dgvDocSummary.Rows[i].Cells["Img"].Value = global::MyBooksERP.Properties.Resources.DocumentIn;//ImageList1.Images[0];// In
                                    dgvDocSummary.Rows[i].Cells["Img"].ToolTipText = "In";
                                }
                                else
                                {
                                    dgvDocSummary.Rows[i].Cells["Img"].Value = global::MyBooksERP.Properties.Resources.DocumentOut;
                                    dgvDocSummary.Rows[i].Cells["Img"].ToolTipText = "Out";
                                }
                            }
                        }

                        clmdelete.DisplayIndex = 12;
                        EnableDisablePrint();
                    }

                }

                private void EnableDisablePrint()
                {
                       if (dgvDocSummary.RowCount > 0)
                        {
                            if (dgvDocSummary.RowCount == 1)
                            {
                                dgvDocSummary.Rows[0].Cells["clmdelete"].ReadOnly = true;
                                dgvDocSummary.Rows[0].Cells["clmdelete"].Style.BackColor = Color.LightGray;
                                dgvDocSummary.Rows[0].Cells["clmdelete"].ToolTipText = "Can't Delete";
                            }
                            else
                            {
                                dgvDocSummary.Rows[0].Cells["clmdelete"].ReadOnly = false;
                                dgvDocSummary.Rows[0].Cells["clmdelete"].Style.BackColor = Color.White;
                                dgvDocSummary.Rows[0].Cells["clmdelete"].ToolTipText = "Delete";
                            }
                            for (int i = 1; i < dgvDocSummary.RowCount; i++)
                            {
                                dgvDocSummary.Rows[i].Cells["clmdelete"].ReadOnly = true;
                                dgvDocSummary.Rows[i].Cells["clmdelete"].Style.BackColor = Color.LightGray;
                                dgvDocSummary.Rows[i].Cells["clmdelete"].ToolTipText = "Can't Delete";
                            }

                            if (dgvDocSummary.Columns.Count > 0)
                            {
                                for (int i = 0; i < dgvDocSummary.Columns.Count; i++)
                                {
                                    dgvDocSummary.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                                }
                            }
                        } 
                }

                private void LoadReport(int iDocumentID,int iStatusID)
                {
                    if (iDocumentID > 0 && iStatusID > 0)
                    {
                        FrmReportviewer ObjViewer = new FrmReportviewer();
                        ObjViewer.PsFormName = this.Text;
                        ObjViewer.PiRecId = iDocumentID;
                        ObjViewer.PiFormID = (int)FormID.Documents;
                        ObjViewer.PiStatus = iStatusID;
                        ObjViewer.ShowDialog();
                    }
                }

                private void  LoadCombo()
                {
                    try
                    {
                            DataTable datCombos = null;
                            datCombos = BLLDocumentMaster.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", "OperationTypeID in (select distinct OperationTypeID from DocDocumentMaster)" });
                            cboOperationType.DisplayMember = "OperationType";
                            cboOperationType.ValueMember = "OperationTypeID";
                            cboOperationType.DataSource = datCombos;

                            datCombos = MobjClsBLLDocumentMaster.FillCombos(new string[] { "DocumentTypeID, DocumentType", "DocumentTypeReference", "DocumentTypeID in (select distinct DocumentTypeID from DocDocumentMaster)" });
                            cboDocumentType.DisplayMember = "DocumentType";
                            cboDocumentType.ValueMember = "DocumentTypeID";
                            cboDocumentType.DataSource = datCombos;

                            cboPageCount.Items.Add("All");
                            cboPageCount.Items.Add("25");
                            cboPageCount.Items.Add("50");
                            cboPageCount.Items.Add("100");
                            cboPageCount.ControlText = "25";


                    }
                    catch (Exception ex)
                    {
                        ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                    }
                }

        #endregion

            
  
                enum NavButton
                {
                    First = 1,
                    Next = 2,
                    Previous = 3,
                    Last = 4
                };

              
    }
}
