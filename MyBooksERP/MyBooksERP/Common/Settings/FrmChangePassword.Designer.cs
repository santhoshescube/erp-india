﻿namespace MyBooksERP
{
    partial class FrmChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmChangePassword));
            this.lblxUserName = new DevComponents.DotNetBar.LabelX();
            this.pnlxMain = new DevComponents.DotNetBar.PanelEx();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtxNewPassword = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblxNewPassword = new DevComponents.DotNetBar.LabelX();
            this.lblxOldPassWord = new DevComponents.DotNetBar.LabelX();
            this.txtxRetypeNewpassword = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblxRetypePassword = new DevComponents.DotNetBar.LabelX();
            this.txtxUserName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtxOldPAssword = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.toolStripStatusLabel11 = new DevComponents.DotNetBar.LabelItem();
            this.lblChangePasswordStatus = new DevComponents.DotNetBar.LabelItem();
            this.btnSave = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.errorProChangePassword = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrChangePassowrd = new System.Windows.Forms.Timer(this.components);
            this.pnlxMain.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProChangePassword)).BeginInit();
            this.SuspendLayout();
            // 
            // lblxUserName
            // 
            this.lblxUserName.AutoSize = true;
            // 
            // 
            // 
            this.lblxUserName.BackgroundStyle.Class = "";
            this.lblxUserName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblxUserName.Location = new System.Drawing.Point(8, 13);
            this.lblxUserName.Name = "lblxUserName";
            this.lblxUserName.Size = new System.Drawing.Size(58, 15);
            this.lblxUserName.TabIndex = 0;
            this.lblxUserName.Text = "User Name";
            // 
            // pnlxMain
            // 
            this.pnlxMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlxMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlxMain.Controls.Add(this.panel1);
            this.pnlxMain.Controls.Add(this.bar1);
            this.pnlxMain.Controls.Add(this.btnSave);
            this.pnlxMain.Controls.Add(this.BtnCancel);
            this.pnlxMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlxMain.Location = new System.Drawing.Point(0, 0);
            this.pnlxMain.Name = "pnlxMain";
            this.pnlxMain.Size = new System.Drawing.Size(365, 173);
            this.pnlxMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlxMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlxMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlxMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlxMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlxMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlxMain.Style.GradientAngle = 90;
            this.pnlxMain.TabIndex = 1;
            this.pnlxMain.Click += new System.EventHandler(this.pnlxMain_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtxNewPassword);
            this.panel1.Controls.Add(this.lblxUserName);
            this.panel1.Controls.Add(this.lblxNewPassword);
            this.panel1.Controls.Add(this.lblxOldPassWord);
            this.panel1.Controls.Add(this.txtxRetypeNewpassword);
            this.panel1.Controls.Add(this.lblxRetypePassword);
            this.panel1.Controls.Add(this.txtxUserName);
            this.panel1.Controls.Add(this.txtxOldPAssword);
            this.panel1.Location = new System.Drawing.Point(9, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(345, 115);
            this.panel1.TabIndex = 0;
            // 
            // txtxNewPassword
            // 
            this.txtxNewPassword.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtxNewPassword.Border.Class = "TextBoxBorder";
            this.txtxNewPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtxNewPassword.Location = new System.Drawing.Point(108, 58);
            this.txtxNewPassword.Name = "txtxNewPassword";
            this.txtxNewPassword.ShortcutsEnabled = false;
            this.txtxNewPassword.Size = new System.Drawing.Size(217, 20);
            this.txtxNewPassword.TabIndex = 1;
            this.txtxNewPassword.UseSystemPasswordChar = true;
            // 
            // lblxNewPassword
            // 
            this.lblxNewPassword.AutoSize = true;
            // 
            // 
            // 
            this.lblxNewPassword.BackgroundStyle.Class = "";
            this.lblxNewPassword.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblxNewPassword.Location = new System.Drawing.Point(8, 59);
            this.lblxNewPassword.Name = "lblxNewPassword";
            this.lblxNewPassword.Size = new System.Drawing.Size(76, 15);
            this.lblxNewPassword.TabIndex = 1;
            this.lblxNewPassword.Text = "New Password";
            // 
            // lblxOldPassWord
            // 
            this.lblxOldPassWord.AutoSize = true;
            // 
            // 
            // 
            this.lblxOldPassWord.BackgroundStyle.Class = "";
            this.lblxOldPassWord.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblxOldPassWord.Location = new System.Drawing.Point(8, 36);
            this.lblxOldPassWord.Name = "lblxOldPassWord";
            this.lblxOldPassWord.Size = new System.Drawing.Size(90, 15);
            this.lblxOldPassWord.TabIndex = 2;
            this.lblxOldPassWord.Text = "Current Password";
            // 
            // txtxRetypeNewpassword
            // 
            this.txtxRetypeNewpassword.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtxRetypeNewpassword.Border.Class = "TextBoxBorder";
            this.txtxRetypeNewpassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtxRetypeNewpassword.Location = new System.Drawing.Point(108, 82);
            this.txtxRetypeNewpassword.Name = "txtxRetypeNewpassword";
            this.txtxRetypeNewpassword.ShortcutsEnabled = false;
            this.txtxRetypeNewpassword.Size = new System.Drawing.Size(217, 20);
            this.txtxRetypeNewpassword.TabIndex = 2;
            this.txtxRetypeNewpassword.UseSystemPasswordChar = true;
            this.txtxRetypeNewpassword.Leave += new System.EventHandler(this.txtxRetypeNewpassword_Leave);
            // 
            // lblxRetypePassword
            // 
            this.lblxRetypePassword.AutoSize = true;
            // 
            // 
            // 
            this.lblxRetypePassword.BackgroundStyle.Class = "";
            this.lblxRetypePassword.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblxRetypePassword.Location = new System.Drawing.Point(8, 85);
            this.lblxRetypePassword.Name = "lblxRetypePassword";
            this.lblxRetypePassword.Size = new System.Drawing.Size(99, 15);
            this.lblxRetypePassword.TabIndex = 3;
            this.lblxRetypePassword.Text = "Re-Type  Password";
            // 
            // txtxUserName
            // 
            // 
            // 
            // 
            this.txtxUserName.Border.Class = "TextBoxBorder";
            this.txtxUserName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtxUserName.Location = new System.Drawing.Point(108, 10);
            this.txtxUserName.Name = "txtxUserName";
            this.txtxUserName.ReadOnly = true;
            this.txtxUserName.Size = new System.Drawing.Size(217, 20);
            this.txtxUserName.TabIndex = 0;
            this.txtxUserName.TabStop = false;
            // 
            // txtxOldPAssword
            // 
            this.txtxOldPAssword.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtxOldPAssword.Border.Class = "TextBoxBorder";
            this.txtxOldPAssword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtxOldPAssword.Location = new System.Drawing.Point(108, 34);
            this.txtxOldPAssword.Name = "txtxOldPAssword";
            this.txtxOldPAssword.ShortcutsEnabled = false;
            this.txtxOldPAssword.Size = new System.Drawing.Size(217, 20);
            this.txtxOldPAssword.TabIndex = 0;
            this.txtxOldPAssword.UseSystemPasswordChar = true;
            this.txtxOldPAssword.Leave += new System.EventHandler(this.txtxOldPAssword_Leave);
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.toolStripStatusLabel11,
            this.lblChangePasswordStatus});
            this.bar1.Location = new System.Drawing.Point(0, 154);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(365, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 82;
            this.bar1.TabStop = false;
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Text = "Status:";
            // 
            // lblChangePasswordStatus
            // 
            this.lblChangePasswordStatus.Name = "lblChangePasswordStatus";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(202, 127);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(281, 127);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 2;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // errorProChangePassword
            // 
            this.errorProChangePassword.ContainerControl = this;
            // 
            // tmrChangePassowrd
            // 
            this.tmrChangePassowrd.Tick += new System.EventHandler(this.tmrChangePassowrd_Tick);
            // 
            // FrmChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 173);
            this.Controls.Add(this.pnlxMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmChangePassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Change Password";
            this.Load += new System.EventHandler(this.FrmChangePassword_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmChangePassword_KeyDown);
            this.pnlxMain.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProChangePassword)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX lblxUserName;
        private DevComponents.DotNetBar.PanelEx pnlxMain;
        private DevComponents.DotNetBar.LabelX lblxNewPassword;
        private DevComponents.DotNetBar.LabelX lblxOldPassWord;
        private DevComponents.DotNetBar.LabelX lblxRetypePassword;
        private DevComponents.DotNetBar.Controls.TextBoxX txtxRetypeNewpassword;
        private DevComponents.DotNetBar.Controls.TextBoxX txtxNewPassword;
        private DevComponents.DotNetBar.Controls.TextBoxX txtxOldPAssword;
        private DevComponents.DotNetBar.Controls.TextBoxX txtxUserName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.ErrorProvider errorProChangePassword;
        private System.Windows.Forms.Timer tmrChangePassowrd;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel11;
        private DevComponents.DotNetBar.LabelItem lblChangePasswordStatus;
        private System.Windows.Forms.Panel panel1;
    }
}