﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;


/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,1 Mar 2011>
Description:	<Description,UserInformation Form>
================================================
*/

namespace MyBooksERP
{
    public partial class FrmMandatoryDocumentTypes : DevComponents.DotNetBar.Office2007Form
    {
        #region Variables Declaration

        ClsLogWriter MobjClsLogWriter;
        ClsNotificationNew MobjClsNotification;
        clsBLLMandatoryDocumentTypes MobjclsBLLMandatoryDocumentTypes;
        clsBLLCommonUtility MobjBLLCommonUtility = new clsBLLCommonUtility();
        //bool MblnShowErrorMess = true;   // Checking whether error messages are showing or not
        string MstrCommonMessage;
        DataTable MdatMessages;
        //MessageBoxIcon MmsgMessageIcon;

        //private bool MblnChangeStatus = false;
        //private bool MblnAddStatus;
        private bool MblnPrintEmailPermission = false;//To Set Print/Email Permission
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnAddUpdatePermission = false;//To Set Add/Update Permission

        #endregion //Variables Declaration

        #region Constructor

        public FrmMandatoryDocumentTypes()
        {
            InitializeComponent();

            MobjclsBLLMandatoryDocumentTypes = new clsBLLMandatoryDocumentTypes();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotificationNew();
        }
        #endregion //Constructor

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.General, (Int32)eMenuID.MandatoryDocuments, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                if (MblnAddPermission == true || MblnUpdatePermission == true)
                    MblnAddUpdatePermission = true;
               // MblnAddUpdatePermission = MblnUpdatePermission;
            }
            else
                MblnAddPermission = MblnAddUpdatePermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            //BindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
            //bnDeleteItem.Enabled = MblnDeletePermission;
            //BtnOk.Enabled = MblnAddUpdatePermission;
        }

        private void LoadCombos(int intType)
        {
            DataTable datCombos = null;

            if (intType == 1)
            {
                datCombos = MobjclsBLLMandatoryDocumentTypes.FillCombos(new string[] { "OperationTypeID,OperationType", "OperationTypeReference", "OperationTypeID In (" + (int)OperationType.PurchaseQuotation + "," + (int)OperationType.PurchaseOrder + "," + (int)OperationType.SalesOrder + "," + (int)OperationType.Suppliers + ", " + (int)OperationType.Customers + ")" });
                cboOperationType.ValueMember = "OperationTypeID";
                cboOperationType.DisplayMember = "OperationType";
                cboOperationType.DataSource = datCombos;
            }
            if (intType == 0 || intType == 2)
            {
                datCombos = MobjclsBLLMandatoryDocumentTypes.FillCombos(new string[] { "DocumentTypeID,DocumentType", "DocumentTypeReference", "" });
                dgvColDocumentTypes.ValueMember = "DocumentTypeID";
                dgvColDocumentTypes.DisplayMember = "DocumentType";
                dgvColDocumentTypes.DataSource = datCombos;
            }
            if (intType == 0 || intType == 3) // For Loading Company Combobox
            {
                datCombos = MobjclsBLLMandatoryDocumentTypes.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = datCombos;
            }
        }

        private void cboOperationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvMandatoryDocuments.Rows.Clear();
            if (cboCompany.SelectedValue != null)
            {
                if (cboOperationType.SelectedValue != null)
                {
                    DataTable datMandatoryDocumentTypes = MobjclsBLLMandatoryDocumentTypes.GetMandatoryDocumentTypes(cboOperationType.SelectedValue.ToInt32(), cboCompany.SelectedValue.ToInt32());
                    if (datMandatoryDocumentTypes.Rows.Count > 0)
                    {
                        for (int intICounter = 0; intICounter < datMandatoryDocumentTypes.Rows.Count; intICounter++)
                        {
                            dgvMandatoryDocuments.Rows.Add();
                            dgvMandatoryDocuments[dgvColDocumentTypes.Index, intICounter].Value = datMandatoryDocumentTypes.Rows[intICounter]["DocumentTypeID"];
                        }
                        bnDeleteItem.Enabled = MblnDeletePermission;
                    }
                    else
                        bnDeleteItem.Enabled = false;
                    bnClear.Enabled = true;
                }
            }
            
            BindingNavigatorSaveItem.Enabled = BtnOk.Enabled = MblnAddUpdatePermission;
            
        }
        private void LoadMessage()
        {
            MdatMessages = MobjClsNotification.FillMessageArray((int)FormID.MandatoryDocumentTypes, (int)ClsCommonSettings.ProductID);
        }    
        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateFields())
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 3).Replace("#", "");
                    if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        FillParameters();
                        string strDuplicatingMandatoryDocuments = MobjclsBLLMandatoryDocumentTypes.ValidationInsertMandatoryDocumentTypes();
                        if( strDuplicatingMandatoryDocuments!="")
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 9473).Replace("#", "");
                            if (MessageBox.Show(MstrCommonMessage.Replace("***", strDuplicatingMandatoryDocuments), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                             {
                                  return;
                             }
                           
                        }
                        MobjclsBLLMandatoryDocumentTypes.InsertMandatoryDocumentTypes();
                        int intCompanyID = cboCompany.SelectedValue.ToInt32();
                        int intOperationTypeID = cboOperationType.SelectedValue.ToInt32();
                        bnClear_Click(null, null);
                        cboOperationType.SelectedValue = intOperationTypeID;
                        cboCompany.SelectedValue = intCompanyID;
                        lblStatus.Text = MobjClsNotification.GetErrorMessage(MdatMessages, 2).Replace("#", "");
                        TmMandatoryDocumentTypes.Enabled = true;
                    }
                    if (sender.GetType() == typeof(Button))
                    {
                        if (((Button)sender).Name == BtnOk.Name)
                            Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmMadatoryDocumentTypes :FrmMandatoryDocumentTypes " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmMandatoryDocumentTypes() " + ex.Message.ToString());
            }
        }

        private void FillParameters()
        {
            MobjclsBLLMandatoryDocumentTypes.clsDTOMandatoryDocumentTypes.intCompanyID = cboCompany.SelectedValue.ToInt32();
            MobjclsBLLMandatoryDocumentTypes.clsDTOMandatoryDocumentTypes.intOperationTypeID = cboOperationType.SelectedValue.ToInt32();
            MobjclsBLLMandatoryDocumentTypes.clsDTOMandatoryDocumentTypes.lstMandatoryDocumentTypes = new List<clsDTOMandatoryDocumentType>();
            foreach (DataGridViewRow dr in dgvMandatoryDocuments.Rows)
            {
                if (dr.Cells[dgvColDocumentTypes.Index].Value != null)
                {
                    clsDTOMandatoryDocumentType objClsDTOMandatoryDocumentType = new clsDTOMandatoryDocumentType();
                    objClsDTOMandatoryDocumentType.intDocumentTypeID = dr.Cells[dgvColDocumentTypes.Index].Value.ToInt32();
                    MobjclsBLLMandatoryDocumentTypes.clsDTOMandatoryDocumentTypes.lstMandatoryDocumentTypes.Add(objClsDTOMandatoryDocumentType);
                }
            }
        }

        private void bnClear_Click(object sender, EventArgs e)
        {
            cboOperationType.SelectedIndex = -1;
            cboOperationType.DataSource = null;
            cboCompany.SelectedIndex = -1;
            dgvMandatoryDocuments.Rows.Clear();
            MobjclsBLLMandatoryDocumentTypes = new clsBLLMandatoryDocumentTypes();
            BindingNavigatorSaveItem.Enabled = BtnOk.Enabled = false;
            bnDeleteItem.Enabled = false;
            bnClear.Enabled = false;
        }

        private void FrmMandatoryDocumentTypes_Load(object sender, EventArgs e)
        {
            LoadCombos(0);
            SetPermissions();
            LoadMessage();
            TmMandatoryDocumentTypes.Interval = ClsCommonSettings.TimerInterval;
            bnClear_Click(null, null);
        }

        private void bnDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 4268).Replace("#", "");
                if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MobjclsBLLMandatoryDocumentTypes.clsDTOMandatoryDocumentTypes.intOperationTypeID = cboOperationType.SelectedValue.ToInt32();
                    MobjclsBLLMandatoryDocumentTypes.clsDTOMandatoryDocumentTypes.intCompanyID = cboCompany.SelectedValue.ToInt32();
                    MobjclsBLLMandatoryDocumentTypes.DeleteMandatoryDocumentTypes();
                    int intOperationTypeID = cboOperationType.SelectedValue.ToInt32();
                    int intcompanyID = cboCompany.SelectedValue.ToInt32();
                    bnClear_Click(null, null);
                    cboOperationType.SelectedValue = intOperationTypeID;
                    cboCompany.SelectedValue = intcompanyID;
                    lblStatus.Text = MobjClsNotification.GetErrorMessage(MdatMessages, 4).Replace("#", "");
                    TmMandatoryDocumentTypes.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on form MandatoryDocumentTypes : " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on  bnDeleteItem_Click " + ex.Message.ToString());
            }
        }

        private bool ValidateFields()
        {
            dgvMandatoryDocuments.EndEdit();
            bool blnExists = false;
            int intCount = 0;
            for (int intICounter = 0; intICounter < dgvMandatoryDocuments.Rows.Count; intICounter++)
            {
                if(dgvMandatoryDocuments.Rows[intICounter].Cells[dgvColDocumentTypes.Index].Value.ToInt32() !=0)
                {
                    intCount ++;
                for (int intJCounter = intICounter + 1; intJCounter < dgvMandatoryDocuments.Rows.Count; intJCounter++)
                {
                    if (dgvMandatoryDocuments.Rows[intICounter].Cells[dgvColDocumentTypes.Index].Value.ToInt32() == dgvMandatoryDocuments.Rows[intJCounter].Cells[dgvColDocumentTypes.Index].Value.ToInt32())
                    {
                        blnExists = true;
                    }
                }
            }
            }
            if(intCount == 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages,9471).Replace("#","");
                MessageBox.Show(MstrCommonMessage,ClsCommonSettings.MessageCaption,MessageBoxButtons.OK,MessageBoxIcon.Information);
                return false;
            }
            if (blnExists)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 9472).Replace("#", "");
                MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        //private void BtnOk_Click(object sender, EventArgs e)
        //{
        //    BindingNavigatorSaveItem_Click(null, null);
            
        //    this.Close();
        //}

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bnDocumentType_Click(object sender, EventArgs e)
        {
            try
            {
                // Calling Reference form for ItemCategory
                FrmCommonRef objCommon = new FrmCommonRef("Document Type", new int[] { 2, 2 }, "DocumentTypeID,IsPredefined, DocumentType As [Document Type]", "DocumentTypeReference", "");
                objCommon.MstrInsertFields = "IsPredefined";
                objCommon.MstrInsertValues = 0.ToString();
                objCommon.ShowDialog();
                objCommon.Dispose();

                LoadCombos(2);
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmCommonRef() " + Ex.Message.ToString());
            }
        }

        private void TmMandatoryDocumentTypes_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            TmMandatoryDocumentTypes.Enabled = false;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedIndex != -1)
            {
                cboOperationType.DataSource = null;
                dgvMandatoryDocuments.Rows.Clear();
                LoadCombos(1);
            }
        }

        private void dgvMandatoryDocuments_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            
        }
        private void Cbo_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }
        private void FrmMandatoryDocumentTypes_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "MandatoryDocuments";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void dgvMandatoryDocuments_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjBLLCommonUtility.SetSerialNo(dgvMandatoryDocuments, e.RowIndex, false);
        }

        private void dgvMandatoryDocuments_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjBLLCommonUtility.SetSerialNo(dgvMandatoryDocuments, e.RowIndex, false);
        }
    }

}
