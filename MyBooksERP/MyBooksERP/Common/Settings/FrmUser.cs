﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,28 Feb 2011>
Description:	<Description,, UserInformation Form>
================================================
*/

namespace MyBooksERP
{
    public partial class FrmUser : DevComponents.DotNetBar.Office2007Form
    {
        #region Variables Declaration

        private bool MblnAddStatus;
        private bool MblnChangeStatus;
        //private bool MblnViewPermission = true;//To Set View Permission
        private bool MblnAddPermission = true;//To Set Add Permission
        private bool MblnUpdatePermission = true;//To Set Update Permission
        private bool MblnDeletePermission = true;//To Set Delete Permission
        private bool MblnAddUpdatePermission = true;//To Set Add/Update Permission

        private string MstrMessageCaption;
        private string MstrMessageCommon;
        private MessageBoxIcon MmessageIcon;

        private int MintCurrentRecCnt;
        private int MintRecordCnt;
        private int MintTimerInterval;
        private int MintUserID;

        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;

        clsBLLUserInformation MObjclsBLLUserInformation;
        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;
        // ArrayList sarrFormnames; ////showing webbrowser in toolstripsplitbtn
        WebBrowser MobjWebBrowser;
        ToolStripControlHost MobjToolStripControlHost;

        #endregion //Variables Declaration

        #region Constructor
        public FrmUser()
        {
            InitializeComponent();
            MintUserID = 0;
            MintTimerInterval = ClsCommonSettings.TimerInterval;

            MstrMessageCaption = ClsCommonSettings.MessageCaption;    //Message caption
            TmUser.Interval = MintTimerInterval;   // Setting timer interval
            MmessageIcon = MessageBoxIcon.Information;

            MObjclsBLLUserInformation = new clsBLLUserInformation();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification();
            ////showing webbrowser in toolstripsplitbtn

            MobjWebBrowser = new WebBrowser();
            MobjWebBrowser.ScrollBarsEnabled = false;
            MobjWebBrowser.Name = "UsedLevel";
            MobjToolStripControlHost = new ToolStripControlHost(MobjWebBrowser);
            btnWebBrowserUser.DropDownItems.Add(MobjToolStripControlHost);
        }
        #endregion //Constructor

        private void FrmUser_Load(object sender, EventArgs e)
        {
            try
            {
                MblnChangeStatus = false;
                LoadCombos(0);
                LoadMessage();
                ChangeStatus();
                AddNewItem();
                TmUser.Enabled = true;
                TmFocus.Enabled = true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form Load:FrmUser_Load " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmUser_Load " + Ex.Message.ToString());
            }
        }

        private void ChangeStatus()
        {
            if (Convert.ToString(CboRoleID.SelectedValue) == "1")
            {
                CboEmployeeID.BackColor = SystemColors.Window;
            }
            else
            {
                CboEmployeeID.BackColor = SystemColors.Info;
            }
            MblnChangeStatus = true;

            if (MblnAddStatus == true)
            {
                BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnAddPermission;
                BindingNavigatorDeleteItem.Enabled = false;
            }
            else
            {
                BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            }
            BtnClear.Enabled = true;
            ErrorProUser.Clear();
        }

        private void ChangeStatus(object sender, EventArgs e) // to set the event
        {
            ChangeStatus();
        }

        private void SetPermissions()
        {
            if (MblnAddPermission == true || MblnUpdatePermission == true)
                MblnAddUpdatePermission = true;

            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            BtnOk.Enabled = MblnUpdatePermission;
            BtnClear.Enabled = MblnUpdatePermission;
        }

        public bool AddNewItem()
        {
            try
            {
                MblnAddStatus = true;
                TmUser.Enabled = true;
                SetEnable();
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3456, out MmessageIcon);
                lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                MintRecordCnt = MObjclsBLLUserInformation.RecCountNavigate();
                BindingAddNew();
                EnableDisableButtons(true);
                TxtUserName.Focus();
                ClearCompanyList();
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form AddNewItem:AddNewItem() " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewItem() " + Ex.Message.ToString());
                return false;
            }
        }

        private void ClearCompanyList()
        {
            for (int iCounter = 0; iCounter < chkPermittedCompany.Items.Count; iCounter++)
                chkPermittedCompany.SetItemChecked(iCounter, false);
        }

        private void BindingAddNew()
        {
            bindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt + 1) + "";
            bindingNavigatorPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
            MintCurrentRecCnt = MintRecordCnt + 1;
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = false;
            BindingNavigatorMoveLastItem.Enabled = false;
        }

        private void SetBindingNavigatorButtons()
        {
            bindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
            bindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";

            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveFirstItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(bindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
            }
            BindingNavigatorAddNewItem.Enabled = true;
            BindingNavigatorDeleteItem.Enabled = true;
            lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            BtnPrint.Enabled = true;
            btnWebBrowserUser.Enabled = true;
            BtnSave.Enabled = false;
            BtnOk.Enabled = false;
            BindingNavigatorSaveItem.Enabled = false;
        }

        /// <summary>
        /// Enable/disable buttons according to the action
        /// </summary>
        private void EnableDisableButtons(bool bEnable)
        {
            BindingNavigatorAddNewItem.Enabled = !bEnable;
            BindingNavigatorDeleteItem.Enabled = !bEnable;
            BtnPrint.Enabled = !bEnable;
            btnWebBrowserUser.Enabled = !bEnable;

            if (bEnable)
            {
                BindingNavigatorSaveItem.Enabled = !bEnable;
                BtnOk.Enabled = !bEnable;
                BtnClear.Enabled = !bEnable;
                BtnSave.Enabled = !bEnable;
                MblnChangeStatus = !bEnable;
            }
            else
            {
                BindingNavigatorSaveItem.Enabled = bEnable;
                BtnOk.Enabled = bEnable;
                BtnClear.Enabled = bEnable;
                BtnSave.Enabled = bEnable;
                MblnChangeStatus = bEnable;
            }
        }

        private void SetEnable()
        {
            TxtUserName.Text = "";
            TxtPassword.Text = "";
            TxtEmailID.Text = "";
            CboEmployeeID.Text = "";
            CboEmployeeID.SelectedIndex = -1;
            CboRoleID.SelectedIndex = -1;
        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.User, ClsCommonSettings.ProductID);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.User, ClsCommonSettings.ProductID);
        }

        private bool LoadCombos(int intType)
        {
            // 0 - For Loading All Combo
            //  1 - For Loading CboRoleID 
            //  2-  For Loading CboEmployeeID           
            try
            {
                DataTable datCombos = new DataTable();

                if (intType == 0 || intType == 1)
                {
                    datCombos = MObjclsBLLUserInformation.FillCombos(new string[] { "RoleID,RoleName FROM RoleReference WHERE RoleID = 2/*Admin*/ UNION " +
                        "SELECT DISTINCT RR.RoleID,RR.RoleName", "RoleReference RR " +
                        "INNER JOIN RoleDetails RD ON RR.RoleID=RD.RoleID ", "RR.RoleID NOT IN(1,2)" });
                    CboRoleID.ValueMember = "RoleID";
                    CboRoleID.DisplayMember = "RoleName";
                    CboRoleID.DataSource = datCombos;
                }

                if (intType == 0 || intType == 2)
                {
                    datCombos = MObjclsBLLUserInformation.FillCombos(new string[] { "EmployeeID,EmployeeFullName + ' - ' + EmployeeNumber AS FirstName", "" +
                        "EmployeeMaster", "" });
                    CboEmployeeID.ValueMember = "EmployeeID";
                    CboEmployeeID.DisplayMember = "FirstName";
                    CboEmployeeID.DataSource = datCombos;
                }

                if (intType == 0 || intType == 3)
                {
                    datCombos = MObjclsBLLUserInformation.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                    chkPermittedCompany.DataSource = datCombos;
                    chkPermittedCompany.ValueMember = "CompanyID";
                    chkPermittedCompany.DisplayMember = "CompanyName";
                }

                MObjClsLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }

            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private void Cbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        // To check Validtion
        private bool UserInformationValidation()
        {
            ErrorProUser.Clear();

            if (TxtUserName.Text.Trim() == "")
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3451, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrorProUser.SetError(TxtUserName, MstrMessageCommon.Replace("#", "").Trim());
                lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmUser.Enabled = true;
                TxtUserName.Focus();
                return false;
            }

            if (MObjclsBLLUserInformation.CheckDuplication(MblnAddStatus, new string[] { TxtUserName.Text.Replace("'", "").Trim() }, MintUserID))
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3457, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrorProUser.SetError(TxtUserName, MstrMessageCommon.Replace("#", "").Trim());
                lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmUser.Enabled = true;
                TxtUserName.Focus();
                return false;
            }

            if (TxtPassword.Text.Trim() == "")
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3452, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrorProUser.SetError(TxtPassword, MstrMessageCommon.Replace("#", "").Trim());
                lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmUser.Enabled = true;

                TxtPassword.Focus();
                return false;
            }

            if (Convert.ToInt32(CboRoleID.SelectedValue) == 0)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3453, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrorProUser.SetError(CboRoleID, MstrMessageCommon.Replace("#", "").Trim());
                lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmUser.Enabled = true;

                CboRoleID.Focus();
                return false;
            }

            if (Convert.ToInt32(CboEmployeeID.SelectedValue) == 0)
            {
                if (CboRoleID.SelectedValue.ToString() != "1")
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3458, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErrorProUser.SetError(CboEmployeeID, MstrMessageCommon.Replace("#", "").Trim());
                    lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmUser.Enabled = true;

                    CboRoleID.Focus();
                    return false;
                }
            }

            if (TxtEmailID.Text.Trim().Length > 0 && !MObjclsBLLUserInformation.CheckValidEmail(TxtEmailID.Text.Trim()))
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3454, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrorProUser.SetError(TxtEmailID, MstrMessageCommon.Replace("#", "").Trim());
                lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmUser.Enabled = true;
                TxtEmailID.Focus();
                return false;
            }

            return true;
        }

        // Function To Save And Update
        private bool SaveUserInformation()
        {
            try
            {
                if (MblnAddStatus == true)//insert 
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);
                    MObjclsBLLUserInformation.clsDTOUserInformation.intUserID = 0;
                }
                //Update
                else
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
                    MObjclsBLLUserInformation.clsDTOUserInformation.intUserID = MintUserID;
                }

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;

                MObjclsBLLUserInformation.clsDTOUserInformation.intRoleID = Convert.ToInt32(CboRoleID.SelectedValue);
                MObjclsBLLUserInformation.clsDTOUserInformation.strUserName = Convert.ToString(TxtUserName.Text.Trim());
                MObjclsBLLUserInformation.clsDTOUserInformation.strPassword = clsBLLCommonUtility.Encrypt(TxtPassword.Text.Trim(), ClsCommonSettings.strEncryptionKey);
                MObjclsBLLUserInformation.clsDTOUserInformation.strEmailID = Convert.ToString(TxtEmailID.Text.Trim());
                MObjclsBLLUserInformation.clsDTOUserInformation.intEmployeeID = Convert.ToInt32(CboEmployeeID.SelectedValue);

                MObjclsBLLUserInformation.clsDTOUserInformation.lstUserInformation = new List<clsDTOUserCompanyDetails>();
                for (int iCounter = 0; iCounter <= chkPermittedCompany.Items.Count - 1; iCounter++)
                {
                    if (chkPermittedCompany.GetItemChecked(iCounter) == true)
                    {
                        DataRowView drv = (DataRowView)chkPermittedCompany.Items[iCounter];
                        clsDTOUserCompanyDetails objUserCompanyDetails = new clsDTOUserCompanyDetails();
                        objUserCompanyDetails.CompanyID = drv.Row[0].ToInt32();
                        MObjclsBLLUserInformation.clsDTOUserInformation.lstUserInformation.Add(objUserCompanyDetails);
                    }
                }

                if (MObjclsBLLUserInformation.SaveUser(MblnAddStatus))
                {
                    MintUserID = MObjclsBLLUserInformation.clsDTOUserInformation.intUserID;
                    return true;
                }
                return false;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form Save:SaveUserInformation() " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Save SaveUserInformation() " + Ex.Message.ToString());
                return false;
            }
        }

        // Function To Display
        public void DisplayUserInformation()
        {
            int rowno = MintCurrentRecCnt;
            if (MObjclsBLLUserInformation.DisplayUser(rowno))
            {
                MintUserID = MObjclsBLLUserInformation.clsDTOUserInformation.intUserID;
                CboRoleID.SelectedValue = MObjclsBLLUserInformation.clsDTOUserInformation.intRoleID.ToString();
                TxtUserName.Text = MObjclsBLLUserInformation.clsDTOUserInformation.strUserName.ToString();
                TxtPassword.Text = clsBLLCommonUtility.Decrypt(MObjclsBLLUserInformation.clsDTOUserInformation.strPassword.ToString(), ClsCommonSettings.strEncryptionKey);
                CboEmployeeID.SelectedValue = MObjclsBLLUserInformation.clsDTOUserInformation.intEmployeeID.ToString();
                TxtEmailID.Text = MObjclsBLLUserInformation.clsDTOUserInformation.strEmailID.ToString();

                ClearCompanyList();

                DataTable datTemp = MObjclsBLLUserInformation.getUserCompanyDetails();

                for (int iCounter = 0; iCounter < chkPermittedCompany.Items.Count; iCounter++)
                {
                    DataRowView drv = (DataRowView)chkPermittedCompany.Items[iCounter];

                    for (int jCounter = 0; jCounter <= datTemp.Rows.Count - 1; jCounter++)
                    {
                        if (drv.Row[0].ToInt32() == datTemp.Rows[jCounter]["CompanyID"].ToInt32())
                        {
                            chkPermittedCompany.SetItemChecked(iCounter, true);
                            break;
                        }
                    }
                }
            }
        }

        private void UserMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (UserInformationValidation())
            {
                if (SaveUserInformation())
                {
                    if (MblnAddStatus)
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                        lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmUser.Enabled = true;
                    }
                    else
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);
                        lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmUser.Enabled = true;
                    }

                    MblnAddStatus = false;
                    AddNewItem();
                }
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (UserInformationValidation())
            {
                if (SaveUserInformation())
                {
                    if (MblnAddStatus)
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                        lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmUser.Enabled = true;
                    }
                    else
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);
                        lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    }
                    AddNewItem();
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
            }
        }

        private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            try
            {
                MintRecordCnt = MObjclsBLLUserInformation.RecCountNavigate();

                if (MintRecordCnt > 0)
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                    MblnAddStatus = false;
                    if (MintCurrentRecCnt >= MintRecordCnt)
                    {
                        MintCurrentRecCnt = MintRecordCnt;
                    }
                }
                else
                {
                    MintCurrentRecCnt = 0;
                }

                DisplayUserInformation();
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 11, out MmessageIcon);
                SetBindingNavigatorButtons();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form MoveNextItem:MoveNextItem_Click" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveNextItem_Click" + Ex.Message.ToString());
            }
        }

        private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorProUser.Clear();
                MintRecordCnt = MObjclsBLLUserInformation.RecCountNavigate();

                if (MintRecordCnt > 0)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    MblnAddStatus = false;
                }
                else
                {
                    MintCurrentRecCnt = 0;
                }

                DisplayUserInformation();
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 12, out MmessageIcon);
                SetBindingNavigatorButtons();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form MoveLastItem:MoveLastItem_Click" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveLastItem_Click" + Ex.Message.ToString());
            }
        }

        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MObjclsBLLUserInformation.RecCountNavigate() > 0)
                {
                    MblnAddStatus = false;
                    MintCurrentRecCnt = MintCurrentRecCnt - 1;

                    if (MintCurrentRecCnt <= 0)
                    {
                        MintCurrentRecCnt = 1;
                    }
                }
                else
                {
                    bindingNavigatorPositionItem.Text = "of 0";
                    MintRecordCnt = 0;
                }

                DisplayUserInformation();
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 10, out MmessageIcon);
                SetBindingNavigatorButtons();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form MovePreviousItem:MovePreviousItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MovePreviousItem_Click " + Ex.Message.ToString());
            }
        }

        private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorProUser.Clear();
                if (MObjclsBLLUserInformation.RecCountNavigate() > 0)
                {
                    MblnAddStatus = false;
                    MintCurrentRecCnt = 1;
                }
                else
                    MintCurrentRecCnt = 0;

                DisplayUserInformation();
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9, out MmessageIcon);
                SetBindingNavigatorButtons();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form MoveFirstItem:MoveFirstItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveFirstItem_Click " + Ex.Message.ToString());
            }
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewItem();
        }

        private bool DeleteValidation(int intUserID)
        {
            DataTable dtIds = MObjclsBLLUserInformation.CheckExistReferences(intUserID);
            if (dtIds.Rows.Count > 0)
            {
                if (Convert.ToString(dtIds.Rows[0]["FormName"]) != "0")
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9001, out MmessageIcon).Replace("*", "User");

                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    TmUser.Enabled = true;
                    return false;
                }
            }
            return true;
        }


        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            bool blnDeleteStatus = true;
            try
            {
                int MintUserID = MObjclsBLLUserInformation.clsDTOUserInformation.intUserID;
                if (MintUserID == ClsCommonSettings.UserID)
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3459, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmUser.Enabled = true;
                    return;

                }
                else
                {
                    if (DeleteValidation(MintUserID))
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
                        if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            if (MObjclsBLLUserInformation.DeleteUserInformation())
                            {
                                MintRecordCnt = MintRecordCnt - 1;
                                MintCurrentRecCnt = MintCurrentRecCnt - 1;

                                bindingNavigatorCountItem.Text = MintRecordCnt.ToString();
                                bindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
                                //MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
                                //if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                                //    return;
                                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                                lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmUser.Enabled = true;
                                AddNewItem();
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                blnDeleteStatus = false;
                MObjClsLogWriter.WriteLog("Error on form Delete:DeleteItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteItem_Click " + Ex.Message.ToString());
            }

            if (blnDeleteStatus == false)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9004, out MmessageIcon);
                lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            AddNewItem();
            TxtUserName.Focus();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (UserInformationValidation())
            {
                if (SaveUserInformation())
                {
                    if (MblnAddStatus == true)
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                        lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmUser.Enabled = true;
                    }
                    else
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);
                        lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmUser.Enabled = true;
                    }
                    MblnChangeStatus = false;
                    this.Close();
                }
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmUser_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MblnChangeStatus)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void TmUser_Tick(object sender, EventArgs e)
        {
            TmUser.Enabled = false;
            lblUserStatus.Text = "";
        }

        private void TmFocus_Tick(object sender, EventArgs e)
        {
            TxtUserName.Focus();
            TmFocus.Enabled = false;
        }


        private void btnWebBrowserUser_DropDownOpened(object sender, EventArgs e)
        {
            MobjWebBrowser.DocumentText = MObjclsBLLUserInformation.GetFormsInUse("User information exists in", MintUserID, "name <> 'UserMaster'", "UserID");

        }

        private void btnUserRoles_Click(object sender, EventArgs e)
        {
            try
            {
                // Calling Reference form for UserRoles
                FrmCommonRef objCommon = new FrmCommonRef("RoleName", new int[] { 1, 0 }, "RoleID, RoleName As RoleName", "RoleReference", "Type<>0");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(CboRoleID.SelectedValue);
                LoadCombos(1);
                if (objCommon.NewID != 0)
                    CboRoleID.SelectedValue = objCommon.NewID;
                else
                    CboRoleID.SelectedValue = intWorkingAt;

            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnUserRoles_Click() " + Ex.Message.ToString());
            }
        }

        private void FrmUser_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    //case Keys.Control | Keys.S:
                    //    btnSave_Click(sender,new EventArgs());//save
                    //  break;
                    //case Keys.Control | Keys.O:
                    //    btnOk_Click(sender, new EventArgs());//OK
                    //    break; 
                    //case Keys.Control | Keys.L:
                    //    btnCancel_Click(sender, new EventArgs());//Cancel
                    //    break;
                    case Keys.Control | Keys.Enter:
                        bindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        bindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        toolStripButton1_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        bindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        bindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        bindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        bindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    //case Keys.Control | Keys.P:
                    //    btnPrint_Click(sender, new EventArgs());//Cancel
                    //    break;
                    //case Keys.Control | Keys.M:
                    //    BtnEmail_Click(sender, new EventArgs());//Cancel
                    //    break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnEmployee_Click(object sender, EventArgs e)
        {
            using (FrmEmployee objEmployee = new FrmEmployee())
            {
                objEmployee.PintEmployeeID = Convert.ToInt32(CboEmployeeID.SelectedValue);
                objEmployee.ShowDialog();
            }

            int intEmployeeID = Convert.ToInt32(CboEmployeeID.SelectedValue);
            LoadCombos(2);
            CboEmployeeID.SelectedValue = intEmployeeID;
        }

        private void bnRoleSettings_Click(object sender, EventArgs e)
        {
            try
            {
                frmRole objRoleSettings = new frmRole();
                objRoleSettings.ShowDialog();
                objRoleSettings.Dispose();
                int WorkingID = Convert.ToInt32(CboRoleID.SelectedValue);
                LoadCombos(1);
                //if (objRoleSettings.NewID > 0)
                //    CboRoleID.SelectedValue = objRoleSettings.NewID;
                //else
                //    CboRoleID.SelectedValue = WorkingID;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnUserRoles_Click() " + Ex.Message.ToString());
            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "User";
            objHelp.ShowDialog();
            objHelp = null;
        }
    }
}