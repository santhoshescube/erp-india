﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using System.Net.Mail;

using System.Data.SqlClient;

namespace MyBooksERP
{
    public partial class FrmEmailSettings : DevComponents.DotNetBar.Office2007Form
    {
        string MsMessageCommon;
        string MsMessageCaption;

        ArrayList MaMessageArray;
        ArrayList MaStatusMessage;

        MessageBoxIcon MsMessageBoxIcon;

        ClsNotification MObjNotification;
        ClsCommonUtility MObjCommonUtility;
        clsBLLEmailSettings MobjclsBLLEmailSettings;
        

        public FrmEmailSettings()
        {
            InitializeComponent();

            MsMessageCaption = ClsCommonSettings.MessageCaption;
            MObjNotification = new ClsNotification();
            MObjCommonUtility = new ClsCommonUtility();
            MobjclsBLLEmailSettings = new clsBLLEmailSettings();
        }

        /// <summary>
        /// Loading the variables for messages
        /// </summary>
        private void LoadMessage()
        {
            MaMessageArray = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArray = MObjNotification.FillMessageArray((int)FormID.EmailSettings, ClsCommonSettings.ProductID);
            MaStatusMessage = MObjNotification.FillStatusMessageArray((int)FormID.EmailSettings, ClsCommonSettings.ProductID);
        }

        public void GetEmailSmsSettings()
        {
            // Getting Email settings
            if (MobjclsBLLEmailSettings.GetEmailSmsSettings(1))
            {
                TxtEmailUserName.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.UserName;
                TxtEmailPassword.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.PassWord;
                TxtEmailIncomingServer.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.IncomingServer;
                TxtEmailOutgoingServer.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.OutgoingServer;
                TxtEmailPortNumber.Text = (MobjclsBLLEmailSettings.clsDTOEmailSettings.PortNumber == 0 ? string.Empty : MobjclsBLLEmailSettings.clsDTOEmailSettings.PortNumber.ToString());
            }
            // Getting SMS settings
            
            if (MobjclsBLLEmailSettings.GetEmailSmsSettings(2))
            {
                TxtSmsUsername.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.UserName;
                TxtSmsPassword.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.PassWord;
                TxtSmsServer.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.IncomingServer;
                TxtSmsOutgoingserver.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.OutgoingServer;
                TxtSmsPortNumber.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.PortNumber.ToString();

            }
        }

        private void FrmEmailSettings_Load(object sender, EventArgs e)
        {
            LoadMessage();
            GetEmailSmsSettings();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (TabEmailSettings.SelectedTab == TpEmail)
            {
                if (ValidateEmailSettings() && ValidateSmsSettings())
                {
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.UserName = TxtEmailUserName.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.PassWord = TxtEmailPassword.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.IncomingServer = TxtEmailIncomingServer.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.OutgoingServer = TxtEmailOutgoingServer.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.PortNumber = Convert.ToInt32(TxtEmailPortNumber.Text.Trim());
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.AccountType = "Email";
                    if (MobjclsBLLEmailSettings.SaveEmailSettings(1))
                        MessageBox.Show(MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.SaveEmailMessage, out MsMessageBoxIcon).Replace("#", "").Trim(),
                            MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                }
            }
            else
            {
                if (ValidateSmsSettings())
                {
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.UserName = TxtSmsUsername.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.PassWord = TxtSmsPassword.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.IncomingServer = TxtSmsServer.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.OutgoingServer = TxtSmsOutgoingserver.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.PortNumber = Convert.ToInt32(TxtSmsPortNumber.Text.Trim());
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.AccountType = "SMS";
                    if (MobjclsBLLEmailSettings.SaveEmailSettings(2))
                        MessageBox.Show(MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.SaveSmsMessage, out MsMessageBoxIcon).Replace("#", "").Trim(),
                            MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                }
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        enum ErrorCode
        {
            UserNameValidate = 4001,
            PasswordValidate = 4002,
            IncomingServerValidate = 4003,
            OutgoingServerValidate = 4004,
            PortNumberValidate = 4005,
            UserNameValidEmail = 4006,
            SaveEmailMessage = 4007,
            SaveSmsMessage = 4008,
            TestFailed = 4009,
            TestSucceeded = 4010
        }

        private bool ValidateEmailSettings()
        {
            bool bValidationOk = true;

            if (String.IsNullOrEmpty(TxtEmailUserName.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.UserNameValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtEmailUserName, MsMessageCommon.Replace("#", "").Trim());
                TxtEmailUserName.Focus();
                bValidationOk = false;
            }
            else if (!MObjCommonUtility.CheckValidEmail(TxtEmailUserName.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.UserNameValidEmail, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtEmailUserName, MsMessageCommon.Replace("#", "").Trim());
                TxtEmailUserName.Focus();
                bValidationOk = false;
            }
            else if (String.IsNullOrEmpty(TxtEmailPassword.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.PasswordValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtEmailPassword, MsMessageCommon.Replace("#", "").Trim());
                TxtEmailPassword.Focus();
                bValidationOk = false;
            }
            else if (String.IsNullOrEmpty(TxtEmailIncomingServer.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.IncomingServerValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtEmailIncomingServer, MsMessageCommon.Replace("#", "").Trim());
                TxtEmailIncomingServer.Focus();
                bValidationOk = false;
            }
            else if (String.IsNullOrEmpty(TxtEmailOutgoingServer.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.OutgoingServerValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtEmailOutgoingServer, MsMessageCommon.Replace("#", "").Trim());
                TxtEmailOutgoingServer.Focus();
                bValidationOk = false;
            }
            else if (String.IsNullOrEmpty(TxtEmailPortNumber.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.PortNumberValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtEmailPortNumber, MsMessageCommon.Replace("#", "").Trim());
                TxtEmailPortNumber.Focus();
                bValidationOk = false;
            }
            else if ((TxtEmailPortNumber.Text.Trim().Length > 0 ? Convert.ToInt32(TxtEmailPortNumber.Text) : 0) == 0)
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.PortNumberValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtEmailPortNumber, MsMessageCommon.Replace("#", "").Trim());
                TxtEmailPortNumber.Focus();
                bValidationOk = false;
            }
            if (!bValidationOk)
            {
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
            }
            return bValidationOk;
        }

        private bool ValidateSmsSettings()
        {
            bool bValidationOk = true;
            if (String.IsNullOrEmpty(TxtSmsUsername.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.UserNameValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtSmsUsername, MsMessageCommon.Replace("#", "").Trim());
                TxtSmsUsername.Focus();
                bValidationOk = false;
            }
            else if (!MObjCommonUtility.CheckValidEmail(TxtSmsUsername.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.UserNameValidEmail, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtSmsUsername, MsMessageCommon.Replace("#", "").Trim());
                TxtSmsUsername.Focus();
                bValidationOk = false;
            }
            else if (String.IsNullOrEmpty(TxtSmsPassword.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.PasswordValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtSmsPassword, MsMessageCommon.Replace("#", "").Trim());
                TxtSmsPassword.Focus();
                bValidationOk = false;
            }
            else if (String.IsNullOrEmpty(TxtSmsServer.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.IncomingServerValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtSmsServer, MsMessageCommon.Replace("#", "").Trim());
                TxtSmsServer.Focus();
                bValidationOk = false;
            }
            else if (String.IsNullOrEmpty(TxtSmsOutgoingserver.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.OutgoingServerValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtSmsOutgoingserver, MsMessageCommon.Replace("#", "").Trim());
                TxtSmsOutgoingserver.Focus();
                bValidationOk = false;
            }
            else if (String.IsNullOrEmpty(TxtSmsPortNumber.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.PortNumberValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtSmsPortNumber, MsMessageCommon.Replace("#", "").Trim());
                TxtSmsPortNumber.Focus();
                bValidationOk = false;
            }
            else if ((TxtSmsPortNumber.Text.Trim().Length > 0 ? Convert.ToInt32(TxtSmsPortNumber.Text) : 0) == 0)
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.PortNumberValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtSmsPortNumber, MsMessageCommon.Replace("#", "").Trim());
                TxtSmsPortNumber.Focus();
                bValidationOk = false;
            }
            if (!bValidationOk)
            {
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
            }
            return bValidationOk;
        }

        private void BtnTestSettings_Click(object sender, EventArgs e)
        {
            if (ValidateEmailSettings())
            {
                this.Cursor = Cursors.WaitCursor;

                ClsInternet objInternet = new ClsInternet();
                if (objInternet.IsInternetConnected() &&
                    SendEmail(TxtEmailOutgoingServer.Text.Trim(), TxtEmailUserName.Text.Trim(), TxtEmailPassword.Text.Trim(),
                        Convert.ToInt32(TxtEmailPortNumber.Text.Trim()), "Test Mail", "This is a test mail"))
                {
                    MessageBox.Show(MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.TestSucceeded, out MsMessageBoxIcon).Replace("#", "").Trim(),
                                           MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                }
                else
                {
                    MessageBox.Show(MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.TestFailed, out MsMessageBoxIcon).Replace("#", "").Trim(),
                       MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                }

                this.Cursor = Cursors.Default;
            }
        }

        private bool SendEmail(string SmtpServerAddress, string UserName, string Password, int PortNumber, string Subject, string Message)
        {
            try
            {
                SmtpClient objSmtpClient = new SmtpClient();
                MailMessage mailMessage = new MailMessage();
                objSmtpClient.Credentials = new System.Net.NetworkCredential(UserName, Password);
                objSmtpClient.Port = PortNumber;
                objSmtpClient.Host = SmtpServerAddress;

                if (SmtpServerAddress.IndexOf("gmail") != -1)
                {
                    objSmtpClient.EnableSsl = true;
                }

                mailMessage.From = new MailAddress(UserName);
                mailMessage.To.Add(UserName);
                mailMessage.Subject = Subject;
                mailMessage.Body = Message;
                objSmtpClient.Send(mailMessage);

                return true;
            }
            catch
            {
                return false;
            }
        }

        private void ClearErrorProvider(object sender, EventArgs e)
        {
            ErrorEmailSettings.Clear();
        }

        private void FrmEmailSettings_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Email";
            objHelp.ShowDialog();
            objHelp = null;
        }
    }
}
