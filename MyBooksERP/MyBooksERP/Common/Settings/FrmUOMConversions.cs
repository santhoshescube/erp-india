﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;


namespace MyBooksERP//.Settings.Forms
{
    public partial class FrmUOMConversions : Form
    {
        private int MiUOMConversionID;
        //private int MiUOMRowID;
        //private int MiUOMOtherRowID;
        private int MiTimerInterval;

        private string MsMessageCaption;
        private string MsMessageCommon;

        private bool MbAddStatus;
        private bool MbChangeStatus;

        private bool MblnPrintEmailPermission = true;
        private bool MblnAddPermission = true;
        private bool MblnUpdatePermission = true;
        private bool MblnDeletePermission = true;
        private bool MblnAddUpdatePermission = true;

        //private ArrayList MaMessageArr;                  // Error Message display
        //private ArrayList MaStatusMessage;
        private MessageBoxIcon MmessageIcon;
        clsBLLCommonUtility MobjBLLCommonUtility = new clsBLLCommonUtility();
        ClsBLLUOMConversion mobjBLLUOMConversion;
        ClsLogWriter mObjLogs;
        //ClsNotification mObjNotification;
        ClsNotificationNew mObjNotification;
        DataTable MdatMessages;
        
        public FrmUOMConversions()
        {
            InitializeComponent();
            MiUOMConversionID = 0;
            MiTimerInterval = ClsCommonSettings.TimerInterval;

            MsMessageCaption = ClsCommonSettings.MessageCaption;    //Message caption
                       
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            //mObjNotification = new ClsNotification();
            mObjNotification = new ClsNotificationNew();
            mobjBLLUOMConversion = new ClsBLLUOMConversion();
            MmessageIcon = MessageBoxIcon.Information;            
        }

        private void LoadMessage()
        {
            //MaMessageArr = new ArrayList();
            //MaStatusMessage = new ArrayList();
            //MaMessageArr = mObjNotification.FillMessageArray((int)FormID.UnitConversion, 4);
            //MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.UnitConversion, 4);
            MdatMessages = mObjNotification.FillMessageArray((int)FormID.UnitConversion, 4);
        }

        private bool LoadCombo()
        {
            try
            {
                DataTable dtTemp = new DataTable();

                dtTemp = mobjBLLUOMConversion.FillCombos(new string[] { "ConversionFactorID,ConversionFactor", "ConversionFactorReference", "" });
                cboConvFactor.DisplayMember = "ConversionFactor";
                cboConvFactor.ValueMember = "ConversionFactorID";
                cboConvFactor.DataSource = dtTemp;

                dtTemp = mobjBLLUOMConversion.FillCombos(new string[] { "UOMTypeID,UOMType", "InvUOMTypeReference", "" });
                cboBaseClass.DisplayMember = "UOMType";
                cboBaseClass.ValueMember = "UOMTypeID";
                cboBaseClass.DataSource = dtTemp;
            }
            catch { }
            return true;
        }

        private void AddNewUOM()
        {
            MbAddStatus = true;
            lblConversionDisplay.Text = "1 Base UOM = N * Alternate UOM";

            SetEnable();
            UoMBindingNavigatorAddNewItem.Enabled = false;
            bindingNavigatorDeleteItem.Enabled = false;
            ErrorProUOM.Clear();
            MbChangeStatus = false;
            BtnOk.Enabled = MbChangeStatus;
            BtnSave.Enabled = MbChangeStatus;
            UoMBindingNavigatorSaveButton.Enabled = MbChangeStatus;
            DgvUOM.DataSource = getNewTable();
            BtnPrint.Enabled = BtnEmail.Enabled = false;
            LoadCombo();
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.UOMConversions, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                //if (MblnAddPermission == true || MblnUpdatePermission == true)
                //    MblnAddUpdatePermission = true;
                //else
                //    MblnAddUpdatePermission = false;

                //if (MblnAddPermission == true || MblnUpdatePermission == true || MblnDeletePermission == true)
                //    MblnViewPermission = true;
            }
            else
                MblnAddPermission =  MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            //UoMBindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            //UoMBindingNavigatorSaveButton.Enabled = MblnAddUpdatePermission;
            //bindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            //BtnOk.Enabled = MblnAddUpdatePermission;
            //BtnSave.Enabled = MblnAddUpdatePermission;
            //BtnPrint.Enabled = MblnPrintEmailPermission;
            //BtnEmail.Enabled = MblnPrintEmailPermission;
        }
        private void SetEnable()
        {
            txtConvValue.Text = "";
            cboBaseClass.SelectedIndex = -1;
            cboBaseUom.SelectedIndex = -1;
            cboUom.SelectedIndex = -1;
            cboConvFactor.SelectedIndex = 0;
            cboBaseClass.Text = cboBaseUom.Text = cboUom.Text = "";
            ErrorProUOM.Clear();
        }

        private bool LoadDataDataGridView()//DataGridView DataLoading
        {            
            DataTable DtUOMConversion = mobjBLLUOMConversion.GetUOMDetails();
            if (DtUOMConversion.Rows.Count > 0)
            {
                DgvUOM.DataSource = DtUOMConversion;
            }
            else
            {
                DgvUOM.DataSource = getNewTable();
            }
            return true;
        }

        private void Changestatus()
        {
            SetPermissions();
            //MbChangeStatus = true;
            //BtnOk.Enabled = MbChangeStatus;
            //BtnSave.Enabled = MbChangeStatus;
            //UoMBindingNavigatorSaveButton.Enabled = MbChangeStatus;
            //UoMBindingNavigatorAddNewItem.Enabled = MbChangeStatus;
            if (MbAddStatus == true)
                BtnOk.Enabled = BtnSave.Enabled = UoMBindingNavigatorSaveButton.Enabled = MblnAddPermission;
            else
                BtnOk.Enabled = BtnSave.Enabled = UoMBindingNavigatorSaveButton.Enabled = MblnUpdatePermission;
            ErrorProUOM.Clear();
        }

        private void FrmUOMConversions_Load(object sender, EventArgs e)
        {            
            MbChangeStatus = false;
            MbAddStatus = true;
            LoadDataDataGridView();
            LoadMessage();
            LoadCombo();
            AddNewUOM();
            SetPermissions();
        }

        private bool UOMValidation()
        {
            ErrorProUOM.Clear();

            if (Convert.ToInt32(cboBaseClass.SelectedValue) == 0)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3305, out MmessageIcon);
                ErrorProUOM.SetError(cboBaseClass, MsMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                UOMStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmUOM.Enabled = true;

                cboBaseClass.Focus();
                return false;
            }

            if (Convert.ToInt32(cboBaseUom.SelectedValue) == 0)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3301, out MmessageIcon);
                ErrorProUOM.SetError(cboBaseUom, MsMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                UOMStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmUOM.Enabled = true;

                cboBaseUom.Focus();
                return false;
            }

            if (Convert.ToDouble(txtConvValue.Text.Trim()) == 0)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3303, out MmessageIcon);
                ErrorProUOM.SetError(txtConvValue, MsMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                UOMStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmUOM.Enabled = true;

                txtConvValue.Focus();
                return false;
            }

            if (Convert.ToInt32(cboConvFactor.SelectedValue) == 0)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3306, out MmessageIcon);
                ErrorProUOM.SetError(cboConvFactor, MsMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                UOMStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmUOM.Enabled = true;

                cboConvFactor.Focus();
                return false;
            }

            if (Convert.ToInt32(cboUom.SelectedValue) == 0)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3302, out MmessageIcon);
                ErrorProUOM.SetError(cboUom, MsMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                UOMStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmUOM.Enabled = true;

                cboUom.Focus();
                return false;
            }

            if (cboUom.SelectedIndex > -1 && cboBaseUom.SelectedIndex > -1)
            {
                if (Convert.ToInt32(cboBaseUom.SelectedValue) == Convert.ToInt32(cboUom.SelectedValue))
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3304, out MmessageIcon);
                    ErrorProUOM.SetError(cboUom, MsMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    UOMStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmUOM.Enabled = true;
                    cboUom.Focus();
                    return false;
                }
            }
            
            if (MbAddStatus)
            {
                if (mobjBLLUOMConversion.CheckDuplication(new string[] { "UOMConversionID", "InvUOMConversions", "UOMID=" + cboBaseUom.SelectedValue.ToString() + " AND UOMID=" + cboUom.SelectedValue.ToString() }))
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3304, out MmessageIcon);
                    ErrorProUOM.SetError(cboUom, MsMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    UOMStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmUOM.Enabled = true;
                    cboUom.Focus();
                    return false;
                }
            }
            return true;
        }

        private bool SaveUOMDetails()
        {
            if (MbAddStatus)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 1, out MmessageIcon);
            }
            else
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3, out MmessageIcon);
            }

            if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                return false;
            if (MbAddStatus == true)
            {
                mobjBLLUOMConversion.clsDTOUOMConversion.intUomConversionID = 0;
            }
            else
            {
                mobjBLLUOMConversion.clsDTOUOMConversion.intUomConversionID = MiUOMConversionID;
            }

            mobjBLLUOMConversion.clsDTOUOMConversion.intUOMClassID = Convert.ToInt32(cboBaseClass.SelectedValue.ToString());
            mobjBLLUOMConversion.clsDTOUOMConversion.intUOMBaseID = Convert.ToInt32(cboBaseUom.SelectedValue.ToString());
            mobjBLLUOMConversion.clsDTOUOMConversion.intUOMID = Convert.ToInt32(cboUom.SelectedValue.ToString());
            mobjBLLUOMConversion.clsDTOUOMConversion.intConversionFactorID = Convert.ToInt32(cboConvFactor.SelectedValue.ToString());
            mobjBLLUOMConversion.clsDTOUOMConversion.dblConversionValue = Convert.ToDouble(txtConvValue.Text);
            if (mobjBLLUOMConversion.SaveUOM(MbAddStatus))
            {
                mobjBLLUOMConversion.clsDTOUOMConversion.intUomConversionID = MiUOMConversionID;
                LoadDataDataGridView();
                return true;
            }
            else
                return false;
        }

        private void DisplayUOMDetails(int ConversionID)
        {
            UOMStatusLabel.Text = "";
            lblConversionDisplay.Text = "1 Base UOM = N * Alternate UOM";

            if (ConversionID > 0)
            {
                MiUOMConversionID = ConversionID;

                if (mobjBLLUOMConversion.DisplayUOM(MiUOMConversionID))
                {
                    cboBaseClass.SelectedValue = Convert.ToInt32(mobjBLLUOMConversion.clsDTOUOMConversion.intUOMClassID);
                    cboBaseUom.SelectedValue = Convert.ToInt32(mobjBLLUOMConversion.clsDTOUOMConversion.intUOMBaseID);
                    cboUom.SelectedValue = Convert.ToInt32(mobjBLLUOMConversion.clsDTOUOMConversion.intUOMID);
                    cboConvFactor.SelectedValue = Convert.ToInt32(mobjBLLUOMConversion.clsDTOUOMConversion.intConversionFactorID);
                    txtConvValue.Text = mobjBLLUOMConversion.clsDTOUOMConversion.dblConversionValue.ToString();
                }

                DisplayConversion();
                MbAddStatus = false;
                BtnOk.Enabled = false;
                BtnSave.Enabled = false;
                UoMBindingNavigatorSaveButton.Enabled = false;
                UoMBindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                bindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
                Changestatus();
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (UOMValidation())
            {
                if (SaveUOMDetails())
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 2, out MmessageIcon);
                    UOMStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmUOM.Enabled = true;
                    UpdateGridAfterSaveUpdateDelete();
                }
            }
        }

        private void UpdateGridAfterSaveUpdateDelete()
        {
            MbAddStatus = true;
            lblConversionDisplay.Text = "1 Base UOM = N * Alternate UOM";
            int intBaseClass = cboBaseClass.SelectedValue.ToInt32();
            int intBaseUOM = cboBaseUom.SelectedValue.ToInt32();

            SetEnable();
            UoMBindingNavigatorAddNewItem.Enabled = false;
            bindingNavigatorDeleteItem.Enabled = false;
            ErrorProUOM.Clear();
            MbChangeStatus = false;
            BtnOk.Enabled = MbChangeStatus;
            BtnSave.Enabled = MbChangeStatus;
            UoMBindingNavigatorSaveButton.Enabled = MbChangeStatus;
            DgvUOM.DataSource = getNewTable();
            BtnPrint.Enabled = BtnEmail.Enabled = false;
                        
            LoadCombo();
            cboBaseClass.SelectedValue = intBaseClass;
            LoadCombos(3);
            cboBaseUom.SelectedValue = intBaseUOM;
        }

        private void DgvUOM_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DisplayUOMDetails(Convert.ToInt32(DgvUOM.Rows[e.RowIndex].Cells["UOMConversionID"].Value.ToString()));
            }
            catch { }
        }

        private void UoMBindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewUOM();
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e) // Deleting
        {
            UOMStatusLabel.Text = "";
            bool blnTempDeleteStatus = true;
            if (MiUOMConversionID > 0)
            {
                DataTable datTemp = mobjBLLUOMConversion.FillCombos(new string[] { "*", "InvUOMConversions", "UOMConversionID = " + MiUOMConversionID + " AND IsPredefined = 1" });
                if (datTemp.Rows.Count > 0)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3307, out MmessageIcon);//Predefined
                    UOMStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    blnTempDeleteStatus = false;
                }

                DataTable datTempBaseUomExists = mobjBLLUOMConversion.GetUOMExistingOrNot(Convert.ToInt32(cboBaseUom.SelectedValue));
                DataTable datTempAlterUomExists = mobjBLLUOMConversion.GetUOMExistingOrNot(Convert.ToInt32(cboUom.SelectedValue));
                try { datTempBaseUomExists.Columns["CNT"].ColumnName = "Count"; } catch { }
                try { datTempAlterUomExists.Columns["CNT"].ColumnName = "Count"; } catch { }
                
                if (Convert.ToInt32(datTempBaseUomExists.Rows[0]["Count"].ToString()) > 1
                        || Convert.ToInt32(datTempAlterUomExists.Rows[0]["Count"].ToString()) > 1)
                {
                    blnTempDeleteStatus = false;
                    MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3308, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    TmUOM.Enabled = true;
                }
                else
                    blnTempDeleteStatus = true;

                if (blnTempDeleteStatus == true)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 13, out MmessageIcon);//Delete
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        mobjBLLUOMConversion.DeleteUOM(MiUOMConversionID);
                        MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 4, out MmessageIcon);//Delete
                        UOMStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmUOM.Enabled = true;
                        UpdateGridAfterSaveUpdateDelete();
                    }
                }
            }
        }

        private void DgvUOM_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DisplayUOMDetails(Convert.ToInt32(DgvUOM.Rows[e.RowIndex].Cells["UOMConversionID"].Value.ToString()));
            }
            catch { }
        }

        private void UoMBindingNavigatorSaveButton_Click(object sender, EventArgs e)
        {
            if (UOMValidation())
            {
                if (SaveUOMDetails())
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 2, out MmessageIcon);
                    UOMStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmUOM.Enabled = true;
                    UpdateGridAfterSaveUpdateDelete();
                }
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (UOMValidation())
            {
                if (SaveUOMDetails())
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 2, out MmessageIcon);
                    UOMStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmUOM.Enabled = true;
                    this.Close();
                }
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CancelToolStripButton_Click(object sender, EventArgs e)
        {
            AddNewUOM();
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "UOM Conversion";
                    ObjEmailPopUp.EmailFormType = EmailFormID.UOMConversion;
                    int intUomClassID=0, intUomBaseID=0;
                    if (cboBaseClass.SelectedValue != null)
                        intUomClassID = Convert.ToInt32(cboBaseClass.SelectedValue.ToString());
                    if (cboBaseUom.SelectedValue != null)
                        intUomBaseID = Convert.ToInt32(cboBaseUom.SelectedValue.ToString());
                    ObjEmailPopUp.EmailSource = mobjBLLUOMConversion.DisplayUOMConversionReport(intUomClassID, intUomBaseID);
                    //ObjEmailPopUp.EmailSource = mobjBLLUOMConversion.GetUOMReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch { }
        }

        private void FrmUOMConversions_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        UoMBindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        bindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        CancelToolStripButton_Click(sender, new EventArgs());//Clear
                        break;
                     case Keys.Control | Keys.P:
                        BtnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        BtnEmail_Click(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnUom_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmUnitOfMeasurement objUnitOfMeasurement = new FrmUnitOfMeasurement())
                {
                    objUnitOfMeasurement.ShowDialog();
                }

                LoadCombos(3);
            }
            catch (Exception Ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnBaseUom_Click() " + Ex.Message.ToString());
            }
        }

        private bool LoadCombos(int intType)
        {
            try
            {                
                if (intType == 0 || intType == 3)
                {
                    if (cboBaseClass.SelectedValue != null)
                    {
                        DataTable datCombos = new DataTable();
                        datCombos = mobjBLLUOMConversion.FillCombos(new string[] { "UOMID, UOMName", "InvUOMReference", "UOMTypeID=" + cboBaseClass.SelectedValue.ToString() });

                        cboBaseUom.DisplayMember = "UOMName";
                        cboBaseUom.ValueMember = "UOMID";
                        cboBaseUom.DataSource = datCombos;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }
        private void LoadReport()
        {
            try
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiFormID = (int)FormID.UnitConversion;
                if (cboBaseClass.SelectedValue != null)
                    ObjViewer.ITem = Convert.ToInt32(cboBaseClass.SelectedValue.ToString());
                else
                    ObjViewer.ITem = 0;
                if (cboBaseUom.SelectedValue != null)
                    ObjViewer.PintoperationType = Convert.ToInt32(cboBaseUom.SelectedValue.ToString());
                else
                    ObjViewer.PintoperationType = 0;
                ObjViewer.ShowDialog();
            }
            catch { }
        }

        private void txtConvValue_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
            DisplayConversion();
        }

        DataTable getNewTable()
        {
            DataTable dtTemp = new DataTable();
            dtTemp.Columns.Add("UOMConversionID");
            dtTemp.Columns.Add("UOMClass");
            dtTemp.Columns.Add("UOMBase");
            dtTemp.Columns.Add("UOM");
            dtTemp.Columns.Add("ConversionFactor");
            dtTemp.Columns.Add("ConversionValue");
            return dtTemp;
        }

        private void cboBaseClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboBaseClass.SelectedValue != null)
            {
                DataTable datCombos = new DataTable();
                datCombos = mobjBLLUOMConversion.FillCombos(new string[] { "UOMID, UOMName", "InvUOMReference", "UOMTypeID=" + cboBaseClass.SelectedValue.ToInt32() });

                cboBaseUom.DisplayMember = "UOMName";
                cboBaseUom.ValueMember = "UOMID";
                cboBaseUom.DataSource = datCombos;

                cboBaseUom.SelectedIndex = -1;
                cboUom.SelectedIndex = -1;
                cboConvFactor.SelectedIndex = 0;
                cboBaseUom.Text = cboUom.Text = "";

                DataTable DtUOMConversion = mobjBLLUOMConversion.GetUOMDetails(cboBaseClass.SelectedValue.ToInt32());

                if (DtUOMConversion.Rows.Count > 0)
                    DgvUOM.DataSource = DtUOMConversion;
                else
                    DgvUOM.DataSource = getNewTable();

                Changestatus();
                DisplayConversion();
            }
        }

        private void cboBaseUom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboBaseUom.SelectedValue != null)
            {
                DataTable datCombos = new DataTable();
                if (cboBaseClass.SelectedValue != null)
                {
                    datCombos = mobjBLLUOMConversion.FillCombos(new string[] { "UOMID, UOMName", "InvUOMReference", "" +
                        "UOMTypeID=" + cboBaseClass.SelectedValue.ToInt32() + "AND UOMID<>" + cboBaseUom.SelectedValue.ToString() });
                }

                cboUom.DisplayMember = "UOMName";
                cboUom.ValueMember = "UOMID";
                cboUom.DataSource = datCombos;

                cboUom.SelectedIndex = -1;
                cboConvFactor.SelectedIndex = 0;
                cboUom.Text = "";
                DataTable DtUOMConversion = new DataTable();

                if (cboBaseClass.SelectedValue != null)
                    DtUOMConversion = mobjBLLUOMConversion.GetUOMDetails(cboBaseClass.SelectedValue.ToInt32(), cboBaseUom.SelectedValue.ToInt32());

                if (DtUOMConversion.Rows.Count > 0)
                    DgvUOM.DataSource = DtUOMConversion;
                else
                    DgvUOM.DataSource = getNewTable();

                Changestatus();

                DisplayConversion();
            }
        }
        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }
        private void cboUom_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus();
            DisplayConversion();
        }

        private void cboConvFactor_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus();
            DisplayConversion();
        }

        private void QuestionToolStripButton_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "UOMconversion";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void DgvUOM_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjBLLCommonUtility.SetSerialNo(DgvUOM , e.RowIndex, true);
        }

        private void DgvUOM_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjBLLCommonUtility.SetSerialNo(DgvUOM , e.RowIndex, true);
        }

        private void DisplayConversion()
        {
            lblConversionDisplay.Text = "1 Base UOM = N * Alternate UOM";

            if (cboConvFactor.SelectedValue.ToInt32() == 1)
            {
                if (cboBaseUom.Text.Trim() == "" && txtConvValue.Text.ToDecimal() == 0 && cboUom.Text.Trim() == "")
                    lblConversionDisplay.Text = "1 Base UOM = N * Alternate UOM";

                else if (cboBaseUom.Text.Trim() != "" && txtConvValue.Text.ToDecimal() == 0 && cboUom.Text.Trim() == "")
                    lblConversionDisplay.Text = "1" + cboBaseUom.Text.Trim() + " = N * Alternate UOM";

                else if (cboBaseUom.Text.Trim() == "" && txtConvValue.Text.ToDecimal() > 0 && cboUom.Text.Trim() == "")
                    lblConversionDisplay.Text = "1 Base UOM = " + txtConvValue.Text.Trim() + " * Alternate UOM";

                else if (cboBaseUom.Text.Trim() == "" && txtConvValue.Text.ToDecimal() == 0 && cboUom.Text.Trim() != "")
                    lblConversionDisplay.Text = "1 Base UOM = N * " + cboUom.Text.Trim();

                else if (cboBaseUom.Text.Trim() != "" && txtConvValue.Text.ToDecimal() > 0 && cboUom.Text.Trim() == "")
                    lblConversionDisplay.Text = "1" + cboBaseUom.Text.Trim() + " = " + txtConvValue.Text.Trim() + " * Alternate UOM";

                else if (cboBaseUom.Text.Trim() != "" && txtConvValue.Text.ToDecimal() == 0 && cboUom.Text.Trim() != "")
                    lblConversionDisplay.Text = "1" + cboBaseUom.Text.Trim() + " = N * " + cboUom.Text.Trim();

                else if (cboBaseUom.Text.Trim() == "" && txtConvValue.Text.ToDecimal() > 0 && cboUom.Text.Trim() != "")
                    lblConversionDisplay.Text = "1 Base UOM = " + txtConvValue.Text.Trim() + " * " + cboUom.Text.Trim();

                else if (cboBaseUom.Text.Trim() != "" && txtConvValue.Text.ToDecimal() > 0 && cboUom.Text.Trim() != "")
                    lblConversionDisplay.Text = "1" + cboBaseUom.Text.Trim() + " = " + txtConvValue.Text.Trim() + " * " + cboUom.Text.Trim();
            }
            else if (cboConvFactor.SelectedValue.ToInt32() == 2)
            {
                if (cboBaseUom.Text.Trim() == "" && txtConvValue.Text.ToDecimal() == 0 && cboUom.Text.Trim() == "")
                    lblConversionDisplay.Text = "1 Base UOM = Alternate UOM / N";

                else if (cboBaseUom.Text.Trim() != "" && txtConvValue.Text.ToDecimal() == 0 && cboUom.Text.Trim() == "")
                    lblConversionDisplay.Text = "1" + cboBaseUom.Text.Trim() + " = Alternate UOM / N";

                else if (cboBaseUom.Text.Trim() == "" && txtConvValue.Text.ToDecimal() > 0 && cboUom.Text.Trim() == "")
                    lblConversionDisplay.Text = "1 Base UOM = Alternate UOM / " + txtConvValue.Text.Trim();

                else if (cboBaseUom.Text.Trim() == "" && txtConvValue.Text.ToDecimal() == 0 && cboUom.Text.Trim() != "")
                    lblConversionDisplay.Text = "1 Base UOM = " + cboUom.Text.Trim() + " / N";

                else if (cboBaseUom.Text.Trim() != "" && txtConvValue.Text.ToDecimal() > 0 && cboUom.Text.Trim() == "")
                    lblConversionDisplay.Text = "1" + cboBaseUom.Text.Trim() + " = Alternate UOM / " + txtConvValue.Text.Trim();

                else if (cboBaseUom.Text.Trim() != "" && txtConvValue.Text.ToDecimal() == 0 && cboUom.Text.Trim() != "")
                    lblConversionDisplay.Text = "1" + cboBaseUom.Text.Trim() + " = " + cboUom.Text.Trim() + " / N";

                else if (cboBaseUom.Text.Trim() == "" && txtConvValue.Text.ToDecimal() > 0 && cboUom.Text.Trim() != "")
                    lblConversionDisplay.Text = "1 Base UOM = " + cboUom.Text.Trim() + " / " + txtConvValue.Text.Trim();

                else if (cboBaseUom.Text.Trim() != "" && txtConvValue.Text.ToDecimal() > 0 && cboUom.Text.Trim() != "")
                    lblConversionDisplay.Text = "1" + cboBaseUom.Text.Trim() + " = " + cboUom.Text.Trim() + " / " + txtConvValue.Text.Trim();
            }
            else
            {
                if (cboBaseUom.Text.Trim() == "" && txtConvValue.Text.ToDecimal() == 0 && cboUom.Text.Trim() == "")
                    lblConversionDisplay.Text = "1 Base UOM = N Alternate UOM";

                else if (cboBaseUom.Text.Trim() != "" && txtConvValue.Text.ToDecimal() == 0 && cboUom.Text.Trim() == "")
                    lblConversionDisplay.Text = "1" + cboBaseUom.Text.Trim() + " = N Alternate UOM";

                else if (cboBaseUom.Text.Trim() == "" && txtConvValue.Text.ToDecimal() > 0 && cboUom.Text.Trim() == "")
                    lblConversionDisplay.Text = "1 Base UOM = " + txtConvValue.Text.Trim() + " Alternate UOM";

                else if (cboBaseUom.Text.Trim() == "" && txtConvValue.Text.ToDecimal() == 0 && cboUom.Text.Trim() != "")
                    lblConversionDisplay.Text = "1 Base UOM = N " + cboUom.Text.Trim();

                else if (cboBaseUom.Text.Trim() != "" && txtConvValue.Text.ToDecimal() > 0 && cboUom.Text.Trim() == "")
                    lblConversionDisplay.Text = "1" + cboBaseUom.Text.Trim() + " = " + txtConvValue.Text.Trim() + " Alternate UOM";

                else if (cboBaseUom.Text.Trim() != "" && txtConvValue.Text.ToDecimal() == 0 && cboUom.Text.Trim() != "")
                    lblConversionDisplay.Text = "1" + cboBaseUom.Text.Trim() + " = N " + cboUom.Text.Trim();

                else if (cboBaseUom.Text.Trim() == "" && txtConvValue.Text.ToDecimal() > 0 && cboUom.Text.Trim() != "")
                    lblConversionDisplay.Text = "1 Base UOM = " + txtConvValue.Text.Trim() + " " + cboUom.Text.Trim();

                else if (cboBaseUom.Text.Trim() != "" && txtConvValue.Text.ToDecimal() > 0 && cboUom.Text.Trim() != "")
                    lblConversionDisplay.Text = "1" + cboBaseUom.Text.Trim() + " = " + txtConvValue.Text.Trim() + " " + cboUom.Text.Trim();
            }
        }

        private void txtConvValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                    e.Handled = true;

                if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                    e.Handled = true;
            }
            catch { }
        }
    }
}
