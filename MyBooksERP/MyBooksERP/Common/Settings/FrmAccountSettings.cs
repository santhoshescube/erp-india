﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace MyBooksERP
{
    /// <summary>
    /// Created By:Arun
    /// Created Date:25 Feb 2011
    /// Modified By:Arun
    /// Modified Date:
    /// Description:Company Account Settings
    /// </summary>
    public partial class FrmAccountSettings : DevComponents.DotNetBar.Office2007Form
    {
            //ID pass from navigation

      
    
        private bool MblnChangeStatus = false;//To Specify Change Status
        private bool MblnAddStatus;  //To Specify Add/Update mode                 

        private bool MblnPrintEmailPermission = false;//To Set Print/Email Permission
        private bool MblnAddPermission = false;///To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnAddUpdatePermission = false;
        private bool MblnDeleteMode = false;
        private bool MbShowErrorMess;
        private bool MblnClose = false;

        private int MintAccSettingsID;
    
        private int MiTimerInterval;
        private int MiCompanyId;
        private int MiUserId;
        //private int iPCompanyID = -1;               //Identity for present company selection
        //private int MiCurrencyID;
        //private int MiComboID = 0;
        private int MintCompanyID = 0;

        private string MstrMessageCaption;
        private string MstrMessageCommon;
        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;
        private MessageBoxIcon MmessageIcon;


        clsBLLAccountSettings MobjclsBLLAccountSettings;
        ClsLogWriter mObjClsLogWriter;
        ClsNotification mObjNotification;

        private clsMessage objUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.AccountSettings );

                return this.objUserMessage;
            }
        }

        public FrmAccountSettings()
        {
            InitializeComponent();
            MbShowErrorMess = true;
            MiCompanyId = ClsCommonSettings.CompanyID;         // current companyid
            MiUserId = ClsCommonSettings.UserID;            // current userid
            MiTimerInterval = ClsCommonSettings.TimerInterval;     // current companyid
            MstrMessageCaption = ClsCommonSettings.MessageCaption;    //Message caption
            MobjclsBLLAccountSettings = new clsBLLAccountSettings();
            mObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
        }

        private void FrmAccountSettings_Load(object sender, EventArgs e)
        {
            try
            {

                SetPermissions();
                LoadMessage();
                LoadCombos(0, "");
                CboCompanyName.SelectedValue = MintCompanyID;

                if (Convert.ToInt32(CboCompanyName.SelectedValue) >0)
                {
                    DisplayGridInfo();
                }
                
                AddNewAccountSettings();
                
            }
            catch (Exception Ex)
            {
                mObjClsLogWriter.WriteLog("Error on FrmAccountSettings_Load:Form Load " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on FormLoad() " + Ex.Message.ToString());

                
            }
        }
        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                //if (CboCompanyName.SelectedValue != null)
                //    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, Convert.ToInt32(CboCompanyName.SelectedValue), (Int32)ModuleID.Settings, (Int32)MenuID.AccountSettings, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                //else
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Accounts, (Int32)eMenuID.AccountSettings, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                if (MblnAddPermission == true || MblnUpdatePermission == true)
                    MblnAddUpdatePermission = true;
                else
                    MblnAddUpdatePermission = false;
            }
            else
                MblnAddPermission = MblnAddUpdatePermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            //GrpBxForm.Enabled = MblnViewPermission;
            BtnPrint.Enabled = MblnPrintEmailPermission;
            BtnEmail.Enabled = MblnPrintEmailPermission;
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            AccountSettingBindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
            btnOk.Enabled = MblnUpdatePermission;
            btnSave.Enabled = MblnAddUpdatePermission;
            
        }
        private void BtnAccount_Click(object sender, EventArgs e)//Chart Of Accounts
        {
            if (CboCompanyName.SelectedIndex != -1)
            {
                MiCompanyId = (int)CboCompanyName.SelectedValue;
            }
            using (FrmChartOfAccounts objChartOfAccounts = new FrmChartOfAccounts())
            {
                if (this.CboGoup.SelectedIndex != -1)
                {
                    try
                    {
                        // user will only be able to create account head or group under the account group that we set here
                        objChartOfAccounts.PermittedAccountGroup = (AccountGroups)this.CboGoup.SelectedValue.ToInt32();
                        if (this.CboAccount.SelectedIndex != -1)
                        {
                            objChartOfAccounts .SelectedAccount = (Accounts )this.CboAccount .SelectedValue .ToInt32 ();                            
                        }
                        objChartOfAccounts.intGroupID = CboGoup.SelectedValue.ToInt32();
                        objChartOfAccounts.MbCalledFromAccountSettings = true;
                    }
                    catch (Exception)
                    {
                        this.mObjClsLogWriter.WriteLog("Error occured while converting int to its equivalent enum (AccountGroups).", 3);
                    }
                }
                objChartOfAccounts.ShowDialog();
            }
            if (Convert.ToInt32(CboGoup.SelectedValue) > 0)
            {
                int iGlAccountId = Convert.ToInt32(CboGoup.SelectedValue);
                //MstrCondition = "GlAccountId=" + iGlAccountId;
                int intComboID = CboAccount.SelectedValue.ToInt32();
                LoadCombos(3, "AccountGroupID=" + iGlAccountId + " AND (CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL)");
                CboAccount.SelectedValue = intComboID;
            }
        }

        private void BtnCompany_Click(object sender, EventArgs e)//New Company
        {
            using (FrmCompany objCom = new FrmCompany())
            {
                objCom.PintCompany = Convert.ToInt32(CboCompanyName.SelectedValue);
                objCom.ShowDialog();
            }
            int intComboID = CboCompanyName.SelectedValue.ToInt32();
            LoadCombos(0,"");
            CboCompanyName.SelectedValue = intComboID;
        }

        

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveAccountSettingInfo())
                {

                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, MblnAddStatus==true ? (int)MessageCode.SavedSuccessfully: (int)MessageCode.UpdatedSuccessfully  , out  MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblAccountSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    //this.UserMessage.ShowMessage(2);
                    TmAccountSettings.Enabled = true;
                    MblnAddStatus = true;//display grid on new and update mode
                    DisplayGridInfo();
                    MblnAddStatus = false;
                    AddNewAccountSettings();
                    
                }
            }
           
           catch (Exception Ex)
            {
                mObjClsLogWriter.WriteLog("Error on filling btnSave_Click:Save" + this.Name + " " + Ex.Message.ToString(), 2);
                     
            }
          
        }

        //Showing Gird Values
        private void DgvConfiguredAccounts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex > -1 && e.RowIndex > -1)
                {
                    DisplayGridInfoToCombos();
                    Changestatus();
                }
            }
            catch (Exception Ex)
            {
                mObjClsLogWriter.WriteLog("Error on DgvConfigureAccounts_CellClick: " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

                  

        private void DgvConfiguredAccounts_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           try
           {
               if (e.ColumnIndex > -1 && e.RowIndex > -1)
               {

                   DisplayGridInfoToCombos();
                  

               }
           }
            catch (Exception Ex)
            {
                mObjClsLogWriter.WriteLog("Error on DgvConfigureAccounts_CellClickContentClick: " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

               
            }
            
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveAccountSettingInfo())
                {
                    MblnClose = true;
                    this.Close();
                }
            }
            catch (Exception Ex)
            {
                mObjClsLogWriter.WriteLog("Error on BtnOK_Click: " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on btnOk_click() " + Ex.Message.ToString());


            }
        }

        private void AccountSettingBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveAccountSettingInfo())
                {

                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, (int)MessageCode.SavedSuccessfully , out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblAccountSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    //this.UserMessage.ShowMessage(2);
                    TmAccountSettings.Enabled = true;
                    DisplayGridInfo();
                    AddNewAccountSettings();
                   
                }
            }
            catch (Exception Ex)
            {
                mObjClsLogWriter.WriteLog("Error on BindingNavigatorSAveItem: " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on Bindingnavigatorsaveitem " + Ex.Message.ToString());


            }
           
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                int MiAccountID = CboAccount.SelectedValue.ToInt32();
                if (DeleteValidation(MiAccountID))
                {
                    if (DgvConfiguredAccounts.RowCount != 0)
                    {
                        if (DgvConfiguredAccounts.SelectedRows != null)
                        {
                            if (DeleteAccountSettingsInfo())
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, (int)MessageCode.DeletedSuccessfully , out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblAccountSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                //this.UserMessage.ShowMessage(4);
                              
                                TmAccountSettings.Enabled = true;
                                DisplayGridInfo();
                               
                                AddNewAccountSettings();

                            }
                        }
                    }
                }
                
            }
            catch (Exception Ex)
            {
                mObjClsLogWriter.WriteLog("Error on BindingNAvigatorDEleteITem: " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on BindingNAvigatorDeleteitem " + Ex.Message.ToString());


            }
        }
        private bool DeleteValidation(int MiAccountID)
        {
            bool blnretvalue = true;
            DataTable dtIds = MobjclsBLLAccountSettings.CheckAccountIDExists(MiAccountID);
            if (dtIds.Rows.Count > 0)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3958, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblAccountSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //this.UserMessage.ShowMessage(2370);
               TmAccountSettings.Enabled = true;
               blnretvalue = false;
            }
            return blnretvalue;
        }
        private void DgvConfiguredAccounts_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (DgvConfiguredAccounts.IsCurrentCellDirty)
                {
                    if (DgvConfiguredAccounts.CurrentCell != null)
                        DgvConfiguredAccounts.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch (Exception ex)
            {
                mObjClsLogWriter.WriteLog("Error on DgvConfiguredAccounts_CurrentCellDirtyStateChanged: " + this.Name + " " + ex.Message.ToString(), 2);
                
            }
        
        }

        private void CancelToolStripButton_Click(object sender, EventArgs e)
        {
            AddNewAccountSettings();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
      

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewAccountSettings();
        }
        private void FrmAccountSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool blnCloseconfirm = false;

            if (MblnChangeStatus && MblnClose==false)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    mObjNotification = null;
                    MobjclsBLLAccountSettings = null;
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }

        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void LoadReport()
        {
            if (MintCompanyID > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = MintCompanyID;
                ObjViewer.PiFormID = (int)FormID.AccountSettings;
                ObjViewer.ShowDialog();
            }
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            //using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
            //{
            //    ObjEmailPopUp.MsSubject = "Account Settings";
            //    ObjEmailPopUp.MiRecordID = Convert.ToInt32(CboCompanyName.SelectedValue);
            //    ObjEmailPopUp.EmailFormType = EmailFormID.AccountSettings;
            //    ObjEmailPopUp.ShowDialog();
            //}

            using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
            {
                ObjEmailPopUp.MsSubject = "Account Settings";
                ObjEmailPopUp.MiRecordID = Convert.ToInt32(CboCompanyName.SelectedValue);
                ObjEmailPopUp.EmailFormType = EmailFormID.AccountSettings;
                ObjEmailPopUp.EmailSource = MobjclsBLLAccountSettings.GetAccountSettingsReport();
                ObjEmailPopUp.ShowDialog();
            }
        }

        private void FrmAccountSettings_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                         break;
                    //case Keys.Control | Keys.S:
                    //    btnSave_Click(sender,new EventArgs());//save
                    //  break;
                    //case Keys.Control | Keys.O:
                    //    btnOk_Click(sender, new EventArgs());//OK
                    //    break; 
                    //case Keys.Control | Keys.L:
                    //    btnCancel_Click(sender, new EventArgs());//Cancel
                    //    break;
                    case Keys.Control | Keys.Enter:
                      BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                       BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        CancelToolStripButton_Click(sender, new EventArgs());//Clear
                        break;
                                   
                    case Keys.Control | Keys.P:
                        BtnPrint_Click(sender, new EventArgs());//Cancel
                        break;
                    case Keys.Control | Keys.M:
                        BtnEmail_Click(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void CboCompanyName_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void CboTransactionType_SelectionChangeCommitted(object sender, EventArgs e)
        {
           
        }

        private void CboGoup_SelectionChangeCommitted(object sender, EventArgs e)
        {
           
        }

        /////-----------------------Functions------------------////////      
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = mObjNotification.FillMessageArray((int)FormID.AccountSettings, 4);
            MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.AccountSettings, 4);
        }

        private bool LoadCombos(int Type, string MstrCondition)
        {
            try
            {

                switch (Type)
                {
                    case 0:
                    case 1:
                    case 2:

                        {
                            DataTable datCompanyCombos = new DataTable();
                            datCompanyCombos = MobjclsBLLAccountSettings.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID });
                            CboCompanyName.ValueMember = "CompanyID";
                            CboCompanyName.DisplayMember = "CompanyName";
                            CboCompanyName.DataSource = datCompanyCombos;
                            //datCombos.Clear();
                            DataTable datCombosTrns = new DataTable();
                            datCombosTrns = MobjclsBLLAccountSettings.FillCombos(new string[] { "AccountTransactionTypeID,AccountTransactionType", "AccAccountTransactionTypeReference", "" });
                            CboTransactionType.ValueMember = "AccountTransactionTypeID";
                            CboTransactionType.DisplayMember = "AccountTransactionType";
                            CboTransactionType.DataSource = datCombosTrns;
                           
                            break;

                        }
                    case 3:
                        {
                            DataTable datAccountCombos = new DataTable();
                            CboAccount.DataSource = null;
                            datAccountCombos = MobjclsBLLAccountSettings.FillCombos(new string[] { "AccountID,AccountName ", "AccAccountMaster", MstrCondition });
                            CboAccount.ValueMember = "AccountID";
                            CboAccount.DisplayMember = "AccountName";
                            CboAccount.DataSource = datAccountCombos;
                            CboAccount.SelectedIndex = -1;
                            break;
                        }

                    case 4:
                        {
                            DataTable datGoupCombos = new DataTable();
                            CboGoup.DataSource = null;
                            datGoupCombos = MobjclsBLLAccountSettings.FillCombos(new string[] { "AccountGroupID,AccountGroupName", "AccAccountGroup", MstrCondition, });
                            CboGoup.ValueMember = "AccountGroupID";
                            CboGoup.DisplayMember = "AccountGroupName";
                            CboGoup.DataSource = datGoupCombos;
                            //CboGoup.SelectedIndex = -1;
                            
                            break;

                        }
                        
                }
                return true;
                
            }
            catch (Exception Ex)
            {
                mObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

       

        //// Validation For Saving
        private bool AccountSettingsValidation()
        {
            if (CboCompanyName.SelectedIndex == -1)
            {
                ErrorProviderAccountSettings.Clear();
                //MsMessageCommon = "Please enter a valid Company Name";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3951, out MmessageIcon);
                //MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrorProviderAccountSettings.SetError(CboCompanyName, MstrMessageCommon.Replace("#", "").Trim());
                lblAccountSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmAccountSettings.Enabled = true;
                CboCompanyName.Focus();
                return false;
            }

            if (CboTransactionType.SelectedIndex == -1)
            {
                ErrorProviderAccountSettings.Clear();
                //MsMessageCommon = "Please enter a valid Transcaction Type";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3952, out MmessageIcon);
                //MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrorProviderAccountSettings.SetError(CboTransactionType, MstrMessageCommon.Replace("#", "").Trim());
                lblAccountSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmAccountSettings.Enabled = true;
                CboTransactionType.Focus();
                return false;
            }

            if (CboGoup.SelectedIndex == -1)
            {
                ErrorProviderAccountSettings.Clear();
                //MsMessageCommon = "Please enter a valid Group";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3953, out MmessageIcon);
                //MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrorProviderAccountSettings.SetError(CboGoup, MstrMessageCommon.Replace("#", "").Trim());
                lblAccountSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmAccountSettings.Enabled = true;
                CboGoup.Focus();
                return false;
            }

            if (CboAccount.SelectedIndex == -1)
            {
                ErrorProviderAccountSettings.Clear();
                //MsMessageCommon = "Please enter a valid Account";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3954, out MmessageIcon);
                //MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrorProviderAccountSettings.SetError(CboAccount, MstrMessageCommon.Replace("#", "").Trim());
                lblAccountSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmAccountSettings.Enabled = true;
                CboAccount.Focus();
                return false;
            }

            if (MobjclsBLLAccountSettings.CheckDuplication(MblnAddStatus, new string[] { Convert.ToString(CboCompanyName.SelectedValue), Convert.ToString(CboTransactionType.SelectedValue), Convert.ToString(CboGoup.SelectedValue), Convert.ToString(CboAccount.SelectedValue) }, Convert.ToInt32(MobjclsBLLAccountSettings.clsDTOAccountSettings.intSettingsID)))
            {
                ErrorProviderAccountSettings.Clear();
                //MsMessageCommon = "Duplicate values not permitted";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3955, out MmessageIcon);
                //MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrorProviderAccountSettings.SetError(CboTransactionType, MstrMessageCommon.Replace("#", "").Trim());
                lblAccountSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmAccountSettings.Enabled = true;
                CboCompanyName.Focus();
                return false;
            }
            return true;
        }

        private bool SaveAccountSettingInfo()
        {
            try
            {
                bool mblnSaveConfirmation = false;
                
                if (!AccountSettingsValidation())
                {
                    return false;
                }
               MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, MblnAddStatus == true ? MessageCode.DoYouWishToSave :MessageCode.DoYouWishToUpdate , out MmessageIcon);
                   //if (!mblnSaveConfirmation)
                    //return false;
               if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return false;

                FillParameters();
                if (MobjclsBLLAccountSettings.SaveAccountSettings())
                {
                    mObjClsLogWriter.WriteLog("Saved successfully:SaveAccountSettingInfo()  " + this.Name + "", 0);
                    return true;
                }
                else
                {
                    return false;
                }
                }
            
            catch (Exception Ex)
            {
                mObjClsLogWriter.WriteLog("Error on Save:SaveAccountSettingInfo() " + this.Name + " " + Ex.Message.ToString(), 3);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on SaveAccountSettingInfo() " + Ex.Message.ToString());
                return false;
            }
        }
        private void FillParameters()
        {
            
            MobjclsBLLAccountSettings.clsDTOAccountSettings.intSettingsID = MintAccSettingsID;
            MobjclsBLLAccountSettings.clsDTOAccountSettings.intCompanyID = Convert.ToInt32(CboCompanyName.SelectedValue);
            MobjclsBLLAccountSettings.clsDTOAccountSettings.intOperationModId = Convert.ToInt32(CboTransactionType.SelectedValue);
            MobjclsBLLAccountSettings.clsDTOAccountSettings.intGLAccountID = Convert.ToInt32(CboGoup.SelectedValue);
            MobjclsBLLAccountSettings.clsDTOAccountSettings.intAccountID = Convert.ToInt32(CboAccount.SelectedValue); 
        }
        private void DisplayGridInfo()
        {
            if (MblnAddStatus )
            {
                DgvConfiguredAccounts.Rows.Clear();
                MobjclsBLLAccountSettings.clsDTOAccountSettings.intCompanyID = Convert.ToInt32(CboCompanyName.SelectedValue);
                DataTable DtGrid = MobjclsBLLAccountSettings.DisplayGridInformation();

                if (DtGrid.Rows.Count > 0)
                {
                    for (int i = 0; i < DtGrid.Rows.Count; i++)//displaying grid values
                    {
                        DgvConfiguredAccounts.RowCount = DgvConfiguredAccounts.RowCount + 1;
                        DgvConfiguredAccounts.Rows[i].Cells["ColSettingsID"].Value = DtGrid.Rows[i]["AccountSettingsID"];

                        DgvConfiguredAccounts.Rows[i].Cells["ColTransactionType"].Tag = DtGrid.Rows[i]["AccountTransactionTypeID"];
                        DgvConfiguredAccounts.Rows[i].Cells["ColTransactionType"].Value = DtGrid.Rows[i]["AccountTransactionType"];

                        DgvConfiguredAccounts.Rows[i].Cells["ColGroup"].Tag = DtGrid.Rows[i]["AccountGroupID"];
                        DgvConfiguredAccounts.Rows[i].Cells["ColGroup"].Value = DtGrid.Rows[i]["AccountGroupName"];

                        DgvConfiguredAccounts.Rows[i].Cells["ColAccount"].Tag = DtGrid.Rows[i]["AccountId"];
                        DgvConfiguredAccounts.Rows[i].Cells["ColAccount"].Value = DtGrid.Rows[i]["AccountName"];
                    }
                }
                Changestatus();
                DgvConfiguredAccounts.ClearSelection();
            }
        }

        private void DisplayGridInfoToCombos()
        {

            MblnAddStatus = false;
            CboCompanyName.SelectedValue = MobjclsBLLAccountSettings.clsDTOAccountSettings.intCompanyID;
            MintAccSettingsID = Convert.ToInt32(DgvConfiguredAccounts.CurrentRow.Cells["ColSettingsID"].Value);
            CboTransactionType.SelectedValue = Convert.ToInt32(DgvConfiguredAccounts.CurrentRow.Cells["ColTransactionType"].Tag);
            int intTransType = Convert.ToInt32(DgvConfiguredAccounts.CurrentRow.Cells["ColTransactionType"].Tag);
            switch (intTransType)
            {
                case (int)TransactionTypes.CashPurchase:
                case  (int)TransactionTypes.CreditPurchase:
                case (int)TransactionTypes.PurchaseReturn:
                    {
                        LoadCombos(4, "AccountGroupID=" + Convert.ToInt32(AccountGroups.PurchaseAccounts) + " AND (CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL)");
                        break;
                    }
                case (int)TransactionTypes.CashSale:
                case (int)TransactionTypes.CreditSale:
                case (int)TransactionTypes.SaleReturn:

                    {
                        LoadCombos(4, "AccountGroupID=" + Convert.ToInt32(AccountGroups.SalesAccounts) + " AND (CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL)");
                        break;
                    }
                case (int)TransactionTypes.Payment:
                    {
                        LoadCombos(4, "AccountGroupID=" + Convert.ToInt32(AccountGroups.IndirectExpense) + " AND (CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL)");

                        break;
                    }
                case (int)TransactionTypes.Receipts:
                    {
                        LoadCombos(4, "AccountGroupID=" + Convert.ToInt32(AccountGroups.IndirectIncome) + " AND (CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL)");

                        break;
                    }
            }

            CboGoup.SelectedValue = Convert.ToInt32(DgvConfiguredAccounts.CurrentRow.Cells["ColGroup"].Tag);
            this.BtnAccount.Enabled = CboGoup.SelectedValue.ToInt32() > 0;
            int intGrp = Convert.ToInt32(DgvConfiguredAccounts.CurrentRow.Cells["ColGroup"].Tag);
           
            if (intGrp > 0)
            {
                 //MstrCondition = "GlAccountId=" + intGrp;
                LoadCombos(3, "AccountGroupID=" + intGrp + " AND (CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL)");
            }
            CboAccount.SelectedValue = Convert.ToInt32(DgvConfiguredAccounts.CurrentRow.Cells["ColAccount"].Tag);
            BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
            CboCompanyName.Enabled = false;
            CboTransactionType.Enabled = false;
            BtnCompany.Enabled = false;

                      
        }

        private bool AddNewAccountSettings()
        {
            this.BtnAccount.Enabled = false;
           
            //TmAccountSettings.Enabled = true;
            CboCompanyName.Focus();
            ClearAllControls();
            MblnAddStatus = true;
            MblnChangeStatus = false;
            BtnCompany.Enabled = true;
            ErrorProviderAccountSettings.Clear();
            return true;
        }
        private void ClearAllControls()
        {

            MintAccSettingsID = 0;
            CboCompanyName.SelectedIndex   = -1;
            CboTransactionType.SelectedIndex = -1;
            CboGoup.SelectedIndex = -1;
            CboAccount.SelectedIndex = -1;
            DgvConfiguredAccounts.ClearSelection();
            CboCompanyName.Enabled = true ;
            CboTransactionType.Enabled = true;
            ErrorProviderAccountSettings.Clear();
        }
            
        private void setEnable()
        {
            MblnAddStatus = true;
            BindingNavigatorDeleteItem.Enabled = false;
            AccountSettingBindingNavigatorSaveItem.Enabled = false;
            btnOk.Enabled = false;
            btnSave.Enabled = false;
            BtnPrint.Enabled = false;
            BtnEmail.Enabled = false;
            BindingNavigatorAddNewItem.Enabled = false;
           
        }

        private void Changestatus()
        {
            MblnChangeStatus = true;
            //btnOk.Enabled = MblnAddUpdatePermission;
            //btnSave.Enabled = MblnAddUpdatePermission;
            //BtnPrint.Enabled = MblnPrintEmailPermission;
            //BtnEmail.Enabled = MblnPrintEmailPermission;
            //AccountSettingBindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
            //BindingNavigatorDeleteItem.Enabled = MblnDeletePermission; ;
            //BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            if (MblnAddStatus == true)
            {
                btnOk.Enabled = btnSave.Enabled = AccountSettingBindingNavigatorSaveItem.Enabled = MblnAddPermission;
                BtnPrint.Enabled = BtnEmail.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
            }
            else
            {
                btnOk.Enabled = btnSave.Enabled = AccountSettingBindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission ;
                BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            }
            BindingNavigatorAddNewItem.Enabled = true;
            ErrorProviderAccountSettings.Clear();
        }
        private void Changestatus(object sender, EventArgs e)
        {
            Changestatus();
        }
    
        private bool DeleteAccountSettingsInfo()
        {
            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3956, out MmessageIcon);

            if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                return false;
            if (DgvConfiguredAccounts.CurrentRow != null)
            {
                MobjclsBLLAccountSettings.clsDTOAccountSettings.intSettingsID = Convert.ToInt32(DgvConfiguredAccounts.CurrentRow.Cells[0].Value);
                MobjclsBLLAccountSettings.DeleteInfo();
                return true;
            }
            else
            {
                return false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                // Calling Reference form for UserRoles\

                FrmBackup objfrmBackUp;
                objfrmBackUp = new FrmBackup();
                objfrmBackUp.ShowDialog();
                objfrmBackUp = null;

                //LoadCombos(1);
            }
            catch (Exception Ex)
            {

                MessageBox.Show("Error on btnUserRoles_Click() " + Ex.Message.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                // Calling Reference form for UserRoles\

                FrmOrganizationHierarchySetting objFrmHierarchySetting;
                objFrmHierarchySetting = new FrmOrganizationHierarchySetting();
                objFrmHierarchySetting.ShowDialog();
                objFrmHierarchySetting = null;

                //LoadCombos(1);
            }
            catch (Exception Ex)
            {

                MessageBox.Show("Error on btnUserRoles_Click() " + Ex.Message.ToString());
            }
        }

        private void CboCompanyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboCompanyName.SelectedValue != null && CboCompanyName.SelectedValue.ToInt32() > 0)
            { 
                MintCompanyID = Convert.ToInt32(CboCompanyName.SelectedValue);
                DisplayGridInfo();
            }

                

            //CboTransactionType.SelectedValue = -1;           
            //CboGoup.SelectedValue = -1;
            //CboAccount.SelectedValue = -1;
            //SetPermissions();
            //Changestatus();
            //BindingNavigatorDeleteItem.Enabled = false;
            //AccountSettingBindingNavigatorSaveItem.Enabled = false;
            //BtnPrint.Enabled = false;
            //BtnEmail.Enabled = false;
            //btnSave.Enabled = false;
            //btnOk.Enabled = false;
        }

        private void Combo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                ((ComboBox)sender).DroppedDown = false;
            }
            catch (Exception )
            {
            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Accountsettings";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void CboCompanyName_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(CboCompanyName.SelectedValue) > 0)
            {
                DisplayGridInfo();
            }
        }


        private void CboGoup_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void CboTransactionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboTransactionType.SelectedIndex != -1 && Convert.ToInt32(CboTransactionType.SelectedValue) > 0)
            {
                switch (CboTransactionType.SelectedValue.ToInt32 ())
                {
                    case (int)TransactionTypes.CashPurchase:
                    case (int)TransactionTypes.CreditPurchase:
                    case (int)TransactionTypes.PurchaseReturn:

                        {
                            LoadCombos(4, "AccountGroupID = " + Convert.ToInt32(AccountGroups.PurchaseAccounts) + " AND (CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL)");//Cash Purchase, Credit Purchase, Purchase Return
                            break;
                        }

                    case (int)TransactionTypes.CashSale:
                    case (int)TransactionTypes.CreditSale:
                    case (int)TransactionTypes.SaleReturn:
                    case (int)TransactionTypes.POS:
                        {
                            LoadCombos(4, "AccountGroupID = " + Convert.ToInt32(AccountGroups.SalesAccounts) + " AND (CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL)");//Cash Sales, Credit Sales, Sales Return
                            break;
                        }
                    case (int)TransactionTypes.Receipts: 
                        {
                            LoadCombos(4, "AccountGroupID = " + Convert.ToInt32(AccountGroups.IndirectIncome) + " AND (CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL)");//Recipts

                            break;
                        }
                    case (int)TransactionTypes.Payment:
                    case (int)TransactionTypes.PurchaseOrder:
                    case (int)TransactionTypes.PurchaseInvoice:
                    case (int)TransactionTypes.SalesOrder:
                    case (int)TransactionTypes.SalesInvoice:
                        {
                            LoadCombos(4, "AccountGroupID = " + Convert.ToInt32(AccountGroups.IndirectExpense) + " AND (CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL)");//Payments

                            break;
                        }
                    case (int)TransactionTypes.Salary:
                    case (int)TransactionTypes.SalaryAdvance:
                    case (int)TransactionTypes.VacationSalary:
                    case (int)TransactionTypes.Settlement:
                        {
                            LoadCombos(4, "AccountGroupID = " + Convert.ToInt32(AccountGroups.SalaryAndAllowance) + " AND (CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL)");//SalaryAndAllowance
                            break;
                        }
                    case (int)TransactionTypes.JobOrder:
                        {
                            LoadCombos(4, "AccountGroupID = " + Convert.ToInt32(AccountGroups.IndirectIncome) + " AND (CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL)");//Job Order
                            break;
                        }
                }
                this.BtnAccount.Enabled = true;
            }
        }

        private void CboGoup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboGoup.SelectedIndex != -1 && Convert.ToInt32(CboGoup.SelectedValue) > 0)
            {
                int iGlAccountId = Convert.ToInt32(CboGoup.SelectedValue);
                this.BtnAccount.Enabled = true;
                LoadCombos(3, "AccountGroupID=" + iGlAccountId + " AND (CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL)");
            }

        }

      

    }
}
