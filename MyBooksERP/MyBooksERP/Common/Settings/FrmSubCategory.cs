﻿using System;
using System.Data;
using System.Windows.Forms;


namespace MyBooksERP
{
    public partial class frmSubCategory : DevComponents.DotNetBar.Office2007Form
    {
        #region Private Variables

        bool MblnShowErrorMess = true;   // Checking whether error messages are showing or not

        string MstrCommonMessage;

        DataTable MdatMessages;

        MessageBoxIcon MmsgMessageIcon;

        clsBLLSubCategory MobjClsBLLSubCategory;
        ClsNotificationNew MobjClsNotification;
        ClsLogWriter MobjClsLogWriter;

        #endregion

        public int CategoryID { get; set; }

        public frmSubCategory()
        {
            this.CategoryID = -1;

            InitializeComponent();

            MobjClsBLLSubCategory = new clsBLLSubCategory();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotificationNew();

            tmSubCategory.Interval = ClsCommonSettings.TimerInterval;
        }

        private void FormLoad(object sender, EventArgs e)
        {
            LoadMessage();
            LoadCombo(0);
            ResetForm();
        }

        /// <summary>
        /// Loading the variables for messages
        /// </summary>
        private void LoadMessage()
        {
            MdatMessages = MobjClsNotification.FillMessageArray((int)FormID.ItemMaster, ClsCommonSettings.ProductID);
        }

        private bool LoadCombo(int intType)
        {
            try
            {
                if (intType == 0 || intType == 1) // Operation Types
                {
                    DataTable datCombos = MobjClsBLLSubCategory.FillCombos(new string[] { "CategoryID, CategoryName", "InvCategoryReference", "" });
                    cboCategory.DisplayMember = "CategoryName";
                    cboCategory.ValueMember = "CategoryID";
                    cboCategory.DataSource = datCombos;
                }
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private void ResetForm()
        {
            try
            {
                ClearControls();
                cboCategory.SelectedIndex = 0;
                CategoryChanged(cboCategory, new EventArgs());

                if (this.CategoryID > 0)
                    this.cboCategory.SelectedValue = this.CategoryID;

                bnAddNewItem.Enabled = false;
                bnDeleteItem.Enabled = false;
                bnEmail.Enabled = false;
                bnPrint.Enabled = false;
                btnOk.Enabled = false;
                btnSave.Enabled = false;
                bnSaveItem.Enabled = false;

                lblStatus.Text = "Add new Information";
                tmSubCategory.Enabled = true;

                cboCategory.Focus();
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void ClearControls()
        {
            txtDescription.Clear();
            txtSubCategoryName.Clear();
            txtSubCategoryName.Tag = "0";
        }

        private void AddNewItem(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void SaveItem(object sender, EventArgs e)
        {
            try
            {
                if (!btnSave.Enabled) return;

                if (ValidateFields())
                {
                    if (SaveSubCategory())
                    {
                        if (this.MobjClsBLLSubCategory.objDTOSubCategory.intCategoryID > 0)
                            this.CategoryID = this.MobjClsBLLSubCategory.objDTOSubCategory.intCategoryID;

                        ResetForm();
                    }
                    else
                        return;

                    if (sender.GetType() == typeof(Button))
                    {
                        if (((Button)sender).Name == btnOk.Name)
                            Close();
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private bool SaveSubCategory()
        {
            MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, (Convert.ToInt32(txtSubCategoryName.Tag) == 0 ? 1 : 3),
                out MmsgMessageIcon);
            if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                MessageBoxIcon.Information) == DialogResult.No)
                return false;

            MobjClsBLLSubCategory.objDTOSubCategory.intCategoryID = Convert.ToInt32(cboCategory.SelectedValue);
            MobjClsBLLSubCategory.objDTOSubCategory.intSubCategoryID = Convert.ToInt32(txtSubCategoryName.Tag) == 0 ? 0 : Convert.ToInt32(txtSubCategoryName.Tag);
            MobjClsBLLSubCategory.objDTOSubCategory.strDescription = txtDescription.Text.Trim();
            MobjClsBLLSubCategory.objDTOSubCategory.strSubCategoryName = txtSubCategoryName.Text.Trim();

            return MobjClsBLLSubCategory.SaveSubCategoryInfo();
        }

        private bool ValidateFields()
        {
            bool blnValid = true;
            Control cntrlFocus = null;

            errSubCategory.Clear();

            MobjClsBLLSubCategory.objDTOSubCategory.strSubCategoryName = txtSubCategoryName.Text.Trim();

            if (Convert.ToInt32(cboCategory.SelectedValue) == 0)
            {
                //Please select Category
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 1604, out MmsgMessageIcon);
                cntrlFocus = cboCategory;
                blnValid = false;
            }
            else if (txtSubCategoryName.Text.Trim().Length == 0)
            {
                //Please enter Sub Category Name
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 1645, out MmsgMessageIcon);
                cntrlFocus = txtSubCategoryName;
                blnValid = false;
            }
            else if (MobjClsBLLSubCategory.CheckDuplicateSubCategory())
            {
                //Please enter Sub Category Name
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 1646, out MmsgMessageIcon);
                cntrlFocus = txtSubCategoryName;
                blnValid = false;
            }

            if (!blnValid)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                errSubCategory.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                cntrlFocus.Focus();
            }
            return blnValid;
        }

        private void DisplaySubCategoryInfo()
        {
            if (MobjClsBLLSubCategory.GetSubCategoryInfo())
            {
                txtSubCategoryName.Tag = MobjClsBLLSubCategory.objDTOSubCategory.intSubCategoryID.ToString();
                txtSubCategoryName.Text = MobjClsBLLSubCategory.objDTOSubCategory.strSubCategoryName.Trim();
                txtDescription.Text = MobjClsBLLSubCategory.objDTOSubCategory.strDescription.Trim();
                cboCategory.SelectedValue = MobjClsBLLSubCategory.objDTOSubCategory.intCategoryID;

                bnSaveItem.Enabled = false;
                btnOk.Enabled = false;
                btnSave.Enabled = false;

                bnAddNewItem.Enabled = true;
                bnDeleteItem.Enabled = true;
                bnEmail.Enabled = true;
                bnPrint.Enabled = true;
            }
        }

        private void CategoryChanged(object sender, EventArgs e)
        {
            dgvSubCategory.Rows.Clear();

            int intRowIndex = 0;

            MobjClsBLLSubCategory.objDTOSubCategory.intCategoryID = Convert.ToInt32(cboCategory.SelectedValue);
            DataTable datCategory = MobjClsBLLSubCategory.GetSubCategoryReport().Tables[0];

            foreach (DataRow drow in datCategory.Rows)
            {
                dgvSubCategory.Rows.Add();
                dgvSubCategory[dgvColSubCategoryID.Index, intRowIndex].Value = drow["SubCategoryID"];
                dgvSubCategory[dgvColSubCategory.Index, intRowIndex].Value = drow["SubCategoryName"];
                dgvSubCategory[dgvColDescription.Index, intRowIndex].Value = drow["Description"];
                intRowIndex++;
            }

            dgvSubCategory.CurrentCell = null;
            dgvSubCategory.ClearSelection();
            dgvSubCategory_SelectionChanged(dgvSubCategory, e);
        }

        private void dgvSubCategory_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvSubCategory.CurrentRow != null)
            {
                MobjClsBLLSubCategory.objDTOSubCategory.intSubCategoryID =
                    Convert.ToInt32(dgvSubCategory.CurrentRow.Cells[dgvColSubCategoryID.Index].Value);
                DisplaySubCategoryInfo();
            }
            else
            {
                ClearControls();
            }
        }

        private void DeleteItem(object sender, EventArgs e)
        {
            try
            {
                DeleteSubCategoryInfo();
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }
        private bool DeleteValidation()
        {
            if (MobjClsBLLSubCategory.GetFormsInUse(MobjClsBLLSubCategory.objDTOSubCategory.intSubCategoryID))
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 9001, out MmsgMessageIcon);
                MstrCommonMessage = MstrCommonMessage.Replace("*", "Sub Category");
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                tmSubCategory.Enabled = true;
                return false;
            }
            return true;
        }

        private bool DeleteSubCategoryInfo()
        {
            try
            {
                if (DeleteValidation())
                {
                    //Do you want to delete the Item ?
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 13, out MmsgMessageIcon);
                    if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    {
                        return false;
                    }

                    if (MobjClsBLLSubCategory.DeleteSubCategoryInfo())
                    {
                        ResetForm();
                        return true;
                    }

                    return false;
                }
                return false;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on Delete Function() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on Delete Function() " + Ex.Message.ToString());

                return false;
            }
        }

        private void FormKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (!DeleteSubCategoryInfo())
                {
                    e.Handled = true;
                }
            }
        }

        private void Cancel(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ChangeStatus(object sender, EventArgs e)
        {
            errSubCategory.Clear();

            btnSave.Enabled = true;
            btnOk.Enabled = true;
            bnSaveItem.Enabled = true;
        }

        private void Close(object sender, EventArgs e)
        {
            Close();
        }

        private void TimerTick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            tmSubCategory.Enabled = false;
        }

        private void Email(object sender, EventArgs e)
        {
            using (FrmEmailPopup objEmailPopUp = new FrmEmailPopup())
            {
                //objEmailPopUp.MsSubject = "Expense Details";
                //objEmailPopUp.EmailFormType = EmailFormID.Expense;
                //objEmailPopUp.EmailSource = MobjClsBLLSubCategory.GetExpenseReport();
                //objEmailPopUp.ShowDialog();
            }
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (sender.GetType() == typeof(ComboBox))
            {
                ComboBox cbo = (ComboBox)sender;
                cbo.DroppedDown = false;
            }
        }

        private void ClosingForm(object sender, FormClosingEventArgs e)
        {
            if (btnOk.Enabled)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 8, out MmsgMessageIcon).Replace("#", "").Trim();
                if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.Yes)
                {
                    MobjClsNotification = null;
                    MobjClsLogWriter = null;
                    MobjClsBLLSubCategory = null;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {

        }

        private void bnHelp_Click(object sender, EventArgs e)
        {

        }

        private void btnCategory_Click(object sender, EventArgs e)
        {
            try
            {
                // Calling Reference form for ItemCategory
                FrmCommonRef objCommon = new FrmCommonRef("Category", new int[] { 1, 0 }, "CategoryID, CategoryName As Category", "InvCategoryReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                LoadCombo(1);
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnCategory_Click() " + Ex.Message.ToString());
            }
        }

        private void cboCategory_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void frmSubCategory_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    //case Keys.Control | Keys.S:
                    //    btnSave_Click(sender,new EventArgs());//save
                    //  break;
                    //case Keys.Control | Keys.O:
                    //    btnOk_Click(sender, new EventArgs());//OK
                    //    break; 
                    //case Keys.Control | Keys.L:
                    //    btnCancel_Click(sender, new EventArgs());//Cancel
                    //    break;
                    case Keys.Control | Keys.Enter:
                        AddNewItem(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        DeleteItem(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        Cancel(sender, new EventArgs());//Clear
                        break;
                    //case Keys.Control | Keys.Left:
                    //    BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                    //    break;
                    //case Keys.Control | Keys.Right:
                    //    BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                    //    break;
                    //case Keys.Control | Keys.Up:
                    //    BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                    //    break;
                    //case Keys.Control | Keys.Down:
                    //    BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                    case Keys.Control | Keys.P:
                       bnPrint_Click(sender, new EventArgs());//Cancel
                        break;
                    case Keys.Control | Keys.M:
                        Email(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
