﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Microsoft.VisualBasic;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,1 Mar 2011>
Description:	<Description,UserInformation Form>
================================================
*/

namespace MyBooksERP
{
    public partial class FrmConfigurationSetting : DevComponents.DotNetBar.Office2007Form
    {
        #region Variables Declaration

        private bool MblnChangeStatus; //Check state of the page
        //private bool MblnAddStatus; //Add/Update mode 
        //private bool MblnShowErrorMess;
        private bool MblnViewPermission = true;//To Set View Permission
        private bool MblnAddPermission = true;//To Set Add Permission
        private bool MblnUpdatePermission = true;//To Set Update Permission
        private bool MblnDeletePermission = true;//To Set Delete Permission
        private bool MblnAddUpdatePermission = true;//
        private bool MblnPrintEmailPermission = true;
        private int MintTimerInterval;
        private int MintCompanyId;
        private int MintUserId;
        
        
        //private int MintRecordCnt;
        //private int MintCurrentRecCnt;

        private string MstrMessageCommon;
        private string MstrMessageCaption;
        private string YesOrNo;//Grid validation

        private TextBox TxtB;

        private ArrayList MaMessageArr; //' Error Message display
        private ArrayList MaStatusMessageArr;
        private MessageBoxIcon MmessageIcon;

        ClsLogWriter MobjClsLogWriter;
        ClsNotification MobjClsNotification;
        clsBLLConfigurationSettings MobjclsBLLConfigurationSettings;
        clsBLLLogin MobjClsBLLLogin;
        #endregion //Variables Declaration

        #region Constructor

        public FrmConfigurationSetting(int intModuleID)
        {
            InitializeComponent();
            //MblnShowErrorMess = true;
            MstrMessageCommon = "";
            ModuleID = intModuleID;
            MintCompanyId = ClsCommonSettings.CompanyID; // current companyid
            MintUserId = ClsCommonSettings.UserID; // current userid
            MintTimerInterval = ClsCommonSettings.TimerInterval; // current companyid

            MstrMessageCaption = ClsCommonSettings.MessageCaption; //Message caption

            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            MobjclsBLLConfigurationSettings = new clsBLLConfigurationSettings();
            MobjClsBLLLogin = new clsBLLLogin();
        }

        public FrmConfigurationSetting()
        {
            InitializeComponent();
            //MblnShowErrorMess = true;
            MstrMessageCommon = "";

            MintCompanyId = ClsCommonSettings.CompanyID; // current companyid
            MintUserId = ClsCommonSettings.UserID; // current userid
            MintTimerInterval = ClsCommonSettings.TimerInterval; // current companyid

            MstrMessageCaption = ClsCommonSettings.MessageCaption; //Message caption

            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            MobjclsBLLConfigurationSettings = new clsBLLConfigurationSettings();
            MobjClsBLLLogin = new clsBLLLogin();
        }
        #endregion //Constructor

        public int ModuleID { get; set; }

        private void FrmConfigurationSetting_Load(object sender, EventArgs e)
        {
            MblnChangeStatus = false;
            SetPermissions();
            LoadMessage();
            ChangeStatus();
            AddNewItem();
            DisplayInfo();
            TmConfig.Enabled = true;

        }

        private void ChangeStatus()
        {
            MblnChangeStatus = true;
            BtnOk.Enabled = MblnUpdatePermission;
            BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
        }

        private void SetPermissions()
        {

            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.General, (Int32)eMenuID.ConfigurationSetting,
                 out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }

           
            BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            BtnOk.Enabled = MblnUpdatePermission;
            btnReset.Enabled = MblnUpdatePermission;
        }

        private bool AddNewItem()
        {
            TmConfig.Enabled = true;
            EnableDisableButtons(true);
            return true;
        }

        /// <summary>
        /// Enable/disable buttons according to the action
        /// </summary>
        private void EnableDisableButtons(bool bEnable)
        {
            if (bEnable)
            {
                BtnOk.Enabled = !bEnable;
                BindingNavigatorSaveItem.Enabled = !bEnable;
                MblnChangeStatus = !bEnable;
            }
            else
            {
                BindingNavigatorSaveItem.Enabled = bEnable;
                BtnOk.Enabled = bEnable;
                MblnChangeStatus = bEnable;
            }
        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessageArr = new ArrayList();
            MaMessageArr = MobjClsNotification.FillMessageArray((int)FormID.Configuration, ClsCommonSettings.ProductID);
            MaStatusMessageArr = MobjClsNotification.FillStatusMessageArray((int)FormID.Configuration, ClsCommonSettings.ProductID);
        }

        private bool DisplayInfo()
        {
            DgvConfigurationSetting.Rows.Clear();
            DataTable datTemp = MobjclsBLLConfigurationSettings.Display(ModuleID);
            
            if (ClsCommonSettings.UserID > 1)
                datTemp.DefaultView.RowFilter = "Predefined = 0";

            DataTable DtGrid = datTemp.DefaultView.ToTable();

            if (DtGrid.Rows.Count > 0)
            {
                for (int i = 0; i < DtGrid.Rows.Count; i++)
                {
                    DgvConfigurationSetting.RowCount = DgvConfigurationSetting.RowCount + 1;
                    DgvConfigurationSetting.Rows[i].Cells[0].Value = DtGrid.Rows[i]["ConfigurationID"];
                    DgvConfigurationSetting.Rows[i].Cells[1].Value = DtGrid.Rows[i]["ConfigurationItem"];
                    DgvConfigurationSetting.Rows[i].Cells[2].Value = DtGrid.Rows[i]["ConfigurationValue"];
                    DgvConfigurationSetting.Rows[i].Cells[3].Value = DtGrid.Rows[i]["Predefined"];
                    DgvConfigurationSetting.Rows[i].Cells[4].Value = DtGrid.Rows[i]["ValueLimt"];
                    DgvConfigurationSetting.Rows[i].Cells[5].Value = DtGrid.Rows[i]["DefaultValue"];
                    //DgvConfigurationSetting.Rows[i].Cells[6].Value = DtGrid.Rows[i]["ProductID"];
                }
            }
            return true;
        }



        private void btnReset_Click(object sender, EventArgs e)
        {
            if (update())
            {
                //MsMessageCommon = "Values Are Restored";
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MaMessageArr, 3001, out MmessageIcon);
                LblConfiguration.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            }
        }

        private bool update()//Setting Default Value
        {
            MstrMessageCommon = MobjClsNotification.GetErrorMessage(MaMessageArr, 3002, out MmessageIcon);
            if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                return false;

            DgvConfigurationSetting.Rows.Clear();
            DgvConfigurationSetting.Rows.Clear();
            DataTable DtGrid = MobjclsBLLConfigurationSettings.Display(ModuleID);

            if (DtGrid.Rows.Count > 0)
            {
                for (int i = 0; i < DtGrid.Rows.Count; i++)
                {
                    DgvConfigurationSetting.RowCount = DgvConfigurationSetting.RowCount + 1;
                    DgvConfigurationSetting.Rows[i].Cells[0].Value = DtGrid.Rows[i]["ConfigurationID"];
                    DgvConfigurationSetting.Rows[i].Cells[1].Value = DtGrid.Rows[i]["ConfigurationItem"];
                    DgvConfigurationSetting.Rows[i].Cells[2].Value = DtGrid.Rows[i]["DefaultValue"];
                    DgvConfigurationSetting.Rows[i].Cells[3].Value = DtGrid.Rows[i]["Predefined"];
                    DgvConfigurationSetting.Rows[i].Cells[4].Value = DtGrid.Rows[i]["ValueLimt"];
                    DgvConfigurationSetting.Rows[i].Cells[5].Value = DtGrid.Rows[i]["DefaultValue"];

                    MobjclsBLLConfigurationSettings.clsDTOConfigurationSettings.intConfigurationID = Convert.ToInt32(DgvConfigurationSetting.Rows[i].Cells[0].Value);
                    MobjclsBLLConfigurationSettings.clsDTOConfigurationSettings.strConfigurationValue = Convert.ToString(DgvConfigurationSetting.Rows[i].Cells[5].Value);
                    MobjclsBLLConfigurationSettings.update();//updating with Defaultvalues
                }
            }

            BtnOk.Enabled = false;
            return true;
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (ConfigValueValidation())
            {
                if (SaveConfigSettingInfo())
                {

                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    LblConfiguration.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmConfig.Enabled = true;
                    this.Close();
                }
            }
        }

        public bool SaveConfigSettingInfo()
        {
            MstrMessageCommon = MobjClsNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
            if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                return false;

            if (DgvConfigurationSetting.Rows.Count > 0)
            {
                for (int i = 0; i < DgvConfigurationSetting.Rows.Count; i++)
                {

                    MobjclsBLLConfigurationSettings.clsDTOConfigurationSettings.intConfigurationID = Convert.ToInt32(DgvConfigurationSetting.Rows[i].Cells[0].Value);
                    MobjclsBLLConfigurationSettings.clsDTOConfigurationSettings.strConfigurationValue = Convert.ToString(DgvConfigurationSetting.Rows[i].Cells[2].Value);
                    MobjclsBLLConfigurationSettings.update();//Updating grid Values 
                    SetCommonSettingsConfigurationSettingsInfo();
                        
                }
            }
            return true;
        }


        private string GetConfigurationValue(string strConfigurationItem, DataTable dtConfigurationSettings)
        {
            dtConfigurationSettings.DefaultView.RowFilter = "ConfigurationItem = '" + strConfigurationItem + "'";
            if (dtConfigurationSettings.DefaultView.ToTable().Rows.Count > 0)
                return dtConfigurationSettings.DefaultView.ToTable().Rows[0]["ConfigurationValue"].ToString();
            return "";
        }
        public void SetCommonSettingsConfigurationSettingsInfo()
        {
            DataTable dtConfigurationSettings = MobjClsBLLLogin.GetDefaultConfigurationSettings();
            string strValue = "";

            ClsCommonSettings.MessageCaption = GetConfigurationValue("MessageCaption", dtConfigurationSettings);
            ClsCommonSettings.ReportFooter = GetConfigurationValue("ReportFooter", dtConfigurationSettings);
            ClsCommonSettings.strProductSelectionURL = GetConfigurationValue("ProductSelectionURL", dtConfigurationSettings);
            ClsCommonSettings.ProductImageUrl = GetConfigurationValue("ProductImageUrl", dtConfigurationSettings);

            strValue = GetConfigurationValue("BackupInterval", dtConfigurationSettings);
            if (!string.IsNullOrEmpty(strValue))
                ClsCommonSettings.intBackupInterval = Convert.ToInt32(strValue);
            else
                ClsCommonSettings.intBackupInterval = 0;

            strValue = GetConfigurationValue("DefaultStatusBarMessageTime", dtConfigurationSettings);
            if (!string.IsNullOrEmpty(strValue))
                ClsCommonSettings.TimerInterval = Convert.ToInt32(strValue);
            else
                ClsCommonSettings.TimerInterval = 1000;



            if (Strings.UCase(GetConfigurationValue("SalaryDayIsEditable", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.GlbSalaryDayIsEditable = true;
            }
            else
            {
                ClsCommonSettings.GlbSalaryDayIsEditable = false;
            }


            if (Strings.UCase(GetConfigurationValue("SalarySlipIsEditable", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.GlSalarySlipIsEditable = true;
            }
            else
            {
                ClsCommonSettings.GlSalarySlipIsEditable = false;
            }
            if (Strings.UCase(GetConfigurationValue("24HourTimeFormat", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.Glb24HourFormat = true;
            }
            else
            {
                ClsCommonSettings.Glb24HourFormat = false;
            }


            if (Strings.UCase(GetConfigurationValue("DisplayLeaveSummaryInSalarySlip", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.GlDisplayLeaveSummaryInSalarySlip = true;
            }
            else
            {
                ClsCommonSettings.GlDisplayLeaveSummaryInSalarySlip = false;
            }


            strValue = GetConfigurationValue("LocationWise", dtConfigurationSettings);
            if (!string.IsNullOrEmpty(strValue) && strValue.ToUpper() == "YES")
                ClsCommonSettings.PblnIsLocationWise = true;
            else
                ClsCommonSettings.PblnIsLocationWise = false;

            strValue = GetConfigurationValue("CustomerLoginRequired", dtConfigurationSettings);
            if (!string.IsNullOrEmpty(strValue) && strValue.ToUpper() == "YES")
                ClsCommonSettings.PblnIsCustLoginRequired = true;
            else
                ClsCommonSettings.PblnIsCustLoginRequired = false;

        }


        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (ConfigValueValidation())
            {
                if (SaveConfigSettingInfo())
                {
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    LblConfiguration.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmConfig.Enabled = true;
                }
            }

        }


        private bool ConfigValueValidation()//grid validation
        {
            if (DgvConfigurationSetting.Rows.Count > 0)
            {
                int iValueLimit;
                int iCurrentValue;
                for (int i = 0; i < DgvConfigurationSetting.Rows.Count; i++)//correction
                {
                    iValueLimit = Convert.ToInt32(DgvConfigurationSetting.Rows[i].Cells[4].Value);
                    if (iValueLimit > 0)
                    {
                        iCurrentValue = Convert.ToInt32(DgvConfigurationSetting.Rows[i].Cells[2].Value);

                        if (iCurrentValue > iValueLimit)
                        {
                            MstrMessageCommon = "Configuration Value Exceeds Limit";
                            MstrMessageCommon = "Configuration Value Exceeds Limit";
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LblConfiguration.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            DgvConfigurationSetting.CurrentCell = DgvConfigurationSetting.Rows[i].Cells[2];
                            TmConfig.Enabled = true;
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private void DgvConfigurationSetting_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)//For UpperCase
        {
            if (DgvConfigurationSetting.CurrentCell.ColumnIndex == 2)
            {
                this.TxtB = (TextBox)e.Control;
                this.TxtB.KeyPress -= this.TextBox_KeyPress;
                this.TxtB.KeyPress += this.TextBox_KeyPress;
            }
        }

        private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((YesOrNo.ToUpper() == "YES") || (YesOrNo.ToUpper() == "NO"))
            {
                TxtB.CharacterCasing = CharacterCasing.Upper;
                TxtB.Text = TxtB.Text;
            }
            else
            {
                TxtB.CharacterCasing = CharacterCasing.Normal;
            }
        }

        private void DgvConfigurationSetting_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex > -1)
            {
                YesOrNo = Convert.ToString(DgvConfigurationSetting.CurrentCell.Value);
            }
        }

        private void DgvConfigurationSetting_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex > -1)
            {
                DgvConfigurationSetting.CurrentCell.Value = TxtB.Text;
                TxtB.CharacterCasing = CharacterCasing.Normal;
                YesOrNo = "";
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DgvConfigurationSetting_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            //    if (DgvConfigurationSetting.RowCount >= 1)
            //    {
            //        MintCurrentRecCnt = e.RowIndex + 1;
            //        BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
            //    }
        }

        private void DgvConfigurationSetting_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //ChangeStatus();
        }

        private void DgvConfigurationSetting_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DgvConfigurationSetting_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ChangeStatus();

        }

        private void FrmConfigurationSetting_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Alt | Keys.S:
                        BindingNavigatorSaveItem_Click(sender, new EventArgs());//save
                        break;
               }
            }
            catch (Exception)
            {
            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Configuration";
            objHelp.ShowDialog();
            objHelp = null;
        }
    }
}
