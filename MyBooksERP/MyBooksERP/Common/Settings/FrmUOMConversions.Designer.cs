﻿namespace MyBooksERP
{
    partial class FrmUOMConversions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUOMConversions));
            this.SStripUOM = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.UOMStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboUom = new System.Windows.Forms.ComboBox();
            this.cboConvFactor = new System.Windows.Forms.ComboBox();
            this.cboBaseUom = new System.Windows.Forms.ComboBox();
            this.cboBaseClass = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.DgvUOM = new DemoClsDataGridview.ClsDataGirdView();
            this.UOMConversionID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMBase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConversionFactor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConversionValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.BtnSave = new System.Windows.Forms.Button();
            this.UOMBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.UoMBindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.UoMBindingNavigatorSaveButton = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.CancelToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnUom = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.QuestionToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ErrorProUOM = new System.Windows.Forms.ErrorProvider(this.components);
            this.TmUOM = new System.Windows.Forms.Timer(this.components);
            this.lblConversionDisplay = new System.Windows.Forms.Label();
            this.txtConvValue = new System.Windows.Forms.TextBox();
            this.SStripUOM.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvUOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UOMBindingNavigator)).BeginInit();
            this.UOMBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProUOM)).BeginInit();
            this.SuspendLayout();
            // 
            // SStripUOM
            // 
            this.SStripUOM.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.UOMStatusLabel});
            this.SStripUOM.Location = new System.Drawing.Point(0, 452);
            this.SStripUOM.Name = "SStripUOM";
            this.SStripUOM.Size = new System.Drawing.Size(544, 22);
            this.SStripUOM.TabIndex = 4;
            this.SStripUOM.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel1.Text = "Status";
            // 
            // UOMStatusLabel
            // 
            this.UOMStatusLabel.Name = "UOMStatusLabel";
            this.UOMStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnCancel.Location = new System.Drawing.Point(474, 423);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(66, 23);
            this.BtnCancel.TabIndex = 3;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnOk.Location = new System.Drawing.Point(402, 423);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(66, 23);
            this.BtnOk.TabIndex = 2;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtConvValue);
            this.panel1.Controls.Add(this.cboUom);
            this.panel1.Controls.Add(this.cboConvFactor);
            this.panel1.Controls.Add(this.cboBaseUom);
            this.panel1.Controls.Add(this.cboBaseClass);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.DgvUOM);
            this.panel1.Controls.Add(this.shapeContainer1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(544, 372);
            this.panel1.TabIndex = 0;
            // 
            // cboUom
            // 
            this.cboUom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboUom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboUom.FormattingEnabled = true;
            this.cboUom.Location = new System.Drawing.Point(381, 133);
            this.cboUom.Name = "cboUom";
            this.cboUom.Size = new System.Drawing.Size(150, 21);
            this.cboUom.TabIndex = 5;
            this.cboUom.SelectedIndexChanged += new System.EventHandler(this.cboUom_SelectedIndexChanged);
            this.cboUom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // cboConvFactor
            // 
            this.cboConvFactor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboConvFactor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboConvFactor.FormattingEnabled = true;
            this.cboConvFactor.Location = new System.Drawing.Point(120, 133);
            this.cboConvFactor.Name = "cboConvFactor";
            this.cboConvFactor.Size = new System.Drawing.Size(150, 21);
            this.cboConvFactor.TabIndex = 4;
            this.cboConvFactor.SelectedIndexChanged += new System.EventHandler(this.cboConvFactor_SelectedIndexChanged);
            this.cboConvFactor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // cboBaseUom
            // 
            this.cboBaseUom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBaseUom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBaseUom.FormattingEnabled = true;
            this.cboBaseUom.Location = new System.Drawing.Point(120, 64);
            this.cboBaseUom.Name = "cboBaseUom";
            this.cboBaseUom.Size = new System.Drawing.Size(150, 21);
            this.cboBaseUom.TabIndex = 2;
            this.cboBaseUom.SelectedIndexChanged += new System.EventHandler(this.cboBaseUom_SelectedIndexChanged);
            this.cboBaseUom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // cboBaseClass
            // 
            this.cboBaseClass.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBaseClass.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBaseClass.FormattingEnabled = true;
            this.cboBaseClass.Location = new System.Drawing.Point(120, 29);
            this.cboBaseClass.Name = "cboBaseClass";
            this.cboBaseClass.Size = new System.Drawing.Size(150, 21);
            this.cboBaseClass.TabIndex = 1;
            this.cboBaseClass.SelectedIndexChanged += new System.EventHandler(this.cboBaseClass_SelectedIndexChanged);
            this.cboBaseClass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Conversion Value";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Conversion Factor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(295, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Alternate Uom";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Base Uom";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Uom Class";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "UOM Conversion Details";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(4, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "UOM Conversions";
            // 
            // DgvUOM
            // 
            this.DgvUOM.AddNewRow = false;
            this.DgvUOM.AllowUserToAddRows = false;
            this.DgvUOM.AllowUserToDeleteRows = false;
            this.DgvUOM.AllowUserToResizeColumns = false;
            this.DgvUOM.AllowUserToResizeRows = false;
            this.DgvUOM.AlphaNumericCols = new int[0];
            this.DgvUOM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.DgvUOM.BackgroundColor = System.Drawing.Color.White;
            this.DgvUOM.CapsLockCols = new int[0];
            this.DgvUOM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvUOM.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UOMConversionID,
            this.UOMClass,
            this.UOMBase,
            this.UOM,
            this.ConversionFactor,
            this.ConversionValue});
            this.DgvUOM.DecimalCols = new int[0];
            this.DgvUOM.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DgvUOM.HasSlNo = false;
            this.DgvUOM.LastRowIndex = 0;
            this.DgvUOM.Location = new System.Drawing.Point(4, 185);
            this.DgvUOM.MultiSelect = false;
            this.DgvUOM.Name = "DgvUOM";
            this.DgvUOM.NegativeValueCols = new int[0];
            this.DgvUOM.NumericCols = new int[0];
            this.DgvUOM.ReadOnly = true;
            this.DgvUOM.RowHeadersWidth = 50;
            this.DgvUOM.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DgvUOM.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvUOM.Size = new System.Drawing.Size(535, 182);
            this.DgvUOM.TabIndex = 6;
            this.DgvUOM.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DgvUOM_RowsAdded);
            this.DgvUOM.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvUOM_CellClick);
            this.DgvUOM.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.DgvUOM_RowsRemoved);
            this.DgvUOM.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvUOM_CellContentClick);
            // 
            // UOMConversionID
            // 
            this.UOMConversionID.DataPropertyName = "UOMConversionID";
            this.UOMConversionID.HeaderText = "UOMConversionID";
            this.UOMConversionID.Name = "UOMConversionID";
            this.UOMConversionID.ReadOnly = true;
            this.UOMConversionID.Visible = false;
            // 
            // UOMClass
            // 
            this.UOMClass.DataPropertyName = "UOMClass";
            this.UOMClass.HeaderText = "Base Class";
            this.UOMClass.Name = "UOMClass";
            this.UOMClass.ReadOnly = true;
            this.UOMClass.Visible = false;
            // 
            // UOMBase
            // 
            this.UOMBase.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.UOMBase.DataPropertyName = "UOMBase";
            this.UOMBase.HeaderText = "Base Uom";
            this.UOMBase.Name = "UOMBase";
            this.UOMBase.ReadOnly = true;
            this.UOMBase.Visible = false;
            // 
            // UOM
            // 
            this.UOM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.UOM.DataPropertyName = "UOM";
            this.UOM.HeaderText = "Alternate Uom";
            this.UOM.Name = "UOM";
            this.UOM.ReadOnly = true;
            // 
            // ConversionFactor
            // 
            this.ConversionFactor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ConversionFactor.DataPropertyName = "ConversionFactor";
            this.ConversionFactor.HeaderText = "Conversion Factor";
            this.ConversionFactor.Name = "ConversionFactor";
            this.ConversionFactor.ReadOnly = true;
            // 
            // ConversionValue
            // 
            this.ConversionValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ConversionValue.DataPropertyName = "ConversionValue";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ConversionValue.DefaultCellStyle = dataGridViewCellStyle4;
            this.ConversionValue.HeaderText = "Conversion Value";
            this.ConversionValue.Name = "ConversionValue";
            this.ConversionValue.ReadOnly = true;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(542, 370);
            this.shapeContainer1.TabIndex = 9;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 147;
            this.lineShape2.X2 = 535;
            this.lineShape2.Y1 = 13;
            this.lineShape2.Y2 = 13;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 105;
            this.lineShape1.X2 = 535;
            this.lineShape1.Y1 = 178;
            this.lineShape1.Y2 = 178;
            // 
            // BtnSave
            // 
            this.BtnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnSave.Location = new System.Drawing.Point(5, 423);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 1;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // UOMBindingNavigator
            // 
            this.UOMBindingNavigator.AddNewItem = null;
            this.UOMBindingNavigator.CountItem = null;
            this.UOMBindingNavigator.DeleteItem = null;
            this.UOMBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.UoMBindingNavigatorAddNewItem,
            this.UoMBindingNavigatorSaveButton,
            this.bindingNavigatorDeleteItem,
            this.CancelToolStripButton,
            this.toolStripSeparator1,
            this.btnUom,
            this.toolStripSeparator3,
            this.BtnPrint,
            this.BtnEmail,
            this.toolStripSeparator2,
            this.QuestionToolStripButton});
            this.UOMBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.UOMBindingNavigator.MoveFirstItem = null;
            this.UOMBindingNavigator.MoveLastItem = null;
            this.UOMBindingNavigator.MoveNextItem = null;
            this.UOMBindingNavigator.MovePreviousItem = null;
            this.UOMBindingNavigator.Name = "UOMBindingNavigator";
            this.UOMBindingNavigator.PositionItem = null;
            this.UOMBindingNavigator.Size = new System.Drawing.Size(544, 25);
            this.UOMBindingNavigator.TabIndex = 5;
            this.UOMBindingNavigator.Text = "bindingNavigator1";
            // 
            // UoMBindingNavigatorAddNewItem
            // 
            this.UoMBindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.UoMBindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("UoMBindingNavigatorAddNewItem.Image")));
            this.UoMBindingNavigatorAddNewItem.Name = "UoMBindingNavigatorAddNewItem";
            this.UoMBindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.UoMBindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.UoMBindingNavigatorAddNewItem.Text = "Add new";
            this.UoMBindingNavigatorAddNewItem.Click += new System.EventHandler(this.UoMBindingNavigatorAddNewItem_Click);
            // 
            // UoMBindingNavigatorSaveButton
            // 
            this.UoMBindingNavigatorSaveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.UoMBindingNavigatorSaveButton.Image = ((System.Drawing.Image)(resources.GetObject("UoMBindingNavigatorSaveButton.Image")));
            this.UoMBindingNavigatorSaveButton.Name = "UoMBindingNavigatorSaveButton";
            this.UoMBindingNavigatorSaveButton.Size = new System.Drawing.Size(23, 22);
            this.UoMBindingNavigatorSaveButton.Text = "Save";
            this.UoMBindingNavigatorSaveButton.Click += new System.EventHandler(this.UoMBindingNavigatorSaveButton_Click);
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            this.bindingNavigatorDeleteItem.Click += new System.EventHandler(this.bindingNavigatorDeleteItem_Click);
            // 
            // CancelToolStripButton
            // 
            this.CancelToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CancelToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelToolStripButton.Image")));
            this.CancelToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CancelToolStripButton.Name = "CancelToolStripButton";
            this.CancelToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.CancelToolStripButton.Text = "toolStripButton1";
            this.CancelToolStripButton.ToolTipText = "Cancel";
            this.CancelToolStripButton.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnUom
            // 
            this.btnUom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUom.Image = ((System.Drawing.Image)(resources.GetObject("btnUom.Image")));
            this.btnUom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUom.Name = "btnUom";
            this.btnUom.Size = new System.Drawing.Size(23, 22);
            this.btnUom.Text = "Uom";
            this.btnUom.Click += new System.EventHandler(this.btnUom_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.ToolTipText = "Print\r\n";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // QuestionToolStripButton
            // 
            this.QuestionToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.QuestionToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("QuestionToolStripButton.Image")));
            this.QuestionToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.QuestionToolStripButton.Name = "QuestionToolStripButton";
            this.QuestionToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.QuestionToolStripButton.Text = "Help";
            this.QuestionToolStripButton.Click += new System.EventHandler(this.QuestionToolStripButton_Click);
            // 
            // ErrorProUOM
            // 
            this.ErrorProUOM.ContainerControl = this;
            this.ErrorProUOM.RightToLeft = true;
            // 
            // lblConversionDisplay
            // 
            this.lblConversionDisplay.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblConversionDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConversionDisplay.Location = new System.Drawing.Point(0, 397);
            this.lblConversionDisplay.Name = "lblConversionDisplay";
            this.lblConversionDisplay.Size = new System.Drawing.Size(544, 17);
            this.lblConversionDisplay.TabIndex = 26;
            this.lblConversionDisplay.Text = "1 Base UOM = N * Alternate UOM";
            this.lblConversionDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtConvValue
            // 
            this.txtConvValue.Location = new System.Drawing.Point(120, 100);
            this.txtConvValue.MaxLength = 12;
            this.txtConvValue.Name = "txtConvValue";
            this.txtConvValue.Size = new System.Drawing.Size(150, 20);
            this.txtConvValue.TabIndex = 27;
            this.txtConvValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtConvValue.TextChanged += new System.EventHandler(this.txtConvValue_TextChanged);
            this.txtConvValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtConvValue_KeyPress);
            // 
            // FrmUOMConversions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(544, 474);
            this.Controls.Add(this.lblConversionDisplay);
            this.Controls.Add(this.SStripUOM);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.UOMBindingNavigator);
            this.Controls.Add(this.BtnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmUOMConversions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "UOM Conversion";
            this.Load += new System.EventHandler(this.FrmUOMConversions_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmUOMConversions_KeyDown);
            this.SStripUOM.ResumeLayout(false);
            this.SStripUOM.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvUOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UOMBindingNavigator)).EndInit();
            this.UOMBindingNavigator.ResumeLayout(false);
            this.UOMBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProUOM)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip SStripUOM;
        private System.Windows.Forms.ToolStripStatusLabel UOMStatusLabel;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private DemoClsDataGridview.ClsDataGirdView DgvUOM;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.BindingNavigator UOMBindingNavigator;
        private System.Windows.Forms.ToolStripButton UoMBindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton UoMBindingNavigatorSaveButton;
        private System.Windows.Forms.ToolStripButton CancelToolStripButton;
        private System.Windows.Forms.ToolStripButton QuestionToolStripButton;
        private System.Windows.Forms.ErrorProvider ErrorProUOM;
        private System.Windows.Forms.Timer TmUOM;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripButton BtnPrint;
        private System.Windows.Forms.ToolStripButton BtnEmail;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnUom;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Label label1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMConversionID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMBase;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConversionFactor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConversionValue;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboUom;
        private System.Windows.Forms.ComboBox cboConvFactor;
        private System.Windows.Forms.ComboBox cboBaseUom;
        private System.Windows.Forms.ComboBox cboBaseClass;
        private System.Windows.Forms.Label lblConversionDisplay;
        private System.Windows.Forms.TextBox txtConvValue;
    }
}