﻿namespace MyBooksERP//.Settings.Forms
{
    partial class FrmMandatoryDocumentTypes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMandatoryDocumentTypes));
            this.MandatoryDocumentTypesBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnClear = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bnDocumentType = new System.Windows.Forms.ToolStripButton();
            this.pnlConfigSetting = new System.Windows.Forms.Panel();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.lblOperationType = new DevComponents.DotNetBar.LabelX();
            this.cboOperationType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.dgvMandatoryDocuments = new System.Windows.Forms.DataGridView();
            this.dgvColDocumentTypes = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.BtnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.TmMandatoryDocumentTypes = new System.Windows.Forms.Timer(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.LblConfiguration = new DevComponents.DotNetBar.LabelItem();
            this.toolStripStatusLabel11 = new DevComponents.DotNetBar.LabelItem();
            this.lblStatus = new DevComponents.DotNetBar.LabelItem();
            ((System.ComponentModel.ISupportInitialize)(this.MandatoryDocumentTypesBindingNavigator)).BeginInit();
            this.MandatoryDocumentTypesBindingNavigator.SuspendLayout();
            this.pnlConfigSetting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMandatoryDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // MandatoryDocumentTypesBindingNavigator
            // 
            this.MandatoryDocumentTypesBindingNavigator.AddNewItem = null;
            this.MandatoryDocumentTypesBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.MandatoryDocumentTypesBindingNavigator.CountItem = null;
            this.MandatoryDocumentTypesBindingNavigator.DeleteItem = null;
            this.MandatoryDocumentTypesBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnClear,
            this.BindingNavigatorSaveItem,
            this.bnDeleteItem,
            this.bnDocumentType});
            this.MandatoryDocumentTypesBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.MandatoryDocumentTypesBindingNavigator.MoveFirstItem = null;
            this.MandatoryDocumentTypesBindingNavigator.MoveLastItem = null;
            this.MandatoryDocumentTypesBindingNavigator.MoveNextItem = null;
            this.MandatoryDocumentTypesBindingNavigator.MovePreviousItem = null;
            this.MandatoryDocumentTypesBindingNavigator.Name = "MandatoryDocumentTypesBindingNavigator";
            this.MandatoryDocumentTypesBindingNavigator.PositionItem = null;
            this.MandatoryDocumentTypesBindingNavigator.Size = new System.Drawing.Size(362, 25);
            this.MandatoryDocumentTypesBindingNavigator.TabIndex = 0;
            this.MandatoryDocumentTypesBindingNavigator.Text = "BindingNavigator1";
            // 
            // bnClear
            // 
            this.bnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.bnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnClear.Name = "bnClear";
            this.bnClear.Size = new System.Drawing.Size(54, 22);
            this.bnClear.Text = "Clear";
            this.bnClear.Click += new System.EventHandler(this.bnClear_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(51, 22);
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.bnDeleteItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.Size = new System.Drawing.Size(60, 22);
            this.bnDeleteItem.Text = "Delete";
            this.bnDeleteItem.Click += new System.EventHandler(this.bnDeleteItem_Click);
            // 
            // bnDocumentType
            // 
            this.bnDocumentType.Image = global::MyBooksERP.Properties.Resources.DocumentType;
            this.bnDocumentType.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnDocumentType.Name = "bnDocumentType";
            this.bnDocumentType.Size = new System.Drawing.Size(112, 22);
            this.bnDocumentType.Text = "Document Type";
            this.bnDocumentType.Click += new System.EventHandler(this.bnDocumentType_Click);
            // 
            // pnlConfigSetting
            // 
            this.pnlConfigSetting.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlConfigSetting.Controls.Add(this.cboCompany);
            this.pnlConfigSetting.Controls.Add(this.lblCompany);
            this.pnlConfigSetting.Controls.Add(this.lblOperationType);
            this.pnlConfigSetting.Controls.Add(this.cboOperationType);
            this.pnlConfigSetting.Controls.Add(this.dgvMandatoryDocuments);
            this.pnlConfigSetting.Location = new System.Drawing.Point(4, 28);
            this.pnlConfigSetting.Name = "pnlConfigSetting";
            this.pnlConfigSetting.Size = new System.Drawing.Size(354, 229);
            this.pnlConfigSetting.TabIndex = 1;
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 80;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(95, 5);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(251, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 0;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cbo_KeyDown);
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(6, 2);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(83, 23);
            this.lblCompany.TabIndex = 15;
            this.lblCompany.Text = "Company";
            // 
            // lblOperationType
            // 
            // 
            // 
            // 
            this.lblOperationType.BackgroundStyle.Class = "";
            this.lblOperationType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblOperationType.Location = new System.Drawing.Point(6, 28);
            this.lblOperationType.Name = "lblOperationType";
            this.lblOperationType.Size = new System.Drawing.Size(83, 23);
            this.lblOperationType.TabIndex = 14;
            this.lblOperationType.Text = "Operation Type";
            // 
            // cboOperationType
            // 
            this.cboOperationType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOperationType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOperationType.DisplayMember = "Text";
            this.cboOperationType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboOperationType.DropDownHeight = 80;
            this.cboOperationType.FormattingEnabled = true;
            this.cboOperationType.IntegralHeight = false;
            this.cboOperationType.ItemHeight = 14;
            this.cboOperationType.Location = new System.Drawing.Point(95, 31);
            this.cboOperationType.Name = "cboOperationType";
            this.cboOperationType.Size = new System.Drawing.Size(251, 20);
            this.cboOperationType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboOperationType.TabIndex = 1;
            this.cboOperationType.SelectedIndexChanged += new System.EventHandler(this.cboOperationType_SelectedIndexChanged);
            this.cboOperationType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cbo_KeyDown);
            // 
            // dgvMandatoryDocuments
            // 
            this.dgvMandatoryDocuments.BackgroundColor = System.Drawing.Color.White;
            this.dgvMandatoryDocuments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMandatoryDocuments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColDocumentTypes});
            this.dgvMandatoryDocuments.Location = new System.Drawing.Point(6, 57);
            this.dgvMandatoryDocuments.MultiSelect = false;
            this.dgvMandatoryDocuments.Name = "dgvMandatoryDocuments";
            this.dgvMandatoryDocuments.RowHeadersWidth = 50;
            this.dgvMandatoryDocuments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvMandatoryDocuments.Size = new System.Drawing.Size(341, 190);
            this.dgvMandatoryDocuments.TabIndex = 12;
            this.dgvMandatoryDocuments.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvMandatoryDocuments_RowsAdded);
            this.dgvMandatoryDocuments.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvMandatoryDocuments_DataError);
            this.dgvMandatoryDocuments.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvMandatoryDocuments_RowsRemoved);
            // 
            // dgvColDocumentTypes
            // 
            this.dgvColDocumentTypes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvColDocumentTypes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColDocumentTypes.HeaderText = "Document Types";
            this.dgvColDocumentTypes.Name = "dgvColDocumentTypes";
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(202, 263);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 2;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(283, 263);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // TmMandatoryDocumentTypes
            // 
            this.TmMandatoryDocumentTypes.Tick += new System.EventHandler(this.TmMandatoryDocumentTypes_Tick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ConfigurationID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Configuration Item";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 50;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 300;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Configuration Value";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 300;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "PreDefined";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "ValueLimit";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "DefaultValue";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.LblConfiguration,
            this.toolStripStatusLabel11,
            this.lblStatus});
            this.bar1.Location = new System.Drawing.Point(0, 292);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(362, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 81;
            this.bar1.TabStop = false;
            // 
            // LblConfiguration
            // 
            this.LblConfiguration.Name = "LblConfiguration";
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Text = "Status:";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            // 
            // FrmMandatoryDocumentTypes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 311);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.pnlConfigSetting);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.MandatoryDocumentTypesBindingNavigator);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMandatoryDocumentTypes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mandatory Document Types";
            this.Load += new System.EventHandler(this.FrmMandatoryDocumentTypes_Load);
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.FrmMandatoryDocumentTypes_HelpButtonClicked);
            ((System.ComponentModel.ISupportInitialize)(this.MandatoryDocumentTypesBindingNavigator)).EndInit();
            this.MandatoryDocumentTypesBindingNavigator.ResumeLayout(false);
            this.MandatoryDocumentTypesBindingNavigator.PerformLayout();
            this.pnlConfigSetting.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMandatoryDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator MandatoryDocumentTypesBindingNavigator;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        private System.Windows.Forms.Panel pnlConfigSetting;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridView dgvMandatoryDocuments;
        private System.Windows.Forms.Timer TmMandatoryDocumentTypes;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem LblConfiguration;
        private System.Windows.Forms.ToolStripButton bnDeleteItem;
        private DevComponents.DotNetBar.LabelX lblOperationType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboOperationType;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColDocumentTypes;
        private System.Windows.Forms.ToolStripButton bnClear;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel11;
        private DevComponents.DotNetBar.LabelItem lblStatus;
        private System.Windows.Forms.ToolStripButton bnDocumentType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.LabelX lblCompany;
    }
}