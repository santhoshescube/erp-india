﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Windows.Forms;



namespace MyBooksERP
{
    public partial class FrmOrganizationHierarchySetting : DevComponents.DotNetBar.Office2007Form
    {
        #region Private variables

        bool MblnShowErrorMess, MblnChangeStatus;
        TreeNode _prevHierarchyNode;
        TreeNode _prevUserNode;
        private int MiTimerInterval;

        clsBLLWorkFlowSettingsSetting MobjBLLWorkFlowSettingsSetting;
        ClsLogWriter MobjClsLogWriter;      // Object of the LogWriter class
        
        private string MstrMessageCaption = "";
        private string MstrMessageCommon;
        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;
        private MessageBoxIcon MmessageIcon;
        ClsNotification mObjNotification;

        #endregion // Private variables

        public FrmOrganizationHierarchySetting()
        {
            InitializeComponent();
            this.MblnShowErrorMess = true;
            this.MblnChangeStatus = false;
            this.MstrMessageCommon = "";
            this._prevHierarchyNode = null;
            this._prevUserNode = null;
            MiTimerInterval = ClsCommonSettings.TimerInterval;     // current companyid

            this.MobjBLLWorkFlowSettingsSetting = new clsBLLWorkFlowSettingsSetting();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);   // Application startup path

            MmessageIcon = MessageBoxIcon.Information;
            this.mObjNotification = new ClsNotification();
        }

        ~FrmOrganizationHierarchySetting()
        {
            this.MblnShowErrorMess = false;
            this.MblnChangeStatus = false;
            this.MstrMessageCommon = null;
            this._prevHierarchyNode = null;
            this._prevUserNode = null;

            this.MobjClsLogWriter = null;
            this.mObjNotification = null;
            this.MobjBLLWorkFlowSettingsSetting = null;
        }

        #region Events

        private void FrmOrganizationHierarchySetting_Load(object sender, EventArgs e)
        {
            this.LoadMessage();
            this.LoadRoles();
            this.LoadUsers();
            //this.LoadHierarchySetting();
            this.Changestatus(false);
            tvwHierarchy.ExpandAll();
            tvwUsers.ExpandAll();
        }

        private void tvwRole_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void tvwHierarchy_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                object data = e.Data.GetData(typeof(TreeNode));

                if (data == null) return;

                TreeNode node = this.tvwHierarchy.GetNodeAt(
                            this.tvwHierarchy.PointToClient(new System.Drawing.Point(e.X, e.Y)));

                TreeNode tndNew = (TreeNode)(data as TreeNode).Clone(); ;

                if (data != node && node.Name.Substring(0, 3) != "W_U")
                {
                    if (tndNew.Name.Substring(0, 2) != "U_")
                    {
                        this.tvwHierarchy.Nodes.Remove(data as TreeNode);
                    }
                    else
                    {
                        tndNew.Name = tndNew.Name.Replace("U_", "");
                    }

                    if (node == null)
                        this.tvwHierarchy.Nodes.Add(tndNew);
                    else
                    {
                        if (!node.Nodes.ContainsKey(tndNew.Name) && node.Name.Substring(0, 2) == "W_")
                        {
                            node.Nodes.Add(tndNew);
                            node.Expand();
                        }
                    }
                    tndNew = null;

                    this.Changestatus(true);
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling roles:Role drag drop " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show(Ex.Message.ToString());
            }
        }

        private void tvwHierarchy_DragEnter(object sender, DragEventArgs e)
        {
            object data = e.Data.GetData(typeof(TreeNode));

            TreeNode tndNew = (TreeNode)(data as TreeNode).Clone(); ;

            if (tndNew.Name.Substring(0, 3) != "W_W")
            {
                e.Effect = DragDropEffects.Move;
            }
        }

        private void tvwHierarchy_DragOver(object sender, DragEventArgs e)
        {
            try
            {
                this.tvwHierarchy.SelectedNode = null;
                if (this._prevHierarchyNode != null)
                {
                    this._prevHierarchyNode.BackColor = System.Drawing.SystemColors.HighlightText;
                    this._prevHierarchyNode.ForeColor = System.Drawing.SystemColors.ControlText;
                }
                TreeNode node = this.tvwHierarchy.GetNodeAt(
                            this.tvwHierarchy.PointToClient(new System.Drawing.Point(e.X, e.Y)));
                if (node == null) return;
                this.tvwHierarchy.SelectedNode = node;
                node.BackColor = System.Drawing.SystemColors.Highlight;
                node.ForeColor = System.Drawing.SystemColors.HighlightText;
                this._prevHierarchyNode = node;
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling roles:Role drag drop " + this.Name + " " + ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show(ex.Message.ToString());
            }
        }

        private void tvwRole_ItemDrag(object sender, ItemDragEventArgs e)
        {
            TreeNode tndNew = (TreeNode)(e.Item); ;

            if (tndNew.Name.Substring(0, 3) != "W_W")
            {
                (sender as TreeView).DoDragDrop(e.Item, DragDropEffects.Move);
            }
        }

        private void tvwHierarchy_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (this._prevHierarchyNode != null)
            {
                this._prevHierarchyNode.BackColor = System.Drawing.SystemColors.HighlightText;
                this._prevHierarchyNode.ForeColor = System.Drawing.SystemColors.ControlText;
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;

                Save();

                this.MobjBLLWorkFlowSettingsSetting.DTOWorkFlowSettingsSetting.Clear();
                this.MobjClsLogWriter.WriteLog("Saved successfully:WorkFlowSettingsSettings()", 0);

                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                this.lblHierarchySettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmHierarchySettings.Enabled = true;
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling Organization Hierarchy Settings" + " " + ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show(ex.Message.ToString());
            }
        }

        private void Save()
        {
            try
            {
                FillParameters();

                if (!this.MobjBLLWorkFlowSettingsSetting.SaveHierarchySetting())
                    return;

                this.Changestatus(false);
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling Organization Hierarchy Settings" + " " + ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show(ex.Message.ToString());
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.tvwHierarchy.SelectedNode == null)
                    return;
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3956, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;

                this.Changestatus(true);
                TreeNode node = this.tvwHierarchy.SelectedNode;
                this.tvwHierarchy.Nodes.Remove(node);
                this.tvwHierarchy.Nodes.Add(node);
                node = null;

                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                this.lblHierarchySettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling delete Organization Hierarchy Settings:Delete Organization Hierarchy Settings " + this.Name + " " + ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show(ex.Message.ToString());
            }
        }

        private void btnRemoveAll_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.tvwHierarchy.Nodes.Count == 0)
                    return;

                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return; ;

                this.tvwHierarchy.Nodes[0].Nodes.Clear();
                this.tvwUsers.Nodes[0].Nodes.Clear();

                this.Changestatus(true);
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                this.lblHierarchySettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);

                Save();

                this.LoadRoles();
                this.LoadUsers();
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling delete Organization Hierarchy Settings:Delete Organization Hierarchy Settings " + this.Name + " " + ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show(ex.Message.ToString());
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmOrganizationHierarchySetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (MblnChangeStatus)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        this.mObjNotification = null;
                        this.MobjBLLWorkFlowSettingsSetting = null;
                        e.Cancel = false;
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling from closing:Organization Hierarchy Settings " + this.Name + " " + ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show(ex.Message.ToString());
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Loading the variables for messages
        /// </summary>
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = mObjNotification.FillMessageArray((int)FormID.AccountSettings, 4);
            MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.AccountSettings, 4);
        }

        /// <summary>
        /// Loading user roles.
        /// </summary>
        /// <returns></returns>
        private bool LoadRoles()
        {
            DataTable datRoles = null;

            int intWorkFlowTypeID = 0;
            int intPrevWorkFlowTypeID = 0;
            int intICounter = -1;

            int intPrevRoleID = 0;
            int intRoleID = 0;
            int intJCounter = -1;

            TreeNode nodeWorkFlowType = null;
            TreeNode nodeUser = null;
            TreeNode nodeRole = null;

            try
            {
                datRoles = this.MobjBLLWorkFlowSettingsSetting.LoadRoles();

                if (datRoles != null && datRoles.Rows.Count > 0)
                {
                    foreach (DataRow dtRow in datRoles.Rows)
                    {
                        intWorkFlowTypeID = dtRow["WorkFlowTypeID"].ToInt32();

                        if (intPrevWorkFlowTypeID != intWorkFlowTypeID)
                        {
                            nodeWorkFlowType = new TreeNode
                            {
                                Name = "W_W_" + dtRow["WorkFlowType"].ToString(),
                                Text = dtRow["WorkFlowType"].ToString(),
                                Tag = dtRow["WorkFlowTypeID"].ToString() + "_" + dtRow["UserID"].ToString()
                            };

                            this.tvwHierarchy.Nodes[0].Nodes.Add(nodeWorkFlowType);
                            intICounter += 1;
                            intJCounter = -1;
                            intPrevRoleID = 0;
                        }

                        intRoleID = dtRow["RoleID"].ToInt32();

                        if (intRoleID != 0)
                        {
                            if (intPrevRoleID != intRoleID)
                            {
                                nodeRole = new TreeNode
                                {
                                    Name = "W_R_" + dtRow["RoleName"].ToString(),
                                    Text = dtRow["RoleName"].ToString(),
                                    Tag = dtRow["RoleID"].ToString()
                                };

                                this.tvwHierarchy.Nodes[0].Nodes[intICounter].Nodes.Add(nodeRole);
                                intJCounter += 1;
                            }

                            nodeUser = new TreeNode
                            {
                                Name = "W_U_" + dtRow["UserName"].ToString(),
                                Text = dtRow["UserName"].ToString(),
                                Tag = dtRow["UserID"].ToString()
                            };

                            this.tvwHierarchy.Nodes[0].Nodes[intICounter].Nodes[intJCounter].Nodes.Add(nodeUser);
                        }
                        intPrevWorkFlowTypeID = dtRow["WorkFlowTypeID"].ToInt32();
                        intPrevRoleID = dtRow["RoleID"].ToInt32();
                    }
                }
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling roles:LoadRoles " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadRoles() " + Ex.Message.ToString());

                return false;
            }
            finally
            {
                if (datRoles != null)
                {
                    datRoles.Clear();
                    datRoles.Dispose();
                    datRoles = null;
                }
            }
        }

        /// <summary>
        /// Loading user Users.
        /// </summary>
        /// <returns></returns>
        private bool LoadUsers()
        {
            DataTable datUsers = null;
            int intPrevRoleID = 0;
            int intRoleID = 0;
            int intICounter = -1;
            TreeNode nodeUser = null;
            TreeNode nodeRole = null;

            try
            {
                datUsers = this.MobjBLLWorkFlowSettingsSetting.LoadUsers();

                if (datUsers != null && datUsers.Rows.Count > 0)
                {
                    foreach (DataRow dtRow in datUsers.Rows)
                    {
                        intRoleID = dtRow["RoleID"].ToInt32();

                        if (intPrevRoleID != intRoleID)
                        {
                            nodeRole = new TreeNode
                            {
                                Name = "U_R_" + dtRow["RoleName"].ToString(),
                                Text = dtRow["RoleName"].ToString(),
                                Tag = dtRow["RoleID"].ToString()
                            };

                            this.tvwUsers.Nodes[0].Nodes.Add(nodeRole);
                            intICounter += 1;
                        }

                        nodeUser = new TreeNode
                        {
                            Name = "U_U_" + dtRow["UserName"].ToString(),
                            Text = dtRow["UserName"].ToString(),
                            Tag = dtRow["UserID"].ToString()
                        };

                        this.tvwUsers.Nodes[0].Nodes[intICounter].Nodes.Add(nodeUser);

                        intPrevRoleID = dtRow["RoleID"].ToInt32();
                    }
                }
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling roles:LoadUsers " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadUsers() " + Ex.Message.ToString());

                return false;
            }
            finally
            {
                if (datUsers != null)
                {
                    datUsers.Clear();
                    datUsers.Dispose();
                    datUsers = null;
                }
            }
        }

        private bool LoadHierarchySetting()
        {
            DataTable dtHierarchySetting = null;
            TreeNode tndNode = null;

            try
            {
                this.tvwHierarchy.Nodes[0].Nodes.Clear();

                dtHierarchySetting = this.MobjBLLWorkFlowSettingsSetting.GetSavedHierarchySetting();
                if (dtHierarchySetting != null && dtHierarchySetting.Rows.Count > 0)
                {
                    DataRow[] dtRows = dtHierarchySetting.Select("UserID=0");
                    foreach (DataRow dtRow in dtRows)
                    {
                        tndNode = new TreeNode
                          {
                              Name = "W" + dtRow["WorkFlowType"].ToString(),
                              Text = dtRow["WorkFlowType"].ToString(),
                              Tag = dtRow["WorkFlowTypeID"].ToString()
                          };
                        this.tvwHierarchy.Nodes[0].Nodes.Add(tndNode);
                        this.FillHierarchySetting(tndNode, Convert.ToInt32(dtRow["WorkFlowTypeID"]), dtHierarchySetting);

                        tndNode = null;
                    }
                }
                else
                {
                    this.LoadRoles();
                    this.LoadUsers();
                }
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling roles:LoadRoles " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadRoles() " + Ex.Message.ToString());

                return false;
            }
            finally
            {
                if (dtHierarchySetting != null)
                {
                    dtHierarchySetting.Clear();
                    dtHierarchySetting.Dispose();
                    dtHierarchySetting = null;
                }
                tndNode = null;
            }
        }

        private void FillHierarchySetting(TreeNode tndNode, int iParentID, DataTable datHierarchySetting)
        {
            TreeNode tndNewNode;

            try
            {
                DataRow[] dtRows = datHierarchySetting.Select("ParentID=" + iParentID.ToString());
                foreach (DataRow dtRow in dtRows)
                {
                    tndNewNode = new TreeNode
                    {
                        Name = "W" + dtRow["WorkFlowType"].ToString(),
                        Text = dtRow["WorkFlowType"].ToString(),
                        Tag = dtRow["WorkFlowTypeID"].ToString()
                    };
                    tndNode.Nodes.Add(tndNewNode);
                    this.FillHierarchySetting(tndNewNode, Convert.ToInt32(dtRow["WorkFlowTypeID"]), datHierarchySetting);

                    tndNewNode = null;
                }
            }
            catch (Exception ex) { throw ex; }
            finally { tndNewNode = null; }
        }

        private void Changestatus(bool blnStatus)
        {
            MblnChangeStatus = blnStatus;
            this.BindingNavigatorSaveItem.Enabled = this.MblnChangeStatus;
        }

        //// Validation For Saving
        private bool HierarchySettingValidation()
        {
            return true;
        }

        private void FillParameters()
        {
            try
            {
                string strNodeTag = "";
                int intICounter = 0;
                int intJCounter = 0;

                foreach (TreeNode nodeWorkType in this.tvwHierarchy.Nodes[0].Nodes)
                {
                    intJCounter = 0;

                    foreach (TreeNode nodeRoles in this.tvwHierarchy.Nodes[0].Nodes[intICounter].Nodes)
                    {
                        foreach (TreeNode nodeUsers in this.tvwHierarchy.Nodes[0].Nodes[intICounter].Nodes[intJCounter].Nodes)
                        {
                            strNodeTag = nodeWorkType.Tag.ToString();

                            this.MobjBLLWorkFlowSettingsSetting.DTOWorkFlowSettingsSetting.Enqueue(new clsDTOWorkFlowSettingsSetting
                            {
                                intWorkFlowTypeID = strNodeTag.Substring(0, strNodeTag.IndexOf("_", 0)).ToInt32(),
                                intUserID = nodeUsers.Tag.ToInt32()
                            });
                        }
                        intJCounter += 1;
                    }
                    intICounter += 1;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        private void FillParameters(TreeNode ndeParentNode)
        {
            try
            {
                foreach (TreeNode node in ndeParentNode.Nodes)
                {
                    this.MobjBLLWorkFlowSettingsSetting.DTOWorkFlowSettingsSetting.Enqueue(new clsDTOWorkFlowSettingsSetting
                    {
                        intWorkFlowTypeID = Convert.ToInt32(node.Tag),
                        intUserID = Convert.ToInt32(ndeParentNode.Tag)
                    });
                    this.FillParameters(node);
                }
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        private void btnSave_Click(object sender, EventArgs e)
        {
            BindingNavigatorSaveItem_Click(sender, e);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            BindingNavigatorSaveItem_Click(sender, e);
            this.Close();
        }

        private void tvwUsers_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                object data = e.Data.GetData(typeof(TreeNode));
                if (data == null) return;

                TreeNode node = this.tvwHierarchy.GetNodeAt(
                            this.tvwUsers.PointToClient(new System.Drawing.Point(e.X, e.Y)));

                TreeNode tndNew = (TreeNode)(data as TreeNode).Clone(); ;

                if (data != node)
                {
                    if (tndNew.Parent == null)
                        this.tvwUsers.Nodes.Remove(data as TreeNode);

                    if (node == null)
                        this.tvwUsers.Nodes.Add(tndNew);
                    else
                    {
                        node.Nodes.Add(tndNew);
                        node.Expand();
                    }
                    tndNew = null;

                    this.Changestatus(true);
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling roles:Role drag drop " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show(Ex.Message.ToString());
            }
        }

        private void tvwUsers_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void tvwUsers_DragOver(object sender, DragEventArgs e)
        {
            try
            {
                this.tvwUsers.SelectedNode = null;
                if (this._prevUserNode != null)
                {
                    this._prevUserNode.BackColor = System.Drawing.SystemColors.HighlightText;
                    this._prevUserNode.ForeColor = System.Drawing.SystemColors.ControlText;
                }

                TreeNode node = this.tvwUsers.GetNodeAt(
                            this.tvwUsers.PointToClient(new System.Drawing.Point(e.X, e.Y)));
                if (node == null) return;

                this.tvwUsers.SelectedNode = node;
                node.BackColor = System.Drawing.SystemColors.Highlight;
                node.ForeColor = System.Drawing.SystemColors.HighlightText;
                this._prevUserNode = node;
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling roles:Role drag drop " + this.Name + " " + ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show(ex.Message.ToString());
            }
        }

        private void tvwUsers_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (this._prevUserNode != null)
            {
                this._prevUserNode.BackColor = System.Drawing.SystemColors.HighlightText;
                this._prevUserNode.ForeColor = System.Drawing.SystemColors.ControlText;
            }
        }

        private void tvwUsers_ItemDrag(object sender, ItemDragEventArgs e)
        {
            (sender as TreeView).DoDragDrop(e.Item, DragDropEffects.Move);
        }

        private void tvwHierarchy_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Nodes.Count < 1)
                return;

            foreach (TreeNode Node in e.Node.Nodes)
            {
                Node.Checked = e.Node.Checked;
            }
        }

        private void tvwUsers_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Nodes.Count < 1)
                return;

            foreach (TreeNode Node in e.Node.Nodes)
            {
                Node.Checked = e.Node.Checked;
            }
        }
    }
}