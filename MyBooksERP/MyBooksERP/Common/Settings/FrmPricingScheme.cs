﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Microsoft.VisualBasic;

namespace MyBooksERP//.Settings.Forms
{
    public partial class frmPricingScheme : Form
    {
        private bool MbAddStatus;
        private bool MbChangeStatus;
        private int MiTimerInterval;
        private string MsMessageCaption;
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        ClsNotificationNew mObjNotification;
        clsBLLPricingScheme mobjBLLPricingScheme;
        DataTable MdatMessages;
        ClsLogWriter mObjLogs;
        private int MiPricingSchemeID;
        bool MblnShowErrorMess = false;             // Checking whether error messages are showing or not
        int MintRecordCount, MintRecordPosition;    // for current record position and total record count
        bool MblnAddPermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission = false;

        public frmPricingScheme()
        {
            InitializeComponent();
            mObjNotification = new ClsNotificationNew();
            mobjBLLPricingScheme = new clsBLLPricingScheme();

            MiTimerInterval = ClsCommonSettings.TimerInterval;
            MsMessageCaption = ClsCommonSettings.MessageCaption;
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            MmessageIcon = MessageBoxIcon.Information;
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmPricingScheme_Load(object sender, EventArgs e)
        {
            MbChangeStatus = false;
            MbAddStatus = true;
            SetPermissions();
            LoadMessage();
            LoadCombos();
            AddNewPricingScheme();
            SetBindingNavigatorButtons();
        }

        private void LoadMessage()
        {
            MdatMessages = mObjNotification.FillMessageArray((int)FormID.PricingScheme, (int)ClsCommonSettings.ProductID);
        }

        private void LoadCombos()
        {
            //DataTable datTemp = mobjBLLPricingScheme.FillCombos(new string[] { "CurrencyID,CurrencyName", "CurrencyReference", "" });
            DataTable datTemp = mobjBLLPricingScheme.FillCombos(new string[] { "distinct CR.CurrencyID,CR.CurrencyName", "CurrencyReference CR INNER JOIN CurrencyDetails CD ON CR.CurrencyID=CD.CurrencyID", "CD.CurrencyID=" + "(SELECT S.CurrencyId FROM CompanyMaster S Where CompanyID=" + ClsCommonSettings.CompanyID + ")" });
            cboCurrency.ValueMember = "CurrencyID";
            cboCurrency.DisplayMember = "CurrencyName";
            cboCurrency.DataSource = datTemp;
        }

        private void AddNewPricingScheme()
        {
            DataTable datTemp = mobjBLLPricingScheme.FillCombos(new string[] { "COUNT(*) AS RecCount", "InvPricingSchemeReference", "" });
            if (datTemp != null && datTemp.Rows.Count > 0)
                MintRecordCount = datTemp.Rows[0]["RecCount"].ToString().ToInt32();
            else
                MintRecordCount = 0;
            bindingNavigatorPositionItem.Text = (MintRecordCount + 1).ToString();
            MintRecordPosition = MintRecordCount + 1;
            bindingNavigatorCountItem.Text = " of " + (MintRecordCount + 1).ToString();
            SetBindingNavigatorButtons();

            MbAddStatus = true;
            SetEnable();
            ErrorProPricingScheme.Clear();
            MbChangeStatus = false;
            BtnPrint.Enabled = btnEmail.Enabled = false;
            bindingNavigatorSaveItem.Enabled = bindingNavigatorAddNewItem.Enabled = bindingNavigatorDeleteItem.Enabled =
                BtnOk.Enabled = BtnSave.Enabled = MbChangeStatus;
        }

        private void SetEnable()
        {
            txtSchemeName.Text = txtShortName.Text = txtRate.Text = "";
            rdbAmount.Checked = true;
            ErrorProPricingScheme.Clear();
        }

        private void SetBindingNavigatorButtons()
        {
            bindingNavigatorMoveNextItem.Enabled = bindingNavigatorMoveFirstItem.Enabled = bindingNavigatorMoveLastItem.Enabled = bindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(bindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCount;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                bindingNavigatorMoveNextItem.Enabled = bindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                bindingNavigatorMoveFirstItem.Enabled = bindingNavigatorMovePreviousItem.Enabled = false;
            }
        }

        private bool PricingSchemeValidation()
        {
            ErrorProPricingScheme.Clear();

            if (txtSchemeName.Text == "")
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3201, out MmessageIcon);
                ErrorProPricingScheme.SetError(txtSchemeName, MsMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                PricingSchemeStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmPricingScheme.Enabled = true;
                txtSchemeName.Focus();
                return false;
            }
            if (MbAddStatus)
            {
                try
                {
                    if (mobjBLLPricingScheme.CheckDuplication(new string[] { "*", "InvPricingSchemeReference", "upper(PricingSchemeName) = upper('" + txtSchemeName.Text.Trim() + "')" }))
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3202, out MmessageIcon);
                        ErrorProPricingScheme.SetError(txtSchemeName, MsMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        PricingSchemeStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmPricingScheme.Enabled = true;
                        txtSchemeName.Focus();
                        return false;
                    }
                }
                catch
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3201, out MmessageIcon);
                    ErrorProPricingScheme.SetError(txtSchemeName, MsMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    PricingSchemeStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmPricingScheme.Enabled = true;
                    txtSchemeName.Focus();
                    return false;
                }
            }

            if (rdbAmount.Checked == true && Convert.ToInt32(cboCurrency.SelectedValue) == 0)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3204, out MmessageIcon);
                ErrorProPricingScheme.SetError(cboCurrency, MsMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                PricingSchemeStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                cboCurrency.Focus();
                return false;
            }

            if (txtRate.Text == "")
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3205, out MmessageIcon);
                ErrorProPricingScheme.SetError(txtRate, MsMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                PricingSchemeStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmPricingScheme.Enabled = true;
                txtRate.Focus();
                return false;
            }

            //if (txtRate.Text != "")
            //{
            //    if (Convert.ToDouble(txtRate.Text) == 0)
            //    {
            //        MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3205, out MmessageIcon);
            //        ErrorProPricingScheme.SetError(txtRate, MsMessageCommon.Replace("#", "").Trim());
            //        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
            //        PricingSchemeStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            //        TmPricingScheme.Enabled = true;
            //        txtRate.Focus();
            //        return false;
            //    }
            //}
            return true;
        }

        private bool SavePricingSchemeDetails()
        {
            if (MbAddStatus)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 1, out MmessageIcon);
            }
            else
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3, out MmessageIcon);
            }
            if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                return false;

            mobjBLLPricingScheme.clsDTOPricingScheme.strPricingShortName = txtShortName.Text;
            mobjBLLPricingScheme.clsDTOPricingScheme.strDescription = txtSchemeName.Text;
            if (rdbAmount.Checked == true)
            {
                mobjBLLPricingScheme.clsDTOPricingScheme.intPercentOrAmount = 1;
                mobjBLLPricingScheme.clsDTOPricingScheme.intCurrencyID = Convert.ToInt32(cboCurrency.SelectedValue.ToString());
            }
            else
            {
                mobjBLLPricingScheme.clsDTOPricingScheme.intPercentOrAmount = 2;
                mobjBLLPricingScheme.clsDTOPricingScheme.intCurrencyID = 0;
                DataTable datTemp = mobjBLLPricingScheme.FillCombos(new string[] { "CurrencyId", "CompanyMaster", "CompanyID=1" });
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        mobjBLLPricingScheme.clsDTOPricingScheme.intCurrencyID = datTemp.Rows[0]["CurrencyID"].ToInt32();
                }
            }
            try { mobjBLLPricingScheme.clsDTOPricingScheme.dblPercOrAmtValue = Convert.ToDouble(txtRate.Text); }
            catch { txtRate.Text = "0"; mobjBLLPricingScheme.clsDTOPricingScheme.dblPercOrAmtValue = Convert.ToDouble(txtRate.Text); }
            if (mobjBLLPricingScheme.SavePricingSchemeDetails(MbAddStatus))
                return true;
            else
                return false;
        }

        private void bindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (PricingSchemeValidation())
            {
                if (SavePricingSchemeDetails())
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 2, out MmessageIcon);
                    PricingSchemeStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmPricingScheme.Enabled = true;
                    AddNewPricingScheme();
                }
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (PricingSchemeValidation())
            {
                if (SavePricingSchemeDetails())
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 2, out MmessageIcon);
                    PricingSchemeStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmPricingScheme.Enabled = true;
                    AddNewPricingScheme();
                }
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (PricingSchemeValidation())
            {
                if (SavePricingSchemeDetails())
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 2, out MmessageIcon);
                    PricingSchemeStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmPricingScheme.Enabled = true;
                    MbChangeStatus = false;
                }
            }
            this.Close();
        }

        private void rdbAmount_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbAmount.Checked == true)
            {
                lblRate.Text = "Amount";
                lblCurrency.Enabled = true;
                cboCurrency.Enabled = true;
            }
        }

        private void rdbPercentage_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbPercentage.Checked == true)
            {
                lblCurrency.Enabled = false;
                cboCurrency.Enabled = false;
                lblRate.Text = "Percentage";
            }
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.PricingScheme, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void DisplayPricingScheme()
        {
            mobjBLLPricingScheme.clsDTOPricingScheme.intRowNumber = MintRecordPosition;
            DataTable datTemp = mobjBLLPricingScheme.DisplayPricingScheme();

            if (datTemp != null && datTemp.Rows.Count > 0)
            {
                mobjBLLPricingScheme.clsDTOPricingScheme.intPricingSchemeID = Convert.ToInt32(datTemp.Rows[0]["PricingSchemeID"].ToString());
                txtSchemeName.Text = datTemp.Rows[0]["PricingSchemeName"].ToString();
                txtShortName.Text = datTemp.Rows[0]["PricingShortName"].ToString();
                if (Convert.ToInt32(datTemp.Rows[0]["PercentOrAmount"].ToString()) == 1)
                    rdbAmount.Checked = true;
                else
                    rdbPercentage.Checked = true;
                cboCurrency.SelectedValue = Convert.ToInt32(datTemp.Rows[0]["CurrencyID"].ToString());
                txtRate.Text = datTemp.Rows[0]["PercOrAmtValue"].ToString();

                bindingNavigatorPositionItem.Text = mobjBLLPricingScheme.clsDTOPricingScheme.intRowNumber.ToString();
                bindingNavigatorCountItem.Text = " of " + MintRecordCount.ToString();
                bindingNavigatorAddNewItem.Enabled = MblnAddPermission; // MblnAddPermission;
                bindingNavigatorDeleteItem.Enabled = MblnDeletePermission; // MblnDeletePermission;
                MbAddStatus = false;
                MbChangeStatus = false;
                bindingNavigatorSaveItem.Enabled =
                BtnOk.Enabled = BtnSave.Enabled = MbChangeStatus;

                BtnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
            }
        }

        private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition > 1)
                {
                    MintRecordPosition = 1;
                    DisplayPricingScheme();
                    PricingSchemeStatusLabel.Text = mObjNotification.GetErrorMessage(MdatMessages, 9);
                    TmPricingScheme.Enabled = true;
                    SetBindingNavigatorButtons();
                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on BindingNavigator Move first item click()" + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on BindingNavigator Move first item click " + Ex.Message.ToString());
            }
        }

        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition > 1)
                {
                    MintRecordPosition--;
                    DisplayPricingScheme();
                    PricingSchemeStatusLabel.Text = mObjNotification.GetErrorMessage(MdatMessages, 10);
                    TmPricingScheme.Enabled = true;
                    SetBindingNavigatorButtons();
                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on BindingNavigator Move Previous item click() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on BindingNavigator Move Previous item click() " + Ex.Message.ToString());
            }
        }

        private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition < MintRecordCount)
                {
                    MintRecordPosition++;
                    DisplayPricingScheme();
                    PricingSchemeStatusLabel.Text = mObjNotification.GetErrorMessage(MdatMessages, 11);
                    TmPricingScheme.Enabled = true;
                    SetBindingNavigatorButtons();

                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on BindingNavigator move next item click() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on BindingNavigator move next item click() " + Ex.Message.ToString());
            }
        }

        private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition <= MintRecordCount + 1)
                {
                    MintRecordPosition = MintRecordCount;
                    DisplayPricingScheme();
                    PricingSchemeStatusLabel.Text = mObjNotification.GetErrorMessage(MdatMessages, 12);
                    TmPricingScheme.Enabled = true;
                    SetBindingNavigatorButtons();
                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on Binding navigator move last item click() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on Binding Navigator move last item click() " + Ex.Message.ToString());
            }
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewPricingScheme();
        }

        private void bindingNavigatorCancelItem_Click(object sender, EventArgs e)
        {
            AddNewPricingScheme();
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable datTemp = mobjBLLPricingScheme.FillCombos(new string[] { "*", "InvCostingAndPricingDetails", "PricingSchemeID=" + mobjBLLPricingScheme.clsDTOPricingScheme.intPricingSchemeID });
                if (datTemp.Rows.Count == 0)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 13, out MmessageIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (mobjBLLPricingScheme.DeletePricingScheme())
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 4, out MmessageIcon);
                            PricingSchemeStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                            TmPricingScheme.Enabled = true;
                            AddNewPricingScheme();
                        }
                    }
                }
                else
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 3203, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    PricingSchemeStatusLabel.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmPricingScheme.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnDeleteItem_Click() " + ex.Message);
                mObjLogs.WriteLog("Error in bnDeleteItem_Click() " + ex.Message, 2);
            }
        }

        private void TmPricingScheme_Tick(object sender, EventArgs e)
        {
            PricingSchemeStatusLabel.Text = "";
            ErrorProPricingScheme.Clear();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = MiPricingSchemeID;
                ObjViewer.PiFormID = (int)FormID.PricingScheme;
                ObjViewer.ShowDialog();
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on form print:LoadReport() " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport() " + Ex.Message.ToString());
            }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Pricing Scheme";
                    ObjEmailPopUp.EmailFormType = EmailFormID.PricingScheme;
                    ObjEmailPopUp.EmailSource = mobjBLLPricingScheme.GetPricingSchemeReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on form Email: BtnEmail_Click" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Email BtnEmail_Click " + Ex.Message.ToString());
            }
        }

        private void frmPricingScheme_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MbChangeStatus)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MdatMessages, 8, out MmessageIcon);
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        public void ChangeStatus(object sender, EventArgs e) // to set the event
        {
            ChangeStatus();
        }

        private void ChangeStatus()
        {
            MbChangeStatus = true;
            if (MbAddStatus)
                BtnOk.Enabled = BtnSave.Enabled = bindingNavigatorSaveItem.Enabled = MblnAddPermission;
            else
                BtnOk.Enabled = BtnSave.Enabled = bindingNavigatorSaveItem.Enabled = MblnUpdatePermission;

            ErrorProPricingScheme.Clear();
        }
        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }
    }
}
