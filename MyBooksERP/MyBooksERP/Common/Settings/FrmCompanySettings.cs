﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;


namespace MyBooksERP
{
    /* ===============================================
Author:		<Author,,Sawmya>
Modified by:<Modified by,,Arun>
Modified Date:<Modified Date,24 feb 2011>
Create date: <Create Date,,23 Feb 2011>
Description:	<Description,, Company Settings Form->
================================================*/
    public partial class FrmCompanySettings : DevComponents.DotNetBar.Office2007Form
    {
        
        //private bool MblnAddStatus; //To Specify Add/Update mode 
        private bool MblnChangeStatus = false; //To Specify Change Status
        private bool MblnViewPermission = true;//To Set View Permission
        private bool MblnAddPermission = true;//To Set Add Permission
        private bool MblnUpdatePermission = true;//To Set Update Permission
        private bool MblnDeletePermission = true;//To Set Delete Permission
        private bool MblnAddUpdatePermission = true;//
        private bool MblnPrintEmailPermission = true;
        
        private int MintTimerInterval;
        private int MintCompany;

        private string MstrMessageCommon;
        private string MstrMessageCaption;
        private MessageBoxIcon MmessageIcon;
        private ArrayList MArrlstMessage;                  // Error Message display
        private ArrayList MArrlstStatusMessage;

        private ClsBLLCompanySettings MobjClsBLLCompanySettings;
        ClsCompanyList MObjCompanyList = new ClsCompanyList();
        ClsLogWriter mObjLogs;
        ClsNotification mObjNotification;

        public FrmCompanySettings()
        {
            InitializeComponent();

            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MstrMessageCommon = "";

            MstrMessageCaption = ClsCommonSettings.MessageCaption;    //Message caption
            MmessageIcon = MessageBoxIcon.Information;
            MobjClsBLLCompanySettings = new ClsBLLCompanySettings();
           
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
        }

        private void FrmCompSettings_Load(object sender, EventArgs e)
        {
            try
            {
                SetPermissions();
                LoadCombo();
                LoadMessage();
                CboCompanyName.SelectedIndex = 0;
                if (Convert.ToInt32(CboCompanyName.SelectedValue) > 0)
                {
                    DisplayCompanySettings();
                }
                else
                {
                    PrgCompanySettings.SelectedObject = MObjCompanyList;
                    PrgCompanySettings.ExpandAllGridItems();
                }
                MblnChangeStatus = false;
                ChangeStatus();
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on Companysettings formload" + this.Name + " " + Ex.Message.ToString(), 2);

            }

            
        }
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.General, (Int32)eMenuID.CompanySettings,
                 out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }

            if (MblnAddPermission == true || MblnUpdatePermission == true)
                MblnAddUpdatePermission = true;

             pnlForm.Enabled = MblnViewPermission;
             BtnPrint.Enabled = MblnPrintEmailPermission;
             BtnEmail.Enabled = MblnPrintEmailPermission;
             BtnOk.Enabled = MblnUpdatePermission;
             BtnReset.Enabled = MblnAddUpdatePermission;
            
        }
     

        private void LoadMessage()
        {
            MArrlstMessage = new ArrayList();
            MArrlstStatusMessage = new ArrayList();
            MArrlstMessage = mObjNotification.FillMessageArray((int)FormID.CompanySettings, (int)ClsCommonSettings.ProductID);
            MArrlstStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.CompanySettings, (int)ClsCommonSettings.ProductID);
        }

        private void ChangeStatus()
        {
            BtnReset.Enabled = MblnUpdatePermission;
            BtnOk.Enabled = MblnUpdatePermission;
            CompSettingsBindingNavigatorSaveButton.Enabled = MblnUpdatePermission;
            ResetButton.Enabled = MblnUpdatePermission;
        }

        // To Fill Combobox
        private bool LoadCombo()
        {
            try
            {
                DataTable datCombos = new DataTable();
                datCombos = MobjClsBLLCompanySettings.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                CboCompanyName.ValueMember = "CompanyID";
                CboCompanyName.DisplayMember = "CompanyName";
                CboCompanyName.DataSource = datCombos;
                return true;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on LoadCombo()" + this.Name + " " + Ex.Message.ToString(), 2);
                return false;
            }
           
            //mobjCompSettings.FillCompanyNameComboBox(CmbCompanyName, new string[] { "CompanyID,Name", "CompanyMaster", "", "CompanyID", "Name" });
            
        }

      
        private void CboCompanyName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CboCompanyName_SelectionChangeCommitted(sender,new EventArgs());
            }
        }
        private void CboCompanyName_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(CboCompanyName.SelectedValue) > 0)
                {

                    InsertDefaultValues();
                    DisplayCompanySettings();
                    
                }
            }
            catch(Exception Ex)
            {
                mObjLogs.WriteLog("Error on CboCompanyName_SelectionChangeCommitted" + this.Name + " " + Ex.Message.ToString(), 2);

            }
        }

        private bool DisplayCompanySettings()
        {
            try
            {

                clsDTOCompanySettings.intCompanyID = Convert.ToInt32(CboCompanyName.SelectedValue);
                MobjClsBLLCompanySettings.DisplayCompanysettings(Convert.ToInt32(CboCompanyName.SelectedValue), 6, "");
                MObjCompanyList = new ClsCompanyList();
                PrgCompanySettings.SelectedObject = MObjCompanyList;
                PrgCompanySettings.ExpandAllGridItems();

                BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
                return true;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on DisplayCompanySettings()" + this.Name + " " + Ex.Message.ToString(), 2);
                return false;
            }
        }
        private bool InsertDefaultValues()//Inserting Default values to a new Copmany;
        {
            try
            {

                if (MobjClsBLLCompanySettings.Insert(Convert.ToInt32(CboCompanyName.SelectedValue)))
                {
                    return true;
                }
                return false;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on InsertDefaultValues " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("InsertDefaultValues " + Ex.Message.ToString());

                return false;
            }
           
        }


        //FUNCTION TO SAVE displayed by propertydescriptor
        public bool SaveDetails()
        {
            try
            {
                if (Convert.ToInt32(CboCompanyName.SelectedValue) > 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MArrlstMessage, 3, out MmessageIcon);
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                        == System.Windows.Forms.DialogResult.No)
                        return false;

                    while (PrgCompanySettings.SelectedGridItem.Parent.PropertyDescriptor != null)
                    {
                        PrgCompanySettings.SelectedGridItem = PrgCompanySettings.SelectedGridItem.Parent;
                    }

                    GetSelectedItems(PrgCompanySettings.SelectedGridItem);
                }
                return true;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on SaveDetails() " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("SaveDetails() " + Ex.Message.ToString());
                return false;
               
            }
            
        }

        private void GetSelectedItems(GridItem currentGridItem)
        {

            try
            {
                foreach (GridItem item in currentGridItem.GridItems)
                {
                    PropertyDescriptor propertyDescriptor = item.PropertyDescriptor;

                    //Calling the method for saving the details of each grid item
                    MObjCompanyList.SavePropertGrid(propertyDescriptor);
                    GetSelectedItems(item); // Recursive call 
                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on GetSelectedItems() " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("GetSelectedItems() " + Ex.Message.ToString());

            }
        }

        private void CompSettingsBindingNavigatorSaveButton_Click(object sender, EventArgs e)
        {
            try
            {

                if (SaveDetails())
                {

                    using (FrmLogin objLogin = new FrmLogin())
                    {
                        objLogin.SetCommonSettingsCompanySettingsInfo();
                        objLogin.SetCommonSettingsConfigurationSettingsInfo();
                    }

                    PrgCompanySettings.Refresh();

                    MstrMessageCommon = mObjNotification.GetErrorMessage(MArrlstMessage, 21, out MmessageIcon);
                    lblCompSettingsStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmCompSettings.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                this.mObjLogs.WriteLog(string.Format("Error on CompSettingsBindingNavigatorSaveButton_Click() FormName :{0}, Exception :{1}", this.Name, ex.ToString()), 3);
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {

            if (SaveDetails())
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MArrlstMessage, 21, out MmessageIcon);
                lblCompSettingsStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmCompSettings.Enabled = true;
                MblnChangeStatus = false;
             }
            SetCommonSettingsCompanySettingsInfo();
            this.Close();
        }

        private void SetCommonSettingsCompanySettingsInfo()
        {
            clsBLLLogin MobjClsBLLLogin = new clsBLLLogin();
            DataTable dtCompanySettingsInfo = MobjClsBLLLogin.GetCompanySettings(ClsCommonSettings.CompanyID);
            //string strValue = "";

            //strValue = GetCompanySettingsValue("Installments", "First payment of installment", dtCompanySettingsInfo);
            //if (!string.IsNullOrEmpty(strValue))
            //    ClsCommonSettings.PintFirstInstallmentInterval = Convert.ToInt32(strValue);
            //else
            //    ClsCommonSettings.PintFirstInstallmentInterval = 0;

            //strValue = GetCompanySettingsValue("Installments", "Installment Interval", dtCompanySettingsInfo);
            //if (!string.IsNullOrEmpty(strValue))
            //    ClsCommonSettings.PintInstallmentInterval = Convert.ToInt32(strValue);
            //else
            //    ClsCommonSettings.PintInstallmentInterval = 0;

            //strValue = GetCompanySettingsValue("Installments", "Installments starts After", dtCompanySettingsInfo);
            //if (!string.IsNullOrEmpty(strValue) && strValue.ToUpper() == "INVOICE")
            //    ClsCommonSettings.PintInstallmentStartFrom = 1;
            //else
            //    ClsCommonSettings.PintInstallmentStartFrom = 2;

            //ClsCommonSettings.strSQPrefix = GetCompanySettingsValue("Sales Quotation", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.blnSQAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Sales Quotation", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.strSQDueDateLimit = GetCompanySettingsValue("Sales Quotation", "Due Date Limit", dtCompanySettingsInfo);
            //ClsCommonSettings.strSOPrefix = GetCompanySettingsValue("Sales Order", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.blnSOAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Sales Order", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.strSODueDateLimit = GetCompanySettingsValue("Sales Order", "Due Date Limit", dtCompanySettingsInfo);
            //ClsCommonSettings.strSIPrefix = GetCompanySettingsValue("Sales Invoice", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.blnSIAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Sales Invoice", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.strSIDueDateLimit = GetCompanySettingsValue("Sales Invoice", "Due Date Limit", dtCompanySettingsInfo);
            //ClsCommonSettings.strPOSPrefix = GetCompanySettingsValue("POS", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.blnPOSAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("POS", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.strSRPrefix = GetCompanySettingsValue("Sales Return", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.blnSRAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Sales Return", "Prefix Editable", dtCompanySettingsInfo));

            //ClsCommonSettings.PIPrefix = GetCompanySettingsValue("Purchase Indent", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.PIAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Indent", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.PIDueDateLimit = GetCompanySettingsValue("Purchase Indent", "Due Date Limit", dtCompanySettingsInfo);
            //ClsCommonSettings.PQPrefix = GetCompanySettingsValue("Purchase Quotation", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.PQAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Quotation", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.PQDueDateLimit = GetCompanySettingsValue("Purchase Quotation", "Due Date Limit", dtCompanySettingsInfo);
            //ClsCommonSettings.POPrefix = GetCompanySettingsValue("Purchase Order", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.POAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Order", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.PODueDateLimit = GetCompanySettingsValue("Purchase Order", "Due Date Limit", dtCompanySettingsInfo);
            //ClsCommonSettings.PINVPrefix = GetCompanySettingsValue("Purchase Invoice", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.PINVAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Invoice", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.PGRNPrefix = GetCompanySettingsValue("Goods Receipt Note", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.PGRNAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Goods Receipt Note", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.PRPrefix = GetCompanySettingsValue("Purchase Return", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.PRAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Return", "Prefix Editable", dtCompanySettingsInfo));

            //ClsCommonSettings.DeliverySchedulePrefix = GetCompanySettingsValue("Delivery Schedule", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.DeliveryScheduleAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Delivery Schedule", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.CollectionSchedulePrefix = GetCompanySettingsValue("Collection Schedule", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.CollectionScheduleAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Collection Schedule", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.ComplaintSchedulePrefix = GetCompanySettingsValue("Complaint Schedule", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.ComplaintScheduleAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Complaint Schedule", "Prefix Editable", dtCompanySettingsInfo));

            //ClsCommonSettings.ItemIssuePrefix = GetCompanySettingsValue("Item Issue", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.ItemIssueAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Item Issue", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.InstallmentCollectionPrefix = GetCompanySettingsValue("Installment Collection", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.InstallmentCollectionAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Installment Collection", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.PaymentsPrefix = GetCompanySettingsValue("Payments", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.PaymentsAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Payments", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.ReceiptsPrefix = GetCompanySettingsValue("Receipts", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.ReceiptsAutoGenerate = ConvertStringToBoolean(GetCompanySettingsValue("Receipts", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.ComplaintRegistrationPrefix = GetCompanySettingsValue("Complaint Registration", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.ComplaintRegistrationAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Complaint Registration", "Prefix Editable", dtCompanySettingsInfo));

            //ClsCommonSettings.strEmployeeNumberPrefix = GetCompanySettingsValue("Employee Number", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.blnEmployeeNumberAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Employee Number", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.strCustomerCodePrefix = GetCompanySettingsValue("Customer Code", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.blnCustomerAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Customer Code", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.strSupplierCodePrefix = GetCompanySettingsValue("Supplier Code", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.blnSupplierAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Supplier Code", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.strAgentBankCodePrefix = GetCompanySettingsValue("AgentBank Code", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.strContracterCodePrefix = GetCompanySettingsValue("Contractor Code", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.strProductCodePrefix = GetCompanySettingsValue("Product Code", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.blnProductCodeAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Product Code", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.strProductGroupCodePrefix = GetCompanySettingsValue("ProductGroup Code", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.blnProductGroupCodeAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("ProductGroup Code", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.strPlanningPrefix = GetCompanySettingsValue("Plan Number", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.blnPlanningAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Plan Number", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.strStockAdjustmentNumberPrefix = GetCompanySettingsValue("Stock Adjustment Number", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.blnStockAdjustmentNumberAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Stock Adjustment Number", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.strRFQNumber = GetCompanySettingsValue("RFQ", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.blnRFQAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("RFQ", "Prefix Editable", dtCompanySettingsInfo));
            //ClsCommonSettings.strRFQDueDateLimit = GetCompanySettingsValue("RFQ", "Due Date Limit", dtCompanySettingsInfo);
            //ClsCommonSettings.strWareHouseLocationPrefix = GetCompanySettingsValue("WareHouse", "Location Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.strWareHouseRowPrefix = GetCompanySettingsValue("WareHouse", "Row Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.strWareHouseBlockPrefix = GetCompanySettingsValue("WareHouse", "Block Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.strWareHouseLotPrefix = GetCompanySettingsValue("WareHouse", "Lot Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.strCollectionRecoveryPrefix = GetCompanySettingsValue("Collection Recovery Number", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.strProjectCodePrefix = GetCompanySettingsValue("Project Code", "Prefix", dtCompanySettingsInfo);


            //ClsCommonSettings.PblnAutoGeneratePartyAccount = ConvertStringToBoolean(GetCompanySettingsValue("Advanced", "Auto generate Party accounts", dtCompanySettingsInfo));
            //ClsCommonSettings.RFQApproval = ConvertStringToBoolean(GetCompanySettingsValue("RFQ", "Approval", dtCompanySettingsInfo));
            //ClsCommonSettings.PQApproval = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Quotation", "Approval", dtCompanySettingsInfo));
            //ClsCommonSettings.POApproval = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Order", "Approval", dtCompanySettingsInfo));
            //ClsCommonSettings.SQApproval = ConvertStringToBoolean(GetCompanySettingsValue("Sales Quotation", "Approval", dtCompanySettingsInfo));
            //ClsCommonSettings.SOApproval = ConvertStringToBoolean(GetCompanySettingsValue("Sales Order", "Approval", dtCompanySettingsInfo));
            //ClsCommonSettings.StockTransferApproval = ConvertStringToBoolean(GetCompanySettingsValue("Stock Transfer", "Approval", dtCompanySettingsInfo));
        }
        private bool ConvertStringToBoolean(string strValue)
        {
            if (strValue == "Yes")
                return true;
            return false;
        }

        public string GetCompanySettingsValue(string strConfigurationItem, string strSubItem, DataTable dtCompanySettings)
        {
            dtCompanySettings.DefaultView.RowFilter = "ConfigurationItem = '" + strConfigurationItem + "' And SubItem = '" + strSubItem + "'";
            if (dtCompanySettings.DefaultView.ToTable().Rows.Count > 0)
                return dtCompanySettings.DefaultView.ToTable().Rows[0]["ConfigurationValue"].ToString();
            return "";
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmCompSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MblnChangeStatus)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MArrlstMessage, 8, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    MobjClsBLLCompanySettings = null;
                    MObjCompanyList = null;
                    mObjLogs = null;
                    mObjNotification = null;
                    e.Cancel = false;
                }
            }
            else
            {
                MobjClsBLLCompanySettings = null;
                MObjCompanyList = null;
                mObjLogs = null;
                mObjNotification = null;
            }
        }

        private void TmCompSettings_Tick(object sender, EventArgs e)
        {
            lblCompSettingsStatus.Text = "";
            TmCompSettings.Enabled = false;
        }

        private void BtnReset_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(CboCompanyName.SelectedValue) > 0)
            {
                if (Reset())
                {
                    PrgCompanySettings.Refresh();
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MArrlstMessage, 452, out MmessageIcon);
                    lblCompSettingsStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmCompSettings.Enabled = true;
                    PrgCompanySettings.Refresh();

                }
            }
          
          
        }

        private bool Reset()
        {
            try
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MArrlstMessage, 451, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    == System.Windows.Forms.DialogResult.No)
                    return false;
                if (MobjClsBLLCompanySettings.Reset(Convert.ToInt32(CboCompanyName.SelectedValue)))
                    return true;
                else return false;
            }
            catch(Exception Ex)
            {
                mObjLogs.WriteLog("Error on Reset() " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Reset() " + Ex.Message.ToString());
                return false;
            }
        }
        private void ResetButton_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(CboCompanyName.SelectedValue) > 0)
            {
                if (Reset())
                {
                    PrgCompanySettings.Refresh();
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MArrlstMessage, 452, out MmessageIcon);
                    lblCompSettingsStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmCompSettings.Enabled = true;
                    PrgCompanySettings.Refresh();

                }
            }
          
        }

        private void PrgCompanySettings_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            MblnChangeStatus = true;
            ChangeStatus();
        }

        private void CmbCompanyName_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboCompanyName.DroppedDown = false;
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();

        }
        private void LoadReport()
        {
            if (Convert.ToInt32(CboCompanyName.SelectedValue) > 0)
                MintCompany = Convert.ToInt32(CboCompanyName.SelectedValue);
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = MintCompany;

                ObjViewer.PiFormID = (int)FormID.CompanySettings;
                ObjViewer.ShowDialog();
            }
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
            {
                ObjEmailPopUp.MsSubject = "Company Settings";
                ObjEmailPopUp.EmailFormType = EmailFormID.CompanySettings;
                MintCompany = Convert.ToInt32(CboCompanyName.SelectedValue);
                ObjEmailPopUp.EmailSource = MobjClsBLLCompanySettings.GetCompanySettingsReport(MintCompany);
                ObjEmailPopUp.ShowDialog();
            }
        }

        private void FrmCompanySettings_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.S:
                        CompSettingsBindingNavigatorSaveButton_Click(sender, new EventArgs());//save
                        break;
                    //case Keys.Control | Keys.O:
                    //    btnOk_Click(sender, new EventArgs());//OK
                    //    break; 
                    //case Keys.Control | Keys.L:
                    //    btnCancel_Click(sender, new EventArgs());//Cancel
                    //    break;
                   
                    case Keys.Control | Keys.P:
                        BtnPrint_Click(sender, new EventArgs());//Cancel
                        break;
                    case Keys.Control | Keys.M:
                        BtnEmail_Click(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "CompanySettings";
            objHelp.ShowDialog();
            objHelp = null;
        }

        

       
    }
}
