﻿namespace MyBooksERP
{
    partial class FrmEmailSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label PassWordLabel;
            System.Windows.Forms.Label UserNameLabel;
            System.Windows.Forms.Label PortNumberLabel;
            System.Windows.Forms.Label OutgoingServerLabel;
            System.Windows.Forms.Label IncomingServerLabel;
            System.Windows.Forms.Label Label13;
            System.Windows.Forms.Label Label14;
            System.Windows.Forms.Label Label16;
            System.Windows.Forms.Label Label10;
            System.Windows.Forms.Label Label11;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmailSettings));
            this.PnlEmailSettings = new System.Windows.Forms.Panel();
            this.TabEmailSettings = new System.Windows.Forms.TabControl();
            this.TpEmail = new System.Windows.Forms.TabPage();
            this.Label1 = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.BtnTestSettings = new System.Windows.Forms.Button();
            this.Label2 = new System.Windows.Forms.Label();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.TxtEmailPassword = new System.Windows.Forms.TextBox();
            this.TxtEmailUserName = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.TxtEmailPortNumber = new DemoClsDataGridview.NumericTextBox();
            this.TxtEmailOutgoingServer = new System.Windows.Forms.TextBox();
            this.TxtEmailIncomingServer = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.TpSms = new System.Windows.Forms.TabPage();
            this.GroupBox5 = new System.Windows.Forms.GroupBox();
            this.TxtSmsPortNumber = new DemoClsDataGridview.NumericTextBox();
            this.TxtSmsOutgoingserver = new System.Windows.Forms.TextBox();
            this.TxtSmsServer = new System.Windows.Forms.TextBox();
            this.Label15 = new System.Windows.Forms.Label();
            this.GroupBox4 = new System.Windows.Forms.GroupBox();
            this.TxtSmsPassword = new System.Windows.Forms.TextBox();
            this.TxtSmsUsername = new System.Windows.Forms.TextBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.ErrorEmailSettings = new System.Windows.Forms.ErrorProvider(this.components);
            PassWordLabel = new System.Windows.Forms.Label();
            UserNameLabel = new System.Windows.Forms.Label();
            PortNumberLabel = new System.Windows.Forms.Label();
            OutgoingServerLabel = new System.Windows.Forms.Label();
            IncomingServerLabel = new System.Windows.Forms.Label();
            Label13 = new System.Windows.Forms.Label();
            Label14 = new System.Windows.Forms.Label();
            Label16 = new System.Windows.Forms.Label();
            Label10 = new System.Windows.Forms.Label();
            Label11 = new System.Windows.Forms.Label();
            this.PnlEmailSettings.SuspendLayout();
            this.TabEmailSettings.SuspendLayout();
            this.TpEmail.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            this.GroupBox3.SuspendLayout();
            this.TpSms.SuspendLayout();
            this.GroupBox5.SuspendLayout();
            this.GroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorEmailSettings)).BeginInit();
            this.SuspendLayout();
            // 
            // PassWordLabel
            // 
            PassWordLabel.AutoSize = true;
            PassWordLabel.Location = new System.Drawing.Point(7, 55);
            PassWordLabel.Name = "PassWordLabel";
            PassWordLabel.Size = new System.Drawing.Size(53, 13);
            PassWordLabel.TabIndex = 2;
            PassWordLabel.Text = "Password";
            // 
            // UserNameLabel
            // 
            UserNameLabel.AutoSize = true;
            UserNameLabel.Location = new System.Drawing.Point(7, 26);
            UserNameLabel.Name = "UserNameLabel";
            UserNameLabel.Size = new System.Drawing.Size(60, 13);
            UserNameLabel.TabIndex = 0;
            UserNameLabel.Text = "User Name";
            // 
            // PortNumberLabel
            // 
            PortNumberLabel.AutoSize = true;
            PortNumberLabel.Location = new System.Drawing.Point(10, 87);
            PortNumberLabel.Name = "PortNumberLabel";
            PortNumberLabel.Size = new System.Drawing.Size(66, 13);
            PortNumberLabel.TabIndex = 4;
            PortNumberLabel.Text = "Port Number";
            // 
            // OutgoingServerLabel
            // 
            OutgoingServerLabel.AutoSize = true;
            OutgoingServerLabel.Location = new System.Drawing.Point(10, 58);
            OutgoingServerLabel.Name = "OutgoingServerLabel";
            OutgoingServerLabel.Size = new System.Drawing.Size(84, 13);
            OutgoingServerLabel.TabIndex = 2;
            OutgoingServerLabel.Text = "Outgoing Server";
            // 
            // IncomingServerLabel
            // 
            IncomingServerLabel.AutoSize = true;
            IncomingServerLabel.Location = new System.Drawing.Point(10, 29);
            IncomingServerLabel.Name = "IncomingServerLabel";
            IncomingServerLabel.Size = new System.Drawing.Size(84, 13);
            IncomingServerLabel.TabIndex = 0;
            IncomingServerLabel.Text = "Incoming Server";
            // 
            // Label13
            // 
            Label13.AutoSize = true;
            Label13.Location = new System.Drawing.Point(10, 86);
            Label13.Name = "Label13";
            Label13.Size = new System.Drawing.Size(66, 13);
            Label13.TabIndex = 4;
            Label13.Text = "Port Number";
            // 
            // Label14
            // 
            Label14.AutoSize = true;
            Label14.Location = new System.Drawing.Point(10, 57);
            Label14.Name = "Label14";
            Label14.Size = new System.Drawing.Size(84, 13);
            Label14.TabIndex = 2;
            Label14.Text = "Outgoing Server";
            // 
            // Label16
            // 
            Label16.AutoSize = true;
            Label16.Location = new System.Drawing.Point(10, 28);
            Label16.Name = "Label16";
            Label16.Size = new System.Drawing.Size(55, 13);
            Label16.TabIndex = 0;
            Label16.Text = "SMS URL";
            // 
            // Label10
            // 
            Label10.AutoSize = true;
            Label10.Location = new System.Drawing.Point(7, 55);
            Label10.Name = "Label10";
            Label10.Size = new System.Drawing.Size(53, 13);
            Label10.TabIndex = 2;
            Label10.Text = "Password";
            // 
            // Label11
            // 
            Label11.AutoSize = true;
            Label11.Location = new System.Drawing.Point(7, 26);
            Label11.Name = "Label11";
            Label11.Size = new System.Drawing.Size(60, 13);
            Label11.TabIndex = 0;
            Label11.Text = "User Name";
            // 
            // PnlEmailSettings
            // 
            this.PnlEmailSettings.BackColor = System.Drawing.Color.Transparent;
            this.PnlEmailSettings.Controls.Add(this.TabEmailSettings);
            this.PnlEmailSettings.Location = new System.Drawing.Point(0, 0);
            this.PnlEmailSettings.Name = "PnlEmailSettings";
            this.PnlEmailSettings.Size = new System.Drawing.Size(404, 428);
            this.PnlEmailSettings.TabIndex = 0;
            // 
            // TabEmailSettings
            // 
            this.TabEmailSettings.Controls.Add(this.TpEmail);
            this.TabEmailSettings.Controls.Add(this.TpSms);
            this.TabEmailSettings.Location = new System.Drawing.Point(8, 7);
            this.TabEmailSettings.Name = "TabEmailSettings";
            this.TabEmailSettings.SelectedIndex = 0;
            this.TabEmailSettings.Size = new System.Drawing.Size(389, 420);
            this.TabEmailSettings.TabIndex = 30;
            // 
            // TpEmail
            // 
            this.TpEmail.Controls.Add(this.Label1);
            this.TpEmail.Controls.Add(this.GroupBox1);
            this.TpEmail.Controls.Add(this.Label2);
            this.TpEmail.Controls.Add(this.GroupBox2);
            this.TpEmail.Controls.Add(this.GroupBox3);
            this.TpEmail.Location = new System.Drawing.Point(4, 22);
            this.TpEmail.Name = "TpEmail";
            this.TpEmail.Padding = new System.Windows.Forms.Padding(3);
            this.TpEmail.Size = new System.Drawing.Size(381, 394);
            this.TpEmail.TabIndex = 0;
            this.TpEmail.Text = "Email";
            this.TpEmail.UseVisualStyleBackColor = true;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(6, 12);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(185, 13);
            this.Label1.TabIndex = 14;
            this.Label1.Text = "Internet E-mail Settings (POP3)";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.Label8);
            this.GroupBox1.Controls.Add(this.Label9);
            this.GroupBox1.Controls.Add(this.Label7);
            this.GroupBox1.Controls.Add(this.BtnTestSettings);
            this.GroupBox1.Location = new System.Drawing.Point(6, 293);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(368, 86);
            this.GroupBox1.TabIndex = 26;
            this.GroupBox1.TabStop = false;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(6, 49);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(258, 13);
            this.Label8.TabIndex = 28;
            this.Label8.Text = "by clicking this button. (Requires Internet connection)";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.BackColor = System.Drawing.Color.White;
            this.Label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.Location = new System.Drawing.Point(6, 0);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(144, 13);
            this.Label9.TabIndex = 26;
            this.Label9.Text = "Test your email settings";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(6, 25);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(358, 13);
            this.Label7.TabIndex = 27;
            this.Label7.Text = "After filling the above information, we recommend you to test your account,";
            // 
            // BtnTestSettings
            // 
            this.BtnTestSettings.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTestSettings.Location = new System.Drawing.Point(270, 44);
            this.BtnTestSettings.Name = "BtnTestSettings";
            this.BtnTestSettings.Size = new System.Drawing.Size(94, 27);
            this.BtnTestSettings.TabIndex = 5;
            this.BtnTestSettings.Text = "Test Settings";
            this.BtnTestSettings.UseVisualStyleBackColor = true;
            this.BtnTestSettings.Click += new System.EventHandler(this.BtnTestSettings_Click);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(6, 37);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(377, 13);
            this.Label2.TabIndex = 15;
            this.Label2.Text = "Each of these settings are required for your E-Mail sending to function properly." +
                "";
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.TxtEmailPassword);
            this.GroupBox2.Controls.Add(PassWordLabel);
            this.GroupBox2.Controls.Add(this.TxtEmailUserName);
            this.GroupBox2.Controls.Add(UserNameLabel);
            this.GroupBox2.Controls.Add(this.Label4);
            this.GroupBox2.Location = new System.Drawing.Point(6, 72);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(367, 85);
            this.GroupBox2.TabIndex = 21;
            this.GroupBox2.TabStop = false;
            // 
            // TxtEmailPassword
            // 
            this.TxtEmailPassword.Location = new System.Drawing.Point(98, 51);
            this.TxtEmailPassword.MaxLength = 50;
            this.TxtEmailPassword.Name = "TxtEmailPassword";
            this.TxtEmailPassword.PasswordChar = '*';
            this.TxtEmailPassword.Size = new System.Drawing.Size(260, 20);
            this.TxtEmailPassword.TabIndex = 1;
            this.TxtEmailPassword.TextChanged += new System.EventHandler(this.ClearErrorProvider);
            // 
            // TxtEmailUserName
            // 
            this.TxtEmailUserName.Location = new System.Drawing.Point(98, 22);
            this.TxtEmailUserName.MaxLength = 50;
            this.TxtEmailUserName.Name = "TxtEmailUserName";
            this.TxtEmailUserName.Size = new System.Drawing.Size(260, 20);
            this.TxtEmailUserName.TabIndex = 0;
            this.TxtEmailUserName.TextChanged += new System.EventHandler(this.ClearErrorProvider);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.BackColor = System.Drawing.Color.White;
            this.Label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Location = new System.Drawing.Point(6, -1);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(112, 13);
            this.Label4.TabIndex = 17;
            this.Label4.Text = "Logon Information";
            // 
            // GroupBox3
            // 
            this.GroupBox3.Controls.Add(this.TxtEmailPortNumber);
            this.GroupBox3.Controls.Add(this.TxtEmailOutgoingServer);
            this.GroupBox3.Controls.Add(this.TxtEmailIncomingServer);
            this.GroupBox3.Controls.Add(PortNumberLabel);
            this.GroupBox3.Controls.Add(OutgoingServerLabel);
            this.GroupBox3.Controls.Add(this.Label3);
            this.GroupBox3.Controls.Add(IncomingServerLabel);
            this.GroupBox3.Location = new System.Drawing.Point(6, 168);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(369, 116);
            this.GroupBox3.TabIndex = 22;
            this.GroupBox3.TabStop = false;
            // 
            // TxtEmailPortNumber
            // 
            this.TxtEmailPortNumber.Location = new System.Drawing.Point(108, 83);
            this.TxtEmailPortNumber.MaxLength = 4;
            this.TxtEmailPortNumber.Name = "TxtEmailPortNumber";
            this.TxtEmailPortNumber.ShortcutsEnabled = false;
            this.TxtEmailPortNumber.Size = new System.Drawing.Size(100, 20);
            this.TxtEmailPortNumber.TabIndex = 4;
            this.TxtEmailPortNumber.TextChanged += new System.EventHandler(this.ClearErrorProvider);
            // 
            // TxtEmailOutgoingServer
            // 
            this.TxtEmailOutgoingServer.Location = new System.Drawing.Point(108, 54);
            this.TxtEmailOutgoingServer.MaxLength = 50;
            this.TxtEmailOutgoingServer.Name = "TxtEmailOutgoingServer";
            this.TxtEmailOutgoingServer.Size = new System.Drawing.Size(252, 20);
            this.TxtEmailOutgoingServer.TabIndex = 3;
            this.TxtEmailOutgoingServer.TextChanged += new System.EventHandler(this.ClearErrorProvider);
            // 
            // TxtEmailIncomingServer
            // 
            this.TxtEmailIncomingServer.Location = new System.Drawing.Point(108, 25);
            this.TxtEmailIncomingServer.MaxLength = 50;
            this.TxtEmailIncomingServer.Name = "TxtEmailIncomingServer";
            this.TxtEmailIncomingServer.Size = new System.Drawing.Size(252, 20);
            this.TxtEmailIncomingServer.TabIndex = 2;
            this.TxtEmailIncomingServer.TextChanged += new System.EventHandler(this.ClearErrorProvider);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.BackColor = System.Drawing.Color.White;
            this.Label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(8, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(116, 13);
            this.Label3.TabIndex = 17;
            this.Label3.Text = "Server Information";
            // 
            // TpSms
            // 
            this.TpSms.Controls.Add(this.GroupBox5);
            this.TpSms.Controls.Add(this.GroupBox4);
            this.TpSms.Controls.Add(this.Label6);
            this.TpSms.Controls.Add(this.Label5);
            this.TpSms.Location = new System.Drawing.Point(4, 22);
            this.TpSms.Name = "TpSms";
            this.TpSms.Padding = new System.Windows.Forms.Padding(3);
            this.TpSms.Size = new System.Drawing.Size(381, 394);
            this.TpSms.TabIndex = 1;
            this.TpSms.Text = "SMS";
            this.TpSms.UseVisualStyleBackColor = true;
            // 
            // GroupBox5
            // 
            this.GroupBox5.Controls.Add(this.TxtSmsPortNumber);
            this.GroupBox5.Controls.Add(this.TxtSmsOutgoingserver);
            this.GroupBox5.Controls.Add(this.TxtSmsServer);
            this.GroupBox5.Controls.Add(Label13);
            this.GroupBox5.Controls.Add(Label14);
            this.GroupBox5.Controls.Add(this.Label15);
            this.GroupBox5.Controls.Add(Label16);
            this.GroupBox5.Location = new System.Drawing.Point(8, 212);
            this.GroupBox5.Name = "GroupBox5";
            this.GroupBox5.Size = new System.Drawing.Size(369, 122);
            this.GroupBox5.TabIndex = 23;
            this.GroupBox5.TabStop = false;
            // 
            // TxtSmsPortNumber
            // 
            this.TxtSmsPortNumber.Location = new System.Drawing.Point(108, 82);
            this.TxtSmsPortNumber.MaxLength = 4;
            this.TxtSmsPortNumber.Name = "TxtSmsPortNumber";
            this.TxtSmsPortNumber.ShortcutsEnabled = false;
            this.TxtSmsPortNumber.Size = new System.Drawing.Size(100, 20);
            this.TxtSmsPortNumber.TabIndex = 21;
            this.TxtSmsPortNumber.TextChanged += new System.EventHandler(this.ClearErrorProvider);
            // 
            // TxtSmsOutgoingserver
            // 
            this.TxtSmsOutgoingserver.Location = new System.Drawing.Point(108, 53);
            this.TxtSmsOutgoingserver.MaxLength = 50;
            this.TxtSmsOutgoingserver.Name = "TxtSmsOutgoingserver";
            this.TxtSmsOutgoingserver.Size = new System.Drawing.Size(252, 20);
            this.TxtSmsOutgoingserver.TabIndex = 20;
            this.TxtSmsOutgoingserver.TextChanged += new System.EventHandler(this.ClearErrorProvider);
            // 
            // TxtSmsServer
            // 
            this.TxtSmsServer.Location = new System.Drawing.Point(108, 24);
            this.TxtSmsServer.MaxLength = 50;
            this.TxtSmsServer.Name = "TxtSmsServer";
            this.TxtSmsServer.Size = new System.Drawing.Size(252, 20);
            this.TxtSmsServer.TabIndex = 19;
            this.TxtSmsServer.TextChanged += new System.EventHandler(this.ClearErrorProvider);
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.BackColor = System.Drawing.Color.White;
            this.Label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label15.Location = new System.Drawing.Point(8, -1);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(116, 13);
            this.Label15.TabIndex = 17;
            this.Label15.Text = "Server Information";
            // 
            // GroupBox4
            // 
            this.GroupBox4.Controls.Add(this.TxtSmsPassword);
            this.GroupBox4.Controls.Add(Label10);
            this.GroupBox4.Controls.Add(this.TxtSmsUsername);
            this.GroupBox4.Controls.Add(Label11);
            this.GroupBox4.Controls.Add(this.Label12);
            this.GroupBox4.Location = new System.Drawing.Point(8, 104);
            this.GroupBox4.Name = "GroupBox4";
            this.GroupBox4.Size = new System.Drawing.Size(367, 91);
            this.GroupBox4.TabIndex = 22;
            this.GroupBox4.TabStop = false;
            // 
            // TxtSmsPassword
            // 
            this.TxtSmsPassword.Location = new System.Drawing.Point(98, 51);
            this.TxtSmsPassword.MaxLength = 50;
            this.TxtSmsPassword.Name = "TxtSmsPassword";
            this.TxtSmsPassword.PasswordChar = '*';
            this.TxtSmsPassword.Size = new System.Drawing.Size(260, 20);
            this.TxtSmsPassword.TabIndex = 29;
            this.TxtSmsPassword.TextChanged += new System.EventHandler(this.ClearErrorProvider);
            // 
            // TxtSmsUsername
            // 
            this.TxtSmsUsername.Location = new System.Drawing.Point(98, 22);
            this.TxtSmsUsername.MaxLength = 50;
            this.TxtSmsUsername.Name = "TxtSmsUsername";
            this.TxtSmsUsername.Size = new System.Drawing.Size(260, 20);
            this.TxtSmsUsername.TabIndex = 28;
            this.TxtSmsUsername.TextChanged += new System.EventHandler(this.ClearErrorProvider);
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.BackColor = System.Drawing.Color.White;
            this.Label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.Location = new System.Drawing.Point(6, -1);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(112, 13);
            this.Label12.TabIndex = 17;
            this.Label12.Text = "Logon Information";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(6, 43);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(371, 13);
            this.Label6.TabIndex = 16;
            this.Label6.Text = "Each of these settings are required for your SMS sending to function properly.";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(6, 19);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(81, 13);
            this.Label5.TabIndex = 15;
            this.Label5.Text = "SMS Settings";
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(241, 432);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 6;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(322, 432);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 7;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // ErrorEmailSettings
            // 
            this.ErrorEmailSettings.ContainerControl = this;
            this.ErrorEmailSettings.RightToLeft = true;
            // 
            // FrmEmailSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 460);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.PnlEmailSettings);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEmailSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Email Settings";
            this.Load += new System.EventHandler(this.FrmEmailSettings_Load);
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.FrmEmailSettings_HelpButtonClicked);
            this.PnlEmailSettings.ResumeLayout(false);
            this.TabEmailSettings.ResumeLayout(false);
            this.TpEmail.ResumeLayout(false);
            this.TpEmail.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            this.GroupBox3.ResumeLayout(false);
            this.GroupBox3.PerformLayout();
            this.TpSms.ResumeLayout(false);
            this.TpSms.PerformLayout();
            this.GroupBox5.ResumeLayout(false);
            this.GroupBox5.PerformLayout();
            this.GroupBox4.ResumeLayout(false);
            this.GroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorEmailSettings)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PnlEmailSettings;
        internal System.Windows.Forms.TabControl TabEmailSettings;
        internal System.Windows.Forms.TabPage TpEmail;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Button BtnTestSettings;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.TextBox TxtEmailPassword;
        internal System.Windows.Forms.TextBox TxtEmailUserName;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.GroupBox GroupBox3;
        internal DemoClsDataGridview.NumericTextBox TxtEmailPortNumber;
        internal System.Windows.Forms.TextBox TxtEmailOutgoingServer;
        internal System.Windows.Forms.TextBox TxtEmailIncomingServer;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TabPage TpSms;
        internal System.Windows.Forms.GroupBox GroupBox5;
        internal DemoClsDataGridview.NumericTextBox TxtSmsPortNumber;
        internal System.Windows.Forms.TextBox TxtSmsOutgoingserver;
        internal System.Windows.Forms.TextBox TxtSmsServer;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.GroupBox GroupBox4;
        internal System.Windows.Forms.TextBox TxtSmsPassword;
        internal System.Windows.Forms.TextBox TxtSmsUsername;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.ErrorProvider ErrorEmailSettings;
    }
}