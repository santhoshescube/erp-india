﻿namespace MyBooksERP
{
    partial class frmRole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label FomDateLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRole));
            this.PanelMain = new System.Windows.Forms.Panel();
            this.chkAllPrintEmail = new System.Windows.Forms.CheckBox();
            this.chkalldelete = new System.Windows.Forms.CheckBox();
            this.chkAllupdate = new System.Windows.Forms.CheckBox();
            this.chkallview = new System.Windows.Forms.CheckBox();
            this.chkAlladd = new System.Windows.Forms.CheckBox();
            this.dgvRoleSettings = new System.Windows.Forms.DataGridView();
            this.clmRoleID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmModuleID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmMenuID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmModuleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmMenuName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmIsCreate = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clmIsView = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clmIsUpdate = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clmIsDelete = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clmIsPrintEmail = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.lstModules = new System.Windows.Forms.CheckedListBox();
            this.cboRole = new System.Windows.Forms.ComboBox();
            this.btnNewRole = new System.Windows.Forms.Button();
            this.btnBottomCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountSettingsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.btnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.toolStripStatusLabel11 = new DevComponents.DotNetBar.LabelItem();
            this.lblAccountSettingsstatus = new DevComponents.DotNetBar.LabelItem();
            this.lblStatus = new DevComponents.DotNetBar.LabelItem();
            FomDateLabel = new System.Windows.Forms.Label();
            this.PanelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoleSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccountSettingsBindingNavigator)).BeginInit();
            this.AccountSettingsBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // FomDateLabel
            // 
            FomDateLabel.AutoSize = true;
            FomDateLabel.Location = new System.Drawing.Point(3, 15);
            FomDateLabel.Name = "FomDateLabel";
            FomDateLabel.Size = new System.Drawing.Size(63, 13);
            FomDateLabel.TabIndex = 14;
            FomDateLabel.Text = "Select role :";
            // 
            // PanelMain
            // 
            this.PanelMain.Controls.Add(this.chkAllPrintEmail);
            this.PanelMain.Controls.Add(this.chkalldelete);
            this.PanelMain.Controls.Add(this.chkAllupdate);
            this.PanelMain.Controls.Add(this.chkallview);
            this.PanelMain.Controls.Add(this.chkAlladd);
            this.PanelMain.Controls.Add(this.dgvRoleSettings);
            this.PanelMain.Controls.Add(this.lstModules);
            this.PanelMain.Controls.Add(FomDateLabel);
            this.PanelMain.Controls.Add(this.cboRole);
            this.PanelMain.Controls.Add(this.btnNewRole);
            this.PanelMain.Location = new System.Drawing.Point(4, 34);
            this.PanelMain.Name = "PanelMain";
            this.PanelMain.Size = new System.Drawing.Size(780, 301);
            this.PanelMain.TabIndex = 16;
            // 
            // chkAllPrintEmail
            // 
            this.chkAllPrintEmail.AutoSize = true;
            this.chkAllPrintEmail.Location = new System.Drawing.Point(743, 54);
            this.chkAllPrintEmail.Name = "chkAllPrintEmail";
            this.chkAllPrintEmail.Size = new System.Drawing.Size(15, 14);
            this.chkAllPrintEmail.TabIndex = 21;
            this.chkAllPrintEmail.UseVisualStyleBackColor = true;
            this.chkAllPrintEmail.CheckedChanged += new System.EventHandler(this.chkAllPrintEmail_CheckedChanged);
            // 
            // chkalldelete
            // 
            this.chkalldelete.AutoSize = true;
            this.chkalldelete.Location = new System.Drawing.Point(656, 54);
            this.chkalldelete.Name = "chkalldelete";
            this.chkalldelete.Size = new System.Drawing.Size(15, 14);
            this.chkalldelete.TabIndex = 20;
            this.chkalldelete.UseVisualStyleBackColor = true;
            this.chkalldelete.CheckedChanged += new System.EventHandler(this.chkalldelete_CheckedChanged);
            // 
            // chkAllupdate
            // 
            this.chkAllupdate.AutoSize = true;
            this.chkAllupdate.Location = new System.Drawing.Point(588, 54);
            this.chkAllupdate.Name = "chkAllupdate";
            this.chkAllupdate.Size = new System.Drawing.Size(15, 14);
            this.chkAllupdate.TabIndex = 19;
            this.chkAllupdate.UseVisualStyleBackColor = true;
            this.chkAllupdate.CheckedChanged += new System.EventHandler(this.chkAllupdate_CheckedChanged);
            // 
            // chkallview
            // 
            this.chkallview.AutoSize = true;
            this.chkallview.Location = new System.Drawing.Point(508, 54);
            this.chkallview.Name = "chkallview";
            this.chkallview.Size = new System.Drawing.Size(15, 14);
            this.chkallview.TabIndex = 18;
            this.chkallview.UseVisualStyleBackColor = true;
            this.chkallview.CheckedChanged += new System.EventHandler(this.chkallview_CheckedChanged);
            // 
            // chkAlladd
            // 
            this.chkAlladd.AutoSize = true;
            this.chkAlladd.Location = new System.Drawing.Point(439, 54);
            this.chkAlladd.Name = "chkAlladd";
            this.chkAlladd.Size = new System.Drawing.Size(15, 14);
            this.chkAlladd.TabIndex = 17;
            this.chkAlladd.UseVisualStyleBackColor = true;
            this.chkAlladd.CheckedChanged += new System.EventHandler(this.chkAlladd_CheckedChanged);
            // 
            // dgvRoleSettings
            // 
            this.dgvRoleSettings.AllowUserToAddRows = false;
            this.dgvRoleSettings.AllowUserToDeleteRows = false;
            this.dgvRoleSettings.AllowUserToResizeRows = false;
            this.dgvRoleSettings.BackgroundColor = System.Drawing.Color.White;
            this.dgvRoleSettings.ColumnHeadersHeight = 30;
            this.dgvRoleSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvRoleSettings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmRoleID,
            this.clmModuleID,
            this.clmMenuID,
            this.clmModuleName,
            this.clmMenuName,
            this.clmIsCreate,
            this.clmIsView,
            this.clmIsUpdate,
            this.clmIsDelete,
            this.clmIsPrintEmail});
            this.dgvRoleSettings.Location = new System.Drawing.Point(168, 44);
            this.dgvRoleSettings.Name = "dgvRoleSettings";
            this.dgvRoleSettings.RowHeadersVisible = false;
            this.dgvRoleSettings.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvRoleSettings.Size = new System.Drawing.Size(609, 252);
            this.dgvRoleSettings.TabIndex = 15;
            this.dgvRoleSettings.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvRoleSettings_CellMouseUp);
            this.dgvRoleSettings.ColumnDividerWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dgvRoleSettings_ColumnDividerWidthChanged);
            this.dgvRoleSettings.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dgvRoleSettings_ColumnWidthChanged);
            // 
            // clmRoleID
            // 
            this.clmRoleID.HeaderText = "RoleID";
            this.clmRoleID.Name = "clmRoleID";
            this.clmRoleID.ReadOnly = true;
            this.clmRoleID.Visible = false;
            // 
            // clmModuleID
            // 
            this.clmModuleID.HeaderText = "ModuleID";
            this.clmModuleID.Name = "clmModuleID";
            this.clmModuleID.ReadOnly = true;
            this.clmModuleID.Visible = false;
            // 
            // clmMenuID
            // 
            this.clmMenuID.HeaderText = "MenuID";
            this.clmMenuID.Name = "clmMenuID";
            this.clmMenuID.ReadOnly = true;
            this.clmMenuID.Visible = false;
            // 
            // clmModuleName
            // 
            this.clmModuleName.HeaderText = "Main Menu";
            this.clmModuleName.Name = "clmModuleName";
            this.clmModuleName.ReadOnly = true;
            this.clmModuleName.Width = 110;
            // 
            // clmMenuName
            // 
            this.clmMenuName.HeaderText = "Menu Items";
            this.clmMenuName.Name = "clmMenuName";
            this.clmMenuName.ReadOnly = true;
            this.clmMenuName.Width = 120;
            // 
            // clmIsCreate
            // 
            this.clmIsCreate.HeaderText = "Add";
            this.clmIsCreate.Name = "clmIsCreate";
            this.clmIsCreate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clmIsCreate.Width = 70;
            // 
            // clmIsView
            // 
            this.clmIsView.HeaderText = "View";
            this.clmIsView.Name = "clmIsView";
            this.clmIsView.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clmIsView.Width = 70;
            // 
            // clmIsUpdate
            // 
            this.clmIsUpdate.HeaderText = "Update";
            this.clmIsUpdate.Name = "clmIsUpdate";
            this.clmIsUpdate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clmIsUpdate.Width = 70;
            // 
            // clmIsDelete
            // 
            this.clmIsDelete.HeaderText = "Delete";
            this.clmIsDelete.Name = "clmIsDelete";
            this.clmIsDelete.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clmIsDelete.Width = 70;
            // 
            // clmIsPrintEmail
            // 
            this.clmIsPrintEmail.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmIsPrintEmail.HeaderText = "PrintEmail";
            this.clmIsPrintEmail.Name = "clmIsPrintEmail";
            this.clmIsPrintEmail.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clmIsPrintEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // lstModules
            // 
            this.lstModules.CausesValidation = false;
            this.lstModules.CheckOnClick = true;
            this.lstModules.IntegralHeight = false;
            this.lstModules.Location = new System.Drawing.Point(3, 44);
            this.lstModules.Margin = new System.Windows.Forms.Padding(5);
            this.lstModules.Name = "lstModules";
            this.lstModules.Size = new System.Drawing.Size(163, 252);
            this.lstModules.TabIndex = 16;
            this.lstModules.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lstModules_ItemCheck);
            // 
            // cboRole
            // 
            this.cboRole.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRole.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRole.FormattingEnabled = true;
            this.cboRole.Location = new System.Drawing.Point(72, 12);
            this.cboRole.Name = "cboRole";
            this.cboRole.Size = new System.Drawing.Size(269, 21);
            this.cboRole.TabIndex = 1;
            this.cboRole.SelectedIndexChanged += new System.EventHandler(this.cboRole_SelectedIndexChanged);
            // 
            // btnNewRole
            // 
            this.btnNewRole.Image = global::MyBooksERP.Properties.Resources.Add;
            this.btnNewRole.Location = new System.Drawing.Point(347, 12);
            this.btnNewRole.Name = "btnNewRole";
            this.btnNewRole.Size = new System.Drawing.Size(22, 22);
            this.btnNewRole.TabIndex = 13;
            this.btnNewRole.UseVisualStyleBackColor = true;
            this.btnNewRole.Click += new System.EventHandler(this.btnNewRole_Click);
            // 
            // btnBottomCancel
            // 
            this.btnBottomCancel.Location = new System.Drawing.Point(697, 339);
            this.btnBottomCancel.Name = "btnBottomCancel";
            this.btnBottomCancel.Size = new System.Drawing.Size(75, 23);
            this.btnBottomCancel.TabIndex = 20;
            this.btnBottomCancel.Text = "&Cancel";
            this.btnBottomCancel.UseVisualStyleBackColor = true;
            this.btnBottomCancel.Click += new System.EventHandler(this.btnBottomCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(5, 339);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 19;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(618, 339);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 18;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "RoleID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "ModuleID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "MenuID";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Main Menu";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 120;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Menu Items";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 130;
            // 
            // AccountSettingsBindingNavigator
            // 
            this.AccountSettingsBindingNavigator.AddNewItem = null;
            this.AccountSettingsBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.AccountSettingsBindingNavigator.CountItem = null;
            this.AccountSettingsBindingNavigator.DeleteItem = null;
            this.AccountSettingsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSaveItem,
            this.toolStripSeparator1,
            this.toolStripButton1});
            this.AccountSettingsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.AccountSettingsBindingNavigator.MoveFirstItem = null;
            this.AccountSettingsBindingNavigator.MoveLastItem = null;
            this.AccountSettingsBindingNavigator.MoveNextItem = null;
            this.AccountSettingsBindingNavigator.MovePreviousItem = null;
            this.AccountSettingsBindingNavigator.Name = "AccountSettingsBindingNavigator";
            this.AccountSettingsBindingNavigator.PositionItem = null;
            this.AccountSettingsBindingNavigator.Size = new System.Drawing.Size(784, 25);
            this.AccountSettingsBindingNavigator.TabIndex = 23;
            this.AccountSettingsBindingNavigator.Text = "BindingNavigator1";
            // 
            // btnSaveItem
            // 
            this.btnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveItem.Image")));
            this.btnSaveItem.Name = "btnSaveItem";
            this.btnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.btnSaveItem.ToolTipText = "save";
            this.btnSaveItem.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "He&lp";
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.toolStripStatusLabel11,
            this.lblAccountSettingsstatus,
            this.lblStatus});
            this.bar1.Location = new System.Drawing.Point(0, 369);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(784, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 82;
            this.bar1.TabStop = false;
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Text = "Status:";
            // 
            // lblAccountSettingsstatus
            // 
            this.lblAccountSettingsstatus.Name = "lblAccountSettingsstatus";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            // 
            // frmRole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 388);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.AccountSettingsBindingNavigator);
            this.Controls.Add(this.btnBottomCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.PanelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRole";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Role Settings";
            this.Load += new System.EventHandler(this.frmRole_Load);
            this.PanelMain.ResumeLayout(false);
            this.PanelMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoleSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccountSettingsBindingNavigator)).EndInit();
            this.AccountSettingsBindingNavigator.ResumeLayout(false);
            this.AccountSettingsBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Panel PanelMain;
        internal System.Windows.Forms.CheckBox chkalldelete;
        internal System.Windows.Forms.CheckBox chkAllupdate;
        internal System.Windows.Forms.DataGridView dgvRoleSettings;
        internal System.Windows.Forms.CheckBox chkallview;
        internal System.Windows.Forms.CheckedListBox lstModules;
        internal System.Windows.Forms.CheckBox chkAlladd;
        internal System.Windows.Forms.ComboBox cboRole;
        internal System.Windows.Forms.Button btnNewRole;
        internal System.Windows.Forms.Button btnBottomCancel;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.CheckBox chkAllPrintEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmRoleID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmModuleID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmMenuID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmModuleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmMenuName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmIsCreate;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmIsView;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmIsUpdate;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmIsDelete;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmIsPrintEmail;
        internal System.Windows.Forms.BindingNavigator AccountSettingsBindingNavigator;
        internal System.Windows.Forms.ToolStripButton btnSaveItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton toolStripButton1;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel11;
        private DevComponents.DotNetBar.LabelItem lblAccountSettingsstatus;
        private DevComponents.DotNetBar.LabelItem lblStatus;
    }
}