﻿namespace MyBooksERP//.Settings.Forms
{
    partial class FrmAccountSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAccountSettings));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.AccountSettingsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.AccountSettingBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.CancelToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.lblTransactionType = new System.Windows.Forms.Label();
            this.lblGroup = new System.Windows.Forms.Label();
            this.lblAccount = new System.Windows.Forms.Label();
            this.GrpBxForm = new System.Windows.Forms.GroupBox();
            this.lblGRp = new System.Windows.Forms.Label();
            this.CboTransactionType = new System.Windows.Forms.ComboBox();
            this.BtnCompany = new System.Windows.Forms.Button();
            this.BtnAccount = new System.Windows.Forms.Button();
            this.DgvConfiguredAccounts = new System.Windows.Forms.DataGridView();
            this.ColSettingsID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTransactionType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAccount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblconfiguredAccount = new System.Windows.Forms.Label();
            this.CboAccount = new System.Windows.Forms.ComboBox();
            this.CboGoup = new System.Windows.Forms.ComboBox();
            this.CboCompanyName = new System.Windows.Forms.ComboBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.ErrorProviderAccountSettings = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.TmAccountSettings = new System.Windows.Forms.Timer(this.components);
            this.toolTipAccountsetting = new System.Windows.Forms.ToolTip(this.components);
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.toolStripStatusLabel11 = new DevComponents.DotNetBar.LabelItem();
            this.lblAccountSettingsstatus = new DevComponents.DotNetBar.LabelItem();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.AccountSettingsBindingNavigator)).BeginInit();
            this.AccountSettingsBindingNavigator.SuspendLayout();
            this.GrpBxForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvConfiguredAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderAccountSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // AccountSettingsBindingNavigator
            // 
            this.AccountSettingsBindingNavigator.AddNewItem = null;
            this.AccountSettingsBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.AccountSettingsBindingNavigator.CountItem = null;
            this.AccountSettingsBindingNavigator.DeleteItem = null;
            this.AccountSettingsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorDeleteItem,
            this.AccountSettingBindingNavigatorSaveItem,
            this.CancelToolStripButton,
            this.BtnPrint,
            this.BtnEmail,
            this.toolStripSeparator1,
            this.BtnHelp});
            this.AccountSettingsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.AccountSettingsBindingNavigator.MoveFirstItem = null;
            this.AccountSettingsBindingNavigator.MoveLastItem = null;
            this.AccountSettingsBindingNavigator.MoveNextItem = null;
            this.AccountSettingsBindingNavigator.MovePreviousItem = null;
            this.AccountSettingsBindingNavigator.Name = "AccountSettingsBindingNavigator";
            this.AccountSettingsBindingNavigator.PositionItem = null;
            this.AccountSettingsBindingNavigator.Size = new System.Drawing.Size(508, 25);
            this.AccountSettingsBindingNavigator.TabIndex = 3;
            this.AccountSettingsBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.ToolTipText = "Add new setting";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.ToolTipText = "Delete selected setting";
            this.BindingNavigatorDeleteItem.Visible = false;
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // AccountSettingBindingNavigatorSaveItem
            // 
            this.AccountSettingBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("AccountSettingBindingNavigatorSaveItem.Image")));
            this.AccountSettingBindingNavigatorSaveItem.Name = "AccountSettingBindingNavigatorSaveItem";
            this.AccountSettingBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.AccountSettingBindingNavigatorSaveItem.ToolTipText = "save";
            this.AccountSettingBindingNavigatorSaveItem.Click += new System.EventHandler(this.AccountSettingBindingNavigatorSaveItem_Click);
            // 
            // CancelToolStripButton
            // 
            this.CancelToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CancelToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelToolStripButton.Image")));
            this.CancelToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CancelToolStripButton.Name = "CancelToolStripButton";
            this.CancelToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.CancelToolStripButton.ToolTipText = "Clear";
            this.CancelToolStripButton.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.ToolTipText = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.AutoSize = true;
            this.lblCompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyName.Location = new System.Drawing.Point(12, 23);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(51, 13);
            this.lblCompanyName.TabIndex = 1;
            this.lblCompanyName.Text = "Company";
            // 
            // lblTransactionType
            // 
            this.lblTransactionType.AutoSize = true;
            this.lblTransactionType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransactionType.Location = new System.Drawing.Point(12, 50);
            this.lblTransactionType.Name = "lblTransactionType";
            this.lblTransactionType.Size = new System.Drawing.Size(90, 13);
            this.lblTransactionType.TabIndex = 2;
            this.lblTransactionType.Text = "Transaction Type";
            // 
            // lblGroup
            // 
            this.lblGroup.AutoSize = true;
            this.lblGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGroup.Location = new System.Drawing.Point(12, 78);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(36, 13);
            this.lblGroup.TabIndex = 3;
            this.lblGroup.Text = "Group";
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccount.Location = new System.Drawing.Point(12, 106);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(47, 13);
            this.lblAccount.TabIndex = 4;
            this.lblAccount.Text = "Account";
            // 
            // GrpBxForm
            // 
            this.GrpBxForm.BackColor = System.Drawing.Color.Transparent;
            this.GrpBxForm.Controls.Add(this.lblGRp);
            this.GrpBxForm.Controls.Add(this.CboTransactionType);
            this.GrpBxForm.Controls.Add(this.BtnCompany);
            this.GrpBxForm.Controls.Add(this.BtnAccount);
            this.GrpBxForm.Controls.Add(this.DgvConfiguredAccounts);
            this.GrpBxForm.Controls.Add(this.btnSave);
            this.GrpBxForm.Controls.Add(this.lblconfiguredAccount);
            this.GrpBxForm.Controls.Add(this.CboAccount);
            this.GrpBxForm.Controls.Add(this.CboGoup);
            this.GrpBxForm.Controls.Add(this.CboCompanyName);
            this.GrpBxForm.Controls.Add(this.lblCompanyName);
            this.GrpBxForm.Controls.Add(this.lblTransactionType);
            this.GrpBxForm.Controls.Add(this.lblGroup);
            this.GrpBxForm.Controls.Add(this.lblAccount);
            this.GrpBxForm.Controls.Add(this.shapeContainer1);
            this.GrpBxForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrpBxForm.Location = new System.Drawing.Point(6, 28);
            this.GrpBxForm.Name = "GrpBxForm";
            this.GrpBxForm.Size = new System.Drawing.Size(497, 320);
            this.GrpBxForm.TabIndex = 0;
            this.GrpBxForm.TabStop = false;
            // 
            // lblGRp
            // 
            this.lblGRp.AutoSize = true;
            this.lblGRp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGRp.Location = new System.Drawing.Point(4, -1);
            this.lblGRp.Name = "lblGRp";
            this.lblGRp.Size = new System.Drawing.Size(160, 13);
            this.lblGRp.TabIndex = 0;
            this.lblGRp.Text = "Configure Default Accounts";
            // 
            // CboTransactionType
            // 
            this.CboTransactionType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboTransactionType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboTransactionType.BackColor = System.Drawing.SystemColors.Info;
            this.CboTransactionType.DropDownHeight = 134;
            this.CboTransactionType.FormattingEnabled = true;
            this.CboTransactionType.IntegralHeight = false;
            this.CboTransactionType.Location = new System.Drawing.Point(117, 46);
            this.CboTransactionType.MaxDropDownItems = 10;
            this.CboTransactionType.Name = "CboTransactionType";
            this.CboTransactionType.Size = new System.Drawing.Size(228, 21);
            this.CboTransactionType.TabIndex = 2;
            this.toolTipAccountsetting.SetToolTip(this.CboTransactionType, "Select transaction type");
            this.CboTransactionType.SelectedIndexChanged += new System.EventHandler(this.CboTransactionType_SelectedIndexChanged);
            this.CboTransactionType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_KeyDown);
            // 
            // BtnCompany
            // 
            this.BtnCompany.Location = new System.Drawing.Point(430, 18);
            this.BtnCompany.Name = "BtnCompany";
            this.BtnCompany.Size = new System.Drawing.Size(32, 23);
            this.BtnCompany.TabIndex = 1;
            this.BtnCompany.Text = "...";
            this.toolTipAccountsetting.SetToolTip(this.BtnCompany, "Add  new company");
            this.BtnCompany.UseVisualStyleBackColor = true;
            this.BtnCompany.Click += new System.EventHandler(this.BtnCompany_Click);
            // 
            // BtnAccount
            // 
            this.BtnAccount.Location = new System.Drawing.Point(353, 102);
            this.BtnAccount.Name = "BtnAccount";
            this.BtnAccount.Size = new System.Drawing.Size(32, 23);
            this.BtnAccount.TabIndex = 5;
            this.BtnAccount.Text = "...";
            this.toolTipAccountsetting.SetToolTip(this.BtnAccount, "Add new account");
            this.BtnAccount.UseVisualStyleBackColor = true;
            this.BtnAccount.Click += new System.EventHandler(this.BtnAccount_Click);
            // 
            // DgvConfiguredAccounts
            // 
            this.DgvConfiguredAccounts.AllowUserToAddRows = false;
            this.DgvConfiguredAccounts.AllowUserToDeleteRows = false;
            this.DgvConfiguredAccounts.AllowUserToResizeColumns = false;
            this.DgvConfiguredAccounts.AllowUserToResizeRows = false;
            this.DgvConfiguredAccounts.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvConfiguredAccounts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DgvConfiguredAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvConfiguredAccounts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColSettingsID,
            this.ColTransactionType,
            this.ColGroup,
            this.ColAccount});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvConfiguredAccounts.DefaultCellStyle = dataGridViewCellStyle2;
            this.DgvConfiguredAccounts.Location = new System.Drawing.Point(5, 148);
            this.DgvConfiguredAccounts.MultiSelect = false;
            this.DgvConfiguredAccounts.Name = "DgvConfiguredAccounts";
            this.DgvConfiguredAccounts.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvConfiguredAccounts.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DgvConfiguredAccounts.RowHeadersVisible = false;
            this.DgvConfiguredAccounts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvConfiguredAccounts.Size = new System.Drawing.Size(487, 164);
            this.DgvConfiguredAccounts.TabIndex = 7;
            this.DgvConfiguredAccounts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvConfiguredAccounts_CellClick);
            this.DgvConfiguredAccounts.CurrentCellDirtyStateChanged += new System.EventHandler(this.DgvConfiguredAccounts_CurrentCellDirtyStateChanged);
            // 
            // ColSettingsID
            // 
            this.ColSettingsID.HeaderText = "Settings ID";
            this.ColSettingsID.Name = "ColSettingsID";
            this.ColSettingsID.ReadOnly = true;
            this.ColSettingsID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColSettingsID.Visible = false;
            // 
            // ColTransactionType
            // 
            this.ColTransactionType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColTransactionType.HeaderText = "Transaction Type";
            this.ColTransactionType.Name = "ColTransactionType";
            this.ColTransactionType.ReadOnly = true;
            this.ColTransactionType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColGroup
            // 
            this.ColGroup.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColGroup.HeaderText = "Group";
            this.ColGroup.Name = "ColGroup";
            this.ColGroup.ReadOnly = true;
            this.ColGroup.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColAccount
            // 
            this.ColAccount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColAccount.HeaderText = "Account";
            this.ColAccount.Name = "ColAccount";
            this.ColAccount.ReadOnly = true;
            this.ColAccount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(413, 102);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "&Save";
            this.toolTipAccountsetting.SetToolTip(this.btnSave, "Save");
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblconfiguredAccount
            // 
            this.lblconfiguredAccount.AutoSize = true;
            this.lblconfiguredAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblconfiguredAccount.Location = new System.Drawing.Point(4, 130);
            this.lblconfiguredAccount.Name = "lblconfiguredAccount";
            this.lblconfiguredAccount.Size = new System.Drawing.Size(156, 13);
            this.lblconfiguredAccount.TabIndex = 49;
            this.lblconfiguredAccount.Text = "View Configured Accounts";
            // 
            // CboAccount
            // 
            this.CboAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboAccount.BackColor = System.Drawing.SystemColors.Info;
            this.CboAccount.DropDownHeight = 134;
            this.CboAccount.FormattingEnabled = true;
            this.CboAccount.IntegralHeight = false;
            this.CboAccount.Location = new System.Drawing.Point(117, 102);
            this.CboAccount.MaxDropDownItems = 10;
            this.CboAccount.Name = "CboAccount";
            this.CboAccount.Size = new System.Drawing.Size(228, 21);
            this.CboAccount.TabIndex = 4;
            this.toolTipAccountsetting.SetToolTip(this.CboAccount, "select account ");
            this.CboAccount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_KeyDown);
            // 
            // CboGoup
            // 
            this.CboGoup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboGoup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboGoup.BackColor = System.Drawing.SystemColors.Info;
            this.CboGoup.DropDownHeight = 134;
            this.CboGoup.Enabled = false;
            this.CboGoup.FormattingEnabled = true;
            this.CboGoup.IntegralHeight = false;
            this.CboGoup.Location = new System.Drawing.Point(117, 74);
            this.CboGoup.MaxDropDownItems = 10;
            this.CboGoup.Name = "CboGoup";
            this.CboGoup.Size = new System.Drawing.Size(228, 21);
            this.CboGoup.TabIndex = 3;
            this.toolTipAccountsetting.SetToolTip(this.CboGoup, "select group");
            this.CboGoup.SelectedIndexChanged += new System.EventHandler(this.CboGoup_SelectedIndexChanged);
            this.CboGoup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_KeyDown);
            // 
            // CboCompanyName
            // 
            this.CboCompanyName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompanyName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompanyName.BackColor = System.Drawing.SystemColors.Info;
            this.CboCompanyName.DropDownHeight = 134;
            this.CboCompanyName.FormattingEnabled = true;
            this.CboCompanyName.IntegralHeight = false;
            this.CboCompanyName.Location = new System.Drawing.Point(117, 19);
            this.CboCompanyName.MaxDropDownItems = 10;
            this.CboCompanyName.Name = "CboCompanyName";
            this.CboCompanyName.Size = new System.Drawing.Size(307, 21);
            this.CboCompanyName.TabIndex = 0;
            this.toolTipAccountsetting.SetToolTip(this.CboCompanyName, "Enter company name");
            this.CboCompanyName.SelectedIndexChanged += new System.EventHandler(this.CboCompanyName_SelectedIndexChanged);
            this.CboCompanyName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_KeyDown);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(491, 301);
            this.shapeContainer1.TabIndex = 5;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 129;
            this.lineShape1.X2 = 484;
            this.lineShape1.Y1 = 123;
            this.lineShape1.Y2 = 123;
            // 
            // ErrorProviderAccountSettings
            // 
            this.ErrorProviderAccountSettings.ContainerControl = this;
            this.ErrorProviderAccountSettings.RightToLeft = true;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(342, 354);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&Ok";
            this.toolTipAccountsetting.SetToolTip(this.btnOk, "Save and close");
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(423, 354);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.toolTipAccountsetting.SetToolTip(this.btnCancel, "Close");
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.toolStripStatusLabel11,
            this.lblAccountSettingsstatus});
            this.bar1.Location = new System.Drawing.Point(0, 383);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(508, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 81;
            this.bar1.TabStop = false;
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Text = "Status:";
            // 
            // lblAccountSettingsstatus
            // 
            this.lblAccountSettingsstatus.Name = "lblAccountSettingsstatus";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Settings ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Transaction Type";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "Group";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.HeaderText = "Account";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FrmAccountSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 402);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.GrpBxForm);
            this.Controls.Add(this.AccountSettingsBindingNavigator);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAccountSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Account Settings";
            this.Load += new System.EventHandler(this.FrmAccountSettings_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAccountSettings_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmAccountSettings_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.AccountSettingsBindingNavigator)).EndInit();
            this.AccountSettingsBindingNavigator.ResumeLayout(false);
            this.AccountSettingsBindingNavigator.PerformLayout();
            this.GrpBxForm.ResumeLayout(false);
            this.GrpBxForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvConfiguredAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderAccountSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator AccountSettingsBindingNavigator;
        internal System.Windows.Forms.ToolStripButton AccountSettingBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton CancelToolStripButton;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        private System.Windows.Forms.Label lblCompanyName;
        private System.Windows.Forms.Label lblTransactionType;
        private System.Windows.Forms.Label lblGroup;
        private System.Windows.Forms.Label lblAccount;
        private System.Windows.Forms.GroupBox GrpBxForm;
        internal System.Windows.Forms.ComboBox CboAccount;
        internal System.Windows.Forms.ComboBox CboGoup;
        internal System.Windows.Forms.ComboBox CboCompanyName;
        private System.Windows.Forms.Label lblconfiguredAccount;
        private System.Windows.Forms.DataGridView DgvConfiguredAccounts;
        private System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.ErrorProvider ErrorProviderAccountSettings;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button BtnCompany;
        internal System.Windows.Forms.Button BtnAccount;
        internal System.Windows.Forms.ComboBox CboTransactionType;
        private System.Windows.Forms.Timer TmAccountSettings;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.ToolTip toolTipAccountsetting;
        private System.Windows.Forms.Label lblGRp;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel11;
        private DevComponents.DotNetBar.LabelItem lblAccountSettingsstatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColSettingsID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTransactionType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAccount;

    }
}