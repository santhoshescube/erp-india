﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;



namespace MyBooksERP
{
    /*
    =====================================================
    Author:<Author,,Amal Raj> 
    Create Date:<Create Date,,07 June 2011> 
    Description:<Description,,Terms And Condition Forms>
    =====================================================
   */
    public partial class FrmTermsAndConditions : DevComponents.DotNetBar.Office2007Form
    {
        #region Variable Declaration

        private bool MblnChangeStatus = false;          //To Specify Change Status
        private bool MblnAddStatus;                     //To Specify Add/Update mode                 
        private int MintTermsID;
        private int MintTypeID;
        private int MintCompanyID;
        private int MiTimerInterval;
        public int operationType;

        private string MstrMessageCaption;
        private string MstrMessageCommon;
        private ArrayList MaMessageArr;                 // Error Message display
        private MessageBoxIcon MmessageIcon;

        ClsLogWriter MobjClsLogWriter;
        ClsNotification MObjClsNotification;
        ClsBLLTermsAndCondition MobjClsBLLTermsAndCondition;
        clsDTOTermsAndCond objclsDTOTermsAndCond;
        bool MblnAddPermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission = false;

        #endregion

        # region Constructor
        public FrmTermsAndConditions()
        {
            InitializeComponent();
            MiTimerInterval = tmTermsAndCond.Interval = ClsCommonSettings.TimerInterval;  // Setting timer interval
            MstrMessageCaption = ClsCommonSettings.MessageCaption;      //Message Caption
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification();

            MmessageIcon = MessageBoxIcon.Information;
            MobjClsBLLTermsAndCondition = new ClsBLLTermsAndCondition();
            objclsDTOTermsAndCond = new clsDTOTermsAndCond();
        }
        #endregion Constructor

        private void FrmTermsAndConditions_Load(object sender, EventArgs e)
        {
            try
            {              
                MblnChangeStatus = false;
                MblnAddStatus = true;
                SetPermissions();
                LoadMessage();
                LoadCombos(0);
                AddNewTAC();
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on FrmTermsAndConditions:FrmTermsAndConditions_Load" + this.Name + "" + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                {
                    MessageBox.Show("Error on FrmTermsAndConditions_Load " + Ex.Message.ToString());
                }
            }
        }       

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!TermsAndConditionsValidation())
                return;
                if(!Save())
                    return;
            if (sender.GetType() == typeof(Button))
            {
                if (((Button)sender).Name == btnOk.Name)
                {
                    MblnChangeStatus = false;
                    Close();
                }
            }
        }   

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tmTermsAndCond_Tick(object sender, EventArgs e)
        {
            tmTermsAndCond.Enabled = false;
            lblTACStatus.Text = "";
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.General, (Int32)eMenuID.TermsAndConditions, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void FrmTermsAndConditions_FormClosing(object sender, FormClosingEventArgs e)
         {
            if (MblnChangeStatus)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    MObjClsNotification = null;
                    MobjClsBLLTermsAndCondition = null;
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void ChangeStatus()
        {
            MblnChangeStatus = true;
            if (MblnAddStatus == true)
            {
                btnOk.Enabled = btnSave.Enabled = TermsAndCondBindingNavigatorSaveItem.Enabled = BtnClear.Enabled = MblnAddPermission;
            }
            else
            {
                btnOk.Enabled = btnSave.Enabled = TermsAndCondBindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                BtnClear.Enabled= MblnUpdatePermission;
            }
            BindingNavigatorAddNewTerms.Enabled = MblnAddPermission;
            errorProviderTermsAndCond.Clear();
        }

        private void ChangeStatus(object sender, EventArgs e)
        {
            errorProviderTermsAndCond.Clear();
            ChangeStatus();
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            MblnAddStatus = true;
            rtxtDescInEng.Text = "";
            rtxtDescInArb.Text = "";
            DgvTermsAndCond.ClearSelection();
        }

        private void TermsAndCondBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (TermsAndConditionsValidation())  
                Save();
        }

        private void DgvTermsAndCond_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
                if (DgvTermsAndCond.CurrentRow == null || e.RowIndex==-1)
                    return;
                MblnAddStatus = false;
                rtxtDescInEng.Text = DgvTermsAndCond.Rows[e.RowIndex].Cells["DescriptionEnglish"].Value.ToString();
                rtxtDescInArb.Text = DgvTermsAndCond.Rows[e.RowIndex].Cells["DescriptionArabic"].Value.ToString();
                CboCompany.SelectedValue = DgvTermsAndCond.Rows[e.RowIndex].Cells["CompanyID"].Value.ToString();
                cboOperationType.SelectedValue = DgvTermsAndCond.Rows[e.RowIndex].Cells["operationTypeID"].Value.ToString();
                MintTypeID = Convert.ToInt32(DgvTermsAndCond.CurrentRow.Cells["operationTypeID"].Value); 
                MintTermsID = Convert.ToInt32(DgvTermsAndCond.CurrentRow.Cells["TermsID"].Value);
                BindingNavigatorAddNewTerms.Enabled = MblnAddPermission;
                ChangeStatus();
                showprintemail();
                bnDeleteItem.Enabled = MblnDeletePermission;
         }

        private void cboOperationType_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void cboOperationType_Enter(object sender, EventArgs e)
        {
            //cboOperationType.Enabled = false;
            //cboOperationType.Enabled = true;
        }

        private void rtxtDescInEng_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void rtxtDescInArb_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void BindingNavigatorAddNewTerms_Click(object sender, EventArgs e)
        {
            AddNewTAC();
        }

        private void DgvTermsAndCond_CurrentCellDirtyStateChanged_1(object sender, EventArgs e)
        {
            try
            {
                if (DgvTermsAndCond.IsCurrentCellDirty)
                {
                    if (DgvTermsAndCond.CurrentCell != null)
                    {
                        DgvTermsAndCond.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on DgvTermsAndCond_CurrentCellDirtyStateChanged: " + this.Name + "" + Ex.Message.ToString(), 2);
            }
        }
        
        private void bnPrint_Click(object sender, EventArgs e)
        {
           //printing
            try
            {
                LoadReport();
            }
            catch (Exception ex)
            {
             MobjClsLogWriter.WriteLog("Error on BtnPrint_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnPrint_Click" + ex.Message.ToString());
           }
        }

        private void bnEmail_Click(object sender, EventArgs e) //Email
        {
            int intOperationType = Convert.ToInt32(cboOperationType.SelectedValue);
            int intCompanyID = Convert.ToInt32(CboCompany.SelectedValue);
            try
            {
             using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
               {
                   ObjEmailPopUp.MsSubject = "Terms And Conditions";
                   ObjEmailPopUp.EmailFormType = EmailFormID.TermsAndConditions;
                   ObjEmailPopUp.EmailSource = MobjClsBLLTermsAndCondition.GetTermsAndConditionsReport(intOperationType, intCompanyID);
                   ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception ex)
            {
             MobjClsLogWriter.WriteLog("Error on bnEmail_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on bnEmail_Click" + ex.Message.ToString());
           }
        }
        /////--------------------------------------------------------FUNCTIONS-----------------------------------------------------------/////

        private bool Save()
        {
              if (SaveTermsAndConditions())
              {
                 if (MblnAddStatus == true)
                 {
                     MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                     MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                     lblTACStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                     tmTermsAndCond.Enabled = true;
                 }
                 else
                 {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblTACStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmTermsAndCond.Enabled = true;
                 }
                    
                 btnOk.Enabled = false;
                 BindingNavigatorAddNewTerms.Enabled = false;
                 rtxtDescInEng.Text = null;
                 rtxtDescInArb.Text = null;
                 //AddNewTAC();
                 GenerateTermsAndConditionGrid();
                 btnSave.Enabled = false;
                 TermsAndCondBindingNavigatorSaveItem.Enabled = false;
                 CboCompany.Focus();
                 return true;
              }
              return false;
        }

        private void LoadMessage()
        {
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.TermsAndConditions, (int)ClsCommonSettings.ProductID);            
        }

        private bool LoadCombos(int intType)
        {
            // function for loading combo
            // intType = 0 - For Loading Operation Type Combo
            try
            {        
                DataTable datCombos = new DataTable();
                if (intType == 0 || intType == 1)
                {
                    datCombos = MobjClsBLLTermsAndCondition.FillCombos(new string[] { "OperationTypeID,OperationType", "OperationTypeReference", "OperationTypeID in  (2, 3, 5, 7, 8, 9, 10, 11, 19, 21, 22, 24)" });
                    cboOperationType.ValueMember = "OperationTypeID";
                    cboOperationType.DisplayMember = "OperationType";
                    cboOperationType.DataSource = datCombos;
                }
                if (intType == 0 || intType == 2)
                {
                    datCombos = MobjClsBLLTermsAndCondition.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                    CboCompany.ValueMember = "CompanyID";
                    CboCompany.DisplayMember = "CompanyName";
                    CboCompany.DataSource = datCombos;
                }
                MobjClsLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                {
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
                }
                return false;
            }
        }

        //Validation for Saving
        private bool TermsAndConditionsValidation()
        {
            lblTACStatus.Text = "";
            if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 4404, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errorProviderTermsAndCond.SetError(CboCompany, MstrMessageCommon.Replace("#", "").Trim());
                lblTACStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmTermsAndCond.Enabled = true;
                CboCompany.Focus();
                return false;
            }
            if (Convert.ToInt32(cboOperationType.SelectedValue) == 0)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 4402, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errorProviderTermsAndCond.SetError(cboOperationType, MstrMessageCommon.Replace("#", "").Trim());
                lblTACStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmTermsAndCond.Enabled = true;
                cboOperationType.Focus();
                return false;
            }
            if (rtxtDescInEng.Text.Trim() == "")
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 4403, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errorProviderTermsAndCond.SetError(rtxtDescInEng, MstrMessageCommon.Replace("#", "").Trim());
                lblTACStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmTermsAndCond.Enabled = true;
                rtxtDescInEng.Focus();
                return false;
            }
            return true;
        }

        private void AddNewTAC()
        {
            try
            {
                MblnAddStatus = true;
                DgvTermsAndCond.ClearSelection();
                errorProviderTermsAndCond.Clear();
                tmTermsAndCond.Enabled = true;
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 4400, out MmessageIcon);
                lblTACStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                EnableDisableButtons(true);
                CboCompany.SelectedIndex = 0;
                cboOperationType.SelectedIndex = 0;
                GenerateTermsAndConditionGrid();
                rtxtDescInArb.Text = "";
                rtxtDescInEng.Text = "";
                bnPrint.Enabled = false;
                bnEmail.Enabled = false;
                BtnClear.Enabled = false;
                bnDeleteItem.Enabled = false;
                CboCompany.Focus();
                BindingNavigatorAddNewTerms.Enabled = false;
                TermsAndCondBindingNavigatorSaveItem.Enabled = false;
                btnOk.Enabled = false;
                btnSave.Enabled = false;
                bnPrint.Enabled = false;
                bnEmail.Enabled = false;
                MblnChangeStatus = false;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form AddNewTermsAndConditions:AddNewTermsAndConditions()" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                {
                    MessageBox.Show("Error on AddNewTermsAndConditions " + Ex.Message.ToString());
                }
            }
        }

        private void EnableDisableButtons(bool bEnable)
        {
            if (bEnable)
            {
                BindingNavigatorAddNewTerms.Enabled = !bEnable;
                bnDeleteItem.Enabled = !bEnable;
            }
            else
            {
                BindingNavigatorAddNewTerms.Enabled = bEnable;
                bnDeleteItem.Enabled = bEnable;
            }
            if (bEnable)
            {
                btnOk.Enabled = !bEnable;
                btnSave.Enabled = !bEnable;
                MblnChangeStatus = !bEnable;
            }
            else
            {
                btnOk.Enabled = bEnable;
                btnSave.Enabled = bEnable;
                MblnChangeStatus = bEnable;
            }
        }

      
        private void ClearAllControls() //Clear All Controls
        {
            rtxtDescInArb.Clear();
            rtxtDescInEng.Clear();
            DgvTermsAndCond.ClearSelection();
            if (DgvTermsAndCond.Rows.Count > 0)
                DgvTermsAndCond.Rows.Clear();
            errorProviderTermsAndCond.Clear();
        }

        private void setEnableDisable(bool bEnable)
        {
            if (bEnable)
            {
                BindingNavigatorAddNewTerms.Enabled = !bEnable;
            }
            else
            {
                BindingNavigatorAddNewTerms.Enabled = bEnable;
            }
            if (bEnable)
            {
                //TermsAndCondBindingNavigatorSaveItem.Enabled = !bEnable;
                //btnOk.Enabled = !bEnable;
                //btnSave.Enabled = !bEnable;
                MblnChangeStatus = !bEnable;
            }
            else
            {
                //TermsAndCondBindingNavigatorSaveItem.Enabled = bEnable;
                //btnOk.Enabled = bEnable;
                //btnSave.Enabled = bEnable;
                MblnChangeStatus = bEnable;
            }
        }
      
        private bool SaveTermsAndConditions()
        {
            try
            {
                if (!TermsAndConditionsValidation())
                {
                    return false;
                }
                if (MblnAddStatus == true)
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);
                    MobjClsBLLTermsAndCondition.clsDTOTermsAndCond.intTermsID = 0;
                }
                else
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
                    MobjClsBLLTermsAndCondition.clsDTOTermsAndCond.intTermsID = MintTermsID;
                }
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    return false;
                }
                MobjClsBLLTermsAndCondition.clsDTOTermsAndCond.intCompanyID = Convert.ToInt32(CboCompany.SelectedValue);
                MobjClsBLLTermsAndCondition.clsDTOTermsAndCond.intTypeID = Convert.ToInt32(cboOperationType.SelectedValue);
                MobjClsBLLTermsAndCondition.clsDTOTermsAndCond.strDescriptionEnglish = rtxtDescInEng.Text.Replace("'", "").Trim();
                MobjClsBLLTermsAndCondition.clsDTOTermsAndCond.strDescriptionArabic = rtxtDescInArb.Text.Replace("'", "").Trim();
                if (MobjClsBLLTermsAndCondition.SaveTermsAndConditions(MblnAddStatus))
                {
                    MobjClsBLLTermsAndCondition.clsDTOTermsAndCond.intTermsID = MintTermsID;
                    MobjClsLogWriter.WriteLog("Saved successfully:SaveTermsAndConditions()  " + this.Name + "", 0);
                    return true;
                }
                return false;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on Form Save():SaveTermsAndConditions" + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                {
                    MessageBox.Show("Error on SaveTermsAndConditions()" + Ex.Message.ToString());
                }
                return false;
            }
        }

        private void showprintemail()
        {
            if (DgvTermsAndCond.RowCount > 0)
            {
                bnPrint.Enabled = MblnPrintEmailPermission;
                bnEmail.Enabled = MblnPrintEmailPermission;
            }
            else
            {
                bnPrint.Enabled = false;
                bnEmail.Enabled = false;
            }
        }

        private void LoadReport()
        {
            try
            {
                operationType = Convert.ToInt32(cboOperationType.SelectedValue);
                MintCompanyID = Convert.ToInt32(CboCompany.SelectedValue);
                if (operationType > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PintoperationType = operationType;
                    ObjViewer.PintCompany = MintCompanyID;
                    ObjViewer.PiFormID = (int)FormID.TermsAndConditions;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmBankDetails:LoadReport " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport() " + Ex.Message.ToString());
            }
        }
        private void bnDeleteItem_Click(object sender, EventArgs e)
        {
             MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr,13, out MmessageIcon);
             if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                 return;
             MintTermsID = Convert.ToInt32(DgvTermsAndCond.CurrentRow.Cells["TermsID"].Value);
             if (MobjClsBLLTermsAndCondition.DeleteTAC(MintTermsID))
                 MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr,4, out MmessageIcon);
                 MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                 lblTACStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                 GenerateTermsAndConditionGrid();
                 //AddNewTAC();
        }

        private void CboCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (Convert.ToInt32(CboCompany.SelectedValue) > 0)
            {
               GenerateTermsAndConditionGrid();
            }
            ChangeStatus();
        }
        private void GenerateTermsAndConditionGrid()
        {
            ClearAllControls();
            MintCompanyID = Convert.ToInt32(CboCompany.SelectedValue);
            if(Convert.ToInt32(cboOperationType.SelectedValue) > 0)
                MintTypeID = Convert.ToInt32(cboOperationType.SelectedValue);
            else
                MintTypeID = 0;
            DataTable dtTAC = MobjClsBLLTermsAndCondition.GetTermsAndCondn(MintTypeID, MintCompanyID);
            for (int intICounter = 0; intICounter < dtTAC.Rows.Count; intICounter++)
            {
                DgvTermsAndCond.Rows.Add();
                DgvTermsAndCond.Rows[intICounter].Cells["SINo"].Value = (intICounter + 1).ToString();
                DgvTermsAndCond.Rows[intICounter].Cells["descriptionEnglish"].Value = dtTAC.Rows[intICounter][1].ToString();
                DgvTermsAndCond.Rows[intICounter].Cells["descriptionArabic"].Value = dtTAC.Rows[intICounter][2].ToString();
                DgvTermsAndCond.Rows[intICounter].Cells["operationTypeID"].Value = dtTAC.Rows[intICounter][0].ToString();
                DgvTermsAndCond.Rows[intICounter].Cells["operationTypeDescription"].Value = dtTAC.Rows[intICounter][5].ToString();
                DgvTermsAndCond.Rows[intICounter].Cells["TermsID"].Value = dtTAC.Rows[intICounter][3].ToString();
                DgvTermsAndCond.Rows[intICounter].Cells["CompanyID"].Value = dtTAC.Rows[intICounter][4].ToString();
                DgvTermsAndCond.Columns[0].Width = 35;
            }
            MblnAddStatus = true;
        }

        private void cboOperationType_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        private void CboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboCompany.SelectedValue != null && CboCompany.SelectedValue.ToInt32() > 0)
            { 
                MintCompanyID = Convert.ToInt32(CboCompany.SelectedValue);
                GenerateTermsAndConditionGrid();
            }
        }

        private void cboOperationType_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(CboCompany.SelectedValue) > 0)
            {
                GenerateTermsAndConditionGrid();
            }
        }
   }
}



