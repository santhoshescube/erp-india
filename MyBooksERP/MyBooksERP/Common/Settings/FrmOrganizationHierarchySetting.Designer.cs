﻿namespace MyBooksERP
{
    partial class FrmOrganizationHierarchySetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOrganizationHierarchySetting));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Users");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("WorkFlow");
            this.ConfigurationSettingBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BtnDelete = new System.Windows.Forms.ToolStripButton();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.pnlConfigSetting = new System.Windows.Forms.Panel();
            this.tvwUsers = new System.Windows.Forms.TreeView();
            this.label2 = new System.Windows.Forms.Label();
            this.tvwHierarchy = new System.Windows.Forms.TreeView();
            this.btnRemoveAll = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SStrHierarchySettings = new System.Windows.Forms.StatusStrip();
            this.StatusLabelHierarchySettings = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblHierarchySettingsstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.TmHierarchySettings = new System.Windows.Forms.Timer(this.components);
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.imgLstWorkFlow = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ConfigurationSettingBindingNavigator)).BeginInit();
            this.ConfigurationSettingBindingNavigator.SuspendLayout();
            this.pnlConfigSetting.SuspendLayout();
            this.SStrHierarchySettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // ConfigurationSettingBindingNavigator
            // 
            this.ConfigurationSettingBindingNavigator.AddNewItem = null;
            this.ConfigurationSettingBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.ConfigurationSettingBindingNavigator.CountItem = null;
            this.ConfigurationSettingBindingNavigator.DeleteItem = null;
            this.ConfigurationSettingBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorSaveItem,
            this.BtnDelete,
            this.BtnHelp});
            this.ConfigurationSettingBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.ConfigurationSettingBindingNavigator.MoveFirstItem = null;
            this.ConfigurationSettingBindingNavigator.MoveLastItem = null;
            this.ConfigurationSettingBindingNavigator.MoveNextItem = null;
            this.ConfigurationSettingBindingNavigator.MovePreviousItem = null;
            this.ConfigurationSettingBindingNavigator.Name = "ConfigurationSettingBindingNavigator";
            this.ConfigurationSettingBindingNavigator.PositionItem = null;
            this.ConfigurationSettingBindingNavigator.Size = new System.Drawing.Size(551, 25);
            this.ConfigurationSettingBindingNavigator.TabIndex = 37;
            this.ConfigurationSettingBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(51, 22);
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnDelete.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.BtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(23, 22);
            this.BtnDelete.Text = "toolStripButton1";
            this.BtnDelete.Click += new System.EventHandler(this.btnRemoveAll_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            // 
            // pnlConfigSetting
            // 
            this.pnlConfigSetting.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlConfigSetting.Controls.Add(this.tvwUsers);
            this.pnlConfigSetting.Controls.Add(this.label2);
            this.pnlConfigSetting.Controls.Add(this.tvwHierarchy);
            this.pnlConfigSetting.Location = new System.Drawing.Point(8, 29);
            this.pnlConfigSetting.Name = "pnlConfigSetting";
            this.pnlConfigSetting.Size = new System.Drawing.Size(534, 306);
            this.pnlConfigSetting.TabIndex = 40;
            // 
            // tvwUsers
            // 
            this.tvwUsers.ImageIndex = 2;
            this.tvwUsers.ImageList = this.imgLstWorkFlow;
            this.tvwUsers.Location = new System.Drawing.Point(270, 26);
            this.tvwUsers.Name = "tvwUsers";
            treeNode1.Name = "Users";
            treeNode1.Text = "Users";
            this.tvwUsers.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.tvwUsers.SelectedImageIndex = 0;
            this.tvwUsers.Size = new System.Drawing.Size(255, 264);
            this.tvwUsers.TabIndex = 7;
            this.tvwUsers.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.tvwUsers_AfterCheck);
            this.tvwUsers.DragDrop += new System.Windows.Forms.DragEventHandler(this.tvwUsers_DragDrop);
            this.tvwUsers.DragEnter += new System.Windows.Forms.DragEventHandler(this.tvwUsers_DragEnter);
            this.tvwUsers.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.tvwUsers_BeforeSelect);
            this.tvwUsers.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tvwUsers_ItemDrag);
            this.tvwUsers.DragOver += new System.Windows.Forms.DragEventHandler(this.tvwUsers_DragOver);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Work Flow Details";
            // 
            // tvwHierarchy
            // 
            this.tvwHierarchy.AllowDrop = true;
            this.tvwHierarchy.ImageIndex = 1;
            this.tvwHierarchy.ImageList = this.imgLstWorkFlow;
            this.tvwHierarchy.Location = new System.Drawing.Point(8, 26);
            this.tvwHierarchy.Name = "tvwHierarchy";
            treeNode2.Name = "Work Flow";
            treeNode2.Text = "WorkFlow";
            this.tvwHierarchy.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode2});
            this.tvwHierarchy.SelectedImageIndex = 0;
            this.tvwHierarchy.Size = new System.Drawing.Size(257, 264);
            this.tvwHierarchy.TabIndex = 1;
            this.tvwHierarchy.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.tvwHierarchy_AfterCheck);
            this.tvwHierarchy.DragDrop += new System.Windows.Forms.DragEventHandler(this.tvwHierarchy_DragDrop);
            this.tvwHierarchy.DragEnter += new System.Windows.Forms.DragEventHandler(this.tvwHierarchy_DragEnter);
            this.tvwHierarchy.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.tvwHierarchy_BeforeSelect);
            this.tvwHierarchy.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tvwRole_ItemDrag);
            this.tvwHierarchy.DragOver += new System.Windows.Forms.DragEventHandler(this.tvwHierarchy_DragOver);
            // 
            // btnRemoveAll
            // 
            this.btnRemoveAll.Location = new System.Drawing.Point(610, 97);
            this.btnRemoveAll.Name = "btnRemoveAll";
            this.btnRemoveAll.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveAll.TabIndex = 6;
            this.btnRemoveAll.Text = "Remove &All";
            this.btnRemoveAll.UseVisualStyleBackColor = true;
            this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(610, 126);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRemove.TabIndex = 5;
            this.btnRemove.Text = "&Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(468, 339);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 41;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // SStrHierarchySettings
            // 
            this.SStrHierarchySettings.BackColor = System.Drawing.Color.Transparent;
            this.SStrHierarchySettings.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabelHierarchySettings,
            this.lblHierarchySettingsstatus});
            this.SStrHierarchySettings.Location = new System.Drawing.Point(0, 365);
            this.SStrHierarchySettings.Name = "SStrHierarchySettings";
            this.SStrHierarchySettings.Size = new System.Drawing.Size(551, 22);
            this.SStrHierarchySettings.TabIndex = 42;
            this.SStrHierarchySettings.Text = "StatusStrip1";
            // 
            // StatusLabelHierarchySettings
            // 
            this.StatusLabelHierarchySettings.Name = "StatusLabelHierarchySettings";
            this.StatusLabelHierarchySettings.Size = new System.Drawing.Size(39, 17);
            this.StatusLabelHierarchySettings.Text = "Status";
            this.StatusLabelHierarchySettings.ToolTipText = "Status";
            // 
            // lblHierarchySettingsstatus
            // 
            this.lblHierarchySettingsstatus.Name = "lblHierarchySettingsstatus";
            this.lblHierarchySettingsstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(10, 341);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 43;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(387, 339);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 44;
            this.btnOK.Text = "&Ok";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // imgLstWorkFlow
            // 
            this.imgLstWorkFlow.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgLstWorkFlow.ImageStream")));
            this.imgLstWorkFlow.TransparentColor = System.Drawing.Color.Transparent;
            this.imgLstWorkFlow.Images.SetKeyName(0, "batch.png");
            this.imgLstWorkFlow.Images.SetKeyName(1, "blocks.png");
            this.imgLstWorkFlow.Images.SetKeyName(2, "locations.png");
            this.imgLstWorkFlow.Images.SetKeyName(3, "lots.png");
            this.imgLstWorkFlow.Images.SetKeyName(4, "raws.png");
            // 
            // FrmOrganizationHierarchySetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 387);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnRemoveAll);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.SStrHierarchySettings);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.pnlConfigSetting);
            this.Controls.Add(this.ConfigurationSettingBindingNavigator);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmOrganizationHierarchySetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WorkFlow Settings";
            this.TitleText = "WorkFlow Settings";
            this.Load += new System.EventHandler(this.FrmOrganizationHierarchySetting_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmOrganizationHierarchySetting_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ConfigurationSettingBindingNavigator)).EndInit();
            this.ConfigurationSettingBindingNavigator.ResumeLayout(false);
            this.ConfigurationSettingBindingNavigator.PerformLayout();
            this.pnlConfigSetting.ResumeLayout(false);
            this.pnlConfigSetting.PerformLayout();
            this.SStrHierarchySettings.ResumeLayout(false);
            this.SStrHierarchySettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator ConfigurationSettingBindingNavigator;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        private System.Windows.Forms.Panel pnlConfigSetting;
        private System.Windows.Forms.TreeView tvwHierarchy;
        private System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.StatusStrip SStrHierarchySettings;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabelHierarchySettings;
        internal System.Windows.Forms.ToolStripStatusLabel lblHierarchySettingsstatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnRemoveAll;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Timer TmHierarchySettings;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TreeView tvwUsers;
        private System.Windows.Forms.ToolStripButton BtnDelete;
        private System.Windows.Forms.ImageList imgLstWorkFlow;
    }
}