﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class frmRole : DevComponents.DotNetBar.Office2007Form//Form
    {
        clsBLLRoleSettings objclsRoleSettingsBLL;
       // clsBLLPermissionSettings objClsBLLPermissionSettings;
        clsDTORoleSettings objRoleSettings;
        clsMessage objclsMessage;
       
        bool blngridcheck = false;
        int MintComboID;
     

        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission

        string MstrMessageCommon; // Error/Information/Warning messages
        DataTable MdatMessages;
        MessageBoxIcon MmsgMessageBoxIcon;
        ClsNotificationNew MobjNotification; // Object of the Notification class

        private int intAddWidth = 0, intUpdateWidth = 0,intDeleteWidth =0,intViewWidth=0,intPrintWidth=0;
        private bool blnLoad = true;

        public frmRole()
        {
            InitializeComponent();
            objclsRoleSettingsBLL = new clsBLLRoleSettings();
            MobjNotification = new ClsNotificationNew();
            objclsMessage = new clsMessage(FormID.RoleSettings);
        }


        private void frmRole_Load(object sender, EventArgs e)
        {
               LoadMessage();
            LoadCombosNdModules();
            BindRoleDetails();


            if (dgvRoleSettings.Columns.Count > 0)
            {
                intAddWidth = chkAlladd.Left - (dgvRoleSettings.GetCellDisplayRectangle(clmIsCreate.HeaderCell.ColumnIndex, -1, false).Left + dgvRoleSettings.Left);
                intUpdateWidth = chkAllupdate.Left - (dgvRoleSettings.GetCellDisplayRectangle(clmIsUpdate.HeaderCell.ColumnIndex, -1, false).Left + dgvRoleSettings.Left);
                intDeleteWidth = chkalldelete.Left - (dgvRoleSettings.GetCellDisplayRectangle(clmIsDelete.HeaderCell.ColumnIndex, -1, false).Left + dgvRoleSettings.Left);
                intViewWidth = chkallview.Left - (dgvRoleSettings.GetCellDisplayRectangle(clmIsView.HeaderCell.ColumnIndex, -1, false).Left + dgvRoleSettings.Left);
                intPrintWidth = chkAllPrintEmail.Left - (dgvRoleSettings.GetCellDisplayRectangle(clmIsPrintEmail.HeaderCell.ColumnIndex, -1, false).Left + dgvRoleSettings.Left);
            }

           // SetPermissions();
            blnLoad = false;
        }

        private void LoadCombosNdModules()
        {
            try
            {
                
                cboRole.ValueMember = "RoleID";
                cboRole.DisplayMember = "RoleName";
                cboRole.DataSource = objclsRoleSettingsBLL.BindRoles();
            }
            catch (Exception ex)
            {
                //clsCommon.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        //private void SetPermissions()
        //{
        //    // Function for setting permissions
        //    objClsBLLPermissionSettings = new clsBLLPermissionSettings();

        //    if (ClsCommonSettings.RoleID != 1)
        //    {
        //        objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ModuleID.Settings, (int)MenuID.UserRole, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

        //        if (MblnAddPermission == true || MblnUpdatePermission == true)
        //            MblnAddUpdatePermission = true;  
        //    }
        //    else
        //        MblnAddPermission = MblnAddUpdatePermission = MblnUpdatePermission = true;

        //    btnSave.Enabled = MblnAddUpdatePermission;
        //    btnOk.Enabled = MblnAddUpdatePermission;
        //    btnSaveItem.Enabled = MblnAddUpdatePermission;
        //}

        private void LoadMessage() // Loading the variables for messages
        {
            MdatMessages = MobjNotification.FillMessageArray((int)FormID.RoleSettings, 4);
        }

        private void GetLstItemChecked()
        {
            try
            {
                DataTable dtModuleIDs = objclsRoleSettingsBLL.GetModuleIDs();
                for (int i = 0; i < dtModuleIDs.Rows.Count; i++)
                {
                    for (int j = 0; j < this.lstModules.Items.Count; j++)
                    {
                        if (dtModuleIDs.Rows[i]["ModuleID"].ToInt32() == Convert.ToInt32(((clsListItem)(lstModules.Items[j])).Value))
                            this.lstModules.SetItemChecked(j, true);
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void BindRoleDetails()
        {
            try
            {
                objclsRoleSettingsBLL.objclsRoleSettings.RoleID = Convert.ToInt32(cboRole.SelectedValue);
                chkAlladd.Checked = chkallview.Checked = chkAllupdate.Checked = chkalldelete.Checked = chkAllPrintEmail.Checked= false;

                DataTable dtModules = objclsRoleSettingsBLL.BindModules();
                lstModules.Items.Clear();
                if (dtModules.Rows.Count > 0)
                {
                    for (int i = 0; i < dtModules.Rows.Count; i++)
                    {
                        lstModules.Items.Add(new clsListItem(dtModules.Rows[i]["ModuleId"], dtModules.Rows[i]["Description"].ToString()));
                    }
                }

                DataTable dtRoleDetails = objclsRoleSettingsBLL.BindRoleDetails();
                dgvRoleSettings.Rows.Clear();

                if (dtRoleDetails.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRoleDetails.Rows.Count; i++)
                    {
                        dgvRoleSettings.RowCount = dgvRoleSettings.RowCount + 1;
                        dgvRoleSettings.Rows[i].Cells["clmRoleID"].Value = dtRoleDetails.Rows[i]["RoleID"];
                        dgvRoleSettings.Rows[i].Cells["clmModuleID"].Value = dtRoleDetails.Rows[i]["ModuleID"];
                        dgvRoleSettings.Rows[i].Cells["clmMenuID"].Value = dtRoleDetails.Rows[i]["MenuID"];
                        dgvRoleSettings.Rows[i].Cells["clmModuleName"].Value = dtRoleDetails.Rows[i]["ModuleName"];
                        dgvRoleSettings.Rows[i].Cells["clmMenuName"].Value = dtRoleDetails.Rows[i]["MenuName"];
                      //  dgvRoleSettings.Rows[i].Cells[5].Value = dtRoleDetails.Rows[i]["FormName"];
                        dgvRoleSettings.Rows[i].Cells["clmIsCreate"].Value = dtRoleDetails.Rows[i]["IsCreate"];
                        dgvRoleSettings.Rows[i].Cells["clmIsView"].Value = dtRoleDetails.Rows[i]["IsView"];
                        dgvRoleSettings.Rows[i].Cells["clmIsUpdate"].Value = dtRoleDetails.Rows[i]["IsUpdate"];
                        dgvRoleSettings.Rows[i].Cells["clmIsDelete"].Value = dtRoleDetails.Rows[i]["IsDelete"];
                        dgvRoleSettings.Rows[i].Cells["clmIsPrintEmail"].Value = dtRoleDetails.Rows[i]["IsPrintEmail"];
                    }
                   // dgvRoleSettings.Sort(dgvRoleSettings.Columns[1], ListSortDirection.Ascending);
                    GetLstItemChecked();

                    if (fnChalladdtick() == false)
                        chkAlladd.Checked = false;
                    else
                        chkAlladd.Checked = true;

                    if (fnChallviewtick() == false)
                        chkallview.Checked = false;
                    else
                        chkallview.Checked = true;

                    if (fnChallupdatetick() == false)
                        chkAllupdate.Checked = false;
                    else
                        chkAllupdate.Checked = true;

                    if (fnChalldeletetick() == false)
                        chkalldelete.Checked = false;
                    else
                        chkalldelete.Checked = true;

                    if (fnChallPrintEmailtick() == false)
                        chkAllPrintEmail.Checked = false;
                    else
                        chkAllPrintEmail.Checked = true;              
                    
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void cboRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindRoleDetails();
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void btnBottomCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void lstModules_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            try
            {
                clsListItem o;
                if (lstModules.SelectedItem == null) return;
                o = (clsListItem)lstModules.SelectedItem;
                int tag = (int)o.Value;

                objclsRoleSettingsBLL.objclsRoleSettings.ModuleID = tag;

                if (e.NewValue == CheckState.Unchecked)
                {
                    RemoveGridRow(tag);
                }
                else
                {
                    BindRoleDetails(tag);
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void BindRoleDetails(int iModuleId)
        {
            try
            {
                objclsRoleSettingsBLL.objclsRoleSettings.RoleID = Convert.ToInt32(cboRole.SelectedValue);
                objclsRoleSettingsBLL.objclsRoleSettings.ModuleID = iModuleId;
                DataTable dtRoleDetails = objclsRoleSettingsBLL.BindaModulePermissions();

                if (dtRoleDetails.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRoleDetails.Rows.Count; i++)
                    {
                        dgvRoleSettings.RowCount = dgvRoleSettings.RowCount + 1;
                        dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmRoleID"].Value = dtRoleDetails.Rows[i]["RoleID"];
                        dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmModuleID"].Value = dtRoleDetails.Rows[i]["ModuleID"];
                        dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmMenuID"].Value = dtRoleDetails.Rows[i]["MenuID"];
                        dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmModuleName"].Value = dtRoleDetails.Rows[i]["ModuleName"];
                        dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmMenuName"].Value = dtRoleDetails.Rows[i]["MenuName"];
                     //   dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells[5].Value = dtRoleDetails.Rows[i]["FormName"];
                        dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmIsCreate"].Value = dtRoleDetails.Rows[i]["IsCreate"];
                        dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmIsView"].Value = dtRoleDetails.Rows[i]["IsView"];
                        dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmIsUpdate"].Value = dtRoleDetails.Rows[i]["IsUpdate"];
                        dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmIsDelete"].Value = dtRoleDetails.Rows[i]["IsDelete"];
                        dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmIsPrintEmail"].Value = dtRoleDetails.Rows[i]["IsPrintEmail"];
                    }
                }
                dgvRoleSettings.Sort(dgvRoleSettings.Columns[1], ListSortDirection.Ascending);
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void RemoveGridRow(int iModuleId)
        {
            try
            {
                for (int i = dgvRoleSettings.Rows.Count - 1; i >= 0; i--)
                {
                    if (dgvRoleSettings.Rows[i].Cells["clmModuleID"].Value.ToInt32() == iModuleId)
                    {
                        dgvRoleSettings.Rows.Remove(dgvRoleSettings.Rows[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void chkAlladd_CheckedChanged(object sender, EventArgs e)
        {
            if (blngridcheck == true) return;
            try
            {
                if (chkAlladd.Checked)
                {
                    for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
                    {
                        if (dgvRoleSettings.Rows[i].Cells["clmModuleID"].Value.ToInt32() == (int)eModuleID.Reports)
                        {
                            dgvRoleSettings.Rows[i].Cells["clmIsCreate"].Value = false;
                        }
                        else
                        {
                            dgvRoleSettings.Rows[i].Cells["clmIsCreate"].Value = true;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
                    {
                        dgvRoleSettings.Rows[i].Cells["clmIsCreate"].Value = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }
      
        private void chkallview_CheckedChanged(object sender, EventArgs e)
        {
            if (blngridcheck == true) return;
            try
            {
                if (chkallview.Checked)
                {  
                    for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
                    {
                        dgvRoleSettings.Rows[i].Cells["clmIsView"].Value = true;
                    }
                }
                else
                {
                    for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
                    {
                        dgvRoleSettings.Rows[i].Cells["clmIsView"].Value = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void chkAllupdate_CheckedChanged(object sender, EventArgs e)
        {
            if (blngridcheck == true) return;
            try
            {
                if (chkAllupdate.Checked)
                {
                    for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
                    {
                        if (dgvRoleSettings.Rows[i].Cells[1].Value.ToInt32() == (int)eModuleID.Reports)
                        {
                            dgvRoleSettings.Rows[i].Cells["clmIsUpdate"].Value = false;
                        }
                        else
                        {
                            dgvRoleSettings.Rows[i].Cells["clmIsUpdate"].Value = true;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
                    {
                        dgvRoleSettings.Rows[i].Cells["clmIsUpdate"].Value = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void chkalldelete_CheckedChanged(object sender, EventArgs e)
        {
            if (blngridcheck == true) return;
            try
            {
                if (chkalldelete.Checked)
                {
                    for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
                    {
                        if (dgvRoleSettings.Rows[i].Cells[1].Value.ToInt32() == (int)eModuleID.Reports)
                        {
                            dgvRoleSettings.Rows[i].Cells["clmIsDelete"].Value = false;
                        }
                        else
                        {
                            dgvRoleSettings.Rows[i].Cells["clmIsDelete"].Value = true;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
                    {
                        dgvRoleSettings.Rows[i].Cells["clmIsDelete"].Value = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private bool SaveRoleDetails()
        {
            this.dgvRoleSettings.Focus();
            try
            {
                objclsRoleSettingsBLL.objclsRoleSettings.RoleID = Convert.ToInt32(cboRole.SelectedValue);
                objclsRoleSettingsBLL.objclsRoleSettings.lstRoleDetails = new List<clsDTORoleSettings>();
               
                if(objclsMessage.ShowMessage(1783))
                {
                    for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
                    {
                        objRoleSettings = new clsDTORoleSettings();
                        objRoleSettings.RoleID = Convert.ToInt32(cboRole.SelectedValue);
                        objRoleSettings.ModuleID = dgvRoleSettings.Rows[i].Cells["clmModuleID"].Value.ToInt32();
                        objRoleSettings.MenuID = dgvRoleSettings.Rows[i].Cells["clmMenuID"].Value.ToInt32();
                        objRoleSettings.IsCreate = Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsCreate"].Value);
                        objRoleSettings.IsView = Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsView"].Value);
                        objRoleSettings.IsUpdate = Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsUpdate"].Value);
                        objRoleSettings.IsDelete = Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsDelete"].Value);
                        objRoleSettings.IsPrintEmail = Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsPrintEmail"].Value);
                        objclsRoleSettingsBLL.objclsRoleSettings.lstRoleDetails.Add(objRoleSettings);
                    }
                    if (objclsRoleSettingsBLL.SaveRoleDetails())
                    {
                        for (int j = 0; j < this.lstModules.Items.Count; j++)
                        {
                            if (!(this.lstModules.GetItemChecked(j)))
                            {
                               // objRoleSettings.RoleID = Convert.ToInt32(cboRole.SelectedValue);
                              //  objRoleSettings.ModuleID = Convert.ToInt32(((clsListItem)(lstModules.Items[j])).Value);

                                objclsRoleSettingsBLL.DeleteModulePermissions(Convert.ToInt32(cboRole.SelectedValue), Convert.ToInt32(((clsListItem)(lstModules.Items[j])).Value));
                            }
                        }
                        objclsMessage.ShowMessage(1784);
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Error Occured", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                return false;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveRoleDetails())
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveRoleDetails();
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                SaveRoleDetails();
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }     
      
        private bool fnChalladdtick()
        {
            for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsCreate"].Value) == false )
                {
                    return false;
                }
                
            }
            return true;
        }

        private bool fnChallviewtick()
        {
            for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsView"].Value) == false)
                {
                    return false;
                }

            }
            return true;
        }

        private bool fnChallupdatetick()
        {
            for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsUpdate"].Value) == false)
                {
                    return false;
                }

            }
            return true;
        }

        private bool fnChalldeletetick()
        {
            dgvRoleSettings.CommitEdit(DataGridViewDataErrorContexts.Commit);

            for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsDelete"].Value) == false)
                {
                    return false;
                }
            }
            return true;
        }

        private bool fnChallPrintEmailtick()
        {
            dgvRoleSettings.CommitEdit(DataGridViewDataErrorContexts.Commit);

            for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsPrintEmail"].Value) == false)
                {
                    return false;
                }
            }
            return true;
        }

        private void dgvRoleSettings_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (Convert.ToInt32(dgvRoleSettings.Rows[e.RowIndex].Cells[1].Value) == (int)eModuleID.Reports)
                    {
                        if (e.ColumnIndex == 5 || e.ColumnIndex == 7 || e.ColumnIndex == 8)
                        {
                            dgvRoleSettings.CancelEdit();
                        }
                    }
                    else
                    {
                        dgvRoleSettings.CommitEdit(DataGridViewDataErrorContexts.Commit);

                        blngridcheck = true;
                        if (e.ColumnIndex == 5)
                        {
                            if (fnChalladdtick() == false)
                                chkAlladd.Checked = false;
                            else
                                chkAlladd.Checked = true;

                        }
                        else if (e.ColumnIndex == 6)
                        {
                            if (fnChallviewtick() == false)
                                chkallview.Checked = false;
                            else
                                chkallview.Checked = true;
                        }
                        else if (e.ColumnIndex == 7)
                        {
                            if (fnChallupdatetick() == false)
                                chkAllupdate.Checked = false;
                            else
                                chkAllupdate.Checked = true;
                        }
                        else if (e.ColumnIndex == 8)
                        {
                            if (fnChalldeletetick() == false)
                                chkalldelete.Checked = false;
                            else
                                chkalldelete.Checked = true;
                        }

                        else if (e.ColumnIndex == 9)
                        {
                            if (fnChallPrintEmailtick() == false)
                                chkAllPrintEmail.Checked = false;
                            else
                                chkAllPrintEmail.Checked = true;
                        }
                       
                        blngridcheck = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void btnNewRole_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(cboRole.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Role", new int[] { 1, 0 }, "RoleID,RoleName", "RoleReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                LoadCombosNdModules();
                if (objCommon.NewID != 0)
                    cboRole.SelectedValue = objCommon.NewID;
                else
                    cboRole.SelectedValue = MintComboID;
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }


        private void chkAllPrintEmail_CheckedChanged(object sender, EventArgs e)
        {
            if (blngridcheck == true) return;
            try
            {
                if (chkAllPrintEmail.Checked)
                {
                    for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
                    {
                        dgvRoleSettings.Rows[i].Cells["clmIsPrintEmail"].Value = true;
                    }
                }
                else
                {
                    for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
                    {
                        dgvRoleSettings.Rows[i].Cells["clmIsPrintEmail"].Value = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }      
       

        private void cboRole_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        private void dgvRoleSettings_ColumnDividerWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
           int iPointL = dgvRoleSettings.GetCellDisplayRectangle(e.Column.HeaderCell.ColumnIndex, e.Column.HeaderCell.RowIndex, false).Left ;
          
        }

        private void dgvRoleSettings_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            if (!blnLoad)
            {
                //dgvRoleSettings.Location.X

                int iPointAddLeft = dgvRoleSettings.Left + dgvRoleSettings.GetCellDisplayRectangle(clmIsCreate.HeaderCell.ColumnIndex,-1, false).Left + intAddWidth;
                int iPointViewLeft = dgvRoleSettings.Left + dgvRoleSettings.GetCellDisplayRectangle(clmIsView.HeaderCell.ColumnIndex, -1, false).Left + intViewWidth;
                int iPointUpadateLeft = dgvRoleSettings.Left + dgvRoleSettings.GetCellDisplayRectangle(clmIsUpdate.HeaderCell.ColumnIndex, -1, false).Left + intUpdateWidth;
                int iPointDeleteLeft = dgvRoleSettings.Left + dgvRoleSettings.GetCellDisplayRectangle(clmIsDelete.HeaderCell.ColumnIndex, -1, false).Left + intDeleteWidth;
                int iPointPrintEMLeft = dgvRoleSettings.Left + dgvRoleSettings.GetCellDisplayRectangle(clmIsPrintEmail.HeaderCell.ColumnIndex, -1, false).Left + intPrintWidth;

                chkAlladd.Left = iPointAddLeft;
                chkAllupdate.Left = iPointUpadateLeft;
                chkalldelete.Left = iPointDeleteLeft;
                chkallview.Left = iPointViewLeft;
                chkAllPrintEmail.Left = iPointPrintEMLeft;

                //chkAlladd.Visible = dgvRoleSettings.GetCellDisplayRectangle(clmIsCreate.HeaderCell.ColumnIndex, e.Column.HeaderCell.RowIndex, false).Left==0? false : true;
                //chkAllupdate.Visible = dgvRoleSettings.GetCellDisplayRectangle(clmIsUpdate.HeaderCell.ColumnIndex, e.Column.HeaderCell.RowIndex, false).Left ==0? false : true;
                //chkalldelete.Visible = dgvRoleSettings.GetCellDisplayRectangle(clmIsDelete.HeaderCell.ColumnIndex, e.Column.HeaderCell.RowIndex, false).Left==0 ? false : true;
                //chkallview.Visible = dgvRoleSettings.GetCellDisplayRectangle(clmIsView.HeaderCell.ColumnIndex, e.Column.HeaderCell.RowIndex, false).Left ==0? false : true;
                //chkAllPrintEmail.Visible = dgvRoleSettings.GetCellDisplayRectangle(clmIsPrintEmail.HeaderCell.ColumnIndex, e.Column.HeaderCell.RowIndex, false).Left ==0? false : true;

                  int iDisplayCount=  dgvRoleSettings.DisplayedColumnCount(false);


                //
                  chkAlladd.Visible = iDisplayCount > 2 ? true : false;
                  chkallview.Visible = iDisplayCount > 3 ? true : false;
                  chkAllupdate.Visible = iDisplayCount > 4 ? true : false;
                  chkalldelete.Visible = iDisplayCount > 5 ? true : false;
                  chkAllPrintEmail.Visible = iDisplayCount > 6 ? true : false;

                  chkAllPrintEmail.Visible = clmIsPrintEmail.Width > 70 ? true : false;  
                //chkAlladd.Visible =clmIsCreate.Displayed;
                //chkAllupdate.Visible =clmIsUpdate.Displayed;
                //chkalldelete.Visible = clmIsDelete.Displayed;
                //chkallview.Visible =clmIsView.Displayed;
                //chkAllPrintEmail.Visible = clmIsPrintEmail.Displayed;
               

                //chkAlladd.Visible = chkAlladd.Left > dgvRoleSettings.Right - 10 ? false : true;

                //chkAlladd.Location = new Point(12, chkAlladd.Location.Y);
                //chkallview.Location = new Point(12, chkallview.Location.Y);
                //chkAllupdate.Location= new Point(12, chkAllupdate.Location.Y);
                //chkalldelete.Location = new Point(12, chkalldelete.Location.Y);
                //chkAllPrintEmail.Location = new Point(12, chkAllPrintEmail.Location.Y);
            }
        }    

       
    }
}
