﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;

/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,1 Mar 2011>
Description:	<Description, UnitOfMeasurement Form>
================================================
*/

namespace MyBooksERP
{
    public partial class FrmUnitOfMeasurement : DevComponents.DotNetBar.Office2007Form
    {
        #region Variables Declaration

        string MstrMessageCommon;
        MessageBoxIcon MmsgMessageIcon;

        public int PintUOM = 0;
        private int MintUOMID;

        private bool MblnChangeStatus = false;
        private bool MblnAddStatus;
        private bool MblnPrintEmailPermission = false;//To Set Print/Email Permission
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnAddUpdatePermission = false;//To Set Add/Update Permission

        //private ArrayList MaMessageArr;// Error Message display
        
        DataTable MdatMessages;
        ArrayList MsarFormNames; ////showing webbrowser in toolstripsplitbtn
        WebBrowser MwebBroswer;
        ToolStripControlHost MhostControlHost;
        clsBLLCommonUtility MobjBLLCommonUtility = new clsBLLCommonUtility();
        clsBLLUnitsOfMeasuremt MobjBLLUnitsOfMeasuremt;
        ClsLogWriter MobjLogWriter;
        ClsNotificationNew MobjNotification;
        #endregion //Variable Declaration

        #region Constructor

        public FrmUnitOfMeasurement()
        {
            InitializeComponent();

            MobjBLLUnitsOfMeasuremt = new clsBLLUnitsOfMeasuremt();
            MobjLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjNotification = new ClsNotificationNew();
            if(ClsCommonSettings.TimerInterval>0)
            {
            TmUOM.Interval = ClsCommonSettings.TimerInterval;  
            }
            else
            {
                TmUOM.Interval = 10;
            }// Setting timer interval

            //showing webbrowser in toolstripsplitbtn
            MwebBroswer = new WebBrowser();
            MwebBroswer.ScrollBarsEnabled = false;
            MwebBroswer.Name = "UsedLevel";
            MhostControlHost = new ToolStripControlHost(MwebBroswer);
            BtnwebUOMReference.DropDownItems.Add(MhostControlHost);
        }
        #endregion Constructor

        private void FrmUnitOfMeasurement_Load(object sender, EventArgs e)
        {
            try
            {
                SetPermissions();
                MblnChangeStatus = false;
                MblnAddStatus = true;
                LoadMessage();
                LoadCombos(0);
                if (PintUOM > 0)
                {
                    DisplayUOM(PintUOM);
                    FillUOMDetails();
                }
                else
                {
                    FillUOMDetails();
                    AddNewUOM();
                }
            }                
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form Load:FrmUnitOfMeasurement_Load" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmUnitOfMeasurement_Load " + Ex.Message.ToString());
            }
        }

        private void AddNewUOM()
        {
            try
            {
                MblnAddStatus = true;
                ClearControls();
                TmUOM.Enabled = true;
                dgvUOM.ClearSelection();
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3056, out MmsgMessageIcon);
                lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                EnableDisableButtons(true);
                ErrUom.Clear();
                if (dgvUOM.RowCount > 0)
                {
                    btnPrint.Enabled = MblnPrintEmailPermission;
                    btnEmail.Enabled = MblnPrintEmailPermission;
                }
                else
                {
                    btnPrint.Enabled = false;
                    btnEmail.Enabled = false;
                }
                btnClear.Enabled = false;
                txtDescription.Focus();
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form AddNewUOM:AddNewUOM()" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewUOM " + Ex.Message.ToString());
            }

        }

        private void ChangeStatus()
        {
            MblnChangeStatus = true;
            if (MblnAddStatus == true)
                btnOk.Enabled = btnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            else
                btnOk.Enabled = btnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            //btnOk.Enabled = MblnAddUpdatePermission;
            //btnSave.Enabled = MblnAddUpdatePermission;
            //BindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
            btnClear.Enabled = true;
            ErrUom.Clear();
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.UOM, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                //if (MblnAddPermission == true || MblnUpdatePermission == true)
                //    MblnAddUpdatePermission = true;
                //else
                //    MblnAddUpdatePermission = false;

                //if (MblnAddPermission == true || MblnUpdatePermission == true || MblnDeletePermission == true)
                //    MblnViewPermission = true;
            }
            else
                MblnAddPermission =  MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            //BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            //BindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
            //BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            //btnOk.Enabled = MblnUpdatePermission;
            //btnPrint.Enabled = MblnPrintEmailPermission;
            //btnEmail.Enabled = MblnPrintEmailPermission;
        }

        public void ChangeStatus(object sender, EventArgs e) // to set the event
        {
            ChangeStatus();
        }

        private void Cbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        /// <summary>
        /// Enable/disable buttons according to the action
        /// </summary>
        private void EnableDisableButtons(bool bEnable)
        {
            if (bEnable)
            {
                BindingNavigatorAddNewItem.Enabled = !bEnable;
                BindingNavigatorDeleteItem.Enabled = !bEnable;
            }
            else
            {
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            }
            //btnPrint.Enabled = !bEnable;
            //btnEmail.Enabled = !bEnable;
            BtnwebUOMReference.Enabled = !bEnable;

            if (bEnable)
            {
                btnPrint.Enabled = !bEnable;
                btnEmail.Enabled = !bEnable;
                BindingNavigatorSaveItem.Enabled = !bEnable;
                btnOk.Enabled = !bEnable;
                btnSave.Enabled = !bEnable;
                MblnChangeStatus = !bEnable;
            }
            else
            {
                btnPrint.Enabled = MblnPrintEmailPermission;
                btnEmail.Enabled = MblnPrintEmailPermission;
                BindingNavigatorSaveItem.Enabled = bEnable;
                btnOk.Enabled = bEnable;               
                btnSave.Enabled = bEnable;
                MblnChangeStatus = bEnable;
            }
        }

        // Clear Controls
        private void ClearControls()
        {
            txtDescription.Clear();
            txtShortName.Clear();
            txtShortName.Tag = "0";
            cboUOMClass.SelectedIndex = -1;
            txtQuantityPerMeasurement.Text = "1";
            txtScale.Clear();           
            dgvUOM.ClearSelection();
            ErrUom.Clear();
            txtScale.Enabled = cboUOMClass.Enabled = txtQuantityPerMeasurement.Enabled = true;
        }

        // Loading UOMClass Combo
        private bool LoadCombos(int intType)
        {
            // 0 - For loading UOMClass Combo
           
            try
            {
                DataTable datCombos = new DataTable();

                if (intType == 0 || intType == 1)
                {
                    datCombos = MobjBLLUnitsOfMeasuremt.FillCombos(new string[] { "UOMTypeID,UOMType", "InvUOMTypeReference", "" });
                    cboUOMClass.ValueMember = "UOMTypeID";
                    cboUOMClass.DisplayMember = "UOMType";
                    cboUOMClass.DataSource = datCombos;
                }               

                MobjLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }


        private void BtnUOMClass_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("UOMClass", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "UOMTypeID,UOMType As UOMClass", "InvUOMTypeReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(cboUOMClass.SelectedValue);
                LoadCombos(1);
                if (objCommon.NewID != 0)
                    cboUOMClass.SelectedValue = objCommon.NewID;
                else
                    cboUOMClass.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnUOMClass" + Ex.Message.ToString());
            }
        }       

        private void LoadMessage()
        {
            MdatMessages = MobjNotification.FillMessageArray((int)FormID.UnitsOfMeasurement, ClsCommonSettings.ProductID);
        }        

        private void FillUOMDetails()
        {
            dgvUOM.DataSource = null;
            dgvUOM.DataSource =MobjBLLUnitsOfMeasuremt.GetUOMDetails();
            if (dgvUOM.ColumnCount>0)
            {
            dgvUOM.Columns[0].Visible = false;
            dgvUOM.Columns[1].Width = 125;
            dgvUOM.Columns[1].HeaderText = "UOM Name";
            dgvUOM.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvUOM.Columns[2].Width = 50;
            dgvUOM.Columns[2].HeaderText = "Code";
            dgvUOM.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;

            dgvUOM.Columns[3].Width = 100;
            dgvUOM.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvUOM.Columns[3].HeaderText = "UOM Type";
          //  dgvUOM.Columns[4].Width = 70;
          //  dgvUOM.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;

            //dgvUOM.Columns[4].HeaderText = "Qty Per UOM";
            //dgvUOM.Columns[4].Width = 50;
            //dgvUOM.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
            //dgvUOM.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvUOM.Columns[4].HeaderText = "Scale";
            dgvUOM.Columns[4].Width = 50;
            dgvUOM.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
            dgvUOM.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
            
            dgvUOM.ClearSelection();
            }
        }

        // UOM Validation
        private bool UOMValidation()
        {           
            lblUOMStatus.Text = "";           

            if (MobjBLLUnitsOfMeasuremt.CheckDuplication(MblnAddStatus, new string[] { txtDescription.Text.Replace("'", "").Trim() }, MintUOMID))
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3058, out MmsgMessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                ErrUom.SetError(txtDescription, MstrMessageCommon.Replace("#", "").Trim());
                lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmUOM.Enabled = true;
                txtDescription.Focus();
                return false;
            }   

            if (txtDescription.Text.Trim() == "")
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3051, out MmsgMessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                ErrUom.SetError(txtDescription, MstrMessageCommon.Replace("#", "").Trim());
                lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmUOM.Enabled = true;
                txtDescription.Focus();
                return false;
            }             

             if (txtShortName.Text.Trim() == "")
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3052, out MmsgMessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                ErrUom.SetError(txtShortName, MstrMessageCommon.Replace("#", "").Trim());
                lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmUOM.Enabled = true;
                txtShortName.Focus();
                return false;
            }
            // if ((txtQuantityPerMeasurement.Text.Trim().Length > 0 ? Convert.ToDouble(txtQuantityPerMeasurement.Text.Trim()) : 0) == 0)
            //{
            //    MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3054, out MmsgMessageIcon);
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
            //    ErrUom.SetError(txtQuantityPerMeasurement, MstrMessageCommon.Replace("#", "").Trim());
            //    lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            //    TmUOM.Enabled = true;
            //    txtQuantityPerMeasurement.Focus();
            //    return false;
            //}
             if (Convert.ToInt32(cboUOMClass.SelectedValue) == 0)
            {

                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3053, out MmsgMessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                ErrUom.SetError(cboUOMClass, MstrMessageCommon.Replace("#", "").Trim());
                lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmUOM.Enabled = true;
                cboUOMClass.Focus();
                return false;
            }
           
             if (txtScale.Text.Trim() == "")
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3060, out MmsgMessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                ErrUom.SetError(txtScale, MstrMessageCommon.Replace("#", "").Trim());
                lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmUOM.Enabled = true;
                txtScale.Focus();
                return false;
               
            }
             if ((txtScale.Text.Trim().Length > 0 ? Convert.ToInt32(txtScale.Text) : 0) > 10)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3061, out MmsgMessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                ErrUom.SetError(txtScale, MstrMessageCommon.Replace("#", "").Trim());
                lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmUOM.Enabled = true;
                txtScale.Focus();
                return false;
            }
           
           return true;
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewUOM();
        }

        // SaveUOMDetails 
        private bool SaveUOMDetails()
        {
            try
            {

                if (MblnAddStatus == true)
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 1, out MmsgMessageIcon);
                    MobjBLLUnitsOfMeasuremt.clsDTOUnitsOfMeasurement.intUOMID = 0;
                }
                else
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3, out MmsgMessageIcon);
                    MobjBLLUnitsOfMeasuremt.clsDTOUnitsOfMeasurement.intUOMID = MintUOMID;
                }
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;

                MobjBLLUnitsOfMeasuremt.clsDTOUnitsOfMeasurement.strDescription = txtDescription.Text.Replace("'", "`").Trim();
                MobjBLLUnitsOfMeasuremt.clsDTOUnitsOfMeasurement.strShortName = txtShortName.Text.Replace("'", "`").Trim();
                MobjBLLUnitsOfMeasuremt.clsDTOUnitsOfMeasurement.intUOMClassID = Convert.ToInt32(cboUOMClass.SelectedValue);
                //MobjBLLUnitsOfMeasuremt.clsDTOUnitsOfMeasurement.dblQuantityPerUOM = Convert.ToDouble(txtQuantityPerMeasurement.Text.Trim());
                MobjBLLUnitsOfMeasuremt.clsDTOUnitsOfMeasurement.intScale = Convert.ToInt32(txtScale.Text.Trim());

                if (MobjBLLUnitsOfMeasuremt.SaveUOM(MblnAddStatus))
                {
                    MobjBLLUnitsOfMeasuremt.clsDTOUnitsOfMeasurement.intUOMID = MintUOMID;

                    return true;
                }

                return false;
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form Save:SaveUOMDetails()" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Saving SaveUOMDetails() " + Ex.Message.ToString());
                return false;
            }

        }

        private void UoMBindingNavigatorSaveButton_Click(object sender, EventArgs e)
        {
            if (UOMValidation())
            {
                if (SaveUOMDetails())
                {
                    if (MblnAddStatus == true)
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 2, out MmsgMessageIcon);
                        lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmUOM.Enabled = true;
                    }
                    else
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3057, out MmsgMessageIcon);
                        lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);

                        TmUOM.Enabled = true;
                    }

                    FillUOMDetails();
                    AddNewUOM();
                }
            }
        }     
        
                         
       #region To get in webbrowser

        private bool GetFormNameTest(string strTablename)
        {
            MobjBLLUnitsOfMeasuremt.GetFormNameTest(strTablename);
            return true;
        }       

        //WebBrowserInformation
         private bool GetFormNameForBrowser()
         {
             if (MintUOMID == 0)
             {
                 return false;
             }
             DataTable dtIds = MobjBLLUnitsOfMeasuremt.UOMIDExists(MintUOMID);
             if (dtIds.Rows.Count > 0)
             {
                 if (Convert.ToString(dtIds.Rows[0][0]) != "0")
                 {
                     //SqlDataReader sReader = MobjUnitOfMeasurement.UOMIDExists(MintUOMID);
                     //if (sReader.Read())
                     //{

                     //    if (Convert.ToInt32(sReader[0]) != 0)
                     //    {
                             //if (sReader.VisibleFieldCount == 2)
                     //{
                         string sTableNames = Convert.ToString(dtIds.Rows[0][1]);//Get Table names
                         sTableNames = sTableNames.TrimStart(' ');
                         sTableNames = sTableNames.TrimEnd(',');
                         string[] tablename = sTableNames.Split(',');
                         //sReader.Close();
                         MsarFormNames = new ArrayList();
                         for (int j = 0; j < tablename.Length; j++)
                         {
                             DataTable dt;
                             dt = MobjBLLUnitsOfMeasuremt.GetFormNameTest(tablename[j]);
                             if (dt.Rows.Count > 0)
                             {

                                 for (int k = 0; k < dt.Rows.Count; k++)//
                                 {
                                     string fromName = Convert.ToString(dt.Rows[k]["FormName"]);
                                     // sarrFormnames = (ArrayList)fromName;
                                     MsarFormNames.Add(fromName.ToString());
                                 }
                             }
                         }
                         return true;
                     }
                 }
             //}
             //sReader.Close();
             return false;
         }
            
        //CREATING webBrowser Information
        private string SetWebBrowsernformation(ArrayList formnames)
        {

            StringBuilder MsbHtml = new StringBuilder();
            MsbHtml.Append("<html><head><style>" +
                          "td {font-family:arial;font-size:12px;border:none;color:black;padding:5px}" +
                          "a.link{font-family:arial;color:Blue;text-decoration: none}" +
                          "a.link:hover{font-family:arial;color: #3399CC;text-decoration: none}" +
                          "</style></head><body bgcolor=White topmargin=0 oncontextmenu=return false ondragstart=return false onselectstart=return false><div align=center>");
            MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>" +
                          "<tr bgcolor=#3a3a53><td colspan=2 align=left height=30px><font size=1 color=white><b>" + txtDescription.Text.Trim() + " Information Exists In</b></font></td></tr><tr><td colspan=2></td></tr>");


            MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");
            MsbHtml.Append("<tr bgcolor=#e5e5ef height=15px><td colspan=2 align=left><font size=1 color=#3a3a53><b>Forms</b></font></td></tr>");

            for (int i = 0; i < formnames.Count; i++)
            {
                //MsbHtml.Append("<tr height=25px><td width=30% align=left></td><td width=70% align=left>"+ formnames[i] +"</td></tr>");
                MsbHtml.Append("<tr><td width=70% align=left>" + formnames[i] + "</td></tr>");
            }

            MsbHtml.Append("</table></Body></HTML>");

            return MsbHtml.ToString();
        }

        # endregion To Get in Webbrowser


        private void SStripWebBrowserUOM_DropDownOpened(object sender, EventArgs e)
        {
            if (GetFormNameForBrowser())
            {
                MwebBroswer.DocumentText = SetWebBrowsernformation(MsarFormNames);
            }
            else
            {
                MwebBroswer.DocumentText = SetWebBrowsernformation(new ArrayList { "No Information Found" });
            }
        }       

        private void DisplayUOMDetails(int UOMID)
        {
            if (Convert.ToInt32(dgvUOM.CurrentRow.Cells[0].Value) != 0)
            {
                DisplayUOM(MintUOMID);
            }
        }
        private void DisplayUOM(int UOMID)
        {
            if (PintUOM > 0)
            {
                MintUOMID = PintUOM;
                PintUOM = 0;
            }
            else
                MintUOMID = Convert.ToInt32(dgvUOM.CurrentRow.Cells[0].Value);

            if (MobjBLLUnitsOfMeasuremt.DisplayUOM(UOMID))
            {
                txtDescription.Tag = MobjBLLUnitsOfMeasuremt.clsDTOUnitsOfMeasurement.intUOMID.ToString();
                txtDescription.Text = MobjBLLUnitsOfMeasuremt.clsDTOUnitsOfMeasurement.strDescription.ToString();
                txtShortName.Text = MobjBLLUnitsOfMeasuremt.clsDTOUnitsOfMeasurement.strShortName.ToString();
                cboUOMClass.SelectedValue = MobjBLLUnitsOfMeasuremt.clsDTOUnitsOfMeasurement.intUOMClassID.ToString();
               // txtQuantityPerMeasurement.Text = MobjBLLUnitsOfMeasuremt.clsDTOUnitsOfMeasurement.dblQuantityPerUOM.ToString();
                txtScale.Text = MobjBLLUnitsOfMeasuremt.clsDTOUnitsOfMeasurement.intScale.ToString();

                cboUOMClass.Enabled = txtQuantityPerMeasurement.Enabled = txtScale.Enabled = true;
                //DataTable dtIds = MobjBLLUnitsOfMeasuremt.UOMIDExists(UOMID);
                
                //if (dtIds.Rows.Count > 0)
                //{
                //    if (Convert.ToString(dtIds.Rows[0][0]) != "0")
                //    {

                //        cboUOMClass.Enabled = txtQuantityPerMeasurement.Enabled = txtScale.Enabled = false;
                //        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3062, out MmsgMessageIcon);
                //        lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //        TmUOM.Enabled = true;
                //    }
                //}

            }
            txtDescription.Focus();
            MblnAddStatus = false;
            EnableDisableButtons(false);

            //SqlDataReader sReader = MobjUnitOfMeasurement.UOMIDExists(MintUOMID);

            //if (sReader.Read())
            //{
            //    object s = sReader[0];

            //    if (Convert.ToInt32(sReader[0]) != 0)
            //    {
            //        string sTableName = Convert.ToString(sReader[1]);
            //        webUOMReference.Enabled = true;
            //    }
            //    else
            //    {
            //        webUOMReference.Enabled = false;
            //    }

            //    sReader.Close();
            //}

            //bnPrint.Enabled = true;
            //MblnChangeStatus = false;
        
        }
        private void DgvUOM_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                MintUOMID = Convert.ToInt32(dgvUOM.CurrentRow.Cells[0].Value);
                DisplayUOMDetails(MintUOMID);
            }
        }

        private void DgvUOM_KeyDown(object sender, KeyEventArgs e)
        {
            if (dgvUOM.CurrentRow != null && dgvUOM.CurrentRow.Cells[0].Value != null)
            {
                MintUOMID = Convert.ToInt32(dgvUOM.CurrentRow.Cells[0].Value);
                DisplayUOMDetails(MintUOMID);
            }
        }

        private void DgvUOM_KeyUp(object sender, KeyEventArgs e)
        {
            if (dgvUOM.CurrentRow != null && dgvUOM.CurrentRow.Cells[0].Value != null)
            {
                MintUOMID = Convert.ToInt32(dgvUOM.CurrentRow.Cells[0].Value);
                DisplayUOMDetails(MintUOMID);
            }
        }

        // To check the existence of UOMID
        private bool DeleteValidation(int UOMID)
        {
            DataTable dtIds = MobjBLLUnitsOfMeasuremt.UOMIDExists(UOMID);
            if (dtIds.Rows.Count > 0)
            {
                if (Convert.ToString(dtIds.Rows[0][0]) != "0")
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3059, out MmsgMessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    TmUOM.Enabled = true;
                    return false;
                }
            }
            return true;
        }      

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                MintUOMID = Convert.ToInt32(dgvUOM.CurrentRow.Cells[0].Value);
                if (DeleteValidation(MintUOMID))
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 13, out MmsgMessageIcon);
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (MobjBLLUnitsOfMeasuremt.DeleteUOM(MintUOMID))
                        {
                            MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 4, out MmsgMessageIcon);
                            lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);

                            TmUOM.Enabled = true;
                            FillUOMDetails();
                            AddNewUOM();

                        }
                        return;
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form Delete:DeleteItem_Click" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Delete DeleteItem_Click  " + Ex.Message.ToString());
            }

        }

        private void UoMBindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewUOM();
            dgvUOM.ClearSelection();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (UOMValidation())
            {
                if (SaveUOMDetails())
                {
                    if (MblnAddStatus == true)
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 2, out MmsgMessageIcon);
                        lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmUOM.Enabled = true;
                    }
                    else
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3057, out MmsgMessageIcon);
                        lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmUOM.Enabled = true;
                    }
                    AddNewUOM();
                    FillUOMDetails();
                }
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (UOMValidation())
            {
                if (SaveUOMDetails())
                {
                    if (MblnAddStatus == true)
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 2, out MmsgMessageIcon);
                        lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmUOM.Enabled = true;
                    }
                    else
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3057, out MmsgMessageIcon);
                        lblUOMStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmUOM.Enabled = true;
                    }

                    MblnChangeStatus = false;
                    this.Close();
                    AddNewUOM();
                    FillUOMDetails();
                }
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
               

        private void CancelToolStripButton_Click(object sender, EventArgs e)
        {
            AddNewUOM();
        }            
        
        private void FrmUnitOfMeasurement_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MblnChangeStatus)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 8, out MmsgMessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void TmUOM_Tick(object sender, EventArgs e)
        {
            TmUOM.Enabled = false;
            lblUOMStatus.Text = "";
        }

        private void TmFocus_Tick(object sender, EventArgs e)
        {
            txtDescription.Focus();
           
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void LoadReport()
        {
            try
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiFormID = (int)FormID.UnitsOfMeasurement;
                ObjViewer.ShowDialog();
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form Print:LoadReport()" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport() " + Ex.Message.ToString());
            }

        }       
       
         private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "UOM Information";
                    ObjEmailPopUp.EmailFormType = EmailFormID.UnitsOfMeasurement;
                    ObjEmailPopUp.EmailSource = MobjBLLUnitsOfMeasuremt.GetUOMReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form Email: BtnEmail_Click" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Email BtnEmail_Click " + Ex.Message.ToString());
            }
        }

         private void txtQuantityPerMeasurement_TextChanged(object sender, EventArgs e)
         {
             ChangeStatus();

         }

         private void FrmUnitOfMeasurement_KeyDown(object sender, KeyEventArgs e)
         {
             try
             {
                 switch (e.KeyData)
                 {
                     case Keys.F1:
                         //Dim objHelp As New VisaHelp
                         //objHelp.frmWhere = "nEmp"
                         //objHelp.Show()
                         //objHelp = Nothing
                         break;
                     case Keys.Escape:
                         this.Close();
                         break;
                     //case Keys.Control | Keys.S:
                     //    btnSave_Click(sender,new EventArgs());//save
                     //  break;
                     //case Keys.Control | Keys.O:
                     //    btnOk_Click(sender, new EventArgs());//OK
                     //    break; 
                     //case Keys.Control | Keys.L:
                     //    btnCancel_Click(sender, new EventArgs());//Cancel
                     //    break;
                     case Keys.Control | Keys.Enter:
                         BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                         break;
                     case Keys.Alt | Keys.R:
                         bindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                         break;
                     case Keys.Control | Keys.E:
                         CancelToolStripButton_Click(sender, new EventArgs());//Clear
                         break;
                     case Keys.Control | Keys.P:
                         BtnPrint_Click(sender, new EventArgs());//Cancel
                         break;
                     case Keys.Control | Keys.M:
                         BtnEmail_Click(sender, new EventArgs());//Cancel
                         break;
                 }
             }
             catch (Exception)
             {
             }
         }

         private void btnHelp_Click(object sender, EventArgs e)
         {
             FrmHelp objHelp = new FrmHelp();
             objHelp.strFormName = "UOM";
             objHelp.ShowDialog();
             objHelp = null;
         }

         private void dgvUOM_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
         {
             
MobjBLLCommonUtility.SetSerialNo(dgvUOM , e.RowIndex, true);
         }

         private void dgvUOM_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
         {
             
MobjBLLCommonUtility.SetSerialNo(dgvUOM , e.RowIndex, true);
         }             
     
            
    }
}
