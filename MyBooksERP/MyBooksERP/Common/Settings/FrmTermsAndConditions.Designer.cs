﻿namespace MyBooksERP
{
    partial class FrmTermsAndConditions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTermsAndConditions));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            //AmberERP.DtSetTermsandConditions dtSetTermsandConditions1 = new AmberERP.DtSetTermsandConditions();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TermsAndCondBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorAddNewTerms = new System.Windows.Forms.ToolStripButton();
            this.TermsAndCondBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BtnClear = new System.Windows.Forms.ToolStripButton();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.bnEmail = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.lblOperationType = new System.Windows.Forms.Label();
            this.lblDesInEng = new System.Windows.Forms.Label();
            this.lblDesInArabic = new System.Windows.Forms.Label();
            this.cboOperationType = new System.Windows.Forms.ComboBox();
            this.rtxtDescInEng = new System.Windows.Forms.RichTextBox();
            this.rtxtDescInArb = new System.Windows.Forms.RichTextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.GrpBxForm = new System.Windows.Forms.GroupBox();
            this.CboCompany = new System.Windows.Forms.ComboBox();
            this.lblCompany = new System.Windows.Forms.Label();
            this.lblCofigTermAndCond = new System.Windows.Forms.Label();
            this.lblTermsAndCond = new System.Windows.Forms.Label();
            this.DgvTermsAndCond = new System.Windows.Forms.DataGridView();
            this.SINo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescriptionEnglish = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescriptionArabic = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OperationTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OperationTypeDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TermsID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.CV = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.sTTermsConditionsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tmTermsAndCond = new System.Windows.Forms.Timer(this.components);
            this.errorProviderTermsAndCond = new System.Windows.Forms.ErrorProvider(this.components);
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.toolStripStatusLabel = new DevComponents.DotNetBar.LabelItem();
            this.lblTACStatus = new DevComponents.DotNetBar.LabelItem();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sTTermsConditionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.TermsAndCondBindingNavigator)).BeginInit();
            this.TermsAndCondBindingNavigator.SuspendLayout();
            this.GrpBxForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTermsAndCond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sTTermsConditionsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderTermsAndCond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sTTermsConditionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // TermsAndCondBindingNavigator
            // 
            this.TermsAndCondBindingNavigator.AddNewItem = null;
            this.TermsAndCondBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.TermsAndCondBindingNavigator.CountItem = null;
            this.TermsAndCondBindingNavigator.DeleteItem = null;
            this.TermsAndCondBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorAddNewTerms,
            this.TermsAndCondBindingNavigatorSaveItem,
            this.bnDeleteItem,
            this.BtnClear,
            this.bnPrint,
            this.bnEmail,
            this.BindingNavigatorSeparator,
            this.BtnHelp,
            this.BtnEmail});
            this.TermsAndCondBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.TermsAndCondBindingNavigator.MoveFirstItem = null;
            this.TermsAndCondBindingNavigator.MoveLastItem = null;
            this.TermsAndCondBindingNavigator.MoveNextItem = null;
            this.TermsAndCondBindingNavigator.MovePreviousItem = null;
            this.TermsAndCondBindingNavigator.Name = "TermsAndCondBindingNavigator";
            this.TermsAndCondBindingNavigator.PositionItem = null;
            this.TermsAndCondBindingNavigator.Size = new System.Drawing.Size(472, 25);
            this.TermsAndCondBindingNavigator.TabIndex = 2;
            this.TermsAndCondBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorAddNewTerms
            // 
            this.BindingNavigatorAddNewTerms.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewTerms.Image")));
            this.BindingNavigatorAddNewTerms.Name = "BindingNavigatorAddNewTerms";
            this.BindingNavigatorAddNewTerms.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewTerms.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewTerms.ToolTipText = "Add New Terms And Conditions";
            this.BindingNavigatorAddNewTerms.Click += new System.EventHandler(this.BindingNavigatorAddNewTerms_Click);
            // 
            // TermsAndCondBindingNavigatorSaveItem
            // 
            this.TermsAndCondBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("TermsAndCondBindingNavigatorSaveItem.Image")));
            this.TermsAndCondBindingNavigatorSaveItem.Name = "TermsAndCondBindingNavigatorSaveItem";
            this.TermsAndCondBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.TermsAndCondBindingNavigatorSaveItem.ToolTipText = "Save Terms And Condition";
            this.TermsAndCondBindingNavigatorSaveItem.Click += new System.EventHandler(this.TermsAndCondBindingNavigatorSaveItem_Click);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bnDeleteItem.Image")));
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.ToolTipText = "Delete Item";
            this.bnDeleteItem.Click += new System.EventHandler(this.bnDeleteItem_Click);
            // 
            // BtnClear
            // 
            this.BtnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnClear.Image = ((System.Drawing.Image)(resources.GetObject("BtnClear.Image")));
            this.BtnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnClear.Name = "BtnClear";
            this.BtnClear.Size = new System.Drawing.Size(23, 22);
            this.BtnClear.ToolTipText = "Clear";
            this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // bnPrint
            // 
            this.bnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.bnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(23, 22);
            this.bnPrint.ToolTipText = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // bnEmail
            // 
            this.bnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.bnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Size = new System.Drawing.Size(23, 22);
            this.bnEmail.ToolTipText = "Email";
            this.bnEmail.Click += new System.EventHandler(this.bnEmail_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Visible = false;
            // 
            // lblOperationType
            // 
            this.lblOperationType.AutoSize = true;
            this.lblOperationType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblOperationType.Location = new System.Drawing.Point(22, 63);
            this.lblOperationType.Name = "lblOperationType";
            this.lblOperationType.Size = new System.Drawing.Size(80, 13);
            this.lblOperationType.TabIndex = 5;
            this.lblOperationType.Text = "Operation Type";
            // 
            // lblDesInEng
            // 
            this.lblDesInEng.AutoSize = true;
            this.lblDesInEng.Location = new System.Drawing.Point(22, 88);
            this.lblDesInEng.Name = "lblDesInEng";
            this.lblDesInEng.Size = new System.Drawing.Size(103, 13);
            this.lblDesInEng.TabIndex = 6;
            this.lblDesInEng.Text = "Description (English)";
            // 
            // lblDesInArabic
            // 
            this.lblDesInArabic.AutoSize = true;
            this.lblDesInArabic.Location = new System.Drawing.Point(22, 139);
            this.lblDesInArabic.Name = "lblDesInArabic";
            this.lblDesInArabic.Size = new System.Drawing.Size(99, 13);
            this.lblDesInArabic.TabIndex = 8;
            this.lblDesInArabic.Text = "Description (Arabic)";
            // 
            // cboOperationType
            // 
            this.cboOperationType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboOperationType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOperationType.BackColor = System.Drawing.SystemColors.Info;
            this.cboOperationType.DropDownHeight = 75;
            this.cboOperationType.FormattingEnabled = true;
            this.cboOperationType.IntegralHeight = false;
            this.cboOperationType.ItemHeight = 13;
            this.cboOperationType.Location = new System.Drawing.Point(135, 60);
            this.cboOperationType.Name = "cboOperationType";
            this.cboOperationType.Size = new System.Drawing.Size(252, 21);
            this.cboOperationType.TabIndex = 1;
            this.cboOperationType.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.cboOperationType.Enter += new System.EventHandler(this.cboOperationType_Enter);
            this.cboOperationType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboOperationType_KeyPress);
            this.cboOperationType.SelectedValueChanged += new System.EventHandler(this.cboOperationType_SelectedValueChanged);
            this.cboOperationType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboOperationType_KeyDown);
            // 
            // rtxtDescInEng
            // 
            this.rtxtDescInEng.BackColor = System.Drawing.SystemColors.Info;
            this.rtxtDescInEng.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtxtDescInEng.Location = new System.Drawing.Point(135, 86);
            this.rtxtDescInEng.MaxLength = 4500;
            this.rtxtDescInEng.Name = "rtxtDescInEng";
            this.rtxtDescInEng.Size = new System.Drawing.Size(252, 45);
            this.rtxtDescInEng.TabIndex = 2;
            this.rtxtDescInEng.Text = "";
            this.rtxtDescInEng.TextChanged += new System.EventHandler(this.rtxtDescInEng_TextChanged);
            // 
            // rtxtDescInArb
            // 
            this.rtxtDescInArb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtxtDescInArb.Location = new System.Drawing.Point(135, 137);
            this.rtxtDescInArb.MaxLength = 4500;
            this.rtxtDescInArb.Name = "rtxtDescInArb";
            this.rtxtDescInArb.Size = new System.Drawing.Size(252, 45);
            this.rtxtDescInArb.TabIndex = 3;
            this.rtxtDescInArb.Text = "";
            this.rtxtDescInArb.TextChanged += new System.EventHandler(this.rtxtDescInArb_TextChanged);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(395, 159);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(69, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // GrpBxForm
            // 
            this.GrpBxForm.BackColor = System.Drawing.Color.Transparent;
            this.GrpBxForm.Controls.Add(this.lblOperationType);
            this.GrpBxForm.Controls.Add(this.cboOperationType);
            this.GrpBxForm.Controls.Add(this.CboCompany);
            this.GrpBxForm.Controls.Add(this.lblCompany);
            this.GrpBxForm.Controls.Add(this.lblCofigTermAndCond);
            this.GrpBxForm.Controls.Add(this.lblTermsAndCond);
            this.GrpBxForm.Controls.Add(this.btnSave);
            this.GrpBxForm.Controls.Add(this.lblDesInArabic);
            this.GrpBxForm.Controls.Add(this.rtxtDescInArb);
            this.GrpBxForm.Controls.Add(this.rtxtDescInEng);
            this.GrpBxForm.Controls.Add(this.lblDesInEng);
            this.GrpBxForm.Controls.Add(this.DgvTermsAndCond);
            this.GrpBxForm.Controls.Add(this.shapeContainer1);
            this.GrpBxForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrpBxForm.Location = new System.Drawing.Point(0, 25);
            this.GrpBxForm.Name = "GrpBxForm";
            this.GrpBxForm.Size = new System.Drawing.Size(471, 360);
            this.GrpBxForm.TabIndex = 3;
            this.GrpBxForm.TabStop = false;
            // 
            // CboCompany
            // 
            this.CboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompany.BackColor = System.Drawing.SystemColors.Info;
            this.CboCompany.DropDownHeight = 75;
            this.CboCompany.FormattingEnabled = true;
            this.CboCompany.IntegralHeight = false;
            this.CboCompany.ItemHeight = 13;
            this.CboCompany.Location = new System.Drawing.Point(135, 33);
            this.CboCompany.Name = "CboCompany";
            this.CboCompany.Size = new System.Drawing.Size(252, 21);
            this.CboCompany.TabIndex = 0;
            this.CboCompany.SelectionChangeCommitted += new System.EventHandler(this.CboCompany_SelectionChangeCommitted);
            this.CboCompany.SelectedIndexChanged += new System.EventHandler(this.CboCompany_SelectedIndexChanged);
            this.CboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboOperationType_KeyDown);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblCompany.Location = new System.Drawing.Point(22, 36);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 54;
            this.lblCompany.Text = "Company";
            // 
            // lblCofigTermAndCond
            // 
            this.lblCofigTermAndCond.AutoSize = true;
            this.lblCofigTermAndCond.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCofigTermAndCond.Location = new System.Drawing.Point(5, 11);
            this.lblCofigTermAndCond.Name = "lblCofigTermAndCond";
            this.lblCofigTermAndCond.Size = new System.Drawing.Size(187, 13);
            this.lblCofigTermAndCond.TabIndex = 2;
            this.lblCofigTermAndCond.Text = "Configure Terms and Conditions";
            // 
            // lblTermsAndCond
            // 
            this.lblTermsAndCond.AutoSize = true;
            this.lblTermsAndCond.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblTermsAndCond.Location = new System.Drawing.Point(6, 189);
            this.lblTermsAndCond.Name = "lblTermsAndCond";
            this.lblTermsAndCond.Size = new System.Drawing.Size(225, 13);
            this.lblTermsAndCond.TabIndex = 52;
            this.lblTermsAndCond.Text = "View Configured Terms and Conditions";
            // 
            // DgvTermsAndCond
            // 
            this.DgvTermsAndCond.AllowUserToAddRows = false;
            this.DgvTermsAndCond.AllowUserToDeleteRows = false;
            this.DgvTermsAndCond.AllowUserToResizeRows = false;
            this.DgvTermsAndCond.BackgroundColor = System.Drawing.Color.White;
            this.DgvTermsAndCond.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvTermsAndCond.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DgvTermsAndCond.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvTermsAndCond.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SINo,
            this.DescriptionEnglish,
            this.DescriptionArabic,
            this.OperationTypeID,
            this.OperationTypeDescription,
            this.TermsID,
            this.CompanyID});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvTermsAndCond.DefaultCellStyle = dataGridViewCellStyle2;
            this.DgvTermsAndCond.Location = new System.Drawing.Point(7, 205);
            this.DgvTermsAndCond.MultiSelect = false;
            this.DgvTermsAndCond.Name = "DgvTermsAndCond";
            this.DgvTermsAndCond.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvTermsAndCond.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DgvTermsAndCond.RowHeadersVisible = false;
            this.DgvTermsAndCond.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DgvTermsAndCond.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvTermsAndCond.Size = new System.Drawing.Size(457, 150);
            this.DgvTermsAndCond.TabIndex = 5;
            this.DgvTermsAndCond.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvTermsAndCond_CellClick_1);
            this.DgvTermsAndCond.CurrentCellDirtyStateChanged += new System.EventHandler(this.DgvTermsAndCond_CurrentCellDirtyStateChanged_1);
            this.DgvTermsAndCond.SelectionChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // SINo
            // 
            this.SINo.DataPropertyName = "SlNo";
            this.SINo.HeaderText = "SlNo";
            this.SINo.Name = "SINo";
            this.SINo.ReadOnly = true;
            this.SINo.Width = 35;
            // 
            // DescriptionEnglish
            // 
            this.DescriptionEnglish.HeaderText = "DescriptionEnglish";
            this.DescriptionEnglish.Name = "DescriptionEnglish";
            this.DescriptionEnglish.ReadOnly = true;
            this.DescriptionEnglish.Width = 209;
            // 
            // DescriptionArabic
            // 
            this.DescriptionArabic.HeaderText = "DescriptionArabic";
            this.DescriptionArabic.Name = "DescriptionArabic";
            this.DescriptionArabic.ReadOnly = true;
            this.DescriptionArabic.Width = 209;
            // 
            // OperationTypeID
            // 
            this.OperationTypeID.HeaderText = "OperationTypeID";
            this.OperationTypeID.Name = "OperationTypeID";
            this.OperationTypeID.ReadOnly = true;
            this.OperationTypeID.Visible = false;
            // 
            // OperationTypeDescription
            // 
            this.OperationTypeDescription.HeaderText = "OperationTypeDescription";
            this.OperationTypeDescription.Name = "OperationTypeDescription";
            this.OperationTypeDescription.ReadOnly = true;
            this.OperationTypeDescription.Visible = false;
            // 
            // TermsID
            // 
            this.TermsID.HeaderText = "TermsID";
            this.TermsID.Name = "TermsID";
            this.TermsID.ReadOnly = true;
            this.TermsID.Visible = false;
            // 
            // CompanyID
            // 
            this.CompanyID.HeaderText = "CompanyID";
            this.CompanyID.Name = "CompanyID";
            this.CompanyID.ReadOnly = true;
            this.CompanyID.Visible = false;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1,
            this.lineShape2,
            this.CV});
            this.shapeContainer1.Size = new System.Drawing.Size(465, 341);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 179;
            this.lineShape1.X2 = 459;
            this.lineShape1.Y1 = 3;
            this.lineShape1.Y2 = 3;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 164;
            this.lineShape2.X2 = 474;
            this.lineShape2.Y1 = -1;
            this.lineShape2.Y2 = -5;
            // 
            // CV
            // 
            this.CV.Name = "lineShape1";
            this.CV.X1 = 221;
            this.CV.X2 = 461;
            this.CV.Y1 = 181;
            this.CV.Y2 = 181;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(321, 386);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(69, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(396, 386);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(69, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tmTermsAndCond
            // 
            this.tmTermsAndCond.Tick += new System.EventHandler(this.tmTermsAndCond_Tick);
            // 
            // errorProviderTermsAndCond
            // 
            this.errorProviderTermsAndCond.ContainerControl = this;
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.toolStripStatusLabel,
            this.lblTACStatus});
            this.bar1.Location = new System.Drawing.Point(0, 414);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(472, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 82;
            this.bar1.TabStop = false;
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Text = "Status:";
            // 
            // lblTACStatus
            // 
            this.lblTACStatus.Name = "lblTACStatus";
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn1.HeaderText = " TermsID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Operation Type";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Description in English";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 230;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Description in Arabic";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 230;
            // 
            // FrmTermsAndConditions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 433);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.GrpBxForm);
            this.Controls.Add(this.TermsAndCondBindingNavigator);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmTermsAndConditions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Terms And Conditions";
            this.Load += new System.EventHandler(this.FrmTermsAndConditions_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmTermsAndConditions_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.TermsAndCondBindingNavigator)).EndInit();
            this.TermsAndCondBindingNavigator.ResumeLayout(false);
            this.TermsAndCondBindingNavigator.PerformLayout();
            this.GrpBxForm.ResumeLayout(false);
            this.GrpBxForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTermsAndCond)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sTTermsConditionsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderTermsAndCond)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sTTermsConditionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator TermsAndCondBindingNavigator;
        internal System.Windows.Forms.ToolStripButton TermsAndCondBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BtnClear;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        private System.Windows.Forms.Label lblOperationType;
        private System.Windows.Forms.Label lblDesInEng;
        private System.Windows.Forms.Label lblDesInArabic;
        private System.Windows.Forms.RichTextBox rtxtDescInEng;
        private System.Windows.Forms.RichTextBox rtxtDescInArb;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox GrpBxForm;
        private System.Windows.Forms.DataGridView DgvTermsAndCond;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape CV;
        //private MyBooksERP.Reports.Company.DtSetExecutiveTableAdapters.GetExecutiveInformationTableAdapter getExecutiveInformationTableAdapter1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblTermsAndCond;
        private System.Windows.Forms.Label lblCofigTermAndCond;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Timer tmTermsAndCond;
        internal System.Windows.Forms.ErrorProvider errorProviderTermsAndCond;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel;
        private DevComponents.DotNetBar.LabelItem lblTACStatus;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        private System.Windows.Forms.ComboBox cboOperationType;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewTerms;
        private System.Windows.Forms.ToolStripButton bnPrint;
        private System.Windows.Forms.ToolStripButton bnEmail;
        private System.Windows.Forms.BindingSource sTTermsConditionsBindingSource;
        //private DtSetTermsandConditions dtSetTermsandConditions;
        private System.Windows.Forms.BindingSource sTTermsConditionsBindingSource1;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        private System.Windows.Forms.ComboBox CboCompany;
        private System.Windows.Forms.Label lblCompany;
        private System.Windows.Forms.DataGridViewTextBoxColumn SINo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescriptionEnglish;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescriptionArabic;
        private System.Windows.Forms.DataGridViewTextBoxColumn OperationTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn OperationTypeDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn TermsID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyID;


    }
}