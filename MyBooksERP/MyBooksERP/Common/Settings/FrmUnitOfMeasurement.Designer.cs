﻿namespace MyBooksERP
{
    partial class FrmUnitOfMeasurement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUnitOfMeasurement));
            this.UOMBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnwebUOMReference = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.pnlUOM = new System.Windows.Forms.Panel();
            this.txtQuantityPerMeasurement = new NumericTextBox();
            this.txtScale = new DemoClsDataGridview.NumericTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvUOM = new DemoClsDataGridview.ClsDataGirdView();
            this.BtnUOMClass = new System.Windows.Forms.Button();
            this.cboUOMClass = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtShortName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblUOMType = new System.Windows.Forms.Label();
            this.lblCode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.btnOk = new System.Windows.Forms.Button();
            this.ErrUom = new System.Windows.Forms.ErrorProvider(this.components);
            this.TmUOM = new System.Windows.Forms.Timer(this.components);
            this.btnCancel = new System.Windows.Forms.Button();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.toolStripStatusLabel11 = new DevComponents.DotNetBar.LabelItem();
            this.lblUOMStatus = new DevComponents.DotNetBar.LabelItem();
            ((System.ComponentModel.ISupportInitialize)(this.UOMBindingNavigator)).BeginInit();
            this.UOMBindingNavigator.SuspendLayout();
            this.pnlUOM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrUom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // UOMBindingNavigator
            // 
            this.UOMBindingNavigator.AddNewItem = null;
            this.UOMBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.UOMBindingNavigator.CountItem = null;
            this.UOMBindingNavigator.DeleteItem = null;
            this.UOMBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.btnClear,
            this.toolStripSeparator4,
            this.btnPrint,
            this.btnEmail,
            this.toolStripSeparator3,
            this.BtnwebUOMReference,
            this.btnHelp});
            this.UOMBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.UOMBindingNavigator.MoveFirstItem = null;
            this.UOMBindingNavigator.MoveLastItem = null;
            this.UOMBindingNavigator.MoveNextItem = null;
            this.UOMBindingNavigator.MovePreviousItem = null;
            this.UOMBindingNavigator.Name = "UOMBindingNavigator";
            this.UOMBindingNavigator.PositionItem = null;
            this.UOMBindingNavigator.Size = new System.Drawing.Size(379, 25);
            this.UOMBindingNavigator.TabIndex = 0;
            this.UOMBindingNavigator.Text = "bindingNavigator1";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.UoMBindingNavigatorSaveButton_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.bindingNavigatorDeleteItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.ToolTipText = "Clear";
            this.btnClear.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.ToolTipText = "Print";
            this.btnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnwebUOMReference
            // 
            this.BtnwebUOMReference.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnwebUOMReference.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1});
            this.BtnwebUOMReference.Image = global::MyBooksERP.Properties.Resources.Usage;
            this.BtnwebUOMReference.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnwebUOMReference.Name = "BtnwebUOMReference";
            this.BtnwebUOMReference.Size = new System.Drawing.Size(32, 22);
            this.BtnwebUOMReference.Text = "toolStripSplitButton1";
            this.BtnwebUOMReference.ToolTipText = "Show usage forms";
            this.BtnwebUOMReference.Visible = false;
            this.BtnwebUOMReference.DropDownOpened += new System.EventHandler(this.SStripWebBrowserUOM_DropDownOpened);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(57, 6);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = global::MyBooksERP.Properties.Resources.help;
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.Text = "toolStripButton4";
            this.btnHelp.ToolTipText = "Help";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // pnlUOM
            // 
            this.pnlUOM.BackColor = System.Drawing.Color.Transparent;
            this.pnlUOM.Controls.Add(this.txtQuantityPerMeasurement);
            this.pnlUOM.Controls.Add(this.txtScale);
            this.pnlUOM.Controls.Add(this.label6);
            this.pnlUOM.Controls.Add(this.txtDescription);
            this.pnlUOM.Controls.Add(this.label5);
            this.pnlUOM.Controls.Add(this.dgvUOM);
            this.pnlUOM.Controls.Add(this.BtnUOMClass);
            this.pnlUOM.Controls.Add(this.cboUOMClass);
            this.pnlUOM.Controls.Add(this.btnSave);
            this.pnlUOM.Controls.Add(this.txtShortName);
            this.pnlUOM.Controls.Add(this.label4);
            this.pnlUOM.Controls.Add(this.lblUOMType);
            this.pnlUOM.Controls.Add(this.lblCode);
            this.pnlUOM.Controls.Add(this.label1);
            this.pnlUOM.Controls.Add(this.shapeContainer1);
            this.pnlUOM.Location = new System.Drawing.Point(0, 30);
            this.pnlUOM.Name = "pnlUOM";
            this.pnlUOM.Size = new System.Drawing.Size(379, 291);
            this.pnlUOM.TabIndex = 0;
            // 
            // txtQuantityPerMeasurement
            // 
            this.txtQuantityPerMeasurement.BackColor = System.Drawing.SystemColors.Info;
            this.txtQuantityPerMeasurement.DecimalPlaces = 0;
            this.txtQuantityPerMeasurement.Location = new System.Drawing.Point(159, 192);
            this.txtQuantityPerMeasurement.MaxLength = 9;
            this.txtQuantityPerMeasurement.Name = "txtQuantityPerMeasurement";
            this.txtQuantityPerMeasurement.ShortcutsEnabled = false;
            this.txtQuantityPerMeasurement.Size = new System.Drawing.Size(62, 20);
            this.txtQuantityPerMeasurement.TabIndex = 4;
            this.txtQuantityPerMeasurement.Text = "1";
            this.txtQuantityPerMeasurement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQuantityPerMeasurement.Visible = false;
            this.txtQuantityPerMeasurement.TextChanged += new System.EventHandler(this.txtQuantityPerMeasurement_TextChanged);
            // 
            // txtScale
            // 
            this.txtScale.BackColor = System.Drawing.SystemColors.Info;
            this.txtScale.Location = new System.Drawing.Point(119, 89);
            this.txtScale.MaxLength = 1;
            this.txtScale.Name = "txtScale";
            this.txtScale.ShortcutsEnabled = false;
            this.txtScale.Size = new System.Drawing.Size(62, 20);
            this.txtScale.TabIndex = 5;
            this.txtScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtScale.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Scale";
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Info;
            this.txtDescription.Location = new System.Drawing.Point(119, 10);
            this.txtDescription.MaxLength = 49;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(251, 20);
            this.txtDescription.TabIndex = 0;
            this.txtDescription.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(5, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "UOM Details";
            // 
            // dgvUOM
            // 
            this.dgvUOM.AddNewRow = false;
            this.dgvUOM.AllowUserToAddRows = false;
            this.dgvUOM.AllowUserToDeleteRows = false;
            this.dgvUOM.AllowUserToResizeColumns = false;
            this.dgvUOM.AllowUserToResizeRows = false;
            this.dgvUOM.AlphaNumericCols = new int[0];
            this.dgvUOM.BackgroundColor = System.Drawing.Color.White;
            this.dgvUOM.CapsLockCols = new int[0];
            this.dgvUOM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUOM.DecimalCols = new int[0];
            this.dgvUOM.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvUOM.HasSlNo = false;
            this.dgvUOM.LastRowIndex = 0;
            this.dgvUOM.Location = new System.Drawing.Point(8, 130);
            this.dgvUOM.MultiSelect = false;
            this.dgvUOM.Name = "dgvUOM";
            this.dgvUOM.NegativeValueCols = new int[0];
            this.dgvUOM.NumericCols = new int[0];
            this.dgvUOM.RowHeadersWidth = 50;
            this.dgvUOM.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvUOM.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUOM.Size = new System.Drawing.Size(363, 157);
            this.dgvUOM.TabIndex = 7;
            this.dgvUOM.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvUOM_RowsAdded);
            this.dgvUOM.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvUOM_CellClick);
            this.dgvUOM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DgvUOM_KeyDown);
            this.dgvUOM.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvUOM_RowsRemoved);
            this.dgvUOM.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DgvUOM_KeyUp);
            // 
            // BtnUOMClass
            // 
            this.BtnUOMClass.Location = new System.Drawing.Point(339, 61);
            this.BtnUOMClass.Name = "BtnUOMClass";
            this.BtnUOMClass.Size = new System.Drawing.Size(32, 22);
            this.BtnUOMClass.TabIndex = 3;
            this.BtnUOMClass.Text = "..";
            this.BtnUOMClass.UseVisualStyleBackColor = true;
            this.BtnUOMClass.Click += new System.EventHandler(this.BtnUOMClass_Click);
            // 
            // cboUOMClass
            // 
            this.cboUOMClass.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboUOMClass.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboUOMClass.BackColor = System.Drawing.SystemColors.Info;
            this.cboUOMClass.DropDownHeight = 75;
            this.cboUOMClass.FormattingEnabled = true;
            this.cboUOMClass.IntegralHeight = false;
            this.cboUOMClass.Location = new System.Drawing.Point(119, 62);
            this.cboUOMClass.Name = "cboUOMClass";
            this.cboUOMClass.Size = new System.Drawing.Size(213, 21);
            this.cboUOMClass.TabIndex = 2;
            this.cboUOMClass.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.cboUOMClass.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(296, 89);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // txtShortName
            // 
            this.txtShortName.BackColor = System.Drawing.SystemColors.Info;
            this.txtShortName.Location = new System.Drawing.Point(119, 36);
            this.txtShortName.MaxLength = 10;
            this.txtShortName.Name = "txtShortName";
            this.txtShortName.Size = new System.Drawing.Size(213, 20);
            this.txtShortName.TabIndex = 1;
            this.txtShortName.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 195);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Quantity Per UOM";
            this.label4.Visible = false;
            // 
            // lblUOMType
            // 
            this.lblUOMType.AutoSize = true;
            this.lblUOMType.Location = new System.Drawing.Point(20, 65);
            this.lblUOMType.Name = "lblUOMType";
            this.lblUOMType.Size = new System.Drawing.Size(59, 13);
            this.lblUOMType.TabIndex = 2;
            this.lblUOMType.Text = "UOM Type";
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(20, 39);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(32, 13);
            this.lblCode.TabIndex = 1;
            this.lblCode.Text = "Code";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "UOM Name";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(379, 291);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 86;
            this.lineShape1.X2 = 368;
            this.lineShape1.Y1 = 122;
            this.lineShape1.Y2 = 121;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(215, 323);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 7;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // ErrUom
            // 
            this.ErrUom.ContainerControl = this;
            this.ErrUom.RightToLeft = true;
            // 
            // TmUOM
            // 
            this.TmUOM.Tick += new System.EventHandler(this.TmUOM_Tick);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(296, 323);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.toolStripStatusLabel11,
            this.lblUOMStatus});
            this.bar1.Location = new System.Drawing.Point(0, 350);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(379, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 81;
            this.bar1.TabStop = false;
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Text = "Status:";
            // 
            // lblUOMStatus
            // 
            this.lblUOMStatus.Name = "lblUOMStatus";
            // 
            // FrmUnitOfMeasurement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 369);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.pnlUOM);
            this.Controls.Add(this.UOMBindingNavigator);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmUnitOfMeasurement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Units of Measurement(UOM)";
            this.Load += new System.EventHandler(this.FrmUnitOfMeasurement_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmUnitOfMeasurement_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmUnitOfMeasurement_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.UOMBindingNavigator)).EndInit();
            this.UOMBindingNavigator.ResumeLayout(false);
            this.UOMBindingNavigator.PerformLayout();
            this.pnlUOM.ResumeLayout(false);
            this.pnlUOM.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrUom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator UOMBindingNavigator;
        private System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        private System.Windows.Forms.ToolStripButton btnClear;
        private System.Windows.Forms.ToolStripButton btnHelp;
        private System.Windows.Forms.Panel pnlUOM;
        private System.Windows.Forms.Label lblUOMType;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboUOMClass;
        private System.Windows.Forms.TextBox txtShortName;
        private DemoClsDataGridview.ClsDataGirdView dgvUOM;

        private System.Windows.Forms.Button BtnUOMClass;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.ErrorProvider ErrUom;
        private System.Windows.Forms.Timer TmUOM;
        private System.Windows.Forms.Label label5;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.ToolStripSplitButton BtnwebUOMReference;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ToolStripButton btnPrint;
        private System.Windows.Forms.Label label6;
        private DemoClsDataGridview.NumericTextBox txtScale;
        private System.Windows.Forms.ToolStripButton btnEmail;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel11;
        private DevComponents.DotNetBar.LabelItem lblUOMStatus;
        private NumericTextBox txtQuantityPerMeasurement;
        private System.Windows.Forms.Label label4;
    }
}