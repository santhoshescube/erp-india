﻿namespace MyBooksERP//.Settings.Forms
{
    partial class FrmConfigurationSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmConfigurationSetting));
            this.ConfigurationSettingBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.pnlConfigSetting = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.DgvConfigurationSetting = new System.Windows.Forms.DataGridView();
            this.ColConfigurationID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCofigItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColConfigValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPreDefined = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColValueLimit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDefaultValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LblWarning = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.TmConfig = new System.Windows.Forms.Timer(this.components);
            this.toolTipConfigurationSetting = new System.Windows.Forms.ToolTip(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.toolStripStatusLabel11 = new DevComponents.DotNetBar.LabelItem();
            this.LblConfiguration = new DevComponents.DotNetBar.LabelItem();
            ((System.ComponentModel.ISupportInitialize)(this.ConfigurationSettingBindingNavigator)).BeginInit();
            this.ConfigurationSettingBindingNavigator.SuspendLayout();
            this.pnlConfigSetting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvConfigurationSetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // ConfigurationSettingBindingNavigator
            // 
            this.ConfigurationSettingBindingNavigator.AddNewItem = null;
            this.ConfigurationSettingBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.ConfigurationSettingBindingNavigator.CountItem = null;
            this.ConfigurationSettingBindingNavigator.DeleteItem = null;
            this.ConfigurationSettingBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorSaveItem,
            this.BtnHelp});
            this.ConfigurationSettingBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.ConfigurationSettingBindingNavigator.MoveFirstItem = null;
            this.ConfigurationSettingBindingNavigator.MoveLastItem = null;
            this.ConfigurationSettingBindingNavigator.MoveNextItem = null;
            this.ConfigurationSettingBindingNavigator.MovePreviousItem = null;
            this.ConfigurationSettingBindingNavigator.Name = "ConfigurationSettingBindingNavigator";
            this.ConfigurationSettingBindingNavigator.PositionItem = null;
            this.ConfigurationSettingBindingNavigator.Size = new System.Drawing.Size(484, 25);
            this.ConfigurationSettingBindingNavigator.TabIndex = 36;
            this.ConfigurationSettingBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(51, 22);
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // pnlConfigSetting
            // 
            this.pnlConfigSetting.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlConfigSetting.Controls.Add(this.label3);
            this.pnlConfigSetting.Controls.Add(this.DgvConfigurationSetting);
            this.pnlConfigSetting.Controls.Add(this.label2);
            this.pnlConfigSetting.Controls.Add(this.label1);
            this.pnlConfigSetting.Controls.Add(this.LblWarning);
            this.pnlConfigSetting.Location = new System.Drawing.Point(7, 28);
            this.pnlConfigSetting.Name = "pnlConfigSetting";
            this.pnlConfigSetting.Size = new System.Drawing.Size(469, 242);
            this.pnlConfigSetting.TabIndex = 37;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 221);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = " to restore the default values.";
            // 
            // DgvConfigurationSetting
            // 
            this.DgvConfigurationSetting.AllowUserToAddRows = false;
            this.DgvConfigurationSetting.AllowUserToDeleteRows = false;
            this.DgvConfigurationSetting.BackgroundColor = System.Drawing.Color.White;
            this.DgvConfigurationSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvConfigurationSetting.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColConfigurationID,
            this.ColCofigItem,
            this.ColConfigValue,
            this.ColPreDefined,
            this.ColValueLimit,
            this.ColDefaultValue});
            this.DgvConfigurationSetting.Location = new System.Drawing.Point(5, 4);
            this.DgvConfigurationSetting.MultiSelect = false;
            this.DgvConfigurationSetting.Name = "DgvConfigurationSetting";
            this.DgvConfigurationSetting.RowHeadersVisible = false;
            this.DgvConfigurationSetting.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DgvConfigurationSetting.Size = new System.Drawing.Size(459, 155);
            this.DgvConfigurationSetting.TabIndex = 12;
            this.DgvConfigurationSetting.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvConfigurationSetting_CellValueChanged);
            this.DgvConfigurationSetting.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DgvConfigurationSetting_CellBeginEdit);
            this.DgvConfigurationSetting.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvConfigurationSetting_RowEnter);
            this.DgvConfigurationSetting.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvConfigurationSetting_CellEndEdit);
            this.DgvConfigurationSetting.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvConfigurationSetting_CellClick);
            this.DgvConfigurationSetting.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DgvConfigurationSetting_EditingControlShowing);
            this.DgvConfigurationSetting.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvConfigurationSetting_CellContentClick);
            // 
            // ColConfigurationID
            // 
            this.ColConfigurationID.HeaderText = "ConfigurationID";
            this.ColConfigurationID.Name = "ColConfigurationID";
            this.ColConfigurationID.Visible = false;
            // 
            // ColCofigItem
            // 
            this.ColCofigItem.HeaderText = "Configuration Item";
            this.ColCofigItem.MaxInputLength = 50;
            this.ColCofigItem.Name = "ColCofigItem";
            this.ColCofigItem.ReadOnly = true;
            this.ColCofigItem.Width = 180;
            // 
            // ColConfigValue
            // 
            this.ColConfigValue.HeaderText = "Configuration Value";
            this.ColConfigValue.MaxInputLength = 100;
            this.ColConfigValue.Name = "ColConfigValue";
            this.ColConfigValue.Width = 300;
            // 
            // ColPreDefined
            // 
            this.ColPreDefined.HeaderText = "PreDefined";
            this.ColPreDefined.Name = "ColPreDefined";
            this.ColPreDefined.Visible = false;
            // 
            // ColValueLimit
            // 
            this.ColValueLimit.HeaderText = "ValueLimit";
            this.ColValueLimit.Name = "ColValueLimit";
            this.ColValueLimit.Visible = false;
            // 
            // ColDefaultValue
            // 
            this.ColDefaultValue.HeaderText = "DefaultValue";
            this.ColDefaultValue.Name = "ColDefaultValue";
            this.ColDefaultValue.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 203);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(396, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = " effect the performance and may result in abnormal behavior. Use the Reset button" +
                "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 186);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(423, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "These settings are used for configuring the system. Changing these setting may in" +
                "versely";
            // 
            // LblWarning
            // 
            this.LblWarning.AutoSize = true;
            this.LblWarning.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblWarning.Location = new System.Drawing.Point(8, 165);
            this.LblWarning.Name = "LblWarning";
            this.LblWarning.Size = new System.Drawing.Size(60, 13);
            this.LblWarning.TabIndex = 9;
            this.LblWarning.Text = "Warning :";
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(7, 276);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 11;
            this.btnReset.Text = "&Reset";
            this.toolTipConfigurationSetting.SetToolTip(this.btnReset, "Restoring the default values");
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(322, 276);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 38;
            this.BtnOk.Text = "&Ok";
            this.toolTipConfigurationSetting.SetToolTip(this.BtnOk, "Save and close");
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(401, 276);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 39;
            this.btnCancel.Text = "&Cancel";
            this.toolTipConfigurationSetting.SetToolTip(this.btnCancel, "Close");
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ConfigurationID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Configuration Item";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 50;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 300;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Configuration Value";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 300;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "PreDefined";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "ValueLimit";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "DefaultValue";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.toolStripStatusLabel11,
            this.LblConfiguration});
            this.bar1.Location = new System.Drawing.Point(0, 306);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(484, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 81;
            this.bar1.TabStop = false;
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Text = "Status:";
            // 
            // LblConfiguration
            // 
            this.LblConfiguration.Name = "LblConfiguration";
            // 
            // FrmConfigurationSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 325);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.pnlConfigSetting);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.ConfigurationSettingBindingNavigator);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConfigurationSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuration Settings";
            this.Load += new System.EventHandler(this.FrmConfigurationSetting_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmConfigurationSetting_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.ConfigurationSettingBindingNavigator)).EndInit();
            this.ConfigurationSettingBindingNavigator.ResumeLayout(false);
            this.ConfigurationSettingBindingNavigator.PerformLayout();
            this.pnlConfigSetting.ResumeLayout(false);
            this.pnlConfigSetting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvConfigurationSetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator ConfigurationSettingBindingNavigator;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        private System.Windows.Forms.Panel pnlConfigSetting;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridView DgvConfigurationSetting;
        private System.Windows.Forms.Timer TmConfig;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LblWarning;
        private System.Windows.Forms.ToolTip toolTipConfigurationSetting;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel11;
        private DevComponents.DotNetBar.LabelItem LblConfiguration;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColConfigurationID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCofigItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColConfigValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPreDefined;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColValueLimit;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDefaultValue;
    }
}