﻿namespace MyBooksERP
{
    partial class FrmCompanySettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCompanySettings));
            this.CompSettingsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.CompSettingsBindingNavigatorSaveButton = new System.Windows.Forms.ToolStripButton();
            this.ResetButton = new System.Windows.Forms.ToolStripButton();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.TmCompSettings = new System.Windows.Forms.Timer(this.components);
            this.pnlForm = new System.Windows.Forms.Panel();
            this.CboCompanyName = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.PrgCompanySettings = new System.Windows.Forms.PropertyGrid();
            this.BtnOk = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnReset = new System.Windows.Forms.Button();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.StatusLabel = new DevComponents.DotNetBar.LabelItem();
            this.lblCompSettingsStatus = new DevComponents.DotNetBar.LabelItem();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.CompSettingsBindingNavigator)).BeginInit();
            this.CompSettingsBindingNavigator.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // CompSettingsBindingNavigator
            // 
            this.CompSettingsBindingNavigator.AddNewItem = null;
            this.CompSettingsBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.CompSettingsBindingNavigator.CountItem = null;
            this.CompSettingsBindingNavigator.DeleteItem = null;
            this.CompSettingsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CompSettingsBindingNavigatorSaveButton,
            this.ResetButton,
            this.BtnPrint,
            this.BtnEmail,
            this.BtnHelp});
            this.CompSettingsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.CompSettingsBindingNavigator.MoveFirstItem = null;
            this.CompSettingsBindingNavigator.MoveLastItem = null;
            this.CompSettingsBindingNavigator.MoveNextItem = null;
            this.CompSettingsBindingNavigator.MovePreviousItem = null;
            this.CompSettingsBindingNavigator.Name = "CompSettingsBindingNavigator";
            this.CompSettingsBindingNavigator.PositionItem = null;
            this.CompSettingsBindingNavigator.Size = new System.Drawing.Size(480, 25);
            this.CompSettingsBindingNavigator.TabIndex = 0;
            this.CompSettingsBindingNavigator.Text = "bindingNavigator1";
            // 
            // CompSettingsBindingNavigatorSaveButton
            // 
            this.CompSettingsBindingNavigatorSaveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CompSettingsBindingNavigatorSaveButton.Image = ((System.Drawing.Image)(resources.GetObject("CompSettingsBindingNavigatorSaveButton.Image")));
            this.CompSettingsBindingNavigatorSaveButton.Name = "CompSettingsBindingNavigatorSaveButton";
            this.CompSettingsBindingNavigatorSaveButton.Size = new System.Drawing.Size(23, 22);
            this.CompSettingsBindingNavigatorSaveButton.Text = "Save";
            this.CompSettingsBindingNavigatorSaveButton.Click += new System.EventHandler(this.CompSettingsBindingNavigatorSaveButton_Click);
            // 
            // ResetButton
            // 
            this.ResetButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ResetButton.Image = global::MyBooksERP.Properties.Resources.Rest1;
            this.ResetButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(23, 22);
            this.ResetButton.Text = "Reset";
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.ToolTipText = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // TmCompSettings
            // 
            this.TmCompSettings.Tick += new System.EventHandler(this.TmCompSettings_Tick);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.CboCompanyName);
            this.pnlForm.Controls.Add(this.label1);
            this.pnlForm.Controls.Add(this.PrgCompanySettings);
            this.pnlForm.Location = new System.Drawing.Point(4, 27);
            this.pnlForm.Name = "pnlForm";
            this.pnlForm.Size = new System.Drawing.Size(473, 352);
            this.pnlForm.TabIndex = 2;
            // 
            // CboCompanyName
            // 
            this.CboCompanyName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompanyName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompanyName.Cursor = System.Windows.Forms.Cursors.Default;
            this.CboCompanyName.FormattingEnabled = true;
            this.CboCompanyName.Location = new System.Drawing.Point(92, 11);
            this.CboCompanyName.Name = "CboCompanyName";
            this.CboCompanyName.Size = new System.Drawing.Size(371, 21);
            this.CboCompanyName.TabIndex = 0;
            this.CboCompanyName.SelectionChangeCommitted += new System.EventHandler(this.CboCompanyName_SelectionChangeCommitted);
            this.CboCompanyName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CmbCompanyName_KeyPress);
            this.CboCompanyName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboCompanyName_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Company";
            // 
            // PrgCompanySettings
            // 
            this.PrgCompanySettings.AllowDrop = true;
            this.PrgCompanySettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrgCompanySettings.Location = new System.Drawing.Point(5, 38);
            this.PrgCompanySettings.Name = "PrgCompanySettings";
            this.PrgCompanySettings.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.PrgCompanySettings.Size = new System.Drawing.Size(459, 311);
            this.PrgCompanySettings.TabIndex = 1;
            this.PrgCompanySettings.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.PrgCompanySettings_PropertyValueChanged);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(311, 383);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 2;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(392, 383);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 4;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnReset
            // 
            this.BtnReset.Location = new System.Drawing.Point(9, 382);
            this.BtnReset.Name = "BtnReset";
            this.BtnReset.Size = new System.Drawing.Size(75, 23);
            this.BtnReset.TabIndex = 3;
            this.BtnReset.Text = "&Reset";
            this.BtnReset.UseVisualStyleBackColor = true;
            this.BtnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.StatusLabel,
            this.lblCompSettingsStatus});
            this.bar1.Location = new System.Drawing.Point(0, 413);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(480, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 18;
            this.bar1.TabStop = false;
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Text = "Status:";
            // 
            // lblCompSettingsStatus
            // 
            this.lblCompSettingsStatus.Name = "lblCompSettingsStatus";
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // FrmCompanySettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 432);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.BtnReset);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.pnlForm);
            this.Controls.Add(this.CompSettingsBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(496, 468);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(496, 468);
            this.Name = "FrmCompanySettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Company Settings";
            this.Load += new System.EventHandler(this.FrmCompSettings_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCompSettings_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmCompanySettings_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.CompSettingsBindingNavigator)).EndInit();
            this.CompSettingsBindingNavigator.ResumeLayout(false);
            this.CompSettingsBindingNavigator.PerformLayout();
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator CompSettingsBindingNavigator;
        private System.Windows.Forms.Timer TmCompSettings;
        internal System.Windows.Forms.ToolStripButton CompSettingsBindingNavigatorSaveButton;
        private System.Windows.Forms.Panel pnlForm;
        private System.Windows.Forms.PropertyGrid PrgCompanySettings;
        private System.Windows.Forms.ComboBox CboCompanyName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnReset;
        private System.Windows.Forms.ToolStripButton ResetButton;
        private System.Windows.Forms.ToolStripButton BtnPrint;
        private System.Windows.Forms.ToolStripButton BtnEmail;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem StatusLabel;
        private DevComponents.DotNetBar.LabelItem lblCompSettingsStatus;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
    }
}