﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using MyBooksERP;
 
class ClsCompanyList
{
    /* ===============================================
  Author:		<Author,,Sawmya,Devi>
  Modified by:  <Arun-24 Feb 2011>
  Create date: <Create Date,,23 Feb 2011>
  Description:	<Description,,For Setting properties in the  Property Grid for Company Settings Form->
================================================*/
    
    string sAdvanced = "";
    //string sPurchaseIndent = "";
    string sPurchaseInvoice = "";
    string sPurchaseOrder = "";
    string sSalesInvoice = "";
    string sSalesQuotation = "";
    string sSalesOrder = "";
    string sSalesReturn = "";
    string sPointOfSale = "";
    string sPurchaseQutation = "";
    string sGoodsReceiptNote="";
    //string sMaterialReturn = "";
    string sPurchaseReturn = "";
    string sPayments = "";
    string sReceipt = "";
    string sItemIssue="";
    //string sMaterialIssue = "";
    string sCustomerCode = "";
    string sSupplierCode = "";
    string sProductCode = "";
    string sProductGroupCode = "";
    string sEmployeeNumber = "";
    string sStockAdjustmentNumber = "";
    //string sRFQNumber = "";
    //string sProjectsNumber = "";
    string sStockTransferNumber = "";
    //string sWarehouse = "";
    string sAsset = "";
    string sLiability = "";
    string sIncome = "";
    string sExpense = "";
    //---------------------------------------------------------------------------------
    [TypeConverter(typeof(TestStringConverter)), CategoryAttribute("Company Settings")]
    [DisplayName("Advanced")]
    public string Advanced
    {
        get { return sAdvanced; }
        set { sAdvanced = value; }
    }
    //------------------------------------- Create/Modify
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Customer Code")]
    public string CustomerCode
    {
        get { return sCustomerCode; }
        set { sCustomerCode = value; }
    }

    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Supplier Code")]
    public string SupplierCode
    {
        get { return sSupplierCode; }
        set { sSupplierCode = value; }
    }    
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Employee Number")]
    public string EmployeeNumber
    {
        get { return sEmployeeNumber; }
        set { sEmployeeNumber = value; }
    }
    //[CategoryAttribute("Company Settings")]
    //[TypeConverter(typeof(TestStringConverter)), DisplayName("Projects")]
    //public string ProjectsNumber
    //{
    //    get { return sProjectsNumber; }
    //    set { sProjectsNumber = value; }
    //}

    //[CategoryAttribute("Company Settings")]
    //[TypeConverter(typeof(TestStringConverter)), DisplayName("WareHouse")]
    //public string WarehousePrefix
    //{
    //    get { return sWarehouse; }
    //    set { sWarehouse = value; }
    //}
    //-------------------------------------Purchase
    //[CategoryAttribute("Company Settings")]
    //[TypeConverter(typeof(TestStringConverter)), DisplayName("Purchase Indent")]
    //public string PurchaseIndent
    //{
    //    get { return sPurchaseIndent; }
    //    set { sPurchaseIndent = value; }
    //}
    //[CategoryAttribute("Company Settings")]
    //[TypeConverter(typeof(TestStringConverter)), DisplayName("RFQ")]
    //public string RFQNumber
    //{
    //    get { return sRFQNumber; }
    //    set { sRFQNumber = value; }
    //}
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Purchase Order")]
    public string PurchaseOrder
    {
        get { return sPurchaseOrder; }
        set { sPurchaseOrder = value; }
    }
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Purchase Quotation")]
    public string PurchaseQutation
    {
        get { return sPurchaseQutation; }
        set { sPurchaseQutation = value; }
    }    
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Purchase Invoice")]
    public string PurchaseInvoice
    {
        get { return sPurchaseInvoice; }
        set { sPurchaseInvoice = value; }
    }
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Goods Receipt Note")]
    public string GoodsReceiptNote
    {
        get { return sGoodsReceiptNote; }
        set { sGoodsReceiptNote = value; }
    }
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Debit Note")]
    public string DebitNote
    {
        get { return sPurchaseReturn; }
        set { sPurchaseReturn = value; }
    }
    //-------------------------------------Sales
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Sales Order")]
    public string SalesOrder
    {
        get { return sSalesOrder; }
        set { sSalesOrder = value; }
    }
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Sales Quotation")]
    public string SalesQuotation
    {
        get { return sSalesQuotation; }
        set { sSalesQuotation = value; }
    }
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Sales Invoice")]
    public string SalesInvoice
    {
        get { return sSalesInvoice; }
        set { sSalesInvoice = value; }
    }
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Point Of Sale")]
    public string PointOfSale
    {
        get { return sPointOfSale; }
        set { sPointOfSale = value; }
    }
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Credit Note")]
    public string SalesReturn
    {
        get { return sSalesReturn; }
        set { sSalesReturn = value; }
    }
    //-------------------------------------Delivery
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Delivery Note")]
    public string ItemIssue
    {
        get { return sItemIssue; }
        set { sItemIssue = value; }
    }
    //-------------------------------------Inventory
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Product Code")]
    public string ProductCode
    {
        get { return sProductCode; }
        set { sProductCode = value; }
    }
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Product Assembly Code")]
    public string ProductGroupCode
    {
        get { return sProductGroupCode; }
        set { sProductGroupCode = value; }
    }
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Stock Adjustment Number")]
    public string StockAdjustmentNumber
    {
        get { return sStockAdjustmentNumber; }
        set { sStockAdjustmentNumber = value; }
    }
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Transfer Order")]
    public string StockTransferNumber
    {
        get { return sStockTransferNumber; }
        set { sStockTransferNumber = value; }
    }
    //-------------------------------------Task
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Payments")]
    public string Payments 
    {
        get { return sPayments; }
        set { sPayments = value; }
    }
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Receipts")]
    public string Receipt
    {
        get { return sReceipt; }
        set { sReceipt = value; }
    }
    //-------------------------------------Accounts
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Asset")]
    public string Asset
    {
        get { return sAsset; }
        set { sAsset = value; }
    }
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Liability")]
    public string Liability
    {
        get { return sLiability; }
        set { sLiability = value; }
    }
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Income")]
    public string Income
    {
        get { return sIncome; }
        set { sIncome = value; }
    }
    [CategoryAttribute("Company Settings")]
    [TypeConverter(typeof(TestStringConverter)), DisplayName("Expense")]
    public string Expense
    {
        get { return sExpense; }
        set { sExpense = value; }
    }
    //[CategoryAttribute("Company Settings")]
    //[TypeConverter(typeof(TestStringConverter)), DisplayName("Material Return")]
    //public string MaterialReturn
    //{
    //    get { return sMaterialReturn; }
    //    set { sMaterialReturn = value; }
    //}

    //[CategoryAttribute("Company Settings")]
    //[TypeConverter(typeof(TestStringConverter)), DisplayName("Material Issue")]
    //public string MaterialIssue
    //{
    //    get { return sMaterialIssue; }
    //    set { sMaterialIssue = value; }
    //}
    //-------------------------------------Settings
    //[CategoryAttribute("Company Settings")]
    //[TypeConverter(typeof(TestStringConverter)), DisplayName("Report Settings")]
    //public string ReportSettings
    //{
    //    get { return sReportSettings; }
    //    set { sReportSettings = value; }
    //}
    /// <summary>
    /// Class for converting the modules and menu list as expandable list
    /// </summary>
    public class TestStringConverter : TypeConverter
    {
        public PropertyDescriptorCollection myCollection = new PropertyDescriptorCollection(null);
        public static PropertyDescriptorCollection col = new PropertyDescriptorCollection(null);
        public ITypeDescriptorContext context = null;

        ClsBLLCompanySettings MObjClsBLLCompanySettings = new ClsBLLCompanySettings();

        #region Methods

        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
        {
            if (context != null)
            {
                this.myCollection.Clear();  // First clears the properties

                //int iID = 0;    // Module/menu identification number                               
                int iMode = 6; //SELECTING SUBITEM UNDER CONFIGURATIONITEM
                string sConfigurationItem;

                TestStringDescriptor mObjTestStringDescriptor = null;
                PropertyDescriptor mObjPropertyDescriptor = null;
                sConfigurationItem = context.PropertyDescriptor.DisplayName;

                if (context.PropertyDescriptor.GetType() == typeof(TestStringDescriptor))
                {
                    mObjTestStringDescriptor = (TestStringDescriptor)context.PropertyDescriptor;
                    iMode = mObjTestStringDescriptor.Level;
                    sConfigurationItem = mObjTestStringDescriptor.ConfigurationItem;
                }

                //Looping through the datatable of modules and menu list
                DataTable dattemp = MObjClsBLLCompanySettings.DisplayCompanysettings(clsDTOCompanySettings.intCompanyID, iMode, sConfigurationItem);
                foreach (DataRow dRow in MObjClsBLLCompanySettings.DisplayCompanysettings(clsDTOCompanySettings.intCompanyID, iMode, sConfigurationItem).Rows)
                {
                    if (Convert.ToInt32(dRow["Level"]) == 3)
                    {
                        mObjPropertyDescriptor = new TestStringDescriptor(Convert.ToString(dRow["ConfigurationItem"]), context,
                            new Attribute[] { new DescriptionAttribute(Convert.ToString(dRow["Remarks"])),
                                new DefaultValueAttribute(Convert.ToString(dRow["DefaultValue"])) },
                              Convert.ToInt32(dRow["CompanySettingsID"]), (Convert.ToString(dRow["ConfigurationValue"]).ToUpper()),
                              Convert.ToInt32(dRow["Level"]), false, Convert.ToString(dRow["DefaultValue"]));
                    }
                    else if (Convert.ToInt32(dRow["Level"]) == 4)
                    {
                        mObjPropertyDescriptor = new TestStringDescriptor(Convert.ToString(dRow["ConfigurationItem"]), context,
                           new Attribute[] { new TypeConverterAttribute(typeof(field)), new DescriptionAttribute(Convert.ToString(dRow["Remarks"])),
                           new DefaultValueAttribute(Convert.ToString(dRow["DefaultValue"])) },
                             Convert.ToInt32(dRow["CompanySettingsID"]), Convert.ToString(dRow["ConfigurationValue"]),
                             Convert.ToInt32(dRow["Level"]), false, Convert.ToString(dRow["DefaultValue"]));
                    }
                    else if (Convert.ToInt32(dRow["Level"]) == 5)
                    {
                        mObjPropertyDescriptor = new TestStringDescriptor(Convert.ToString(dRow["ConfigurationItem"]), context,
                           new Attribute[] { new TypeConverterAttribute(typeof(settings)), new DescriptionAttribute(Convert.ToString(dRow["Remarks"])),
                           new DefaultValueAttribute(Convert.ToString(dRow["DefaultValue"])) },
                             Convert.ToInt32(dRow["CompanySettingsID"]), Convert.ToString(dRow["ConfigurationValue"]),
                             Convert.ToInt32(dRow["Level"]), false, Convert.ToString(dRow["DefaultValue"]));

                    }
                    else if (Convert.ToInt32(dRow["Level"]) == 6)//for showing combobox in installments in collection "Delivery"
                    {
                        mObjPropertyDescriptor = new TestStringDescriptor(Convert.ToString(dRow["ConfigurationItem"]), context,
                           new Attribute[] { new TypeConverterAttribute(typeof(SettingField)), new DescriptionAttribute(Convert.ToString(dRow["Remarks"])),
                           new DefaultValueAttribute(Convert.ToString(dRow["DefaultValue"])) },
                             Convert.ToInt32(dRow["CompanySettingsID"]), Convert.ToString(dRow["ConfigurationValue"]),
                             Convert.ToInt32(dRow["Level"]), false, Convert.ToString(dRow["DefaultValue"]));

                    }
                    // Adding the property into the root
                    myCollection.Add(mObjPropertyDescriptor);
                    if (!col.Contains(mObjPropertyDescriptor))
                        col.Add(mObjPropertyDescriptor);
                    //if (!ClsCompSettings.collection.Contains(mObjPropertyDescriptor))
                  clsDTOCompanySettings.collection.Add(mObjPropertyDescriptor);
                }
                return myCollection;
            }

            return base.GetProperties(context, value, attributes);
        }

        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
        {
            if (context != null)
            {
                return true;
            }
            return base.GetPropertiesSupported(context);
        }

        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return false;
        }

        public override bool GetCreateInstanceSupported(ITypeDescriptorContext context)
        {
            return base.GetCreateInstanceSupported(context);
        }

        #endregion

        /// <summary>
        /// Class for the single property in the property grid
        /// </summary>
        class TestStringDescriptor : SimplePropertyDescriptor
        {
            ITypeDescriptorContext fContext;
            int iSettingsID;
            int iProductId;
            int iValueLimt;
            int iLevel;

            string iParentItem;
            string sConfigurationItem;
            string sConfigurationValue;
            string sDefaultValue;
            bool bPredefined;
            bool bReadOnly = false;
            //bool bBoldText = false;

            PropertyDescriptorCollection collection = new PropertyDescriptorCollection(null);

            // Creating new property of module and menu
            public TestStringDescriptor(string ConfigurationItem, ITypeDescriptorContext Context, Attribute[] Attributes,
                int SettingsID, string ConfigurationValue, int Level, bool ReadOnly, string DefaultValue)
                : base(typeof(string), ConfigurationItem, typeof(string), Attributes)
            {
                fContext = Context;
                this.iSettingsID = SettingsID;
                this.sConfigurationItem = ConfigurationItem;
                this.ConfigurationValue = ConfigurationValue;
                this.bReadOnly = ReadOnly;
                this.iLevel = Level;
                this.sDefaultValue = DefaultValue;

                AddCollection();    // Adding the property into its parent module
            }

            /// <summary>
            /// Adding a property into its parent module/menu
            /// </summary>
            public void AddCollection()
            {
                if (fContext.PropertyDescriptor.GetType() == typeof(TestStringDescriptor))
                {
                    TestStringDescriptor property = (TestStringDescriptor)fContext.PropertyDescriptor;
                    property.collection.Add(this);
                }
            }

            /// <summary>
            /// Readonly peroperty setting of module/menu/field
            /// </summary>
            public override bool IsReadOnly
            {
                get
                {
                    return bReadOnly;
                }
            }

            public override object GetValue(object component)
            {
                return this.sConfigurationValue; // Getting selected value(permission) of module/menu/field
            }

            public override void SetValue(object component, object value)
            {
                this.ConfigurationValue = value.ToString();
                //throw new NotImplementedException();
            }

            //public override void ResetValue(object component)
            //{
            //    base.ResetValue(component);
            //}

            public override bool ShouldSerializeValue(object component)
            {
                if (DefaultValue.Trim().Equals(ConfigurationValue.Trim()))
                {
                    return false;
                }
                else
                {
                    return true;
                }
                //return base.ShouldSerializeValue(component);
            }

            #region Public properties

            public int SettingsID
            {
                get { return iSettingsID; }
                set { iSettingsID = value; }
            }

            public int ProductId
            {
                get { return iProductId; }
                set { iProductId = value; }
            }

            public int ValueLimt
            {
                get { return iValueLimt; }
                set { iValueLimt = value; }
            }

            public string ConfigurationItem
            {
                get { return sConfigurationItem; }
                set { sConfigurationItem = value; }
            }

            public string ConfigurationValue
            {
                get { return sConfigurationValue; }
                set { sConfigurationValue = value; }
            }

            public string DefaultValue
            {
                get { return sDefaultValue; }
                set { sDefaultValue = value; }
            }

            public bool Predefined
            {
                get { return bPredefined; }
                set { bPredefined = value; }
            }

            public int Level
            {
                get { return iLevel; }
                set { iLevel = value; }
            }

            public string ParentItem
            {
                get { return iParentItem; }
                set { iParentItem = value; }
            }

            public PropertyDescriptorCollection Collection
            {
                get { return collection; }
                set { collection = value; }
            }

            #endregion // Public properties

            public override PropertyDescriptorCollection GetChildProperties(object instance, Attribute[] filter)
            {
                return base.GetChildProperties(instance, filter);
            }
        }
        // CLASS FOR INHERTING A COMBOBOX 
        public class settings : StringConverter
        {
            string[] _strvalue = new string[] { "Yes", "No" };
            public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
            {
                return new StandardValuesCollection(_strvalue);
            }

            public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
            {
                return true;
            }

            public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
            {
                return true;
            }
        }

        // CLASS FOR INHERTING A COMBOBOX 
        public class field : StringConverter
        {
            string[] _strvalue = new string[] { "True", "False" };
            public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
            {
                return new StandardValuesCollection(_strvalue);
            }

            public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
            {
                return true;
            }

            public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
            {
                return true;
            }
        }

        public class SettingField : StringConverter//for showing Combobox 
        {
            string[] _strvalue = new string[] { "Delivery", "Invoice" };
            public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
            {
                return new StandardValuesCollection(_strvalue);
            }

            public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
            {
                return true;
            }

            public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
            {
                return true;
            }
        }

        public void ShowPropertyGrid(PropertyDescriptor objPropertyDescriptor)
        {
            foreach (PropertyDescriptor d in clsDTOCompanySettings.collection)
            {
                TestStringDescriptor objTestStringDescriptor = (TestStringDescriptor)d;
                string strConfigValue = "";
                strConfigValue = objTestStringDescriptor.ConfigurationValue;

                if (objTestStringDescriptor.ConfigurationItem == "Prefix")
                {
                    if (strConfigValue.Length > 10)
                        strConfigValue = strConfigValue.Remove(10);
                }
                MObjClsBLLCompanySettings.SavePro(objTestStringDescriptor.SettingsID, clsDTOCompanySettings.intCompanyID, strConfigValue);
                //MObjClsBLLCompanySettings.SavePro(objTestStringDescriptor.SettingsID, clsDTOCompanySettings.intCompanyID, objTestStringDescriptor.ConfigurationValue);

                //ArrayList prmCompany = new ArrayList();
                //prmCompany.Add(new SqlParameter("@Mode", 3));  // SAVE USING PROPERTYDESCRIPTOR

                //prmCompany.Add(new SqlParameter("@SettingsID", objTestStringDescriptor.SettingsID));
                //prmCompany.Add(new SqlParameter("@CompanyID", ClsCompSettings.CompanyID));
                //prmCompany.Add(new SqlParameter("@ConfigurationValue", objTestStringDescriptor.ConfigurationValue));
                ////prmCompany.Add(new SqlParameter("@ConfigurationValue",
                ////(objTestStringDescriptor.Level == 4 ? objTestStringDescriptor.ConfigurationValue : objTestStringDescriptor.ConfigurationValue.ToUpper())));

                //MObjCompSettings.save(prmCompany);
            }
        }
    }

    public void SavePropertGrid(PropertyDescriptor objPropertyDescriptor)
    {
        TestStringConverter objTestStringConverter = new TestStringConverter();
        objTestStringConverter.ShowPropertyGrid(objPropertyDescriptor);
    }
}

