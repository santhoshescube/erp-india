﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;

class ClsCommonRef
{
        private string sField;
        private string sTableName;
        private string sCondition;       

        clsConnection MobjCommon;

        public ClsCommonRef()
        {
            MobjCommon = new clsConnection();
        }
    
        public string Field
        {
            get { return sField; }
            set { sField = value; }
        }
       
        public string TableName
        {
            get { return sTableName; }

            set { sTableName = value; }

        }
        public string Condition
        {
            get { return sCondition; }

            set { sCondition = value; }

        }
        public DataTable DisplayInformation()
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 0));
            prmCommon.Add(new SqlParameter("@Fields", sField));
            prmCommon.Add(new SqlParameter("@TableName", sTableName));
            prmCommon.Add(new SqlParameter("@Condition", sCondition));
            return MobjCommon.FillDataSet("CommonReferenceTransaction", prmCommon).Tables[0];

        }
       
        public bool SaveInformation(System.Windows.Forms.DataGridViewRow grdRow,out int iPmId)
        {
            int iMode;
            string sFiledTemp;
            string sFieldValues;
            string[] sValueSpilt;
            string[] sFiledSpilt;
            
            ArrayList prmCommon;
            iPmId = 0;
            if (grdRow.Cells.Count > 0)
            {

                sFieldValues = "";
                prmCommon = new ArrayList();
                if (Convert.ToString(grdRow.Cells[0].Value).Trim() == "")
                {
                    iMode = 1;
                    for (int i = 1; i < grdRow.Cells.Count; i++)
                    {
                        if (grdRow.Cells[i].ValueType.ToString() == "System.String")
                            sFieldValues += "'" + Convert.ToString(grdRow.Cells[i].Value).Replace("'", "’").Replace(",","¸").Trim() + "',";
                        else
                            sFieldValues += Convert.ToString(grdRow.Cells[i].Value).Replace("'", "").Replace(",", "¸").Trim() + ",";
                    }
                    if (sField != "")
                        sField = sField.Remove(0, sField.IndexOf(',') + 1);

                    sFiledSpilt = sField.Split(',');
                    sFiledTemp = sField;
                    sField = "";
                    for (int i = 0; i < sFiledSpilt.Length; i++)
                    {
                        if (sFiledSpilt[i].IndexOf("As") != -1)
                            sField += sFiledSpilt[i].Substring(0, sFiledSpilt[i].IndexOf("As")).Trim() + ",";
                        else
                            sField += sFiledSpilt[i] + ",";
                    }
                    if (sField != "")
                        sField = sField.Substring(0, sField.Length - 1).Trim();
                    else
                    {
                        if (sFiledTemp.IndexOf("As") != -1)
                            sField = sFiledTemp.Substring(0, sFiledTemp.IndexOf("As")).Trim();
                    }
                    if (sFieldValues != "")
                    {
                        sFieldValues = sFieldValues.Substring(0, sFieldValues.Length - 1);

                        sFieldValues = sFieldValues.Replace("False", "0").Replace("false", "0").Replace("True", "1").Replace("true", "1").Trim();
                    }
                    prmCommon.Add(new SqlParameter("@Mode", iMode));
                    prmCommon.Add(new SqlParameter("@Fields", sField));
                    prmCommon.Add(new SqlParameter("@FieldValue", sFieldValues));
                    prmCommon.Add(new SqlParameter("@TableName", sTableName));
                    SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
                    objParam.Direction = ParameterDirection.ReturnValue;
                    prmCommon.Add(objParam);
                    if (MobjCommon.ExecutesQuery(prmCommon, "CommonReferenceTransaction", out iPmId))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    iMode = 2;  //update
                    for (int i = 0; i < grdRow.Cells.Count; i++)
                    {
                        if (grdRow.Cells[i].ValueType.ToString() == "System.String")
                            sFieldValues += "'" + Convert.ToString(grdRow.Cells[i].Value).Replace("'", "’").Replace(",", "¸").Trim() + "',";
                        else
                            sFieldValues += Convert.ToString(grdRow.Cells[i].Value).Replace("'", "’").Replace(",", "¸").Trim() + ",";
                        
                    }
                    if (sFieldValues != "")
                      sFieldValues = sFieldValues.Substring(0, sFieldValues.Length - 1);

                    sFiledSpilt = sField.Split(',');
                    sValueSpilt = sFieldValues.Split(',');
                    sFieldValues = "";
                    for (int i = 1; i < sFiledSpilt.Length; i++)
                    {
                        sValueSpilt[i] = sValueSpilt[i].Replace("False", "0").Replace("false", "0").Replace("True", "1").Replace("true", "1").Trim();
                        if (sFiledSpilt[i].IndexOf("As") != -1)
                            sFiledSpilt[i] = sFiledSpilt[i].Substring(0, sFiledSpilt[i].IndexOf("As")).Trim();

                        sFieldValues += sFiledSpilt[i] + "=" + sValueSpilt[i].Trim() + ",";
                    }
                    sField = sFiledSpilt[0] + "=" + sValueSpilt[0].Trim();
                    if (sFieldValues != "")
                        sFieldValues = sFieldValues.Substring(0, sFieldValues.Length - 1);

                    prmCommon.Add(new SqlParameter("@Mode", iMode));
                    prmCommon.Add(new SqlParameter("@Fields", sField));
                    prmCommon.Add(new SqlParameter("@FieldValue", sFieldValues));
                    prmCommon.Add(new SqlParameter("@TableName", sTableName));
                    if (MobjCommon.ExecutesQuery(prmCommon, "CommonReferenceTransaction"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            else
                return false;

        }
        public bool DeleteInformation(string sConditions)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 3));
            prmCommon.Add(new SqlParameter("@Fields", sConditions));
            prmCommon.Add(new SqlParameter("@TableName", sTableName));
            if (MobjCommon.ExecutesQuery(prmCommon, "CommonReferenceTransaction"))
            {
                return true;
            }
            else
            {
                return false;
            }
           
        }

        

       

}

