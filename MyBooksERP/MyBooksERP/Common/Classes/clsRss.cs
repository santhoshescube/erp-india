﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace MyBooksERP 
{
    public class clsRss
    {
        public string GetRssFeedLive()
        {
            XDocument xmlDoc=new XDocument();
            string strXpathRemote = "http://msg.mindsoft.ae/Rssfeeds/ProductInfo.xml";
            xmlDoc = XDocument.Load(strXpathRemote);
            return GetRssHtmlLive(xmlDoc);
        }
      
        public string GetRssFeedInitial()
        {
            XDocument xmlDoc=new XDocument();
            string strRssPath = Application.StartupPath + "\\rss\\ProductInfo.xml";
            xmlDoc = XDocument.Load(strRssPath);
            return GetRssHtmlLocal(xmlDoc);
        }

        private string GetRssHtmlLocal(XDocument xmlDoc)
        {
            string strhtmdc = "<html><head></head><body>MindSoft RSS</body></html>";
            string strimgLocalpath = Application.StartupPath + "\\rss\\";
            try
            {
                string sImage="";
                var Product = from img in xmlDoc.Descendants("MyBooksERP")
                              select img.Element("image").Element("url").Value;

                sImage = Product.ElementAt(0);
                strhtmdc = "<html> " +
                           " <head>" +
                              "  <style>" +
                            "td {font-family:arial;font-size:12px;border:none;}" +
                           " a.link{font-family:arial;color:Blue;text-decoration: none}" +
                           " a.link:hover{font-family:arial;color: #3399CC;text-decoration: none}" +
                         " </style>" +
                           " </head>" +
                          "  <body topmargin='0' leftmargin='0' bgcolor='white' oncontextmenu='return false' ondragstart='return false' onselectstart='return false'>" +
                              "  <table border='0' cellspacing='0' width='100%'>" +
                               " <tr><td align='left'><a href='http://www.mindsoft.ae' target='_blank'>" +
                               "<img src='" + strimgLocalpath + sImage + "' border='none' alt='Mindsoft RSS'></img></a></td></tr>" +
                             "</table>" +
                          "  </body>" +
                      "  </html>";
                return strhtmdc;
            }
            catch (Exception)
            {
                return strhtmdc;
            }
        }

        private string GetRssHtmlLive(XDocument xmlDoc)
        {
            string strhtmdc = "<html><head></head><body>MindSoft RSS</body></html>";
            try
            {
                string sImageUrl = "";
                var Product = from img in xmlDoc.Descendants("MyBooksERP")
                              select img.Element("image").Element("url").Value;

                sImageUrl = Product.ElementAt(0);
                strhtmdc = "<html> " +
                           " <head>" +
                              "  <style>" +
                            "td {font-family:arial;font-size:12px;border:none;}" +
                           " a.link{font-family:arial;color:Blue;text-decoration: none}" +
                           " a.link:hover{font-family:arial;color: #3399CC;text-decoration: none}" +
                         " </style>" +
                           " </head>" +
                          "  <body topmargin='0' leftmargin='0' bgcolor='white' oncontextmenu='return false' ondragstart='return false' onselectstart='return false'>" +
                              "  <table border='0' cellspacing='0' width='100%'>" +
                               " <tr><td align='left'><a href='http://www.mindsoft.ae' target='_blank'>" +
                               "<img src='" + sImageUrl + "' border='none' alt='Mindsoft RSS'></img></a></td></tr>" +
                             "</table>" +
                          "  </body>" +
                      "  </html>";
                return strhtmdc;
            }
            catch (Exception)
            {
                return strhtmdc;
            }
        }


    }
}
