﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

public class ClsImages
{
    public Byte[] GetImageStream(Image filePath)
    {
        Image imgFile;
        ImageConverter converter = new ImageConverter();
        imgFile = Image.FromFile(filePath.ToString());
        FileStream fs = new FileStream(filePath.ToString(), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        return (byte[])converter.ConvertTo(imgFile, typeof(byte[]));
    }

    public Byte[] GetImageDataStream(Image mImage, System.Drawing.Imaging.ImageFormat picFormat)
    {
        try
        {
            MemoryStream mstream = new MemoryStream();
            mImage.Save(mstream, picFormat);
            Byte[] bImg = mstream.ToArray();
            return bImg;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public Image GetImage(Byte[] ByData)
    {
        Image imgfile;
        MemoryStream stmData;

        stmData = new MemoryStream(ByData);
        imgfile = Image.FromStream(stmData);
        return imgfile;
    }
    //public Image GetImage(Byte[] ByData)
    //{
    //    var imageStream = new MemoryStream(ByData);
    //    var imageBmp = (Bitmap)Bitmap.FromStream(imageStream);
    //    return imageBmp;
    //}
    public Byte[] GetByte(Image Imgname)
    {
        Image imgFile;
        ImageConverter converter = new ImageConverter();
        imgFile = Image.FromFile(Imgname.ToString());
        //FileStream fs = new FileStream(filePath.ToString(), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        return (byte[])converter.ConvertTo(imgFile, typeof(byte[]));
    }
}
