﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MyBooksERP
{
    /******************************************
     * Created By : Ratheesh
     * Creation Date : 18 June 2011
     * *****************************************/

    public enum eReferenceTables
    {
        Company = 1,
        WareHouse = 2,
        StockAdjustmentReason = 3,
        BatchNumbers = 4
    }

    class clsUtilities
    {


       private static DataLayer MobjDataLayer = null;


       private static DataLayer DataLayer
        {
            get
            {
                if (MobjDataLayer == null)
                    MobjDataLayer = new DataLayer();

                return MobjDataLayer;
            }
            set
            {
                MobjDataLayer = value;
            }
        }
        



        private static int? NullInt = null;

        private static clsConnection objConnection = null;

        private static clsConnection db
        {
            get 
            {
                if (objConnection == null)
                    objConnection = new clsConnection();

                return objConnection;
            }
        }

        public static void GetPermissionCounts()
        {
            DataLayer objdata = new DataLayer();
            ClsMainSettings.Empcount = objdata.ExecuteScalar("Select count(*) from EmployeeMaster where WorkstatusID>5").ToInt32();
            ClsMainSettings.Cmpcount = objdata.ExecuteScalar("Select count(*) from CompanyMaster").ToInt32();
            ClsMainSettings.Itmcount = objdata.ExecuteScalar("Select count(*) from InvItemMaster where StatusID=31").ToInt32();
        }

        public static void BindComboBox(ComboBox cbo, string ValueMember, string DisplayMember, object DataSource)
        {
            // clear existing items (if any)
            cbo.DataSource = null;
            cbo.Items.Clear();

            // bind current data
            cbo.ValueMember = ValueMember;
            cbo.DisplayMember = DisplayMember;
            cbo.DataSource = DataSource;
        }

        public static void BindComboBox(ComboBox cbo, eReferenceTables referenceTable)
        {
            DataTable dt = GetReferenceData(referenceTable, NullInt, null);
            BindingHelper bh = GetBindingHelper(referenceTable);

            if (dt == null || bh == null || cbo == null)
                return;

            BindComboBox(cbo, bh.ValueMember, bh.DisplayMember, dt);
        }

        public static void BindComboBox(ComboBox cbo, eReferenceTables referenceTable, int? RollID, object ConditionalParameter)
        {
            DataTable dt = GetReferenceData(referenceTable, RollID, ConditionalParameter);
            BindingHelper bh = GetBindingHelper(referenceTable);

            if (dt == null || bh == null || cbo == null)
                return;

            BindComboBox(cbo, bh.ValueMember, bh.DisplayMember, dt);
        }

        private static BindingHelper GetBindingHelper(eReferenceTables referenceTable)
        {
            BindingHelper objBindingHelper = null;
            switch (referenceTable)
            {
                case eReferenceTables.Company:
                    objBindingHelper = new BindingHelper { DisplayMember = "Name", ValueMember = "CompanyID" };
                    break;

                case eReferenceTables.WareHouse:
                    objBindingHelper = new BindingHelper { DisplayMember = "WarehouseName", ValueMember = "WarehouseID" };
                    break;

                case eReferenceTables.StockAdjustmentReason:
                    objBindingHelper = new BindingHelper { DisplayMember = "Description", ValueMember = "ReasonID" };
                    break;

                case eReferenceTables.BatchNumbers:
                    objBindingHelper = new BindingHelper { DisplayMember = "BatchNo", ValueMember = "BatchID" };
                    break;

            }
            return objBindingHelper;
        }
        public static DataTable GetAutoCompleteListWithCompany(int intCompanyID, string strSearchKey, int intEmployeeID, string strFieldName, string strTableName)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 15));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@SearchKey", ""));
            parameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            parameters.Add(new SqlParameter("@FieldName", strFieldName));
            parameters.Add(new SqlParameter("@TableName", strTableName));
            //parameters.Add(new SqlParameter("@IsArabicView", 0));
            return new DataLayer().ExecuteDataTable("spEmployee", parameters);
        }
        public static DataTable GetReferenceData(eReferenceTables referenceTable, int? RollID, object ConditionalParameter)
        {
            ArrayList alSqlParams = new ArrayList();
            DataTable dtData = null;

            switch (referenceTable)
            {
                case eReferenceTables.Company:
                    alSqlParams.Add(new SqlParameter("@Company", true));
                    
                    if (RollID != NullInt && RollID != 1 && RollID != 2)
                        alSqlParams.Add(new SqlParameter("@RollID", (int)RollID));

                    break;

                case eReferenceTables.WareHouse:
                    alSqlParams.Add(new SqlParameter("@WareHouse", true));
                    if (ConditionalParameter != null)
                    {
                        int CompanyId = -1;
                        int.TryParse(ConditionalParameter.ToString(), out CompanyId);

                        if (CompanyId > 0)
                            alSqlParams.Add(new SqlParameter("@CompanyID", CompanyId));
                    }
                    break;

                case eReferenceTables.StockAdjustmentReason:
                    alSqlParams.Add(new SqlParameter("@StockAdjustmentReason", true));
                    break;

                case eReferenceTables.BatchNumbers:
                    alSqlParams.Add(new SqlParameter("@BatchNumbers", true));
                    if (ConditionalParameter != null)
                        // Expecting ItemId
                        alSqlParams.Add(new SqlParameter("@ItemID", ConditionalParameter.ToInt64()));
                    break;
            }

            if (alSqlParams.Count > 0)
                dtData = db.FillDataTable("spInvReferenceTables", alSqlParams);

            return dtData;
        }

        public static bool IsDocumentExists(int intOperationTypeID, long lngReferenceID)
        {
            List<SqlParameter> sqlParameters ;
            sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",1),
                            new SqlParameter("@OperationTypeID",intOperationTypeID),
                             new SqlParameter("@ReferenceID",lngReferenceID)};
            int intRetIsExists = DataLayer.ExecuteScalar("spCommonUtility", sqlParameters).ToInt32();
            return (intRetIsExists > 0);
        }
        public static DataTable GetAutoCompleteList(string strSearchKey, int intEmployeeID, string strFieldName, string strTableName)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 18));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@SearchKey", strSearchKey));
            parameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            parameters.Add(new SqlParameter("@FieldName", strFieldName));
            parameters.Add(new SqlParameter("@TableName", strTableName));
            //parameters.Add(new SqlParameter("@IsArabicView", 0));
            return new DataLayer().ExecuteDataTable("spEmployee", parameters);
        }

        public static AutoCompleteStringCollection ConvertAutoCompleteCollection(DataTable dt)
        {
            string[] array = { };

            if (dt != null && dt.Rows.Count > 0)
                array = Array.ConvertAll(dt.Select(), row => (string)row[0]);

            var collection = new AutoCompleteStringCollection();
            collection.AddRange(array);

            return collection;
        }
    }
    
    public class BindingHelper
    {
        public string ValueMember { get; set; }
        public string DisplayMember { get; set; }
    }

}
