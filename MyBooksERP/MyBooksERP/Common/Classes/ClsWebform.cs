﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Microsoft.VisualBasic;
class ClsWebform
{
    public int PRecID;
    long MintRecordID;
    int MintFormType;
    int intType = 0;
    //string CollectionType;

    string MstrCaption;
    string imgPath = Application.StartupPath + "\\NoInfo.jpg";    
    string strColor = "#fff2eb";
    string sAltColor = "#ffffff";
    string Type;
    string strName;
    string strValue;
    string strTypeName;
    string strTypeValue;
    string strRefNoName;
    string strRefNoValue;
    string strMainNoName;
    string strMainNoValue;
    string strDateName;
    string strDateValue;
    string strWareHouseValue;
    string strEmployeeValue;
    bool mFlag;
    public DataTable datHistory = null;

    DataSet EmailSource;

    StringBuilder MsbHtml;
    clsConnection MobjConnection;
    //ClsCommonUtility MObjCommonUtility;

    public ClsWebform()
    {
        MobjConnection = new clsConnection();
        //MObjCommonUtility = new ClsCommonUtility();

        MsbHtml = new StringBuilder();
    }

    public string GetWebFormData(string sCaption, EmailFormID iFormType, DataSet dtsEmail)
    {
        EmailSource = dtsEmail;
        MstrCaption = sCaption;

        string strNoInformation = "<html><head></head><body oncontextmenu=return false ondragstart=return false onselectstart=return false><center><img src='" + imgPath + "' border=none alt=No Information Found></img></center></body></html>";
        string strHtml = string.Empty;

        switch (iFormType)
        {
            case EmailFormID.Company:
                strHtml = GetCompanyEmail();
                break;
            case EmailFormID.Bank:
                strHtml = GetBankEmail();
                break;
            case EmailFormID.Employee:
                strHtml = GetEmployeeEmail();
                break;
            case EmailFormID.Vendor:
                strHtml = GetVendorEmail();
                break;
            case EmailFormID.Warehouse:
                strHtml = GetWarehouseEmail();
                break;
            case EmailFormID.ComplaintRegistration:
                strHtml = GetComplaintRegistrationEmail();
                break;
            case EmailFormID.DeductionPolicy:
                strHtml = GetDeduction();
                break;
            case EmailFormID.ComplaintScheduling:
                strHtml = GetComplaintScheduleEmail();
                break;
            case EmailFormID.SettlementProcess:
                strHtml = GetEmployeeSettlement();
                break;
            case EmailFormID.ComplaintStatusUpdation:
                strHtml = GetComplaintStatusUpdationEmail();
                break;
            case EmailFormID.ColectionStatusUpdation:
                strHtml = GetCollectionStatusUpdationEmail();
                break;
            case EmailFormID.AlertSettings:
                strHtml = GetAlertSettingsEmail();
                break;
            case EmailFormID.PlanSubmission:
                strHtml = GetPlanSubmissionEmail();
                break;
            case EmailFormID.RFQ:
                strHtml = GetRFQEmail();
                break;
            case EmailFormID.DeliverySchedule:
                strHtml = GetDeliveryScheduleEmail();
                break;
            case EmailFormID.DeliveryStatusUpdation:
                strHtml = GetDeliveryStatusUpdationEmail();
                break;
            case EmailFormID.UnitsOfMeasurement:
                strHtml = GetUOMEmail();
                break;
            case EmailFormID.UOMConversion:
                strHtml = GetUOMConversionEmail();
                break;
            case EmailFormID.PaymentTerms:
                strHtml = GetPaymentTermsEmail();
                break;
            case EmailFormID.ExchangeCurrency:
                strHtml = GetExchangeCurrencyEmail();
                break;
            case EmailFormID.DiscountNormal:
                strHtml = GetNormalEmail();
                break;
            case EmailFormID.LeaveStructure:
                strHtml = GetLeaveStructureEmail();
                break;
            case EmailFormID.Discountitemwise:
                strHtml = GetItemWiseEmail();
                break;
            case EmailFormID.Discountcustomerwise:
                strHtml = GetCustomerwiseEmail();
                break;
            case EmailFormID.LeaveEntry:
                strHtml = GetLeaveEntry();
                break;
            case EmailFormID.Expense:
                strHtml = GetExpenseEmail();
                break;
            case EmailFormID.Payments:
                strHtml = GetPaymentsEmail(false);
                break;
            case EmailFormID.Receipts:
                strHtml = GetPaymentsEmail(true);
                break;
            case EmailFormID.GeneralReceiptsAndPayments:
                strHtml = GetGeneralreceiptsAndPaymentsEmail();
                break;
            case EmailFormID.Product:
                strHtml = GetProductsEmail();
                break;
            case EmailFormID.ItemGroup:
                strHtml = GetItemGroupingEmail();
                break;
            case EmailFormID.PurchaseIndent:
                strHtml = GetPurchaseEmail(1);
                break;
            case EmailFormID.PurchaseQuotation:
                strHtml = GetPurchaseEmail(2);
                break;
            case EmailFormID.PurchaseOrder:
                strHtml = GetPurchaseEmail(3);
                break;
            case EmailFormID.GRN:
                strHtml = GetPurchaseEmail(4);
                break;
            case EmailFormID.PurchaseInvoice:
                strHtml = GetPurchaseEmail(5);
                break;
            case EmailFormID.DebitNote:
                strHtml = GetPurchaseEmail(6);
                break;
            case EmailFormID.SalesQuotation:
                strHtml = GetSalesQuotationEmail();
                break;
            case EmailFormID.SalesOrder:
                strHtml = GetSalesOrderEmail();
                break;
            case EmailFormID.SalesInvoice:
                strHtml = GetSalesInvoiceEmail();
                break;
            case EmailFormID.SalesReturn:
                strHtml = GetSalesReturnEmail();
                break;
            case EmailFormID.ItemIssue:
                strHtml = GetDeliveryNoteEmail();
                break;
            case EmailFormID.CollectionSchedule:
                strHtml = GetCollectionScheduleEmail();
                break;
            case EmailFormID.InstallmentCollection:
                strHtml = GetInstallmentCollectionEmail();
                break;
            case EmailFormID.Installments:
                strHtml = GetInstallmentsEmail();
                break;
            case EmailFormID.Documents:
                strHtml = GetDocumentEmail();
                break;
            case EmailFormID.RoleSettings:
                strHtml = GetRoleSettingsEmail();
                break;
            case EmailFormID.CompanySettings:
                strHtml = GetCompanySettingsEmail();
                break;
            case EmailFormID.ItemBatch:
                strHtml = GetProductBatchEmail();
                strHtml = (strHtml.Equals(string.Empty) ? strNoInformation : strHtml);
                break;
            case EmailFormID.OpeningStock:
                strHtml = GetOpeningStockEmail();
                strHtml = (strHtml.Equals(string.Empty) ? strNoInformation : strHtml);
                break;
            case EmailFormID.AccountSettings:
                strHtml = GetAccountSettingsEmail();
                strHtml = (strHtml.Equals(string.Empty) ? strNoInformation : strHtml);
                break;
            case EmailFormID.StockAdjustment:
                strHtml = GetStockAdjustmentEmail();
                break;
            case EmailFormID.TermsAndConditions:
                strHtml = GetTermsAndConditionsEmail();
                break;
            case EmailFormID.Projects:
                strHtml = GetProjectEmail();
                break;
            case EmailFormID.StockTransfer:
                strHtml = GetStockTransferEmail();
                break;
            case EmailFormID.PricingScheme:
                strHtml = GetPricingSchemeEmail();
                break;
            case EmailFormID.PickList:
                strHtml = GetPickListEmail();
                break;
            case EmailFormID.PointofSale:
                strHtml = GetPOSEmail();
                break;
            case EmailFormID.SalaryStructure :
                strHtml = GetSalaryStructureEmail();
                break;
            case EmailFormID.ShiftPolicy  :
                strHtml = GetShiftPolicyEmail();
                break;
            case EmailFormID.WorkPolicy :
                strHtml = GetWorkPolicyEmail();
                break ;
            case EmailFormID.LeavePolicy :
                strHtml = GetLeavePolicyEmail();
                break;
            case EmailFormID.SalaryPayment :
                strHtml = GetPayment();
                break;
            case EmailFormID.MaterialIssue:
                strHtml = GetMaterialIssueEmail();
                break;
            case EmailFormID.MaterialReturn:
                strHtml = GetMaterialReturnEmail();
                break;
            case EmailFormID.DirectGRN:
                strHtml = GetDirectGRNEmail();
                break;
            case EmailFormID.ExtraCharges:
                strHtml = getExtraChargeEmail();
                break;
            case EmailFormID.ExtraChargePayment:
                strHtml = getExtraChargePaymentEmail();
                break;
            case EmailFormID.VacationProcess:
                strHtml = GetEmployeeVacationEmail();
                break;
            case EmailFormID.GroupJV:
                strHtml = GetGroupJVEmail();
                break;
        }
        return strHtml;
    }
    private string GetEmployeeVacationEmail()
    {
        mFlag = false;
        GetWebformHeaderInfo();

        string sType = "";
        DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
        if (EmailSource.Tables[0].Rows.Count > 0)
        {

            MsbHtml.Append("<table border='0' cellspading='0' cellspacing='0' width='100%'> " +
                            "<tr height='25px'><td colspan='2' align='left'><font size='2' color='#3a3a53'><b>General Information</b></font></td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Employee</td><td align='left'>" + Convert.ToString(DrWebInfo["EmployeeFullName"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Process Date</td><td align='left'>" + Convert.ToString(DrWebInfo["Date"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Vacation From Date</td><td align='left'>" + Convert.ToString(DrWebInfo["FromDate"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Vacation To Date</td><td align='left'>" + Convert.ToString(DrWebInfo["ToDate"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Experience Days</td><td align='left'>" + Convert.ToString(DrWebInfo["ExperienceDays"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Consider Absent Days</td><td align='left'>" + Convert.ToString(DrWebInfo["ConsiderAbsentDays"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Absent Days</td><td align='left'>" + Convert.ToString(DrWebInfo["AbsentDays"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Taken Leaves</td><td align='left'>" + Convert.ToString(DrWebInfo["TakenLeaves"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>OverTaken Leaves</td><td align='left'>" + Convert.ToString(DrWebInfo["OvertakenLeaves"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Issued Tickets</td><td align='left'>" + Convert.ToString(DrWebInfo["IssuedTickets"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Ticket Expense</td><td align='left'>" + Convert.ToString(DrWebInfo["TicketExpense"]) + "</td></tr>" +
                             "<tr height='25px'><td width='30%' align='left'>Earnings</td><td align='left'>" + Convert.ToString(DrWebInfo["Credit"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Deductions</td><td align='left'>" + Convert.ToString(DrWebInfo["Debit"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Net Amount</td><td align='left'>" + Convert.ToString(DrWebInfo["NetAmount"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Currency</td><td align='left'>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
                             "<tr height='25px'><td width='30%' align='left'>Remarks</td><td align='left'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr> " +
                         " </table>");

            mFlag = true;
        }
        else
        {
            mFlag = false;
        }


        if (mFlag == true)
        {
            MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><b>Vacation Info</b></font></td></tr>" +
                      "<tr><td colspan=2>" + GetEmployeeVacationDetails() + " </td></tr>");
        }
        else
        {
            MsbHtml.Append("<html><head></head><body bgcolor=White oncontextmenu=return false ondragstart=return false onselectstart=return false><center><img src='" + imgPath + "' border=none alt=No Information Found></img></center></body></html>");
        }
        return MsbHtml.ToString();
    }

    private string GetEmployeeVacationDetails()
    {
        StringBuilder sbHtmlEarning = new StringBuilder();

        sbHtmlEarning.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>" +
                             "<tr bgcolor=#3a3a53><td width=35% align=left><font size=2 color=white>Particulars</td><td align=left><font size=2 color=white>Earning</td>" +
                             "<td align=left><font size=2 color=white>Deduction</td>" +
                             "<td width=35% align=left><font size=2 color=white>NoOfDays</td></tr>");

        string color = "#e5e5ef";
        bool blExists = false;


        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            sbHtmlEarning.Append(" <tr bgcolor=" + color + "><td align='left'>" + Convert.ToString(drowEmail["Description"]) + "</td> " +
                               " <td align='left'>" + Convert.ToString(drowEmail["Credit"]) + "</td> " +
                               " <td align='left'>" + Convert.ToString(drowEmail["Debit"]) + "</td> " +
                               " <td align='left'>" + Convert.ToString(drowEmail["PaidDays"]) + "</td> " +
                               " </tr> ");
            if (color == "#e5e5ef")
            {
                color = "#FFFFFF";
            }
            else
            {
                color = "#e5e5ef";
            }

        }

        blExists = true;

        if (blExists == false)
        {
            sbHtmlEarning.Append("<table border=0 cellspading=0 cellspacing=0 width=100%><tr><td align=left><body bgcolor=White><font color=blue>No Earning Found</font></td></tr></table>");
        }
        else
        {
            sbHtmlEarning.Append("</table>");
        }
        return sbHtmlEarning.ToString();

    }

    private string getExtraChargePaymentEmail()
    {
        if (EmailSource.Tables[1].Rows.Count > 0)
        {
            GetWebformHeaderInfo();

            // Heading
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
            DataRow DrWebInfo = EmailSource.Tables[1].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Currency</td><td width=70%>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Payment No</td><td width=70%>" + Convert.ToString(DrWebInfo["PaymentNo"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Payment Date</td><td>" + Convert.ToString(DrWebInfo["PaymentDate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Operation Type</td><td>" + Convert.ToString(DrWebInfo["OperationType"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Reference No</td><td>" + Convert.ToString(DrWebInfo["ReferenceNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Shipment Vendor</td><td>" + Convert.ToString(DrWebInfo["VendorName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Extra Charge No</td><td>" + Convert.ToString(DrWebInfo["ExtraChargeNo"]) + "</td></tr>");

            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");


            strColor = "#ffffff";
            MsbHtml.Append("<tr  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Extra Charge Payment Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Sl No</b></font></td><td width=50%><font color=#FFFFF><b>Expense Type</b></font></td>" +
                           "<td><font color=#FFFFF><b>Total Amount</b></font></td><td><font color=#FFFFF><b>Current Balance</b></font></td>" +
                           "<td><font color=#FFFFF><b>Amount</b></font></td></tr>");

            foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
            {
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["SerialNo"]) + "</td><td>" + Convert.ToString(drowEmail["ExpenseType"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["TotalAmount"]) + "</td>" +
                                "<td>" + Convert.ToString(drowEmail["CurrentBalance"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["Amount"]) + "</td></tr>");
            }
            MsbHtml.Append("</table></br><table border=0 cellspading=0 cellspacing=0 width=100%>");

            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF>Payment Mode </font></td><td width=30%><font color=#FFFFF>" + Convert.ToString(DrWebInfo["PaymentMode"]) + " </font></td>  <td width=40% align=right><font color=#FFFFF>Total Amount in " + Convert.ToString(DrWebInfo["Currency"]) + " </font></td><td width=10%><font color=#FFFFF>" + Convert.ToString(DrWebInfo["TotalAmount"]) + "</font></td></tr>");
            MsbHtml.Append("</table>");
            MsbHtml.Append("</br>");
            if (Convert.ToInt32(Convert.ToString(DrWebInfo["PaymentModeID"])) == 1)
            {
                MsbHtml.Append("<table width=100% border=0 cellspacing=0 cellpadding=0><tr height=25px><td width=30%>Account</td><td width=70%>" + Convert.ToString(DrWebInfo["AccountName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Cheque No</td><td width=70%>" + Convert.ToString(DrWebInfo["ChNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Cheque Date</td><td width=70%>" + Convert.ToString(DrWebInfo["ChDate"]) + "</td></tr></table>");
            }
            MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");

            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");

            MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
            MsbHtml.Append("</td></tr></td></tr></table></div></body></html>");
        }
        return MsbHtml.ToString();
    }
    private string getExtraChargeEmail()
    {
        if (EmailSource.Tables[1].Rows.Count > 0)
        {
            GetWebformHeaderInfo();

            // Heading
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
            DataRow DrWebInfo = EmailSource.Tables[1].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Extra Charge No</td><td width=70%>" + Convert.ToString(DrWebInfo["ExtraChargeNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Date</td><td width=70%>" + Convert.ToString(DrWebInfo["ExtraChargeDate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Operation Type</td><td>" + Convert.ToString(DrWebInfo["OperationType"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Reference No</td><td>" + Convert.ToString(DrWebInfo["ReferenceNo"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Currency</td><td>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>");

            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

            //    MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Customer Information</h2></font></td></tr>");
            //    MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td width=30%>customer</td><td>" + Convert.ToString(DrWebInfo["VendorName"]) + "</td></tr>" +
            //              "<tr  height=25px><td width=30%>Address</td><td>" + Convert.ToString(DrWebInfo["AddressName"]) + "</td></tr>" +
            //              "<tr  bgcolor=#fff2eb height=25px><td width=30%></td><td>" + Convert.ToString(DrWebInfo["Contactperson"]) + "</td></tr>" +
            //              "<tr  height=25px><td></td><td>" + Convert.ToString(DrWebInfo["Address"]) + " , " + Convert.ToString(DrWebInfo["State"]) + " , " + Convert.ToString(DrWebInfo["Country"]) + " , " + Convert.ToString(DrWebInfo["ZipCode"]) + "</td></tr>");

            //}

            //MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

            strColor = "#ffffff";
            MsbHtml.Append("<tr  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Extra Charge Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Expense Type</b></font></td>" +
                           "<td><font color=#FFFFF><b>Shipment Vendor</b></font></td><td width=50%><font color=#FFFFF><b>Description</b></font></td>" +
                           "<td><font color=#FFFFF><b>Amount</b></font></td></tr>");

            foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
            {
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ExtraChargeDescription"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["VendorName"]) + "</td>" +
                                "<td>" + Convert.ToString(drowEmail["Description"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["Amount"]) + "</td></tr>");
            }
            MsbHtml.Append("</br>");
            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=90% align=right><font color=#FFFFF>Total Amount in " + Convert.ToString(DrWebInfo["Currency"]) + " </font></td><td width=10%><font color=#FFFFF>" + Convert.ToString(DrWebInfo["TotalAmount"]) + "</font></td></tr>");

            MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        }
        return MsbHtml.ToString();
    }

    private string GetExchangeCurrencyEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td>" +
                "<tr height=25px><td width=30%>Company Currency</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyCurrency"]) + "</td></tr>");           
               
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Currency Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Currency</b></font></td>" +
                       "<td><font color=#FFFFF><b>Exchange Rate</b></font></td><td><font color=#FFFFF><b>Exchange Date</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["Currency"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ExchangeRate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ExchangeDate"]) + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }


    private string GetProductBatchEmail()
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        strColor = "#ffffff";

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            GetWebformHeaderInfo();

            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px>");
            MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Batch No</h2></font></td>");
            //MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Barcode Value</h2></font></td>");
            MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Expiry Date</h2></font></td>");
            MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Purchase Rate</h2></font></td>");
            MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Actual Rate</h2></font></td>");
            MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Sale Rate</h2></font></td>");
            MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Ware House</h2></font></td>");
            MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Current Stock</h2></font></td>");
            MsbHtml.Append("</tr>");

            foreach (DataRow drWebInfo in EmailSource.Tables[0].Rows)
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
                for (int i = 0; i < EmailSource.Tables[0].Columns.Count; i++)
                {
                    MsbHtml.Append("<td align=left>" + Convert.ToString(drWebInfo[i]) + "</td>");
                }
                MsbHtml.Append("</tr>");

                if (strColor == "#fff2eb")
                    strColor = "#ffffff";
                else
                    strColor = "#fff2eb";
            }
            
            iIndex++;
            MsbHtml.Append("</table></td></tr></table></div></body></html>");
        }
        
        return MsbHtml.ToString();
    }
    private string GetAlertSettingsEmail()
    {
        string strRolename = string.Empty;

        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
       
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
          
            MsbHtml.Append("<tr height=25px><td width=30%> Alert Name </td><td width=70%>" + Convert.ToString(DrWebInfo["AlertName"]) + "</td></tr>" +
              "<tr bgcolor=#fff2eb height=25px><td width=30%>Alert Type</td><td width=70%>" + Convert.ToString(DrWebInfo["AlertType"]) + "</td></tr>" +
                           "<tr height=25px><td width=30%>Operation Type </td><td width=70%>" + Convert.ToString(DrWebInfo["OperationType"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Alert Settings Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Role Name/ User Name</b></font></td>" +

                       "<td><font color=#FFFFF><b>Processing Interval</b></font></td><td><font color=#FFFFF><b>Repeat Interval</b></font></td>" +
                       "<td><font color=#FFFFF><b>Is Repeat</b></font></td><td><font color=#FFFFF><b>Is Alert ON</b></font></td>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            if (!strRolename.Trim().Equals(Convert.ToString(drowEmail["Rolename"]).Trim()))
            {

                MsbHtml.Append("<tr height=25px><td><b>" + Convert.ToString(drowEmail["Rolename"]) + "</b></td>" );
                               //"<td><b>" + Convert.ToString(drowEmail["RoleProcessingInterval"]) + "</b></td>" +
                               //"<td><b>" + Convert.ToString(drowEmail["RoleRepeatInterval"]) + "</b></td>" +
                               // "<td><b>" + Convert.ToString(drowEmail["RoleIsRepeat"]) + "</b></td>" +
                  //"<td><b>" + Convert.ToString(drowEmail["RoleIsAlertON"]) + "</b></td></tr>");//bgcolor=" + strColor + "

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["username"]) + "</td>" +
               "<td>" + Convert.ToString(drowEmail["userProcessingInterval"]) + "</td>" +
               "<td>" + Convert.ToString(drowEmail["userRepeatInterval"]) + "</td>" +
                "<td>" + Convert.ToString(drowEmail["userIsRepeat"]) + "</td>" +
                "<td>" + Convert.ToString(drowEmail["userIsAlertON"]) + "</td></tr>");
            }
            else
            {
                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["username"]) + "</td>" +
                              "<td>" + Convert.ToString(drowEmail["userProcessingInterval"]) + "</td>" +
                              "<td>" + Convert.ToString(drowEmail["userRepeatInterval"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["userIsRepeat"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["userIsAlertON"]) +"</td></tr>");
            }

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                 strRolename = Convert.ToString(drowEmail["Rolename"]);
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }


    private string GetPlanSubmissionEmail()
    {
        
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            MsbHtml.Append("<tr height=25px><td width=30%> Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
                          "<tr bgcolor=#fff2eb height=25px><td width=30%>Department</td><td width=70%>" + Convert.ToString(DrWebInfo["Department"]) + "</td></tr>" +
                           "<tr height=25px><td width=30%>Currency</td><td width=70%>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
                         "<tr bgcolor=#fff2eb height=25px><td width=30%>Plan No</td><td width=70%>" + Convert.ToString(DrWebInfo["PlanNo"]) + "</td></tr>" +
                           "<tr height=25px><td width=30%>From Date</td><td width=70%>" + Convert.ToString(DrWebInfo["StartDate"]) + "</td></tr>" +
                         "<tr bgcolor=#fff2eb height=25px><td width=30%>To Date</td><td width=70%>" + Convert.ToString(DrWebInfo["EndDate"]) + "</td></tr>" +
                         "<tr height=25px><td width=30%>Status</td><td width=70%>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
                          "<tr bgcolor=#fff2eb height=25px><td width=30%>Employee</td><td width=70%>" + Convert.ToString(DrWebInfo["Executive"]) + "</td></tr>" +
                           "<tr   height=25px><td width=30%>Remarks</td><td width=70%>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Plan Submission Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Serial No</b></font></td>" +
                       "<td><font color=#FFFFF><b>Plan Type</b></font></td><td><font color=#FFFFF><b>Quantity</b></font></td>" +
                       "<td><font color=#FFFFF><b>Amount</b></font></td><td><font color=#FFFFF><b>Total</b></font></td>" +
                        "<td><font color=#FFFFF><b>Status</b></font></td><td><font color=#FFFFF><b>Reason</b></font></td>");
                    

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td><b>" + Convert.ToString(drowEmail["SerialNo"]) + "</b></td>" +
                               "<td><b>" + Convert.ToString(drowEmail["PlanType"]) + "</b></td>" +
                               "<td><b>" + Convert.ToString(drowEmail["Quantity"]) + "</b></td>" +
                                "<td><b>" + Convert.ToString(drowEmail["Amount"]) + "</b></td>" +
                               "<td><b>" + Convert.ToString(drowEmail["ItemTotal"]) + "</b></td>"+
                               "<td><b>" + Convert.ToString(drowEmail["Status"]) + "</b></td>" +
                               "<td><b>" + Convert.ToString(drowEmail["Reason"]) + "</b></td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";                     
        }
       

        //strColor = "#ffffff";
        //if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        //if (EmailSource.Tables[0].Rows.Count > 0)
        //{
        //    DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
        //    MsbHtml.Append("<tr><td>&nbsp;</td></tr><tr bgcolor=" + strColor + " height=25px><td colspan=2>&nbsp;</td><td colspan=3><b>Sub Total</b></td><td colspan=2><b>" + Convert.ToString(DrWebInfo["SubTotal"]) + "</b></td></tr>" +
        //       
        //        "<tr bgcolor=#fff2eb height=25px><td colspan=2>&nbsp;</td><td colspan=3><b>Discount Amount<b></td><td colspan=2><b>" + Convert.ToString(DrWebInfo["GrandDiscountAmount"]) + "</b></td></tr>" +
        //        "<tr height=25px><td colspan=2>&nbsp;</td><td colspan=3><b>Total Amount<b></td><td colspan=2><b>" + Convert.ToString(DrWebInfo["GrandAmount"]) + "</b></td></tr>");
        //}
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        //MsbHtml.Append("</table></td></tr></td></tr></table>");
       strColor = "#ffffff";
        //if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
       if (EmailSource.Tables[0].Rows.Count > 0)
       {
           DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
           MsbHtml.Append("<tr><td>&nbsp;</td></tr><tr bgcolor=" + strColor + " height=25px><td colspan=9>&nbsp;</td><td colspan=5><b>Net Amount</b>&nbsp;</td><td colspan=11><b>" + Convert.ToString(DrWebInfo["NetAmount"]) + "</b></td></tr>");

             }
       //MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

       MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
      
        return MsbHtml.ToString();
       
    }
    private string GetCompanySettingsEmail()
    {
        string strConfigurationaItem = string.Empty;

        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Company Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%></td><td width=70%>" + Convert.ToString(DrWebInfo["HPOBox"]) + "</td></tr>" +
               "<tr height=25px><td width=30%></td><td width=70%>" + Convert.ToString(DrWebInfo["PhonePABXNo"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%></td><td>" + Convert.ToString(DrWebInfo["RoadArea"]) + "</td></tr>" +
               "<tr height=25px><td width=30%></td><td>" + Convert.ToString(DrWebInfo["BlockCity"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%></td><td>" + Convert.ToString(DrWebInfo["WebSite"]) + "</td></tr>");
       }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";
        MsbHtml.Append("<tr  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Company Settings Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Configuration Item</b></font></td>" +
                       "<td width=50%><font color=#FFFFF><b>Sub Item</b></font></td><td><font color=#FFFFF><b>Configuration Value</b></font></td></tr>");

       
        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {

            if (!strConfigurationaItem.Trim().Equals(Convert.ToString(drowEmail["ConfigurationItem"]).Trim()))
            {
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ConfigurationItem"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["SubItem"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["ConfigurationValue"]) + "</td></tr>");
            }
            else
            {
                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>&nbsp;</td>" +
                               "<td>" + Convert.ToString(drowEmail["SubItem"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["ConfigurationValue"]) + "</td></tr>");
            }
            strConfigurationaItem = Convert.ToString(drowEmail["ConfigurationItem"]);
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        return MsbHtml.ToString();
    }

    private string GetRoleSettingsEmail()
    {
        int intIndex = 0;
        string strFieldName = string.Empty;
        string strPermissions = string.Empty;
        string strCompany = string.Empty, strMenu = string.Empty, strModule = string.Empty;
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow drowEmail in EmailSource.Tables[0].Rows)
            {
                if (intIndex == 0)
                {
                    MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td align=left><font size=2><h2>Role:</h2></font></td><td align=left><font size=2><h2>" +
                        Convert.ToString(drowEmail["RoleName"]) + "</h2></font></td></tr></table>");
                    MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");
                    MsbHtml.Append("<tr height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Permission Settings</h2></font></td></tr>");

                    MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px>");
                    for (int i = 1; i < 5; i++)
                    {
                        strFieldName = EmailSource.Tables[0].Columns[i].ColumnName;
                        MsbHtml.Append("<td align=left><h2><font color=#FFFFF>" + strFieldName + "</font></h2></td>");
                    }
                    MsbHtml.Append("<td align=left><h2><font color=#FFFFF>Permissions</font></h2></td></tr>");
                }

                if (!strCompany.Trim().Equals(Convert.ToString(drowEmail["Company"]).Trim()))
                {
                    MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=5>" + Convert.ToString(drowEmail["Company"]).Trim() + 
                        "</td></tr>");
                    if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                }

                if (!(strCompany + strModule).Trim().Equals(Convert.ToString(drowEmail["Company"]).Trim() + 
                    Convert.ToString(drowEmail["Module"]).Trim()))
                {
                    MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=1>&nbsp;</td><td colspan=4>" + 
                        Convert.ToString(drowEmail["Module"]).Trim() + "</td></tr>");
                    if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                }

                if (!(strCompany + strModule + strMenu).Trim().Equals(Convert.ToString(drowEmail["Company"]).Trim() + 
                    Convert.ToString(drowEmail["Module"]).Trim() + Convert.ToString(drowEmail["Menu"]).Trim()))
                {
                    MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=2>&nbsp;</td><td colspan=2>" + 
                        Convert.ToString(drowEmail["Menu"]).Trim() + "</td><td>" + Convert.ToString(drowEmail["MenuPermissions"]).Trim() + 
                        "</td></tr>");
                    if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                }

                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=3>&nbsp;</td><td>" + 
                    Convert.ToString(drowEmail["Fields"]).Trim() + "</td><td>" + Convert.ToString(drowEmail["FieldPermissions"]).Trim() + 
                    "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

                strCompany = Convert.ToString(drowEmail["Company"]);
                strModule = Convert.ToString(drowEmail["Module"]);
                strMenu = Convert.ToString(drowEmail["Menu"]);
                strFieldName = Convert.ToString(drowEmail["Fields"]);
                strPermissions = Convert.ToString(drowEmail["FieldPermissions"]);
                intIndex++;
            }
        }

        MsbHtml.Append("</table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetDocumentEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        // Expense Master details
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            // Master data
            MsbHtml.Append("<tr height=25px><td>Document Type</td><td>" + Convert.ToString(drowEmail["DocumentType"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Operation Type</td><td>" + Convert.ToString(drowEmail["OperationType"]) + "</td></tr>" +
                           "<tr height=25px><td>Reference Number</td><td>" + Convert.ToString(drowEmail["ReferenceNumber"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Document Number</td><td>" + Convert.ToString(drowEmail["DocumentNumber"]) + "</td></tr>"+
                           "<tr height=25px><td>Issued Date</td><td>" + Convert.ToString(drowEmail["IssuedDate"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Expiry Date</td><td>" + Convert.ToString(drowEmail["ExpiryDate"]) + "</td></tr>"+
                           "<tr height=25px><td>Remarks</td><td>" + Convert.ToString(drowEmail["Remarks"]) + "</td></tr>"
                           );
        }
        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetCompanyEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        // Company Master details
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            if (Convert.ToBoolean(DrWebInfo["CompanyBranchIndicator"]) == true)
            {
                MsbHtml.Append("<tr height=25px><td width=30%> Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["PCompanyName"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Employer Code</td><td width=70%>" + Convert.ToString(DrWebInfo["EPID"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Short Name</td><td width=70%>" + Convert.ToString(DrWebInfo["ShortName"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Company Industry</td><td>" + Convert.ToString(DrWebInfo["CompanyIndustry"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Company Type</td><td>" + Convert.ToString(DrWebInfo["CompanyType"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Company Or Branch</td><td>" + (Convert.ToBoolean(DrWebInfo["CompanyBranchIndicator"]) == true ? "Company" : "Branch") + "</td></tr>" +
                "<tr height=25px><td width=30%>Telephone</td><td>" + Convert.ToString(DrWebInfo["ContactPersonPhone"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Fax</td><td>" + Convert.ToString(DrWebInfo["PABXNumber"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Website</td><td>" + Convert.ToString(DrWebInfo["WebSite"]) + "</td></tr>" +

               "<tr  height=25px><td width=30%>Daily Pay based On</td><td>" + (Convert.ToBoolean(DrWebInfo["DailyPaymentType"]) == true ? "Month" : "Year") + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Working Days</td><td>" + Convert.ToString(DrWebInfo["WorkingDaysInMonth"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>OffDay</td><td>" + Convert.ToString(DrWebInfo["OffDay"]) + "</td></tr>" +


                "<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Contact Information</h2></font></td></tr>" +
                "<tr height=25px><td width=30%>Contact Person Name</td><td>" + Convert.ToString(DrWebInfo["ContactPerson"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>Contact Person Designation</td><td>" + Convert.ToString(DrWebInfo["ContactDesignation"]) + "</td></tr>" +
                "<tr height=25px><td>Primary Email</td><td>" + Convert.ToString(DrWebInfo["PrimaryEmail"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>Secondary Email</td><td>" + Convert.ToString(DrWebInfo["SecondaryEmail"]) + "</td></tr>" +
                "<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Postal Address</h2></font></td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>PO Box</td><td>" + Convert.ToString(DrWebInfo["POBox"]) + "</td></tr>" +
                "<tr height=25px><td>Street</td><td>" + Convert.ToString(DrWebInfo["Road"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>Area</td><td>" + Convert.ToString(DrWebInfo["Area"]) + "</td></tr>" +
                "<tr height=25px><td>Block</td><td>" + Convert.ToString(DrWebInfo["Block"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>City</td><td>" + Convert.ToString(DrWebInfo["City"]) + "</td></tr>" +
                "<tr height=25px><td>Province</td><td>" + Convert.ToString(DrWebInfo["Province"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>Country</td><td>" + Convert.ToString(DrWebInfo["Country"]) + "</td></tr>" +
                "<tr height=25px><td>Other Info</td><td>" + Convert.ToString(DrWebInfo["OtherInfo"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>Currency</td><td>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
                "<tr height=25px><td>Financial Year Start Date</td><td>" + Convert.ToString(DrWebInfo["FinacialYearStartDt"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>Book Start Date</td><td>" + Convert.ToString(DrWebInfo["BookStartDate"]) + "</td></tr>");
                strColor = "#ffffff";
            }
            else
            {
                MsbHtml.Append("<tr height=25px><td width=30%>Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["PCompanyName"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Branch Name</td><td width=70%>" + Convert.ToString(DrWebInfo["Name"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Employer Code</td><td width=70%>" + Convert.ToString(DrWebInfo["EPID"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Short Name</td><td width=70%>" + Convert.ToString(DrWebInfo["ShortName"]) + "</td></tr>" +
               "<tr  height=25px><td width=30%>Company Industry</td><td>" + Convert.ToString(DrWebInfo["CompanyIndustry"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Company Type</td><td>" + Convert.ToString(DrWebInfo["CompanyType"]) + "</td></tr>" +
               "<tr  height=25px><td width=30%>Company Or Branch</td><td>" + (Convert.ToBoolean(DrWebInfo["CompanyBranchIndicator"]) == true ? "Company" : "Branch") + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Telephone</td><td>" + Convert.ToString(DrWebInfo["ContactPersonPhone"]) + "</td></tr>" +
               "<tr  height=25px><td width=30%>Fax</td><td>" + Convert.ToString(DrWebInfo["PABXNumber"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Website</td><td>" + Convert.ToString(DrWebInfo["WebSite"]) + "</td></tr>" +

               "<tr  height=25px><td width=30%>Daily Pay based On</td><td>" + (Convert.ToBoolean(DrWebInfo["DailyPaymentType"]) == true ? "Month" : "Year") + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Working Days</td><td>" + Convert.ToString(DrWebInfo["WorkingDaysInMonth"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>OffDay</td><td>" + Convert.ToString(DrWebInfo["OffDay"]) + "</td></tr>" +

               "<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Contact Information</h2></font></td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Contact Person Name</td><td>" + Convert.ToString(DrWebInfo["ContactPerson"]) + "</td></tr>" +
               "<tr  height=25px><td>Contact Person Designation</td><td>" + Convert.ToString(DrWebInfo["ContactDesignation"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td>Primary Email</td><td>" + Convert.ToString(DrWebInfo["PrimaryEmail"]) + "</td></tr>" +
               "<tr  height=25px><td>Secondary Email</td><td>" + Convert.ToString(DrWebInfo["SecondaryEmail"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Postal Address</h2></font></td></tr>" +
               "<tr  height=25px><td>PO Box</td><td>" + Convert.ToString(DrWebInfo["POBox"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td>Street</td><td>" + Convert.ToString(DrWebInfo["Road"]) + "</td></tr>" +
               "<tr  height=25px><td>Area</td><td>" + Convert.ToString(DrWebInfo["Area"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td>Block</td><td>" + Convert.ToString(DrWebInfo["Block"]) + "</td></tr>" +
               "<tr  height=25px><td>City</td><td>" + Convert.ToString(DrWebInfo["City"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td>Province</td><td>" + Convert.ToString(DrWebInfo["Province"]) + "</td></tr>" +
               "<tr  height=25px><td>Country</td><td>" + Convert.ToString(DrWebInfo["Country"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td>Other Info</td><td>" + Convert.ToString(DrWebInfo["OtherInfo"]) + "</td></tr>" +
               "<tr  height=25px><td>Currency</td><td>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td>Financial Year Start Date</td><td>" + Convert.ToString(DrWebInfo["FinacialYearStartDt"]) + "</td></tr>" +
               "<tr height=25px><td>Book Start Date</td><td>" + Convert.ToString(DrWebInfo["BookStartDate"]) + "</td></tr>");
                strColor = "#fff2eb";
            }
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        // Company Details
        if (EmailSource.Tables[1].Rows.Count > 0)
        {       
        
        MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Bank Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>BankName</b></font></td>" +
                       "<td><font color=#FFFFF><b>Account Number</b></font></td></font></tr>");

        strColor = "#fff2eb";
        
        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
           
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["BankName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["AccountNo"]) + "</td></tr>");
                          
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }
        }
       
        else
        {
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Bank Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");
                
        }        
             
        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetVendorEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        // Company Master details
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            if (Convert.ToString(DrWebInfo["TransactionType"]) == "Cash")
            {
                if (Convert.ToInt32(DrWebInfo["VendorTypeID"]) == (int)VendorType.Customer) // For Customer
                {
                    MsbHtml.Append("<tr height=25px><td width=30%> Associative Type </td><td width=70%>" + Convert.ToString(DrWebInfo["Associative"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Code</td><td width=70%>" + Convert.ToString(DrWebInfo["VendorCode"]) + "</td></tr>" +
                    "<tr height=25px><td width=30%>Name</td><td width=70%>" + Convert.ToString(DrWebInfo["VendorName"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Short Name</td><td width=70%>" + Convert.ToString(DrWebInfo["ShortName"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>Gender</td><td width=70%>" + Convert.ToString(DrWebInfo["Gender"]) + "</td></tr>" +
                        //"<tr bgcolor=#fff2eb height=25px><td width=30%>EmploymentType</td><td width=70%>" + Convert.ToString(DrWebInfo["EmploymentType"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>Designation</td><td width=70%>" + Convert.ToString(DrWebInfo["Designation"]) + "</td></tr>" +
                        //"<tr bgcolor=#fff2eb height=25px><td width=30%>AccountType</td><td width=70%>" + Convert.ToString(DrWebInfo["AccountType"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>Salary/Income</td><td width=70%>" + Convert.ToString(DrWebInfo["SalaryOrIncome"]) + "</td></tr>" +
                    "<tr height=25px><td width=30%>Transaction Type</td><td>" + Convert.ToString(DrWebInfo["TransactionType"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Email</td><td>" + Convert.ToString(DrWebInfo["Email"]) + "</td></tr>" +
                    "<tr height=25px><td width=30%>Website</td><td>" + Convert.ToString(DrWebInfo["Website"]) + "</td></tr>" +

                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Rating</td><td>" + Convert.ToString(DrWebInfo["Rating"]) + "</td></tr>" +
                   "<tr height=25px><td width=30%>RatingRemarks</td><td>" + Convert.ToString(DrWebInfo["RatingRemarks"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Status</td><td>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
                    "<tr height=25px><td width=30%>Credit Limit</td><td>" + Convert.ToString(DrWebInfo["CreditLimit"]) + "</td></tr>" +
                    "<tr  bgcolor=#fff2eb height=25px><td width=30%>Advance Limit</td><td>" + Convert.ToString(DrWebInfo["AdvanceLimit"]) + "</td></tr>" +

                    "<tr height=25px><td width=30%>Credit Limit Days</td><td>" + Convert.ToString(DrWebInfo["CreditLimitDays"]) + "</td></tr>" +

                    //"<tr height=25px><td width=30%>Serv.Tax No</td><td>" + Convert.ToString(DrWebInfo["CompanyServiceTaxNo"]) + "</td></tr>" +
                        //"<tr bgcolor=#fff2eb height=25px><td width=30%>CST No</td><td>" + Convert.ToString(DrWebInfo["CompanyCSTNo"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>TIN No</td><td>" + Convert.ToString(DrWebInfo["CompanyTINNo"]) + "</td></tr>" +
                        //"<tr bgcolor=#fff2eb height=25px><td width=30%>PAN No</td><td>" + Convert.ToString(DrWebInfo["CompanyPANNo"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>VAT NO</td><td>" + Convert.ToString(DrWebInfo["VATRegistrationNo"]) + 
                    "</td></tr>");
                }
                else // For supplier
                {
                    MsbHtml.Append("<tr height=25px><td width=30%> Associative Type </td><td width=70%>" + Convert.ToString(DrWebInfo["Associative"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Code</td><td width=70%>" + Convert.ToString(DrWebInfo["VendorCode"]) + "</td></tr>" +
                    "<tr height=25px><td width=30%>Name</td><td width=70%>" + Convert.ToString(DrWebInfo["VendorName"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Short Name</td><td width=70%>" + Convert.ToString(DrWebInfo["ShortName"]) + "</td></tr>" +
                    "<tr height=25px><td width=30%>Transaction Type</td><td>" + Convert.ToString(DrWebInfo["TransactionType"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Email</td><td>" + Convert.ToString(DrWebInfo["Email"]) + "</td></tr>" +
                    "<tr height=25px><td width=30%>Website</td><td>" + Convert.ToString(DrWebInfo["Website"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Rating</td><td>" + Convert.ToString(DrWebInfo["Rating"]) + "</td></tr>" +
                    "<tr height=25px><td width=30%>RatingRemarks</td><td>" + Convert.ToString(DrWebInfo["RatingRemarks"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Status</td><td>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>SourceMedia</td><td>" + Convert.ToString(DrWebInfo["SourceMedia"]) + "</td></tr>" +
                    "<tr height=25px><td width=30%>classification</td><td>" + Convert.ToString(DrWebInfo["VendorClassification"]) + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td width=30%>Advance Limit</td><td>" + Convert.ToString(DrWebInfo["AdvanceLimit"]) + "</td></tr>" +
                        //"<tr bgcolor=#fff2eb height=25px><td width=30%>Factory Visited</td><td>" + Convert.ToString(DrWebInfo["IsFactoryVisited"]) + "</td></tr>" +



                        //"<tr height=25px><td width=30%>Serv.Tax No</td><td>" + Convert.ToString(DrWebInfo["CompanyServiceTaxNo"]) + "</td></tr>" +
                        //"<tr bgcolor=#fff2eb height=25px><td width=30%>CST No</td><td>" + Convert.ToString(DrWebInfo["CompanyCSTNo"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>TIN No</td><td>" + Convert.ToString(DrWebInfo["CompanyTINNo"]) + "</td></tr>" +
                        //"<tr bgcolor=#fff2eb height=25px><td width=30%>PAN No</td><td>" + Convert.ToString(DrWebInfo["CompanyPANNo"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>VAT NO</td><td>" + Convert.ToString(DrWebInfo["VATRegistrationNo"]) + 
                    "</td></tr>");

                }
            }
            else
            {
                if (Convert.ToInt32(DrWebInfo["VendorTypeID"]) == (int)VendorType.Customer) // For Customer
                {

                    MsbHtml.Append("<tr height=25px><td width=30%> Associative Type </td><td width=70%>" + Convert.ToString(DrWebInfo["Associative"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Code</td><td width=70%>" + Convert.ToString(DrWebInfo["VendorCode"]) + "</td></tr>" +
                    "<tr height=25px><td width=30%>Name</td><td width=70%>" + Convert.ToString(DrWebInfo["VendorName"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Short Name</td><td width=70%>" + Convert.ToString(DrWebInfo["ShortName"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>Gender</td><td width=70%>" + Convert.ToString(DrWebInfo["Gender"]) + "</td></tr>" +
                        //"<tr bgcolor=#fff2eb height=25px><td width=30%>EmploymentType</td><td width=70%>" + Convert.ToString(DrWebInfo["EmploymentType"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>Designation</td><td width=70%>" + Convert.ToString(DrWebInfo["Designation"]) + "</td></tr>" +
                        //"<tr bgcolor=#fff2eb height=25px><td width=30%>AccountType</td><td width=70%>" + Convert.ToString(DrWebInfo["AccountType"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>Salary/Income</td><td width=70%>" + Convert.ToString(DrWebInfo["SalaryOrIncome"]) + "</td></tr>" +


                    "<tr height=25px><td width=30%>Transaction Type</td><td>" + Convert.ToString(DrWebInfo["TransactionType"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Bank Name</td><td>" + Convert.ToString(DrWebInfo["BankName"]) + "</td></tr>" +
                     "<tr height=25px><td width=30%>Branch Name</td><td>" + Convert.ToString(DrWebInfo["BranchName"]) + "</td></tr>" +

                    //"<tr height=25px><td width=30%>Account Number</td><td>" + Convert.ToString(DrWebInfo["AccountNumber"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Account Head</td><td>" + Convert.ToString(DrWebInfo["AccountHead"]) + "</td></tr>" +
                    "<tr height=25px><td width=30%>Email</td><td>" + Convert.ToString(DrWebInfo["Email"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Website</td><td>" + Convert.ToString(DrWebInfo["Website"]) + "</td></tr>" +
                    "<tr height=25px><td width=30%>Rating</td><td>" + Convert.ToString(DrWebInfo["Rating"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>RatingRemarks</td><td>" + Convert.ToString(DrWebInfo["RatingRemarks"]) + "</td></tr>" +
                    "<tr height=25px><td width=30%>Status</td><td>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>SourceMedia</td><td>" + Convert.ToString(DrWebInfo["SourceMedia"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>Factory Visited</td><td>" + Convert.ToString(DrWebInfo["IsFactoryVisited"]) + "</td></tr>" +
                    "<tr bgcolor=#fff2eb height=25px><td width=30%>Credit Limit</td><td>" + Convert.ToString(DrWebInfo["CreditLimit"]) + "</td></tr>" +
                    "<tr height=25px><td width=30%>Advance Limit</td><td>" + Convert.ToString(DrWebInfo["AdvanceLimit"]) + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td width=30%>Credit Limit Days</td><td>" + Convert.ToString(DrWebInfo["CreditLimitDays"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>Serv.Tax No</td><td>" + Convert.ToString(DrWebInfo["CompanyServiceTaxNo"]) + "</td></tr>" +
                        //"<tr bgcolor=#fff2eb height=25px><td width=30%>CST No</td><td>" + Convert.ToString(DrWebInfo["CompanyCSTNo"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>TIN No</td><td>" + Convert.ToString(DrWebInfo["CompanyTINNo"]) + "</td></tr>" +
                        //"<tr bgcolor=#fff2eb height=25px><td width=30%>PAN No</td><td>" + Convert.ToString(DrWebInfo["CompanyPANNo"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>VAT NO</td><td>" + Convert.ToString(DrWebInfo["VATRegistrationNo"]) + 
                    "</td></tr>");
                }
                else //For supplier
                {
                    MsbHtml.Append("<tr height=25px><td width=30%> Associative Type </td><td width=70%>" + Convert.ToString(DrWebInfo["Associative"]) + "</td></tr>" +
                       "<tr bgcolor=#fff2eb height=25px><td width=30%>Code</td><td width=70%>" + Convert.ToString(DrWebInfo["VendorCode"]) + "</td></tr>" +
                       "<tr height=25px><td width=30%>Name</td><td width=70%>" + Convert.ToString(DrWebInfo["VendorName"]) + "</td></tr>" +
                       "<tr bgcolor=#fff2eb height=25px><td width=30%>Short Name</td><td width=70%>" + Convert.ToString(DrWebInfo["ShortName"]) + "</td></tr>" +
                       "<tr height=25px><td width=30%>Transaction Type</td><td>" + Convert.ToString(DrWebInfo["TransactionType"]) + "</td></tr>" +
                       "<tr bgcolor=#fff2eb height=25px><td width=30%>Bank Name</td><td>" + Convert.ToString(DrWebInfo["BankName"]) + "</td></tr>" +
                       "<tr height=25px><td width=30%>Branch Name</td><td>" + Convert.ToString(DrWebInfo["BranchName"]) + "</td></tr>" +


                       //"<tr bgcolor=#fff2eb height=25px><td width=30%>Account Number</td><td>" + Convert.ToString(DrWebInfo["AccountNumber"]) + "</td></tr>" +
                       "<tr bgcolor=#fff2eb height=25px><td width=30%>Account Head</td><td>" + Convert.ToString(DrWebInfo["AccountHead"]) + "</td></tr>" +
                       "<tr height=25px><td width=30%>Email</td><td>" + Convert.ToString(DrWebInfo["Email"]) + "</td></tr>" +
                       "<tr bgcolor=#fff2eb height=25px><td width=30%>Website</td><td>" + Convert.ToString(DrWebInfo["Website"]) + "</td></tr>" +
                       "<tr height=25px><td width=30%>Rating</td><td>" + Convert.ToString(DrWebInfo["Rating"]) + "</td></tr>" +
                       "<tr bgcolor=#fff2eb height=25px><td width=30%>RatingRemarks</td><td>" + Convert.ToString(DrWebInfo["RatingRemarks"]) + "</td></tr>" +
                       "<tr height=25px><td width=30%>Status</td><td>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
                        //"<tr bgcolor=#fff2eb height=25px><td width=30%>SourceMedia</td><td>" + Convert.ToString(DrWebInfo["SourceMedia"]) + "</td></tr>" +
                       "<tr bgcolor=#fff2eb height=25px><td width=30%>Classification</td><td>" + Convert.ToString(DrWebInfo["VendorClassification"]) + "</td></tr>" +
                       "<tr height=25px><td width=30%>Advance Limit</td><td>" + Convert.ToString(DrWebInfo["AdvanceLimit"]) + "</td></tr>" +

                        //"<tr height=25px><td width=30%>Serv.Tax No</td><td>" + Convert.ToString(DrWebInfo["CompanyServiceTaxNo"]) + "</td></tr>" +
                        //"<tr bgcolor=#fff2eb height=25px><td width=30%>CST No</td><td>" + Convert.ToString(DrWebInfo["CompanyCSTNo"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>TIN No</td><td>" + Convert.ToString(DrWebInfo["CompanyTINNo"]) + "</td></tr>" +
                        //"<tr bgcolor=#fff2eb height=25px><td width=30%>PAN No</td><td>" + Convert.ToString(DrWebInfo["CompanyPANNo"]) + "</td></tr>" +
                        //"<tr height=25px><td width=30%>VAT NO</td><td>" + Convert.ToString(DrWebInfo["VATRegistrationNo"]) + 
                       "</td></tr>");
                }
            }
        }
        //234
        //MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Other Information</h2></font></td></tr>");

        DataRow DrWebInfo1 = EmailSource.Tables[0].Rows[0];
        if (Convert.ToInt32(DrWebInfo1["VendorTypeID"]) == (int)VendorType.Customer) // For Customer
        {
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Other Information</h2></font></td></tr>");


            MsbHtml.Append("<tr height=25px><td width=30%> Gender</td><td width=70%>" + Convert.ToString(DrWebInfo1["Gender"]) + "</td></tr>" +
                             "<tr bgcolor=#fff2eb height=25px><td width=30%>Department </td><td width=70%>" + Convert.ToString(DrWebInfo1["Department"]) + "</td></tr>" +
                             "<tr height=25px><td width=30%>Designation </td><td width=70%>" + Convert.ToString(DrWebInfo1["Designation"]) + "</td></tr>" +
                             "<tr bgcolor=#fff2eb height=25px><td width=30%>Account Type </td><td width=70%>" + Convert.ToString(DrWebInfo1["AccountType"]) + "</td></tr>" +
                             "<tr height=25px><td width=30%>Employement Type </td><td width=70%>" + Convert.ToString(DrWebInfo1["EmploymentType"]) + "</td></tr>" +
                             "<tr bgcolor=#fff2eb height=25px><td width=30%>Salary/income</td><td width=70%>" + Convert.ToString(DrWebInfo1["SalaryOrIncome"]) + "</td></tr>" +
                //   "<tr height=25px><td width=30%>Source Media</td><td width=70%>" + Convert.ToString(DrWebInfo1["SourceMedia"]) + "</td></tr>" +

             "</td></tr>");
        }

        //234

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Contact Information</h2></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            if (Convert.ToInt32(drowEmail["VendorTypeID"]) == (int)VendorType.Customer)//For Customer
            {
                MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>" + Convert.ToString(drowEmail["AddressName"]) + "</h2></font></td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>Contact Person Name</td><td>" + Convert.ToString(drowEmail["Contact"]) + "</td></tr>" +
                           "<tr height=25px><td>Mailing Address</td><td>" + Convert.ToString(drowEmail["Address"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Country</td><td>" + Convert.ToString(drowEmail["Country"]) + "</td></tr>" +
                           "<tr height=25px><td>State</td><td>" + Convert.ToString(drowEmail["State"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Zip Code</td><td>" + Convert.ToString(drowEmail["ZipCode"]) + "</td></tr>" +
                           "<tr height=25px><td>Telephone No</td><td>" + Convert.ToString(drowEmail["Telephone"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Mobile No</td><td>" + Convert.ToString(drowEmail["MobileNo"]) + "</td></tr>" +
                           "<tr height=25px><td>Fax</td><td>" + Convert.ToString(drowEmail["Fax"]) + "</td></tr>");
                //"<tr bgcolor=#fff2eb height=25px><td>BuildingType</td><td>" + Convert.ToString(drowEmail["BuildingType"]) + "</td></tr>" +
                //"<tr height=25px><td>BuildingNo</td><td>" + Convert.ToString(drowEmail["BuildingNo"]) + "</td></tr>" +
                //"<tr bgcolor=#fff2eb height=25px><td>Landmark</td><td>" + Convert.ToString(drowEmail["Landmark"]) + "</td></tr>");
            }
            else // For Supplier
            {
                MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>" + Convert.ToString(drowEmail["AddressName"]) + "</h2></font></td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>Contact Person Name</td><td>" + Convert.ToString(drowEmail["Contact"]) + "</td></tr>" +
                           "<tr height=25px><td>Mailing Address</td><td>" + Convert.ToString(drowEmail["Address"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Country</td><td>" + Convert.ToString(drowEmail["Country"]) + "</td></tr>" +
                           "<tr height=25px><td>State</td><td>" + Convert.ToString(drowEmail["State"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Zip Code</td><td>" + Convert.ToString(drowEmail["ZipCode"]) + "</td></tr>" +
                           "<tr height=25px><td>Telephone No</td><td>" + Convert.ToString(drowEmail["Telephone"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Mobile No</td><td>" + Convert.ToString(drowEmail["MobileNo"]) + "</td></tr>" +
                           "<tr height=25px><td>Fax</td><td>" + Convert.ToString(drowEmail["Fax"]) + "</td></tr>");

            }
        }


        //111
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        if (EmailSource.Tables[3].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[3].Rows[0];
            if (Convert.ToInt32(DrWebInfo["VendorTypeID"]) == (int)VendorType.Supplier)
            {
                if (Convert.ToString(EmailSource.Tables[3].Rows[0]["VendorName"]) != "")
                {

                    strColor = "#fff2eb";
                    MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[3].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Nearest Supplier Details</h2></font></td></tr>");
                    MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>S.No</b></font></td>" +
                                   "<td><font color=#FFFFF><b>supplier Name</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Rating</b></font></td></font></tr>");

                    int RowIndex = 1; // For Display Serial No

                    foreach (DataRow drowEmail in EmailSource.Tables[3].Rows)
                    {
                        MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + RowIndex + "</td>" +
                                        "<td>" + Convert.ToString(drowEmail["VendorName"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["Rating"]) + "</td></tr>");

                        if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

                        RowIndex = RowIndex + 1;
                    }

                }
            }
        }
        //111



        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        if (EmailSource.Tables[2].Rows.Count > 0)
        {
            if (Convert.ToString(EmailSource.Tables[2].Rows[0]["DocumentNo"]) != "")
            {

                strColor = "#fff2eb";

                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Document Details</h2></font></td></tr>");
                MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Document No</b></font></td>" +
                               "<td><font color=#FFFFF><b>Document Type</b></font></td><td><font color=#FFFFF><b>Expiry Date</b></font></td>" +
                               "<td><font color=#FFFFF><b>Status</b></font></td></font></tr>");

                foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
                {
                    MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["DocumentNo"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["DocumentType"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["ExpiryDate"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["DocumentStatus"]) + "</td></tr>");

                    if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                }
            }
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        //123


        return MsbHtml.ToString();

    }

    

    private string GetDeliveryScheduleEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Schedule No</td><td width=70%>" + Convert.ToString(DrWebInfo["ScheduleNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Assigned By</td><td width=70%>" + Convert.ToString(DrWebInfo["AssignedBy"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%> Assigned To</td><td>" + Convert.ToString(DrWebInfo["AssignedTo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Warehouse</td><td>" + Convert.ToString(DrWebInfo["Warehouse"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Route Name</td><td>" + Convert.ToString(DrWebInfo["RouteName"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>From Date</td><td>" + Convert.ToString(DrWebInfo["FromDate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>To Date</td><td>" + Convert.ToString(DrWebInfo["ToDate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Status</td><td>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";
        MsbHtml.Append("<tr bgcolor=#ffffff height=25px><td colspan=14 align=left><font size=2 color=#3a3a53><h2>Delivery Schedule Details</h2></font></td></tr>");

        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Operation Type</b></font></td>" +
                       "<td  ><font color=#FFFFF><b>Ref No</b></font></td>" +
                       "<td ><font color=#FFFFF><b>Customer Name & Mobile No</b></font></td><td width=200 ><font color=#FFFFF><b>Location</b></font></td>" +
                       "<td  ><font color=#FFFFF><b>Item Name</b></font></td>" +
                       "<td  ><font color=#FFFFF><b>Item Quantity</b></font></td><td width=200><font color=#FFFFF><b>Delivered Quantity</b></font></td></font></tr>");                      

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["OperationType"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ReferenceNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["VendorName"]) + "-" + Convert.ToString(drowEmail["MobileNo1"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Location"]) + "</td>" + 
                           "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Quantity"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["DeliveredQty"]) + "</td></tr>");                         

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }

    private string GetDeliveryStatusUpdationEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Schedule No</td><td width=70%>" + Convert.ToString(DrWebInfo["ScheduleNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Assigned By</td><td width=70%>" + Convert.ToString(DrWebInfo["AssignedBy"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%> Assigned To</td><td>" + Convert.ToString(DrWebInfo["AssignedTo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Warehouse</td><td>" + Convert.ToString(DrWebInfo["Warehouse"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Route Name</td><td>" + Convert.ToString(DrWebInfo["RouteName"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>From Date</td><td>" + Convert.ToString(DrWebInfo["FromDate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>To Date</td><td>" + Convert.ToString(DrWebInfo["ToDate"]) + "</td></tr>"+
               "<tr height=25px><td width=30%>Starting KM</td><td>" + Convert.ToString(DrWebInfo["StartingKM"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Ending KM</td><td>" + Convert.ToString(DrWebInfo["EndingKM"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Vehicle No</td><td>" + Convert.ToString(DrWebInfo["VehicleNo"]) + "</td></tr>");    
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";
        
        MsbHtml.Append("<tr bgcolor=#ffffff height=25px><td colspan=6 align=left><font size=2 color=#3a3a53><h2>Delivery Status Updation Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Operation Type</b></font></td>" +
                       "<td><font color=#FFFFF><b>Reference No</b></font></td><td><font color=#FFFFF><b>Item Name</b></font></td>" +
                       "<td><font color=#FFFFF><b>Quantity</b></font></td><td><font color=#FFFFF><b>Delivered Quantity</b></font></td>" +
                       "<td><font color=#FFFFF><b>Usage</b></font></td><td><font color=#FFFFF><b>Left Time</b></font></td>" +
                        "<td><font color=#FFFFF><b>Reached Time</b></font></td>" +
                                              "<td><font color=#FFFFF><b>Status</b></font></td></tr>");
                 

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["OperationType"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ReferenceNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Quantity"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["DeliveredQty"]) + "</td>" +
                            "<td>" + Convert.ToString(drowEmail["Usage"]) + "</td>" +
                             "<td>" + Convert.ToString(drowEmail["LeftTime"]) + "</td>" +
                              "<td>" + Convert.ToString(drowEmail["ReachedTime"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Status"]) + "</td></tr>"); 
             
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }

    private string GetComplaintRegistrationEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Customer</td><td width=70%>" + Convert.ToString(DrWebInfo["VendorName"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Operation Type</td><td width=70%>" + Convert.ToString(DrWebInfo["OperationType"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%> Reference No</td><td>" + Convert.ToString(DrWebInfo["ReferenceNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Status</td><td>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Complaint Date</td><td>" + Convert.ToString(DrWebInfo["ComplaintDate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Complaint No</td><td>" + Convert.ToString(DrWebInfo["ComplaintNo"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Complaint</td><td>" + Convert.ToString(DrWebInfo["ComplaintRemarks"])  + "</td></tr>");
                     }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";

        MsbHtml.Append("<tr height=25px><td colspan= 6 align=left><font size=2 color=#3a3a53><h2>Complaint Registration Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item code</b></font></td>" +
                       "<td><font color=#FFFFF><b>Item Name</b></font></td><td><font color=#FFFFF><b>Rate</b></font></td>" +
                         "<td><font color=#FFFFF><b>Quantity</b></font></td>" +
                       "<td><font color=#FFFFF><b>Location</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[0].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["itemname"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                            "<td>" + Convert.ToString(drowEmail["Quantity"]) +  Convert.ToString(drowEmail["UOMName"]) + "</td>" +                          
                           "<td>" + Convert.ToString(drowEmail["Location"]) + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }

    private string GetComplaintScheduleEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Schedule No</td><td width=70%>" + Convert.ToString(DrWebInfo["ScheduleNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Assigned By</td><td width=70%>" + Convert.ToString(DrWebInfo["AssignedBy"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Assigned To</td><td>" + Convert.ToString(DrWebInfo["AssignedTo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>From Date</td><td>" + Convert.ToString(DrWebInfo["FromDate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>To Date</td><td>" + Convert.ToString(DrWebInfo["ToDate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Status</td><td>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Vehicle</td><td>" + Convert.ToString(DrWebInfo["RegistrationNumber"]) + "</td></tr>" +
               "<tr  height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";

        MsbHtml.Append("<tr height=25px><td colspan= 8 align=left><font size=2 color=#3a3a53><h2>Complaint Schedule Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Complaint No</b></font></td>" +
                       "<td><font color=#FFFFF><b>Operation Type</b></font></td><td><font color=#FFFFF><b>Ref No</b></font></td>" +
                         "<td><font color=#FFFFF><b>Item Name</b></font></td><td><font color=#FFFFF><b>Location</b></font></td>" +
                       "<td><font color=#FFFFF><b>Complaint</b></font></td><td><font color=#FFFFF><b>Remarks</b></font></td>" +
                       "<td><font color=#FFFFF><b>Status</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ComplaintNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["OperationType"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ReferenceNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Location"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Complaint"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Remarks"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Status"])  + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }
    private string GetDeduction()
    {
        string strRateonly = "", strPercentage = "";
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            if ((drowEmail["RateOnly"]).ToInt32() == 1) { strRateonly = "Yes"; } else { strRateonly = "No"; strPercentage = "(%)"; }

            // Master data
            MsbHtml.Append("<tr height=25px><td>PolicyName</td><td>" + Convert.ToString(drowEmail["PolicyName"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>Deduction</td><td>" + Convert.ToString(drowEmail["Particulars"]) + "</td></tr>" +
                            "<tr height=25px><td>Deduct From</td><td>" + Convert.ToString(drowEmail["DeductFrom"]) + "</td></tr>" +
                             "<tr bgcolor=#fff2eb height=25px><td>Rate Only</td><td>" + strRateonly + "</td></tr>" +
                           "<tr height=25px><td>PolicyCondition</td><td>" + Convert.ToString(drowEmail["PolicyCondition"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Effect From</td><td>" + Convert.ToString(drowEmail["WithEffectFrom"]) + "</td></tr>" +
                           "<tr height=25px><td>Employer Contribution" + strPercentage + "</td><td>" + Convert.ToString(drowEmail["EmployerPart"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>Employee Contribution" + strPercentage + "</td><td>" + Convert.ToString(drowEmail["EmployeePart"]) + "</td></tr>");
            if (drowEmail["RateOnly"].ToInt32() == 3)
                MsbHtml.Append("<tr height=25px><td>Fixed Amount</td><td>" + Convert.ToString(drowEmail["FixedAmount"]) + "</td></tr>");
            if (drowEmail["RateOnly"].ToInt32() != 1)
                MsbHtml.Append("<tr height=25px><td>Particulars</td><td>" + Convert.ToString(EmailSource.Tables[1].Rows[0]["ParicularName"]) + "</td></tr>");

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }
    private string GetEmployeeSettlement()
    {
        mFlag = false;
        GetWebformHeaderInfo();

        string sType = "";
        DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            if (DrWebInfo["WorkStatus"] != System.DBNull.Value)
            {
                sType = Convert.ToString(DrWebInfo["WorkStatus"]);
            }
            MsbHtml.Append("<table border='0' cellspading='0' cellspacing='0' width='100%'> " +
                            "<tr height='25px'><td colspan='2' align='left'><font size='2' color='#3a3a53'><b>General Information</b></font></td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Employee</td><td align='left'>" + Convert.ToString(DrWebInfo["EmployeeFullName"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Joining Date</td><td align='left'>" + Convert.ToString(DrWebInfo["FromDate"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Type</td><td align='left'>" + Convert.ToString(DrWebInfo["WorkStatus"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>To Date</td><td align='left'>" + Convert.ToString(DrWebInfo["ToDate"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Process Date</td><td align='left'>" + Convert.ToString(DrWebInfo["Date"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Document Return</td><td align='left'>" + Convert.ToString(DrWebInfo["DocumentReturn"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Earnings</td><td align='left'>" + Convert.ToString(DrWebInfo["Credit"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Deductions</td><td align='left'>" + Convert.ToString(DrWebInfo["Debit"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Net Amount</td><td align='left'>" + Convert.ToString(DrWebInfo["NetAmount"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Currency</td><td align='left'>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Remarks</td><td align='left'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>" +
                         " </table>");
            mFlag = true;
        }
        else
        {
            mFlag = false;
        }


        if (mFlag == true)
        {
            MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><b>Settlement Info</b></font></td></tr>" +
                      "<tr><td colspan=2>" + GetEmployeeSettlementDetails() + " </td></tr>");
        }
        else
        {
            MsbHtml.Append("<html><head></head><body bgcolor=White oncontextmenu=return false ondragstart=return false onselectstart=return false><center><img src='" + imgPath + "' border=none alt=No Information Found></img></center></body></html>");
        }
        return MsbHtml.ToString();
    }
    private string GetEmployeeSettlementDetails()
    {
        StringBuilder sbHtmlEarning = new StringBuilder();

        sbHtmlEarning.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>" +
                             "<tr bgcolor=#3a3a53><td width=35% align=left><font size=2 color=white>Particulars</td><td align=left><font size=2 color=white>Earning</td>" +
                             "<td align=left><font size=2 color=white>Deduction</td></tr>");

        string color = "#e5e5ef";
        bool blExists = false;


        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            sbHtmlEarning.Append(" <tr bgcolor=" + color + "><td align='left'>" + Convert.ToString(drowEmail["Particulars"]) + "</td> " +
                               " <td align='left'>" + Convert.ToString(drowEmail["Credit"]) + "</td> " +
                               " <td align='left'>" + Convert.ToString(drowEmail["Debit"]) + "</td> " +
                               " </tr> ");
            if (color == "#e5e5ef")
            {
                color = "#FFFFFF";
            }
            else
            {
                color = "#e5e5ef";
            }

        }

        blExists = true;

        if (blExists == false)
        {
            sbHtmlEarning.Append("<table border=0 cellspading=0 cellspacing=0 width=100%><tr><td align=left><body bgcolor=White><font color=blue>No Earning Found</font></td></tr></table>");
        }
        else
        {
            sbHtmlEarning.Append("</table>");
        }
        return sbHtmlEarning.ToString();

    }

    private string GetComplaintStatusUpdationEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Schedule No</td><td width=70%>" + Convert.ToString(DrWebInfo["ScheduleNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Assigned By</td><td width=70%>" + Convert.ToString(DrWebInfo["AssignedBy"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Assigned To</td><td width=70%>" + Convert.ToString(DrWebInfo["AssignedTo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Vehicle</td><td width=70%>" + Convert.ToString(DrWebInfo["RegistrationNumber"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Starting Km</td><td width=70%>" + Convert.ToString(DrWebInfo["StartingKM"]) + "</td></tr>" +
               "<tr  height=25px><td width=30%>Ending Km</td><td>" + Convert.ToString(DrWebInfo["EndingKM"]) + "</td></tr>");              
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";

        MsbHtml.Append("<tr height=25px><td colspan= 8 align=left><font size=2 color=#3a3a53><h2>Complaint Status Updation Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Complaint No</b></font></td>" +
                       "<td><font color=#FFFFF><b>Complaint Date</b></font></td>" +
                       "<td><font color=#FFFFF><b>Type</b></font></td><td><font color=#FFFFF><b>Ref No</b></font></td>" +
                       "<td><font color=#FFFFF><b>Item Name</b></font></td>" +
                         "<td><font color=#FFFFF><b>Complaint</b></font></td>" +
                       "<td><font color=#FFFFF><b>Expense</b></font></td><td><font color=#FFFFF><b>Remarks</b></font></td>" +
                       "<td><font color=#FFFFF><b>Reched Time</b></font></td><td><font color=#FFFFF><b>Left Time</b></font></td>" +
                       "<td><font color=#FFFFF><b>Status</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ComplaintNo"]) + "</td>" +
                            "<td>" + Convert.ToString(drowEmail["ComplaintDate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["OperationType"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ReferenceNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +                          
                           "<td>" + Convert.ToString(drowEmail["Complaint"]) + "</td>" +
                            "<td>" + Convert.ToString(drowEmail["Expense"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Remarks"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ReachedTime"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["LeftTime"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Status"]) + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";

        MsbHtml.Append("<tr height=25px><td colspan= 8 align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Expense Type</b></font></td>" +
                       "<td><font color=#FFFFF><b>Date</b></font></td>" +
                       "<td><font color=#FFFFF><b>Quantity</b></font></td>" +
                       "<td><font color=#FFFFF><b>Amount</b></font></td>" +
                         "<td><font color=#FFFFF><b>Total</b></font></td>" +
                       "<td><font color=#FFFFF><b>Description</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ExpenseType"]) + "</td>" +
                            "<td>" + Convert.ToString(drowEmail["CreatedDate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ExpenseQuantity"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ExpenseDetailAmount"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Total"]) + "</td>" +
       
             
                           "<td>" + Convert.ToString(drowEmail["ExpenseDescription"]) + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }
   
    private string GetWarehouseEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%> Name </td><td width=70%>" + Convert.ToString(DrWebInfo["WarehouseName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Short Name</td><td width=70%>" + Convert.ToString(DrWebInfo["ShortName"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["Company"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%> Address1 </td><td>" + Convert.ToString(DrWebInfo["Address1"]) + "</td></tr>" +
               "<tr height=25px><td width=30%> Address2 </td><td>" + Convert.ToString(DrWebInfo["Address2"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Zip Code</td><td>" + Convert.ToString(DrWebInfo["ZipCode"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>City</td><td>" + Convert.ToString(DrWebInfo["CityState"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Country</td><td>" + Convert.ToString(DrWebInfo["Country"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Description</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>In Charge</td><td>" + Convert.ToString(DrWebInfo["EmployeeFullName"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Phone No</td><td>" + Convert.ToString(DrWebInfo["Phone"]) + "</td></tr>");
        }

        //MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        //strColor = "#fff2eb";

        //MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Location Details</h2></font></td></tr>");
        //MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Location Name</b></font></td>" +
        //               "<td><font color=#FFFFF><b>Row Number</b></font></td><td><font color=#FFFFF><b>Block Number</b></font></td>" +
        //               "<td><font color=#FFFFF><b>Lot Number</b></font></td></font></tr>");

        //foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        //{
        //    MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["Location"]) + "</td>" +
        //                   "<td>" + Convert.ToString(drowEmail["RowNumber"]) + "</td>" +
        //                   "<td>" + Convert.ToString(drowEmail["BlockNumber"]) + "</td>" +
        //                   "<td>" + Convert.ToString(drowEmail["LotNumber"]) + "</td></tr>");

        //    if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        //}

        //MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }
    private string GetRFQEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>RFQ No</td><td width=70%>" + Convert.ToString(DrWebInfo["RFQNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Quotation Date</td><td width=70%>" + Convert.ToString(DrWebInfo["QuotationDate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Due Date</td><td>" + Convert.ToString(DrWebInfo["DueDate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Employee</td><td>" + Convert.ToString(DrWebInfo["EmployeeName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Status</td><td>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Cancelled By</td><td>" + Convert.ToString(DrWebInfo["Cancelledby"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>cancel Date</td><td>" + Convert.ToString(DrWebInfo["canceldate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Cancel Description</td><td>" + Convert.ToString(DrWebInfo["CancelDescription"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");
               }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";

        MsbHtml.Append("<tr height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>RFQ Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item Code</b></font></td>" +
                       "<td><font color=#FFFFF><b>Item Name</b></font></td><td><font color=#FFFFF><b>Quantity</b></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Quantity"]) + Convert.ToString(drowEmail["UOM"])+"</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }

    private string GetCollectionStatusUpdationEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        List<string> lstScheduleNos = new List<string>();
        foreach (DataRow dr in EmailSource.Tables[0].Rows)
        {
            if (!lstScheduleNos.Contains(dr["ScheduleNo"].ToString()))
                lstScheduleNos.Add(dr["ScheduleNo"].ToString());
        }
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
           if(lstScheduleNos.Count >1)
            {

                MsbHtml.Append("<tr height=25px><td width=30%> Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Executive</td><td width=70%>" + Convert.ToString(DrWebInfo["Executive"]) + "</td></tr>" +
                             "<tr height=25px><td width=30%>ScheduleNo </td><td width=70%> "+ "All" +" </td></tr>");               

            }
            else
            {
                MsbHtml.Append("<tr height=25px><td width=30%> Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
              "<tr bgcolor=#fff2eb height=25px><td width=30%>Executive</td><td width=70%>" + Convert.ToString(DrWebInfo["Executive"]) + "</td></tr>" +
                           "<tr height=25px><td width=30%>ScheduleNo </td><td width=70%>" + Convert.ToString(DrWebInfo["ScheduleNo"]) + "</td></tr>");           
            }          
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Collection Status Updation Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Sales Order No</b></font></td>" +
                       "<td><font color=#FFFFF><b>Customer</b></font></td><td><font color=#FFFFF><b>Cheque No</b></font></td>" +
                       "<td><font color=#FFFFF><b>Cheque Date</b></font></td><td><font color=#FFFFF><b>Amount</b></font></td>" +
                        "<td><font color=#FFFFF><b>Cheque Status</b></font></td><td><font color=#FFFFF><b>Commitment date</b></font></td>" +
                       " <td><font color=#FFFFF><b>Call Status</b></font></td>" +
                       "<td><font color=#FFFFF><b>Remarks</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
           

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["SalesOrderNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["VendorName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ChequeNo"]) + "</td>" +
                            "<td>" + Convert.ToString(drowEmail["ChequeDate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Amount"]) + "</td>" +
                            "<td>" + Convert.ToString(drowEmail["ChequeStatus"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["CommitmentDate"]) + "</td>" +
                              "<td>" + Convert.ToString(drowEmail["CallStatus"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Remarks"]) + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }

   
    private string GetCollectionScheduleEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Schedule No</td><td width=70%>" + Convert.ToString(DrWebInfo["ScheduleNo"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Assigned By</td><td width=70%>" + Convert.ToString(DrWebInfo["AssignedBy"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Assigned To</td><td width=70%>" + Convert.ToString(DrWebInfo["AssignedTo"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Company Name</td><td>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>From Date</td><td>" + Convert.ToString(DrWebInfo["FromDate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>To Date</td><td>" + Convert.ToString(DrWebInfo["ToDate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Status</td><td>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");
               
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Collection Schedule Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Sales Order No</b></font></td>" +
                       "<td><font color=#FFFFF width=30%><b>Company Name</b></font></td>" +
                       "<td><font color=#FFFFF><b>Customer Name</b></font></td><td><font color=#FFFFF><b>Cheque No</b></font></td>" +
                       "<td><font color=#FFFFF><b>Cheque Date</b></font></td><td><font color=#FFFFF><b>Amount</b></font></td>" +
                       "<td><font color=#FFFFF><b>Total Installments</b></font></td>"+
                       "<td><font color=#FFFFF><b>Paid Installments</b></font></td><td><font color=#FFFFF><b>Total Amount</b></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["SalesOrderNo"]) + "</td>" +
                           "<td width=30%>" + Convert.ToString(drowEmail["CompanyName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["VendorName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ChequeNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ChequeDate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Amount"]) + "</td>"+
                           "<td>" + Convert.ToString(drowEmail["TotalInstallments"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["PaidInstallments"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["TotalAmount"]) + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }
    private string GetInstallmentCollectionEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>CollectionNo</td><td width=70%>" + Convert.ToString(DrWebInfo["CollectionNo"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Customer Name</td><td width=70%>" + Convert.ToString(DrWebInfo["VendorName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Account Date</td><td>" + Convert.ToString(DrWebInfo["AccountDate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Currency</td><td>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Description</td><td>" + Convert.ToString(DrWebInfo["Description"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>TotalAmount</td><td>" + Convert.ToString(DrWebInfo["TotalAmount"]) + "</td></tr>" );              
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Installment Collection Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Sales Order No</b></font></td>" +
                       "<td><font color=#FFFFF><b>Cheque No</b></font></td><td><font color=#FFFFF><b>Cheque Date</b></font></td>" +
                       "<td><font color=#FFFFF><b>Amount</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["SalesOrderNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ChequeNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ChequeDate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Amount"]) + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }


    private string GetEmployeeEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%> Employee Number </td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeNumber"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Full Name</td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeFullName"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Short Name</td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeShortName"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Visa Obtained From</td><td>" + Convert.ToString(DrWebInfo["VComp"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Official Email</td><td>" + Convert.ToString(DrWebInfo["OfficialEmail"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Date of Joining</td><td>" + Convert.ToString(DrWebInfo["doj"]) + "</td></tr>" +
                "<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Status</h2></font></td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Work Status</td><td width=70%>" + Convert.ToString(DrWebInfo["workstatusdescription"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Working At</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Department</td><td>" + Convert.ToString(DrWebInfo["dept"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Designation</td><td>" + Convert.ToString(DrWebInfo["Designation"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Bank Identity</h2></font></td></tr>" +
                "<tr height=25px><td width=30%>Transaction Type</td><td>" + Convert.ToString(DrWebInfo["transactiontype"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Employer BankName</td><td>" + Convert.ToString(DrWebInfo["employerbankname"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Employee BankName</td><td>" + Convert.ToString(DrWebInfo["employeebankname"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Employer Bank Acc</td><td>" + Convert.ToString(DrWebInfo["Employerbankacc"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Account Number</td><td>" + Convert.ToString(DrWebInfo["AccountNumber"]) + "</td></tr>" +
                //"<tr  bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Dates</h2></font></td></tr>" +
                //"<tr  height=25px><td width=30%>Probation End Date</td><td>" + Convert.ToString(DrWebInfo["dopend"]) + "</td></tr>" +
                //"<tr  bgcolor=#fff2eb height=25px><td width=30%>Performance Review Date</td><td>" + Convert.ToString(DrWebInfo["dopreview"]) + "</td></tr>" +
                "<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Personal Contacts</h2></font></td></tr>" +
                "<tr  bgcolor=#fff2eb height=25px><td width=30%>Gender</td><td>" + Convert.ToString(DrWebInfo["Gender"]) + "</td></tr>" +
                "<tr  height=25px><td width=30%>Country Of Origin</td><td>" + Convert.ToString(DrWebInfo["Country"]) + "</td></tr>" +
                "<tr  bgcolor=#fff2eb height=25px><td width=30%>Date Of Marriage</td><td>" + Convert.ToString(DrWebInfo["dom"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Marital Status</td><td>" + Convert.ToString(DrWebInfo["meritalstatus"]) + "</td></tr>" +
                "<tr   bgcolor=#fff2eb height=25px><td width=30%>Nationality</td><td>" + Convert.ToString(DrWebInfo["NationalityDescription"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Religion</td><td>" + Convert.ToString(DrWebInfo["Religion"]) + "</td></tr>" +
                "<tr   bgcolor=#fff2eb height=25px><td>Blood Group</td><td>" + Convert.ToString(DrWebInfo["BloodGroup"]) + "</td></tr>" +
                "<tr  height=25px><td width=30%>Date Of Birth</td><td>" + Convert.ToString(DrWebInfo["dob"]) + "</td></tr>" +
                "<tr  bgcolor=#fff2eb height=25px><td width=30%>Ethnic Origin</td><td>" + Convert.ToString(DrWebInfo["EthnicOrigin"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Languages known</td><td>" + Convert.ToString(DrWebInfo["LanguagesKnown"]) + "</td></tr>" +
                "<tr  bgcolor=#fff2eb height=25px><td width=30%>Identification Marks</td><td>" + Convert.ToString(DrWebInfo["IdentificationMarks"]) + "</td></tr>" +
                "<tr  height=25px><td>Mother Tongue</td><td>" + Convert.ToString(DrWebInfo["mothertongue"]) + "</td></tr>" +
                "<tr   bgcolor=#fff2eb height=25px><td>Spouse Name</td><td>" + Convert.ToString(DrWebInfo["SpouseName"]) + "</td></tr>" +
                "<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Family Related</h2></font></td></tr>" +
                "<tr   bgcolor=#fff2eb height=25px><td>Father's Name</td><td>" + Convert.ToString(DrWebInfo["FathersName"]) + "</td></tr>" +
                "<tr  height=25px><td>Mother's Name</td><td>" + Convert.ToString(DrWebInfo["MothersName"]) + "</td></tr>" +
                "<tr   bgcolor=#fff2eb height=25px><td>Father's Job</td><td>" + Convert.ToString(DrWebInfo["FathersJob"]) + "</td></tr>" +
                "<tr  height=25px><td>Mother's Job</td><td>" + Convert.ToString(DrWebInfo["MothersJob"]) + "</td></tr>" +
                "<tr   bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Local Contacts</h2></font></td></tr>" +
                "<tr height=25px><td>Phone</td><td>" + Convert.ToString(DrWebInfo["LocalPhone"]) + "</td></tr>" +
                "<tr  bgcolor=#fff2eb height=25px><td>Mobile</td><td>" + Convert.ToString(DrWebInfo["LocalMobile"]) + "</td></tr>" +
                "<tr  height=25px><td>Fax</td><td>" + Convert.ToString(DrWebInfo["LocalFax"]) + "</td></tr>" +
                "<tr  bgcolor=#fff2eb height=25px><td>Email</td><td>" + Convert.ToString(DrWebInfo["LocalEmailID"]) + "</td></tr>" +
                "<tr  height=25px><td>Address</td><td>" + Convert.ToString(DrWebInfo["LocalAddress"]) + "</td></tr>" +
                "<tr   bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Other Informations</h2></font></td></tr>" +
                "<tr height=25px><td>Visa Taken By</td><td>" + Convert.ToString(DrWebInfo["visaprocessor"]) + "</td></tr>" +
                "<tr   bgcolor=#fff2eb height=25px><td>Employment Type</td><td>" + Convert.ToString(DrWebInfo["emptype"]) + "</td></tr>" +
                "<tr   height=25px><td>Division</td><td>" + Convert.ToString(DrWebInfo["Division"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>Police Clearence Required</td><td>" + Convert.ToString(DrWebInfo["PoliceClearenceReq"]) + "</td></tr>" +
                "<tr   height=25px><td>Police Clearence Completed</td><td>" + Convert.ToString(DrWebInfo["PoliceClearenceCom"]) + "</td></tr>" +
                //"<tr   bgcolor=#fff2eb height=25px><td>Production Stage</td><td>" + Convert.ToString(DrWebInfo["ProductionStage"]) + "</td></tr>" +
                "<tr  bgcolor=#fff2eb height=25px><td>Other Info</td><td>" + Convert.ToString(DrWebInfo["Notes"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }

    private string GetUOMEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("</tr></table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>UOM Name</b></font></td>" +
                       "<td><font color=#FFFFF><b>Code</b></font></td><td><font color=#FFFFF><b>UOM Type</b></font></td>" +
                       "<td><font color=#FFFFF><b>Scale</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[0].Rows)
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["UOMName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Code"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["UOMType"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Scale"]) + "</td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetPaymentTermsEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("</tr></table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Terms Name</b></font></td>" +
                       "<td><font color=#FFFFF><b>Description</b></font></td><td><font color=#FFFFF><b>PaymentMode</b></font></td>" +
                       "<td><font color=#FFFFF><b>MonthEnd</b></font></td><td><font color=#FFFFF><b>Cash on Delivery</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[0].Rows)
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["TermsName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Description"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["PaymentMode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["MonthEnd"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["CashonDelivery"]) + "</td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetNormalEmail()
    {
         GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td>Discount Name</td><td>" + Convert.ToString(drowEmail["DiscountShortName"]) + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td>Description</td><td>" + Convert.ToString(drowEmail["Description"]) + "</td></tr>" +
                     "<tr height=25px><td>Discount Mode</td><td>" + Convert.ToString(drowEmail["DiscountMode"]) + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td>Discount</td><td>" + Convert.ToString(drowEmail["DiscountForAmountDetails"]) + "</td></tr>" +
                     "<tr height=25px><td>Discount Type</td><td>" + Convert.ToString(drowEmail["IsSaleType"]) + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td>Value</td><td>" + Convert.ToString(drowEmail["value"]) + "</td></tr>" +
                     "<tr height=25px><td>Mode</td><td>" + Convert.ToString(drowEmail["PercentOrAmount"]) + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td>Period Applicable</td><td>" + Convert.ToString(drowEmail["PeriodApplicable"]) + "</td></tr>" +
                     "<tr height=25px><td>Period From</td><td>" + ((Convert.ToBoolean(drowEmail["PeriodApplicable"].ToString()) == true) ? Convert.ToString(drowEmail["PeriodFrom"]) : "") + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td>Period To</td><td>" + ((Convert.ToBoolean(drowEmail["PeriodApplicable"].ToString()) == true) ? Convert.ToString(drowEmail["PeriodTo"]) : "") + "</td></tr>" +
                     "<tr height=25px><td>Slab Applicable</td><td>" + Convert.ToString(drowEmail["SlabApplicable"]) + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td>Applicable For Total Amount</td><td>" + Convert.ToString(drowEmail["DiscountForAmount"]) + "</td></tr>" +
                     "<tr height=25px><td>Minimum Amount</td><td>" + Convert.ToString(drowEmail["MinSlab"]) + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td>Currency</td><td>" + Convert.ToString(drowEmail["Currency"]) + "</td></tr>" +
                      "<tr height=25px><td>Active</td><td>" + Convert.ToString(drowEmail["Active"]) + "</td></tr>"
                             );
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }
    private string GetLeaveStructureEmail()
    {
        decimal decBalance = 0;
        GetWebformHeaderInfo();
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];


            MsbHtml.Append("<tr height=25px><td width=30%> Employee Name </td><td width=70%>" + Convert.ToString(DrWebInfo["Name"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Employee Number</td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeNumber"]) + "</td></tr>" +
            "<tr height=25px><td width=30%>Financial Year</td><td width=70%>" + Convert.ToString(DrWebInfo["FinYear"]) + "</td></tr>");

            strColor = "#ffffff";

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        if (EmailSource.Tables[1].Rows.Count > 0)
        {

            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Leave Structure Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Leave Type</b></font></td>" +
                           "<td><font color=#FFFFF><b>Days</b></font></td><td><font color=#FFFFF><b>Carry Forward</b></font></td></font>" +
                           "<td><font color=#FFFFF><b>Taken Leaves</b></font></td><td><font color=#FFFFF><b>Extended Leaves</b></font></td></font>" +
                           "<td><font color=#FFFFF><b>Balance</b></font></td><td><font color=#FFFFF><b>Month Leave</b></font></td></font>" +
                           "<td><font color=#FFFFF><b>Encash Leaves</b></font></td></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
            {
                decBalance = 0;
                decBalance = drowEmail["BalanceLeave"].ToDecimal() + drowEmail["CarryForwardDays"].ToDecimal() + drowEmail["ExtendedLeaves"].ToDecimal() - drowEmail["TakenLeaves"].ToDecimal();
                if (decBalance < 0) decBalance = 0;
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["LeaveType"]) + "</td>" +
                              "<td style='text-align:center'>" + drowEmail["BalanceLeave"].ToDecimal().ToString("0.0") + "</td> " +
                               "<td style='text-align:center'>" + drowEmail["CarryForwardDays"].ToDecimal().ToString("0.0") + "</td> " +
                               "<td style='text-align:center'>" + drowEmail["TakenLeaves"].ToDecimal().ToString("0.0") + "</td> " +
                               "<td style='text-align:center'>" + drowEmail["ExtendedLeaves"].ToDecimal().ToString("0.0") + "</td>" +
                               "<td style='text-align:center'>" + decBalance.ToString("0.0") + "</td>" +
                               "<td style='text-align:center'>" + drowEmail["MonthLeaves"].ToDecimal().ToString("0.0") + "</td>" +
                               "<td style='text-align:center'>" + drowEmail["EncashLeaves"].ToDecimal().ToString("0.0") + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }
        else
        {
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Leave Structure Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetItemWiseEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            // Master data
            MsbHtml.Append("<tr height=25px><td>Discount Name</td><td>" + Convert.ToString(drowEmail["DiscountShortName"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Description</td><td>" + Convert.ToString(drowEmail["DiscountName"]) + "</td></tr>" +
                           "<tr height=25px><td>Discount Mode</td><td>" + Convert.ToString(drowEmail["DiscountMode"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Discount</td><td>" + Convert.ToString(drowEmail["Discount"]) + "</td></tr>" +
                           "<tr height=25px><td>Discount Type</td><td>" + Convert.ToString(drowEmail["IsSaleType"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>Period Applicable</td><td>" + Convert.ToString(drowEmail["IsPeriodApplicable"]) + "</td></tr>" +
                             "<tr height=25px><td>Period From</td><td>" + ((Convert.ToBoolean(drowEmail["IsPeriodApplicable"].ToString()) == true) ? Convert.ToString(drowEmail["PeriodFrom"]) : "") + "</td></tr>" +
                             "<tr bgcolor=#fff2eb height=25px><td>Period To</td><td>" + ((Convert.ToBoolean(drowEmail["IsPeriodApplicable"].ToString()) == true) ? Convert.ToString(drowEmail["PeriodTo"]) : "") + "</td></tr>" +
                             "<tr height=25px><td>Currency</td><td>" + Convert.ToString(drowEmail["Currency"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Active</td><td>" + Convert.ToString(drowEmail["IsActive"]) + "</td></tr>"
                           );
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";
        MsbHtml.Append("<tr height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Item Wise Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item Code</b></font></td>" +
                       "<td><font color=#FFFFF><b>Item Name</b></font></td><td><font color=#FFFFF><b>Mode</b></font></td>" +
                       "<td><font color=#FFFFF><b>Value</b></font></td>" +
                       "<td><font color=#FFFFF><b>Slab Applicable</b></font></td><td><font color=#FFFFF><b>Minimum Amount</b></font></td>" +
                       "<td><font color=#FFFFF><b>UOM</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Mode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Value"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["IsSlabApplicable"]) + "</td>" +
                          "<td>" + ((Convert.ToBoolean(drowEmail["IsSlabApplicable"].ToString()) == true) ? Convert.ToString(drowEmail["MinSlab"]) : "NA") + "</td>" +
                           "<td>" + ((Convert.ToBoolean(drowEmail["IsSlabApplicable"].ToString()) == true) ? Convert.ToString(drowEmail["UOM"]) : "NA") + "</td>" + "</td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }
        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();

    }
    private string GetLeaveEntry()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Leave Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];

            // Master data

            MsbHtml.Append("<tr height=25px><td>Employee Name</td><td>" + Convert.ToString(drowEmail["employeefullname"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>LeaveType</td><td>" + Convert.ToString(drowEmail["LeaveType"]) + "</td></tr>"
                          );
            if (Convert.ToString(drowEmail["HalfDay"]) == "Yes")
            {
                MsbHtml.Append("<tr height=25px><td>Date</td><td>" + Convert.ToString(drowEmail["ldtfrom"]) + "</td></tr>");
            }
            else
            {
                MsbHtml.Append("<tr height=25px><td>Date from</td><td>" + Convert.ToString(drowEmail["ldtfrom"]) + "</td></tr>" +
                                    "<tr bgcolor=#fff2eb height=25px><td>Date to</td><td>" + Convert.ToString(drowEmail["ldtTo"]) + "</td></tr>");
            }
            //"<tr bgcolor=#fff2eb height=25px><td>RejoinDate</td><td>" + Convert.ToString(drowEmail["ldtrejoin"]) + "</td></tr>"+
            MsbHtml.Append("<tr height=25px><td>HalfDay</td><td>" + Convert.ToString(drowEmail["HalfDay"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td>LeaveStatus</td><td>" + Convert.ToString(drowEmail["LeaveStatus"]) + "</td></tr>"

    );
            if (drowEmail["Remarks"].ToStringCustom().Trim() != "")
                MsbHtml.Append("<tr height=25px><td>Remarks</td><td>" + Convert.ToString(drowEmail["Remarks"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }
    private string GetCustomerwiseEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            // Master data
            MsbHtml.Append("<tr height=25px><td>Discount Name</td><td>" + Convert.ToString(drowEmail["DiscountShortName"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Description</td><td>" + Convert.ToString(drowEmail["DiscountName"]) + "</td></tr>" +
                           "<tr height=25px><td>Discount Mode</td><td>" + Convert.ToString(drowEmail["DiscountMode"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Discount</td><td>" + Convert.ToString(drowEmail["Discount"]) + "</td></tr>" +
                           "<tr height=25px><td>Discount Type</td><td>" + Convert.ToString(drowEmail["IsSaleType"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>Period Applicable</td><td>" + Convert.ToString(drowEmail["IsPeriodApplicable"]) + "</td></tr>" +
                             "<tr height=25px><td>Period From</td><td>" + ((Convert.ToBoolean(drowEmail["IsPeriodApplicable"].ToString()) == true) ? Convert.ToString(drowEmail["PeriodFrom"]) : "") + "</td></tr>" +
                             "<tr bgcolor=#fff2eb height=25px><td>Period To</td><td>" + ((Convert.ToBoolean(drowEmail["IsPeriodApplicable"].ToString()) == true) ? Convert.ToString(drowEmail["PeriodTo"]) : "") + "</td></tr>" +
                              "<tr height=25px><td>Currency</td><td>" + Convert.ToString(drowEmail["Currency"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Active</td><td>" + Convert.ToString(drowEmail["IsActive"]) + "</td></tr>"
                           );
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";

        MsbHtml.Append("<tr height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Customer Wise Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Vendor Code</b></font></td>" +
                       "<td><font color=#FFFFF><b>Vendor Name</b></font></td><td><font color=#FFFFF><b>Mode</b></font></td>" +
                       "<td><font color=#FFFFF><b>Value</b></font></td>" +
                       "<td><font color=#FFFFF><b>Slab Applicable</b></font></td><td><font color=#FFFFF><b>Minimum Amount</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["VendorCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["VendorName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Mode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Value"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["IsSlabApplicable"]) + "</td>" +
                           "<td>" + ((Convert.ToBoolean(drowEmail["IsSlabApplicable"].ToString()) == true) ? Convert.ToString(drowEmail["MinSlab"]) : "NA") + "</td>" + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }
        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetExpenseEmail()
    {
        GetWebformHeaderInfo();
        
        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        // Expense Master details
        if (EmailSource.Tables[0].Rows.Count > 0)
        {         
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            Type = Convert.ToString(drowEmail["OperationType"]);                  
           
            // Master data
            MsbHtml.Append("<tr height=25px><td>Company</td><td>" + Convert.ToString(drowEmail["CompanyName"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Operation Type</td><td>" + Convert.ToString(drowEmail["OperationType"]) + "</td></tr>" +
                           "<tr height=25px><td> "+ Type +" </td><td>"+ Convert.ToString(drowEmail["ReferenceNumber"]) + "</td></tr>" +
                          // "<tr bgcolor=#fff2eb height=25px><td>Post To Account</td><td>" + Convert.ToString(drowEmail["IsDirectExpense"]) + "</td></tr>" +                          
                            "<tr bgcolor=#fff2eb height=25px><td>Remarks</td><td>" + Convert.ToString(drowEmail["Remarks"]) + "</td></tr>" +
                            "<tr height=25px><td>Total Amount</td><td>" + Convert.ToString(drowEmail["TotalAmount"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>Amount in Words</td><td>" + Convert.ToString(drowEmail["AmtInWords"]) + "</td></tr>" 
                           );
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        // Expense Details

        strColor = "#ffffff";
        if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        MsbHtml.Append("<tr  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Expense Type</b></font></td>" +
                       "<td><font color=#FFFFF><b>Quantity</b></font></td><td><font color=#FFFFF><b>Amount</b></font></td>" +
                       "<td><font color=#FFFFF><b>Total</b></font></td><td><font color=#FFFFF><b>Remarks</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ExpenseType"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Quantity"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Amount"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Total"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Remarks"]) + "</td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }
        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        //if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        //if (EmailSource.Tables[0].Rows.Count > 0)
        //{
        //    DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
        //    //MsbHtml.Append("<tr><td>&nbsp;</td></tr><tr bgcolor=" + strColor + " height=25px><td colspan=2>&nbsp;</td><td colspan=3><b>Total</b>&nbsp;</td><td colspan=2><b>" + Convert.ToString(DrWebInfo["TotalAmount"]) + "</b></td></tr>");
        //    MsbHtml.Append("<tr><td>&nbsp;</td></tr><tr bgcolor=" + strColor + " height=25px><td colspan=2><b>Total</b>&nbsp;</td><td colspan=3><b>" + Convert.ToString(DrWebInfo["TotalAmount"]) + "</b></td></tr>");

        //}
        //MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }

    private string GetPaymentsEmail(bool blnIsReceipt)
    {
        string strPaymentNo = "Payment No", strPaymentDate = "Payment Date", strSupplier = "Supplier Name";
        if (blnIsReceipt)
        {
            strPaymentNo = "Receipt No"; strPaymentDate = "Receipt Date"; strSupplier = "Customer Name";
        }

        GetWebformHeaderInfo();
        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        // Payment Master details        
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            MsbHtml.Append("<tr height=25px><td>" + strPaymentNo + "</td><td>" + Convert.ToString(drowEmail["PaymentNumber"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>" + strPaymentDate + "</td><td>" + Convert.ToString(drowEmail["PaymentDate"]) + "</td></tr>" +
                           "<tr height=25px><td>Operation Type</td><td>" + Convert.ToString(drowEmail["OperationType"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Payment Type</td><td>" + Convert.ToString(drowEmail["PaymentType"]) + "</td></tr>" +
                           "<tr height=25px><td>" + strSupplier + "</td><td>" + Convert.ToString(drowEmail["VendorName"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Address</td><td>" + Convert.ToString(drowEmail["VendorAddress"]) + "</td></tr>"
                           );
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        // Payment Details
        MsbHtml.Append("<tr height=25px><td colspan=" + EmailSource.Tables[0].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Reference No</b></font></td>" +
                       "<td><font color=#FFFFF><b>Amount</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr height=25px><td>" + Convert.ToString(drowEmail["ReferenceNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Amount"]) + "</td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td>Payment Mode</td><td>" + Convert.ToString(drowEmail["TransactionType"]) + "</td></tr>" +
                           "<tr height=25px><td>Bank Account</td><td>" + Convert.ToString(drowEmail["BankAccount"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Ch. No.</td><td>" + Convert.ToString(drowEmail["ChequeNo"]) + "</td></tr>" +
                           "<tr height=25px><td>Ch. Date</td><td>" + Convert.ToString(drowEmail["ChequeDate"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Total Amount</td><td>" + Convert.ToString(drowEmail["TotalAmount"]) + "</td></tr>" +
                           "<tr height=25px><td>Amount In Words</td><td>" + Convert.ToString(drowEmail["AmtInWords"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Remarks</td><td>" + Convert.ToString(drowEmail["Remarks"]) + "</td></tr>"
                           );
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        return MsbHtml.ToString();
    }

    private string GetGeneralreceiptsAndPaymentsEmail()
    {
        decimal DebitTot = 0;
        decimal CreditTot = 0;
        GetWebformHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        // Voucher Master data
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            string strChDate = "";
            if (drowEmail["ChNo"].ToString() != "")
                strChDate = drowEmail["ChDate"].ToString();

            MsbHtml.Append("<tr height=25px><td>Company</td><td>" + EmailSource.Tables[2].Rows[0]["CompanyName"].ToString() + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Voucher No</td><td>" + drowEmail["VoucherNo"].ToString() + "</td></tr>" +
                           "<tr height=25px><td>Voucher Type</td><td>" + drowEmail["VoucherType"].ToString() + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Date</td><td>" + drowEmail["VoucherDate"].ToString() + "</td></tr>" +
                           "<tr height=25px><td>Remarks</td><td>" + drowEmail["Remarks"].ToString() + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Ch. No.</td><td>" + drowEmail["ChNo"].ToString() + "</td></tr>" +
                           "<tr height=25px><td>Ch. Date</td><td>" + strChDate.ToString() + "</td></tr>"
                           );
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        // Voucher Entry Details
        if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[0].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Voucher Entry Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Code</b></font></td><td><font color=#FFFFF><b>Account</b></font></td><td><font color=#FFFFF><b>Remarks</b></font></td>" +
                       "<td><font color=#FFFFF><b>Debit</b></font></td><td><font color=#FFFFF><b>Credit</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            DebitTot = DebitTot + drowEmail["DebitAmount"].ToDecimal();
            CreditTot = CreditTot + drowEmail["CreditAmount"].ToDecimal();

            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + drowEmail["Code"].ToString() + "</td><td>" + drowEmail["AccountName"].ToString() + "</td><td>" + drowEmail["Remarks"].ToString() + "</td>" +
                           "<td>" + drowEmail["DebitAmount"].ToString() + "</td>" +
                           "<td>" + drowEmail["CreditAmount"].ToString() + "</td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td><b>&nbsp</td><td><b>&nbsp</td><td><b>Total</td>" +
                         "<td><b>" + DebitTot + "</td>" +
                          "<td><b>" + CreditTot + "</td></tr>");
        return MsbHtml.ToString();
    }

    private string GetGroupJVEmail()
    {
        decimal DebitTot = 0;
        decimal CreditTot = 0;
        GetWebformHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        // Voucher Master data
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            MsbHtml.Append("<tr height=25px><td>Company</td><td>" + EmailSource.Tables[2].Rows[0]["CompanyName"].ToString() + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Voucher No</td><td>" + drowEmail["VoucherNo"].ToString() + "</td></tr>" +
                           "<tr height=25px><td>Voucher Type</td><td>" + drowEmail["VoucherType"].ToString() + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Date</td><td>" + drowEmail["VoucherDate"].ToString() + "</td></tr>" +
                           "<tr height=25px><td>Remarks</td><td>" + drowEmail["Remarks"].ToString() + "</td></tr>"
                           );
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        // Voucher Entry Details
        if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[0].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Voucher Entry Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Code</b></font></td><td><font color=#FFFFF><b>Account</b></font></td><td><font color=#FFFFF><b>Remarks</b></font></td>" +
                       "<td><font color=#FFFFF><b>Debit</b></font></td><td><font color=#FFFFF><b>Credit</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            DebitTot = DebitTot + drowEmail["DebitAmount"].ToDecimal();
            CreditTot = CreditTot + drowEmail["CreditAmount"].ToDecimal();

            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + drowEmail["Code"].ToString() + "</td><td>" + drowEmail["AccountName"].ToString() + "</td><td>" + drowEmail["Remarks"].ToString() + "</td>" +
                           "<td>" + drowEmail["DebitAmount"].ToString() + "</td>" +
                           "<td>" + drowEmail["CreditAmount"].ToString() + "</td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td><b>&nbsp</td><td><b>&nbsp</td><td><b>Total</td>" +
                         "<td><b>" + DebitTot + "</td>" +
                          "<td><b>" + CreditTot + "</td></tr>");
        return MsbHtml.ToString();
    }

    private string GetProductsEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        // Expense Master details
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            // Master data
            MsbHtml.Append("<tr height=25px><td>Item Code</td><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Item Name</td><td>" + Convert.ToString(drowEmail["ItemName"]) + "</td></tr>" +
                           "<tr height=25px><td>Short Name</td><td>" + Convert.ToString(drowEmail["ShortName"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Category</td><td>" + Convert.ToString(drowEmail["Category"]) + "</td></tr>" +
                           "<tr height=25px><td>SubCategory</td><td>" + Convert.ToString(drowEmail["SubCategory"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Base Uom</td><td>" + Convert.ToString(drowEmail["BaseUom"]) + "</td></tr>" +
                           "<tr height=25px><td>Status</td><td>" + Convert.ToString(drowEmail["ItemStatus"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Expiry Date Mandatory</td><td>" + Convert.ToString(drowEmail["ExpiryDateMandatory"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Product Info</h2></font></td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Model</td><td>" + Convert.ToString(drowEmail["Model"]) + "</td></tr>" +
                           "<tr height=25px><td>Manufacturer</td><td>" + Convert.ToString(drowEmail["Manufacturer"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>MadeIn</td><td>" + Convert.ToString(drowEmail["MadeIn"]) + "</td></tr>" +
                           "<tr height=25px><td>Warranty Period</td><td>" + Convert.ToString(drowEmail["WarrantyPeriod"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Guarantee Period</td><td>" + Convert.ToString(drowEmail["GuaranteePeriod"]) + "</td></tr>" +
                           "<tr height=25px><td>Reorder Point</td><td>" + Convert.ToString(drowEmail["DetailsReorderLevel"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Reorder Quantity</td><td>" + Convert.ToString(drowEmail["DetailsReorderQuantity"]) + "</td></tr>" +
                           "<tr height=25px><td>Notes</td><td>" + Convert.ToString(drowEmail["Notes"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Measurements</h2></font></td></tr>" +
                           "<tr height=25px><td>Width</td><td>" + Convert.ToString(drowEmail["Width"]) + "</td></tr>"+
                           "<tr bgcolor=#fff2eb height=25px><td>Height</td><td>" + Convert.ToString(drowEmail["Height"]) + "</td></tr>" +
                           "<tr height=25px><td>Length</td><td>" + Convert.ToString(drowEmail["Length"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Size UOM</td><td>" + Convert.ToString(drowEmail["SizeUOM"]) + "</td></tr>" +
                           "<tr height=25px><td>Weight</td><td>" + Convert.ToString(drowEmail["Weight"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Weight UOM</td><td>" + Convert.ToString(drowEmail["WeightUOM"]) + "</td></tr>" +
                           "<tr height=25px><td>Colour</td><td>" + Convert.ToString(drowEmail["Colour"]) + "</td></tr>"                          
                           );
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        if (EmailSource.Tables[1].Rows.Count > 0)
        {
            // Reorder Details
            strColor = "#fff2eb";
            MsbHtml.Append("<tr  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Location Information</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Warehouse</b></font></td>" +
                           "<td><font color=#FFFFF><b>Reorder Point</b></font></td><td><font color=#FFFFF><b>Reorder Quantity</b></font></td></tr>");

            foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["Warehouse"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["ReorderLevel"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["ReorderQuantity"]) + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        if (EmailSource.Tables[2].Rows.Count > 0)
        {
            // Measurement Details
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Units of Measurement</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>UOM Type</b></font></td>" +
                           "<td><font color=#FFFFF><b>UOM</b></font></td><td><font color=#FFFFF><b>Conversion Factor</b></font></td>" +
                           "<td><font color=#FFFFF><b>Conversion Value</b></font></td></tr>");

            strColor = "#fff2eb";
            foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemUOMType"]) + "</td>" +
                                 "<td>" + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                                 "<td></td><td></td>" +
                               "</tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        // Costing & Pricing Details
        MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Costing Method & Pricing Scheme Details</h2></font></td></tr>");
        strColor = "#fff2eb";
        MsbHtml.Append("<tr height=25px><td>Costing Method</td><td>" + Convert.ToString(EmailSource.Tables[0].Rows[0]["CostingMethod"].ToString()) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Pricing Scheme</td><td>" + Convert.ToString(EmailSource.Tables[0].Rows[0]["PricingScheme"].ToString()) + "</td></tr>");

      //  if (Convert.ToString(EmailSource.Tables[0].Rows[0]["CostingMethod"].ToString()) != "Batch")
       // {
            MsbHtml.Append("<tr height=25px><td>Purchase Rate</td><td>" + Convert.ToString(EmailSource.Tables[0].Rows[0]["PurchaseRate"].ToString()) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Sale Rate</td><td>" + Convert.ToString(EmailSource.Tables[0].Rows[0]["SaleRate"].ToString()) + "</td></tr>");            
    //    }
            if (Convert.ToString(EmailSource.Tables[0].Rows[0]["CostingMethod"].ToString()) == "Batch" & EmailSource.Tables[3].Rows.Count>0)
        {
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Batch No</b></font></td>" +
                       "<td><font color=#FFFFF><b>Expiry Date</b></font></td><td><font color=#FFFFF><b>Purchase Rate</b></font></td>" +
                       "<td><font color=#FFFFF><b>Sale Rate</b></font></td></tr>");
            foreach (DataRow drowEmail in EmailSource.Tables[3].Rows)
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["BatchNo"]) + "</td>" +
                             "<td>" + Convert.ToString(drowEmail["ExpiryDate"]) + "</td>" +
                             "<td>" + Convert.ToString(drowEmail["PurchaseRate"]) + "</td>" +
                             "<td>" + Convert.ToString(drowEmail["SaleRate"]) + "</td>" +
                           "</tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }
        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetInstallmentsEmail()
    {
        //GetWebformHeaderInfo();

        //// Heading
        //MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        //// Master details
        //if (EmailSource.Tables[0].Rows.Count > 0)
        //{
        //    DataRow drowEmail = EmailSource.Tables[0].Rows[0];
        //    // Master data
        //    MsbHtml.Append("<tr height=25px><td width=40%>Sales Order No</td><td>" + Convert.ToString(drowEmail["SalesOrderNo"]) + "</td></tr>" +
        //                   "<tr bgcolor=#fff2eb height=25px><td>Total Amount</td><td>" + Convert.ToString(drowEmail["TotalAmount"]) + "</td></tr>" +
        //                    "<tr height=25px><td>Order Amount</td><td>" + Convert.ToString(drowEmail["OrderAmount"]) + "</td></tr>" +
        //                    "<tr bgcolor=#fff2eb height=25px><td>Advance Paymentt</td><td>" + Convert.ToString(drowEmail["AdvancePayment"]) + "</td></tr>" +

        //                   "<tr height=25px><td>Status</td><td>" + Convert.ToString(drowEmail["Status"]) + "</td></tr>" +
        //                   "<tr bgcolor=#fff2eb height=25px><td>Description</td><td>" + Convert.ToString(drowEmail["Description"]) + "</td></tr>");
                         
        //    MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        //    int No = 0;
        //        for (int iCount = 0; iCount < EmailSource.Tables[1].Rows.Count; iCount++)
        //        {
        //            if ((EmailSource.Tables[1].Rows[iCount]["ChequeTypeID"].ToInt32() == 2) && (EmailSource.Tables[1].Rows[iCount]["StatusID"].ToInt32() != (int)InstallmentStatus.Cancelled))
        //                No = No + 1;

        //        }
        //        MsbHtml.Append("<tr height=25px><td width=40%>No of Installments</td><td>" + Convert.ToString(No) + "</td></tr>");
         
        //    MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");


        //    MsbHtml.Append("<tr bgcolor=#fff2eb  height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Customer Information</h2></font></td></tr>");
        //    MsbHtml.Append("<tr height=25px><td width=30%>customer</td><td>" + Convert.ToString(drowEmail["VendorName"]) + "</td></tr>" +
        //              "<tr  bgcolor=#fff2eb height=25px><td width=30%>Address</td><td>" + Convert.ToString(drowEmail["AddressName"]) + "</td></tr>" +
        //              "<tr   height=25px><td width=30%></td><td>" + Convert.ToString(drowEmail["Contactperson"]) + "</td></tr>" +
        //              "<tr  bgcolor=#fff2eb height=25px><td></td><td>" + Convert.ToString(drowEmail["Address"]) + " , " + Convert.ToString(drowEmail["State"]) + " , " + Convert.ToString(drowEmail["Country"]) + " , " + Convert.ToString(drowEmail["ZipCode"]) + "</td></tr>");

        //}
        //MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");      

        //// Installment Details
        //strColor = "#fff2eb";
        //MsbHtml.Append("<tr  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Cheque Details</h2></font></td></tr>");
        //MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Cheque Type</b></font></td>" +
        //               "<td><font color=#FFFFF><b>Cheque No</b></font></td><td><font color=#FFFFF><b>Cheque Date</b></font></td>" +
        //               "<td><font color=#FFFFF><b>Bank</b></font></td><td><font color=#FFFFF><b>Amount</b></font></td>" +
        //               "<td><font color=#FFFFF><b>Status</b></font></td></font></tr>");

        //foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        //{
        //    MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ChequeType"]) + "</td>" +
        //                   "<td>" + Convert.ToString(drowEmail["ChequeNo"]) + "</td>" +
        //                   "<td>" + Convert.ToString(drowEmail["ChequeDate"]) + "</td>" +
        //                   "<td>" + Convert.ToString(drowEmail["Bank"]) + "</td>" +
        //                   "<td>" + Convert.ToString(drowEmail["Amount"]) + "</td>" +
        //                   "<td>" + Convert.ToString(drowEmail["Status"]) + "</td></tr>");
        //    if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        //}
        //MsbHtml.Append("</table></td></tr></table></div></body></html>");
        //return MsbHtml.ToString();
        return "";
    }

    private string GetPurchaseEmail(int intFormType)
    {
        // 1 - PurchaseIndent
        // 2 - PurchaseQuotation
        // 3 - PurchaseOrder
        // 4 - GRN
        // 5 - PurchaseInvoice 
        // 6 - Debit Note
        GetWebformHeaderInfo();
        DataRow drowGeneralInfo = EmailSource.Tables[0].Rows[0];

        string strAddress = string.Empty;
        if (intFormType != 1)
        {
           strAddress =  drowGeneralInfo["Address"].ToString();
            if (!string.IsNullOrEmpty(drowGeneralInfo["State"].ToString()))
                strAddress += "," + drowGeneralInfo["State"].ToString();
            strAddress += "," + drowGeneralInfo["Country"].ToString();
            if (!string.IsNullOrEmpty(drowGeneralInfo["ZipCode"].ToString()))
                strAddress += ",ZipCode-" + drowGeneralInfo["ZipCode"].ToString();
        }
        switch (intFormType)
        {
            case 1:        // Heading
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
                // Purchase Indent Master details
                
                //foreach (DataRow drowEmail in EmailSource.Tables[0].Rows)
               // {
                    // Master data
                    MsbHtml.Append("<tr height=25px><td>Purchase Indent No</td><td>" + Convert.ToString(drowGeneralInfo["PurchaseIndentNo"]) + "</td></tr>" +
                                   "<tr bgcolor=#fff2eb height=25px><td>Indent Date</td><td>" + Convert.ToString(drowGeneralInfo["IndentDate"]) + "</td></tr>" +
                                   "<tr height=25px><td>Due Date</td><td>" + Convert.ToString(drowGeneralInfo["DueDate"]) + "</td></tr>" +
                                   "<tr bgcolor=#fff2eb height=25px><td>Department Name</td><td>" + Convert.ToString(drowGeneralInfo["DepartmentName"]) + "</td></tr>" +
                                   "<tr height=25px><td>Employee Name</td><td>" + Convert.ToString(drowGeneralInfo["EmployeeName"]) + "</td></tr>" +
                                   "<tr bgcolor=#fff2eb height=25px><td>Status</td><td>" + Convert.ToString(drowGeneralInfo["Status"]) + "</td></tr>" +
                                   "<tr height=25px><td>Cancelled by</td><td>" + Convert.ToString(drowGeneralInfo["Cancelledby"]) + "</td></tr>" +
                                   "<tr bgcolor=#fff2eb height=25px><td>Cancel Date</td><td>" + Convert.ToString(drowGeneralInfo["canceldate"]) + "</td></tr>" +
                                   "<tr height=25px><td>Cancel Description</td><td>" + Convert.ToString(drowGeneralInfo["CancelDescription"]) + "</td></tr>"
                                   );
                //}
                    MsbHtml.Append("<tr><td colspan=2 align=left><table border=0 cellspading=0 cellspacing=0 width=100%>");

                // Purchase Indent Details
                strColor = "#ffffff";
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Purchase Indent Details</h2></font></td></tr>");
                MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item Code</b></font></td>" +
                               "<td><font color=#FFFFF><b>Item Name</b></font></td><td><font color=#FFFFF><b>Quantity</b></font></td></tr>");
                              

                foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
                {
                    MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["Quantity"]) + Convert.ToString(drowEmail["UOM"])+ "</td></tr>");
                                
                    if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                }
                string sAltColor = "#ffffff";
                if (strColor == "#fff2eb") sAltColor = "#ffffff"; else sAltColor = "#fff2eb";
                MsbHtml.Append("</table></td></tr>");
                    MsbHtml.Append("<tr bgcolor="+strColor+" height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Remarks</h2></font></td></tr>" +
                                    "<tr bgcolor=" + sAltColor + " height=25px><td colspan=2 align=left>" + Convert.ToString(drowGeneralInfo["Remarks"]) + "</td></tr>"
                                   );
                MsbHtml.Append("</table></div></body></html>");
                return MsbHtml.ToString();
            case 2:
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
                // Purchase Quotation Master details
                drowGeneralInfo = EmailSource.Tables[0].Rows[0];
                // Master data
                MsbHtml.Append("<tr height=25px><td>Quotation No</td><td>" + Convert.ToString(drowGeneralInfo["PurchaseQuotationNo"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Quotation Date</td><td>" + Convert.ToString(drowGeneralInfo["QuotationDate"]) + "</td></tr>" +
                               "<tr height=25px><td>Due Date</td><td>" + Convert.ToString(drowGeneralInfo["DueDate"]) + "</td></tr>" +
                            //   "<tr bgcolor=#fff2eb height=25px><td>Quotation Type</td><td>" + Convert.ToString(drowGeneralInfo["Types"]) +// ((!string.IsNullOrEmpty(drowGeneralInfo["ReferenceNo"].ToString())) ? "-" + Convert.ToString(drowGeneralInfo["ReferenceNo"]) : "") + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Employee</td><td>" + Convert.ToString(drowGeneralInfo["EmployeeName"]) + "</td></tr>" +
                               "<tr  height=25px><td>Status</td><td>" + Convert.ToString(drowGeneralInfo["Status"]) + "</td></tr>" +
                              //"<tr height=25px><td>Inco Terms</td><td>" + Convert.ToString(drowGeneralInfo["IncoTerms"]) + "</td></tr>" +
                               //"<tr bgcolor=#fff2eb height=25px><td>Lead Time</td><td>" + Convert.ToString(drowGeneralInfo["LeadTime"]) + " Days" + "</td></tr>" +
                                //"<tr height=25px><td>Production Date</td><td>" + Convert.ToString(drowGeneralInfo["ProductionDate"]) + "</td></tr>" +
                                //"<tr bgcolor=#fff2eb height=25px><td>Transhipment Date</td><td>" + Convert.ToString(drowGeneralInfo["TranshipmentDate"]) + "</td></tr>" +
                              //"<tr height=25px><td>Clearence Date</td><td>" + Convert.ToString(drowGeneralInfo["ClearenceDate"]) + "</td></tr>" +
                               //"<tr bgcolor=#fff2eb height=25px><td>Spare Parts Percentage</td><td>" + Convert.ToString(drowGeneralInfo["SparePartsPercentage"]) + "</td></tr>" +
                                 //"<tr bgcolor=#fff2eb height=25px><td>Container Type</td><td>" + Convert.ToString(drowGeneralInfo["ContainerTypeDescription"]) + "</td></tr>" +
                                //"<tr height=25px><td>No of Containers</td><td>" + Convert.ToString(drowGeneralInfo["NoOfContainers"]) + "</td></tr>" +

                               // "<tr height=25px><td>Verification Criteria</td><td>" + Convert.ToString(drowGeneralInfo["VerificationCriteria"]) + "</td></tr>" +

                               "<tr bgcolor=#fff2eb height=25px><td>Cancelled by</td><td>" + Convert.ToString(drowGeneralInfo["Cancelledby"]) + "</td></tr>" +
                               "<tr  height=25px><td>Cancel Date</td><td>" + Convert.ToString(drowGeneralInfo["canceldate"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Cancel Description</td><td>" + Convert.ToString(drowGeneralInfo["CancelDescription"]) + "</td></tr>" +
                               "<tr  height=25px><td>Vendor</td><td>" + Convert.ToString(drowGeneralInfo["VendorName"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Vendor Address</td><td>" + Convert.ToString(drowGeneralInfo["AddressName"]) + "</td></tr>" +
                               "<tr height=25px><td>Contact Person</td><td>" + Convert.ToString(drowGeneralInfo["ContactPerson"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td></td><td>" + Convert.ToString(drowGeneralInfo["Address"]) + " , " + Convert.ToString(drowGeneralInfo["State"]) + " , " + Convert.ToString(drowGeneralInfo["Country"]) + " , " + Convert.ToString(drowGeneralInfo["ZipCode"]) + "</td></tr>");

                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

                // Purchase Quotation Details
                strColor = "#fff2eb";
                MsbHtml.Append("<tr height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Quotation Details</h2></font></td></tr>");
                MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item Code</b></font></td>" +
                               "<td><font color=#FFFFF><b>Item Name</b></font></td><td><font color=#FFFFF><b>Quantity</b></font></td>" +
                               "<td><font color=#FFFFF><b>Rate</b></font></td><td><font color=#FFFFF><b>DiscountAmount</b></font></td><td><font color=#FFFFF><b>Total</b></font></td></tr>");

                foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
                {
                    MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["Quantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["DiscountAmount"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["NetAmount"]) + "</td></tr>");
                    if (strColor == "#fff2eb") strColor = "#ffffff";
                    else strColor = "#fff2eb";
                }

                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
                if (EmailSource.Tables[2].Rows.Count > 0)
                {
                    strColor = "#ffffff";
                    //MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
                    //MsbHtml.Append("<tr height=25px><td width=20%><font color=#FFFFF><b>Expense Type</b></font></td>" +
                    //               "<td width=50%><font color=#FFFFF><b>Date</b></font></td>" +
                    //               "<td><font color=#FFFFF><b>Quantity</b></font></td>" +
                    //               "<td><font color=#FFFFF><b>Amount</b></font></td>" +
                    //               "<td><font color=#FFFFF><b>Total</b></font></td>" +
                    //               "<td ><font color=#FFFFF><b>Description</b></font></td></tr>");
                    MsbHtml.Append("<tr  height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
                    MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Expense Type</b></font></td>" +
                              "<td width=50%><font color=#FFFFF><b>Date</b></font></td>" +
                              "<td><font color=#FFFFF><b>Quantity</b></font></td>" +
                              "<td><font color=#FFFFF><b>Amount</b></font></td>" +
                               "<td><font color=#FFFFF><b>Total</b></font></td>" +
                              "<td ><font color=#FFFFF><b>Description</b></font></td></tr>");

                    foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
                    {
                        if (strColor == "#ffffff") strColor = "#fff2eb"; else strColor = "#ffffff";

                        MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ExpenseType"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["CreatedDate"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ExpenseQuantity"]) + "</td>" +
                                        "<td>" + Convert.ToString(drowEmail["ExpenseDetailAmount"]) + "</td>" +
                                         "<td>" + Convert.ToString(drowEmail["Total"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ExpenseDescription"]) + "</td></tr>");
                    }
                }
                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
                // For Documents

                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
                if (EmailSource.Tables[3].Rows.Count > 0)
                {
                    if (Convert.ToString(EmailSource.Tables[3].Rows[0]["DocumentNo"]) != "")
                    {
                        strColor = "#fff2eb";

                        MsbHtml.Append("<tr height=25px><td colspan=" + EmailSource.Tables[3].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Document Details</h2></font></td></tr>");
                        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Document No</b></font></td>" +
                                       "<td><font color=#FFFFF><b>Document Type</b></font></td><td><font color=#FFFFF><b>Expiry Date</b></font></td>" +
                                       "<td><font color=#FFFFF><b>Status</b></font></td></font></tr>");

                        foreach (DataRow drowEmail in EmailSource.Tables[3].Rows)
                        {
                            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["DocumentNo"]) + "</td>" +
                                           "<td>" + Convert.ToString(drowEmail["DocumentType"]) + "</td>" +
                                           "<td>" + Convert.ToString(drowEmail["ExpiryDate"]) + "</td>" +
                                           "<td>" + Convert.ToString(drowEmail["Status"]) + "</td></tr>");

                            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                        }
                    }
                }
                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

                sAltColor = "#fff2eb";
                if (strColor == "#ffffff") strColor = "#fff2eb"; else strColor = "#ffffff";
                if (strColor == "#fff2eb") sAltColor = "#ffffff"; else sAltColor = "   fff2eb";
                MsbHtml.Append( "<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Payment Details</h2></font></td></tr>");
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td>Currency</td><td>" + Convert.ToString(drowGeneralInfo["Currency"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td>Payment Terms</td><td>" + Convert.ToString(drowGeneralInfo["PaymentTerms"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td>Discount Type</td><td>" + Convert.ToString(drowGeneralInfo["DiscountType"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td>Grand Discount</td><td>" + Convert.ToString(drowGeneralInfo["DiscountAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td>Gross Amount</td><td>" + Convert.ToString(drowGeneralInfo["TotalAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td>Expense</td><td>" + Convert.ToString(drowGeneralInfo["ExpenseAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td><b>Total Amount</td><td><b>" + Convert.ToString(drowGeneralInfo["SubTotal"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td><b>Amount in Words</td><td><b>" + Convert.ToString(drowGeneralInfo["AmtInWords"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Remarks</h2></font></td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td colspan=2 align=left>" + Convert.ToString(drowGeneralInfo["Remarks"]) + "</td></tr>"
                               );

                MsbHtml.Append("</table></div></body></html>");
                return MsbHtml.ToString();
            case 3:
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
                // Purchase Order Master details
                drowGeneralInfo = EmailSource.Tables[0].Rows[0];
                intType = Convert.ToInt32(drowGeneralInfo["OrderTypeID"]);
                //if (intType == 4)
                //{
                //    Type = "Purchase Indent No";
                //}
                //else
                if (intType == (int)OperationOrderType.POrderFromQuotation)
                {
                    Type = "Purchase Quotation No";
                }
                else
                {
                    Type = "Reference No";
                }

                // Master data
                MsbHtml.Append("<tr height=25px><td>Purchase Order No</td><td>" + Convert.ToString(drowGeneralInfo["PurchaseOrderNo"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Order Date</td><td>" + Convert.ToString(drowGeneralInfo["OrderDate"]) + "</td></tr>" +
                               "<tr height=25px><td>DueDate</td><td>" + Convert.ToString(drowGeneralInfo["DueDate"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Order Type</td><td>" + Convert.ToString(drowGeneralInfo["OrderType"]) + "</td></tr>" +
                               "<tr height=25px><td>ReferenceNo</td><td>" + Convert.ToString(drowGeneralInfo["ReferenceNo"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>GRN Required</td><td>" + Convert.ToString(drowGeneralInfo["IsGRNRequired"]) + "</td></tr>" +
                               "<tr height=25px><td>EmployeeName</td><td>" + Convert.ToString(drowGeneralInfo["EmployeeName"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Status</td><td>" + Convert.ToString(drowGeneralInfo["Status"]) + "</td></tr>" +

                               //"<tr height=25px><td>Verification Criteria</td><td>" + Convert.ToString(drowGeneralInfo["VerificationCriteria"]) + "</td></tr>" +

                              //"<tr bgcolor=#fff2eb height=25px><td>Inco Terms</td><td>" + Convert.ToString(drowGeneralInfo["IncoTermsDescription"]) + "</td></tr>" +
                              //"<tr  height=25px><td>Lead Time</td><td>" + Convert.ToString(drowGeneralInfo["LeadTime"]) + " Days" + "</td></tr>" +
                              //"<tr bgcolor=#fff2eb height=25px><td>Production Date</td><td>" + Convert.ToString(drowGeneralInfo["ProductionDate"]) + "</td></tr>" +
                              //"<tr  height=25px><td>Transhipment Date</td><td>" + Convert.ToString(drowGeneralInfo["TranshipmentDate"]) + "</td></tr>" +
                              //"<tr bgcolor=#fff2eb height=25px><td>Clearence Date</td><td>" + Convert.ToString(drowGeneralInfo["ClearenceDate"]) + "</td></tr>" +
                              //"<tr  height=25px><td>Spare Parts Percentage</td><td>" + Convert.ToString(drowGeneralInfo["SparePartsPercentage"]) + "</td></tr>" +
                             //"<tr height=25px><td>Container Type</td><td>" + Convert.ToString(drowGeneralInfo["ContainerTypeDescription"]) + "</td></tr>" +
                            //"<tr bgcolor=#fff2eb height=25px><td>No of Containers</td><td>" + Convert.ToString(drowGeneralInfo["NoOfContainers"]) + "</td></tr>" +
                             "<tr height=25px><td>Cancelled by</td><td>" + Convert.ToString(drowGeneralInfo["Cancelledby"]) + "</td></tr>" +
                            "<tr  bgcolor=#fff2eb height=25px><td>Cancel Date</td><td>" + Convert.ToString(drowGeneralInfo["canceldate"]) + "</td></tr>" +
                            "<tr height=25px><td>Cancel Description</td><td>" + Convert.ToString(drowGeneralInfo["CancelDescription"]) + "</td></tr>" +
                            "<tr  bgcolor=#fff2eb height=25px><td>Vendor Name</td><td>" + Convert.ToString(drowGeneralInfo["VendorName"]) + "</td></tr>" +
                            "<tr height=25px><td>Vendor Address</td><td>" + Convert.ToString(drowGeneralInfo["AddressName"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td></td><td>" + Convert.ToString(drowGeneralInfo["ContactPerson"]) + "</td></tr>" +
                            "<tr height=25px><td></td><td>" + Convert.ToString(drowGeneralInfo["Address"]) + " , " + Convert.ToString(drowGeneralInfo["State"]) + " , " + Convert.ToString(drowGeneralInfo["Country"]) + " , " + Convert.ToString(drowGeneralInfo["ZipCode"]) + "</td></tr>");

                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

                // Purchase Order Details
                strColor = "#fff2eb";
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Purchase Order Details</h2></font></td></tr>");
                MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item Code</b></font></td>" +
                               "<td><font color=#FFFFF><b>Item Name</b></font></td><td><font color=#FFFFF><b>Quantity</b></font></td>" +
                               "<td><font color=#FFFFF><b>Rate</b></font></td><td><font color=#FFFFF><b>DiscountAmount</b></font></td><td><font color=#FFFFF><b>Total</b></font></td></tr>");

                foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
                {
                    MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["Quantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["DiscountAmount"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["NetAmount"]) + "</td></tr>");
                    if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                }
                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
                if (EmailSource.Tables[2].Rows.Count > 0)
                {
                    strColor = "#ffffff";
                    MsbHtml.Append("<tr  height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
                    MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Expense Type</b></font></td>" +
                                   "<td width=50%><font color=#FFFFF><b>Date</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Quantity</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Amount</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Total</b></font></td>" +
                                   "<td ><font color=#FFFFF><b>Description</b></font></td></tr>");

                    foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
                    {
                        if (strColor == "#ffffff") strColor = "#fff2eb"; else strColor = "#ffffff";

                        MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ExpenseType"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["CreatedDate"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ExpenseQuantity"]) + "</td>" +
                                        "<td>" + Convert.ToString(drowEmail["ExpenseDetailAmount"]) + "</td>" +
                                           "<td>" + Convert.ToString(drowEmail["Total"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ExpenseDescription"]) + "</td></tr>");
                    }
                }
                else
                {
                    //MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
                    //MsbHtml.Append("<tr bgcolor=#fff2eb  height=25px><td colspan=7 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

                }
                // For Documents

                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
                if (EmailSource.Tables[3].Rows.Count > 0)
                {
                    if (Convert.ToString(EmailSource.Tables[3].Rows[0]["DocumentNo"]) != "")
                    {
                        strColor = "#fff2eb";

                        MsbHtml.Append("<tr height=25px><td colspan=" + EmailSource.Tables[3].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Document Details</h2></font></td></tr>");
                        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Document No</b></font></td>" +
                                       "<td><font color=#FFFFF><b>Document Type</b></font></td><td><font color=#FFFFF><b>Expiry Date</b></font></td>" +
                                       "<td><font color=#FFFFF><b>Status</b></font></td></font></tr>");

                        foreach (DataRow drowEmail in EmailSource.Tables[3].Rows)
                        {
                            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["DocumentNo"]) + "</td>" +
                                           "<td>" + Convert.ToString(drowEmail["DocumentType"]) + "</td>" +
                                           "<td>" + Convert.ToString(drowEmail["ExpiryDate"]) + "</td>" +
                                           "<td>" + Convert.ToString(drowEmail["Status"]) + "</td></tr>");

                            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                        }
                    }
                }
                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

                sAltColor = "#ffffff";
                if (strColor == "#fff2eb") strColor = "#ffffff"; else sAltColor = "fff2eb";

                MsbHtml.Append("<tr  height=25px bgcolor=" + strColor + "><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Payment Details</h2></font></td></tr>");
                MsbHtml.Append("<tr height=25px bgcolor=" + sAltColor + "><td>Currency</td><td>" + Convert.ToString(drowGeneralInfo["Currency"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td>Payment Terms</td><td>" + Convert.ToString(drowGeneralInfo["PaymentTerms"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td>Extra Charges </td><td>" + Convert.ToString(drowGeneralInfo["ExtraCharges"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td>Advance Amount</td><td>" + Convert.ToString(drowGeneralInfo["AdvanceAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td>Discount Type</td><td>" + Convert.ToString(drowGeneralInfo["DiscountType"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td>Grand Discount</td><td>" + Convert.ToString(drowGeneralInfo["DiscountAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td>Gross Amount</td><td>" + Convert.ToString(drowGeneralInfo["SubTotal"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td>Expense Amount</td><td>" + Convert.ToString(drowGeneralInfo["ExpenseAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td><b>Total Amount</td><td><b>" + Convert.ToString(drowGeneralInfo["TotalAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td><b>Amount in Words</td><td><b>" + Convert.ToString(drowGeneralInfo["AmtInWords"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Remarks</h2></font></td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td colspan=2 align=left>" + Convert.ToString(drowGeneralInfo["Remarks"]) + "</td></tr>"
                               );

                MsbHtml.Append("</table></div></body></html>");
                return MsbHtml.ToString();
            case 4: 
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
                // GRN Master details
                drowGeneralInfo = EmailSource.Tables[0].Rows[0];
                intType = Convert.ToInt32(drowGeneralInfo["OrderTypeID"]);
                if (intType == (int)OperationOrderType.GRNFromInvoice)
                {
                    Type = "Purchase Invoice No";
                }
                else if (intType == (int)OperationOrderType.GRNFromTransfer)
                {
                    Type = "Stock Transfer No";
                }
                else
                {
                    Type = "Material Issue No";
                }
                // Master data
                MsbHtml.Append("<tr height=25px><td>GRN No</td><td>" + Convert.ToString(drowGeneralInfo["GRNNo"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>GRN Date</td><td>" + Convert.ToString(drowGeneralInfo["GRNDate"]) + "</td></tr>" +
                               "<tr height=25px><td>" + Type + "</td><td>" + Convert.ToString(drowGeneralInfo["ReferenceNo"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Warehouse</td><td>" + Convert.ToString(drowGeneralInfo["WarehouseName"]) + "</td></tr>" +
                               "<tr height=25px><td>EmployeeName</td><td>" + Convert.ToString(drowGeneralInfo["EmployeeName"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Status</td><td>" + Convert.ToString(drowGeneralInfo["Status"]) + "</td></tr>"+
                               "<tr height=25px><td>Cancelled by</td><td>" + Convert.ToString(drowGeneralInfo["Cancelledby"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Cancel Date</td><td>" + Convert.ToString(drowGeneralInfo["canceldate"]) + "</td></tr>" +
                               "<tr height=25px><td>Cancel Description</td><td>" + Convert.ToString(drowGeneralInfo["CancelDescription"]) + "</td></tr>" );
                             //  "<tr bgcolor=#fff2eb height=25px><td>Vendor Name</td><td>" + Convert.ToString(drowGeneralInfo["VendorName"]) + "</td></tr>" +
                              // "<tr height=25px><td>Vendor Address</td><td>"+ Convert.ToString(drowGeneralInfo["AddressName"]) + "</td></tr>" +
                             //  "<tr bgcolor=#fff2eb height=25px><td></td><td>" + Convert.ToString(drowGeneralInfo["ContactPerson"]) + "</td></tr>" +
                             //  "<tr  height=25px><td></td><td>" + Convert.ToString(drowGeneralInfo["Address"]) + " , " + Convert.ToString(drowGeneralInfo["State"]) + " , " + Convert.ToString(drowGeneralInfo["Country"]) + " , " + Convert.ToString(drowGeneralInfo["ZipCode"]) + "</td></tr>");

                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

                

                if (Convert.ToString(drowGeneralInfo["VendorName"]) != "")
                {
                  MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Vendor Info</h2></font></td></tr>");

                  MsbHtml.Append("<tr height=25px><td>Vendor Name</td><td>" + Convert.ToString(drowGeneralInfo["VendorName"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Vendor Address</td><td>" + Convert.ToString(drowGeneralInfo["AddressName"]) + "</td></tr>" +
                                 "<tr height=25px><td>Contact Person</td><td>" + Convert.ToString(drowGeneralInfo["ContactPerson"]) + "</td></tr>" +
                               "<tr  bgcolor=#fff2eb height=25px><td></td><td>" + Convert.ToString(drowGeneralInfo["Address"]) + " , " + Convert.ToString(drowGeneralInfo["State"]) + " , " + Convert.ToString(drowGeneralInfo["Country"]) + " , " + Convert.ToString(drowGeneralInfo["ZipCode"]) + "</td></tr>");
                  MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

                }
                // GRN Details
                strColor = "#ffffff";

                if (intType == (int)OperationOrderType.GRNFromTransfer)
                {
                    MsbHtml.Append("<tr height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Item Details</h2></font></td></tr>");
                    MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item Code</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Item Name</b></font></td> "+
                                    //"<td><font color=#FFFFF><b>Batch No</b></font></td> " +
                                   "<td><font color=#FFFFF><b> Received Qty </b></font></td> ");
                                   //"<td><font color=#FFFFF><b>Invoiced Qty</b></font></td><td><font color=#FFFFF><b>Extra Qty</b></font></td><td><font color=#FFFFF><b>Rate</b></font></td><td><font color=#FFFFF><b>Purchase Rate</b></font></td>");

                    foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
                    {
                        MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                                       //"<td>" + Convert.ToString(drowEmail["BatchNo"]) + "</td>" +
                            //"<td>" + Convert.ToString(drowEmail["ExpiryDate"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ReceivedQuantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>");
                                       //"<td>" + Convert.ToString(drowEmail["OrderQuantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                                       //"<td>" + Convert.ToString(drowEmail["ExtraQuantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                                       //"<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                                       //"<td>" + Convert.ToString(drowEmail["PurchaseRate"]) + "</td>");
                        if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                    }
                    MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
                }
              
                else
                {
                    MsbHtml.Append("<tr height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Item Details</h2></font></td></tr>");
                    MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item Code</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Item Name</b></font></td> "+
                                   //"<td><font color=#FFFFF><b>Batch No</b></font></td> " +
                                   //"<td><font color=#FFFFF><b>Expiry Date</b></font></td>"+
                                    "<td><font color=#FFFFF><b>Received Qty</b></font></td> " +
                                   "<td><font color=#FFFFF><b>Invoiced Qty</b></font></td><td><font color=#FFFFF><b>Extra Qty</b></font></td>");

                    foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
                    {
                        MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                                       //"<td>" + Convert.ToString(drowEmail["BatchNo"]) + "</td>" +
                                       //"<td>" + Convert.ToString(drowEmail["ExpiryDate"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ReceivedQuantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["OrderQuantity"])+"</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ExtraQuantity"]) + "</td>" );
                        if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                    }
                    MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
                }
                sAltColor = "#ffffff";
                if (strColor == "#fff2eb") sAltColor = "#ffffff"; else sAltColor = "#fff2eb";
                //MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Payment Details</h2></font></td></tr>");
                //MsbHtml.Append("<tr height=25px bgcolor=" + sAltColor + "><td>Payment Terms</td><td>" + Convert.ToString(drowGeneralInfo["PaymentTerms"]) + "</td></tr>" +
                //                "<tr height=25px bgcolor=" + strColor + "><td>Currency</td><td>" + Convert.ToString(drowGeneralInfo["Currency"]) + "</td></tr>" +
                //                "<tr height=25px bgcolor=" + sAltColor + "><td>Discount Type</td><td>" + Convert.ToString(drowGeneralInfo["DiscountType"]) + "</td></tr>" +
                //                "<tr height=25px bgcolor=" + strColor + "><td>Grand Discount</td><td>" + Convert.ToString(drowGeneralInfo["DiscountAmount"]) + "</td></tr>" +
                //              "<tr height=25px bgcolor=" + sAltColor + "><td>Gross Amount</td><td>" + Convert.ToString(drowGeneralInfo["SubTotal"]) + "</td></tr>" +
                //                "<tr height=25px bgcolor=" + strColor + "><td>Total Amount</td><td>" + Convert.ToString(drowGeneralInfo["TotalAmount"]) + "</td></tr>" +
                //                "<tr height=25px bgcolor=" + sAltColor + "><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Remarks</h2></font></td></tr>" +
                //                "<tr height=25px bgcolor=" + strColor + "><td colspan=2 align=left>" + Convert.ToString(drowGeneralInfo["Remarks"]) + "</td></tr>"
                //               );

                MsbHtml.Append("</table></div></body></html>");
                return MsbHtml.ToString();
            case 5:
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
                // Purchase Invoice Master details
                drowGeneralInfo = EmailSource.Tables[0].Rows[0];
                intType = Convert.ToInt32(drowGeneralInfo["InvoiceTypeID"]);
                if (intType == (int)OperationOrderType.PInvoiceFromOrder)
                {
                    Type = "Purchase Order No";
                }
                else
                {
                    Type = "Reference No";
                }
                   
                // Master data
                MsbHtml.Append("<tr height=25px><td>Purchase Invoice No</td><td>" + Convert.ToString(drowGeneralInfo["PurchaseInvoiceNo"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Invoice Date</td><td>" + Convert.ToString(drowGeneralInfo["InvoiceDate"]) + "</td></tr>" +
                               "<tr height=25px><td>Supplier Bill No</td><td>" + Convert.ToString(drowGeneralInfo["PurchaseBillNo"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Invoice Type</td><td>" + Convert.ToString(drowGeneralInfo["InvoiceType"]) + "</td></tr>" +
                               "<tr height=25px><td>"+ Type +"</td><td>" + Convert.ToString(drowGeneralInfo["ReferenceNo"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Warehouse</td><td>" + Convert.ToString(drowGeneralInfo["WarehouseName"]) + "</td></tr>" +
                               "<tr height=25px><td>DueDate</td><td>" + Convert.ToString(drowGeneralInfo["DueDate"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>EmployeeName</td><td>" + Convert.ToString(drowGeneralInfo["EmployeeName"]) + "</td></tr>" +
                               "<tr height=25px><td>Status</td><td>" + Convert.ToString(drowGeneralInfo["Status"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Cancelled by</td><td>" + Convert.ToString(drowGeneralInfo["Cancelledby"]) + "</td></tr>" +
                               "<tr height=25px><td>Cancel Date</td><td>" + Convert.ToString(drowGeneralInfo["canceldate"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Cancel Description</td><td>" + Convert.ToString(drowGeneralInfo["CancelDescription"]) + "</td></tr>" +
                               "<tr height=25px><td>Vendor Name</td><td>" + Convert.ToString(drowGeneralInfo["VendorName"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Vendor Address</td><td>" + Convert.ToString(drowGeneralInfo["AddressName"]) + "</td></tr>" +
                               "<tr height=25px><td></td><td>" + Convert.ToString(drowGeneralInfo["ContactPerson"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td></td><td>" + Convert.ToString(drowGeneralInfo["Address"]) + " , " + Convert.ToString(drowGeneralInfo["State"]) + " , " + Convert.ToString(drowGeneralInfo["Country"]) + " , " + Convert.ToString(drowGeneralInfo["ZipCode"]) + "</td></tr>");


                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

                // Purchase Invoice Details
                strColor = "#fff2eb";
                MsbHtml.Append("<tr height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Purchase Invoice Details</h2></font></td></tr>");

                if (intType == (int)OperationOrderType.PInvoiceFromOrder)
                {

                    MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item Code</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Item Name</b></font></td>"+
                                   //<td><font color=#FFFFF><b>Batch No</b></font></td><td><font color=#FFFFF><b>ExpiryDate</b></font></td>
                    "<td><font color=#FFFFF><b>Invoiced Qty</b></font></td><td><font color=#FFFFF><b>Ordered Qty</b></font></td><td><font color=#FFFFF><b>Extra Qty</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Purchase Rate</b></font></td><td><font color=#FFFFF><b>DiscountAmount</b></font></td><td><font color=#FFFFF><b>Total</b></font></td></tr>");

                    foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
                    {
                        MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                                       //"<td>" + Convert.ToString(drowEmail["BatchNo"]) + "</td>" +
                                       //"<td>" + Convert.ToString(drowEmail["ExpiryDate"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ReceivedQuantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["OrderedQuantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ExtraQuantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["DiscountAmount"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["NetAmount"]) + "</td></tr>");
                        if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                    }
                    MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
                }
                else
                {

                    MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item Code</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Item Name</b></font></td><td><font color=#FFFFF><b>Quantity</b></font></td><td><font color=#FFFFF><b>ExtraQuantity</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Purchase Rate</b></font></td><td><font color=#FFFFF><b>DiscountAmount</b></font></td><td><font color=#FFFFF><b>Total</b></font></td></tr>");

                    foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
                    {
                        MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                                       //"<td>" + Convert.ToString(drowEmail["BatchNo"]) + "</td>" +
                                       //"<td>" + Convert.ToString(drowEmail["ExpiryDate"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ReceivedQuantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ExtraQuantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["DiscountAmount"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["NetAmount"]) + "</td></tr>");
                        if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                    }
                    MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
               
                }
                if (EmailSource.Tables[2].Rows.Count > 0)
                {                  

                    strColor = "#ffffff";
                    MsbHtml.Append("<tr  height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
                    MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=30%><font color=#FFFFF><b>Expense Catagory</b></font></td>" +

                                   "<td width=20% ><font color=#FFFFF><b>Expense Type</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Date</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Quantity</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Amount</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Total</b></font></td>" +
                                   "<td ><font color=#FFFFF><b>Description</b></font></td></tr>");

                    foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
                    {
                        if (strColor == "#ffffff") strColor = "#fff2eb"; else strColor = "#ffffff";

                        MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ExpenseCatagory"]) + "</td>" +
                             "<td>" + Convert.ToString(drowEmail["ExpenseType"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["CreatedDate"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ExpenseQuantity"]) + "</td>" +
                                        "<td>" + Convert.ToString(drowEmail["ExpenseDetailAmount"]) + "</td>" +
                                         "<td>" + Convert.ToString(drowEmail["Total"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ExpenseDescription"]) + "</td></tr>");
                    }
                }
                else
                {
                    //MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
                    //MsbHtml.Append("<tr bgcolor=#fff2eb  height=25px><td colspan=7 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");
                }
                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
                sAltColor = "#ffffff";
                if (strColor == "#fff2eb") sAltColor = "#ffffff"; else sAltColor = "fff2eb";
                MsbHtml.Append("<tr bgcolor=" + sAltColor + " height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Payment Details</h2></font></td></tr>");
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td>Currency</td><td>" + Convert.ToString(drowGeneralInfo["Currency"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td>Payment Terms</td><td>" + Convert.ToString(drowGeneralInfo["PaymentTerms"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td>Extra Charges</td><td>" + Convert.ToString(drowGeneralInfo["ExtraCharges"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td>Discount Type</td><td>" + Convert.ToString(drowGeneralInfo["DiscountType"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td>Advance Amount</td><td>" + Convert.ToString(drowGeneralInfo["AdvanceAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td>Grand Discount</td><td>" + Convert.ToString(drowGeneralInfo["DiscountAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td>Gross Amount</td><td>" + Convert.ToString(drowGeneralInfo["SubTotal"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td>Expense Amount</td><td>" + Convert.ToString(drowGeneralInfo["ExpenseAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td><b>Total Amount</td><td><b>" + Convert.ToString(drowGeneralInfo["TotalAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td><b>Amount in Words</td><td><b>" + Convert.ToString(drowGeneralInfo["AmtInWords"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Remarks</h2></font></td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td colspan=2 align=left>" + Convert.ToString(drowGeneralInfo["Remarks"]) + "</td></tr>"
                               );

                MsbHtml.Append("</table></div></body></html>");
                return MsbHtml.ToString();
            case 6 :
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
                //Debit Note  Master details
                drowGeneralInfo = EmailSource.Tables[0].Rows[0];
                  
                // Master data
                MsbHtml.Append("<tr height=25px><td>Purchase Return No</td><td>" + Convert.ToString(drowGeneralInfo["PurchaseReturnNo"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Purchase Invoice No</td><td>" + Convert.ToString(drowGeneralInfo["PurchaseInvoiceNo"]) + "</td></tr>" +
                               "<tr height=25px><td>ReturnDate</td><td>" + Convert.ToString(drowGeneralInfo["ReturnDate"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Status</td><td>" + Convert.ToString(drowGeneralInfo["Status"]) + "</td></tr>" +
                               "<tr height=25px><td>Employee Name</td><td>" + Convert.ToString(drowGeneralInfo["EmployeeName"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Currency</td><td>" + Convert.ToString(drowGeneralInfo["Currency"]) + "</td></tr>" +
                               "<tr height=25px><td>Cancelled by</td><td>" + Convert.ToString(drowGeneralInfo["Cancelledby"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Cancel Date</td><td>" + Convert.ToString(drowGeneralInfo["canceldate"]) + "</td></tr>" +
                               "<tr height=25px><td>Cancel Description</td><td>" + Convert.ToString(drowGeneralInfo["CancelDescription"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Vendor Name</td><td>" + Convert.ToString(drowGeneralInfo["VendorName"]) + "</td></tr>" +
                               "<tr height=25px><td>Vendor Address</td><td>"+ Convert.ToString(drowGeneralInfo["AddressName"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td></td><td>" + Convert.ToString(drowGeneralInfo["ContactPerson"]) + "</td></tr>" +
                               "<tr height=25px><td>Contact Person</td><td>" + Convert.ToString(drowGeneralInfo["Address"]) + " , " + Convert.ToString(drowGeneralInfo["State"]) + " , " + Convert.ToString(drowGeneralInfo["Country"]) + " , " + Convert.ToString(drowGeneralInfo["ZipCode"]) + "</td></tr>" +
                               "<tr bgcolor=#fff2eb height=25px><td>Remarks</td><td>" + Convert.ToString(drowGeneralInfo["Remarks"]) + "</td></tr>");

                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

                // Debit Note Details
                strColor = "#fff2eb";
                MsbHtml.Append("<tr height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Debit Note Item Details</h2></font></td></tr>");
                MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item Code</b></font></td>" +
                               "<td><font color=#FFFFF><b>Item Name</b></font></td>" +
                               "<td><font color=#FFFFF><b>Invoiced Quantity</b></font></td> " +
                               "<td><font color=#FFFFF><b>Received Quantity</b></font></td>" +
                               "<td><font color=#FFFFF><b>Return Quantity</b></font></td>" +
                               "<td><font color=#FFFFF><b>Rate</b></font></td>" +
                               "<td><font color=#FFFFF><b>Grand Amount</b></font></td>" +
                               "<td><font color=#FFFFF><b>Discount Amount</b></font></td>" +
                               "<td><font color=#FFFFF><b>Net Amount</b></font></td>" +
                               "<td><font color=#FFFFF><b>Return Amount</b></font></td>" +
                               "<td><font color=#FFFFF><b>Reason</b></font></td></tr>");

                foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
                {
                    MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["InvoicedQuantity"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["ReceivedQuantity"])  + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["ReturnQuantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["Rate"]) +  "</td>" +                                  
                                   "<td>" + Convert.ToString(drowEmail["GrandAmount"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["DiscountAmount"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["NetAmount"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["ReturnAmount"]) + "</td>" +
                                   "<td>" + Convert.ToString(drowEmail["Reason"]) + "</td></tr>");
                    if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                }
                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
                sAltColor = "#ffffff";
                if (strColor == "#fff2eb") sAltColor = "#ffffff"; else sAltColor = "fff2eb";
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Payment Details</h2></font></td></tr>");
                MsbHtml.Append("<tr height=25px bgcolor=" + sAltColor + "><td>Currency</td><td>" + Convert.ToString(drowGeneralInfo["Currency"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td>Discount Type</td><td>" + Convert.ToString(drowGeneralInfo["DiscountType"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td>Grand Discount</td><td>" + Convert.ToString(drowGeneralInfo["GrandDiscountAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td>Gross Amount</td><td>" + Convert.ToString(drowGeneralInfo["GrandAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td><b>Net Amount</td><td><b>" + Convert.ToString(drowGeneralInfo["NetAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + sAltColor + "><td><b>Return Amount</td><td><b>" + Convert.ToString(drowGeneralInfo["GrandReturnAmount"]) + "</td></tr>" +
                                "<tr height=25px bgcolor=" + strColor + "><td><b>Amount in Words</td><td><b>" + Convert.ToString(drowGeneralInfo["AmtInWords"]) + "</td></tr>"
                               );

                MsbHtml.Append("</table></div></body></html>");
                return MsbHtml.ToString();
            default :
                return null;
        }
    }
    private string GetSalesQuotationEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%> Sales Quotation No </td><td width=70%>" + Convert.ToString(DrWebInfo["SalesQuotationNo"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Quotation Date</td><td width=70%>" + Convert.ToString(DrWebInfo["QuotationDate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Due Date</td><td width=70%>" + Convert.ToString(DrWebInfo["DueDate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Status</td><td width=70%>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Cancelled By</td><td width=70%>" + Convert.ToString(DrWebInfo["Cancelledby"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Cancel Date</td><td>" + Convert.ToString(DrWebInfo["canceldate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Description</td><td>" + Convert.ToString(DrWebInfo["CancelDescription"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");

            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

           
            MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Customer Information</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td width=30%>customer</td><td>" + Convert.ToString(DrWebInfo["VendorName"]) + "</td></tr>" +
                  "<tr  height=25px><td width=30%>Address</td><td>" + Convert.ToString(DrWebInfo["AddressName"]) + "</td></tr>" +
                  "<tr  bgcolor=#fff2eb height=25px><td width=30%>Contactperson</td><td>" + Convert.ToString(DrWebInfo["Contactperson"]) + "</td></tr>" +
                  "<tr  height=25px><td></td><td>" + Convert.ToString(DrWebInfo["Address"]) + " , " + Convert.ToString(DrWebInfo["State"]) + " , " + Convert.ToString(DrWebInfo["Country"]) + " , " + Convert.ToString(DrWebInfo["ZipCode"]) + "</td></tr>");

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";
        MsbHtml.Append("<tr  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Sales Quotation Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Item Code</b></font></td>" +
                       "<td width=50%><font color=#FFFFF><b>Item Name</b></font></td><td><font color=#FFFFF><b>Quantity</b></font></td>" +                       
                       "<td><font color=#FFFFF><b>Rate</b></font></td>" +
                       "<td><font color=#FFFFF><b>Grand Amount</b></font></td>" +
                       "<td ><font color=#FFFFF><b>Discount</b></font></td>" +
                       "<td><font color=#FFFFF><b>Total</b></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["itemname"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Quantity"]) +  Convert.ToString(drowEmail["UOM"]) +  "</td>" +                          
                           "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["GrandAmount"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Discountname"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["NetAmount"]) + "</td></tr>");
        }
                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

                if (EmailSource.Tables[2].Rows.Count > 0)
                {

                    strColor = "#ffffff";
                    MsbHtml.Append("<tr  height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
                    MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Expense Type</b></font></td>" +
                                   "<td width=50%><font color=#FFFFF><b>Date</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Quantity</b></font></td>" +
                                   "<td><font color=#FFFFF><b>Amount</b></font></td>" +
                                    "<td><font color=#FFFFF><b>Total</b></font></td>" +
                                   "<td ><font color=#FFFFF><b>Description</b></font></td></tr>");

                    foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
                    {
                        if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

                        MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ExpenseType"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["CreatedDate"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ExpenseQuantity"]) + "</td>" +
                                        "<td>" + Convert.ToString(drowEmail["ExpenseDetailAmount"]) + "</td>" +
                                          "<td>" + Convert.ToString(drowEmail["Total"]) + "</td>" +
                                       "<td>" + Convert.ToString(drowEmail["ExpenseDescription"]) + "</td></tr>");
                    }
                }
                else
                {
                    //MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
                    //MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=7 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");


                }
                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");      
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
            strColor = "#ffffff";
            sAltColor = "#fff2eb";
            if (strColor == "#ffffff") sAltColor = "#fff2eb"; else sAltColor = "#ffffff";
            //if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Payment Details</h2></font></td></tr>");
            MsbHtml.Append("<tr height=25px bgcolor=" + sAltColor + "><td width=30%>Employee</td><td>" + Convert.ToString(DrWebInfo["Executive"]) + "</td></tr>" +
                           "<tr height=25px bgcolor=" + strColor + "><td width=30%>Currency</td><td>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
                           "<tr height=25px bgcolor=" + sAltColor + "><td width=30%>Payment Terms</td><td>" + Convert.ToString(DrWebInfo["PaymentTermName"]) + "</td></tr>" +
                           "<tr height=25px bgcolor=" + strColor + "><td width=30%>Gross Amount</td><td>" + Convert.ToString(DrWebInfo["SubTotal"]) + "</td></tr>" +
                           "<tr height=25px bgcolor=" + sAltColor + "><td width=30%>Expense</td><td>" + Convert.ToString(DrWebInfo["ExpenseAmount"]) + "</td></tr>" +
                           //"<tr height=25px bgcolor=" + strColor + "><td width=30%>Discount Name</td><td>" + Convert.ToString(DrWebInfo["Discountname"]) + "</b></td></tr>" +
                           "<tr height=25px bgcolor=" + sAltColor + "><td width=30%>Grand Discount</td><td>" + Convert.ToString(DrWebInfo["GrandDiscountAmount"]) + "</b></td></tr>" +
                           "<tr height=25px bgcolor=" + strColor + "><td width=30%><b>Total Amount<b></td><td><b>" + Convert.ToString(DrWebInfo["NetAmount"]) + "</b></td></tr>" +
                           "<tr height=25px bgcolor=" + sAltColor + "><td width=30%><b>Amount In Words<b></td><td><b>" + Convert.ToString(DrWebInfo["AmountInWords"]) + "</b></td></tr>");

        }      

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        return MsbHtml.ToString();
       
    }

    private string GetSalesOrderEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Order Type</td><td width=70%>" + Convert.ToString(DrWebInfo["OrderType"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Sales Quotation No</td><td width=70%>" + Convert.ToString(DrWebInfo["SalesQuotationNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Sales Order No</td><td>" + Convert.ToString(DrWebInfo["SalesOrderNo"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Order Date</td><td>" + Convert.ToString(DrWebInfo["OrderDate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Due Date</td><td>" + Convert.ToString(DrWebInfo["DueDate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Status</td><td width=70%>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Executive</td><td width=70%>" + Convert.ToString(DrWebInfo["Executive"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Cancelled By</td><td width=70%>" + Convert.ToString(DrWebInfo["Cancelledby"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Cancel Date</td><td>" + Convert.ToString(DrWebInfo["canceldate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Description</td><td>" + Convert.ToString(DrWebInfo["CancelDescription"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>LPONumber</td><td>" + Convert.ToString(DrWebInfo["LPONumber"]) + "</td></tr>");
        
            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
          
        MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Customer Information</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td width=30%>customer</td><td>" + Convert.ToString(DrWebInfo["VendorName"]) + "</td></tr>" +
                  "<tr  height=25px><td width=30%>Address</td><td>" + Convert.ToString(DrWebInfo["AddressName"]) + "</td></tr>" +
                  "<tr  bgcolor=#fff2eb height=25px><td width=30%></td><td>" + Convert.ToString(DrWebInfo["Contactperson"]) + "</td></tr>" +
                  "<tr  height=25px><td></td><td>" + Convert.ToString(DrWebInfo["Address"]) + " , " + Convert.ToString(DrWebInfo["State"]) + " , " + Convert.ToString(DrWebInfo["Country"]) + " , " + Convert.ToString(DrWebInfo["ZipCode"]) + "</td></tr>");

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";
        MsbHtml.Append("<tr  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Sales Order Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Item Code</b></font></td>" +
                       "<td width=50%><font color=#FFFFF><b>Item Name</b></font></td><td><font color=#FFFFF><b>Quantity</b></font></td>" +                      
                       "<td><font color=#FFFFF><b>Rate</b></font></td>" +
                        "<td><font color=#FFFFF><b>Grand Amount</b></font></td>" +
                       "<td ><font color=#FFFFF><b>Discount</b></font></td>" +
                       "<td><font color=#FFFFF><b>Total</b></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["itemname"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Quantity"]) +  Convert.ToString(drowEmail["UOM"])  + "</td>" +                          
                           "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["GrandAmount"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["DiscountAmount"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["NetAmount"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        if (EmailSource.Tables[2].Rows.Count > 0)
        {

            strColor = "#ffffff";
            MsbHtml.Append("<tr  height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Expense Type</b></font></td>" +
                           "<td width=50%><font color=#FFFFF><b>Date</b></font></td>" +
                           "<td><font color=#FFFFF><b>Quantity</b></font></td>" +
                           "<td><font color=#FFFFF><b>Amount</b></font></td>" +
                           "<td><font color=#FFFFF><b>Total</b></font></td>" + 
                           "<td ><font color=#FFFFF><b>Description</b></font></td></tr>");

            foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
            {
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ExpenseType"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["CreatedDate"]) + "</td>" +                              
                               "<td>" + Convert.ToString(drowEmail["ExpenseQuantity"]) + "</td>" +
                                "<td>" + Convert.ToString(drowEmail["ExpenseDetailAmount"]) + "</td>" +
                                 "<td>" + Convert.ToString(drowEmail["Total"]) + "</td>" +  
                               "<td>" + Convert.ToString(drowEmail["ExpenseDescription"]) + "</td></tr>");
            }
        }
        else
        {
            //MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
            //MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=7 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");
               

        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        
        strColor = "#ffffff";
            if (strColor == "#ffffff") sAltColor = "#fff2eb"; else sAltColor = "#ffffff";
        //if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        if (EmailSource.Tables[0].Rows.Count > 0)
        {

            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
           MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Payment Details</h2></font></td></tr>");
           MsbHtml.Append("<tr height=25px bgcolor=" + sAltColor + "><td> Currency</td><td>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
                          "<tr height=25px bgcolor=" + strColor + "><td>Payment Terms</td><td>" + Convert.ToString(DrWebInfo["PaymentTermName"]) + "</td></tr>" +
                          "<tr height=25px bgcolor=" + sAltColor + "><td>Advance Amount</td><td>" + Convert.ToString(DrWebInfo["AdvanceAmount"]) + "</td></tr>" +
                          "<tr height=25px bgcolor=" + strColor + "><td>Gross Amount</td><td>" + Convert.ToString(DrWebInfo["GrandAmount"]) + "</td></tr>" +
                          //"<tr height=25px bgcolor=" + sAltColor + "><td>Discount</td><td>" + Convert.ToString(DrWebInfo["Discountname"]) + "</td></tr>" +
                          "<tr height=25px bgcolor=" + strColor + "><td>Expense</td><td>" + Convert.ToString(DrWebInfo["ExpenseAmount"]) + "</td></tr>" +
                          "<tr height=25px bgcolor=" + sAltColor + "><td>Grand Discount</td><td>" + Convert.ToString(DrWebInfo["GrandDiscountAmount"]) + "</td></tr>" +
                          "<tr height=25px bgcolor=" + strColor + "><td><b>Total Amount</td><td><b>" + Convert.ToString(DrWebInfo["NetAmount"]) + "</td></tr>" +
                          "<tr height=25px bgcolor=" + sAltColor + "><td><b>Amount In Words</td><td><b>" + Convert.ToString(DrWebInfo["AmountInWords"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        return MsbHtml.ToString();
    }

    private string GetStockTransferEmail()
    {
        GetWebformHeaderInfo();
        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            if (Convert.ToString(EmailSource.Tables[0].Rows[0]["WarehouseName"]) != "")
            {
                strName = "To Warehouse";
                strValue = Convert.ToString(DrWebInfo["WarehouseName"]);
            }
            else if (Convert.ToString(EmailSource.Tables[0].Rows[0]["EmployeeName"]) != "")
            {
                strName = "To Employee";
                strValue = Convert.ToString(DrWebInfo["EmployeeName"]);
            }
            else if (Convert.ToString(EmailSource.Tables[0].Rows[0]["ProjectName"]) != "")
            {
                strName = "To Project";
                strValue = Convert.ToString(DrWebInfo["ProjectName"]);
            }
            else if (Convert.ToString(EmailSource.Tables[0].Rows[0]["DepartmentName"]) != "")
            {
                strName = "To Department";
                strValue = Convert.ToString(DrWebInfo["DepartmentName"]);
            }
            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Transfer Type</td><td width=70%>" + Convert.ToString(DrWebInfo["TransferType"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Stock Transfer No</td><td width=70%>" + Convert.ToString(DrWebInfo["StockTransferNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>From WareHouse</td><td>" + Convert.ToString(DrWebInfo["WareHouse"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>" + strName + "</td><td>" + strValue + "</td></tr>" +
               "<tr height=25px><td width=30%>Status</td><td>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>In Charge</td><td width=70%>" + Convert.ToString(DrWebInfo["Incharge"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Employee</td><td width=70%>" + Convert.ToString(DrWebInfo["Employee"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Transfer Date</td><td width=70%>" + Convert.ToString(DrWebInfo["OrderDate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Due Date</td><td>" + Convert.ToString(DrWebInfo["DueDate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Cancelled By</td><td>" + Convert.ToString(DrWebInfo["Cancelledby"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Cancel Date</td><td>" + Convert.ToString(DrWebInfo["canceldate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Cancel Description</td><td>" + Convert.ToString(DrWebInfo["CancelDescription"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");

            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
            if (Convert.ToString(EmailSource.Tables[0].Rows[0]["VendorName"]) != "")
            {
                MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Supplier Information</h2></font></td></tr>");
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td width=30%>Supplier</td><td>" + Convert.ToString(DrWebInfo["VendorName"]) + "</td></tr>" +
                          "<tr  height=25px><td width=30%>Address</td><td>" + Convert.ToString(DrWebInfo["AddressName"]) + "</td></tr>" +
                          "<tr  bgcolor=#fff2eb height=25px><td width=30%></td><td>" + Convert.ToString(DrWebInfo["ContactPerson"]) + "</td></tr>" +
                          "<tr  height=25px><td></td><td>" + Convert.ToString(DrWebInfo["Address"]) + " , " + Convert.ToString(DrWebInfo["State"]) + " , " + Convert.ToString(DrWebInfo["Country"]) + " , " + Convert.ToString(DrWebInfo["ZipCode"]) + "</td></tr>");
            }
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";
        MsbHtml.Append("<tr  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Stock Transfer Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Item Code</b></font></td>" +
                       "<td width=50%><font color=#FFFFF><b>Item Name</b></font></td>" +
                       //"<td><font color=#FFFFF><b>BatchNo</b></font></td>" +
                       "<td><font color=#FFFFF><b>Quantity</b></font></td>" +
                       "<td><font color=#FFFFF><b>Rate</b></font></td>" +
                       "<td ><font color=#FFFFF><b>Grand Amount</b></font></td>" +
                       "<td ><font color=#FFFFF><b>Discount</b></font></td>" +
                       "<td><font color=#FFFFF><b>Total</b></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                           //"<td>" + Convert.ToString(drowEmail["BatchNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Quantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["GrandAmount"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["DiscountAmount"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["NetAmount"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        if (EmailSource.Tables[2].Rows.Count > 0)
        {

            strColor = "#fff2eb";
            MsbHtml.Append("<tr  height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Expense Category</b></font></td>" +
                           "<td width=50%><font color=#FFFFF><b>Expense Type</b></font></td>" +
                           "<td width=50%><font color=#FFFFF><b>Date</b></font></td>" +
                           "<td><font color=#FFFFF><b>Quantity</b></font></td>" +
                           "<td><font color=#FFFFF><b>Amount</b></font></td>" +
                           "<td><font color=#FFFFF><b>Total</b></font></td>" +
                           "<td ><font color=#FFFFF><b>Description</b></font></td></tr>");

            foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
            {
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ExpenseCatagory"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["ExpenseType"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["CreatedDate"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["ExpenseQuantity"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["ExpenseDetailAmount"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["ExpenseTotal"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["ExpenseDescription"]) + "</td></tr>");
            }
        }
        else
        {
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");


        strColor = "#ffffff";
        if (strColor == "#ffffff") sAltColor = "#fff2eb"; else sAltColor = "#ffffff";
        if (EmailSource.Tables[0].Rows.Count > 0)
        {

            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
            MsbHtml.Append("<tr bgcolor=" + sAltColor + " height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Payment Details</h2></font></td></tr>");
            MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td>Currency</td><td>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
                 "<tr height=25px bgcolor=" + sAltColor + "><td>Gross Amount</td><td>" + Convert.ToString(DrWebInfo["GrandAmount"]) + "</td></tr>" +
                 "<tr height=25px bgcolor=" + strColor + "><td>Discount</td><td>" + Convert.ToString(DrWebInfo["DiscountType"]) + "</td></tr>" +
                 "<tr height=25px bgcolor=" + sAltColor + "><td>Extra Charges</td><td>" + Convert.ToString(DrWebInfo["ExtraCharges"]) + "</td></tr>" +
                 "<tr height=25px bgcolor=" + strColor + "><td>Expense</td><td>" + Convert.ToString(DrWebInfo["ExpenseAmount"]) + "</td></tr>" +
                 "<tr height=25px bgcolor=" + sAltColor + "><td>Grand Discount</td><td>" + Convert.ToString(DrWebInfo["GrandDiscountAmount"]) + "</td></tr>" +
                  "<tr height=25px bgcolor=" + strColor + "><td><b>Total Amount</td><td><b>" + Convert.ToString(DrWebInfo["NetAmount"]) + "</td></tr>" +
                 "<tr height=25px bgcolor=" + sAltColor + "><td><b>Amount In Words</td><td><b>" + Convert.ToString(DrWebInfo["AmountInWords"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        return MsbHtml.ToString();
    }
    private string GetDeliveryNoteEmail()
    {      
        
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
          intType = Convert.ToInt32(DrWebInfo["OperationTypeID"]);
            if (intType == (int)OperationOrderType.DNOTSalesInvoice)
            {
                Type = "Sales Invoice No";
                strRefNoName = "Invoiced Qty";
            }
            else if(intType==(int)OperationOrderType.DNOTSalesOrder)
            {
                Type = "Sales Order No";
                strRefNoName = "Ordered Qty";
            }
            else if(intType==(int)OperationOrderType.DNOTTransfer)
            {
                Type = "Transfer Order No";
                strRefNoName = "Ordered Qty";
            }
           
            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Item Issue No</td><td width=70%>" + Convert.ToString(DrWebInfo["ItemIssueNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Operation Type</td><td width=70%>" + Convert.ToString(DrWebInfo["OperationType"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>" + Type + "</td><td>" + Convert.ToString(DrWebInfo["ReferenceNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Warehouse</td><td>" + Convert.ToString(DrWebInfo["Warehouse"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Issued By</td><td>" + Convert.ToString(DrWebInfo["IssuedBy"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Issued Date</td><td>" + Convert.ToString(DrWebInfo["IssuedDate"]) + "</td></tr>" +            
               "<tr height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");              
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";
        MsbHtml.Append("<tr  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Delivery Note Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Item Code</b></font></td>" +
            "<td width=50%><font color=#FFFFF><b>Item Name</b></font></td>" 
            //+
                       //"<td width=50%><font color=#FFFFF><b>Batch No</b></font></td>"
        );
                        if(intType != 22)
                            MsbHtml.Append("<td><font color=#FFFFF><b>" + strRefNoName + "</b></font></td>" );
         MsbHtml.Append("<td><font color=#FFFFF><b>Quantity</b></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                 "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" 
                 //+
                         //"<td>" + Convert.ToString(drowEmail["BatchNo"]) + "</td>" 
                           );
            if(intType != 22)
                MsbHtml.Append("<td>" + Convert.ToString(drowEmail["InvoicedQty"]) + "</td>" );

            MsbHtml.Append("<td>" + Convert.ToString(drowEmail["Quantity"]) + Convert.ToString(drowEmail["Uom"]) + "</td></tr>");  
        }
    
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        return MsbHtml.ToString();
    }
    

    private string GetSalesInvoiceEmail()  
    {
        GetWebformHeaderInfo();  

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Sales Invoice No</td><td width=70%>" + Convert.ToString(DrWebInfo["SalesInvoiceNo"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Sales Order No</td><td width=70%>" + Convert.ToString(DrWebInfo["SalesOrderNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Invoice Date</td><td width=70%>" + Convert.ToString(DrWebInfo["OrderDate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Due Date</td><td>" + Convert.ToString(DrWebInfo["DueDate"]) + "</td></tr>" +              
               "<tr  height=25px><td width=30%>Status</td><td width=70%>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
               "<tr  bgcolor=#fff2eb height=25px><td width=30%>Executive</td><td width=70%>" + Convert.ToString(DrWebInfo["Executive"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Cancelled By</td><td width=70%>" + Convert.ToString(DrWebInfo["Cancelledby"]) + "</td></tr>" +
               "<tr  bgcolor=#fff2eb height=25px><td width=30%>Cancel Date</td><td>" + Convert.ToString(DrWebInfo["canceldate"]) + "</td></tr>" +

               "<tr height=25px><td width=30%>Description</td><td>" + Convert.ToString(DrWebInfo["CancelDescription"]) + "</td></tr>" +              
               // "<tr bgcolor=#fff2eb height=25px><td width=30%>Advance Payment</td><td>" + Convert.ToString(DrWebInfo["AdvanceAmount"]) + "</td></tr>" + 
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>LPONumber</td><td>" + Convert.ToString(DrWebInfo["LPONumber"]) + "</td></tr>");
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Customer Information</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td width=30%>customer</td><td>" + Convert.ToString(DrWebInfo["VendorName"]) + "</td></tr>" +
                  "<tr  height=25px><td width=30%>Address</td><td>" + Convert.ToString(DrWebInfo["AddressName"]) + "</td></tr>" +
                  "<tr  bgcolor=#fff2eb height=25px><td width=30%></td><td>" + Convert.ToString(DrWebInfo["CustContact"]) + "</td></tr>" +
                  "<tr  height=25px><td></td><td>" + Convert.ToString(DrWebInfo["Address"]) + " , " + Convert.ToString(DrWebInfo["State"]) + " , " + Convert.ToString(DrWebInfo["Country"]) + " , " + Convert.ToString(DrWebInfo["ZipCode"]) + "</td></tr>");

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";
        MsbHtml.Append("<tr  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Sales Invoice Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Item Code</b></font></td>" +
                       "<td width=50%><font color=#FFFFF><b>Item Name</b></font></td><td><font color=#FFFFF><b>Quantity</b></font></td>" +                     
                      // "<td><font color=#FFFFF><b>Location</b></font></td>" +
                       "<td><font color=#FFFFF><b>Rate</b></font></td>" +
                        "<td><font color=#FFFFF><b>Grand Amount</b></font></td>" +
                       "<td ><font color=#FFFFF><b>Discount</b></font></td>" +
                       "<td><font color=#FFFFF><b>Total</b></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["itemname"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Quantity"]) + Convert.ToString(drowEmail["UOM"]) +  "</td>" +                       
                         //  "<td>" + Convert.ToString(drowEmail["Location"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                           //"<td>" + Convert.ToString(drowEmail["DiscountAmount"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["GrandAmount"]) + "</td>" +
                            "<td>" + Convert.ToString(drowEmail["DiscountAmount"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["NetAmount"]) + "</td></tr>");
                            
                          // "<td>" + Convert.ToString(drowEmail["ItemTotal"]) + "</td></tr>");
          
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        if (EmailSource.Tables[2].Rows.Count > 0)
        {
            strColor = "#ffffff";

            MsbHtml.Append("<tr  height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=30%><font color=#FFFFF><b>Expense Type</b></font></td>" +
                           "<td width=20%><font color=#FFFFF><b>Date</b></font></td>" +
                           "<td><font color=#FFFFF><b>Quantity</b></font></td>" +
                           "<td><font color=#FFFFF><b>Amount</b></font></td>" +
                            "<td><font color=#FFFFF><b>Total</b></font></td>" +  
                           "<td ><font color=#FFFFF><b>Description</b></font></td></tr>");

            foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
            {
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ExpenseType"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["CreatedDate"]) + "</td>" +                              
                               "<td>" + Convert.ToString(drowEmail["ExpenseQuantity"]) + "</td>" +
                                "<td>" + Convert.ToString(drowEmail["Amount"]) + "</td>" +
                                 "<td>" + Convert.ToString(drowEmail["Total"]) + "</td>" +   
                               "<td>" + Convert.ToString(drowEmail["ExpenseDescription"]) + "</td></tr>");
            }
        }
        else
        {
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            //MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Expense Details</h2></font></td></tr>");
            //MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=7 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");


        }
                MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");


        strColor = "#ffffff";
       if (strColor == "#ffffff") sAltColor = "#fff2eb"; else sAltColor = "#ffffff";

        //if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
            MsbHtml.Append( "<tr bgcolor=" + strColor + " height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Payment Details</h2></font></td></tr>");
            MsbHtml.Append( "<tr height=25px bgcolor=" + sAltColor + "><td> Currency</td><td>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
                            "<tr height=25px bgcolor=" + strColor + "><td>Payment Terms</td><td>" + Convert.ToString(DrWebInfo["PaymentTermName"]) + "</b></td></tr>" +
                            "<tr height=25px bgcolor=" + sAltColor + "><td>Gross Amountt</td><td>" + Convert.ToString(DrWebInfo["GrandAmount"]) + "</b></td></tr>" +
                          // "<tr height=25px bgcolor=" + strColor + "><td>Discount</td><td>" + Convert.ToString(DrWebInfo["Discountname"]) + "</b></td></tr>" +
                            "<tr height=25px bgcolor=" + strColor + "><td>Expense</td><td>" + Convert.ToString(DrWebInfo["ExpenseAmount"]) + "</b></td></tr>" +
                            "<tr height=25px bgcolor=" + sAltColor + "><td>Grand Discount</td><td>" + Convert.ToString(DrWebInfo["GrandDiscountAmount"]) + "</b></td></tr>" +
                            "<tr height=25px bgcolor=" + strColor + "><td><b>Total</td><td><b>" + Convert.ToString(DrWebInfo["NetAmount"]) + "</td></tr>" +
                            "<tr height=25px bgcolor=" + sAltColor + "><td><b>Amount In Words</td><td><b>" + Convert.ToString(DrWebInfo["AmountInWords"]) + "</b></td></tr>");

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");


        

        return MsbHtml.ToString();
    }
    private string GetSalesReturnEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Sales Return No</td><td width=70%>" + Convert.ToString(DrWebInfo["ReturnNo"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Sales Invoice No</td><td width=70%>" + Convert.ToString(DrWebInfo["SalesInvoiceNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Date</td><td width=70%>" + Convert.ToString(DrWebInfo["ReturnDate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Status</td><td width=70%>" + Convert.ToString(DrWebInfo["Status"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Cancelled By</td><td width=70%>" + Convert.ToString(DrWebInfo["Cancelledby"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Cancel Date</td><td>" + Convert.ToString(DrWebInfo["canceldate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Description</td><td>" + Convert.ToString(DrWebInfo["CancelDescription"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");

            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

            MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Customer Information</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td width=30%>customer</td><td>" + Convert.ToString(DrWebInfo["VendorName"]) + "</td></tr>" +
                      "<tr  height=25px><td width=30%>Address</td><td>" + Convert.ToString(DrWebInfo["AddressName"]) + "</td></tr>" +
                      "<tr  bgcolor=#fff2eb height=25px><td width=30%></td><td>" + Convert.ToString(DrWebInfo["Contactperson"]) + "</td></tr>" +
                      "<tr  height=25px><td></td><td>" + Convert.ToString(DrWebInfo["Address"]) + " , " + Convert.ToString(DrWebInfo["State"]) + " , " + Convert.ToString(DrWebInfo["Country"]) + " , " + Convert.ToString(DrWebInfo["ZipCode"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";
        MsbHtml.Append("<tr  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Sales Return Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Item Code</b></font></td>" +
                       "<td width=50%><font color=#FFFFF><b>Item Name</b></font></td>"+
                       //<td><font color=#FFFFF><b>Batch No</b></font></td>" +
                     //  "<td><font color=#FFFFF><b>Location</b></font></td>" +
                       "<td><font color=#FFFFF><b>Inv Qty</b></font></td>" +
                       "<td><font color=#FFFFF><b>Ret Qty</b></font></td>" +                       
                       "<td><font color=#FFFFF><b>Rate</b></font></td>" +
                       "<td ><font color=#FFFFF><b>Discount</b></font></td>" +
                       "<td><font color=#FFFFF><b>Total</b></font></td>"+
                       "<td><font color=#FFFFF><b>Return Amt</b></font></td>"+
                       "<td><font color=#FFFFF><b>Reason</b></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                           //"<td>" + Convert.ToString(drowEmail["BatchNo"]) + "</td>" +
                           //"<td>" + Convert.ToString(drowEmail["Location"]) + "</td>" +
                            "<td>" + Convert.ToString(drowEmail["InvQty"])  + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["RetQty"]) + Convert.ToString(drowEmail["ShortName"]) + "</td>" +                        
                           "<td>" + Convert.ToString(drowEmail["InvRate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Discount"]) + "</td>" +
                            "<td>" + Convert.ToString(drowEmail["NetAmount"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ReturnAmount"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Reason"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");      


        strColor = "#ffffff";
        if (strColor == "#ffffff") sAltColor = "#fff2eb"; else sAltColor = "#ffffff";

        //if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Payment Details</h2></font></td></tr>");
            MsbHtml.Append("<tr height=25px bgcolor=" + sAltColor + "><td>Currency</td><td>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
                "<tr height=25px bgcolor=" + strColor + "><td>Gross Amount</td><td>" + Convert.ToString(DrWebInfo["GrandAmount"]) + "</td></tr>" +
               "<tr height=25px bgcolor=" + sAltColor + "><td>Grand Discountt</td><td>" + Convert.ToString(DrWebInfo["GrandDiscountAmount"]) + "</b></td></tr>" +
                "<tr height=25px bgcolor=" + strColor + "><td><b>Total</td><td><b>" + Convert.ToString(DrWebInfo["NetAmount"]) + "</b></td></tr>" +
                  "<tr height=25px bgcolor=" + sAltColor + "><td><b>Return Amount</td><td><b>" + Convert.ToString(DrWebInfo["GrandReturnAmount"]) + "</b></td></tr>" +
               "<tr height=25px bgcolor=" + strColor + "><td><b>Amount In Words</td><td><b>" + Convert.ToString(DrWebInfo["AmountInWords"]) + "</b></td></tr>"); 
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }


    private string GetItemGroupingEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[0].Rows)
        {
            // Master data
            MsbHtml.Append("<tr height=25px><td width =30%>Code</td><td width =70%>" + Convert.ToString(drowEmail["GroupCode"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Name</td><td>" + Convert.ToString(drowEmail["GroupName"]) + "</td></tr>" +
                           "<tr height=25px><td>Actual Amount</td><td>" + Convert.ToString(drowEmail["ActualAmount"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>Labour Cost</td><td>" + Convert.ToString(drowEmail["LabourCharge"]) + "</td></tr>" +
                           "<tr height=25px><td>Sale Amount</td><td>" + Convert.ToString(drowEmail["SaleAmount"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Status</td><td>" + Convert.ToString(drowEmail["GroupStatus"]) + "</td></tr>" +
                           "<tr height=25px><td>Remarks</td><td>" + Convert.ToString(drowEmail["Remarks"]) + "</td></tr>"
                           );
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Item Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item Code</b></font></td><td><font color=#FFFFF><b>Item Name</b></font></td>" +
                       "<td><font color=#FFFFF><b>Quantity</b></font></td>" +
                       "<td><font color=#FFFFF><b>Rate</b></font></td><td><font color=#FFFFF><b>Total</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Quantity"]) + Convert.ToString(drowEmail["Uom"]) +  "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Total"]) + "</td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }
        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    public string GetWebFormData(string sCaption, int iFormType, long iID)
    {
        MstrCaption = sCaption;
        MintRecordID = iID;
        MintFormType = iFormType;

        string sHtml = "<html><head></head><body oncontextmenu=return false ondragstart=return false onselectstart=return false><center><img src='" + imgPath + "' border=none alt=No Information Found></img></center></body></html>";
        SqlDataReader DrWebInfo = GetWebformInfo();
        switch (iFormType)
        {


            //case (int)EmailFormID.Vendor:
            //    sHtml = GetWebformAssociateInformation(DrWebInfo);
            //    break;
            case (int)EmailFormID.Warehouse:
                sHtml = GetWebformWarehouseInformation(DrWebInfo);
                break;
            case (int)EmailFormID.AccountSettings:
                sHtml = GetWebformWithCompanyInfoInGrid(DrWebInfo, "Account Details");
                break;
            case (int)EmailFormID.ChartOfAccounts:
                sHtml = GetWebformChartofAccounts(DrWebInfo);
                break;
            case (int)EmailFormID.DiscountNormal:
                sHtml = GetWebformDiscountNormal(DrWebInfo);
                break;
            case (int)EmailFormID.Discountitemwise:
                sHtml = GetWebformDiscountitemwise(DrWebInfo);
                break;
            case (int)EmailFormID.Discountcustomerwise:
                sHtml = GetWebformDiscountcustomerwise(DrWebInfo);
                break;
            case (int)EmailFormID.PaymentTerms:
            case (int)EmailFormID.TaxScheme:
            case (int)EmailFormID.UnitsOfMeasurement:
            case (int)EmailFormID.UOMConversion:
            case (int)EmailFormID.Configuration:
            case (int)EmailFormID.ItemBatch:
                sHtml = GetWebformInformationInGrid(DrWebInfo);
                break;
            case (int)EmailFormID.RoleSettings:
                sHtml = GetWebformRoleSettings(DrWebInfo);
                break;
            case (int)EmailFormID.CompanySettings:
                sHtml = GetWebformCompanySettings(DrWebInfo);
                break;
            case (int)EmailFormID.Bank:
                sHtml = GetWebformCommonInformation(DrWebInfo);
                break;
            case (int)EmailFormID.VendorHistory:
            case (int)EmailFormID.CustomerHistory:
                sHtml = GetWebformVendorHistory((iFormType == (int)EmailFormID.CustomerHistory ? "Customer" : "Vendor"));
                break;
        }
        DrWebInfo.Close();
        return sHtml;
    }

    private SqlDataReader GetWebformInfo()
    {
        ArrayList arrParameters = new ArrayList();
        SqlDataReader sdrWebInfo;
        if (MintFormType == (int)EmailFormID.RoleSettings)
        {
            arrParameters.Add(new SqlParameter("@RoleID", MintRecordID));
            sdrWebInfo = MobjConnection.ExecutesReader("STReportRoleSettings", arrParameters, CommandBehavior.CloseConnection);
        }
        else if (MintFormType == (int)EmailFormID.Payments)
        {
            arrParameters.Add(new SqlParameter("PaymentID", MintRecordID));
            sdrWebInfo = MobjConnection.ExecutesReader("STPaymentTransaction", arrParameters, CommandBehavior.CloseConnection);
        }
        else
        {
            arrParameters.Add(new SqlParameter("@FormType", MintFormType));
            arrParameters.Add(new SqlParameter("@ID", MintRecordID));
            sdrWebInfo = MobjConnection.ExecutesReader("STGetWebformInformation", arrParameters, CommandBehavior.CloseConnection);
        }

        return sdrWebInfo;
    }

    private string GetWebformCommonInformation(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        strColor = "#ffffff";
        
        if (DrWebInfo.Read())
        {
            GetWebformHeaderInfo();

            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
            MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");

            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(iIndex);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(iIndex).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb")
                    strColor = "#ffffff";
                else
                    strColor = "#fff2eb";
                iIndex++;
            }
            MsbHtml.Append("</table></td></tr></table></div></body></html>");
        }
        return MsbHtml.ToString();
    }

    private string GetWebformInformationInGrid(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        strColor = "#ffffff";

        if (DrWebInfo.HasRows)
            GetWebformGridHeaderInfo();

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        while (DrWebInfo.Read())
        {
            if (iIndex == 0)
            {
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px>");
                for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
                {
                    sFieldName = DrWebInfo.GetName(i);
                    MsbHtml.Append("<td align=left><font color=#3a3a53><h2>" + sFieldName + "</h2></font></td>");                    
                }
                MsbHtml.Append("</tr>");
            }

            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<td align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td>");                
            }
            if (strColor == "#fff2eb")
                strColor = "#ffffff";
            else
                strColor = "#fff2eb";

            MsbHtml.Append("</tr>");
            iIndex++;            
        }
        MsbHtml.Append("</table></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    public string GetUsedFormsInformation(string sCaption, int iPrimeryId, string sCondition, string sFileds)
    {
        ArrayList fmParameters = new ArrayList();
        fmParameters.Add(new SqlParameter("@PrimeryID", iPrimeryId));
        fmParameters.Add(new SqlParameter("@Condition", sCondition));
        fmParameters.Add(new SqlParameter("@FieldName", sFileds));
        DataTable dtForms = MobjConnection.FillDataSet("STCheckIDExists", fmParameters).Tables[0];

        StringBuilder MsbHtml = new StringBuilder();

        MsbHtml.Append("<html><head>" +
                       "<style>" +
                      "td {font-family:arial;font-size:12px;border:none;color:black;padding:5px}" +
                      "a.link{font-family:arial;color:Blue;text-decoration: none}" +
                      "a.link:hover{font-family:arial;color: #3399CC;text-decoration: none}" +
                      "</style></head><body bgcolor=whiteSmoke topmargin=0 leftmargin=0 oncontextmenu=return false ondragstart=return false onselectstart=return false><div align=center>");
        MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>" +
                      "<tr bgcolor=#3a3a53><td colspan=2 align=left height=30px><font size=1 color=white><b>" + sCaption + "</b></font></td></tr>");

        MsbHtml.Append("<tr bgcolor=#fff2eb height=15px><td colspan=2 align=left><font size=1 color=#3a3a53><h2>Forms</h2></font></td></tr>");
        strColor = "#ffaaee";
        if (dtForms.Rows.Count > 0)
        {
            if (Convert.ToString(dtForms.Rows[0]["FormName"]) != "0")
            {
                for (int i = 0; i < dtForms.Rows.Count; i++)
                {
                    MsbHtml.Append("<tr bgcolor=" + strColor + "><td width=70% align=left>" + Convert.ToString(dtForms.Rows[i]["FormName"]) + "</td></tr>");
                    if (strColor == "#ffaaee")
                        strColor = "#ffffff";
                    else
                        strColor = "#ffaaee";
                }
            }
            else
                MsbHtml.Append("<tr><td width=70% align=left> No Information found</td></tr>");
        }
        else
        {
            MsbHtml.Append("<tr><td width=70% align=left> No Information found</td></tr>");
        }

        MsbHtml.Append("</table></div></Body></HTML>");

        return MsbHtml.ToString(); ;
    }

    private string GetWebformCompanySettings(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        string sConfigItem = string.Empty;
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");

        if (DrWebInfo.Read())
        {
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(i).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb")
                    strColor = "#ffffff";
                else
                    strColor = "#fff2eb";
            }
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        DrWebInfo.NextResult();
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=3 align=left><font size=2 color=#3a3a53><h2>Company Settings</h2></font></td></tr>");
        strColor = "#ffffff";
        while (DrWebInfo.Read())
        {
            if (!sConfigItem.Trim().Equals(Convert.ToString(DrWebInfo["ConfigurationItem"]).Trim()))
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=3>" + Convert.ToString(DrWebInfo["ConfigurationItem"]).Trim() + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }

            sConfigItem = Convert.ToString(DrWebInfo["ConfigurationItem"]);

            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td width=10%>&nbsp;</td>");
            for (int i = 1; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<td align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td>");                
            }
            if (strColor == "#fff2eb")
                strColor = "#ffffff";
            else
                strColor = "#fff2eb";
            MsbHtml.Append("</tr>");
            iIndex++;
        }
        MsbHtml.Append("</table></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetWebformRoleSettings(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        string sPermissions = string.Empty;
        string sCompany = string.Empty, sMenu = string.Empty, sModule = string.Empty;
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        while (DrWebInfo.Read())
        {
            if (iIndex == 0)
            {
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td align=left><font size=2><h2>Role:</h2></font></td><td align=left><font size=2><h2>" +
                    Convert.ToString(DrWebInfo["RoleName"]) + "</h2></font></td></tr></table>");
                MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");
                MsbHtml.Append("<tr height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Permission Settings</h2></font></td></tr>");

                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px>");
                for (int i = 1; i <5; i++)
                {
                    sFieldName = DrWebInfo.GetName(i);
                    MsbHtml.Append("<td align=left><font color=#3a3a53><h2>" + sFieldName + "</h2></font></td>");
                }
                MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Permissions</h2></font></td></tr>");
            }

            if (!sCompany.Trim().Equals(Convert.ToString(DrWebInfo["Company"]).Trim()))
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=5>" + Convert.ToString(DrWebInfo["Company"]).Trim() + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }

            if (!(sCompany + sModule).Trim().Equals(Convert.ToString(DrWebInfo["Company"]).Trim() + Convert.ToString(DrWebInfo["Module"]).Trim()))
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=1>&nbsp;</td><td colspan=4>" + Convert.ToString(DrWebInfo["Module"]).Trim() + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
           
            if (!sMenu.Trim().Equals(Convert.ToString(DrWebInfo["Menu"]).Trim()))
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=2>&nbsp;</td><td colspan=2>" + Convert.ToString(DrWebInfo["Menu"]).Trim() + "</td><td>" + Convert.ToString(DrWebInfo["MenuPermissions"]).Trim() + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
     
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=3>&nbsp;</td><td>" + Convert.ToString(DrWebInfo["Fields"]).Trim() + "</td><td>" + Convert.ToString(DrWebInfo["FieldPermissions"]).Trim() + "</td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            sCompany = Convert.ToString(DrWebInfo["Company"]);            
            sModule = Convert.ToString(DrWebInfo["Module"]);
            sMenu = Convert.ToString(DrWebInfo["Menu"]);
            sFieldName = Convert.ToString(DrWebInfo["Fields"]);
            sPermissions = Convert.ToString(DrWebInfo["FieldPermissions"]);
            iIndex++;
        }
        MsbHtml.Append("</table></div></body></html>");
        DrWebInfo.Close();
        return MsbHtml.ToString();
    }

    private string GetWebformWithCompanyInfoInGrid(SqlDataReader DrWebInfo,string sHeader)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr></table>");
        MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");

        if (DrWebInfo.Read())
        {
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(i).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb")
                    strColor = "#ffffff";
                else
                    strColor = "#fff2eb";
            }
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        DrWebInfo.NextResult();
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + DrWebInfo.VisibleFieldCount.ToString() + " align=left><font size=2 color=#3a3a53><h2>" + sHeader + "</h2></font></td></tr>");
        
        while (DrWebInfo.Read())
        {
            if (iIndex == 0)
            {
                MsbHtml.Append("<tr height=25px>");
                for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
                {
                    sFieldName = DrWebInfo.GetName(i);
                    MsbHtml.Append("<td align=left><font color=#3a3a53><h2>" + sFieldName + "</h2></font></td>");
                }
                MsbHtml.Append("</tr>");
            }

            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<td align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td>");
                if (strColor == "#fff2eb")
                    strColor = "#ffffff";
                else
                    strColor = "#fff2eb";
            }
            MsbHtml.Append("</tr>");
            iIndex++;
        }
        MsbHtml.Append("</table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetWebformChartofAccounts(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        string sConfigItem = string.Empty;
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr></table>");
        MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");

        if (DrWebInfo.Read())
        {
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(i).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb")
                    strColor = "#ffffff";
                else
                    strColor = "#fff2eb";
            }
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        DrWebInfo.NextResult();
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Chart Of Accounts</h2></font></td></tr>");
        strColor = "#ffffff";

        while (DrWebInfo.Read())
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=2>" + (Convert.ToInt32(DrWebInfo["AccountType"]) == 0 ? "<h2>" : "") + Convert.ToString(DrWebInfo["Description"]) + (Convert.ToInt32(DrWebInfo["AccountType"]) == 0 ? "</h2>" : "") + "</td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            iIndex++;
        }
        MsbHtml.Append("</table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetWebformVendorHistory(string sAssociateType)
    {
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%><tr bgcolor=#fff2eb height=25px>");
        MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Company</h2></font></td>");
        MsbHtml.Append("<td align=left><font color=#3a3a53><h2>" + sAssociateType + "</h2></font></td>");
        MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Date</h2></font></td>");
        MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Invoice No</h2></font></td>");
        MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Amount</h2></font></td>");
        MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Status</h2></font></td></tr>");

        foreach (DataRow Row in datHistory.Rows)
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");

            MsbHtml.Append("<td align=left>" + Convert.ToString(Row["Name"]) + "</td>");
            MsbHtml.Append("<td align=left>" + Convert.ToString(Row["VendorName"]) + "</td>");
            MsbHtml.Append("<td align=left>" + Convert.ToString(Row["InvoiceDate"]) + "</td>");
            MsbHtml.Append("<td align=left>" + Convert.ToString(Row["InvoiceNo"]) + "</td>");
            MsbHtml.Append("<td align=left>" + Convert.ToString(Row["InvoiceAmount"]) + "</td>");
            MsbHtml.Append("<td align=left>" + Convert.ToString(Row["Status"]) + "</td></tr>");

            if (strColor == "#fff2eb")
                strColor = "#ffffff";
            else
                strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetWebformMasterDetails(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        strColor = "#ffffff";

        if (DrWebInfo.Read())
        {
            GetWebformGridHeaderInfo(); 

            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<tr height=25px bgcolor="+strColor+"><td width=30% align=left> " + DrWebInfo.GetName(i).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }

            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        }

        
        DrWebInfo.NextResult();
        if (DrWebInfo.HasRows)
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=" + DrWebInfo.FieldCount + " align=left><font size=2 color=#3a3a53><h2>" + MstrCaption + "</h2></font></td></tr>");

            while (DrWebInfo.Read())
            {
                if (iIndex == 0)
                {
                    if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                    MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
                    for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
                    {
                        sFieldName = DrWebInfo.GetName(i);
                        MsbHtml.Append("<td align=left><font color=#3a3a53><h2>" + sFieldName + "</h2></font></td>");
                    }
                    MsbHtml.Append("</tr>");
                }

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
                for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
                {
                    sFieldName = DrWebInfo.GetName(i);
                    MsbHtml.Append("<td>" + Convert.ToString(DrWebInfo[sFieldName]).Trim() + "</td>");

                }
                MsbHtml.Append("</tr>");
                iIndex++;
            }
            MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        }
        return MsbHtml.ToString();
    }

    
    private void GetWebformHeaderInfo()
    {
        MsbHtml = new StringBuilder();
        MsbHtml.Append("<html><head><style type=text/css> <!-- body,td,th { font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000; }" +
            " h1{ font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#FFF; padding:0px; margin:0px; } " +
            " h2{ font-family:Arial, Helvetica, sans-serif; font-size:13px; color: #000; padding:0 0 0 0px; margin:0px; } " +
            " body { margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; } " +
            " td { padding-left:10px; } --> </style></head><body bgcolor=White topmargin=0 oncontextmenu=return false ondragstart=return false onselectstart=return false><div align=center>");
        MsbHtml.Append("<table width=100% border=0 cellspacing=0 cellpadding=0><tr><td style=padding:10px;><table width=100% border=0 cellspacing=0 cellpadding=0>" +
            " <tr><td height=34 colspan=2 bgcolor=#6d6e71><h1>" + MstrCaption + "</h1></td></tr><tr><td height=5 colspan=2>&nbsp;</td></tr>");
    }

    private string GetWebformDiscountNormal(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        strColor = "#ffffff";

        if (DrWebInfo.Read())
        {
            GetWebformHeaderInfo();

            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
            MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");

            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(iIndex);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(iIndex).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb")
                    strColor = "#ffffff";
                else
                    strColor = "#fff2eb";
                iIndex++;
            }
            MsbHtml.Append("</table></td></tr></table></div></body></html>");
        }
        return MsbHtml.ToString();
    }
    private string GetWebformDiscountitemwise(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        string sConfigItem = string.Empty;
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (DrWebInfo.Read())
        {
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(i).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }

        MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");
        DrWebInfo.NextResult();
        MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Itemwise Details</h2></font></td></tr>");

        while (DrWebInfo.Read())
        {
            if (iIndex == 0)
            {
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
                for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
                {
                    sFieldName = DrWebInfo.GetName(i);
                    MsbHtml.Append("<td align=left><font color=#3a3a53><h2>" + sFieldName + "</h2></font></td>");
                }
                MsbHtml.Append("</tr>");
            }

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<td>" + Convert.ToString(DrWebInfo[sFieldName]).Trim() + "</td>");

            }
            MsbHtml.Append("</tr>");
            iIndex++;
        }
        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
        
    }

    private string GetWebformDiscountcustomerwise(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        string sConfigItem = string.Empty;
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (DrWebInfo.Read())
        {
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(i).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }

        MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");
        DrWebInfo.NextResult();
        MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Customerwise Details</h2></font></td></tr>");

        while (DrWebInfo.Read())
        {
            if (iIndex == 0)
            {
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
                for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
                {
                    sFieldName = DrWebInfo.GetName(i);
                    MsbHtml.Append("<td align=left><font color=#3a3a53><h2>" + sFieldName + "</h2></font></td>");
                }
                MsbHtml.Append("</tr>");
            }

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<td>" + Convert.ToString(DrWebInfo[sFieldName]).Trim() + "</td>");

            }
            MsbHtml.Append("</tr>");
            iIndex++;
        }
        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
        
    }   

   

    private string GetWebformWarehouseInformation(SqlDataReader DrWebInfo)
    {
        GetWebformHeaderInfo();        

        if (DrWebInfo.Read())
        {
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
            MsbHtml.Append("<tr height=25px><td width=30%> Name </td><td width=70%>" + Convert.ToString(DrWebInfo["WarehouseName"]) + "</td></tr>" +
              "<tr bgcolor=#fff2eb height=25px><td width=30%>Short Name</td><td width=70%>" + Convert.ToString(DrWebInfo["ShortName"]) + "</td></tr>" +
              "<tr height=25px><td width=30%>Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["Company"]) + "</td></tr>" +
              "<tr bgcolor=#fff2eb height=25px><td width=30%> Address1 </td><td>" + Convert.ToString(DrWebInfo["Address1"]) + "</td></tr>" +
              "<tr height=25px><td width=30%> Address2 </td><td>" + Convert.ToString(DrWebInfo["Address2"]) + "</td></tr>" +
              "<tr bgcolor=#fff2eb height=25px><td width=30%>Zip Code</td><td>" + Convert.ToString(DrWebInfo["ZipCode"]) + "</td></tr>" +
              "<tr height=25px><td width=30%>City</td><td>" + Convert.ToString(DrWebInfo["CityState"]) + "</td></tr>" +
              "<tr bgcolor=#fff2eb height=25px><td width=30%>Country</td><td>" + Convert.ToString(DrWebInfo["Country"]) + "</td></tr>" +
              "<tr height=25px><td width=30%>Description</td><td>" + Convert.ToString(DrWebInfo["Description"]) + "</td></tr>" +
              "<tr bgcolor=#fff2eb height=25px><td width=30%>In Charge</td><td>" + Convert.ToString(DrWebInfo["EmployeeFullName"]) + "</td></tr>" +
              "<tr height=25px><td width=30%>Phone No</td><td>" + Convert.ToString(DrWebInfo["Phone"]) + "</td></tr>");
            MsbHtml.Append("</table>");
        }
        return MsbHtml.ToString();
    }
    private string GetOpeningStockEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        decimal decTotalAmount = 0;
        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            decTotalAmount += (drowEmail["Quantity"].ToDecimal() * drowEmail["Rate"].ToDecimal());
        }
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%> WareHouse Name </td><td width=70%>" + Convert.ToString(DrWebInfo["WarehouseName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Opening Date</td><td width=70%>" + Convert.ToString(DrWebInfo["OpeningDate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Net Amount</td><td width=70%>" + decTotalAmount.ToString("F"+ClsCommonSettings.Scale) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");


        strColor = "#ffffff";

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Opening Stock Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item Code</b></font></td>" +
                       "<td><font color=#FFFFF><b>Item Name</b></font></td>" +
                       //"<td><font color=#FFFFF><b>Batch No</b></font></td>" +
                       //"<td><font color=#FFFFF><b>Expiry Date</b></font></td>"+
                       "<td><font color=#FFFFF><b>Quantity</b></font></td><td><font color=#FFFFF><b>Rate</b></font></td>" +
                       "<td><font color=#FFFFF><b>Total</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                           //"<td>" + Convert.ToString(drowEmail["BatchNo"]) + "</td>" +
                           //"<td>" + Convert.ToString(drowEmail["ExpiryDate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Quantity"]) + Convert.ToString(drowEmail["UOMName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                           "<td>" + (drowEmail["Quantity"].ToDecimal() * drowEmail["Rate"].ToDecimal()).ToString("F"+ClsCommonSettings.Scale) + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }

    private string GetAccountSettingsEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[1].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[1].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%> Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[0].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Account Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Transaction Type</b></font></td>" +
                       "<td><font color=#FFFFF><b>Group</b></font></td><td><font color=#FFFFF><b>Account</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[0].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["AccountTransactionType"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["AccountGroupName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["AccountName"]) + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }

    private void GetWebformGridHeaderInfo()
    {
        MsbHtml = new StringBuilder();
        MsbHtml.Append("<html><head><style type=text/css> <!-- body,td,th { font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000; }" +
            " h1{ font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#FFF; padding:0px; margin:0px; } " +
            " h2{ font-family:Arial, Helvetica, sans-serif; font-size:13px; color: #000; padding:0 0 0 0px; margin:0px; } " +
            " body { margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; } " +
            " td { padding-left:10px; } --> </style></head><body bgcolor=White topmargin=0 oncontextmenu=return false ondragstart=return false onselectstart=return false><div align=center>");
        MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%><tr><td style=padding:10px;><table border=0 cellspading=0 cellspacing=0 width=100%>" +
                      "<tr bgcolor=#6d6e71><td align=left height=34px colspan=2><h1>" + MstrCaption + "</h1></td></tr><tr><td height=5 colspan=2>&nbsp;</td></tr>");
    }

    private string GetUOMConversionEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("</tr></table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px>" +
                       "<td><font color=#FFFFF><b>UOM Class</b></font></td><td><font color=#FFFFF><b>UOM Base</b></font></td>" +
                       "<td><font color=#FFFFF><b>UOM</b></font></td><td><font color=#FFFFF><b>Conversion Factor</b></font></td><td><font color=#FFFFF><b>Conversion Value</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[0].Rows)
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>" +
                           "<td>" + Convert.ToString(drowEmail["UOMClass"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["UOMBase"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ConversionFactor"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ConversionRate"]) + "</td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetStockAdjustmentEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[1].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[1].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Warehouse </td><td width=70%>" + Convert.ToString(DrWebInfo["Warehouse"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Reason</td><td width=70%>" + Convert.ToString(DrWebInfo["Reason"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Adjustment No</td><td width=70%>" + Convert.ToString(DrWebInfo["AdjustmentNo"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>AdjustmentDate</td><td width=70%>" + Convert.ToString(DrWebInfo["AdjustmentDate"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";

        MsbHtml.Append("<tr height=25px><td colspan=" + EmailSource.Tables[0].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Stock Adjustment Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item Code</b></font></td>" +
            "<td><font color=#FFFFF><b>Item Name</b></font></td>" +
            //"<td><font color=#FFFFF><b>Batch No</b></font></td>" +
            //"<td><font color=#FFFFF><b>Actual Qty</b></font></td>" +
            "<td><font color=#FFFFF><b>Current Qty</b></font></td>" +
            "<td><font color=#FFFFF><b>New Qty</b></font></td>" +

                       "<td><font color=#FFFFF><b>Difference</b></font></td><td><font color=#FFFFF><b>Rate</b></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                           //"<td>" + Convert.ToString(drowEmail["BatchNo"]) + "</td>" +
                          // "<td>" + Convert.ToString(drowEmail["ActualQuantity"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["CurrentQuantity"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["NewQuantity"]) + "</td>" +
                           "<td>" + Math.Abs(Convert.ToDouble(drowEmail["Difference"])).ToString() + "</td>" +
                       "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }
        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }
    private string GetTermsAndConditionsEmail()
    {
        GetWebformHeaderInfo();
        // Heading        
        MsbHtml.Append("</tr></table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            // Master data
            MsbHtml.Append("<tr height=25px><td>Company</td><td>" + Convert.ToString(drowEmail["CompanyName"]) + "</td></tr>"
                + "<tr bgcolor=#fff2eb height=25px><td width=30%>Operation Type</td><td>" + Convert.ToString(drowEmail["OperationTypeDescription"]) + "</td></tr>");
        }
        MsbHtml.Append("</tr></table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Terms And Condition Details</h2></font></td></tr>");

        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Description English</b></font></td>" +
                       "<td><font color=#FFFFF><b>Description Arabic</b></font></td></font></tr>");

        // return MsbHtml.ToString();

        foreach (DataRow drowEmail in EmailSource.Tables[0].Rows)
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["DescriptionEnglish"]) + "</td>" +
                          "<td>" + Convert.ToString(drowEmail["DescriptionArabic"]) + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }


        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetProjectEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%> Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Project Code</td><td width=70%>" + Convert.ToString(DrWebInfo["ProjectCode"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Description</td><td width=70%>" + Convert.ToString(DrWebInfo["ProjectName"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Project Manager</td><td>" + Convert.ToString(DrWebInfo["ProjectManager"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Start Date</td><td>" + Convert.ToString(DrWebInfo["StartDate"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>End Date</td><td>" + Convert.ToString(DrWebInfo["EndDate"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Estimated Amount</td><td>" + Convert.ToString(DrWebInfo["EstimatedAmount"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }

    private string GetPricingSchemeEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
           // DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            //MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>");
            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + "Default Company" + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[0].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Pricing Scheme Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Scheme Name</b></font></td>" +
                       "<td><font color=#FFFFF><b>Short Name</b></font></td><td><font color=#FFFFF><b>Mode</b></font></td> " +
                       "<td><font color=#FFFFF><b>Currency</b></font></td><td><font color=#FFFFF><b>Amount</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[0].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["Description"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["PricingShortName"]) + "</td>" +
                            "<td>" + Convert.ToString(drowEmail["Mode"]) + "</td>" +
                             "<td>" + Convert.ToString(drowEmail["CurrencyName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["PercOrAmtValue"]) + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }
    private string GetPickListEmail()
    {
        GetWebformHeaderInfo();
        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            if (Convert.ToString(EmailSource.Tables[0].Rows[0]["Name"]) == "DebitNote")
            {
                strMainNoName = "Return No";
                strMainNoValue = Convert.ToString(DrWebInfo["MainNo"]);
                strRefNoName = "Invoice No";
                strRefNoValue = Convert.ToString(DrWebInfo["RefernceNo"]);
                strDateName = "Return Date";
                strDateValue = Convert.ToString(DrWebInfo["Date"]);
                strTypeName = "Type";
                strTypeValue = "";
                strWareHouseValue = "";
                strEmployeeValue = Convert.ToString(DrWebInfo["Employee"]);
            }
            else if (Convert.ToString(EmailSource.Tables[0].Rows[0]["Name"]) == "PurchaseInvoice")
            {
                strMainNoName = "Invoice No";
                strMainNoValue = Convert.ToString(DrWebInfo["MainNo"]);
                strRefNoName = "Supplier Bill No";
                strRefNoValue = Convert.ToString(DrWebInfo["RefernceNo"]);
                strDateName = "Invoice Date";
                strDateValue = Convert.ToString(DrWebInfo["Date"]);
                strTypeName = "Invoice Type";
                strTypeValue = Convert.ToString(DrWebInfo["Type"]);
                strWareHouseValue = Convert.ToString(DrWebInfo["Warehouse"]); 
                strEmployeeValue = Convert.ToString(DrWebInfo["Employee"]);
            }
            else if (Convert.ToString(EmailSource.Tables[0].Rows[0]["Name"]) == "GRN")
            {
                strMainNoName = "GRN No";
                strMainNoValue = Convert.ToString(DrWebInfo["MainNo"]);
                strRefNoName = "Reference No";
                strRefNoValue = Convert.ToString(DrWebInfo["RefernceNo"]);
                strDateName = "GRN Date";
                strDateValue = Convert.ToString(DrWebInfo["Date"]);
                strTypeName = "GRN Type";
                strTypeValue = Convert.ToString(DrWebInfo["Type"]);
                strWareHouseValue = Convert.ToString(DrWebInfo["Warehouse"]); 
                strEmployeeValue = Convert.ToString(DrWebInfo["Employee"]);
            }
            else if (Convert.ToString(EmailSource.Tables[0].Rows[0]["Name"]) == "ItemIssue")
            {
                strMainNoName = "Issue No";
                strMainNoValue = Convert.ToString(DrWebInfo["MainNo"]);
                strRefNoName = "Reference No";
                strRefNoValue = Convert.ToString(DrWebInfo["RefernceNo"]);
                strDateName = "Issue Date";
                strDateValue = Convert.ToString(DrWebInfo["Date"]);
                strTypeName = "Operation Type";
                strTypeValue = Convert.ToString(DrWebInfo["Type"]);
                strWareHouseValue = Convert.ToString(DrWebInfo["Warehouse"]);
                strEmployeeValue = Convert.ToString(DrWebInfo["Employee"]);
            }
            else 
            {
                return MsbHtml.ToString();
            }
            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>" + strMainNoName + "</td><td width=70%>" + strMainNoValue + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>" + strRefNoName + "</td><td width=70%>" + strRefNoValue + "</td></tr>" +
                           "<tr height=25px><td width=30%>" + strDateName + "</td><td>" + strDateValue + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>Employee</td><td>" + strEmployeeValue + "</td></tr>" +
                           "<tr height=25px><td width=30%>" + strTypeName + "</td><td>" + strTypeValue + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>Warehouse</td><td>" + strWareHouseValue + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";
        MsbHtml.Append("<tr  height=25px><td colspan=" + EmailSource.Tables[0].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Location Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Item Code</b></font></td>" +
                       "<td width=50%><font color=#FFFFF><b>Item Name</b></font></td> " +
                       "<td><font color=#FFFFF><b>BatchNo</b></font></td>" +
                       "<td><font color=#FFFFF><b>Quantity</b></font></td>" +
                       "<td><font color=#FFFFF><b>Location</b></font></td>" +
                       "<td><font color=#FFFFF><b>Row</b></font></td>" +
                       "<td ><font color=#FFFFF><b>Block</b></font></td>" +
                       "<td><font color=#FFFFF><b>Lot</b></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[0].Rows)
        {
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["BatchNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Quantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Location"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Row"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Block"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Lot"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        return MsbHtml.ToString();
    
    }
    //a

    private string GetPOSEmail()
    {
        GetWebformHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];

            MsbHtml.Append("<tr height=25px><td>Company</td><td>" + Convert.ToString(drowEmail["CompanyName"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>POS No</td><td>" + Convert.ToString(drowEmail["POSNo"]) + "</td></tr>" +
                            "<tr height=25px><td>POS Date</td><td>" + Convert.ToString(drowEmail["POSDate"]) + "</td></tr>" +
                            //"<tr bgcolor=#fff2eb height=25px><td>Due Date</td><td>" + Convert.ToString(drowEmail["DueDate"]) + "</td></tr>" +
                            "<tr height=25px><td>Employee</td><td>" + Convert.ToString(drowEmail["createdby"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>Status</td><td>" + Convert.ToString(drowEmail["Status"]) + "</td></tr>" +
                            "<tr height=25px><td>Warehouse Name</td><td>" + Convert.ToString(drowEmail["WarehouseName"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>Counter</td><td>" + Convert.ToString(drowEmail["CounterName"]) + "</td></tr>" +
                            "<tr height=25px><td>Cancelled By</td><td>" + Convert.ToString(drowEmail["Cancelledby"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>Cancel Date</td><td>" + Convert.ToString(drowEmail["CancelDate"]) + "</td></tr>" +
                            "<tr height=25px><td>Cancel Description</td><td>" + Convert.ToString(drowEmail["CancelDescription"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>Remarks</td><td>" + Convert.ToString(drowEmail["Remarks"]) + "</td></tr>");
        }
        MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Customer Info</h2></font></td></tr>");
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];

            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td>Name</td><td>" + Convert.ToString(drowEmail["vendorName"]) + "</td></tr>" +
                            "<tr height=25px><td>Address</td><td>" + Convert.ToString(drowEmail["AddressName"]) + "</td></tr>" +

                            "<tr bgcolor=#fff2eb height=25px><td width=30%>Contactperson</td><td>" + Convert.ToString(drowEmail["Contactperson"]) + "</td></tr>" +
                            "<tr height=25px><td></td><td>" + Convert.ToString(drowEmail["Address"]) + " , " + Convert.ToString(drowEmail["State"]) + " , " + Convert.ToString(drowEmail["Country"]) + " , " + Convert.ToString(drowEmail["ZipCode"]) + "</td></tr>");

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>POS Item Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Item Code</b></font></td>" +
                       "<td width=50%><font color=#FFFFF><b>Item Name</b></font></td>"+
                       //<td><font color=#FFFFF><b>Batch No</b></font></td>
        "<td><font color=#FFFFF><b>Quantity</b></font></td>" +

                       "<td><font color=#FFFFF><b>Rate</b></font></td>" +
                       "<td><font color=#FFFFF><b>Grand Amount</b></font></td>" +
                       "<td ><font color=#FFFFF><b>Discount</b></font></td>" +
                       "<td><font color=#FFFFF><b>Total</b></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["itemname"]) + "</td>" +
                            //"<td>" + Convert.ToString(drowEmail["BatchNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Quantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["GrandAmount"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["DiscountAmount"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["NetAmount"]) + "</td></tr>");
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Payment Details</h2></font></td></tr>");

         if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];

        MsbHtml.Append("<tr height=25px><td>Sub Total</td><td>" + Convert.ToString(drowEmail["GrandAmount"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>Discount</td><td>" + Convert.ToString(drowEmail["GrandDiscountAmount"]) + "</td></tr>" +
                            "<tr height=25px><td>Total</td><td>" + Convert.ToString(drowEmail["NetAmount"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td><b>Net Amount</td><td><b>" + Convert.ToString(drowEmail["NetAmountRounded"]) + "</td></tr>" +
                            "<tr height=25px><td><b>Amount In Words</td><td><b>" + Convert.ToString(drowEmail["AmountInWords"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>Tender Amount</td><td>" + Convert.ToString(drowEmail["TenderAmount"]) + "</td></tr>" +
                            "<tr height=25px><td>Change</td><td>" + Convert.ToString(drowEmail["Change"]) + "</td></tr>");

                            }
        return MsbHtml.ToString();


    }
    private string GetBankEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            // Master data
            MsbHtml.Append("<tr height=25px><td>Bank Name</td><td>" + Convert.ToString(drowEmail["BankName"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Branch Name</td><td>" + Convert.ToString(drowEmail["BankBranchName"]) + "</td></tr>" +
                           "<tr height=25px><td>Bank Code</td><td>" + Convert.ToString(drowEmail["BankCode"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Address</td><td>" + Convert.ToString(drowEmail["Address"]) + "</td></tr>" +
                           "<tr height=25px><td>Telephone</td><td>" + Convert.ToString(drowEmail["Telephone"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Fax</td><td>" + Convert.ToString(drowEmail["Fax"]) + "</td></tr>" +
                           "<tr height=25px><td>Country</td><td>" + Convert.ToString(drowEmail["Country"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Email</td><td>" + Convert.ToString(drowEmail["Email"]) + "</td></tr>" +
                           "<tr height=25px><td>Website</td><td>" + Convert.ToString(drowEmail["Website"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Bank Routing Code</td><td>" + Convert.ToString(drowEmail["BankRoutingCode"]) + "</td></tr>"
                           );
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }
    private string GetSalaryStructureEmail()
    {
        decimal decAdditionAmount = 0, decDeductionAmount = 0, decRemunerationAmount = 0;
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        // Company Master details
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
            MsbHtml.Append("<tr height=25px><td width=30%> Employee Name </td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeNumber"]) + "-" + Convert.ToString(DrWebInfo["EmployeeFullName"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Currency Name</td><td>" + Convert.ToString(DrWebInfo["CurrencyName"]) + "</td></tr>" +
            "<tr height=25px><td width=30%>Payment Mode</td><td width=70%>" + Convert.ToString(DrWebInfo["PaymentClassification"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Payment Calculation</td><td>" + Convert.ToString(DrWebInfo["PayCalculationType"]) + "</td></tr>" +
            "<tr  height=25px><td width=30%>Salary Process Day</td><td>" + Convert.ToString(DrWebInfo["SalaryDay"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Settlement Policy</td><td>" + Convert.ToString(DrWebInfo["SettlementPolicy"]) + "</td></tr>" +
            "<tr  height=25px><td width=30%>Absent Policy</td><td>" + Convert.ToString(DrWebInfo["AbsentPolicy"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td width=30%>OT Policy</td><td>" + Convert.ToString(DrWebInfo["OTPolicy"]) + "</td></tr>" +
            "<tr  height=25px><td width=30%>Encash Policy</td><td>" + Convert.ToString(DrWebInfo["EncashPolicy"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Holiday Policy</td><td>" + Convert.ToString(DrWebInfo["HolidayPolicy"]) + "</td></tr>" +
            "<tr  height=25px><td width=30%>Remarks</td><td style='word-break:break-all'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");
            strColor = "#ffffff";
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        // salary structure Details
        if (EmailSource.Tables[1].Rows.Count > 0)
        {

            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Addition Deduction Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Addition/Deduction</b></font></td>" +
                           "<td><font color=#FFFFF><b>Deduction Policy</b></font></td></font>" + "<td><font color=#FFFFF><b>Category</b></font></td></font>" + "<td style='text-align:right'><font color=#FFFFF><b>Amount</b></font></td></font></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
            {
                if (drowEmail["IsAddtion"].ToBoolean())
                {
                    decAdditionAmount += drowEmail["Amount"].ToDecimal();
                }
                else
                {
                    decDeductionAmount += drowEmail["Amount"].ToDecimal();
                }

                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td><font color=black><b>" + Convert.ToString(drowEmail["AdditionDeduction"]) + "</b></font></td>" +
                             "<td><font color=black><b>" + Convert.ToString(drowEmail["PolicyName"]) + "</td>" + " <td><font color=black><b>" + Convert.ToString(drowEmail["AmountCategory"]) + "</td>" + "<td style='text-align:right'>" + Convert.ToString(drowEmail["Amount"]) + "</td></tr>");

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td><font color=black><b>Total Additions : " + Convert.ToString(decAdditionAmount) + "</b></font></td>" +
                             "<td><font color=black><b>Total Deductions : " + Convert.ToString(decDeductionAmount) + "</b></font></td>" + "<td style='text-align:right'><font color=black><b>Net Amount  : " + Convert.ToString(decAdditionAmount - decDeductionAmount) + "</b></font></td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        else
        {
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Addition Deduction Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

        }
        MsbHtml.Append("</table>");

        // Other remuneration
        if (EmailSource.Tables[2].Rows.Count > 0)
        {
            MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");
            strColor = "#ffffff";
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Other Remunerations</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Particular</b></font></td>" +
                           "<td style='text-align:right'><font color=#FFFFF><b>Amount</b></font></td></font></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
            {
                decRemunerationAmount += drowEmail["Amount"].ToDecimal();
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td><font color=black><b>" + Convert.ToString(drowEmail["Particular"]) + "</b></font></td>" +
                             "<td style='text-align:right'><font color=black><b>" + Convert.ToString(drowEmail["Amount"]) + "</td></tr>");


                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td style='text-align:right'><font color=black><b>Total :</b></font></td>" +
                             "<td style='text-align:right'><font color=black><b>" + Convert.ToString(decRemunerationAmount) + "</b></font></td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }
    private string GetShiftPolicyEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Shift Name</td><td width=70%>" + Convert.ToString(drowEmail["Shiftname"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>From Time</td><td width=70%>" + Convert.ToString(drowEmail["Fromtime"]) + "</td></tr>" +
                           "<tr height=25px><td width=30%>To Time</td><td width=70%>" + Convert.ToString(drowEmail["Totime"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>Shift Type</td><td width=70%>" + Convert.ToString(drowEmail["Shifttype"]) + "</td></tr>" +
                           "<tr height=25px><td width=30%>Duration</td><td width=70%>" + Convert.ToString(drowEmail["Duration"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>Min WorkingHours</td><td width=70%>" + Convert.ToString(drowEmail["MinWorkingHours"]) + "</td></tr>" +
                           "<tr height=25px><td width=30%>Allowed BreakTime</td><td width=70%>" + Convert.ToString(drowEmail["AllowedBreakTime"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>Late After</td><td width=70%>" + Convert.ToString(drowEmail["LateAfter"]) + "</td></tr>" +
                           "<tr height=25px><td width=30%>Early Before</td><td width=70%>" + Convert.ToString(drowEmail["EarlyBefore"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>Buffer</td><td width=70%>" + Convert.ToString(drowEmail["Buffer"]) + "</td></tr>"
                           );
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }
    private string GetWorkPolicyEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        // Company Master details
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];


            MsbHtml.Append("<tr height=25px><td width=30%> Policy Name </td><td width=70%>" + Convert.ToString(DrWebInfo["PolicyName"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Shift</td><td width=70%>" + Convert.ToString(DrWebInfo["OffDayshift"]) + "</td></tr>" );
        
            strColor = "#ffffff";

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        // Work Policy Details
        if (EmailSource.Tables[1].Rows.Count > 0)
        {

            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Policy Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Working Days</b></font></td>" +
                           "<td><font color=#FFFFF><b>Shift</b></font></td></font></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["DayName"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["ShiftName"]) + "</td></tr>");

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }
        else
        {
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Policy Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

        }
        if (EmailSource.Tables[2].Rows.Count > 0)
        {

            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2> Policy Consequences</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Consequence</b></font></td>" +
                           "<td><font color=#FFFFF><b>LOP</b></font></td></font><td><font color=#FFFFF><b>Casual</b></font></td></font><td><font color=#FFFFF><b>Amount</b></font></td></font></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["Conseq"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["LOP"]) + "</td> "+
                               "<td>" + Convert.ToString(drowEmail["Casual"]) + "</td> "+
                               "<td>" + Convert.ToString(drowEmail["Amount"]) + "</td></tr>");

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }

        else
        {
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>WorkPolicy Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetMaterialIssueEmail()
    {

        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Material Issue No</td><td width=70%>" + Convert.ToString(DrWebInfo["MaterialIssueNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>MaterialIssueDate</td><td width=70%>" + Convert.ToString(DrWebInfo["MaterialIssueDate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Warehouse</td><td>" + Convert.ToString(DrWebInfo["WarehouseName"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>BOM</td><td>" + Convert.ToString(DrWebInfo["ProjectName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Quantity</td><td>" + Convert.ToString(DrWebInfo["Quantity"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>NetAmount</td><td>" + Convert.ToString(DrWebInfo["NetAmount"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Currency</td><td>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Issued By</td><td>" + Convert.ToString(DrWebInfo["IssuedBy"]) + "</td></tr>");               
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "##fff2eb";
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Material Issue Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71  height=25px><td width=20%><font color=#FFFFF><b>Item Code</b></font></td>" +
            "<td width=50%><font color=#FFFFF><b>Item Name</b></font></td>" +
                       "<td width=50%><font color=#FFFFF><b>Batch No</b></font></td>" +
                       "<td><font color=#FFFFF><b>Issued Quantity</b></font></td>" +
                       "<td><font color=#FFFFF><b>Rate</b></font></td>" +
                       "<td><font color=#FFFFF><b>NetAmount</b></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                 "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["BatchNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Quantity"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["NetAmount"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        return MsbHtml.ToString();
    }

    private string GetMaterialReturnEmail()
    {

        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Material Return No</td><td width=70%>" + Convert.ToString(DrWebInfo["MaterialReturnNo"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>MaterialReturnDate</td><td width=70%>" + Convert.ToString(DrWebInfo["MaterialReturnDate"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Warehouse</td><td>" + Convert.ToString(DrWebInfo["WarehouseName"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Project</td><td>" + Convert.ToString(DrWebInfo["ProjectName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Currency</td><td>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>NetAmount</td><td>" + Convert.ToString(DrWebInfo["NetAmount"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";
        MsbHtml.Append("<tr height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Material Return Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Item Code</b></font></td>" +
            "<td width=50%><font color=#FFFFF><b>Item Name</b></font></td>" +
                       "<td width=50%><font color=#FFFFF><b>Batch No</b></font></td>" +
                       "<td><font color=#FFFFF><b>Return Quantity</b></font></td>" +
                       "<td><font color=#FFFFF><b>Rate</b></font></td>" +
                       "<td><font color=#FFFFF><b>NetAmount</b></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                 "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["BatchNo"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Quantity"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["Rate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["NetAmount"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        return MsbHtml.ToString();
    }

    private string GetPayment()
    {
        String htmStr  = "<html><head><style>" +
                       "td {font-family:arial;font-size:12px;border:none;color:black;padding:5px}" +
                       "a.link{font-family:arial;color:Blue;text-decoration: none}" +
                       "a.link:hover{font-family:arial;color: #3399CC;text-decoration: none}" +
                       "</style></head><body bgcolor=White topmargin=0 oncontextmenu=return false ondragstart=return false onselectstart=return false><div align=center>" +
                       "<table border=0 cellspading=0 cellspacing=0 width=100%";

        Boolean blnFlag=false;

        //Try
            String TSQL   = "";
            int intCompanyID  = 0;
          
            SqlDataReader sdrValues;
            MintRecordID = PRecID;
            TSQL = "SELECT CompanyID FROM PayEmployeePayment WHERE PaymentID = " + MintRecordID + "";
            sdrValues = MobjConnection.ExecutesReader(TSQL); 

            if (sdrValues.Read())
            {
                intCompanyID = Convert.ToInt32(sdrValues["CompanyId"]);
            }
            sdrValues.Close();

            mFlag = false;

            ArrayList paramCompany =new ArrayList();
            SqlDataReader sdrCompany;

            paramCompany.Add(new SqlParameter("@ids", intCompanyID));
            sdrCompany = MobjConnection.ExecutesReader("spPayGaReportCompanyForm", paramCompany, CommandBehavior.CloseConnection);

            if (sdrCompany.Read()) 
            {
                String  htmMain = "<table border='0' cellspading='0' cellspacing='0' width='100%'>"+
                                  "<tr>"+
                                      "<td align='center'><font size='2'><b>"+ Convert.ToString(sdrCompany["CompanyName"]) +"</b></font></td>"+
                                  "</tr>"+
                                  "<tr>"+
                                      "<td align='center'> "+ Convert.ToString(sdrCompany["FaxNumber"])  +" </td>"+
                                  "</tr>"+
                                  "<tr>"+
                                      "<td align='center'>"+ Convert.ToString(sdrCompany["RoadArea"]) +"</td>" +
                                  "</tr>"+
                                 " <tr>"+
                                    "  <td align='center'>"+ Convert.ToString(sdrCompany["BlockCity"]) +"</td>" +
                                  "</tr>" +
                             " </table>";
                htmStr += htmMain.ToString();
            }
            sdrCompany.Close();
            sdrCompany = null;

            ArrayList paramSalarySlipMain = new ArrayList(); 
            SqlDataReader sdrSalarySlipMain;
            String strDetailBottom="";
            String strDetailsTable="";

            paramSalarySlipMain.Add(new SqlParameter("@PayID", MintRecordID));
            paramSalarySlipMain.Add(new SqlParameter("@CompanyPayFlag", 0));
            paramSalarySlipMain.Add(new SqlParameter("@DivisionID", 0));
            sdrSalarySlipMain = MobjConnection.ExecutesReader("spPayEmployeePaymentSalarySlipMaster", paramSalarySlipMain, CommandBehavior.CloseConnection);
   
            while (sdrSalarySlipMain.Read() )
            {
                if (blnFlag == false) 
                {
                    String htmHead = "<table border='0' cellspading='0' cellspacing='0' width='100%'>"+ 
                                      "<tr>" +
                                          " <td style='border:solid 1 black' align='center'><font size='3'><b>"+ Convert.ToString(sdrSalarySlipMain["Head"]) +"</b></font></td>"+
                                      "</tr>"+
                                  "</table>";

                    htmStr += htmHead.ToString();

                    String htmMain = "<table border='0' cellspading='0' cellspacing='0' width='100%'>"+
                                      "<tr height='25px'>"+
                                          "<td style='border-left:solid 1 black' width='100px' align='left'>Employee Name  : </td>"+
                                          "<td style='border-right:solid 1 black' colspan='3' align='left'>"+ Convert.ToString(sdrSalarySlipMain["EmployeeName"]) +"</td>"+
                                      "</tr>"+
                                      "<tr height='25px'>"+
                                          "<td style='border-left:solid 1 black' width='100px' align='left'>Employee Code  : </td>"+
                                          "<td style='border-right:solid 1 black' colspan='3' align='left'>"+ Convert.ToString(sdrSalarySlipMain["EmployeeNo"]) +"</td>"+
                                      "</tr>"+
                                      "<tr height='25px'>"+
                                          "<td style='border-left:solid 1 black' width='100px' align='left'>Designation    : </td>"+
                                          "<td align='left'>"+ Convert.ToString(sdrSalarySlipMain["Designation"]) + " </td>" +
                                          "<td width='100px' align='left'>Payment Mode   : </td>"+
                                          "<td style='border-right:solid 1 black' align='left'>"+ Convert.ToString(sdrSalarySlipMain["WorkingMode"]) +"</td>"+
                                      "</tr>"+
                                      "<tr height='25px'>"+
                                          "<td style='border-left:solid 1 black' width='100px' align='left'>Bank Name      : </td>"+
                                          "<td align='left'>"+ Convert.ToString(sdrSalarySlipMain["EmpBank"]) +"</td>"+
                                          "<td width='100px' align='left'>Account No     : </td>"+
                                          "<td style='border-right:solid 1 black' align='left'>"+ (Convert.ToString(sdrSalarySlipMain["AccountNo"]) == "" ? " - " : Convert.ToString(sdrSalarySlipMain["AccountNo"])) +"</td>"+
                                      "</tr>"+
                                  "</table>";
                    htmStr += htmMain.ToString();

                    String strDetailHead = "<table border=0 cellspading=0 cellspacing=0 width=100%" +
                                                      "<tr height=25px>"+
                                                          "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align=left><font size=2><b>Gross Pay</b></font></td>"+
                                                          "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align=right><font size=2><b>Amount( "+ Convert.ToString(sdrSalarySlipMain["Currency"]) + ")</b></font></td> " +
                                                          "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align=left><font size=2><b>Deductions</b></font></td>" +
                                                          "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black;border-right:solid 1 black' align=right><font size=2><b>Amount( " + Convert.ToString(sdrSalarySlipMain["Currency"]) + ")</b></font></td>"+
                                                      "</tr> ";

                    htmStr += strDetailHead.ToString();

                    strDetailBottom = "<tr height=25px>" +
                                              "<td style='border-left:solid 1 black;border-top:solid 1 black' align=left><font size=2><b>Total</b></font></td>" +
                                              "<td style='border-left:solid 1 black;border-top:solid 1 black' align=right><font size=2><b>" + Convert.ToString(sdrSalarySlipMain["IsAddition"]) + "</b></font></td>" + 
                                              "<td style='border-left:solid 1 black;border-top:solid 1 black' align=left><font size=2><b>Total</b></font></td>" +
                                              "<td style='border-left:solid 1 black;border-top:solid 1 black;border-right:solid 1 black' align=right><font size=2><b>" + Convert.ToString(sdrSalarySlipMain["Deduction"]) + "</b></font></td>"+
                                          "</tr> " +
                                          " <tr height=25px><tr height=25px>" +
                                              "<td colspan=3 style='border-left:solid 1 black;border-top:solid 1 black' align=right><font size=2><b>Net Pay</b></font></td>" +
                                              "<td style='border-left:solid 1 black;border-top:solid 1 black;border-right:solid 1 black' align=right><font size=2><b>" + Convert.ToString(sdrSalarySlipMain["NetAmount"]) + "</b></font></td>" +
                                          "</tr>" +
                                          "<tr height=25px><tr height=25px> " +
                                              "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align=left><font size=2><b>Amount in words</b></font></td>" +
                                              "<td colspan=3 style='border-left:solid 1 black;border-top:solid 1 black;border-right:solid 1 black;border-bottom:solid 1 black' align=left><font size=2><b>"+ Convert.ToString(sdrSalarySlipMain["AmountInWords"]) +"</b></font></td>" +
                                          "</tr> </table>";
                    blnFlag = true;
                }

                String strDetailes   = "<tr height=25px>" +
                                          "<td style='border-left:solid 1 black' align=left>"+ (Convert.ToString(sdrSalarySlipMain["AddItem"]).Trim() == "" ? "&nbsp" : Convert.ToString(sdrSalarySlipMain["AddItem"]).Trim()) +"</td>" +
                                          "<td style='border-left:solid 1 black' align=right>"+  (Convert.ToString(sdrSalarySlipMain["AdAmount"]).Trim() == "0" ? "&nbsp" : Convert.ToString(sdrSalarySlipMain["AdAmount"]).Trim()) +"</td>" +
                                          "<td style='border-left:solid 1 black' align=left>"+ (Convert.ToString(sdrSalarySlipMain["DedItem"]).Trim() == "" ? "&nbsp" : Convert.ToString(sdrSalarySlipMain["DedItem"]).Trim()) +"</td>" +
                                          "<td style='border-left:solid 1 black;border-right:solid 1 black' align=right>"+ (Convert.ToString(sdrSalarySlipMain["DedAmount"]).Trim() == "0" ? "&nbsp" : Convert.ToString(sdrSalarySlipMain["DedAmount"]).Trim())  +"</td>" +
                                      "</tr>";
                htmStr += strDetailes;
            }

            htmStr += strDetailBottom.ToString();

            sdrSalarySlipMain.Close();
            sdrSalarySlipMain = null; 

            ArrayList paramWorkInfo = new  ArrayList();
            SqlDataReader sdrWorkInfo;
            Boolean blnStatus=false;

            paramWorkInfo.Add(new SqlParameter("@Type", 1));
            paramWorkInfo.Add(new SqlParameter("@Id", MintRecordID));
            sdrWorkInfo = MobjConnection.ExecutesReader("spPayGetInfoWebformMain", paramWorkInfo, CommandBehavior.CloseConnection);

            if (sdrWorkInfo.Read() )
            {
                sdrWorkInfo.Close();
                String htmWorkInfo = GetWorkInfo(MintRecordID, blnStatus);

                if (blnStatus==true)
                {
                    String htmMain = "<tr><td colspan=4 align=center><font size=3><b>Work Info</b></font></td></tr>" +
                    "<tr><td colspan=8>"+ htmWorkInfo +" </td></tr></table>";
                    htmStr += htmMain.ToString();
                    mFlag = true;
                }
            }
            else
            {
                sdrWorkInfo.Close();
                mFlag = false;
            }
           
            sdrWorkInfo = null;

            return htmStr;
    }

   

     private string  GetWorkInfo(long  intPaymentId, bool blnStatus )
     {
        String htmStr = "";

        Boolean blExists= false;

       int IntScale =0;


            ArrayList paramvehicle = new ArrayList();
            paramvehicle.Add(new SqlParameter("@PaymentID", intPaymentId));

            blnStatus = false;

            SqlDataReader sdV = MobjConnection.ExecutesReader("spPayGetSalarySlipWorkInfo", paramvehicle, CommandBehavior.CloseConnection);

            string htmHead = "<tr><td width='100%' colspan='4'><table border=0 cellspading=0 cellspacing=0 width=100%>" +
            "<tr>" +
                "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2><b>Total Work Days</b></td>"+
                "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2><b>Worked Days</b></td>" +
                "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2><b>Rest Days</b></td> " +
                "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2 ><b>HoliDays</b></td>" +
                "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2><b>Paid Leaves</b></td> " +
                "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2><b>Unpaid Leaves</b></td> " +
                "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2 ><b>Absent Hours</b></td> " +
                "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2 ><b>OT</b></td>" +
                "<td style='border-left:solid 1 black;border-top:solid 1 black;border-right:solid 1 black'  align=center><font size=2 ><b>Shortage</b></td> " +
            "</tr>";
            htmStr += htmHead.ToString();

            while (sdV.Read())
            {
                string htminner = "<tr><td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><"+ Convert.ToString(sdV["TotalWorkDays"]) +"></td>"+
                                   "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><"+ GlobalNumFormat(Convert.ToString(sdV["DaysWorked"]), IntScale) +"></td>"+
                                   "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><"+ GlobalNumFormat(Convert.ToString(sdV["Rest"]), IntScale) +"></td>"+
                                   "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><"+ GlobalNumFormat(Convert.ToString(sdV["Holiday"]), IntScale) +"></td>"+
                                   "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><"+ GlobalNumFormat(Convert.ToString(sdV["Leave"]), IntScale) +"></td>"+
                                   "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><"+ GlobalNumFormat(Convert.ToString(sdV["UnPaidLeave"]), IntScale) +"></td>"+
                                   "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><"+ GlobalNumFormat(Convert.ToString(sdV["Absent"]), IntScale) +"></td>"+
                                   "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><"+ GlobalNumFormat(Convert.ToString(sdV["OT"]), IntScale) +"></td>"+
                                   "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black;border-right:solid 1 black' align='center'><"+ GlobalNumFormat(Convert.ToString(sdV["Shortage"]), IntScale) +"></td>"+
                               "</tr>";
                blnStatus = true;
                htmStr += htminner.ToString();
                blExists = true;
     }
            sdV.Close();
            sdV = null;
            htmStr += "</table></td></tr>";

            return htmStr;
     }



        public string GlobalNumFormat(string StrVal  ,int Scale) 
        {

            string StrDecimal;
            int A ;
            StrDecimal = "0.";


            for( A = 0;Scale - 1>=A;++A)
            {
                StrDecimal = StrDecimal +"0";
            }

            if (StrDecimal == "0.")
            {
                StrDecimal = "0";
            }
            StrVal = Strings.Format(Convert.ToDecimal(StrVal), StrDecimal);

            return StrVal;
        }
    private string GetLeavePolicyEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        // Company Master details
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];


            MsbHtml.Append("<tr height=25px><td width=30%> Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Policy Name</td><td width=70%>" + Convert.ToString(DrWebInfo["LeavePolicyName"]) + "</td></tr>"+
            "<tr height=25px><td width=30%>Applicable From</td><td width=70%>" + Convert.ToString(DrWebInfo["ApplicableFrom"]) + "</td></tr>");

            strColor = "#ffffff";

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        // Work Policy Details
        if (EmailSource.Tables[1].Rows.Count > 0)
        {

            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Policy Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Leave Type</b></font></td>" +
                           "<td><font color=#FFFFF><b>No Of Leaves</b></font></td><td><font color=#FFFFF><b>Month Leaves</b></font></td></font>"+
                           "<td><font color=#FFFFF><b>Carryforwarded Leaves</b></font></td><td><font color=#FFFFF><b>Encashable Days</b></font></td></font></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["lvtype"]) + "</td>" +
                                "<td>" + Convert.ToString(drowEmail["NoOfLeave"]) + "</td> " +
                                "<td>" + Convert.ToString(drowEmail["MonthLeave"]) + "</td> " +
                               "<td>" + Convert.ToString(drowEmail["CarryForwardLeave"]) + "</td> " +
                               "<td>" + Convert.ToString(drowEmail["EncashDays"]) + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }
        else
        {
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Policy Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

        }
        if (EmailSource.Tables[2].Rows.Count > 0)
        {

            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2> Policy Consequences</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Leave Type</b></font></td>" +
                           "<td><font color=#FFFFF><b>Calculation Type</b></font></td></font><td><font color=#FFFFF><b>Calculation Percentage</b></font></td></font><td><font color=#FFFFF><b>Applicable Days</b></font></td></font></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["LeaveType"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["CalculationType"]) + "</td> " +
                               "<td>" + Convert.ToString(drowEmail["CalculationPercentage"]) + "</td> " +
                               "<td>" + Convert.ToString(drowEmail["ApplicableDays"]) + "</td></tr>");

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }

        else
        {
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Policy Consequences</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetDirectGRNEmail()
    {
        GetWebformHeaderInfo();
        DataRow drowGeneralInfo = EmailSource.Tables[0].Rows[0];

        string strAddress = string.Empty;
        strAddress = drowGeneralInfo["Address"].ToString();
        if (!string.IsNullOrEmpty(drowGeneralInfo["State"].ToString()))
            strAddress += "," + drowGeneralInfo["State"].ToString();
        strAddress += "," + drowGeneralInfo["Country"].ToString();
        if (!string.IsNullOrEmpty(drowGeneralInfo["ZipCode"].ToString()))
            strAddress += ",ZipCode-" + drowGeneralInfo["ZipCode"].ToString();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        drowGeneralInfo = EmailSource.Tables[0].Rows[0];
        // Master data
        MsbHtml.Append("<tr height=25px><td>GRN No</td><td>" + Convert.ToString(drowGeneralInfo["GRNNo"]) + "</td></tr>" +
                       "<tr bgcolor=#fff2eb height=25px><td>GRN Date</td><td>" + Convert.ToString(drowGeneralInfo["GRNDate"]) + "</td></tr>" +
                       "<tr bgcolor=#fff2eb height=25px><td>Warehouse</td><td>" + Convert.ToString(drowGeneralInfo["WarehouseName"]) + "</td></tr>" +
                       "<tr height=25px><td>EmployeeName</td><td>" + Convert.ToString(drowGeneralInfo["EmployeeName"]) + "</td></tr>" +
                       "<tr bgcolor=#fff2eb height=25px><td>Status</td><td>" + Convert.ToString(drowGeneralInfo["Status"]) + "</td></tr>" +
                       "<tr height=25px><td>Cancelled by</td><td>" + Convert.ToString(drowGeneralInfo["Cancelledby"]) + "</td></tr>" +
                       "<tr bgcolor=#fff2eb height=25px><td>Cancel Date</td><td>" + Convert.ToString(drowGeneralInfo["canceldate"]) + "</td></tr>" +
                       "<tr height=25px><td>Cancel Description</td><td>" + Convert.ToString(drowGeneralInfo["CancelDescription"]) + "</td></tr>");
       
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");



        if (Convert.ToString(drowGeneralInfo["VendorName"]) != "")
        {
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Vendor Info</h2></font></td></tr>");

            MsbHtml.Append("<tr height=25px><td>Vendor Name</td><td>" + Convert.ToString(drowGeneralInfo["VendorName"]) + "</td></tr>" +
                         "<tr bgcolor=#fff2eb height=25px><td>Vendor Address</td><td>" + Convert.ToString(drowGeneralInfo["AddressName"]) + "</td></tr>" +
                           "<tr height=25px><td>Contact Person</td><td>" + Convert.ToString(drowGeneralInfo["ContactPerson"]) + "</td></tr>" +
                         "<tr  bgcolor=#fff2eb height=25px><td></td><td>" + Convert.ToString(drowGeneralInfo["Address"]) + " , " + Convert.ToString(drowGeneralInfo["State"]) + " , " + Convert.ToString(drowGeneralInfo["Country"]) + " , " + Convert.ToString(drowGeneralInfo["ZipCode"]) + "</td></tr>");
            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        }
        // GRN Details
        strColor = "#ffffff";

        MsbHtml.Append("<tr height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Item Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Item Code</b></font></td>" +
                       "<td><font color=#FFFFF><b>Item Name</b></font></td> " +
                       //"<td><font color=#FFFFF><b>Expiry Date</b></font></td> "+
                       "<td><font color=#FFFFF><b>Received Qty</b></font></td>"+
                       "<td><font color=#FFFFF><b>Extra Qty</b></font></td>" + "<td><font color=#FFFFF><b>Purchase Rate</b></font></td>" + "<td><font color=#FFFFF><b>Net Amount</b></font></td></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ItemCode"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ItemName"]) + "</td>" +
                           //"<td>" + Convert.ToString(drowEmail["ExpiryDate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ReceivedQuantity"]) + Convert.ToString(drowEmail["UOM"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ExtraQuantity"])+ "</td>"+
                           "<td>" + Convert.ToString(drowEmail["PurchaseRate"]) + "</td>"+
                           "<td>" + Convert.ToString(drowEmail["Amount"]) + "</td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        strColor = "#ffffff";
        

        if (Convert.ToString(drowGeneralInfo["Remarks"]) != "")
        {
            MsbHtml.Append("<tr bgcolor=#ffffff height=25px><td colspan=2 align=left><font size=2 color=#fff2eb><h2>Remarks</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td>" + Convert.ToString(drowGeneralInfo["Remarks"]) + "</td></tr>");
            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        }

        if (strColor == "#ffffff") strColor = "#fff2eb"; else strColor = "#ffffff";
        if (strColor == "#fff2eb") sAltColor = "#ffffff"; else sAltColor = "   fff2eb";

        MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#fff2eb><h2>Payment Details</h2></font></td></tr>");
        MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td>Currency</td><td>" + Convert.ToString(drowGeneralInfo["Currency"]) + "</td></tr>" +
                        "<tr height=25px bgcolor=" + sAltColor + "><td>Sub Total</td><td>" + Convert.ToString(drowGeneralInfo["TotalAmount"]) + "</td></tr>" +
                        "<tr height=25px bgcolor=" + strColor + "><td><b>Net Amount</td><td><b>" + Convert.ToString(drowGeneralInfo["TotalAmount"]) + "</td></tr>" +
                        "<tr height=25px bgcolor=" + sAltColor + "><td><b>Amount in Words</td><td><b>" + Convert.ToString(drowGeneralInfo["AmtInWords"]) + "</td></tr>" 
                       );

        MsbHtml.Append("</table></div></body></html>");

        sAltColor = "#ffffff";
        if (strColor == "#fff2eb") sAltColor = "#ffffff"; else sAltColor = "#fff2eb";
        MsbHtml.Append("</table></div></body></html>");
        return MsbHtml.ToString();
    }
}