﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

/// <summary>
/// Created by  : Devi
/// Created on  : 04.11.2010
/// </summary>
class ClsInternet
{
    enum InternetConnectionState
    {
        Modem = 0x1,
        LAN = 0x2,
        Proxy = 0x4,
        RAS = 0x10,
        Offline = 0x20,
        Configured = 0x40
    }

    [DllImport("wininet.dll")]
    private extern static bool InternetGetConnectedState(int sDescription, int iReservedValue);

    public bool IsInternetConnected()
    {
        try
        {
            int iDescription = 0;
            if (InternetGetConnectedState(iDescription, 0))
            {
                if (IsConnectionAvailable())
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }
        catch
        {
            return false;
        }
    }

    private bool IsConnectionAvailable()
    {
        Uri objURL = new Uri("http://www.microsoft.com/");
        System.Net.WebRequest objWebRequest;
        objWebRequest = System.Net.WebRequest.Create(objURL);
        System.Net.WebResponse objWebResponse;

        try
        {
            objWebResponse = objWebRequest.GetResponse();
            objWebResponse.Close();
            objWebRequest = null;
            return true;
        }
        catch
        {
            objWebRequest = null;
            return false;
        }
        return false; //Above code is needed ,Commented to avoid checking during debugging with exceptions on.
    }

    public bool IsUrlAvailable(string sUrl)
    {
        System.Uri Url = new System.Uri(sUrl);
        System.Net.WebRequest WebReq;
        System.Net.WebResponse Resp;
        WebReq = System.Net.WebRequest.Create(Url);
        try
        {
            Resp = WebReq.GetResponse();
            Resp.Close();
            WebReq = null;
            return true;
        }
        catch
        {
            WebReq = null;
            return false;
        }
    }
}