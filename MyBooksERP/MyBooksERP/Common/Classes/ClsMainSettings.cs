﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyBooksERP;

public static class ClsMainSettings
{
    public static bool blnLoginStatus { get; set; }
    public static bool blnProductStatus { get; set; }
    public static bool blnActivationStatus { get; set; }
    public static string strProductSerial { get; set; }
    public static string GLSlno { get { return "999"; } }
    public static int Empcount { get; set; }
    public static int Cmpcount { get; set; }
    public static int Branchcount { get; set; }
    public static int Itmcount { get; set; }
    public static short SaveProcessStatus { get; set; } 

    public static FrmMain objFrmMain { get; set; }

    // For wisdomprinters
    public static bool BOQEnabled { get; set; } // false

    // For HBC Fruits
    public static bool JVAccDuplication { get; set; } // true
    public static bool ExpensePosted { get; set; } // true
    public static bool AutoVoucherNo { get; set; } // true

    // zigma plastic 
    public static bool ProductionEnabled { get; set; } // true
    public static bool QCEnabled { get; set; } // true
    public static bool PayrollEnabled { get; set; } // false

    // Techno Trust
    public static bool ExpenseShownInSalesBill { get; set; } // false

    // Almajadal
    public static bool AlMajdalPrePrintedFormat { get; set; } // true
    public static bool CommercialInvoice { get; set; } // true
}