﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class frmExtraCharges : Form
    {
        clsBLLExtraCharges MobjclsBLLExtraCharges;
        ClsNotificationNew MobjClsNotification;
        ClsCommonUtility MobjCommonUtility = new ClsCommonUtility();

        public int PintFormType = 0;
        public int PintOperationType;
        public long PlngReferenceID = 0;

        private bool MblnAddPermission = false;///To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnPrintEmailPermission = false;
               
        DataTable MdatMessages;
        string MstrCommonMessage;
        MessageBoxIcon MmsgMessageIcon;
        bool blnAddStatus, blnNavigateStatus = false;
        long TotalRecordCnt, CurrentRecCnt;

        public frmExtraCharges()
        {
            InitializeComponent();
            MobjclsBLLExtraCharges = new clsBLLExtraCharges();
            MobjClsNotification = new ClsNotificationNew();
            MobjclsBLLExtraCharges.objDTOExtraCharges.intCompanyID = ClsCommonSettings.CompanyID;
        }        

        private void frmExtraCharges_Load(object sender, EventArgs e)
        {
            try
            {
                SetPermissions();
                LoadMessage();
                LoadCombo(0);

                CboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;

                if (PintOperationType > 0)
                    cboOperationType.SelectedValue = PintOperationType;
                if (PlngReferenceID > 0)
                    cboReferenceNumber.SelectedValue = PlngReferenceID;

                ClearControls();
            }
            catch (Exception ex)
            {
            }
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.ExtraCharges, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void LoadMessage()
        {
            MdatMessages = new DataTable();
            MdatMessages = MobjClsNotification.FillMessageArray((int)FormID.Expense, 4);
        }

        private void LoadCombo(int intType)
        {
            DataTable datCombos = null;           

            if (intType == 0 || intType == 1) // Company
            {
                datCombos = null;
                datCombos = MobjclsBLLExtraCharges.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID });
                CboCompany.ValueMember = "CompanyID";
                CboCompany.DisplayMember = "CompanyName";
                CboCompany.DataSource = datCombos;
                
                MobjclsBLLExtraCharges.objDTOExtraCharges.intCompanyID = ClsCommonSettings.LoginCompanyID;
            }

            if (intType == 0 || intType == 2) // Currency
            {
                datCombos = null;
                datCombos = MobjclsBLLExtraCharges.FillCombos(new string[] { "DISTINCT CR.CurrencyID,CR.CurrencyName", "" +
                    "CurrencyReference CR INNER JOIN CurrencyDetails CD ON CR.CurrencyID = CD.CurrencyID", "" +
                    "CD.CompanyID = " + ClsCommonSettings.LoginCompanyID + "" });                
                cboCurrency.ValueMember = "CurrencyID";
                cboCurrency.DisplayMember = "CurrencyName";
                cboCurrency.DataSource = datCombos;
            }

            if (intType == 0 || intType == 3) // Operation Types
            {
                datCombos = null;

                //if (PintFormType == 0)
                //{
                //    datCombos = MobjclsBLLExtraCharges.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", "" +
                //                "OperationTypeID In ( " + (Int32)OperationType.PurchaseOrder + "," + (Int32)OperationType.PurchaseInvoice + "," +
                //                "" + (Int32)OperationType.SalesOrder + "," + (Int32)OperationType.SalesInvoice + ")" });
                //}
                //else if (PintFormType == 1) // Purchase
                //{
                    datCombos = MobjclsBLLExtraCharges.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", "" +
                                "OperationTypeID In ( " + (Int32)OperationType.PurchaseInvoice + ")" });
                //}
                //else if (PintFormType == 2) // Sale
                //{
                //    datCombos = MobjclsBLLExtraCharges.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", "" +
                //                "OperationTypeID In ( " + (Int32)OperationType.SalesOrder + "," + (Int32)OperationType.SalesInvoice + ")" });
                //}

                cboOperationType.ValueMember = "OperationTypeID";
                cboOperationType.DisplayMember = "OperationType";
                cboOperationType.DataSource = datCombos;

                if (PintOperationType > 0)
                    cboOperationType.SelectedValue = PintOperationType;
                else
                    cboOperationType.SelectedIndex = -1;
            } 

            if (intType == 0 || intType == 4) // Expense Type
            {
                datCombos = null;
                datCombos = MobjclsBLLExtraCharges.FillCombos(new string[] { "AccountID, AccountName", "AccAccountMaster", "dbo.fnGetGroupAccountId(AccountGroupID,1,13)=13" });
                ExpenseTypeID.ValueMember = "AccountID";
                ExpenseTypeID.DisplayMember = "AccountName";
                ExpenseTypeID.DataSource = datCombos;
            }

            if (intType == 0 || intType == 5) // Shipment Vendor
            {
                datCombos = null;
                datCombos = MobjclsBLLExtraCharges.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", "VendorTypeID = " + (int)VendorType.Supplier + "" });
                VendorID.DisplayMember = "VendorName";
                VendorID.ValueMember = "VendorID";
                VendorID.DataSource = datCombos;
            }
        }

        private string getExtraChargeNo()
        {
            string strTemp = "";

            if (CboCompany.SelectedIndex >= 0)
                strTemp = MobjclsBLLExtraCharges.getExtraChargeNo(CboCompany.SelectedValue.ToInt32());

            return strTemp;
        }

        private void ClearControls()
        {
            errExtraCharge.Clear();            
            cboCurrency.SelectedIndex = -1;
            txtExtraChargeNo.Tag = 0;
            txtExtraChargeNo.Text = getExtraChargeNo();
            dtpExtraChargeDate.Value = ClsCommonSettings.GetServerDate();

            if (PintOperationType == 0)
            {
                cboOperationType.SelectedIndex = -1;
                cboOperationType.Enabled = true;
            }
            else
                cboOperationType.Enabled = false;

            if (PlngReferenceID == 0)
            {
                cboReferenceNumber.SelectedIndex = -1;
                cboReferenceNumber.Enabled = true;
            }
            else
                cboReferenceNumber.Enabled = false;
            
            txtDescription.Text = "";
            dgvExtraCharges.Rows.Clear();
            txtTotalAmount.Text = "";
            pnlExpense.Enabled = true;
            CboCompany.Focus();

            GetRecordCountwithValues();

            blnAddStatus = true;
            bnAddNewItem.Enabled = bnCancel.Enabled = MblnAddPermission;
            bnSaveItem.Enabled = bnDeleteItem.Enabled = bnPrint.Enabled = bnEmail.Enabled = false;
            btnSave.Enabled = btnOk.Enabled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void bnCancel_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void CboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboCompany.SelectedIndex >= 0)
            {
                txtExtraChargeNo.Text = getExtraChargeNo();

                txtExtraChargeNo.ReadOnly = false;
                if (MobjclsBLLExtraCharges.CheckExtraChargeAutoGenerate(CboCompany.SelectedValue.ToInt32()).ToUpper() == "YES")
                    txtExtraChargeNo.ReadOnly = true;

                LoadCombo(2); // Load Currency
                cboCurrency.SelectedValue = MobjclsBLLExtraCharges.getCurrency(CboCompany.SelectedValue.ToInt32());
                
                getReferenceNo();
            }
            setEnableDisable();
        }

        private void cboOperationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            getReferenceNo();
            setEnableDisable();
        }

        private void getReferenceNo()
        {
            cboReferenceNumber.DataSource = null;
            if (cboOperationType.SelectedIndex >= 0)
            {
                int intTempMode = 0;
                if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseOrder)
                    intTempMode = 9;
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseInvoice)
                    intTempMode = 10;
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesOrder)
                    intTempMode = 25;
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesInvoice)
                    intTempMode = 26;

                cboReferenceNumber.DataSource = MobjclsBLLExtraCharges.getReferenceNo(intTempMode, CboCompany.SelectedValue.ToInt32());
                cboReferenceNumber.ValueMember = "ReferenceID";
                cboReferenceNumber.DisplayMember = "ReferenceNo";
            }
        }

        private void CboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboCompany.DroppedDown = false;
        }

        private void cboCurrency_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCurrency.DroppedDown = false;
        }

        private void cboOperationType_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboOperationType.DroppedDown = false;
        }

        private void cboReferenceNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboReferenceNumber.DroppedDown = false;
        }

        private void setEnableDisable()
        {
            errExtraCharge.Clear();
            if (blnAddStatus)
            {
                bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnAddPermission;
                bnDeleteItem.Enabled = bnPrint.Enabled = bnEmail.Enabled = false;
            }
            else
            {
                bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                bnDeleteItem.Enabled = MblnDeletePermission;
                bnPrint.Enabled = bnEmail.Enabled = MblnPrintEmailPermission;
            }
        }

        private void GetRecordCountwithValues()
        {
            RecordCount();
            bnCountItem.Text = "of " + (TotalRecordCnt + 1).ToString();
            bnPositionItem.Text = (TotalRecordCnt + 1).ToString();
        }

        private void RecordCount()
        {
            if (PintOperationType > 0 && PlngReferenceID > 0)
                TotalRecordCnt = MobjclsBLLExtraCharges.getRecordCountForSpecificValue(ClsCommonSettings.LoginCompanyID, PintOperationType, PlngReferenceID);
            else
                TotalRecordCnt = MobjclsBLLExtraCharges.getRecordCount();

            CurrentRecCnt = TotalRecordCnt;
            bnCountItem.Text = "of " + TotalRecordCnt.ToString();
            bnPositionItem.Text = TotalRecordCnt.ToString();

            if (TotalRecordCnt == 0)
            {
                bnCountItem.Text = "of 0";
                CurrentRecCnt = 0;
                TotalRecordCnt = 0;
            }
            BindingEnableDisable();
        }

        private void BindingEnableDisable()
        {
            bnMoveFirstItem.Enabled = true;
            bnMovePreviousItem.Enabled = true;
            bnMoveNextItem.Enabled = true;
            bnMoveLastItem.Enabled = true;

            if (TotalRecordCnt == 0)
            {
                bnMoveFirstItem.Enabled = false;
                bnMovePreviousItem.Enabled = false;
                bnMoveNextItem.Enabled = false;
                bnMoveLastItem.Enabled = false;
            }
            else
            {
                if (Convert.ToDouble(bnPositionItem.Text) == Convert.ToDouble(TotalRecordCnt))
                {
                    bnMoveNextItem.Enabled = false;
                    bnMoveLastItem.Enabled = false;
                }
                else if (Convert.ToDouble(bnPositionItem.Text) == 1)
                {
                    bnMoveFirstItem.Enabled = false;
                    bnMovePreviousItem.Enabled = false;
                }
            }
        }

        private void bnMoveFirstItem_Click(object sender, EventArgs e)
        {
            bnPositionItem.Text = "1";
            if (TotalRecordCnt > 1)
                bnCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                bnCountItem.Text = "of 1";
            blnNavigateStatus = true;
            DisplayExtraCharge();
        }

        private void bnMovePreviousItem_Click(object sender, EventArgs e)
        {
            bnPositionItem.Text = (Convert.ToInt32(bnPositionItem.Text) - 1).ToString();
            if (Convert.ToInt32(bnPositionItem.Text) <= 0)
                bnPositionItem.Text = "1";
            if (TotalRecordCnt > 1)
                bnCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                bnCountItem.Text = "of 1";
            blnNavigateStatus = true;
            DisplayExtraCharge();
        }

        private void bnMoveNextItem_Click(object sender, EventArgs e)
        {
            bnPositionItem.Text = (Convert.ToInt32(bnPositionItem.Text) + 1).ToString();
            if (TotalRecordCnt < Convert.ToInt32(bnPositionItem.Text))
            {
                if (TotalRecordCnt > 1)
                    bnPositionItem.Text = TotalRecordCnt.ToString();
                else
                    bnPositionItem.Text = "1";
            }
            if (TotalRecordCnt > 1)
                bnCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                bnCountItem.Text = "of 1";
            blnNavigateStatus = true;
            DisplayExtraCharge();
        }

        private void bnMoveLastItem_Click(object sender, EventArgs e)
        {
            if (TotalRecordCnt > 1)
            {
                bnPositionItem.Text = TotalRecordCnt.ToString();
                bnCountItem.Text = "of " + TotalRecordCnt.ToString();
            }
            else
            {
                bnPositionItem.Text = "1";
                bnCountItem.Text = "of 1";
            }
            blnNavigateStatus = true;
            DisplayExtraCharge();
        }

        private void DisplayExtraCharge()
        {
            errExtraCharge.Clear();
            dgvExtraCharges.Rows.Clear();
            DataTable datTemp = new DataTable();

            if (PintOperationType > 0 && PlngReferenceID > 0)
                datTemp = MobjclsBLLExtraCharges.DisplayExtraChargeMasterForSpecificValue(bnPositionItem.Text.ToInt64(), 
                    ClsCommonSettings.LoginCompanyID, PintOperationType, PlngReferenceID);
            else
                datTemp = MobjclsBLLExtraCharges.DisplayExtraChargeMaster(bnPositionItem.Text.ToInt64());

            blnAddStatus = false;
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    txtExtraChargeNo.Tag = MobjclsBLLExtraCharges.objDTOExtraCharges.lngExtraChargeID;
                    CboCompany.SelectedValue = MobjclsBLLExtraCharges.objDTOExtraCharges.intCompanyID;
                    cboCurrency.SelectedValue = MobjclsBLLExtraCharges.objDTOExtraCharges.intCurrencyID;
                    if (cboCurrency.SelectedValue == null)
                    {
                        DataTable datTemp1 = MobjclsBLLExtraCharges.FillCombos(new string[] { "CurrencyName", "CurrencyReference", "CurrencyID = " + MobjclsBLLExtraCharges.objDTOExtraCharges.intCurrencyID });
                        if (datTemp1.Rows.Count > 0)
                            cboCurrency.Text = datTemp1.Rows[0]["CurrencyName"].ToString();
                    }

                    txtExtraChargeNo.Text = MobjclsBLLExtraCharges.objDTOExtraCharges.strExtraChargeNo;
                    dtpExtraChargeDate.Value = MobjclsBLLExtraCharges.objDTOExtraCharges.dtExtraChargeDate;

                    cboOperationType.SelectedValue = MobjclsBLLExtraCharges.objDTOExtraCharges.intOperationTypeID;
                    if (cboOperationType.SelectedValue == null)
                    {
                        DataTable datTemp1 = MobjclsBLLExtraCharges.FillCombos(new string[] { "OperationType", "OperationTypeReference", "OperationTypeID = " + MobjclsBLLExtraCharges.objDTOExtraCharges.intOperationTypeID });
                        if (datTemp1.Rows.Count > 0)
                            cboOperationType.Text = datTemp1.Rows[0]["OperationType"].ToString();
                    }

                    cboReferenceNumber.SelectedValue = MobjclsBLLExtraCharges.objDTOExtraCharges.lngReferenceID;
                    if (cboReferenceNumber.SelectedValue == null)
                    {
                        DataTable datTemp1 = new DataTable();
                        if (MobjclsBLLExtraCharges.objDTOExtraCharges.intOperationTypeID == (int)OperationType.PurchaseOrder)
                            datTemp1 = MobjclsBLLExtraCharges.FillCombos(new string[] { "PurchaseOrderNo AS ReferenceNo", "InvPurchaseOrderMaster", "PurchaseOrderID = " + MobjclsBLLExtraCharges.objDTOExtraCharges.lngReferenceID });
                        else if (MobjclsBLLExtraCharges.objDTOExtraCharges.intOperationTypeID == (int)OperationType.PurchaseInvoice)
                            datTemp1 = MobjclsBLLExtraCharges.FillCombos(new string[] { "PurchaseInvoiceNo AS ReferenceNo", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + MobjclsBLLExtraCharges.objDTOExtraCharges.lngReferenceID });
                        else if (MobjclsBLLExtraCharges.objDTOExtraCharges.intOperationTypeID == (int)OperationType.SalesOrder)
                            datTemp1 = MobjclsBLLExtraCharges.FillCombos(new string[] { "SalesOrderNo AS ReferenceNo", "InvSalesOrderMaster", "SalesOrderID = " + MobjclsBLLExtraCharges.objDTOExtraCharges.lngReferenceID });
                        else if (MobjclsBLLExtraCharges.objDTOExtraCharges.intOperationTypeID == (int)OperationType.SalesInvoice)
                            datTemp1 = MobjclsBLLExtraCharges.FillCombos(new string[] { "SalesInvoiceNo AS ReferenceNo", "InvSalesInvoiceMaster", "SalesInvoiceID = " + MobjclsBLLExtraCharges.objDTOExtraCharges.lngReferenceID });

                        if (datTemp1.Rows.Count > 0)
                            cboReferenceNumber.Text = datTemp1.Rows[0]["ReferenceNo"].ToString();
                    }

                    txtTotalAmount.Text = MobjclsBLLExtraCharges.objDTOExtraCharges.decTotalAmount.ToString(); 
                    txtDescription.Text = MobjclsBLLExtraCharges.objDTOExtraCharges.strDescription;

                    for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                    {
                        dgvExtraCharges.Rows.Add();
                        dgvExtraCharges.Rows[iCounter].Cells["ExpenseTypeID"].Value = datTemp.Rows[iCounter]["ExpenseTypeID"].ToInt32();
                        dgvExtraCharges.Rows[iCounter].Cells["VendorID"].Value = datTemp.Rows[iCounter]["VendorID"].ToInt32();
                        dgvExtraCharges.Rows[iCounter].Cells["Amount"].Value = datTemp.Rows[iCounter]["Amount"].ToDecimal();
                        dgvExtraCharges.Rows[iCounter].Cells["Description"].Value = datTemp.Rows[iCounter]["Description"].ToString();
                        dgvExtraCharges.Rows[iCounter].Cells["SerialNo"].Value = datTemp.Rows[iCounter]["SerialNo"].ToInt32();
                        dgvExtraCharges.Rows[iCounter].Cells["VoucherID"].Value = datTemp.Rows[iCounter]["VoucherID"].ToDecimal();
                    }
                    dgvExtraCharges.PerformLayout();
                }
            }
            BindingEnableDisable();
            setEnableDisable();

            pnlExpense.Enabled = true;
            if (txtExtraChargeNo.Tag.ToInt64() > 0)
            {
                if (MobjclsBLLExtraCharges.CheckIDExists(txtExtraChargeNo.Tag.ToInt64()))
                    pnlExpense.Enabled = bnDeleteItem.Enabled = false;
            }
            bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false;
        }

        private void dgvExtraCharges_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvExtraCharges.Columns["VendorID"].Index || e.ColumnIndex == dgvExtraCharges.Columns["Amount"].Index)
                    txtTotalAmount.Text = getTotalByExchangeRate();
            }
            setEnableDisable();
        }

        private void dgvExtraCharges_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgvExtraCharges.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private string getTotalByExchangeRate()
        {
            decimal decAmount = 0;
            if (CboCompany.SelectedIndex >= 0)
            {                
                for (int iCounter = 0; iCounter <= dgvExtraCharges.Rows.Count - 1; iCounter++)
                {                    
                    decAmount += dgvExtraCharges.Rows[iCounter].Cells["Amount"].Value.ToDecimal();
                }                
            }
            return decAmount.ToString();
        }

        private void bnSaveItem_Click(object sender, EventArgs e)
        {
           
            if (SaveExtraCharges())
            {
                if (blnAddStatus == true)
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 2, out MmsgMessageIcon);
                else
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 21, out MmsgMessageIcon);

                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();

                bnAddNewItem_Click(sender, e);
            }
        }

        private bool SaveExtraCharges()
        {
            bool blnStatus = false;
            if (Validations())
            {
                if (!CheckStatus())
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, (txtExtraChargeNo.Tag.ToInt64() == 0 ? 1 : 3), out MmsgMessageIcon);
                    if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                        MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        fillParameters();
                        DataTable datTemp = FillDetailParameters();

                        if (MobjclsBLLExtraCharges.SaveExtraCharge(datTemp))
                            blnStatus = true;
                    }
                }
                else
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7286, out MmsgMessageIcon);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                }
            }
            return blnStatus;
        }

        private bool CheckStatus()
        {
            int intReferenceID = cboReferenceNumber.SelectedValue.ToInt32();

            if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseOrder)
                return MobjclsBLLExtraCharges.CheckPurchaseOrderStatus(intReferenceID);
            else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseInvoice)
                return MobjclsBLLExtraCharges.CheckPurchaseInvoiceStatus(intReferenceID);
            else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesOrder)
                return MobjclsBLLExtraCharges.CheckSalesInvoiceStatus(intReferenceID);
            else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesInvoice)
                return MobjclsBLLExtraCharges.CheckSalesOrderStatus(intReferenceID);
            else
                return true;
        }

        private bool Validations()
        {
            Control cntrlFocus = null;
            errExtraCharge.Clear();

            if (CboCompany.SelectedIndex == -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7265, out MmsgMessageIcon);
                cntrlFocus = CboCompany;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);

                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());

                    cntrlFocus.Focus();
                }

                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            if (cboCurrency.SelectedIndex == -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7253, out MmsgMessageIcon);
                cntrlFocus = cboCurrency;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);

                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());

                    cntrlFocus.Focus();
                }

                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            if (cboOperationType.SelectedIndex == -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7251, out MmsgMessageIcon);
                cntrlFocus = cboOperationType;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            if (cboReferenceNumber.SelectedIndex == -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7252, out MmsgMessageIcon);
                cntrlFocus = cboReferenceNumber;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            if (txtExtraChargeNo.Text == string.Empty)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7273, out MmsgMessageIcon);
                cntrlFocus = txtExtraChargeNo;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }
            else
            {
                if (MobjclsBLLExtraCharges.CheckExtraChargeDuplication(CboCompany.SelectedValue.ToInt32(), txtExtraChargeNo.Tag.ToInt64(), txtExtraChargeNo.Text))
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7275, out MmsgMessageIcon);
                    cntrlFocus = txtExtraChargeNo;
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    if (cntrlFocus != null)
                    {
                        if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                            errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                        cntrlFocus.Focus();
                    }
                    lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                    return false;
                }
            }

            if (Convert.ToDateTime(dtpExtraChargeDate.Value.ToString("dd MMM yyyy")) > ClsCommonSettings.GetServerDate())
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7282, out MmsgMessageIcon);
                cntrlFocus = dtpExtraChargeDate;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            DataTable datTemp = new DataTable();
            if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseOrder)
                datTemp = MobjclsBLLExtraCharges.FillCombos(new string[] { "CONVERT(VARCHAR,OrderDate,106) AS RefDate", "InvPurchaseOrderMaster", "PurchaseOrderID = " + cboReferenceNumber.SelectedValue.ToInt64() + "" });
            else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseInvoice)
                datTemp = MobjclsBLLExtraCharges.FillCombos(new string[] { "CONVERT(VARCHAR,InvoiceDate,106) AS RefDate", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + cboReferenceNumber.SelectedValue.ToInt64() + "" });
            else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesOrder)
                datTemp = MobjclsBLLExtraCharges.FillCombos(new string[] { "CONVERT(VARCHAR,OrderDate,106) AS RefDate", "InvSalesOrderMaster", "SalesOrderID = " + cboReferenceNumber.SelectedValue.ToInt64() + "" });
            else if  (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesInvoice)
                datTemp = MobjclsBLLExtraCharges.FillCombos(new string[] { "CONVERT(VARCHAR,InvoiceDate,106) AS RefDate", "InvSalesInvoiceMaster", "SalesInvoiceID = " + cboReferenceNumber.SelectedValue.ToInt64() + "" });

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    if (Convert.ToDateTime(dtpExtraChargeDate.Value.ToString("dd MMM yyyy")) < Convert.ToDateTime((datTemp.Rows[0]["RefDate"].ToDateTime()).ToString("dd MMM yyyy")))
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7284, out MmsgMessageIcon);
                        cntrlFocus = dtpExtraChargeDate;
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        if (cntrlFocus != null)
                        {
                            if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                                errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                            cntrlFocus.Focus();
                        }
                        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                        return false;
                    }
                }
            }    
            
            dgvExtraCharges.CommitEdit(DataGridViewDataErrorContexts.Commit);

            if (dgvExtraCharges.Rows.Count == 1)
            {
                cntrlFocus = null;
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7274, out MmsgMessageIcon);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            foreach (DataGridViewRow dgvrow in dgvExtraCharges.Rows)
            {
                cntrlFocus = null;

                if (dgvrow.Cells[ExpenseTypeID.Index].Value.ToInt32() == 0 &&
                    (dgvrow.Cells[VendorID.Index].Value.ToInt32() > 0 || dgvrow.Cells[Amount.Index].Value.ToInt32() > 0))
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7269, out MmsgMessageIcon);
                    dgvExtraCharges.CurrentCell = dgvrow.Cells[ExpenseTypeID.Index];
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    if (cntrlFocus != null)
                    {
                        if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                            errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                        cntrlFocus.Focus();
                    }
                    lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                    return false;
                }

                if (dgvrow.Cells[ExpenseTypeID.Index].Value.ToInt32() > 0)
                {
                    if (dgvrow.Cells[VendorID.Index].Value.ToInt32() == 0)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7272, out MmsgMessageIcon);
                        dgvExtraCharges.CurrentCell = dgvrow.Cells[VendorID.Index];
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        if (cntrlFocus != null)
                        {
                            if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                                errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                            cntrlFocus.Focus();
                        }
                        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                        return false;
                    }

                    else if (dgvrow.Cells[Amount.Index].Value.ToDecimal() == 0)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7257, out MmsgMessageIcon);
                        dgvExtraCharges.CurrentCell = dgvrow.Cells[Amount.Index];
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        if (cntrlFocus != null)
                        {
                            if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                                errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                            cntrlFocus.Focus();
                        }
                        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                        return false;
                    }
                }                
            }

            for (int iCounter = 0; iCounter <= dgvExtraCharges.Rows.Count - 2; iCounter++)
            {
                int intExpTypeID = dgvExtraCharges.Rows[iCounter].Cells["ExpenseTypeID"].Value.ToInt32();
                int intVenID = dgvExtraCharges.Rows[iCounter].Cells["VendorID"].Value.ToInt32();

                for (int jCounter = 0; jCounter <= dgvExtraCharges.Rows.Count - 1; jCounter++)
                {
                    if (jCounter != iCounter)
                    {
                        if (intExpTypeID == dgvExtraCharges.Rows[jCounter].Cells["ExpenseTypeID"].Value.ToInt32() &&
                            intVenID == dgvExtraCharges.Rows[jCounter].Cells["VendorID"].Value.ToInt32())
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7269, out MmsgMessageIcon);
                            dgvExtraCharges.CurrentCell = dgvExtraCharges.Rows[jCounter].Cells[VendorID.Index];
                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                            if (cntrlFocus != null)
                            {
                                if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                                    errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                                cntrlFocus.Focus();
                            }
                            lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                            return false;
                        }
                    }
                }                
            }

            //--------Accounts Validation----------//
            if (cboOperationType.SelectedValue.ToInt32() > 0)
            {
                int intOperationType = cboOperationType.SelectedValue.ToInt32();
                int intCompanyID = CboCompany.SelectedValue.ToInt32();

                //if (intOperationType == (int)OperationType.PurchaseOrder)
                //{
                //    if (!MobjclsBLLExtraCharges.GetCompanyAccount(intCompanyID, (int)TransactionTypes.PurchaseOrder))
                //    {
                //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7287, out MmsgMessageIcon);
                //        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                //        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                //        return false;
                //    }
                //}
                //else if (intOperationType == (int)OperationType.PurchaseInvoice)
                //{
                //    if (!MobjclsBLLExtraCharges.GetCompanyAccount(intCompanyID, (int)TransactionTypes.PurchaseInvoice))
                //    {
                //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7288, out MmsgMessageIcon);
                //        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                //        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                //        return false;
                //    }
                //}
                //else if (intOperationType == (int)OperationType.SalesOrder)
                //{
                //    if (!MobjclsBLLExtraCharges.GetCompanyAccount(intCompanyID, (int)TransactionTypes.SalesOrder))
                //    {
                //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7293, out MmsgMessageIcon);
                //        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                //        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                //        return false;
                //    }
                //}
                //else if (intOperationType == (int)OperationType.SalesInvoice)
                //{
                //    if (!MobjclsBLLExtraCharges.GetCompanyAccount(intCompanyID, (int)TransactionTypes.SalesInvoice))
                //    {
                //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7293, out MmsgMessageIcon);
                //        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                //        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                //        return false;
                //    }
                //}
            }

            return true;
        }

        private void fillParameters()
        {
            MobjclsBLLExtraCharges.objDTOExtraCharges.lngExtraChargeID = txtExtraChargeNo.Tag.ToInt64();
            MobjclsBLLExtraCharges.objDTOExtraCharges.strExtraChargeNo = txtExtraChargeNo.Text;
            MobjclsBLLExtraCharges.objDTOExtraCharges.dtExtraChargeDate = dtpExtraChargeDate.Value.ToString("dd MMM yyyy").ToDateTime();
            MobjclsBLLExtraCharges.objDTOExtraCharges.intCompanyID = CboCompany.SelectedValue.ToInt32();
            MobjclsBLLExtraCharges.objDTOExtraCharges.intCurrencyID = cboCurrency.SelectedValue.ToInt32();
            MobjclsBLLExtraCharges.objDTOExtraCharges.intOperationTypeID = cboOperationType.SelectedValue.ToInt32();
            MobjclsBLLExtraCharges.objDTOExtraCharges.lngReferenceID = cboReferenceNumber.SelectedValue.ToInt64();
            MobjclsBLLExtraCharges.objDTOExtraCharges.strDescription = txtDescription.Text;
            MobjclsBLLExtraCharges.objDTOExtraCharges.decTotalAmount = txtTotalAmount.Text.ToDecimal();
        }

        private DataTable FillDetailParameters()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("ExpenseTypeID");
            datTemp.Columns.Add("VendorID");
            datTemp.Columns.Add("Amount");
            datTemp.Columns.Add("Description");
            datTemp.Columns.Add("VoucherID");
            for (int iCounter = 0; iCounter <= dgvExtraCharges.Rows.Count - 2; iCounter++)
            {
                datTemp.Rows.Add();
                datTemp.Rows[iCounter]["ExpenseTypeID"] = dgvExtraCharges.Rows[iCounter].Cells["ExpenseTypeID"].Value;
                datTemp.Rows[iCounter]["VendorID"] = dgvExtraCharges.Rows[iCounter].Cells["VendorID"].Value;
                datTemp.Rows[iCounter]["Amount"] = dgvExtraCharges.Rows[iCounter].Cells["Amount"].Value;
                datTemp.Rows[iCounter]["Description"] = dgvExtraCharges.Rows[iCounter].Cells["Description"].Value;
                datTemp.Rows[iCounter]["VoucherID"] = dgvExtraCharges.Rows[iCounter].Cells["VoucherID"].Value;
            }

            return datTemp;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bnSaveItem_Click(sender, e);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveExtraCharges())
            {
                if (blnAddStatus == true)
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 2, out MmsgMessageIcon);
                else
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 21, out MmsgMessageIcon);

                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);

                this.Close();
            }
        }

        private void cboReferenceNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            setEnableDisable();
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            setEnableDisable();
        }

        private void txtExtraChargeNo_TextChanged(object sender, EventArgs e)
        {
            setEnableDisable();
        }

        private void dtpExtraChargeDate_ValueChanged(object sender, EventArgs e)
        {
            setEnableDisable();
        }

        private void txtTotalAmount_TextChanged(object sender, EventArgs e)
        {
            if (cboCurrency.SelectedIndex >= 0)
            {
                if (txtTotalAmount.Text.ToDecimal() > 0)
                    lblAmountInWords.Text = "Amount In Words " + MobjclsBLLExtraCharges.ConvertToWord(txtTotalAmount.Text, cboCurrency.SelectedValue.ToInt32());
                else
                    lblAmountInWords.Text = "Amount In Words";
            }
            else
                lblAmountInWords.Text = "Amount In Words";
            setEnableDisable();
        }

        private void txtDescription_TextChanged(object sender, EventArgs e)
        {
            setEnableDisable();
        }

        private void bnDeleteItem_Click(object sender, EventArgs e)
        {
            if (txtExtraChargeNo.Tag.ToInt64() > 0)
            {
                if (!MobjclsBLLExtraCharges.CheckPaymentExist(txtExtraChargeNo.Tag.ToInt32()))
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 13, out MmsgMessageIcon);
                    if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                        MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        if (DeleteExtraCharge())
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 4, out MmsgMessageIcon);
                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                            lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();

                            bnAddNewItem_Click(sender, e);
                        }
                    }
                }
                else
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7285, out MmsgMessageIcon);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                }
            }
        }

        private bool DeleteExtraCharge()
        {
            bool blnStatus = false;
            MobjclsBLLExtraCharges.objDTOExtraCharges.lngExtraChargeID = txtExtraChargeNo.Tag.ToInt64();

            if (MobjclsBLLExtraCharges.DeleteExtraCharge())
                blnStatus = true;

            return blnStatus;
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {
            if (txtExtraChargeNo.Tag.ToInt64() > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = txtExtraChargeNo.Tag.ToInt64();
                ObjViewer.PiFormID = (int)FormID.ExtraCharges;
                ObjViewer.ShowDialog();
            }
        }

        private void bnEmail_Click(object sender, EventArgs e)
        {
            if (txtExtraChargeNo.Tag.ToInt64() > 0)
            {
                using (FrmEmailPopup objEmailPopUp = new FrmEmailPopup())
                {
                    objEmailPopUp.MsSubject = "Extra Charges Information";
                    objEmailPopUp.EmailFormType = EmailFormID.ExtraCharges;
                    objEmailPopUp.EmailSource = MobjclsBLLExtraCharges.PrintExtraCharges(txtExtraChargeNo.Tag.ToInt64());
                    objEmailPopUp.ShowDialog();
                }
            }
        }

        private void btnExpenseTypeRef_Click(object sender, EventArgs e)
        {
            try
            {
                // Calling Reference form for Expense Type
                FrmChartOfAccounts objFrmChartOfAccounts = new FrmChartOfAccounts();
                objFrmChartOfAccounts.ShowDialog();

                LoadCombo(4);
            }
            catch { }
        }

        private void btnExtraChargePayment_Click(object sender, EventArgs e)
        {
            if (txtExtraChargeNo.Tag.ToInt64() > 0)
            {
                using (frmExtraChargePayment objfrmExtraChargePayment = new frmExtraChargePayment())
                {
                    //objfrmExtraChargePayment.PintFormType
                    objfrmExtraChargePayment.PintOperationType = cboOperationType.SelectedValue.ToInt32();
                    objfrmExtraChargePayment.PlngReferenceID = cboReferenceNumber.SelectedValue.ToInt64();
                    objfrmExtraChargePayment.PlngExtraChargeID = txtExtraChargeNo.Tag.ToInt64();
                    objfrmExtraChargePayment.ShowDialog();
                }
            }
        }

        private void dgvExtraCharges_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            this.dgvExtraCharges.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvExtraCharges.CurrentCell.OwningColumn.Name == "Amount")
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (dgvExtraCharges.EditingControl != null && !string.IsNullOrEmpty(dgvExtraCharges.EditingControl.Text) &&
                    dgvExtraCharges.EditingControl.Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
        }

        private void dgvExtraCharges_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void dgvExtraCharges_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            //MobjCommonUtility.SetSerialNo(dgvExtraCharges, e.RowIndex, false);
        }

        private void dgvExtraCharges_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            //MobjCommonUtility.SetSerialNo(dgvExtraCharges, e.RowIndex, false);
            txtTotalAmount.Text = getTotalByExchangeRate();
        }

        private void dgvExtraCharges_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (txtExtraChargeNo.Tag.ToInt64() > 0)
            {
                if (!MobjclsBLLExtraCharges.CheckPaymentExist(txtExtraChargeNo.Tag.ToInt32()))
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 13, out MmsgMessageIcon);
                    if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                        MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        MobjclsBLLExtraCharges.objDTOExtraCharges.lngExtraChargeID = txtExtraChargeNo.Tag.ToInt64();

                        if (MobjclsBLLExtraCharges.DeleteExtraChargeDetails(true, dgvExtraCharges.CurrentRow.Cells["SerialNo"].Value.ToInt32()))
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 4, out MmsgMessageIcon);
                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                            lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                        }
                    }
                }
                else
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7285, out MmsgMessageIcon);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                }
            }
        }
    }
}