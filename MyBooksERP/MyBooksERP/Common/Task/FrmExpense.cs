﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;


using System.Text;
using System.Net.Mail;

namespace MyBooksERP
{
    public partial class frmExpense : DevComponents.DotNetBar.Office2007Form
    {
        #region Private Variables
        public int PintCompanyID=0;
        public long PintOperationID;
        public int PintOperationType;
        public decimal PdecTotalExpenseAmount;
        public string PstrOperationNumber;
        public bool PBlnIsEditable { get; set; }
        public bool PblnPostToAccount = true;
        private clsBLLCommonUtility MObjClsBllCommonUtility;
        
        bool MblnEditable = true, MblnShowErrorMess = false, MblnEditMode = false;  // Checking whether error messages are showing or not

        private bool MblnViewPermission = false;//To Set View Permission
        private bool MblnAddPermission = false;///To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnPrintEmailPermission = false;//To set Print/Email Permission
        private bool MblnAddUpdatePermission = false;
        private int MintCurrencyScale = 0;

        int MintRecordCount, MintRecordPosition;

        string MstrCommonMessage;

        DataTable MdatMessages;

        MessageBoxIcon MmsgMessageIcon;

        clsBLLExpenseMaster MobjClsBLLExpenseMaster;
        ClsNotificationNew MobjClsNotification;
        ClsLogWriter MobjClsLogWriter;

        public DataTable PDtExpenseDetails { get; set; }
        private bool MblnCalledDirect = false;

        clsBLLEmailPopUp MobjclsBLLEmailPopUp;

        DataTable datDetails = new DataTable();
        bool blnCheckStatus = false;

        public int intModuleID = 0; // 1 - Purchase 2 - Sales 3 - Inventory

        #endregion

        public frmExpense(bool blnCalledDirect)
        {
            InitializeComponent();

            PBlnIsEditable = true;
            MobjClsBLLExpenseMaster = new clsBLLExpenseMaster();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotificationNew();
            MobjClsBLLExpenseMaster.objDTOExpenseMaster.intCompanyID = ClsCommonSettings.CompanyID;
            MObjClsBllCommonUtility = new clsBLLCommonUtility();
            tmExpense.Interval = ClsCommonSettings.TimerInterval;
            MblnShowErrorMess = true;
            MblnCalledDirect = blnCalledDirect;
            MobjclsBLLEmailPopUp = new clsBLLEmailPopUp();
        }

        private void FormLoad(object sender, EventArgs e)
        {
            this.MblnEditMode = false;
            SetPermissions();
            LoadMessage();
            LoadCombo(0);
            ResetForm();

            if (!MblnCalledDirect)
            {
                bnAddNewItem.Visible = false;
                this.Text = "Expenses";
                this.Icon = ((System.Drawing.Icon)(Properties.Resources.Expense12));
            }
            else
            {
                bnAddNewItem.Visible = true;
                this.Text = "Extra Charges";
                this.Icon = ((System.Drawing.Icon)(Properties.Resources.Extra_Charges1));
            }

            //if (PDtExpenseDetails != null && PDtExpenseDetails.Rows.Count > 0)
            //{
            //    for (int i = 0; i < PDtExpenseDetails.Rows.Count; i++)
            //    {
            //        dgvExpense.Rows.Add();
            //        dgvExpense[dgvColExpenseType.Name, i].Value = Convert.ToInt32(PDtExpenseDetails.Rows[i]["ExpenseTypeID"]);
            //        dgvExpense[dgvColQuantity.Name, i].Value = Convert.ToDecimal(PDtExpenseDetails.Rows[i]["Quantity"]);
            //        dgvExpense[dgvColAmount.Name, i].Value = Convert.ToDecimal(PDtExpenseDetails.Rows[i]["Amount"]);
            //        if (PDtExpenseDetails.Rows[i]["Description"] != DBNull.Value)
            //            dgvExpense[dgvColDescription.Name, i].Value = Convert.ToString(PDtExpenseDetails.Rows[i]["Description"]);
            //    }
            //    //txtTotalAmount.Text = Convert.ToString(PDtExpenseDetails.Rows[0]["TotalAmount"]);
            //    PDtExpenseDetails = null;
            //    AmountValueChanged(sender, new DataGridViewCellEventArgs(2, 0));
            //}
        }

        /// <summary>
        /// Loading the variables for messages
        /// </summary>
        private void LoadMessage()
        {
            MdatMessages = new DataTable();
            MdatMessages = MobjClsNotification.FillMessageArray((int)FormID.Expense, ClsCommonSettings.ProductID);
        }

        private bool LoadCombo(int intType)
        {
            try
            {
                DataTable datCombos = null;
                
                if (intType == 0 || intType == 1) // Operation Types
                {
                    if (!MblnCalledDirect || MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngExpenseID != 0)
                    {
                        datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", "OperationTypeID In (" + (Int32)OperationType.PurchaseInvoice + "," + (Int32)OperationType.PurchaseQuotation + "," + (Int32)OperationType.PurchaseOrder + "," + (Int32)OperationType.SalesQuotation + "," + (Int32)OperationType.SalesOrder + "," + (Int32)OperationType.SalesInvoice + "," + (int)OperationType.StockTransfer + ")" });
                    }
                    else
                    {
                       // if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                       // {
                        if(intModuleID == 1)
                            datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", "OperationTypeID In ( " + (Int32)OperationType.PurchaseInvoice + "," + (Int32)OperationType.PurchaseOrder + ")" });
                        else if(intModuleID == 2)
                            datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", "OperationTypeID In ( " + (Int32)OperationType.SalesInvoice +")" });
                        else if(intModuleID == 3)
                            datCombos = datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", "OperationTypeID In ( " + (Int32)OperationType.StockTransfer + ")" });
                      //  }
                        //else
                        //{
                        //    DataTable datTemp = MobjClsBLLExpenseMaster.GetOperationTypeByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.ExpType);                            
                        //    string strTemp = "";
                        //    if (datTemp != null)
                        //    {
                        //        datTemp.DefaultView.RowFilter = "OrderTypeID='" + Convert.ToString((int)OperationOrderType.ECOTPurchaseInvoice) + "'";
                        //        if (datTemp.DefaultView.ToTable().Rows.Count > 0)
                        //            strTemp += (int)OperationType.PurchaseInvoice + ",";
                        //        datTemp.DefaultView.RowFilter = "OrderTypeID='" + Convert.ToString((int)OperationOrderType.ECOTSalesInvoice) + "'";
                        //        if (datTemp.DefaultView.ToTable().Rows.Count > 0)
                        //            strTemp += (int)OperationType.SalesInvoice + ",";
                        //        datTemp.DefaultView.RowFilter = "OrderTypeID='" + Convert.ToString((int)OperationOrderType.ECOTPurchaseOrder) + "'";
                        //        if (datTemp.DefaultView.ToTable().Rows.Count > 0)
                        //            strTemp += (int)OperationType.PurchaseOrder + ",";
                        //        datTemp.DefaultView.RowFilter = "OrderTypeID='" + Convert.ToString((int)OperationOrderType.ECOTStockTransfer) + "'";
                        //        if (datTemp.DefaultView.ToTable().Rows.Count > 0)
                        //            strTemp += (int)OperationType.StockTransfer + ",";
                        //        if (strTemp != "")
                        //            strTemp = strTemp.Remove(strTemp.Length - 1);
                        //    }
                        //    if (strTemp != "")
                        //        try
                        //        {
                        //            datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "OperationTypeID,Description", "OperationTypeReference", "OperationTypeID In (" + strTemp + ")" });
                        //        }
                        //        catch { }
                        //}
                    }
                    cboOperationType.DisplayMember = "OperationType";
                    cboOperationType.ValueMember = "OperationTypeID";
                    cboOperationType.DataSource = datCombos;

                    if (PintOperationType != 0)
                    {
                        cboOperationType.SelectedValue = PintOperationType;
                        if (cboOperationType.SelectedValue == null)
                        {
                            DataTable datTemp = MobjClsBLLExpenseMaster.FillCombos(new string[] { "*", "OperationTypeReference", "OperationTypeID = " + PintOperationType });
                            if (datTemp.Rows.Count > 0)
                                cboOperationType.Text = datTemp.Rows[0]["OperationType"].ToString();
                        }
                    }
                    else
                        cboOperationType.SelectedIndex = -1;
                }

                if (intType == 0 || intType == 2) // Expense
                {
                    datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "AccountID, AccountName", "AccAccountMaster", "dbo.fnGetGroupAccountId(AccountGroupID,1,0)=4 and AccountID not in (9,10)" });
                    dgvColExpenseType.DisplayMember = "AccountName";
                    dgvColExpenseType.ValueMember = "AccountID";
                    dgvColExpenseType.DataSource = datCombos;
                }

                //if (intType == 0 || intType == 3) // Credit Head
                //{
                //    datCombos = MobjClsBLLExpenseMaster.GetAccounts();
                //    cboCreditHead.DisplayMember = "Account";
                //    cboCreditHead.ValueMember = "AccountID";
                //    cboCreditHead.DataSource = datCombos;
                //}

                //if (intType == 0 || intType == 4) // Debit Head
                //{
                //    datCombos = MobjClsBLLExpenseMaster.GetAccounts();
                //    cboDebitHead.DisplayMember = "Account";
                //    cboDebitHead.ValueMember = "AccountID";
                //    cboDebitHead.DataSource = datCombos;
                //}

                if (intType == 0 || intType == 5)
                {
                    datCombos = null;
                 //   if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2 || PintCompanyID !=0)
                  //  {
                        datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                    //}
                    //else
                    //{
                    //    try
                    //    {
                    //        datCombos = MobjClsBLLExpenseMaster.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.ExpCompany);
                    //    }
                    //    catch { }
                    //}
                    CboCompany.ValueMember = "CompanyID";
                    CboCompany.DisplayMember = "CompanyName";
                    CboCompany.DataSource = datCombos;
                    if (PintCompanyID != 0)
                    {
                        CboCompany.SelectedValue = PintCompanyID;
                        if (CboCompany.SelectedValue == null)
                        {
                            DataTable datTemp = MobjClsBLLExpenseMaster.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + PintCompanyID });
                            if (datTemp.Rows.Count > 0)
                                CboCompany.Text = datTemp.Rows[0]["CompanyName"].ToString();
                        }
                    }
                    else
                    {
                        CboCompany.SelectedValue = ClsCommonSettings.CompanyID;
                        if (CboCompany.SelectedValue == null)
                        {
                            DataTable datTemp = MobjClsBLLExpenseMaster.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + ClsCommonSettings.CompanyID });
                            if (datTemp.Rows.Count > 0)
                                CboCompany.Text = datTemp.Rows[0]["CompanyName"].ToString();
                        }
                    }
                    try
                    {
                        MobjClsBLLExpenseMaster.objDTOExpenseMaster.intCompanyID = (Int32)CboCompany.SelectedValue;
                    }
                    catch { }
                }

                if (intType == 0 || intType == 6)
                {
                    datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "CurrencyID,CurrencyName", "CurrencyReference", "" });
                    cboCurrency.DisplayMember = "CurrencyName";
                    cboCurrency.ValueMember = "CurrencyID";
                    cboCurrency.DataSource = datCombos;
                }

                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            //if (MblnCalledDirect)
            //{
                clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
                if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.ExtraCharges, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                    if (MblnAddPermission == true || MblnUpdatePermission == true)
                        MblnAddUpdatePermission = true;

                    if (MblnAddPermission == true || MblnUpdatePermission == true || MblnDeletePermission == true || MblnPrintEmailPermission == true)
                        MblnViewPermission = true;
                }
                else
                    MblnAddPermission = MblnViewPermission = MblnUpdatePermission = MblnDeletePermission = MblnPrintEmailPermission=true;
            //}
            //else
            //    MblnAddPermission = MblnViewPermission = MblnUpdatePermission = MblnDeletePermission = true;
            //bnAddNewItem.Enabled = MblnAddPermission;
            //bnDeleteItem.Enabled = MblnDeletePermission;
            //bnSaveItem.Enabled = btnOk.Enabled = btnSave.Enabled = MblnAddUpdatePermission;
            //bnPrint.Enabled = bnEmail.Enabled = MblnPrintEmailPermission;
        }

        private void ExpenseTypeRef(object sender, EventArgs e)
        {
            try
            {
                // Calling Reference form for Expense Type
                FrmChartOfAccounts objFrmChartOfAccounts = new FrmChartOfAccounts();
                objFrmChartOfAccounts.ShowDialog();

                LoadCombo(2);
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnCategory_Click() " + Ex.Message.ToString());
            }
        }

        private void ResetForm()
        {
            try
            {
                this.MblnEditMode = false;
                MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngExpenseID = 0;
                cboCurrency.SelectedIndex = -1;
                txtDescription.Clear();
                txtTotalAmount.Clear();
                cboReferenceNumber.Tag = "0";

                cboOperationType.SelectedIndex = -1;
                cboReferenceNumber.DataSource = null;
                cboReferenceNumber.SelectedIndex = -1;
                cboCreditHead.SelectedIndex = -1;
                cboDebitHead.SelectedIndex = -1;

                dgvExpense.Rows.Clear();
                bool blnRead = false;
                chkPostToAccount.Checked = false;

                if (PintOperationID > 0)
                {
                    MintRecordPosition = 1;
                    CboCompany.Enabled = false;
                    cboOperationType.Enabled = false;
                    cboReferenceNumber.Enabled = false;

                 //   if(PblnPostToAccount)
               chkPostToAccount.Checked = true; // commented by thasni

                    cboOperationType.SelectedValue = PintOperationType;
                    if (cboOperationType.SelectedValue == null)
                    {
                        DataTable datTemp = MobjClsBLLExpenseMaster.FillCombos(new string[] { "*", "OperationTypeReference", "OperationTypeID = " + PintOperationType });
                        if (datTemp.Rows.Count > 0)
                            cboOperationType.Text = datTemp.Rows[0]["OperationType"].ToString();
                    }
                    try
                    {
                        OperationTypeChanged(cboOperationType, new EventArgs());
                    }
                    catch { }
                    cboReferenceNumber.SelectedValue = PintOperationID;
                }
                else
                {
                    CboCompany.Enabled = true;
                    cboOperationType.Enabled = true;
                    cboReferenceNumber.Enabled = true;
                }
                if (MblnCalledDirect)
                {
                    MintRecordCount = MobjClsBLLExpenseMaster.GetRecordCount(PintOperationType, PintOperationID, ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.ExpCompany,intModuleID);
                    MintRecordPosition = MintRecordCount + 1;
                    bnCountItem.Text = " of " + (MintRecordCount + 1).ToString();
                    bnPositionItem.Text = (MintRecordCount + 1).ToString();
                    //MobjClsBLLExpenseMaster.objDTOExpenseMaster.blnPostToAccount = false;
                    //DisplayExpenseInfo();
                }
              


                //
                LoadCombo(1);

                btnOk.Enabled = false;
                btnSave.Enabled = false;

                //bnDeleteItem.Enabled = (MblnEditable && blnRead);
                bnDeleteItem.Enabled = MblnDeletePermission && MblnEditMode;
                bnEmail.Enabled = blnRead;
                bnPrint.Enabled = blnRead;
                bnSaveItem.Enabled = false;
                bnAddNewItem.Enabled = false;

                if (!MblnCalledDirect)
                {
                    MintRecordCount = 0;
                    MintRecordPosition = MintRecordCount + 1;
                    bnCountItem.Text = " of " + (MintRecordCount + 1).ToString();
                    bnPositionItem.Text = (MintRecordCount + 1).ToString();
                    if(PblnPostToAccount)
                    MobjClsBLLExpenseMaster.objDTOExpenseMaster.blnPostToAccount = true;
                    DisplayExpenseInfo();

                    //lblStatus.Text = MobjClsNotification.GetErrorMessage(MdatMessages, 10);
                    tmExpense.Enabled = true;
                   
                }
                SetBindingNavigatorButtons();

                cboOperationType.Focus();

                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7262, out MmsgMessageIcon);// Could not delete
                if (MblnEditable && !blnRead)
                    lblStatus.Text = "Add new Information";
                else if (MblnEditable && blnRead)
                    lblStatus.Text = "Expense Information";
                else
                    lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmExpense.Enabled = true;

               // SetBindingNavigatorButtons();
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void SetEnableDisable()
        {
            txtDescription.ReadOnly = !MblnEditable;
            //cboCreditHead.Enabled = MblnEditable;
            //cboDebitHead.Enabled = MblnEditable; 
           // dgvExpense.ReadOnly = !MblnEditable;  -- Coomented for testing putpose
        }

        private void AddNewItem(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void SaveItem(object sender, EventArgs e)
        {
            try
            {
                if (ValidateFields())
                {
                    if (SaveExpenseInfo())
                    {
                        foreach (DataGridViewRow dgvrow in dgvExpense.Rows)
                        {
                            if (Convert.ToInt32(dgvrow.Cells[dgvColExpenseType.Index].Value) > 0)
                            {
                                if (Convert.ToInt32(dgvrow.Cells[dgvColExpenseType.Index].Value) == 11)
                                {
                                    switch (Convert.ToInt32(cboOperationType.SelectedValue))
                                    {
                                        case (int)OperationType.SalesQuotation:
                                            try
                                            {
                                                this.SendSMS(dgvrow.Cells[dgvColExpenseType.Index].Value.ToString());
                                            }
                                            catch { }
                                            break;
                                        case (int)OperationType.SalesOrder:
                                            try
                                            {
                                                this.SendSMS(dgvrow.Cells[dgvColExpenseType.Index].Value.ToString());
                                            }
                                            catch { }
                                            break;
                                        case (int)OperationType.SalesInvoice:
                                            try
                                            {
                                                this.SendSMS(dgvrow.Cells[dgvColExpenseType.Index].Value.ToString());
                                            }
                                            catch { }
                                            break;
                                        case (int)OperationType.PurchaseQuotation:
                                            try
                                            {
                                                SendMail(dgvrow.Cells[dgvColExpenseType.Index].Value.ToString(), true);
                                            }
                                            catch { }
                                            break;
                                        case (int)OperationType.PurchaseOrder:
                                            try
                                            {
                                                SendMail(dgvrow.Cells[dgvColExpenseType.Index].Value.ToString(), true);
                                            }
                                            catch { }
                                            break;
                                        case (int)OperationType.PurchaseInvoice:
                                            try
                                            {
                                                SendMail(dgvrow.Cells[dgvColExpenseType.Index].Value.ToString(), true);
                                            }
                                            catch { }
                                            break;
                                        case (int)OperationType.StockTransfer:
                                            try
                                            {
                                                SendMail(dgvrow.Cells[dgvColExpenseType.Index].Value.ToString(), true);
                                            }
                                            catch { }
                                            break;
                                    }
                                }
                            }
                        }
                        ResetForm();
                    }
                    else
                        return;

                    if (sender.GetType() == typeof(Button))
                    {
                        if (((Button)sender).Name == btnOk.Name)
                            Close();
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on  SaveItem: " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private bool SaveExpenseInfo()
        {

            if (MblnEditMode && IsPaymentDone(false))
                return false;

            MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, (Convert.ToInt32(cboReferenceNumber.Tag) == 0 ? 1 : 3),
                out MmsgMessageIcon);
            if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                MessageBoxIcon.Information) == DialogResult.No)
                return false;

            MobjClsBLLExpenseMaster.objDTOExpenseMaster.decTotalAmount = txtTotalAmount.Text.Trim().Length > 0 ? Convert.ToDecimal(txtTotalAmount.Text) : 0;
            MobjClsBLLExpenseMaster.objDTOExpenseMaster.intCompanyID = PintCompanyID;
            MobjClsBLLExpenseMaster.objDTOExpenseMaster.intCurrencyID = Convert.ToInt32(cboCurrency.SelectedValue);
            MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngExpenseID = Convert.ToInt32(cboReferenceNumber.Tag);
            MobjClsBLLExpenseMaster.objDTOExpenseMaster.intOperationType = Convert.ToInt32(cboOperationType.SelectedValue);
            MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngReferenceID = Convert.ToInt32(cboReferenceNumber.SelectedValue);
            MobjClsBLLExpenseMaster.objDTOExpenseMaster.strRemarks = txtDescription.Text.Trim();
            MobjClsBLLExpenseMaster.objDTOExpenseMaster.intCreditHeadID = Convert.ToInt32(cboCreditHead.SelectedValue);
            MobjClsBLLExpenseMaster.objDTOExpenseMaster.intDebitHeadID = Convert.ToInt32(cboDebitHead.SelectedValue);
            MobjClsBLLExpenseMaster.objDTOExpenseMaster.intCompanyID = Convert.ToInt32(CboCompany.SelectedValue);
            MobjClsBLLExpenseMaster.objDTOExpenseMaster.strAccountDate = string.Empty;
            if (chkPostToAccount.Checked)
                MobjClsBLLExpenseMaster.objDTOExpenseMaster.blnPostToAccount = true;
            else
                MobjClsBLLExpenseMaster.objDTOExpenseMaster.blnPostToAccount = false;

            if (!PblnPostToAccount)
                MobjClsBLLExpenseMaster.objDTOExpenseMaster.blnPostToAccount = false;

            SetExpenseDetails();
            return MobjClsBLLExpenseMaster.SaveExpenseInfo();
        }

        private void SetExpenseDetails()
        {
            MobjClsBLLExpenseMaster.objDTOExpenseMaster.objDTOExpenseDetails = new List<clsDTOExpenseDetails>();
            foreach (DataGridViewRow dgvrow in dgvExpense.Rows)
            {
                if (Convert.ToInt32(dgvrow.Cells[dgvColExpenseType.Index].Value) > 0)
                {
                    clsDTOExpenseDetails objExpenseDetails = new clsDTOExpenseDetails();
                    objExpenseDetails.decQuantity = Convert.ToString(dgvrow.Cells[dgvColQuantity.Index].Value).Trim().Length == 0 ? 0 : Convert.ToDecimal(dgvrow.Cells[dgvColQuantity.Index].Value);
                    objExpenseDetails.intSerialNo = Convert.ToInt32(dgvrow.Cells[dgvColSerialNo.Index].Value);
                    objExpenseDetails.decAmount = Convert.ToDecimal(dgvrow.Cells[dgvColAmount.Index].Value);
                    objExpenseDetails.intCreatedBy = ClsCommonSettings.UserID;
                    objExpenseDetails.intExpenseTypeID = Convert.ToInt32(dgvrow.Cells[dgvColExpenseType.Index].Value);
                    objExpenseDetails.strDescription = Convert.ToString(dgvrow.Cells[dgvColDescription.Index].Value);
                    objExpenseDetails.intVoucherID = dgvrow.Cells[VoucherID.Index].Value.ToInt32();
                    MobjClsBLLExpenseMaster.objDTOExpenseMaster.objDTOExpenseDetails.Add(objExpenseDetails);
                }
            }
        }

        private bool ValidateFields()
        {
            bool blnValid = true;
            Control cntrlFocus = null;

            errExpense.Clear();

            MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngReferenceID = Convert.ToInt64(cboReferenceNumber.SelectedValue);
            if (Convert.ToInt32(cboOperationType.SelectedValue) == 0)
            {
                //Please select Operation Type
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7251, out MmsgMessageIcon);
                cntrlFocus = cboOperationType;
                blnValid = false;
            }
            else if (Convert.ToInt32(cboReferenceNumber.SelectedValue) == 0)
            {
                //Please select Reference Number
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7252, out MmsgMessageIcon);
                cntrlFocus = cboReferenceNumber;
                blnValid = false;
            }
            //else if (Convert.ToInt32(cboCreditHead.SelectedValue) == 0)
            //{
            //    if (chkPostToAccount.Checked)
            //    {
            //        //Please enter Credit Head
            //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7254, out MmsgMessageIcon);
            //        cntrlFocus = cboCreditHead;
            //        blnValid = false;
            //    }
            //}
            //else if (Convert.ToInt32(cboDebitHead.SelectedValue) == 0)
            //{
            //    if (chkPostToAccount.Checked)
            //    {
            //        //Please enter Debit Head";
            //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7255, out MmsgMessageIcon);
            //        cntrlFocus = cboDebitHead;
            //        blnValid = false;
            //    }
            //}
            //else if (Convert.ToInt32(cboCreditHead.SelectedValue) == Convert.ToInt32(cboDebitHead.SelectedValue))
            //{
            //    //Please enter Credit Head
            //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7263, out MmsgMessageIcon);
            //    cntrlFocus = cboCreditHead;
            //    blnValid = false;
            //}
            else if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
            {
                //Please enter Credit Head
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7265, out MmsgMessageIcon);
                cntrlFocus = cboCreditHead;
                blnValid = false;
            }
            else
            {
                blnValid = ValidateGrid();
                cntrlFocus = dgvExpense;
            }

            if (!blnValid)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExpense.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
            }
            return blnValid;
        }

        private bool ValidateGrid()
        {
            dgvExpense.CommitEdit(DataGridViewDataErrorContexts.Commit);
            
              for (int iCounter = 0; iCounter <= dgvExpense.Rows.Count-2; iCounter++)
            {
                if(Convert.ToInt32(dgvExpense.Rows[iCounter].Cells[dgvColExpenseType.Index].Value)<=0)
                {
                    //"Please enter ExpenseType";
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7269, out MmsgMessageIcon);
                        dgvExpense.CurrentCell = dgvExpense.Rows[iCounter].Cells[dgvColExpenseType.Index];
                        return false;
                }
                else if (Convert.ToInt32(dgvExpense.Rows[iCounter].Cells[dgvColQuantity.Index].Value.ToDecimal())<=0)
                {
                    //"Please enter Quantity";
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7256, out MmsgMessageIcon);
                        dgvExpense.CurrentCell = dgvExpense.Rows[iCounter].Cells[dgvColQuantity.Index];
                        return false;
                }
                else if(Convert.ToInt32(dgvExpense.Rows[iCounter].Cells[dgvColAmount.Index].Value.ToDecimal()) <=0)
                {
                     //Please enter Amount";
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7257, out MmsgMessageIcon);
                        dgvExpense.CurrentCell = dgvExpense.Rows[iCounter].Cells[dgvColAmount.Index];
                        return false;
                }
            }

            //foreach (DataGridViewRow dgvrow in dgvExpense.Rows)
            //{
            //    if (Convert.ToInt32(dgvrow.Cells[dgvColExpenseType.Index].Value) > 0)
            //    {
            //        if (dgvrow.Cells[dgvColQuantity.Index].Value.ToDecimal()<=0)
            //        {
            //            //"Please enter Quantity";
            //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7256, out MmsgMessageIcon);
            //            dgvExpense.CurrentCell = dgvrow.Cells[dgvColQuantity.Index];
            //            return false;
            //        }
            //        else if (dgvrow.Cells[dgvColAmount.Index].Value.ToDecimal() <=0)
            //        {
            //            //Please enter Amount";
            //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7257, out MmsgMessageIcon);
            //            dgvExpense.CurrentCell = dgvrow.Cells[dgvColAmount.Index];
            //            return false;
            //        }
            //    }
            //}

            List<int> intlistItems = new List<int>();
            int intValidRowCount = 0;

            foreach (DataGridViewRow dgvrow in dgvExpense.Rows)
            {
                if (Convert.ToInt32(dgvrow.Cells[dgvColExpenseType.Index].Value) > 0)
                {
                    intValidRowCount++;

                    if (intlistItems.Contains(Convert.ToInt32(dgvrow.Cells[dgvColExpenseType.Index].Value)))
                    {
                        //Duplicate item is not permitted";
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7258, out MmsgMessageIcon);
                        dgvExpense.CurrentCell = dgvrow.Cells[dgvColExpenseType.Index];
                        return false;
                    }
                    else
                    {
                        intlistItems.Add(Convert.ToInt32(dgvrow.Cells[dgvColExpenseType.Index].Value));
                    }
                }
            }

            if (intValidRowCount == 0)
            {
                //Please enter Item";
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7259, out MmsgMessageIcon);
                dgvExpense.CurrentCell = dgvExpense.Rows[0].Cells[dgvColExpenseType.Index];
                return false;
            }
            return true;
        }

        private bool DisplayExpenseInfo()
        {
            MobjClsBLLExpenseMaster.objDTOExpenseMaster.intRowNumber = MintRecordPosition;
            if (MobjClsBLLExpenseMaster.GetExpenseInfo(PintOperationType, PintOperationID,intModuleID))
            {
                this.MblnEditMode = true;
                LoadCombo(1);
                CboCompany.SelectedValue = MobjClsBLLExpenseMaster.objDTOExpenseMaster.intCompanyID;
                if (CboCompany.SelectedValue == null)
                {
                    DataTable datTemp = MobjClsBLLExpenseMaster.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjClsBLLExpenseMaster.objDTOExpenseMaster.intCompanyID });
                    if (datTemp.Rows.Count > 0)
                        CboCompany.Text = datTemp.Rows[0]["CompanyName"].ToString();
                    blnCheckStatus = true;
                }
                cboOperationType.SelectedValue = MobjClsBLLExpenseMaster.objDTOExpenseMaster.intOperationType.ToString();
                if (cboOperationType.SelectedValue == null)
                {
                    DataTable datTemp = MobjClsBLLExpenseMaster.FillCombos(new string[] { "*", "OperationTypeReference", "OperationTypeID = " + MobjClsBLLExpenseMaster.objDTOExpenseMaster.intOperationType.ToString() });
                    if (datTemp.Rows.Count > 0)
                        cboOperationType.Text = datTemp.Rows[0]["OperationType"].ToString();
                    blnCheckStatus = true;
                }
                cboReferenceNumber.Tag = MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngExpenseID.ToString();
                txtTotalAmount.Text = MobjClsBLLExpenseMaster.objDTOExpenseMaster.decTotalAmount.ToString();
                txtDescription.Text = MobjClsBLLExpenseMaster.objDTOExpenseMaster.strRemarks;
                
                OperationTypeChanged(cboOperationType, new EventArgs());
                cboReferenceNumber.SelectedValue = MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngReferenceID;
                cboCreditHead.SelectedValue = MobjClsBLLExpenseMaster.objDTOExpenseMaster.intCreditHeadID;
                cboDebitHead.SelectedValue = MobjClsBLLExpenseMaster.objDTOExpenseMaster.intDebitHeadID;
                if (Convert.ToBoolean(MobjClsBLLExpenseMaster.objDTOExpenseMaster.blnPostToAccount))
                {
                    chkPostToAccount.Checked = true; 
                    if (MblnCalledDirect)
                        MblnEditable = false;
                    else
                    {
                        int intMode = 0; string strSearchCondition = string.Empty;
                        switch (Convert.ToInt32(cboOperationType.SelectedValue))
                        {
                            case (int)OperationType.SalesOrder:
                                strSearchCondition += "SalesOrderID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In( " + (int)OperationStatusType.SOrderOpen +","+(int)OperationStatusType.SOrderRejected+")";
                                intMode = 7;
                                if (MObjClsBllCommonUtility.FillCombos(new string[] { "Payments.ReceiptAndPaymentID", "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", "Payments.OperationTypeID = " + (int)OperationType.SalesOrder + " And RPD.ReferenceID = " + cboReferenceNumber.SelectedValue.ToInt64() + " And ReceiptAndPaymentTypeID = " + (int)PaymentTypes.AdvancePayment }).Rows.Count > 0)
                                    PBlnIsEditable = false;
                                break;
                            case (int)OperationType.SalesInvoice:
                                strSearchCondition += "SalesInvoiceID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue)+ " And StatusID = " + (int)OperationStatusType.SInvoiceOpen;
                                intMode = 12;
                                if (MObjClsBllCommonUtility.FillCombos(new string[] { "PaymentTermsID", "InvSalesInvoiceMaster", "SalesInvoiceID = " + cboReferenceNumber.SelectedValue.ToInt64() }).Rows[0]["PaymentTermsID"].ToInt32() == (int)PaymentTerms.Credit && MObjClsBllCommonUtility.FillCombos(new string[] { "Payments.ReceiptAndPaymentID", "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", "Payments.OperationTypeID = " + (int)OperationType.SalesInvoice + " And RPD.ReferenceID = " + cboReferenceNumber.SelectedValue.ToInt64() + " And ReceiptAndPaymentTypeID = " + (int)PaymentTypes.InvoicePayment }).Rows.Count > 0)
                                    PBlnIsEditable = false;
                                break;
                            case (int)OperationType.PurchaseOrder:
                                strSearchCondition += "PurchaseOrderID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In(" + (int)OperationStatusType.POrderOpened + "," + (int)OperationStatusType.POrderRejected + ")";
                                intMode = 13;
                                if (MObjClsBllCommonUtility.FillCombos(new string[] { "Payments.ReceiptAndPaymentID", "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", "Payments.OperationTypeID = " + (int)OperationType.PurchaseOrder + " And RPD.ReferenceID = " + cboReferenceNumber.SelectedValue.ToInt64() + " And ReceiptAndPaymentTypeID = " + (int)PaymentTypes.AdvancePayment }).Rows.Count > 0)
                                    PBlnIsEditable = false;
                                break;
                            case (int)OperationType.PurchaseInvoice:
                                strSearchCondition += "PurchaseInvoiceID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID = " + (int)OperationStatusType.PInvoiceOpened;
                                intMode = 16;
                                if (MObjClsBllCommonUtility.FillCombos(new string[] {"PaymentTermsID","InvPurchaseInvoiceMaster","PurchaseInvoiceID = "+cboReferenceNumber.SelectedValue.ToInt64()}).Rows[0]["PaymentTermsID"].ToInt32() == (int)PaymentTerms.Credit && MObjClsBllCommonUtility.FillCombos(new string[] { "Payments.ReceiptAndPaymentID", "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", "Payments.OperationTypeID = " + (int)OperationType.PurchaseInvoice + " And RPD.ReferenceID = "+cboReferenceNumber.SelectedValue.ToInt64()+" And ReceiptAndPaymentTypeID = " + (int)PaymentTypes.InvoicePayment }).Rows.Count > 0)
                                    PBlnIsEditable = false;
                                break;
                            case (int)OperationType.PurchaseQuotation:
                                strSearchCondition += "PurchaseQuotationID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In( " + (int)OperationStatusType.PQuotationOpened+","+(int)OperationStatusType.PQuotationRejected+")";
                                intMode = 14;
                                break;
                            case (int)OperationType.SalesQuotation:
                                strSearchCondition += "SalesQuotationID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In( " + (int)OperationStatusType.SQuotationOpen + "," + (int)OperationStatusType.SQuotationRejected + ")";
                                intMode = 19;
                                break;
                            case (int)OperationType.StockTransfer:
                                strSearchCondition += "StockTransferID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In( " + (int)OperationStatusType.STOpened + "," + (int)OperationStatusType.STRejected + ")";
                                intMode = 18;
                                break;
                        }
                        if (PBlnIsEditable)
                        {
                            MblnEditable = MobjClsBLLExpenseMaster.CheckSalesOrderEditable(intMode, Convert.ToInt64(cboReferenceNumber.SelectedValue), strSearchCondition);
                        }
                        else
                            MblnEditable = false;
                    }
                }
                else
                {
                    chkPostToAccount.Checked = false;
                    if (!PblnPostToAccount)
                    {
                        int intMode = 0; string strSearchCondition = string.Empty;
                        switch (Convert.ToInt32(cboOperationType.SelectedValue))
                        {
                            case (int)OperationType.SalesInvoice:
                                strSearchCondition += "SalesInvoiceID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID = " + (int)OperationStatusType.SInvoiceOpen;
                                intMode = 12;
                                break;
                        }
                        MblnEditable = MobjClsBLLExpenseMaster.CheckSalesOrderEditable(intMode, Convert.ToInt64(cboReferenceNumber.SelectedValue), strSearchCondition);
                    }
                    else
                    {
                        if (Convert.ToInt32(cboOperationType.SelectedValue) == (int)OperationType.PurchaseOrder)
                        {
                            string strSearchCondition = "PurchaseOrderID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID = " + (int)OperationStatusType.POrderApproved;
                            MblnEditable = MobjClsBLLExpenseMaster.CheckSalesOrderEditable(13, Convert.ToInt64(cboReferenceNumber.SelectedValue), strSearchCondition);
                        }
                        else if (Convert.ToInt32(cboOperationType.SelectedValue) == (int)OperationType.StockTransfer)
                        {
                            string strSearchCondition = "StockTransferID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID = " + (int)OperationStatusType.STIssued;
                            MblnEditable = MobjClsBLLExpenseMaster.CheckSalesOrderEditable(18, Convert.ToInt64(cboReferenceNumber.SelectedValue), strSearchCondition);
                        }
                        else
                            MblnEditable = true;
                    }
                }
                if (MblnCalledDirect && Convert.ToInt32(cboOperationType.SelectedValue) != (int)OperationType.PurchaseOrder && Convert.ToInt32(cboOperationType.SelectedValue) != (int)OperationType.SalesInvoice && Convert.ToInt32(cboOperationType.SelectedValue) != (int)OperationType.PurchaseInvoice && Convert.ToInt32(cboOperationType.SelectedValue) != (int)OperationType.StockTransfer)
                    MblnEditable = false;
                SetEnableDisable();
                cboCurrency.SelectedValue = MobjClsBLLExpenseMaster.objDTOExpenseMaster.intCurrencyID;
                CboCompany.Enabled = false;
                cboReferenceNumber.Enabled = false;
                cboOperationType.Enabled = false;
                DisplayExpenseDetails();

                if (!MblnCalledDirect)
                {
                    bnPositionItem.Text = MobjClsBLLExpenseMaster.objDTOExpenseMaster.intRowNumber.ToString();
                    bnCountItem.Text = " of " + (MintRecordCount + 1).ToString();
                }
                else
                {
                    bnPositionItem.Text = MobjClsBLLExpenseMaster.objDTOExpenseMaster.intRowNumber.ToString();
                    bnCountItem.Text = " of " + (MintRecordCount).ToString();
                }

                bnSaveItem.Enabled = false;
                btnOk.Enabled = false;
                btnSave.Enabled = false;

                bnAddNewItem.Enabled = MblnAddPermission;
                bnDeleteItem.Enabled = MblnDeletePermission;
                bnEmail.Enabled = MblnPrintEmailPermission;
                bnPrint.Enabled = MblnPrintEmailPermission;

                return true;
            }
            else
            {
                int intMode = 0; string strSearchCondition = string.Empty;
                switch (Convert.ToInt32(cboOperationType.SelectedValue))
                {
                    case (int)OperationType.SalesOrder:
                        strSearchCondition += "SalesOrderID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In ( " + (int)OperationStatusType.SOrderOpen + "," + (int)OperationStatusType.SOrderRejected + ")";
                        intMode = 7;
                        break;
                    case (int)OperationType.SalesInvoice:
                        strSearchCondition += "SalesInvoiceID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue)/* + " And StatusID = " + (int)OperationStatusType.SInvoiceOpen*/;
                        intMode = 12;
                        break;
                    case (int)OperationType.PurchaseOrder:
                        strSearchCondition += "PurchaseOrderID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In(" + (int)OperationStatusType.POrderOpened + "," + (int)OperationStatusType.POrderRejected + ","+(int)OperationStatusType.POrderApproved+")";
                        intMode = 13;
                        break;
                    case (int)OperationType.PurchaseInvoice:
                        strSearchCondition += "PurchaseInvoiceID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID = " + (int)OperationStatusType.PInvoiceOpened;
                        intMode = 16;
                        break;
                    case (int)OperationType.PurchaseQuotation:
                        strSearchCondition += "PurchaseQuotationID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In( " + (int)OperationStatusType.PQuotationOpened + "," + (int)OperationStatusType.PQuotationRejected + ")";
                        intMode = 14;
                        break;
                    case (int)OperationType.SalesQuotation:
                        strSearchCondition += "SalesQuotationID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In ( " + (int)OperationStatusType.SQuotationOpen + "," + (int)OperationStatusType.SQuotationRejected + ")";
                        intMode = 19;
                        break;
                    case (int)OperationType.StockTransfer:
                        strSearchCondition += "StockTransferID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In ( " + (int)OperationStatusType.STOpened + "," + (int)OperationStatusType.STRejected + ")";
                        intMode = 18;
                        break;
                }
                if (PBlnIsEditable)
                {
                      MblnEditable = MobjClsBLLExpenseMaster.CheckSalesOrderEditable(intMode, Convert.ToInt64(cboReferenceNumber.SelectedValue), strSearchCondition);
                } 
                else      
                      MblnEditable = false; 
     
                SetEnableDisable();
            }
            return false;
        }

        private void DisplayExpenseDetails()
        {
            int intRowIndex = 0;

            dgvExpense.Rows.Clear();

            foreach (clsDTOExpenseDetails objExpenseDetails in MobjClsBLLExpenseMaster.objDTOExpenseMaster.objDTOExpenseDetails)
            {
                dgvExpense.RowCount++;

                dgvExpense.Rows[intRowIndex].Cells[dgvColExpenseType.Index].Value = (object)objExpenseDetails.intExpenseTypeID;
                dgvExpense.Rows[intRowIndex].Cells[dgvColQuantity.Index].Value = objExpenseDetails.decQuantity.ToString();
                dgvExpense.Rows[intRowIndex].Cells[dgvColSerialNo.Index].Value = objExpenseDetails.intSerialNo.ToString();
                dgvExpense.Rows[intRowIndex].Cells[dgvColAmount.Index].Value = objExpenseDetails.decAmount;
                dgvExpense.Rows[intRowIndex].Cells[dgvColDescription.Index].Value = objExpenseDetails.strDescription.ToString();
                dgvExpense.Rows[intRowIndex].Cells[VoucherID.Index].Value =Convert.ToString( objExpenseDetails.intVoucherID);


                intRowIndex++;
            }
        }
        private void SetBindingNavigatorButtons()
        {
            bnMoveNextItem.Enabled = bnMoveFirstItem.Enabled = bnMoveLastItem.Enabled = MblnViewPermission;
           bnMovePreviousItem.Enabled = MblnViewPermission;

            int iCurrentRec = Convert.ToInt32(bnPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCount;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                bnMoveNextItem.Enabled = bnMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                bnMoveFirstItem.Enabled = bnMovePreviousItem.Enabled = false;
            }
        }
        private void MoveFirstItem(object sender, EventArgs e)
        {
            try
            {
                blnCheckStatus = false;
                if (MintRecordPosition > 1)
                {
                    MintRecordPosition = 1;
                    DisplayExpenseInfo();

                    lblStatus.Text = MobjClsNotification.GetErrorMessage(MdatMessages, 9);
                    tmExpense.Enabled = true;

                    SetBindingNavigatorButtons();

                    if (blnCheckStatus == true)
                    {
                        bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = bnDeleteItem.Enabled = false;
                    }
                    else
                    {
                        bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                        bnDeleteItem.Enabled = MblnDeletePermission;
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void MovePreviousItem(object sender, EventArgs e)
        {
            try
            {
                blnCheckStatus = false;
                if (MintRecordPosition > 1)
                {
                    MintRecordPosition--;
                    DisplayExpenseInfo();

                    lblStatus.Text = MobjClsNotification.GetErrorMessage(MdatMessages, 10);
                    tmExpense.Enabled = true;

                    SetBindingNavigatorButtons();

                    if (blnCheckStatus == true)
                    {
                        bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = bnDeleteItem.Enabled = false;
                    }
                    else
                    {
                        bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                        bnDeleteItem.Enabled = MblnDeletePermission;
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void MoveNextItem(object sender, EventArgs e)
        {
            try
            {
                blnCheckStatus = false;
                if (MintRecordPosition < MintRecordCount)
                {
                    MintRecordPosition++;
                    DisplayExpenseInfo();

                    lblStatus.Text = MobjClsNotification.GetErrorMessage(MdatMessages, 11);
                    tmExpense.Enabled = true;

                    SetBindingNavigatorButtons();

                    if (blnCheckStatus == true)
                    {
                        bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = bnDeleteItem.Enabled = false;
                    }
                    else
                    {
                        bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                        bnDeleteItem.Enabled = MblnDeletePermission;
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void MoveLastItem(object sender, EventArgs e)
        {
            try
            {
                blnCheckStatus = false;
                if (MintRecordPosition <= MintRecordCount + 1 )
                {
                    MintRecordPosition = MintRecordCount;
                    DisplayExpenseInfo();

                    lblStatus.Text = MobjClsNotification.GetErrorMessage(MdatMessages, 12);
                    tmExpense.Enabled = true;

                    SetBindingNavigatorButtons();

                    if (blnCheckStatus == true)
                    {
                        bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = bnDeleteItem.Enabled = false;
                    }
                    else
                    {
                        bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                        bnDeleteItem.Enabled = MblnDeletePermission;
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void DeleteItem(object sender, EventArgs e)
        {
            try
            {
                if (!MblnEditable)
                {
                    btnOk.Enabled = false;
                    btnSave.Enabled = false;
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7266, out MmsgMessageIcon);// Could not delete
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                    return;
                }
                if (IsPaymentDone(true))
                    return;
                //Do you want to delete the Item ?
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7260, out MmsgMessageIcon);
                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return;

                MobjClsBLLExpenseMaster.DeleteExpenseInfo();
                ResetForm();
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void Cancel(object sender, EventArgs e)
        {
//            ResetForm();
            dgvExpense.Rows.Clear();
            txtDescription.Clear();
        }

        private void ChangeStatus(object sender, EventArgs e)
        {
            errExpense.Clear();
            if (MblnEditable == true)
            {
                btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = MblnAddPermission;
                if (MobjClsBLLExpenseMaster != null &&  MobjClsBLLExpenseMaster.objDTOExpenseMaster != null && MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngExpenseID > 0)
                  btnSave.Enabled =  bnSaveItem.Enabled = btnOk.Enabled = MblnUpdatePermission;
                bnDeleteItem.Enabled = false;
                bnPrint.Enabled = bnEmail.Enabled = false;                
            }
            else
            {
                btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = MblnUpdatePermission;
                bnDeleteItem.Enabled = MblnDeletePermission;
                bnPrint.Enabled = bnEmail.Enabled = MblnPrintEmailPermission;
            }

            //try
            //{
            //    if (CboCompany.SelectedValue != null)
            //    {
            //        if (MblnCalledDirect == true)
            //        {
            //            //DataTable datTemp = MobjClsBLLExpenseMaster.GetOperationTypeByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.ExpType);
            //            //string strTemp = "";
            //            //if (datTemp != null)
            //            //{
            //            //    datTemp.DefaultView.RowFilter = "OrderTypeID='" + Convert.ToString((int)OperationOrderType.ECOTPurchaseInvoice) + "'";
            //            //    if (datTemp.DefaultView.ToTable().Rows.Count > 0)
            //            //        strTemp += (int)OperationType.PurchaseInvoice + ",";
            //            //    datTemp.DefaultView.RowFilter = "OrderTypeID='" + Convert.ToString((int)OperationOrderType.ECOTSalesInvoice) + "'";
            //            //    if (datTemp.DefaultView.ToTable().Rows.Count > 0)
            //            //        strTemp += (int)OperationType.SalesInvoice + ",";
            //            //    datTemp.DefaultView.RowFilter = "OrderTypeID='" + Convert.ToString((int)OperationOrderType.ECOTPurchaseOrder) + "'";
            //            //    if (datTemp.DefaultView.ToTable().Rows.Count > 0)
            //            //        strTemp += (int)OperationType.PurchaseOrder + ",";
            //            //    datTemp.DefaultView.RowFilter = "OrderTypeID='" + Convert.ToString((int)OperationOrderType.ECOTStockTransfer) + "'";
            //            //    if (datTemp.DefaultView.ToTable().Rows.Count > 0)
            //            //        strTemp += (int)OperationType.StockTransfer + ",";
            //            //    if (strTemp != "")
            //            //        strTemp = strTemp.Remove(strTemp.Length - 1);
            //            //}
            //           // DataTable datCombos1 = new DataTable();
            //            //if (strTemp != "")
            //              //  datCombos1 = MobjClsBLLExpenseMaster.FillCombos(new string[] { "OperationTypeID,Description", "OperationTypeReference", "OperationTypeID In (" + strTemp + ")" });

            //            //if (cboOperationType.SelectedIndex != -1)
            //            //{
            //            //    datCombos1.DefaultView.RowFilter = "OperationTypeID=" + cboOperationType.SelectedValue.ToString();

            //            //    if (datCombos1.DefaultView.ToTable().Rows.Count == 0)
            //            //    {
            //            //        btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = false;
            //            //        bnDeleteItem.Enabled = false;
            //            //        bnPrint.Enabled = bnEmail.Enabled = false;
            //            //    }
            //            //}
            //            //else
            //            //{
            //                ////btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = false;
            //                ////bnDeleteItem.Enabled = false;
            //                ////bnPrint.Enabled = bnEmail.Enabled = false;
            //          //  }
            //        }
            //    }
            //}
            //catch(Exception Ex)
            //{
            //    MobjClsLogWriter.WriteLog("Error on Setting Permission in Change Status Function " + Ex.Message.ToString(), 2);
            //    btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = MblnAddUpdatePermission;
            //    bnDeleteItem.Enabled = MblnDeletePermission;
            //    bnPrint.Enabled = bnEmail.Enabled = MblnPrintEmailPermission;
            //}
        }

        private void Close(object sender, EventArgs e)
        {
            Close();
        }

        private void TimerTick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            tmExpense.Enabled = false;
        }

        private void Email(object sender, EventArgs e)
        {
            using (FrmEmailPopup objEmailPopUp = new FrmEmailPopup())
            {
                objEmailPopUp.MsSubject = "Expense Details";
                objEmailPopUp.EmailFormType = EmailFormID.Expense;
                objEmailPopUp.EmailSource = MobjClsBLLExpenseMaster.GetExpenseReport();
             
                objEmailPopUp.ShowDialog();
            }
        }

        //private void OnKeyDown(object sender, KeyEventArgs e)
        //{
        //    if (sender.GetType() == typeof(ComboBox))
        //    {
        //        ComboBox cbo = (ComboBox)sender;
        //        cbo.DroppedDown = false;
        //    }
        //}

        private void AmountValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == dgvColQuantity.Index || e.ColumnIndex == dgvColAmount.Index)
                {
                    decimal decItemTotal = 0, decTotalAmount = 0;

                    foreach (DataGridViewRow dgvrow in dgvExpense.Rows)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dgvrow.Cells[dgvColAmount.Index].Value)) && !string.IsNullOrEmpty(Convert.ToString(dgvrow.Cells[dgvColQuantity.Index].Value)))
                        {
                            if (dgvrow.Cells[dgvColAmount.Index].Value.ToDecimal() > 0 && dgvrow.Cells[dgvColQuantity.Index].Value.ToDecimal() >0)
                            {
                                if ((Convert.ToString(dgvrow.Cells[dgvColQuantity.Index].Value).Trim().Length == 0 ? 0 : Convert.ToDecimal(dgvrow.Cells[dgvColQuantity.Index].Value)) == 0)
                                {
                                    decItemTotal = Convert.ToDecimal(dgvrow.Cells[dgvColAmount.Index].Value);
                                }
                                else
                                {
                                    decItemTotal = Convert.ToDecimal(dgvrow.Cells[dgvColQuantity.Index].Value) *
                                        Convert.ToDecimal(dgvrow.Cells[dgvColAmount.Index].Value);
                                }
                                dgvrow.Cells["Total"].Value = decItemTotal.ToString("F"+MintCurrencyScale);
                                decTotalAmount = decTotalAmount + decItemTotal;
                            }
                        }
                    }

                    txtTotalAmount.Text = decTotalAmount.ToString("F" + MintCurrencyScale);
                    PdecTotalExpenseAmount = decTotalAmount;
                }
               ChangeStatus(sender, new EventArgs());
            }
            catch (Exception Ex)
            {
                //"Please enter valid Quantity " 
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7261, out MmsgMessageIcon);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();

                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void ReferenceNumbeChanged(object sender, EventArgs e)
        {
            int intMode = 0; string strSearchCondition = string.Empty;
            int intCurrencyID = 0;
            DataTable datCurrency = null;
            datCurrency = MobjClsBLLExpenseMaster.FillCombos(new string[] { "CurrencyId", "CompanyMaster", "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) });
            if (datCurrency.Rows.Count > 0)
                intCurrencyID = Convert.ToInt32(datCurrency.Rows[0]["CurrencyId"]);

            switch (Convert.ToInt32(cboOperationType.SelectedValue))
            {
                case (int)OperationType.SalesOrder:
                    strSearchCondition += "SalesOrderID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID = " + (int)OperationStatusType.SOrderOpen;
                    intMode = 7;
                    if (!MblnCalledDirect)
                    {
                        datCurrency = MobjClsBLLExpenseMaster.FillCombos(new string[] { "CurrencyID", "InvSalesOrderMaster", "SalesOrderID = " + Convert.ToInt32(cboReferenceNumber.SelectedValue) });
                        if (datCurrency.Rows.Count > 0)
                            intCurrencyID = Convert.ToInt32(datCurrency.Rows[0]["CurrencyID"]);

                        datDetails = MobjClsBLLExpenseMaster.FillCombos(new string[] { "*", "InvSalesOrderMaster", "SalesOrderID = " + Convert.ToInt32(cboReferenceNumber.SelectedValue) });
                    }
                    break;
                case (int)OperationType.SalesInvoice:
                    strSearchCondition += "SalesInvoiceID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID = " + (int)OperationStatusType.SInvoiceOpen;
                    intMode = 12;
                    if (!MblnCalledDirect)
                    {
                        datCurrency = MobjClsBLLExpenseMaster.FillCombos(new string[] { "CurrencyID", "InvSalesInvoiceMaster", "SalesInvoiceID = " + Convert.ToInt32(cboReferenceNumber.SelectedValue) });
                        if (datCurrency.Rows.Count > 0)
                            intCurrencyID = Convert.ToInt32(datCurrency.Rows[0]["CurrencyID"]);

                        datDetails = MobjClsBLLExpenseMaster.FillCombos(new string[] { "*", "InvSalesInvoiceMaster", "SalesInvoiceID = " + Convert.ToInt32(cboReferenceNumber.SelectedValue) });
                    }
                    break;
                case (int)OperationType.PurchaseOrder:

                    if (!MblnCalledDirect)
                    {
                        strSearchCondition += "PurchaseOrderID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In(" + (int)OperationStatusType.POrderOpened + "," + (int)OperationStatusType.POrderRejected + ")";
                        intMode = 13;
                        datCurrency = MobjClsBLLExpenseMaster.FillCombos(new string[] { "CurrencyID", "InvPurchaseOrderMaster", "PurchaseOrderID = " + Convert.ToInt32(cboReferenceNumber.SelectedValue) });
                        if (datCurrency.Rows.Count > 0)
                            intCurrencyID = Convert.ToInt32(datCurrency.Rows[0]["CurrencyID"]);

                        datDetails = MobjClsBLLExpenseMaster.FillCombos(new string[] { "*", "InvPurchaseOrderMaster", "PurchaseOrderID = " + Convert.ToInt32(cboReferenceNumber.SelectedValue) });
                    }
                    else
                    {
                        strSearchCondition += "PurchaseOrderID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In(" + (int)OperationStatusType.POrderApproved+")";
                        intMode = 13;
                    }
                    break;
                case (int)OperationType.PurchaseInvoice:
                    strSearchCondition += "PurchaseInvoiceID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID = " + (int)OperationStatusType.PInvoiceOpened;
                    intMode = 16;
                    if (!MblnCalledDirect)
                    {
                        datCurrency = MobjClsBLLExpenseMaster.FillCombos(new string[] { "CurrencyID", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + Convert.ToInt32(cboReferenceNumber.SelectedValue) });
                        if (datCurrency.Rows.Count > 0)
                            intCurrencyID = Convert.ToInt32(datCurrency.Rows[0]["CurrencyID"]);

                        datDetails = MobjClsBLLExpenseMaster.FillCombos(new string[] { "*", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + Convert.ToInt32(cboReferenceNumber.SelectedValue) });
                    }
                    break;
                case (int)OperationType.PurchaseQuotation:
                    strSearchCondition += "PurchaseQuotationID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In( " + (int)OperationStatusType.PQuotationOpened+","+(int)OperationStatusType.PQuotationRejected+")";
                    intMode = 14;
                    if (!MblnCalledDirect)
                    {
                        datCurrency = MobjClsBLLExpenseMaster.FillCombos(new string[] { "CurrencyID", "InvPurchaseQuotationMaster", "PurchaseQuotationID = " + Convert.ToInt32(cboReferenceNumber.SelectedValue) });
                        if (datCurrency.Rows.Count > 0)
                            intCurrencyID = Convert.ToInt32(datCurrency.Rows[0]["CurrencyID"]);

                        datDetails = MobjClsBLLExpenseMaster.FillCombos(new string[] { "*", "InvPurchaseQuotationMaster", "PurchaseQuotationID = " + Convert.ToInt32(cboReferenceNumber.SelectedValue) });
                    }
                    break;
                case (int)OperationType.SalesQuotation:
                    //strSearchCondition += "SalesQuotationID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID = " + (int)OperationStatusType.PQuotationOpened;
                    //intMode = 14;
                    if (!MblnCalledDirect)
                    {
                        datCurrency = MobjClsBLLExpenseMaster.FillCombos(new string[] { "CurrencyID", "InvSalesQuotationMaster", "SalesQuotationID = " + Convert.ToInt32(cboReferenceNumber.SelectedValue) });
                        if (datCurrency.Rows.Count > 0)
                            intCurrencyID = Convert.ToInt32(datCurrency.Rows[0]["CurrencyID"]);

                        datDetails = MobjClsBLLExpenseMaster.FillCombos(new string[] { "*", "InvSalesQuotationMaster", "SalesQuotationID = " + Convert.ToInt32(cboReferenceNumber.SelectedValue) });
                    }
                    break;
                case (int)OperationType.StockTransfer:
                    

                    if (!MblnCalledDirect)
                    {
                        strSearchCondition += "StockTransferID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In(" + (int)OperationStatusType.STOpened + "," + (int)OperationStatusType.STRejected + ")";
                        intMode = 18;
                        datDetails = MobjClsBLLExpenseMaster.FillCombos(new string[] { "*", "InvStockTransferMaster", "StockTransferID = " + Convert.ToInt32(cboReferenceNumber.SelectedValue) });
                        if (datDetails.Rows.Count > 0)
                            intCurrencyID = datDetails.Rows[0]["CurrencyID"].ToInt32(); 
                    }
                    else
                    {
                        strSearchCondition += "StockTransferID = " + Convert.ToInt64(cboReferenceNumber.SelectedValue) + " And StatusID In(" + (int)OperationStatusType.STIssued + ")";
                        intMode = 18;
                    }
                     
                    break;
            }
            cboCurrency.SelectedValue = intCurrencyID;
            if (chkPostToAccount.Checked)
                MblnEditable = MobjClsBLLExpenseMaster.CheckSalesOrderEditable(intMode,Convert.ToInt64(cboReferenceNumber.SelectedValue), strSearchCondition);
            else
                MblnEditable = true;
            SetEnableDisable();

            if (!MblnEditable)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7262, out MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmExpense.Enabled = true;
            }
            else
            {
                lblStatus.Text = "";
            }
        }

        private void OperationTypeChanged(object sender, EventArgs e)
        {
            DataTable datCombos = null;
                switch (Convert.ToInt32(cboOperationType.SelectedValue))
                {
                    case (int)OperationType.PurchaseInvoice:
                        if(!MblnCalledDirect)
                        datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "PurchaseInvoiceID as ReferenceID, PurchaseInvoiceNo as ReferenceNumber", "InvPurchaseInvoiceMaster", "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + (PintOperationID > 0 ? "" : " And StatusID = " + (int)OperationStatusType.PInvoiceOpened) });
                        else if(MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngExpenseID !=0)
                           datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "PurchaseInvoiceID as ReferenceID, PurchaseInvoiceNo as ReferenceNumber", "InvPurchaseInvoiceMaster", " CompanyID = "+CboCompany.SelectedValue.ToInt32() });
                        else
                            datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "PurchaseInvoiceID as ReferenceID, PurchaseInvoiceNo as ReferenceNumber", "InvPurchaseInvoiceMaster", "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + (PintOperationID > 0 ? "" : " And StatusID = " + (int)OperationStatusType.PInvoiceOpened) });
                        break;
                    case (int)OperationType.PurchaseOrder:
                        if (!MblnCalledDirect)
                        datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "PurchaseOrderID as ReferenceID, PurchaseOrderNo as ReferenceNumber", "InvPurchaseOrderMaster", "CompanyID = " + CboCompany.SelectedValue + (PintOperationID > 0 ? "" : " And StatusID In = " + (int)OperationStatusType.POrderOpened)});
                        else if (MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngExpenseID > 0)
                            datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "PurchaseOrderID as ReferenceID, PurchaseOrderNo as ReferenceNumber", "InvPurchaseOrderMaster", "CompanyID = " + CboCompany.SelectedValue});
                            else
                            datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "PurchaseOrderID as ReferenceID, PurchaseOrderNo as ReferenceNumber", "InvPurchaseOrderMaster", "CompanyID = " + CboCompany.SelectedValue + (PintOperationID > 0 ? "" : " And StatusID = "+(int)OperationStatusType.POrderApproved) });
                        break;
                    case (int)OperationType.PurchaseQuotation:
                        datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "PurchaseQuotationID as ReferenceID, PurchaseQuotationNo as ReferenceNumber", "InvPurchaseQuotationMaster", "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + (PintOperationID > 0 ? "" : " And StatusID = " + (int)OperationStatusType.PQuotationOpened) });
                        break;
                    case (int)OperationType.SalesQuotation:
                        datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "SalesQuotationID as ReferenceID, SalesQuotationNo as ReferenceNumber", "InvSalesQuotationMaster", "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + (PintOperationID > 0 ? "" : " And StatusID = " + (int)OperationStatusType.SQuotationOpen) });
                        break;
                    case (int)OperationType.SalesInvoice:
                        if (!MblnCalledDirect)
                        datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "SalesInvoiceID as ReferenceID, SalesInvoiceNo as ReferenceNumber", "InvSalesInvoiceMaster", "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + (PintOperationID > 0 ? "" : " And StatusID = " + (int)OperationStatusType.SInvoiceOpen) });
                        else
                            datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "SalesInvoiceID as ReferenceID, SalesInvoiceNo as ReferenceNumber", "InvSalesInvoiceMaster", "CompanyID = "+Convert.ToInt32(CboCompany.SelectedValue)+" And StatusID <> "+(int)OperationStatusType.SInvoiceCancelled+"" });
                        break;
                    case (int)OperationType.SalesOrder:
                        //datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "SalesOrderID as ReferenceID, SalesOrderNo as ReferenceNumber", "InvSalesOrderMaster", "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + (PintOperationID > 0 ? "" : " And StatusID = " + (int)OperationStatusType.SOrderClosed) });
                        if(MblnCalledDirect)
                            datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "SalesOrderID as ReferenceID, SalesOrderNo as ReferenceNumber", "InvSalesOrderMaster", "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + (MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngExpenseID > 0 ? "" : " And StatusID = " + (int)OperationStatusType.SOrderClosed) });
                        else
                            datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "SalesOrderID as ReferenceID, SalesOrderNo as ReferenceNumber", "InvSalesOrderMaster", "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue)});
                        break;
                    case (int)OperationType.StockTransfer:
                        if (MblnCalledDirect)
                            datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "StockTransferID as ReferenceID, StockTransferNo as ReferenceNumber", "InvStockTransferMaster", "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + (MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngExpenseID > 0 ? "" : " And StatusID = " + (int)OperationStatusType.STIssued) });
                        else
                            datCombos = MobjClsBLLExpenseMaster.FillCombos(new string[] { "StockTransferID as ReferenceID, StockTransferNo as ReferenceNumber", "InvStockTransferMaster", "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) });
                        break;
                }

            cboReferenceNumber.DisplayMember = "ReferenceNumber";
            cboReferenceNumber.ValueMember = "ReferenceID";
            cboReferenceNumber.DataSource = datCombos;
            cboReferenceNumber.SelectedIndex = -1;
        }

        //private void FormKeyDown(object sender, KeyEventArgs e)
        //{
        //    //if (e.Control)
        //    //{
        //    //    switch (e.KeyCode)
        //    //    {
        //    //        case Keys.Left:
        //    //            MovePreviousItem(sender, e);
        //    //            break;
        //    //        case Keys.Right:
        //    //            MoveNextItem(sender, e);
        //    //            break;
        //    //        case Keys.Up:
        //    //            MoveLastItem(sender, e);
        //    //            break;
        //    //        case Keys.Down:
        //    //            MoveFirstItem(sender, e);
        //    //            break;
        //    //    }
        //    //}
        //}

        //private void ExpenseRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        //{
        //    ChangeStatus(sender, new EventArgs());
        //    AmountValueChanged(dgvExpense, new DataGridViewCellEventArgs(dgvColQuantity.Index, 0));
        //}

        //private void dgvExpense_KeyDown(object sender, KeyEventArgs e)
        //{
        //        //if (e.KeyCode == Keys.Delete)
        //        //{
                    
        //        //}
        //}

        private bool DeleteExpenseDetails()
        {
            if (Convert.ToInt32(dgvExpense.CurrentRow.Cells[dgvColSerialNo.Index].Value) == 0)
                return true;

            if (!MblnEditable)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7262, out MmsgMessageIcon);// Could not delete
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            if (dgvExpense.Rows.Count == 2)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7264, out MmsgMessageIcon);// Could not delete
                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                    MmsgMessageIcon) == DialogResult.No)
                    return false;
                else
                {
                    MobjClsBLLExpenseMaster.DeleteExpenseDetails(Convert.ToInt32(dgvExpense.CurrentRow.Cells[dgvColSerialNo.Index].Value), dgvExpense.CurrentRow.Cells[Total.Index].Value.ToDecimal());
                    MobjClsBLLExpenseMaster.DeleteExpenseInfo();
                    ResetForm();
                    dgvExpense.Rows.Add();
                    return true;
                }
            }
            else
            {
                //Do you want to delete the Item ?
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7260, out MmsgMessageIcon);
                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return false;
            }

            return MobjClsBLLExpenseMaster.DeleteExpenseDetails(Convert.ToInt32(dgvExpense.CurrentRow.Cells[dgvColSerialNo.Index].Value),dgvExpense.CurrentRow.Cells[Total.Index].Value.ToDecimal());
        }

        private void ClosingForm(object sender, FormClosingEventArgs e)
        {
            if (btnOk.Enabled)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 8, out MmsgMessageIcon).Replace("#", "").Trim();
                if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.Yes)
                {
                    PdecTotalExpenseAmount = (txtTotalAmount.Text.Trim().Equals(string.Empty) ? 0 : Convert.ToDecimal(txtTotalAmount.Text));
                    MobjClsNotification = null;
                    MobjClsLogWriter = null;
                  //  MobjClsBLLExpenseMaster = null;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {         
            try
            {
                if (MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngExpenseID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngExpenseID;
                    //cboOperationType.SelectedValue = MobjClsBLLExpenseMaster.objDTOExpenseMaster.intOperationType.ToString();
                    ObjViewer.Type =Convert.ToString(cboOperationType.Text.Trim());
                    ObjViewer.PiFormID = (int)FormID.Expense;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmExpense:LoadReport " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport() " + Ex.Message.ToString());
            }
        }

        public double GetExpenseAmount(int intOperationType, Int64 intReferenceID,long lngExpenseID)
        {
            return MobjClsBLLExpenseMaster.GetExpenseAmount(intOperationType, intReferenceID,lngExpenseID); 
        }

        private void dgvExpense_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {

        }

        private void dgvExpense_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            AmountValueChanged(sender, e);
        }

        private void dgvExpense_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (dgvExpense.ReadOnly)
                e.Cancel = true;
            else
            {
                decimal decTotalAmount = txtTotalAmount.Text.ToDecimal();
                txtTotalAmount.Text = (decTotalAmount - dgvExpense[Total.Index, e.Row.Index].Value.ToDecimal()).ToString("F" + MintCurrencyScale);
                if (!IsPaymentDone(false))
                {
                    if (!DeleteExpenseDetails())
                    {
                        e.Cancel = true;
                        txtTotalAmount.Text = decTotalAmount.ToString();
                    }
                }
                else
                {
                    e.Cancel = true;
                    txtTotalAmount.Text = decTotalAmount.ToString();
                }
            }
        }

        private void dgvExpense_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if(dgvExpense.Columns[e.ColumnIndex].Name == "dgvColQuantity" || dgvExpense.Columns[e.ColumnIndex].Name == "dgvColAmount")
                AmountValueChanged(sender, e);
        }

        private void dgvExpense_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void BtnCreditAccount_Click(object sender, EventArgs e)
        {
            try
            {
                FrmChartOfAccounts objFrmChartOfAccounts = new FrmChartOfAccounts();
                objFrmChartOfAccounts.ShowDialog();
                FillCreditAccountHeads();
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form ChartOfAccounts:BtnAccountHead_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on ChartOfAccounts() " + Ex.Message.ToString());
            }
        }
        
       
        private void BtnDebitAccount_Click(object sender, EventArgs e)
        {
            try
            {
                FrmChartOfAccounts objFrmChartOfAccounts = new FrmChartOfAccounts();
                objFrmChartOfAccounts.ShowDialog();
                FillDebitAccountHeads();
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form ChartOfAccounts:BtnAccountHead_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on ChartOfAccounts() " + Ex.Message.ToString());
            }
        }
        private void FillCreditAccountHeads()
        {
            DataTable datAccountHeads = new DataTable();

            datAccountHeads = MobjClsBLLExpenseMaster.AccountDetails();
            cboCreditHead.DataSource = datAccountHeads;
            cboCreditHead.ValueMember = "AccountID";
            cboCreditHead.DisplayMember = "Account";
        }
        private void FillDebitAccountHeads()
        {
            DataTable datAccountHeads = new DataTable();

            datAccountHeads = MobjClsBLLExpenseMaster.AccountDetails();
            cboDebitHead.DataSource = datAccountHeads;
            cboDebitHead.ValueMember = "AccountID";
            cboDebitHead.DisplayMember = "Account";
        }

        private void chkPostToAccount_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPostToAccount.Checked)
            {
                cboCreditHead.Enabled=true;
                cboDebitHead.Enabled=true;
                BtnCreditAccount.Enabled = true;
                BtnDebitAccount.Enabled = true;
            }
            else
            {
                cboCreditHead.Enabled=false;
                cboDebitHead.Enabled = false;
                BtnCreditAccount.Enabled = false;
                BtnDebitAccount.Enabled = false;
                cboCreditHead.SelectedValue = 0;
                cboDebitHead.SelectedValue = 0;
            }
        }
        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }
        private void btnAccountSettings_Click(object sender, EventArgs e)
        {
            using (FrmAccountSettings objFrmAccountSettings = new FrmAccountSettings())
            {
                objFrmAccountSettings.ShowDialog();
            }
        }

        private void dgvExpense_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvExpense.IsCurrentCellDirty)
                {
                    if (dgvExpense.CurrentCell != null)
                    {
                        dgvExpense.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    }
                }
            }
            catch
            {
            }
        }

        private void dgvExpense_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                return;
            }
            catch
            {
                return;
            }
        }

        private void frmExpense_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    //case Keys.Control | Keys.S:
                    //    btnSave_Click(sender,new EventArgs());//save
                    //  break;
                    //case Keys.Control | Keys.O:
                    //    btnOk_Click(sender, new EventArgs());//OK
                    //    break; 
                    //case Keys.Control | Keys.L:
                    //    btnCancel_Click(sender, new EventArgs());//Cancel
                    //    break;
                    case Keys.Control | Keys.Enter:
                       AddNewItem(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                       DeleteItem(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        Cancel(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        MovePreviousItem(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        MoveNextItem(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                         MoveFirstItem(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        MoveLastItem(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        bnPrint_Click(sender, new EventArgs());//Cancel
                        break;
                    case Keys.Control | Keys.M:
                        Email(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void cboOperationType_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cboOperationType.SelectedValue != null)
            {
                if (MblnCalledDirect)
                {
                    bnPayments.Visible = true;
                    int intOperationType = Convert.ToInt32(cboOperationType.SelectedValue);
                    if (intOperationType == (int)OperationType.PurchaseOrder || intOperationType == (int)OperationType.PurchaseInvoice || intOperationType == (int)OperationType.StockTransfer)
                    {
                        bnPayments.Text = "Payments";
                    }
                    else
                    {
                        bnPayments.Text = "Receipts";
                    }
                }
            }
            else
            {
                bnPayments.Visible = false;
            }
        }

        private void bnPayments_Click(object sender, EventArgs e)
        {
            int intMode = (int)eMenuID.Receipt;
            if (Convert.ToInt32(cboOperationType.SelectedValue) == (int)OperationType.PurchaseInvoice || Convert.ToInt32(cboOperationType.SelectedValue) == (int)OperationType.PurchaseOrder || cboOperationType.SelectedValue.ToInt32() == (int)OperationType.StockTransfer)
                intMode = (int)eMenuID.Payment;
            
            int intVendorID = 0;
            int intOperationTypeID = cboOperationType.SelectedValue.ToInt32();
            DataTable datVendor = new DataTable();
            switch (intOperationTypeID)
            {
                case (int)OperationType.PurchaseInvoice:
                    datVendor = MObjClsBllCommonUtility.FillCombos(new string[] { "VendorID", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + cboReferenceNumber.SelectedValue.ToInt64() });
                    break;
                case (int)OperationType.PurchaseOrder:
                    datVendor = MObjClsBllCommonUtility.FillCombos(new string[] { "VendorID", "InvPurchaseOrderMaster", "PurchaseOrderID = " + cboReferenceNumber.SelectedValue.ToInt64() });
                    break;
                case (int)OperationType.StockTransfer :
                    datVendor = MObjClsBllCommonUtility.FillCombos(new string[] { "IsNull(VendorID,0) as VendorID", "InvStockTransferMaster", "StockTransferID = " + cboReferenceNumber.SelectedValue.ToInt64() });
                    break;
                case (int)OperationType.SalesOrder :
                    datVendor = MObjClsBllCommonUtility.FillCombos(new string[] { "VendorID", "InvSalesOrderMaster", "SalesOrderID = " + cboReferenceNumber.SelectedValue.ToInt64() });
                    break;
                case (int)OperationType.SalesInvoice :
                    datVendor = MObjClsBllCommonUtility.FillCombos(new string[] { "VendorID", "InvSalesInvoiceMaster", "SalesInvoiceID = " + cboReferenceNumber.SelectedValue.ToInt64() });
                    break;
            }

            if (datVendor.Rows.Count > 0)
                intVendorID = datVendor.Rows[0]["VendorID"].ToInt32();

                using (frmReceiptsAndPayments objFrmPayment = new frmReceiptsAndPayments(intMode,CboCompany.SelectedValue.ToInt32(),intOperationTypeID,(int)PaymentTypes.DirectExpense,intVendorID))
                {
                    objFrmPayment.ShowDialog();
                }
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable datCurrency = MobjClsBLLExpenseMaster.FillCombos(new string[] { "CurrencyID,Scale", "CurrencyReference", "CurrencyID = " + Convert.ToInt32(cboCurrency.SelectedValue) });
            if (datCurrency.Rows.Count > 0)
                MintCurrencyScale = Convert.ToInt32(datCurrency.Rows[0]["Scale"]);
        }

        private void SendSMS(string Amt)
        {
            StringBuilder sbrMessage;
            clsSendmail clsSendSMSMail;
            string strToAddress = "";

            try
            {
                this.MobjClsBLLExpenseMaster.GetMailSetting();
                if (this.MobjClsBLLExpenseMaster.objclsDTOMailSetting == null) return;

                strToAddress = this.MobjClsBLLExpenseMaster.GetVendorMobileNo(Convert.ToInt32(datDetails.Rows[0]["VendorID"].ToString())) +
                                "@" + this.MobjClsBLLExpenseMaster.objclsDTOMailSetting.IncomingServer;
                sbrMessage = new StringBuilder();
                sbrMessage.Append("Please Pay Demurrage charge Amount : " + Amt);
                
                clsSendSMSMail = new clsSendmail();
                if (!clsSendSMSMail.IsInternet_Connected())
                    return;

                clsSendSMSMail.SendSmsMail(this.MobjClsBLLExpenseMaster.objclsDTOMailSetting.UserName,
                                            this.MobjClsBLLExpenseMaster.objclsDTOMailSetting.PassWord,
                                            this.MobjClsBLLExpenseMaster.objclsDTOMailSetting.OutgoingServer,
                                            this.MobjClsBLLExpenseMaster.objclsDTOMailSetting.PortNumber,
                                            this.MobjClsBLLExpenseMaster.objclsDTOMailSetting.UserName,
                                            strToAddress, "Demurrage charge",
                                            sbrMessage.ToString(),
                                            this.MobjClsBLLExpenseMaster.objclsDTOMailSetting.EnableSsl);
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling sending sms:SendSMS " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on SendSMS() " + Ex.Message.ToString());
            }
        }

        private bool SendMail(string Amt, bool bFl)
        {
            try
            {
                DataTable datTemp = datDetails;
                for (int i = 0; i < datTemp.Rows.Count; i++)
                {
                    string Tomailid = datTemp.Rows[i]["EmailID"].ToString();
                    string Subject = "Demurrage charge";
                    string Message = "Please Pay Demurrage charge Amount : " + Amt;
                    //string argstrpath = "";

                    string FromMailID = "";
                    string PWD = "";
                    string SmtpServerAddress = "";
                    int PortNumber = 0;

                    MobjclsBLLEmailPopUp.send(out FromMailID, out PWD, out SmtpServerAddress, out PortNumber);
                    SmtpClient objSmtpClient = new SmtpClient();
                    MailMessage mailMessage = new MailMessage();
                    objSmtpClient.Credentials = new System.Net.NetworkCredential(FromMailID, PWD);
                    objSmtpClient.Port = PortNumber;
                    objSmtpClient.Host = SmtpServerAddress;

                    if (SmtpServerAddress.IndexOf("gmail") != -1)
                    {
                        objSmtpClient.EnableSsl = true;
                    }

                    mailMessage.From = new MailAddress(FromMailID);
                    mailMessage.To.Add(Tomailid);

                    mailMessage.Subject = Subject;
                    mailMessage.Body = Message;
                    objSmtpClient.Send(mailMessage);
                }
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form SendMail:SendMail" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on SendMail " + Ex.Message.ToString());
                return false;
            }
        }

        private bool IsPaymentDone(bool blnIsDeletion)
        {
            bool blnIsPaymentDone = false;
            if (cboReferenceNumber.SelectedValue.ToInt64() > 0)
            {
                double dblExpenseAmount = MobjClsBLLExpenseMaster.GetExpenseAmount((int)cboOperationType.SelectedValue, (Int64)cboReferenceNumber.SelectedValue, MobjClsBLLExpenseMaster.objDTOExpenseMaster.lngExpenseID);
                double dblPaidAmount = MobjClsBLLExpenseMaster.GetPaidAmount((int)cboOperationType.SelectedValue, (Int64)cboReferenceNumber.SelectedValue);
                if (blnIsDeletion)
                {
                    if (dblExpenseAmount < dblPaidAmount)
                    {
                        blnIsPaymentDone = true;
                    }
                }
                else
                {
                    if ((dblExpenseAmount + txtTotalAmount.Text.ToDouble()) < dblPaidAmount)
                    {
                        blnIsPaymentDone = true;
                    }
                }

                if (blnIsPaymentDone)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7267).Replace("#", "");
                    MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            return blnIsPaymentDone;
        }

        private void CboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboCompany.SelectedValue != null)
                //SetPermissionsCompanywisePermission(Convert.ToInt32(CboCompany.SelectedValue));
            LoadCombo(1);
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "ExtraCharges";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void txtTotalAmount_TextChanged(object sender, EventArgs e)
        {
            if(MObjClsBllCommonUtility != null )
           lblAmountInWords.Text = MObjClsBllCommonUtility.ConvertToWord(Convert.ToString(txtTotalAmount.Text), Convert.ToInt32(cboCurrency.SelectedValue));
        }

        private void dgvExpense_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MObjClsBllCommonUtility.SetSerialNo(dgvExpense, e.RowIndex, false);
        }

        private void dgvExpense_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MObjClsBllCommonUtility.SetSerialNo(dgvExpense, e.RowIndex, false);
        }

        private void dgvExpense_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == Total.Index)
                e.Cancel = true;
        }
    }
}