﻿
namespace MyBooksERP

{
    partial class frmRPFilter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRPFilter));
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboOperationType = new System.Windows.Forms.ComboBox();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.lblToDate = new System.Windows.Forms.Label();
            this.lblOperationType = new System.Windows.Forms.Label();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.cboReferenceNumber = new System.Windows.Forms.ComboBox();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblReferenceNo = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.cboPaymentType = new System.Windows.Forms.ComboBox();
            this.lblPaymentType = new System.Windows.Forms.Label();
            this.cboCustomer = new System.Windows.Forms.ComboBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.lblNo = new System.Windows.Forms.Label();
            this.cboNo = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblNo);
            this.panel1.Controls.Add(this.cboNo);
            this.panel1.Controls.Add(this.cboOperationType);
            this.panel1.Controls.Add(this.lblFromDate);
            this.panel1.Controls.Add(this.lblToDate);
            this.panel1.Controls.Add(this.lblOperationType);
            this.panel1.Controls.Add(this.lblCustomer);
            this.panel1.Controls.Add(this.cboReferenceNumber);
            this.panel1.Controls.Add(this.dtpFromDate);
            this.panel1.Controls.Add(this.lblReferenceNo);
            this.panel1.Controls.Add(this.dtpToDate);
            this.panel1.Controls.Add(this.cboPaymentType);
            this.panel1.Controls.Add(this.lblPaymentType);
            this.panel1.Controls.Add(this.cboCustomer);
            this.panel1.Location = new System.Drawing.Point(5, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(334, 179);
            this.panel1.TabIndex = 229;
            // 
            // cboOperationType
            // 
            this.cboOperationType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOperationType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOperationType.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cboOperationType.DropDownHeight = 75;
            this.cboOperationType.FormattingEnabled = true;
            this.cboOperationType.IntegralHeight = false;
            this.cboOperationType.Location = new System.Drawing.Point(93, 16);
            this.cboOperationType.Name = "cboOperationType";
            this.cboOperationType.Size = new System.Drawing.Size(228, 21);
            this.cboOperationType.TabIndex = 0;
            this.cboOperationType.SelectedIndexChanged += new System.EventHandler(this.cboOperationType_SelectedIndexChanged);
            this.cboOperationType.TextChanged += new System.EventHandler(this.cboOperationType_TextChanged);
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Location = new System.Drawing.Point(7, 124);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(56, 13);
            this.lblFromDate.TabIndex = 221;
            this.lblFromDate.Text = "Date From";
            // 
            // lblToDate
            // 
            this.lblToDate.AutoSize = true;
            this.lblToDate.Location = new System.Drawing.Point(197, 124);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(20, 13);
            this.lblToDate.TabIndex = 222;
            this.lblToDate.Text = "To";
            // 
            // lblOperationType
            // 
            this.lblOperationType.AutoSize = true;
            this.lblOperationType.Location = new System.Drawing.Point(7, 19);
            this.lblOperationType.Name = "lblOperationType";
            this.lblOperationType.Size = new System.Drawing.Size(80, 13);
            this.lblOperationType.TabIndex = 225;
            this.lblOperationType.Text = "Operation Type";
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Location = new System.Drawing.Point(7, 97);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(51, 13);
            this.lblCustomer.TabIndex = 220;
            this.lblCustomer.Text = "Customer";
            // 
            // cboReferenceNumber
            // 
            this.cboReferenceNumber.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboReferenceNumber.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboReferenceNumber.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cboReferenceNumber.DropDownHeight = 75;
            this.cboReferenceNumber.FormattingEnabled = true;
            this.cboReferenceNumber.IntegralHeight = false;
            this.cboReferenceNumber.Location = new System.Drawing.Point(93, 40);
            this.cboReferenceNumber.Name = "cboReferenceNumber";
            this.cboReferenceNumber.Size = new System.Drawing.Size(228, 21);
            this.cboReferenceNumber.TabIndex = 1;
            this.cboReferenceNumber.SelectedIndexChanged += new System.EventHandler(this.cboReferenceNumber_SelectedIndexChanged);
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Checked = false;
            this.dtpFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(93, 121);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.ShowCheckBox = true;
            this.dtpFromDate.Size = new System.Drawing.Size(98, 20);
            this.dtpFromDate.TabIndex = 4;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            // 
            // lblReferenceNo
            // 
            this.lblReferenceNo.AutoSize = true;
            this.lblReferenceNo.Location = new System.Drawing.Point(7, 43);
            this.lblReferenceNo.Name = "lblReferenceNo";
            this.lblReferenceNo.Size = new System.Drawing.Size(74, 13);
            this.lblReferenceNo.TabIndex = 224;
            this.lblReferenceNo.Text = "Reference No";
            // 
            // dtpToDate
            // 
            this.dtpToDate.Checked = false;
            this.dtpToDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(223, 121);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.ShowCheckBox = true;
            this.dtpToDate.Size = new System.Drawing.Size(98, 20);
            this.dtpToDate.TabIndex = 5;
            this.dtpToDate.ValueChanged += new System.EventHandler(this.dtpToDate_ValueChanged);
            // 
            // cboPaymentType
            // 
            this.cboPaymentType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPaymentType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPaymentType.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cboPaymentType.DropDownHeight = 75;
            this.cboPaymentType.FormattingEnabled = true;
            this.cboPaymentType.IntegralHeight = false;
            this.cboPaymentType.Location = new System.Drawing.Point(93, 67);
            this.cboPaymentType.Name = "cboPaymentType";
            this.cboPaymentType.Size = new System.Drawing.Size(228, 21);
            this.cboPaymentType.TabIndex = 2;
            this.cboPaymentType.TabStop = false;
            this.cboPaymentType.SelectedIndexChanged += new System.EventHandler(this.cboPaymentType_SelectedIndexChanged);
            // 
            // lblPaymentType
            // 
            this.lblPaymentType.AutoSize = true;
            this.lblPaymentType.Location = new System.Drawing.Point(7, 70);
            this.lblPaymentType.Name = "lblPaymentType";
            this.lblPaymentType.Size = new System.Drawing.Size(75, 13);
            this.lblPaymentType.TabIndex = 223;
            this.lblPaymentType.Text = "Payment Type";
            // 
            // cboCustomer
            // 
            this.cboCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCustomer.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cboCustomer.DropDownHeight = 75;
            this.cboCustomer.FormattingEnabled = true;
            this.cboCustomer.IntegralHeight = false;
            this.cboCustomer.Location = new System.Drawing.Point(93, 94);
            this.cboCustomer.Name = "cboCustomer";
            this.cboCustomer.Size = new System.Drawing.Size(228, 21);
            this.cboCustomer.TabIndex = 3;
            this.cboCustomer.SelectedIndexChanged += new System.EventHandler(this.cboCustomer_SelectedIndexChanged);
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(257, 188);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(75, 23);
            this.btnFilter.TabIndex = 228;
            this.btnFilter.Text = "&OK";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // lblNo
            // 
            this.lblNo.AutoSize = true;
            this.lblNo.Location = new System.Drawing.Point(7, 150);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(21, 13);
            this.lblNo.TabIndex = 227;
            this.lblNo.Text = "No";
            // 
            // cboNo
            // 
            this.cboNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboNo.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cboNo.DropDownHeight = 75;
            this.cboNo.FormattingEnabled = true;
            this.cboNo.IntegralHeight = false;
            this.cboNo.Location = new System.Drawing.Point(93, 147);
            this.cboNo.Name = "cboNo";
            this.cboNo.Size = new System.Drawing.Size(228, 21);
            this.cboNo.TabIndex = 226;
            // 
            // frmRPFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.ClientSize = new System.Drawing.Size(344, 214);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnFilter);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRPFilter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Filter";
            this.Load += new System.EventHandler(this.frmRPFilter_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cboOperationType;
        private System.Windows.Forms.Label lblFromDate;
        private System.Windows.Forms.Label lblToDate;
        private System.Windows.Forms.Label lblOperationType;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.ComboBox cboReferenceNumber;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Label lblReferenceNo;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.ComboBox cboPaymentType;
        private System.Windows.Forms.Label lblPaymentType;
        private System.Windows.Forms.ComboBox cboCustomer;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.Label lblNo;
        private System.Windows.Forms.ComboBox cboNo;
    }
}