﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class frmExtraChargePayment : Form
    {
        clsBLLExtraChargePayment MobjclsBLLExtraChargePayment;
        ClsNotificationNew MobjClsNotification;
        ClsCommonUtility MobjCommonUtility = new ClsCommonUtility();

        public int PintFormType = 0;
        public int PintOperationType;
        public long PlngReferenceID = 0;
        public long PlngExtraChargeID = 0;

        private bool MblnAddPermission = false;///To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnPrintEmailPermission = false;

        DataTable MdatMessages;
        string MstrCommonMessage;
        MessageBoxIcon MmsgMessageIcon;
        bool blnAddStatus, blnNavigateStatus = false;
        long TotalRecordCnt, CurrentRecCnt;

        public frmExtraChargePayment()
        {
            InitializeComponent();
            MobjclsBLLExtraChargePayment = new clsBLLExtraChargePayment();
            MobjClsNotification = new ClsNotificationNew();
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intCompanyID = ClsCommonSettings.CompanyID;
        }

        private void frmExtraChargePayment_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadMessage();
            LoadCombo(0);
            cboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;

            if (PintOperationType > 0)
            {
                cboOperationType.SelectedIndex = -1;
                cboOperationType.SelectedValue = PintOperationType;
            }

            if (PlngReferenceID > 0)
            {
                cboReferenceNumber.SelectedIndex = -1;
                cboReferenceNumber.SelectedValue = PlngReferenceID;
            }

            if (PlngExtraChargeID > 0)
            {
                cboExtraChargeNo.SelectedIndex = -1;
                cboExtraChargeNo.SelectedValue = PlngExtraChargeID;
                //getShipmentVendorWithExtraChargeNo();
            }

            ClearControls();
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.ExtraChargePayment, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void LoadMessage()
        {
            MdatMessages = new DataTable();
            MdatMessages = MobjClsNotification.FillMessageArray((int)FormID.Expense, 4);
        }

        private void LoadCombo(int intType)
        {
            DataTable datCombos = null;

            if (intType == 0 || intType == 1) // Currency
            {
                datCombos = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "CurrencyID,CurrencyName", "CurrencyReference", "" });
                cboCurrency.DisplayMember = "CurrencyName";
                cboCurrency.ValueMember = "CurrencyID";
                cboCurrency.DataSource = datCombos;
            }

            if (intType == 0 || intType == 2) // Company
            {
                datCombos = null;
                datCombos = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID });
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = datCombos;

                MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intCompanyID = ClsCommonSettings.LoginCompanyID;
            }

            if (intType == 0 || intType == 3) // Operation Types
            {
                datCombos = null;
                //if (PintFormType == 0)
                //{
                //    datCombos = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", "" +
                //                "OperationTypeID In ( " + (Int32)OperationType.PurchaseOrder + "," + (Int32)OperationType.PurchaseInvoice + "," +
                //                "" + (Int32)OperationType.SalesOrder + "," + (Int32)OperationType.SalesInvoice + ")" });
                //}
                //else if (PintFormType == 1) // Purchase
                //{
                    datCombos = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", "" +
                                "OperationTypeID In ( " + (Int32)OperationType.PurchaseInvoice + ")" });
                //}
                //else if (PintFormType == 2) // Sale
                //{
                //    datCombos = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", "" +
                //                "OperationTypeID In ( " + (Int32)OperationType.SalesOrder + "," + (Int32)OperationType.SalesInvoice + ")" });
                //}

                cboOperationType.ValueMember = "OperationTypeID";
                cboOperationType.DisplayMember = "OperationType";
                cboOperationType.DataSource = datCombos;

                if (PintOperationType > 0)
                    cboOperationType.SelectedValue = PintOperationType;
                else
                    cboOperationType.SelectedIndex = -1;
            }

            if (intType == 0 || intType == 4) // Payment Mode
            {
                cboPaymentMode.DataSource = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "TransactionTypeID,TransactionType", "" +
                    "TransactionTypeReference", "TransactionTypeID IN (1,2)" }); // 1-Bank,2-Cash
                cboPaymentMode.DisplayMember = "TransactionType";
                cboPaymentMode.ValueMember = "TransactionTypeID";
                cboPaymentMode.SelectedValue = 2;
            }

            if (intType == 0 || intType == 5) // Bank Account
            {
                cboAccount.DataSource = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "AccountID,AccountName", "AccAccountMaster", "AccountGroupID = " + (int)AccountGroups.BankAccounts });
                cboAccount.DisplayMember = "AccountName";
                cboAccount.ValueMember = "AccountID";
            }
        }

        private void ClearControls()
        {
            errExtraCharge.Clear();
            txtExtraChargePaymentNo.Tag = 0;
            txtExtraChargePaymentNo.Text = getExtraChargePaymentNo();
            dtpExtraChargePaymentDate.Value = ClsCommonSettings.GetServerDate();

            if (PintOperationType == 0)
            {
                cboOperationType.SelectedIndex = -1;
                cboOperationType.Enabled = true;
            }
            else
                cboOperationType.Enabled = false;

            if (PlngReferenceID == 0)
            {
                cboReferenceNumber.SelectedIndex = -1;
                cboReferenceNumber.Enabled = true;
            }
            else
                cboReferenceNumber.Enabled = false;

            cboShipmentVendor.SelectedIndex = -1;
            if (PlngExtraChargeID == 0)
            {                
                cboExtraChargeNo.SelectedIndex = -1;
                cboCurrency.SelectedIndex = -1;
                cboExtraChargeNo.Enabled = true;
            }
            else
                cboExtraChargeNo.Enabled = false;
            
            txtDescription.Text = "";
            dgvExtraChargePayment.Rows.Clear();
            txtTotalAmount.Text = "";
            cboAccount.SelectedIndex = -1;
            txtChNo.Text = string.Empty;
            dtpChDate.Value = ClsCommonSettings.GetServerDate();
            lblAmountInWords.Text = "Amount In Words";
            cboCompany.Focus();
            cboPaymentMode.SelectedValue = 2; // Cash
            cboAccount.Enabled = BtnAccount.Enabled = txtChNo.Enabled = dtpChDate.Enabled = false;

            GetRecordCountwithValues();

            blnAddStatus = true;
            bnAddNewItem.Enabled = bnCancel.Enabled = MblnAddPermission;
            bnSaveItem.Enabled = bnDeleteItem.Enabled = bnPrint.Enabled = bnEmail.Enabled = false;
            btnSave.Enabled = btnOk.Enabled = false;
        }

        private string getExtraChargePaymentNo()
        {
            string strTemp = "";

            if (cboCompany.SelectedIndex >= 0)
                strTemp = MobjclsBLLExtraChargePayment.getExtraChargePaymentNo(cboCompany.SelectedValue.ToInt32());

            return strTemp;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedIndex >= 0)
            {
                txtExtraChargePaymentNo.Text = getExtraChargePaymentNo();

                txtExtraChargePaymentNo.ReadOnly = false;
                if (MobjclsBLLExtraChargePayment.CheckExtraChargePaymentNoAutoGenerate(cboCompany.SelectedValue.ToInt32()).ToUpper() == "YES")
                    txtExtraChargePaymentNo.ReadOnly = true;

                cboOperationType_SelectedIndexChanged(sender, e);
            }
            setEnableDisable();
        }

        private void cboOperationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            getReferenceNo();
            setEnableDisable();
        }

        private void getReferenceNo()
        {
            cboReferenceNumber.DataSource = null;
            if (cboOperationType.SelectedIndex >= 0)
            {
                int intTempMode = 0;
                if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseOrder)
                    intTempMode = 9;
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseInvoice)
                    intTempMode = 10;
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesOrder)
                    intTempMode = 24;
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesInvoice)
                    intTempMode = 25;

                cboReferenceNumber.DataSource = MobjclsBLLExtraChargePayment.getReferenceNo(intTempMode, cboCompany.SelectedValue.ToInt32());
                cboReferenceNumber.ValueMember = "ReferenceID";
                cboReferenceNumber.DisplayMember = "ReferenceNo";
            }
        }

        private void getExtraChargeNo()
        {
            cboExtraChargeNo.DataSource = null;
            if (cboOperationType.SelectedIndex >= 0)
            {
                cboExtraChargeNo.DataSource = MobjclsBLLExtraChargePayment.getExtraChargeNo(cboCompany.SelectedValue.ToInt32(), 
                    cboOperationType.SelectedValue.ToInt32(), cboReferenceNumber.SelectedValue.ToInt64(), cboShipmentVendor.SelectedValue.ToInt32());
                cboExtraChargeNo.ValueMember = "ExtraChargeID";
                cboExtraChargeNo.DisplayMember = "ExtraChargeNo";
            }
        }

        private void getShipmentVendor()
        {
            cboShipmentVendor.DataSource = null;
            if (cboOperationType.SelectedIndex >= 0)
            {
                cboShipmentVendor.DataSource = MobjclsBLLExtraChargePayment.getShipmentVendor(cboCompany.SelectedValue.ToInt32(),
                    cboOperationType.SelectedValue.ToInt32(), cboReferenceNumber.SelectedValue.ToInt64());
                cboShipmentVendor.ValueMember = "VendorID";
                cboShipmentVendor.DisplayMember = "VendorName";
            }
        }

        private void getShipmentVendorWithExtraChargeNo()
        {
            cboShipmentVendor.DataSource = null;
            if (cboExtraChargeNo.SelectedIndex >= 0)
            {
                cboShipmentVendor.DataSource = MobjclsBLLExtraChargePayment.getShipmentVendorWithExtraChargeNo(cboCompany.SelectedValue.ToInt32(),
                    cboOperationType.SelectedValue.ToInt32(), cboReferenceNumber.SelectedValue.ToInt64(), cboExtraChargeNo.SelectedValue.ToInt64());
                cboShipmentVendor.ValueMember = "VendorID";
                cboShipmentVendor.DisplayMember = "VendorName";
            }
        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void cboOperationType_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboOperationType.DroppedDown = false;
        }

        private void cboReferenceNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboReferenceNumber.DroppedDown = false;
        }

        private void cboShipmentVendor_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboShipmentVendor.DroppedDown = false;
        }

        private void cboExtraChargeNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboExtraChargeNo.DroppedDown = false;
        }

        private void cboPaymentMode_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboPaymentMode.DroppedDown = false;
        }

        private void cboAccount_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboAccount.DroppedDown = false;
        }

        private void setEnableDisable()
        {
            errExtraCharge.Clear();
            if (blnAddStatus)
            {
                bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnAddPermission;
                bnDeleteItem.Enabled = bnPrint.Enabled = bnEmail.Enabled = false;
            }
            else
            {
                bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                bnDeleteItem.Enabled = MblnDeletePermission;
                bnPrint.Enabled = bnEmail.Enabled = MblnPrintEmailPermission;
            }
        }

        private void GetRecordCountwithValues()
        {
            RecordCount();
            bnCountItem.Text = "of " + (TotalRecordCnt + 1).ToString();
            bnPositionItem.Text = (TotalRecordCnt + 1).ToString();
        }

        private void RecordCount()
        {
            if (PintOperationType > 0 && PlngReferenceID > 0 && PlngExtraChargeID > 0)
                TotalRecordCnt = MobjclsBLLExtraChargePayment.getRecordCountForSpecificValue(ClsCommonSettings.LoginCompanyID, 
                    PintOperationType, PlngReferenceID, PlngExtraChargeID);
            else
                TotalRecordCnt = MobjclsBLLExtraChargePayment.getRecordCount();

            CurrentRecCnt = TotalRecordCnt;
            bnCountItem.Text = "of " + TotalRecordCnt.ToString();
            bnPositionItem.Text = TotalRecordCnt.ToString();

            if (TotalRecordCnt == 0)
            {
                bnCountItem.Text = "of 0";
                CurrentRecCnt = 0;
                TotalRecordCnt = 0;
            }
            BindingEnableDisable();
        }

        private void BindingEnableDisable()
        {
            bnMoveFirstItem.Enabled = true;
            bnMovePreviousItem.Enabled = true;
            bnMoveNextItem.Enabled = true;
            bnMoveLastItem.Enabled = true;

            if (TotalRecordCnt == 0)
            {
                bnMoveFirstItem.Enabled = false;
                bnMovePreviousItem.Enabled = false;
                bnMoveNextItem.Enabled = false;
                bnMoveLastItem.Enabled = false;
            }
            else
            {
                if (Convert.ToDouble(bnPositionItem.Text) == Convert.ToDouble(TotalRecordCnt))
                {
                    bnMoveNextItem.Enabled = false;
                    bnMoveLastItem.Enabled = false;
                }
                else if (Convert.ToDouble(bnPositionItem.Text) == 1)
                {
                    bnMoveFirstItem.Enabled = false;
                    bnMovePreviousItem.Enabled = false;
                }
            }
        }

        private void bnMoveFirstItem_Click(object sender, EventArgs e)
        {
            bnPositionItem.Text = "1";
            if (TotalRecordCnt > 1)
                bnCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                bnCountItem.Text = "of 1";
            blnNavigateStatus = true;
            DisplayExtraCharge();
        }

        private void bnMovePreviousItem_Click(object sender, EventArgs e)
        {
            bnPositionItem.Text = (Convert.ToInt32(bnPositionItem.Text) - 1).ToString();
            if (Convert.ToInt32(bnPositionItem.Text) <= 0)
                bnPositionItem.Text = "1";
            if (TotalRecordCnt > 1)
                bnCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                bnCountItem.Text = "of 1";
            blnNavigateStatus = true;
            DisplayExtraCharge();
        }

        private void bnMoveNextItem_Click(object sender, EventArgs e)
        {
            bnPositionItem.Text = (Convert.ToInt32(bnPositionItem.Text) + 1).ToString();
            if (TotalRecordCnt < Convert.ToInt32(bnPositionItem.Text))
            {
                if (TotalRecordCnt > 1)
                    bnPositionItem.Text = TotalRecordCnt.ToString();
                else
                    bnPositionItem.Text = "1";
            }
            if (TotalRecordCnt > 1)
                bnCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                bnCountItem.Text = "of 1";
            blnNavigateStatus = true;
            DisplayExtraCharge();
        }

        private void bnMoveLastItem_Click(object sender, EventArgs e)
        {
            if (TotalRecordCnt > 1)
            {
                bnPositionItem.Text = TotalRecordCnt.ToString();
                bnCountItem.Text = "of " + TotalRecordCnt.ToString();
            }
            else
            {
                bnPositionItem.Text = "1";
                bnCountItem.Text = "of 1";
            }
            blnNavigateStatus = true;
            DisplayExtraCharge();
        }

        private void DisplayExtraCharge()
        {
            dgvExtraChargePayment.Rows.Clear();
            DataTable datTemp = new DataTable();

            if (PintOperationType > 0 && PlngReferenceID > 0 && PlngExtraChargeID > 0)
                datTemp = MobjclsBLLExtraChargePayment.DisplayExtraChargePaymentMasterForSpecificValue(bnPositionItem.Text.ToInt64(), 
                    ClsCommonSettings.LoginCompanyID, PintOperationType, PlngReferenceID, PlngExtraChargeID);
            else
                datTemp = MobjclsBLLExtraChargePayment.DisplayExtraChargePaymentMaster(bnPositionItem.Text.ToInt64());

            blnAddStatus = false;
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    txtExtraChargePaymentNo.Tag = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.lngExtraChargePaymentID;
                    cboCompany.SelectedValue = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intCompanyID;
                    
                    cboCurrency.SelectedValue = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intCurrencyID;
                    if (cboCurrency.SelectedValue == null)
                    {
                        DataTable datTemp1 = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "CurrencyName", "CurrencyReference", "CurrencyID = " + MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intCurrencyID });
                        if (datTemp1.Rows.Count > 0)
                            cboCurrency.Text = datTemp1.Rows[0]["Description"].ToString();
                    }

                    txtExtraChargePaymentNo.Text = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.strExtraChargePaymentNo;
                    dtpExtraChargePaymentDate.Value = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.dtExtraChargePaymentDate;

                    cboOperationType.SelectedValue = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intOperationTypeID;
                    if (cboOperationType.SelectedValue == null)
                    {
                        DataTable datTemp1 = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "OperationType", "OperationTypeReference", "OperationTypeID = " + MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intOperationTypeID });
                        if (datTemp1.Rows.Count > 0)
                            cboOperationType.Text = datTemp1.Rows[0]["OperationType"].ToString();
                    }

                    cboReferenceNumber.SelectedValue = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.lngReferenceID;
                    if (cboReferenceNumber.SelectedValue == null)
                    {
                        DataTable datTemp1 = new DataTable();
                        if (MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intOperationTypeID == (int)OperationType.PurchaseOrder)
                            datTemp1 = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "PurchaseOrderNo AS ReferenceNo", "InvPurchaseOrderMaster", "PurchaseOrderID = " + MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.lngReferenceID });
                        else if (MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intOperationTypeID == (int)OperationType.PurchaseInvoice)
                            datTemp1 = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "PurchaseInvoiceNo AS ReferenceNo", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.lngReferenceID });
                        else if (MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intOperationTypeID == (int)OperationType.SalesOrder)
                            datTemp1 = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "SalesOrderNo AS ReferenceNo", "InvSalesOrderMaster", "SalesOrderID = " + MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.lngReferenceID });
                        else if (MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intOperationTypeID == (int)OperationType.SalesInvoice)
                            datTemp1 = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "SalesInvoiceNo AS ReferenceNo", "InvSalesInvoiceMaster", "SalesInvoiceID = " + MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.lngReferenceID });


                        if (datTemp1.Rows.Count > 0)
                            cboReferenceNumber.Text = datTemp1.Rows[0]["ReferenceNo"].ToString();
                    }

                    cboShipmentVendor.SelectedValue = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intVendorID;
                    if (cboShipmentVendor.SelectedValue == null)
                    {
                        DataTable datTemp1 = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "VendorName", "InvVendorInformation", "VendorID = " + MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intVendorID });
                        if (datTemp1.Rows.Count > 0)
                            cboShipmentVendor.Text = datTemp1.Rows[0]["VendorName"].ToString();
                    }

                    cboExtraChargeNo.SelectedValue = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.lngExtraChargeID;
                    if (cboExtraChargeNo.SelectedValue == null)
                    {
                        DataTable datTemp1 = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "ExtraChargeNo", "InvExtraChargeMaster", "ExtraChargeID = " + MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.lngExtraChargeID });
                        if (datTemp1.Rows.Count > 0)
                            cboExtraChargeNo.Text = datTemp1.Rows[0]["ExtraChargeNo"].ToString();

                        cboCurrency.SelectedValue = MobjclsBLLExtraChargePayment.getCurrency(MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.lngExtraChargeID);
                    }

                    cboPaymentMode.SelectedValue = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intPaymentModeID;
                    cboAccount.SelectedValue = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intAccountID;
                    txtChNo.Text = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.strChNo;
                    dtpChDate.Value = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.dtChDate;
                    txtTotalAmount.Text = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.decTotalAmount.ToString();
                    txtDescription.Text = MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.strDescription;

                    for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                    {
                        if (dgvExtraChargePayment.RowCount == iCounter)
                            dgvExtraChargePayment.Rows.Add();
                        dgvExtraChargePayment.Rows[iCounter].Cells["SerialNo"].Value = datTemp.Rows[iCounter]["SerialNo"].ToInt32();
                        dgvExtraChargePayment.Rows[iCounter].Cells["ExpenseTypeID"].Value = datTemp.Rows[iCounter]["ExpenseTypeID"].ToInt32();
                        dgvExtraChargePayment.Rows[iCounter].Cells["ExpenseType"].Value = datTemp.Rows[iCounter]["ExpenseType"].ToString();
                        dgvExtraChargePayment.Rows[iCounter].Cells["TotalAmount"].Value = datTemp.Rows[iCounter]["TotalAmount"].ToDecimal();
                        dgvExtraChargePayment.Rows[iCounter].Cells["CurrentBalance"].Value = datTemp.Rows[iCounter]["CurrentBalance"].ToDecimal();
                        dgvExtraChargePayment.Rows[iCounter].Cells["Amount"].Value = datTemp.Rows[iCounter]["Amount"].ToDecimal();
                        dgvExtraChargePayment.Rows[iCounter].Cells["EntryAmount"].Value = datTemp.Rows[iCounter]["EntryAmount"].ToDecimal();
                    }
                    dgvExtraChargePayment.PerformLayout();
                }
            }
            BindingEnableDisable();
            setEnableDisable();            
            bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false;
        }

        private string getTotalByExchangeRate()
        {
            decimal decAmount = 0;
            if (cboCompany.SelectedIndex >= 0)
            {
                for (int iCounter = 0; iCounter <= dgvExtraChargePayment.Rows.Count - 1; iCounter++)
                {
                    decAmount += dgvExtraChargePayment.Rows[iCounter].Cells["Amount"].Value.ToDecimal();
                }
            }
            return decAmount.ToString();
        }

        private void bnSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveExtraChargePayment())
            {
                if (blnAddStatus == true)
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 2, out MmsgMessageIcon);
                else
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 21, out MmsgMessageIcon);

                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();

                bnAddNewItem_Click(sender, e);
            }
        }

        private bool SaveExtraChargePayment()
        {
            bool blnStatus = false;
            if (Validations())
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, (txtExtraChargePaymentNo.Tag.ToInt64() == 0 ? 1 : 3), out MmsgMessageIcon);
                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    fillParameters();
                    DataTable datTemp = FillDetailParameters();

                    if (MobjclsBLLExtraChargePayment.SaveExtraChargePayment(datTemp))
                        blnStatus = true;
                }                
            }
            return blnStatus;
        }

        private bool Validations()
        {
            Control cntrlFocus = null;
            errExtraCharge.Clear();

            if (cboCompany.SelectedIndex == -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7265, out MmsgMessageIcon);
                cntrlFocus = cboCompany;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            if (cboCurrency.SelectedIndex == -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7253, out MmsgMessageIcon);
                cntrlFocus = cboCurrency;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            if (cboOperationType.SelectedIndex == -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7251, out MmsgMessageIcon);
                cntrlFocus = cboOperationType;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            if (cboReferenceNumber.SelectedIndex == -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7252, out MmsgMessageIcon);
                cntrlFocus = cboReferenceNumber;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            if (Convert.ToDateTime(dtpExtraChargePaymentDate.Value.ToString("dd MMM yyyy")) > ClsCommonSettings.GetServerDate())
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7282, out MmsgMessageIcon);
                cntrlFocus = dtpExtraChargePaymentDate;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            DataTable datTemp = MobjclsBLLExtraChargePayment.FillCombos(new string[] { "CONVERT(VARCHAR,ExtraChargeDate,106) AS ExtraChargeDate", "InvExtraChargeMaster", "ExtraChargeID = " + cboExtraChargeNo.SelectedValue.ToInt64() + "" });
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    if (Convert.ToDateTime(dtpExtraChargePaymentDate.Value.ToString("dd MMM yyyy")) < Convert.ToDateTime((datTemp.Rows[0]["ExtraChargeDate"].ToDateTime()).ToString("dd MMM yyyy")))
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7283, out MmsgMessageIcon);
                        cntrlFocus = dtpExtraChargePaymentDate;
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        if (cntrlFocus != null)
                        {
                            if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                                errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                            cntrlFocus.Focus();
                        }
                        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                        return false;
                    }
                }
            }            

            if (cboShipmentVendor.SelectedIndex == -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7272, out MmsgMessageIcon);
                cntrlFocus = cboShipmentVendor;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            if (cboExtraChargeNo.SelectedIndex == -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7281, out MmsgMessageIcon);
                cntrlFocus = cboExtraChargeNo;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            if (cboPaymentMode.SelectedIndex == -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7278, out MmsgMessageIcon);
                cntrlFocus = cboPaymentMode;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            if (cboPaymentMode.SelectedValue.ToInt32() == 1) // Bank
            {
                if (cboAccount.SelectedIndex == -1)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7279, out MmsgMessageIcon);
                    cntrlFocus = cboAccount;
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    if (cntrlFocus != null)
                    {
                        if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                            errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                        cntrlFocus.Focus();
                    }
                    lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                    return false;
                }

                if (txtChNo.Text.Trim() == "")
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7280, out MmsgMessageIcon);
                    cntrlFocus = txtChNo;
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    if (cntrlFocus != null)
                    {
                        if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                            errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                        cntrlFocus.Focus();
                    }
                    lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                    return false;
                }
            }

            if (txtExtraChargePaymentNo.Text == string.Empty)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7277, out MmsgMessageIcon);
                cntrlFocus = txtExtraChargePaymentNo;
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }
            else
            {
                if (MobjclsBLLExtraChargePayment.CheckExtraChargePaymentNoDuplication(cboCompany.SelectedValue.ToInt32(), txtExtraChargePaymentNo.Tag.ToInt64(), txtExtraChargePaymentNo.Text))
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7275, out MmsgMessageIcon);
                    cntrlFocus = txtExtraChargePaymentNo;
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    if (cntrlFocus != null)
                    {
                        if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                            errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                        cntrlFocus.Focus();
                    }
                    lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                    return false;
                }
            }

            dgvExtraChargePayment.CommitEdit(DataGridViewDataErrorContexts.Commit);

            if (dgvExtraChargePayment.Rows.Count == 0)
            {
                cntrlFocus = null;
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7274, out MmsgMessageIcon);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }
                        
            foreach (DataGridViewRow dgvrow in dgvExtraChargePayment.Rows)
            {
                cntrlFocus = null;

                if (dgvrow.Cells[Amount.Index].Value.ToDecimal() > 0)
                {
                    if (dgvrow.Cells[Amount.Index].Value.ToDecimal() > 
                        (dgvrow.Cells[CurrentBalance.Index].Value.ToDecimal() + dgvrow.Cells[EntryAmount.Index].Value.ToDecimal()))
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7276, out MmsgMessageIcon);
                        dgvExtraChargePayment.CurrentCell = dgvrow.Cells[Amount.Index];
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        if (cntrlFocus != null)
                        {
                            if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                                errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                            cntrlFocus.Focus();
                        }
                        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                        return false;
                    }
                }
            }

            bool blnTemp = true;
            foreach (DataGridViewRow dgvrow1 in dgvExtraChargePayment.Rows)
            {
                if (dgvrow1.Cells[Amount.Index].Value.ToDecimal() > 0)
                    blnTemp = false;
            }

            if (blnTemp == true)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 7257, out MmsgMessageIcon);
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus != null)
                {
                    if (cntrlFocus.GetType() != typeof(DemoClsDataGridview.ClsDataGirdView))
                        errExtraCharge.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                return false;
            }

            return true;
        }

        private void fillParameters()
        {
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.lngExtraChargePaymentID = txtExtraChargePaymentNo.Tag.ToInt64();
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.strExtraChargePaymentNo = txtExtraChargePaymentNo.Text;
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.dtExtraChargePaymentDate = dtpExtraChargePaymentDate.Value.ToString("dd MMM yyyy").ToDateTime();
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intCompanyID = cboCompany.SelectedValue.ToInt32();
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intCurrencyID = cboCurrency.SelectedValue.ToInt32();
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intOperationTypeID = cboOperationType.SelectedValue.ToInt32();
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.lngReferenceID = cboReferenceNumber.SelectedValue.ToInt64();
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intVendorID = cboShipmentVendor.SelectedValue.ToInt32();
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.lngExtraChargeID = cboExtraChargeNo.SelectedValue.ToInt64();
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intPaymentModeID = cboPaymentMode.SelectedValue.ToInt32();
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.intAccountID = cboAccount.SelectedValue.ToInt32();
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.strChNo = txtChNo.Text;
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.dtChDate = dtpChDate.Value.ToString("dd MMM yyyy").ToDateTime();
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.strDescription = txtDescription.Text;
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.decTotalAmount = txtTotalAmount.Text.ToDecimal();
        }

        private DataTable FillDetailParameters()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("ExpenseTypeID");
            datTemp.Columns.Add("Amount");
            for (int iCounter = 0; iCounter <= dgvExtraChargePayment.Rows.Count - 1; iCounter++)
            {
                datTemp.Rows.Add();
                datTemp.Rows[iCounter]["ExpenseTypeID"] = dgvExtraChargePayment.Rows[iCounter].Cells["ExpenseTypeID"].Value;
                datTemp.Rows[iCounter]["Amount"] = dgvExtraChargePayment.Rows[iCounter].Cells["Amount"].Value;
            }

            return datTemp;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bnSaveItem_Click(sender, e);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveExtraChargePayment())
            {
                if (blnAddStatus == true)
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 2, out MmsgMessageIcon);
                else
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 21, out MmsgMessageIcon);

                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                
                this.Close();
            }            
        }

        private void txtExtraChargePaymentNo_TextChanged(object sender, EventArgs e)
        {
            setEnableDisable();
        }

        private void dtpExtraChargePaymentDate_ValueChanged(object sender, EventArgs e)
        {
            setEnableDisable();
        }

        private void cboReferenceNumber_SelectedIndexChanged(object sender, EventArgs e)
        {            
            getShipmentVendor();
            setEnableDisable();
        }

        private void cboShipmentVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (PintOperationType > 0 && PlngReferenceID > 0 && PlngExtraChargeID > 0)
            //    fillGrid();
            //else
            //    cboExtraChargeNo.SelectedIndex = -1;

            getExtraChargeNo();
            setEnableDisable();
        }

        private void cboExtraChargeNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboExtraChargeNo.SelectedIndex >= 0)
                cboCurrency.SelectedValue = MobjclsBLLExtraChargePayment.getCurrency(cboExtraChargeNo.SelectedValue.ToInt64());

            fillGrid();
            setEnableDisable();
        }

        private void fillGrid()
        {
            dgvExtraChargePayment.Rows.Clear();
            if (cboExtraChargeNo.SelectedIndex >= 0)
            {
                DataTable datTemp = MobjclsBLLExtraChargePayment.getExtraChargeDetails(cboShipmentVendor.SelectedValue.ToInt32(), cboExtraChargeNo.SelectedValue.ToInt64());
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                        {
                            dgvExtraChargePayment.RowCount++;
                            dgvExtraChargePayment.Rows[iCounter].Cells["SerialNo"].Value = datTemp.Rows[iCounter]["SerialNo"].ToInt32();
                            dgvExtraChargePayment.Rows[iCounter].Cells["ExpenseTypeID"].Value = datTemp.Rows[iCounter]["ExpenseTypeID"].ToInt32();
                            dgvExtraChargePayment.Rows[iCounter].Cells["ExpenseType"].Value = datTemp.Rows[iCounter]["ExpenseType"].ToString();
                            dgvExtraChargePayment.Rows[iCounter].Cells["TotalAmount"].Value = datTemp.Rows[iCounter]["TotalAmount"].ToDecimal();
                            decimal decTempCurrentBal = MobjclsBLLExtraChargePayment.getCurrentBalance(cboExtraChargeNo.SelectedValue.ToInt64(),
                                datTemp.Rows[iCounter]["ExpenseTypeID"].ToInt32(), cboShipmentVendor.SelectedValue.ToInt32());
                            dgvExtraChargePayment.Rows[iCounter].Cells["CurrentBalance"].Value = decTempCurrentBal;
                            dgvExtraChargePayment.Rows[iCounter].Cells["Amount"].Value = 0;
                            dgvExtraChargePayment.Rows[iCounter].Cells["EntryAmount"].Value = 0;
                        }
                    }
                }
            }
        }

        private void cboPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboPaymentMode.SelectedIndex >= 0)
            {
                if (cboPaymentMode.SelectedValue.ToInt32() == 1)
                    cboAccount.Enabled = BtnAccount.Enabled = txtChNo.Enabled = dtpChDate.Enabled = true;
                else
                {
                    cboAccount.SelectedIndex = -1;
                    txtChNo.Text = "";
                    dtpChDate.Value = ClsCommonSettings.GetServerDate();
                    cboAccount.Enabled = BtnAccount.Enabled = txtChNo.Enabled = dtpChDate.Enabled = false;
                }
            }
            setEnableDisable();
        }

        private void txtTotalAmount_TextChanged(object sender, EventArgs e)
        {
            if (cboCurrency.SelectedIndex >= 0)
            {
                if (txtTotalAmount.Text.ToDecimal() > 0)
                    lblAmountInWords.Text = "Amount In Words " + MobjclsBLLExtraChargePayment.ConvertToWord(txtTotalAmount.Text, cboCurrency.SelectedValue.ToInt32());
                else
                    lblAmountInWords.Text = "Amount In Words";
            }
            else
                lblAmountInWords.Text = "Amount In Words";

            setEnableDisable();
        }

        private void cboAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            setEnableDisable();
        }

        private void txtChNo_TextChanged(object sender, EventArgs e)
        {
            setEnableDisable();
        }

        private void dtpChDate_ValueChanged(object sender, EventArgs e)
        {
            setEnableDisable();
        }

        private void txtDescription_TextChanged(object sender, EventArgs e)
        {
            setEnableDisable();
        }

        private void bnDeleteItem_Click(object sender, EventArgs e)
        {
            if (txtExtraChargePaymentNo.Tag.ToInt64() > 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 13, out MmsgMessageIcon);
                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    if (DeleteExtraChargePayment())
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MdatMessages, 4, out MmsgMessageIcon);
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();

                        bnAddNewItem_Click(sender, e);
                    }
                }
            }
        }

        private bool DeleteExtraChargePayment()
        {
            bool blnStatus = false;
            MobjclsBLLExtraChargePayment.objDTOExtraChargePayment.lngExtraChargePaymentID = txtExtraChargePaymentNo.Tag.ToInt64();

            if (MobjclsBLLExtraChargePayment.DeleteExtraChargePayment())
                blnStatus = true;

            return blnStatus;
        }        

        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            ClearControls();
            if (PintOperationType > 0 && PlngReferenceID > 0 && PlngExtraChargeID > 0)
                cboExtraChargeNo_SelectedIndexChanged(sender, e);
        }

        private void bnCancel_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAccount_Click(object sender, EventArgs e)        {
            
            using (FrmChartOfAccounts objChartOfAccounts = new FrmChartOfAccounts())
            {
                try
                {
                    // user will only be able to create account head or group under the account group that we set here
                    objChartOfAccounts.PermittedAccountGroup = AccountGroups.BankAccounts;
                    if (this.cboAccount.SelectedIndex != -1)
                        objChartOfAccounts.SelectedAccount = (Accounts)this.cboAccount.SelectedValue.ToInt32();

                    objChartOfAccounts.MbCalledFromAccountSettings = true;
                }
                catch { }
                objChartOfAccounts.ShowDialog();
            }
            
            int intComboID = cboAccount.SelectedValue.ToInt32();
            LoadCombo(5);
            cboAccount.SelectedValue = intComboID;
        }

        private void dgvExtraChargePayment_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvExtraChargePayment.Columns["Amount"].Index)
                    txtTotalAmount.Text = getTotalByExchangeRate();
            }
            setEnableDisable();
        }

        private void dgvExtraChargePayment_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgvExtraChargePayment.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {
            if (txtExtraChargePaymentNo.Tag.ToInt64() > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = txtExtraChargePaymentNo.Tag.ToInt64();
                ObjViewer.PiFormID = (int)FormID.ExtraChargePayment;
                ObjViewer.ShowDialog();
            }
        }

        private void bnEmail_Click(object sender, EventArgs e)
        {
            if (txtExtraChargePaymentNo.Tag.ToInt64() > 0)
            {
                using (FrmEmailPopup objEmailPopUp = new FrmEmailPopup())
                {
                    objEmailPopUp.MsSubject = "Extra Charge Payment Information";
                    objEmailPopUp.EmailFormType = EmailFormID.ExtraChargePayment;
                    objEmailPopUp.EmailSource = MobjclsBLLExtraChargePayment.DisplayExtraChargePayment(txtExtraChargePaymentNo.Tag.ToInt64());
                    objEmailPopUp.ShowDialog();
                }
            }
        }

        private void dgvExtraChargePayment_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            this.dgvExtraChargePayment.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvExtraChargePayment.CurrentCell.OwningColumn.Name == "Amount")
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (dgvExtraChargePayment.EditingControl != null && !string.IsNullOrEmpty(dgvExtraChargePayment.EditingControl.Text) &&
                    dgvExtraChargePayment.EditingControl.Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
        }
    }
}