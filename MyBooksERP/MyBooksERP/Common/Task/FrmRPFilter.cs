﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MyBooksERP;

namespace MyBooksERP 
{
    public partial class frmRPFilter : DevComponents.DotNetBar.Office2007Form
    {
        clsBLLPaymentMaster MobjClsBLLRPFilter;//obj of bll payment master
        ClsLogWriter MobjClsRPFilterLogWriter;// obj of log writer
        public int intFormType { get; set; }
        public int CompanyID;
        public DataSet dtsRecPay { get; set; }
        public int intModuleID = 0;
        public string strFilterCondition { get; set; }
        public frmRPFilter()
        {
            InitializeComponent();
        }

        public frmRPFilter(int CID, string FType)
        {
            InitializeComponent();
            CompanyID = CID;
            if (FType == "Receipts")
                intFormType = 1;
            else
                intFormType = 0;
            MobjClsBLLRPFilter = new clsBLLPaymentMaster();
            MobjClsRPFilterLogWriter = new ClsLogWriter(Application.StartupPath);
        }

        private void frmRPFilter_Load(object sender, EventArgs e)
        {
            LoadCombos(0);
            cboOperationType.SelectedIndex = -1;
            cboReferenceNumber.SelectedIndex = -1;
            cboCustomer.SelectedIndex = -1;
            cboPaymentType.SelectedIndex = -1;            
            dtpFromDate.Value = dtpToDate.Value = ClsCommonSettings.GetServerDate();
            dtpFromDate.Checked = false;
            dtpToDate.Checked = false;
            strFilterCondition = "";
            cboNo.SelectedIndex = -1;
        }

        private void LoadCombos(int intType)
        {
            DataTable datCombos = null;
            if (intType == 0 || intType == 1)
            {
                datCombos = null;
                string strCondition = "";
                if (intModuleID == 1)
                {
                    strCondition = "OperationTypeID In (" + (int)OperationType.PurchaseOrder + "," + (int)OperationType.PurchaseInvoice + ")";
                }
                else if(intModuleID ==2)
                {
                    strCondition = "OperationTypeID In (" + (int)OperationType.SalesOrder + "," + (int)OperationType.SalesInvoice +"," + (int)OperationType.POS+")";
                }
                else if (intModuleID == 3)
                {
                    strCondition = "OperationTypeID In (" + (int)OperationType.StockTransfer + ")";
                }
                datCombos = MobjClsBLLRPFilter.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", strCondition });                
                cboOperationType.DisplayMember = "OperationType";
                cboOperationType.ValueMember = "OperationTypeID";
                cboOperationType.DataSource = datCombos;
            }

            if (intType == 0 || intType == 2)
            {
                datCombos = MobjClsBLLRPFilter.FillCombos(new string[] { "ReceiptAndPaymentTypeID,ReceiptAndPaymentType", "InvReceiptAndPaymentTypeReference", "" });
                cboPaymentType.DisplayMember = "ReceiptAndPaymentType";
                cboPaymentType.ValueMember = "ReceiptAndPaymentTypeID";
                cboPaymentType.DataSource = datCombos;
            }

            datCombos = null;
            if (intFormType == 0)
            {
                lblCustomer.Text = "Supplier";
                datCombos = MobjClsBLLRPFilter.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", "VendorTypeID="+(int)VendorType.Supplier+"" });
            }
            else
            {
                lblCustomer.Text = "Customer";
                datCombos = MobjClsBLLRPFilter.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", "VendorTypeID="+(int)VendorType.Customer+"" });
            }
            cboCustomer.ValueMember = "VendorID";
            cboCustomer.DisplayMember = "VendorName";
            cboCustomer.DataSource = datCombos;            
        }

        private void cboOperationType_TextChanged(object sender, EventArgs e)
        {            
            FillRefNoAndPaymentType();
        }

        private void FillRefNoAndPaymentType()
        {
            try
            {
                if (cboOperationType.SelectedValue != null)
                {
                    int intSelectedValue = Convert.ToInt32(cboOperationType.SelectedValue);
                    DataTable datCombos = null;
                    DataTable datPaymentTypes = MobjClsBLLRPFilter.FillCombos(new string[] { "ReceiptAndPaymentTypeID,ReceiptAndPaymentType", "InvReceiptAndPaymentTypeReference", "" });
                    switch (intSelectedValue)
                    {
                        case (Int32)OperationType.PurchaseOrder:
                            datCombos = null;
                            cboReferenceNumber.DataSource = datCombos;
                         //   if (MobjClsBLLRPFilter.objDTOPaymentMaster.intPaymentID == 0)
                           //     datCombos = MobjClsBLLRPFilter.FillCombos(new string[] { "PurchaseOrderID,PurchaseOrderNo", "InvPurchaseOrderMaster", "StatusID = " + Convert.ToInt32(OperationStatusType.POrderApproved) + " And PurchaseOrderID Not In (Select PurchaseOrderID From InvPurchaseInvoiceMaster Where OrderTypeID = " + (int)OperationOrderType.PInvoiceFromOrder + ") And CompanyID = " + CompanyID + "" });
                          //  else
                                datCombos = MobjClsBLLRPFilter.FillCombos(new string[] { "PurchaseOrderID,PurchaseOrderNo", "InvPurchaseOrderMaster", "CompanyID = " + CompanyID + "" });
                            cboReferenceNumber.DisplayMember = "PurchaseOrderNo";
                            cboReferenceNumber.ValueMember = "PurchaseOrderID";
                            cboReferenceNumber.DataSource = datCombos;

                            datPaymentTypes.DefaultView.RowFilter = "ReceiptAndPaymentTypeID IN('" + (int)PaymentTypes.AdvancePayment + "','" + (int)PaymentTypes.DirectExpense + "')";
                            cboPaymentType.DataSource = datPaymentTypes.DefaultView.ToTable();
                            break;
                        case (Int32)OperationType.PurchaseInvoice:
                            datCombos = null;
                            cboReferenceNumber.DataSource = datCombos;
                            //if (MobjClsBLLRPFilter.objDTOPaymentMaster.intPaymentID == 0)
                            //    datCombos = MobjClsBLLRPFilter.FillCombos(new string[] { "PurchaseInvoiceID,PurchaseInvoiceNo", "InvPurchaseInvoiceMaster", "StatusID <> " + Convert.ToInt32(OperationStatusType.PInvoiceCancelled) + " And CompanyID = " + CompanyID + "" });
                            //else
                                datCombos = MobjClsBLLRPFilter.FillCombos(new string[] { "PurchaseInvoiceID,PurchaseInvoiceNo", "InvPurchaseInvoiceMaster", "CompanyID = " + CompanyID + "" });
                            cboReferenceNumber.DisplayMember = "PurchaseInvoiceNo";
                            cboReferenceNumber.ValueMember = "PurchaseInvoiceID";
                            cboReferenceNumber.DataSource = datCombos;

                            datPaymentTypes.DefaultView.RowFilter = "ReceiptAndPaymentTypeID IN('" + (int)PaymentTypes.InvoicePayment + "','" + (int)PaymentTypes.DirectExpense + "')";
                            cboPaymentType.DataSource = datPaymentTypes.DefaultView.ToTable();
                            break;
                        case (Int32)OperationType.StockTransfer:
                            datCombos = null;
                            cboReferenceNumber.DataSource = datCombos;
                            datCombos = MobjClsBLLRPFilter.FillCombos(new string[] { "StockTransferID,StockTransferNo", "InvStockTransferMaster", "" });
                            cboReferenceNumber.DisplayMember = "StockTransferNo";
                            cboReferenceNumber.ValueMember = "StockTransferID";
                            cboReferenceNumber.DataSource = datCombos;

                            datPaymentTypes.DefaultView.RowFilter = "ReceiptAndPaymentTypeID IN('" + (int)PaymentTypes.DirectExpense + "','" + (int)PaymentTypes.InvoiceExpense + "')";
                            cboPaymentType.DataSource = datPaymentTypes.DefaultView.ToTable();
                            break;
                        case (Int32)OperationType.SalesOrder:
                            datCombos = null;
                            cboReferenceNumber.DataSource = datCombos;
                            //if (MobjClsBLLRPFilter.objDTOPaymentMaster.intPaymentID == 0)
                            //    datCombos = MobjClsBLLRPFilter.FillCombos(new string[] { "SalesOrderID,SalesOrderNo", "InvSalesOrderMaster", "StatusID = " + (Int32)OperationStatusType.SOrderOpen + " And CompanyID = " + CompanyID + "" });
                            //else
                                datCombos = MobjClsBLLRPFilter.FillCombos(new string[] { "SalesOrderID,SalesOrderNo", "InvSalesOrderMaster", "CompanyID = " + CompanyID + "" });
                            cboReferenceNumber.DisplayMember = "SalesOrderNo";
                            cboReferenceNumber.ValueMember = "SalesOrderID";
                            cboReferenceNumber.DataSource = datCombos;

                            datPaymentTypes.DefaultView.RowFilter = "ReceiptAndPaymentTypeID = '" + (int)PaymentTypes.AdvancePayment + "'";
                            cboPaymentType.DataSource = datPaymentTypes.DefaultView.ToTable();
                            break;
                        case (Int32)OperationType.SalesInvoice:
                            datCombos = null;
                            cboReferenceNumber.DataSource = datCombos;
                            datCombos = MobjClsBLLRPFilter.FillCombos(new string[] { "SalesInvoiceID,SalesInvoiceNo", "InvSalesInvoiceMaster", "CompanyID = " + CompanyID + " And StatusID <> " + (Int32)OperationStatusType.SInvoiceCancelled });
                            cboReferenceNumber.DisplayMember = "SalesInvoiceNo";
                            cboReferenceNumber.ValueMember = "SalesInvoiceID";
                            cboReferenceNumber.DataSource = datCombos;

                            datPaymentTypes.DefaultView.RowFilter = "ReceiptAndPaymentTypeID IN ('" + (int)PaymentTypes.InvoicePayment + "','" + (int)PaymentTypes.DirectExpense + "')";
                            cboPaymentType.DataSource = datPaymentTypes.DefaultView.ToTable();
                            break;
                        case (Int32)OperationType.POS:
                            datCombos = null;
                            cboReferenceNumber.DataSource = datCombos;
                            //if (MobjClsBLLRPFilter.objDTOPaymentMaster.intPaymentID == 0)
                            //    datCombos = MobjClsBLLRPFilter.FillCombos(new string[] { "PointOfSalesID,SalesNo", "InvPOSMaster", "StatusID = " + (Int32)OperationStatusType.SPOSDeliverd + " And CompanyID = " + CompanyID + "" });
                            //else
                                datCombos = MobjClsBLLRPFilter.FillCombos(new string[] { "POSID,POSNo", "InvPOSMaster", "CompanyID = " + CompanyID + "" });
                            cboReferenceNumber.DisplayMember = "POSNo";
                            cboReferenceNumber.ValueMember = "POSID";
                            cboReferenceNumber.DataSource = datCombos;

                            datPaymentTypes.DefaultView.RowFilter = "ReceiptAndPaymentTypeID IN('" + (int)PaymentTypes.InvoicePayment + "')";
                            cboPaymentType.DataSource = datPaymentTypes.DefaultView.ToTable();
                            break;
                        default:
                            cboReferenceNumber.DataSource = null;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in cboOperationType_SelectedIndexChanged() " + ex.Message);
                MobjClsRPFilterLogWriter.WriteLog("Error in cboOperationType_SelectedIndexChanged() " + ex.Message, 2);
            }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            //dtsRecPay = MobjClsBLLRPFilter.GetFilterDetails(GetFilterString());
            strFilterCondition = GetFilterString();
            this.DialogResult = DialogResult.OK;
            this.Close();
           
        }

        private string GetFilterString()
        {try{
            string STRFilter = "";
            if (Convert.ToInt32(cboOperationType.SelectedValue) > 0)
            {
                STRFilter = "IsReceipt=" + intFormType + " AND OperationTypeID=" + cboOperationType.SelectedValue.ToString();
            }
            if (Convert.ToInt32(cboReferenceNumber.SelectedValue) > 0)
            {
                if (STRFilter == "")
                    STRFilter = "IsReceipt=" + intFormType + " AND ReferenceID=" + cboReferenceNumber.SelectedValue.ToString();
                else
                    STRFilter += " AND ReferenceID=" + cboReferenceNumber.SelectedValue.ToString();
            }
            if (Convert.ToInt32(cboPaymentType.SelectedValue.ToInt32()) > 0)
            {
                if (STRFilter == "")
                    STRFilter = "IsReceipt=" + intFormType + " AND ReceiptAndPaymentTypeID=" + cboPaymentType.SelectedValue.ToString();
                else
                    STRFilter += " AND ReceiptAndPaymentTypeID=" + cboPaymentType.SelectedValue.ToString();
            }
            if (Convert.ToInt32(cboCustomer.SelectedValue) > 0)
            {
                if (STRFilter == "")
                    STRFilter = "IsReceipt=" + intFormType + " AND VendorID=" + cboCustomer.SelectedValue.ToString();
                else
                    STRFilter += " AND VendorID=" + cboCustomer.SelectedValue.ToString();
            }
            if (dtpFromDate.Checked == true && dtpToDate.Checked == true)
            {
                if (STRFilter == "")
                    STRFilter = "IsReceipt=" + intFormType + " AND PaymentDate >= '" + dtpFromDate.Text + "' AND PaymentDate <= '" + dtpToDate.Text + "'";
                else
                    STRFilter += " AND PaymentDate >= '" + dtpFromDate.Text + "' AND PaymentDate <= '" + dtpToDate.Text + "'";
            }
            else if (dtpFromDate.Checked == true)
            {
                if (STRFilter == "")
                    STRFilter = "IsReceipt=" + intFormType + " AND PaymentDate>='" + dtpFromDate.Text + "'";
                else
                    STRFilter += " AND PaymentDate='" + dtpFromDate.Text + "'";
            }
            else if (dtpToDate.Checked == true)
            {
                if (STRFilter == "")
                    STRFilter = "IsReceipt=" + intFormType + " AND PaymentDate<='" + dtpToDate.Text + "'";
                else
                    STRFilter += " AND PaymentDate='" + dtpToDate.Text + "'";
            }
            if(cboNo.SelectedIndex!=-1)
            {
                if (cboNo.SelectedValue.ToInt32() > 0)
                {
                    if (STRFilter == "")
                        STRFilter = "ReceiptAndPaymentID = " + cboNo.SelectedValue;
                    else
                        STRFilter += " AND ReceiptAndPaymentID = " + cboNo.SelectedValue;
                }
            }

            if (STRFilter == "")
            {
                STRFilter = "IsReceipt=" + intFormType;
            }
            return STRFilter;
        }
        catch (Exception ex)
        {
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error in GetFilterString() " + ex.Message);
            MobjClsRPFilterLogWriter.WriteLog("Error in GetFilterString() " + ex.Message, 2);
            return "";
        }
        }

        private void cboOperationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboNo.SelectedIndex = -1;
            fillNoCombo();
        }
        private void fillNoCombo()
        {
        try
         {
            DataSet dtsNo = MobjClsBLLRPFilter.GetFilterDetails(GetFilterString());
            DataView dv = dtsNo.Tables[0].DefaultView;
            DataTable datNo = dv.ToTable(true, new string[] { "ReceiptAndPaymentID", "PaymentNumber" });
            DataRow dr = datNo.NewRow();
            dr["PaymentNumber"] = "ALL";
            dr["ReceiptAndPaymentID"] = "0";
            datNo.Rows.InsertAt(dr, 0);
            cboNo.DisplayMember = "PaymentNumber";
            cboNo.ValueMember = "ReceiptAndPaymentID";
            cboNo.DataSource = datNo;
        }
        catch (Exception ex)
        {
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error in fillNoCombo() " + ex.Message);
            MobjClsRPFilterLogWriter.WriteLog("Error in fillNoCombo() " + ex.Message, 2);
        }
        }

        private void cboReferenceNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboNo.SelectedIndex = -1;
            fillNoCombo();
        }

        private void cboPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboNo.SelectedIndex = -1;
            fillNoCombo();
        }

        private void cboCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboNo.SelectedIndex = -1;
            fillNoCombo();
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            cboNo.SelectedIndex = -1;
            fillNoCombo();
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            cboNo.SelectedIndex = -1;
            fillNoCombo();
        }
    }
}
