﻿namespace MyBooksERP
{
    partial class frmExtraChargePayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExtraChargePayment));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ExpenseBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bnMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bnMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bnPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bnCancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.bnEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnHelp = new System.Windows.Forms.ToolStripButton();
            this.errExtraCharge = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlExpense = new System.Windows.Forms.Panel();
            this.BtnAccount = new System.Windows.Forms.Button();
            this.cboOperationType = new System.Windows.Forms.ComboBox();
            this.lblItemCode = new System.Windows.Forms.Label();
            this.lblAmountInWords = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpChDate = new System.Windows.Forms.DateTimePicker();
            this.txtChNo = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cboAccount = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cboPaymentMode = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cboExtraChargeNo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboShipmentVendor = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpExtraChargePaymentDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txtExtraChargePaymentNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.LblCompany = new System.Windows.Forms.Label();
            this.cboReferenceNumber = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTotalAmount = new DemoClsDataGridview.DecimalTextBox();
            this.dgvExtraChargePayment = new DemoClsDataGridview.ClsDataGirdView();
            this.SerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpenseTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpenseType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EntryAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblShortName = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.ExpenseBindingNavigator)).BeginInit();
            this.ExpenseBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errExtraCharge)).BeginInit();
            this.pnlExpense.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExtraChargePayment)).BeginInit();
            this.ssStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // ExpenseBindingNavigator
            // 
            this.ExpenseBindingNavigator.AddNewItem = null;
            this.ExpenseBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.ExpenseBindingNavigator.CountItem = this.bnCountItem;
            this.ExpenseBindingNavigator.DeleteItem = null;
            this.ExpenseBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnMoveFirstItem,
            this.bnMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bnPositionItem,
            this.bnCountItem,
            this.bindingNavigatorSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnDeleteItem,
            this.bnCancel,
            this.toolStripSeparator2,
            this.bnPrint,
            this.bnEmail,
            this.toolStripSeparator1,
            this.bnHelp});
            this.ExpenseBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.ExpenseBindingNavigator.MoveFirstItem = null;
            this.ExpenseBindingNavigator.MoveLastItem = null;
            this.ExpenseBindingNavigator.MoveNextItem = null;
            this.ExpenseBindingNavigator.MovePreviousItem = null;
            this.ExpenseBindingNavigator.Name = "ExpenseBindingNavigator";
            this.ExpenseBindingNavigator.PositionItem = this.bnPositionItem;
            this.ExpenseBindingNavigator.Size = new System.Drawing.Size(670, 25);
            this.ExpenseBindingNavigator.TabIndex = 84;
            this.ExpenseBindingNavigator.Text = "bindingNavigator1";
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.Size = new System.Drawing.Size(35, 22);
            this.bnCountItem.Text = "of {0}";
            this.bnCountItem.ToolTipText = "Total number of items";
            // 
            // bnMoveFirstItem
            // 
            this.bnMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            this.bnMoveFirstItem.Name = "bnMoveFirstItem";
            this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveFirstItem.Text = "Move first";
            this.bnMoveFirstItem.Click += new System.EventHandler(this.bnMoveFirstItem_Click);
            // 
            // bnMovePreviousItem
            // 
            this.bnMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem.Image")));
            this.bnMovePreviousItem.Name = "bnMovePreviousItem";
            this.bnMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bnMovePreviousItem.Text = "Move previous";
            this.bnMovePreviousItem.Click += new System.EventHandler(this.bnMovePreviousItem_Click);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPositionItem
            // 
            this.bnPositionItem.AccessibleName = "Position";
            this.bnPositionItem.AutoSize = false;
            this.bnPositionItem.Name = "bnPositionItem";
            this.bnPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bnPositionItem.Text = "0";
            this.bnPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Move next";
            this.bnMoveNextItem.Click += new System.EventHandler(this.bnMoveNextItem_Click);
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Move last";
            this.bnMoveLastItem.Click += new System.EventHandler(this.bnMoveLastItem_Click);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewItem.ToolTipText = "Add new Item";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bnSaveItem.ToolTipText = "Save Item";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bnDeleteItem.Image")));
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.ToolTipText = "Delete Item";
            this.bnDeleteItem.Click += new System.EventHandler(this.bnDeleteItem_Click);
            // 
            // bnCancel
            // 
            this.bnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnCancel.Image = ((System.Drawing.Image)(resources.GetObject("bnCancel.Image")));
            this.bnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnCancel.Name = "bnCancel";
            this.bnCancel.Size = new System.Drawing.Size(23, 22);
            this.bnCancel.ToolTipText = "Clear";
            this.bnCancel.Click += new System.EventHandler(this.bnCancel_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPrint
            // 
            this.bnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnPrint.Image = ((System.Drawing.Image)(resources.GetObject("bnPrint.Image")));
            this.bnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(23, 22);
            this.bnPrint.ToolTipText = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // bnEmail
            // 
            this.bnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnEmail.Image = ((System.Drawing.Image)(resources.GetObject("bnEmail.Image")));
            this.bnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Size = new System.Drawing.Size(23, 22);
            this.bnEmail.ToolTipText = "Email";
            this.bnEmail.Click += new System.EventHandler(this.bnEmail_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnHelp
            // 
            this.bnHelp.Enabled = false;
            this.bnHelp.Image = ((System.Drawing.Image)(resources.GetObject("bnHelp.Image")));
            this.bnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnHelp.Name = "bnHelp";
            this.bnHelp.Size = new System.Drawing.Size(23, 22);
            this.bnHelp.ToolTipText = "Help";
            // 
            // errExtraCharge
            // 
            this.errExtraCharge.ContainerControl = this;
            // 
            // pnlExpense
            // 
            this.pnlExpense.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlExpense.BackColor = System.Drawing.Color.Transparent;
            this.pnlExpense.Controls.Add(this.BtnAccount);
            this.pnlExpense.Controls.Add(this.cboOperationType);
            this.pnlExpense.Controls.Add(this.lblItemCode);
            this.pnlExpense.Controls.Add(this.lblAmountInWords);
            this.pnlExpense.Controls.Add(this.txtDescription);
            this.pnlExpense.Controls.Add(this.label12);
            this.pnlExpense.Controls.Add(this.dtpChDate);
            this.pnlExpense.Controls.Add(this.txtChNo);
            this.pnlExpense.Controls.Add(this.label11);
            this.pnlExpense.Controls.Add(this.cboAccount);
            this.pnlExpense.Controls.Add(this.label10);
            this.pnlExpense.Controls.Add(this.cboPaymentMode);
            this.pnlExpense.Controls.Add(this.label9);
            this.pnlExpense.Controls.Add(this.cboExtraChargeNo);
            this.pnlExpense.Controls.Add(this.label8);
            this.pnlExpense.Controls.Add(this.cboCurrency);
            this.pnlExpense.Controls.Add(this.label7);
            this.pnlExpense.Controls.Add(this.cboShipmentVendor);
            this.pnlExpense.Controls.Add(this.label5);
            this.pnlExpense.Controls.Add(this.dtpExtraChargePaymentDate);
            this.pnlExpense.Controls.Add(this.label4);
            this.pnlExpense.Controls.Add(this.txtExtraChargePaymentNo);
            this.pnlExpense.Controls.Add(this.label2);
            this.pnlExpense.Controls.Add(this.cboCompany);
            this.pnlExpense.Controls.Add(this.LblCompany);
            this.pnlExpense.Controls.Add(this.cboReferenceNumber);
            this.pnlExpense.Controls.Add(this.label3);
            this.pnlExpense.Controls.Add(this.txtTotalAmount);
            this.pnlExpense.Controls.Add(this.dgvExtraChargePayment);
            this.pnlExpense.Controls.Add(this.label1);
            this.pnlExpense.Controls.Add(this.label6);
            this.pnlExpense.Controls.Add(this.lblShortName);
            this.pnlExpense.Controls.Add(this.shapeContainer1);
            this.pnlExpense.Location = new System.Drawing.Point(8, 28);
            this.pnlExpense.Name = "pnlExpense";
            this.pnlExpense.Size = new System.Drawing.Size(655, 435);
            this.pnlExpense.TabIndex = 85;
            this.pnlExpense.Tag = "";
            // 
            // BtnAccount
            // 
            this.BtnAccount.Location = new System.Drawing.Point(266, 305);
            this.BtnAccount.Name = "BtnAccount";
            this.BtnAccount.Size = new System.Drawing.Size(32, 23);
            this.BtnAccount.TabIndex = 36;
            this.BtnAccount.Text = "...";
            this.BtnAccount.UseVisualStyleBackColor = true;
            this.BtnAccount.Click += new System.EventHandler(this.BtnAccount_Click);
            // 
            // cboOperationType
            // 
            this.cboOperationType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOperationType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOperationType.BackColor = System.Drawing.SystemColors.Info;
            this.cboOperationType.DropDownHeight = 75;
            this.cboOperationType.ForeColor = System.Drawing.Color.Black;
            this.cboOperationType.FormattingEnabled = true;
            this.cboOperationType.IntegralHeight = false;
            this.cboOperationType.Location = new System.Drawing.Point(107, 106);
            this.cboOperationType.Name = "cboOperationType";
            this.cboOperationType.Size = new System.Drawing.Size(188, 21);
            this.cboOperationType.TabIndex = 34;
            this.cboOperationType.SelectedIndexChanged += new System.EventHandler(this.cboOperationType_SelectedIndexChanged);
            this.cboOperationType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboOperationType_KeyPress);
            // 
            // lblItemCode
            // 
            this.lblItemCode.AutoSize = true;
            this.lblItemCode.Location = new System.Drawing.Point(13, 109);
            this.lblItemCode.Name = "lblItemCode";
            this.lblItemCode.Size = new System.Drawing.Size(80, 13);
            this.lblItemCode.TabIndex = 35;
            this.lblItemCode.Text = "Operation Type";
            // 
            // lblAmountInWords
            // 
            this.lblAmountInWords.AutoSize = true;
            this.lblAmountInWords.Location = new System.Drawing.Point(14, 337);
            this.lblAmountInWords.Name = "lblAmountInWords";
            this.lblAmountInWords.Size = new System.Drawing.Size(89, 13);
            this.lblAmountInWords.TabIndex = 33;
            this.lblAmountInWords.Text = "Amount In Words";
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtDescription.Location = new System.Drawing.Point(7, 372);
            this.txtDescription.MaxLength = 200;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(640, 56);
            this.txtDescription.TabIndex = 6;
            this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(4, 356);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "Remarks";
            // 
            // dtpChDate
            // 
            this.dtpChDate.CustomFormat = "dd MMM yyyy";
            this.dtpChDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpChDate.Location = new System.Drawing.Point(543, 306);
            this.dtpChDate.Name = "dtpChDate";
            this.dtpChDate.Size = new System.Drawing.Size(106, 20);
            this.dtpChDate.TabIndex = 31;
            this.dtpChDate.ValueChanged += new System.EventHandler(this.dtpChDate_ValueChanged);
            // 
            // txtChNo
            // 
            this.txtChNo.Location = new System.Drawing.Point(409, 307);
            this.txtChNo.Name = "txtChNo";
            this.txtChNo.Size = new System.Drawing.Size(128, 20);
            this.txtChNo.TabIndex = 30;
            this.txtChNo.TextChanged += new System.EventHandler(this.txtChNo_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(325, 310);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Ch. No. && Date";
            // 
            // cboAccount
            // 
            this.cboAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAccount.DropDownHeight = 75;
            this.cboAccount.ForeColor = System.Drawing.Color.Black;
            this.cboAccount.FormattingEnabled = true;
            this.cboAccount.IntegralHeight = false;
            this.cboAccount.Location = new System.Drawing.Point(108, 307);
            this.cboAccount.Name = "cboAccount";
            this.cboAccount.Size = new System.Drawing.Size(152, 21);
            this.cboAccount.TabIndex = 28;
            this.cboAccount.SelectedIndexChanged += new System.EventHandler(this.cboAccount_SelectedIndexChanged);
            this.cboAccount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboAccount_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 310);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "Account";
            // 
            // cboPaymentMode
            // 
            this.cboPaymentMode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPaymentMode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPaymentMode.BackColor = System.Drawing.SystemColors.Info;
            this.cboPaymentMode.DropDownHeight = 75;
            this.cboPaymentMode.ForeColor = System.Drawing.Color.Black;
            this.cboPaymentMode.FormattingEnabled = true;
            this.cboPaymentMode.IntegralHeight = false;
            this.cboPaymentMode.Location = new System.Drawing.Point(108, 280);
            this.cboPaymentMode.Name = "cboPaymentMode";
            this.cboPaymentMode.Size = new System.Drawing.Size(188, 21);
            this.cboPaymentMode.TabIndex = 26;
            this.cboPaymentMode.SelectedIndexChanged += new System.EventHandler(this.cboPaymentMode_SelectedIndexChanged);
            this.cboPaymentMode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboPaymentMode_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 283);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Payment Mode";
            // 
            // cboExtraChargeNo
            // 
            this.cboExtraChargeNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboExtraChargeNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboExtraChargeNo.BackColor = System.Drawing.SystemColors.Info;
            this.cboExtraChargeNo.DropDownHeight = 75;
            this.cboExtraChargeNo.ForeColor = System.Drawing.Color.Black;
            this.cboExtraChargeNo.FormattingEnabled = true;
            this.cboExtraChargeNo.IntegralHeight = false;
            this.cboExtraChargeNo.Location = new System.Drawing.Point(427, 79);
            this.cboExtraChargeNo.Name = "cboExtraChargeNo";
            this.cboExtraChargeNo.Size = new System.Drawing.Size(188, 21);
            this.cboExtraChargeNo.TabIndex = 24;
            this.cboExtraChargeNo.SelectedIndexChanged += new System.EventHandler(this.cboExtraChargeNo_SelectedIndexChanged);
            this.cboExtraChargeNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboExtraChargeNo_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(333, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Extra Charge No.";
            // 
            // cboCurrency
            // 
            this.cboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCurrency.DropDownHeight = 75;
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cboCurrency.Enabled = false;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.IntegralHeight = false;
            this.cboCurrency.Location = new System.Drawing.Point(427, 106);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(188, 21);
            this.cboCurrency.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(333, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Currency";
            // 
            // cboShipmentVendor
            // 
            this.cboShipmentVendor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboShipmentVendor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboShipmentVendor.BackColor = System.Drawing.SystemColors.Info;
            this.cboShipmentVendor.DropDownHeight = 75;
            this.cboShipmentVendor.ForeColor = System.Drawing.Color.Black;
            this.cboShipmentVendor.FormattingEnabled = true;
            this.cboShipmentVendor.IntegralHeight = false;
            this.cboShipmentVendor.Location = new System.Drawing.Point(427, 52);
            this.cboShipmentVendor.Name = "cboShipmentVendor";
            this.cboShipmentVendor.Size = new System.Drawing.Size(188, 21);
            this.cboShipmentVendor.TabIndex = 19;
            this.cboShipmentVendor.SelectedIndexChanged += new System.EventHandler(this.cboShipmentVendor_SelectedIndexChanged);
            this.cboShipmentVendor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboShipmentVendor_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(333, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Vendor";
            // 
            // dtpExtraChargePaymentDate
            // 
            this.dtpExtraChargePaymentDate.CustomFormat = "dd MMM yyyy";
            this.dtpExtraChargePaymentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExtraChargePaymentDate.Location = new System.Drawing.Point(107, 79);
            this.dtpExtraChargePaymentDate.Name = "dtpExtraChargePaymentDate";
            this.dtpExtraChargePaymentDate.Size = new System.Drawing.Size(106, 20);
            this.dtpExtraChargePaymentDate.TabIndex = 5;
            this.dtpExtraChargePaymentDate.ValueChanged += new System.EventHandler(this.dtpExtraChargePaymentDate_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Payment Date";
            // 
            // txtExtraChargePaymentNo
            // 
            this.txtExtraChargePaymentNo.BackColor = System.Drawing.SystemColors.Info;
            this.txtExtraChargePaymentNo.Location = new System.Drawing.Point(107, 52);
            this.txtExtraChargePaymentNo.Name = "txtExtraChargePaymentNo";
            this.txtExtraChargePaymentNo.Size = new System.Drawing.Size(106, 20);
            this.txtExtraChargePaymentNo.TabIndex = 4;
            this.txtExtraChargePaymentNo.TextChanged += new System.EventHandler(this.txtExtraChargePaymentNo_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Payment No.";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.Location = new System.Drawing.Point(107, 25);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(188, 21);
            this.cboCompany.TabIndex = 0;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCompany_KeyPress);
            // 
            // LblCompany
            // 
            this.LblCompany.AutoSize = true;
            this.LblCompany.Location = new System.Drawing.Point(13, 28);
            this.LblCompany.Name = "LblCompany";
            this.LblCompany.Size = new System.Drawing.Size(51, 13);
            this.LblCompany.TabIndex = 10;
            this.LblCompany.Text = "Company";
            // 
            // cboReferenceNumber
            // 
            this.cboReferenceNumber.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboReferenceNumber.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboReferenceNumber.BackColor = System.Drawing.SystemColors.Info;
            this.cboReferenceNumber.DropDownHeight = 75;
            this.cboReferenceNumber.ForeColor = System.Drawing.Color.Black;
            this.cboReferenceNumber.FormattingEnabled = true;
            this.cboReferenceNumber.IntegralHeight = false;
            this.cboReferenceNumber.Location = new System.Drawing.Point(427, 25);
            this.cboReferenceNumber.Name = "cboReferenceNumber";
            this.cboReferenceNumber.Size = new System.Drawing.Size(188, 21);
            this.cboReferenceNumber.TabIndex = 2;
            this.cboReferenceNumber.SelectedIndexChanged += new System.EventHandler(this.cboReferenceNumber_SelectedIndexChanged);
            this.cboReferenceNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboReferenceNumber_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(333, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Reference No.";
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(500, 280);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.ShortcutsEnabled = false;
            this.txtTotalAmount.Size = new System.Drawing.Size(148, 20);
            this.txtTotalAmount.TabIndex = 8;
            this.txtTotalAmount.TabStop = false;
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalAmount.TextChanged += new System.EventHandler(this.txtTotalAmount_TextChanged);
            // 
            // dgvExtraChargePayment
            // 
            this.dgvExtraChargePayment.AddNewRow = false;
            this.dgvExtraChargePayment.AllowUserToAddRows = false;
            this.dgvExtraChargePayment.AllowUserToDeleteRows = false;
            this.dgvExtraChargePayment.AllowUserToResizeColumns = false;
            this.dgvExtraChargePayment.AllowUserToResizeRows = false;
            this.dgvExtraChargePayment.AlphaNumericCols = new int[0];
            this.dgvExtraChargePayment.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.dgvExtraChargePayment.CapsLockCols = new int[0];
            this.dgvExtraChargePayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvExtraChargePayment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SerialNo,
            this.ExpenseTypeID,
            this.ExpenseType,
            this.TotalAmount,
            this.CurrentBalance,
            this.Amount,
            this.EntryAmount});
            this.dgvExtraChargePayment.DecimalCols = new int[] {
        1,
        2};
            this.dgvExtraChargePayment.HasSlNo = false;
            this.dgvExtraChargePayment.LastRowIndex = 0;
            this.dgvExtraChargePayment.Location = new System.Drawing.Point(7, 151);
            this.dgvExtraChargePayment.Name = "dgvExtraChargePayment";
            this.dgvExtraChargePayment.NegativeValueCols = new int[0];
            this.dgvExtraChargePayment.NumericCols = new int[0];
            this.dgvExtraChargePayment.RowHeadersWidth = 50;
            this.dgvExtraChargePayment.Size = new System.Drawing.Size(640, 123);
            this.dgvExtraChargePayment.TabIndex = 7;
            this.dgvExtraChargePayment.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExtraChargePayment_CellValueChanged);
            this.dgvExtraChargePayment.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvExtraChargePayment_EditingControlShowing);
            this.dgvExtraChargePayment.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvExtraChargePayment_CurrentCellDirtyStateChanged);
            // 
            // SerialNo
            // 
            this.SerialNo.HeaderText = "SerialNumber";
            this.SerialNo.Name = "SerialNo";
            this.SerialNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SerialNo.Visible = false;
            // 
            // ExpenseTypeID
            // 
            this.ExpenseTypeID.HeaderText = "ExpenseTypeID";
            this.ExpenseTypeID.Name = "ExpenseTypeID";
            this.ExpenseTypeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ExpenseTypeID.Visible = false;
            // 
            // ExpenseType
            // 
            this.ExpenseType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ExpenseType.HeaderText = "Expense Type";
            this.ExpenseType.Name = "ExpenseType";
            this.ExpenseType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ExpenseType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // TotalAmount
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.TotalAmount.DefaultCellStyle = dataGridViewCellStyle13;
            this.TotalAmount.HeaderText = "Total Amount";
            this.TotalAmount.Name = "TotalAmount";
            this.TotalAmount.ReadOnly = true;
            this.TotalAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TotalAmount.Width = 150;
            // 
            // CurrentBalance
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.CurrentBalance.DefaultCellStyle = dataGridViewCellStyle14;
            this.CurrentBalance.HeaderText = "Current Balance";
            this.CurrentBalance.Name = "CurrentBalance";
            this.CurrentBalance.ReadOnly = true;
            this.CurrentBalance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CurrentBalance.Width = 150;
            // 
            // Amount
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Amount.DefaultCellStyle = dataGridViewCellStyle15;
            this.Amount.HeaderText = "Amount";
            this.Amount.MaxInputLength = 10;
            this.Amount.Name = "Amount";
            this.Amount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Amount.Width = 150;
            // 
            // EntryAmount
            // 
            this.EntryAmount.HeaderText = "EntryAmount";
            this.EntryAmount.Name = "EntryAmount";
            this.EntryAmount.ReadOnly = true;
            this.EntryAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.EntryAmount.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Extra Charge Payment Details";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(4, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(160, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Extra Charge Payment Info";
            // 
            // lblShortName
            // 
            this.lblShortName.AutoSize = true;
            this.lblShortName.Location = new System.Drawing.Point(424, 283);
            this.lblShortName.Name = "lblShortName";
            this.lblShortName.Size = new System.Drawing.Size(70, 13);
            this.lblShortName.TabIndex = 18;
            this.lblShortName.Text = "Total Amount";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3,
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(655, 435);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape3
            // 
            this.lineShape3.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 40;
            this.lineShape3.X2 = 644;
            this.lineShape3.Y1 = 365;
            this.lineShape3.Y2 = 365;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 77;
            this.lineShape2.X2 = 647;
            this.lineShape2.Y1 = 11;
            this.lineShape2.Y2 = 11;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 81;
            this.lineShape1.X2 = 646;
            this.lineShape1.Y1 = 140;
            this.lineShape1.Y2 = 140;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(507, 466);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 87;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(8, 466);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 86;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(588, 466);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 88;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lblStatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 492);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(670, 22);
            this.ssStatus.TabIndex = 1050;
            this.ssStatus.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(45, 17);
            this.toolStripStatusLabel1.Text = "Status: ";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // frmExtraChargePayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 514);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.pnlExpense);
            this.Controls.Add(this.ExpenseBindingNavigator);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExtraChargePayment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Extra Charge Payment";
            this.Load += new System.EventHandler(this.frmExtraChargePayment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ExpenseBindingNavigator)).EndInit();
            this.ExpenseBindingNavigator.ResumeLayout(false);
            this.ExpenseBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errExtraCharge)).EndInit();
            this.pnlExpense.ResumeLayout(false);
            this.pnlExpense.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExtraChargePayment)).EndInit();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator ExpenseBindingNavigator;
        private System.Windows.Forms.ToolStripLabel bnCountItem;
        private System.Windows.Forms.ToolStripButton bnMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bnMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bnPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bnMoveNextItem;
        private System.Windows.Forms.ToolStripButton bnMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton bnAddNewItem;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        private System.Windows.Forms.ToolStripButton bnCancel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton bnPrint;
        private System.Windows.Forms.ToolStripButton bnEmail;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton bnHelp;
        private System.Windows.Forms.ErrorProvider errExtraCharge;
        private System.Windows.Forms.Panel pnlExpense;
        private System.Windows.Forms.DateTimePicker dtpExtraChargePaymentDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtExtraChargePaymentNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboCompany;
        private System.Windows.Forms.Label LblCompany;
        private System.Windows.Forms.ComboBox cboReferenceNumber;
        private System.Windows.Forms.Label label3;
        private DemoClsDataGridview.DecimalTextBox txtTotalAmount;
        private DemoClsDataGridview.ClsDataGirdView dgvExtraChargePayment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblShortName;
        private System.Windows.Forms.TextBox txtDescription;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cboShipmentVendor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboCurrency;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboExtraChargeNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboPaymentMode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtpChDate;
        private System.Windows.Forms.TextBox txtChNo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cboAccount;
        private System.Windows.Forms.Label label10;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private System.Windows.Forms.Label lblAmountInWords;
        private System.Windows.Forms.ComboBox cboOperationType;
        private System.Windows.Forms.Label lblItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn SerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpenseTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpenseType;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn EntryAmount;
        internal System.Windows.Forms.Button BtnAccount;
        private System.Windows.Forms.StatusStrip ssStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
    }
}