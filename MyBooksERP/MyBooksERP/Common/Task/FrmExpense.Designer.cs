﻿namespace MyBooksERP
{
    partial class frmExpense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExpense));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ExpenseBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bnMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bnMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bnPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bnCancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAccountSettings = new System.Windows.Forms.ToolStripButton();
            this.btnExpenseTypeRef = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.bnEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.bnPayments = new System.Windows.Forms.ToolStripButton();
            this.pnlExpense = new System.Windows.Forms.Panel();
            this.lblAmountInWords = new System.Windows.Forms.Label();
            this.lblAmountIn = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CboCompany = new System.Windows.Forms.ComboBox();
            this.LblCompany = new System.Windows.Forms.Label();
            this.cboReferenceNumber = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboOperationType = new System.Windows.Forms.ComboBox();
            this.txtTotalAmount = new DemoClsDataGridview.DecimalTextBox();
            this.dgvExpense = new DemoClsDataGridview.ClsDataGirdView();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblShortName = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblItemCode = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.BtnDebitAccount = new System.Windows.Forms.Button();
            this.BtnCreditAccount = new System.Windows.Forms.Button();
            this.cboDebitHead = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboCreditHead = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkPostToAccount = new System.Windows.Forms.CheckBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tmExpense = new System.Windows.Forms.Timer(this.components);
            this.errExpense = new System.Windows.Forms.ErrorProvider(this.components);
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.toolStripStatusLabel11 = new DevComponents.DotNetBar.LabelItem();
            this.lblStatus = new DevComponents.DotNetBar.LabelItem();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColExpenseType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvColQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColSerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VoucherID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ExpenseBindingNavigator)).BeginInit();
            this.ExpenseBindingNavigator.SuspendLayout();
            this.pnlExpense.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExpense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errExpense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // ExpenseBindingNavigator
            // 
            this.ExpenseBindingNavigator.AddNewItem = null;
            this.ExpenseBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.ExpenseBindingNavigator.CountItem = this.bnCountItem;
            this.ExpenseBindingNavigator.DeleteItem = null;
            this.ExpenseBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnMoveFirstItem,
            this.bnMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bnPositionItem,
            this.bnCountItem,
            this.bindingNavigatorSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnDeleteItem,
            this.bnCancel,
            this.toolStripSeparator2,
            this.btnAccountSettings,
            this.btnExpenseTypeRef,
            this.ToolStripSeparator,
            this.bnPrint,
            this.bnEmail,
            this.toolStripSeparator1,
            this.BtnHelp,
            this.bnPayments});
            this.ExpenseBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.ExpenseBindingNavigator.MoveFirstItem = null;
            this.ExpenseBindingNavigator.MoveLastItem = null;
            this.ExpenseBindingNavigator.MoveNextItem = null;
            this.ExpenseBindingNavigator.MovePreviousItem = null;
            this.ExpenseBindingNavigator.Name = "ExpenseBindingNavigator";
            this.ExpenseBindingNavigator.PositionItem = this.bnPositionItem;
            this.ExpenseBindingNavigator.Size = new System.Drawing.Size(670, 25);
            this.ExpenseBindingNavigator.TabIndex = 4;
            this.ExpenseBindingNavigator.Text = "bindingNavigator1";
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.Size = new System.Drawing.Size(35, 22);
            this.bnCountItem.Text = "of {0}";
            this.bnCountItem.ToolTipText = "Total number of items";
            // 
            // bnMoveFirstItem
            // 
            this.bnMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            this.bnMoveFirstItem.Name = "bnMoveFirstItem";
            this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveFirstItem.Text = "Move first";
            this.bnMoveFirstItem.Click += new System.EventHandler(this.MoveFirstItem);
            // 
            // bnMovePreviousItem
            // 
            this.bnMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem.Image")));
            this.bnMovePreviousItem.Name = "bnMovePreviousItem";
            this.bnMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bnMovePreviousItem.Text = "Move previous";
            this.bnMovePreviousItem.Click += new System.EventHandler(this.MovePreviousItem);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPositionItem
            // 
            this.bnPositionItem.AccessibleName = "Position";
            this.bnPositionItem.AutoSize = false;
            this.bnPositionItem.Name = "bnPositionItem";
            this.bnPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bnPositionItem.Text = "0";
            this.bnPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Move next";
            this.bnMoveNextItem.Click += new System.EventHandler(this.MoveNextItem);
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Move last";
            this.bnMoveLastItem.Click += new System.EventHandler(this.MoveLastItem);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewItem.ToolTipText = "Add new Item";
            this.bnAddNewItem.Click += new System.EventHandler(this.AddNewItem);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bnSaveItem.ToolTipText = "Save Item";
            this.bnSaveItem.Click += new System.EventHandler(this.SaveItem);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bnDeleteItem.Image")));
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.ToolTipText = "Delete Item";
            this.bnDeleteItem.Click += new System.EventHandler(this.DeleteItem);
            // 
            // bnCancel
            // 
            this.bnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnCancel.Image = ((System.Drawing.Image)(resources.GetObject("bnCancel.Image")));
            this.bnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnCancel.Name = "bnCancel";
            this.bnCancel.Size = new System.Drawing.Size(23, 22);
            this.bnCancel.ToolTipText = "Clear";
            this.bnCancel.Click += new System.EventHandler(this.Cancel);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnAccountSettings
            // 
            this.btnAccountSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAccountSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnAccountSettings.Image")));
            this.btnAccountSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAccountSettings.Name = "btnAccountSettings";
            this.btnAccountSettings.Size = new System.Drawing.Size(23, 22);
            this.btnAccountSettings.Text = "Account";
            this.btnAccountSettings.ToolTipText = "Account Settings";
            this.btnAccountSettings.Visible = false;
            this.btnAccountSettings.Click += new System.EventHandler(this.btnAccountSettings_Click);
            // 
            // btnExpenseTypeRef
            // 
            this.btnExpenseTypeRef.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnExpenseTypeRef.Image = global::MyBooksERP.Properties.Resources.ExpenseType1;
            this.btnExpenseTypeRef.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExpenseTypeRef.Name = "btnExpenseTypeRef";
            this.btnExpenseTypeRef.Size = new System.Drawing.Size(23, 22);
            this.btnExpenseTypeRef.Text = "Expense Types";
            this.btnExpenseTypeRef.Click += new System.EventHandler(this.ExpenseTypeRef);
            // 
            // ToolStripSeparator
            // 
            this.ToolStripSeparator.Name = "ToolStripSeparator";
            this.ToolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPrint
            // 
            this.bnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnPrint.Image = ((System.Drawing.Image)(resources.GetObject("bnPrint.Image")));
            this.bnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(23, 22);
            this.bnPrint.ToolTipText = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // bnEmail
            // 
            this.bnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnEmail.Image = ((System.Drawing.Image)(resources.GetObject("bnEmail.Image")));
            this.bnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Size = new System.Drawing.Size(23, 22);
            this.bnEmail.ToolTipText = "Email";
            this.bnEmail.Click += new System.EventHandler(this.Email);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // bnPayments
            // 
            this.bnPayments.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnPayments.Image = global::MyBooksERP.Properties.Resources.Payments;
            this.bnPayments.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnPayments.Name = "bnPayments";
            this.bnPayments.Size = new System.Drawing.Size(23, 22);
            this.bnPayments.Text = "Payments";
            this.bnPayments.Visible = false;
            this.bnPayments.Click += new System.EventHandler(this.bnPayments_Click);
            // 
            // pnlExpense
            // 
            this.pnlExpense.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlExpense.BackColor = System.Drawing.Color.Transparent;
            this.pnlExpense.Controls.Add(this.lblAmountInWords);
            this.pnlExpense.Controls.Add(this.lblAmountIn);
            this.pnlExpense.Controls.Add(this.cboCurrency);
            this.pnlExpense.Controls.Add(this.label5);
            this.pnlExpense.Controls.Add(this.CboCompany);
            this.pnlExpense.Controls.Add(this.LblCompany);
            this.pnlExpense.Controls.Add(this.cboReferenceNumber);
            this.pnlExpense.Controls.Add(this.label3);
            this.pnlExpense.Controls.Add(this.cboOperationType);
            this.pnlExpense.Controls.Add(this.txtTotalAmount);
            this.pnlExpense.Controls.Add(this.dgvExpense);
            this.pnlExpense.Controls.Add(this.label1);
            this.pnlExpense.Controls.Add(this.label6);
            this.pnlExpense.Controls.Add(this.lblShortName);
            this.pnlExpense.Controls.Add(this.txtDescription);
            this.pnlExpense.Controls.Add(this.lblDescription);
            this.pnlExpense.Controls.Add(this.lblItemCode);
            this.pnlExpense.Controls.Add(this.shapeContainer1);
            this.pnlExpense.Controls.Add(this.BtnDebitAccount);
            this.pnlExpense.Controls.Add(this.BtnCreditAccount);
            this.pnlExpense.Controls.Add(this.cboDebitHead);
            this.pnlExpense.Controls.Add(this.label2);
            this.pnlExpense.Controls.Add(this.cboCreditHead);
            this.pnlExpense.Controls.Add(this.label4);
            this.pnlExpense.Controls.Add(this.chkPostToAccount);
            this.pnlExpense.Location = new System.Drawing.Point(8, 31);
            this.pnlExpense.Name = "pnlExpense";
            this.pnlExpense.Size = new System.Drawing.Size(655, 343);
            this.pnlExpense.TabIndex = 0;
            // 
            // lblAmountInWords
            // 
            this.lblAmountInWords.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountInWords.Location = new System.Drawing.Point(93, 317);
            this.lblAmountInWords.Name = "lblAmountInWords";
            this.lblAmountInWords.Size = new System.Drawing.Size(313, 17);
            this.lblAmountInWords.TabIndex = 254;
            // 
            // lblAmountIn
            // 
            this.lblAmountIn.AutoSize = true;
            this.lblAmountIn.Location = new System.Drawing.Point(4, 317);
            this.lblAmountIn.Name = "lblAmountIn";
            this.lblAmountIn.Size = new System.Drawing.Size(92, 13);
            this.lblAmountIn.TabIndex = 253;
            this.lblAmountIn.Text = "Amount In Words:";
            // 
            // cboCurrency
            // 
            this.cboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCurrency.BackColor = System.Drawing.SystemColors.Info;
            this.cboCurrency.DropDownHeight = 75;
            this.cboCurrency.Enabled = false;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.IntegralHeight = false;
            this.cboCurrency.Location = new System.Drawing.Point(441, 25);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(188, 21);
            this.cboCurrency.TabIndex = 227;
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            this.cboCurrency.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(332, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 228;
            this.label5.Text = "Currency";
            // 
            // CboCompany
            // 
            this.CboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompany.BackColor = System.Drawing.SystemColors.Info;
            this.CboCompany.FormattingEnabled = true;
            this.CboCompany.Location = new System.Drawing.Point(117, 25);
            this.CboCompany.Name = "CboCompany";
            this.CboCompany.Size = new System.Drawing.Size(188, 21);
            this.CboCompany.TabIndex = 0;
            this.CboCompany.SelectedIndexChanged += new System.EventHandler(this.CboCompany_SelectedIndexChanged);
            this.CboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // LblCompany
            // 
            this.LblCompany.AutoSize = true;
            this.LblCompany.Location = new System.Drawing.Point(13, 28);
            this.LblCompany.Name = "LblCompany";
            this.LblCompany.Size = new System.Drawing.Size(51, 13);
            this.LblCompany.TabIndex = 226;
            this.LblCompany.Text = "Company";
            // 
            // cboReferenceNumber
            // 
            this.cboReferenceNumber.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboReferenceNumber.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboReferenceNumber.BackColor = System.Drawing.SystemColors.Info;
            this.cboReferenceNumber.DropDownHeight = 75;
            this.cboReferenceNumber.ForeColor = System.Drawing.Color.Black;
            this.cboReferenceNumber.FormattingEnabled = true;
            this.cboReferenceNumber.IntegralHeight = false;
            this.cboReferenceNumber.Location = new System.Drawing.Point(441, 49);
            this.cboReferenceNumber.Name = "cboReferenceNumber";
            this.cboReferenceNumber.Size = new System.Drawing.Size(98, 21);
            this.cboReferenceNumber.TabIndex = 2;
            this.cboReferenceNumber.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.cboReferenceNumber.SelectedValueChanged += new System.EventHandler(this.ReferenceNumbeChanged);
            this.cboReferenceNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(332, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Reference No";
            // 
            // cboOperationType
            // 
            this.cboOperationType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOperationType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOperationType.BackColor = System.Drawing.SystemColors.Info;
            this.cboOperationType.DropDownHeight = 75;
            this.cboOperationType.ForeColor = System.Drawing.Color.Black;
            this.cboOperationType.FormattingEnabled = true;
            this.cboOperationType.IntegralHeight = false;
            this.cboOperationType.Location = new System.Drawing.Point(117, 49);
            this.cboOperationType.Name = "cboOperationType";
            this.cboOperationType.Size = new System.Drawing.Size(188, 21);
            this.cboOperationType.TabIndex = 1;
            this.cboOperationType.SelectionChangeCommitted += new System.EventHandler(this.OperationTypeChanged);
            this.cboOperationType.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.cboOperationType.SelectedValueChanged += new System.EventHandler(this.cboOperationType_SelectedValueChanged);
            this.cboOperationType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(497, 314);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.ShortcutsEnabled = false;
            this.txtTotalAmount.Size = new System.Drawing.Size(150, 20);
            this.txtTotalAmount.TabIndex = 10;
            this.txtTotalAmount.TabStop = false;
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalAmount.TextChanged += new System.EventHandler(this.txtTotalAmount_TextChanged);
            // 
            // dgvExpense
            // 
            this.dgvExpense.AddNewRow = false;
            this.dgvExpense.AlphaNumericCols = new int[0];
            this.dgvExpense.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.dgvExpense.CapsLockCols = new int[0];
            this.dgvExpense.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvExpense.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColExpenseType,
            this.dgvColQuantity,
            this.dgvColAmount,
            this.Total,
            this.dgvColDescription,
            this.dgvColSerialNo,
            this.VoucherID});
            this.dgvExpense.DecimalCols = new int[] {
        1,
        2};
            this.dgvExpense.HasSlNo = false;
            this.dgvExpense.LastRowIndex = 0;
            this.dgvExpense.Location = new System.Drawing.Point(7, 135);
            this.dgvExpense.Name = "dgvExpense";
            this.dgvExpense.NegativeValueCols = new int[0];
            this.dgvExpense.NumericCols = new int[0];
            this.dgvExpense.RowHeadersWidth = 50;
            this.dgvExpense.Size = new System.Drawing.Size(641, 173);
            this.dgvExpense.TabIndex = 8;
            this.dgvExpense.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExpense_CellValueChanged);
            this.dgvExpense.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvExpense_UserDeletingRow);
            this.dgvExpense.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvExpense_CellBeginEdit);
            this.dgvExpense.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvExpense_UserDeletedRow);
            this.dgvExpense.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvExpense_RowsAdded);
            this.dgvExpense.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExpense_CellEndEdit);
            this.dgvExpense.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvExpense_CurrentCellDirtyStateChanged);
            this.dgvExpense.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvExpense_DataError);
            this.dgvExpense.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvExpense_KeyDown);
            this.dgvExpense.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvExpense_RowsRemoved);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 52;
            this.label1.Text = "Expense Details";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(4, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 51;
            this.label6.Text = "Expense Info";
            // 
            // lblShortName
            // 
            this.lblShortName.AutoSize = true;
            this.lblShortName.Location = new System.Drawing.Point(413, 317);
            this.lblShortName.Name = "lblShortName";
            this.lblShortName.Size = new System.Drawing.Size(70, 13);
            this.lblShortName.TabIndex = 11;
            this.lblShortName.Text = "Total Amount";
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtDescription.Location = new System.Drawing.Point(117, 76);
            this.txtDescription.MaxLength = 1000;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(531, 39);
            this.txtDescription.TabIndex = 3;
            this.txtDescription.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(13, 86);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(49, 13);
            this.lblDescription.TabIndex = 10;
            this.lblDescription.Text = "Remarks";
            // 
            // lblItemCode
            // 
            this.lblItemCode.AutoSize = true;
            this.lblItemCode.Location = new System.Drawing.Point(13, 52);
            this.lblItemCode.Name = "lblItemCode";
            this.lblItemCode.Size = new System.Drawing.Size(80, 13);
            this.lblItemCode.TabIndex = 12;
            this.lblItemCode.Text = "Operation Type";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(655, 343);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 77;
            this.lineShape2.X2 = 647;
            this.lineShape2.Y1 = 11;
            this.lineShape2.Y2 = 11;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 77;
            this.lineShape1.X2 = 646;
            this.lineShape1.Y1 = 125;
            this.lineShape1.Y2 = 124;
            // 
            // BtnDebitAccount
            // 
            this.BtnDebitAccount.Location = new System.Drawing.Point(540, 253);
            this.BtnDebitAccount.Name = "BtnDebitAccount";
            this.BtnDebitAccount.Size = new System.Drawing.Size(32, 23);
            this.BtnDebitAccount.TabIndex = 7;
            this.BtnDebitAccount.Text = "...";
            this.BtnDebitAccount.UseVisualStyleBackColor = true;
            this.BtnDebitAccount.Visible = false;
            this.BtnDebitAccount.Click += new System.EventHandler(this.BtnDebitAccount_Click);
            // 
            // BtnCreditAccount
            // 
            this.BtnCreditAccount.Location = new System.Drawing.Point(540, 226);
            this.BtnCreditAccount.Name = "BtnCreditAccount";
            this.BtnCreditAccount.Size = new System.Drawing.Size(32, 23);
            this.BtnCreditAccount.TabIndex = 5;
            this.BtnCreditAccount.Text = "...";
            this.BtnCreditAccount.UseVisualStyleBackColor = true;
            this.BtnCreditAccount.Visible = false;
            this.BtnCreditAccount.Click += new System.EventHandler(this.BtnCreditAccount_Click);
            // 
            // cboDebitHead
            // 
            this.cboDebitHead.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDebitHead.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDebitHead.BackColor = System.Drawing.SystemColors.Info;
            this.cboDebitHead.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDebitHead.DropDownHeight = 75;
            this.cboDebitHead.Enabled = false;
            this.cboDebitHead.FormattingEnabled = true;
            this.cboDebitHead.IntegralHeight = false;
            this.cboDebitHead.Location = new System.Drawing.Point(335, 253);
            this.cboDebitHead.Name = "cboDebitHead";
            this.cboDebitHead.Size = new System.Drawing.Size(197, 21);
            this.cboDebitHead.TabIndex = 6;
            this.cboDebitHead.Visible = false;
            this.cboDebitHead.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(248, 257);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Debit Account";
            this.label2.Visible = false;
            // 
            // cboCreditHead
            // 
            this.cboCreditHead.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCreditHead.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCreditHead.BackColor = System.Drawing.SystemColors.Info;
            this.cboCreditHead.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCreditHead.DropDownHeight = 75;
            this.cboCreditHead.Enabled = false;
            this.cboCreditHead.FormattingEnabled = true;
            this.cboCreditHead.IntegralHeight = false;
            this.cboCreditHead.Location = new System.Drawing.Point(335, 228);
            this.cboCreditHead.Name = "cboCreditHead";
            this.cboCreditHead.Size = new System.Drawing.Size(197, 21);
            this.cboCreditHead.TabIndex = 4;
            this.cboCreditHead.Visible = false;
            this.cboCreditHead.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(249, 231);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Cedit Account";
            this.label4.Visible = false;
            // 
            // chkPostToAccount
            // 
            this.chkPostToAccount.BackColor = System.Drawing.Color.Transparent;
            this.chkPostToAccount.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.chkPostToAccount.Location = new System.Drawing.Point(63, 289);
            this.chkPostToAccount.Name = "chkPostToAccount";
            this.chkPostToAccount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkPostToAccount.Size = new System.Drawing.Size(106, 19);
            this.chkPostToAccount.TabIndex = 9;
            this.chkPostToAccount.Text = "Post To Account";
            this.chkPostToAccount.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.chkPostToAccount.UseVisualStyleBackColor = false;
            this.chkPostToAccount.Visible = false;
            this.chkPostToAccount.CheckedChanged += new System.EventHandler(this.chkPostToAccount_CheckedChanged);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(499, 380);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 9;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.SaveItem);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(8, 380);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.SaveItem);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(581, 380);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.Close);
            // 
            // errExpense
            // 
            this.errExpense.ContainerControl = this;
            this.errExpense.RightToLeft = true;
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.toolStripStatusLabel11,
            this.lblStatus});
            this.bar1.Location = new System.Drawing.Point(0, 406);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(670, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 81;
            this.bar1.TabStop = false;
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Text = "Status:";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn1.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 8;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn2.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 120;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "Description";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "SerialNumber";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dgvColExpenseType
            // 
            this.dgvColExpenseType.FillWeight = 90F;
            this.dgvColExpenseType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColExpenseType.HeaderText = "Expense";
            this.dgvColExpenseType.Name = "dgvColExpenseType";
            this.dgvColExpenseType.Width = 120;
            // 
            // dgvColQuantity
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColQuantity.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvColQuantity.HeaderText = "Quantity";
            this.dgvColQuantity.MaxInputLength = 8;
            this.dgvColQuantity.Name = "dgvColQuantity";
            this.dgvColQuantity.Width = 70;
            // 
            // dgvColAmount
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColAmount.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvColAmount.HeaderText = "Amount";
            this.dgvColAmount.MaxInputLength = 10;
            this.dgvColAmount.Name = "dgvColAmount";
            // 
            // Total
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Total.DefaultCellStyle = dataGridViewCellStyle3;
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // dgvColDescription
            // 
            this.dgvColDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvColDescription.HeaderText = "Remarks";
            this.dgvColDescription.MaxInputLength = 100;
            this.dgvColDescription.Name = "dgvColDescription";
            // 
            // dgvColSerialNo
            // 
            this.dgvColSerialNo.HeaderText = "SerialNumber";
            this.dgvColSerialNo.Name = "dgvColSerialNo";
            this.dgvColSerialNo.Visible = false;
            // 
            // VoucherID
            // 
            this.VoucherID.HeaderText = "VoucherID";
            this.VoucherID.Name = "VoucherID";
            this.VoucherID.Visible = false;
            // 
            // frmExpense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 425);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.pnlExpense);
            this.Controls.Add(this.ExpenseBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExpense";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Expense";
            this.Load += new System.EventHandler(this.FormLoad);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClosingForm);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmExpense_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.ExpenseBindingNavigator)).EndInit();
            this.ExpenseBindingNavigator.ResumeLayout(false);
            this.ExpenseBindingNavigator.PerformLayout();
            this.pnlExpense.ResumeLayout(false);
            this.pnlExpense.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExpense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errExpense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator ExpenseBindingNavigator;
        private System.Windows.Forms.ToolStripLabel bnCountItem;
        private System.Windows.Forms.ToolStripButton bnMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bnMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bnPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bnMoveNextItem;
        private System.Windows.Forms.ToolStripButton bnMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton bnAddNewItem;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        private System.Windows.Forms.ToolStripButton bnCancel;
        private System.Windows.Forms.ToolStripSeparator ToolStripSeparator;
        private System.Windows.Forms.ToolStripButton bnPrint;
        private System.Windows.Forms.ToolStripButton bnEmail;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Panel pnlExpense;
        private DemoClsDataGridview.DecimalTextBox txtTotalAmount;
        private DemoClsDataGridview.ClsDataGirdView dgvExpense;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblShortName;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblItemCode;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cboOperationType;
        private System.Windows.Forms.ComboBox cboReferenceNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer tmExpense;
        private System.Windows.Forms.ErrorProvider errExpense;
        private System.Windows.Forms.ComboBox cboDebitHead;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboCreditHead;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnExpenseTypeRef;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel11;
        private DevComponents.DotNetBar.LabelItem lblStatus;
        private System.Windows.Forms.CheckBox chkPostToAccount;
        internal System.Windows.Forms.Button BtnDebitAccount;
        internal System.Windows.Forms.Button BtnCreditAccount;
        private System.Windows.Forms.Label LblCompany;
        private System.Windows.Forms.ToolStripButton btnAccountSettings;
        private System.Windows.Forms.ComboBox CboCompany;
        private System.Windows.Forms.ToolStripButton bnPayments;
        private System.Windows.Forms.ComboBox cboCurrency;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        private System.Windows.Forms.Label lblAmountIn;
        private System.Windows.Forms.Label lblAmountInWords;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColExpenseType;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColSerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoucherID;
    }
}