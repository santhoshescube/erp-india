﻿namespace MyBooksERP
{
    partial class FrmCommonRef 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCommonRef));
            this.panel1 = new System.Windows.Forms.Panel();
            this.grdCommon = new System.Windows.Forms.DataGridView();
            this.TStripTop = new System.Windows.Forms.ToolStrip();
            this.VendorInformationBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.TlsStatusLabel = new DevComponents.DotNetBar.LabelItem();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCommon)).BeginInit();
            this.TStripTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grdCommon);
            this.panel1.Location = new System.Drawing.Point(4, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(352, 276);
            this.panel1.TabIndex = 2;
            // 
            // grdCommon
            // 
            this.grdCommon.AllowUserToResizeColumns = false;
            this.grdCommon.AllowUserToResizeRows = false;
            this.grdCommon.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdCommon.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grdCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdCommon.Location = new System.Drawing.Point(3, 6);
            this.grdCommon.MultiSelect = false;
            this.grdCommon.Name = "grdCommon";
            this.grdCommon.RowHeadersVisible = false;
            this.grdCommon.RowHeadersWidth = 30;
            this.grdCommon.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdCommon.Size = new System.Drawing.Size(346, 267);
            this.grdCommon.TabIndex = 0;
            this.grdCommon.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCommon_CellValueChanged);
            this.grdCommon.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdCommon_CellBeginEdit);
            this.grdCommon.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCommon_CellEndEdit);
            this.grdCommon.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdCommon_DataError);
            this.grdCommon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdCommon_KeyDown);
            // 
            // TStripTop
            // 
            this.TStripTop.BackColor = System.Drawing.Color.Transparent;
            this.TStripTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.VendorInformationBindingNavigatorSaveItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorDeleteItem,
            this.toolStripButton1});
            this.TStripTop.Location = new System.Drawing.Point(0, 0);
            this.TStripTop.Name = "TStripTop";
            this.TStripTop.Size = new System.Drawing.Size(360, 25);
            this.TStripTop.TabIndex = 3;
            this.TStripTop.Text = "toolStrip1";
            // 
            // VendorInformationBindingNavigatorSaveItem
            // 
            this.VendorInformationBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("VendorInformationBindingNavigatorSaveItem.Image")));
            this.VendorInformationBindingNavigatorSaveItem.Name = "VendorInformationBindingNavigatorSaveItem";
            this.VendorInformationBindingNavigatorSaveItem.Size = new System.Drawing.Size(78, 22);
            this.VendorInformationBindingNavigatorSaveItem.Text = "Save Data";
            this.VendorInformationBindingNavigatorSaveItem.Click += new System.EventHandler(this.VendorInformationBindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(60, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(38, 22);
            this.toolStripButton1.Text = "Clear";
            this.toolStripButton1.Visible = false;
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(7, 305);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 4;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(278, 305);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 6;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(197, 305);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 5;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // bar1
            // 
            this.bar1.AccessibleDescription = "bar1 (bar1)";
            this.bar1.AccessibleName = "bar1";
            this.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.TlsStatusLabel});
            this.bar1.Location = new System.Drawing.Point(0, 332);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(360, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 57;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "Status : ";
            // 
            // TlsStatusLabel
            // 
            this.TlsStatusLabel.Name = "TlsStatusLabel";
            // 
            // FrmCommonRef
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 351);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.TStripTop);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCommonRef";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.FrmCommonRef_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCommonRef_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmCommonRef_KeyDown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdCommon)).EndInit();
            this.TStripTop.ResumeLayout(false);
            this.TStripTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStrip TStripTop;
        internal System.Windows.Forms.ToolStripButton VendorInformationBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnOk;
        internal System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.DataGridView grdCommon;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.LabelItem TlsStatusLabel;
    }
}