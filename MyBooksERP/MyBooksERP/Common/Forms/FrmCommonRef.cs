﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;


/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,16 Feb 2011>
   Description:	<Description,,CommonRef Form>
================================================
*/
namespace MyBooksERP
{
    public partial class FrmCommonRef : DevComponents.DotNetBar.Office2007Form
    {
        private ClsLogWriter Logger;
        private int MintNoOfHiddenColums;//hidden column count
        private int MintPredefinedIndex;// predefined index
        private int MintTimerValue;// timer value
        private string MstrMessCaption; //Message caption
        private string MstrReportFooter; //Report Footer
        private string MstrFields;// Field Names
        public string MstrInsertFields;// Insert Fields Names
        public string MstrInsertValues;// Insert Fields Values
        private string MstrTableName;//Table Name
        private string MstrCondition;//Conditions
        private string MstrFormName;// Form caption
        private Boolean MblnChangeStatus;//  'Check state of the page
        private string MstrMessageCommon;
        private string MstrMessageCaption;
        clsBLLCommonRef MobjClsBLLCommonRef;
        ClsLogWriter MobjClsLogWriter;

        public int NewID;
        private string MgstrOldValue;
        private int MgIntEditRowIndex;

        public FrmCommonRef(string strFormName, int[] intaCommonValues, string strFilelds, string strTableName, string strCondition)
        {
            //Constructor Initialising Components
            InitializeComponent();
            MobjClsBLLCommonRef = new clsBLLCommonRef();
            MintNoOfHiddenColums = intaCommonValues[0];     
            MintPredefinedIndex = intaCommonValues[1] - 1;
            MintTimerValue = ClsCommonSettings.TimerInterval;
            MstrMessCaption = ClsCommonSettings.MessageCaption;   
            MstrReportFooter = ClsCommonSettings.ReportFooter;  
            MstrFormName = strFormName;      
            MstrFields = strFilelds;
            MstrTableName = strTableName;
            MstrCondition = strCondition;
            MgstrOldValue = string.Empty;

            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
        }

        private void FrmCommonRef_Load(object sender, EventArgs e)
        {
            //Load function filling common grid
            this.Text = MstrFormName;
         
            try
            {
                FillCommonGrid();
            }
            catch(Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on FrmCommonRef_Load " + this.Name + " -- " + this.Text + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmCommonRef_Load() " + ex.Message.ToString());
            }
        }

        private bool FillCommonGrid()
        {
            //Filling Common Grid
            try
            {
                grdCommon.DataSource = null;

                MobjClsBLLCommonRef.clsDTOCommonRef.strCondition = MstrCondition;
                MobjClsBLLCommonRef.clsDTOCommonRef.strFields = MstrFields;
                MobjClsBLLCommonRef.clsDTOCommonRef.strTableName = MstrTableName;
                grdCommon.DataSource = new DataTable();
                grdCommon.DataSource = MobjClsBLLCommonRef.DisplayInformation();

                if (grdCommon.RowCount > 0)
                {
                    for (int i = 0; i < MintNoOfHiddenColums; i++)
                    {
                        grdCommon.Columns[i].Visible = false;
                    }

                    for (int i = MintNoOfHiddenColums; i < grdCommon.ColumnCount; i++)
                    {
                        ((DataGridViewTextBoxColumn)grdCommon.Columns[i]).MaxInputLength = 50;
                    }

                    // set focus to new row.
                    grdCommon.CurrentCell = grdCommon[MintNoOfHiddenColums, grdCommon.Rows.Count - 1];

                    //// set current cell in edit mode
                    //grdCommon.CurrentCell.DataGridView.BeginEdit(false);
                }


                return true;
            }
            catch(Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on FillCommonGrid " + this.Name + " -- " + this.Text + " " + ex.Message.ToString(), 2);
                return false;
            }
        }

        private bool GridValidation(int iRwindex, int iColIndex)
        {
            try
            {
                //Validating Grid Checking For Null And Duplicate Values
                if (grdCommon.Rows.Count > 0)
                {
                    if (iRwindex >= 0)
                    {
                        if (grdCommon.IsCurrentCellDirty)
                            grdCommon.CommitEdit(DataGridViewDataErrorContexts.Commit);

                        grdCommon.EndEdit();

                        //for (int j = 1; j < grdCommon.Columns.Count; j++)
                        //{
                        grdCommon.Rows[iRwindex].Cells[MintNoOfHiddenColums].Value = Convert.ToString(grdCommon.Rows[iRwindex].Cells[MintNoOfHiddenColums].Value).Replace("'", "’").Trim();

                        if (Convert.ToString(grdCommon.Rows[iRwindex].Cells[MintNoOfHiddenColums].Value).Trim() == "")
                        {
                            MessageBox.Show("Please enter value", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            grdCommon.CurrentCell = grdCommon[iColIndex, iRwindex];
                            return false;
                        }
                        //}
                        string sCurrentValue = Convert.ToString(grdCommon.Rows[iRwindex].Cells[iColIndex].Value).Trim();

                        for (int i = 0; i < grdCommon.RowCount - 1; i++)
                        {
                            if (i != iRwindex && Convert.ToString(grdCommon.Rows[i].Cells[iColIndex].Value).Trim().ToLower() == sCurrentValue.ToLower())
                            {
                                MessageBox.Show("Duplicate values not permitted", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                grdCommon.Rows[iRwindex].Cells[iColIndex].Value = "";
                                //grdCommon.Rows[iRwindex].Selected = true;
                                grdCommon.CurrentCell = grdCommon[iColIndex, iRwindex];
                                //grdCommon.CurrentCell.DataGridView.BeginEdit(true);
                                return false;
                            }
                        }
                        return true;
                    }
                    else
                    {
                        if (grdCommon.RowCount > 0)
                            grdCommon.CurrentCell = grdCommon[iColIndex, 0];
                        return false;
                    }
                }
                else
                    return false;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on GridValidation()" + this.Name + " -- " + this.Text + " " + Ex.Message.ToString(), 2);
                return false;
            }
        }

        private bool SaveInformation(int iRwindex)
        {
            //Saving Data
            try
            {
                int iRetId = 0;
                string sField = MstrFields;
                string sFiledTemp;
                string sFieldValues;
                string[] sValueSpilt;
                string[] sFiledSpilt;

                if (grdCommon.Rows[iRwindex].Cells.Count > 0)
                {
                    sFieldValues = "";

                    if (Convert.ToString(grdCommon.Rows[iRwindex].Cells[0].Value).Trim() == "")
                    {

                        //for (int i = 0; i < grdCommon.Rows[iRwindex].Cells.Count; i++)
                        //{
                        //    if (i < MintNoOfHiddenColums)
                        //        continue;

                            if (grdCommon.Rows[iRwindex].Cells[MintNoOfHiddenColums].ValueType.ToString() == "System.String")
                                sFieldValues = "'" + Convert.ToString(grdCommon.Rows[iRwindex].Cells[MintNoOfHiddenColums].Value).Replace("'", "’").Replace(",", "¸").Trim() + "',";
                            else
                                sFieldValues = Convert.ToString(grdCommon.Rows[iRwindex].Cells[MintNoOfHiddenColums].Value).Replace("'", "").Replace(",", "¸").Trim() + ",";
                        //}

                            sFieldValues += (MstrInsertValues != "" && MstrInsertValues != null) ? MstrInsertValues + "," : "";

                        if (sField != "")
                            sField = sField.Remove(0, sField.LastIndexOf(',') + 1);

                        sFiledSpilt = sField.Split(',');
                        sFiledTemp = sField;
                        sField = "";

                        for (int i = 0; i < sFiledSpilt.Length; i++)
                        {
                            if (sFiledSpilt[i].IndexOf("As") != -1)
                                sField += sFiledSpilt[i].Substring(0, sFiledSpilt[i].IndexOf("As")).Trim() + ",";
                            else
                                sField += sFiledSpilt[i] + ",";
                        }

                        sField += (MstrInsertFields != "" && MstrInsertFields != null) ? MstrInsertFields + "," : "";

                        if (sField != "")
                            sField = sField.Substring(0, sField.Length - 1).Trim();
                        else
                        {
                            if (sFiledTemp.IndexOf("As") != -1)
                                sField = sFiledTemp.Substring(0, sFiledTemp.IndexOf("As")).Trim();
                        }
                        
                        if (sFieldValues != "")
                        {
                            sFieldValues = sFieldValues.Substring(0, sFieldValues.Length - 1);

                            sFieldValues = sFieldValues.Replace("False", "0").Replace("false", "0").Replace("True", "1").Replace("true", "1").Trim();
                        }

                        if (sField == "CostCenter")
                        {
                            sField = sField + ",ReferenceID,CostCenterGroupID";
                            sFieldValues = sFieldValues + "," + (int)CostCenterGroupReference.General + "," + (int)CostCenterGroupReference.General;
                        }

                        MobjClsBLLCommonRef.clsDTOCommonRef.strFields = sField;
                        MobjClsBLLCommonRef.clsDTOCommonRef.strFieldValues = sFieldValues;
                        MobjClsBLLCommonRef.clsDTOCommonRef.strTableName = MstrTableName;

                        if (MobjClsBLLCommonRef.AddInformation(out iRetId))
                        {
                            grdCommon.Rows[iRwindex].Cells[0].Value = iRetId;
                            this.NewID = iRetId;
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        //Updating
                        for (int i = 0; i < grdCommon.Rows[iRwindex].Cells.Count; i++)
                        {

                            if (grdCommon.Rows[iRwindex].Cells[i].ValueType.ToString() == "System.String")
                                sFieldValues += "'" + Convert.ToString(grdCommon.Rows[iRwindex].Cells[i].Value).Replace("'", "’").Replace(",", "¸").Trim() + "',";
                            else
                                sFieldValues += Convert.ToString(grdCommon.Rows[iRwindex].Cells[i].Value).Replace("'", "’").Replace(",", "¸").Trim() + ",";

                        }

                        if (sFieldValues != "")
                            sFieldValues = sFieldValues.Substring(0, sFieldValues.Length - 1);

                        sFiledSpilt = sField.Split(',');
                        sValueSpilt = sFieldValues.Split(',');
                        sFieldValues = "";

                        for (int i = 1; i < sFiledSpilt.Length; i++)
                        {
                            sValueSpilt[i] = sValueSpilt[i].Replace("False", "0").Replace("false", "0").Replace("True", "1").Replace("true", "1").Trim();

                            if (sFiledSpilt[i].IndexOf("As") != -1)
                                sFiledSpilt[i] = sFiledSpilt[i].Substring(0, sFiledSpilt[i].IndexOf("As")).Trim();

                            sFieldValues += sFiledSpilt[i] + "=" + sValueSpilt[i].Trim() + ",";
                        }
                        sField = sFiledSpilt[0] + "=" + sValueSpilt[0].Trim();

                        if (sFieldValues != "")
                            sFieldValues = sFieldValues.Substring(0, sFieldValues.Length - 1);

                        MobjClsBLLCommonRef.clsDTOCommonRef.strFields = sField;
                        MobjClsBLLCommonRef.clsDTOCommonRef.strFieldValues = sFieldValues;
                        MobjClsBLLCommonRef.clsDTOCommonRef.strTableName = MstrTableName;

                        if (MobjClsBLLCommonRef.UpdateInformation())
                        {
                            this.NewID = Convert.ToInt32(sValueSpilt[0]);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                    return false;
            }
            catch (SqlException )
            {
                MessageBox.Show("Information already exists!");
                return false;
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on  SaveInformation() " + this.Name+ " -- " + this.Text +" " + ex.Message.ToString(), 2); 
                throw ex;
            }
        }

        private bool DeleteInformation(int iRowindex)
        {
            //Deleting Data
            try
            {
                if (iRowindex >= 0)
                {
                    if (grdCommon.Rows[iRowindex].Cells[0].Value == DBNull.Value)
                        return false;

                    if (MintPredefinedIndex > 0)
                    {
                        if (grdCommon.Rows[iRowindex].Cells[MintPredefinedIndex].Value != DBNull.Value && grdCommon.Rows[iRowindex].Cells[MintPredefinedIndex].Value != null && Convert.ToBoolean(grdCommon.Rows[iRowindex].Cells[MintPredefinedIndex].Value) == true)
                        {
                            MessageBox.Show("You cannot delete a prefefined item.", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK,MessageBoxIcon.Information);

                            return false;
                        }
                    }
                    string sCondition = "";

                    if (MstrFields != "" && Convert.ToString(grdCommon.Rows[iRowindex].Cells[0].Value).Trim() != "")
                    {
                        sCondition = MstrFields.Substring(0, MstrFields.IndexOf(','));
                        sCondition += "=" + grdCommon.Rows[iRowindex].Cells[0].Value;
                        MobjClsBLLCommonRef.clsDTOCommonRef.strFields = MstrFields;
                        MobjClsBLLCommonRef.clsDTOCommonRef.strTableName = MstrTableName;
                        MobjClsBLLCommonRef.clsDTOCommonRef.strCondition = sCondition;

                        if (MobjClsBLLCommonRef.DeleteInformation())
                        {
                            grdCommon.Rows.RemoveAt(iRowindex);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on  DeleteInformation() " + this.Name + " -- " + this.Text + " " + ex.Message.ToString(), 2); 
                
                throw ex;
            }
        }

        private void grdCommon_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                MgstrOldValue = grdCommon.CurrentCell.Value.ToString();
                MgIntEditRowIndex = grdCommon.CurrentRow.Index;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on  grdCommon_CellBeginEdit() " + this.Name + " -- " + this.Text + " " + Ex.Message.ToString(), 2); 
               
            }
            //MblnChangeStatus = true;

            //if (Convert.ToString(grdCommon.Rows[e.RowIndex].Cells[0].Value) == "")  //add mode
            //{
            //    for (int i = 1; i < MintNoOfHiddenColums; i++)
            //    {
            //        grdCommon.Rows[e.RowIndex].Cells[i].Value = grdCommon.Rows[e.RowIndex - 1].Cells[i].Value;
            //    }
            //    if (MintPredefinedIndex > 0)
            //    {
            //        grdCommon.Rows[e.RowIndex].Cells[MintPredefinedIndex].Value = false;
            //    }
            //}
            //else
            //{
            //    if (MintPredefinedIndex > 0)
            //    {
            //        if (Convert.ToBoolean(grdCommon.Rows[e.RowIndex].Cells[MintPredefinedIndex].Value))
            //        {
            //            grdCommon.Rows[e.RowIndex].ReadOnly = true;
            //            e.Cancel = true;
            //        }
            //    }
            //}
        }

        private void grdCommon_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //try
            //{
            //    if (Convert.ToString(grdCommon.Rows[e.RowIndex].Cells[e.ColumnIndex].Value).Trim() == "")
            //        grdCommon.AllowUserToAddRows = false;
            //    else
            //        grdCommon.AllowUserToAddRows = true;
            //}
            //catch (Exception)
            //{
            //    return;
            //}
        }

        private void grdCommon_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //Saving Information
            try
            {
                 if (GridValidation(e.RowIndex, e.ColumnIndex))
                {
                    SaveInformation(e.RowIndex);
                    //FillCommonGrid();
                    MblnChangeStatus = false;
                }
                else
                {
                    grdCommon.CurrentCell = grdCommon[e.ColumnIndex, e.RowIndex];
                    return;
                }
            }
            catch (Exception ex)
            {
               
                MobjClsLogWriter.WriteLog("Error on grdCommon_CellEndEdit " + this.Name + " -- " + this.Text + " " + ex.Message.ToString(), 2); 
                //MessageBox.Show("Error On grdCommon_CellEndEdit() " + ex.Message);
            }
        }

        private void grdCommon_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            //Catching Grid Error
            try
            {

            }
            catch (Exception)
            {
                return;
            }
        }

        private void VendorInformationBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            //Saving Information
            try
            {
                grdCommon.EndEdit();
                //if (grdCommon.RowCount > 0 && grdCommon.CurrentRow !=null)
                //{ 
                //    if (grdCommon.CurrentRow.Index > 0)
                //    {
                //        if (GridValidation(grdCommon.CurrentRow.Index, grdCommon.CurrentCell.ColumnIndex))
                //        {
                //            if (SaveInformation(grdCommon.CurrentRow.Index))
                //            {
                //                TlsStatusLabel.Text = "Saved Successfully";
                //                MblnChangeStatus = false;
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
               
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on VendorInformationBindingNavigatorSaveItem_Click() " + ex.Message.ToString());
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            //Saving Information
            try
            {
                grdCommon.EndEdit();
                //if (grdCommon.RowCount > 0 && grdCommon.CurrentRow !=null)
                //{
                //    if (grdCommon.CurrentRow.Index > 0)
                //    {
                //        if (GridValidation(grdCommon.CurrentRow.Index, grdCommon.CurrentCell.ColumnIndex))
                //        {
                //            if (SaveInformation(grdCommon.CurrentRow.Index))
                //            {
                //                TlsStatusLabel.Text = "Saved Successfully";

                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnSave_Click() " + ex.Message.ToString());
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            //Saving And Closing
            try
            {
                if (grdCommon.CurrentCell != null && Convert.ToString(grdCommon.CurrentCell.Value).Trim() != "" && grdCommon.CurrentCell.Selected)
                    this.NewID = Convert.ToInt32(grdCommon.CurrentRow.Cells[0].Value);
                grdCommon.EndEdit();

                this.Close();
                //if (grdCommon.RowCount > 0 && grdCommon.CurrentRow !=null)
                //{
                //    if (grdCommon.CurrentRow.Index >= 0)
                //    {
                //        if (GridValidation(grdCommon.CurrentRow.Index, grdCommon.CurrentCell.ColumnIndex))
                //        {
                //            if (SaveInformation(grdCommon.CurrentRow.Index))
                //            {
                //                this.Close();
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnOk_Click() " + ex.Message.ToString());
            }
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            //Deleting Data
            try
            {
                if (grdCommon.CurrentRow != null && !string.IsNullOrEmpty(Convert.ToString(grdCommon.CurrentCell.Value)))
                {
                    if (MessageBox.Show("Are you sure you wish to delete this item?", ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        DeleteInformation(grdCommon.CurrentRow.Index);
                }
            }
            catch (Exception ex)
            {

                string strExceptionType = ex.GetType().ToString();

                if (strExceptionType == "System.Data.SqlClient.SqlException")
                    MessageBox.Show("This item cannot be deleted. Details exists in the system", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK,MessageBoxIcon.Information);

                //MessageBox.Show("Detai", MstrMessCaption, MessageBoxButtons.OK);
                //return;

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorDeleteItem_Click " + ex.Message.ToString());
            }
        }

        private void grdCommon_KeyDown(object sender, KeyEventArgs e)
        {
            //Deleting Data
            try
            {
                switch (e.KeyCode)
                {
                    case Keys.Delete:
                        if(grdCommon.CurrentRow !=null)
                        DeleteInformation(grdCommon.CurrentRow.Index);
                        break;

                    case Keys.Escape:
                        this.Close();
                        break;
                }
            }
            catch (Exception ex)
            {
               // MessageBox.Show("Delete the details to procced the item deletion", MstrMessCaption, MessageBoxButtons.OK);
              //  return;
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on grdCommonKeyDown() " + ex.Message.ToString());
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmCommonRef_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MblnChangeStatus == true)
            {
                MstrMessageCommon = "Your changes will be lost.Please confirm if you wish to cancel?";
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    MobjClsBLLCommonRef = null;
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void FrmCommonRef_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }
    }
}