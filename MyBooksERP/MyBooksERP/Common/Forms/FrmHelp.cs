﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmHelp : Form
    {
        public string strFormName = "";

        public FrmHelp()
        {
            InitializeComponent();
        }

        private void FrmHelp_Load(object sender, EventArgs e)
        {
            SetPosition();
            GetHelp();
        }

        private void SetPosition()
        {
            Rectangle WorkingArea = SystemInformation.WorkingArea;
            int intXaxis = WorkingArea.Left + WorkingArea.Width - this.Width;
            int intYaxis = WorkingArea.Top + 100;
            this.Location = new Point(intXaxis, intYaxis);
        }

        private void GetHelp()
        {
            switch (strFormName)
            {
                case "Accountsettings":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Account_settings.html");
                    break;
                case "Alertsettings":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Alert_settings.html");
                    break;
                case "Aproval":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Aproval.html");
                    break;
                case "Backup":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Backup.html");
                    break;
                case "CompanySettings":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\CompanySettings.html");
                    break;
                case "Configuration":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Configuration.html");
                    break;
                case "Createmodify":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Create_modify.html");
                    break;
                case "CreditNote":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\CreditNote.html");
                    break;
                case "Currency":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Currency.html");
                    break;
                case "Customer":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Customer.html");
                    break;
                case "Delivery":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Delivery.html");
                    break;
                case "Deliverynote":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Delivery_note.html");
                    break;
                case "Discount":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Discount.html");
                    break;
                case "EasyView":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Easy_View.html");
                    break;
                case "Email":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Email.html");
                    break;
                case "Employee":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Employee.html");
                    break;
                case "ExtraCharges":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\ExtraCharges.html");
                    break;
                case "Features":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Features.html");
                    break;
                case "GRN":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\GRN.html");
                    break;
                case "introduction":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\introduction.html");
                    break;
                case "Inventory":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Inventory.html");
                    break;
                case "MandatoryDocuments":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\MandatoryDocuments.html");
                    break;
                case "Newcompany":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Newcompany.html");
                    break;
                case "OpeningStock":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\OpeningStock.html");
                    break;
                case "Payments":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Payments.html");
                    break;
                case "Pointofsale":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Pointof_sale.html");
                    break;
                case "ProductGroup":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\ProductGroup.html");
                    break;
                case "Products":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Products.html");
                    break;
                case "Purchase":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Purchase.html");
                    break;
                case "PurchaseIndent":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Purchase_Indent.html");
                    break;
                case "Purchaseorder":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Purchase_order.html");
                    break;
                case "PurchaseQuotation":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Purchase_Quotation.html");
                    break;
                case "Purchaseinvoice":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Purchaseinvoice.html");
                    break;
                case "Receipts":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Receipts.html");
                    break;
                case "Reports":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Reports.html");
                    break;
                case "RFQ":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\RFQ.html");
                    break;
                case "Sales":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Sales.html");
                    break;
                case "Salesinvoice":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Sales_invoice.html");
                    break;
                case "SalesOrder":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Sales_Order.html");
                    break;
                case "Salesquotation":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Salesquotation.html");
                    break;
                case "StockAdjustment":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\StockAdjustment.html");
                    break;
                case "Suppliers":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Suppliers.html");
                    break;
                case "UOM":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\UOM.html");
                    break;
                case "UOMconversion":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\UOM_conversion.html");
                    break;
                case "User":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\User.html");
                    break;
                case "UserRoll":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\UserRoll.html");
                    break;
                case "Warehouses":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Warehouses.html");
                    break;
                default :
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Home.html");
                    break;
            }
        }
    }
}
