﻿namespace MyBooksERP
{
    partial class FrmAlertRoleSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAlertRoleSettings));
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.LblEmployeeStatus = new DevComponents.DotNetBar.LabelItem();
            this.lblAlertRoleSettingsstatus = new DevComponents.DotNetBar.LabelItem();
            this.PnlAlertRoleSetting = new System.Windows.Forms.Panel();
            this.TxtMessage = new System.Windows.Forms.RichTextBox();
            this.txtValidPeriod = new DemoClsDataGridview.NumericTextBox();
            this.lblValidDays = new System.Windows.Forms.Label();
            this.lblValidPeriod = new System.Windows.Forms.Label();
            this.chkbxAllUserSelect = new System.Windows.Forms.CheckBox();
            this.txtReapeatInterval = new DemoClsDataGridview.NumericTextBox();
            this.txtProcessingInterval = new DemoClsDataGridview.NumericTextBox();
            this.btnAlertTemplates = new DevComponents.DotNetBar.ButtonX();
            this.TxtOperationType = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.chkAlertRepeat = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkAlertON = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.CboRole = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblRepeatIntervalDays = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblRepeatInterval = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvUserSettings = new System.Windows.Forms.DataGridView();
            this.LblAlertName = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.eprAlertRoleSetting = new System.Windows.Forms.ErrorProvider(this.components);
            this.TmlblAlertRoleSetting = new System.Windows.Forms.Timer(this.components);
            this.BtnSave = new DevComponents.DotNetBar.ButtonX();
            this.BtnClose = new DevComponents.DotNetBar.ButtonX();
            this.btnOk = new DevComponents.DotNetBar.ButtonX();
            this.cmsAlertTemplates = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAlertUserSettingID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColChkUser = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colUserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProcessingInterval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsRepeat = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colRepatInterval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colValidPeriod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsAlertOn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColIsEmailAlertOn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.chkEmailAlertOn = new DevComponents.DotNetBar.Controls.CheckBoxX();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.PnlAlertRoleSetting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eprAlertRoleSetting)).BeginInit();
            this.SuspendLayout();
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.LblEmployeeStatus,
            this.lblAlertRoleSettingsstatus});
            this.bar1.Location = new System.Drawing.Point(0, 394);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(606, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 83;
            this.bar1.TabStop = false;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "Status:";
            // 
            // LblEmployeeStatus
            // 
            this.LblEmployeeStatus.Name = "LblEmployeeStatus";
            // 
            // lblAlertRoleSettingsstatus
            // 
            this.lblAlertRoleSettingsstatus.Name = "lblAlertRoleSettingsstatus";
            // 
            // PnlAlertRoleSetting
            // 
            this.PnlAlertRoleSetting.Controls.Add(this.chkEmailAlertOn);
            this.PnlAlertRoleSetting.Controls.Add(this.TxtMessage);
            this.PnlAlertRoleSetting.Controls.Add(this.txtValidPeriod);
            this.PnlAlertRoleSetting.Controls.Add(this.lblValidDays);
            this.PnlAlertRoleSetting.Controls.Add(this.lblValidPeriod);
            this.PnlAlertRoleSetting.Controls.Add(this.chkbxAllUserSelect);
            this.PnlAlertRoleSetting.Controls.Add(this.txtReapeatInterval);
            this.PnlAlertRoleSetting.Controls.Add(this.txtProcessingInterval);
            this.PnlAlertRoleSetting.Controls.Add(this.btnAlertTemplates);
            this.PnlAlertRoleSetting.Controls.Add(this.TxtOperationType);
            this.PnlAlertRoleSetting.Controls.Add(this.label8);
            this.PnlAlertRoleSetting.Controls.Add(this.label7);
            this.PnlAlertRoleSetting.Controls.Add(this.chkAlertRepeat);
            this.PnlAlertRoleSetting.Controls.Add(this.chkAlertON);
            this.PnlAlertRoleSetting.Controls.Add(this.CboRole);
            this.PnlAlertRoleSetting.Controls.Add(this.lblRepeatIntervalDays);
            this.PnlAlertRoleSetting.Controls.Add(this.label5);
            this.PnlAlertRoleSetting.Controls.Add(this.lblRepeatInterval);
            this.PnlAlertRoleSetting.Controls.Add(this.label1);
            this.PnlAlertRoleSetting.Controls.Add(this.label4);
            this.PnlAlertRoleSetting.Controls.Add(this.label3);
            this.PnlAlertRoleSetting.Controls.Add(this.dgvUserSettings);
            this.PnlAlertRoleSetting.Controls.Add(this.LblAlertName);
            this.PnlAlertRoleSetting.Controls.Add(this.shapeContainer1);
            this.PnlAlertRoleSetting.Location = new System.Drawing.Point(0, 0);
            this.PnlAlertRoleSetting.Name = "PnlAlertRoleSetting";
            this.PnlAlertRoleSetting.Size = new System.Drawing.Size(605, 358);
            this.PnlAlertRoleSetting.TabIndex = 0;
            // 
            // TxtMessage
            // 
            this.TxtMessage.BackColor = System.Drawing.SystemColors.Info;
            this.TxtMessage.Location = new System.Drawing.Point(112, 134);
            this.TxtMessage.Name = "TxtMessage";
            this.TxtMessage.Size = new System.Drawing.Size(450, 55);
            this.TxtMessage.TabIndex = 21;
            this.TxtMessage.Text = "";
            // 
            // txtValidPeriod
            // 
            this.txtValidPeriod.BackColor = System.Drawing.SystemColors.Info;
            this.txtValidPeriod.Location = new System.Drawing.Point(112, 108);
            this.txtValidPeriod.MaxLength = 3;
            this.txtValidPeriod.Name = "txtValidPeriod";
            this.txtValidPeriod.ShortcutsEnabled = false;
            this.txtValidPeriod.Size = new System.Drawing.Size(64, 20);
            this.txtValidPeriod.TabIndex = 18;
            this.txtValidPeriod.TextChanged += new System.EventHandler(this.txtProcessingInterval_TextChanged);
            this.txtValidPeriod.Leave += new System.EventHandler(this.txtValidPeriod_Leave);
            this.txtValidPeriod.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtProcessingInterval_MouseClick);
            this.txtValidPeriod.Enter += new System.EventHandler(this.txtProcessingInterval_Enter);
            // 
            // lblValidDays
            // 
            this.lblValidDays.AutoSize = true;
            this.lblValidDays.Location = new System.Drawing.Point(182, 111);
            this.lblValidDays.Name = "lblValidDays";
            this.lblValidDays.Size = new System.Drawing.Size(31, 13);
            this.lblValidDays.TabIndex = 19;
            this.lblValidDays.Text = "Days";
            // 
            // lblValidPeriod
            // 
            this.lblValidPeriod.AllowDrop = true;
            this.lblValidPeriod.AutoSize = true;
            this.lblValidPeriod.Location = new System.Drawing.Point(12, 111);
            this.lblValidPeriod.Name = "lblValidPeriod";
            this.lblValidPeriod.Size = new System.Drawing.Size(63, 13);
            this.lblValidPeriod.TabIndex = 20;
            this.lblValidPeriod.Text = "Valid Period";
            // 
            // chkbxAllUserSelect
            // 
            this.chkbxAllUserSelect.AutoSize = true;
            this.chkbxAllUserSelect.Location = new System.Drawing.Point(62, 212);
            this.chkbxAllUserSelect.Name = "chkbxAllUserSelect";
            this.chkbxAllUserSelect.Size = new System.Drawing.Size(15, 14);
            this.chkbxAllUserSelect.TabIndex = 17;
            this.chkbxAllUserSelect.UseVisualStyleBackColor = true;
            this.chkbxAllUserSelect.CheckedChanged += new System.EventHandler(this.chkbxAllUserSelect_CheckedChanged);
            // 
            // txtReapeatInterval
            // 
            this.txtReapeatInterval.BackColor = System.Drawing.SystemColors.Info;
            this.txtReapeatInterval.Location = new System.Drawing.Point(347, 82);
            this.txtReapeatInterval.MaxLength = 3;
            this.txtReapeatInterval.Name = "txtReapeatInterval";
            this.txtReapeatInterval.ShortcutsEnabled = false;
            this.txtReapeatInterval.Size = new System.Drawing.Size(64, 20);
            this.txtReapeatInterval.TabIndex = 4;
            this.txtReapeatInterval.TextChanged += new System.EventHandler(this.txtProcessingInterval_TextChanged);
            this.txtReapeatInterval.Leave += new System.EventHandler(this.txtReapeatInterval_Leave);
            this.txtReapeatInterval.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtProcessingInterval_MouseClick);
            this.txtReapeatInterval.Enter += new System.EventHandler(this.txtProcessingInterval_Enter);
            // 
            // txtProcessingInterval
            // 
            this.txtProcessingInterval.BackColor = System.Drawing.SystemColors.Info;
            this.txtProcessingInterval.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtProcessingInterval.Location = new System.Drawing.Point(112, 82);
            this.txtProcessingInterval.MaxLength = 3;
            this.txtProcessingInterval.Name = "txtProcessingInterval";
            this.txtProcessingInterval.ShortcutsEnabled = false;
            this.txtProcessingInterval.Size = new System.Drawing.Size(64, 20);
            this.txtProcessingInterval.TabIndex = 3;
            this.txtProcessingInterval.TextChanged += new System.EventHandler(this.txtProcessingInterval_TextChanged);
            this.txtProcessingInterval.Leave += new System.EventHandler(this.txtProcessingInterval_Leave);
            this.txtProcessingInterval.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtProcessingInterval_MouseClick);
            this.txtProcessingInterval.Enter += new System.EventHandler(this.txtProcessingInterval_Enter);
            // 
            // btnAlertTemplates
            // 
            this.btnAlertTemplates.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAlertTemplates.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAlertTemplates.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnAlertTemplates.Location = new System.Drawing.Point(568, 134);
            this.btnAlertTemplates.Name = "btnAlertTemplates";
            this.btnAlertTemplates.Size = new System.Drawing.Size(25, 20);
            this.btnAlertTemplates.TabIndex = 7;
            this.btnAlertTemplates.Text = "6";
            this.btnAlertTemplates.Click += new System.EventHandler(this.btnAlertTemplates_Click);
            // 
            // TxtOperationType
            // 
            this.TxtOperationType.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.TxtOperationType.Border.Class = "TextBoxBorder";
            this.TxtOperationType.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtOperationType.Location = new System.Drawing.Point(112, 30);
            this.TxtOperationType.Name = "TxtOperationType";
            this.TxtOperationType.ReadOnly = true;
            this.TxtOperationType.Size = new System.Drawing.Size(299, 20);
            this.TxtOperationType.TabIndex = 0;
            this.TxtOperationType.TabStop = false;
            this.TxtOperationType.Leave += new System.EventHandler(this.txtProcessingInterval_Leave);
            this.TxtOperationType.TextChanged += new System.EventHandler(this.txtProcessingInterval_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Operation Type";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 136);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Message";
            // 
            // chkAlertRepeat
            // 
            // 
            // 
            // 
            this.chkAlertRepeat.BackgroundStyle.Class = "";
            this.chkAlertRepeat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkAlertRepeat.Location = new System.Drawing.Point(347, 102);
            this.chkAlertRepeat.Name = "chkAlertRepeat";
            this.chkAlertRepeat.Size = new System.Drawing.Size(84, 23);
            this.chkAlertRepeat.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkAlertRepeat.TabIndex = 5;
            this.chkAlertRepeat.Text = "Repeat Alert";
            this.chkAlertRepeat.CheckedChanged += new System.EventHandler(this.chkAlertRepeat_CheckedChanged);
            // 
            // chkAlertON
            // 
            // 
            // 
            // 
            this.chkAlertON.BackgroundStyle.Class = "";
            this.chkAlertON.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkAlertON.Location = new System.Drawing.Point(478, 56);
            this.chkAlertON.Name = "chkAlertON";
            this.chkAlertON.Size = new System.Drawing.Size(84, 23);
            this.chkAlertON.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkAlertON.TabIndex = 2;
            this.chkAlertON.Text = "Alert ON";
            this.chkAlertON.CheckedChanged += new System.EventHandler(this.chkAlertON_CheckedChanged);
            // 
            // CboRole
            // 
            this.CboRole.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboRole.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboRole.DisplayMember = "Text";
            this.CboRole.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboRole.FormattingEnabled = true;
            this.CboRole.ItemHeight = 14;
            this.CboRole.Location = new System.Drawing.Point(112, 56);
            this.CboRole.Name = "CboRole";
            this.CboRole.Size = new System.Drawing.Size(299, 20);
            this.CboRole.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboRole.TabIndex = 1;
            this.CboRole.SelectionChangeCommitted += new System.EventHandler(this.CboRole_SelectionChangeCommitted);
            // 
            // lblRepeatIntervalDays
            // 
            this.lblRepeatIntervalDays.AutoSize = true;
            this.lblRepeatIntervalDays.Location = new System.Drawing.Point(417, 86);
            this.lblRepeatIntervalDays.Name = "lblRepeatIntervalDays";
            this.lblRepeatIntervalDays.Size = new System.Drawing.Size(31, 13);
            this.lblRepeatIntervalDays.TabIndex = 6;
            this.lblRepeatIntervalDays.Text = "Days";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(182, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Days";
            // 
            // lblRepeatInterval
            // 
            this.lblRepeatInterval.AutoSize = true;
            this.lblRepeatInterval.Location = new System.Drawing.Point(260, 86);
            this.lblRepeatInterval.Name = "lblRepeatInterval";
            this.lblRepeatInterval.Size = new System.Drawing.Size(80, 13);
            this.lblRepeatInterval.TabIndex = 10;
            this.lblRepeatInterval.Text = "Repeat Interval";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Processing Interval";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "User Settings";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Configure Alert Role Setting";
            // 
            // dgvUserSettings
            // 
            this.dgvUserSettings.AllowUserToAddRows = false;
            this.dgvUserSettings.AllowUserToDeleteRows = false;
            this.dgvUserSettings.AllowUserToResizeRows = false;
            this.dgvUserSettings.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvUserSettings.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvUserSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvUserSettings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colAlertUserSettingID,
            this.ColChkUser,
            this.colUserName,
            this.colProcessingInterval,
            this.colIsRepeat,
            this.colRepatInterval,
            this.colValidPeriod,
            this.colIsAlertOn,
            this.ColIsEmailAlertOn});
            this.dgvUserSettings.Location = new System.Drawing.Point(15, 206);
            this.dgvUserSettings.MultiSelect = false;
            this.dgvUserSettings.Name = "dgvUserSettings";
            this.dgvUserSettings.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvUserSettings.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvUserSettings.Size = new System.Drawing.Size(578, 149);
            this.dgvUserSettings.TabIndex = 8;
            this.dgvUserSettings.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUserSettings_CellValueChanged);
            this.dgvUserSettings.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvUserSettings_RowsAdded);
            this.dgvUserSettings.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvUserSettings_CurrentCellDirtyStateChanged);
            this.dgvUserSettings.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvUserSettings_DataError);
            this.dgvUserSettings.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvUserSettings_RowsRemoved);
            // 
            // LblAlertName
            // 
            this.LblAlertName.AutoSize = true;
            this.LblAlertName.Location = new System.Drawing.Point(12, 60);
            this.LblAlertName.Name = "LblAlertName";
            this.LblAlertName.Size = new System.Drawing.Size(29, 13);
            this.LblAlertName.TabIndex = 4;
            this.LblAlertName.Text = "Role";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(605, 358);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 66;
            this.lineShape2.X2 = 600;
            this.lineShape2.Y1 = 198;
            this.lineShape2.Y2 = 198;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 147;
            this.lineShape1.X2 = 600;
            this.lineShape1.Y1 = 17;
            this.lineShape1.Y2 = 17;
            // 
            // eprAlertRoleSetting
            // 
            this.eprAlertRoleSetting.ContainerControl = this;
            this.eprAlertRoleSetting.RightToLeft = true;
            // 
            // BtnSave
            // 
            this.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnSave.Location = new System.Drawing.Point(15, 365);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnSave.TabIndex = 0;
            this.BtnSave.Text = "&Save";
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnClose
            // 
            this.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnClose.Location = new System.Drawing.Point(518, 365);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Size = new System.Drawing.Size(75, 23);
            this.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnClose.TabIndex = 2;
            this.BtnClose.Text = "&Cancel";
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // btnOk
            // 
            this.btnOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnOk.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnOk.Location = new System.Drawing.Point(437, 365);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&Ok";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // cmsAlertTemplates
            // 
            this.cmsAlertTemplates.Name = "ContextMenuStrip1";
            this.cmsAlertTemplates.Size = new System.Drawing.Size(61, 4);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "AlertUserSettingID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "User Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            dataGridViewCellStyle5.NullValue = "0";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn3.HeaderText = "Processing Interval";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N0";
            dataGridViewCellStyle6.NullValue = "0";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn4.HeaderText = "Repeat Interval";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N0";
            dataGridViewCellStyle7.NullValue = "0";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn5.HeaderText = "Valid Period";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // colAlertUserSettingID
            // 
            this.colAlertUserSettingID.HeaderText = "AlertUserSettingID";
            this.colAlertUserSettingID.Name = "colAlertUserSettingID";
            this.colAlertUserSettingID.ReadOnly = true;
            this.colAlertUserSettingID.Visible = false;
            // 
            // ColChkUser
            // 
            this.ColChkUser.HeaderText = "";
            this.ColChkUser.Name = "ColChkUser";
            this.ColChkUser.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColChkUser.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColChkUser.Width = 25;
            // 
            // colUserName
            // 
            this.colUserName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colUserName.HeaderText = "User Name";
            this.colUserName.Name = "colUserName";
            this.colUserName.ReadOnly = true;
            // 
            // colProcessingInterval
            // 
            this.colProcessingInterval.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = "0";
            this.colProcessingInterval.DefaultCellStyle = dataGridViewCellStyle2;
            this.colProcessingInterval.HeaderText = "Processing Interval";
            this.colProcessingInterval.MaxInputLength = 3;
            this.colProcessingInterval.Name = "colProcessingInterval";
            this.colProcessingInterval.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colProcessingInterval.Width = 122;
            // 
            // colIsRepeat
            // 
            this.colIsRepeat.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.colIsRepeat.HeaderText = "Is Repeat";
            this.colIsRepeat.Name = "colIsRepeat";
            this.colIsRepeat.Visible = false;
            this.colIsRepeat.Width = 59;
            // 
            // colRepatInterval
            // 
            this.colRepatInterval.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = "0";
            this.colRepatInterval.DefaultCellStyle = dataGridViewCellStyle3;
            this.colRepatInterval.HeaderText = "Repeat Interval";
            this.colRepatInterval.MaxInputLength = 3;
            this.colRepatInterval.Name = "colRepatInterval";
            this.colRepatInterval.Visible = false;
            this.colRepatInterval.Width = 105;
            // 
            // colValidPeriod
            // 
            this.colValidPeriod.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N0";
            dataGridViewCellStyle4.NullValue = "0";
            this.colValidPeriod.DefaultCellStyle = dataGridViewCellStyle4;
            this.colValidPeriod.HeaderText = "Valid Period";
            this.colValidPeriod.MaxInputLength = 3;
            this.colValidPeriod.Name = "colValidPeriod";
            this.colValidPeriod.Width = 88;
            // 
            // colIsAlertOn
            // 
            this.colIsAlertOn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.colIsAlertOn.HeaderText = "Alert ON";
            this.colIsAlertOn.Name = "colIsAlertOn";
            this.colIsAlertOn.Width = 53;
            // 
            // ColIsEmailAlertOn
            // 
            this.ColIsEmailAlertOn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.ColIsEmailAlertOn.HeaderText = "Email Alert ON";
            this.ColIsEmailAlertOn.Name = "ColIsEmailAlertOn";
            this.ColIsEmailAlertOn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColIsEmailAlertOn.Width = 81;
            // 
            // chkEmailAlertOn
            // 
            this.chkEmailAlertOn.AutoSize = true;
            // 
            // 
            // 
            this.chkEmailAlertOn.BackgroundStyle.Class = "";
            this.chkEmailAlertOn.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkEmailAlertOn.Location = new System.Drawing.Point(478, 87);
            this.chkEmailAlertOn.Name = "chkEmailAlertOn";
            this.chkEmailAlertOn.Size = new System.Drawing.Size(95, 15);
            this.chkEmailAlertOn.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkEmailAlertOn.TabIndex = 22;
            this.chkEmailAlertOn.Text = "Email Alert ON";
            this.chkEmailAlertOn.CheckedChanged += new System.EventHandler(this.chkEmailAlertOn_CheckedChanged);
            // 
            // FrmAlertRoleSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 413);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BtnClose);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.PnlAlertRoleSetting);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAlertRoleSettings";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alert Role Settings";
            this.Load += new System.EventHandler(this.FrmAlertRoleSetting_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAlertRoleSetting_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.PnlAlertRoleSetting.ResumeLayout(false);
            this.PnlAlertRoleSetting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eprAlertRoleSetting)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.LabelItem LblEmployeeStatus;
        private System.Windows.Forms.Panel PnlAlertRoleSetting;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvUserSettings;
        private System.Windows.Forms.Label LblAlertName;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Label lblRepeatInterval;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRepeatIntervalDays;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ErrorProvider eprAlertRoleSetting;
        private DevComponents.DotNetBar.LabelItem lblAlertRoleSettingsstatus;
        private System.Windows.Forms.Timer TmlblAlertRoleSetting;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboRole;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkAlertRepeat;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkAlertON;
        private DevComponents.DotNetBar.ButtonX BtnClose;
        private DevComponents.DotNetBar.ButtonX BtnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DevComponents.DotNetBar.ButtonX btnOk;
        private System.Windows.Forms.Label label7;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtOperationType;
        private System.Windows.Forms.Label label8;
        private DevComponents.DotNetBar.ButtonX btnAlertTemplates;
        internal System.Windows.Forms.ContextMenuStrip cmsAlertTemplates;
        private DemoClsDataGridview.NumericTextBox txtReapeatInterval;
        private DemoClsDataGridview.NumericTextBox txtProcessingInterval;
        private System.Windows.Forms.CheckBox chkbxAllUserSelect;
        private DemoClsDataGridview.NumericTextBox txtValidPeriod;
        private System.Windows.Forms.Label lblValidDays;
        private System.Windows.Forms.Label lblValidPeriod;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.RichTextBox TxtMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAlertUserSettingID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColChkUser;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProcessingInterval;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIsRepeat;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRepatInterval;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValidPeriod;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIsAlertOn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColIsEmailAlertOn;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkEmailAlertOn;
    }
}