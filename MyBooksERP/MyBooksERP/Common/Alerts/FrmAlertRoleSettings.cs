﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmAlertRoleSettings : DevComponents.DotNetBar.Office2007Form
    {        
        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;
        private MessageBoxIcon MmessageIcon;
        ClsNotification mObjNotification;
        private string MstrMessageCommon;
        private string MstrMessageCaption;

        private bool MbShowErrorMess;
        private long MlngAlertRoleSettingsID, MlngAlertSettingsID, MlngAlertTypeID;
        private bool MblnAddStatus;
        private bool MblnChangeStatus;
        clsBLLCommonUtility MobjBLLCommonUtility = new clsBLLCommonUtility();
        clsBLLAlertRoleSettings MobjclsBLLAlertRoleSettings;
        ClsLogWriter MobjClsLogWriter;      // Object of the LogWriter class

        public long AlertSettingID
        {
            get { return this.MlngAlertSettingsID; }
            set { this.MlngAlertSettingsID = value; }
        }

        public long AlertTypeID
        {
            get { return this.MlngAlertTypeID; }
            set { this.MlngAlertTypeID = value; }
        }

        public string OperationType
        {
            get { return this.TxtOperationType.Text; }
            set { this.TxtOperationType.Text = value; }
        }

        public FrmAlertRoleSettings()
        {
            this.InitializeComponent();

            this.dgvUserSettings.Columns["colProcessingInterval"].ValueType = typeof(int);
            this.dgvUserSettings.Columns["colRepatInterval"].ValueType = typeof(int);
            this.dgvUserSettings.Columns["colValidPeriod"].ValueType = typeof(int);
            this.MbShowErrorMess = true;
            this.mObjNotification = new ClsNotification();
            this.MobjclsBLLAlertRoleSettings = new clsBLLAlertRoleSettings();
            this.MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            this.LoadMessage();
            this.LoadCombos(0);
            this.AddNewUserAlertSettings();           
        }

        ~FrmAlertRoleSettings()
        {
            this.MbShowErrorMess = false;
        }

        private void FrmAlertRoleSetting_Load(object sender, EventArgs e)
        {
            this.MblnChangeStatus = false;
            this.LoadAlertTemplates();
            this.CboRole.Focus();

            //if (AlertSettingID >= 5 && AlertSettingID <= 13)
            if(AlertSettingID == (int)AlertSettingsTypes.PurchaseQuotationDueDate || AlertSettingID == (int)AlertSettingsTypes.PurchaseOrderDueDate ||
                    AlertSettingID == (int)AlertSettingsTypes.SalesQuotationDueDate || AlertSettingID == (int)AlertSettingsTypes.SalesOrderDueDate ||
                    AlertSettingID == (int)AlertSettingsTypes.SalesInvoiceDueDate || AlertSettingID == (int)AlertSettingsTypes.DocumentExpiry)
            {
                txtProcessingInterval.Enabled = true;
                colProcessingInterval.ReadOnly = false;
            }
            else
            {
                txtProcessingInterval.Enabled = false;
                colProcessingInterval.ReadOnly = true;
            }
            if (AlertTypeID == (int)AlertTypes.Alert)
            {
                lblRepeatInterval.Visible = txtReapeatInterval.Visible = lblRepeatIntervalDays.Visible = chkAlertRepeat.Visible = false;
                colRepatInterval.Visible = colIsRepeat.Visible = false;
                colRepatInterval.ReadOnly = colIsRepeat.ReadOnly = true;
            }
            else
            {
                lblRepeatInterval.Visible = txtReapeatInterval.Visible = lblRepeatIntervalDays.Visible = chkAlertRepeat.Visible = true;
                colRepatInterval.Visible = colIsRepeat.Visible = true;
                colRepatInterval.ReadOnly = colIsRepeat.ReadOnly = false;
            }
            if (AlertSettingID == (int)AlertSettingsTypes.ReorderLevelEntered
                || AlertSettingID == (int)AlertSettingsTypes.RecurringJournalAlert || AlertSettingID == (int)AlertSettingsTypes.PostDatedCheque)
            {
                txtValidPeriod.Enabled = false;
                colValidPeriod.ReadOnly = true;
            }
            else
            {
                txtValidPeriod.Enabled = true;
                colValidPeriod.ReadOnly = false;
            }
        }

        private void CboRole_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                this.ClearAllControls();
                this.MblnChangeStatus = false;
                if (this.CboRole.SelectedIndex == -1)
                    return;
                if (this.MobjclsBLLAlertRoleSettings.GetAlertRoleSetting(this.MlngAlertSettingsID, (int)this.CboRole.SelectedValue))
                    this.FillAlertRoleSetting();
                this.FillRoleUserSetting();
                this.MblnChangeStatus = false;
                if (TxtOperationType.Text.Contains("Recurring Journal"))
                {
                    lblValidPeriod.Enabled = false;
                    txtValidPeriod.Enabled = false;
                    lblValidDays.Enabled = false;
                    chkAlertRepeat.Checked = true;
                    chkAlertRepeat.Enabled = false;
                    txtReapeatInterval.Text = "1";
                    txtReapeatInterval.Enabled = false;
                }
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on filling user settings:CboRole_SelectionChangeCommitted " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on CboRole_SelectionChangeCommitted() " + Ex.Message.ToString());
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.SaveAlertRoleSetting();
                this.MblnChangeStatus = false;
                this.CboRole.Focus();
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on filling user settings:CboRole_SelectionChangeCommitted " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on CboRole_SelectionChangeCommitted() " + Ex.Message.ToString());
            }
        }

        private void chkAlertRepeat_CheckedChanged(object sender, EventArgs e)
        {
            this.MblnChangeStatus = true;
            if (this.txtReapeatInterval.Text.Trim().Length == 0 && this.chkAlertRepeat.Checked == false)
                this.txtReapeatInterval.Text = "0";

            foreach (DataGridViewRow dgvRow in this.dgvUserSettings.Rows)
            {
                dgvRow.Cells["colIsRepeat"].Value = this.chkAlertRepeat.Checked;
            }
        }

        private void chkAlertON_CheckedChanged(object sender, EventArgs e)
        {
            this.MblnChangeStatus = true;
            foreach (DataGridViewRow dgvRow in this.dgvUserSettings.Rows)
            {
                dgvRow.Cells["colIsAlertOn"].Value = this.chkAlertON.Checked;
            }
        }

        private void chkEmailAlertOn_CheckedChanged(object sender, EventArgs e)
        {
            this.MblnChangeStatus = true;
            foreach (DataGridViewRow dgvRow in this.dgvUserSettings.Rows)
            {
                dgvRow.Cells["colIsEmailAlertOn"].Value = this.chkEmailAlertOn.Checked;
            }
        } 

        private void txtProcessingInterval_Leave(object sender, EventArgs e)
        {
            int iDays = 0;
            try
            {
                if (this.txtProcessingInterval.Text.Trim().Length > 0)
                    iDays = int.Parse(this.txtProcessingInterval.Text.Trim());
                foreach (DataGridViewRow dgvRow in this.dgvUserSettings.Rows)
                {
                    dgvRow.Cells["colProcessingInterval"].Value = iDays;
                }
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on filling Processing Interval:txtProcessingInterval_Leave " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on txtProcessingInterval_Leave() " + Ex.Message.ToString());
            }
        }

        private void txtReapeatInterval_Leave(object sender, EventArgs e)
        {
            int iDays = 0;
            try
            {
                if (this.txtReapeatInterval.Text.Trim().Length > 0)
                    iDays = int.Parse(this.txtReapeatInterval.Text.Trim());
                foreach (DataGridViewRow dgvRow in this.dgvUserSettings.Rows)
                {
                    dgvRow.Cells["colRepatInterval"].Value = iDays;
                }
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on filling Reapeat Interval:txtReapeatInterval_Leave " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on txtReapeatInterval_Leave() " + Ex.Message.ToString());
            }
        }

        private void txtValidPeriod_Leave(object sender, EventArgs e)
        {
            int iDays = 0;
            try
            {
                if (this.txtValidPeriod.Text.Trim().Length > 0)
                    iDays = int.Parse(this.txtValidPeriod.Text.Trim());
                foreach (DataGridViewRow dgvRow in this.dgvUserSettings.Rows)
                {
                    dgvRow.Cells["colValidPeriod"].Value = iDays;
                }
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on filling Valid Period:txtValidPeriod_Leave " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on txtValidPeriod_Leave() " + Ex.Message.ToString());
            }
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmAlertRoleSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (MblnChangeStatus)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        mObjNotification = null;
                        this.MobjclsBLLAlertRoleSettings = null;
                        e.Cancel = false;
                    }
                    else
                        e.Cancel = true;
                }
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on form Form Closing:FrmAlertRoleSetting_FormClosing " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmAlertRoleSetting_FormClosing " + Ex.Message.ToString());
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveAlertRoleSetting())
                    this.Close();
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on filling user settings:CboRole_SelectionChangeCommitted " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on CboRole_SelectionChangeCommitted() " + Ex.Message.ToString());

            }
        }

        private void txtProcessingInterval_TextChanged(object sender, EventArgs e)
        {
            this.MblnChangeStatus = true;
        }
        
        private void LoadMessage()
        {
            MaMessageArr = mObjNotification.FillMessageArray((int)FormID.AlertSetting, ClsCommonSettings.ProductID);
            MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.AlertSetting, (int)ClsCommonSettings.ProductID);
        }

        private bool LoadAlertTemplates()
        {
            DataTable datTemplates = MobjclsBLLAlertRoleSettings.FillCombos(new string[] { "TemplateName,TemplateValue", "AlertTemplates", "AlertSettingsID = " + MlngAlertSettingsID });
            cmsAlertTemplates.Items.Clear();

            foreach (DataRow dr in datTemplates.Rows)
            {
                ToolStripMenuItem tsMenuItem = new ToolStripMenuItem();
                tsMenuItem.Text = Convert.ToString(dr["TemplateName"]);
                tsMenuItem.Tag = Convert.ToString(dr["TemplateValue"]);
                cmsAlertTemplates.Items.Add(tsMenuItem);
            }
            cmsAlertTemplates.ItemClicked -= new ToolStripItemClickedEventHandler(cmsAlertTemplates_ItemClicked);
                cmsAlertTemplates.ItemClicked += new ToolStripItemClickedEventHandler(cmsAlertTemplates_ItemClicked);
                return true;
        }

        void cmsAlertTemplates_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Tag != null)
            {
                TxtMessage.Text += Convert.ToString(e.ClickedItem.Tag);
            }
        }

        private bool LoadCombos(int Type)
        {
            try
            {
                if (Type == 0 || Type == 1)
                {
                    DataTable datCombos = new DataTable();
                    datCombos = this.MobjclsBLLAlertRoleSettings.FillCombos(new string[] { "RoleID,RoleName", "RoleReference", "RoleID <> 1" });//"Type<>0" });
                    this.CboRole.ValueMember = "RoleID";
                    this.CboRole.DisplayMember = "RoleName";
                    this.CboRole.DataSource = datCombos;
                    this.CboRole.SelectedIndex = -1;
                }
                //this.MobjClsLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
                return false;
            }
        }

        private bool AddNewUserAlertSettings()
        {
            try
            {
                this.MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3258, out MmessageIcon);
                this.lblAlertRoleSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmlblAlertRoleSetting.Enabled = true;
                this.MblnAddStatus = false;

                ClearAllControls();
                this.chkAlertON.Checked = true;
                this.chkEmailAlertOn.Checked = true;
                this.chkAlertRepeat.Checked = true;
                this.txtProcessingInterval.Text = "0";
                this.txtReapeatInterval.Text = "1";
                this.txtValidPeriod.Text = "0";
                this.CboRole.Focus();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        private void ClearAllControls()
        {
            this.MlngAlertRoleSettingsID = 0;
            this.txtProcessingInterval.Text = "0";
            this.txtReapeatInterval.Text = "1";
            this.txtValidPeriod.Text = "0";
            this.TxtMessage.Clear();
            this.chkAlertON.Checked = true;
            this.chkEmailAlertOn.Checked = true;
            this.chkAlertRepeat.Checked = true;
            this.dgvUserSettings.Rows.Clear();
        }
        
        private void FillParameters()
        {
            try
            {
                this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.blnIsAlertOn = this.chkAlertON.Checked;
                this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.blnIsEmailAlertOn = this.chkEmailAlertOn.Checked;
                this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.blnIsRepeat = this.chkAlertRepeat.Checked;
                this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.lngAlertRoleSettingID = this.MlngAlertRoleSettingsID;
                this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.lngAlertSettingID = this.MlngAlertSettingsID;
                this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.intProcessingInterval = int.Parse(this.txtProcessingInterval.Text);
                this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.intRepeatInterval = int.Parse(this.txtReapeatInterval.Text);
                if (this.txtValidPeriod.Text.Trim()!="")
                this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.intValidPeriod = int.Parse(this.txtValidPeriod.Text);
                this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.intRoleID = Convert.ToInt32(this.CboRole.SelectedValue);
                this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.strAlertMessage = this.TxtMessage.Text;

                foreach (DataGridViewRow dgvRow in this.dgvUserSettings.Rows)
                {
                    if (Convert.ToInt32(dgvRow.Cells["ColChkUser"].Value) == 1 || Convert.ToBoolean(dgvRow.Cells["ColChkUser"].Value) == true)
                    {
                        this.MobjclsBLLAlertRoleSettings.DTOAlertUserSettings.Enqueue(new clsDTOAlertUserSettings
                        {
                            blnIsAlertOn = Convert.ToBoolean(dgvRow.Cells["colIsAlertOn"].Value),
                            blnIsEmailAlertOn = Convert.ToBoolean(dgvRow.Cells["colIsEmailAlertOn"].Value),
                            blnIsRepeat = Convert.ToBoolean(dgvRow.Cells["colIsRepeat"].Value),
                            lngAlertSettingID = this.MlngAlertSettingsID,
                            lngAlertUserSettingID = 0,
                            intProcessingInterval = dgvRow.Cells["colProcessingInterval"].Value == DBNull.Value ? 0 : dgvRow.Cells["colProcessingInterval"].Value.ToInt32(),
                            intRepaetInterval = dgvRow.Cells["colRepatInterval"].Value == DBNull.Value ? 0 : dgvRow.Cells["colRepatInterval"].Value.ToInt32(),
                            intValidPeriod = dgvRow.Cells["colValidPeriod"].Value == DBNull.Value ? 0 : dgvRow.Cells["colValidPeriod"].Value.ToInt32(),
                            intUserID = dgvRow.Cells["colUserName"].Tag.ToInt32()
                        });
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }

        private bool AlertUserSettingsValidation()
        {
            try
            {
                if (this.CboRole.SelectedIndex == -1 || this.CboRole.SelectedValue == null)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8603, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    eprAlertRoleSetting.SetError(this.CboRole, MstrMessageCommon.Replace("#", "").Trim());
                    lblAlertRoleSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmlblAlertRoleSetting.Enabled = true;
                    this.CboRole.Focus();
                    return false;
                }
                if (this.txtProcessingInterval.Text.Trim().Length == 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8604, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    eprAlertRoleSetting.SetError(this.txtProcessingInterval, MstrMessageCommon.Replace("#", "").Trim());
                    lblAlertRoleSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmlblAlertRoleSetting.Enabled = true;
                    txtProcessingInterval.Focus();
                    return false;
                }
                if (this.txtReapeatInterval.Text.Trim().Length == 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8603, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    eprAlertRoleSetting.SetError(this.txtProcessingInterval, MstrMessageCommon.Replace("#", "").Trim());
                    lblAlertRoleSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmlblAlertRoleSetting.Enabled = true;
                    txtReapeatInterval.Focus();
                    return false;
                }
                if (this.TxtMessage.Text.Trim().Length == 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8607, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    eprAlertRoleSetting.SetError(TxtMessage, MstrMessageCommon.Replace("#", "").Trim());
                    lblAlertRoleSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmlblAlertRoleSetting.Enabled = true;
                    TxtMessage.Focus();
                    return false;
                }
                if (dgvUserSettings.Rows.Count == 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8608, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblAlertRoleSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmlblAlertRoleSetting.Enabled = true;                   
                    return false;
                }
                if (this.dgvUserSettings.RowCount > 0)
                {
                    int iSelectedRows = 0;                    
                    foreach (DataGridViewRow dgvRow in this.dgvUserSettings.Rows)
                    {
                        if (Convert.ToBoolean(dgvRow.Cells["ColChkUser"].Value) == true || Convert.ToInt32(dgvRow.Cells["ColChkUser"].Value) == 1)
                        {
                            iSelectedRows += 1;
                        }
                    }
                    if (iSelectedRows == 0)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8610, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblAlertRoleSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmlblAlertRoleSetting.Enabled = true;
                        return false;
                    }
                }

                int iCount = 0;
                bool CheckDuplicate=this.MobjclsBLLAlertRoleSettings.CheckDuplicate(MlngAlertSettingsID, Convert.ToInt32(CboRole.SelectedValue),out iCount);
                if (MblnAddStatus)
                {
                    if (CheckDuplicate == true)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8608, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        eprAlertRoleSetting.SetError(TxtMessage, MstrMessageCommon.Replace("#", "").Trim());
                        lblAlertRoleSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmlblAlertRoleSetting.Enabled = true;
                        return false;
                    }
                }
                else
                {
                    if (CheckDuplicate == true && iCount > 1)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8609, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        eprAlertRoleSetting.SetError(TxtMessage, MstrMessageCommon.Replace("#", "").Trim());
                        lblAlertRoleSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmlblAlertRoleSetting.Enabled = true;
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool ViewAlertRoleSetting(int iAlertRoleSetting)
        {
            try
            {
                if (!this.MobjclsBLLAlertRoleSettings.GetAlertRoleSetting(iAlertRoleSetting))
                    return false;
                this.MlngAlertSettingsID = this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.lngAlertSettingID;
                this.FillAlertRoleSetting();
                this.FillRoleUserSetting();
                this.MblnChangeStatus = false;
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        private void FillAlertRoleSetting()
        {
            try
            {
                this.txtProcessingInterval.Text = this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.intProcessingInterval.ToString();
                this.txtReapeatInterval.Text = this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.intRepeatInterval.ToString();                
                this.chkAlertRepeat.Checked = this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.blnIsRepeat;
                this.txtValidPeriod.Text = this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.intValidPeriod.ToString();
                this.CboRole.SelectedValue = this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.intRoleID;
                this.chkAlertON.Checked = this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.blnIsAlertOn;
                this.chkEmailAlertOn.Checked = this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.blnIsEmailAlertOn;
                this.MlngAlertRoleSettingsID = this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.lngAlertRoleSettingID;
                this.TxtMessage.Text = this.MobjclsBLLAlertRoleSettings.DTOAlertRoleSettings.strAlertMessage;
            }
            catch (Exception ex) { throw ex; }
        }

        private void FillRoleUserSetting()
        {
            DataRow[] dtUserRows;
            try
            {
                dtUserRows = this.MobjclsBLLAlertRoleSettings.GetAlertSettingUsers(this.MlngAlertSettingsID, (int)this.CboRole.SelectedValue);
                foreach (DataRow dtRow in dtUserRows)
                {
                    int intRowPos = this.dgvUserSettings.Rows.Add();
                    this.dgvUserSettings.Rows[intRowPos].Cells["ColChkUser"].Value = dtRow["Checked"];
                    this.dgvUserSettings.Rows[intRowPos].Cells["colUserName"].Tag = dtRow["UserID"];
                    this.dgvUserSettings.Rows[intRowPos].Cells["colUserName"].Value = dtRow["UserName"];
                    this.dgvUserSettings.Rows[intRowPos].Cells["colIsAlertOn"].Value = dtRow["IsAlertON"];
                    this.dgvUserSettings.Rows[intRowPos].Cells["colIsEmailAlertOn"].Value = dtRow["IsEmailAlertON"];
                    this.dgvUserSettings.Rows[intRowPos].Cells["colAlertUserSettingID"].Value = dtRow["AlertUserSettingID"];
                    if (Convert.ToInt32(dtRow["AlertUserSettingID"]) == 0)
                    {
                        this.dgvUserSettings.Rows[intRowPos].Cells["colProcessingInterval"].Value = this.txtProcessingInterval.Text;
                        this.dgvUserSettings.Rows[intRowPos].Cells["colRepatInterval"].Value = this.txtReapeatInterval.Text;
                        this.dgvUserSettings.Rows[intRowPos].Cells["colIsRepeat"].Value = this.chkAlertRepeat.Checked;
                        this.dgvUserSettings.Rows[intRowPos].Cells["colValidPeriod"].Value = this.txtValidPeriod.Text;
                    }
                    else
                    {
                        this.dgvUserSettings.Rows[intRowPos].Cells["colProcessingInterval"].Value = dtRow["ProcessingInterval"];
                        this.dgvUserSettings.Rows[intRowPos].Cells["colRepatInterval"].Value = dtRow["RepeatInterval"];
                        this.dgvUserSettings.Rows[intRowPos].Cells["colIsRepeat"].Value = dtRow["IsRepeat"];
                        this.dgvUserSettings.Rows[intRowPos].Cells["colValidPeriod"].Value = dtRow["ValidPeriod"];
                    }
                }
                this.dgvUserSettings.Update();
            }
            catch (Exception ex) { throw ex; }
        }

        private bool SaveAlertRoleSetting()
        {
            try
            {
                if (!this.AlertUserSettingsValidation())
                    return false;

                if (MblnAddStatus)
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);
                else
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;

                this.FillParameters();

                if (MblnAddStatus==false)
                    this.MobjclsBLLAlertRoleSettings.DeleteAlertRoleSetting(MlngAlertSettingsID,Convert.ToInt32(CboRole.SelectedValue));// delete Before Save

                if (this.MobjclsBLLAlertRoleSettings.SaveAlertUserSettings())
                {
                    this.MblnChangeStatus = false;
                    this.MobjClsLogWriter.WriteLog("Saved successfully:SaveAlertUserSettings()  " + this.Name + "", 0);

                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    this.lblAlertRoleSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    this.TmlblAlertRoleSetting.Enabled = true;
                    this.MblnChangeStatus = false;
                   
                    AddNewUserAlertSettings(); 
                    this.CboRole.SelectedIndex = -1;
                    return true;
                }
                return false;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void btnAlertTemplates_Click(object sender, EventArgs e)
        {
            cmsAlertTemplates.Show(btnAlertTemplates, btnAlertTemplates.PointToClient(System.Windows.Forms.Cursor.Position));
        }

        private void txtProcessingInterval_MouseClick(object sender, MouseEventArgs e)
        {
            System.Windows.Forms.Clipboard.Clear();
        }

        private void txtProcessingInterval_Enter(object sender, EventArgs e)
        {
            System.Windows.Forms.Clipboard.Clear();
        }

        private void chkbxAllUserSelect_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvRow in this.dgvUserSettings.Rows)
            {
                dgvRow.Cells["ColChkUser"].Value = chkbxAllUserSelect.Checked;
                dgvRow.Cells["colIsAlertOn"].Value = chkbxAllUserSelect.Checked;
                dgvRow.Cells["colIsEmailAlertOn"].Value = chkbxAllUserSelect.Checked;
            }
            this.MblnChangeStatus = true;
        }

        private void dgvUserSettings_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjBLLCommonUtility.SetSerialNo(dgvUserSettings, e.RowIndex, true);
        }

        private void dgvUserSettings_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjBLLCommonUtility.SetSerialNo(dgvUserSettings, e.RowIndex, true);
        }

        private void dgvUserSettings_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvUserSettings.Columns["ColChkUser"].Index)
                {
                    dgvUserSettings.Rows[e.RowIndex].Cells["colIsAlertOn"].Value = Convert.ToBoolean(dgvUserSettings.Rows[e.RowIndex].Cells["ColChkUser"].Value);
                    dgvUserSettings.Rows[e.RowIndex].Cells["ColIsEmailAlertOn"].Value = Convert.ToBoolean(dgvUserSettings.Rows[e.RowIndex].Cells["ColChkUser"].Value);
                }
            }
            this.MblnChangeStatus = true;
        }

        private void dgvUserSettings_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgvUserSettings.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dgvUserSettings_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }
    }
}