﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmAlertSettings : DevComponents.DotNetBar.Office2007Form
    {       
        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;
        private MessageBoxIcon MmessageIcon;
        ClsNotification mObjNotification;
        private string MstrMessageCommon;
        private string MstrMessageCaption;

        private int MiTimerInterval;
        private bool MbShowErrorMess;
        private long MlngAlertSettingsID;
        private bool MblnAddStatus;
        private bool MblnChangeStatus;
        private int MintCurrentRecCnt, MintRecordCnt;
        clsBLLCommonUtility MobjBLLCommonUtility = new clsBLLCommonUtility();
        clsBLLAlertSetting MobjclsBLLAlertSetting;
        ClsLogWriter MobjClsLogWriter;      // Object of the LogWriter class
        private  List<long> lnglstAlertRoleSettingsID;
        public int ModuleID { get; set; }

        public FrmAlertSettings()
        {
            this.InitializeComponent();
            this.MobjclsBLLAlertSetting = new clsBLLAlertSetting();
            this.MbShowErrorMess = true;
            this.MaMessageArr = new ArrayList();
            this.MaStatusMessage = new ArrayList();
            this.MmessageIcon = MessageBoxIcon.Information;
            this.mObjNotification = new ClsNotification();
            this.MiTimerInterval = ClsCommonSettings.TimerInterval;
            this.MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            this.lnglstAlertRoleSettingsID=new List<long>();
            this.MlngAlertSettingsID = 0;
            this.MblnAddStatus = true;
            this.MblnChangeStatus = false;
            this.MstrMessageCaption = ClsCommonSettings.MessageCaption;
            this.MintCurrentRecCnt = 1; this.MintRecordCnt = 0;
        }

        ~FrmAlertSettings()
        {
            this.MbShowErrorMess = false;
            if (this.MaMessageArr != null)
            {
                this.MaMessageArr.Clear();
                this.MaMessageArr = null;
            }
            if (this.MaStatusMessage != null)
            {
                this.MaStatusMessage.Clear();
                this.MaStatusMessage = null;
            }
            this.mObjNotification = null;
            this.MiTimerInterval = 0;
            this.MobjclsBLLAlertSetting = null;
            this.MobjClsLogWriter = null;
            this.MlngAlertSettingsID = 0;
            this.MblnAddStatus = false;
            this.MblnChangeStatus = false;
            this.MstrMessageCaption = null;
            this.MintCurrentRecCnt = 0;
        }

        private void FrmAlertSetting_Load(object sender, EventArgs e)
        {
            this.DgvRoles.Columns["colProcessingInterval"].ValueType = typeof(int);
            this.DgvRoles.Columns["colRepeatInterval"].ValueType = typeof(int);
            this.DgvRoles.Columns["colIsRepeat"].ValueType = typeof(bool);
            this.DgvRoles.Columns["colValidPeriod"].ValueType = typeof(int);
            this.DgvRoles.Columns["colIsAlertON"].ValueType = typeof(bool);
            this.DgvRoles.Columns["colIsEmailAlertON"].ValueType = typeof(bool);
            this.LoadMessage();
            this.LoadCombos(0);
            this.MobjclsBLLAlertSetting.DTOAlertSettings.intModuleID = ModuleID;
            this.MintRecordCnt = this.MobjclsBLLAlertSetting.RecCountNavigate();           
            this.DisplayAlertInformation();
            this.SetBindingNavigatorButtons();
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            this.AddNewAlertSettings();
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.SaveAlertSettingInfo())
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                   // this.MobjclsBLLAlertSetting.DTOAlertSettings.intModuleID = ModuleID;
                    //this.MintRecordCnt = this.MintCurrentRecCnt = this.MobjclsBLLAlertSetting.RecCountNavigate();
                    //this.SetBindingNavigatorButtons();
                    //this.TmAlertSettings.Enabled = true;
                    //this.DisplayAlertInformation();
                }
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on BindingNavigatorSAveItem: " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on Bindingnavigatorsaveitem " + Ex.Message.ToString());
            }
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            this.AddNewAlertSettings();
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            try
            {
                ErrAlertSettings.Clear();
                this.MobjclsBLLAlertSetting.DTOAlertSettings.intModuleID = ModuleID;
                if (this.MobjclsBLLAlertSetting.RecCountNavigate() > 0)
                {
                    MblnAddStatus = false;
                    MintCurrentRecCnt = 1;
                }
                else
                    MintCurrentRecCnt = 0;

                this.DisplayAlertInformation();
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9, out MmessageIcon);
                SetBindingNavigatorButtons();
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on form MoveFirstItem:MoveFirstItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveFirstItem_Click " + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.MobjclsBLLAlertSetting.DTOAlertSettings.intModuleID = ModuleID;
                if (this.MobjclsBLLAlertSetting.RecCountNavigate() > 0)
                {
                    MblnAddStatus = false;
                    MintCurrentRecCnt = MintCurrentRecCnt - 1;

                    if (MintCurrentRecCnt <= 0)
                        MintCurrentRecCnt = 1;
                }
                else
                {
                    BindingNavigatornPositionItem.Text = "of 0";
                    this.MintRecordCnt = 0;
                }
                this.DisplayAlertInformation();
                MstrMessageCommon = this.mObjNotification.GetErrorMessage(MaMessageArr, 10, out MmessageIcon);
                SetBindingNavigatorButtons();
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on form MovePreviousItem:MovePreviousItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MovePreviousItem_Click " + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.MobjclsBLLAlertSetting.DTOAlertSettings.intModuleID = ModuleID;
                MintRecordCnt = this.MobjclsBLLAlertSetting.RecCountNavigate();

                if (MintRecordCnt > 0)
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                    MblnAddStatus = false;
                    if (MintCurrentRecCnt >= MintRecordCnt)
                    {
                        MintCurrentRecCnt = MintRecordCnt;
                    }
                }
                else
                    MintCurrentRecCnt = 0;

                this.DisplayAlertInformation();
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 11, out MmessageIcon);
                SetBindingNavigatorButtons();
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on form MoveNextItem:MoveNextItem_Click" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveNextItem_Click" + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            try
            {
                ErrAlertSettings.Clear();
                this.MobjclsBLLAlertSetting.DTOAlertSettings.intModuleID = ModuleID;
                MintRecordCnt = this.MobjclsBLLAlertSetting.RecCountNavigate();

                if (MintRecordCnt > 0)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    MblnAddStatus = false;
                }
                else
                    MintCurrentRecCnt = 0;

                this.DisplayAlertInformation();
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 12, out MmessageIcon);
                SetBindingNavigatorButtons();
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on form MoveLastItem:MoveLastItem_Click" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveLastItem_Click" + Ex.Message.ToString());
            }
        }

        private void btnRoleSetting_Click(object sender, EventArgs e)
        {
            try
            {
                FrmAlertRoleSettings objFrmAlertRoleSetting;
                objFrmAlertRoleSetting = new FrmAlertRoleSettings
                {
                    AlertSettingID = MlngAlertSettingsID,
                    AlertTypeID = Convert.ToInt64(CboAlertType.SelectedValue),
                    OperationType = this.CboOperationType.Text
                };
                objFrmAlertRoleSetting.ShowDialog();
                objFrmAlertRoleSetting = null;
                this.DisplayAlertInformation();
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on form Open FrmAlertRoleSetting:btnRoleSetting_Click" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnRoleSetting_Click" + Ex.Message.ToString());
            }
        }


        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmAlertSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (MblnChangeStatus)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        mObjNotification = null;
                        this.MobjclsBLLAlertSetting = null;
                        e.Cancel = false;
                    }
                    else
                        e.Cancel = true;
                }
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on form Form Closing:FrmAlertSetting_FormClosing " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmAlertSetting_FormClosing " + Ex.Message.ToString());
            }
        }

        private void DgvRoles_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int iAlertRoleSettingID = 0;
            try
            {
                iAlertRoleSettingID = Convert.ToInt32(this.DgvRoles.Rows[e.RowIndex].Cells[0].Value);
                if (iAlertRoleSettingID == 0) return;

                FrmAlertRoleSettings objFrmAlertRoleSetting;
                objFrmAlertRoleSetting = new FrmAlertRoleSettings();
                objFrmAlertRoleSetting.OperationType = this.CboOperationType.Text;
                objFrmAlertRoleSetting.AlertTypeID = Convert.ToInt64(this.CboAlertType.SelectedValue);
                if (objFrmAlertRoleSetting.ViewAlertRoleSetting(iAlertRoleSettingID))
                {
                    objFrmAlertRoleSetting.ShowDialog();
                    this.DisplayAlertInformation();
                }
                else
                    objFrmAlertRoleSetting.Close();
                objFrmAlertRoleSetting = null;
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on form open add new alert role setting:DgvRoles_CellDoubleClick" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DgvRoles_CellDoubleClick" + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.DeleteValidation())
                {
                    if (this.MobjclsBLLAlertSetting.DeleteAlertSettings(this.MlngAlertSettingsID))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                        this.MobjclsBLLAlertSetting.DTOAlertSettings.intModuleID = ModuleID;
                        this.MintRecordCnt = this.MobjclsBLLAlertSetting.RecCountNavigate();
                        this.SetBindingNavigatorButtons();
                        TmAlertSettings.Enabled = true;
                        this.AddNewAlertSettings();
                    }
                }
            }
            catch (Exception ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on BindingNavigatorDeleteItem_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorDeleteItem_Click" + ex.Message.ToString());
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.MblnChangeStatus && this.SaveAlertSettingInfo())
                {
                    this.MblnChangeStatus = false;
                    this.MblnAddStatus = false;
                }
                this.Close();
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on BtnOk_Click: " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on BtnOk_Click " + Ex.Message.ToString());
            }
        }

        private void TxtAlertName_TextChanged(object sender, EventArgs e)
        {
            this.Changestatus(true);
        }

        private void FrmAlertSetting_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {                    
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.D:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        BtnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        BtnPrint_Click(sender, new EventArgs());//Cancel
                        break;
                    case Keys.Control | Keys.M:
                        BtnEmail_Click(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception)
            {
            }
        }
        
        private void LoadMessage()
        {
            MaMessageArr = mObjNotification.FillMessageArray((int)FormID.AlertSetting, ClsCommonSettings.ProductID);
            MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.AlertSetting, (int)ClsCommonSettings.ProductID);
        }

        private bool LoadCombos(int Type)
        {
            try
            {
                if (Type == 0 || Type == 1)
                {
                    DataTable datCombos = new DataTable();
                    datCombos = this.MobjclsBLLAlertSetting.FillCombos(new string[] { "AlertTypeID,AlertType", "AlertTypeReference", "AlertTypeID = 1 " });
                    CboAlertType.ValueMember = "AlertTypeID";
                    CboAlertType.DisplayMember = "AlertType";
                    CboAlertType.DataSource = datCombos;
                }
                if (Type == 0 || Type == 2)
                {
                    DataTable datCombos = new DataTable();
                    datCombos = this.MobjclsBLLAlertSetting.FillCombos(new string[] { "OperationTypeID,OperationType", "OperationTypeReference", "" });
                    if (datCombos != null && datCombos.Rows.Count > 0)
                    {
                        string strFilter = " OperationTypeID = " + Convert.ToInt32(OperationType.CreditNote).ToString() +
                                           " OR OperationTypeID = " + Convert.ToInt32(OperationType.DebitNote).ToString() +
                                           " OR OperationTypeID = " + Convert.ToInt32(OperationType.GRN).ToString() +
                                            " OR OperationTypeID = " + Convert.ToInt32(OperationType.POS).ToString() +
                                           " OR OperationTypeID = " + Convert.ToInt32(OperationType.PurchaseInvoice).ToString();
                                           
                        DataRow[] dtFindRows = datCombos.Select(strFilter);
                        foreach (DataRow dtRow in dtFindRows)
                        {
                            datCombos.Rows.Remove(dtRow);
                        }
                        datCombos.AcceptChanges();
                    }
                    CboOperationType.ValueMember = "OperationTypeID";
                    CboOperationType.DisplayMember = "OperationType";
                    CboOperationType.DataSource = datCombos;
                }
                this.MobjClsLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private bool SaveAlertSettingInfo()
        {
            try
            {
                if (!AlertSettingsValidation())
                    return false;

                if (MblnAddStatus)
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);
                else
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;

                FillParameters();

                if (lnglstAlertRoleSettingsID.Count > 0)//Deleteing data from   rows deleted from grid
                    this.MobjclsBLLAlertSetting.DeleteAlertRolesInGrid(lnglstAlertRoleSettingsID);

                if (this.MobjclsBLLAlertSetting.SaveAlertSettings())
                {
                    this.MobjClsLogWriter.WriteLog("Saved successfully:SaveAlertSettingInfo()  " + this.Name + "", 0);
                    return true;
                }
                return false;
            }
            catch (Exception Ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on Save:SaveAlertSettingInfo() " + this.Name + " " + Ex.Message.ToString(), 3);
                if (MbShowErrorMess)
                    MessageBox.Show("Error on SaveAlertSettingInfo() " + Ex.Message.ToString());
                return false;
            }
        }
        private void FillParameters()
        {
            try
            {
                this.MobjclsBLLAlertSetting.DTOAlertSettings.lngAlertSettingID= MlngAlertSettingsID;
                this.MobjclsBLLAlertSetting.DTOAlertSettings.intAlertTypeID = Convert.ToInt32(this.CboAlertType.SelectedValue);
                this.MobjclsBLLAlertSetting.DTOAlertSettings.intOperationTypeID = Convert.ToInt32(this.CboOperationType.SelectedValue);
                this.MobjclsBLLAlertSetting.DTOAlertSettings.strDescription = this.TxtAlertName.Text;
            }
            catch (Exception ex) { throw ex; }
        }

        private bool AlertSettingsValidation()
        {
            try
            {
                if (this.TxtAlertName.Text.Trim().Length == 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8600, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErrAlertSettings.SetError(TxtAlertName, MstrMessageCommon.Replace("#", "").Trim());
                    lblAlertSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmAlertSettings.Enabled = true;
                    TxtAlertName.Focus();
                    return false;
                }
                if (CboAlertType.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8601, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErrAlertSettings.SetError(CboAlertType, MstrMessageCommon.Replace("#", "").Trim());
                    lblAlertSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmAlertSettings.Enabled = true;
                    CboAlertType.Focus();
                    return false;
                }

                if (CboOperationType.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8602, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErrAlertSettings.SetError(CboOperationType, MstrMessageCommon.Replace("#", "").Trim());
                    lblAlertSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmAlertSettings.Enabled = true;
                    CboOperationType.Focus();
                    return false;
                }
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        private bool AddNewAlertSettings()
        {
            try
            {
                MintCurrentRecCnt = 0;
                this.SetBindingNavigatorButtons();
                BindingNavigatornPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
                BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt + 1) + "";

                this.MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3258, out MmessageIcon);
                this.lblAlertSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmAlertSettings.Enabled = true;
                ClearAllControls();
                this.ErrAlertSettings.Clear();
                this.btnRoleSetting.Enabled = false;
                TxtAlertName.Focus();

                this.Changestatus(false);
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        private void ClearAllControls()
        {
            this.MlngAlertSettingsID = 0;
            this.TxtAlertName.Clear();
            this.CboAlertType.SelectedIndex = 0;
            this.CboOperationType.SelectedIndex = 0;
            this.DgvRoles.Rows.Clear();            
        }

        private void Changestatus(bool blnStatus)
        {
            this.MblnChangeStatus = blnStatus;
            BtnOk.Enabled = MblnChangeStatus;
            BtnSave.Enabled = MblnChangeStatus;
            BtnPrint.Enabled = MblnChangeStatus;
            BtnEmail.Enabled = MblnChangeStatus;
            BindingNavigatorSaveItem.Enabled = MblnChangeStatus;
            BindingNavigatorDeleteItem.Enabled = false; //MblnChangeStatus;
            BindingNavigatorAddNewItem.Enabled = false;// MblnChangeStatus;
            ErrAlertSettings.Clear();
        }
                
        public void DisplayAlertInformation()
        {
            DataTable datRoleSetting = null;

            try
            {
                this.ClearAllControls();
                int rowno = MintCurrentRecCnt;

                this.MobjclsBLLAlertSetting.DTOAlertSettings.intModuleID = ModuleID;

                if (this.MobjclsBLLAlertSetting.DisplayAlertSetting(rowno))
                {
                    this.MlngAlertSettingsID = this.MobjclsBLLAlertSetting.DTOAlertSettings.lngAlertSettingID;
                    datRoleSetting = this.MobjclsBLLAlertSetting.GetAlertRoleSetting(this.MlngAlertSettingsID);
                    this.TxtAlertName.Text = this.MobjclsBLLAlertSetting.DTOAlertSettings.strDescription;
                    this.CboAlertType.SelectedValue = this.MobjclsBLLAlertSetting.DTOAlertSettings.intAlertTypeID;
                    this.CboOperationType.SelectedValue = this.MobjclsBLLAlertSetting.DTOAlertSettings.intOperationTypeID;
                }
                if (datRoleSetting != null && datRoleSetting.Rows.Count > 0)
                {
                    foreach (DataRow dtRow in datRoleSetting.Rows)
                    {
                        int intRow = this.DgvRoles.Rows.Add();
                        this.DgvRoles.Rows[intRow].Cells["colAlertRoleSettingID"].Value = dtRow["AlertRoleSettingID"];
                        this.DgvRoles.Rows[intRow].Cells["colRole"].Tag = dtRow["RoleID"];
                        this.DgvRoles.Rows[intRow].Cells["colRole"].Value = dtRow["RoleName"];
                        this.DgvRoles.Rows[intRow].Cells["colProcessingInterval"].Value = dtRow["ProcessingInterval"];
                        this.DgvRoles.Rows[intRow].Cells["colIsRepeat"].Value = dtRow["IsRepeat"];
                        this.DgvRoles.Rows[intRow].Cells["colRepeatInterval"].Value = dtRow["RepeatInterval"];
                        this.DgvRoles.Rows[intRow].Cells["colValidPeriod"].Value = dtRow["ValidPeriod"];
                        this.DgvRoles.Rows[intRow].Cells["colIsAlertON"].Value = dtRow["IsAlertON"];
                        this.DgvRoles.Rows[intRow].Cells["colIsEmailAlertON"].Value = dtRow["IsEmailAlertON"];
                    }
                    this.DgvRoles.Update();
                }
                this.MblnChangeStatus = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SetBindingNavigatorButtons()
        {
            BindingNavigatornPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
            BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";

            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveFirstItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatornPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
            }
            if (!String.IsNullOrEmpty(MstrMessageCommon))
                lblAlertSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            BtnPrint.Enabled = true;
            BtnSave.Enabled = false;
            BtnOk.Enabled = false;
            BindingNavigatorSaveItem.Enabled = false;
            this.btnRoleSetting.Enabled = true;
        }

        private bool DeleteValidation()
        {
            try
            {
                if (this.MobjclsBLLAlertSetting.IsAlertRoleSettingExists(this.MlngAlertSettingsID))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8606, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    this.lblAlertSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    this.TmAlertSettings.Enabled = true;
                    return false;
                }
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                this.MobjClsLogWriter.WriteLog("Error on DeleteValidation() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteValidation()" + ex.Message.ToString());
                return false;
            }
        }       

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            this.MlngAlertSettingsID = this.MobjclsBLLAlertSetting.DTOAlertSettings.lngAlertSettingID;
            if (this.MlngAlertSettingsID > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = this.MlngAlertSettingsID;
                ObjViewer.PiFormID = (int)FormID.AlertSetting;
                ObjViewer.ShowDialog();
            }
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
            {
                ObjEmailPopUp.MsSubject = "Alert Settings Information";
                ObjEmailPopUp.EmailFormType = EmailFormID.AlertSettings;
                ObjEmailPopUp.EmailSource = MobjclsBLLAlertSetting.DisplayAlertSettingsEmail();
                ObjEmailPopUp.ShowDialog();
            }
        }

        private void DgvRoles_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.Delete:

                        if (DgvRoles.RowCount > 0 && DgvRoles.CurrentRow!=null)
                        {
                            this.Changestatus(true);
                            if (DgvRoles.CurrentRow.Cells["colAlertRoleSettingID"].Value != null && Convert.ToString(DgvRoles.CurrentRow.Cells["colAlertRoleSettingID"].Value) != "")
                                lnglstAlertRoleSettingsID.Add(Convert.ToInt64(DgvRoles.CurrentRow.Cells["colAlertRoleSettingID"].Value));
                        }                        
                        break;
                    default:
                        break;                    
                }
            }
            catch (Exception)
            { }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Alertsettings";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void DgvRoles_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjBLLCommonUtility.SetSerialNo(DgvRoles , e.RowIndex, true);
        }

        private void DgvRoles_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjBLLCommonUtility.SetSerialNo(DgvRoles , e.RowIndex, true);
        }

        private void CboAlertType_SelectedIndexChanged(object sender, EventArgs e)
        {
            colIsRepeat.Visible = colRepeatInterval.Visible = false;
            if (CboAlertType.SelectedIndex >= 0)
            {
                if (CboAlertType.SelectedValue.ToInt32() != (int)AlertTypes.Alert)
                    colIsRepeat.Visible = colRepeatInterval.Visible = true;                    
            }
        }
    }
}