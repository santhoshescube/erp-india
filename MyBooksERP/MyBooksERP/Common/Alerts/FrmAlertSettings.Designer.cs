﻿namespace MyBooksERP
{
    partial class FrmAlertSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAlertSettings));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ErrAlertSettings = new System.Windows.Forms.ErrorProvider(this.components);
            this.LblAlertName = new System.Windows.Forms.Label();
            this.lblAlertType = new System.Windows.Forms.Label();
            this.PnlAlertSetting = new System.Windows.Forms.Panel();
            this.btnRoleSetting = new DevComponents.DotNetBar.ButtonX();
            this.CboOperationType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboAlertType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.TxtAlertName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.DgvRoles = new System.Windows.Forms.DataGridView();
            this.TipAlertSettings = new System.Windows.Forms.ToolTip(this.components);
            this.BtnSave = new DevComponents.DotNetBar.ButtonX();
            this.BtnCancel = new DevComponents.DotNetBar.ButtonX();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.LblEmployeeStatus = new DevComponents.DotNetBar.LabelItem();
            this.lblAlertSettingsstatus = new DevComponents.DotNetBar.LabelItem();
            this.BnAlertSetting = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BnSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatornPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BnSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BnSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BtnClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.TmAlertSettings = new System.Windows.Forms.Timer(this.components);
            this.BtnOk = new DevComponents.DotNetBar.ButtonX();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAlertRoleSettingID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRole = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProcessingInterval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsRepeat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRepeatInterval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColValidPeriod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsAlertON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsEmailAlertON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ErrAlertSettings)).BeginInit();
            this.PnlAlertSetting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvRoles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BnAlertSetting)).BeginInit();
            this.BnAlertSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // ErrAlertSettings
            // 
            this.ErrAlertSettings.ContainerControl = this;
            this.ErrAlertSettings.RightToLeft = true;
            // 
            // LblAlertName
            // 
            this.LblAlertName.AutoSize = true;
            this.LblAlertName.Location = new System.Drawing.Point(16, 38);
            this.LblAlertName.Name = "LblAlertName";
            this.LblAlertName.Size = new System.Drawing.Size(35, 13);
            this.LblAlertName.TabIndex = 2;
            this.LblAlertName.Text = "Name";
            // 
            // lblAlertType
            // 
            this.lblAlertType.AutoSize = true;
            this.lblAlertType.Location = new System.Drawing.Point(16, 64);
            this.lblAlertType.Name = "lblAlertType";
            this.lblAlertType.Size = new System.Drawing.Size(31, 13);
            this.lblAlertType.TabIndex = 4;
            this.lblAlertType.Text = "Type";
            // 
            // PnlAlertSetting
            // 
            this.PnlAlertSetting.Controls.Add(this.btnRoleSetting);
            this.PnlAlertSetting.Controls.Add(this.CboOperationType);
            this.PnlAlertSetting.Controls.Add(this.CboAlertType);
            this.PnlAlertSetting.Controls.Add(this.TxtAlertName);
            this.PnlAlertSetting.Controls.Add(this.label4);
            this.PnlAlertSetting.Controls.Add(this.label3);
            this.PnlAlertSetting.Controls.Add(this.label1);
            this.PnlAlertSetting.Controls.Add(this.lblAlertType);
            this.PnlAlertSetting.Controls.Add(this.LblAlertName);
            this.PnlAlertSetting.Controls.Add(this.shapeContainer1);
            this.PnlAlertSetting.Controls.Add(this.DgvRoles);
            this.PnlAlertSetting.Location = new System.Drawing.Point(0, 27);
            this.PnlAlertSetting.Name = "PnlAlertSetting";
            this.PnlAlertSetting.Size = new System.Drawing.Size(525, 257);
            this.PnlAlertSetting.TabIndex = 2;
            // 
            // btnRoleSetting
            // 
            this.btnRoleSetting.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRoleSetting.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnRoleSetting.Location = new System.Drawing.Point(432, 59);
            this.btnRoleSetting.Name = "btnRoleSetting";
            this.btnRoleSetting.Size = new System.Drawing.Size(83, 23);
            this.btnRoleSetting.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnRoleSetting.TabIndex = 3;
            this.btnRoleSetting.Text = "&Role Settings";
            this.TipAlertSettings.SetToolTip(this.btnRoleSetting, "Click here to add role settings to this alert.");
            this.btnRoleSetting.Click += new System.EventHandler(this.btnRoleSetting_Click);
            // 
            // CboOperationType
            // 
            this.CboOperationType.DisplayMember = "Text";
            this.CboOperationType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboOperationType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboOperationType.Enabled = false;
            this.CboOperationType.FormattingEnabled = true;
            this.CboOperationType.ItemHeight = 14;
            this.CboOperationType.Location = new System.Drawing.Point(258, 62);
            this.CboOperationType.Name = "CboOperationType";
            this.CboOperationType.Size = new System.Drawing.Size(168, 20);
            this.CboOperationType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboOperationType.TabIndex = 2;
            this.CboOperationType.SelectionChangeCommitted += new System.EventHandler(this.TxtAlertName_TextChanged);
            // 
            // CboAlertType
            // 
            this.CboAlertType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboAlertType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboAlertType.DisplayMember = "Text";
            this.CboAlertType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboAlertType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboAlertType.FormattingEnabled = true;
            this.CboAlertType.ItemHeight = 14;
            this.CboAlertType.Location = new System.Drawing.Point(57, 62);
            this.CboAlertType.Name = "CboAlertType";
            this.CboAlertType.Size = new System.Drawing.Size(100, 20);
            this.CboAlertType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboAlertType.TabIndex = 1;
            this.CboAlertType.SelectionChangeCommitted += new System.EventHandler(this.TxtAlertName_TextChanged);
            this.CboAlertType.SelectedIndexChanged += new System.EventHandler(this.CboAlertType_SelectedIndexChanged);
            this.CboAlertType.TextChanged += new System.EventHandler(this.TxtAlertName_TextChanged);
            // 
            // TxtAlertName
            // 
            this.TxtAlertName.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.TxtAlertName.Border.Class = "TextBoxBorder";
            this.TxtAlertName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtAlertName.Location = new System.Drawing.Point(57, 36);
            this.TxtAlertName.Name = "TxtAlertName";
            this.TxtAlertName.ReadOnly = true;
            this.TxtAlertName.Size = new System.Drawing.Size(369, 20);
            this.TxtAlertName.TabIndex = 0;
            this.TxtAlertName.TextChanged += new System.EventHandler(this.TxtAlertName_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Role Settings";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Configure Alert Settings";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(172, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Operation Type";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(525, 257);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 99;
            this.lineShape2.X2 = 515;
            this.lineShape2.Y1 = 96;
            this.lineShape2.Y2 = 96;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 148;
            this.lineShape1.X2 = 514;
            this.lineShape1.Y1 = 19;
            this.lineShape1.Y2 = 19;
            // 
            // DgvRoles
            // 
            this.DgvRoles.AllowUserToAddRows = false;
            this.DgvRoles.AllowUserToResizeColumns = false;
            this.DgvRoles.AllowUserToResizeRows = false;
            this.DgvRoles.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvRoles.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DgvRoles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colAlertRoleSettingID,
            this.colRole,
            this.colProcessingInterval,
            this.colIsRepeat,
            this.colRepeatInterval,
            this.ColValidPeriod,
            this.colIsAlertON,
            this.colIsEmailAlertON});
            this.DgvRoles.Location = new System.Drawing.Point(15, 111);
            this.DgvRoles.Name = "DgvRoles";
            this.DgvRoles.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DgvRoles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvRoles.Size = new System.Drawing.Size(500, 143);
            this.DgvRoles.TabIndex = 4;
            this.DgvRoles.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvRoles_CellDoubleClick);
            this.DgvRoles.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DgvRoles_RowsAdded);
            this.DgvRoles.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DgvRoles_KeyDown);
            this.DgvRoles.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.DgvRoles_RowsRemoved);
            // 
            // BtnSave
            // 
            this.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnSave.Location = new System.Drawing.Point(19, 290);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnSave.TabIndex = 5;
            this.BtnSave.Text = "&Save";
            this.TipAlertSettings.SetToolTip(this.BtnSave, "Click here to save the Executive information");
            this.BtnSave.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnCancel.Location = new System.Drawing.Point(441, 290);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnCancel.TabIndex = 7;
            this.BtnCancel.Text = "&Cancel";
            this.TipAlertSettings.SetToolTip(this.BtnCancel, "Click here to close the form");
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.LblEmployeeStatus,
            this.lblAlertSettingsstatus});
            this.bar1.Location = new System.Drawing.Point(0, 322);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(530, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 79;
            this.bar1.TabStop = false;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "Status:";
            // 
            // LblEmployeeStatus
            // 
            this.LblEmployeeStatus.Name = "LblEmployeeStatus";
            // 
            // lblAlertSettingsstatus
            // 
            this.lblAlertSettingsstatus.Name = "lblAlertSettingsstatus";
            this.lblAlertSettingsstatus.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // BnAlertSetting
            // 
            this.BnAlertSetting.AddNewItem = null;
            this.BnAlertSetting.BackColor = System.Drawing.Color.Transparent;
            this.BnAlertSetting.CountItem = this.BindingNavigatorCountItem;
            this.BnAlertSetting.DeleteItem = null;
            this.BnAlertSetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BnSeparator,
            this.BindingNavigatornPositionItem,
            this.BindingNavigatorCountItem,
            this.BnSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BnSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BtnClear,
            this.toolStripSeparator2,
            this.BtnPrint,
            this.BtnEmail,
            this.BtnHelp});
            this.BnAlertSetting.Location = new System.Drawing.Point(0, 0);
            this.BnAlertSetting.MoveFirstItem = null;
            this.BnAlertSetting.MoveLastItem = null;
            this.BnAlertSetting.MoveNextItem = null;
            this.BnAlertSetting.MovePreviousItem = null;
            this.BnAlertSetting.Name = "BnAlertSetting";
            this.BnAlertSetting.PositionItem = this.BindingNavigatornPositionItem;
            this.BnAlertSetting.Size = new System.Drawing.Size(530, 25);
            this.BnAlertSetting.TabIndex = 0;
            this.BnAlertSetting.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BnSeparator
            // 
            this.BnSeparator.Name = "BnSeparator";
            this.BnSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatornPositionItem
            // 
            this.BindingNavigatornPositionItem.AccessibleName = "Position";
            this.BindingNavigatornPositionItem.AutoSize = false;
            this.BindingNavigatornPositionItem.Name = "BindingNavigatornPositionItem";
            this.BindingNavigatornPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatornPositionItem.Text = "0";
            this.BindingNavigatornPositionItem.ToolTipText = "Current position";
            // 
            // BnSeparator1
            // 
            this.BnSeparator1.Name = "BnSeparator1";
            this.BnSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BnSeparator2
            // 
            this.BnSeparator2.Name = "BnSeparator2";
            this.BnSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.Enabled = false;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New";
            this.BindingNavigatorAddNewItem.Visible = false;
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.ToolTipText = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.Enabled = false;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.ToolTipText = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BtnClear
            // 
            this.BtnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnClear.Enabled = false;
            this.BtnClear.Image = ((System.Drawing.Image)(resources.GetObject("BtnClear.Image")));
            this.BtnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnClear.Name = "BtnClear";
            this.BtnClear.Size = new System.Drawing.Size(23, 22);
            this.BtnClear.ToolTipText = "Clear";
            this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.ToolTipText = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.ToolTipText = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.ToolTipText = "Help";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnOk.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnOk.Location = new System.Drawing.Point(360, 290);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnOk.TabIndex = 2;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "AlertroleID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Role";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn3.HeaderText = "Processing Interval";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn4.HeaderText = "Is Repeat";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn5.HeaderText = "Repeat Interval";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn6.HeaderText = "Alert ON";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colAlertRoleSettingID
            // 
            this.colAlertRoleSettingID.HeaderText = "AlertRoleSettingID";
            this.colAlertRoleSettingID.Name = "colAlertRoleSettingID";
            this.colAlertRoleSettingID.ReadOnly = true;
            this.colAlertRoleSettingID.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colAlertRoleSettingID.Visible = false;
            // 
            // colRole
            // 
            this.colRole.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colRole.HeaderText = "Role";
            this.colRole.Name = "colRole";
            this.colRole.ReadOnly = true;
            this.colRole.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colProcessingInterval
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colProcessingInterval.DefaultCellStyle = dataGridViewCellStyle2;
            this.colProcessingInterval.HeaderText = "Processing Interval";
            this.colProcessingInterval.Name = "colProcessingInterval";
            this.colProcessingInterval.ReadOnly = true;
            this.colProcessingInterval.Width = 110;
            // 
            // colIsRepeat
            // 
            this.colIsRepeat.HeaderText = "Is Repeat";
            this.colIsRepeat.Name = "colIsRepeat";
            this.colIsRepeat.ReadOnly = true;
            this.colIsRepeat.Visible = false;
            this.colIsRepeat.Width = 70;
            // 
            // colRepeatInterval
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colRepeatInterval.DefaultCellStyle = dataGridViewCellStyle3;
            this.colRepeatInterval.HeaderText = "Repeat Interval";
            this.colRepeatInterval.Name = "colRepeatInterval";
            this.colRepeatInterval.ReadOnly = true;
            this.colRepeatInterval.Visible = false;
            this.colRepeatInterval.Width = 70;
            // 
            // ColValidPeriod
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ColValidPeriod.DefaultCellStyle = dataGridViewCellStyle4;
            this.ColValidPeriod.HeaderText = "Valid Period";
            this.ColValidPeriod.Name = "ColValidPeriod";
            this.ColValidPeriod.ReadOnly = true;
            this.ColValidPeriod.Width = 70;
            // 
            // colIsAlertON
            // 
            this.colIsAlertON.HeaderText = "Alert ON";
            this.colIsAlertON.Name = "colIsAlertON";
            this.colIsAlertON.ReadOnly = true;
            this.colIsAlertON.Width = 60;
            // 
            // colIsEmailAlertON
            // 
            this.colIsEmailAlertON.HeaderText = "Email Alert ON";
            this.colIsEmailAlertON.Name = "colIsEmailAlertON";
            this.colIsEmailAlertON.ReadOnly = true;
            this.colIsEmailAlertON.Width = 90;
            // 
            // FrmAlertSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 341);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BnAlertSetting);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.PnlAlertSetting);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAlertSettings";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alert Settings";
            this.Load += new System.EventHandler(this.FrmAlertSetting_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAlertSetting_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmAlertSetting_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.ErrAlertSettings)).EndInit();
            this.PnlAlertSetting.ResumeLayout(false);
            this.PnlAlertSetting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvRoles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BnAlertSetting)).EndInit();
            this.BnAlertSetting.ResumeLayout(false);
            this.BnAlertSetting.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ErrorProvider ErrAlertSettings;
        private System.Windows.Forms.Panel PnlAlertSetting;
        private System.Windows.Forms.Label lblAlertType;
        private System.Windows.Forms.Label LblAlertName;
        private System.Windows.Forms.ToolTip TipAlertSettings;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.LabelItem LblEmployeeStatus;
        internal System.Windows.Forms.BindingNavigator BnAlertSetting;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BnSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatornPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BnSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BnSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BtnClear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DgvRoles;
        private System.Windows.Forms.Label label3;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Label label4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DevComponents.DotNetBar.LabelItem lblAlertSettingsstatus;
        private System.Windows.Forms.Timer TmAlertSettings;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtAlertName;
        private DevComponents.DotNetBar.ButtonX btnRoleSetting;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboOperationType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboAlertType;
        private DevComponents.DotNetBar.ButtonX BtnCancel;
        private DevComponents.DotNetBar.ButtonX BtnSave;
        private DevComponents.DotNetBar.ButtonX BtnOk;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAlertRoleSettingID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRole;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProcessingInterval;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIsRepeat;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRepeatInterval;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColValidPeriod;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIsAlertON;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIsEmailAlertON;
    }
}