﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,17 Feb 2011>
Description:	<Description,, BankDetails Form>
================================================
*/
namespace MyBooksERP
{
    public partial class FrmBankDetails : DevComponents.DotNetBar.Office2007Form
    {
        #region Variables Declaration

        private bool MblnAddStatus; //To Specify Add/Update mode 
        private bool MblnChangeStatus = false; //To Specify Change Status
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        //private bool MblnAddUpdatePermission = false;//To Set Add/Update Permission
        private bool MblnPrintEmailPermission = false;
        private string MsMessageCaption;
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;

        private int MintCurrentRecCnt;
        private int MintRecordCnt;
        private int MintTimerInterval;
        private int MintBankNameID;
        private int MintBankBranchID;
        private int MintCompanyId;
        private int MintUserId;
        private int MintBankID;
       
        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;

        bool blnIsEditMode = false;

        // ArrayList sarrFormnames; ////showing webbrowser in toolstripsplitbtn
        WebBrowser MobjWebBrowser;
        ToolStripControlHost MobjToolStripControlHost;

        clsBLLBankDetails MobjclsBLLBankDetails;
        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;
        #endregion //Variables Declaration

        #region Constructor
        public FrmBankDetails(int intBankID)
        {

            InitializeComponent();

            MintCompanyId = ClsCommonSettings.CompanyID;         // current companyid
            MintUserId = ClsCommonSettings.UserID;
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MmessageIcon = MessageBoxIcon.Information;
            //MintBankID = intBankID;

            MsMessageCaption = ClsCommonSettings.MessageCaption;    //Message caption
            TmBank.Interval = MintTimerInterval;   // Setting timer interval

            MobjclsBLLBankDetails = new clsBLLBankDetails();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification();

            ////showing webbrowser in toolstripsplitbtn
            MobjWebBrowser = new WebBrowser();
            MobjWebBrowser.ScrollBarsEnabled = false;
            MobjWebBrowser.Name = "UsedLevel";
            MobjToolStripControlHost = new ToolStripControlHost(MobjWebBrowser);
            WebBrowserBankbtn.DropDownItems.Add(MobjToolStripControlHost);

        #endregion //Constructor

        }

        private void FrmBankDetails_Load(object sender, EventArgs e)
        {
            try
            {
                MblnChangeStatus = false;
                SetPermissions();
                LoadCombos(0);
              //  FillAccountHeads();
                LoadMessage();
                ChangeStatus();
                AddNewItem();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form Load:FrmBankDetails_Load " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmBankDetails_Load() " + Ex.Message.ToString());
            }

        }

        private void ChangeStatus()
        {
            ErrProBank.Clear();
            MblnChangeStatus = true;
            if (!blnIsEditMode)
            {
                btnOk.Enabled = MblnAddPermission;
                btnSave.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;

            }
            else
            {
                btnOk.Enabled = MblnUpdatePermission;
                btnSave.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
        }

        public void ChangeStatus(object sender, EventArgs e) // to set the event
        {
            ChangeStatus();
        }

        private void Cbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }


        public bool AddNewItem()
        {
            try
            {
                MblnAddStatus = true;
                TmBank.Enabled = true;
                SetEnable();
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 655, out MmessageIcon);
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                if (MintBankID > 0)
                {
                    MintRecordCnt = MobjclsBLLBankDetails.RecCountBankNavigate(MintBankID);
                }
                else
                {
                    MintRecordCnt = MobjclsBLLBankDetails.RecCountNavigate();
                }
                BindingAddNew();

                EnableDisableButtons(true);

                BindingNavigatorAddNewItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                btnPrint.Enabled = false;
                btnEmail.Enabled = false;
                WebBrowserBankbtn.Enabled = false;

                btnOk.Enabled = false;
                btnSave.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;


                CboBankBranchID.Focus();

                blnIsEditMode = false;
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form AddNewItem:AddNewItem " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewItem() " + Ex.Message.ToString());

                return false;
            }
            
        }

        private void BindingAddNew()
        {
          
            BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt + 1) + "";
            BindingNavigatorPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
            MintCurrentRecCnt = MintRecordCnt + 1;
            if (MintRecordCnt >= 1)
            {
                BindingNavigatorMoveFirstItem.Enabled = true;
                BindingNavigatorMovePreviousItem.Enabled = true;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
        }

        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
            BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";

            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveFirstItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
            }

            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            btnPrint.Enabled = MblnPrintEmailPermission;
            btnEmail.Enabled = MblnPrintEmailPermission;
            WebBrowserBankbtn.Enabled = true;
            btnSave.Enabled = false;
            btnOk.Enabled = false;
            BindingNavigatorSaveItem.Enabled = false;

        }

        /// <summary>
        /// Enable/disable buttons according to the action
        /// </summary>
        private void EnableDisableButtons(bool bEnable)
        {
           
            if (bEnable)
            {
                
                MblnChangeStatus = !bEnable;
            }
            else
            {
                
                MblnChangeStatus = bEnable;
            }
        }


        private bool LoadCombos(int intType)
        {
            // 0 - For Loading All Combo
            // 1 - For Loading CboBankBranchID 
            // 2 - For Loading CboCountry 
            // 3 - For Loading CboAccountID 
            try
            {
                DataTable datCombos = new DataTable();

                if (intType == 0 || intType == 1)
                {
                    datCombos = MobjclsBLLBankDetails.FillCombos(new string[] { "BankID,BankName", "BankReference", "" });
                    CboBankBranchID.ValueMember = "BankID";
                    CboBankBranchID.DisplayMember = "BankName";
                    CboBankBranchID.DataSource = datCombos;
                }
                if (intType == 0 || intType == 2)
                {
                    datCombos = null;
                    datCombos = MobjclsBLLBankDetails.FillCombos(new string[] { "CountryID,CountryName", "CountryReference", "" });
                    CboCountry.ValueMember = "CountryID";
                    CboCountry.DisplayMember = "CountryName";
                    CboCountry.DataSource = datCombos;
                }
                //if (intType == 0 || intType == 3)
                //{
                //    datCombos = null;
                //    datCombos = MobjclsBLLBankDetails.FillCombos(new string[] { "AccountID,Description", "AccountMaster", "GLAccountID=33" });
                //    CboAccountID.ValueMember = "AccountID";
                //    CboAccountID.DisplayMember = "Description";
                //    CboAccountID.DataSource = datCombos;
                //}

                //AccountIDComboBox, "PayGetAccounts", parameters, "AccountID", "Account"

                MObjClsLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }


        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.BankInformation, ClsCommonSettings.ProductID);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.BankInformation, ClsCommonSettings.ProductID);
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.General, (Int32)eMenuID.Bank, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void SetEnable()
        {
            CboBankBranchID.Text = "";
            CboBankBranchID.SelectedIndex = -1;
            txtDescription.Text = "";
            txtBankCode.Text = "";
            txtAddress.Text = "";
            txtTelephone.Text = "";
            txtFax.Text = "";
            CboCountry.Text = "";
            CboCountry.SelectedIndex = -1;
            txtEmail.Text = "";
            txtWebsite.Text = "";
           // CboAccountID.SelectedIndex = -1;
            txtUAEBankCode.Text = "";
        }



        private void btnBankBranch_Click(object sender, EventArgs e)
        {
            try
            {
                MintBankBranchID = Convert.ToInt32(CboBankBranchID.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("BankName", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "BankID,BankName As BankName", "BankReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(CboBankBranchID.SelectedValue);
                LoadCombos(1);
                if (objCommon.NewID != 0)
                    CboBankBranchID.SelectedValue = objCommon.NewID;
                else
                    CboBankBranchID.SelectedValue = intWorkingAt;  
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnBankBranch " + Ex.Message.ToString());
            }

        }

        private void btnCountry_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("CountryName", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "CountryID,CountryName As CountryName", "CountryReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(CboCountry.SelectedValue);
                LoadCombos(2);
                if (objCommon.NewID != 0)
                    CboCountry.SelectedValue = objCommon.NewID;
                else
                    CboCountry.SelectedValue = intWorkingAt; 
               
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnCountry " + Ex.Message.ToString());
            }

        }

        // To check Validtion
        private bool BankDetailsFormValidation()
        {
            lblBankStatus.Text = "";
            if (Convert.ToInt32(CboBankBranchID.SelectedValue) == 0)
            {

                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 651, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrProBank.SetError(CboBankBranchID, MsMessageCommon.Replace("#", "").Trim());
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmBank.Enabled = true;
                CboBankBranchID.Focus();
                return false;
            }
            if (txtDescription.Text.Trim() == "")
            {

                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 652, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrProBank.SetError(txtDescription, MsMessageCommon.Replace("#", "").Trim());
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmBank.Enabled = true;
                txtDescription.Focus();
                return false;
            }


            if (txtEmail.Text.Trim().Length > 0 && !MobjclsBLLBankDetails.CheckValidEmail(txtEmail.Text.Trim()))
            {

                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 653, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrProBank.SetError(txtEmail, MsMessageCommon.Replace("#", "").Trim());
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmBank.Enabled = true;
                txtEmail.Focus();
                return false;
            }


            //if (Convert.ToInt32(CboAccountID.SelectedValue) == 0)
            //{
            //    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 654, out MmessageIcon);
            //    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            //    ErrProBank.SetError(CboAccountID, MsMessageCommon.Replace("#", "").Trim());
            //    lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            //    TmBank.Enabled = true;
            //    CboAccountID.Focus();
            //    return false;
            //}
            return true;
        }

        private bool DeleteValidation(int intBankNameID)
        {
            DataTable dtIds = MobjclsBLLBankDetails.CheckExistReferences(intBankNameID);
            if (dtIds.Rows.Count > 0)
            {
                if (Convert.ToString(dtIds.Rows[0]["FormName"]) != "0")
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 667, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    TmBank.Enabled = true;
                    return false;
                }
            }
            return true;
        }

        #region GetidExists

        private void SStripWebBrowserBank_DropDownOpened(object sender, EventArgs e)
        {
            MobjWebBrowser.DocumentText = MobjclsBLLBankDetails.GetFormsInUse("Bank information exists in", MintBankNameID, "name <> 'BankNameReference'", "BankNameID");
        }

        # endregion GetidExists



        // Function To Save And Update

        private bool SaveBank()
        {
            try
            {

                if (MblnAddStatus == true)//insert
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);

                    MobjclsBLLBankDetails.clsDTOBankDetails.intBankNameID = 0;
                }
                else
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);

                    MobjclsBLLBankDetails.clsDTOBankDetails.intBankNameID = MintBankNameID;
                }

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;

                MobjclsBLLBankDetails.clsDTOBankDetails.intBankBranchID = Convert.ToInt32(CboBankBranchID.SelectedValue);
                MobjclsBLLBankDetails.clsDTOBankDetails.strDescription = Convert.ToString(txtDescription.Text.Replace("'", "`").Trim());
                MobjclsBLLBankDetails.clsDTOBankDetails.strBankCode = Convert.ToString(txtBankCode.Text.Replace("'", "`").Trim());
                MobjclsBLLBankDetails.clsDTOBankDetails.strAddress = Convert.ToString(txtAddress.Text.Replace("'", "`").Trim());
                MobjclsBLLBankDetails.clsDTOBankDetails.strTelephone = Convert.ToString(txtTelephone.Text.Replace("'", "`").Trim());
                MobjclsBLLBankDetails.clsDTOBankDetails.strFax = Convert.ToString(txtFax.Text.Replace("'", "`").Trim());
                MobjclsBLLBankDetails.clsDTOBankDetails.intCountryID = Convert.ToInt32(CboCountry.SelectedValue);
                MobjclsBLLBankDetails.clsDTOBankDetails.strEmail = Convert.ToString(txtEmail.Text.Replace("'", "`").Trim());
                MobjclsBLLBankDetails.clsDTOBankDetails.strWebsite = Convert.ToString(txtWebsite.Text.Replace("'", "`").Trim());
               // MobjclsBLLBankDetails.clsDTOBankDetails.intAccountID = Convert.ToInt32(CboAccountID.SelectedValue);
                //MobjclsBLLBankDetails.clsDTOBankDetails.strBankSortCode = null;
               

                if (txtUAEBankCode.Text == "")
                    MobjclsBLLBankDetails.clsDTOBankDetails.lngUAEBankCode = 0;
                else
                    MobjclsBLLBankDetails.clsDTOBankDetails.lngUAEBankCode = Convert.ToInt64(txtUAEBankCode.Text.Replace("'", "`").Trim());

                if (MobjclsBLLBankDetails.SaveBankDetails(MblnAddStatus))
                {
                    MintBankNameID = MobjclsBLLBankDetails.clsDTOBankDetails.intBankNameID;

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on Save:SaveBank() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Saving SaveBank()" + Ex.Message.ToString());
                return false;
            }
        }


        // Function To Display

        public void DisplayBank()
        {

            if (MobjclsBLLBankDetails.DisplayBankDetails(MintCurrentRecCnt))
            {
                GetBankDetails();
            }
        }
        public void DisplayBankID()
        { 
            if(MobjclsBLLBankDetails.DisplayBankIDDetails(MintCurrentRecCnt,MintBankID))
            {
                GetBankDetails();
            }
        }
        private void GetBankDetails()
        { 
             MintBankNameID = MobjclsBLLBankDetails.clsDTOBankDetails.intBankNameID;
             CboBankBranchID.SelectedValue = MobjclsBLLBankDetails.clsDTOBankDetails.intBankBranchID.ToString();
             txtDescription.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strDescription;
             txtBankCode.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strBankCode.ToString();
             txtAddress.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strAddress.ToString();
             txtTelephone.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strTelephone.ToString();
             txtFax.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strFax.ToString();
             CboCountry.SelectedValue = MobjclsBLLBankDetails.clsDTOBankDetails.intCountryID.ToString();
             txtEmail.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strEmail.ToString();
             txtWebsite.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strWebsite.ToString();
           //  CboAccountID.SelectedValue = MobjclsBLLBankDetails.clsDTOBankDetails.intAccountID.ToString();
             if (MobjclsBLLBankDetails.clsDTOBankDetails.lngUAEBankCode == 0)
                txtUAEBankCode.Text = "";
             else
                txtUAEBankCode.Text = MobjclsBLLBankDetails.clsDTOBankDetails.lngUAEBankCode.ToString();

             //DataTable dtIds = MobjclsBLLBankDetails.CheckExistReferences(MintBankNameID);
             //if (dtIds.Rows.Count > 0)
             //{
             //   if (Convert.ToString(dtIds.Rows[0]["FormName"]) != "0")
             //   {
             //       WebBrowserBankbtn.Enabled = true;

             //   }
             //   else
             //   {
             //       WebBrowserBankbtn.Enabled = false;
             //   }
             //}
             MblnChangeStatus = false;
        
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            if (BankDetailsFormValidation())
            {
                if (SaveBank())
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                    bindingNavigatorMovePreviousItem_Click(null, null);
                    if (MblnAddStatus == true)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                        lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmBank.Enabled = true;

                    }
                    else
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 666, out MmessageIcon);
                        lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmBank.Enabled = true;
                    }
                    //MblnAddStatus = false;
                    //AddNewItem();
                }
            }

        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (BankDetailsFormValidation())
            {
                if (SaveBank())
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                    bindingNavigatorMovePreviousItem_Click(null, null);
                    //TmBank.Enabled = false;
                    if (MblnAddStatus == true)
                    {

                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                        lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmBank.Enabled = true;
                    }
                    else
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 666, out MmessageIcon);
                        lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmBank.Enabled = true;
                    }

                    //AddNewItem();
                    //BindingNavigatorMoveNextItem.Enabled = false;
                    //BindingNavigatorMoveLastItem.Enabled = false;
                }
            }
        }

        private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)        
        {
            try
            {
                if (MintBankID > 0)
                    MintRecordCnt = MobjclsBLLBankDetails.RecCountBankNavigate(MintBankID);
                else
                    MintRecordCnt = MobjclsBLLBankDetails.RecCountNavigate();
                                 
                if (MintRecordCnt > 0)
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                    MblnAddStatus = false;
                    if (MintCurrentRecCnt >= MintRecordCnt)
                    {
                        MintCurrentRecCnt = MintRecordCnt;
                    }
                }
                else
                {
                    MintCurrentRecCnt = 0;
                }

                if (MintBankID > 0)
                    DisplayBankID();
                else
                    DisplayBank();
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 11, out MmessageIcon);
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                SetBindingNavigatorButtons();
                blnIsEditMode = true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MoveNextItem:MoveNextItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveNextItem_Click " + Ex.Message.ToString());
            }


        }


        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MintBankID > 0)
                    MintRecordCnt = MobjclsBLLBankDetails.RecCountBankNavigate(MintBankID);
                else
                    MintRecordCnt = MobjclsBLLBankDetails.RecCountNavigate();

                if (MintRecordCnt > 0)
                {
                    MblnAddStatus = false;
                    MintCurrentRecCnt = MintCurrentRecCnt - 1;

                    if (MintCurrentRecCnt <= 0)
                    {
                        MintCurrentRecCnt = 1;

                    }
                }
                else
                {
                    BindingNavigatorPositionItem.Text = "of 0";
                    MintRecordCnt = 0;
                }
                if (MintBankID > 0)
                    DisplayBankID();
                else
                    DisplayBank();
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 10, out MmessageIcon);
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                SetBindingNavigatorButtons();
                blnIsEditMode = true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MovePreviousItem:MovePreviousItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MovePreviousItem_Click " + Ex.Message.ToString());
            }

        }

        private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            try
            {
                ErrProBank.Clear();
                if (MintBankID > 0)
                    MintRecordCnt = MobjclsBLLBankDetails.RecCountBankNavigate(MintBankID);
                else
                    MintRecordCnt = MobjclsBLLBankDetails.RecCountNavigate();

                if (MintRecordCnt > 0)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    MblnAddStatus = false;
                }
                else
                {
                    MintCurrentRecCnt = 0;
                }

                if (MintBankID > 0)
                    DisplayBankID();
                else
                    DisplayBank();
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 12, out MmessageIcon);
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                SetBindingNavigatorButtons();
                blnIsEditMode = true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MoveLastItem:MoveLastItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveLastItem_Click() " + Ex.Message.ToString());
            }


        }

        private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            try
            {
                ErrProBank.Clear();
                if (MintBankID > 0)
                    MintRecordCnt = MobjclsBLLBankDetails.RecCountBankNavigate(MintBankID);
                else
                    MintRecordCnt = MobjclsBLLBankDetails.RecCountNavigate();

                if (MintRecordCnt > 0)
                {
                    MblnAddStatus = false;
                    MintCurrentRecCnt = 1;
                }
                else
                    MintCurrentRecCnt = 0;
                if (MintBankID > 0)
                    DisplayBankID();
                else
                    DisplayBank();
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9, out MmessageIcon);
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                SetBindingNavigatorButtons();
                blnIsEditMode = true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MoveFirstItem:MoveFirstItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveFirstItem_Click() " + Ex.Message.ToString());
            }



        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewItem();

        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                int intBankNameID = MobjclsBLLBankDetails.clsDTOBankDetails.intBankNameID;
                if (DeleteValidation(intBankNameID))
                {
                    if (MobjclsBLLBankDetails.DeleteBankDetails())
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
                        if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                            return;
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                        lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmBank.Enabled = true;
                        AddNewItem();
                    }

                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on DeleteItem:DeleteItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteItem_Click() " + Ex.Message.ToString());
            }
            
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (BankDetailsFormValidation())
            {
                if (SaveBank())
                {
                    if (MblnAddStatus == true)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                        lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmBank.Enabled = true;
                    }
                    else
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 666, out MmessageIcon);
                        lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmBank.Enabled = true;
                    }
                    MblnChangeStatus = false;
                    this.Close();
                }
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            AddNewItem();
            //BindingNavigatorDeleteItem.Enabled = false;
            //BindingNavigatorMoveLastItem.Enabled = false;
            //BindingNavigatorMoveNextItem.Enabled = false;

        }

        private void BtnCancel_Click_1(object sender, EventArgs e)
        {

            this.Close();
        }

        private void FrmBankDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MblnChangeStatus)
            {

                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void TmBank_Tick(object sender, EventArgs e)
        {
            TmBank.Enabled = false;
            lblBankStatus.Text = "";
        }

        // Function to fill Account Head
        private void FillAccountHeads()
        {
            //DataTable datAccountHeads = new DataTable();

            //datAccountHeads = MobjclsBLLBankDetails.AccountDetails();
            //CboAccountID.DataSource = datAccountHeads;
            //CboAccountID.ValueMember = "AccountID";
            //CboAccountID.DisplayMember = "Account";
        }


        private void BtnAccountHead_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    FrmChartOfAccounts objChartOfAccounts = new FrmChartOfAccounts();
            //    objChartOfAccounts.PermittedAccountGroup = AccountGroups.BankAccounts;
            //    objChartOfAccounts.ShowDialog();
            //    objChartOfAccounts.Dispose();
            //    int intCurrentAccntID = CboAccountID.SelectedValue.ToInt32();
            //    FillAccountHeads();
            //    CboAccountID.SelectedValue = intCurrentAccntID;
            //}
            //catch (Exception Ex)
            //{
            //    MObjClsLogWriter.WriteLog("Error on form ChartOfAccounts:BtnAccountHead_Click " + this.Name + " " + Ex.Message.ToString(), 2);

            //    if (ClsCommonSettings.ShowErrorMess)
            //        MessageBox.Show("Error on ChartOfAccounts() " + Ex.Message.ToString());
            //}

        }



        private void TmFocus_Tick(object sender, EventArgs e)
        {
            CboBankBranchID.Focus();
            TmFocus.Enabled = false;

        }

        private void PrintStripButton_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void LoadReport()
        {
            try
            {
                if (MintBankNameID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MintBankNameID;
                    ObjViewer.PiFormID = (int)FormID.BankInformation;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmBankDetails:LoadReport " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport() " + Ex.Message.ToString());
            }
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Bank Information";
                    ObjEmailPopUp.EmailFormType = EmailFormID.Bank;
                    ObjEmailPopUp.EmailSource = MobjclsBLLBankDetails.GetBankReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on Email:BtnEmail_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnEmail_Click " + Ex.Message.ToString());
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            //SetEnable();
            AddNewItem();
        }

        private void FrmBankDetails_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    //case Keys.Control | Keys.S:
                    //    btnSave_Click(sender,new EventArgs());//save
                    //  break;
                    //case Keys.Control | Keys.O:
                    //    btnOk_Click(sender, new EventArgs());//OK
                    //    break; 
                    //case Keys.Control | Keys.L:
                    //    btnCancel_Click(sender, new EventArgs());//Cancel
                    //    break;
                    case Keys.Control | Keys.Enter:
                        bindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        bindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        btnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        bindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        bindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        bindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        bindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        PrintStripButton_Click(sender, new EventArgs());//Cancel
                        break;
                    case Keys.Control | Keys.M:
                        BtnEmail_Click(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception)
            {
            }
        }



    }
}

