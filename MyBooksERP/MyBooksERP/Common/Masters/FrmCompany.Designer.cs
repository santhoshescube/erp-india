﻿namespace MyBooksERP
{
    partial class FrmCompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label3;
            System.Windows.Forms.Label Label9;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.Label label21;
            System.Windows.Forms.Label label25;
            System.Windows.Forms.Label label26;
            System.Windows.Forms.Label label27;
            System.Windows.Forms.Label label28;
            System.Windows.Forms.Label label29;
            System.Windows.Forms.Label label30;
            System.Windows.Forms.Label label31;
            System.Windows.Forms.Label label32;
            System.Windows.Forms.Label label33;
            System.Windows.Forms.Label label34;
            System.Windows.Forms.Label label35;
            System.Windows.Forms.Label label36;
            System.Windows.Forms.Label label37;
            System.Windows.Forms.Label label38;
            System.Windows.Forms.Label label39;
            System.Windows.Forms.Label label40;
            System.Windows.Forms.Label label41;
            System.Windows.Forms.Label label45;
            System.Windows.Forms.Label label16;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label15;
            System.Windows.Forms.Label lblDailiyPayBasedOn;
            System.Windows.Forms.Label lblWorkingDays;
            System.Windows.Forms.Label label17;
            System.Windows.Forms.Label lblWeeklyOffday;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCompany));
            this.CompanyMasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.CompanyMasterBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.CancelToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnComSettings = new System.Windows.Forms.ToolStripButton();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.BtnBank = new System.Windows.Forms.ToolStripButton();
            this.BtnFinYear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.DtpBookStartDate = new System.Windows.Forms.DateTimePicker();
            this.Label11 = new System.Windows.Forms.Label();
            this.DtpFinYearStartDate = new System.Windows.Forms.DateTimePicker();
            this.ComboBoxBankNameIDD = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ErrorProviderCompany = new System.Windows.Forms.ErrorProvider(this.components);
            this.TmrFocus = new System.Windows.Forms.Timer(this.components);
            this.TmrComapny = new System.Windows.Forms.Timer(this.components);
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.TxtPOBox = new System.Windows.Forms.TextBox();
            this.CboCompanyIndustry = new System.Windows.Forms.ComboBox();
            this.CboCurrency = new System.Windows.Forms.ComboBox();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.CboCompanyName = new System.Windows.Forms.ComboBox();
            this.CboCountry = new System.Windows.Forms.ComboBox();
            this.TxtShortName = new System.Windows.Forms.TextBox();
            this.TxtEmployerCode = new System.Windows.Forms.TextBox();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LogoFilePictureBox = new System.Windows.Forms.PictureBox();
            this.TxtCity = new System.Windows.Forms.TextBox();
            this.TxtBlock = new System.Windows.Forms.TextBox();
            this.TxtFaxNumber = new System.Windows.Forms.TextBox();
            this.TxtContactPersonPhone = new System.Windows.Forms.TextBox();
            this.TxtContactPerson = new System.Windows.Forms.TextBox();
            this.TxtSecondaryEmail = new System.Windows.Forms.TextBox();
            this.TxtPrimaryEmail = new System.Windows.Forms.TextBox();
            this.TxtArea = new System.Windows.Forms.TextBox();
            this.CboProvince = new System.Windows.Forms.ComboBox();
            this.CboCompanyType = new System.Windows.Forms.ComboBox();
            this.TxtStreet = new System.Windows.Forms.TextBox();
            this.CboDesignation = new System.Windows.Forms.ComboBox();
            this.TxtOtherInfo = new System.Windows.Forms.TextBox();
            this.TxtBranchName = new System.Windows.Forms.TextBox();
            this.TxtWebSite = new System.Windows.Forms.TextBox();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.RBtnCompany = new System.Windows.Forms.RadioButton();
            this.RbtnBranch = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.DtpFinyearDate = new System.Windows.Forms.DateTimePicker();
            this.CompanyTab = new System.Windows.Forms.TabControl();
            this.CompanyInfoTab = new System.Windows.Forms.TabPage();
            this.btnUnearnedPolicy = new DevComponents.DotNetBar.ButtonX();
            this.cboUnearnedPolicyID = new System.Windows.Forms.ComboBox();
            this.NumTxtWorkingDays = new NumericTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.RBtnYear = new System.Windows.Forms.RadioButton();
            this.RBtnMonth = new System.Windows.Forms.RadioButton();
            this.BtnCompanyType = new DevComponents.DotNetBar.ButtonX();
            this.BtnComIndustry = new DevComponents.DotNetBar.ButtonX();
            this.BtnCurrency = new DevComponents.DotNetBar.ButtonX();
            this.BtnCountry = new DevComponents.DotNetBar.ButtonX();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.LblName = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.GeneralTab = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkMonday = new System.Windows.Forms.CheckBox();
            this.chkSaturday = new System.Windows.Forms.CheckBox();
            this.chkTuesDay = new System.Windows.Forms.CheckBox();
            this.chkFriday = new System.Windows.Forms.CheckBox();
            this.chkSunday = new System.Windows.Forms.CheckBox();
            this.chkWednesday = new System.Windows.Forms.CheckBox();
            this.chkThursday = new System.Windows.Forms.CheckBox();
            this.BtnDesignation = new DevComponents.DotNetBar.ButtonX();
            this.BtnProvince = new DevComponents.DotNetBar.ButtonX();
            this.Accdates = new System.Windows.Forms.TabPage();
            this.txtTAAN = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTAN = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTaxAgentName = new System.Windows.Forms.TextBox();
            this.txtTaxAgencyName = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtTRN = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtTaxablePersonArb = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtTaxablePerson = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.GrdCompanyBanks = new DemoClsDataGridview.ClsDataGirdView();
            this.CompanyBankId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BankNameId = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.AccountNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DtpBkStartDate = new System.Windows.Forms.DateTimePicker();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.TabLogo = new System.Windows.Forms.TabPage();
            this.FilePathButton = new DevComponents.DotNetBar.ButtonX();
            this.BtnRemove = new DevComponents.DotNetBar.ButtonX();
            this.TxtVATNo = new System.Windows.Forms.TextBox();
            this.TxtPANNo = new System.Windows.Forms.TextBox();
            this.TxtTINNo = new System.Windows.Forms.TextBox();
            this.TxtCSTNo = new System.Windows.Forms.TextBox();
            this.TxtSTNo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.StatusLabel = new DevComponents.DotNetBar.LabelItem();
            this.lblcompanystatus = new DevComponents.DotNetBar.LabelItem();
            this.BtnOk = new DevComponents.DotNetBar.ButtonX();
            this.BtnCancel = new DevComponents.DotNetBar.ButtonX();
            this.BtnSave = new DevComponents.DotNetBar.ButtonX();
            this.CompanyAccIDD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyIDD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountNoo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADDFLAG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            Label3 = new System.Windows.Forms.Label();
            Label9 = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            label21 = new System.Windows.Forms.Label();
            label25 = new System.Windows.Forms.Label();
            label26 = new System.Windows.Forms.Label();
            label27 = new System.Windows.Forms.Label();
            label28 = new System.Windows.Forms.Label();
            label29 = new System.Windows.Forms.Label();
            label30 = new System.Windows.Forms.Label();
            label31 = new System.Windows.Forms.Label();
            label32 = new System.Windows.Forms.Label();
            label33 = new System.Windows.Forms.Label();
            label34 = new System.Windows.Forms.Label();
            label35 = new System.Windows.Forms.Label();
            label36 = new System.Windows.Forms.Label();
            label37 = new System.Windows.Forms.Label();
            label38 = new System.Windows.Forms.Label();
            label39 = new System.Windows.Forms.Label();
            label40 = new System.Windows.Forms.Label();
            label41 = new System.Windows.Forms.Label();
            label45 = new System.Windows.Forms.Label();
            label16 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label15 = new System.Windows.Forms.Label();
            lblDailiyPayBasedOn = new System.Windows.Forms.Label();
            lblWorkingDays = new System.Windows.Forms.Label();
            label17 = new System.Windows.Forms.Label();
            lblWeeklyOffday = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.CompanyMasterBindingNavigator)).BeginInit();
            this.CompanyMasterBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogoFilePictureBox)).BeginInit();
            this.Panel1.SuspendLayout();
            this.CompanyTab.SuspendLayout();
            this.CompanyInfoTab.SuspendLayout();
            this.panel2.SuspendLayout();
            this.GeneralTab.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.Accdates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdCompanyBanks)).BeginInit();
            this.TabLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // Label3
            // 
            Label3.AutoSize = true;
            Label3.Location = new System.Drawing.Point(29, 25);
            Label3.Name = "Label3";
            Label3.Size = new System.Drawing.Size(125, 13);
            Label3.TabIndex = 0;
            Label3.Text = "Financial Year Start Date";
            // 
            // Label9
            // 
            Label9.AutoSize = true;
            Label9.Location = new System.Drawing.Point(177, 100);
            Label9.Name = "Label9";
            Label9.Size = new System.Drawing.Size(134, 13);
            Label9.TabIndex = 0;
            Label9.Text = "Width 2 , Height 1 (Inches)";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new System.Drawing.Point(14, 41);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(125, 13);
            label13.TabIndex = 4;
            label13.Text = "Financial Year Start Date";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(18, 114);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(78, 13);
            label14.TabIndex = 8;
            label14.Text = "Employer Code";
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.Location = new System.Drawing.Point(18, 223);
            label21.Name = "label21";
            label21.Size = new System.Drawing.Size(49, 13);
            label21.TabIndex = 17;
            label21.Text = "Currency";
            // 
            // label25
            // 
            label25.AutoSize = true;
            label25.Location = new System.Drawing.Point(18, 196);
            label25.Name = "label25";
            label25.Size = new System.Drawing.Size(43, 13);
            label25.TabIndex = 14;
            label25.Text = "Country";
            // 
            // label26
            // 
            label26.AutoSize = true;
            label26.Location = new System.Drawing.Point(18, 250);
            label26.Name = "label26";
            label26.Size = new System.Drawing.Size(91, 13);
            label26.TabIndex = 20;
            label26.Text = "Company Industry";
            // 
            // label27
            // 
            label27.AutoSize = true;
            label27.Location = new System.Drawing.Point(18, 141);
            label27.Name = "label27";
            label27.Size = new System.Drawing.Size(63, 13);
            label27.TabIndex = 10;
            label27.Text = "Short Name";
            // 
            // label28
            // 
            label28.AutoSize = true;
            label28.Location = new System.Drawing.Point(18, 169);
            label28.Name = "label28";
            label28.Size = new System.Drawing.Size(43, 13);
            label28.TabIndex = 12;
            label28.Text = "PO Box";
            // 
            // label29
            // 
            label29.AutoSize = true;
            label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label29.Location = new System.Drawing.Point(15, 328);
            label29.Name = "label29";
            label29.Size = new System.Drawing.Size(94, 13);
            label29.TabIndex = 27;
            label29.Text = "Other Information";
            // 
            // label30
            // 
            label30.AutoSize = true;
            label30.Location = new System.Drawing.Point(15, 162);
            label30.Name = "label30";
            label30.Size = new System.Drawing.Size(69, 13);
            label30.TabIndex = 14;
            label30.Text = "Primary Email";
            // 
            // label31
            // 
            label31.AutoSize = true;
            label31.Location = new System.Drawing.Point(18, 277);
            label31.Name = "label31";
            label31.Size = new System.Drawing.Size(78, 13);
            label31.TabIndex = 11;
            label31.Text = "Company Type";
            // 
            // label32
            // 
            label32.AutoSize = true;
            label32.Location = new System.Drawing.Point(16, 134);
            label32.Name = "label32";
            label32.Size = new System.Drawing.Size(79, 13);
            label32.TabIndex = 8;
            label32.Text = "Province/State";
            // 
            // label33
            // 
            label33.AutoSize = true;
            label33.Location = new System.Drawing.Point(16, 22);
            label33.Name = "label33";
            label33.Size = new System.Drawing.Size(29, 13);
            label33.TabIndex = 0;
            label33.Text = "Area";
            // 
            // label34
            // 
            label34.AutoSize = true;
            label34.Location = new System.Drawing.Point(15, 190);
            label34.Name = "label34";
            label34.Size = new System.Drawing.Size(86, 13);
            label34.TabIndex = 16;
            label34.Text = "Secondary Email";
            // 
            // label35
            // 
            label35.AutoSize = true;
            label35.Location = new System.Drawing.Point(15, 218);
            label35.Name = "label35";
            label35.Size = new System.Drawing.Size(80, 13);
            label35.TabIndex = 18;
            label35.Text = "Contact Person";
            // 
            // label36
            // 
            label36.AutoSize = true;
            label36.Location = new System.Drawing.Point(15, 246);
            label36.Name = "label36";
            label36.Size = new System.Drawing.Size(63, 13);
            label36.TabIndex = 20;
            label36.Text = "Designation";
            // 
            // label37
            // 
            label37.AutoSize = true;
            label37.Location = new System.Drawing.Point(15, 274);
            label37.Name = "label37";
            label37.Size = new System.Drawing.Size(75, 13);
            label37.TabIndex = 23;
            label37.Text = "Telephone No";
            // 
            // label38
            // 
            label38.AutoSize = true;
            label38.Location = new System.Drawing.Point(16, 300);
            label38.Name = "label38";
            label38.Size = new System.Drawing.Size(64, 13);
            label38.TabIndex = 25;
            label38.Text = "Fax Number";
            // 
            // label39
            // 
            label39.AutoSize = true;
            label39.Location = new System.Drawing.Point(16, 50);
            label39.Name = "label39";
            label39.Size = new System.Drawing.Size(34, 13);
            label39.TabIndex = 2;
            label39.Text = "Block";
            // 
            // label40
            // 
            label40.AutoSize = true;
            label40.Location = new System.Drawing.Point(16, 78);
            label40.Name = "label40";
            label40.Size = new System.Drawing.Size(35, 13);
            label40.TabIndex = 4;
            label40.Text = "Street";
            // 
            // label41
            // 
            label41.AutoSize = true;
            label41.Location = new System.Drawing.Point(16, 106);
            label41.Name = "label41";
            label41.Size = new System.Drawing.Size(24, 13);
            label41.TabIndex = 6;
            label41.Text = "City";
            // 
            // label45
            // 
            label45.AutoSize = true;
            label45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label45.Location = new System.Drawing.Point(12, 31);
            label45.Name = "label45";
            label45.Size = new System.Drawing.Size(90, 13);
            label45.TabIndex = 106;
            label45.Text = "Company Logo";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Location = new System.Drawing.Point(18, 302);
            label16.Name = "label16";
            label16.Size = new System.Drawing.Size(51, 13);
            label16.TabIndex = 29;
            label16.Text = "Web Site";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label12.Location = new System.Drawing.Point(14, 13);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(95, 13);
            label12.TabIndex = 171;
            label12.Text = "Account Details";
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label15.Location = new System.Drawing.Point(659, 304);
            label15.Name = "label15";
            label15.Size = new System.Drawing.Size(70, 13);
            label15.TabIndex = 172;
            label15.Text = "Tax Details";
            label15.Visible = false;
            // 
            // lblDailiyPayBasedOn
            // 
            lblDailiyPayBasedOn.AutoSize = true;
            lblDailiyPayBasedOn.Location = new System.Drawing.Point(18, 331);
            lblDailiyPayBasedOn.Name = "lblDailiyPayBasedOn";
            lblDailiyPayBasedOn.Size = new System.Drawing.Size(101, 13);
            lblDailiyPayBasedOn.TabIndex = 30;
            lblDailiyPayBasedOn.Text = "Daily Pay Based On";
            // 
            // lblWorkingDays
            // 
            lblWorkingDays.AutoSize = true;
            lblWorkingDays.Location = new System.Drawing.Point(18, 356);
            lblWorkingDays.Name = "lblWorkingDays";
            lblWorkingDays.Size = new System.Drawing.Size(74, 13);
            lblWorkingDays.TabIndex = 124;
            lblWorkingDays.Text = "Working Days";
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Location = new System.Drawing.Point(18, 382);
            label17.Name = "label17";
            label17.Size = new System.Drawing.Size(85, 13);
            label17.TabIndex = 129;
            label17.Text = "Unearned Policy";
            // 
            // lblWeeklyOffday
            // 
            lblWeeklyOffday.AutoSize = true;
            lblWeeklyOffday.Location = new System.Drawing.Point(27, 399);
            lblWeeklyOffday.Name = "lblWeeklyOffday";
            lblWeeklyOffday.Size = new System.Drawing.Size(82, 13);
            lblWeeklyOffday.TabIndex = 129;
            lblWeeklyOffday.Text = "Weekly Off Day";
            // 
            // CompanyMasterBindingNavigator
            // 
            this.CompanyMasterBindingNavigator.AddNewItem = null;
            this.CompanyMasterBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.CompanyMasterBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.CompanyMasterBindingNavigator.DeleteItem = null;
            this.CompanyMasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.CompanyMasterBindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.CancelToolStripButton,
            this.ToolStripSeparator1,
            this.BtnComSettings,
            this.BtnPrint,
            this.BtnEmail,
            this.BtnBank,
            this.BtnFinYear,
            this.ToolStripSeparator2,
            this.BtnHelp});
            this.CompanyMasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.CompanyMasterBindingNavigator.MoveFirstItem = null;
            this.CompanyMasterBindingNavigator.MoveLastItem = null;
            this.CompanyMasterBindingNavigator.MoveNextItem = null;
            this.CompanyMasterBindingNavigator.MovePreviousItem = null;
            this.CompanyMasterBindingNavigator.Name = "CompanyMasterBindingNavigator";
            this.CompanyMasterBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.CompanyMasterBindingNavigator.Size = new System.Drawing.Size(552, 25);
            this.CompanyMasterBindingNavigator.TabIndex = 1;
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Enabled = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.ReadOnly = true;
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New Company";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // CompanyMasterBindingNavigatorSaveItem
            // 
            this.CompanyMasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CompanyMasterBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("CompanyMasterBindingNavigatorSaveItem.Image")));
            this.CompanyMasterBindingNavigatorSaveItem.Name = "CompanyMasterBindingNavigatorSaveItem";
            this.CompanyMasterBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.CompanyMasterBindingNavigatorSaveItem.Text = "Save";
            this.CompanyMasterBindingNavigatorSaveItem.Click += new System.EventHandler(this.CompanyMasterBindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Remove";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // CancelToolStripButton
            // 
            this.CancelToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelToolStripButton.Image")));
            this.CancelToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CancelToolStripButton.Name = "CancelToolStripButton";
            this.CancelToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.CancelToolStripButton.ToolTipText = "Clear";
            this.CancelToolStripButton.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnComSettings
            // 
            this.BtnComSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnComSettings.Image = global::MyBooksERP.Properties.Resources.Company_Settings11;
            this.BtnComSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnComSettings.Name = "BtnComSettings";
            this.BtnComSettings.Size = new System.Drawing.Size(23, 22);
            this.BtnComSettings.Text = "Settings";
            this.BtnComSettings.ToolTipText = "Company Settings";
            this.BtnComSettings.Visible = false;
            this.BtnComSettings.Click += new System.EventHandler(this.BtnComSettings_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print/Fax";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // BtnBank
            // 
            this.BtnBank.Image = global::MyBooksERP.Properties.Resources.Bank;
            this.BtnBank.Name = "BtnBank";
            this.BtnBank.Size = new System.Drawing.Size(53, 22);
            this.BtnBank.Text = "Bank";
            this.BtnBank.Click += new System.EventHandler(this.BtnBank_Click);
            // 
            // BtnFinYear
            // 
            this.BtnFinYear.Image = global::MyBooksERP.Properties.Resources.Financial__year;
            this.BtnFinYear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnFinYear.Name = "BtnFinYear";
            this.BtnFinYear.Size = new System.Drawing.Size(65, 22);
            this.BtnFinYear.Text = "FinYear";
            this.BtnFinYear.Visible = false;
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // DtpBookStartDate
            // 
            this.DtpBookStartDate.CustomFormat = "dd-MMM-yyyy";
            this.DtpBookStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpBookStartDate.Location = new System.Drawing.Point(178, 56);
            this.DtpBookStartDate.Name = "DtpBookStartDate";
            this.DtpBookStartDate.Size = new System.Drawing.Size(104, 20);
            this.DtpBookStartDate.TabIndex = 3;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(29, 60);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(83, 13);
            this.Label11.TabIndex = 2;
            this.Label11.Text = "Book Start Date";
            // 
            // DtpFinYearStartDate
            // 
            this.DtpFinYearStartDate.Location = new System.Drawing.Point(0, 0);
            this.DtpFinYearStartDate.Name = "DtpFinYearStartDate";
            this.DtpFinYearStartDate.Size = new System.Drawing.Size(200, 20);
            this.DtpFinYearStartDate.TabIndex = 0;
            // 
            // ComboBoxBankNameIDD
            // 
            this.ComboBoxBankNameIDD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ComboBoxBankNameIDD.HeaderText = "Bank Name";
            this.ComboBoxBankNameIDD.Name = "ComboBoxBankNameIDD";
            this.ComboBoxBankNameIDD.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ComboBoxBankNameIDD.ToolTipText = "Name of the Bank and its account number for pay processing and release.";
            this.ComboBoxBankNameIDD.Width = 300;
            // 
            // ErrorProviderCompany
            // 
            this.ErrorProviderCompany.ContainerControl = this;
            this.ErrorProviderCompany.RightToLeft = true;
            // 
            // TmrFocus
            // 
            this.TmrFocus.Tick += new System.EventHandler(this.tmfocus_Tick);
            // 
            // TmrComapny
            // 
            this.TmrComapny.Interval = 2000;
            // 
            // TxtPOBox
            // 
            this.TxtPOBox.BackColor = System.Drawing.SystemColors.Info;
            this.TxtPOBox.Location = new System.Drawing.Point(146, 163);
            this.TxtPOBox.MaxLength = 10;
            this.TxtPOBox.Name = "TxtPOBox";
            this.TxtPOBox.Size = new System.Drawing.Size(104, 20);
            this.TxtPOBox.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.TxtPOBox, "Post box number of the company.");
            this.TxtPOBox.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // CboCompanyIndustry
            // 
            this.CboCompanyIndustry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompanyIndustry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompanyIndustry.BackColor = System.Drawing.SystemColors.Info;
            this.CboCompanyIndustry.DropDownHeight = 75;
            this.CboCompanyIndustry.FormattingEnabled = true;
            this.CboCompanyIndustry.IntegralHeight = false;
            this.CboCompanyIndustry.Location = new System.Drawing.Point(146, 243);
            this.CboCompanyIndustry.MaxDropDownItems = 10;
            this.CboCompanyIndustry.Name = "CboCompanyIndustry";
            this.CboCompanyIndustry.Size = new System.Drawing.Size(257, 21);
            this.CboCompanyIndustry.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.CboCompanyIndustry, "The Industry category in which the company belongs to.");
            this.CboCompanyIndustry.SelectedIndexChanged += new System.EventHandler(this.txt_TextChanged);
            this.CboCompanyIndustry.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // CboCurrency
            // 
            this.CboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCurrency.BackColor = System.Drawing.SystemColors.Info;
            this.CboCurrency.DropDownHeight = 75;
            this.CboCurrency.FormattingEnabled = true;
            this.CboCurrency.IntegralHeight = false;
            this.CboCurrency.Location = new System.Drawing.Point(146, 216);
            this.CboCurrency.MaxDropDownItems = 10;
            this.CboCurrency.Name = "CboCurrency";
            this.CboCurrency.Size = new System.Drawing.Size(257, 21);
            this.CboCurrency.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.CboCurrency, "Currency of the country in which the Company operates. ");
            this.CboCurrency.SelectedIndexChanged += new System.EventHandler(this.txt_TextChanged);
            this.CboCurrency.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // NameTextBox
            // 
            this.NameTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.NameTextBox.Location = new System.Drawing.Point(146, 85);
            this.NameTextBox.MaxLength = 50;
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(331, 20);
            this.NameTextBox.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.NameTextBox, "Enter the name of the company you wish to create.");
            this.NameTextBox.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // CboCompanyName
            // 
            this.CboCompanyName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompanyName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompanyName.BackColor = System.Drawing.SystemColors.Info;
            this.CboCompanyName.DropDownHeight = 75;
            this.CboCompanyName.FormattingEnabled = true;
            this.CboCompanyName.IntegralHeight = false;
            this.CboCompanyName.Location = new System.Drawing.Point(146, 58);
            this.CboCompanyName.MaxDropDownItems = 10;
            this.CboCompanyName.Name = "CboCompanyName";
            this.CboCompanyName.Size = new System.Drawing.Size(331, 21);
            this.CboCompanyName.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.CboCompanyName, "Select the name of the parent company in which the branch belongs to.");
            this.CboCompanyName.SelectedIndexChanged += new System.EventHandler(this.txt_TextChanged);
            this.CboCompanyName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // CboCountry
            // 
            this.CboCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCountry.BackColor = System.Drawing.SystemColors.Info;
            this.CboCountry.DropDownHeight = 75;
            this.CboCountry.FormattingEnabled = true;
            this.CboCountry.IntegralHeight = false;
            this.CboCountry.Location = new System.Drawing.Point(146, 189);
            this.CboCountry.MaxDropDownItems = 10;
            this.CboCountry.Name = "CboCountry";
            this.CboCountry.Size = new System.Drawing.Size(257, 21);
            this.CboCountry.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.CboCountry, "Country in which the company is registered.");
            this.CboCountry.SelectedIndexChanged += new System.EventHandler(this.txt_TextChanged);
            this.CboCountry.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // TxtShortName
            // 
            this.TxtShortName.AcceptsReturn = true;
            this.TxtShortName.BackColor = System.Drawing.SystemColors.Info;
            this.TxtShortName.Location = new System.Drawing.Point(146, 137);
            this.TxtShortName.MaxLength = 20;
            this.TxtShortName.Name = "TxtShortName";
            this.TxtShortName.Size = new System.Drawing.Size(104, 20);
            this.TxtShortName.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.TxtShortName, "Short name or the abbreviation of the company name.");
            this.TxtShortName.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtEmployerCode
            // 
            this.TxtEmployerCode.BackColor = System.Drawing.SystemColors.Info;
            this.TxtEmployerCode.Location = new System.Drawing.Point(146, 111);
            this.TxtEmployerCode.MaxLength = 14;
            this.TxtEmployerCode.Name = "TxtEmployerCode";
            this.TxtEmployerCode.Size = new System.Drawing.Size(104, 20);
            this.TxtEmployerCode.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.TxtEmployerCode, "This code is provided by the authority for identification of the company.");
            this.TxtEmployerCode.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn1.HeaderText = "Bank Name";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.ToolTipText = "Name of the Bank and its account number for pay processing and release.";
            this.dataGridViewComboBoxColumn1.Width = 300;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn2.HeaderText = "Bank Name";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.ToolTipText = "Name of the Bank and its account number for pay processing and release.";
            this.dataGridViewComboBoxColumn2.Width = 300;
            // 
            // LogoFilePictureBox
            // 
            this.LogoFilePictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LogoFilePictureBox.Location = new System.Drawing.Point(157, 116);
            this.LogoFilePictureBox.Name = "LogoFilePictureBox";
            this.LogoFilePictureBox.Size = new System.Drawing.Size(192, 96);
            this.LogoFilePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LogoFilePictureBox.TabIndex = 3;
            this.LogoFilePictureBox.TabStop = false;
            // 
            // TxtCity
            // 
            this.TxtCity.Location = new System.Drawing.Point(138, 102);
            this.TxtCity.MaxLength = 50;
            this.TxtCity.Name = "TxtCity";
            this.TxtCity.Size = new System.Drawing.Size(330, 20);
            this.TxtCity.TabIndex = 3;
            this.TxtCity.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtBlock
            // 
            this.TxtBlock.Location = new System.Drawing.Point(138, 46);
            this.TxtBlock.MaxLength = 50;
            this.TxtBlock.Name = "TxtBlock";
            this.TxtBlock.Size = new System.Drawing.Size(330, 20);
            this.TxtBlock.TabIndex = 1;
            this.TxtBlock.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtFaxNumber
            // 
            this.TxtFaxNumber.Location = new System.Drawing.Point(138, 300);
            this.TxtFaxNumber.MaxLength = 40;
            this.TxtFaxNumber.Name = "TxtFaxNumber";
            this.TxtFaxNumber.Size = new System.Drawing.Size(257, 20);
            this.TxtFaxNumber.TabIndex = 12;
            this.TxtFaxNumber.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtContactPersonPhone
            // 
            this.TxtContactPersonPhone.Location = new System.Drawing.Point(138, 272);
            this.TxtContactPersonPhone.MaxLength = 40;
            this.TxtContactPersonPhone.Name = "TxtContactPersonPhone";
            this.TxtContactPersonPhone.Size = new System.Drawing.Size(257, 20);
            this.TxtContactPersonPhone.TabIndex = 11;
            this.TxtContactPersonPhone.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtContactPerson
            // 
            this.TxtContactPerson.Location = new System.Drawing.Point(138, 215);
            this.TxtContactPerson.MaxLength = 50;
            this.TxtContactPerson.Name = "TxtContactPerson";
            this.TxtContactPerson.Size = new System.Drawing.Size(330, 20);
            this.TxtContactPerson.TabIndex = 8;
            this.TxtContactPerson.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtSecondaryEmail
            // 
            this.TxtSecondaryEmail.Location = new System.Drawing.Point(138, 187);
            this.TxtSecondaryEmail.MaxLength = 50;
            this.TxtSecondaryEmail.Name = "TxtSecondaryEmail";
            this.TxtSecondaryEmail.Size = new System.Drawing.Size(330, 20);
            this.TxtSecondaryEmail.TabIndex = 7;
            this.TxtSecondaryEmail.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtPrimaryEmail
            // 
            this.TxtPrimaryEmail.Location = new System.Drawing.Point(138, 159);
            this.TxtPrimaryEmail.MaxLength = 50;
            this.TxtPrimaryEmail.Name = "TxtPrimaryEmail";
            this.TxtPrimaryEmail.Size = new System.Drawing.Size(330, 20);
            this.TxtPrimaryEmail.TabIndex = 6;
            this.TxtPrimaryEmail.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtArea
            // 
            this.TxtArea.BackColor = System.Drawing.SystemColors.Window;
            this.TxtArea.Location = new System.Drawing.Point(138, 18);
            this.TxtArea.MaxLength = 50;
            this.TxtArea.Name = "TxtArea";
            this.TxtArea.Size = new System.Drawing.Size(330, 20);
            this.TxtArea.TabIndex = 0;
            this.TxtArea.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // CboProvince
            // 
            this.CboProvince.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboProvince.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboProvince.BackColor = System.Drawing.Color.White;
            this.CboProvince.DropDownHeight = 134;
            this.CboProvince.FormattingEnabled = true;
            this.CboProvince.IntegralHeight = false;
            this.CboProvince.Location = new System.Drawing.Point(138, 130);
            this.CboProvince.MaxDropDownItems = 10;
            this.CboProvince.Name = "CboProvince";
            this.CboProvince.Size = new System.Drawing.Size(296, 21);
            this.CboProvince.TabIndex = 4;
            this.CboProvince.SelectedIndexChanged += new System.EventHandler(this.txt_TextChanged);
            this.CboProvince.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // CboCompanyType
            // 
            this.CboCompanyType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompanyType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompanyType.DropDownHeight = 75;
            this.CboCompanyType.FormattingEnabled = true;
            this.CboCompanyType.IntegralHeight = false;
            this.CboCompanyType.Location = new System.Drawing.Point(146, 270);
            this.CboCompanyType.MaxDropDownItems = 10;
            this.CboCompanyType.Name = "CboCompanyType";
            this.CboCompanyType.Size = new System.Drawing.Size(257, 21);
            this.CboCompanyType.TabIndex = 13;
            this.CboCompanyType.SelectedIndexChanged += new System.EventHandler(this.txt_TextChanged);
            this.CboCompanyType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // TxtStreet
            // 
            this.TxtStreet.Location = new System.Drawing.Point(138, 74);
            this.TxtStreet.MaxLength = 50;
            this.TxtStreet.Name = "TxtStreet";
            this.TxtStreet.Size = new System.Drawing.Size(331, 20);
            this.TxtStreet.TabIndex = 2;
            this.TxtStreet.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // CboDesignation
            // 
            this.CboDesignation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboDesignation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDesignation.DropDownHeight = 134;
            this.CboDesignation.FormattingEnabled = true;
            this.CboDesignation.IntegralHeight = false;
            this.CboDesignation.Location = new System.Drawing.Point(138, 243);
            this.CboDesignation.MaxDropDownItems = 10;
            this.CboDesignation.Name = "CboDesignation";
            this.CboDesignation.Size = new System.Drawing.Size(257, 21);
            this.CboDesignation.TabIndex = 9;
            this.CboDesignation.SelectedIndexChanged += new System.EventHandler(this.txt_TextChanged);
            this.CboDesignation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // TxtOtherInfo
            // 
            this.TxtOtherInfo.Location = new System.Drawing.Point(137, 328);
            this.TxtOtherInfo.MaxLength = 200;
            this.TxtOtherInfo.Multiline = true;
            this.TxtOtherInfo.Name = "TxtOtherInfo";
            this.TxtOtherInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TxtOtherInfo.Size = new System.Drawing.Size(332, 51);
            this.TxtOtherInfo.TabIndex = 13;
            this.TxtOtherInfo.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtBranchName
            // 
            this.TxtBranchName.BackColor = System.Drawing.SystemColors.Window;
            this.TxtBranchName.Enabled = false;
            this.TxtBranchName.Location = new System.Drawing.Point(186, 85);
            this.TxtBranchName.MaxLength = 50;
            this.TxtBranchName.Name = "TxtBranchName";
            this.TxtBranchName.Size = new System.Drawing.Size(291, 20);
            this.TxtBranchName.TabIndex = 3;
            this.TxtBranchName.Visible = false;
            this.TxtBranchName.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtWebSite
            // 
            this.TxtWebSite.Location = new System.Drawing.Point(146, 297);
            this.TxtWebSite.MaxLength = 50;
            this.TxtWebSite.Name = "TxtWebSite";
            this.TxtWebSite.Size = new System.Drawing.Size(331, 20);
            this.TxtWebSite.TabIndex = 15;
            this.TxtWebSite.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // Panel1
            // 
            this.Panel1.Controls.Add(this.RBtnCompany);
            this.Panel1.Controls.Add(this.RbtnBranch);
            this.Panel1.Location = new System.Drawing.Point(147, 18);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(216, 32);
            this.Panel1.TabIndex = 0;
            // 
            // RBtnCompany
            // 
            this.RBtnCompany.AutoSize = true;
            this.RBtnCompany.Checked = true;
            this.RBtnCompany.Location = new System.Drawing.Point(3, 13);
            this.RBtnCompany.Name = "RBtnCompany";
            this.RBtnCompany.Size = new System.Drawing.Size(69, 17);
            this.RBtnCompany.TabIndex = 0;
            this.RBtnCompany.TabStop = true;
            this.RBtnCompany.Text = "Company";
            this.RBtnCompany.UseVisualStyleBackColor = true;
            this.RBtnCompany.CheckedChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // RbtnBranch
            // 
            this.RbtnBranch.AutoSize = true;
            this.RbtnBranch.Location = new System.Drawing.Point(99, 13);
            this.RbtnBranch.Name = "RbtnBranch";
            this.RbtnBranch.Size = new System.Drawing.Size(59, 17);
            this.RbtnBranch.TabIndex = 1;
            this.RbtnBranch.Text = "Branch";
            this.RbtnBranch.UseVisualStyleBackColor = true;
            this.RbtnBranch.CheckedChanged += new System.EventHandler(this.RbtnBranch_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(326, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Book Start Date";
            // 
            // DtpFinyearDate
            // 
            this.DtpFinyearDate.CustomFormat = "dd-MMM-yyyy";
            this.DtpFinyearDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFinyearDate.Location = new System.Drawing.Point(146, 39);
            this.DtpFinyearDate.Name = "DtpFinyearDate";
            this.DtpFinyearDate.Size = new System.Drawing.Size(104, 20);
            this.DtpFinyearDate.TabIndex = 0;
            this.DtpFinyearDate.ValueChanged += new System.EventHandler(this.DtpFinyearDate_ValueChanged);
            // 
            // CompanyTab
            // 
            this.CompanyTab.Controls.Add(this.CompanyInfoTab);
            this.CompanyTab.Controls.Add(this.GeneralTab);
            this.CompanyTab.Controls.Add(this.Accdates);
            this.CompanyTab.Controls.Add(this.TabLogo);
            this.CompanyTab.Location = new System.Drawing.Point(6, 39);
            this.CompanyTab.Name = "CompanyTab";
            this.CompanyTab.SelectedIndex = 0;
            this.CompanyTab.Size = new System.Drawing.Size(546, 478);
            this.CompanyTab.TabIndex = 0;
            // 
            // CompanyInfoTab
            // 
            this.CompanyInfoTab.AutoScroll = true;
            this.CompanyInfoTab.Controls.Add(this.btnUnearnedPolicy);
            this.CompanyInfoTab.Controls.Add(this.cboUnearnedPolicyID);
            this.CompanyInfoTab.Controls.Add(label17);
            this.CompanyInfoTab.Controls.Add(this.NumTxtWorkingDays);
            this.CompanyInfoTab.Controls.Add(lblWorkingDays);
            this.CompanyInfoTab.Controls.Add(this.panel2);
            this.CompanyInfoTab.Controls.Add(lblDailiyPayBasedOn);
            this.CompanyInfoTab.Controls.Add(this.BtnCompanyType);
            this.CompanyInfoTab.Controls.Add(this.BtnComIndustry);
            this.CompanyInfoTab.Controls.Add(this.BtnCurrency);
            this.CompanyInfoTab.Controls.Add(this.BtnCountry);
            this.CompanyInfoTab.Controls.Add(label14);
            this.CompanyInfoTab.Controls.Add(this.TxtEmployerCode);
            this.CompanyInfoTab.Controls.Add(this.TxtShortName);
            this.CompanyInfoTab.Controls.Add(label16);
            this.CompanyInfoTab.Controls.Add(this.TxtWebSite);
            this.CompanyInfoTab.Controls.Add(this.TxtBranchName);
            this.CompanyInfoTab.Controls.Add(label31);
            this.CompanyInfoTab.Controls.Add(label21);
            this.CompanyInfoTab.Controls.Add(this.CboCompanyType);
            this.CompanyInfoTab.Controls.Add(this.NameTextBox);
            this.CompanyInfoTab.Controls.Add(this.label22);
            this.CompanyInfoTab.Controls.Add(this.TxtPOBox);
            this.CompanyInfoTab.Controls.Add(this.label23);
            this.CompanyInfoTab.Controls.Add(this.LblName);
            this.CompanyInfoTab.Controls.Add(this.Panel1);
            this.CompanyInfoTab.Controls.Add(label25);
            this.CompanyInfoTab.Controls.Add(label26);
            this.CompanyInfoTab.Controls.Add(label27);
            this.CompanyInfoTab.Controls.Add(label28);
            this.CompanyInfoTab.Controls.Add(this.Label6);
            this.CompanyInfoTab.Controls.Add(this.CboCompanyIndustry);
            this.CompanyInfoTab.Controls.Add(this.Label5);
            this.CompanyInfoTab.Controls.Add(this.CboCurrency);
            this.CompanyInfoTab.Controls.Add(this.CboCompanyName);
            this.CompanyInfoTab.Controls.Add(this.CboCountry);
            this.CompanyInfoTab.Location = new System.Drawing.Point(4, 22);
            this.CompanyInfoTab.Name = "CompanyInfoTab";
            this.CompanyInfoTab.Padding = new System.Windows.Forms.Padding(3);
            this.CompanyInfoTab.Size = new System.Drawing.Size(538, 452);
            this.CompanyInfoTab.TabIndex = 0;
            this.CompanyInfoTab.Tag = "0";
            this.CompanyInfoTab.Text = "Company Info";
            this.CompanyInfoTab.UseVisualStyleBackColor = true;
            // 
            // btnUnearnedPolicy
            // 
            this.btnUnearnedPolicy.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnUnearnedPolicy.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnUnearnedPolicy.Location = new System.Drawing.Point(410, 381);
            this.btnUnearnedPolicy.Name = "btnUnearnedPolicy";
            this.btnUnearnedPolicy.Size = new System.Drawing.Size(28, 22);
            this.btnUnearnedPolicy.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnUnearnedPolicy.TabIndex = 131;
            this.btnUnearnedPolicy.Text = "...";
            this.btnUnearnedPolicy.Click += new System.EventHandler(this.btnUnearnedPolicy_Click);
            // 
            // cboUnearnedPolicyID
            // 
            this.cboUnearnedPolicyID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboUnearnedPolicyID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboUnearnedPolicyID.BackColor = System.Drawing.Color.White;
            this.cboUnearnedPolicyID.DropDownHeight = 75;
            this.cboUnearnedPolicyID.FormattingEnabled = true;
            this.cboUnearnedPolicyID.IntegralHeight = false;
            this.cboUnearnedPolicyID.Location = new System.Drawing.Point(146, 382);
            this.cboUnearnedPolicyID.MaxDropDownItems = 10;
            this.cboUnearnedPolicyID.Name = "cboUnearnedPolicyID";
            this.cboUnearnedPolicyID.Size = new System.Drawing.Size(257, 21);
            this.cboUnearnedPolicyID.TabIndex = 130;
            this.cboUnearnedPolicyID.SelectedIndexChanged += new System.EventHandler(this.cboUnearnedPolicyID_SelectedIndexChanged);
            this.cboUnearnedPolicyID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboUnearnedPolicyID_KeyPress);
            // 
            // NumTxtWorkingDays
            // 
            this.NumTxtWorkingDays.BackColor = System.Drawing.SystemColors.Info;
            this.NumTxtWorkingDays.DecimalPlaces = 0;
            this.NumTxtWorkingDays.Location = new System.Drawing.Point(146, 356);
            this.NumTxtWorkingDays.MaxLength = 3;
            this.NumTxtWorkingDays.Name = "NumTxtWorkingDays";
            this.NumTxtWorkingDays.ShortcutsEnabled = false;
            this.NumTxtWorkingDays.Size = new System.Drawing.Size(88, 20);
            this.NumTxtWorkingDays.TabIndex = 125;
            this.NumTxtWorkingDays.Text = "0";
            this.NumTxtWorkingDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NumTxtWorkingDays.TextChanged += new System.EventHandler(this.NumTxtWorkingDays_TextChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.RBtnYear);
            this.panel2.Controls.Add(this.RBtnMonth);
            this.panel2.Location = new System.Drawing.Point(146, 325);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(216, 25);
            this.panel2.TabIndex = 31;
            // 
            // RBtnYear
            // 
            this.RBtnYear.AutoSize = true;
            this.RBtnYear.Checked = true;
            this.RBtnYear.Location = new System.Drawing.Point(3, 5);
            this.RBtnYear.Name = "RBtnYear";
            this.RBtnYear.Size = new System.Drawing.Size(47, 17);
            this.RBtnYear.TabIndex = 0;
            this.RBtnYear.TabStop = true;
            this.RBtnYear.Text = "Year";
            this.RBtnYear.UseVisualStyleBackColor = true;
            this.RBtnYear.CheckedChanged += new System.EventHandler(this.RBtnYear_CheckedChanged);
            // 
            // RBtnMonth
            // 
            this.RBtnMonth.AutoSize = true;
            this.RBtnMonth.Location = new System.Drawing.Point(99, 5);
            this.RBtnMonth.Name = "RBtnMonth";
            this.RBtnMonth.Size = new System.Drawing.Size(55, 17);
            this.RBtnMonth.TabIndex = 1;
            this.RBtnMonth.Text = "Month";
            this.RBtnMonth.UseVisualStyleBackColor = true;
            this.RBtnMonth.CheckedChanged += new System.EventHandler(this.RBtnMonth_CheckedChanged);
            // 
            // BtnCompanyType
            // 
            this.BtnCompanyType.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnCompanyType.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnCompanyType.Location = new System.Drawing.Point(410, 270);
            this.BtnCompanyType.Name = "BtnCompanyType";
            this.BtnCompanyType.Size = new System.Drawing.Size(28, 22);
            this.BtnCompanyType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnCompanyType.TabIndex = 14;
            this.BtnCompanyType.Text = "...";
            this.BtnCompanyType.Click += new System.EventHandler(this.BtnCompanyType_Click);
            // 
            // BtnComIndustry
            // 
            this.BtnComIndustry.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnComIndustry.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnComIndustry.Location = new System.Drawing.Point(410, 243);
            this.BtnComIndustry.Name = "BtnComIndustry";
            this.BtnComIndustry.Size = new System.Drawing.Size(28, 22);
            this.BtnComIndustry.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnComIndustry.TabIndex = 12;
            this.BtnComIndustry.Text = "...";
            this.BtnComIndustry.Click += new System.EventHandler(this.CboComIndustry_Click);
            // 
            // BtnCurrency
            // 
            this.BtnCurrency.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnCurrency.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnCurrency.Location = new System.Drawing.Point(410, 216);
            this.BtnCurrency.Name = "BtnCurrency";
            this.BtnCurrency.Size = new System.Drawing.Size(28, 22);
            this.BtnCurrency.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnCurrency.TabIndex = 10;
            this.BtnCurrency.Text = "...";
            this.BtnCurrency.Click += new System.EventHandler(this.BtnCurrency_Click);
            // 
            // BtnCountry
            // 
            this.BtnCountry.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnCountry.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnCountry.Location = new System.Drawing.Point(410, 189);
            this.BtnCountry.Name = "BtnCountry";
            this.BtnCountry.Size = new System.Drawing.Size(28, 22);
            this.BtnCountry.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnCountry.TabIndex = 8;
            this.BtnCountry.Text = "...";
            this.BtnCountry.Click += new System.EventHandler(this.BtnCountry_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(18, 33);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(79, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Creation Mode ";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(18, 60);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(82, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Company Name";
            // 
            // LblName
            // 
            this.LblName.AutoSize = true;
            this.LblName.Location = new System.Drawing.Point(18, 87);
            this.LblName.Name = "LblName";
            this.LblName.Size = new System.Drawing.Size(72, 13);
            this.LblName.TabIndex = 6;
            this.LblName.Text = "Branch Name";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(49, 420);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(185, 13);
            this.Label6.TabIndex = 16;
            this.Label6.Text = "Fields marked in yellow are mandatory";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.Label5.Font = new System.Drawing.Font("Wingdings", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.Label5.ForeColor = System.Drawing.SystemColors.Info;
            this.Label5.Location = new System.Drawing.Point(18, 416);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(20, 17);
            this.Label5.TabIndex = 15;
            this.Label5.Text = "n";
            // 
            // GeneralTab
            // 
            this.GeneralTab.AutoScroll = true;
            this.GeneralTab.Controls.Add(lblWeeklyOffday);
            this.GeneralTab.Controls.Add(this.groupBox1);
            this.GeneralTab.Controls.Add(this.BtnDesignation);
            this.GeneralTab.Controls.Add(this.BtnProvince);
            this.GeneralTab.Controls.Add(this.TxtOtherInfo);
            this.GeneralTab.Controls.Add(this.TxtStreet);
            this.GeneralTab.Controls.Add(label29);
            this.GeneralTab.Controls.Add(this.TxtArea);
            this.GeneralTab.Controls.Add(label30);
            this.GeneralTab.Controls.Add(this.TxtPrimaryEmail);
            this.GeneralTab.Controls.Add(this.TxtSecondaryEmail);
            this.GeneralTab.Controls.Add(label32);
            this.GeneralTab.Controls.Add(this.TxtContactPerson);
            this.GeneralTab.Controls.Add(label33);
            this.GeneralTab.Controls.Add(this.TxtContactPersonPhone);
            this.GeneralTab.Controls.Add(label34);
            this.GeneralTab.Controls.Add(this.TxtFaxNumber);
            this.GeneralTab.Controls.Add(label35);
            this.GeneralTab.Controls.Add(this.TxtBlock);
            this.GeneralTab.Controls.Add(label36);
            this.GeneralTab.Controls.Add(this.TxtCity);
            this.GeneralTab.Controls.Add(label37);
            this.GeneralTab.Controls.Add(label38);
            this.GeneralTab.Controls.Add(this.CboDesignation);
            this.GeneralTab.Controls.Add(label39);
            this.GeneralTab.Controls.Add(label40);
            this.GeneralTab.Controls.Add(label41);
            this.GeneralTab.Controls.Add(this.CboProvince);
            this.GeneralTab.Location = new System.Drawing.Point(4, 22);
            this.GeneralTab.Name = "GeneralTab";
            this.GeneralTab.Padding = new System.Windows.Forms.Padding(3);
            this.GeneralTab.Size = new System.Drawing.Size(538, 452);
            this.GeneralTab.TabIndex = 1;
            this.GeneralTab.Tag = "1";
            this.GeneralTab.Text = "General Info";
            this.GeneralTab.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkMonday);
            this.groupBox1.Controls.Add(this.chkSaturday);
            this.groupBox1.Controls.Add(this.chkTuesDay);
            this.groupBox1.Controls.Add(this.chkFriday);
            this.groupBox1.Controls.Add(this.chkSunday);
            this.groupBox1.Controls.Add(this.chkWednesday);
            this.groupBox1.Controls.Add(this.chkThursday);
            this.groupBox1.Location = new System.Drawing.Point(138, 385);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(331, 59);
            this.groupBox1.TabIndex = 128;
            this.groupBox1.TabStop = false;
            // 
            // chkMonday
            // 
            this.chkMonday.AutoSize = true;
            this.chkMonday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkMonday.Location = new System.Drawing.Point(80, 14);
            this.chkMonday.Name = "chkMonday";
            this.chkMonday.Size = new System.Drawing.Size(64, 17);
            this.chkMonday.TabIndex = 12;
            this.chkMonday.Tag = "2";
            this.chkMonday.Text = "Monday";
            this.chkMonday.UseVisualStyleBackColor = true;
            this.chkMonday.CheckedChanged += new System.EventHandler(this.chkSunday_CheckedChanged);
            // 
            // chkSaturday
            // 
            this.chkSaturday.AutoSize = true;
            this.chkSaturday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkSaturday.Location = new System.Drawing.Point(149, 37);
            this.chkSaturday.Name = "chkSaturday";
            this.chkSaturday.Size = new System.Drawing.Size(68, 17);
            this.chkSaturday.TabIndex = 17;
            this.chkSaturday.Tag = "7";
            this.chkSaturday.Text = "Saturday";
            this.chkSaturday.UseVisualStyleBackColor = true;
            this.chkSaturday.CheckedChanged += new System.EventHandler(this.chkSunday_CheckedChanged);
            // 
            // chkTuesDay
            // 
            this.chkTuesDay.AutoSize = true;
            this.chkTuesDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkTuesDay.Location = new System.Drawing.Point(149, 14);
            this.chkTuesDay.Name = "chkTuesDay";
            this.chkTuesDay.Size = new System.Drawing.Size(67, 17);
            this.chkTuesDay.TabIndex = 13;
            this.chkTuesDay.Tag = "3";
            this.chkTuesDay.Text = "Tuesday";
            this.chkTuesDay.UseVisualStyleBackColor = true;
            this.chkTuesDay.CheckedChanged += new System.EventHandler(this.chkSunday_CheckedChanged);
            // 
            // chkFriday
            // 
            this.chkFriday.AutoSize = true;
            this.chkFriday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkFriday.Location = new System.Drawing.Point(80, 37);
            this.chkFriday.Name = "chkFriday";
            this.chkFriday.Size = new System.Drawing.Size(54, 17);
            this.chkFriday.TabIndex = 16;
            this.chkFriday.Tag = "6";
            this.chkFriday.Text = "Friday";
            this.chkFriday.UseVisualStyleBackColor = true;
            this.chkFriday.CheckedChanged += new System.EventHandler(this.chkSunday_CheckedChanged);
            // 
            // chkSunday
            // 
            this.chkSunday.AutoSize = true;
            this.chkSunday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkSunday.Location = new System.Drawing.Point(8, 14);
            this.chkSunday.Name = "chkSunday";
            this.chkSunday.Size = new System.Drawing.Size(62, 17);
            this.chkSunday.TabIndex = 11;
            this.chkSunday.Tag = "1";
            this.chkSunday.Text = "Sunday";
            this.chkSunday.UseVisualStyleBackColor = true;
            this.chkSunday.CheckedChanged += new System.EventHandler(this.chkSunday_CheckedChanged);
            // 
            // chkWednesday
            // 
            this.chkWednesday.AutoSize = true;
            this.chkWednesday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkWednesday.Location = new System.Drawing.Point(221, 14);
            this.chkWednesday.Name = "chkWednesday";
            this.chkWednesday.Size = new System.Drawing.Size(83, 17);
            this.chkWednesday.TabIndex = 14;
            this.chkWednesday.Tag = "4";
            this.chkWednesday.Text = "Wednesday";
            this.chkWednesday.UseVisualStyleBackColor = true;
            this.chkWednesday.CheckedChanged += new System.EventHandler(this.chkSunday_CheckedChanged);
            // 
            // chkThursday
            // 
            this.chkThursday.AutoSize = true;
            this.chkThursday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkThursday.Location = new System.Drawing.Point(8, 37);
            this.chkThursday.Name = "chkThursday";
            this.chkThursday.Size = new System.Drawing.Size(70, 17);
            this.chkThursday.TabIndex = 15;
            this.chkThursday.Tag = "5";
            this.chkThursday.Text = "Thursday";
            this.chkThursday.UseVisualStyleBackColor = true;
            this.chkThursday.CheckedChanged += new System.EventHandler(this.chkSunday_CheckedChanged);
            // 
            // BtnDesignation
            // 
            this.BtnDesignation.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnDesignation.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnDesignation.Location = new System.Drawing.Point(401, 242);
            this.BtnDesignation.Name = "BtnDesignation";
            this.BtnDesignation.Size = new System.Drawing.Size(28, 22);
            this.BtnDesignation.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnDesignation.TabIndex = 10;
            this.BtnDesignation.Text = "...";
            this.BtnDesignation.Click += new System.EventHandler(this.BtnDesignation_Click);
            // 
            // BtnProvince
            // 
            this.BtnProvince.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnProvince.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnProvince.Location = new System.Drawing.Point(440, 130);
            this.BtnProvince.Name = "BtnProvince";
            this.BtnProvince.Size = new System.Drawing.Size(28, 22);
            this.BtnProvince.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnProvince.TabIndex = 5;
            this.BtnProvince.Text = "...";
            this.BtnProvince.Click += new System.EventHandler(this.BtnProvince_Click);
            // 
            // Accdates
            // 
            this.Accdates.Controls.Add(this.txtTAAN);
            this.Accdates.Controls.Add(this.label18);
            this.Accdates.Controls.Add(this.txtTAN);
            this.Accdates.Controls.Add(this.label19);
            this.Accdates.Controls.Add(this.label20);
            this.Accdates.Controls.Add(this.txtTaxAgentName);
            this.Accdates.Controls.Add(this.txtTaxAgencyName);
            this.Accdates.Controls.Add(this.label24);
            this.Accdates.Controls.Add(this.txtTRN);
            this.Accdates.Controls.Add(this.label42);
            this.Accdates.Controls.Add(this.txtTaxablePersonArb);
            this.Accdates.Controls.Add(this.label43);
            this.Accdates.Controls.Add(this.txtTaxablePerson);
            this.Accdates.Controls.Add(this.label44);
            this.Accdates.Controls.Add(label12);
            this.Accdates.Controls.Add(this.GrdCompanyBanks);
            this.Accdates.Controls.Add(this.DtpBkStartDate);
            this.Accdates.Controls.Add(label13);
            this.Accdates.Controls.Add(this.label8);
            this.Accdates.Controls.Add(this.DtpFinyearDate);
            this.Accdates.Controls.Add(this.shapeContainer1);
            this.Accdates.Location = new System.Drawing.Point(4, 22);
            this.Accdates.Name = "Accdates";
            this.Accdates.Padding = new System.Windows.Forms.Padding(3);
            this.Accdates.Size = new System.Drawing.Size(538, 452);
            this.Accdates.TabIndex = 3;
            this.Accdates.Tag = "2";
            this.Accdates.Text = "Accounts And Dates";
            this.Accdates.UseVisualStyleBackColor = true;
            this.Accdates.Click += new System.EventHandler(this.Accdates_Click);
            // 
            // txtTAAN
            // 
            this.txtTAAN.Location = new System.Drawing.Point(170, 424);
            this.txtTAAN.MaxLength = 40;
            this.txtTAAN.Name = "txtTAAN";
            this.txtTAAN.Size = new System.Drawing.Size(348, 20);
            this.txtTAAN.TabIndex = 196;
            this.txtTAAN.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(14, 362);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 13);
            this.label18.TabIndex = 193;
            this.label18.Text = "TAN";
            // 
            // txtTAN
            // 
            this.txtTAN.Location = new System.Drawing.Point(170, 358);
            this.txtTAN.MaxLength = 40;
            this.txtTAN.Name = "txtTAN";
            this.txtTAN.Size = new System.Drawing.Size(348, 20);
            this.txtTAN.TabIndex = 192;
            this.txtTAN.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(14, 428);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 13);
            this.label19.TabIndex = 197;
            this.label19.Text = "TAAN";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(14, 395);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(81, 13);
            this.label20.TabIndex = 195;
            this.label20.Text = "TaxAgentName";
            // 
            // txtTaxAgentName
            // 
            this.txtTaxAgentName.Location = new System.Drawing.Point(170, 391);
            this.txtTaxAgentName.MaxLength = 40;
            this.txtTaxAgentName.Name = "txtTaxAgentName";
            this.txtTaxAgentName.Size = new System.Drawing.Size(348, 20);
            this.txtTaxAgentName.TabIndex = 194;
            this.txtTaxAgentName.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtTaxAgencyName
            // 
            this.txtTaxAgencyName.Location = new System.Drawing.Point(170, 328);
            this.txtTaxAgencyName.MaxLength = 40;
            this.txtTaxAgencyName.Name = "txtTaxAgencyName";
            this.txtTaxAgencyName.Size = new System.Drawing.Size(348, 20);
            this.txtTaxAgencyName.TabIndex = 190;
            this.txtTaxAgencyName.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(14, 332);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(89, 13);
            this.label24.TabIndex = 191;
            this.label24.Text = "TaxAgencyName";
            // 
            // txtTRN
            // 
            this.txtTRN.Location = new System.Drawing.Point(170, 295);
            this.txtTRN.MaxLength = 40;
            this.txtTRN.Name = "txtTRN";
            this.txtTRN.Size = new System.Drawing.Size(348, 20);
            this.txtTRN.TabIndex = 188;
            this.txtTRN.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(14, 299);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(153, 13);
            this.label42.TabIndex = 189;
            this.label42.Text = "TRN(Tax Registration Number)";
            // 
            // txtTaxablePersonArb
            // 
            this.txtTaxablePersonArb.Location = new System.Drawing.Point(170, 262);
            this.txtTaxablePersonArb.MaxLength = 40;
            this.txtTaxablePersonArb.Name = "txtTaxablePersonArb";
            this.txtTaxablePersonArb.Size = new System.Drawing.Size(348, 20);
            this.txtTaxablePersonArb.TabIndex = 186;
            this.txtTaxablePersonArb.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(14, 266);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(145, 13);
            this.label43.TabIndex = 187;
            this.label43.Text = "Taxable Person Name Arabic";
            // 
            // txtTaxablePerson
            // 
            this.txtTaxablePerson.Location = new System.Drawing.Point(170, 229);
            this.txtTaxablePerson.MaxLength = 40;
            this.txtTaxablePerson.Name = "txtTaxablePerson";
            this.txtTaxablePerson.Size = new System.Drawing.Size(348, 20);
            this.txtTaxablePerson.TabIndex = 184;
            this.txtTaxablePerson.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(14, 233);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(112, 13);
            this.label44.TabIndex = 185;
            this.label44.Text = "Taxable Person Name";
            // 
            // GrdCompanyBanks
            // 
            this.GrdCompanyBanks.AddNewRow = true;
            this.GrdCompanyBanks.AlphaNumericCols = new int[0];
            this.GrdCompanyBanks.BackgroundColor = System.Drawing.Color.White;
            this.GrdCompanyBanks.CapsLockCols = new int[0];
            this.GrdCompanyBanks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrdCompanyBanks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CompanyBankId,
            this.CompanyId,
            this.BankNameId,
            this.AccountNumber,
            this.AccountID});
            this.GrdCompanyBanks.DecimalCols = new int[0];
            this.GrdCompanyBanks.HasSlNo = false;
            this.GrdCompanyBanks.LastRowIndex = 0;
            this.GrdCompanyBanks.Location = new System.Drawing.Point(17, 75);
            this.GrdCompanyBanks.Name = "GrdCompanyBanks";
            this.GrdCompanyBanks.NegativeValueCols = new int[0];
            this.GrdCompanyBanks.NumericCols = new int[0];
            this.GrdCompanyBanks.RowHeadersWidth = 50;
            this.GrdCompanyBanks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GrdCompanyBanks.Size = new System.Drawing.Size(505, 148);
            this.GrdCompanyBanks.TabIndex = 3;
            this.GrdCompanyBanks.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.GrdCompanyBanks_CellValueChanged);
            this.GrdCompanyBanks.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.GrdCompanyBanks_UserDeletingRow);
            this.GrdCompanyBanks.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.GrdCompanyBanks_CellBeginEdit);
            this.GrdCompanyBanks.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.GrdCompanyBanks_RowsAdded);
            this.GrdCompanyBanks.CurrentCellDirtyStateChanged += new System.EventHandler(this.GrdCompanyBanks_CurrentCellDirtyStateChanged);
            this.GrdCompanyBanks.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GrdCompanyBanks_KeyDown);
            this.GrdCompanyBanks.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.GrdCompanyBanks_RowsRemoved);
            // 
            // CompanyBankId
            // 
            this.CompanyBankId.HeaderText = "CompanyAccIDD";
            this.CompanyBankId.Name = "CompanyBankId";
            this.CompanyBankId.Visible = false;
            // 
            // CompanyId
            // 
            this.CompanyId.HeaderText = "CompanyIDD";
            this.CompanyId.Name = "CompanyId";
            this.CompanyId.Visible = false;
            // 
            // BankNameId
            // 
            this.BankNameId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BankNameId.HeaderText = "Bank Name";
            this.BankNameId.Name = "BankNameId";
            this.BankNameId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BankNameId.Width = 300;
            // 
            // AccountNumber
            // 
            this.AccountNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AccountNumber.HeaderText = "Account Number";
            this.AccountNumber.MaxInputLength = 49;
            this.AccountNumber.Name = "AccountNumber";
            this.AccountNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AccountID
            // 
            this.AccountID.HeaderText = "AccountID";
            this.AccountID.Name = "AccountID";
            this.AccountID.Visible = false;
            // 
            // DtpBkStartDate
            // 
            this.DtpBkStartDate.CustomFormat = "dd-MMM-yyyy";
            this.DtpBkStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpBkStartDate.Location = new System.Drawing.Point(418, 39);
            this.DtpBkStartDate.Name = "DtpBkStartDate";
            this.DtpBkStartDate.Size = new System.Drawing.Size(104, 20);
            this.DtpBkStartDate.TabIndex = 2;
            this.DtpBkStartDate.ValueChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2});
            this.shapeContainer1.Size = new System.Drawing.Size(532, 446);
            this.shapeContainer1.TabIndex = 20;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.Visible = false;
            this.lineShape2.X1 = 70;
            this.lineShape2.X2 = 519;
            this.lineShape2.Y1 = 18;
            this.lineShape2.Y2 = 18;
            // 
            // TabLogo
            // 
            this.TabLogo.AutoScroll = true;
            this.TabLogo.Controls.Add(this.FilePathButton);
            this.TabLogo.Controls.Add(this.BtnRemove);
            this.TabLogo.Controls.Add(Label9);
            this.TabLogo.Controls.Add(label45);
            this.TabLogo.Controls.Add(this.LogoFilePictureBox);
            this.TabLogo.Location = new System.Drawing.Point(4, 22);
            this.TabLogo.Name = "TabLogo";
            this.TabLogo.Padding = new System.Windows.Forms.Padding(3);
            this.TabLogo.Size = new System.Drawing.Size(538, 452);
            this.TabLogo.TabIndex = 2;
            this.TabLogo.Tag = "3";
            this.TabLogo.Text = "Logo       ";
            this.TabLogo.ToolTipText = "Logo";
            this.TabLogo.UseVisualStyleBackColor = true;
            // 
            // FilePathButton
            // 
            this.FilePathButton.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.FilePathButton.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.FilePathButton.Location = new System.Drawing.Point(283, 216);
            this.FilePathButton.Name = "FilePathButton";
            this.FilePathButton.Size = new System.Drawing.Size(65, 23);
            this.FilePathButton.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.FilePathButton.TabIndex = 2;
            this.FilePathButton.Text = "&Browse";
            this.FilePathButton.Click += new System.EventHandler(this.FilePathButton_Click);
            // 
            // BtnRemove
            // 
            this.BtnRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnRemove.Location = new System.Drawing.Point(157, 216);
            this.BtnRemove.Name = "BtnRemove";
            this.BtnRemove.Size = new System.Drawing.Size(65, 23);
            this.BtnRemove.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnRemove.TabIndex = 1;
            this.BtnRemove.Text = "&Remove";
            this.BtnRemove.Click += new System.EventHandler(this.BtnRemove_Click);
            // 
            // TxtVATNo
            // 
            this.TxtVATNo.Location = new System.Drawing.Point(771, 382);
            this.TxtVATNo.MaxLength = 30;
            this.TxtVATNo.Name = "TxtVATNo";
            this.TxtVATNo.Size = new System.Drawing.Size(56, 20);
            this.TxtVATNo.TabIndex = 8;
            this.TxtVATNo.Visible = false;
            // 
            // TxtPANNo
            // 
            this.TxtPANNo.Location = new System.Drawing.Point(771, 356);
            this.TxtPANNo.MaxLength = 30;
            this.TxtPANNo.Name = "TxtPANNo";
            this.TxtPANNo.Size = new System.Drawing.Size(56, 20);
            this.TxtPANNo.TabIndex = 7;
            this.TxtPANNo.Visible = false;
            // 
            // TxtTINNo
            // 
            this.TxtTINNo.Location = new System.Drawing.Point(771, 330);
            this.TxtTINNo.MaxLength = 30;
            this.TxtTINNo.Name = "TxtTINNo";
            this.TxtTINNo.Size = new System.Drawing.Size(56, 20);
            this.TxtTINNo.TabIndex = 6;
            this.TxtTINNo.Visible = false;
            // 
            // TxtCSTNo
            // 
            this.TxtCSTNo.Location = new System.Drawing.Point(771, 304);
            this.TxtCSTNo.MaxLength = 30;
            this.TxtCSTNo.Name = "TxtCSTNo";
            this.TxtCSTNo.Size = new System.Drawing.Size(56, 20);
            this.TxtCSTNo.TabIndex = 5;
            this.TxtCSTNo.Visible = false;
            // 
            // TxtSTNo
            // 
            this.TxtSTNo.Location = new System.Drawing.Point(771, 278);
            this.TxtSTNo.MaxLength = 30;
            this.TxtSTNo.Name = "TxtSTNo";
            this.TxtSTNo.Size = new System.Drawing.Size(56, 20);
            this.TxtSTNo.TabIndex = 4;
            this.TxtSTNo.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(645, 317);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "CST Number";
            this.label10.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(645, 395);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(127, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "VAT Registration Number";
            this.label7.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(645, 369);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "PAN Number";
            this.label4.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(645, 343);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "TIN Number";
            this.label2.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(645, 281);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Service Tax Number";
            this.label1.Visible = false;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn3.HeaderText = "Bank Name";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.ToolTipText = "Name of the Bank and its account number for pay processing and release.";
            this.dataGridViewComboBoxColumn3.Width = 300;
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.StatusLabel,
            this.lblcompanystatus});
            this.bar1.Location = new System.Drawing.Point(0, 567);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(552, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 13;
            this.bar1.TabStop = false;
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Text = "Status:";
            // 
            // lblcompanystatus
            // 
            this.lblcompanystatus.Name = "lblcompanystatus";
            // 
            // BtnOk
            // 
            this.BtnOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnOk.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnOk.Location = new System.Drawing.Point(396, 536);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnOk.TabIndex = 1;
            this.BtnOk.Text = "&OK";
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnCancel.Location = new System.Drawing.Point(476, 536);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnCancel.TabIndex = 2;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnSave.Location = new System.Drawing.Point(6, 536);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnSave.TabIndex = 0;
            this.BtnSave.Text = "&Save";
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // CompanyAccIDD
            // 
            this.CompanyAccIDD.HeaderText = "CompanyAccIDD";
            this.CompanyAccIDD.Name = "CompanyAccIDD";
            this.CompanyAccIDD.Visible = false;
            // 
            // CompanyIDD
            // 
            this.CompanyIDD.HeaderText = "CompanyIDD";
            this.CompanyIDD.Name = "CompanyIDD";
            this.CompanyIDD.Visible = false;
            // 
            // AccountNoo
            // 
            this.AccountNoo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AccountNoo.HeaderText = "Account Number";
            this.AccountNoo.MaxInputLength = 49;
            this.AccountNoo.Name = "AccountNoo";
            this.AccountNoo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ADDFLAG
            // 
            this.ADDFLAG.HeaderText = "ADDFLAG";
            this.ADDFLAG.Name = "ADDFLAG";
            this.ADDFLAG.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "CompanyAccIDD";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "CompanyIDD";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "Account Number";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 49;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "ADDFLAG";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "CompanyAccIDD";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "CompanyIDD";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn7.HeaderText = "Account Number";
            this.dataGridViewTextBoxColumn7.MaxInputLength = 49;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "ADDFLAG";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "CompanyAccIDD";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "CompanyIDD";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn11.HeaderText = "Account Number";
            this.dataGridViewTextBoxColumn11.MaxInputLength = 49;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "ADDFLAG";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // FrmCompany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 586);
            this.Controls.Add(label15);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.TxtVATNo);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.TxtPANNo);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.TxtTINNo);
            this.Controls.Add(this.CompanyTab);
            this.Controls.Add(this.TxtCSTNo);
            this.Controls.Add(this.CompanyMasterBindingNavigator);
            this.Controls.Add(this.TxtSTNo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCompany";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Company Information";
            this.Load += new System.EventHandler(this.FrmCompany_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCompany_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmCompany_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.CompanyMasterBindingNavigator)).EndInit();
            this.CompanyMasterBindingNavigator.ResumeLayout(false);
            this.CompanyMasterBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogoFilePictureBox)).EndInit();
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.CompanyTab.ResumeLayout(false);
            this.CompanyInfoTab.ResumeLayout(false);
            this.CompanyInfoTab.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.GeneralTab.ResumeLayout(false);
            this.GeneralTab.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Accdates.ResumeLayout(false);
            this.Accdates.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdCompanyBanks)).EndInit();
            this.TabLogo.ResumeLayout(false);
            this.TabLogo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator CompanyMasterBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton CompanyMasterBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton CancelToolStripButton;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripButton BtnBank;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.DateTimePicker DtpBookStartDate;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.DateTimePicker DtpFinYearStartDate;
        internal System.Windows.Forms.DataGridViewTextBoxColumn CompanyAccIDD;
        internal System.Windows.Forms.DataGridViewTextBoxColumn CompanyIDD;
        internal System.Windows.Forms.DataGridViewComboBoxColumn ComboBoxBankNameIDD;
        internal System.Windows.Forms.DataGridViewTextBoxColumn AccountNoo;
        internal System.Windows.Forms.DataGridViewTextBoxColumn ADDFLAG;
        internal System.Windows.Forms.ErrorProvider ErrorProviderCompany;
        internal System.Windows.Forms.Timer TmrFocus;
        internal System.Windows.Forms.Timer TmrComapny;
        internal System.Windows.Forms.ToolTip ToolTip1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        internal System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        internal System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        internal System.Windows.Forms.TextBox TxtEmployerCode;
        internal System.Windows.Forms.TextBox TxtShortName;
        internal System.Windows.Forms.TextBox TxtWebSite;
        internal System.Windows.Forms.TextBox TxtBranchName;
        internal System.Windows.Forms.TextBox NameTextBox;
        internal System.Windows.Forms.TextBox TxtPOBox;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.RadioButton RBtnCompany;
        internal System.Windows.Forms.RadioButton RbtnBranch;
        internal System.Windows.Forms.ComboBox CboCountry;
        internal System.Windows.Forms.ComboBox CboCompanyName;
        internal System.Windows.Forms.ComboBox CboCurrency;
        internal System.Windows.Forms.ComboBox CboCompanyIndustry;
        internal System.Windows.Forms.TextBox TxtOtherInfo;
        internal System.Windows.Forms.TextBox TxtStreet;
        internal System.Windows.Forms.TextBox TxtArea;
        internal System.Windows.Forms.TextBox TxtPrimaryEmail;
        internal System.Windows.Forms.TextBox TxtSecondaryEmail;
        internal System.Windows.Forms.TextBox TxtContactPerson;
        internal System.Windows.Forms.TextBox TxtContactPersonPhone;
        internal System.Windows.Forms.TextBox TxtFaxNumber;
        internal System.Windows.Forms.TextBox TxtBlock;
        internal System.Windows.Forms.TextBox TxtCity;
        internal System.Windows.Forms.ComboBox CboDesignation;
        internal System.Windows.Forms.ComboBox CboCompanyType;
        internal System.Windows.Forms.ComboBox CboProvince;
        internal System.Windows.Forms.PictureBox LogoFilePictureBox;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.DateTimePicker DtpFinyearDate;
        internal System.Windows.Forms.TabControl CompanyTab;
        internal System.Windows.Forms.TabPage CompanyInfoTab;
        internal System.Windows.Forms.Label label22;
        internal System.Windows.Forms.Label label23;
        internal System.Windows.Forms.Label LblName;
        internal System.Windows.Forms.TabPage GeneralTab;
        internal System.Windows.Forms.TabPage Accdates;
        internal System.Windows.Forms.TabPage TabLogo;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        internal System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private DemoClsDataGridview.ClsDataGirdView GrdCompanyBanks;
        private System.Windows.Forms.ToolStripButton BtnFinYear;
        private System.Windows.Forms.ToolStripButton BtnComSettings;
        private System.Windows.Forms.TextBox TxtVATNo;
        private System.Windows.Forms.TextBox TxtPANNo;
        private System.Windows.Forms.TextBox TxtTINNo;
        private System.Windows.Forms.TextBox TxtCSTNo;
        private System.Windows.Forms.TextBox TxtSTNo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem StatusLabel;
        private DevComponents.DotNetBar.LabelItem lblcompanystatus;
        private DevComponents.DotNetBar.ButtonX BtnCountry;
        private DevComponents.DotNetBar.ButtonX BtnCompanyType;
        private DevComponents.DotNetBar.ButtonX BtnComIndustry;
        private DevComponents.DotNetBar.ButtonX BtnCurrency;
        private DevComponents.DotNetBar.ButtonX BtnSave;
        private DevComponents.DotNetBar.ButtonX BtnCancel;
        private DevComponents.DotNetBar.ButtonX BtnOk;
        private DevComponents.DotNetBar.ButtonX BtnDesignation;
        private DevComponents.DotNetBar.ButtonX BtnProvince;
        private DevComponents.DotNetBar.ButtonX FilePathButton;
        private DevComponents.DotNetBar.ButtonX BtnRemove;
        internal System.Windows.Forms.DateTimePicker DtpBkStartDate;
        internal System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.RadioButton RBtnYear;
        internal System.Windows.Forms.RadioButton RBtnMonth;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        private NumericTextBox NumTxtWorkingDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyBankId;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyId;
        private System.Windows.Forms.DataGridViewComboBoxColumn BankNameId;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountID;
        internal System.Windows.Forms.ComboBox cboUnearnedPolicyID;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.CheckBox chkMonday;
        internal System.Windows.Forms.CheckBox chkSaturday;
        internal System.Windows.Forms.CheckBox chkTuesDay;
        internal System.Windows.Forms.CheckBox chkFriday;
        internal System.Windows.Forms.CheckBox chkSunday;
        internal System.Windows.Forms.CheckBox chkWednesday;
        internal System.Windows.Forms.CheckBox chkThursday;
        private DevComponents.DotNetBar.ButtonX btnUnearnedPolicy;
        private System.Windows.Forms.TextBox txtTAAN;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtTAN;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtTaxAgentName;
        private System.Windows.Forms.TextBox txtTaxAgencyName;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtTRN;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtTaxablePersonArb;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtTaxablePerson;
        private System.Windows.Forms.Label label44;
    }
}