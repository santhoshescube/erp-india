﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;


/* 
================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,18 Feb 2011>
   Description:	<Description,,Company Form>
================================================
*/

namespace MyBooksERP
{
    public partial class FrmCompany : DevComponents.DotNetBar.Office2007Form
    {

        private string OffDayIds = string.Empty;// for setting the offday ids
        public int PintCompanyID;                     //ID pass from navigation
        public int PintCompany = 0;                       // To set Company Externally
        public bool PblnEmployerBankStatus = false;

        //private bool MblnChangeStatus;                //Check state of the page
        private bool MblnAddStatus;                   //Add/Update mode 
        private bool MblnCorB = true;                  //company or branch from navigation
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission
        private bool MblnShowErrorMess;                //set to Show Error Message or not
        private int MintCurrentRecCnt;                  //Contains Current Record count
        //private bool MblnAddNewMessage = false;

        private int MintRecordCnt;                      //Contains Total Record count
        private int MintTimerInterval;                  // To set timer interval
        private int MintCompanyId;                      // current companyid
        private int MintUserId;                         // current userid
        private int MintPCompanyID = -1;               //Identity for present company selection
        private int MintCurrencyID;                     // Current currency id
        private int MintComboID = 0;                    // To Setting SelectedValue for combo box temporarily
        private string MstrMessageCaption;              //Message caption
        private string MstrMessageCommon;               //to set the message
        private string MstrFilePath;                   //to set the company logo path 
        private MessageBoxIcon MmessageIcon;            // to set the message icon
        private DateTime MdteFinyearDate;              // to set financial year date
        private ArrayList MsarMessageArr;                  // Error Message display
        private ArrayList MsarStatusMessage;              // to set status message
        private List<int> lstDeletedBankIds;           // to set deleted companyAccId From Grid While Updating
        ClsLogWriter mObjLogs;                            
        ClsNotification mObjNotification;
        clsBLLCompanyInformation MobjClsBLLCompanyInformation;
        private bool MblnIsFromOK = true;
        private bool bnlGridRowDelete;

        bool blnIsEditMode = false;
        clsBLLCommonUtility MobjClsBLLCommonUtility = new clsBLLCommonUtility();

        public FrmCompany()
        {
            //Constructor
            InitializeComponent();
            MblnShowErrorMess = true;
            MintCompanyId = ClsCommonSettings.CompanyID;         
            MintUserId = ClsCommonSettings.UserID;            
            MintTimerInterval = ClsCommonSettings.TimerInterval;     
            MstrMessageCaption = ClsCommonSettings.MessageCaption;    
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            MobjClsBLLCompanyInformation = new clsBLLCompanyInformation();
            lstDeletedBankIds = new List<int>();
        }

        private void ComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void FrmCompany_Load(object sender, EventArgs e)
        {
            try
            {
                //Load Function
                LoadCombos(0);
                SetPermissions();
                LoadInitial();
                LoadMessage();
                BindingEnableDisable(sender, e);
                lstDeletedBankIds = new List<int>();

                if (PintCompany > 0)
                {
                    MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID = PintCompany;
                    MintRecordCnt = MobjClsBLLCompanyInformation.RecCountNavigate(ClsCommonSettings.LoginCompanyID);
                    MintCurrentRecCnt = MobjClsBLLCompanyInformation.GetRowNumber();
                    lblcompanystatus.Text = "";
                    TmrFocus.Enabled = true;
                    DisplayCompanyInfo();
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                    SetEnableDisable();
                    SetBindingNavigatorButtons();

                    if (PblnEmployerBankStatus == true)
                        CompanyTab.SelectedTab = Accdates;
                }
                else
                {
                    if (RBtnCompany.Checked == true)
                    {
                        NameTextBox.Enabled = true;
                        NameTextBox.Focus();
                    }
                    else
                        CboCompanyName.Focus();

                    Fillcompanyinfo();                   
                    SetBindingNavigatorButtons();
                    TmrFocus.Enabled = true;
                    CompanyMasterBindingNavigatorSaveItem.Enabled = false;
                    BtnSave.Enabled = false;
                    BtnOk.Enabled = false;
                    BindingNavigatorDeleteItem.Enabled = false;
                    BindingNavigatorAddNewItem.Enabled = false;
                    MblnIsFromOK = true;                    
                }            
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on combobox:LoadCombos " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + ex.Message.ToString());                
            }
        }

        private void tmfocus_Tick(object sender, EventArgs e)
        {
            TmrFocus.Enabled = false;
            NameTextBox.Focus();
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            LoadCombos(2); // Load Current Login Company (Not Branch)
            AddNewCompany();
        }

        private void CompanyMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (CompanyValidation())
                {
                    if (SaveCompany())
                    {
                        MintCurrentRecCnt = MintCurrentRecCnt + 1;
                        BindingNavigatorMovePreviousItem_Click(null, null);
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrFocus.Enabled = true;
                        MblnIsFromOK = true;
                    }
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on CompanyMasterBindingNavigatorSaveItem_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on CompanyMasterBindingNavigatorSaveItem_Click" + ex.Message.ToString());                
            }
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (DeleteValidation())
                {
                    if (MobjClsBLLCompanyInformation.DeleteCompany(Convert.ToInt32(NameTextBox.Tag)))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 4, out MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrFocus.Enabled = true;
                        AddNewCompany();
                    }
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BindingNavigatorDeleteItem_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorDeleteItem_Click" + ex.Message.ToString());             
            }
        }

        private void CancelToolStripButton_Click(object sender, EventArgs e)
        {
            CompanyTab.SelectedTab = CompanyInfoTab;

            if (PintCompanyID < 0)
                BindingEnableDisable(sender, e);
            else
                AddNewCompany();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                LoadReport();
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnPrint_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnPrint_Click" + ex.Message.ToString());                
            }
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Company Information";
                    ObjEmailPopUp.EmailFormType = EmailFormID.Company;
                    ObjEmailPopUp.EmailSource = MobjClsBLLCompanyInformation.GetCompanyReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnEmail_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnEmail_Click" + ex.Message.ToString());                
            }
        }
   
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (CompanyValidation())
                {
                    if (SaveCompany())
                    {
                        MintCurrentRecCnt = MintCurrentRecCnt + 1;
                        BindingNavigatorMovePreviousItem_Click(null, null);
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrFocus.Enabled = true;                      
                    }
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnSave_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnSave_Click" + ex.Message.ToString());                
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (CompanyValidation())
                {
                    if (SaveCompany())
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrFocus.Enabled = true;
                        MblnIsFromOK = true;
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnOk_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnOk_Click" + ex.Message.ToString());
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            try
            {
                MblnAddStatus = false;
                ErrorProviderCompany.Clear();
                MintRecordCnt = MobjClsBLLCompanyInformation.RecCountNavigate(ClsCommonSettings.LoginCompanyID);

                if (MintRecordCnt > 0)
                {
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";
                    MintCurrentRecCnt = 1;
                }
                else
                    MintCurrentRecCnt = 0;

                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9, out MmessageIcon);
                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrFocus.Enabled = true;
                DisplayCompanyInfo();
                BindingNavigatorPositionItem.Text = "1";
                ErrorProviderCompany.Clear();
                SetEnableDisable();
                SetBindingNavigatorButtons();
                MblnIsFromOK = true;
                blnIsEditMode = true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BindingNavigatorMoveFirstItem_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorMoveFirstItem_Click" + ex.Message.ToString());                
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            try
            {
                MintRecordCnt = MobjClsBLLCompanyInformation.RecCountNavigate(ClsCommonSettings.LoginCompanyID);

                if (MintRecordCnt > 0)
                {
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;

                    if (MintCurrentRecCnt >= MintRecordCnt)
                        MintCurrentRecCnt = MintRecordCnt;
                }
                else
                    MintCurrentRecCnt = 0;

                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 11, out MmessageIcon);
                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrFocus.Enabled = true;
                DisplayCompanyInfo();
                BindingNavigatorPositionItem.Text = "1";
                SetEnableDisable();
                SetBindingNavigatorButtons();
                MblnIsFromOK = true;
                blnIsEditMode = true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BindingNavigatorMoveNextItem_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorMoveNextItem_Click" + ex.Message.ToString());                
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            try
            {
                MblnAddStatus = false;
                ErrorProviderCompany.Clear();
                MintRecordCnt = MobjClsBLLCompanyInformation.RecCountNavigate(ClsCommonSettings.LoginCompanyID);

                if (MintRecordCnt > 0)
                {
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";
                    MintCurrentRecCnt = MintRecordCnt;
                }
                else
                    MintCurrentRecCnt = 0;

                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 12, out MmessageIcon);
                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrFocus.Enabled = true;
                DisplayCompanyInfo();
                BindingNavigatorPositionItem.Text = "1";
                SetEnableDisable();
                SetBindingNavigatorButtons();
                MblnIsFromOK = true;
                blnIsEditMode = true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BindingNavigatorMoveLastItem_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorMoveLastItem_Click" + ex.Message.ToString());                
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            try
            {
                MintRecordCnt = MobjClsBLLCompanyInformation.RecCountNavigate(ClsCommonSettings.LoginCompanyID);

                if (MintRecordCnt > 0)
                {
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";
                    MintCurrentRecCnt = MintCurrentRecCnt - 1;

                    if (MintCurrentRecCnt <= 0)
                        MintCurrentRecCnt = 1;
                }
                else
                {
                    BindingNavigatorPositionItem.Text = "of 0";
                    MintRecordCnt = 0;
                }

                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 10, out MmessageIcon);
                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrFocus.Enabled = true;
                DisplayCompanyInfo();
                BindingNavigatorPositionItem.Text = "1";
                SetEnableDisable();
                SetBindingNavigatorButtons();
                blnIsEditMode = true;
                MblnIsFromOK = true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BindingNavigatorMovePreviousItem_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on CompanyMasterBindingNavigatorMovePreviousItem_Click" + ex.Message.ToString());
            }
        }

        private void RbtnBranch_CheckedChanged(object sender, EventArgs e)
        {
            if (RBtnCompany.Checked == true)
            {
                CboCompanyName.Visible = false;
                TxtBranchName.Visible = true;
                ErrorProviderCompany.SetError(RBtnCompany, "");
                CboCompanyName.SelectedIndex = -1;
                NameTextBox.Focus();
                CboCompanyName.Text = "";
                TxtBranchName.Location = new Point(145, 87);
                TxtBranchName.Visible = true;
                LblName.Enabled = false;
        
                if (MblnAddStatus == true)
                    NameTextBox.Text = "";

                NameTextBox.Visible = true;
                NameTextBox.Location = new Point(146, 59);               
            }
            else
            {
                TxtBranchName.Visible = false;
                CboCompanyName.Visible = true;
                ErrorProviderCompany.SetError(RBtnCompany, "");
                NameTextBox.Visible = true;
                NameTextBox.Location = new Point(145, 87);
                CboCompanyName.Focus();
                TxtBranchName.Visible = false;
                LblName.Enabled = true;

                if (MblnAddStatus == true)
                    NameTextBox.Text = "";

                LoadCombos(2);
            }

            if (RBtnCompany.Checked == true)
            {
                CboCompanyName.SelectedIndex = -1;
                CboCompanyName.Text = "";
                CboCompanyName.Visible = false;
                NameTextBox.Visible = true;
                NameTextBox.Location = new Point(146, 59);
            }
            else
                CboCompanyName.Visible = true;  
        }

        private void BtnRemove_Click(object sender, EventArgs e)
        {
            MstrFilePath = "";
            LogoFilePictureBox.Image = null;
            Changestatus();
        }

        private void FilePathButton_Click(object sender, EventArgs e)
        {
            try
            {
                Image ImageFile;
                OpenFileDialog Dlg;
                Dlg = new OpenFileDialog();
                Dlg.Filter = "Images (*.jpg;*.jpeg)|*.jpg;*.jpeg";
                Dlg.ShowDialog();
                MstrFilePath = Dlg.FileName;

                if (Convert.ToString(MstrFilePath) != "")
                {
                    if (System.IO.File.Exists(MstrFilePath) == true)
                        ImageFile = Image.FromFile(MstrFilePath.ToString());

                    LogoFilePictureBox.Image = Image.FromFile(MstrFilePath.ToString());
                    ResizeEmpImage();
                }

                Changestatus();
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on FilePathButton_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FilePathButton_Click" + ex.Message.ToString());
            }
        }

        private void DtpFinyearDate_ValueChanged(object sender, EventArgs e)
        {
            if (MblnAddStatus == true)
                DtpBkStartDate.Value = DtpFinyearDate.Value;

            Changestatus();
        }

        private void GrdCompanyBanks_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (GrdCompanyBanks.IsCurrentCellDirty)
            {
                if (GrdCompanyBanks.CurrentCell != null)
                    GrdCompanyBanks.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void GrdCompanyBanks_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                int iRowIndex = GrdCompanyBanks.CurrentCell.RowIndex;

                if (e.ColumnIndex == 3)
                {
                    if (GrdCompanyBanks.CurrentRow.Cells[2].Value == null)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 202,out MmessageIcon );
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        GrdCompanyBanks.CurrentCell = GrdCompanyBanks[2, iRowIndex];
                        e.Cancel = true;
                    }
                }

                if (GrdCompanyBanks.Rows.Count >= 2)
                {
                    if (e.ColumnIndex == 2)
                    {
                        int iCurRowIndex = GrdCompanyBanks.CurrentRow.Index - 1;
                        if (iCurRowIndex >= 0)
                        {
                            if (Convert.ToString(GrdCompanyBanks.Rows[iCurRowIndex].Cells[3].Value).Trim() == "")
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 203,out MmessageIcon );
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                GrdCompanyBanks.CurrentCell = GrdCompanyBanks[3, iCurRowIndex];
                                e.Cancel = true;
                            }
                        }
                    }
                }
            }
           
            if (MblnAddStatus != true)
            {
                if (GrdCompanyBanks.CurrentRow != null)
                {
                    if (GrdCompanyBanks.CurrentRow.Cells["CompanyBankId"].Value.ToInt32() > 0)
                    {
                        if (MobjClsBLLCompanyInformation.GetBankBranchExistsInEmployee(GrdCompanyBanks.CurrentRow.Cells["BankNameId"].Value.ToInt32()))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 234, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrComapny.Enabled = true;
                            e.Cancel = true;
                            return;
                        }
                    }
                }

                Changestatus();
            }
        }

        private void BtnCountry_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(CboCountry.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Country", new int[] { 1, 0, MintTimerInterval }, "CountryID,CountryName As Country", "CountryReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                LoadCombos(4);

                if (objCommon.NewID != 0)
                    CboCountry.SelectedValue = objCommon.NewID;
                else
                    CboCountry.SelectedValue = MintComboID;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnCountry_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnCountry_Click" + ex.Message.ToString());                
            }
        }

        private void CboComIndustry_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(CboCompanyIndustry.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Company Industry", new int[] { 1, 0, MintTimerInterval }, "CompanyIndustryID,CompanyIndustry As [Company Industry]", "IndustryReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                LoadCombos(1);

                if (objCommon.NewID != 0)
                    CboCompanyIndustry.SelectedValue = objCommon.NewID;
                else
                    CboCompanyIndustry.SelectedValue = MintComboID;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on CboComIndustry_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on CboComIndustry_Click" + ex.Message.ToString());
            }
        }

        private void BtnProvince_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(CboProvince.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Province/State", new int[] { 1, 0, MintTimerInterval }, "ProvinceID,ProvinceName As Province", "ProvinceReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                LoadCombos(7);

                if (objCommon.NewID != 0)
                    CboProvince.SelectedValue = objCommon.NewID;
                else
                    CboProvince.SelectedValue = MintComboID;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnProvince_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnProvince_Click" + ex.Message.ToString());
            }
        }

        private void BtnCompanyType_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(CboCompanyType.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Company Type", new int[] { 1, 0, MintTimerInterval }, "CompanyTypeID,CompanyType As CompanyType", "CompanyTypeReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                LoadCombos(3);

                if (objCommon.NewID != 0)
                    CboCompanyType.SelectedValue = objCommon.NewID;
                else
                    CboCompanyType.SelectedValue = MintComboID;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnCompanyType_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnCompanyType_Click" + ex.Message.ToString());
            }
        }

        private void BtnDesignation_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(CboDesignation.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Designation", new int[] { 1, 0, MintTimerInterval }, "DesignationID,Designation As Designation", "DesignationReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                LoadCombos(6);

                if (objCommon.NewID != 0)
                    CboDesignation.SelectedValue = objCommon.NewID;
                else
                    CboDesignation.SelectedValue = MintComboID;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnDesignation_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnDesignation_Click" + ex.Message.ToString());
            }
        }

        private void BtnBank_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmBankDetails objBank = new FrmBankDetails(0))
                {
                    objBank.ShowDialog();
                }

                LoadCombos(8);
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on  BtnBank_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnBank_Click" + ex.Message.ToString());
            }
        }

        private void BtnCurrency_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(CboCurrency.SelectedValue);

                using (FrmCurrencyReference objCur = new FrmCurrencyReference())
                {
                    objCur.PintCurrency = Convert.ToInt32(CboCurrency.SelectedValue);
                    objCur.ShowDialog();
                }

                LoadCombos(5);
                CboCurrency.SelectedValue = MintComboID;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnCurrency_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnCurrency_Click" + ex.Message.ToString());

            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------- Functions ------------------------------------------------------------- 

        private void Fillcompanyinfo()
        {
            try
            {
                if (Convert.ToInt32(MintPCompanyID) > 0)
                {
                    int ComID;
                    ComID = Convert.ToInt32(NameTextBox.Tag);
                    DisplayCompanyInfo();
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintPCompanyID);
                    MintCurrentRecCnt = Convert.ToInt32(MintPCompanyID);
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on FillCompanyInfo() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FillCompanyInfo() " + ex.Message.ToString());
            }
        }

        private void DisplayCompanyInfo()
        {
            try
            {
                int ParentID = 0;
                bool blnIsExists = false;
                blnIsExists = MobjClsBLLCompanyInformation.DisplayCompanyInfo(MintCurrentRecCnt, ClsCommonSettings.LoginCompanyID);

                if (blnIsExists)
                {
                    NameTextBox.Tag = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID;                    
                    
                    if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.blnCompanyBranchIndicator == true)
                    {
                        RBtnCompany.Checked = true;
                        RbtnBranch.Checked = false;
                    }
                    else
                    {
                        RbtnBranch.Checked = true;
                        RBtnCompany.Checked = false;
                    }
                                        
                    ParentID = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intParentID;

                    if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.blnCompanyBranchIndicator == false)
                    {
                        RbtnBranch.Checked = true;
                        CboCompanyName.SelectedValue = ParentID;

                        // If user Login into Branch
                        if (CboCompanyName.SelectedIndex == -1)
                            LoadParentCompany(ParentID);
                    }
                    else
                    {
                        RBtnCompany.Checked = true;
                        CboCompanyName.SelectedIndex = -1;
                    }

                    if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strFinYearStartDate != null)
                    {
                        DtpFinyearDate.Value = Convert.ToDateTime(MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strFinYearStartDate);
                        MdteFinyearDate = DtpFinyearDate.Value.Date;
                    }

                    if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strBookStartDate != null)
                        DtpBkStartDate.Value = Convert.ToDateTime(MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strBookStartDate);

                    NameTextBox.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strName;
                    //VALIDATION TO PREVENT UPDATING BOOKSTARTDATE,IF OPENINGSTOCK IS CREATED AGANIST THIS COMPANY'S WAREHOUSE
                    DtpBkStartDate.Enabled = true; ;
                    DataTable datTemp = MobjClsBLLCompanyInformation.FillCombos(new string[] { "Count(1) as [Count]", "CompanyMaster CM INNER JOIN InvWarehouse WH ON CM.CompanyID=WH.CompanyID INNER JOIN InvOpeningStock STOS ON STOS.WarehouseID=WH.WarehouseID", "CM.CompanyID = " + MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID });
                    
                    if (datTemp.Rows.Count > 0)
                    {
                        if (datTemp.Rows[0]["Count"].ToInt32() > 0)
                            DtpBkStartDate.Enabled = false;
                    }

                    TxtEmployerCode.Text = Convert.ToString(MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strEPID);
                    TxtShortName.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strShortName;
                    TxtPOBox.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strPOBox;
                    CboCountry.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCountryID;
                    CboCurrency.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCurrencyId;
                    MintCurrencyID = Convert.ToInt32(CboCurrency.SelectedValue);
                    CboCompanyIndustry.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyIndustryID;
                    CboCompanyType.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyTypeID;
                    CboProvince.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intProvinceID;
                    TxtArea.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strArea;
                    TxtBlock.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strBlock;
                    TxtStreet.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strRoad;
                    TxtCity.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strCity;
                    TxtPrimaryEmail.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strPrimaryEmail;
                    TxtSecondaryEmail.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strSecondaryEmail;
                    TxtContactPerson.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strContactPerson;
                    CboDesignation.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intContactPersonDesignationID;
                    TxtContactPersonPhone.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strContactPersonPhone;
                    TxtFaxNumber.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strPABXNumber;
                    TxtWebSite.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strWebSite;
                    TxtOtherInfo.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strOtherInfo;
                    TxtSTNo.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strCompanyServiceTaxNo;
                    TxtCSTNo.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strCompanyCSTNo;
                    TxtTINNo.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strCompanyTINNo;
                    TxtPANNo.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strCompanyPANNo;
                    TxtVATNo.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strVATRegistrationNo;
                    

                    ////vat Details
                    txtTaxablePerson.Text=MobjClsBLLCompanyInformation.clsDTOCompanyInformation.TaxablePersonName;
                    txtTaxablePersonArb.Text= MobjClsBLLCompanyInformation.clsDTOCompanyInformation.TaxablePersonNameArb;
                    txtTRN.Text=MobjClsBLLCompanyInformation.clsDTOCompanyInformation.TRN;
                    txtTaxAgencyName.Text=MobjClsBLLCompanyInformation.clsDTOCompanyInformation.TaxAgencyName;
                    txtTAN.Text=MobjClsBLLCompanyInformation.clsDTOCompanyInformation.TAN;
                    txtTaxAgentName.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.TaxAgentName;
                    txtTAAN.Text=MobjClsBLLCompanyInformation.clsDTOCompanyInformation.TAAN;


                    if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.blnIsMonth == true)
                    {
                        RBtnMonth.Checked = true;
                        RBtnYear.Checked = false;
                    }
                    else
                    {
                        RBtnYear.Checked = true;
                        RBtnMonth.Checked = false;
                    }

                    NumTxtWorkingDays.Text = Convert.ToString(MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intWorkingDaysInMonth);
                    //cboDays.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intOffDayID;
                    cboUnearnedPolicyID.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intUnernedPolicyID;
                    if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.byteLogoFile != null)
                    {
                        ClsImages objimg = new ClsImages();
                        LogoFilePictureBox.Image = objimg.GetImage(MobjClsBLLCompanyInformation.clsDTOCompanyInformation.byteLogoFile);
                    }
                    else
                        LogoFilePictureBox.Image = null;

                    CboCurrency.Enabled = true;

                    if (MobjClsBLLCompanyInformation.CheckCompanyCurrencyUsed(MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID, CboCurrency.SelectedValue.ToInt32()))
                        CboCurrency.Enabled = false;

                    SetCheckBoxes("," + MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strOffDayIDs);

                    DisplayCompanyBankInfo();
                    BtnPrint.Enabled = MblnPrintEmailPermission;
                    BtnEmail.Enabled = MblnPrintEmailPermission;
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on DisplayCompanyInfo() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DisplayCompanyInfo()" + ex.Message.ToString());
            }
        }

        private void DisplayCompanyBankInfo()
        {
            try
            {
                GrdCompanyBanks.Rows.Clear();
                DataTable DtCompanyBank = MobjClsBLLCompanyInformation.DisplayBankInformation(Convert.ToInt32(NameTextBox.Tag));

                if (DtCompanyBank.Rows.Count > 0)
                {
                    for (int i = 0; i < DtCompanyBank.Rows.Count; i++)
                    {
                        GrdCompanyBanks.RowCount = GrdCompanyBanks.RowCount + 1;
                        GrdCompanyBanks.Rows[i].Cells[0].Value = DtCompanyBank.Rows[i]["CompanyBankAccountID"];
                        GrdCompanyBanks.Rows[i].Cells[1].Value = DtCompanyBank.Rows[i]["CompanyID"];
                        GrdCompanyBanks.Rows[i].Cells[2].Value = DtCompanyBank.Rows[i]["BankBranchID"];
                        GrdCompanyBanks.Rows[i].Cells[3].Value = DtCompanyBank.Rows[i]["BankAccountNo"];
                    }
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on DisplayCompanyBankInfo() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DisplayCompanyBankInfo()" + ex.Message.ToString());

            }
        }

        private void LoadInitial()
        {
            GrdCompanyBanks.ShowCellToolTips = false;
            this.ToolTip1.ShowAlways = true;
            this.ToolTip1.ReshowDelay = 2;
            this.ToolTip1.AutomaticDelay = 300;
            ToolTip1.SetToolTip(GrdCompanyBanks, "Name of the Bank and its account number for pay processing and release. \nYou can enter multiple bank names from which the pay can be released.");
            ErrorProviderCompany.Clear();
            lblcompanystatus.Text = "";
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            NameTextBox.Focus();
            MblnAddStatus = false;
        }
        
        private void LoadMessage()
        {
            try
            {
                MsarMessageArr = new ArrayList();
                MsarStatusMessage = new ArrayList();
                MsarMessageArr = mObjNotification.FillMessageArray((int)FormID.CompanyInformation, ClsCommonSettings.ProductID);
                MsarStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.CompanyInformation, ClsCommonSettings.ProductID);
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on LoadMessage() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadMessage()" + ex.Message.ToString());
            }
        }
        
        private void BindingEnableDisable(Object sender, EventArgs e)
        {
            try
            {
                MintRecordCnt = MobjClsBLLCompanyInformation.RecCountNavigate(ClsCommonSettings.LoginCompanyID);

                if (MintRecordCnt > 0)
                {
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt + 1) + "";
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
                    MintCurrentRecCnt = MintRecordCnt + 1;
                }
                else
                {
                    BindingNavigatorCountItem.Text = "of 0";
                    MintCurrentRecCnt = 0;
                }

                if (MintPCompanyID <= 0)
                {
                    if (MintRecordCnt == 0)
                    {
                        AddNewCompany();
                        this.BindingNavigatorPositionItem.Enabled = this.BindingNavigatorMovePreviousItem.Enabled = false;
                        this.BindingNavigatorMoveFirstItem.Enabled = this.BindingNavigatorMoveNextItem.Enabled = false;
                        this.BindingNavigatorMoveLastItem.Enabled = false;
                    }
                    else
                    {
                        this.BindingNavigatorPositionItem.Enabled = true;
                        this.BindingNavigatorMovePreviousItem.Enabled = true;
                        this.BindingNavigatorMoveFirstItem.Enabled = this.BindingNavigatorMoveNextItem.Enabled = true;
                        this.BindingNavigatorMoveLastItem.Enabled = true;                        
                        AddNewCompany();
                    }
                }
                else
                {
                    this.BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                    this.BindingNavigatorAddNewItem.Visible = true;
                }

                CompanyTab.SelectedTab = CompanyInfoTab;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BindingEnableDisable() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BindingEnableDisable()" + ex.Message.ToString());
            }
        }

        private void AddNewCompany()
        {
            try
            {
                MobjClsBLLCompanyInformation = new clsBLLCompanyInformation();
                lstDeletedBankIds = new List<int>();
                CboCurrency.Enabled = true;
                ClearControls();
                MblnAddStatus = true;
                BindingNavigatorAddNewItem.Enabled = false;
                CompanyTab.SelectedTab = CompanyInfoTab;
                CompanyInfoTab.Focus();
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 204, out MmessageIcon);
                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrComapny.Enabled = true;
                BindingNavigatorDeleteItem.Enabled = false;
                ErrorProviderCompany.Clear();
                BindingNavigatorAddNewItem.Enabled = false;
                MintRecordCnt = MobjClsBLLCompanyInformation.RecCountNavigate(ClsCommonSettings.LoginCompanyID);

                if (MintRecordCnt > 0)
                {
                    BindingNavigatorMovePreviousItem.Enabled = true;
                    BindingNavigatorMoveFirstItem.Enabled = true;
                    BindingNavigatorMoveNextItem.Enabled = true;
                    BindingNavigatorMoveLastItem.Enabled = true;
                   
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt + 1) + "";
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
                    MintCurrentRecCnt = MintRecordCnt + 1;
                }
                else
                {
                    BindingNavigatorCountItem.Text = " of 1";
                    BindingNavigatorPositionItem.Text = Convert.ToString(1);
                    MintCurrentRecCnt = 1;
                }

                string StrDate;
                DateTime DateDate;
                NumTxtWorkingDays.Text = "365";
                StrDate = "01/" + GetCurrentMonth(ClsCommonSettings.GetServerDate().Month) + "/" + ClsCommonSettings.GetServerDate().Year;
                DateDate = Convert.ToDateTime(StrDate);
                DtpFinyearDate.Value = DateDate;
                DtpBkStartDate.Value = DateDate;
                DtpBkStartDate.Enabled = true;
                NameTextBox.Focus();
                BtnOk.Enabled = false;
                BtnSave.Enabled = false;
                CompanyMasterBindingNavigatorSaveItem.Enabled = false;
                BtnPrint.Enabled = false;
                BtnEmail.Enabled = false;
                BtnFinYear.Enabled = false;
                GrdCompanyBanks.Rows.Clear();
                RbtnBranch.Checked = true;
                RBtnCompany.Checked = false;
                SetBindingNavigatorButtons();
                blnIsEditMode = false;
                MblnIsFromOK = true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on AddNewCompany() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewCompany()" + ex.Message.ToString());
            }
        }

        private void ClearControls()
        {
            ErrorProviderCompany.Clear();
            lblcompanystatus.Text = "";
            CboCompanyName.SelectedIndex = -1;
            CboCompanyName.Text = "";
            NameTextBox.Text = "";
            NameTextBox.Tag = "0";
            TxtEmployerCode.Text = TxtShortName.Text = TxtPOBox.Text = TxtWebSite.Text = "";
            CboCountry.SelectedIndex = -1;
            cboUnearnedPolicyID.SelectedIndex = CboCurrency.SelectedIndex = -1;
            CboCompanyType.Text = "";
            CboCompanyIndustry.SelectedIndex = -1;
            CboCountry.Text = CboCurrency.Text = CboCompanyIndustry.Text = "";
            TxtArea.Text = TxtBlock.Text = TxtStreet.Text = TxtCity.Text = "";
            CboProvince.Text = "";
            CboProvince.SelectedIndex = -1;
            CboCompanyType.SelectedIndex = -1;
            TxtPrimaryEmail.Text =  TxtSecondaryEmail.Text =  TxtContactPerson.Text = "";
            TxtContactPersonPhone.Text =  TxtFaxNumber.Text =  TxtOtherInfo.Text = "";
            CboDesignation.Text = "";
            CboDesignation.SelectedIndex = -1;
            NumTxtWorkingDays.Text = "";
            chkSunday.Checked = chkMonday.Checked = chkTuesDay.Checked = chkWednesday.Checked = chkThursday.Checked = chkFriday.Checked = chkSaturday.Checked = false;
            //cboDays.SelectedIndex = 1;
            DtpFinyearDate.Value = ClsCommonSettings.GetServerDate();
            DtpBkStartDate.Value = ClsCommonSettings.GetServerDate();
            LogoFilePictureBox.Image = null;
            GrdCompanyBanks.ClearSelection();
            TxtSTNo.Text = TxtTINNo.Text = TxtVATNo.Text = TxtPANNo.Text = TxtCSTNo.Text = "";
            RBtnYear.Checked = true;
            txtTaxablePerson.Text = txtTaxablePersonArb.Text = txtTRN.Text = txtTaxAgencyName.Text = txtTAN.Text = txtTaxAgentName.Text = txtTAAN.Text = "";

        }
        private void SetCheckBoxes(string dayIds)
        {
            chkSunday.Checked = dayIds.Contains(Convert.ToString(chkSunday.Tag));
            chkMonday.Checked = dayIds.Contains(Convert.ToString(chkMonday.Tag));
            chkTuesDay.Checked = dayIds.Contains(Convert.ToString(chkTuesDay.Tag));
            chkWednesday.Checked = dayIds.Contains(Convert.ToString(chkWednesday.Tag));
            chkThursday.Checked = dayIds.Contains(Convert.ToString(chkThursday.Tag));
            chkFriday.Checked = dayIds.Contains(Convert.ToString(chkFriday.Tag));
            chkSaturday.Checked = dayIds.Contains(Convert.ToString(chkSaturday.Tag));
        }
        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";
        
            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }
            return Months;
        }

        private void SetEnableDisable()
        {
            ErrorProviderCompany.Clear();
            CompanyTab.SelectedTab = CompanyInfoTab;
            BtnFinYear.Enabled = TmrComapny.Enabled = true;
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            BtnPrint.Enabled = MblnPrintEmailPermission;
            BtnEmail.Enabled = MblnPrintEmailPermission;
        
            if (MintCurrentRecCnt >= 1)
                BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;

            CompanyMasterBindingNavigatorSaveItem.Enabled = false;
            BtnOk.Enabled = false;
            BtnSave.Enabled = false;
            MblnAddStatus = false;
        }

        private void ResizeEmpImage()
        {
            Bitmap bm = new Bitmap(LogoFilePictureBox.Image);
            int width = 192;
            int height = 96;
            Bitmap thumb = new Bitmap(width, height);
            Graphics g;
            g = Graphics.FromImage(thumb);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.DrawImage(bm, new Rectangle(0, 0, width, height), new Rectangle(0, 0, bm.Width, bm.Height), GraphicsUnit.Pixel);
            LogoFilePictureBox.Image = thumb;
            g.Dispose();
            bm.Dispose();
        }

        private bool DeleteValidation()
        {
            try
            {   
                if (MobjClsBLLCompanyInformation.GetCompanyExists(Convert.ToInt32(NameTextBox.Tag)))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 226, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    return false;
                }

                if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID == 1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 230, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    return false;
                }

                for (int i = 0; i < GrdCompanyBanks.RowCount - 1; i++)
                {
                    if (MobjClsBLLCompanyInformation.GetBankBranchExistsInEmployee(GrdCompanyBanks.Rows[i].Cells["BankNameId"].Value.ToInt32()))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 226, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        return false;
                    }
                }

                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 13, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on DeleteValidation() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteValidation()" + ex.Message.ToString());
                return false;
            }
        }

        private bool CompanyValidation()
        {
            try
            {
                string strComNameOld = "";
                string strComNameNew = "";
                ErrorProviderCompany.Clear();
                lblcompanystatus.Text = "";

                if (RBtnCompany.Checked == false)
                {
                    if (Convert.ToInt32(CboCompanyName.SelectedIndex) == -1)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 201, out MmessageIcon);
                        ErrorProviderCompany.SetError(CboCompanyName, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = CompanyInfoTab;
                        CboCompanyName.Focus();
                        return false;
                    }
                }

                if (RbtnBranch.Checked == true)
                {
                    //strComBranch = "Branch";
                    if (NameTextBox.Text.Trim() == "")
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 205, out MmessageIcon);
                        ErrorProviderCompany.SetError(NameTextBox, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = CompanyInfoTab;
                        NameTextBox.Focus();
                        return false;
                    }
                }
                else
                {
                    //strComBranch = "Company";
                    if (NameTextBox.Text.Trim() == "")
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 201, out MmessageIcon);
                        ErrorProviderCompany.SetError(CboCompanyName, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = CompanyInfoTab;
                        CboCompanyName.Focus();
                        return false;
                    }
                }

                strComNameOld = NameTextBox.Text.Trim();
                strComNameNew = NameTextBox.Text.Trim();

                if (MobjClsBLLCompanyInformation.CheckDuplication(MblnAddStatus, new string[] { NameTextBox.Text.Replace("'", "").Trim() }, Convert.ToInt32(NameTextBox.Tag), 1))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 206, out MmessageIcon);
                    ErrorProviderCompany.SetError(NameTextBox, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = CompanyInfoTab;
                    NameTextBox.Focus();
                    return false;
                }

                if (TxtEmployerCode.Text.Trim() == "")
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 207, out MmessageIcon);
                    ErrorProviderCompany.SetError(TxtEmployerCode, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = CompanyInfoTab;
                    TxtEmployerCode.Focus();
                    return false;
                }

                if (TxtShortName.Text.Trim() == "")
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 208, out MmessageIcon);
                    ErrorProviderCompany.SetError(TxtShortName, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = CompanyInfoTab;
                    TxtShortName.Focus();
                    return false;
                }

                if (TxtPOBox.Text.Trim() == "")
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 209, out MmessageIcon);
                    ErrorProviderCompany.SetError(TxtPOBox, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = CompanyInfoTab;
                    TxtPOBox.Focus();
                    return false;
                }

                if (CboCountry.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 5, out MmessageIcon);
                    ErrorProviderCompany.SetError(CboCountry, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = CompanyInfoTab;
                    CboCountry.Focus();
                    return false;
                }

                if (CboCurrency.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 6, out MmessageIcon);
                    ErrorProviderCompany.SetError(CboCurrency, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = CompanyInfoTab;
                    CboCurrency.Focus();
                    return false;
                }

                if (CboCompanyIndustry.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 210, out MmessageIcon);
                    ErrorProviderCompany.SetError(CboCompanyIndustry, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = CompanyInfoTab;
                    CboCompanyIndustry.Focus();
                    return false;
                }

                if (OffDayIds.Length > 12)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 241, out MmessageIcon);
                    ErrorProviderCompany.SetError(groupBox1, MstrMessageCommon.Replace("#", "").Trim());
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = GeneralTab;
                    groupBox1.Focus();
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return false;
                }

                if (RBtnMonth.Checked == true)
                {
                    if ((Convert.ToInt32(NumTxtWorkingDays.Text) > 31))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 227, out MmessageIcon);
                        ErrorProviderCompany.SetError(NumTxtWorkingDays, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = CompanyInfoTab;
                        NumTxtWorkingDays.Focus();
                        return false;
                    }
                }
                else
                {
                    if (DateTime.IsLeapYear(System.DateTime.Now.Year)==true)
                    {
                        if ((Convert.ToInt32(NumTxtWorkingDays.Text) > 366) || (Convert.ToInt32(NumTxtWorkingDays.Text) < 100))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 228, out MmessageIcon);
                            ErrorProviderCompany.SetError(NumTxtWorkingDays, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrComapny.Enabled = true;
                            CompanyTab.SelectedTab = CompanyInfoTab;
                            NumTxtWorkingDays.Focus();
                            return false;
                        }
                        else
                        {
                            if ((Convert.ToInt32(NumTxtWorkingDays.Text) > 365) || (Convert.ToInt32(NumTxtWorkingDays.Text) < 100))
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 229, out MmessageIcon);
                                ErrorProviderCompany.SetError(NumTxtWorkingDays, MstrMessageCommon.Replace("#", "").Trim());
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrComapny.Enabled = true;
                                CompanyTab.SelectedTab = CompanyInfoTab;
                                NumTxtWorkingDays.Focus();
                                return false;
                            }
                        }
                    }
                }

                if (CboProvince.SelectedIndex == -1)
                {
                    if (CboProvince.Text.Trim() != "")
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 211, out MmessageIcon);
                        ErrorProviderCompany.SetError(CboProvince, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = GeneralTab;
                        CboProvince.Focus();
                        return false;
                    }
                }

                if (CboCompanyType.SelectedIndex == -1)
                {
                    if (CboCompanyType.Text != "")
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 212, out MmessageIcon);
                        ErrorProviderCompany.SetError(CboCompanyType, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = CompanyInfoTab;
                        CboCompanyType.Focus();
                        return false;
                    }
                }

                if (TxtPrimaryEmail.Text != "")
                {
                    if (MobjClsBLLCompanyInformation.CheckValidEmail(Convert.ToString(TxtPrimaryEmail.Text.Trim())) == false)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 213, out MmessageIcon);
                        ErrorProviderCompany.SetError(TxtPrimaryEmail, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = GeneralTab;
                        TxtPrimaryEmail.Focus();
                        return false;
                    }
                }

                if (TxtSecondaryEmail.Text != "")
                {
                    if (MobjClsBLLCompanyInformation.CheckValidEmail(Convert.ToString(TxtSecondaryEmail.Text.Trim())) == false)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 213, out MmessageIcon);
                        ErrorProviderCompany.SetError(TxtSecondaryEmail, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = GeneralTab;
                        TxtSecondaryEmail.Focus();
                        return false;
                    }
                }

                if (TxtPrimaryEmail.Text != "" && TxtSecondaryEmail.Text != "")
                {
                    if (TxtPrimaryEmail.Text.Trim() == TxtSecondaryEmail.Text.Trim())
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 214, out MmessageIcon);
                        ErrorProviderCompany.SetError(CboCompanyType, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = GeneralTab;
                        TxtSecondaryEmail.Focus();
                        return false;
                    }
                }

                if (CboDesignation.SelectedIndex == -1)
                {
                    if (CboDesignation.Text != "")
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7, out MmessageIcon);
                        ErrorProviderCompany.SetError(CboDesignation, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = GeneralTab;
                        CboDesignation.Focus();
                        return false;
                    }
                }

                if (MblnAddStatus == false)
                
                if (Convert.ToInt32(NameTextBox.Tag) != 0 && Convert.ToInt32(CboCompanyName.SelectedValue) != 0)
                {
                    if (Convert.ToInt32(NameTextBox.Tag) == Convert.ToInt32(CboCompanyName.SelectedValue))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 216, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        CboCompanyName.Focus();
                        return false;
                    }
                }

                if (RbtnBranch.Checked == true)
                {
                    DateTime dBSDate;
                    if (MobjClsBLLCompanyInformation.CheckDBSdateExists(new string[] { Convert.ToString(CboCompanyName.SelectedValue) }, out  dBSDate))
                    {
                        if (DtpBkStartDate.Value.Date < dBSDate)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 217, out MmessageIcon);
                            ErrorProviderCompany.SetError(DtpBkStartDate, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrComapny.Enabled = true;
                            CompanyTab.SelectedTab = Accdates;
                            DtpBkStartDate.Focus();
                            return false;
                        }
                    }
                }

                if (DtpFinyearDate.Value.Day != 1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 219, out MmessageIcon);
                    ErrorProviderCompany.SetError(DtpFinyearDate, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = Accdates;
                    DtpFinyearDate.Focus();
                    return false;
                }

                if (DtpBkStartDate.Value.Date < DtpFinyearDate.Value.Date)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 220, out MmessageIcon);
                    ErrorProviderCompany.SetError(DtpBkStartDate, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = Accdates;
                    DtpBkStartDate.Focus();
                    return false;
                }

                // -------------------------------Company bank Account Details Validation---------------------------------------
                int iTempID = 0;
                iTempID = CheckDuplicationInGrid(GrdCompanyBanks, 2, 3);

                if (iTempID != -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 222, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    CompanyTab.SelectedTab = Accdates;
                    GrdCompanyBanks.Focus();
                    GrdCompanyBanks.CurrentCell = GrdCompanyBanks[2, iTempID];
                    return false;
                }

                for (int i = 0; i < GrdCompanyBanks.RowCount - 1; i++)
                {
                    int iRowIndex = GrdCompanyBanks.Rows[i].Index;

                    if (Convert.ToString(GrdCompanyBanks.Rows[i].Cells[2].Value).Trim() == "")
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 202, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        CompanyTab.SelectedTab = Accdates;
                        GrdCompanyBanks.Focus();
                        GrdCompanyBanks.CurrentCell = GrdCompanyBanks[2, iRowIndex];
                        return false;
                    }

                    if (Convert.ToString(GrdCompanyBanks.Rows[i].Cells[3].Value).Trim() == "")
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 203, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        CompanyTab.SelectedTab = Accdates;
                        GrdCompanyBanks.Focus();
                        GrdCompanyBanks.CurrentCell = GrdCompanyBanks[3, iRowIndex];
                        return false;
                    }

                    int iReturnID = 0;

                    if (MobjClsBLLCompanyInformation.CheckAccountNoExists(MblnAddStatus, new string[] { Convert.ToString(GrdCompanyBanks.Rows[i].Cells["BankNameId"].Value), Convert.ToString(GrdCompanyBanks.Rows[i].Cells["AccountNumber"].Value).Trim() }, Convert.ToInt32(NameTextBox.Tag), out iReturnID))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 223, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        CompanyTab.SelectedTab = Accdates;
                        GrdCompanyBanks.Focus();
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on CompanyValidation() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on CompanyValidation()" + ex.Message.ToString());
                return false;
            }
        }

        private bool SaveCompany()
        {
            // function for saving company information
            try
            {
                if (RBtnCompany.Checked == true)
                {
                    if (MblnAddStatus == true)
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 224,out MmessageIcon );
                    else
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3,out MmessageIcon );
                }
                else
                {
                    if (MblnAddStatus == true)
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 225,out MmessageIcon );
                    else
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3,out MmessageIcon );
                }
                
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;

                if (RbtnBranch.Checked)
                {
                    if (MobjClsBLLCompanyInformation.FillCombos(new string[] { "1", "CurrencyDetails", "CompanyID = " + CboCompanyName.SelectedValue.ToInt32() + " And CurrencyID = " + CboCurrency.SelectedValue.ToInt32() }).Rows.Count == 0)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 233, out MmessageIcon).Replace("*", CboCurrency.Text);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                     
                        using (FrmCurrency frmCurrency = new FrmCurrency())
                        {
                           frmCurrency.ShowDialog();
                        }

                        if (MobjClsBLLCompanyInformation.FillCombos(new string[] { "1", "CurrencyDetails", "CompanyID = " + CboCompanyName.SelectedValue.ToInt32() + " And CurrencyID = " + CboCurrency.SelectedValue.ToInt32() }).Rows.Count == 0)
                        {
                            CompanyTab.SelectedTab = CompanyInfoTab;
                            CboCurrency.Focus();
                            return false;
                        }
                    }                   
                }


                FillParameters();
                FillDetailParameters();

                if (MobjClsBLLCompanyInformation.SaveCompany(MblnAddStatus,lstDeletedBankIds))
                {
                    NameTextBox.Tag = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID;
                    mObjLogs.WriteLog("Saved successfully:SaveCompany()  " + this.Name + "", 0);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on Save:SaveCompany() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (MblnShowErrorMess)
                    MessageBox.Show("Error on SaveCompany() " + Ex.Message.ToString());
                return false;
            }
        }
        private void FillDetailParameters()
        {
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.lstCompanyBankDetails = new List<clsDTOCompanyBankDetails>();
            for (int i = 0; i < GrdCompanyBanks.RowCount - 1; i++)
            {
                clsDTOCompanyBankDetails objClsDTOCompanyBankDetails = new clsDTOCompanyBankDetails();

                if (string.IsNullOrEmpty(Convert.ToString(GrdCompanyBanks.Rows[i].Cells[0].Value).Trim()))
                    objClsDTOCompanyBankDetails.intCompanyBankId = 0;
                else
                    objClsDTOCompanyBankDetails.intCompanyBankId = Convert.ToInt32(GrdCompanyBanks.Rows[i].Cells[0].Value);

                objClsDTOCompanyBankDetails.intBankId = Convert.ToInt32(GrdCompanyBanks.Rows[i].Cells[2].Value);
                objClsDTOCompanyBankDetails.strAccountNumber = Convert.ToString(GrdCompanyBanks.Rows[i].Cells[3].Value).Replace("'", "’").Trim();
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.lstCompanyBankDetails.Add(objClsDTOCompanyBankDetails);
            }
        }

        private void FillParameters()
        {
            if (MblnAddStatus == true)
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID = 0;
            else
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID = Convert.ToInt32(NameTextBox.Tag);

            if (RbtnBranch.Checked == false)
            {
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intParentID = 0;
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.blnCompanyBranchIndicator = true;
            }
            else
            {
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intParentID = Convert.ToInt32(CboCompanyName.SelectedValue);
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.blnCompanyBranchIndicator = false;
            }

            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strName = NameTextBox.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strShortName = TxtShortName.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strPOBox = TxtPOBox.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strRoad = TxtStreet.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strArea = TxtArea.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strBlock = TxtBlock.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strCity = TxtCity.Text.Replace("'", "’").Trim();

            if (CboProvince.SelectedIndex != -1)
            {
                if (CboProvince.SelectedValue != null)
                    MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intProvinceID = Convert.ToInt32(CboProvince.SelectedValue);
            }

            if (CboCountry.SelectedIndex != -1)
            {
                if (CboCountry.SelectedValue != null)
                    MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCountryID = Convert.ToInt32(CboCountry.SelectedValue);
            }

            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strPrimaryEmail = TxtPrimaryEmail.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strSecondaryEmail = TxtSecondaryEmail.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strContactPerson = TxtContactPerson.Text.Replace("'", "’").Trim();

            if (CboDesignation.SelectedIndex != -1 && CboDesignation != null)
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intContactPersonDesignationID = Convert.ToInt32(CboDesignation.SelectedValue);

            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strContactPersonPhone = TxtContactPersonPhone.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strPABXNumber = TxtFaxNumber.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strWebSite = TxtWebSite.Text.Replace("'", "’").Trim();

            if (CboCompanyIndustry.SelectedIndex != -1 && CboCompanyIndustry != null)
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyIndustryID = Convert.ToInt32(CboCompanyIndustry.SelectedValue);

            if (CboCompanyType.SelectedIndex != -1 && CboCompanyType != null)
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyTypeID = Convert.ToInt32(CboCompanyType.SelectedValue);

            if (LogoFilePictureBox.Image != null)
            {
                ClsImages GetByteImage = new ClsImages();
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.byteLogoFile = GetByteImage.GetImageDataStream(LogoFilePictureBox.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            else
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.byteLogoFile = null;

            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strOtherInfo = TxtOtherInfo.Text.Replace("'", "’");

            if(RBtnYear.Checked == true)
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.blnIsMonth = false;
            else
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.blnIsMonth = true;

            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intWorkingDaysInMonth =Convert.ToInt32(NumTxtWorkingDays.Text);
            //MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intOffDayID = Convert.ToInt32(cboDays.SelectedValue);

            if (CboCurrency.SelectedIndex != -1 && CboCurrency != null)
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCurrencyId = Convert.ToInt32(CboCurrency.SelectedValue);

            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strFinYearStartDate = DtpFinyearDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strBookStartDate = DtpBkStartDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strEPID = TxtEmployerCode.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strCompanyServiceTaxNo = TxtSTNo.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strCompanyCSTNo = TxtCSTNo.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strCompanyTINNo = TxtTINNo.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strCompanyPANNo = TxtPANNo.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strVATRegistrationNo = TxtVATNo.Text.Replace("'", "’").Trim();

           MobjClsBLLCompanyInformation.clsDTOCompanyInformation.TaxablePersonName=txtTaxablePerson.Text.Trim();
           MobjClsBLLCompanyInformation.clsDTOCompanyInformation.TaxablePersonNameArb=txtTaxablePersonArb.Text.Trim();
           MobjClsBLLCompanyInformation.clsDTOCompanyInformation.TRN=txtTRN.Text.Trim();
           MobjClsBLLCompanyInformation.clsDTOCompanyInformation.TaxAgencyName=txtTaxAgencyName.Text.Trim();
           MobjClsBLLCompanyInformation.clsDTOCompanyInformation.TAN=txtTAAN.Text.Trim();
           MobjClsBLLCompanyInformation.clsDTOCompanyInformation.TaxAgentName=txtTaxAgentName.Text.Trim();
           MobjClsBLLCompanyInformation.clsDTOCompanyInformation.TAAN=txtTAAN.Text.Trim();

           MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intUnernedPolicyID = Convert.ToInt32(cboUnearnedPolicyID.SelectedValue.ToInt32());



            if (OffDayIds != string.Empty)
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strOffDayIDs = OffDayIds.Remove(0, 1);
            else
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strOffDayIDs = string.Empty;


        }

        private void Changestatus()
        {
            MblnIsFromOK = false;

            if (!blnIsEditMode)
            {
                BtnOk.Enabled = MblnAddPermission;
                BtnSave.Enabled = MblnAddPermission;
                CompanyMasterBindingNavigatorSaveItem.Enabled = MblnAddPermission; 
            }
            else
            {
                BtnOk.Enabled = MblnUpdatePermission;
                BtnSave.Enabled = MblnUpdatePermission;
                CompanyMasterBindingNavigatorSaveItem.Enabled = MblnUpdatePermission; 
            }

            ErrorProviderCompany.Clear();            
        }

        private int CheckDuplicationInGrid(DataGridView Grd, int ColIndexfirst, int ColIndexsecond)
        {
            string SearchValuefirst = "";
            string SearchValuesecond = "";
            int RowIndexTemp = 0;

            foreach (DataGridViewRow rowValue in Grd.Rows)
            {
                if (rowValue.Cells[ColIndexfirst].Value != null)
                {
                    SearchValuefirst = Convert.ToString(rowValue.Cells[ColIndexfirst].Value);
                    SearchValuesecond = Convert.ToString(rowValue.Cells[ColIndexsecond].Value);
                    RowIndexTemp = rowValue.Index;

                    foreach (DataGridViewRow row in Grd.Rows)
                    {
                        if (RowIndexTemp != row.Index)
                        {
                            if (row.Cells[ColIndexfirst].Value != null)
                            {
                                if (Convert.ToString(row.Cells[ColIndexsecond].Value).Trim() != null)
                                {
                                    if (Convert.ToString(row.Cells[ColIndexsecond].Value).Trim() != null)
                                    {
                                        if ((Convert.ToString(row.Cells[ColIndexfirst].Value).Trim() == Convert.ToString(SearchValuefirst).Trim()) && (Convert.ToString(row.Cells[ColIndexsecond].Value).Trim() == Convert.ToString(SearchValuesecond).Trim()))
                                            return row.Index;
                                    }

                                    if (Convert.ToString(row.Cells[ColIndexsecond].Value).Trim() == null)
                                    {
                                        if (Convert.ToString(row.Cells[ColIndexfirst].Value).Trim() == SearchValuefirst.Trim())
                                            return row.Index;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return -1;
        }

        private void LoadReport()
        {
            try
            {
                if (Convert.ToInt32(NameTextBox.Tag) > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = Convert.ToInt32(NameTextBox.Tag);
                    ObjViewer.PiFormID = (int)FormID.CompanyInformation;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on LoadReport() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport()" + ex.Message.ToString());
            }
        }
        //---------------------------------------------------------------------------------------------------------------------------------------------

        private void FrmCompany_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID > 0)
                PintCompany = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID;

            if (!MblnIsFromOK)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 8,out MmessageIcon );

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question ) == System.Windows.Forms.DialogResult.Yes)
                    e.Cancel = false;
                else
                    e.Cancel = true;
            }
        }

        private void BtnComSettings_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmCompanySettings objComSettings = new FrmCompanySettings())
                {
                    objComSettings.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnComSettings() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnComSettings()" + ex.Message.ToString());
            }
        }
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.General, 
                    (Int32)eMenuID.NewCompany, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, 
                    out MblnDeletePermission);                
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private bool LoadCombos(int intType)
        {
            // function for loading combo
            // 0 - For Loading All Combo
            // 1 - For Loading CboCompanyIndustry
            // 2 - For Loading CboCompanyName
            // 3 - For Loading CboCompanyType
            // 4 - For Loading cboCountry 
            // 5 - For Loading cboCurrency
            // 6 - For Loading cboDesignation
            // 7 - For Loading cboProvince
            // 8 - For Loading DataGridViewComboBoxColumn Bank Id

            try
            {               
                DataTable datCombos = new DataTable();

                if (intType == 0 || intType == 1)
                {
                    datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CompanyIndustryID,CompanyIndustry", "IndustryReference", "", "CompanyIndustryID", "CompanyIndustry" });
                    CboCompanyIndustry.ValueMember = "CompanyIndustryID";
                    CboCompanyIndustry.DisplayMember = "CompanyIndustry";
                    CboCompanyIndustry.DataSource = datCombos;
                }

                if (intType == 0 || intType == 2)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" +
                        "CompanyID = " + ClsCommonSettings.LoginCompanyID + " AND IsBranch=1", "CompanyID", "CompanyName" });
                    CboCompanyName.ValueMember = "CompanyID";
                    CboCompanyName.DisplayMember = "CompanyName";
                    CboCompanyName.DataSource = datCombos;
                }

                if (intType == 0 || intType == 3)
                {
                    datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CompanyTypeID,CompanyType", "CompanyTypeReference", "", "CompanyTypeID", "CompanyType" });
                    CboCompanyType.ValueMember = "CompanyTypeID";
                    CboCompanyType.DisplayMember = "CompanyType";
                    CboCompanyType.DataSource = datCombos;
                }

                if (intType == 0 || intType == 4)
                {
                    datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CountryID,CountryName", "CountryReference", "" });
                    CboCountry.ValueMember = "CountryID";
                    CboCountry.DisplayMember = "CountryName";
                    CboCountry.DataSource = datCombos;
                }

                if (intType == 0 || intType == 5)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CurrencyID,CurrencyName", "CurrencyReference", "", "CurrencyID", "CurrencyName" });
                    CboCurrency.ValueMember = "CurrencyID";
                    CboCurrency.DisplayMember = "CurrencyName";
                    CboCurrency.DataSource = datCombos;
                }

                if (intType == 0 || intType == 6)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "DesignationID,Designation", "DesignationReference", "", "DesignationID", "Designation" });
                    CboDesignation.ValueMember = "DesignationID";
                    CboDesignation.DisplayMember = "Designation";
                    CboDesignation.DataSource = datCombos;
                }

                if (intType == 0 || intType == 7)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "ProvinceID,ProvinceName", "ProvinceReference", "", "ProvinceID", "ProvinceName" });
                    CboProvince.ValueMember = "ProvinceID";
                    CboProvince.DisplayMember = "ProvinceName";
                    CboProvince.DataSource = datCombos;
                }

                if (intType == 0 || intType == 8)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "BB.BankBranchID,(BB.BankBranchName + '-' +  BN.BankName) As Description", "BankBranchReference BB LEFT JOIN BankReference BN on BN.BankID=BB.BankID", "", "BankBranchID", "Description" });
                    BankNameId.ValueMember = "BankBranchID";
                    BankNameId.DisplayMember = "Description";
                    BankNameId.DataSource = datCombos;
                }

                if (intType == 0 || intType == 9)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "UnearnedPolicyID,UnearnedPolicy", "PayUnearnedAmtPolicyMaster", "", "UnearnedPolicyID", "UnearnedPolicy" });
                    cboUnearnedPolicyID.ValueMember = "UnearnedPolicyID";
                    cboUnearnedPolicyID.DisplayMember = "UnearnedPolicy";
                    cboUnearnedPolicyID.DataSource = datCombos;
                }

                mObjLogs.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private void GrdCompanyBanks_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {            
            if (GrdCompanyBanks.Rows[e.Row.Index] != null && GrdCompanyBanks[0, e.Row.Index].Value != null && bnlGridRowDelete == false)
                lstDeletedBankIds.Add(Convert.ToInt32(GrdCompanyBanks[0, e.Row.Index].Value));
        }

        private void txt_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }
        
        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            
            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;

            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
        }

        private void FrmCompany_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    //case Keys.Control | Keys.S:
                    //    btnSave_Click(sender,new EventArgs());//save
                    //  break;
                    //case Keys.Control | Keys.O:
                    //    btnOk_Click(sender, new EventArgs());//OK
                    //    break; 
                    //case Keys.Control | Keys.L:
                    //    btnCancel_Click(sender, new EventArgs());//Cancel
                    //    break;
                    case Keys.Control | Keys.Enter:
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        CancelToolStripButton_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        BtnPrint_Click(sender, new EventArgs());//Cancel
                        break;
                    case Keys.Control | Keys.M:
                        BtnEmail_Click(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Newcompany";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void GrdCompanyBanks_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Changestatus();
        }

        private void NumTxtWorkingDays_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void cboDays_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void RBtnYear_CheckedChanged(object sender, EventArgs e)
        {
            Changestatus();

            if(MblnAddStatus == true)
            {
                if (RBtnYear.Checked == true)
                    NumTxtWorkingDays.Text = "365";
                else
                    NumTxtWorkingDays.Text = "30";
            }
        }

        private void RBtnMonth_CheckedChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void GrdCompanyBanks_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (MblnAddStatus != true)
                {
                    if (e.KeyCode == Keys.Delete)
                    {
                        bnlGridRowDelete = false;
                        if (GrdCompanyBanks.CurrentRow != null)
                        {
                            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.AccountNumber = GrdCompanyBanks.CurrentRow.Cells["AccountNumber"].Value.ToStringCustom();

                            if (MobjClsBLLCompanyInformation.GetBankBranchExistsInEmployee(GrdCompanyBanks.CurrentRow.Cells["BankNameId"].Value.ToInt32()))
                            {
                                e.Handled = true;
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 231, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrComapny.Enabled = true;
                                bnlGridRowDelete = true;
                                return;
                            }
                            else
                                e.Handled = false;
                        }

                        Changestatus();
                    }
                }
            }
            catch { }            
        }

        //private void cboDays_KeyDown(object sender, KeyEventArgs e)
        //{
        //    cboDays.DroppedDown = false;
        //}

        private void GrdCompanyBanks_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjClsBLLCommonUtility.SetSerialNo(GrdCompanyBanks, e.RowIndex, false);
        }

        private void GrdCompanyBanks_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjClsBLLCommonUtility.SetSerialNo(GrdCompanyBanks, e.RowIndex, false);
        }

        private void LoadParentCompany(int intParentID)
        {
            CboCompanyName.DataSource = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" +
                        "CompanyID = " + intParentID + " AND IsBranch=1", "CompanyID", "CompanyName" });
            CboCompanyName.ValueMember = "CompanyID";
            CboCompanyName.DisplayMember = "CompanyName";
        }

        private void cboUnearnedPolicyID_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboUnearnedPolicyID.DroppedDown = false;
        }

        private void chkSunday_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            if (chk.Checked)
                OffDayIds = OffDayIds + "," + Convert.ToString(chk.Tag);
            else
                OffDayIds = OffDayIds.Replace("," + Convert.ToString(chk.Tag), string.Empty);
            Changestatus();
        }

        private void btnUnearnedPolicy_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(cboUnearnedPolicyID.SelectedValue);
                using (FrmUnearnedPolicy objUP = new FrmUnearnedPolicy())
                {
                    objUP.PintUnEarnedPolicyID = MintComboID;
                    objUP.ShowDialog();
                }
                LoadCombos(9);
                cboUnearnedPolicyID.SelectedValue = MintComboID;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnCurrency_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnCurrency_Click" + ex.Message.ToString());

            }
        }

        private void cboUnearnedPolicyID_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Changestatus();
            }
            catch
            {
            }
        }

        private void Accdates_Click(object sender, EventArgs e)
        {

        }


    }
}