﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;



/* 
=================================================
   Author:		<Author,,Albz>
   Create date: <Create Date,,16 Feb 2011>
   Description:	<Description,,VendorInformation Form>
   Modified By: <Author,,Amal>
   Modified date: <Modified Date,,31 Aug 2011>
================================================
*/
namespace MyBooksERP
{
    public partial class FrmVendor : Form
    {
        #region Variables Declaration
        private bool MblnAddStatus; //To Specify Add/Update mode 
        private bool MblnChangeStatus = false; //To Specify Change Status
        private bool MblnPrintEmailPermission = false;//To Set Print Email Permission
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        //private bool MblnStatusPermission = false;//To Status Permission
        private bool MblnIsEditMode = false;
        public int PintVendorID = 0; //To Set VendorID Externally
        private int MintCurrentRecCnt;//Contains Current Record count
        private int MintTotalRecCnt;//Contains Total Record count
        private int MintCurrentVendorID;//Contains Current Vendor ID
        private int MintCurrentAddressID;//Contains Current VendorAddress ID
        private int MintCnt = 0;//To Loop Vendor Address
        private int MintVendorType;//To Identify vendor or customer

        private int MintRowNumber = 0;
        private string MstrMessageCommon; //To Set the Message
        private MessageBoxIcon MmessageIcon;//To Set the MessageIcon
        private DataTable MdatMessages; //To Store All Message

        ToolStripControlHost MobjToolStripControlHost;
        WebBrowser MobjWebBrowser;

        clsBLLVendorInformation MobjClsBLLVendorInformation;
        ClsNotificationNew MobjClsNotification;
        ClsLogWriter MobjClsLogWriter;

        string MsCustomercodeprefix;  // Auto increment Customercode
        string MsSuppliercodeprefix;        //  Autogeneration Customercode
        string MsContractorcodeprefix;    //  Autogeneration Contractorcode
        string MsAgentcodePrefix;     //  Autogeneration Agentcode
        bool blnEditCustomerCode;
        bool blnEditSupplierCode;
        clsBLLCommonUtility MobjClsBLLCommonUtility = new clsBLLCommonUtility();

        #endregion //Variables Declaration

        #region Constructor

        public FrmVendor()
        {
            InitializeComponent();

            MintCurrentVendorID = 0;
            MintCurrentAddressID = 0;
            MstrMessageCommon = "";
            MmessageIcon = MessageBoxIcon.Information;

            MobjClsBLLVendorInformation = new clsBLLVendorInformation();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotificationNew();

            MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = MintCurrentVendorID;
            //Showing Webbrowser in toolstripsplitbtn
            MobjWebBrowser = new WebBrowser();
            MobjWebBrowser.ScrollBarsEnabled = false;
            MobjWebBrowser.Name = "UsedLevel";
            MobjToolStripControlHost = new ToolStripControlHost(MobjWebBrowser);
            //BtnReferences.DropDownItems.Add(MobjToolStripControlHost);
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettingsData(1);
            MsCustomercodeprefix = GetCompanySettingsValue("Customer Code", "Prefix", dtCompanySettingsInfo); //ClsCommonSettings.strCustomerCodePrefix;  // Auto increment Customercode
            MsSuppliercodeprefix = GetCompanySettingsValue("Supplier Code", "Prefix", dtCompanySettingsInfo); //ClsCommonSettings.strSupplierCodePrefix;        //  Autogeneration Customercode
            MsContractorcodeprefix = GetCompanySettingsValue("Contractor Code", "Prefix", dtCompanySettingsInfo); //ClsCommonSettings.strContracterCodePrefix;    //  Autogeneration Contractorcode
            MsAgentcodePrefix = GetCompanySettingsValue("AgentBank Code", "Prefix", dtCompanySettingsInfo); //ClsCommonSettings.strAgentBankCodePrefix;     //  Autogeneration Agentcode
            blnEditCustomerCode= ConvertStringToBoolean(GetCompanySettingsValue("Customer Code", "Prefix Editable", dtCompanySettingsInfo));
            blnEditSupplierCode = ConvertStringToBoolean(GetCompanySettingsValue("Supplier Code", "Prefix Editable", dtCompanySettingsInfo));
        }
        private bool ConvertStringToBoolean(string strValue)
        {
            if (strValue == "Yes")
                return true;
            return false;
        }
        public FrmVendor(int intvendortype)
        {
            if (intvendortype == 3)
                intvendortype = 1;
            InitializeComponent();

            MintCurrentVendorID = 0;
            MintCurrentAddressID = 0;
            MstrMessageCommon = "";
            MmessageIcon = MessageBoxIcon.Information;
            MintVendorType = intvendortype;

            if (intvendortype == (int)VendorType.Customer)
            {
                //MintVendorMenuID = (Int32)MenuID.Customer;
                this.Text = "Customers";
                this.Icon = ((System.Drawing.Icon)(Properties.Resources.Customer1));
            }
            else
            {
                //MintVendorMenuID = (Int32)MenuID.Vendor;
                this.Text = "Suppliers";
                this.Icon = ((System.Drawing.Icon)(Properties.Resources.Supplier1));
            }

            MobjClsBLLVendorInformation = new clsBLLVendorInformation();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotificationNew();

            MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = MintCurrentVendorID;
            //Showing Webbrowser in toolstripsplitbtn
            MobjWebBrowser = new WebBrowser();
            MobjWebBrowser.ScrollBarsEnabled = false;
            MobjWebBrowser.Name = "UsedLevel";
            MobjToolStripControlHost = new ToolStripControlHost(MobjWebBrowser);
            //BtnReferences.DropDownItems.Add(MobjToolStripControlHost);
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettingsData(1);
            MsCustomercodeprefix = GetCompanySettingsValue("Customer Code", "Prefix", dtCompanySettingsInfo); //ClsCommonSettings.strCustomerCodePrefix;  // Auto increment Customercode
            MsSuppliercodeprefix = GetCompanySettingsValue("Supplier Code", "Prefix", dtCompanySettingsInfo); //ClsCommonSettings.strSupplierCodePrefix;        //  Autogeneration Customercode
            blnEditCustomerCode= ConvertStringToBoolean(GetCompanySettingsValue("Customer Code", "Prefix Editable", dtCompanySettingsInfo));
            blnEditSupplierCode = ConvertStringToBoolean(GetCompanySettingsValue("Supplier Code", "Prefix Editable", dtCompanySettingsInfo));
        }

        #endregion //Constructor

        private void FrmVendor_Load(object sender, EventArgs e)
        {
            try
            {
                SetPermissions();
                LoadCombos(0);
                //CboVendorType.SelectedValue = MintVendorType;
                LoadMessage();
                TmVendor.Interval = ClsCommonSettings.TimerInterval;
                //ClsCommonSettings.PblnAutoGeneratePartyAccount = true;
                Settings();
                FillNearestCompetitorColumn(MintCurrentVendorID);
                if (PintVendorID > 0) //Laxmi Added for purchase
                {
                    MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = PintVendorID;
                    MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorType = MintVendorType;
                    MintTotalRecCnt = MobjClsBLLVendorInformation.RecCountNavigate(MintVendorType);
                    MintCurrentRecCnt = MobjClsBLLVendorInformation.GetRowNumber();
                    DisplayInfo();
                    SetEnableAddress();
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintTotalRecCnt) + "";
                    AddToAddressMenu();
                }
                else
                {
                    AddNewItem();
                }
                //EnableDisableButtons();
                //Changestatus(false);
                
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form Load:FrmVendor_Load " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmVendor_Load() " + Ex.Message.ToString());
            }
        }
        public string GetCompanySettingsValue(string strConfigurationItem, string strSubItem, DataTable dtCompanySettings)
        {
            dtCompanySettings.DefaultView.RowFilter = "ConfigurationItem = '" + strConfigurationItem + "' And SubItem = '" + strSubItem + "'";
            if (dtCompanySettings.DefaultView.ToTable().Rows.Count > 0)
                return dtCompanySettings.DefaultView.ToTable().Rows[0]["ConfigurationValue"].ToString();
            return "";
        }
        private void SetPermissions()
        {
            try
            {
                clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
                if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    if (MintVendorType == (int)VendorType.Supplier)//Vendor
                    {
                        objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (int)eMenuID.Supplier , out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                    }
                    else
                    {
                        objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (int)eMenuID.Customer, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                    }
                }
                else
                    MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

                //BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                //VendorInformationBindingNavigatorSaveItem.Enabled = MblnUpdatePermission;                
                //BtnSave.Enabled = MblnUpdatePermission;
                //BtnOk.Enabled = MblnUpdatePermission;
                //BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                //CancelToolStripButton.Enabled = MblnUpdatePermission;
                //BtnPrint.Enabled = MblnPrintEmailPermission;
                //EmailStripButton.Enabled = MblnPrintEmailPermission;
                //btnActions.Enabled = MblnUpdatePermission;
                //if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)// For Vendor History
                //{
                //    DataTable datTemp = MobjClsBLLVendorInformation.FillCombos(new string[] { "*", "RoleDetails", "RoleID=" + ClsCommonSettings.RoleID + " " +
                //        "AND CompanyID=" + ClsCommonSettings.CompanyID + " AND MenuID=" + (int)MenuID.VendorHistory + " AND IsView=1"});
                //    if (datTemp != null)
                //    {
                //        if (datTemp.Rows.Count > 0)
                //            historyToolStripMenuItem.Enabled = true;
                //        else
                //            historyToolStripMenuItem.Enabled = false;
                //    }
                //    else
                //        historyToolStripMenuItem.Enabled = false;
                //}

                //if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                //{
                //    DataTable dtControlsPermissions = new DataTable();
                //    if (MintVendorType == (int)VendorType.Customer)//Customer
                //    {
                //        dtControlsPermissions = objClsBLLPermissionSettings.GetControlsPermissions(ClsCommonSettings.RoleID, (Int32)MenuID.Customer, 0);
                //    }
                //    else
                //    {
                //        dtControlsPermissions = objClsBLLPermissionSettings.GetControlsPermissions(ClsCommonSettings.RoleID, (Int32)MenuID.Vendor, 0);
                //    }
                //    dtControlsPermissions.DefaultView.RowFilter = "ControlDisplayText = 'Status'";
                //    if (dtControlsPermissions.DefaultView.ToTable().Rows.Count > 0)
                //    {
                //        if (Convert.ToBoolean(dtControlsPermissions.DefaultView.ToTable().Rows[0]["IsVisible"]) == true)
                //            CboStatus.Enabled = true;
                //        else
                //            CboStatus.Enabled = false;
                //    }
                //    else
                //        CboStatus.Enabled = false;
                //}
                //else
                //    CboStatus.Enabled = true;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on SetPErmission:FrmVendor" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on SetPErmission " + Ex.Message.ToString());
            }
        }

        private void LoadMessage()
        {
            MdatMessages = new DataTable();
            MdatMessages = MobjClsNotification.FillMessageArray((int)FormID.VendorInformation, ClsCommonSettings.ProductID );
        }
       
        private void Settings()
        {
            if (MintVendorType  == (int)VendorType.Supplier)
            {
                TbAssociates.TabPages.RemoveAt(2);
                lblCreditLimit.Visible = true;
                txtCreditLimit.Visible = true;
                lblCreditLimitDays.Visible = true ;
                txtCreditLimitDays.Visible = true;
                //CboBankName.BackColor = SystemColors.Info;
                //cboBankBranch.BackColor = SystemColors.Info;
                
            }
            else if  (MintVendorType == (int)VendorType.Customer)
            {
                cboCurrency.Visible = true;
                btnCurrencyReference.Visible = true;
                lblCurrency.Visible = true;
                TbAssociates.TabPages.RemoveAt(2);
            }

            else if (MintVendorType == (int)VendorType.Customer && ClsCommonSettings.PblnAutoGeneratePartyAccount == true)
            {
                TbAssociates.TabPages.RemoveAt(3);
                LblAccountHead.Enabled = false;
                cboAccountHead.Enabled = false;
                BtnAccountHead.Enabled = false;
                lblSupClassifications.Visible = false;
                CboSupClassification.Visible = false;
            }
            else if (MintVendorType == (int)VendorType.Customer && ClsCommonSettings.PblnAutoGeneratePartyAccount == false)
            {
                TbAssociates.TabPages.RemoveAt(3);
                LblAccountHead.Enabled = true;
                cboAccountHead.Enabled = true;
                BtnAccountHead.Enabled = true;
                cboAccountHead.BackColor = SystemColors.Info;
                lblSupClassifications.Visible = false;
                CboSupClassification.Visible = false;
            }
            else if (MintVendorType == (int)VendorType.Supplier && ClsCommonSettings.PblnAutoGeneratePartyAccount == true)
            {
                LblAccountHead.Enabled = false;
                cboAccountHead.Enabled = false;
                BtnAccountHead.Enabled = false;
                lblCreditLimit.Visible = false;
                txtCreditLimit.Visible = false;

            }
            else if (MintVendorType == (int)VendorType.Supplier && ClsCommonSettings.PblnAutoGeneratePartyAccount == false)
            {
                LblAccountHead.Enabled = true;
                cboAccountHead.Enabled = true;
                BtnAccountHead.Enabled = true;
                lblCreditLimit.Visible = false;
                txtCreditLimit.Visible = false;
                cboAccountHead.BackColor = SystemColors.Info;
            }
            else
            {
                LblAccountHead.Enabled = true;
                cboAccountHead.Enabled = true;
                BtnAccountHead.Enabled = true;
                cboAccountHead.BackColor = SystemColors.Window;
            }

            //if (MintVendorType == (int)VendorType.Customer)
            //{
            //    cboAccountHead.TabIndex = cboBankBranch.TabIndex;
            //    BtnAccountHead.TabIndex = BtnBank.TabIndex ;

            //    cboBankBranch.TabIndex = CboBankName.TabIndex;
            //    BtnBank.TabIndex = btnBankName.TabIndex;

            //    CboBankName.TabIndex = CboTransactiontype.TabIndex;
            //    btnBankName.TabIndex = CboBankName.TabIndex + 1;

            //    CboTransactiontype.TabIndex = CboSupClassification.TabIndex;

            //}
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewItem();
        }

        private bool AddNewItem()
        {
            try
            {
                MintCurrentVendorID = 0;
                MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorAddID = 0;
                SetEnable();
                btnAddress.Text = "Permanent";
                SetEnableAddress();
                AddToAddressMenu();
                MblnAddStatus = CboBankName.Enabled = BtnBank.Enabled =btnBankName.Enabled = TxtAccountNo.Enabled =cboBankBranch.Enabled = true;
                //CboVendorType.Enabled = true;
                //CboTransactiontype.SelectedIndex = -1;
                CboStatus.SelectedIndex = -1;
                cboCurrency.Enabled = true;
                //rdbMale.Checked = true;
                //Btnvendortype.Enabled = true;
                dgvNearestCompetitor.Rows.Clear();
                dgvNearestCompetitor.ClearSelection();
                MintTotalRecCnt = MobjClsBLLVendorInformation.RecCountNavigate(MintVendorType);
                MblnIsEditMode = false;
                if (MintTotalRecCnt > 0)
                {
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintTotalRecCnt + 1) + "";
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintTotalRecCnt + 1);
                    MintCurrentRecCnt = MintTotalRecCnt + 1;
                }
                else
                {
                    BindingNavigatorCountItem.Text = "of 1";
                    BindingNavigatorPositionItem.Text = "1";
                    MintCurrentRecCnt = 0;
                }
                TbAssociates.SelectedTab = TbGeneral;
                SetBindingNavigatorButtons();
                GenerateCode(MintVendorType);
                if (TxtCode.ReadOnly == true)
                    TxtVendorName.Focus();
                else
                    TxtCode.Focus();
                BindingNavigatorAddNewItem.Enabled = BtnPrint.Enabled = EmailStripButton.Enabled = btnActions.Enabled = BtnVendorMapping.Enabled = false;
                BtnOk.Enabled = BtnSave.Enabled = tlsBtnAddressSave.Enabled = BindingNavigatorDeleteItem.Enabled = false;
                VendorInformationBindingNavigatorSaveItem.Enabled = false;
                CancelToolStripButton.Enabled = true;
                //Changestatus(false);
                LblVendorStatus.Text = "";
                FillNearestCompetitorColumn(MintCurrentVendorID);
                MintRowNumber = 0;
                CboStatus.SelectedIndex = 0;
                cboCurrency.SelectedValue = ClsCommonSettings.CurrencyID;
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form AddNewItem:AddNewItem " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewItem() " + Ex.Message.ToString());

                return false;
            }
        }

        private void GenerateCode(int intVendorType)
        {
            try
            {
                switch (intVendorType)
                {
                    case 1:
                        MobjClsBLLVendorInformation.clsDTOVendorInformation.intSerialNo = (MobjClsBLLVendorInformation.GenerateCode((int)VendorType.Customer) + 1);
                        TxtCode.Text = MsCustomercodeprefix + MobjClsBLLVendorInformation.clsDTOVendorInformation.intSerialNo.ToString();

                        if (blnEditCustomerCode)
                            TxtCode.ReadOnly = false;
                        else
                            TxtCode.ReadOnly = true;

                        break;
                    case 2:
                        MobjClsBLLVendorInformation.clsDTOVendorInformation.intSerialNo = (MobjClsBLLVendorInformation.GenerateCode((int)VendorType.Supplier) + 1);
                        TxtCode.Text = MsSuppliercodeprefix + MobjClsBLLVendorInformation.clsDTOVendorInformation.intSerialNo.ToString();

                        if (blnEditSupplierCode)
                            TxtCode.ReadOnly = false;
                        else
                            TxtCode.ReadOnly = true;

                        break;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in GenerateCode() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in GenerateCode() " + ex.Message, 2);
            }
        }

        private void SetEnable()
        {
            ClearVenderInfo();
            //SetEnableAddress();
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled =  false;
            //EnableDisableButtons();BtnReferences.Enabled
        }
        private void setBNButtons()
        {
            if (MblnIsEditMode)
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BtnPrint.Enabled = BtnVendorMapping.Enabled = EmailStripButton.Enabled = MblnPrintEmailPermission;
            TlsBtnAddressDelete.Enabled = BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            BtnOk.Enabled = BtnSave.Enabled = VendorInformationBindingNavigatorSaveItem.Enabled = false;
        }
        private bool LoadCombos(int intType)
        {
            // 0 - For Loading All Combo
            // 1 - For Loading CboVendorType 
            // 2 - For Loading CboTransactiontype 
            // 3 - For Loading CboBankName 
            // 4 - For Loading cboCountry
            // 5 - For Loading cboLocationType
            // 6 - For Loading cboBankBranch
            // 7 - For Loading Status
            // 8 - For Loading Employment Type
            // 9 - For Loading Designation
            // 10- For Loading Account Type
            // 11- For Loading Source Media
            // 12- For Loading Building Type
            // 13- For Loading CboSupClassification
            // 14- For Loading Department
          try
          {
                DataTable datCombos = new DataTable();
                clsBLLPermissionSettings objBllPermissionSettings = new clsBLLPermissionSettings();
               

                //if (MintVendorType == (int)VendorType.Customer)//Customers
                //{
                //    //if (intType == 0 || intType == 1)
                //    //{
                //        datCombos = MobjClsBLLVendorInformation.FillCombos(new string[] { "VendorTypeID,VendorType", "InvVendorTypeReference", "VendorTypeID="+(int)VendorType.Customer+"" });
                //        //CboVendorType.ValueMember = "VendorTypeID";
                //        //CboVendorType.DisplayMember = "VendorType";
                //        //CboVendorType.DataSource = datCombos;
                //    //}
                //    //Btnvendortype.Enabled = false;
                //}
                //else
                //{
                //    if (intType == 0 || intType == 1)//Suppliers
                //    {
                //        datCombos = MobjClsBLLVendorInformation.FillCombos(new string[] { "VendorTypeID,VendorType", "InvVendorTypeReference", "VendorTypeID = "+(int)VendorType.Supplier+"" });
                //        //CboVendorType.ValueMember = "VendorTypeID";
                //        //CboVendorType.DisplayMember = "VendorType";
                //        //CboVendorType.DataSource = datCombos;
                //    }
                //    //Btnvendortype.Enabled = false;
                //}
                
                //if (intType == 0 || intType == 2)
                //{
                //    datCombos = null;
                //    datCombos = MobjClsBLLVendorInformation.FillCombos(new string[] { "TransactionTypeID,TransactionType", "TransactionTypeReference", "TransactionTypeID < 5" });
                //    CboTransactiontype.ValueMember = "TransactionTypeID";
                //    CboTransactiontype.DisplayMember = "TransactionType";
                //    CboTransactiontype.DataSource = datCombos;
                //}
                if (intType == 0 || intType == 3)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLVendorInformation.FillCombos(new string[] { "BankID,BankName", "BankReference", "" });
                    CboBankName.ValueMember = "BankID";
                    CboBankName.DisplayMember = "BankName";
                    CboBankName.DataSource = datCombos;
                }
                if (intType == 0 || intType == 4)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLVendorInformation.FillCombos(new string[] { "CountryID,CountryName", "CountryReference", "" });
                    cboCountry.ValueMember = "CountryID";
                    cboCountry.DisplayMember = "CountryName";
                    cboCountry.DataSource = datCombos;
                }
                if (intType == 0 || intType == 16)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLVendorInformation.FillCombos(new string[] { "distinct CR.CurrencyID,CR.CurrencyName", "CurrencyReference CR INNER JOIN CurrencyDetails CD ON CR.CurrencyID=CD.CurrencyID", "CD.CompanyID= 1" });
                    cboCurrency.ValueMember = "CurrencyID";
                    cboCurrency.DisplayMember = "CurrencyName";
                    cboCurrency.DataSource = datCombos;
                    cboCurrency.SelectedIndex = -1;
                }
                if (intType == 0 || intType == 5)
                {
                }
                if (intType == 6)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLVendorInformation.FillCombos(new string[] { "BankBranchID,BankBranchName", "BankBranchReference","BankID = "+ Convert.ToInt32(CboBankName.SelectedValue)+ " order by BankBranchName"});
                    cboBankBranch.ValueMember = "BankBranchID";
                    cboBankBranch.DisplayMember = "BankBranchName";
                    cboBankBranch.DataSource = datCombos;
                }
                if (intType == 0 || intType == 7)
                {
                    datCombos = null;
                    if(MintVendorType == (int)VendorType.Customer)
                        datCombos = MobjClsBLLVendorInformation.FillCombos(new string[] { "StatusID, Status", "CommonStatusReference", "OperationTypeID = "+(int)OperationType.Customers });
                    else
                        datCombos = MobjClsBLLVendorInformation.FillCombos(new string[] { "StatusID, Status", "CommonStatusReference", "OperationTypeID = "+(int)OperationType.Suppliers });
                    CboStatus.DisplayMember = "Status";
                    CboStatus.ValueMember = "StatusID";
                    CboStatus.DataSource = datCombos;
                }
                //if (MintVendorType == (int)VendorType.Customer)//Customers
                //{
                   
                    if (intType == 0 || intType == 15)
                    {
                        datCombos = null;
                        datCombos = MobjClsBLLVendorInformation.FillCombos(new string[] { "SourceMediaID,SourceMediaType", "InvSourceMediaTypeReference", "" });
                        cboSourceMedia.ValueMember = "SourceMediaID";
                        cboSourceMedia.DisplayMember = "SourceMediaType";
                        cboSourceMedia.DataSource = datCombos;
                    }
                //}
                //if (MintVendorType == (int)VendorType.Supplier)//Suppliers
                //{
                    if (intType == 0 || intType == 13)
                    {
                        datCombos = null;
                        datCombos = MobjClsBLLVendorInformation.FillCombos(new string[] { "VendorClassificationID,VendorClassification", "InvVendorClassificationReference", "" });
                        CboSupClassification.DisplayMember = "VendorClassification";
                        CboSupClassification.ValueMember = "VendorClassificationID";
                        CboSupClassification.DataSource = datCombos;
                    }
                    FillAccountHeads();
                //}
                MobjClsLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        #region "NearestCompetitor"

        private void FillNearestCompetitorColumn(int MintCurrentVendorID)
        {
            try
            {
                MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = MintCurrentVendorID;
                DataTable dtVendor = MobjClsBLLVendorInformation.GetNearestCompetitor(MintCurrentVendorID);
                ColOtherSuppliers.DataSource = null;
                ColOtherSuppliers.ValueMember = "VendorID";
                ColOtherSuppliers.DisplayMember = "OtherSuppliers";
                ColOtherSuppliers.DataSource = dtVendor;

            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on FillNearestCompetitorColumn() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FillNearestCompetitorColumn()" + ex.Message.ToString());
            }
        }
        private void FillNearestCompetitorGrid()
        {
            try
            {
                DataTable datFillNearestCompetitor = MobjClsBLLVendorInformation.FillNearestCompetitor();
                dgvNearestCompetitor.Rows.Clear();
                if (datFillNearestCompetitor !=null && datFillNearestCompetitor.Rows.Count > 0)
                {
                    for (int iRowCount = 0; iRowCount < datFillNearestCompetitor.Rows.Count; iRowCount++)
                    {
                        dgvNearestCompetitor.RowCount = dgvNearestCompetitor.RowCount + 1;
                        dgvNearestCompetitor.Rows[iRowCount].Cells["ColOtherSuppliers"].Value = datFillNearestCompetitor.Rows[iRowCount]["VendorID"];
                        dgvNearestCompetitor.Rows[iRowCount].Cells["ColRating"].Value = datFillNearestCompetitor.Rows[iRowCount]["Rating"];
                    }
                }
               
            }
            catch(Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on adding to FillNearestCompetitorGrid:FillNearestCompetitorGrid " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FillNearestCompetitorGrid() " + Ex.Message.ToString());
            }
        }

        private void FillNearestCompetitor()
        {

            MobjClsBLLVendorInformation.clsDTOVendorInformation.lstNearestCompetitor = new List<clsDTOVendorNearestCompetitor>();

            foreach (DataGridViewRow dgvrow in dgvNearestCompetitor.Rows)
            {
                if (dgvrow.Cells["ColOtherSuppliers"].Value != DBNull.Value && dgvrow.Cells["ColOtherSuppliers"].Value != null)
                {
                    clsDTOVendorNearestCompetitor objclsDTOVendorNearestCompetitor = new clsDTOVendorNearestCompetitor();
                    //MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = MintCurrentVendorID;
                    objclsDTOVendorNearestCompetitor.intNearestCompetitorID = Convert.ToInt32(dgvrow.Cells["ColOtherSuppliers"].Value);
                    MobjClsBLLVendorInformation.clsDTOVendorInformation.lstNearestCompetitor.Add(objclsDTOVendorNearestCompetitor);
                }
            }
        }
        #endregion

        private void VendorInformationBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (VendorFormValidation())
            {
                if (SaveVendorInfo())
                {
                    SaveAddress();

                    //if (this.MblnAddStatus)
                      //  this.SendSMS(this.TxtVendorName.Text, this.TxtMobile1.Text);


                    TmVendor.Enabled = true;
                    BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                    MblnChangeStatus = MblnAddStatus = false;
                    MintTotalRecCnt = MobjClsBLLVendorInformation.RecCountNavigate(MintVendorType);
                    EnableDisableButtons();
                    //AddNewItem();
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                    BindingNavigatorMovePreviousItem_Click(null, null);
                    TmVendor.Enabled = false;
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 2, out MmessageIcon);
                    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmVendor.Enabled=true;
                }
            }
        }

        private bool AddToAddressMenu()
        {
            try
            {
                int intICounter;

                MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = MintCurrentVendorID;
                CMSAssociates.Items.Clear();
                //CMSAssociates.Items.Add("Permanent");
                DataTable datAddMenu = MobjClsBLLVendorInformation.DtGetAddressName();

                for (intICounter = 0; intICounter <= datAddMenu.Rows.Count - 1; intICounter++)
                {
                    ToolStripMenuItem tItem = new ToolStripMenuItem();
                    tItem.Tag = Convert.ToString(datAddMenu.Rows[intICounter][0]);
                    tItem.Text = Convert.ToString(datAddMenu.Rows[intICounter][1]);
                    CMSAssociates.Items.Add(tItem);
                }
                CMSAssociates.Items.Add("New");
                CMSAssociates.ItemClicked -= new ToolStripItemClickedEventHandler(this.MenuItem_Click);
                CMSAssociates.ItemClicked += new ToolStripItemClickedEventHandler(this.MenuItem_Click);
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on adding to AddressMenu:AddToAddressMenu " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddToAddressMenu() " + Ex.Message.ToString());

                return false;
            }
        }

        private void MenuItem_Click(System.Object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                CMSAssociates.Visible = false;

                if (MintCurrentVendorID <= 0)
                {
                    if (VendorFormValidation())
                    {
                        if (SaveVendorInfo())
                        {
                            if (TxtAddressName.Text.Trim() == "Permanent")
                            {
                                SaveAddress();
                            }
                            MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 2, out MmessageIcon);
                            LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmVendor.Enabled = true;
                            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                            MblnAddStatus = false;

                            MintTotalRecCnt = MobjClsBLLVendorInformation.RecCountNavigate(MintVendorType);

                            btnAddress.Text = e.ClickedItem.Text;
                            MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorAddID = Convert.ToInt32(e.ClickedItem.Tag);
                            MintCurrentAddressID = Convert.ToInt32(e.ClickedItem.Tag);
                            SetEnableAddress();
                            DisplayAddress();
                        }
                    }
                    TxtAddressName.Focus();
                }
                else
                {
                    btnAddress.Text = e.ClickedItem.Text;
                    MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorAddID = Convert.ToInt32(e.ClickedItem.Tag);
                    SetEnableAddress();
                    DisplayAddress();
                    TxtAddressName.Focus();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on address menu item click:MenuItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MenuItem_Click() " + Ex.Message.ToString());
            }
        }

        private bool DisplayAddress()
        {
            if (MobjClsBLLVendorInformation.FindVendorAddress())
            {
                TxtContactPerson.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strContactPerson;
                cboCountry.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intCountry;
                TxtAddress.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strAddress;
                TxtTelephone.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strTelephoneNo;
                txtFax.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strFax;
                TxtState.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strState;
                TxtZipCode.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strZipCode;
                TxtAddressName.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strAddressName;
                txtMobileNo.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strMobile1;
            }
            return true;
        }

        #region "VendorForm Validation"
        private bool VendorFormValidation()
        {
          
            //if (Convert.ToInt32(CboVendorType.SelectedValue) == 0)
            //{
            //    //  MsMessageCommon = "Please enter Vendor Type";
            //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 301, out MmessageIcon);
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            //    ErrProVendor.SetError(CboVendorType, MstrMessageCommon.Replace("#", "").Trim());
            //    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            //    TmVendor.Enabled = true;
            //    TbAssociates.SelectedTab = TbGeneral;
            //    CboVendorType.Focus();
            //    return false;
            //}

            if (TxtCode.Text.Trim() == "")
            {
                //  MsMessageCommon = "Please enter Code";
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 302, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrProVendor.SetError(TxtCode, MstrMessageCommon.Replace("#", "").Trim());
                LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmVendor.Enabled = true;
                TbAssociates.SelectedTab = TbGeneral;
                TxtCode.Focus();
                return false;
            }
            if (TxtVendorName.Text.Trim() == "")
            {
                //  MsMessageCommon = "Please enter Vendor Name";
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 303, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrProVendor.SetError(TxtVendorName, MstrMessageCommon.Replace("#", "").Trim());
                LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmVendor.Enabled = true;
                TbAssociates.SelectedTab = TbGeneral;
                TxtVendorName.Focus();
                return false;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                //MsMessageCommon = "Please select a supplier Currency";
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 6, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrProVendor.SetError(cboCurrency, MstrMessageCommon.Replace("#", "").Trim());
                LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmVendor.Enabled = true;
                TbAssociates.SelectedTab = TbGeneral;
                cboCurrency.Focus();
                return false;
            }


            if (MintVendorType == (int)VendorType.Supplier)
            {
                //if (Convert.ToInt32(CboSupClassification.SelectedValue) == 0)
                //{
                //    //MsMessageCommon = "Please select a Supplier Classification";

                //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 322, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmVendor.Enabled = true;
                //    TbAssociates.SelectedTab = TbGeneral;
                //    CboSupClassification.Focus();
                //    return false; 
                //}
                //if (Convert.ToInt32(CboTransactiontype.SelectedValue) == 0)
                //{
                //    //MsMessageCommon = "Please select Transaction Type";
                //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 305, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    ErrProVendor.SetError(CboTransactiontype, MstrMessageCommon.Replace("#", "").Trim());
                //    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmVendor.Enabled = true;
                //    TbAssociates.SelectedTab = TbGeneral;
                //    CboTransactiontype.Focus();
                //    return false;
                //}
                //if (Convert.ToInt32(CboBankName.SelectedValue) == 0 )
                //{
                //    //MsMessageCommon = "Please select a Bank";

                //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 321, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    ErrProVendor.SetError(CboBankName, MstrMessageCommon.Replace("#", "").Trim());
                //    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmVendor.Enabled = true;
                //    TbAssociates.SelectedTab = TbGeneral;
                //    CboBankName.Focus();
                //    return false;
                //}

                //if (Convert.ToInt32(cboBankBranch.SelectedValue) == 0 && (Convert.ToInt32(CboBankName.SelectedValue) != 0))
                //{
                //    //MsMessageCommon = "Please select a Bank Branch";
                //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 306, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    ErrProVendor.SetError(cboBankBranch, MstrMessageCommon.Replace("#", "").Trim());
                //    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmVendor.Enabled = true;
                //    TbAssociates.SelectedTab = TbGeneral;
                //    cboBankBranch.Focus();
                //    return false;
                //}
                
                //--------------------------------Checking Duplicate Values In Nearest Competittor Grid-----------------------------------------------//
                int iTempID = 0;
                iTempID = CheckDuplicationInGrid(dgvNearestCompetitor,0,1);

                if (iTempID != -1)
                {
                    //MstrMessageCommon = "Please Check Duplication Of Values";
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages,326, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    dgvNearestCompetitor.Focus();
                    dgvNearestCompetitor.CurrentCell = dgvNearestCompetitor[0,iTempID];
                    return false;
                }


            }
            else
            {
                //if (Convert.ToInt32(CboBankName.SelectedValue) == 0 && Convert.ToInt32(CboTransactiontype.SelectedValue) != 2)
                //{
                //    //MsMessageCommon = "Please select a Bank";
                //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 321, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    ErrProVendor.SetError(CboBankName, MstrMessageCommon.Replace("#", "").Trim());
                //    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmVendor.Enabled = true;
                //    TbAssociates.SelectedTab = TbGeneral;
                //    CboBankName.Focus();
                //    return false;
                //}

                //if (Convert.ToInt32(cboBankBranch.SelectedValue) == 0 && (Convert.ToInt32(CboBankName.SelectedValue) != 0))
                //{
                //    //MsMessageCommon = "Please select a Bank Branch";
                //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 306, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    ErrProVendor.SetError(cboBankBranch, MstrMessageCommon.Replace("#", "").Trim());
                //    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmVendor.Enabled = true;
                //    TbAssociates.SelectedTab = TbGeneral;
                //    cboBankBranch.Focus();
                //    return false;
                //}
                
            }


            if (ClsCommonSettings.PblnIsCustLoginRequired == true)
            {
                if (MintVendorType == (int)VendorType.Customer)
                {
                    if (TxtEmail.Text.Trim() == "")
                    {   //Please enter Email
                        MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 328, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        ErrProVendor.SetError(TxtEmail, MstrMessageCommon.Replace("#", "").Trim());
                        LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmVendor.Enabled = true;
                        TbAssociates.SelectedTab = TbGeneral;
                        TxtEmail.Focus();
                        return false;
                    }
                 
                }
            }
            if (Convert.ToInt32(CboStatus.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select a Status";
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 327, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrProVendor.SetError(CboStatus, MstrMessageCommon.Replace("#", "").Trim());
                LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmVendor.Enabled = true;
                TbAssociates.SelectedTab = TbGeneral;
                CboStatus.Focus();
                return false;
            }
            //if (TxtAccountNo.Text.Trim() == "" &&  (Convert.ToInt32(CboTransactiontype.SelectedValue)!= 2))
            //{
            //    //  MsMessageCommon = "Please enter Code";
            //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages,346, out MmessageIcon);
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            //    ErrProVendor.SetError(TxtAccountNo, MstrMessageCommon.Replace("#", "").Trim());
            //    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            //    TmVendor.Enabled = true;
            //    TbAssociates.SelectedTab = TbGeneral;
            //    TxtAccountNo.Focus();
            //    return false;
            //}
            if (ClsCommonSettings.PblnAutoGeneratePartyAccount == false)
            {
                //if (CboVendorType.Text.Trim() == "Customer" || CboVendorType.Text.Trim() == "Supplier")
                //{
                    if (Convert.ToInt32(cboAccountHead.SelectedValue) == 0 )
                    {
                        MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 313, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        ErrProVendor.SetError(cboAccountHead, MstrMessageCommon.Replace("#", "").Trim());
                        LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmVendor.Enabled = true;
                        TbAssociates.SelectedTab = TbGeneral;
                        cboAccountHead.Focus();
                        return false;
                    }

                //}
                if (Convert.ToInt32(cboAccountHead.SelectedValue) == 0 )
                    //&& Convert.ToString(cboAccountHead.Text) != "")
                {
                    //MsMessageCommon = "Please select Bank Name";

                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 313, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErrProVendor.SetError(cboAccountHead, MstrMessageCommon.Replace("#", "").Trim());
                    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmVendor.Enabled = true;
                    TbAssociates.SelectedTab = TbGeneral;
                    cboAccountHead.Focus();
                    return false;
                }
            }
            if (TxtEmail.Text.Trim().Length > 0 && !MobjClsBLLVendorInformation.CheckValidEmail(TxtEmail.Text.Trim()))
            {
                //MsMessageCommon = "Please enter valid Email";
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 312, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrProVendor.SetError(TxtEmail, MstrMessageCommon.Replace("#", "").Trim());
                LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmVendor.Enabled = true;
                TbAssociates.SelectedTab = TbContact;
                TxtEmail.Focus();
                return false;
            }
            if (btnAddress.Text == "Permanent")
            {
                //if (TxtContactPerson.Text.Trim() == "")
                //{
                //    // MsMessageCommon = "Please enter Contact Person";
                //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 307, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    ErrProVendor.SetError(TxtContactPerson, MstrMessageCommon.Replace("#", "").Trim());
                //    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmVendor.Enabled = true;
                //    TbAssociates.SelectedTab = TbContact;
                //    TxtContactPerson.Focus();
                //    return false;
                //}
                //if (TxtAddress.Text.Trim() == "")
                //{
                //    // MsMessageCommon = "Please enter Address";
                //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 308, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    ErrProVendor.SetError(TxtAddress, MstrMessageCommon.Replace("#", "").Trim());
                //    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmVendor.Enabled = true;
                //    TbAssociates.SelectedTab = TbContact;
                //    TxtAddress.Focus();
                //    return false;
                //}
              

                //if (Convert.ToInt32(cboCountry.SelectedValue) == 0)
                //{
                //    // MsMessageCommon = "Please select  Country";
                //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 309, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    ErrProVendor.SetError(cboCountry, MstrMessageCommon.Replace("#", "").Trim());
                //    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmVendor.Enabled = true;
                //    TbAssociates.SelectedTab = TbContact;
                //    cboCountry.Focus();
                //    return false;
                //}
            }
            else
            {
                //if (TxtAddressName.Text.Trim() == "")
                //{
                //    // MsMessageCommon = "Please enter AddressName";
                //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 310, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    ErrProVendor.SetError(TxtAddressName, MstrMessageCommon.Replace("#", "").Trim());
                //    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmVendor.Enabled = true;
                //    TbAssociates.SelectedTab = TbContact;
                //    TxtAddressName.Focus();                  
                //    return false;
                //}
            }

            //if (Convert.ToInt32(cboCountry.SelectedValue) == 0)
            //{
            //    // MsMessageCommon = "Please select  Nationality";
            //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 309, out MmessageIcon);
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            //    ErrProVendor.SetError(cboCountry, MstrMessageCommon.Replace("#", "").Trim());
            //    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            //    TmVendor.Enabled = true;
            //    TbAssociates.SelectedTab = TbContact;
            //    cboCountry.Focus();
            //    return false;
            //}
          
            if (!TxtCode.ReadOnly && MobjClsBLLVendorInformation.CheckDuplication(MblnAddStatus, new string[] { TxtCode.Text.Replace("'", "").Trim() }, MintCurrentVendorID,MintVendorType))
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 311, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrProVendor.SetError(TxtCode, MstrMessageCommon.Replace("#", "").Trim());
                LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmVendor.Enabled = true;
                TbAssociates.SelectedTab = TbGeneral;
                TxtCode.Focus();
                return false;
            }
           
          
           

            return true;
        }

        private bool VendorAddressValidation()
        {
           
            MintCurrentAddressID = MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorAddID;
            if (TxtAddressName.Text.Trim() == "Permanent")
            {
               if (MobjClsBLLVendorInformation.IsPermanentAddressExist(MblnAddStatus, new string[] { TxtAddressName.Text.Replace("'", "").Trim() }, MintCurrentVendorID, MintCurrentAddressID))
                {
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 318, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErrProVendor.SetError(TxtAddressName, MstrMessageCommon.Replace("#", "").Trim());
                    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmVendor.Enabled = true;
                    TbAssociates.SelectedTab = TbContact;
                    TxtAddressName.Focus();
                    return false;
                }
            }
            return true;
        }
        #endregion

        private int CheckDuplicationInGrid(DataGridView Grd, int ColIndexfirst, int ColIndexsecond)
        {
            //function for checking duplication in Nearest Compettitor grid
            string SearchValuefirst = "";
            string SearchValuesecond = "";
            int RowIndexTemp = 0;
            foreach (DataGridViewRow rowValue in Grd.Rows)
            {
                if (rowValue.Cells[ColIndexfirst].Value != null)
                {
                    SearchValuefirst = Convert.ToString(rowValue.Cells[ColIndexfirst].Value);
                    SearchValuesecond = Convert.ToString(rowValue.Cells[ColIndexsecond].Value);
                    RowIndexTemp = rowValue.Index;

                    foreach (DataGridViewRow row in Grd.Rows)
                    {
                        if (RowIndexTemp != row.Index)
                        {
                            if (row.Cells[ColIndexfirst].Value != null)
                            {
                                if (Convert.ToString(row.Cells[ColIndexsecond].Value).Trim() != null)
                                {
                                    if (Convert.ToString(row.Cells[ColIndexsecond].Value).Trim() != null)
                                    {
                                        if ((Convert.ToString(row.Cells[ColIndexfirst].Value).Trim() == Convert.ToString(SearchValuefirst).Trim()) && (Convert.ToString(row.Cells[ColIndexsecond].Value).Trim() == Convert.ToString(SearchValuesecond).Trim()))
                                            return row.Index;
                                    }

                                    if (Convert.ToString(row.Cells[ColIndexsecond].Value).Trim() == null)
                                    {
                                        if (Convert.ToString(row.Cells[ColIndexfirst].Value).Trim() == SearchValuefirst.Trim())
                                            return row.Index;
                                    }
                                }
                            }
                        }
                    }
                }
            }
          return -1;
        }
        private void FillVendorInfoParameters()
        {
            MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = MintCurrentVendorID;
            MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorType = MintVendorType;


            if (MintVendorType == (int)VendorType.Customer && ClsCommonSettings.PblnAutoGeneratePartyAccount == true)
            {
                MobjClsBLLVendorInformation.clsDTOVendorInformation.strVendorCode = TxtCode.Text.Trim(); ;
                MobjClsBLLVendorInformation.clsDTOVendorInformation.strVendorName = TxtVendorName.Text.Trim();
                MobjClsBLLVendorInformation.clsDTOVendorInformation.intGLAccountID = (int)AccountGroups.SundryDebtors;
                MobjClsBLLVendorInformation.clsDTOVendorInformation.chrBSOrPL = Convert.ToChar("B");
                MobjClsBLLVendorInformation.clsDTOVendorInformation.blnPredefined = Convert.ToBoolean("False");
                MobjClsBLLVendorInformation.clsDTOVendorInformation.intCompanyID = 0;
                MobjClsBLLVendorInformation.clsDTOVendorInformation.dblOpeningBalance =0.000;

            }
            else if (MintVendorType == (int)VendorType.Supplier && ClsCommonSettings.PblnAutoGeneratePartyAccount == true)
            {
                MobjClsBLLVendorInformation.clsDTOVendorInformation.strVendorCode = TxtCode.Text.Trim(); ;
                MobjClsBLLVendorInformation.clsDTOVendorInformation.strVendorName = TxtVendorName.Text.Trim();
                MobjClsBLLVendorInformation.clsDTOVendorInformation.intGLAccountID = (int)AccountGroups.SundryCreditors;
                MobjClsBLLVendorInformation.clsDTOVendorInformation.chrBSOrPL = Convert.ToChar("B");
                MobjClsBLLVendorInformation.clsDTOVendorInformation.blnPredefined = Convert.ToBoolean("False");
                MobjClsBLLVendorInformation.clsDTOVendorInformation.intCompanyID = 0;
                MobjClsBLLVendorInformation.clsDTOVendorInformation.dblOpeningBalance = 0.000;

            }
            if (cboCurrency.SelectedIndex != -1)
            {
                MobjClsBLLVendorInformation.clsDTOVendorInformation.intSupplierCurrencyID = cboCurrency.SelectedValue.ToInt32();
            }
            else
            {
                MobjClsBLLVendorInformation.clsDTOVendorInformation.intSupplierCurrencyID = 0;
            }
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strVendorCode = TxtCode.Text.Trim(); 
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strVendorCode = TxtCode.Text.Trim();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strVendorName = TxtVendorName.Text.Trim();
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.strShortName = TxtShortName.Text.Trim();
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.intTransType = Convert.ToInt32(CboTransactiontype.SelectedValue);
            MobjClsBLLVendorInformation.clsDTOVendorInformation.intBankID = Convert.ToInt32(CboBankName.SelectedValue);
            MobjClsBLLVendorInformation.clsDTOVendorInformation.intBranchID = Convert.ToInt32(cboBankBranch.SelectedValue);
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strAccountNumber = TxtAccountNo.Text;
            MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = MintCurrentVendorID;
            MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorType = MintVendorType;
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strTRNNo = txtTRN.Text.Trim();

            //MobjClsBLLVendorInformation.clsDTOVendorInformation.decSalaryOrIncome = txtSalaryIncome.Text.ToDecimal();
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.strVendorCode = TxtCode.Text.Trim();
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.strVendorName = TxtVendorName.Text.Trim();
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.strShortName = TxtShortName.Text.Trim();
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.intTransType = Convert.ToInt32(CboTransactiontype.SelectedValue);
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.strAccountNumber = TxtAccountNo.Text;
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.intEmploymentTypeID = Convert.ToInt32(cboEmploymentType.SelectedValue);
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.intDepartmentID = Convert.ToInt32(cboDepartment.SelectedValue);
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.intDesignationID = Convert.ToInt32(cboDesignation.SelectedValue);
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.intAccountTypeID = Convert.ToInt32(cboAccountType.SelectedValue);
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.strGender = rdbMale.Checked == true ? "Male" : "Female";
            
            
            
            
            MobjClsBLLVendorInformation.clsDTOVendorInformation.intStatusID = Convert.ToInt32(CboStatus.SelectedValue);
            MobjClsBLLVendorInformation.clsDTOVendorInformation.dblRatting = txtRating.Text.ToDouble();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strRatingRemarks = txtRatingRemarks.Text;

            MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorClassificationID = Convert.ToInt32(CboSupClassification.SelectedValue);

            MobjClsBLLVendorInformation.clsDTOVendorInformation.decCreditLimit = txtCreditLimit.Text.ToDecimal();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.intCreditDaysLimit = txtCreditLimitDays.Text.ToInt32();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.decAdvanceLimit = txtAdvanceLimit.Text.ToDecimal();
            if (cboAccountHead.SelectedIndex != -1 && ClsCommonSettings.PblnAutoGeneratePartyAccount==false)
                MobjClsBLLVendorInformation.clsDTOVendorInformation.intAccountHead = Convert.ToInt32(cboAccountHead.SelectedValue);

            MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorClassificationID = Convert.ToInt32(CboSupClassification.SelectedValue);
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.strAddressName = cboLocationType.Text.Trim();
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.strContactPerson = TxtContactPerson.Text.Trim();
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.intCountry = Convert.ToInt32(cboCountry.SelectedValue);
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.strAddress = TxtAddress.Text.Trim();
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.strTelephoneNo = TxtTelephone.Text.Trim();

            //MobjClsBLLVendorInformation.clsDTOVendorInformation.strFax = txtFax.Text.Trim();
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.strState = TxtState.Text.Trim();
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.strZipCode = TxtZipCode.Text.Trim();

            MobjClsBLLVendorInformation.clsDTOVendorInformation.strEmail = TxtEmail.Text.Trim();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strWebsite = TxtWebsite.Text.Trim();

            if (cboSourceMedia.SelectedIndex != -1)
                MobjClsBLLVendorInformation.clsDTOVendorInformation.intSourceMediaID = cboSourceMedia.SelectedValue.ToInt32();
        }


        private bool SaveVendorInfo()
        {
            try
            {
                if (MblnAddStatus == true)
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1, out MmessageIcon);
                else
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 3, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;

                if (MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID == 0 && MobjClsBLLVendorInformation.CheckDuplication(MblnAddStatus, new string[] { TxtCode.Text.Replace("'", "").Trim() }, MintCurrentVendorID, MintVendorType))
                {
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 9002, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("* no", "Code");
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        GenerateCode(MintVendorType);
                }
                else if (MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID >0 && MobjClsBLLVendorInformation.CheckDuplication(MblnAddStatus, new string[] { TxtCode.Text.Replace("'", "").Trim() }, MintCurrentVendorID, MintVendorType))
                {
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 311, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErrProVendor.SetError(TxtCode, MstrMessageCommon.Replace("#", "").Trim());
                    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmVendor.Enabled = true;
                    TbAssociates.SelectedTab = TbGeneral;
                    TxtCode.Focus();
                    return false;
                }
                FillVendorInfoParameters();
                if (MintVendorType == (int)VendorType.Supplier)
                {
                    FillNearestCompetitor();
                    
                }
                if (MblnAddStatus == true)
                {
                    ////if (MobjClsBLLVendorInformation.AddVendorInfo())

                    if (ClsCommonSettings.PblnAutoGeneratePartyAccount==true)
                    {                     
                        MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = MobjClsBLLVendorInformation.AddVendorInfo();
                        {
                            ResetForm(0);
                            return true;
                        }
                    }
                    else
                    {
                        MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = MobjClsBLLVendorInformation.AddVendorInfoRegular();
                        {
                            ResetForm(0);
                            return true;
                        }

                    }
                }
                else
                {
                    if (MobjClsBLLVendorInformation.UpdateVendorInfo())
                    {
                        ResetForm(0);
                        return true;
                    }
                }
                return false;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on Save:SaveVendorInfo() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Saving Vendor Info()" + Ex.Message.ToString());

                return false;
            }
        }

        private void FillVendorPermanentAddressParameters()
        {
            MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = MintCurrentVendorID;
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strContactPerson = TxtContactPerson.Text.Trim();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.intCountry = Convert.ToInt32(cboCountry.SelectedValue);
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strAddress = TxtAddress.Text.Trim();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strTelephoneNo = TxtTelephone.Text.Trim();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strMobile1 = txtMobileNo.Text.Trim();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strFax = txtFax.Text.Trim();
        }

        private bool SaveVendorPermanentAddress()
        {
            try
            {
                FillVendorPermanentAddressParameters();

                if (MblnAddStatus == true)
                {
                    MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = MobjClsBLLVendorInformation.AddVendorInfo();
                    ResetForm(0);
                    return true;
                    
                }
                else
                {
                    if (MobjClsBLLVendorInformation.UpdateVendorInfo())
                    {
                        ResetForm(0);
                        return true;
                    }
                }
                return false;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on Save:SaveVendorPermanentAddress() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Saving vendor permanent address() " + Ex.Message.ToString());

                return false;
            }
        }

        private void ResetForm(int intType)
        {
            // 0 - For VenderInfo Tab
            // 1 - For VenderAddress Tab

            MintCurrentVendorID = MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID;
            MobjClsLogWriter.WriteLog("Saved successfully:SaveVendorInfo()  " + this.Name + "", 0);

            if (intType == 0)
            {
                ErrProVendor.Clear();
                EnableDisableButtons();
            }
        }

        private void FillVendorAddressParameters()
        {
            MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = MintCurrentVendorID;
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strAddressName = TxtAddressName.Text.Trim();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strContactPerson = TxtContactPerson.Text.Trim();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.intCountry = cboCountry.SelectedValue.ToInt32();

            if (MobjClsBLLVendorInformation.clsDTOVendorInformation.intCountry == 0)
            {
                DataTable datTemp = MobjClsBLLCommonUtility.FillCombos(new string[] { "CountryID", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID });
                
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        MobjClsBLLVendorInformation.clsDTOVendorInformation.intCountry = datTemp.Rows[0]["CountryID"].ToInt32();
                }
            }
            
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strAddress = TxtAddress.Text.Trim() == "" ? "Defualt" : TxtAddress.Text.Trim();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strTelephoneNo = TxtTelephone.Text.Trim();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strFax = txtFax.Text.Trim();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strState = TxtState.Text.Trim();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strZipCode = TxtZipCode.Text.Trim();
            MobjClsBLLVendorInformation.clsDTOVendorInformation.strMobile1 = txtMobileNo.Text.Trim();
        }

        private bool SaveVendorAddressInfo()
        {
            try
            {
                FillVendorAddressParameters();

                if (MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorAddID == 0)
                {
                    if (MobjClsBLLVendorInformation.AddAddress())
                    {
                        ResetForm(1);
                        return true;
                    }
                }
                else
                {
                    if (MobjClsBLLVendorInformation.UpdateAddress())
                    {
                        ResetForm(1);
                        return true;
                    }
                }
                return false;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on Save:SaveVendorAddressInfo() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on SaveVendorAddressInfo() " + Ex.Message.ToString());

                return false;
            }
        }

        private void TmVendor_Tick(object sender, EventArgs e)
        {
            TmVendor.Enabled = false;
            LblVendorStatus.Text = "";
        }

    //private void CboVendorType_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    FillAccountHeads();
    //    int mintvendortype = Convert.ToInt32(CboVendorType.SelectedValue);
    //    GenerateCode(mintvendortype);
    //    cboAccountHead.SelectedIndex = -1;
    //    if (Convert.ToInt32(CboVendorType.SelectedValue) == (int)VendorType.Customer && ClsCommonSettings.PblnAutoGeneratePartyAccount == false)
    //    {       
    //            LblAccountHead.Enabled = true;
    //            cboAccountHead.Enabled = true;
    //            BtnAccountHead.Enabled = true;
    //            cboAccountHead.BackColor = SystemColors.Info;
                
    //    }

    //    else if (Convert.ToInt32(CboVendorType.SelectedValue) == (int)VendorType.Supplier && ClsCommonSettings.PblnAutoGeneratePartyAccount == false)
    //    {
    //        LblAccountHead.Enabled = true;
    //        cboAccountHead.Enabled = true;
    //        BtnAccountHead.Enabled = true;
    //        cboAccountHead.BackColor = SystemColors.Info;
    //    }
    //    //else
    //    //{
    //    //    LblAccountHead.Enabled = false;
    //    //    cboAccountHead.Enabled = false;
    //    //    BtnAccountHead.Enabled = false;
    //    //    cboAccountHead.BackColor = SystemColors.Window;
    //    //    TxtMobile1.BackColor = SystemColors.Window;
    //    //    cboLocationType.BackColor = SystemColors.Window;

    //    //}

    //    if (Convert.ToInt32(CboVendorType.SelectedValue) == (int)VendorType.Customer && ClsCommonSettings.PblnAutoGeneratePartyAccount == true)
    //    {
    //        LblAccountHead.Enabled = false;
    //        cboAccountHead.Enabled = false;
    //        BtnAccountHead.Enabled = false;
    //    }
    //    else if (Convert.ToInt32(CboVendorType.SelectedValue) == (int)VendorType.Supplier && ClsCommonSettings.PblnAutoGeneratePartyAccount == true)
    //    {
    //        LblAccountHead.Enabled = false;
    //        cboAccountHead.Enabled = false;
    //        BtnAccountHead.Enabled = false;
    //    }
       
    //    Changestatus(true);
    //}


        private void FillAccountHeads()
        {
            DataTable datAccountHeads = new DataTable();

            MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorType = MintVendorType;
            datAccountHeads = MobjClsBLLVendorInformation.AccountDetails();

            cboAccountHead.ValueMember = "AccountID";
            cboAccountHead.DisplayMember = "Account";
            cboAccountHead.DataSource = datAccountHeads;
        }

        private void cboCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus(true);
        }

        private void TxtAddress_TextChanged(object sender, EventArgs e)
        {
            Changestatus(true);
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            try
            {
                MblnIsEditMode = true;
                bool blnFirstItem=true;
                ErrProVendor.Clear();
                if (MintRowNumber > 0)
                {
                    if (blnFirstItem)
                        MintRowNumber = 1;
                    else
                        MintRowNumber--;
                    EventArgs c = new EventArgs();
                    BtnSearch_Click(BtnSearch, c);
                }
                else
                {
                    MintTotalRecCnt = MobjClsBLLVendorInformation.RecCountNavigate(MintVendorType);
                    if (MintTotalRecCnt > 0)
                    {
                        MblnAddStatus = false;
                        MintCurrentRecCnt = 1;
                        BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                        //BtnReferences.Enabled = true;
                    }
                    else
                    {
                        BindingNavigatorCountItem.Text = "of 1";
                        BindingNavigatorPositionItem.Text = "1";
                        MblnAddStatus = true;
                        MintCurrentRecCnt = MintTotalRecCnt = 0;
                        return;
                    }

                    DisplayInfo();
                    SetEnableAddress();
                    AddToAddressMenu();
                    tlsBtnAddressSave.Enabled = MblnUpdatePermission;
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintTotalRecCnt) + "";
                    SetBindingNavigatorButtons();
                    setBNButtons();
                    TxtSearchText.Clear();
                    CboSearchOption.SelectedIndex = -1;
                    MintRowNumber = 0;
                }
                TbAssociates.SelectedTab = TbGeneral;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on MoveFirstItem:MoveFirstItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveFirstItem_Click() " + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            try
            {
                MblnIsEditMode = true;
                bool blnFirstItem = false;
                if (MintRowNumber > 0)
                {
                    if (blnFirstItem)
                        MintRowNumber = 1;
                    else
                        MintRowNumber--;
                    EventArgs c = new EventArgs();
                    BtnSearch_Click(BtnSearch, c);
                }
                else
                {
                    MintTotalRecCnt = MobjClsBLLVendorInformation.RecCountNavigate(MintVendorType);
                    if (MintTotalRecCnt > 0)
                    {
                        MblnAddStatus = false;
                        MintCurrentRecCnt = MintCurrentRecCnt - 1;
                        BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                       // BtnReferences.Enabled = true;

                        if (MintCurrentRecCnt <= 0)
                            MintCurrentRecCnt = 1;
                    }
                    else
                    {
                        BindingNavigatorCountItem.Text = "of 1";
                        BindingNavigatorPositionItem.Text = "1";
                        MblnAddStatus = true;
                        MintCurrentRecCnt = MintTotalRecCnt = 0;
                        return;
                    }

                    DisplayInfo();
                    SetEnableAddress();
                    AddToAddressMenu();
                    SetBindingNavigatorButtons();
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintTotalRecCnt) + "";
                    SetBindingNavigatorButtons();
                    setBNButtons();
                    TxtSearchText.Clear();
                    CboSearchOption.SelectedIndex = -1;
                    MintRowNumber = 0;
                }
                TbAssociates.SelectedTab = TbGeneral;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on MovePreviousItem:MovePreviousItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MovePreviousItem_Click() " + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
             try
            {
                MblnIsEditMode = true;
                bool bLastItem = false;
                if (MintRowNumber > 0)    // Getting next item in the search results
                {
                    if (bLastItem)
                    {
                        MintRowNumber = MintTotalRecCnt;
                    }
                    else if (Convert.ToInt32(BindingNavigatorPositionItem.Text) < MintTotalRecCnt)
                    {
                        MintRowNumber++;
                    }
                    EventArgs c = new EventArgs();
                    BtnSearch_Click(BtnSearch, c);

                }
                else
                {
                    MintTotalRecCnt = MobjClsBLLVendorInformation.RecCountNavigate(MintVendorType);
                    if (MintTotalRecCnt > 0)
                    {
                        MintCurrentRecCnt = MintCurrentRecCnt + 1;
                        MblnAddStatus = false;
                        BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                        //BtnReferences.Enabled = true;

                        if (MintCurrentRecCnt >= MintTotalRecCnt)
                            MintCurrentRecCnt = MintTotalRecCnt;
                    }
                    else
                    {
                        BindingNavigatorCountItem.Text = "of 1";
                        BindingNavigatorPositionItem.Text = "1";
                        MblnAddStatus = true;
                        MintCurrentRecCnt = MintTotalRecCnt = 0;
                        return;
                    }

                    DisplayInfo();
                    SetEnableAddress();
                    AddToAddressMenu();
                    SetBindingNavigatorButtons();

                    BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintTotalRecCnt) + "";
                    SetBindingNavigatorButtons();
                    setBNButtons();
                    TxtSearchText.Clear();
                    CboSearchOption.SelectedIndex = -1;
                    MintRowNumber = 0;
                }
                TbAssociates.SelectedTab = TbGeneral;

            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on MoveNextItem:MoveNextItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveNextItem_Click() " + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            try
            {
                MblnIsEditMode = true;
                ErrProVendor.Clear();
                bool bLastItem = true;
                if (MintRowNumber > 0)    // Getting next item in the search results
                {
                    if (bLastItem)
                    {
                        MintRowNumber = MintTotalRecCnt;
                    }
                    else if (Convert.ToInt32(BindingNavigatorPositionItem.Text) < MintTotalRecCnt)
                    {
                        MintRowNumber++;
                    }
                    EventArgs c = new EventArgs();
                    // Calling searching event
                    BtnSearch_Click(BtnSearch, c);
                }
                else
                {
                    MintTotalRecCnt = MobjClsBLLVendorInformation.RecCountNavigate(MintVendorType);
                    if (MintTotalRecCnt > 0)
                    {
                        MintCurrentRecCnt = MintTotalRecCnt;
                        BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                        //BtnReferences.Enabled = true;
                        MblnAddStatus = false;
                    }
                    else
                    {
                        BindingNavigatorCountItem.Text = "of 1";
                        BindingNavigatorPositionItem.Text = "1";
                        MblnAddStatus = true;
                        MintCurrentRecCnt = MintTotalRecCnt = 0;
                        return;
                    }

                    DisplayInfo();
                    SetEnableAddress();
                    AddToAddressMenu();
                    SetBindingNavigatorButtons();

                    BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintTotalRecCnt) + "";
                    SetBindingNavigatorButtons();
                    setBNButtons();
                    TxtSearchText.Clear();
                    CboSearchOption.SelectedIndex = -1;
                    MintRowNumber = 0;
                }
                TbAssociates.SelectedTab = TbGeneral;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on MoveLastItem:MoveLastItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveLastItem_Click() " + Ex.Message.ToString());
            }
        }

        /// <summary>
        /// Enable/disable buttons according to the action
        /// </summary>
       private void EnableDisableButtons()
       {
            //TlsBtnAddressDelete.Enabled = BindingNavigatorDeleteItem.Enabled =MblnDeletePermission;
            // = ((MblnAddUpdatePermission && MblnChangeStatus) == true) ? true : false;
            //* BtnOk.Enabled = *BtnSave.Enabled =((MblnAddUpdatePermission && MblnChangeStatus) == true) ? true : false;
            //*BtnPrint.Enabled = *EmailStripButton.Enabled = ((MblnPrintEmailPermission && !MblnAddStatus) == true) ? true : false;
            //*btnActions.Enabled = ((MblnUpdatePermission && !MblnAddStatus && MblnPrintEmailPermission) == true) ? true : false;
           if (!MblnIsEditMode)
           {
               BtnOk.Enabled = BtnSave.Enabled = tlsBtnAddressSave.Enabled = MblnAddPermission ? true : false; ;
               VendorInformationBindingNavigatorSaveItem.Enabled = CancelToolStripButton.Enabled = MblnAddPermission ? true : false; ;
           }
           else
           {
               BtnPrint.Enabled =BtnVendorMapping.Enabled = EmailStripButton.Enabled = MblnPrintEmailPermission ? true : false; ;
               VendorInformationBindingNavigatorSaveItem.Enabled = BtnOk.Enabled = BtnSave.Enabled = MblnUpdatePermission ? true : false; ;
               btnActions.Enabled = MblnUpdatePermission ? true : false; ;
               TlsBtnAddressDelete.Enabled = BindingNavigatorDeleteItem.Enabled = MblnDeletePermission ? true : false; ;
               BindingNavigatorAddNewItem.Enabled = MblnAddPermission ? true : false; ;
           }
           if (MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID == 0)
           {
               tlsBtnAddressSave.Enabled = false;
           }
           else
           {
               tlsBtnAddressSave.Enabled = MblnUpdatePermission;
           }
        }

        private void DisplayInfo()
        {
            if (MobjClsBLLVendorInformation.FindVendorInfo(MintCurrentRecCnt,MintVendorType))
            {
                ClearVenderInfo();
                GetVendorInformations();
            }
        }
        private void GetVendorInformations()
        {
            FillAccountHeads();
            MintCurrentVendorID = MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID;
            //MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorAddID = 0;
            //CboVendorType.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorType;
            //CboVendorType.Enabled = false;
            CboSupClassification.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorClassificationID;
            if( MobjClsBLLVendorInformation.clsDTOVendorInformation.intSupplierCurrencyID != 0)
            {
                cboCurrency.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intSupplierCurrencyID;
                bool result = MobjClsBLLVendorInformation.CheckVendorAlreadyUsed();//to set currency Enability
                if (result)
                    cboCurrency.Enabled = false;
                else
                    cboCurrency.Enabled = true;
            }
            else
            {
                cboCurrency.SelectedIndex = -1;
            }


            txtTRN.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strTRNNo;

            //Btnvendortype.Enabled = false;
            TxtCode.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strVendorCode;
            TxtVendorName.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strVendorName;
            //TxtShortName.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strShortName;
            //CboTransactiontype.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intTransType;
            CboBankName.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intBankID;
            cboBankBranch.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intBranchID;
            TxtAccountNo.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strAccountNumber;
            cboAccountHead.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intAccountHead;
            TxtContactPerson.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strContactPerson;
            cboCountry.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intCountry;
            TxtAddress.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strAddress;
            TxtTelephone.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strTelephoneNo;
            txtMobileNo.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strMobile1;
            TxtState.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strState;
            TxtZipCode.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strZipCode;
            txtFax.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strFax;
            TxtEmail.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strEmail;
            TxtWebsite.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strWebsite;
            TxtAddressName.Text = "Permanent";
            btnAddress.Text = "Permanent";
            //cboEmploymentType.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intEmploymentTypeID;
            //cboDepartment.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intDepartmentID;
            //cboDesignation.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intDesignationID;
            //cboAccountType.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intAccountTypeID;
            //txtSalaryIncome.Text = Convert.ToString(MobjClsBLLVendorInformation.clsDTOVendorInformation.decSalaryOrIncome);
            //rdbFemale.Checked = MobjClsBLLVendorInformation.clsDTOVendorInformation.strGender == "Male" ? false : true;
            //rdbMale.Checked = !rdbFemale.Checked;
            CboStatus.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intStatusID;
            txtRating.Text = Convert.ToString(MobjClsBLLVendorInformation.clsDTOVendorInformation.dblRatting);
            txtRatingRemarks.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strRatingRemarks;
            
            txtCreditLimit.Text= Convert.ToString(MobjClsBLLVendorInformation.clsDTOVendorInformation.decCreditLimit); 
            txtAdvanceLimit.Text= Convert.ToString(MobjClsBLLVendorInformation.clsDTOVendorInformation.decAdvanceLimit) ;
            txtCreditLimitDays.Text = Convert.ToString(MobjClsBLLVendorInformation.clsDTOVendorInformation.intCreditDaysLimit);
            cboSourceMedia.SelectedValue = MobjClsBLLVendorInformation.clsDTOVendorInformation.intSourceMediaID;
            txtTRN.Text = MobjClsBLLVendorInformation.clsDTOVendorInformation.strTRNNo.ToStringCustom();

            FillNearestCompetitorColumn(MintCurrentVendorID);
            FillNearestCompetitorGrid();
            EnableDisableButtons();
        
        }
        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = MintCurrentVendorID;

                if (MintCurrentVendorID > 0)
                {
                    if (MobjClsBLLVendorInformation.CheckExistReferences())
                    {
                        if (MintVendorType == (int)VendorType.Customer)
                        {
                            MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 314, out MmessageIcon).Replace("*","Customer");
                        }
                        else
                        {
                            MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 314, out MmessageIcon).Replace("*", "Supplier");
                        }
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        TmVendor.Enabled = true;
                        return;
                    }
                }
                if (DeleteValidation())
                {
                    if (MobjClsBLLVendorInformation.DeleteVendorInfo())
                    {
                        MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 4, out MmessageIcon);
                        LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmVendor.Enabled = true;
                        AddNewItem();
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on DeleteItem_Click:DeleteItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteItem_Click() " + Ex.Message.ToString());
            }
        }

        private bool DeleteValidation()
        {
            MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 13, out MmessageIcon);

            if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                return false;
            else
                return true;
        }

        private void CancelToolStripButton_Click(object sender, EventArgs e)
        {
            AddNewItem();
            //ClearVenderInfo();
            //SetEnableAddress();
            //Changestatus(false);
            //TxtCode.Focus();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (VendorFormValidation())
            {
                if (SaveVendorInfo())
                {
                    SaveAddress();

                    //if (this.MblnAddStatus)
                    //    this.SendSMS(this.TxtVendorName.Text, this.TxtMobile1.Text);

                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 2, out MmessageIcon);
                    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmVendor.Enabled = true;
                    BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                    MblnChangeStatus = MblnAddStatus = false;
                    MintTotalRecCnt = MobjClsBLLVendorInformation.RecCountNavigate(MintVendorType);
                    //EnableDisableButtons();
                    //AddNewItem();
                    //PintVendorID = MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID;
                    this.Close();
                }
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            VendorInformationBindingNavigatorSaveItem_Click(sender, e);
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }
        private void ButtonStatus(object sender, EventArgs e)
        {
            Changestatus(true);            
        }
        //common strings(servername,message caption,report footer,Form caption) -- common integers( NoOfHiddenColumns,PredefinedColumn,timer value) 
        //common strings,common integers,fields(primarykey should be first index),TableName,condition

        private void Btnvendortype_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objFrmCommonRef = new FrmCommonRef("Associate Type", new int[] { 3, 3, ClsCommonSettings.TimerInterval }, "AccountTypeID,OperationModeID,Predefined,Description As VendorType", "AccountTypes", "OperationModeID=4");
                objFrmCommonRef.ShowDialog();
                objFrmCommonRef.Dispose();

                LoadCombos(1);
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Vendor Type Creation() " + Ex.Message.ToString());
            }
        }

        private void btnCountryoforgin_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objFrmCommonRef = new FrmCommonRef("Country", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "CountryID,CountryName", "CountryReference", "");
                objFrmCommonRef.ShowDialog();
                objFrmCommonRef.Dispose();
                int intCountry = Convert.ToInt32(cboCountry.SelectedValue);
                LoadCombos(4);
                if (objFrmCommonRef.NewID != 0)
                    cboCountry.SelectedValue = objFrmCommonRef.NewID;
                else
                    cboCountry.SelectedValue = intCountry;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Countryoforgin() " + Ex.Message.ToString());
            }
        }

        private void btnContextmenu_Click(object sender, EventArgs e)
        {
            CMSAssociates.Show(btnContextmenu, btnContextmenu.PointToClient(System.Windows.Forms.Cursor.Position));
            
        }

        private bool SetEnableAddress()
        {
            if (btnAddress.Text == "Permanent")
            {
                TxtAddressName.Text = "Permanent";
                //TxtAddressName.BackColor = Color.White;
                //cboCountry.BackColor = TxtAddress.BackColor = Color.LightYellow;
                TxtAddressName.Enabled = false;
            }
            else
            {
                ClearAddress();
                //TxtAddressName.BackColor = Color.LightYellow;
                //TxtContactPerson.BackColor =  TxtAddress.BackColor = Color.White;
                TxtAddressName.Enabled = true;

                //cboCountry.BackColor
            }
            return true;
        }

        private void ClearVenderInfo()
        {
            MobjClsBLLVendorInformation.clsDTOVendorInformation = new clsDTOVendorInformation();
                  //CboStatus.Text = cboEmploymentType.Text = cboDepartment.Text = cboDesignation.Text = cboAccountType.Text =  CboSupClassification.Text = "";
              //CboStatus.SelectedIndex = cboEmploymentType.SelectedIndex = cboDepartment.SelectedIndex = cboDesignation.SelectedIndex = cboAccountType.SelectedIndex =  CboSupClassification.SelectedIndex = -1;
            //TxtVendorName.Text = TxtShortName.Text = TxtAccountNo.Text = TxtEmail.Text =  TxtWebsite.Text =txtRating.Text =txtRatingRemarks.Text =txtSalaryIncome.Text=  "";
            //rdbMale.Select();
            txtAdvanceLimit.Text = txtCreditLimit.Text = txtTRN.Text = "";
          
            txtCreditLimitDays.Text = "";
            //CboTransactiontype.Text = cboAccountHead.Text = cboBankBranch.Text = CboBankName.Text = "";
            cboBankBranch.SelectedIndex =  cboCountry.SelectedIndex = cboAccountHead.SelectedIndex = -1;
            CboBankName.SelectedIndex = -1;
            cboBankBranch.DataSource = null;
            CboSupClassification.SelectedIndex=-1;

            dgvNearestCompetitor.ClearSelection();
            dgvNearestCompetitor.Rows.Clear();
            FillNearestCompetitorColumn(MintCurrentVendorID);
            TxtSearchText.Text = "";
            CboSearchOption.SelectedIndex = -1;
            cboCurrency.SelectedIndex = -1;
            CboSearchOption.Text = "";
            TxtVendorName.Text = TxtAccountNo.Text = TxtWebsite.Text = txtRating.Text = txtRatingRemarks.Text = "";
            ClearAddress();
        }

        private bool ClearAddress()
        {
            //TxtAddressName.Text = TxtContactPerson.Text = TxtAddress.Text = TxtTelephone.Text = txtFax.Text = TxtState.Text = "";
            TxtAddressName.Text = TxtContactPerson.Text = TxtAddress.Text = TxtTelephone.Text = txtFax.Text = TxtState.Text = txtMobileNo.Text =TxtEmail.Text = "";
            TxtZipCode.Text = cboCountry.Text = "";
            cboCountry.SelectedIndex = -1;
            return true;
        }

        private void tlsBtnAddressSave_Click(object sender, EventArgs e)
        {
            if (SaveAddress())
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 315, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            }
        }

        private bool SaveAddress()
        {
            try
            {
                if (TxtAddressName.Text.Trim() != "Permanent")
                {
                    if (VendorFormValidation())
                    {
                        if (SaveVendorAddressInfo())
                        {
                            AddToAddressMenu();
                            btnAddress.Text = TxtAddressName.Text.Trim();
                            return true;
                        }
                    }
                }
                else
                {
                    if (VendorAddressValidation())
                    {
                        if (SaveVendorAddressInfo())
                        {

                            AddToAddressMenu();
                            btnAddress.Text = TxtAddressName.Text.Trim();

                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form SaveAddress:SaveAddress " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on SaveAddress() " + Ex.Message.ToString());

                return false;
            }
        }

        private void TlsBtnAddressDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (TxtAddressName.Text != "Permanent")
                {
                    if (DeleteValidation())
                    {
                        if (MobjClsBLLVendorInformation.DeleteAddress())
                        {
                            ClearAddress();
                            AddToAddressMenu();
                            btnAddress.Text = "Permanent";
                            MintCnt = 0;
                            SetEnableAddress();
                            DisplayAddress();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form AddressDelete_Click:TlsBtnAddressDelete_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddressDelete_Click() " + Ex.Message.ToString());
            }
        }

        private void btnAddress_Click(object sender, EventArgs e)
        {
            try
            {
                if (MintCurrentVendorID > 0)
                {
                    if (MintCnt < CMSAssociates.Items.Count - 1)
                        MintCnt += 1;
                    else
                        MintCnt = 0;

                    btnAddress.Text = CMSAssociates.Items[MintCnt].Text;
                    MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorAddID = Convert.ToInt32(CMSAssociates.Items[MintCnt].Tag);
                    //MintCurrentAddressID = Convert.ToInt32(CMSAssociates.Items[MintCnt].Tag);
                    SetEnableAddress();
                    DisplayAddress();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on Address_Click:btnAddress_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Address_Click() " + Ex.Message.ToString());
            }
        }

        
        private void Changestatus(bool blnStatus)
        {
            ErrProVendor.Clear();
            MblnChangeStatus = blnStatus;
            EnableDisableButtons();            
        }


        /// <summary>
        /// Enables/disables binding navigator buttons according to the current position
        /// </summary>
        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintTotalRecCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }

        private void TxtShortName_TextChanged(object sender, EventArgs e)
        {
            Changestatus(true);
        }

        private void TxtCode_TextChanged(object sender, EventArgs e)
        {
            Changestatus(true);
        }

        private void TxtVendorName_TextChanged(object sender, EventArgs e)
        {
            if (MintVendorType == (int)VendorType.Customer)
            {
                TxtContactPerson.Text = TxtVendorName.Text.Trim();
            }
            Changestatus(true);
        }

        private void CboTransactiontype_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErrProVendor.Clear();
            
            //cash

            if (ClsCommonSettings.PblnAutoGeneratePartyAccount == true)
            {
                CboBankName.Enabled = cboBankBranch.Enabled = BtnBank.Enabled = btnBankName.Enabled = TxtAccountNo.Enabled = false;
                TxtAccountNo.BackColor = SystemColors.Window;
                TxtAccountNo.Enabled = false;
                cboAccountHead.Enabled = false;
                BtnAccountHead.Enabled = false;
                LblAccountHead.Enabled = false;
                TxtAccountNo.Text = "";
            }
            if ( ClsCommonSettings.PblnAutoGeneratePartyAccount == false)
            {
                CboBankName.Enabled = cboBankBranch.Enabled = BtnBank.Enabled = btnBankName.Enabled = TxtAccountNo.Enabled = false;
                TxtAccountNo.BackColor = SystemColors.Window;
                TxtAccountNo.Enabled = false;
                cboAccountHead.Enabled = true;
                BtnAccountHead.Enabled = true;
                LblAccountHead.Enabled = true;
                TxtAccountNo.Text = "";                
            }

            //Bank
            //if (ClsCommonSettings.PblnAutoGeneratePartyAccount == true)
            //{
            //    if (Convert.ToInt32(CboTransactiontype.SelectedValue) == 1 || Convert.ToInt32(CboTransactiontype.SelectedValue) == 3 || Convert.ToInt32(CboTransactiontype.SelectedValue) == 4)
            //    {
            //        CboBankName.Enabled = cboBankBranch.Enabled = BtnBank.Enabled = btnBankName.Enabled = TxtAccountNo.Enabled = TxtAccountNo.Enabled = true;
            //        TxtAccountNo.BackColor = SystemColors.Window;
            //        TxtAccountNo.Enabled = true;
            //        cboAccountHead.Enabled = false;
            //        BtnAccountHead.Enabled = false;
            //        LblAccountHead.Enabled = false;
            //    }
            //}
            //if (ClsCommonSettings.PblnAutoGeneratePartyAccount == false)
            //{

            //    if (Convert.ToInt32(CboTransactiontype.SelectedValue) == 1 || Convert.ToInt32(CboTransactiontype.SelectedValue) == 3 || Convert.ToInt32(CboTransactiontype.SelectedValue) == 4)
            //    {
            //        CboBankName.Enabled = cboBankBranch.Enabled = BtnBank.Enabled = btnBankName.Enabled = TxtAccountNo.Enabled = TxtAccountNo.Enabled = true;
            //        TxtAccountNo.BackColor = SystemColors.Window;
            //        TxtAccountNo.Enabled = true;
            //        cboAccountHead.Enabled = true;
            //        BtnAccountHead.Enabled = true;
            //        LblAccountHead.Enabled = true;
            //    }
            //}
            Changestatus(true);
        }

        private void CboBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboBankName.SelectedIndex != -1)
            {
                cboBankBranch.Text = "";
                LoadCombos(6);
                Changestatus(true);
            }
        }


        //private void BtnReferences_DropDownOpened(object sender, EventArgs e)
        //{
        //    MobjWebBrowser.DocumentText = MobjClsBLLVendorInformation.GetFormsInUse("Associates information exists in", MintCurrentVendorID, "name <>'VendorInformation' and name <>'STVendorAddress'", "VendorID");
        //}

        private void BtnBank_Click(object sender, EventArgs e)
        {
            try
            {
                int MintBankID=Convert.ToInt32(CboBankName.SelectedValue);
                using (FrmBankDetails objFrmBankDetails = new FrmBankDetails(MintBankID))
                {
                    objFrmBankDetails.ShowDialog();
                }
                LoadCombos(6);
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on Bank_Click:BtnBank_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Bank_Click() " + Ex.Message.ToString());
            }
        }

        private void BtnAccountHead_Click(object sender, EventArgs e)
        {
            try
            {
                FrmChartOfAccounts objFrmChartOfAccounts = new FrmChartOfAccounts();

                if (this.MintVendorType == (int)VendorType.Customer)
                    // Customers
                    objFrmChartOfAccounts.PermittedAccountGroup = AccountGroups.SundryDebtors ;
                else
                    // Suppliers
                    objFrmChartOfAccounts.PermittedAccountGroup = AccountGroups.SundryCreditors ;

                objFrmChartOfAccounts.ShowDialog();
                FillAccountHeads();
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form ChartOfAccounts:BtnAccountHead_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on ChartOfAccounts() " + Ex.Message.ToString());
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void LoadReport()
        {
            try
            {
                if (MintCurrentVendorID > 0)
                {
                    FrmReportviewer ObjFrmReportviewer = new FrmReportviewer();
                    ObjFrmReportviewer.PsFormName = this.Text;
                    ObjFrmReportviewer.PiRecId = MintCurrentVendorID;
                    ObjFrmReportviewer.PiFormID = (int)FormID.VendorInformation;
                    ObjFrmReportviewer.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form LoadReport:LoadReport " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport() " + Ex.Message.ToString());
            }
        }

        private void EmailStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjFrmEmailPopup = new FrmEmailPopup())
                {
                    if (MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorType > 0)
                    {
                        if (MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorType == (int)VendorType.Supplier)
                        {
                            ObjFrmEmailPopup.MsSubject = "Supplier Information";
                            ObjFrmEmailPopup.EmailFormType = EmailFormID.Vendor;
                        }
                        else
                        {
                            ObjFrmEmailPopup.MsSubject = "Customer Information";
                            ObjFrmEmailPopup.EmailFormType = EmailFormID.Vendor;
                        }
                        ObjFrmEmailPopup.EmailSource = MobjClsBLLVendorInformation.GetVendorReport();
                        ObjFrmEmailPopup.ShowDialog();
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on EmailStrip:EmailStripButton_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on EmailStrip() " + Ex.Message.ToString());
            }
        }

        private void FrmVendor_Shown(object sender, EventArgs e)
        {
            //CboVendorType.Focus();
            if (TxtCode.ReadOnly == true)
                TxtVendorName.Focus();
            else
                TxtCode.Focus();
        }


        private void SendSMS(string strCustomerName, string strMobileNo)
        {
            StringBuilder sbrMessage;
            clsSendmail clsSendSMSMail;
            string strToAddress = "";

            try
            {
                this.MobjClsBLLVendorInformation.GetMailSetting();
                if (this.MobjClsBLLVendorInformation.objclsDTOMailSetting == null) return;

                strToAddress = strMobileNo + "@" + this.MobjClsBLLVendorInformation.objclsDTOMailSetting.IncomingServer;
                sbrMessage = new StringBuilder();
                sbrMessage.Append(strCustomerName);
                sbrMessage.Append(", your registration is succesful.");

                clsSendSMSMail = new clsSendmail();
                if (!clsSendSMSMail.IsInternet_Connected())
                    return;

                clsSendSMSMail.SendSmsMail(this.MobjClsBLLVendorInformation.objclsDTOMailSetting.UserName,
                                            this.MobjClsBLLVendorInformation.objclsDTOMailSetting.PassWord,
                                            this.MobjClsBLLVendorInformation.objclsDTOMailSetting.OutgoingServer,
                                            this.MobjClsBLLVendorInformation.objclsDTOMailSetting.PortNumber,
                                            this.MobjClsBLLVendorInformation.objclsDTOMailSetting.UserName,
                                            strToAddress, "Customer Registration Confirmation",
                                            sbrMessage.ToString(),
                                            this.MobjClsBLLVendorInformation.objclsDTOMailSetting.EnableSsl);
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling sending sms:SendSMS " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on SendSMS() " + Ex.Message.ToString());
            }
        }
    
        private void btnDocuments_Click(object sender, EventArgs e)
        {
            using (frmDocumentMaster objfrmDocumentMaster = new frmDocumentMaster())
            {
                if (MintVendorType == (int)VendorType.Supplier )
                    objfrmDocumentMaster.PintOperationType = Convert.ToInt32(OperationType.Suppliers);
                else if (MintVendorType == (int)VendorType.Customer)
                    objfrmDocumentMaster.PintOperationType = Convert.ToInt32(OperationType.Customers);

                objfrmDocumentMaster.PlngOperationID = MintCurrentVendorID;
                objfrmDocumentMaster.PstrOperationNo = TxtCode.Text.Trim();
                objfrmDocumentMaster.ShowDialog();
            }
        }

        private void FrmVendor_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    //case Keys.Control | Keys.S:
                    //    btnSave_Click(sender,new EventArgs());//save
                    //  break;
                    //case Keys.Control | Keys.O:
                    //    btnOk_Click(sender, new EventArgs());//OK
                    //    break; 
                    //case Keys.Control | Keys.L:
                    //    btnCancel_Click(sender, new EventArgs());//Cancel
                    //    break;
                    case Keys.Control | Keys.Enter:
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        CancelToolStripButton_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        BtnPrint_Click(sender, new EventArgs());//Cancel
                        break;
                    case Keys.Control | Keys.M:
                        EmailStripButton_Click (sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception)
            {
            }
        }
        
         // Modified by Amal
        private void ClearSearchCount(object sender, EventArgs e)  // For TxtSearchText And CboSearchOption Text Changed Event
        {
            MintRowNumber = 1;
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            int TotalRows;
            // Validating Search Options
            if (CboSearchOption.SelectedIndex == -1 || CboSearchOption.Text.Trim().Length == 0)
            {
                //MstrMessageCommon = "Please select a Search Option";
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 324, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmVendor.Enabled = true;
                CboSearchOption.Focus();
                return;
            }
            // Validating Search Criteria
            if (TxtSearchText.Text.Trim().Length == 0)
            {
                //MstrMessageCommon = "Please enter Search Text";
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 325, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmVendor.Enabled = true;
                TxtSearchText.Focus();
                return; 
            }
            //Searching a Vendor according to the Vendor Name/Vendor Location/Mobile No/Nationality/Transaction Type
            //MintRowNumber =1;
            if (MobjClsBLLVendorInformation.SearchVendor(Convert.ToInt32(CboSearchOption.SelectedIndex),
                "%"+TxtSearchText.Text.Trim()+"%", MintRowNumber,MintVendorType, out TotalRows))
            {
                BindingNavigatorPositionItem.Text = MintRowNumber.ToString();
                BindingNavigatorCountItem.Text = "of " + TotalRows.ToString();

                MintTotalRecCnt = TotalRows;
                MblnAddStatus = false;
                
                GetVendorInformations(); // Displaying Vendor information
                BindingNavigatorPositionItem.Text = Convert.ToString(MintRowNumber);
                BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintTotalRecCnt) + "";

                BindingNavigatorMoveNextItem.Enabled = true;
                BindingNavigatorMoveLastItem.Enabled = true;
                BindingNavigatorMovePreviousItem.Enabled = true;
                BindingNavigatorMoveFirstItem.Enabled = true;

                int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
                int iRecordCnt = MintTotalRecCnt;  // Total count of the records

                if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                BtnPrint.Enabled = MblnPrintEmailPermission;
                EmailStripButton.Enabled = MblnPrintEmailPermission;
                BtnCancel.Enabled = MblnUpdatePermission;
                BtnSave.Enabled = MblnUpdatePermission;
                BtnOk.Enabled = MblnUpdatePermission;
                VendorInformationBindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            else
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 323, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmVendor.Enabled = true;
                MintRowNumber = 0;
            }
            CancelToolStripButton.Enabled = false;
        }
        //private void btnEmploymentType_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        FrmCommonRef objFrmCommonRef = new FrmCommonRef("Employment Type", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "EmploymentTypeID,IsPredefined,EmploymentType", "EmploymentTypeReference", "");
        //        objFrmCommonRef.ShowDialog();
        //        objFrmCommonRef.Dispose();
        //        //int intEmploymentType = Convert.ToInt32(cboEmploymentType.SelectedValue);
        //        LoadCombos(8);
        //        if (objFrmCommonRef.NewID != 0)
        //            cboEmploymentType.SelectedValue = objFrmCommonRef.NewID;
        //        else
        //            cboEmploymentType.SelectedValue = intEmploymentType;
        //    }
        //    catch (Exception Ex)
        //    {
        //        MobjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

        //        if (ClsCommonSettings.ShowErrorMess)
        //            MessageBox.Show("Error on Vendor Type Creation() " + Ex.Message.ToString());
        //    }
        //}

        //private void btnDesignation_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        FrmCommonRef objFrmCommonRef = new FrmCommonRef("Designation", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "DesignationID,IsPredefined,Designation", "DesignationReference", "");
        //        objFrmCommonRef.ShowDialog();
        //        objFrmCommonRef.Dispose();
        //        int intDesignation = Convert.ToInt32(cboDesignation.SelectedValue);
        //        LoadCombos(9);
        //        if (objFrmCommonRef.NewID != 0)
        //            cboDesignation.SelectedValue = objFrmCommonRef.NewID;
        //        else
        //            cboDesignation.SelectedValue = intDesignation;
        //    }
        //    catch (Exception Ex)
        //    {
        //        MobjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

        //        if (ClsCommonSettings.ShowErrorMess)
        //            MessageBox.Show("Error on Vendor Type Creation() " + Ex.Message.ToString());
        //    }
        //}

        //private void btnAccountType_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        FrmCommonRef objFrmCommonRef = new FrmCommonRef("Salary Account Type", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "SalaryAccountTypeID,IsPredefined,SalaryAccountType", "InvSalaryAccountTypeReference", "");
        //        objFrmCommonRef.ShowDialog();
        //        objFrmCommonRef.Dispose();
        //        int intAccountType=Convert.ToInt32(cboAccountType.SelectedValue);
        //        LoadCombos(10);
        //        if (objFrmCommonRef.NewID != 0)
        //            cboAccountType.SelectedValue = objFrmCommonRef.NewID;
        //        else
        //            cboAccountType.SelectedValue = intAccountType;
        //    }
        //    catch (Exception Ex)
        //    {
        //        MobjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

        //        if (ClsCommonSettings.ShowErrorMess)
        //            MessageBox.Show("Error on Vendor Type Creation() " + Ex.Message.ToString());
        //    }
        //}

        
        private void rdbMale_CheckedChanged(object sender, EventArgs e)
        {
            Changestatus(true);
        }

        private void dgvNearestCompetitor_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
           
            
        }
        

        private void dgvNearestCompetitor_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == ColOtherSuppliers.Index && dgvNearestCompetitor.Rows[e.RowIndex].Cells[ColOtherSuppliers.Index].Value != null)
            {
                DataTable datVendor = MobjClsBLLVendorInformation.GetNearestCompetitorRating(Convert.ToInt32(dgvNearestCompetitor.Rows[e.RowIndex].Cells[ColOtherSuppliers.Index].Value));
                    dgvNearestCompetitor.Rows[e.RowIndex].Cells["ColRating"].Value = datVendor.Rows[0]["Rating"];
            }
            Changestatus(true);
        }

        private void btnBankName_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("BankName", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "BankID,BankName", "BankReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intBankName = Convert.ToInt32(CboBankName.SelectedValue);
                LoadCombos(3);
                if (objCommon.NewID != 0)
                {
                    CboBankName.SelectedValue = objCommon.NewID;
                }
                else
                {
                    CboBankName.SelectedValue = intBankName;
                    if (intBankName == 0)
                    {
                        cboBankBranch.Text = "";
                        cboBankBranch.DataSource = null;
                    }
                }

            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on Bank_Click:BtnBank_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Bank_Click() " + Ex.Message.ToString());
            }
        }

        private void dgvNearestCompetitor_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvNearestCompetitor.IsCurrentCellDirty)
            {
                if (dgvNearestCompetitor.CurrentCell != null)
                    dgvNearestCompetitor.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgvNearestCompetitor_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void CboSearchOption_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboSearchOption.DroppedDown = false;
        }

        private void dgvNearestCompetitor_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            Changestatus(true);
        }

        private void dgvNearestCompetitor_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgvNearestCompetitor.CurrentCell.OwningColumn.Index == ColOtherSuppliers.Index)
            {
                ComboBox cbo = (ComboBox)e.Control;
                cbo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                cbo.AutoCompleteSource = AutoCompleteSource.ListItems;
                cbo.DropDownStyle = ComboBoxStyle.DropDown;
                cbo.DropDownHeight = 75;
            }
        }

        //private void btnDepartment_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        FrmCommonRef objFrmCommonRef = new FrmCommonRef("Department",new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "DepartmentID,IsPredefined,Department","DepartmentReference", "");
        //        objFrmCommonRef.ShowDialog();
        //        objFrmCommonRef.Dispose();
        //        int intDepartment = Convert.ToInt32(cboDepartment.SelectedValue);
        //        LoadCombos(14);
        //        if (objFrmCommonRef.NewID != 0)
        //            cboDepartment.SelectedValue = objFrmCommonRef.NewID;
        //        else
        //            cboDepartment.SelectedValue = intDepartment;
        //    }
        //    catch (Exception Ex)
        //    {
        //        MobjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

        //        if (ClsCommonSettings.ShowErrorMess)
        //            MessageBox.Show("Error on Vendor Type Creation() " + Ex.Message.ToString());
        //    }
        //}

        private void FrmVendor_FormClosing(object sender, FormClosingEventArgs e)
        {
            PintVendorID = MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID;
        }

        private void CboStatus_SelectedValueChanged(object sender, EventArgs e)
        {            
        }        

        private void historyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (FrmVendorHistory objFrmVendorHistory = new FrmVendorHistory(MintVendorType, MintCurrentVendorID))
            {
                objFrmVendorHistory.ShowDialog();
            }
        }

        private void CboTransactiontype_SelectionChangeCommitted(object sender, EventArgs e)
        {
            CboBankName.SelectedIndex = -1;
            cboBankBranch.SelectedIndex = -1;
        }

        private void CboSupClassification_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            if (MintVendorType == (int)VendorType.Supplier)
            {
                FrmHelp objHelp = new FrmHelp();
                objHelp.strFormName = "Suppliers";
                objHelp.ShowDialog();
                objHelp = null;
            }
            else
            {
                FrmHelp objHelp = new FrmHelp();
                objHelp.strFormName = "Customer";
                objHelp.ShowDialog();
                objHelp = null;
            }
        }

        private void txtPassowrd_TextChanged(object sender, EventArgs e)
        {
            Changestatus(true);
        }

        private void CboSupClassification_SelectedValueChanged(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(CboSupClassification.SelectedIndex) == 0)
            //{
            //    cboBankBranch.BackColor = SystemColors.Window;
            //    CboBankName.BackColor = SystemColors.Window;
            //    CboTransactiontype.SelectedIndex = 1;
            //    cboBankBranch.SelectedIndex = -1;
            //    CboBankName.SelectedIndex = -1;
            //}
            //else
            //{
            //    cboBankBranch.BackColor = SystemColors.Info;
            //    CboBankName.BackColor = SystemColors.Info;
            //    CboTransactiontype.SelectedIndex = 0;

            //}
            Changestatus(true);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnSourceMedia_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objFrmCommonRef = new FrmCommonRef("Source Media Type", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "SourceMediaID,IsPredefined,SourceMediaType", "InvSourceMediaTypeReference", "");
                objFrmCommonRef.ShowDialog();
                objFrmCommonRef.Dispose();
                int intSourceMediaID = Convert.ToInt32(cboSourceMedia.SelectedValue);
                LoadCombos(15);
                if (objFrmCommonRef.NewID != 0)
                    cboSourceMedia.SelectedValue  = objFrmCommonRef.NewID;
                else
                    cboSourceMedia.SelectedValue = intSourceMediaID;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Source Media Creation() " + Ex.Message.ToString());
            }
        }

        private void txtCreditLimit_TextChanged(object sender, EventArgs e)
        {
            Changestatus(true);
        }

        private void txtAdvanceLimit_TextChanged(object sender, EventArgs e)
        {
            Changestatus(true);
            if (txtAdvanceLimit.Text.ToDecimal() > 100)
            {
                txtAdvanceLimit.Text = "100";
            }
        }

        private void btnCurrencyReference_Click(object sender, EventArgs e)
        {
            try
            {
               int  MintComboID = Convert.ToInt32(cboCurrency.SelectedValue);
                using (FrmCurrency objCurrencyReference = new FrmCurrency())
                {
                    objCurrencyReference.ShowDialog();
                }
                LoadCombos(16);
                cboCurrency.SelectedValue = MintComboID;
                Changestatus(true);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnCurrency_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in btnCurrency_Click() " + ex.Message, 2);
            }
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus(true);
        }

        private void dgvNearestCompetitor_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjClsBLLCommonUtility.SetSerialNo(dgvNearestCompetitor, e.RowIndex, false);
        }

        private void dgvNearestCompetitor_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjClsBLLCommonUtility.SetSerialNo(dgvNearestCompetitor, e.RowIndex, false);
        }

        private void txtCreditLimitDays_TextChanged(object sender, EventArgs e)
        {
            Changestatus(true);
        }

        private void TxtAccountNo_TextChanged(object sender, EventArgs e)
        {
            Changestatus(true);
        }

        private void BtnVendorMapping_Click(object sender, EventArgs e)
        {
            using (FrmVendorMapping objVendorMapping = new FrmVendorMapping())
            {
                objVendorMapping.MintVendorType = MintVendorType;
                objVendorMapping.ShowDialog();
                if (objVendorMapping.PintVendorID > 0)
                {
                    PintVendorID = objVendorMapping.PintVendorID;
                    MobjClsBLLVendorInformation.clsDTOVendorInformation.intVendorID = PintVendorID;
                    MintTotalRecCnt = MobjClsBLLVendorInformation.RecCountNavigate(MintVendorType);
                    MintCurrentRecCnt = MobjClsBLLVendorInformation.GetRowNumber();
                    DisplayInfo();
                    SetEnableAddress();
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintTotalRecCnt) + "";
                    AddToAddressMenu();
                }
                else
                {
                    MintTotalRecCnt = MobjClsBLLVendorInformation.RecCountNavigate(MintVendorType);
                    
                    if (MintCurrentRecCnt>MintTotalRecCnt )
                        MintCurrentRecCnt=MintTotalRecCnt;
                                         
                    
                    //MintCurrentRecCnt = MobjClsBLLVendorInformation.GetRowNumber();

                    DisplayInfo();
                    SetEnableAddress();
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintTotalRecCnt) + "";
                    AddToAddressMenu();

                }
            }
        }

        private void txtTRN_TextChanged(object sender, EventArgs e)
        {
            Changestatus(true);
        }

       
        
    }
}