﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;

/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date, 4 April 2011>
Description:	<Description, Currency Form>
================================================
*/
namespace MyBooksERP
{
    public partial class FrmCurrency : DevComponents.DotNetBar.Office2007Form//Form
    {
        #region Variables Declaration

        string MstrMessageCommon;
        MessageBoxIcon MmsgMessageIcon;

        //private int MintCurrencyID;
        private int MintCurrencyDetailId=0;

        private bool MblnChangeStatus = false;
        private bool MblnAddStatus;
        private bool MblnPrintEmailPermission = false;//To Set Email Print Permission
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnIsEditMode = false;
        //private ArrayList MaMessageArr;// Error Message display

        DataTable MdatMessages;
        //ArrayList MsarFormNames; ////showing webbrowser in toolstripsplitbtn
        //WebBrowser MwebBroswer;
        //ToolStripControlHost MhostControlHost;
        clsBLLCommonUtility MobjBLLCommonUtility = new clsBLLCommonUtility();
        ClsBLLCurrency MobjClsBLLCurrency;
        ClsLogWriter MobjLogWriter;
        ClsNotificationNew MobjNotification;
        #endregion //Variables Declaration

        #region Constructor

        public FrmCurrency()
        {
            InitializeComponent();

            MobjClsBLLCurrency = new ClsBLLCurrency();
            MobjLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjNotification = new ClsNotificationNew();

            TmCurrency.Interval = ClsCommonSettings.TimerInterval;   // Setting timer interval
            ////showing webbrowser in toolstripsplitbtn
            //MwebBroswer = new WebBrowser();
            //MwebBroswer.ScrollBarsEnabled = false;
            //MwebBroswer.Name = "UsedLevel";
            //MhostControlHost = new ToolStripControlHost(MwebBroswer);
            //BtnwebUOMReference.DropDownItems.Add(MhostControlHost);
        }
        #endregion //Constructor

        private void FrmCurrency_Load(object sender, EventArgs e)
        {
            try
            {
                LoadMessage();
                SetPermissions();
                LoadCombos(0);
                MblnChangeStatus = false;
                MblnAddStatus = true;
                
                FillCurrencyDetails();
                AddNewCurrency();
                CboCurrencyType.SelectedIndex = -1;
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form Load:FrmCurrency_Load " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmCurrency_Load " + Ex.Message.ToString());
            }
        }

        private void LoadMessage()
        {
            MdatMessages = MobjNotification.FillMessageArray((int)FormID.ExchangeCurrency, ClsCommonSettings.ProductID);
        }

        private void AddNewCurrency()
        {
            try
            {
                MblnAddStatus = true;
                MblnIsEditMode = false;
                CboCurrencyType.SelectedIndex = -1;
                txtExchangeRate.Enabled = true;
                txtExchangeRate.Text = "";
                dtpExchangeDate.Value = DateTime.Today;
                DgvCurrency.ClearSelection();
                ErrCurrency.Clear();
                TmCurrency.Enabled = true;
                DgvCurrency.ClearSelection();
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3160, out MmsgMessageIcon);
                lblCurrencyStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                EnableDisableButtons(true);
                ErrCurrency.Clear();
                CboCurrencyType.Enabled = true;
                BtnCurrency.Enabled = true;
                dtpExchangeDate.Enabled = true;
                dtpExchangeDate.MaxDate = ClsCommonSettings.GetServerDate();
                BindingNavigatorSaveItem.Enabled = false;
                BtnSave.Enabled = false;
                BtnOk.Enabled = false;

                
                //BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                ExchangeRateLabel();
                CboCompany.Select();
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form AddNewCurrency:AddNewCurrency()" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewCurrency " + Ex.Message.ToString());
            }
        }

        private void ResetCompanyCombo()
        {
            if (ClsCommonSettings.CompanyID <= 0)
                ClsCommonSettings.CompanyID = MobjBLLCommonUtility.GetCompanyID();

            CboCompany.SelectedValue = ClsCommonSettings.CompanyID;
        }

        private void ChangeStatus()
        {
            MblnChangeStatus = true;
            if (!MblnIsEditMode)
            {
                BtnOk.Enabled = BtnSave.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = btnClear.Enabled = MblnAddPermission;
            }
            else
            {
                btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
                BindingNavigatorSaveItem.Enabled = BtnOk.Enabled = BtnSave.Enabled = btnClear.Enabled = MblnUpdatePermission;
                //btnActions.Enabled = MblnUpdatePermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            }
            ErrCurrency.Clear();
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                if (CboCompany.SelectedValue == null)
                {
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (Int32)eModuleID.General, (Int32)eMenuID.Currency, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                }
                else
                {
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, Convert.ToInt32(CboCompany.SelectedValue), (Int32)eModuleID.General, (Int32)eMenuID.ExchangeCurrency, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                }
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            //BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            //BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            //BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            //BtnOk.Enabled = MblnAddPermission;
            //btnClear.Enabled = MblnUpdatePermission;
            //btnPrint.Enabled = MblnPrintEmailPermission;
            //btnEmail.Enabled = MblnPrintEmailPermission;
        }

        public void ChangeStatus(object sender, EventArgs e) // to set the event
        {
            ChangeStatus();
            FillERCurrencylabels();
        }

        private void Cbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        /// <summary>
        /// Enable/disable buttons according to the action
        /// </summary>
        private void EnableDisableButtons(bool bEnable)
        {
            //BindingNavigatorAddNewItem.Enabled = !bEnable;
            //BindingNavigatorDeleteItem.Enabled = !bEnable;
            if (!bEnable)
            {
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            }
            else
            {
                BindingNavigatorAddNewItem.Enabled = !bEnable;
                BindingNavigatorDeleteItem.Enabled = !bEnable;
            }
            //btnPrint.Enabled = !bEnable;
            //btnEmail.Enabled = !bEnable;
            btnClear.Enabled =MblnUpdatePermission;

            if (bEnable)
            {
                btnPrint.Enabled = !bEnable;
                btnEmail.Enabled = !bEnable;
                //BindingNavigatorSaveItem.Enabled = MblnAddPermission;
                //BtnOk.Enabled = MblnAddPermission;
                btnClear.Enabled = !bEnable;
                //BtnSave.Enabled = MblnAddPermission;
                MblnChangeStatus = !bEnable;
            }
            else
            {
                btnPrint.Enabled = MblnPrintEmailPermission;
                btnEmail.Enabled = MblnPrintEmailPermission;
                //BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                //BtnOk.Enabled = MblnUpdatePermission;
                //BtnSave.Enabled = MblnUpdatePermission;
                MblnChangeStatus = bEnable;
            }
        }

        // Clear Controls
        private void ClearControls()
        {
            TxtDefaultCurrency.Clear();
            CboCurrencyType.SelectedIndex = -1;
            txtExchangeRate.Text = "";
            dtpExchangeDate.Value = DateTime.Today;
            DgvCurrency.ClearSelection();
            ErrCurrency.Clear();
        }

        // Loading Currency Combo
        private bool LoadCombos(int intType)
        {
            // 0 - For loading all Combo
            // 1 - For loading Company Combo
            // 2 - For loading Currency Combo

            try
            {
                DataTable datCombos = new DataTable();
                ClsBLLCurrency MobjClsBLLCurrency = new ClsBLLCurrency();
                
                if (intType == 0 || intType == 1)
                {
                    datCombos = MobjClsBLLCurrency.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID });
                    CboCompany.ValueMember = "CompanyID";
                    CboCompany.DisplayMember = "CompanyName";
                    CboCompany.DataSource = datCombos;
                    return true;
                }
                if (intType == 0 || intType == 2)
                {
                   int CompanyId = Convert.ToInt32(CboCompany.SelectedValue);
                    DisplayCompanyCurrency(CompanyId);

                    int CurrencyID = Convert.ToInt32(TxtDefaultCurrency.Tag);
                    datCombos = MobjClsBLLCurrency.FillCombos(new string[] { "CurrencyID,CurrencyName", "CurrencyReference", "CurrencyID not in( " + CurrencyID + ")" });
                    CboCurrencyType.ValueMember = "CurrencyID";
                    CboCurrencyType.DisplayMember = "CurrencyName";
                    CboCurrencyType.DataSource = datCombos;
                }               

                MobjLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private void BtnCurrency_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmCurrencyReference objFrmCurrencyReference = new FrmCurrencyReference())
                {
                    objFrmCurrencyReference.PintCurrency = Convert.ToInt32(CboCurrencyType.SelectedValue);
                    objFrmCurrencyReference.ShowDialog();
                    int intCurrencyType = Convert.ToInt32(CboCurrencyType.SelectedValue);
                    LoadCombos(2);
                    CboCurrencyType.SelectedValue = intCurrencyType;
                    if (intCurrencyType==0)
                        ExchangeRateLabel();
                }              
                
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnCurrency " + Ex.Message.ToString());
            }

        }

        private void FillCurrencyDetails()
        {
            DgvCurrency.DataSource = null;
            int companyid = Convert.ToInt32(CboCompany.SelectedValue);
            DgvCurrency.DataSource = MobjClsBLLCurrency.GetCurrencyDetails(companyid);
            if (DgvCurrency.ColumnCount > 0)
            {
                DgvCurrency.Columns[0].Visible = false;
                DgvCurrency.Columns[1].Width = 175;
                DgvCurrency.Columns[1].HeaderText = "Currency";
                DgvCurrency.Columns[2].Width = 100;
                DgvCurrency.Columns[2].HeaderText = "Exchange Rate";
                //DgvCurrency.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
                DgvCurrency.Columns[3].Width =100;
                //DgvCurrency.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
                DgvCurrency.Columns[3].HeaderText = "Exchange Date";
                DgvCurrency.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                DgvCurrency.ClearSelection();
                if (DgvCurrency.RowCount > 0)
                {
                    btnPrint.Enabled = MblnPrintEmailPermission;
                    btnEmail.Enabled = MblnPrintEmailPermission;
                }
                else
                {
                    btnPrint.Enabled = false;
                    btnEmail.Enabled = false;
                }
            }
        }


        // UOM Validation
        private bool CurrencyValidation()
        {
            lblCurrencyStatus.Text = "";                 

             if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages,7361, out MmsgMessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                ErrCurrency.SetError(CboCompany, MstrMessageCommon.Replace("#", "").Trim());
                lblCurrencyStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmCurrency.Enabled = true;
                CboCompany.Focus();
                return false;
            }

            if (TxtDefaultCurrency.Text.Trim() == "")
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages,7362, out MmsgMessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                ErrCurrency.SetError(TxtDefaultCurrency, MstrMessageCommon.Replace("#", "").Trim());
                lblCurrencyStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmCurrency.Enabled = true;
                TxtDefaultCurrency.Focus();
                return false;
            }

            if (Convert.ToInt32(CboCurrencyType.SelectedValue) == 0)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages,7363, out MmsgMessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                ErrCurrency.SetError(CboCurrencyType, MstrMessageCommon.Replace("#", "").Trim());
                lblCurrencyStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmCurrency.Enabled = true;
                CboCurrencyType.Focus();
                return false;
            }


            if ((txtExchangeRate.Text.Trim().Length > 0 ? Convert.ToDouble(txtExchangeRate.Text.Trim()) : 0) == 0)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages,7364, out MmsgMessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                ErrCurrency.SetError(txtExchangeRate, MstrMessageCommon.Replace("#", "").Trim());
                lblCurrencyStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmCurrency.Enabled = true;
                txtExchangeRate.Focus();
                return false;
            }

            if (MblnAddStatus == true)
            {
                if (MobjClsBLLCurrency.CheckDuplication(Convert.ToInt32(CboCompany.SelectedValue), (Convert.ToInt32(CboCurrencyType.SelectedValue)), (Convert.ToString(dtpExchangeDate.Value))))
                {

                    MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 7366, out MmsgMessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    ErrCurrency.SetError(CboCurrencyType, MstrMessageCommon.Replace("#", "").Trim());
                    lblCurrencyStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmCurrency.Enabled = true;
                    CboCurrencyType.Focus();
                    return false;
                }
            }               
           
           return true;
        }
      
        private void DisplayCompanyCurrency(int CompanyId)
        {
            if (MobjClsBLLCurrency.DisplayCompanyCurrency(CompanyId))
                {
                    TxtDefaultCurrency.Tag = MobjClsBLLCurrency.ClsDTOCurrency.intCurrencyID;
                    TxtDefaultCurrency.Text = MobjClsBLLCurrency.ClsDTOCurrency.strCompanyCurrency.ToString();                  
                    
                }
                CboCompany.Focus();
                EnableDisableButtons(false);
       }
        /* 
       =================================================
           Author:		<Author,Amal>
           Create date: <Create Date,16 August 2011>
           Description:	<Description,,Modification in Currency label>
       ================================================
      */
        private void FillERCurrencylabels()
        {
            lblECompCrncy.Visible = true;
            int iCompanyID;
            iCompanyID=Convert.ToInt32(CboCompany.SelectedValue);
            if (MobjClsBLLCurrency.FillERCompanyCurrencyLabel(iCompanyID))
            {
                lblECompCrncy.Text = MobjClsBLLCurrency.ClsDTOCurrency.strShortDescription.ToString();
            }
            if (TxtDefaultCurrency.Text != CboCurrencyType.Text)
            {
                if (Convert.ToInt32(CboCurrencyType.SelectedValue) != 0)
                {
                    if (Convert.ToInt32(CboCurrencyType.SelectedValue) != -1)
                    {
                        int iCurrencyID;
                        iCurrencyID = Convert.ToInt32(CboCurrencyType.SelectedValue);
                        if (MobjClsBLLCurrency.FillERSelectedCurrencyLabel(iCurrencyID))
                        {
                            lblESelectedCurrency.Text = "One " + MobjClsBLLCurrency.ClsDTOCurrency.strShortDescription.ToString() + " is";
                        }
                    }

                }
            }
            else
            {
                if (MobjClsBLLCurrency.FillERCompanyCurrencyLabel(iCompanyID))
                {
                    lblESelectedCurrency.Text = "One " + MobjClsBLLCurrency.ClsDTOCurrency.strShortDescription.ToString() + " is";
                }  
            
            }
        }
        private void ExchangeRateLabel()
        {
            lblESelectedCurrency.Text = "Exchange Rate";
            lblECompCrncy.Visible = false;
        }

        private void CboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearControls();
            int MintCompanyId;
            MintCompanyId = Convert.ToInt32(CboCompany.SelectedValue);
            DisplayCompanyCurrency(MintCompanyId);
            LoadCombos(2);
            FillCurrencyDetails();
            CboCurrencyType.SelectedIndex = -1;
            CboCurrencyType.Enabled = true;
            BtnCurrency.Enabled = true;
            dtpExchangeDate.Enabled = true;           
            //BtnOk.Enabled = false;
            //BtnSave.Enabled = MblnUpdatePermission;
            //BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            ErrCurrency.Clear();
            AddNewCurrency();
            ExchangeRateLabel();
            SetPermissions();
            ChangeStatus();
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewCurrency();            
       }

        // Save
      private bool SaveCurrencyDetails()        
      {
          try
          {
              if (MblnAddStatus == true)
              {

                  MobjClsBLLCurrency.ClsDTOCurrency.intCurrencyDetailID = 0;
                  MobjClsBLLCurrency.ClsDTOCurrency.intCompanyID = Convert.ToInt32(CboCompany.SelectedValue);
                  MobjClsBLLCurrency.ClsDTOCurrency.strCompanyCurrency = TxtDefaultCurrency.Text.Replace("'", "`").Trim();
                  MobjClsBLLCurrency.ClsDTOCurrency.intCurrencyID = Convert.ToInt32(CboCurrencyType.SelectedValue);
                  MobjClsBLLCurrency.ClsDTOCurrency.dblExchangeRate = Convert.ToDouble(txtExchangeRate.Text.Trim());
                  MobjClsBLLCurrency.ClsDTOCurrency.dtpExchangeDate = dtpExchangeDate.Value.ToString("dd MMM yyyy").ToDateTime(); //Convert.ToDateTime(dtpExchangeDate.Text.Trim());

                  MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 1, out MmsgMessageIcon);
              }
              else
              {
                  MblnAddStatus = false;
                  MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3, out MmsgMessageIcon);
                  MobjClsBLLCurrency.ClsDTOCurrency.intCurrencyDetailID = MintCurrencyDetailId;
                 // DateTime date = DateTime.Today;
                  if ( dtpExchangeDate.Value.ToString("dd MMM yyyy").ToDateTime() == DateTime.Today.ToString("dd MMM yyyy").ToDateTime())
                  {
                      MobjClsBLLCurrency.ClsDTOCurrency.dtpExchangeDate = dtpExchangeDate.Value.ToString("dd MMM yyyy").ToDateTime(); //Convert.ToDateTime(dtpExchangeDate.Text.Trim());
                      MobjClsBLLCurrency.ClsDTOCurrency.intCompanyID = Convert.ToInt32(CboCompany.SelectedValue);
                      MobjClsBLLCurrency.ClsDTOCurrency.strCompanyCurrency = TxtDefaultCurrency.Text.Replace("'", "`").Trim();
                      MobjClsBLLCurrency.ClsDTOCurrency.intCurrencyID = Convert.ToInt32(CboCurrencyType.SelectedValue);
                      MobjClsBLLCurrency.ClsDTOCurrency.dblExchangeRate = Convert.ToDouble(txtExchangeRate.Text.Trim());
                  }
                  else
                  {
                      MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 7367, out MmsgMessageIcon);
                      if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
                          return true;
                  }

              }

              if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                  return false;

              if (MobjClsBLLCurrency.SaveCurrency(MblnAddStatus))
              {
                  MobjClsBLLCurrency.ClsDTOCurrency.intCurrencyDetailID = MintCurrencyDetailId;

                  return true;
              }

              return false;
          }
          catch (Exception Ex)
          {
              MobjLogWriter.WriteLog("Error on form save:SaveCurrencyDetails()   " + this.Name + " " + Ex.Message.ToString(), 2);

              if (ClsCommonSettings.ShowErrorMess)
                  MessageBox.Show("Error on  Saving SaveCurrencyDetails()  " + Ex.Message.ToString());
              return false;
          }

        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (CurrencyValidation())
            {
                if (SaveCurrencyDetails())
                {
                    if (MblnAddStatus == true)
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 2, out MmsgMessageIcon);
                        lblCurrencyStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmCurrency.Enabled = true;
                    }
                    else
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3057, out MmsgMessageIcon);
                        lblCurrencyStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);

                        TmCurrency.Enabled = true;
                    }
                    FillCurrencyDetails();
                    AddNewCurrency();
                }
            }
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (DgvCurrency.CurrentRow != null)
            {
                MintCurrencyDetailId = Convert.ToInt32(DgvCurrency.CurrentRow.Cells[0].Value);
              
                if (MintCurrencyDetailId > 0 )
                {
                    if (DeleteValidation(MintCurrencyDetailId))
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 13, out MmsgMessageIcon);
                        if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                            return;
                        if (MobjClsBLLCurrency.DeleteCurrency(MintCurrencyDetailId))
                        {
                            MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 4, out MmsgMessageIcon);
                            lblCurrencyStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmCurrency.Enabled = true;
                            FillCurrencyDetails();
                            AddNewCurrency();

                        }
                    }
                }
            }
        }

        private bool DeleteValidation(int intCurrDetailId)
        {

            int intDeleteStatus = MobjClsBLLCurrency.GetCurrencyExists(intCurrDetailId);
            int intDeleteStatus1 = MobjClsBLLCurrency.GetCurrencyExists1(intCurrDetailId);
            if (intDeleteStatus > 0)
            {
                      MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 7368, out MmsgMessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    TmCurrency.Enabled = true;
                    return false;
                
            }

            else if (intDeleteStatus1 > 0)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 7369, out MmsgMessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                TmCurrency.Enabled = true;
                return false;
            }
            


            return true;
        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            AddNewCurrency();
            ResetCompanyCombo();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (CurrencyValidation())
            {
                if (SaveCurrencyDetails())
                {
                    if (MblnAddStatus == true)
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 2, out MmsgMessageIcon);
                        lblCurrencyStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmCurrency.Enabled = true;
                    }
                    else
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3057, out MmsgMessageIcon);
                        lblCurrencyStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmCurrency.Enabled = true;
                    }

                    FillCurrencyDetails();
                    AddNewCurrency();
                }
            }

        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (CurrencyValidation())
            {
                if (SaveCurrencyDetails())
                {
                    if (MblnAddStatus == true)
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 2, out MmsgMessageIcon);
                        lblCurrencyStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmCurrency.Enabled = true;
                    }
                    else
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3057, out MmsgMessageIcon);
                        lblCurrencyStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);

                        TmCurrency.Enabled = true;
                    }
                    FillCurrencyDetails();
                    AddNewCurrency();
                    this.Close();
                }
            }

        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DisplayCurrency(int intCurrencyDetailID)
        {
            if (Convert.ToInt32(DgvCurrency.CurrentRow.Cells[0].Value) != 0)
            {
                MintCurrencyDetailId = Convert.ToInt32(DgvCurrency.CurrentRow.Cells[0].Value);

                if (MobjClsBLLCurrency.DisplayCurrency(intCurrencyDetailID))
                {
                    CboCompany.SelectedValue = MobjClsBLLCurrency.ClsDTOCurrency.intCompanyID.ToString();
                    TxtDefaultCurrency.Text = MobjClsBLLCurrency.ClsDTOCurrency.strCompanyCurrency.ToString();
                    CboCurrencyType.Tag = MobjClsBLLCurrency.ClsDTOCurrency.intCurrencyID;
                    CboCurrencyType.Text = MobjClsBLLCurrency.ClsDTOCurrency.strCurrency.ToString();
                    txtExchangeRate.Text = MobjClsBLLCurrency.ClsDTOCurrency.dblExchangeRate.ToString();
                    dtpExchangeDate.Value = MobjClsBLLCurrency.ClsDTOCurrency.dtpExchangeDate;

                    this.txtExchangeRate.Enabled = this.MobjClsBLLCurrency.ClsDTOCurrency.dtpExchangeDate >= Convert.ToDateTime(ClsCommonSettings.GetServerDate().ToString("dd MMM yyyy"));
                }
                  CboCompany.Focus();
                  MblnAddStatus = false;
                  EnableDisableButtons(false);
                  if (CboCurrencyType.Text == TxtDefaultCurrency.Text)
                  {
                      BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                  }
            }
        }

        private void DgvCurrency_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            MblnIsEditMode = true;
            if (e.RowIndex >=0 && e.ColumnIndex>=0)
            {
                MintCurrencyDetailId = Convert.ToInt32(DgvCurrency.CurrentRow.Cells[0].Value);
                DisplayCurrency(MintCurrencyDetailId);
                FillERCurrencylabels();
                if (DgvCurrency.RowCount > 0)
                {
                    btnPrint.Enabled = MblnPrintEmailPermission;
                    dtpExchangeDate.Enabled = false;
                    CboCurrencyType.Enabled = false;
                    BtnCurrency.Enabled = false;
                }
                else
                {
                    btnPrint.Enabled = false;
                }
            }
        }

        private void DgvCurrency_KeyUp(object sender, KeyEventArgs e)
        {
            if (DgvCurrency.CurrentRow != null && DgvCurrency.CurrentRow.Cells[0].Value != null)
            {
                MintCurrencyDetailId = Convert.ToInt32(DgvCurrency.CurrentRow.Cells[0].Value);
                DisplayCurrency(MintCurrencyDetailId);
                FillERCurrencylabels();
            }

        }

        private void DgvCurrency_KeyDown(object sender, KeyEventArgs e)
        {
            if (DgvCurrency.CurrentRow != null && DgvCurrency.CurrentRow.Cells[0].Value != null)
            {
                MintCurrencyDetailId = Convert.ToInt32(DgvCurrency.CurrentRow.Cells[0].Value);
                DisplayCurrency(MintCurrencyDetailId);
                FillERCurrencylabels();
            }

        }

        private void FrmCurrency_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MblnChangeStatus)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 8, out MmsgMessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void LblGetExchangeRate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (new ClsInternet().IsInternetConnected() ==true)
            {
                System.Diagnostics.Process.Start("http://www.xe.com");
            }
            else
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages,7365, out MmsgMessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                    MessageBoxButtons.OK, MessageBoxIcon.Information);                  
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void LoadReport()
        {
            try
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = Convert.ToInt32(CboCompany.SelectedValue);
                ObjViewer.PiFormID = (int)FormID.ExchangeCurrency;
                ObjViewer.ShowDialog();
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form print:LoadReport() " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport() " + Ex.Message.ToString());
            }

        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Currency Information";
                    ObjEmailPopUp.EmailFormType = EmailFormID.ExchangeCurrency;
                    ObjEmailPopUp.EmailSource = MobjClsBLLCurrency.GetExchangeCurrencyReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form Email:btnEmail_Click" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Email btnEmail_Click " + Ex.Message.ToString());
            }
        }

        private void txtExchangeRate_TextChanged(object sender, EventArgs e)
        {
            if (TxtDefaultCurrency.Text == CboCurrencyType.Text)
            {
                ChangeStatus();             
            }
            ChangeStatus();
        }      
       
        private void txtExchangeRate_Enter(object sender, EventArgs e)
        {
            if (TxtDefaultCurrency.Text == CboCurrencyType.Text)
            {
                //BtnOk.Enabled = MblnUpdatePermission;
                //BtnSave.Enabled = MblnUpdatePermission;
                //BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 7367, out MmsgMessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
                    CboCompany.Focus();
                AddNewCurrency();
                CboCurrencyType.SelectedIndex = -1;
                CboCurrencyType.Text = "";
            }        

        }

        private void txtExchangeRate_Leave(object sender, EventArgs e)
        {
            this.txtExchangeRate.Text = this.txtExchangeRate.Text.ToDouble().Format(3);
        }

        private void txtExchangeRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;
            }
        }

        private void FrmCurrency_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    //case Keys.Control | Keys.S:
                    //    btnSave_Click(sender,new EventArgs());//save
                    //  break;
                    //case Keys.Control | Keys.O:
                    //    btnOk_Click(sender, new EventArgs());//OK
                    //    break; 
                    //case Keys.Control | Keys.L:
                    //    btnCancel_Click(sender, new EventArgs());//Cancel
                    //    break;
                    case Keys.Control | Keys.Enter:
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        btnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.P:
                        btnPrint_Click(sender, new EventArgs());//Cancel
                        break;
                    case Keys.Control | Keys.M:
                        btnEmail_Click(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void BtnHelp_Click_1(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Currency";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void DgvCurrency_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjBLLCommonUtility.SetSerialNo(DgvCurrency, e.RowIndex, true);
        }

        private void DgvCurrency_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjBLLCommonUtility.SetSerialNo(DgvCurrency, e.RowIndex, true);
        } 
    }
}
