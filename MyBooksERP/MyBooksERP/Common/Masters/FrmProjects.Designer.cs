﻿namespace MyBooksERP
{
    partial class FrmProjects
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label ProjectManagerLabel;
            System.Windows.Forms.Label ProjectCodeLabel;
            System.Windows.Forms.Label DescriptionLabel;
            System.Windows.Forms.Label StartDateLabel;
            System.Windows.Forms.Label EndDateLabel;
            System.Windows.Forms.Label lblEstimatedAmount;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProjects));
            this.ProjectCreationStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmProjects = new System.Windows.Forms.Timer(this.components);
            this.EmployeeMasterBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.GrpProjectCreation = new System.Windows.Forms.GroupBox();
            this.txtEstimatedAmount = new NumericTextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.CboProjectManager = new System.Windows.Forms.ComboBox();
            this.txtProjectCode = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ProjectCreationStatusStrip = new System.Windows.Forms.StatusStrip();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.btnOK = new System.Windows.Forms.Button();
            this.errProjects = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ProjectsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.btnBnSave = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnScan = new System.Windows.Forms.ToolStripButton();
            this.lblStatus = new System.Windows.Forms.Label();
            ProjectManagerLabel = new System.Windows.Forms.Label();
            ProjectCodeLabel = new System.Windows.Forms.Label();
            DescriptionLabel = new System.Windows.Forms.Label();
            StartDateLabel = new System.Windows.Forms.Label();
            EndDateLabel = new System.Windows.Forms.Label();
            lblEstimatedAmount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeMasterBindingSource1)).BeginInit();
            this.GrpProjectCreation.SuspendLayout();
            this.ProjectCreationStatusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errProjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectsBindingNavigator)).BeginInit();
            this.ProjectsBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // ProjectManagerLabel
            // 
            ProjectManagerLabel.AutoSize = true;
            ProjectManagerLabel.Location = new System.Drawing.Point(13, 79);
            ProjectManagerLabel.Name = "ProjectManagerLabel";
            ProjectManagerLabel.Size = new System.Drawing.Size(85, 13);
            ProjectManagerLabel.TabIndex = 23;
            ProjectManagerLabel.Text = "Project Manager";
            // 
            // ProjectCodeLabel
            // 
            ProjectCodeLabel.AutoSize = true;
            ProjectCodeLabel.Location = new System.Drawing.Point(13, 26);
            ProjectCodeLabel.Name = "ProjectCodeLabel";
            ProjectCodeLabel.Size = new System.Drawing.Size(68, 13);
            ProjectCodeLabel.TabIndex = 70;
            ProjectCodeLabel.Text = "Project Code";
            // 
            // DescriptionLabel
            // 
            DescriptionLabel.AutoSize = true;
            DescriptionLabel.Location = new System.Drawing.Point(13, 52);
            DescriptionLabel.Name = "DescriptionLabel";
            DescriptionLabel.Size = new System.Drawing.Size(60, 13);
            DescriptionLabel.TabIndex = 90;
            DescriptionLabel.Text = "Description";
            // 
            // StartDateLabel
            // 
            StartDateLabel.AutoSize = true;
            StartDateLabel.Location = new System.Drawing.Point(321, 27);
            StartDateLabel.Name = "StartDateLabel";
            StartDateLabel.Size = new System.Drawing.Size(55, 13);
            StartDateLabel.TabIndex = 110;
            StartDateLabel.Text = "Start Date";
            // 
            // EndDateLabel
            // 
            EndDateLabel.AutoSize = true;
            EndDateLabel.Location = new System.Drawing.Point(320, 53);
            EndDateLabel.Name = "EndDateLabel";
            EndDateLabel.Size = new System.Drawing.Size(52, 13);
            EndDateLabel.TabIndex = 13;
            EndDateLabel.Text = "End Date";
            // 
            // lblEstimatedAmount
            // 
            lblEstimatedAmount.AutoSize = true;
            lblEstimatedAmount.Location = new System.Drawing.Point(13, 106);
            lblEstimatedAmount.Name = "lblEstimatedAmount";
            lblEstimatedAmount.Size = new System.Drawing.Size(92, 13);
            lblEstimatedAmount.TabIndex = 111;
            lblEstimatedAmount.Text = "Estimated Amount";
            // 
            // ProjectCreationStatusLabel
            // 
            this.ProjectCreationStatusLabel.Name = "ProjectCreationStatusLabel";
            this.ProjectCreationStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // tmProjects
            // 
            this.tmProjects.Interval = 1000;
            this.tmProjects.Tick += new System.EventHandler(this.tmProjects_Tick);
            // 
            // GrpProjectCreation
            // 
            this.GrpProjectCreation.Controls.Add(this.txtEstimatedAmount);
            this.GrpProjectCreation.Controls.Add(lblEstimatedAmount);
            this.GrpProjectCreation.Controls.Add(this.Label3);
            this.GrpProjectCreation.Controls.Add(this.Label1);
            this.GrpProjectCreation.Controls.Add(this.CboProjectManager);
            this.GrpProjectCreation.Controls.Add(ProjectManagerLabel);
            this.GrpProjectCreation.Controls.Add(ProjectCodeLabel);
            this.GrpProjectCreation.Controls.Add(this.txtProjectCode);
            this.GrpProjectCreation.Controls.Add(DescriptionLabel);
            this.GrpProjectCreation.Controls.Add(this.txtDescription);
            this.GrpProjectCreation.Controls.Add(StartDateLabel);
            this.GrpProjectCreation.Controls.Add(this.dtpEndDate);
            this.GrpProjectCreation.Controls.Add(this.dtpStartDate);
            this.GrpProjectCreation.Controls.Add(EndDateLabel);
            this.GrpProjectCreation.Controls.Add(this.txtRemarks);
            this.GrpProjectCreation.Controls.Add(this.ShapeContainer1);
            this.GrpProjectCreation.Location = new System.Drawing.Point(12, 28);
            this.GrpProjectCreation.Name = "GrpProjectCreation";
            this.GrpProjectCreation.Size = new System.Drawing.Size(514, 236);
            this.GrpProjectCreation.TabIndex = 1;
            this.GrpProjectCreation.TabStop = false;
            // 
            // txtEstimatedAmount
            // 
            this.txtEstimatedAmount.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtEstimatedAmount.DecimalPlaces = 3;
            this.txtEstimatedAmount.Location = new System.Drawing.Point(108, 103);
            this.txtEstimatedAmount.MaxLength = 12;
            this.txtEstimatedAmount.Name = "txtEstimatedAmount";
            this.txtEstimatedAmount.ShortcutsEnabled = false;
            this.txtEstimatedAmount.Size = new System.Drawing.Size(106, 20);
            this.txtEstimatedAmount.TabIndex = 112;
            this.txtEstimatedAmount.Text = "0.000";
            this.txtEstimatedAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEstimatedAmount.TextChanged += new System.EventHandler(this.txtEstimatedAmount_TextChanged);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(9, 135);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(157, 13);
            this.Label3.TabIndex = 36;
            this.Label3.Text = "Other Related Information";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(6, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(131, 13);
            this.Label1.TabIndex = 36;
            this.Label1.Text = "Project Name && Dates";
            // 
            // CboProjectManager
            // 
            this.CboProjectManager.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboProjectManager.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboProjectManager.BackColor = System.Drawing.SystemColors.Info;
            this.CboProjectManager.DropDownHeight = 134;
            this.CboProjectManager.FormattingEnabled = true;
            this.CboProjectManager.IntegralHeight = false;
            this.CboProjectManager.Location = new System.Drawing.Point(108, 75);
            this.CboProjectManager.MaxDropDownItems = 10;
            this.CboProjectManager.Name = "CboProjectManager";
            this.CboProjectManager.Size = new System.Drawing.Size(203, 21);
            this.CboProjectManager.TabIndex = 3;
            this.CboProjectManager.SelectedIndexChanged += new System.EventHandler(this.CboProjectManager_SelectedIndexChanged);
            // 
            // txtProjectCode
            // 
            this.txtProjectCode.BackColor = System.Drawing.SystemColors.Info;
            this.txtProjectCode.Location = new System.Drawing.Point(108, 23);
            this.txtProjectCode.MaxLength = 20;
            this.txtProjectCode.Name = "txtProjectCode";
            this.txtProjectCode.ReadOnly = true;
            this.txtProjectCode.Size = new System.Drawing.Size(132, 20);
            this.txtProjectCode.TabIndex = 1;
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Info;
            this.txtDescription.Location = new System.Drawing.Point(108, 49);
            this.txtDescription.MaxLength = 50;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(203, 20);
            this.txtDescription.TabIndex = 2;
            this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_TextChanged);
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Location = new System.Drawing.Point(385, 50);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(116, 20);
            this.dtpEndDate.TabIndex = 5;
            this.dtpEndDate.ValueChanged += new System.EventHandler(this.dtpEndDate_ValueChanged);
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Location = new System.Drawing.Point(385, 24);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(116, 20);
            this.dtpStartDate.TabIndex = 4;
            this.dtpStartDate.ValueChanged += new System.EventHandler(this.dtpStartDate_ValueChanged);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(12, 155);
            this.txtRemarks.MaxLength = 2000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(491, 72);
            this.txtRemarks.TabIndex = 13;
            this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.ShapeContainer1.Size = new System.Drawing.Size(508, 217);
            this.ShapeContainer1.TabIndex = 37;
            this.ShapeContainer1.TabStop = false;
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 11;
            this.LineShape1.X2 = 499;
            this.LineShape1.Y1 = 127;
            this.LineShape1.Y2 = 127;
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(42, 17);
            this.StatusLabel.Text = "Status:";
            // 
            // ProjectCreationStatusStrip
            // 
            this.ProjectCreationStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.ProjectCreationStatusLabel});
            this.ProjectCreationStatusStrip.Location = new System.Drawing.Point(0, 304);
            this.ProjectCreationStatusStrip.Name = "ProjectCreationStatusStrip";
            this.ProjectCreationStatusStrip.Size = new System.Drawing.Size(535, 22);
            this.ProjectCreationStatusStrip.TabIndex = 41;
            this.ProjectCreationStatusStrip.Text = "StatusStrip1";
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(373, 270);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "&Ok";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // errProjects
            // 
            this.errProjects.ContainerControl = this;
            this.errProjects.RightToLeft = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(454, 270);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(15, 270);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = ((System.Drawing.Image)(resources.GetObject("btnEmail.Image")));
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.ToolTipText = "Print ";
            this.btnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            this.ToolStripSeparator3.Visible = false;
            // 
            // ProjectsBindingNavigator
            // 
            this.ProjectsBindingNavigator.AddNewItem = null;
            this.ProjectsBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.ProjectsBindingNavigator.DeleteItem = null;
            this.ProjectsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorDeleteItem,
            this.btnBnSave,
            this.btnClear,
            this.ToolStripSeparator1,
            this.BtnScan,
            this.ToolStripSeparator3,
            this.btnPrint,
            this.btnEmail,
            this.ToolStripSeparator2,
            this.BtnHelp});
            this.ProjectsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.ProjectsBindingNavigator.MoveFirstItem = null;
            this.ProjectsBindingNavigator.MoveLastItem = null;
            this.ProjectsBindingNavigator.MoveNextItem = null;
            this.ProjectsBindingNavigator.MovePreviousItem = null;
            this.ProjectsBindingNavigator.Name = "ProjectsBindingNavigator";
            this.ProjectsBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.ProjectsBindingNavigator.Size = new System.Drawing.Size(535, 25);
            this.ProjectsBindingNavigator.TabIndex = 0;
            this.ProjectsBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Enabled = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // btnBnSave
            // 
            this.btnBnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnBnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnBnSave.Image")));
            this.btnBnSave.Name = "btnBnSave";
            this.btnBnSave.Size = new System.Drawing.Size(23, 22);
            this.btnBnSave.Text = "Save Data";
            this.btnBnSave.Click += new System.EventHandler(this.ProjectsBindingNavigatorSaveItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Cancel";
            this.btnClear.ToolTipText = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnScan
            // 
            this.BtnScan.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnScan.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnScan.Name = "BtnScan";
            this.BtnScan.Size = new System.Drawing.Size(23, 22);
            this.BtnScan.Text = "Documents";
            this.BtnScan.ToolTipText = "Scan";
            this.BtnScan.Visible = false;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(42, 336);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 13);
            this.lblStatus.TabIndex = 113;
            // 
            // FrmProjects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 326);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.GrpProjectCreation);
            this.Controls.Add(this.ProjectCreationStatusStrip);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ProjectsBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmProjects";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Projects";
            this.Load += new System.EventHandler(this.FrmProjects_Load);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeMasterBindingSource1)).EndInit();
            this.GrpProjectCreation.ResumeLayout(false);
            this.GrpProjectCreation.PerformLayout();
            this.ProjectCreationStatusStrip.ResumeLayout(false);
            this.ProjectCreationStatusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errProjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectsBindingNavigator)).EndInit();
            this.ProjectsBindingNavigator.ResumeLayout(false);
            this.ProjectsBindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStripStatusLabel ProjectCreationStatusLabel;
        internal System.Windows.Forms.BindingSource EmployeeMasterBindingSource1;
        internal System.Windows.Forms.GroupBox GrpProjectCreation;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.ComboBox CboProjectManager;
        internal System.Windows.Forms.TextBox txtProjectCode;
        internal System.Windows.Forms.TextBox txtDescription;
        internal System.Windows.Forms.DateTimePicker dtpEndDate;
        internal System.Windows.Forms.DateTimePicker dtpStartDate;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        internal System.Windows.Forms.StatusStrip ProjectCreationStatusStrip;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.Button btnOK;
        internal System.Windows.Forms.ErrorProvider errProjects;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.BindingNavigator ProjectsBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton btnBnSave;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BtnScan;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        private NumericTextBox txtEstimatedAmount;
        private System.Windows.Forms.Timer tmProjects;
        private System.Windows.Forms.Label lblStatus;
    }
}