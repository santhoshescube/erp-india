﻿namespace MyBooksERP
{
    partial class FrmVendor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label LblCountry;
            System.Windows.Forms.Label LblTelephone;
            System.Windows.Forms.Label LblAddress;
            System.Windows.Forms.Label Label25;
            System.Windows.Forms.Label LblAddressName;
            System.Windows.Forms.Label LblContactPerson;
            System.Windows.Forms.Label LblState;
            System.Windows.Forms.Label LblZip;
            System.Windows.Forms.Label LblSupplierName;
            System.Windows.Forms.Label LblFax;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label15;
            System.Windows.Forms.Label label16;
            System.Windows.Forms.Label label20;
            System.Windows.Forms.Label label24;
            System.Windows.Forms.Label label26;
            System.Windows.Forms.Label label27;
            System.Windows.Forms.Label label28;
            System.Windows.Forms.Label label29;
            System.Windows.Forms.Label label30;
            System.Windows.Forms.Label label31;
            System.Windows.Forms.Label label32;
            System.Windows.Forms.Label lblNearestComp;
            System.Windows.Forms.Label lblMobileNo;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVendor));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlVendorInfo = new System.Windows.Forms.Panel();
            this.TbAssociates = new System.Windows.Forms.TabControl();
            this.TbGeneral = new System.Windows.Forms.TabPage();
            this.pnlAssociates = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.txtTRN = new System.Windows.Forms.TextBox();
            this.lblIBAN = new System.Windows.Forms.Label();
            this.lblWebsite = new System.Windows.Forms.Label();
            this.lblSourceMedia = new System.Windows.Forms.Label();
            this.lblCreditLimitDays = new System.Windows.Forms.Label();
            this.txtCreditLimitDays = new DemoClsDataGridview.DecimalTextBox();
            this.txtCreditLimit = new DemoClsDataGridview.DecimalTextBox();
            this.lblCreditLimit = new System.Windows.Forms.Label();
            this.btnSourceMedia = new System.Windows.Forms.Button();
            this.lblAdvanceLimit = new System.Windows.Forms.Label();
            this.txtAdvanceLimit = new DemoClsDataGridview.DecimalTextBox();
            this.TxtWebsite = new System.Windows.Forms.TextBox();
            this.cboSourceMedia = new System.Windows.Forms.ComboBox();
            this.TxtAccountNo = new System.Windows.Forms.TextBox();
            this.LblAccountNo = new System.Windows.Forms.Label();
            this.btnBankName = new System.Windows.Forms.Button();
            this.LblVendorType = new System.Windows.Forms.Label();
            this.lblSupClassifications = new System.Windows.Forms.Label();
            this.CboSupClassification = new System.Windows.Forms.ComboBox();
            this.btnCurrencyReference = new System.Windows.Forms.Button();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.CboStatus = new System.Windows.Forms.ComboBox();
            this.txtRatingRemarks = new System.Windows.Forms.TextBox();
            this.lblRatingRemarks = new System.Windows.Forms.Label();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.txtRating = new System.Windows.Forms.TextBox();
            this.lblRating = new System.Windows.Forms.Label();
            this.cboBankBranch = new System.Windows.Forms.ComboBox();
            this.lblBankBranch = new System.Windows.Forms.Label();
            this.BtnBank = new System.Windows.Forms.Button();
            this.BtnAccountHead = new System.Windows.Forms.Button();
            this.LblAccountHead = new System.Windows.Forms.Label();
            this.cboAccountHead = new System.Windows.Forms.ComboBox();
            this.LblSupplierCode = new System.Windows.Forms.Label();
            this.TxtCode = new System.Windows.Forms.TextBox();
            this.TxtVendorName = new System.Windows.Forms.TextBox();
            this.CboBankName = new System.Windows.Forms.ComboBox();
            this.LblBankName = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.TbContact = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtMobileNo = new System.Windows.Forms.TextBox();
            this.btnAddress = new System.Windows.Forms.Button();
            this.btnContextmenu = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tlsBtnAddressSave = new System.Windows.Forms.ToolStripButton();
            this.TlsBtnAddressDelete = new System.Windows.Forms.ToolStripButton();
            this.TxtAddressName = new System.Windows.Forms.TextBox();
            this.lblEmailID = new System.Windows.Forms.Label();
            this.TxtZipCode = new System.Windows.Forms.TextBox();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.TxtState = new System.Windows.Forms.TextBox();
            this.txtFax = new System.Windows.Forms.TextBox();
            this.TxtContactPerson = new System.Windows.Forms.TextBox();
            this.cboCountry = new System.Windows.Forms.ComboBox();
            this.TxtTelephone = new System.Windows.Forms.TextBox();
            this.TxtAddress = new System.Windows.Forms.TextBox();
            this.btnCountryoforgin = new System.Windows.Forms.Button();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.TbNearestCompetitor = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dgvNearestCompetitor = new System.Windows.Forms.DataGridView();
            this.ColOtherSuppliers = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColRating = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shapeContainer7 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape9 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeContainer6 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape8 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.VendorInformationBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.VendorInformationBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.CancelToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnActions = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnDocuments = new System.Windows.Forms.ToolStripMenuItem();
            this.historyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnVendorMapping = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.EmailStripButton = new System.Windows.Forms.ToolStripButton();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.ErrProVendor = new System.Windows.Forms.ErrorProvider(this.components);
            this.TmVendor = new System.Windows.Forms.Timer(this.components);
            this.SStripVendor = new System.Windows.Forms.StatusStrip();
            this.LblVendorStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.CMSAssociates = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.BnSearch = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.TxtSearchText = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.CboSearchOption = new System.Windows.Forms.ToolStripComboBox();
            this.BtnSearch = new System.Windows.Forms.ToolStripButton();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.shapeContainer3 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.decimalTextBox1 = new DemoClsDataGridview.DecimalTextBox();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.shapeContainer4 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            LblCountry = new System.Windows.Forms.Label();
            LblTelephone = new System.Windows.Forms.Label();
            LblAddress = new System.Windows.Forms.Label();
            Label25 = new System.Windows.Forms.Label();
            LblAddressName = new System.Windows.Forms.Label();
            LblContactPerson = new System.Windows.Forms.Label();
            LblState = new System.Windows.Forms.Label();
            LblZip = new System.Windows.Forms.Label();
            LblSupplierName = new System.Windows.Forms.Label();
            LblFax = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label15 = new System.Windows.Forms.Label();
            label16 = new System.Windows.Forms.Label();
            label20 = new System.Windows.Forms.Label();
            label24 = new System.Windows.Forms.Label();
            label26 = new System.Windows.Forms.Label();
            label27 = new System.Windows.Forms.Label();
            label28 = new System.Windows.Forms.Label();
            label29 = new System.Windows.Forms.Label();
            label30 = new System.Windows.Forms.Label();
            label31 = new System.Windows.Forms.Label();
            label32 = new System.Windows.Forms.Label();
            lblNearestComp = new System.Windows.Forms.Label();
            lblMobileNo = new System.Windows.Forms.Label();
            this.pnlVendorInfo.SuspendLayout();
            this.TbAssociates.SuspendLayout();
            this.TbGeneral.SuspendLayout();
            this.pnlAssociates.SuspendLayout();
            this.TbContact.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.TbNearestCompetitor.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNearestCompetitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VendorInformationBindingNavigator)).BeginInit();
            this.VendorInformationBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrProVendor)).BeginInit();
            this.SStripVendor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BnSearch)).BeginInit();
            this.BnSearch.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // LblCountry
            // 
            LblCountry.AutoSize = true;
            LblCountry.Location = new System.Drawing.Point(12, 200);
            LblCountry.Name = "LblCountry";
            LblCountry.Size = new System.Drawing.Size(43, 13);
            LblCountry.TabIndex = 156;
            LblCountry.Text = "Country";
            // 
            // LblTelephone
            // 
            LblTelephone.AutoSize = true;
            LblTelephone.Location = new System.Drawing.Point(323, 101);
            LblTelephone.Name = "LblTelephone";
            LblTelephone.Size = new System.Drawing.Size(58, 13);
            LblTelephone.TabIndex = 6;
            LblTelephone.Text = "Telephone";
            // 
            // LblAddress
            // 
            LblAddress.AutoSize = true;
            LblAddress.Location = new System.Drawing.Point(12, 101);
            LblAddress.Name = "LblAddress";
            LblAddress.Size = new System.Drawing.Size(45, 13);
            LblAddress.TabIndex = 160;
            LblAddress.Text = "Address";
            // 
            // Label25
            // 
            Label25.AutoSize = true;
            Label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label25.Location = new System.Drawing.Point(3, 7);
            Label25.Name = "Label25";
            Label25.Size = new System.Drawing.Size(160, 13);
            Label25.TabIndex = 170;
            Label25.Text = "Contact && Other Addresses";
            // 
            // LblAddressName
            // 
            LblAddressName.AutoSize = true;
            LblAddressName.Location = new System.Drawing.Point(12, 35);
            LblAddressName.Name = "LblAddressName";
            LblAddressName.Size = new System.Drawing.Size(76, 13);
            LblAddressName.TabIndex = 169;
            LblAddressName.Text = "Address Name";
            // 
            // LblContactPerson
            // 
            LblContactPerson.AutoSize = true;
            LblContactPerson.Location = new System.Drawing.Point(12, 68);
            LblContactPerson.Name = "LblContactPerson";
            LblContactPerson.Size = new System.Drawing.Size(80, 13);
            LblContactPerson.TabIndex = 172;
            LblContactPerson.Text = "Contact Person";
            // 
            // LblState
            // 
            LblState.AutoSize = true;
            LblState.Location = new System.Drawing.Point(323, 35);
            LblState.Name = "LblState";
            LblState.Size = new System.Drawing.Size(54, 13);
            LblState.TabIndex = 173;
            LblState.Text = "State/City";
            // 
            // LblZip
            // 
            LblZip.AutoSize = true;
            LblZip.Location = new System.Drawing.Point(323, 68);
            LblZip.Name = "LblZip";
            LblZip.Size = new System.Drawing.Size(50, 13);
            LblZip.TabIndex = 176;
            LblZip.Text = "Zip Code";
            // 
            // LblSupplierName
            // 
            LblSupplierName.AutoSize = true;
            LblSupplierName.Location = new System.Drawing.Point(5, 31);
            LblSupplierName.Name = "LblSupplierName";
            LblSupplierName.Size = new System.Drawing.Size(35, 13);
            LblSupplierName.TabIndex = 21;
            LblSupplierName.Text = "Name";
            // 
            // LblFax
            // 
            LblFax.AutoSize = true;
            LblFax.Location = new System.Drawing.Point(323, 167);
            LblFax.Name = "LblFax";
            LblFax.Size = new System.Drawing.Size(41, 13);
            LblFax.TabIndex = 191;
            LblFax.Text = "Fax No";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(3, 7);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(122, 13);
            label1.TabIndex = 3;
            label1.Text = "General Information";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.Location = new System.Drawing.Point(6, 17);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(122, 13);
            label4.TabIndex = 3;
            label4.Text = "General Information";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(22, 203);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(32, 13);
            label12.TabIndex = 210;
            label12.Text = "Email";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new System.Drawing.Point(22, 230);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(46, 13);
            label13.TabIndex = 209;
            label13.Text = "Website";
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Location = new System.Drawing.Point(22, 41);
            label15.Name = "label15";
            label15.Size = new System.Drawing.Size(35, 13);
            label15.TabIndex = 203;
            label15.Text = "Name";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Location = new System.Drawing.Point(22, 68);
            label16.Name = "label16";
            label16.Size = new System.Drawing.Size(63, 13);
            label16.TabIndex = 204;
            label16.Text = "Short Name";
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Location = new System.Drawing.Point(14, 257);
            label20.Name = "label20";
            label20.Size = new System.Drawing.Size(24, 13);
            label20.TabIndex = 191;
            label20.Text = "Fax";
            // 
            // label24
            // 
            label24.AutoSize = true;
            label24.Location = new System.Drawing.Point(261, 178);
            label24.Name = "label24";
            label24.Size = new System.Drawing.Size(50, 13);
            label24.TabIndex = 176;
            label24.Text = "Zip Code";
            // 
            // label26
            // 
            label26.AutoSize = true;
            label26.Location = new System.Drawing.Point(261, 151);
            label26.Name = "label26";
            label26.Size = new System.Drawing.Size(54, 13);
            label26.TabIndex = 173;
            label26.Text = "State/City";
            // 
            // label27
            // 
            label27.AutoSize = true;
            label27.Location = new System.Drawing.Point(10, 58);
            label27.Name = "label27";
            label27.Size = new System.Drawing.Size(80, 13);
            label27.TabIndex = 172;
            label27.Text = "Contact Person";
            // 
            // label28
            // 
            label28.AutoSize = true;
            label28.Location = new System.Drawing.Point(10, 35);
            label28.Name = "label28";
            label28.Size = new System.Drawing.Size(76, 13);
            label28.TabIndex = 169;
            label28.Text = "Address Name";
            // 
            // label29
            // 
            label29.AutoSize = true;
            label29.Location = new System.Drawing.Point(10, 80);
            label29.Name = "label29";
            label29.Size = new System.Drawing.Size(45, 13);
            label29.TabIndex = 160;
            label29.Text = "Address";
            // 
            // label30
            // 
            label30.AutoSize = true;
            label30.Location = new System.Drawing.Point(14, 151);
            label30.Name = "label30";
            label30.Size = new System.Drawing.Size(56, 13);
            label30.TabIndex = 156;
            label30.Text = "Nationality";
            // 
            // label31
            // 
            label31.AutoSize = true;
            label31.Location = new System.Drawing.Point(14, 205);
            label31.Name = "label31";
            label31.Size = new System.Drawing.Size(58, 13);
            label31.TabIndex = 6;
            label31.Text = "Telephone";
            // 
            // label32
            // 
            label32.AutoSize = true;
            label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label32.Location = new System.Drawing.Point(8, 9);
            label32.Name = "label32";
            label32.Size = new System.Drawing.Size(160, 13);
            label32.TabIndex = 170;
            label32.Text = "Contact && Other Addresses";
            // 
            // lblNearestComp
            // 
            lblNearestComp.AutoSize = true;
            lblNearestComp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblNearestComp.Location = new System.Drawing.Point(3, 3);
            lblNearestComp.Name = "lblNearestComp";
            lblNearestComp.Size = new System.Drawing.Size(160, 13);
            lblNearestComp.TabIndex = 228;
            lblNearestComp.Text = "Nearest Competitor Details";
            // 
            // lblMobileNo
            // 
            lblMobileNo.AutoSize = true;
            lblMobileNo.Location = new System.Drawing.Point(323, 134);
            lblMobileNo.Name = "lblMobileNo";
            lblMobileNo.Size = new System.Drawing.Size(55, 13);
            lblMobileNo.TabIndex = 194;
            lblMobileNo.Text = "Mobile No";
            // 
            // pnlVendorInfo
            // 
            this.pnlVendorInfo.Controls.Add(this.TbAssociates);
            this.pnlVendorInfo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pnlVendorInfo.Location = new System.Drawing.Point(8, 52);
            this.pnlVendorInfo.Name = "pnlVendorInfo";
            this.pnlVendorInfo.Size = new System.Drawing.Size(607, 283);
            this.pnlVendorInfo.TabIndex = 0;
            // 
            // TbAssociates
            // 
            this.TbAssociates.Controls.Add(this.TbGeneral);
            this.TbAssociates.Controls.Add(this.TbContact);
            this.TbAssociates.Controls.Add(this.TbNearestCompetitor);
            this.TbAssociates.Location = new System.Drawing.Point(2, 2);
            this.TbAssociates.Name = "TbAssociates";
            this.TbAssociates.SelectedIndex = 0;
            this.TbAssociates.Size = new System.Drawing.Size(605, 282);
            this.TbAssociates.TabIndex = 0;
            // 
            // TbGeneral
            // 
            this.TbGeneral.Controls.Add(label1);
            this.TbGeneral.Controls.Add(this.pnlAssociates);
            this.TbGeneral.Controls.Add(this.shapeContainer2);
            this.TbGeneral.Location = new System.Drawing.Point(4, 22);
            this.TbGeneral.Name = "TbGeneral";
            this.TbGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.TbGeneral.Size = new System.Drawing.Size(597, 256);
            this.TbGeneral.TabIndex = 0;
            this.TbGeneral.Text = "General";
            this.TbGeneral.UseVisualStyleBackColor = true;
            // 
            // pnlAssociates
            // 
            this.pnlAssociates.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAssociates.Controls.Add(this.label33);
            this.pnlAssociates.Controls.Add(this.txtTRN);
            this.pnlAssociates.Controls.Add(this.lblIBAN);
            this.pnlAssociates.Controls.Add(this.lblWebsite);
            this.pnlAssociates.Controls.Add(this.lblSourceMedia);
            this.pnlAssociates.Controls.Add(this.lblCreditLimitDays);
            this.pnlAssociates.Controls.Add(this.txtCreditLimitDays);
            this.pnlAssociates.Controls.Add(this.txtCreditLimit);
            this.pnlAssociates.Controls.Add(this.lblCreditLimit);
            this.pnlAssociates.Controls.Add(this.btnSourceMedia);
            this.pnlAssociates.Controls.Add(this.lblAdvanceLimit);
            this.pnlAssociates.Controls.Add(this.txtAdvanceLimit);
            this.pnlAssociates.Controls.Add(this.TxtWebsite);
            this.pnlAssociates.Controls.Add(this.cboSourceMedia);
            this.pnlAssociates.Controls.Add(this.TxtAccountNo);
            this.pnlAssociates.Controls.Add(this.LblAccountNo);
            this.pnlAssociates.Controls.Add(this.btnBankName);
            this.pnlAssociates.Controls.Add(this.LblVendorType);
            this.pnlAssociates.Controls.Add(this.lblSupClassifications);
            this.pnlAssociates.Controls.Add(this.CboSupClassification);
            this.pnlAssociates.Controls.Add(this.btnCurrencyReference);
            this.pnlAssociates.Controls.Add(this.cboCurrency);
            this.pnlAssociates.Controls.Add(this.lblStatus);
            this.pnlAssociates.Controls.Add(this.CboStatus);
            this.pnlAssociates.Controls.Add(this.txtRatingRemarks);
            this.pnlAssociates.Controls.Add(this.lblRatingRemarks);
            this.pnlAssociates.Controls.Add(this.lblCurrency);
            this.pnlAssociates.Controls.Add(this.txtRating);
            this.pnlAssociates.Controls.Add(this.lblRating);
            this.pnlAssociates.Controls.Add(this.cboBankBranch);
            this.pnlAssociates.Controls.Add(this.lblBankBranch);
            this.pnlAssociates.Controls.Add(this.BtnBank);
            this.pnlAssociates.Controls.Add(this.BtnAccountHead);
            this.pnlAssociates.Controls.Add(this.LblAccountHead);
            this.pnlAssociates.Controls.Add(this.cboAccountHead);
            this.pnlAssociates.Controls.Add(this.LblSupplierCode);
            this.pnlAssociates.Controls.Add(this.TxtCode);
            this.pnlAssociates.Controls.Add(LblSupplierName);
            this.pnlAssociates.Controls.Add(this.TxtVendorName);
            this.pnlAssociates.Controls.Add(this.CboBankName);
            this.pnlAssociates.Controls.Add(this.LblBankName);
            this.pnlAssociates.Location = new System.Drawing.Point(6, 23);
            this.pnlAssociates.Name = "pnlAssociates";
            this.pnlAssociates.Size = new System.Drawing.Size(586, 230);
            this.pnlAssociates.TabIndex = 0;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(5, 205);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(30, 13);
            this.label33.TabIndex = 296;
            this.label33.Text = "TRN";
            // 
            // txtTRN
            // 
            this.txtTRN.Location = new System.Drawing.Point(100, 202);
            this.txtTRN.MaxLength = 50;
            this.txtTRN.Name = "txtTRN";
            this.txtTRN.Size = new System.Drawing.Size(142, 20);
            this.txtTRN.TabIndex = 295;
            this.txtTRN.TextChanged += new System.EventHandler(this.txtTRN_TextChanged);
            // 
            // lblIBAN
            // 
            this.lblIBAN.AutoSize = true;
            this.lblIBAN.Location = new System.Drawing.Point(5, 130);
            this.lblIBAN.Name = "lblIBAN";
            this.lblIBAN.Size = new System.Drawing.Size(94, 13);
            this.lblIBAN.TabIndex = 294;
            this.lblIBAN.Text = "IBAN/Account No";
            // 
            // lblWebsite
            // 
            this.lblWebsite.AutoSize = true;
            this.lblWebsite.Location = new System.Drawing.Point(5, 181);
            this.lblWebsite.Name = "lblWebsite";
            this.lblWebsite.Size = new System.Drawing.Size(46, 13);
            this.lblWebsite.TabIndex = 33;
            this.lblWebsite.Text = "Website";
            // 
            // lblSourceMedia
            // 
            this.lblSourceMedia.AutoSize = true;
            this.lblSourceMedia.Location = new System.Drawing.Point(307, 193);
            this.lblSourceMedia.Name = "lblSourceMedia";
            this.lblSourceMedia.Size = new System.Drawing.Size(73, 13);
            this.lblSourceMedia.TabIndex = 293;
            this.lblSourceMedia.Text = "Source Media";
            // 
            // lblCreditLimitDays
            // 
            this.lblCreditLimitDays.AutoSize = true;
            this.lblCreditLimitDays.Location = new System.Drawing.Point(307, 111);
            this.lblCreditLimitDays.Name = "lblCreditLimitDays";
            this.lblCreditLimitDays.Size = new System.Drawing.Size(85, 13);
            this.lblCreditLimitDays.TabIndex = 211;
            this.lblCreditLimitDays.Text = "Credit Limit Days";
            // 
            // txtCreditLimitDays
            // 
            this.txtCreditLimitDays.BackColor = System.Drawing.SystemColors.Window;
            this.txtCreditLimitDays.Location = new System.Drawing.Point(400, 108);
            this.txtCreditLimitDays.MaxLength = 3;
            this.txtCreditLimitDays.Name = "txtCreditLimitDays";
            this.txtCreditLimitDays.ShortcutsEnabled = false;
            this.txtCreditLimitDays.Size = new System.Drawing.Size(142, 20);
            this.txtCreditLimitDays.TabIndex = 212;
            this.txtCreditLimitDays.TextChanged += new System.EventHandler(this.txtCreditLimitDays_TextChanged);
            // 
            // txtCreditLimit
            // 
            this.txtCreditLimit.BackColor = System.Drawing.SystemColors.Window;
            this.txtCreditLimit.Location = new System.Drawing.Point(400, 81);
            this.txtCreditLimit.MaxLength = 10;
            this.txtCreditLimit.Name = "txtCreditLimit";
            this.txtCreditLimit.ShortcutsEnabled = false;
            this.txtCreditLimit.Size = new System.Drawing.Size(142, 20);
            this.txtCreditLimit.TabIndex = 14;
            this.txtCreditLimit.TextChanged += new System.EventHandler(this.txtCreditLimit_TextChanged);
            // 
            // lblCreditLimit
            // 
            this.lblCreditLimit.AutoSize = true;
            this.lblCreditLimit.Location = new System.Drawing.Point(307, 84);
            this.lblCreditLimit.Name = "lblCreditLimit";
            this.lblCreditLimit.Size = new System.Drawing.Size(58, 13);
            this.lblCreditLimit.TabIndex = 35;
            this.lblCreditLimit.Text = "Credit Limit";
            // 
            // btnSourceMedia
            // 
            this.btnSourceMedia.Location = new System.Drawing.Point(548, 188);
            this.btnSourceMedia.Name = "btnSourceMedia";
            this.btnSourceMedia.Size = new System.Drawing.Size(30, 23);
            this.btnSourceMedia.TabIndex = 12;
            this.btnSourceMedia.Text = "...";
            this.btnSourceMedia.UseVisualStyleBackColor = true;
            this.btnSourceMedia.Click += new System.EventHandler(this.btnSourceMedia_Click);
            // 
            // lblAdvanceLimit
            // 
            this.lblAdvanceLimit.AutoSize = true;
            this.lblAdvanceLimit.Location = new System.Drawing.Point(307, 57);
            this.lblAdvanceLimit.Name = "lblAdvanceLimit";
            this.lblAdvanceLimit.Size = new System.Drawing.Size(91, 13);
            this.lblAdvanceLimit.TabIndex = 34;
            this.lblAdvanceLimit.Text = "Advance Limit (%)";
            // 
            // txtAdvanceLimit
            // 
            this.txtAdvanceLimit.BackColor = System.Drawing.SystemColors.Window;
            this.txtAdvanceLimit.Location = new System.Drawing.Point(400, 54);
            this.txtAdvanceLimit.MaxLength = 3;
            this.txtAdvanceLimit.Name = "txtAdvanceLimit";
            this.txtAdvanceLimit.ShortcutsEnabled = false;
            this.txtAdvanceLimit.Size = new System.Drawing.Size(141, 20);
            this.txtAdvanceLimit.TabIndex = 15;
            this.txtAdvanceLimit.TextChanged += new System.EventHandler(this.txtAdvanceLimit_TextChanged);
            // 
            // TxtWebsite
            // 
            this.TxtWebsite.Location = new System.Drawing.Point(100, 178);
            this.TxtWebsite.MaxLength = 50;
            this.TxtWebsite.Name = "TxtWebsite";
            this.TxtWebsite.Size = new System.Drawing.Size(142, 20);
            this.TxtWebsite.TabIndex = 13;
            this.TxtWebsite.TextChanged += new System.EventHandler(this.ButtonStatus);
            // 
            // cboSourceMedia
            // 
            this.cboSourceMedia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSourceMedia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSourceMedia.BackColor = System.Drawing.Color.White;
            this.cboSourceMedia.DropDownHeight = 75;
            this.cboSourceMedia.FormattingEnabled = true;
            this.cboSourceMedia.IntegralHeight = false;
            this.cboSourceMedia.Location = new System.Drawing.Point(400, 190);
            this.cboSourceMedia.Name = "cboSourceMedia";
            this.cboSourceMedia.Size = new System.Drawing.Size(142, 21);
            this.cboSourceMedia.TabIndex = 11;
            // 
            // TxtAccountNo
            // 
            this.TxtAccountNo.BackColor = System.Drawing.SystemColors.Window;
            this.TxtAccountNo.Location = new System.Drawing.Point(100, 128);
            this.TxtAccountNo.MaxLength = 20;
            this.TxtAccountNo.Name = "TxtAccountNo";
            this.TxtAccountNo.Size = new System.Drawing.Size(138, 20);
            this.TxtAccountNo.TabIndex = 6;
            this.TxtAccountNo.TextChanged += new System.EventHandler(this.TxtAccountNo_TextChanged);
            // 
            // LblAccountNo
            // 
            this.LblAccountNo.AutoSize = true;
            this.LblAccountNo.Location = new System.Drawing.Point(455, 282);
            this.LblAccountNo.Name = "LblAccountNo";
            this.LblAccountNo.Size = new System.Drawing.Size(87, 13);
            this.LblAccountNo.TabIndex = 207;
            this.LblAccountNo.Text = "Account Number";
            this.LblAccountNo.Visible = false;
            // 
            // btnBankName
            // 
            this.btnBankName.Location = new System.Drawing.Point(248, 76);
            this.btnBankName.Name = "btnBankName";
            this.btnBankName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnBankName.Size = new System.Drawing.Size(30, 23);
            this.btnBankName.TabIndex = 6;
            this.btnBankName.Text = "...";
            this.btnBankName.UseVisualStyleBackColor = true;
            this.btnBankName.Click += new System.EventHandler(this.btnBankName_Click);
            // 
            // LblVendorType
            // 
            this.LblVendorType.AutoSize = true;
            this.LblVendorType.Location = new System.Drawing.Point(356, 282);
            this.LblVendorType.Name = "LblVendorType";
            this.LblVendorType.Size = new System.Drawing.Size(80, 13);
            this.LblVendorType.TabIndex = 2;
            this.LblVendorType.Text = "Associate Type";
            this.LblVendorType.Visible = false;
            // 
            // lblSupClassifications
            // 
            this.lblSupClassifications.AutoSize = true;
            this.lblSupClassifications.Location = new System.Drawing.Point(5, 55);
            this.lblSupClassifications.Name = "lblSupClassifications";
            this.lblSupClassifications.Size = new System.Drawing.Size(68, 13);
            this.lblSupClassifications.TabIndex = 23;
            this.lblSupClassifications.Text = "Classification";
            // 
            // CboSupClassification
            // 
            this.CboSupClassification.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSupClassification.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSupClassification.DropDownHeight = 75;
            this.CboSupClassification.FormattingEnabled = true;
            this.CboSupClassification.IntegralHeight = false;
            this.CboSupClassification.Location = new System.Drawing.Point(100, 52);
            this.CboSupClassification.Name = "CboSupClassification";
            this.CboSupClassification.Size = new System.Drawing.Size(142, 21);
            this.CboSupClassification.TabIndex = 3;
            this.CboSupClassification.SelectionChangeCommitted += new System.EventHandler(this.CboSupClassification_SelectionChangeCommitted);
            this.CboSupClassification.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            this.CboSupClassification.SelectedValueChanged += new System.EventHandler(this.CboSupClassification_SelectedValueChanged);
            // 
            // btnCurrencyReference
            // 
            this.btnCurrencyReference.Location = new System.Drawing.Point(548, 26);
            this.btnCurrencyReference.Name = "btnCurrencyReference";
            this.btnCurrencyReference.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCurrencyReference.Size = new System.Drawing.Size(30, 23);
            this.btnCurrencyReference.TabIndex = 209;
            this.btnCurrencyReference.Text = "...";
            this.btnCurrencyReference.UseVisualStyleBackColor = true;
            this.btnCurrencyReference.Click += new System.EventHandler(this.btnCurrencyReference_Click);
            // 
            // cboCurrency
            // 
            this.cboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCurrency.BackColor = System.Drawing.SystemColors.Info;
            this.cboCurrency.DropDownHeight = 75;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.IntegralHeight = false;
            this.cboCurrency.Location = new System.Drawing.Point(400, 28);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(142, 21);
            this.cboCurrency.TabIndex = 208;
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(307, 8);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 28;
            this.lblStatus.Text = "Status";
            // 
            // CboStatus
            // 
            this.CboStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboStatus.BackColor = System.Drawing.SystemColors.Info;
            this.CboStatus.DropDownHeight = 75;
            this.CboStatus.FormattingEnabled = true;
            this.CboStatus.IntegralHeight = false;
            this.CboStatus.Location = new System.Drawing.Point(400, 3);
            this.CboStatus.Name = "CboStatus";
            this.CboStatus.Size = new System.Drawing.Size(142, 21);
            this.CboStatus.TabIndex = 9;
            this.CboStatus.SelectedIndexChanged += new System.EventHandler(this.ButtonStatus);
            this.CboStatus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            this.CboStatus.SelectedValueChanged += new System.EventHandler(this.CboStatus_SelectedValueChanged);
            // 
            // txtRatingRemarks
            // 
            this.txtRatingRemarks.Location = new System.Drawing.Point(400, 162);
            this.txtRatingRemarks.MaxLength = 499;
            this.txtRatingRemarks.Name = "txtRatingRemarks";
            this.txtRatingRemarks.Size = new System.Drawing.Size(142, 20);
            this.txtRatingRemarks.TabIndex = 11;
            this.txtRatingRemarks.TextChanged += new System.EventHandler(this.ButtonStatus);
            // 
            // lblRatingRemarks
            // 
            this.lblRatingRemarks.AutoSize = true;
            this.lblRatingRemarks.Location = new System.Drawing.Point(307, 165);
            this.lblRatingRemarks.Name = "lblRatingRemarks";
            this.lblRatingRemarks.Size = new System.Drawing.Size(83, 13);
            this.lblRatingRemarks.TabIndex = 30;
            this.lblRatingRemarks.Text = "Rating Remarks";
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Location = new System.Drawing.Point(307, 31);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(49, 13);
            this.lblCurrency.TabIndex = 210;
            this.lblCurrency.Text = "Currency";
            // 
            // txtRating
            // 
            this.txtRating.Location = new System.Drawing.Point(400, 135);
            this.txtRating.MaxLength = 3;
            this.txtRating.Name = "txtRating";
            this.txtRating.Size = new System.Drawing.Size(142, 20);
            this.txtRating.TabIndex = 10;
            this.txtRating.TextChanged += new System.EventHandler(this.ButtonStatus);
            // 
            // lblRating
            // 
            this.lblRating.AutoSize = true;
            this.lblRating.Location = new System.Drawing.Point(307, 136);
            this.lblRating.Name = "lblRating";
            this.lblRating.Size = new System.Drawing.Size(38, 13);
            this.lblRating.TabIndex = 29;
            this.lblRating.Text = "Rating";
            // 
            // cboBankBranch
            // 
            this.cboBankBranch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBankBranch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBankBranch.BackColor = System.Drawing.SystemColors.Window;
            this.cboBankBranch.DropDownHeight = 75;
            this.cboBankBranch.FormattingEnabled = true;
            this.cboBankBranch.IntegralHeight = false;
            this.cboBankBranch.Location = new System.Drawing.Point(100, 103);
            this.cboBankBranch.Name = "cboBankBranch";
            this.cboBankBranch.Size = new System.Drawing.Size(142, 21);
            this.cboBankBranch.TabIndex = 7;
            this.cboBankBranch.SelectedIndexChanged += new System.EventHandler(this.ButtonStatus);
            this.cboBankBranch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblBankBranch
            // 
            this.lblBankBranch.AutoSize = true;
            this.lblBankBranch.Location = new System.Drawing.Point(5, 106);
            this.lblBankBranch.Name = "lblBankBranch";
            this.lblBankBranch.Size = new System.Drawing.Size(41, 13);
            this.lblBankBranch.TabIndex = 26;
            this.lblBankBranch.Text = "Branch";
            // 
            // BtnBank
            // 
            this.BtnBank.Location = new System.Drawing.Point(248, 103);
            this.BtnBank.Name = "BtnBank";
            this.BtnBank.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.BtnBank.Size = new System.Drawing.Size(30, 23);
            this.BtnBank.TabIndex = 8;
            this.BtnBank.Text = "...";
            this.BtnBank.UseVisualStyleBackColor = true;
            this.BtnBank.Click += new System.EventHandler(this.BtnBank_Click);
            // 
            // BtnAccountHead
            // 
            this.BtnAccountHead.Location = new System.Drawing.Point(248, 152);
            this.BtnAccountHead.Name = "BtnAccountHead";
            this.BtnAccountHead.Size = new System.Drawing.Size(30, 23);
            this.BtnAccountHead.TabIndex = 16;
            this.BtnAccountHead.Text = "...";
            this.BtnAccountHead.UseVisualStyleBackColor = true;
            this.BtnAccountHead.Click += new System.EventHandler(this.BtnAccountHead_Click);
            // 
            // LblAccountHead
            // 
            this.LblAccountHead.AutoSize = true;
            this.LblAccountHead.Location = new System.Drawing.Point(5, 155);
            this.LblAccountHead.Name = "LblAccountHead";
            this.LblAccountHead.Size = new System.Drawing.Size(76, 13);
            this.LblAccountHead.TabIndex = 31;
            this.LblAccountHead.Text = "Account Head";
            // 
            // cboAccountHead
            // 
            this.cboAccountHead.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAccountHead.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAccountHead.BackColor = System.Drawing.Color.White;
            this.cboAccountHead.DropDownHeight = 75;
            this.cboAccountHead.FormattingEnabled = true;
            this.cboAccountHead.IntegralHeight = false;
            this.cboAccountHead.Location = new System.Drawing.Point(100, 152);
            this.cboAccountHead.Name = "cboAccountHead";
            this.cboAccountHead.Size = new System.Drawing.Size(142, 21);
            this.cboAccountHead.TabIndex = 15;
            this.cboAccountHead.SelectedIndexChanged += new System.EventHandler(this.ButtonStatus);
            this.cboAccountHead.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // LblSupplierCode
            // 
            this.LblSupplierCode.AutoSize = true;
            this.LblSupplierCode.Location = new System.Drawing.Point(5, 6);
            this.LblSupplierCode.Name = "LblSupplierCode";
            this.LblSupplierCode.Size = new System.Drawing.Size(32, 13);
            this.LblSupplierCode.TabIndex = 20;
            this.LblSupplierCode.Text = "Code";
            // 
            // TxtCode
            // 
            this.TxtCode.BackColor = System.Drawing.SystemColors.Info;
            this.TxtCode.Location = new System.Drawing.Point(100, 4);
            this.TxtCode.MaxLength = 30;
            this.TxtCode.Name = "TxtCode";
            this.TxtCode.Size = new System.Drawing.Size(142, 20);
            this.TxtCode.TabIndex = 0;
            this.TxtCode.Text = " ";
            this.TxtCode.TextChanged += new System.EventHandler(this.TxtCode_TextChanged);
            // 
            // TxtVendorName
            // 
            this.TxtVendorName.BackColor = System.Drawing.SystemColors.Info;
            this.TxtVendorName.Location = new System.Drawing.Point(100, 28);
            this.TxtVendorName.MaxLength = 50;
            this.TxtVendorName.Name = "TxtVendorName";
            this.TxtVendorName.Size = new System.Drawing.Size(142, 20);
            this.TxtVendorName.TabIndex = 1;
            this.TxtVendorName.TextChanged += new System.EventHandler(this.TxtVendorName_TextChanged);
            // 
            // CboBankName
            // 
            this.CboBankName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboBankName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboBankName.BackColor = System.Drawing.SystemColors.Window;
            this.CboBankName.DropDownHeight = 75;
            this.CboBankName.FormattingEnabled = true;
            this.CboBankName.IntegralHeight = false;
            this.CboBankName.Location = new System.Drawing.Point(100, 77);
            this.CboBankName.Name = "CboBankName";
            this.CboBankName.Size = new System.Drawing.Size(142, 21);
            this.CboBankName.TabIndex = 5;
            this.CboBankName.SelectedIndexChanged += new System.EventHandler(this.CboBankName_SelectedIndexChanged);
            this.CboBankName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // LblBankName
            // 
            this.LblBankName.AutoSize = true;
            this.LblBankName.Location = new System.Drawing.Point(5, 80);
            this.LblBankName.Name = "LblBankName";
            this.LblBankName.Size = new System.Drawing.Size(63, 13);
            this.LblBankName.TabIndex = 25;
            this.LblBankName.Text = "Bank Name";
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3,
            this.lineShape2});
            this.shapeContainer2.Size = new System.Drawing.Size(591, 250);
            this.shapeContainer2.TabIndex = 4;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape3
            // 
            this.lineShape3.BorderColor = System.Drawing.Color.Silver;
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 100;
            this.lineShape3.X2 = 585;
            this.lineShape3.Y1 = 11;
            this.lineShape3.Y2 = 11;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape2.BorderWidth = 2;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 96;
            this.lineShape2.X2 = 507;
            this.lineShape2.Y1 = 171;
            this.lineShape2.Y2 = 171;
            // 
            // TbContact
            // 
            this.TbContact.Controls.Add(this.panel1);
            this.TbContact.Controls.Add(Label25);
            this.TbContact.Controls.Add(this.shapeContainer1);
            this.TbContact.Location = new System.Drawing.Point(4, 22);
            this.TbContact.Name = "TbContact";
            this.TbContact.Padding = new System.Windows.Forms.Padding(3);
            this.TbContact.Size = new System.Drawing.Size(597, 256);
            this.TbContact.TabIndex = 1;
            this.TbContact.Text = "Contact Information";
            this.TbContact.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(lblMobileNo);
            this.panel1.Controls.Add(this.txtMobileNo);
            this.panel1.Controls.Add(this.btnAddress);
            this.panel1.Controls.Add(this.btnContextmenu);
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Controls.Add(this.TxtAddressName);
            this.panel1.Controls.Add(this.lblEmailID);
            this.panel1.Controls.Add(this.TxtZipCode);
            this.panel1.Controls.Add(LblFax);
            this.panel1.Controls.Add(LblZip);
            this.panel1.Controls.Add(this.TxtEmail);
            this.panel1.Controls.Add(this.TxtState);
            this.panel1.Controls.Add(this.txtFax);
            this.panel1.Controls.Add(LblState);
            this.panel1.Controls.Add(this.TxtContactPerson);
            this.panel1.Controls.Add(LblContactPerson);
            this.panel1.Controls.Add(LblAddressName);
            this.panel1.Controls.Add(this.cboCountry);
            this.panel1.Controls.Add(LblAddress);
            this.panel1.Controls.Add(this.TxtTelephone);
            this.panel1.Controls.Add(this.TxtAddress);
            this.panel1.Controls.Add(LblCountry);
            this.panel1.Controls.Add(LblTelephone);
            this.panel1.Controls.Add(this.btnCountryoforgin);
            this.panel1.Location = new System.Drawing.Point(6, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(585, 228);
            this.panel1.TabIndex = 171;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // txtMobileNo
            // 
            this.txtMobileNo.Location = new System.Drawing.Point(387, 131);
            this.txtMobileNo.MaxLength = 20;
            this.txtMobileNo.Name = "txtMobileNo";
            this.txtMobileNo.Size = new System.Drawing.Size(179, 20);
            this.txtMobileNo.TabIndex = 9;
            this.txtMobileNo.TextChanged += new System.EventHandler(this.ButtonStatus);
            // 
            // btnAddress
            // 
            this.btnAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddress.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnAddress.Location = new System.Drawing.Point(550, 0);
            this.btnAddress.Name = "btnAddress";
            this.btnAddress.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.btnAddress.Size = new System.Drawing.Size(30, 23);
            this.btnAddress.TabIndex = 192;
            this.btnAddress.UseVisualStyleBackColor = true;
            this.btnAddress.Visible = false;
            // 
            // btnContextmenu
            // 
            this.btnContextmenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnContextmenu.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnContextmenu.Location = new System.Drawing.Point(268, 29);
            this.btnContextmenu.Name = "btnContextmenu";
            this.btnContextmenu.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.btnContextmenu.Size = new System.Drawing.Size(30, 23);
            this.btnContextmenu.TabIndex = 1;
            this.btnContextmenu.Text = "4";
            this.btnContextmenu.UseVisualStyleBackColor = true;
            this.btnContextmenu.Click += new System.EventHandler(this.btnContextmenu_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlsBtnAddressSave,
            this.TlsBtnAddressDelete});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(583, 25);
            this.toolStrip1.TabIndex = 19;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tlsBtnAddressSave
            // 
            this.tlsBtnAddressSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tlsBtnAddressSave.Image = ((System.Drawing.Image)(resources.GetObject("tlsBtnAddressSave.Image")));
            this.tlsBtnAddressSave.Name = "tlsBtnAddressSave";
            this.tlsBtnAddressSave.Size = new System.Drawing.Size(23, 22);
            this.tlsBtnAddressSave.Text = "Save";
            this.tlsBtnAddressSave.Click += new System.EventHandler(this.tlsBtnAddressSave_Click);
            // 
            // TlsBtnAddressDelete
            // 
            this.TlsBtnAddressDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TlsBtnAddressDelete.Image = ((System.Drawing.Image)(resources.GetObject("TlsBtnAddressDelete.Image")));
            this.TlsBtnAddressDelete.Name = "TlsBtnAddressDelete";
            this.TlsBtnAddressDelete.RightToLeftAutoMirrorImage = true;
            this.TlsBtnAddressDelete.Size = new System.Drawing.Size(23, 22);
            this.TlsBtnAddressDelete.Text = "Delete";
            this.TlsBtnAddressDelete.Click += new System.EventHandler(this.TlsBtnAddressDelete_Click);
            // 
            // TxtAddressName
            // 
            this.TxtAddressName.Location = new System.Drawing.Point(94, 32);
            this.TxtAddressName.MaxLength = 50;
            this.TxtAddressName.Name = "TxtAddressName";
            this.TxtAddressName.Size = new System.Drawing.Size(167, 20);
            this.TxtAddressName.TabIndex = 0;
            this.TxtAddressName.TextChanged += new System.EventHandler(this.ButtonStatus);
            // 
            // lblEmailID
            // 
            this.lblEmailID.AutoSize = true;
            this.lblEmailID.Location = new System.Drawing.Point(323, 200);
            this.lblEmailID.Name = "lblEmailID";
            this.lblEmailID.Size = new System.Drawing.Size(32, 13);
            this.lblEmailID.TabIndex = 32;
            this.lblEmailID.Text = "Email";
            // 
            // TxtZipCode
            // 
            this.TxtZipCode.Location = new System.Drawing.Point(387, 65);
            this.TxtZipCode.MaxLength = 25;
            this.TxtZipCode.Name = "TxtZipCode";
            this.TxtZipCode.Size = new System.Drawing.Size(179, 20);
            this.TxtZipCode.TabIndex = 7;
            this.TxtZipCode.TextChanged += new System.EventHandler(this.ButtonStatus);
            // 
            // TxtEmail
            // 
            this.TxtEmail.Location = new System.Drawing.Point(387, 197);
            this.TxtEmail.MaxLength = 50;
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(179, 20);
            this.TxtEmail.TabIndex = 10;
            this.TxtEmail.TextChanged += new System.EventHandler(this.ButtonStatus);
            // 
            // TxtState
            // 
            this.TxtState.Location = new System.Drawing.Point(387, 32);
            this.TxtState.MaxLength = 50;
            this.TxtState.Name = "TxtState";
            this.TxtState.Size = new System.Drawing.Size(179, 20);
            this.TxtState.TabIndex = 6;
            this.TxtState.TextChanged += new System.EventHandler(this.ButtonStatus);
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(387, 164);
            this.txtFax.MaxLength = 20;
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(179, 20);
            this.txtFax.TabIndex = 11;
            this.txtFax.TextChanged += new System.EventHandler(this.ButtonStatus);
            // 
            // TxtContactPerson
            // 
            this.TxtContactPerson.BackColor = System.Drawing.SystemColors.Window;
            this.TxtContactPerson.Location = new System.Drawing.Point(94, 65);
            this.TxtContactPerson.MaxLength = 50;
            this.TxtContactPerson.Name = "TxtContactPerson";
            this.TxtContactPerson.Size = new System.Drawing.Size(204, 20);
            this.TxtContactPerson.TabIndex = 2;
            this.TxtContactPerson.TextChanged += new System.EventHandler(this.ButtonStatus);
            // 
            // cboCountry
            // 
            this.cboCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCountry.DropDownHeight = 75;
            this.cboCountry.FormattingEnabled = true;
            this.cboCountry.IntegralHeight = false;
            this.cboCountry.Location = new System.Drawing.Point(94, 197);
            this.cboCountry.Name = "cboCountry";
            this.cboCountry.Size = new System.Drawing.Size(167, 21);
            this.cboCountry.TabIndex = 4;
            this.cboCountry.SelectedIndexChanged += new System.EventHandler(this.cboCountry_SelectedIndexChanged);
            this.cboCountry.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // TxtTelephone
            // 
            this.TxtTelephone.Location = new System.Drawing.Point(387, 98);
            this.TxtTelephone.MaxLength = 12;
            this.TxtTelephone.Name = "TxtTelephone";
            this.TxtTelephone.Size = new System.Drawing.Size(179, 20);
            this.TxtTelephone.TabIndex = 8;
            this.TxtTelephone.TextChanged += new System.EventHandler(this.ButtonStatus);
            // 
            // TxtAddress
            // 
            this.TxtAddress.Location = new System.Drawing.Point(94, 98);
            this.TxtAddress.MaxLength = 300;
            this.TxtAddress.Multiline = true;
            this.TxtAddress.Name = "TxtAddress";
            this.TxtAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TxtAddress.Size = new System.Drawing.Size(204, 86);
            this.TxtAddress.TabIndex = 3;
            this.TxtAddress.TextChanged += new System.EventHandler(this.TxtAddress_TextChanged);
            // 
            // btnCountryoforgin
            // 
            this.btnCountryoforgin.Location = new System.Drawing.Point(268, 195);
            this.btnCountryoforgin.Name = "btnCountryoforgin";
            this.btnCountryoforgin.Size = new System.Drawing.Size(30, 23);
            this.btnCountryoforgin.TabIndex = 5;
            this.btnCountryoforgin.Text = "...";
            this.btnCountryoforgin.UseVisualStyleBackColor = true;
            this.btnCountryoforgin.Click += new System.EventHandler(this.btnCountryoforgin_Click);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(591, 250);
            this.shapeContainer1.TabIndex = 172;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.Color.Silver;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 140;
            this.lineShape1.X2 = 585;
            this.lineShape1.Y1 = 12;
            this.lineShape1.Y2 = 12;
            // 
            // TbNearestCompetitor
            // 
            this.TbNearestCompetitor.Controls.Add(this.panel5);
            this.TbNearestCompetitor.Controls.Add(this.shapeContainer6);
            this.TbNearestCompetitor.Location = new System.Drawing.Point(4, 22);
            this.TbNearestCompetitor.Name = "TbNearestCompetitor";
            this.TbNearestCompetitor.Padding = new System.Windows.Forms.Padding(3);
            this.TbNearestCompetitor.Size = new System.Drawing.Size(597, 256);
            this.TbNearestCompetitor.TabIndex = 3;
            this.TbNearestCompetitor.Text = "Nearest Competitor Details";
            this.TbNearestCompetitor.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(lblNearestComp);
            this.panel5.Controls.Add(this.dgvNearestCompetitor);
            this.panel5.Controls.Add(this.shapeContainer7);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(591, 250);
            this.panel5.TabIndex = 1;
            // 
            // dgvNearestCompetitor
            // 
            this.dgvNearestCompetitor.AllowUserToResizeColumns = false;
            this.dgvNearestCompetitor.AllowUserToResizeRows = false;
            this.dgvNearestCompetitor.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvNearestCompetitor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvNearestCompetitor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNearestCompetitor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColOtherSuppliers,
            this.ColRating});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvNearestCompetitor.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvNearestCompetitor.Location = new System.Drawing.Point(3, 22);
            this.dgvNearestCompetitor.Name = "dgvNearestCompetitor";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvNearestCompetitor.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvNearestCompetitor.RowHeadersWidth = 50;
            this.dgvNearestCompetitor.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvNearestCompetitor.Size = new System.Drawing.Size(585, 225);
            this.dgvNearestCompetitor.TabIndex = 0;
            this.dgvNearestCompetitor.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvNearestCompetitor_CellBeginEdit);
            this.dgvNearestCompetitor.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvNearestCompetitor_UserDeletedRow);
            this.dgvNearestCompetitor.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvNearestCompetitor_RowsAdded);
            this.dgvNearestCompetitor.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvNearestCompetitor_CellEndEdit);
            this.dgvNearestCompetitor.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvNearestCompetitor_EditingControlShowing);
            this.dgvNearestCompetitor.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvNearestCompetitor_CurrentCellDirtyStateChanged);
            this.dgvNearestCompetitor.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvNearestCompetitor_DataError);
            this.dgvNearestCompetitor.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvNearestCompetitor_RowsRemoved);
            // 
            // ColOtherSuppliers
            // 
            this.ColOtherSuppliers.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ColOtherSuppliers.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColOtherSuppliers.FillWeight = 111.9289F;
            this.ColOtherSuppliers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ColOtherSuppliers.HeaderText = "Other Suppliers";
            this.ColOtherSuppliers.Name = "ColOtherSuppliers";
            this.ColOtherSuppliers.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // ColRating
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColRating.DefaultCellStyle = dataGridViewCellStyle3;
            this.ColRating.FillWeight = 111.9289F;
            this.ColRating.HeaderText = "Rating";
            this.ColRating.Name = "ColRating";
            this.ColRating.ReadOnly = true;
            this.ColRating.Width = 96;
            // 
            // shapeContainer7
            // 
            this.shapeContainer7.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer7.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer7.Name = "shapeContainer7";
            this.shapeContainer7.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape9});
            this.shapeContainer7.Size = new System.Drawing.Size(591, 250);
            this.shapeContainer7.TabIndex = 229;
            this.shapeContainer7.TabStop = false;
            // 
            // lineShape9
            // 
            this.lineShape9.BorderColor = System.Drawing.Color.Silver;
            this.lineShape9.Name = "lineShape9";
            this.lineShape9.X1 = 150;
            this.lineShape9.X2 = 585;
            this.lineShape9.Y1 = 11;
            this.lineShape9.Y2 = 11;
            // 
            // shapeContainer6
            // 
            this.shapeContainer6.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer6.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer6.Name = "shapeContainer6";
            this.shapeContainer6.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape8});
            this.shapeContainer6.Size = new System.Drawing.Size(591, 250);
            this.shapeContainer6.TabIndex = 2;
            this.shapeContainer6.TabStop = false;
            // 
            // lineShape8
            // 
            this.lineShape8.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape8.BorderWidth = 2;
            this.lineShape8.Name = "lineShape8";
            this.lineShape8.X1 = 164;
            this.lineShape8.X2 = 494;
            this.lineShape8.Y1 = 11;
            this.lineShape8.Y2 = 138;
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(8, 341);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 0;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(536, 341);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 2;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(455, 341);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 1;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // VendorInformationBindingNavigator
            // 
            this.VendorInformationBindingNavigator.AddNewItem = null;
            this.VendorInformationBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.VendorInformationBindingNavigator.DeleteItem = null;
            this.VendorInformationBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorDeleteItem,
            this.VendorInformationBindingNavigatorSaveItem,
            this.CancelToolStripButton,
            this.toolStripSeparator2,
            this.btnActions,
            this.BtnVendorMapping,
            this.toolStripSeparator3,
            this.BtnPrint,
            this.EmailStripButton,
            this.BtnHelp});
            this.VendorInformationBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.VendorInformationBindingNavigator.MoveFirstItem = null;
            this.VendorInformationBindingNavigator.MoveLastItem = null;
            this.VendorInformationBindingNavigator.MoveNextItem = null;
            this.VendorInformationBindingNavigator.MovePreviousItem = null;
            this.VendorInformationBindingNavigator.Name = "VendorInformationBindingNavigator";
            this.VendorInformationBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.VendorInformationBindingNavigator.Size = new System.Drawing.Size(620, 25);
            this.VendorInformationBindingNavigator.TabIndex = 3;
            this.VendorInformationBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // VendorInformationBindingNavigatorSaveItem
            // 
            this.VendorInformationBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.VendorInformationBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("VendorInformationBindingNavigatorSaveItem.Image")));
            this.VendorInformationBindingNavigatorSaveItem.Name = "VendorInformationBindingNavigatorSaveItem";
            this.VendorInformationBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.VendorInformationBindingNavigatorSaveItem.Text = "Save";
            this.VendorInformationBindingNavigatorSaveItem.Click += new System.EventHandler(this.VendorInformationBindingNavigatorSaveItem_Click);
            // 
            // CancelToolStripButton
            // 
            this.CancelToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CancelToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelToolStripButton.Image")));
            this.CancelToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CancelToolStripButton.Name = "CancelToolStripButton";
            this.CancelToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.CancelToolStripButton.ToolTipText = "Clear";
            this.CancelToolStripButton.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnActions
            // 
            this.btnActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDocuments,
            this.historyToolStripMenuItem});
            this.btnActions.Image = ((System.Drawing.Image)(resources.GetObject("btnActions.Image")));
            this.btnActions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnActions.Name = "btnActions";
            this.btnActions.Size = new System.Drawing.Size(76, 22);
            this.btnActions.Text = "Actions";
            // 
            // btnDocuments
            // 
            this.btnDocuments.Image = global::MyBooksERP.Properties.Resources.document_register;
            this.btnDocuments.Name = "btnDocuments";
            this.btnDocuments.Size = new System.Drawing.Size(135, 22);
            this.btnDocuments.Text = "Documents";
            this.btnDocuments.Click += new System.EventHandler(this.btnDocuments_Click);
            // 
            // historyToolStripMenuItem
            // 
            this.historyToolStripMenuItem.Name = "historyToolStripMenuItem";
            this.historyToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.historyToolStripMenuItem.Text = "History";
            this.historyToolStripMenuItem.Visible = false;
            this.historyToolStripMenuItem.Click += new System.EventHandler(this.historyToolStripMenuItem_Click);
            // 
            // BtnVendorMapping
            // 
            this.BtnVendorMapping.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnVendorMapping.Image = global::MyBooksERP.Properties.Resources.Vendor;
            this.BtnVendorMapping.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnVendorMapping.Name = "BtnVendorMapping";
            this.BtnVendorMapping.Size = new System.Drawing.Size(23, 22);
            this.BtnVendorMapping.Text = "VendorMapping";
            this.BtnVendorMapping.Click += new System.EventHandler(this.BtnVendorMapping_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.ToolTipText = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // EmailStripButton
            // 
            this.EmailStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.EmailStripButton.Image = ((System.Drawing.Image)(resources.GetObject("EmailStripButton.Image")));
            this.EmailStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.EmailStripButton.Name = "EmailStripButton";
            this.EmailStripButton.Size = new System.Drawing.Size(23, 22);
            this.EmailStripButton.Text = "Email";
            this.EmailStripButton.Click += new System.EventHandler(this.EmailStripButton_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // ErrProVendor
            // 
            this.ErrProVendor.ContainerControl = this;
            this.ErrProVendor.RightToLeft = true;
            // 
            // TmVendor
            // 
            this.TmVendor.Tick += new System.EventHandler(this.TmVendor_Tick);
            // 
            // SStripVendor
            // 
            this.SStripVendor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblVendorStatus});
            this.SStripVendor.Location = new System.Drawing.Point(0, 370);
            this.SStripVendor.Name = "SStripVendor";
            this.SStripVendor.Size = new System.Drawing.Size(620, 22);
            this.SStripVendor.TabIndex = 5;
            this.SStripVendor.Text = "statusStrip1";
            // 
            // LblVendorStatus
            // 
            this.LblVendorStatus.Name = "LblVendorStatus";
            this.LblVendorStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // CMSAssociates
            // 
            this.CMSAssociates.Name = "ContextMenuStrip1";
            this.CMSAssociates.Size = new System.Drawing.Size(61, 4);
            // 
            // BnSearch
            // 
            this.BnSearch.AddNewItem = null;
            this.BnSearch.BackColor = System.Drawing.Color.Transparent;
            this.BnSearch.CountItem = null;
            this.BnSearch.DeleteItem = null;
            this.BnSearch.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.TxtSearchText,
            this.toolStripLabel2,
            this.CboSearchOption,
            this.BtnSearch});
            this.BnSearch.Location = new System.Drawing.Point(0, 25);
            this.BnSearch.MoveFirstItem = null;
            this.BnSearch.MoveLastItem = null;
            this.BnSearch.MoveNextItem = null;
            this.BnSearch.MovePreviousItem = null;
            this.BnSearch.Name = "BnSearch";
            this.BnSearch.PositionItem = null;
            this.BnSearch.Size = new System.Drawing.Size(620, 25);
            this.BnSearch.TabIndex = 4;
            this.BnSearch.Text = "bindingNavigator1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(42, 22);
            this.toolStripLabel1.Text = "Search";
            // 
            // TxtSearchText
            // 
            this.TxtSearchText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtSearchText.MaxLength = 25;
            this.TxtSearchText.Name = "TxtSearchText";
            this.TxtSearchText.Size = new System.Drawing.Size(150, 25);
            this.TxtSearchText.ToolTipText = "Enter the search text";
            this.TxtSearchText.TextChanged += new System.EventHandler(this.ClearSearchCount);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(17, 22);
            this.toolStripLabel2.Text = "In";
            // 
            // CboSearchOption
            // 
            this.CboSearchOption.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSearchOption.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSearchOption.DropDownHeight = 75;
            this.CboSearchOption.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.CboSearchOption.IntegralHeight = false;
            this.CboSearchOption.Items.AddRange(new object[] {
            "Mobile No",
            "Name",
            "Nationality",
            "Telephone No",
            "Transaction Type"});
            this.CboSearchOption.Margin = new System.Windows.Forms.Padding(0, 1, 1, 0);
            this.CboSearchOption.Name = "CboSearchOption";
            this.CboSearchOption.Size = new System.Drawing.Size(140, 24);
            this.CboSearchOption.Sorted = true;
            this.CboSearchOption.ToolTipText = "Select a search option";
            this.CboSearchOption.TextChanged += new System.EventHandler(this.ClearSearchCount);
            this.CboSearchOption.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboSearchOption_KeyPress);
            // 
            // BtnSearch
            // 
            this.BtnSearch.Image = global::MyBooksERP.Properties.Resources.Search;
            this.BtnSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnSearch.Name = "BtnSearch";
            this.BtnSearch.Size = new System.Drawing.Size(62, 22);
            this.BtnSearch.Text = "Search";
            this.BtnSearch.ToolTipText = "Search";
            this.BtnSearch.Click += new System.EventHandler(this.BtnSearch_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.comboBox1);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(label4);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.shapeContainer3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(524, 310);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Window;
            this.textBox1.Location = new System.Drawing.Point(424, 9);
            this.textBox1.MaxLength = 20;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(10, 20);
            this.textBox1.TabIndex = 6;
            this.textBox1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(331, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 207;
            this.label2.Text = "Account Number";
            this.label2.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.BackColor = System.Drawing.SystemColors.Info;
            this.comboBox1.DropDownHeight = 75;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.IntegralHeight = false;
            this.comboBox1.Location = new System.Drawing.Point(308, 9);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(17, 21);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(222, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Associate Type";
            this.label3.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.comboBox2);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.textBox2);
            this.panel2.Controls.Add(this.textBox3);
            this.panel2.Controls.Add(this.textBox4);
            this.panel2.Controls.Add(this.textBox5);
            this.panel2.Controls.Add(this.textBox6);
            this.panel2.Controls.Add(label12);
            this.panel2.Controls.Add(this.textBox7);
            this.panel2.Controls.Add(label13);
            this.panel2.Controls.Add(this.textBox8);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.textBox9);
            this.panel2.Controls.Add(label15);
            this.panel2.Controls.Add(this.textBox10);
            this.panel2.Controls.Add(this.textBox11);
            this.panel2.Controls.Add(label16);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.comboBox3);
            this.panel2.Controls.Add(this.comboBox4);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.comboBox5);
            this.panel2.Location = new System.Drawing.Point(6, 43);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(515, 259);
            this.panel2.TabIndex = 1;
            // 
            // comboBox2
            // 
            this.comboBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox2.BackColor = System.Drawing.SystemColors.Info;
            this.comboBox2.DropDownHeight = 75;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.IntegralHeight = false;
            this.comboBox2.Location = new System.Drawing.Point(247, 119);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(77, 21);
            this.comboBox2.TabIndex = 218;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(203, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 217;
            this.label5.Text = "Branch";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 177);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 216;
            this.label6.Text = "Account Head";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(325, 175);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(32, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(370, 234);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 215;
            this.label7.Text = "VAT No";
            this.label7.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(370, 208);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 214;
            this.label8.Tag = "";
            this.label8.Text = "PAN No";
            this.label8.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(370, 180);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 213;
            this.label9.Text = "TIN No";
            this.label9.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(370, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 13);
            this.label10.TabIndex = 212;
            this.label10.Text = "CST No";
            this.label10.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(370, 122);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 9;
            this.label11.Tag = "";
            this.label11.Text = "Serv.Tax No";
            this.label11.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(333, 118);
            this.button2.Name = "button2";
            this.button2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.button2.Size = new System.Drawing.Size(32, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Window;
            this.textBox2.Location = new System.Drawing.Point(438, 229);
            this.textBox2.MaxLength = 30;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(79, 20);
            this.textBox2.TabIndex = 15;
            this.textBox2.Visible = false;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Window;
            this.textBox3.Location = new System.Drawing.Point(438, 202);
            this.textBox3.MaxLength = 30;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(79, 20);
            this.textBox3.TabIndex = 14;
            this.textBox3.Visible = false;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.Window;
            this.textBox4.Location = new System.Drawing.Point(438, 174);
            this.textBox4.MaxLength = 30;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(79, 20);
            this.textBox4.TabIndex = 13;
            this.textBox4.Visible = false;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.Window;
            this.textBox5.Location = new System.Drawing.Point(438, 146);
            this.textBox5.MaxLength = 30;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(79, 20);
            this.textBox5.TabIndex = 12;
            this.textBox5.Visible = false;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.Window;
            this.textBox6.Location = new System.Drawing.Point(438, 119);
            this.textBox6.MaxLength = 30;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(74, 20);
            this.textBox6.TabIndex = 11;
            this.textBox6.Visible = false;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(120, 202);
            this.textBox7.MaxLength = 50;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(237, 20);
            this.textBox7.TabIndex = 9;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(120, 227);
            this.textBox8.MaxLength = 50;
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(237, 20);
            this.textBox8.TabIndex = 10;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(22, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 13);
            this.label14.TabIndex = 188;
            this.label14.Text = "Code";
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.SystemColors.Info;
            this.textBox9.Location = new System.Drawing.Point(120, 12);
            this.textBox9.MaxLength = 30;
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(204, 20);
            this.textBox9.TabIndex = 0;
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.SystemColors.Info;
            this.textBox10.Location = new System.Drawing.Point(120, 38);
            this.textBox10.MaxLength = 50;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(295, 20);
            this.textBox10.TabIndex = 1;
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.Color.White;
            this.textBox11.Location = new System.Drawing.Point(120, 65);
            this.textBox11.MaxLength = 20;
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(204, 20);
            this.textBox11.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(22, 95);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(90, 13);
            this.label17.TabIndex = 205;
            this.label17.Text = "Transaction Type";
            // 
            // comboBox3
            // 
            this.comboBox3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox3.BackColor = System.Drawing.SystemColors.Info;
            this.comboBox3.DropDownHeight = 75;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.IntegralHeight = false;
            this.comboBox3.Location = new System.Drawing.Point(120, 92);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(144, 21);
            this.comboBox3.TabIndex = 3;
            // 
            // comboBox4
            // 
            this.comboBox4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox4.BackColor = System.Drawing.SystemColors.Info;
            this.comboBox4.DropDownHeight = 75;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.IntegralHeight = false;
            this.comboBox4.Location = new System.Drawing.Point(120, 120);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(77, 21);
            this.comboBox4.TabIndex = 4;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(22, 122);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 13);
            this.label18.TabIndex = 206;
            this.label18.Text = "Bank Name";
            // 
            // comboBox5
            // 
            this.comboBox5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox5.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox5.BackColor = System.Drawing.Color.White;
            this.comboBox5.DropDownHeight = 75;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.IntegralHeight = false;
            this.comboBox5.Location = new System.Drawing.Point(120, 175);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(199, 21);
            this.comboBox5.TabIndex = 7;
            // 
            // shapeContainer3
            // 
            this.shapeContainer3.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer3.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer3.Name = "shapeContainer3";
            this.shapeContainer3.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape4,
            this.lineShape5});
            this.shapeContainer3.Size = new System.Drawing.Size(518, 304);
            this.shapeContainer3.TabIndex = 4;
            this.shapeContainer3.TabStop = false;
            // 
            // lineShape4
            // 
            this.lineShape4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape4.BorderWidth = 2;
            this.lineShape4.Name = "lineShape3";
            this.lineShape4.X1 = 123;
            this.lineShape4.X2 = 581;
            this.lineShape4.Y1 = 22;
            this.lineShape4.Y2 = 22;
            // 
            // lineShape5
            // 
            this.lineShape5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape5.BorderWidth = 2;
            this.lineShape5.Name = "lineShape2";
            this.lineShape5.X1 = 96;
            this.lineShape5.X2 = 507;
            this.lineShape5.Y1 = 171;
            this.lineShape5.Y2 = 171;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Controls.Add(label32);
            this.tabPage2.Controls.Add(this.shapeContainer4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(524, 310);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Contact Information";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.toolStrip2);
            this.panel3.Controls.Add(this.textBox12);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.button4);
            this.panel3.Controls.Add(this.button5);
            this.panel3.Controls.Add(label20);
            this.panel3.Controls.Add(this.decimalTextBox1);
            this.panel3.Controls.Add(this.comboBox6);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.textBox13);
            this.panel3.Controls.Add(this.textBox14);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.textBox15);
            this.panel3.Controls.Add(label24);
            this.panel3.Controls.Add(this.textBox16);
            this.panel3.Controls.Add(label26);
            this.panel3.Controls.Add(this.textBox17);
            this.panel3.Controls.Add(label27);
            this.panel3.Controls.Add(label28);
            this.panel3.Controls.Add(this.comboBox7);
            this.panel3.Controls.Add(label29);
            this.panel3.Controls.Add(this.textBox18);
            this.panel3.Controls.Add(this.textBox19);
            this.panel3.Controls.Add(label30);
            this.panel3.Controls.Add(label31);
            this.panel3.Controls.Add(this.button6);
            this.panel3.Controls.Add(this.textBox20);
            this.panel3.Location = new System.Drawing.Point(9, 25);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(504, 282);
            this.panel3.TabIndex = 171;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button3.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.button3.Location = new System.Drawing.Point(389, 31);
            this.button3.Name = "button3";
            this.button3.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.button3.Size = new System.Drawing.Size(20, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "4";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(502, 25);
            this.toolStrip2.TabIndex = 178;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Save";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.RightToLeftAutoMirrorImage = true;
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Delete";
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.SystemColors.Info;
            this.textBox12.Location = new System.Drawing.Point(92, 33);
            this.textBox12.MaxLength = 50;
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(196, 20);
            this.textBox12.TabIndex = 0;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(13, 178);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 13);
            this.label19.TabIndex = 194;
            this.label19.Text = "LocationType";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(294, 31);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(95, 23);
            this.button4.TabIndex = 0;
            this.button4.Text = "Permanent";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(220, 174);
            this.button5.Name = "button5";
            this.button5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.button5.Size = new System.Drawing.Size(32, 23);
            this.button5.TabIndex = 6;
            this.button5.Text = "...";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // decimalTextBox1
            // 
            this.decimalTextBox1.BackColor = System.Drawing.SystemColors.Info;
            this.decimalTextBox1.Location = new System.Drawing.Point(90, 228);
            this.decimalTextBox1.MaxLength = 10;
            this.decimalTextBox1.Name = "decimalTextBox1";
            this.decimalTextBox1.ShortcutsEnabled = false;
            this.decimalTextBox1.Size = new System.Drawing.Size(127, 20);
            this.decimalTextBox1.TabIndex = 8;
            // 
            // comboBox6
            // 
            this.comboBox6.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox6.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox6.BackColor = System.Drawing.SystemColors.Info;
            this.comboBox6.DropDownHeight = 75;
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.IntegralHeight = false;
            this.comboBox6.Location = new System.Drawing.Point(91, 175);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(124, 21);
            this.comboBox6.TabIndex = 5;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(261, 205);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(87, 13);
            this.label21.TabIndex = 185;
            this.label21.Text = "Other Telephone";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(350, 228);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(140, 20);
            this.textBox13.TabIndex = 13;
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(350, 202);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(140, 20);
            this.textBox14.TabIndex = 12;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(261, 231);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(87, 13);
            this.label22.TabIndex = 180;
            this.label22.Text = "Other Mobile No.";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(14, 231);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(58, 13);
            this.label23.TabIndex = 179;
            this.label23.Text = "Mobile No.";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(350, 175);
            this.textBox15.MaxLength = 25;
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(140, 20);
            this.textBox15.TabIndex = 11;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(350, 148);
            this.textBox16.MaxLength = 50;
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(140, 20);
            this.textBox16.TabIndex = 10;
            // 
            // textBox17
            // 
            this.textBox17.BackColor = System.Drawing.SystemColors.Window;
            this.textBox17.Location = new System.Drawing.Point(92, 58);
            this.textBox17.MaxLength = 50;
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(196, 20);
            this.textBox17.TabIndex = 1;
            // 
            // comboBox7
            // 
            this.comboBox7.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox7.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox7.BackColor = System.Drawing.SystemColors.Info;
            this.comboBox7.DropDownHeight = 75;
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.IntegralHeight = false;
            this.comboBox7.Location = new System.Drawing.Point(91, 148);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(124, 21);
            this.comboBox7.TabIndex = 3;
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(90, 202);
            this.textBox18.MaxLength = 50;
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(127, 20);
            this.textBox18.TabIndex = 7;
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.SystemColors.Info;
            this.textBox19.Location = new System.Drawing.Point(91, 84);
            this.textBox19.MaxLength = 300;
            this.textBox19.Multiline = true;
            this.textBox19.Name = "textBox19";
            this.textBox19.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox19.Size = new System.Drawing.Size(316, 55);
            this.textBox19.TabIndex = 2;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(220, 148);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(32, 23);
            this.button6.TabIndex = 4;
            this.button6.Text = "...";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(90, 254);
            this.textBox20.MaxLength = 20;
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(125, 20);
            this.textBox20.TabIndex = 9;
            // 
            // shapeContainer4
            // 
            this.shapeContainer4.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer4.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer4.Name = "shapeContainer4";
            this.shapeContainer4.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape6});
            this.shapeContainer4.Size = new System.Drawing.Size(518, 304);
            this.shapeContainer4.TabIndex = 172;
            this.shapeContainer4.TabStop = false;
            // 
            // lineShape6
            // 
            this.lineShape6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape6.BorderWidth = 2;
            this.lineShape6.Name = "lineShape1";
            this.lineShape6.X1 = 162;
            this.lineShape6.X2 = 509;
            this.lineShape6.Y1 = 14;
            this.lineShape6.Y2 = 13;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn1.FillWeight = 111.9289F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Rating";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 96;
            // 
            // FrmVendor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 392);
            this.Controls.Add(this.BnSearch);
            this.Controls.Add(this.SStripVendor);
            this.Controls.Add(this.VendorInformationBindingNavigator);
            this.Controls.Add(this.pnlVendorInfo);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BtnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmVendor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Associates";
            this.Load += new System.EventHandler(this.FrmVendor_Load);
            this.Shown += new System.EventHandler(this.FrmVendor_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmVendor_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmVendor_KeyDown);
            this.pnlVendorInfo.ResumeLayout(false);
            this.TbAssociates.ResumeLayout(false);
            this.TbGeneral.ResumeLayout(false);
            this.TbGeneral.PerformLayout();
            this.pnlAssociates.ResumeLayout(false);
            this.pnlAssociates.PerformLayout();
            this.TbContact.ResumeLayout(false);
            this.TbContact.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.TbNearestCompetitor.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNearestCompetitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VendorInformationBindingNavigator)).EndInit();
            this.VendorInformationBindingNavigator.ResumeLayout(false);
            this.VendorInformationBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrProVendor)).EndInit();
            this.SStripVendor.ResumeLayout(false);
            this.SStripVendor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BnSearch)).EndInit();
            this.BnSearch.ResumeLayout(false);
            this.BnSearch.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlVendorInfo;
        private System.Windows.Forms.Label LblVendorType;
        internal System.Windows.Forms.Button btnCountryoforgin;
        internal System.Windows.Forms.TextBox txtFax;
        internal System.Windows.Forms.TextBox TxtTelephone;
        internal System.Windows.Forms.TextBox TxtAddress;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnOk;
        internal System.Windows.Forms.BindingNavigator VendorInformationBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton VendorInformationBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton CancelToolStripButton;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton EmailStripButton;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        private System.Windows.Forms.ErrorProvider ErrProVendor;
        private System.Windows.Forms.Timer TmVendor;
        private System.Windows.Forms.StatusStrip SStripVendor;
        private System.Windows.Forms.ToolStripStatusLabel LblVendorStatus;
        private System.Windows.Forms.TabControl TbAssociates;
        private System.Windows.Forms.TabPage TbGeneral;
        private System.Windows.Forms.TabPage TbContact;
        internal System.Windows.Forms.Button btnContextmenu;
        private System.Windows.Forms.Panel panel1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        internal System.Windows.Forms.ContextMenuStrip CMSAssociates;
        internal System.Windows.Forms.TextBox TxtContactPerson;
        internal System.Windows.Forms.TextBox TxtState;
        internal System.Windows.Forms.TextBox TxtZipCode;
        private System.Windows.Forms.ToolStrip toolStrip1;
        internal System.Windows.Forms.ToolStripButton tlsBtnAddressSave;
        internal System.Windows.Forms.ToolStripButton TlsBtnAddressDelete;
        private System.Windows.Forms.Panel pnlAssociates;
        internal System.Windows.Forms.Button BtnAccountHead;
        internal System.Windows.Forms.Button BtnBank;
        internal System.Windows.Forms.TextBox TxtEmail;
        internal System.Windows.Forms.TextBox TxtWebsite;
        private System.Windows.Forms.Label LblSupplierCode;
        internal System.Windows.Forms.TextBox TxtCode;
        internal System.Windows.Forms.TextBox TxtVendorName;
        internal System.Windows.Forms.ComboBox CboBankName;
        internal System.Windows.Forms.Label LblBankName;
        internal System.Windows.Forms.ComboBox cboAccountHead;
        internal System.Windows.Forms.TextBox TxtAddressName;
        internal System.Windows.Forms.ComboBox cboCountry;
        private System.Windows.Forms.Label LblAccountHead;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripDropDownButton btnActions;
        private System.Windows.Forms.ToolStripMenuItem btnDocuments;
        private System.Windows.Forms.BindingNavigator BnSearch;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox TxtSearchText;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox CboSearchOption;
        private System.Windows.Forms.ToolStripButton BtnSearch;
        internal System.Windows.Forms.TextBox TxtAccountNo;
        internal System.Windows.Forms.Label LblAccountNo;
        internal System.Windows.Forms.ComboBox cboBankBranch;
        internal System.Windows.Forms.Label lblBankBranch;
        private System.Windows.Forms.TabPage tabPage1;
        internal System.Windows.Forms.TextBox textBox1;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.ComboBox comboBox2;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Button button2;
        internal System.Windows.Forms.TextBox textBox2;
        internal System.Windows.Forms.TextBox textBox3;
        internal System.Windows.Forms.TextBox textBox4;
        internal System.Windows.Forms.TextBox textBox5;
        internal System.Windows.Forms.TextBox textBox6;
        internal System.Windows.Forms.TextBox textBox7;
        internal System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label14;
        internal System.Windows.Forms.TextBox textBox9;
        internal System.Windows.Forms.TextBox textBox10;
        internal System.Windows.Forms.TextBox textBox11;
        internal System.Windows.Forms.Label label17;
        internal System.Windows.Forms.ComboBox comboBox3;
        internal System.Windows.Forms.ComboBox comboBox4;
        internal System.Windows.Forms.Label label18;
        internal System.Windows.Forms.ComboBox comboBox5;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel3;
        internal System.Windows.Forms.Button button3;
        private System.Windows.Forms.ToolStrip toolStrip2;
        internal System.Windows.Forms.ToolStripButton toolStripButton1;
        internal System.Windows.Forms.ToolStripButton toolStripButton2;
        internal System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label19;
        internal System.Windows.Forms.Button button4;
        internal System.Windows.Forms.Button button5;
        private DemoClsDataGridview.DecimalTextBox decimalTextBox1;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        internal System.Windows.Forms.TextBox textBox15;
        internal System.Windows.Forms.TextBox textBox16;
        internal System.Windows.Forms.TextBox textBox17;
        internal System.Windows.Forms.ComboBox comboBox7;
        internal System.Windows.Forms.TextBox textBox18;
        internal System.Windows.Forms.TextBox textBox19;
        internal System.Windows.Forms.Button button6;
        internal System.Windows.Forms.TextBox textBox20;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape6;
        private System.Windows.Forms.TextBox txtRatingRemarks;
        private System.Windows.Forms.Label lblRatingRemarks;
        private System.Windows.Forms.TextBox txtRating;
        private System.Windows.Forms.Label lblRating;
        private System.Windows.Forms.Label lblStatus;
        internal System.Windows.Forms.ComboBox CboStatus;
        private System.Windows.Forms.TabPage TbNearestCompetitor;
        private System.Windows.Forms.DataGridView dgvNearestCompetitor;
        internal System.Windows.Forms.Button btnBankName;
        private System.Windows.Forms.Panel panel5;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer7;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape9;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer6;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape8;
        internal System.Windows.Forms.Label lblEmailID;
        internal System.Windows.Forms.Label lblWebsite;
        private System.Windows.Forms.ToolStripMenuItem historyToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DemoClsDataGridview.DecimalTextBox txtAdvanceLimit;
        private System.Windows.Forms.Label lblAdvanceLimit;
        private DemoClsDataGridview.DecimalTextBox txtCreditLimit;
        private System.Windows.Forms.Label lblCreditLimit;
        internal System.Windows.Forms.Button btnAddress;
        private System.Windows.Forms.Label lblSourceMedia;
        internal System.Windows.Forms.Button btnSourceMedia;
        internal System.Windows.Forms.ComboBox cboSourceMedia;
        internal System.Windows.Forms.TextBox txtMobileNo;
        internal System.Windows.Forms.ComboBox cboCurrency;
        internal System.Windows.Forms.Label lblCurrency;
        internal System.Windows.Forms.Button btnCurrencyReference;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColOtherSuppliers;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRating;
        private DemoClsDataGridview.DecimalTextBox txtCreditLimitDays;
        internal System.Windows.Forms.Label lblCreditLimitDays;
        internal System.Windows.Forms.Label lblIBAN;
        private System.Windows.Forms.Label lblSupClassifications;
        internal System.Windows.Forms.ComboBox CboSupClassification;
        private System.Windows.Forms.ToolStripButton BtnVendorMapping;
        internal System.Windows.Forms.Label label33;
        internal System.Windows.Forms.TextBox txtTRN;
    }
}