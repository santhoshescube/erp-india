﻿namespace MyBooksERP
{
    partial class FrmWarehouse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label1;
            System.Windows.Forms.Label lblShortName;
            System.Windows.Forms.Label Address1Label;
            System.Windows.Forms.Label CountryIDLabel;
            System.Windows.Forms.Label ZipCodelabel;
            System.Windows.Forms.Label Citylabel;
            System.Windows.Forms.Label Descriptionlabel;
            System.Windows.Forms.Label phoneNolabel;
            System.Windows.Forms.Label Address2label;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmWarehouse));
            this.Label2 = new System.Windows.Forms.Label();
            this.pnlCreateWarehouse = new System.Windows.Forms.Panel();
            this.TbCreateWarehouse = new System.Windows.Forms.TabControl();
            this.TpGeneral = new System.Windows.Forms.TabPage();
            this.cboInchange = new System.Windows.Forms.ComboBox();
            this.btnInCharge = new System.Windows.Forms.Button();
            this.BtnCountry = new System.Windows.Forms.Button();
            this.chbActive = new System.Windows.Forms.CheckBox();
            this.TxtAddress2 = new System.Windows.Forms.TextBox();
            this.TxtPhoneNo = new System.Windows.Forms.TextBox();
            this.TxtDescription = new System.Windows.Forms.TextBox();
            this.TxtCity = new System.Windows.Forms.TextBox();
            this.TxtZipCode = new System.Windows.Forms.TextBox();
            this.TxtName = new System.Windows.Forms.TextBox();
            this.CboCountry = new System.Windows.Forms.ComboBox();
            this.TxtShortName = new System.Windows.Forms.TextBox();
            this.TxtAddress1 = new System.Windows.Forms.TextBox();
            this.TpCompany = new System.Windows.Forms.TabPage();
            this.clbCompany = new System.Windows.Forms.CheckedListBox();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuStripGenerator = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.TxtValue = new System.Windows.Forms.ToolStripTextBox();
            this.CtmRowAdding = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addBlocksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.BindingNavigatorWareHouse = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BtnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.ErrProWarehouse = new System.Windows.Forms.ErrorProvider(this.components);
            this.TmrWarehouse = new System.Windows.Forms.Timer(this.components);
            this.ContMenuLocationAdd = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.LblWarehouseStatus = new DevComponents.DotNetBar.LabelItem();
            Label1 = new System.Windows.Forms.Label();
            lblShortName = new System.Windows.Forms.Label();
            Address1Label = new System.Windows.Forms.Label();
            CountryIDLabel = new System.Windows.Forms.Label();
            ZipCodelabel = new System.Windows.Forms.Label();
            Citylabel = new System.Windows.Forms.Label();
            Descriptionlabel = new System.Windows.Forms.Label();
            phoneNolabel = new System.Windows.Forms.Label();
            Address2label = new System.Windows.Forms.Label();
            this.pnlCreateWarehouse.SuspendLayout();
            this.TbCreateWarehouse.SuspendLayout();
            this.TpGeneral.SuspendLayout();
            this.TpCompany.SuspendLayout();
            this.contextMenuStripGenerator.SuspendLayout();
            this.CtmRowAdding.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigatorWareHouse)).BeginInit();
            this.BindingNavigatorWareHouse.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrProWarehouse)).BeginInit();
            this.ContMenuLocationAdd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new System.Drawing.Point(16, 13);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(35, 13);
            Label1.TabIndex = 65;
            Label1.Text = "Name";
            // 
            // lblShortName
            // 
            lblShortName.AutoSize = true;
            lblShortName.Location = new System.Drawing.Point(16, 39);
            lblShortName.Name = "lblShortName";
            lblShortName.Size = new System.Drawing.Size(63, 13);
            lblShortName.TabIndex = 49;
            lblShortName.Text = "Short Name";
            lblShortName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Address1Label
            // 
            Address1Label.AutoSize = true;
            Address1Label.Location = new System.Drawing.Point(16, 64);
            Address1Label.Name = "Address1Label";
            Address1Label.Size = new System.Drawing.Size(74, 13);
            Address1Label.TabIndex = 56;
            Address1Label.Text = "Address Line1";
            // 
            // CountryIDLabel
            // 
            CountryIDLabel.AutoSize = true;
            CountryIDLabel.Location = new System.Drawing.Point(16, 215);
            CountryIDLabel.Name = "CountryIDLabel";
            CountryIDLabel.Size = new System.Drawing.Size(46, 13);
            CountryIDLabel.TabIndex = 62;
            CountryIDLabel.Text = "Country ";
            // 
            // ZipCodelabel
            // 
            ZipCodelabel.AutoSize = true;
            ZipCodelabel.Location = new System.Drawing.Point(16, 163);
            ZipCodelabel.Name = "ZipCodelabel";
            ZipCodelabel.Size = new System.Drawing.Size(50, 13);
            ZipCodelabel.TabIndex = 72;
            ZipCodelabel.Text = "Zip Code";
            // 
            // Citylabel
            // 
            Citylabel.AutoSize = true;
            Citylabel.Location = new System.Drawing.Point(16, 189);
            Citylabel.Name = "Citylabel";
            Citylabel.Size = new System.Drawing.Size(24, 13);
            Citylabel.TabIndex = 73;
            Citylabel.Text = "City";
            // 
            // Descriptionlabel
            // 
            Descriptionlabel.AutoSize = true;
            Descriptionlabel.Location = new System.Drawing.Point(16, 241);
            Descriptionlabel.Name = "Descriptionlabel";
            Descriptionlabel.Size = new System.Drawing.Size(60, 13);
            Descriptionlabel.TabIndex = 76;
            Descriptionlabel.Text = "Description";
            // 
            // phoneNolabel
            // 
            phoneNolabel.AutoSize = true;
            phoneNolabel.Location = new System.Drawing.Point(16, 325);
            phoneNolabel.Name = "phoneNolabel";
            phoneNolabel.Size = new System.Drawing.Size(55, 13);
            phoneNolabel.TabIndex = 77;
            phoneNolabel.Text = "Phone No";
            // 
            // Address2label
            // 
            Address2label.AutoSize = true;
            Address2label.Location = new System.Drawing.Point(16, 113);
            Address2label.Name = "Address2label";
            Address2label.Size = new System.Drawing.Size(74, 13);
            Address2label.TabIndex = 80;
            Address2label.Text = "Address Line2";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(16, 298);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(53, 13);
            this.Label2.TabIndex = 66;
            this.Label2.Text = "In Charge";
            // 
            // pnlCreateWarehouse
            // 
            this.pnlCreateWarehouse.Controls.Add(this.TbCreateWarehouse);
            this.pnlCreateWarehouse.Location = new System.Drawing.Point(3, 28);
            this.pnlCreateWarehouse.Name = "pnlCreateWarehouse";
            this.pnlCreateWarehouse.Size = new System.Drawing.Size(467, 378);
            this.pnlCreateWarehouse.TabIndex = 0;
            // 
            // TbCreateWarehouse
            // 
            this.TbCreateWarehouse.Controls.Add(this.TpGeneral);
            this.TbCreateWarehouse.Controls.Add(this.TpCompany);
            this.TbCreateWarehouse.Location = new System.Drawing.Point(3, 0);
            this.TbCreateWarehouse.Name = "TbCreateWarehouse";
            this.TbCreateWarehouse.SelectedIndex = 0;
            this.TbCreateWarehouse.Size = new System.Drawing.Size(461, 376);
            this.TbCreateWarehouse.TabIndex = 0;
            // 
            // TpGeneral
            // 
            this.TpGeneral.Controls.Add(this.cboInchange);
            this.TpGeneral.Controls.Add(this.btnInCharge);
            this.TpGeneral.Controls.Add(this.BtnCountry);
            this.TpGeneral.Controls.Add(this.chbActive);
            this.TpGeneral.Controls.Add(Address2label);
            this.TpGeneral.Controls.Add(this.TxtAddress2);
            this.TpGeneral.Controls.Add(this.TxtPhoneNo);
            this.TpGeneral.Controls.Add(phoneNolabel);
            this.TpGeneral.Controls.Add(Descriptionlabel);
            this.TpGeneral.Controls.Add(this.TxtDescription);
            this.TpGeneral.Controls.Add(this.TxtCity);
            this.TpGeneral.Controls.Add(Citylabel);
            this.TpGeneral.Controls.Add(this.TxtZipCode);
            this.TpGeneral.Controls.Add(ZipCodelabel);
            this.TpGeneral.Controls.Add(this.TxtName);
            this.TpGeneral.Controls.Add(this.Label2);
            this.TpGeneral.Controls.Add(Label1);
            this.TpGeneral.Controls.Add(this.CboCountry);
            this.TpGeneral.Controls.Add(lblShortName);
            this.TpGeneral.Controls.Add(this.TxtShortName);
            this.TpGeneral.Controls.Add(Address1Label);
            this.TpGeneral.Controls.Add(this.TxtAddress1);
            this.TpGeneral.Controls.Add(CountryIDLabel);
            this.TpGeneral.Location = new System.Drawing.Point(4, 22);
            this.TpGeneral.Name = "TpGeneral";
            this.TpGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.TpGeneral.Size = new System.Drawing.Size(453, 350);
            this.TpGeneral.TabIndex = 0;
            this.TpGeneral.Text = "General";
            this.TpGeneral.UseVisualStyleBackColor = true;
            // 
            // cboInchange
            // 
            this.cboInchange.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboInchange.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboInchange.BackColor = System.Drawing.Color.White;
            this.cboInchange.DropDownHeight = 75;
            this.cboInchange.DropDownWidth = 257;
            this.cboInchange.FormattingEnabled = true;
            this.cboInchange.IntegralHeight = false;
            this.cboInchange.Location = new System.Drawing.Point(96, 294);
            this.cboInchange.Name = "cboInchange";
            this.cboInchange.Size = new System.Drawing.Size(298, 21);
            this.cboInchange.TabIndex = 11;
            this.cboInchange.SelectionChangeCommitted += new System.EventHandler(this.ChangeStatus);
            this.cboInchange.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // btnInCharge
            // 
            this.btnInCharge.Location = new System.Drawing.Point(400, 292);
            this.btnInCharge.Name = "btnInCharge";
            this.btnInCharge.Size = new System.Drawing.Size(35, 24);
            this.btnInCharge.TabIndex = 12;
            this.btnInCharge.Text = "...";
            this.btnInCharge.UseVisualStyleBackColor = true;
            this.btnInCharge.Click += new System.EventHandler(this.btnInCharge_Click);
            // 
            // BtnCountry
            // 
            this.BtnCountry.Location = new System.Drawing.Point(400, 209);
            this.BtnCountry.Name = "BtnCountry";
            this.BtnCountry.Size = new System.Drawing.Size(35, 24);
            this.BtnCountry.TabIndex = 9;
            this.BtnCountry.Text = "...";
            this.BtnCountry.UseVisualStyleBackColor = true;
            this.BtnCountry.Click += new System.EventHandler(this.BtnCountry_Click);
            // 
            // chbActive
            // 
            this.chbActive.AutoSize = true;
            this.chbActive.Location = new System.Drawing.Point(380, 323);
            this.chbActive.Name = "chbActive";
            this.chbActive.Size = new System.Drawing.Size(56, 17);
            this.chbActive.TabIndex = 14;
            this.chbActive.Text = "Active";
            this.chbActive.UseVisualStyleBackColor = true;
            this.chbActive.CheckStateChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // TxtAddress2
            // 
            this.TxtAddress2.Location = new System.Drawing.Point(96, 110);
            this.TxtAddress2.MaxLength = 50;
            this.TxtAddress2.Multiline = true;
            this.TxtAddress2.Name = "TxtAddress2";
            this.TxtAddress2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TxtAddress2.Size = new System.Drawing.Size(337, 43);
            this.TxtAddress2.TabIndex = 5;
            this.TxtAddress2.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // TxtPhoneNo
            // 
            this.TxtPhoneNo.BackColor = System.Drawing.Color.White;
            this.TxtPhoneNo.Location = new System.Drawing.Point(96, 321);
            this.TxtPhoneNo.MaxLength = 20;
            this.TxtPhoneNo.Name = "TxtPhoneNo";
            this.TxtPhoneNo.Size = new System.Drawing.Size(164, 20);
            this.TxtPhoneNo.TabIndex = 13;
            this.TxtPhoneNo.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // TxtDescription
            // 
            this.TxtDescription.Location = new System.Drawing.Point(96, 238);
            this.TxtDescription.MaxLength = 100;
            this.TxtDescription.Multiline = true;
            this.TxtDescription.Name = "TxtDescription";
            this.TxtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TxtDescription.Size = new System.Drawing.Size(340, 50);
            this.TxtDescription.TabIndex = 10;
            this.TxtDescription.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // TxtCity
            // 
            this.TxtCity.Location = new System.Drawing.Point(96, 185);
            this.TxtCity.MaxLength = 40;
            this.TxtCity.Name = "TxtCity";
            this.TxtCity.Size = new System.Drawing.Size(340, 20);
            this.TxtCity.TabIndex = 7;
            this.TxtCity.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // TxtZipCode
            // 
            this.TxtZipCode.BackColor = System.Drawing.SystemColors.Window;
            this.TxtZipCode.Location = new System.Drawing.Point(96, 159);
            this.TxtZipCode.MaxLength = 20;
            this.TxtZipCode.Name = "TxtZipCode";
            this.TxtZipCode.Size = new System.Drawing.Size(164, 20);
            this.TxtZipCode.TabIndex = 6;
            this.TxtZipCode.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // TxtName
            // 
            this.TxtName.BackColor = System.Drawing.SystemColors.Info;
            this.TxtName.Location = new System.Drawing.Point(96, 9);
            this.TxtName.MaxLength = 50;
            this.TxtName.Name = "TxtName";
            this.TxtName.Size = new System.Drawing.Size(337, 20);
            this.TxtName.TabIndex = 0;
            this.TxtName.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // CboCountry
            // 
            this.CboCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCountry.BackColor = System.Drawing.Color.White;
            this.CboCountry.DropDownHeight = 75;
            this.CboCountry.DropDownWidth = 257;
            this.CboCountry.FormattingEnabled = true;
            this.CboCountry.IntegralHeight = false;
            this.CboCountry.Location = new System.Drawing.Point(96, 211);
            this.CboCountry.Name = "CboCountry";
            this.CboCountry.Size = new System.Drawing.Size(298, 21);
            this.CboCountry.TabIndex = 8;
            this.CboCountry.SelectionChangeCommitted += new System.EventHandler(this.ChangeStatus);
            this.CboCountry.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // TxtShortName
            // 
            this.TxtShortName.BackColor = System.Drawing.Color.White;
            this.TxtShortName.Location = new System.Drawing.Point(96, 35);
            this.TxtShortName.MaxLength = 20;
            this.TxtShortName.Name = "TxtShortName";
            this.TxtShortName.Size = new System.Drawing.Size(164, 20);
            this.TxtShortName.TabIndex = 1;
            this.TxtShortName.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // TxtAddress1
            // 
            this.TxtAddress1.BackColor = System.Drawing.SystemColors.Window;
            this.TxtAddress1.Location = new System.Drawing.Point(96, 61);
            this.TxtAddress1.MaxLength = 50;
            this.TxtAddress1.Multiline = true;
            this.TxtAddress1.Name = "TxtAddress1";
            this.TxtAddress1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TxtAddress1.Size = new System.Drawing.Size(337, 43);
            this.TxtAddress1.TabIndex = 4;
            this.TxtAddress1.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // TpCompany
            // 
            this.TpCompany.Controls.Add(this.clbCompany);
            this.TpCompany.Location = new System.Drawing.Point(4, 22);
            this.TpCompany.Name = "TpCompany";
            this.TpCompany.Padding = new System.Windows.Forms.Padding(3);
            this.TpCompany.Size = new System.Drawing.Size(453, 350);
            this.TpCompany.TabIndex = 1;
            this.TpCompany.Text = "Applicable Company(s)";
            this.TpCompany.UseVisualStyleBackColor = true;
            // 
            // clbCompany
            // 
            this.clbCompany.CheckOnClick = true;
            this.clbCompany.FormattingEnabled = true;
            this.clbCompany.Location = new System.Drawing.Point(84, 68);
            this.clbCompany.Name = "clbCompany";
            this.clbCompany.Size = new System.Drawing.Size(285, 214);
            this.clbCompany.TabIndex = 0;
            this.clbCompany.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "blocks.png");
            this.imageList2.Images.SetKeyName(1, "locations.png");
            this.imageList2.Images.SetKeyName(2, "lots.png");
            this.imageList2.Images.SetKeyName(3, "raws.png");
            this.imageList2.Images.SetKeyName(4, "Warehouse.PNG");
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "ExpandAll.png");
            this.imageList1.Images.SetKeyName(1, "Go.png");
            this.imageList1.Images.SetKeyName(2, "Warehouse.PNG");
            this.imageList1.Images.SetKeyName(3, "Delete.png");
            // 
            // contextMenuStripGenerator
            // 
            this.contextMenuStripGenerator.BackColor = System.Drawing.Color.White;
            this.contextMenuStripGenerator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripSeparator4,
            this.TxtValue});
            this.contextMenuStripGenerator.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Table;
            this.contextMenuStripGenerator.Name = "contextMenuStripGenerator";
            this.contextMenuStripGenerator.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.contextMenuStripGenerator.Size = new System.Drawing.Size(161, 57);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = global::MyBooksERP.Properties.Resources.Warehouse;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(160, 22);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.BackColor = System.Drawing.Color.Lime;
            this.toolStripSeparator4.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(157, 6);
            // 
            // TxtValue
            // 
            this.TxtValue.BackColor = System.Drawing.SystemColors.Info;
            this.TxtValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtValue.MaxLength = 3;
            this.TxtValue.Name = "TxtValue";
            this.TxtValue.Size = new System.Drawing.Size(100, 23);
            this.TxtValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtValue_KeyPress);
            // 
            // CtmRowAdding
            // 
            this.CtmRowAdding.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addBlocksToolStripMenuItem});
            this.CtmRowAdding.Name = "CtmRowAdding";
            this.CtmRowAdding.Size = new System.Drawing.Size(162, 26);
            this.CtmRowAdding.Text = "Add Rows";
            // 
            // addBlocksToolStripMenuItem
            // 
            this.addBlocksToolStripMenuItem.Name = "addBlocksToolStripMenuItem";
            this.addBlocksToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.addBlocksToolStripMenuItem.Text = "Add/ Edit Blocks";
            // 
            // BtnSave
            // 
            this.BtnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSave.Location = new System.Drawing.Point(12, 412);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 0;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnCancel.Location = new System.Drawing.Point(385, 412);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 2;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnOk.Enabled = false;
            this.BtnOk.Location = new System.Drawing.Point(304, 412);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 1;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BindingNavigatorWareHouse
            // 
            this.BindingNavigatorWareHouse.AddNewItem = null;
            this.BindingNavigatorWareHouse.BackColor = System.Drawing.Color.Transparent;
            this.BindingNavigatorWareHouse.CountItem = this.BindingNavigatorCountItem;
            this.BindingNavigatorWareHouse.DeleteItem = null;
            this.BindingNavigatorWareHouse.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BtnClear,
            this.ToolStripSeparator1,
            this.BtnPrint,
            this.BtnEmail,
            this.ToolStripSeparator2,
            this.BtnHelp});
            this.BindingNavigatorWareHouse.Location = new System.Drawing.Point(0, 0);
            this.BindingNavigatorWareHouse.MoveFirstItem = null;
            this.BindingNavigatorWareHouse.MoveLastItem = null;
            this.BindingNavigatorWareHouse.MoveNextItem = null;
            this.BindingNavigatorWareHouse.MovePreviousItem = null;
            this.BindingNavigatorWareHouse.Name = "BindingNavigatorWareHouse";
            this.BindingNavigatorWareHouse.PositionItem = this.BindingNavigatorPositionItem;
            this.BindingNavigatorWareHouse.Size = new System.Drawing.Size(472, 25);
            this.BindingNavigatorWareHouse.TabIndex = 13;
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BtnClear
            // 
            this.BtnClear.Image = ((System.Drawing.Image)(resources.GetObject("BtnClear.Image")));
            this.BtnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnClear.Name = "BtnClear";
            this.BtnClear.Size = new System.Drawing.Size(23, 22);
            this.BtnClear.ToolTipText = "Clear";
            this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print/Fax";
            this.BtnPrint.Visible = false;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Visible = false;
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            this.ToolStripSeparator2.Visible = false;
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // ErrProWarehouse
            // 
            this.ErrProWarehouse.ContainerControl = this;
            this.ErrProWarehouse.RightToLeft = true;
            // 
            // TmrWarehouse
            // 
            this.TmrWarehouse.Tick += new System.EventHandler(this.TmrWarehouse_Tick);
            // 
            // ContMenuLocationAdd
            // 
            this.ContMenuLocationAdd.BackColor = System.Drawing.Color.White;
            this.ContMenuLocationAdd.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripSeparator5,
            this.toolStripMenuItem3});
            this.ContMenuLocationAdd.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Table;
            this.ContMenuLocationAdd.Name = "contextMenuStripGenerator";
            this.ContMenuLocationAdd.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ContMenuLocationAdd.Size = new System.Drawing.Size(108, 54);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = global::MyBooksERP.Properties.Resources.Warehouse;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(107, 22);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.BackColor = System.Drawing.Color.Lime;
            this.toolStripSeparator5.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(104, 6);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.toolStripMenuItem3.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(107, 22);
            this.toolStripMenuItem3.Text = "Delete";
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.LblWarehouseStatus});
            this.bar1.Location = new System.Drawing.Point(0, 441);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(472, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 20;
            this.bar1.TabStop = false;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "Status:";
            // 
            // LblWarehouseStatus
            // 
            this.LblWarehouseStatus.Name = "LblWarehouseStatus";
            // 
            // FrmWarehouse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 460);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.BindingNavigatorWareHouse);
            this.Controls.Add(this.pnlCreateWarehouse);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.BtnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmWarehouse";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Warehouse";
            this.Load += new System.EventHandler(this.FrmWarehouse_Load);
            this.Shown += new System.EventHandler(this.FrmWarehouse_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmWarehouse_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmWarehouse_KeyDown);
            this.pnlCreateWarehouse.ResumeLayout(false);
            this.TbCreateWarehouse.ResumeLayout(false);
            this.TpGeneral.ResumeLayout(false);
            this.TpGeneral.PerformLayout();
            this.TpCompany.ResumeLayout(false);
            this.contextMenuStripGenerator.ResumeLayout(false);
            this.contextMenuStripGenerator.PerformLayout();
            this.CtmRowAdding.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigatorWareHouse)).EndInit();
            this.BindingNavigatorWareHouse.ResumeLayout(false);
            this.BindingNavigatorWareHouse.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrProWarehouse)).EndInit();
            this.ContMenuLocationAdd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlCreateWarehouse;
        private System.Windows.Forms.TabControl TbCreateWarehouse;
        private System.Windows.Forms.TabPage TpGeneral;
        private System.Windows.Forms.TabPage TpCompany;
        internal System.Windows.Forms.ComboBox CboCountry;
        internal System.Windows.Forms.TextBox TxtShortName;
        internal System.Windows.Forms.TextBox TxtAddress1;
        internal System.Windows.Forms.TextBox TxtName;
        internal System.Windows.Forms.TextBox TxtZipCode;
        internal System.Windows.Forms.TextBox TxtDescription;
        internal System.Windows.Forms.TextBox TxtCity;
        internal System.Windows.Forms.TextBox TxtPhoneNo;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Button BtnOk;
        internal System.Windows.Forms.BindingNavigator BindingNavigatorWareHouse;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BtnClear;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.TextBox TxtAddress2;
        private System.Windows.Forms.ErrorProvider ErrProWarehouse;
        private System.Windows.Forms.Timer TmrWarehouse;
        private System.Windows.Forms.Label Label2;
        private System.Windows.Forms.CheckBox chbActive;
        private System.Windows.Forms.ContextMenuStrip CtmRowAdding;
        private System.Windows.Forms.ToolStripMenuItem addBlocksToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList1;
        internal System.Windows.Forms.Button BtnCountry;
        internal System.Windows.Forms.Button btnInCharge;
        internal System.Windows.Forms.ComboBox cboInchange;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripGenerator;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripTextBox TxtValue;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip ContMenuLocationAdd;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.LabelItem LblWarehouseStatus;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.CheckedListBox clbCompany;
    }
}