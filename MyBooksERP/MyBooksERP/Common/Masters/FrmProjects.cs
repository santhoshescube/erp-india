﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



/* 
================================================
   Author:		<Author,,Alvin>
   Create date: <Create Date,,14 Nov 2011>
   Description:	<Description,,Project Form>
================================================
*/


namespace MyBooksERP
{

    public partial class FrmProjects : Form
    {
        ClsLogWriter MObjClsLogWriter;
        clsBLLProjects MobjclsBLLProjects;
        ClsNotificationNew MobjClsNotification;
        DataTable datMessages;
        MessageBoxIcon MmsgMessageIcon; 
        
        bool MblnShowErrorMess = false;
        string MstrCommonMessage;
        private bool MblnAddStatus; //To Specify Add/Update mode 
        int MintTotalRecCnt;
        int MintCurrentRecCnt;
        int intProjectID;          //For Keep ProjectID for Updation /Deletion 

        private bool MblnViewPermission = false;    //To Set View Permission
        private bool MblnAddPermission = false;     //To Set Add Permission
        private bool MblnUpdatePermission = false;  //To Set Update Permission
        private bool MblnDeletePermission = false;  //To Set Delete Permission
        private bool MblnAddUpdatePermission = false;
        private bool MblnPrintEmailPermission = false;
        bool blnIsEditMode = false;
        
        public FrmProjects()
        {
            InitializeComponent();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjclsBLLProjects = new clsBLLProjects();
            MobjClsNotification = new ClsNotificationNew();
        }

        private bool LoadCombos(int intType)  //0 For Loading All Combobox
        {
            try
            {
                DataTable datCombos = new DataTable();

                if (intType == 0 || intType == 2)
                {
                    CboProjectManager.DataSource = null;
                    datCombos = null;
                    datCombos = MobjclsBLLProjects.FillCombos(new string[] { "EmployeeID,FirstName", "EmployeeMaster", "" });
                    CboProjectManager.ValueMember = "EmployeeID";
                    CboProjectManager.DisplayMember = "FirstName";
                    CboProjectManager.DataSource = datCombos;
                }
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private void SetPermissions()  // Function for setting permissions
        {
            try
            {
                clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
                if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.General, (Int32)eMenuID.Projects, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                }
                else
                    MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

                //BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                //BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                //ProjectsBindingNavigator.Enabled = MblnAddUpdatePermission;
                //btnSave.Enabled = MblnAddUpdatePermission;
                //btnBnSave.Enabled = MblnAddUpdatePermission;
                //btnOK.Enabled = MblnUpdatePermission;
                //MblnPrintEmailPermission = MblnViewPermission;
                //btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission; 
            }
                
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SetPermissions() " + ex.Message);
                MObjClsLogWriter.WriteLog("Error in SetPermissions() " + ex.Message, 2);
            }
        }

        private void ChangeStatus()
        {
            if (!blnIsEditMode)
            {
                //ProjectsBindingNavigator.Enabled = MblnAddUpdatePermission;
                btnSave.Enabled = MblnAddPermission;
                btnBnSave.Enabled = MblnAddPermission;
                btnOK.Enabled = MblnAddPermission;
            }
            else
            {
                btnSave.Enabled = MblnUpdatePermission;
                btnBnSave.Enabled = MblnUpdatePermission;
                btnOK.Enabled = MblnUpdatePermission;
            }
        }

        private void DisableButtons()
        {
            //ProjectsBindingNavigator.Enabled = false;
            BindingNavigatorAddNewItem.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = false;
            btnBnSave.Enabled = false;
            btnSave.Enabled = false; ;
            btnOK.Enabled = false;
            btnPrint.Enabled = btnEmail.Enabled = false; 
        }

        private void SetButtons()   //For Navigation purpose Button Status
        {
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
            btnEmail.Enabled = MblnPrintEmailPermission;
            btnBnSave.Enabled = false;
            btnSave.Enabled = false; ;
            btnOK.Enabled = false;
        }

        private void FrmProjects_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadCombos(0);
            LoadMessage();
            AddNewItem();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = intProjectID;
                ObjViewer.PiFormID = (int)FormID.Projects;
                ObjViewer.ShowDialog();
            }
            catch (Exception Ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnPrint_Click " + Ex.Message.ToString());
            }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                //using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                //{
                //    ObjEmailPopUp.MsSubject = "Projects";
                //    ObjEmailPopUp.EmailFormType = EmailFormID.Projects;
                //    ObjEmailPopUp.ShowDialog();
                //}

                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Projects";
                    ObjEmailPopUp.EmailFormType = EmailFormID.Projects;
                    //ObjEmailPopUp.EmailSource = MobjClsBLLCompanyInformation.GetCompanyReport();
                    ObjEmailPopUp.EmailSource = MobjclsBLLProjects.GetEmailInfo();
                    ObjEmailPopUp.ShowDialog();
                }



            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnEmail_Click" + ex.Message.ToString());
            }

        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewItem();
        }

        private string GetPrefix()
        {
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(1);
            return ClsCommonSettings.strProjectCodePrefix;
        }

        private bool AddNewItem()
        {
            try
            {
                MblnAddStatus = true;
                ResetForm();
                GenerateProjectCode();
                clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
                DataTable dt = objClsBLLPermissionSettings.GetPermittedCompany(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.ProjectCompany);
                
                    if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                        MintTotalRecCnt = MobjclsBLLProjects.RecCountNavigate();

                    else if (Convert.ToBoolean(dt.Rows[0]["IsVisible"])==true)
                        MintTotalRecCnt = MobjclsBLLProjects.RecCountNavigate1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.ProjectCompany);

                    else
                    {
                        ProjectsBindingNavigator.Enabled = false;
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 9014, out MmsgMessageIcon);
                        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        tmProjects.Enabled = true;
                        return false;
                    }
                
                if (MintTotalRecCnt > 0)
                {
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintTotalRecCnt + 1) + "";
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintTotalRecCnt + 1);
                    MintCurrentRecCnt = MintTotalRecCnt + 1;
                }
                else
                {
                    BindingNavigatorCountItem.Text = "of 1";
                    BindingNavigatorPositionItem.Text = "1";
                    MintCurrentRecCnt = 1;
                }

                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = true;
                BindingNavigatorMoveFirstItem.Enabled = true;
                errProjects.Clear();
                DisableButtons();
                blnIsEditMode = false;
                lblStatus.Text = "Add New Information";
                tmProjects.Enabled = true;

            }
            catch(Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form AddNewItem:AddNewItem " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewItem() " + Ex.Message.ToString());

                return false;

            }
            return true;
        }
        private void GenerateProjectCode()  //Fn for Generate Project Code on Company Basis
        {
            string ProjectCode;
            ProjectCode = GetPrefix() + MobjclsBLLProjects.GenerateCode(1);
            txtProjectCode.Text = ProjectCode;
        }

        private void CboCompanyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerateProjectCode();
            ChangeStatus();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            AddNewItem();
        }

        private void ResetForm()  
        {
            try
            {
                LoadCombos(0);
                txtProjectCode.Clear();
                txtDescription.Clear();
                txtRemarks.Clear();
                txtEstimatedAmount.Clear();
                dtpStartDate.Value = DateTime.Today;
                dtpEndDate.Value = DateTime.Today;

                GenerateProjectCode();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on ResetForm() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on ResetForm() " + Ex.Message.ToString());
            }
        }

        private bool SaveProjects()
        {
            try
            {
                if (ValidateFields())
                {
                    if (MblnAddStatus == true)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1, out MmsgMessageIcon);
                    }
                    else
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 3, out MmsgMessageIcon);
                    }
                    if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    {
                        return false;
                    }
                    FillParameters();
                    if (MblnAddStatus == true)
                    {
                        MobjclsBLLProjects.AddProjects();
                        return true;
                    }
                    else
                    {
                        //if (UpdateValidation())   
                        //{
                            MobjclsBLLProjects.UpdateProjectsInfo();
                            return true;
                        //}
                    }

                }
                
                return false;

            }
                catch(Exception Ex)
                 {
                     MObjClsLogWriter.WriteLog("Error on SaveProjects() " + this.Name + " " + Ex.Message.ToString(), 2);
                     if (MblnShowErrorMess)
                         MessageBox.Show("Error on SaveProjects() " + Ex.Message.ToString());
                     return false;
                 }
            }
        
        

        public bool ValidateFields()  //For Save & Upadte Validation
        {
            try
            {
                bool blnValid = true;
                Control cntrlFocus = null;
                errProjects.Clear();

                if (string.IsNullOrEmpty(txtProjectCode.Text.Trim()))
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 7291, out MmsgMessageIcon);
                    cntrlFocus = txtProjectCode;
                    blnValid = false;
                }
                if (string.IsNullOrEmpty(txtDescription.Text.Trim()))
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 7292, out MmsgMessageIcon);
                    cntrlFocus = txtDescription;
                    blnValid = false;

                }
                if (CboProjectManager.SelectedValue == null)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 7293, out MmsgMessageIcon);
                    cntrlFocus = CboProjectManager;
                    blnValid = false;
                }

                if (!blnValid)
                {
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    errProjects.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    cntrlFocus.Focus();
                }

                return blnValid;
            }
            catch(Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on ValidateFields() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on ValidateFields() " + Ex.Message.ToString());
                return false;
            }
        }

        private void LoadMessage()  //Loads Error messages
        {
            datMessages = new DataTable();
            datMessages = MobjClsNotification.FillMessageArray((int)FormID.Projects, 4);
        }

        private void FillParameters()
        {
            MobjclsBLLProjects.clsDTOProjects.strProjectCode = txtProjectCode.Text.Trim();
            MobjclsBLLProjects.clsDTOProjects.strDscription = txtDescription.Text.Trim();
            MobjclsBLLProjects.clsDTOProjects.strStartDate = dtpStartDate.Value.ToString("dd-MMM-yyyy");
            MobjclsBLLProjects.clsDTOProjects.strEndDate = dtpEndDate.Value.ToString("dd-MMM-yyyy");
            MobjclsBLLProjects.clsDTOProjects.intProjectInchargeID = Convert.ToInt32(CboProjectManager.SelectedValue);
            MobjclsBLLProjects.clsDTOProjects.strRemarks = txtRemarks.Text.Trim();
            MobjclsBLLProjects.clsDTOProjects.decEstimatedAmount = txtEstimatedAmount.Text.Trim();
        }

        private void DisplayProjectsInfo()
        {
            try
            {
                if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    MobjclsBLLProjects.DisplayProjectsInfo(MintCurrentRecCnt);
                else
                    MobjclsBLLProjects.DisplayProjectsInfo1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.ProjectCompany, MintCurrentRecCnt);  // For Limited users

                LoadCombos(2);   //For Loding Companywise Employee

                intProjectID = MobjclsBLLProjects.clsDTOProjects.intProjectID;

                txtProjectCode.Text = MobjclsBLLProjects.clsDTOProjects.strProjectCode;
                txtDescription.Text = MobjclsBLLProjects.clsDTOProjects.strDscription;
                CboProjectManager.SelectedValue = MobjclsBLLProjects.clsDTOProjects.intProjectInchargeID;
                txtRemarks.Text = MobjclsBLLProjects.clsDTOProjects.strRemarks;
                dtpStartDate.Value = Convert.ToDateTime(MobjclsBLLProjects.clsDTOProjects.strStartDate);
                dtpEndDate.Value = Convert.ToDateTime(MobjclsBLLProjects.clsDTOProjects.strEndDate);
                txtEstimatedAmount.Text = MobjclsBLLProjects.clsDTOProjects.decEstimatedAmount;
            }
            catch(Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on DisplayprojectInfo" + this.Name + " " + Ex.Message.ToString(), 2);

                if (MblnShowErrorMess)
                    MessageBox.Show("Error on DisplayprojectInfo" + Ex.Message.ToString());
            }
        }

        private void DeleteProjectsInfo()
        {
            try
            {
                if (DeleteValidation())
                {
                    if (MobjclsBLLProjects.DeleteProjectsInfo())
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4, out MmsgMessageIcon);
                        AddNewItem();
                    }
                }
            }
            catch(Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on DeleteProjectsInfo()" + this.Name + " " + Ex.Message.ToString(), 2);

                if (MblnShowErrorMess)
                    MessageBox.Show("Error on DeleteProjectsInfo()" + Ex.Message.ToString());
            }
        }

        private bool DeleteValidation()
        {
            try
            {
                //bool Retvalue = false;
                if (MobjclsBLLProjects.GetProjectDetailsExists())
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 7294, out MmsgMessageIcon);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 13, out MmsgMessageIcon);

                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;
                else
                   return true;
            }
            catch(Exception Ex)
            {

                MObjClsLogWriter.WriteLog("Error on DeleteValidation()" + this.Name + " " + Ex.Message.ToString(), 2);

                if (MblnShowErrorMess)
                    MessageBox.Show("Error on DeleteValidation()" + Ex.Message.ToString());

                return false;
            }
        }

        private bool UpdateValidation()
        {
            try
            {
                if (MobjclsBLLProjects.GetProjectDetailsExists())
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 7294, out MmsgMessageIcon);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                    return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on UpdateValidation()" + this.Name + " " + Ex.Message.ToString(), 2);

                if (MblnShowErrorMess)
                    MessageBox.Show("Error on UpdateValidation()" + Ex.Message.ToString());
                return false;
            }
        }

        private void setBindingEnabledisable() //For Binding Navigator 
        {
            if (MintCurrentRecCnt == 1)
            {
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = true;
                BindingNavigatorMoveLastItem.Enabled = true;
            }
            else if (MintCurrentRecCnt == MintTotalRecCnt)
            {
                BindingNavigatorMovePreviousItem.Enabled = true;
                BindingNavigatorMoveFirstItem.Enabled = true;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                BindingNavigatorMovePreviousItem.Enabled = true;
                BindingNavigatorMoveFirstItem.Enabled = true;
                BindingNavigatorMoveNextItem.Enabled = true;
                BindingNavigatorMoveLastItem.Enabled = true;
            }
        }

        private void ProjectsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveProjects())
            {
                AddNewItem();
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                     MintTotalRecCnt = MobjclsBLLProjects.RecCountNavigate();
                else
                    MintTotalRecCnt = MobjclsBLLProjects.RecCountNavigate1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.ProjectCompany);

                if (MintTotalRecCnt > 0)
                {
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintTotalRecCnt) + "";
                    MintCurrentRecCnt = MintCurrentRecCnt - 1;

                    if (MintCurrentRecCnt <= 0)
                    {
                        MintCurrentRecCnt = 1;
                    }
                }
                else
                {
                    BindingNavigatorPositionItem.Text = "of 0";
                    MintTotalRecCnt = 0;
                }

                BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                DisplayProjectsInfo();
                MblnAddStatus = false;
                setBindingEnabledisable();
                errProjects.Clear();
                SetButtons();
                blnIsEditMode = true;
            }
            catch(Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on BindingNavigatorMovePreviousItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (MblnShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorMovePreviousItem_Click" + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    MintTotalRecCnt = MobjclsBLLProjects.RecCountNavigate();
                else
                    MintTotalRecCnt = MobjclsBLLProjects.RecCountNavigate1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.ProjectCompany);

                if (MintTotalRecCnt > 0)
                {
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintTotalRecCnt) + "";
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    MintCurrentRecCnt = 0;
                }
                BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                DisplayProjectsInfo();
                MblnAddStatus = false;
                setBindingEnabledisable();
                errProjects.Clear();
                SetButtons();
                blnIsEditMode = true;
            }
            catch(Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on BindingNavigatorMoveFirstItem_Click  " + this.Name + " " + Ex.Message.ToString(), 2);

                if (MblnShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorMoveFirstItem_Click" + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    MintTotalRecCnt = MobjclsBLLProjects.RecCountNavigate();
                else
                    MintTotalRecCnt = MobjclsBLLProjects.RecCountNavigate1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.ProjectCompany);

                if (MintTotalRecCnt > 0)
                {
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintTotalRecCnt) + "";
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;

                    if (MintCurrentRecCnt >= MintTotalRecCnt)
                    {
                        MintCurrentRecCnt = MintTotalRecCnt;
                    }
                }
                else
                {
                    MintCurrentRecCnt = 0;
                }
                BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                DisplayProjectsInfo();
                MblnAddStatus = false;
                setBindingEnabledisable();
                errProjects.Clear();
                SetButtons();
                blnIsEditMode = true;
            }
            catch(Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on BindingNavigatorMoveNextItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (MblnShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorMoveNextItem_Click" + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    MintTotalRecCnt = MobjclsBLLProjects.RecCountNavigate();
                else
                    MintTotalRecCnt = MobjclsBLLProjects.RecCountNavigate1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.ProjectCompany);

                if (MintTotalRecCnt > 0)
                {
                    BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintTotalRecCnt) + "";
                    MintCurrentRecCnt = MintTotalRecCnt;
                }
                else
                {
                    MintCurrentRecCnt = 0;
                }
                BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                DisplayProjectsInfo();
                MblnAddStatus = false;
                setBindingEnabledisable();
                errProjects.Clear();
                SetButtons();
                blnIsEditMode = true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on BindingNavigatorMoveLastItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (MblnShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorMoveLastItem_Click" + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            DeleteProjectsInfo();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveProjects())
            {
                AddNewItem();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (SaveProjects())
            {
                this.Close();
            }
        }

        private void txtDescription_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void CboProjectManager_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtEstimatedAmount_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtRemarks_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dtpStartDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dtpEndDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void tmProjects_Tick(object sender, EventArgs e)
        {
            lblStatus.Text="";
            tmProjects.Enabled = false;
        }
    }
}
