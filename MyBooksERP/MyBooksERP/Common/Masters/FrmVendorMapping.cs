﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmVendorMapping : Form
    {
        clsBLLVendorMapping MobjclsBLLVendorMapping;
        ClsNotificationNew MobjClsNotification;

        bool MblnPrintEmailPermission = false;
        bool MblnAddPermission = false;
        bool MblnUpdatePermission = false;
        bool MblnDeletePermission = false;

        string MstrMessageCommon;
        MessageBoxIcon MmessageIcon;
        DataTable MdatMessages;

        public int MintVendorType;
        public int PintVendorID = 0;

        public FrmVendorMapping()
        {
            InitializeComponent();
            MobjclsBLLVendorMapping = new clsBLLVendorMapping();
            MobjClsNotification = new ClsNotificationNew();
        }

        private void FrmVendorMapping_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadMessage();
            SetAutoCompleteList();
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 2)
            {
                if (MintVendorType == (int)VendorType.Supplier)
                {
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory,
                        (int)eMenuID.Supplier, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                }
                else
                {
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory,
                        (int)eMenuID.Customer, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                }
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void LoadMessage()
        {
            MdatMessages = new DataTable();
            MdatMessages = MobjClsNotification.FillMessageArray((int)FormID.VendorInformation, ClsCommonSettings.ProductID);
        }

        private void SetAutoCompleteList()
        {
            try
            {
                int intType = CboSearchOption.SelectedIndex.ToInt32() + 1;
                this.TxtSearchText.AutoCompleteSource = AutoCompleteSource.CustomSource;
                DataTable dt = MobjclsBLLVendorMapping.SetAutoCompleteList(intType, TxtSearchText.Text.Trim(), MintVendorType);
                this.TxtSearchText.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);
            }
            catch { }
        }

        private void CboSearchOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            TxtSearchText.Text = "";
            SetAutoCompleteList();
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            if (FormValidation())
            {
                int intType = CboSearchOption.SelectedIndex.ToInt32() + 1;
                DataTable datTemp = MobjclsBLLVendorMapping.getVendorDetails(intType, TxtSearchText.Text.Trim(), MintVendorType);
                dgvVendor.Rows.Clear();

                for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                {
                    dgvVendor.Rows.Add();
                    dgvVendor.Rows[iCounter].Cells["VendorID"].Value = datTemp.Rows[iCounter]["VendorID"].ToInt32();
                    dgvVendor.Rows[iCounter].Cells["VendorName"].Value = datTemp.Rows[iCounter]["VendorName"].ToString();

                    int intCompID = datTemp.Rows[iCounter]["CompanyID"].ToInt32();
                    dgvVendor.Rows[iCounter].Cells["CompanyID"].Value = intCompID;

                    DataGridViewTextBoxCell txtCell = new DataGridViewTextBoxCell();
                    txtCell.Value = "";

                    if (intCompID == ClsCommonSettings.CompanyID)
                    {
                        dgvVendor["Associate", iCounter] = txtCell;
                        dgvVendor.Rows[iCounter].Cells["Associate"].ReadOnly = true;
                    }
                    else
                    {
                        dgvVendor["Remove", iCounter] = txtCell;
                        dgvVendor.Rows[iCounter].Cells["Remove"].ReadOnly = true;
                    }

                    dgvVendor.Rows[iCounter].Cells["CompanyName"].Value = datTemp.Rows[iCounter]["CompanyName"].ToString();
                    dgvVendor.Rows[iCounter].Cells["MobileNo"].Value = datTemp.Rows[iCounter]["MobileNo"].ToString();
                }
            }
        }

        private bool FormValidation()
        {
            if (CboSearchOption.SelectedIndex == -1 || CboSearchOption.Text.Trim().Length == 0)
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 324, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmVendor.Enabled = true;
                CboSearchOption.Focus();
                return false;
            }

            if (TxtSearchText.Text.Trim().Length == 0)
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 325, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmVendor.Enabled = true;
                TxtSearchText.Focus();
                return false;
            }

            return true;
        }

        private void dgvVendor_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvVendor.Columns["Associate"].Index)
                {
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1, out MmessageIcon);

                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;

                    MobjclsBLLVendorMapping.PobjclsDTOVendorInformation.intVendorID = dgvVendor.Rows[e.RowIndex].Cells["VendorID"].Value.ToInt32();
                    MobjclsBLLVendorMapping.PobjclsDTOVendorInformation.intCompanyID = ClsCommonSettings.CompanyID;
                    MobjclsBLLVendorMapping.SaveVendorInfo();

                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 2, out MmessageIcon);
                    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmVendor.Enabled = true;

                    PintVendorID = MobjclsBLLVendorMapping.PobjclsDTOVendorInformation.intVendorID;
                    this.Close();
                }
                else if (e.ColumnIndex == dgvVendor.Columns["Remove"].Index)
                {
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 13, out MmessageIcon);

                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;

                    MobjclsBLLVendorMapping.PobjclsDTOVendorInformation.intVendorID = dgvVendor.Rows[e.RowIndex].Cells["VendorID"].Value.ToInt32();
                    MobjclsBLLVendorMapping.PobjclsDTOVendorInformation.intCompanyID = dgvVendor.Rows[e.RowIndex].Cells["CompanyID"].Value.ToInt32();
                    MobjclsBLLVendorMapping.DeleteVendorInfo();

                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 4, out MmessageIcon);
                    LblVendorStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmVendor.Enabled = true;

                    PintVendorID = 0;
                    this.Close();
                }
            }
        }
    }
}