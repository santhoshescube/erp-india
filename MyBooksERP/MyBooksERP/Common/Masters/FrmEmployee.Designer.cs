﻿namespace MyBooksERP
{
    partial class FrmEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label OfficialEmailLabel;
            System.Windows.Forms.Label DesignationIDLabel;
            System.Windows.Forms.Label DepartmentIDLabel;
            System.Windows.Forms.Label Label8;
            System.Windows.Forms.Label WorkingAtLabel;
            System.Windows.Forms.Label WorkStatusIDLabel;
            System.Windows.Forms.Label DateofJoiningLabel;
            System.Windows.Forms.Label EmploymentTypeLabel;
            System.Windows.Forms.Label CountryofOriginIDLabel;
            System.Windows.Forms.Label Label13;
            System.Windows.Forms.Label DateofBirthLabel;
            System.Windows.Forms.Label ReligionIDLabel;
            System.Windows.Forms.Label NationalityIDLabel;
            System.Windows.Forms.Label Label25;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label EmailLabel;
            System.Windows.Forms.Label LocalAddressLabel;
            System.Windows.Forms.Label LocalPhoneLabel;
            System.Windows.Forms.Label label17;
            System.Windows.Forms.Label label18;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label20;
            System.Windows.Forms.Label LocalMobileLabel;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label TransactionTypeIDLabel;
            System.Windows.Forms.Label Label16;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label9;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmployee));
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.BnEmployee = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BnSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatornPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BnSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BnSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoreActions = new System.Windows.Forms.ToolStripDropDownButton();
            this.mnuItemAttachDocuments = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.BnSearch = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.TxtSearchText = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.CboSearchOption = new System.Windows.Forms.ToolStripComboBox();
            this.BtnSearch = new System.Windows.Forms.ToolStripButton();
            this.ErrEmployee = new System.Windows.Forms.ErrorProvider(this.components);
            this.dtpDateofBirth = new System.Windows.Forms.DateTimePicker();
            this.TmEmployee = new System.Windows.Forms.Timer(this.components);
            this.TxtEmployeeNumber = new System.Windows.Forms.TextBox();
            this.LblEmployeeNumber = new System.Windows.Forms.Label();
            this.LblSalutation = new System.Windows.Forms.Label();
            this.TxtFirstName = new System.Windows.Forms.TextBox();
            this.LblFirstName = new System.Windows.Forms.Label();
            this.TxtMiddleName = new System.Windows.Forms.TextBox();
            this.LblMiddleName = new System.Windows.Forms.Label();
            this.TxtLastName = new System.Windows.Forms.TextBox();
            this.LblLastName = new System.Windows.Forms.Label();
            this.CboSalutation = new System.Windows.Forms.ComboBox();
            this.PnlEmployee = new System.Windows.Forms.Panel();
            this.BtnSalutation = new System.Windows.Forms.Button();
            this.TbEmployee = new System.Windows.Forms.TabControl();
            this.TpGeneral = new System.Windows.Forms.TabPage();
            this.BtnAttach = new System.Windows.Forms.Button();
            this.BtnContxtMenu1 = new System.Windows.Forms.Button();
            this.ContextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuAttach = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuScan = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.PassportPhotoPictureBox = new System.Windows.Forms.PictureBox();
            this.cboReligion = new System.Windows.Forms.ComboBox();
            this.BtnReligion = new System.Windows.Forms.Button();
            this.rdbMale = new System.Windows.Forms.RadioButton();
            this.cboNationality = new System.Windows.Forms.ComboBox();
            this.cboCountryoforgin = new System.Windows.Forms.ComboBox();
            this.rdbFemale = new System.Windows.Forms.RadioButton();
            this.BtnNationality = new System.Windows.Forms.Button();
            this.BtnCountryoforigin = new System.Windows.Forms.Button();
            this.cboWorkStatus = new System.Windows.Forms.ComboBox();
            this.DtpDateofJoining = new System.Windows.Forms.DateTimePicker();
            this.cboEmploymentType = new System.Windows.Forms.ComboBox();
            this.BtnEmptype = new System.Windows.Forms.Button();
            this.TxtOfficialEmail = new System.Windows.Forms.TextBox();
            this.cboDepartment = new System.Windows.Forms.ComboBox();
            this.BtnDesignation = new System.Windows.Forms.Button();
            this.BtnDepartment = new System.Windows.Forms.Button();
            this.cboDesignation = new System.Windows.Forms.ComboBox();
            this.cboWorkingAt = new System.Windows.Forms.ComboBox();
            this.BtnWorkstatus = new System.Windows.Forms.Button();
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.TbContacts = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPermanentAddress = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtMobile = new System.Windows.Forms.TextBox();
            this.txtLocalPhone = new System.Windows.Forms.TextBox();
            this.txtLocalAddress = new System.Windows.Forms.TextBox();
            this.txtEmergencyPhone = new System.Windows.Forms.TextBox();
            this.txtEmergencyAddress = new System.Windows.Forms.TextBox();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.ShapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.tpPayroll = new System.Windows.Forms.TabPage();
            this.cboVisafrom = new System.Windows.Forms.ComboBox();
            this.btnSettlementPolicy = new System.Windows.Forms.Button();
            this.cboSettlementPolicy = new System.Windows.Forms.ComboBox();
            this.txtPersonID = new System.Windows.Forms.TextBox();
            this.Label11 = new System.Windows.Forms.Label();
            this.btnVacationPolicy = new System.Windows.Forms.Button();
            this.cboVacationPolicy = new System.Windows.Forms.ComboBox();
            this.CboEmployeeBankname = new System.Windows.Forms.ComboBox();
            this.Label14 = new System.Windows.Forms.Label();
            this.TransactionTypeReferenceComboBox = new System.Windows.Forms.ComboBox();
            this.AccountNumberTextBox = new System.Windows.Forms.TextBox();
            this.ComBankAccIdComboBox = new System.Windows.Forms.ComboBox();
            this.LblBankAccount = new System.Windows.Forms.Label();
            this.AccountNumberLabel = new System.Windows.Forms.Label();
            this.BankNameReferenceComboBox = new System.Windows.Forms.ComboBox();
            this.LblBankName = new System.Windows.Forms.Label();
            this.btnLeavePolicy = new System.Windows.Forms.Button();
            this.cboLeavePolicy = new System.Windows.Forms.ComboBox();
            this.btnWorkPolicy = new System.Windows.Forms.Button();
            this.cboWorkPolicy = new System.Windows.Forms.ComboBox();
            this.shapeContainer3 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.txtEmployeeFullName = new System.Windows.Forms.TextBox();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.ContextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.LocalContactsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OverseasContactsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EmergencyContactsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TipEmployee = new System.Windows.Forms.ToolTip(this.components);
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.LblEmployeeStatus = new System.Windows.Forms.ToolStripStatusLabel();
            OfficialEmailLabel = new System.Windows.Forms.Label();
            DesignationIDLabel = new System.Windows.Forms.Label();
            DepartmentIDLabel = new System.Windows.Forms.Label();
            Label8 = new System.Windows.Forms.Label();
            WorkingAtLabel = new System.Windows.Forms.Label();
            WorkStatusIDLabel = new System.Windows.Forms.Label();
            DateofJoiningLabel = new System.Windows.Forms.Label();
            EmploymentTypeLabel = new System.Windows.Forms.Label();
            CountryofOriginIDLabel = new System.Windows.Forms.Label();
            Label13 = new System.Windows.Forms.Label();
            DateofBirthLabel = new System.Windows.Forms.Label();
            ReligionIDLabel = new System.Windows.Forms.Label();
            NationalityIDLabel = new System.Windows.Forms.Label();
            Label25 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            EmailLabel = new System.Windows.Forms.Label();
            LocalAddressLabel = new System.Windows.Forms.Label();
            LocalPhoneLabel = new System.Windows.Forms.Label();
            label17 = new System.Windows.Forms.Label();
            label18 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label20 = new System.Windows.Forms.Label();
            LocalMobileLabel = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            TransactionTypeIDLabel = new System.Windows.Forms.Label();
            Label16 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BnEmployee)).BeginInit();
            this.BnEmployee.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BnSearch)).BeginInit();
            this.BnSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrEmployee)).BeginInit();
            this.PnlEmployee.SuspendLayout();
            this.TbEmployee.SuspendLayout();
            this.TpGeneral.SuspendLayout();
            this.ContextMenuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PassportPhotoPictureBox)).BeginInit();
            this.TbContacts.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tpPayroll.SuspendLayout();
            this.ContextMenuStrip1.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // OfficialEmailLabel
            // 
            OfficialEmailLabel.AutoSize = true;
            OfficialEmailLabel.Location = new System.Drawing.Point(17, 280);
            OfficialEmailLabel.Name = "OfficialEmailLabel";
            OfficialEmailLabel.Size = new System.Drawing.Size(67, 13);
            OfficialEmailLabel.TabIndex = 0;
            OfficialEmailLabel.Text = "Official Email";
            // 
            // DesignationIDLabel
            // 
            DesignationIDLabel.AutoSize = true;
            DesignationIDLabel.Location = new System.Drawing.Point(17, 149);
            DesignationIDLabel.Name = "DesignationIDLabel";
            DesignationIDLabel.Size = new System.Drawing.Size(63, 13);
            DesignationIDLabel.TabIndex = 123;
            DesignationIDLabel.Text = "Designation";
            // 
            // DepartmentIDLabel
            // 
            DepartmentIDLabel.AutoSize = true;
            DepartmentIDLabel.Location = new System.Drawing.Point(17, 122);
            DepartmentIDLabel.Name = "DepartmentIDLabel";
            DepartmentIDLabel.Size = new System.Drawing.Size(62, 13);
            DepartmentIDLabel.TabIndex = 122;
            DepartmentIDLabel.Text = "Department";
            // 
            // Label8
            // 
            Label8.AutoSize = true;
            Label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label8.Location = new System.Drawing.Point(5, 14);
            Label8.Name = "Label8";
            Label8.Size = new System.Drawing.Size(53, 13);
            Label8.TabIndex = 1;
            Label8.Text = "Identity";
            // 
            // WorkingAtLabel
            // 
            WorkingAtLabel.AutoSize = true;
            WorkingAtLabel.Location = new System.Drawing.Point(15, 41);
            WorkingAtLabel.Name = "WorkingAtLabel";
            WorkingAtLabel.Size = new System.Drawing.Size(60, 13);
            WorkingAtLabel.TabIndex = 63;
            WorkingAtLabel.Text = "Working At";
            // 
            // WorkStatusIDLabel
            // 
            WorkStatusIDLabel.AutoSize = true;
            WorkStatusIDLabel.Location = new System.Drawing.Point(17, 95);
            WorkStatusIDLabel.Name = "WorkStatusIDLabel";
            WorkStatusIDLabel.Size = new System.Drawing.Size(66, 13);
            WorkStatusIDLabel.TabIndex = 500;
            WorkStatusIDLabel.Text = "Work Status";
            // 
            // DateofJoiningLabel
            // 
            DateofJoiningLabel.AutoSize = true;
            DateofJoiningLabel.Location = new System.Drawing.Point(422, 280);
            DateofJoiningLabel.Name = "DateofJoiningLabel";
            DateofJoiningLabel.Size = new System.Drawing.Size(78, 13);
            DateofJoiningLabel.TabIndex = 136;
            DateofJoiningLabel.Text = "Date of Joining";
            // 
            // EmploymentTypeLabel
            // 
            EmploymentTypeLabel.AutoSize = true;
            EmploymentTypeLabel.Location = new System.Drawing.Point(15, 68);
            EmploymentTypeLabel.Name = "EmploymentTypeLabel";
            EmploymentTypeLabel.Size = new System.Drawing.Size(91, 13);
            EmploymentTypeLabel.TabIndex = 134;
            EmploymentTypeLabel.Text = "Employment Type";
            // 
            // CountryofOriginIDLabel
            // 
            CountryofOriginIDLabel.AutoSize = true;
            CountryofOriginIDLabel.Location = new System.Drawing.Point(17, 199);
            CountryofOriginIDLabel.Name = "CountryofOriginIDLabel";
            CountryofOriginIDLabel.Size = new System.Drawing.Size(46, 13);
            CountryofOriginIDLabel.TabIndex = 4;
            CountryofOriginIDLabel.Text = "Country ";
            // 
            // Label13
            // 
            Label13.AutoSize = true;
            Label13.Location = new System.Drawing.Point(17, 174);
            Label13.Name = "Label13";
            Label13.Size = new System.Drawing.Size(42, 13);
            Label13.TabIndex = 128;
            Label13.Text = "Gender";
            // 
            // DateofBirthLabel
            // 
            DateofBirthLabel.AutoSize = true;
            DateofBirthLabel.Location = new System.Drawing.Point(422, 253);
            DateofBirthLabel.Name = "DateofBirthLabel";
            DateofBirthLabel.Size = new System.Drawing.Size(66, 13);
            DateofBirthLabel.TabIndex = 127;
            DateofBirthLabel.Text = "Date of Birth";
            // 
            // ReligionIDLabel
            // 
            ReligionIDLabel.AutoSize = true;
            ReligionIDLabel.Location = new System.Drawing.Point(17, 253);
            ReligionIDLabel.Name = "ReligionIDLabel";
            ReligionIDLabel.Size = new System.Drawing.Size(45, 13);
            ReligionIDLabel.TabIndex = 124;
            ReligionIDLabel.Text = "Religion";
            // 
            // NationalityIDLabel
            // 
            NationalityIDLabel.AutoSize = true;
            NationalityIDLabel.Location = new System.Drawing.Point(17, 226);
            NationalityIDLabel.Name = "NationalityIDLabel";
            NationalityIDLabel.Size = new System.Drawing.Size(56, 13);
            NationalityIDLabel.TabIndex = 123;
            NationalityIDLabel.Text = "Nationality";
            // 
            // Label25
            // 
            Label25.AutoSize = true;
            Label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label25.Location = new System.Drawing.Point(3, 7);
            Label25.Name = "Label25";
            Label25.Size = new System.Drawing.Size(160, 13);
            Label25.TabIndex = 39;
            Label25.Text = "Contact && Other Addresses";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.Location = new System.Drawing.Point(395, 16);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(93, 13);
            label4.TabIndex = 100011;
            label4.Text = "Passport Photo";
            // 
            // EmailLabel
            // 
            EmailLabel.AutoSize = true;
            EmailLabel.Location = new System.Drawing.Point(6, 176);
            EmailLabel.Name = "EmailLabel";
            EmailLabel.Size = new System.Drawing.Size(32, 13);
            EmailLabel.TabIndex = 14;
            EmailLabel.Text = "Email";
            // 
            // LocalAddressLabel
            // 
            LocalAddressLabel.AutoSize = true;
            LocalAddressLabel.Location = new System.Drawing.Point(6, 22);
            LocalAddressLabel.Name = "LocalAddressLabel";
            LocalAddressLabel.Size = new System.Drawing.Size(45, 13);
            LocalAddressLabel.TabIndex = 10;
            LocalAddressLabel.Text = "Address";
            // 
            // LocalPhoneLabel
            // 
            LocalPhoneLabel.AutoSize = true;
            LocalPhoneLabel.Location = new System.Drawing.Point(6, 124);
            LocalPhoneLabel.Name = "LocalPhoneLabel";
            LocalPhoneLabel.Size = new System.Drawing.Size(38, 13);
            LocalPhoneLabel.TabIndex = 12;
            LocalPhoneLabel.Text = "Phone";
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Location = new System.Drawing.Point(346, 204);
            label17.Name = "label17";
            label17.Size = new System.Drawing.Size(67, 13);
            label17.TabIndex = 537;
            label17.Text = "Local Phone";
            // 
            // label18
            // 
            label18.AutoSize = true;
            label18.Location = new System.Drawing.Point(346, 146);
            label18.Name = "label18";
            label18.Size = new System.Drawing.Size(74, 13);
            label18.TabIndex = 536;
            label18.Text = "Local Address";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(346, 120);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(94, 13);
            label3.TabIndex = 535;
            label3.Text = "Emergency Phone";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(346, 50);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(101, 13);
            label10.TabIndex = 534;
            label10.Text = "Emergency Address";
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label20.Location = new System.Drawing.Point(3, 232);
            label20.Name = "label20";
            label20.Size = new System.Drawing.Size(65, 13);
            label20.TabIndex = 533;
            label20.Text = "Other Info";
            // 
            // LocalMobileLabel
            // 
            LocalMobileLabel.AutoSize = true;
            LocalMobileLabel.Location = new System.Drawing.Point(6, 150);
            LocalMobileLabel.Name = "LocalMobileLabel";
            LocalMobileLabel.Size = new System.Drawing.Size(38, 13);
            LocalMobileLabel.TabIndex = 528;
            LocalMobileLabel.Text = "Mobile";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(10, 64);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(68, 13);
            label2.TabIndex = 512;
            label2.Text = "Leave Policy";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(11, 38);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(64, 13);
            label1.TabIndex = 511;
            label1.Text = "Work Policy";
            // 
            // TransactionTypeIDLabel
            // 
            TransactionTypeIDLabel.AutoSize = true;
            TransactionTypeIDLabel.Location = new System.Drawing.Point(16, 196);
            TransactionTypeIDLabel.Name = "TransactionTypeIDLabel";
            TransactionTypeIDLabel.Size = new System.Drawing.Size(93, 13);
            TransactionTypeIDLabel.TabIndex = 938;
            TransactionTypeIDLabel.Text = "Transaction Type ";
            // 
            // Label16
            // 
            Label16.AutoSize = true;
            Label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label16.Location = new System.Drawing.Point(6, 160);
            Label16.Name = "Label16";
            Label16.Size = new System.Drawing.Size(84, 13);
            Label16.TabIndex = 941;
            Label16.Text = "Bank Identity";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.Location = new System.Drawing.Point(6, 16);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(49, 13);
            label5.TabIndex = 943;
            label5.Text = "Policies";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(10, 91);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(80, 13);
            label6.TabIndex = 946;
            label6.Text = "Vacation Policy";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(10, 118);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(88, 13);
            label7.TabIndex = 951;
            label7.Text = "Settlement Policy";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(376, 38);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(94, 13);
            label9.TabIndex = 953;
            label9.Text = "Visa obtained from";
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(9, 453);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 0;
            this.BtnSave.Text = "&Save";
            this.TipEmployee.SetToolTip(this.BtnSave, "Click here to save the Executive information");
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(654, 453);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 2;
            this.BtnCancel.Text = "&Cancel";
            this.TipEmployee.SetToolTip(this.BtnCancel, "Click here to close the form");
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(574, 453);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 1;
            this.BtnOk.Text = "&Ok";
            this.TipEmployee.SetToolTip(this.BtnOk, "Click here to save the Executive information and close the form");
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BnEmployee
            // 
            this.BnEmployee.AddNewItem = null;
            this.BnEmployee.BackColor = System.Drawing.Color.Transparent;
            this.BnEmployee.CountItem = this.BindingNavigatorCountItem;
            this.BnEmployee.DeleteItem = null;
            this.BnEmployee.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BnSeparator,
            this.BindingNavigatornPositionItem,
            this.BindingNavigatorCountItem,
            this.BnSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BnSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.bnMoreActions,
            this.BtnClear,
            this.toolStripSeparator2,
            this.BtnPrint,
            this.BtnEmail,
            this.BtnHelp});
            this.BnEmployee.Location = new System.Drawing.Point(0, 0);
            this.BnEmployee.MoveFirstItem = null;
            this.BnEmployee.MoveLastItem = null;
            this.BnEmployee.MoveNextItem = null;
            this.BnEmployee.MovePreviousItem = null;
            this.BnEmployee.Name = "BnEmployee";
            this.BnEmployee.PositionItem = this.BindingNavigatornPositionItem;
            this.BnEmployee.Size = new System.Drawing.Size(739, 25);
            this.BnEmployee.TabIndex = 2;
            this.BnEmployee.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BnSeparator
            // 
            this.BnSeparator.Name = "BnSeparator";
            this.BnSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatornPositionItem
            // 
            this.BindingNavigatornPositionItem.AccessibleName = "Position";
            this.BindingNavigatornPositionItem.AutoSize = false;
            this.BindingNavigatornPositionItem.Name = "BindingNavigatornPositionItem";
            this.BindingNavigatornPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatornPositionItem.Text = "0";
            this.BindingNavigatornPositionItem.ToolTipText = "Current position";
            // 
            // BnSeparator1
            // 
            this.BnSeparator1.Name = "BnSeparator1";
            this.BnSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BnSeparator2
            // 
            this.BnSeparator2.Name = "BnSeparator2";
            this.BnSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.ToolTipText = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.ToolTipText = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // bnMoreActions
            // 
            this.bnMoreActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemAttachDocuments});
            this.bnMoreActions.Image = global::MyBooksERP.Properties.Resources.Add1;
            this.bnMoreActions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnMoreActions.Name = "bnMoreActions";
            this.bnMoreActions.Size = new System.Drawing.Size(76, 22);
            this.bnMoreActions.Text = "Actions";
            this.bnMoreActions.ToolTipText = "Actions";
            // 
            // mnuItemAttachDocuments
            // 
            this.mnuItemAttachDocuments.Image = global::MyBooksERP.Properties.Resources.document_register;
            this.mnuItemAttachDocuments.Name = "mnuItemAttachDocuments";
            this.mnuItemAttachDocuments.Size = new System.Drawing.Size(135, 22);
            this.mnuItemAttachDocuments.Text = "Documents";
            this.mnuItemAttachDocuments.Click += new System.EventHandler(this.mnuItemAttachDocuments_Click);
            // 
            // BtnClear
            // 
            this.BtnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnClear.Image = ((System.Drawing.Image)(resources.GetObject("BtnClear.Image")));
            this.BtnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnClear.Name = "BtnClear";
            this.BtnClear.Size = new System.Drawing.Size(23, 22);
            this.BtnClear.ToolTipText = "Clear";
            this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.ToolTipText = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.ToolTipText = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.ToolTipText = "Help";
            this.BtnHelp.Click += new System.EventHandler(this.BnHelpButton_Click);
            // 
            // BnSearch
            // 
            this.BnSearch.AddNewItem = null;
            this.BnSearch.BackColor = System.Drawing.Color.Transparent;
            this.BnSearch.CountItem = null;
            this.BnSearch.DeleteItem = null;
            this.BnSearch.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.TxtSearchText,
            this.toolStripLabel2,
            this.CboSearchOption,
            this.BtnSearch});
            this.BnSearch.Location = new System.Drawing.Point(0, 25);
            this.BnSearch.MoveFirstItem = null;
            this.BnSearch.MoveLastItem = null;
            this.BnSearch.MoveNextItem = null;
            this.BnSearch.MovePreviousItem = null;
            this.BnSearch.Name = "BnSearch";
            this.BnSearch.PositionItem = null;
            this.BnSearch.Size = new System.Drawing.Size(739, 25);
            this.BnSearch.TabIndex = 1;
            this.BnSearch.Text = "bindingNavigator1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(42, 22);
            this.toolStripLabel1.Text = "Search";
            // 
            // TxtSearchText
            // 
            this.TxtSearchText.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.TxtSearchText.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.TxtSearchText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtSearchText.MaxLength = 25;
            this.TxtSearchText.Name = "TxtSearchText";
            this.TxtSearchText.Size = new System.Drawing.Size(150, 25);
            this.TxtSearchText.ToolTipText = "Enter the search text";
            this.TxtSearchText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtSearchText_KeyDown);
            this.TxtSearchText.TextChanged += new System.EventHandler(this.ClearSearchCount);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(17, 22);
            this.toolStripLabel2.Text = "In";
            // 
            // CboSearchOption
            // 
            this.CboSearchOption.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.CboSearchOption.DropDownHeight = 75;
            this.CboSearchOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboSearchOption.IntegralHeight = false;
            this.CboSearchOption.Items.AddRange(new object[] {
            "Department",
            "Designation",
            "Employee Number",
            "First Name"});
            this.CboSearchOption.Margin = new System.Windows.Forms.Padding(0, 1, 1, 0);
            this.CboSearchOption.Name = "CboSearchOption";
            this.CboSearchOption.Size = new System.Drawing.Size(140, 24);
            this.CboSearchOption.Sorted = true;
            this.CboSearchOption.ToolTipText = "Select a search option";
            this.CboSearchOption.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            this.CboSearchOption.TextChanged += new System.EventHandler(this.ClearSearchCount);
            this.CboSearchOption.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboSearchOption_KeyPress);
            // 
            // BtnSearch
            // 
            this.BtnSearch.Image = global::MyBooksERP.Properties.Resources.Search;
            this.BtnSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnSearch.Name = "BtnSearch";
            this.BtnSearch.Size = new System.Drawing.Size(23, 22);
            this.BtnSearch.ToolTipText = "Search";
            this.BtnSearch.Click += new System.EventHandler(this.BnSearchButton_Click);
            // 
            // ErrEmployee
            // 
            this.ErrEmployee.ContainerControl = this;
            this.ErrEmployee.RightToLeft = true;
            // 
            // dtpDateofBirth
            // 
            this.dtpDateofBirth.CustomFormat = "dd-MMM-yyyy";
            this.dtpDateofBirth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ErrEmployee.SetIconAlignment(this.dtpDateofBirth, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.dtpDateofBirth.Location = new System.Drawing.Point(506, 250);
            this.dtpDateofBirth.Name = "dtpDateofBirth";
            this.dtpDateofBirth.Size = new System.Drawing.Size(101, 20);
            this.dtpDateofBirth.TabIndex = 17;
            this.dtpDateofBirth.ValueChanged += new System.EventHandler(this.changestatus);
            // 
            // TmEmployee
            // 
            this.TmEmployee.Interval = 1000;
            this.TmEmployee.Tick += new System.EventHandler(this.TmExecutive_Tick);
            // 
            // TxtEmployeeNumber
            // 
            this.TxtEmployeeNumber.BackColor = System.Drawing.SystemColors.Info;
            this.TxtEmployeeNumber.Location = new System.Drawing.Point(24, 22);
            this.TxtEmployeeNumber.MaxLength = 15;
            this.TxtEmployeeNumber.Name = "TxtEmployeeNumber";
            this.TxtEmployeeNumber.Size = new System.Drawing.Size(93, 20);
            this.TxtEmployeeNumber.TabIndex = 0;
            this.TxtEmployeeNumber.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // LblEmployeeNumber
            // 
            this.LblEmployeeNumber.AutoSize = true;
            this.LblEmployeeNumber.Location = new System.Drawing.Point(24, 6);
            this.LblEmployeeNumber.Name = "LblEmployeeNumber";
            this.LblEmployeeNumber.Size = new System.Drawing.Size(93, 13);
            this.LblEmployeeNumber.TabIndex = 3;
            this.LblEmployeeNumber.Text = "Employee Number";
            // 
            // LblSalutation
            // 
            this.LblSalutation.AutoSize = true;
            this.LblSalutation.Location = new System.Drawing.Point(140, 6);
            this.LblSalutation.Name = "LblSalutation";
            this.LblSalutation.Size = new System.Drawing.Size(54, 13);
            this.LblSalutation.TabIndex = 5;
            this.LblSalutation.Text = "Salutation";
            // 
            // TxtFirstName
            // 
            this.TxtFirstName.BackColor = System.Drawing.SystemColors.Info;
            this.TxtFirstName.Location = new System.Drawing.Point(257, 22);
            this.TxtFirstName.MaxLength = 25;
            this.TxtFirstName.Name = "TxtFirstName";
            this.TxtFirstName.Size = new System.Drawing.Size(152, 20);
            this.TxtFirstName.TabIndex = 3;
            this.TxtFirstName.TextChanged += new System.EventHandler(this.TxtFirstName_TextChanged);
            // 
            // LblFirstName
            // 
            this.LblFirstName.AutoSize = true;
            this.LblFirstName.Location = new System.Drawing.Point(255, 6);
            this.LblFirstName.Name = "LblFirstName";
            this.LblFirstName.Size = new System.Drawing.Size(57, 13);
            this.LblFirstName.TabIndex = 7;
            this.LblFirstName.Text = "First Name";
            // 
            // TxtMiddleName
            // 
            this.TxtMiddleName.Location = new System.Drawing.Point(414, 22);
            this.TxtMiddleName.MaxLength = 25;
            this.TxtMiddleName.Name = "TxtMiddleName";
            this.TxtMiddleName.Size = new System.Drawing.Size(152, 20);
            this.TxtMiddleName.TabIndex = 4;
            this.TxtMiddleName.TextChanged += new System.EventHandler(this.TxtMiddleName_TextChanged);
            // 
            // LblMiddleName
            // 
            this.LblMiddleName.AutoSize = true;
            this.LblMiddleName.Location = new System.Drawing.Point(412, 6);
            this.LblMiddleName.Name = "LblMiddleName";
            this.LblMiddleName.Size = new System.Drawing.Size(69, 13);
            this.LblMiddleName.TabIndex = 9;
            this.LblMiddleName.Text = "Middle Name";
            // 
            // TxtLastName
            // 
            this.TxtLastName.Location = new System.Drawing.Point(571, 22);
            this.TxtLastName.MaxLength = 25;
            this.TxtLastName.Name = "TxtLastName";
            this.TxtLastName.Size = new System.Drawing.Size(158, 20);
            this.TxtLastName.TabIndex = 5;
            this.TxtLastName.TextChanged += new System.EventHandler(this.TxtLastName_TextChanged);
            // 
            // LblLastName
            // 
            this.LblLastName.AutoSize = true;
            this.LblLastName.Location = new System.Drawing.Point(571, 6);
            this.LblLastName.Name = "LblLastName";
            this.LblLastName.Size = new System.Drawing.Size(58, 13);
            this.LblLastName.TabIndex = 11;
            this.LblLastName.Text = "Last Name";
            // 
            // CboSalutation
            // 
            this.CboSalutation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSalutation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSalutation.BackColor = System.Drawing.SystemColors.Info;
            this.CboSalutation.DropDownHeight = 75;
            this.CboSalutation.DropDownWidth = 60;
            this.CboSalutation.FormattingEnabled = true;
            this.CboSalutation.IntegralHeight = false;
            this.CboSalutation.Location = new System.Drawing.Point(143, 22);
            this.CboSalutation.Name = "CboSalutation";
            this.CboSalutation.Size = new System.Drawing.Size(62, 21);
            this.CboSalutation.TabIndex = 1;
            this.CboSalutation.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            this.CboSalutation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // PnlEmployee
            // 
            this.PnlEmployee.Controls.Add(this.BtnSalutation);
            this.PnlEmployee.Controls.Add(this.TbEmployee);
            this.PnlEmployee.Controls.Add(this.CboSalutation);
            this.PnlEmployee.Controls.Add(this.LblLastName);
            this.PnlEmployee.Controls.Add(this.TxtLastName);
            this.PnlEmployee.Controls.Add(this.LblMiddleName);
            this.PnlEmployee.Controls.Add(this.TxtMiddleName);
            this.PnlEmployee.Controls.Add(this.LblFirstName);
            this.PnlEmployee.Controls.Add(this.TxtFirstName);
            this.PnlEmployee.Controls.Add(this.LblSalutation);
            this.PnlEmployee.Controls.Add(this.LblEmployeeNumber);
            this.PnlEmployee.Controls.Add(this.TxtEmployeeNumber);
            this.PnlEmployee.Controls.Add(this.txtEmployeeFullName);
            this.PnlEmployee.Location = new System.Drawing.Point(0, 58);
            this.PnlEmployee.Name = "PnlEmployee";
            this.PnlEmployee.Size = new System.Drawing.Size(738, 390);
            this.PnlEmployee.TabIndex = 0;
            // 
            // BtnSalutation
            // 
            this.BtnSalutation.Location = new System.Drawing.Point(211, 21);
            this.BtnSalutation.Name = "BtnSalutation";
            this.BtnSalutation.Size = new System.Drawing.Size(26, 23);
            this.BtnSalutation.TabIndex = 2;
            this.BtnSalutation.Text = "...";
            this.BtnSalutation.UseVisualStyleBackColor = true;
            this.BtnSalutation.Click += new System.EventHandler(this.BtnSalutation_Click);
            // 
            // TbEmployee
            // 
            this.TbEmployee.Controls.Add(this.TpGeneral);
            this.TbEmployee.Controls.Add(this.TbContacts);
            this.TbEmployee.Controls.Add(this.tpPayroll);
            this.TbEmployee.Location = new System.Drawing.Point(9, 48);
            this.TbEmployee.Name = "TbEmployee";
            this.TbEmployee.SelectedIndex = 0;
            this.TbEmployee.Size = new System.Drawing.Size(720, 338);
            this.TbEmployee.TabIndex = 6;
            // 
            // TpGeneral
            // 
            this.TpGeneral.AutoScroll = true;
            this.TpGeneral.Controls.Add(this.BtnAttach);
            this.TpGeneral.Controls.Add(this.BtnContxtMenu1);
            this.TpGeneral.Controls.Add(this.PassportPhotoPictureBox);
            this.TpGeneral.Controls.Add(ReligionIDLabel);
            this.TpGeneral.Controls.Add(this.cboReligion);
            this.TpGeneral.Controls.Add(this.BtnReligion);
            this.TpGeneral.Controls.Add(this.rdbMale);
            this.TpGeneral.Controls.Add(NationalityIDLabel);
            this.TpGeneral.Controls.Add(this.cboNationality);
            this.TpGeneral.Controls.Add(this.cboCountryoforgin);
            this.TpGeneral.Controls.Add(this.rdbFemale);
            this.TpGeneral.Controls.Add(this.BtnNationality);
            this.TpGeneral.Controls.Add(Label13);
            this.TpGeneral.Controls.Add(CountryofOriginIDLabel);
            this.TpGeneral.Controls.Add(this.BtnCountryoforigin);
            this.TpGeneral.Controls.Add(this.cboWorkStatus);
            this.TpGeneral.Controls.Add(DateofJoiningLabel);
            this.TpGeneral.Controls.Add(this.DtpDateofJoining);
            this.TpGeneral.Controls.Add(DateofBirthLabel);
            this.TpGeneral.Controls.Add(this.dtpDateofBirth);
            this.TpGeneral.Controls.Add(label4);
            this.TpGeneral.Controls.Add(this.cboEmploymentType);
            this.TpGeneral.Controls.Add(EmploymentTypeLabel);
            this.TpGeneral.Controls.Add(this.BtnEmptype);
            this.TpGeneral.Controls.Add(OfficialEmailLabel);
            this.TpGeneral.Controls.Add(this.TxtOfficialEmail);
            this.TpGeneral.Controls.Add(this.cboDepartment);
            this.TpGeneral.Controls.Add(this.BtnDesignation);
            this.TpGeneral.Controls.Add(this.BtnDepartment);
            this.TpGeneral.Controls.Add(this.cboDesignation);
            this.TpGeneral.Controls.Add(DesignationIDLabel);
            this.TpGeneral.Controls.Add(DepartmentIDLabel);
            this.TpGeneral.Controls.Add(Label8);
            this.TpGeneral.Controls.Add(this.cboWorkingAt);
            this.TpGeneral.Controls.Add(WorkingAtLabel);
            this.TpGeneral.Controls.Add(this.BtnWorkstatus);
            this.TpGeneral.Controls.Add(WorkStatusIDLabel);
            this.TpGeneral.Controls.Add(this.ShapeContainer1);
            this.TpGeneral.Location = new System.Drawing.Point(4, 22);
            this.TpGeneral.Name = "TpGeneral";
            this.TpGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.TpGeneral.Size = new System.Drawing.Size(712, 312);
            this.TpGeneral.TabIndex = 1;
            this.TpGeneral.Text = "Employment";
            this.TpGeneral.UseVisualStyleBackColor = true;
            // 
            // BtnAttach
            // 
            this.BtnAttach.Location = new System.Drawing.Point(577, 193);
            this.BtnAttach.Name = "BtnAttach";
            this.BtnAttach.Size = new System.Drawing.Size(75, 23);
            this.BtnAttach.TabIndex = 21;
            this.BtnAttach.Text = "Attach";
            this.BtnAttach.UseVisualStyleBackColor = true;
            this.BtnAttach.Click += new System.EventHandler(this.BtnAttach_Click);
            // 
            // BtnContxtMenu1
            // 
            this.BtnContxtMenu1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.BtnContxtMenu1.ContextMenuStrip = this.ContextMenuStrip2;
            this.BtnContxtMenu1.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.BtnContxtMenu1.Location = new System.Drawing.Point(655, 193);
            this.BtnContxtMenu1.Name = "BtnContxtMenu1";
            this.BtnContxtMenu1.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.BtnContxtMenu1.Size = new System.Drawing.Size(20, 23);
            this.BtnContxtMenu1.TabIndex = 22;
            this.BtnContxtMenu1.Text = "6";
            this.BtnContxtMenu1.UseVisualStyleBackColor = true;
            this.BtnContxtMenu1.Click += new System.EventHandler(this.BtnContxtMenu1_Click);
            // 
            // ContextMenuStrip2
            // 
            this.ContextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuAttach,
            this.ToolStripMenuScan,
            this.ToolStripMenuRemove});
            this.ContextMenuStrip2.Name = "ContextMenuStrip2";
            this.ContextMenuStrip2.Size = new System.Drawing.Size(121, 70);
            // 
            // ToolStripMenuAttach
            // 
            this.ToolStripMenuAttach.Name = "ToolStripMenuAttach";
            this.ToolStripMenuAttach.Size = new System.Drawing.Size(120, 22);
            this.ToolStripMenuAttach.Text = "Attach";
            this.ToolStripMenuAttach.Click += new System.EventHandler(this.ToolStripMenuAttach_Click);
            // 
            // ToolStripMenuScan
            // 
            this.ToolStripMenuScan.Name = "ToolStripMenuScan";
            this.ToolStripMenuScan.Size = new System.Drawing.Size(120, 22);
            this.ToolStripMenuScan.Text = "Scan";
            this.ToolStripMenuScan.Visible = false;
            this.ToolStripMenuScan.Click += new System.EventHandler(this.ToolStripMenuScan_Click);
            // 
            // ToolStripMenuRemove
            // 
            this.ToolStripMenuRemove.Name = "ToolStripMenuRemove";
            this.ToolStripMenuRemove.Size = new System.Drawing.Size(120, 22);
            this.ToolStripMenuRemove.Text = "Remove ";
            this.ToolStripMenuRemove.Click += new System.EventHandler(this.ToolStripMenuRemove_Click);
            // 
            // PassportPhotoPictureBox
            // 
            this.PassportPhotoPictureBox.Location = new System.Drawing.Point(545, 33);
            this.PassportPhotoPictureBox.Name = "PassportPhotoPictureBox";
            this.PassportPhotoPictureBox.Size = new System.Drawing.Size(130, 154);
            this.PassportPhotoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PassportPhotoPictureBox.TabIndex = 1;
            this.PassportPhotoPictureBox.TabStop = false;
            // 
            // cboReligion
            // 
            this.cboReligion.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboReligion.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboReligion.BackColor = System.Drawing.SystemColors.Info;
            this.cboReligion.DropDownHeight = 75;
            this.cboReligion.FormattingEnabled = true;
            this.cboReligion.IntegralHeight = false;
            this.cboReligion.Location = new System.Drawing.Point(115, 249);
            this.cboReligion.Name = "cboReligion";
            this.cboReligion.Size = new System.Drawing.Size(222, 21);
            this.cboReligion.TabIndex = 9;
            this.cboReligion.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            this.cboReligion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // BtnReligion
            // 
            this.BtnReligion.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.BtnReligion.Location = new System.Drawing.Point(343, 248);
            this.BtnReligion.Name = "BtnReligion";
            this.BtnReligion.Size = new System.Drawing.Size(28, 22);
            this.BtnReligion.TabIndex = 10;
            this.BtnReligion.Text = "...";
            this.BtnReligion.UseVisualStyleBackColor = true;
            this.BtnReligion.Click += new System.EventHandler(this.BtnReligion_Click);
            // 
            // rdbMale
            // 
            this.rdbMale.AutoSize = true;
            this.rdbMale.BackColor = System.Drawing.Color.White;
            this.rdbMale.Checked = true;
            this.rdbMale.Location = new System.Drawing.Point(115, 172);
            this.rdbMale.Name = "rdbMale";
            this.rdbMale.Size = new System.Drawing.Size(48, 17);
            this.rdbMale.TabIndex = 1;
            this.rdbMale.TabStop = true;
            this.rdbMale.Text = "Male";
            this.rdbMale.UseVisualStyleBackColor = false;
            this.rdbMale.CheckedChanged += new System.EventHandler(this.changestatus);
            // 
            // cboNationality
            // 
            this.cboNationality.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboNationality.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboNationality.BackColor = System.Drawing.SystemColors.Info;
            this.cboNationality.DropDownHeight = 75;
            this.cboNationality.FormattingEnabled = true;
            this.cboNationality.IntegralHeight = false;
            this.cboNationality.Location = new System.Drawing.Point(115, 222);
            this.cboNationality.Name = "cboNationality";
            this.cboNationality.Size = new System.Drawing.Size(222, 21);
            this.cboNationality.TabIndex = 5;
            this.cboNationality.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            this.cboNationality.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // cboCountryoforgin
            // 
            this.cboCountryoforgin.AllowDrop = true;
            this.cboCountryoforgin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCountryoforgin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCountryoforgin.BackColor = System.Drawing.SystemColors.Info;
            this.cboCountryoforgin.DropDownHeight = 75;
            this.cboCountryoforgin.FormattingEnabled = true;
            this.cboCountryoforgin.IntegralHeight = false;
            this.cboCountryoforgin.Location = new System.Drawing.Point(115, 195);
            this.cboCountryoforgin.Name = "cboCountryoforgin";
            this.cboCountryoforgin.Size = new System.Drawing.Size(222, 21);
            this.cboCountryoforgin.TabIndex = 3;
            this.cboCountryoforgin.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            this.cboCountryoforgin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // rdbFemale
            // 
            this.rdbFemale.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.rdbFemale.AutoSize = true;
            this.rdbFemale.BackColor = System.Drawing.Color.White;
            this.rdbFemale.Location = new System.Drawing.Point(167, 172);
            this.rdbFemale.Name = "rdbFemale";
            this.rdbFemale.Size = new System.Drawing.Size(59, 17);
            this.rdbFemale.TabIndex = 2;
            this.rdbFemale.Text = "Female";
            this.rdbFemale.UseVisualStyleBackColor = false;
            this.rdbFemale.CheckedChanged += new System.EventHandler(this.changestatus);
            // 
            // BtnNationality
            // 
            this.BtnNationality.Location = new System.Drawing.Point(343, 221);
            this.BtnNationality.Name = "BtnNationality";
            this.BtnNationality.Size = new System.Drawing.Size(28, 22);
            this.BtnNationality.TabIndex = 6;
            this.BtnNationality.Text = "...";
            this.BtnNationality.UseVisualStyleBackColor = true;
            this.BtnNationality.Click += new System.EventHandler(this.BtnNationality_Click);
            // 
            // BtnCountryoforigin
            // 
            this.BtnCountryoforigin.Location = new System.Drawing.Point(343, 194);
            this.BtnCountryoforigin.Name = "BtnCountryoforigin";
            this.BtnCountryoforigin.Size = new System.Drawing.Size(28, 22);
            this.BtnCountryoforigin.TabIndex = 4;
            this.BtnCountryoforigin.Text = "...";
            this.BtnCountryoforigin.UseVisualStyleBackColor = true;
            this.BtnCountryoforigin.Click += new System.EventHandler(this.BtnCountryoforigin_Click);
            // 
            // cboWorkStatus
            // 
            this.cboWorkStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkStatus.BackColor = System.Drawing.SystemColors.Info;
            this.cboWorkStatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboWorkStatus.DropDownHeight = 134;
            this.cboWorkStatus.FormattingEnabled = true;
            this.cboWorkStatus.IntegralHeight = false;
            this.cboWorkStatus.Location = new System.Drawing.Point(115, 91);
            this.cboWorkStatus.Name = "cboWorkStatus";
            this.cboWorkStatus.Size = new System.Drawing.Size(222, 21);
            this.cboWorkStatus.TabIndex = 19;
            this.cboWorkStatus.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            this.cboWorkStatus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // DtpDateofJoining
            // 
            this.DtpDateofJoining.CustomFormat = "dd-MMM-yyyy";
            this.DtpDateofJoining.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpDateofJoining.Location = new System.Drawing.Point(506, 276);
            this.DtpDateofJoining.Name = "DtpDateofJoining";
            this.DtpDateofJoining.Size = new System.Drawing.Size(101, 20);
            this.DtpDateofJoining.TabIndex = 18;
            this.DtpDateofJoining.ValueChanged += new System.EventHandler(this.changestatus);
            // 
            // cboEmploymentType
            // 
            this.cboEmploymentType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmploymentType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmploymentType.DropDownHeight = 75;
            this.cboEmploymentType.FormattingEnabled = true;
            this.cboEmploymentType.IntegralHeight = false;
            this.cboEmploymentType.Location = new System.Drawing.Point(115, 64);
            this.cboEmploymentType.Name = "cboEmploymentType";
            this.cboEmploymentType.Size = new System.Drawing.Size(222, 21);
            this.cboEmploymentType.TabIndex = 9;
            this.cboEmploymentType.SelectedIndexChanged += new System.EventHandler(this.cboEmploymentType_SelectedIndexChanged);
            this.cboEmploymentType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // BtnEmptype
            // 
            this.BtnEmptype.Location = new System.Drawing.Point(343, 63);
            this.BtnEmptype.Name = "BtnEmptype";
            this.BtnEmptype.Size = new System.Drawing.Size(28, 22);
            this.BtnEmptype.TabIndex = 10;
            this.BtnEmptype.Text = "...";
            this.BtnEmptype.UseVisualStyleBackColor = true;
            this.BtnEmptype.Click += new System.EventHandler(this.BtnEmptype_Click);
            // 
            // TxtOfficialEmail
            // 
            this.TxtOfficialEmail.Location = new System.Drawing.Point(115, 276);
            this.TxtOfficialEmail.MaxLength = 50;
            this.TxtOfficialEmail.Name = "TxtOfficialEmail";
            this.TxtOfficialEmail.Size = new System.Drawing.Size(222, 20);
            this.TxtOfficialEmail.TabIndex = 29;
            this.TxtOfficialEmail.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // cboDepartment
            // 
            this.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDepartment.BackColor = System.Drawing.SystemColors.Info;
            this.cboDepartment.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboDepartment.DropDownHeight = 75;
            this.cboDepartment.FormattingEnabled = true;
            this.cboDepartment.IntegralHeight = false;
            this.cboDepartment.Location = new System.Drawing.Point(115, 118);
            this.cboDepartment.Name = "cboDepartment";
            this.cboDepartment.Size = new System.Drawing.Size(222, 21);
            this.cboDepartment.TabIndex = 21;
            this.cboDepartment.SelectedIndexChanged += new System.EventHandler(this.cboDepartment_SelectedIndexChanged);
            this.cboDepartment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // BtnDesignation
            // 
            this.BtnDesignation.Location = new System.Drawing.Point(343, 144);
            this.BtnDesignation.Name = "BtnDesignation";
            this.BtnDesignation.Size = new System.Drawing.Size(28, 22);
            this.BtnDesignation.TabIndex = 24;
            this.BtnDesignation.Text = "...";
            this.BtnDesignation.UseVisualStyleBackColor = true;
            this.BtnDesignation.Click += new System.EventHandler(this.BtnDesignation_Click);
            // 
            // BtnDepartment
            // 
            this.BtnDepartment.Location = new System.Drawing.Point(343, 117);
            this.BtnDepartment.Name = "BtnDepartment";
            this.BtnDepartment.Size = new System.Drawing.Size(28, 22);
            this.BtnDepartment.TabIndex = 22;
            this.BtnDepartment.Text = "...";
            this.BtnDepartment.UseVisualStyleBackColor = true;
            this.BtnDepartment.Click += new System.EventHandler(this.BtnDepartment_Click);
            // 
            // cboDesignation
            // 
            this.cboDesignation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDesignation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDesignation.BackColor = System.Drawing.SystemColors.Info;
            this.cboDesignation.DropDownHeight = 75;
            this.cboDesignation.FormattingEnabled = true;
            this.cboDesignation.IntegralHeight = false;
            this.cboDesignation.Location = new System.Drawing.Point(115, 145);
            this.cboDesignation.Name = "cboDesignation";
            this.cboDesignation.Size = new System.Drawing.Size(222, 21);
            this.cboDesignation.TabIndex = 23;
            this.cboDesignation.SelectedIndexChanged += new System.EventHandler(this.cboDesignation_SelectedIndexChanged);
            this.cboDesignation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // cboWorkingAt
            // 
            this.cboWorkingAt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkingAt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkingAt.BackColor = System.Drawing.SystemColors.Info;
            this.cboWorkingAt.DropDownHeight = 75;
            this.cboWorkingAt.FormattingEnabled = true;
            this.cboWorkingAt.IntegralHeight = false;
            this.cboWorkingAt.Location = new System.Drawing.Point(115, 37);
            this.cboWorkingAt.Name = "cboWorkingAt";
            this.cboWorkingAt.Size = new System.Drawing.Size(222, 21);
            this.cboWorkingAt.TabIndex = 7;
            this.cboWorkingAt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // BtnWorkstatus
            // 
            this.BtnWorkstatus.Location = new System.Drawing.Point(343, 90);
            this.BtnWorkstatus.Name = "BtnWorkstatus";
            this.BtnWorkstatus.Size = new System.Drawing.Size(28, 22);
            this.BtnWorkstatus.TabIndex = 20;
            this.BtnWorkstatus.Text = "...";
            this.BtnWorkstatus.UseVisualStyleBackColor = true;
            this.BtnWorkstatus.Click += new System.EventHandler(this.BtnWorkstatus_Click);
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(3, 3);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape5});
            this.ShapeContainer1.Size = new System.Drawing.Size(706, 306);
            this.ShapeContainer1.TabIndex = 0;
            this.ShapeContainer1.TabStop = false;
            // 
            // lineShape5
            // 
            this.lineShape5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape5.Name = "lineShape5";
            this.lineShape5.X1 = 28;
            this.lineShape5.X2 = 704;
            this.lineShape5.Y1 = 19;
            this.lineShape5.Y2 = 19;
            // 
            // TbContacts
            // 
            this.TbContacts.Controls.Add(Label25);
            this.TbContacts.Controls.Add(this.groupBox1);
            this.TbContacts.Controls.Add(this.txtLocalPhone);
            this.TbContacts.Controls.Add(label17);
            this.TbContacts.Controls.Add(this.txtLocalAddress);
            this.TbContacts.Controls.Add(label18);
            this.TbContacts.Controls.Add(this.txtEmergencyPhone);
            this.TbContacts.Controls.Add(label3);
            this.TbContacts.Controls.Add(this.txtEmergencyAddress);
            this.TbContacts.Controls.Add(label10);
            this.TbContacts.Controls.Add(this.txtRemarks);
            this.TbContacts.Controls.Add(label20);
            this.TbContacts.Controls.Add(this.ShapeContainer2);
            this.TbContacts.Location = new System.Drawing.Point(4, 22);
            this.TbContacts.Name = "TbContacts";
            this.TbContacts.Padding = new System.Windows.Forms.Padding(3);
            this.TbContacts.Size = new System.Drawing.Size(712, 312);
            this.TbContacts.TabIndex = 2;
            this.TbContacts.Text = "Contacts";
            this.TbContacts.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(EmailLabel);
            this.groupBox1.Controls.Add(this.txtPermanentAddress);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(LocalAddressLabel);
            this.groupBox1.Controls.Add(this.txtPhone);
            this.groupBox1.Controls.Add(LocalPhoneLabel);
            this.groupBox1.Controls.Add(this.txtMobile);
            this.groupBox1.Controls.Add(LocalMobileLabel);
            this.groupBox1.Location = new System.Drawing.Point(11, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(329, 201);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Permanent  Info";
            // 
            // txtPermanentAddress
            // 
            this.txtPermanentAddress.Location = new System.Drawing.Point(78, 19);
            this.txtPermanentAddress.MaxLength = 300;
            this.txtPermanentAddress.Multiline = true;
            this.txtPermanentAddress.Name = "txtPermanentAddress";
            this.txtPermanentAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtPermanentAddress.Size = new System.Drawing.Size(237, 96);
            this.txtPermanentAddress.TabIndex = 0;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(78, 173);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(237, 20);
            this.txtEmail.TabIndex = 3;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(78, 121);
            this.txtPhone.MaxLength = 15;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(237, 20);
            this.txtPhone.TabIndex = 1;
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new System.Drawing.Point(78, 147);
            this.txtMobile.MaxLength = 40;
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(237, 20);
            this.txtMobile.TabIndex = 2;
            // 
            // txtLocalPhone
            // 
            this.txtLocalPhone.Location = new System.Drawing.Point(453, 201);
            this.txtLocalPhone.MaxLength = 15;
            this.txtLocalPhone.Name = "txtLocalPhone";
            this.txtLocalPhone.Size = new System.Drawing.Size(250, 20);
            this.txtLocalPhone.TabIndex = 4;
            // 
            // txtLocalAddress
            // 
            this.txtLocalAddress.Location = new System.Drawing.Point(453, 143);
            this.txtLocalAddress.MaxLength = 300;
            this.txtLocalAddress.Multiline = true;
            this.txtLocalAddress.Name = "txtLocalAddress";
            this.txtLocalAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLocalAddress.Size = new System.Drawing.Size(250, 52);
            this.txtLocalAddress.TabIndex = 3;
            // 
            // txtEmergencyPhone
            // 
            this.txtEmergencyPhone.Location = new System.Drawing.Point(453, 117);
            this.txtEmergencyPhone.MaxLength = 15;
            this.txtEmergencyPhone.Name = "txtEmergencyPhone";
            this.txtEmergencyPhone.Size = new System.Drawing.Size(250, 20);
            this.txtEmergencyPhone.TabIndex = 2;
            // 
            // txtEmergencyAddress
            // 
            this.txtEmergencyAddress.Location = new System.Drawing.Point(453, 47);
            this.txtEmergencyAddress.MaxLength = 300;
            this.txtEmergencyAddress.Multiline = true;
            this.txtEmergencyAddress.Name = "txtEmergencyAddress";
            this.txtEmergencyAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtEmergencyAddress.Size = new System.Drawing.Size(250, 64);
            this.txtEmergencyAddress.TabIndex = 1;
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(20, 248);
            this.txtRemarks.MaxLength = 1000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(683, 59);
            this.txtRemarks.TabIndex = 4;
            // 
            // ShapeContainer2
            // 
            this.ShapeContainer2.Location = new System.Drawing.Point(3, 3);
            this.ShapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer2.Name = "ShapeContainer2";
            this.ShapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.ShapeContainer2.Size = new System.Drawing.Size(706, 306);
            this.ShapeContainer2.TabIndex = 0;
            this.ShapeContainer2.TabStop = false;
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.Color.Silver;
            this.LineShape1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 52;
            this.LineShape1.X2 = 699;
            this.LineShape1.Y1 = 12;
            this.LineShape1.Y2 = 12;
            // 
            // tpPayroll
            // 
            this.tpPayroll.Controls.Add(this.cboVisafrom);
            this.tpPayroll.Controls.Add(label9);
            this.tpPayroll.Controls.Add(this.btnSettlementPolicy);
            this.tpPayroll.Controls.Add(this.cboSettlementPolicy);
            this.tpPayroll.Controls.Add(label7);
            this.tpPayroll.Controls.Add(this.txtPersonID);
            this.tpPayroll.Controls.Add(this.Label11);
            this.tpPayroll.Controls.Add(this.btnVacationPolicy);
            this.tpPayroll.Controls.Add(this.cboVacationPolicy);
            this.tpPayroll.Controls.Add(label6);
            this.tpPayroll.Controls.Add(label5);
            this.tpPayroll.Controls.Add(Label16);
            this.tpPayroll.Controls.Add(TransactionTypeIDLabel);
            this.tpPayroll.Controls.Add(this.CboEmployeeBankname);
            this.tpPayroll.Controls.Add(this.Label14);
            this.tpPayroll.Controls.Add(this.TransactionTypeReferenceComboBox);
            this.tpPayroll.Controls.Add(this.AccountNumberTextBox);
            this.tpPayroll.Controls.Add(this.ComBankAccIdComboBox);
            this.tpPayroll.Controls.Add(this.LblBankAccount);
            this.tpPayroll.Controls.Add(this.AccountNumberLabel);
            this.tpPayroll.Controls.Add(this.BankNameReferenceComboBox);
            this.tpPayroll.Controls.Add(this.LblBankName);
            this.tpPayroll.Controls.Add(this.btnLeavePolicy);
            this.tpPayroll.Controls.Add(this.cboLeavePolicy);
            this.tpPayroll.Controls.Add(label2);
            this.tpPayroll.Controls.Add(this.btnWorkPolicy);
            this.tpPayroll.Controls.Add(this.cboWorkPolicy);
            this.tpPayroll.Controls.Add(label1);
            this.tpPayroll.Controls.Add(this.shapeContainer3);
            this.tpPayroll.Location = new System.Drawing.Point(4, 22);
            this.tpPayroll.Name = "tpPayroll";
            this.tpPayroll.Padding = new System.Windows.Forms.Padding(3);
            this.tpPayroll.Size = new System.Drawing.Size(712, 312);
            this.tpPayroll.TabIndex = 3;
            this.tpPayroll.Text = "Payroll";
            this.tpPayroll.UseVisualStyleBackColor = true;
            // 
            // cboVisafrom
            // 
            this.cboVisafrom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboVisafrom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboVisafrom.BackColor = System.Drawing.Color.White;
            this.cboVisafrom.DropDownHeight = 134;
            this.cboVisafrom.FormattingEnabled = true;
            this.cboVisafrom.IntegralHeight = false;
            this.cboVisafrom.Location = new System.Drawing.Point(499, 35);
            this.cboVisafrom.Name = "cboVisafrom";
            this.cboVisafrom.Size = new System.Drawing.Size(182, 21);
            this.cboVisafrom.TabIndex = 952;
            this.cboVisafrom.SelectedIndexChanged += new System.EventHandler(this.cboVisafrom_SelectedIndexChanged);
            // 
            // btnSettlementPolicy
            // 
            this.btnSettlementPolicy.Location = new System.Drawing.Point(321, 115);
            this.btnSettlementPolicy.Name = "btnSettlementPolicy";
            this.btnSettlementPolicy.Size = new System.Drawing.Size(28, 22);
            this.btnSettlementPolicy.TabIndex = 950;
            this.btnSettlementPolicy.Text = "...";
            this.btnSettlementPolicy.UseVisualStyleBackColor = true;
            this.btnSettlementPolicy.Click += new System.EventHandler(this.btnSettlementPolicy_Click);
            // 
            // cboSettlementPolicy
            // 
            this.cboSettlementPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSettlementPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSettlementPolicy.BackColor = System.Drawing.SystemColors.Window;
            this.cboSettlementPolicy.DropDownHeight = 134;
            this.cboSettlementPolicy.FormattingEnabled = true;
            this.cboSettlementPolicy.IntegralHeight = false;
            this.cboSettlementPolicy.Location = new System.Drawing.Point(134, 115);
            this.cboSettlementPolicy.Name = "cboSettlementPolicy";
            this.cboSettlementPolicy.Size = new System.Drawing.Size(182, 21);
            this.cboSettlementPolicy.TabIndex = 949;
            this.cboSettlementPolicy.SelectedIndexChanged += new System.EventHandler(this.cboSettlementPolicy_SelectedIndexChanged);
            // 
            // txtPersonID
            // 
            this.txtPersonID.Location = new System.Drawing.Point(134, 274);
            this.txtPersonID.MaxLength = 14;
            this.txtPersonID.Name = "txtPersonID";
            this.txtPersonID.Size = new System.Drawing.Size(184, 20);
            this.txtPersonID.TabIndex = 947;
            this.txtPersonID.Visible = false;
            this.txtPersonID.TextChanged += new System.EventHandler(this.txtPersonID_TextChanged);
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(16, 278);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(88, 13);
            this.Label11.TabIndex = 948;
            this.Label11.Text = "Person ID (WPS)";
            this.Label11.Visible = false;
            // 
            // btnVacationPolicy
            // 
            this.btnVacationPolicy.Location = new System.Drawing.Point(321, 88);
            this.btnVacationPolicy.Name = "btnVacationPolicy";
            this.btnVacationPolicy.Size = new System.Drawing.Size(28, 22);
            this.btnVacationPolicy.TabIndex = 945;
            this.btnVacationPolicy.Text = "...";
            this.btnVacationPolicy.UseVisualStyleBackColor = true;
            this.btnVacationPolicy.Click += new System.EventHandler(this.btnVacationPolicy_Click);
            // 
            // cboVacationPolicy
            // 
            this.cboVacationPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboVacationPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboVacationPolicy.BackColor = System.Drawing.SystemColors.Window;
            this.cboVacationPolicy.DropDownHeight = 134;
            this.cboVacationPolicy.FormattingEnabled = true;
            this.cboVacationPolicy.IntegralHeight = false;
            this.cboVacationPolicy.Location = new System.Drawing.Point(134, 88);
            this.cboVacationPolicy.Name = "cboVacationPolicy";
            this.cboVacationPolicy.Size = new System.Drawing.Size(182, 21);
            this.cboVacationPolicy.TabIndex = 944;
            this.cboVacationPolicy.SelectedIndexChanged += new System.EventHandler(this.cboVacationPolicy_SelectedIndexChanged);
            // 
            // CboEmployeeBankname
            // 
            this.CboEmployeeBankname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboEmployeeBankname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboEmployeeBankname.DropDownHeight = 134;
            this.CboEmployeeBankname.FormattingEnabled = true;
            this.CboEmployeeBankname.IntegralHeight = false;
            this.CboEmployeeBankname.Location = new System.Drawing.Point(502, 220);
            this.CboEmployeeBankname.Name = "CboEmployeeBankname";
            this.CboEmployeeBankname.Size = new System.Drawing.Size(167, 21);
            this.CboEmployeeBankname.TabIndex = 933;
            this.CboEmployeeBankname.SelectedIndexChanged += new System.EventHandler(this.CboEmployeeBankname_SelectedIndexChanged);
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(376, 224);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(112, 13);
            this.Label14.TabIndex = 940;
            this.Label14.Text = "Employee Bank Name";
            // 
            // TransactionTypeReferenceComboBox
            // 
            this.TransactionTypeReferenceComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.TransactionTypeReferenceComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.TransactionTypeReferenceComboBox.BackColor = System.Drawing.Color.White;
            this.TransactionTypeReferenceComboBox.DropDownHeight = 134;
            this.TransactionTypeReferenceComboBox.FormattingEnabled = true;
            this.TransactionTypeReferenceComboBox.IntegralHeight = false;
            this.TransactionTypeReferenceComboBox.Location = new System.Drawing.Point(134, 193);
            this.TransactionTypeReferenceComboBox.Name = "TransactionTypeReferenceComboBox";
            this.TransactionTypeReferenceComboBox.Size = new System.Drawing.Size(184, 21);
            this.TransactionTypeReferenceComboBox.TabIndex = 928;
            this.TransactionTypeReferenceComboBox.SelectedIndexChanged += new System.EventHandler(this.TransactionTypeReferenceComboBox_SelectedIndexChanged);
            // 
            // AccountNumberTextBox
            // 
            this.AccountNumberTextBox.Location = new System.Drawing.Point(502, 247);
            this.AccountNumberTextBox.MaxLength = 40;
            this.AccountNumberTextBox.Name = "AccountNumberTextBox";
            this.AccountNumberTextBox.Size = new System.Drawing.Size(167, 20);
            this.AccountNumberTextBox.TabIndex = 935;
            this.AccountNumberTextBox.TextChanged += new System.EventHandler(this.AccountNumberTextBox_TextChanged);
            // 
            // ComBankAccIdComboBox
            // 
            this.ComBankAccIdComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ComBankAccIdComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ComBankAccIdComboBox.DropDownHeight = 134;
            this.ComBankAccIdComboBox.FormattingEnabled = true;
            this.ComBankAccIdComboBox.IntegralHeight = false;
            this.ComBankAccIdComboBox.Location = new System.Drawing.Point(134, 247);
            this.ComBankAccIdComboBox.Name = "ComBankAccIdComboBox";
            this.ComBankAccIdComboBox.Size = new System.Drawing.Size(184, 21);
            this.ComBankAccIdComboBox.TabIndex = 932;
            this.ComBankAccIdComboBox.SelectedIndexChanged += new System.EventHandler(this.ComBankAccIdComboBox_SelectedIndexChanged);
            // 
            // LblBankAccount
            // 
            this.LblBankAccount.AutoSize = true;
            this.LblBankAccount.Location = new System.Drawing.Point(16, 253);
            this.LblBankAccount.Name = "LblBankAccount";
            this.LblBankAccount.Size = new System.Drawing.Size(119, 13);
            this.LblBankAccount.TabIndex = 936;
            this.LblBankAccount.Text = "Employer Bank A/c No.";
            // 
            // AccountNumberLabel
            // 
            this.AccountNumberLabel.AutoSize = true;
            this.AccountNumberLabel.Location = new System.Drawing.Point(376, 250);
            this.AccountNumberLabel.Name = "AccountNumberLabel";
            this.AccountNumberLabel.Size = new System.Drawing.Size(122, 13);
            this.AccountNumberLabel.TabIndex = 939;
            this.AccountNumberLabel.Text = "Employee Bank A/c No.";
            // 
            // BankNameReferenceComboBox
            // 
            this.BankNameReferenceComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.BankNameReferenceComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.BankNameReferenceComboBox.DropDownHeight = 134;
            this.BankNameReferenceComboBox.FormattingEnabled = true;
            this.BankNameReferenceComboBox.IntegralHeight = false;
            this.BankNameReferenceComboBox.Location = new System.Drawing.Point(134, 220);
            this.BankNameReferenceComboBox.Name = "BankNameReferenceComboBox";
            this.BankNameReferenceComboBox.Size = new System.Drawing.Size(184, 21);
            this.BankNameReferenceComboBox.TabIndex = 930;
            this.BankNameReferenceComboBox.SelectedIndexChanged += new System.EventHandler(this.BankNameReferenceComboBox_SelectedIndexChanged);
            // 
            // LblBankName
            // 
            this.LblBankName.AutoSize = true;
            this.LblBankName.Location = new System.Drawing.Point(16, 224);
            this.LblBankName.Name = "LblBankName";
            this.LblBankName.Size = new System.Drawing.Size(109, 13);
            this.LblBankName.TabIndex = 937;
            this.LblBankName.Text = "Employer Bank Name";
            // 
            // btnLeavePolicy
            // 
            this.btnLeavePolicy.Location = new System.Drawing.Point(321, 59);
            this.btnLeavePolicy.Name = "btnLeavePolicy";
            this.btnLeavePolicy.Size = new System.Drawing.Size(28, 22);
            this.btnLeavePolicy.TabIndex = 510;
            this.btnLeavePolicy.Text = "...";
            this.btnLeavePolicy.UseVisualStyleBackColor = true;
            this.btnLeavePolicy.Click += new System.EventHandler(this.btnLeavePolicy_Click);
            // 
            // cboLeavePolicy
            // 
            this.cboLeavePolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboLeavePolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboLeavePolicy.BackColor = System.Drawing.SystemColors.Window;
            this.cboLeavePolicy.DropDownHeight = 134;
            this.cboLeavePolicy.FormattingEnabled = true;
            this.cboLeavePolicy.IntegralHeight = false;
            this.cboLeavePolicy.Location = new System.Drawing.Point(134, 61);
            this.cboLeavePolicy.Name = "cboLeavePolicy";
            this.cboLeavePolicy.Size = new System.Drawing.Size(182, 21);
            this.cboLeavePolicy.TabIndex = 509;
            this.cboLeavePolicy.SelectedIndexChanged += new System.EventHandler(this.cboLeavePolicy_SelectedIndexChanged);
            // 
            // btnWorkPolicy
            // 
            this.btnWorkPolicy.Location = new System.Drawing.Point(321, 34);
            this.btnWorkPolicy.Name = "btnWorkPolicy";
            this.btnWorkPolicy.Size = new System.Drawing.Size(28, 22);
            this.btnWorkPolicy.TabIndex = 508;
            this.btnWorkPolicy.Text = "...";
            this.btnWorkPolicy.UseVisualStyleBackColor = true;
            this.btnWorkPolicy.Click += new System.EventHandler(this.btnWorkPolicy_Click);
            // 
            // cboWorkPolicy
            // 
            this.cboWorkPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkPolicy.BackColor = System.Drawing.Color.White;
            this.cboWorkPolicy.DropDownHeight = 134;
            this.cboWorkPolicy.FormattingEnabled = true;
            this.cboWorkPolicy.IntegralHeight = false;
            this.cboWorkPolicy.Location = new System.Drawing.Point(134, 35);
            this.cboWorkPolicy.Name = "cboWorkPolicy";
            this.cboWorkPolicy.Size = new System.Drawing.Size(182, 21);
            this.cboWorkPolicy.TabIndex = 507;
            this.cboWorkPolicy.SelectedIndexChanged += new System.EventHandler(this.cboWorkPolicy_SelectedIndexChanged);
            // 
            // shapeContainer3
            // 
            this.shapeContainer3.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer3.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer3.Name = "shapeContainer3";
            this.shapeContainer3.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape6});
            this.shapeContainer3.Size = new System.Drawing.Size(706, 306);
            this.shapeContainer3.TabIndex = 942;
            this.shapeContainer3.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 51;
            this.lineShape2.X2 = 362;
            this.lineShape2.Y1 = 21;
            this.lineShape2.Y2 = 21;
            // 
            // lineShape6
            // 
            this.lineShape6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape6.Name = "lineShape6";
            this.lineShape6.X1 = 73;
            this.lineShape6.X2 = 633;
            this.lineShape6.Y1 = 164;
            this.lineShape6.Y2 = 164;
            // 
            // txtEmployeeFullName
            // 
            this.txtEmployeeFullName.Enabled = false;
            this.txtEmployeeFullName.Location = new System.Drawing.Point(717, 0);
            this.txtEmployeeFullName.MaxLength = 150;
            this.txtEmployeeFullName.Multiline = true;
            this.txtEmployeeFullName.Name = "txtEmployeeFullName";
            this.txtEmployeeFullName.Size = new System.Drawing.Size(12, 20);
            this.txtEmployeeFullName.TabIndex = 1;
            this.txtEmployeeFullName.Visible = false;
            this.txtEmployeeFullName.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // lineShape4
            // 
            this.lineShape4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 45;
            this.lineShape4.X2 = 705;
            this.lineShape4.Y1 = 12;
            this.lineShape4.Y2 = 12;
            // 
            // ContextMenuStrip1
            // 
            this.ContextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LocalContactsToolStripMenuItem,
            this.OverseasContactsToolStripMenuItem,
            this.EmergencyContactsToolStripMenuItem});
            this.ContextMenuStrip1.Name = "ContextMenuStrip1";
            this.ContextMenuStrip1.Size = new System.Drawing.Size(184, 70);
            // 
            // LocalContactsToolStripMenuItem
            // 
            this.LocalContactsToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.LocalContactsToolStripMenuItem.Name = "LocalContactsToolStripMenuItem";
            this.LocalContactsToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.LocalContactsToolStripMenuItem.Text = "Local Contacts";
            // 
            // OverseasContactsToolStripMenuItem
            // 
            this.OverseasContactsToolStripMenuItem.Name = "OverseasContactsToolStripMenuItem";
            this.OverseasContactsToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.OverseasContactsToolStripMenuItem.Text = "Overseas Contacts";
            // 
            // EmergencyContactsToolStripMenuItem
            // 
            this.EmergencyContactsToolStripMenuItem.Name = "EmergencyContactsToolStripMenuItem";
            this.EmergencyContactsToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.EmergencyContactsToolStripMenuItem.Text = "Emergency Contacts";
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.LblEmployeeStatus});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 480);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(739, 22);
            this.StatusStrip1.TabIndex = 67;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(39, 17);
            this.StatusLabel.Text = "Status";
            // 
            // LblEmployeeStatus
            // 
            this.LblEmployeeStatus.Name = "LblEmployeeStatus";
            this.LblEmployeeStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // FrmEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 502);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BnSearch);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BnEmployee);
            this.Controls.Add(this.PnlEmployee);
            this.Controls.Add(this.BtnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEmployee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Employee";
            this.Load += new System.EventHandler(this.FrmEmployee_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmEmployee_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.BnEmployee)).EndInit();
            this.BnEmployee.ResumeLayout(false);
            this.BnEmployee.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BnSearch)).EndInit();
            this.BnSearch.ResumeLayout(false);
            this.BnSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrEmployee)).EndInit();
            this.PnlEmployee.ResumeLayout(false);
            this.PnlEmployee.PerformLayout();
            this.TbEmployee.ResumeLayout(false);
            this.TpGeneral.ResumeLayout(false);
            this.TpGeneral.PerformLayout();
            this.ContextMenuStrip2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PassportPhotoPictureBox)).EndInit();
            this.TbContacts.ResumeLayout(false);
            this.TbContacts.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpPayroll.ResumeLayout(false);
            this.tpPayroll.PerformLayout();
            this.ContextMenuStrip1.ResumeLayout(false);
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnOk;
        internal System.Windows.Forms.BindingNavigator BnEmployee;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BnSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatornPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BnSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BnSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BtnClear;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        private System.Windows.Forms.BindingNavigator BnSearch;
        private System.Windows.Forms.ToolStripComboBox CboSearchOption;
        private System.Windows.Forms.ToolStripTextBox TxtSearchText;
        private System.Windows.Forms.ToolStripButton BtnSearch;
        private System.Windows.Forms.ErrorProvider ErrEmployee;
        private System.Windows.Forms.Timer TmEmployee;
        private System.Windows.Forms.Panel PnlEmployee;
        private System.Windows.Forms.ComboBox CboSalutation;
        private System.Windows.Forms.Label LblLastName;
        private System.Windows.Forms.TextBox TxtLastName;
        private System.Windows.Forms.Label LblMiddleName;
        private System.Windows.Forms.TextBox TxtMiddleName;
        private System.Windows.Forms.Label LblFirstName;
        private System.Windows.Forms.TextBox TxtFirstName;
        private System.Windows.Forms.Label LblSalutation;
        private System.Windows.Forms.Label LblEmployeeNumber;
        private System.Windows.Forms.TextBox TxtEmployeeNumber;
        private System.Windows.Forms.ToolTip TipEmployee;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        internal System.Windows.Forms.TabControl TbEmployee;
        internal System.Windows.Forms.TabPage TpGeneral;
        internal System.Windows.Forms.Button BtnContxtMenu1;
        internal System.Windows.Forms.Button BtnAttach;
        internal System.Windows.Forms.TextBox TxtOfficialEmail;
        internal System.Windows.Forms.ComboBox cboDepartment;
        internal System.Windows.Forms.Button BtnDesignation;
        internal System.Windows.Forms.Button BtnDepartment;
        internal System.Windows.Forms.ComboBox cboDesignation;
        internal System.Windows.Forms.TextBox txtEmployeeFullName;
        internal System.Windows.Forms.PictureBox PassportPhotoPictureBox;
        internal System.Windows.Forms.ComboBox cboWorkingAt;
        internal System.Windows.Forms.Button BtnWorkstatus;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
        internal System.Windows.Forms.DateTimePicker DtpDateofJoining;
        internal System.Windows.Forms.Button BtnEmptype;
        internal System.Windows.Forms.ComboBox cboEmploymentType;
        internal System.Windows.Forms.Button BtnReligion;
        internal System.Windows.Forms.Button BtnNationality;
        internal System.Windows.Forms.Button BtnCountryoforigin;
        internal System.Windows.Forms.ComboBox cboCountryoforgin;
        internal System.Windows.Forms.RadioButton rdbFemale;
        internal System.Windows.Forms.RadioButton rdbMale;
        internal System.Windows.Forms.ComboBox cboReligion;
        internal System.Windows.Forms.ComboBox cboNationality;
        internal System.Windows.Forms.DateTimePicker dtpDateofBirth;
        internal System.Windows.Forms.TabPage TbContacts;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.Button BtnSalutation;
        internal System.Windows.Forms.ContextMenuStrip ContextMenuStrip2;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuAttach;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuScan;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuRemove;
        internal System.Windows.Forms.ContextMenuStrip ContextMenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem LocalContactsToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem OverseasContactsToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem EmergencyContactsToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton bnMoreActions;
        private System.Windows.Forms.ToolStripMenuItem mnuItemAttachDocuments;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private System.Windows.Forms.ComboBox cboWorkStatus;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel LblEmployeeStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.TextBox txtPermanentAddress;
        internal System.Windows.Forms.TextBox txtEmail;
        internal System.Windows.Forms.TextBox txtPhone;
        internal System.Windows.Forms.TextBox txtLocalPhone;
        internal System.Windows.Forms.TextBox txtLocalAddress;
        internal System.Windows.Forms.TextBox txtEmergencyPhone;
        internal System.Windows.Forms.TextBox txtEmergencyAddress;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.TextBox txtMobile;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer2;
        private System.Windows.Forms.TabPage tpPayroll;
        internal System.Windows.Forms.ComboBox CboEmployeeBankname;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.ComboBox TransactionTypeReferenceComboBox;
        internal System.Windows.Forms.TextBox AccountNumberTextBox;
        internal System.Windows.Forms.ComboBox ComBankAccIdComboBox;
        internal System.Windows.Forms.Label LblBankAccount;
        internal System.Windows.Forms.Label AccountNumberLabel;
        internal System.Windows.Forms.ComboBox BankNameReferenceComboBox;
        internal System.Windows.Forms.Label LblBankName;
        internal System.Windows.Forms.ComboBox cboLeavePolicy;
        internal System.Windows.Forms.ComboBox cboWorkPolicy;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer3;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape6;
        internal System.Windows.Forms.ComboBox cboVacationPolicy;
        internal System.Windows.Forms.TextBox txtPersonID;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.ComboBox cboSettlementPolicy;
        internal System.Windows.Forms.Button btnSettlementPolicy;
        internal System.Windows.Forms.Button btnVacationPolicy;
        internal System.Windows.Forms.Button btnLeavePolicy;
        internal System.Windows.Forms.Button btnWorkPolicy;
        internal System.Windows.Forms.ComboBox cboVisafrom;
    }
}