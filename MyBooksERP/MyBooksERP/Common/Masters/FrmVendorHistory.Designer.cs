﻿namespace MyBooksERP
{
    partial class FrmVendorHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVendorHistory));
            this.pnlExpandVendorHistory = new DevComponents.DotNetBar.ExpandablePanel();
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.cboVendor = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboAssociative = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.Customer = new DevComponents.Editors.ComboItem();
            this.Supplier = new DevComponents.Editors.ComboItem();
            this.lblType = new DevComponents.DotNetBar.LabelX();
            this.txtType = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblAssociates = new DevComponents.DotNetBar.LabelX();
            this.cboType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ALL = new DevComponents.Editors.ComboItem();
            this.Country = new DevComponents.Editors.ComboItem();
            this.MobileNumber = new DevComponents.Editors.ComboItem();
            this.TransactionType = new DevComponents.Editors.ComboItem();
            this.tcVendorHistory = new System.Windows.Forms.TabControl();
            this.tpGeneralInfo = new System.Windows.Forms.TabPage();
            this.lblVendorStatusValue = new System.Windows.Forms.Label();
            this.lblVendorStatus = new System.Windows.Forms.Label();
            this.lblDocuments = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblRatingValue = new System.Windows.Forms.Label();
            this.lblRating = new System.Windows.Forms.Label();
            this.lblTransactionTypeValue = new System.Windows.Forms.Label();
            this.lblTransactionType = new System.Windows.Forms.Label();
            this.lblGenderOrSupClassValue = new System.Windows.Forms.Label();
            this.lblGenderOrSupClass = new System.Windows.Forms.Label();
            this.lblEmailValue = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblCodeValue = new System.Windows.Forms.Label();
            this.lblCode = new System.Windows.Forms.Label();
            this.lblNameValue = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.dgvDocuments = new System.Windows.Forms.DataGridView();
            this.dgvContactAddress = new System.Windows.Forms.DataGridView();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.tpCusSales = new System.Windows.Forms.TabPage();
            this.dgvCustmerSales = new System.Windows.Forms.DataGridView();
            this.tpCusPayment = new System.Windows.Forms.TabPage();
            this.dgvCustomerPayments = new System.Windows.Forms.DataGridView();
            this.tpCusSalesItems = new System.Windows.Forms.TabPage();
            this.dgvCustomerItems = new System.Windows.Forms.DataGridView();
            this.tpSupPurchase = new System.Windows.Forms.TabPage();
            this.dgvSupplierPurchase = new System.Windows.Forms.DataGridView();
            this.tpSupPayments = new System.Windows.Forms.TabPage();
            this.dgvSupplierPayments = new System.Windows.Forms.DataGridView();
            this.tpSupStock = new System.Windows.Forms.TabPage();
            this.dgvSupplierItems = new System.Windows.Forms.DataGridView();
            this.tpSupSalesComplaints = new System.Windows.Forms.TabPage();
            this.dgvSupplierSalesComplaints = new System.Windows.Forms.DataGridView();
            this.txtColPIIDPSC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtColGRNIDPSC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtColdDatePSC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtColItemIDPSC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtColItemNamePSC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtColBatchPSC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtColSalesPSC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtColSalesReturnPSC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtColBalanceItemsPSC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtColUOMPSC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtColComplaintsPSC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlExpandVendorHistory.SuspendLayout();
            this.tcVendorHistory.SuspendLayout();
            this.tpGeneralInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContactAddress)).BeginInit();
            this.tpCusSales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustmerSales)).BeginInit();
            this.tpCusPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomerPayments)).BeginInit();
            this.tpCusSalesItems.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomerItems)).BeginInit();
            this.tpSupPurchase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplierPurchase)).BeginInit();
            this.tpSupPayments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplierPayments)).BeginInit();
            this.tpSupStock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplierItems)).BeginInit();
            this.tpSupSalesComplaints.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplierSalesComplaints)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlExpandVendorHistory
            // 
            this.pnlExpandVendorHistory.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlExpandVendorHistory.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlExpandVendorHistory.Controls.Add(this.BtnShow);
            this.pnlExpandVendorHistory.Controls.Add(this.cboVendor);
            this.pnlExpandVendorHistory.Controls.Add(this.CboAssociative);
            this.pnlExpandVendorHistory.Controls.Add(this.lblType);
            this.pnlExpandVendorHistory.Controls.Add(this.txtType);
            this.pnlExpandVendorHistory.Controls.Add(this.lblAssociates);
            this.pnlExpandVendorHistory.Controls.Add(this.cboType);
            this.pnlExpandVendorHistory.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlExpandVendorHistory.Location = new System.Drawing.Point(0, 0);
            this.pnlExpandVendorHistory.Name = "pnlExpandVendorHistory";
            this.pnlExpandVendorHistory.Size = new System.Drawing.Size(766, 68);
            this.pnlExpandVendorHistory.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlExpandVendorHistory.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlExpandVendorHistory.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlExpandVendorHistory.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlExpandVendorHistory.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.pnlExpandVendorHistory.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.pnlExpandVendorHistory.Style.GradientAngle = 90;
            this.pnlExpandVendorHistory.TabIndex = 0;
            this.pnlExpandVendorHistory.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlExpandVendorHistory.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlExpandVendorHistory.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlExpandVendorHistory.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.pnlExpandVendorHistory.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlExpandVendorHistory.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlExpandVendorHistory.TitleStyle.GradientAngle = 90;
            this.pnlExpandVendorHistory.TitleText = "Search";
            this.pnlExpandVendorHistory.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.pnlExpandVendorHistory_ExpandedChanged);
            this.pnlExpandVendorHistory.ExpandedChanging += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.pnlExpandVendorHistory_ExpandedChanging);
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(409, 36);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(75, 20);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 10;
            this.BtnShow.Text = "Show";
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // cboVendor
            // 
            this.cboVendor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboVendor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboVendor.DisplayMember = "Text";
            this.cboVendor.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboVendor.DropDownHeight = 134;
            this.cboVendor.FormattingEnabled = true;
            this.cboVendor.IntegralHeight = false;
            this.cboVendor.ItemHeight = 14;
            this.cboVendor.Location = new System.Drawing.Point(238, 36);
            this.cboVendor.Name = "cboVendor";
            this.cboVendor.Size = new System.Drawing.Size(157, 20);
            this.cboVendor.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboVendor.TabIndex = 9;
            this.cboVendor.TabStop = false;
            this.cboVendor.SelectedIndexChanged += new System.EventHandler(this.cboVendor_SelectedIndexChanged);
            this.cboVendor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboVendor_KeyPress);
            // 
            // CboAssociative
            // 
            this.CboAssociative.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboAssociative.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboAssociative.DisplayMember = "Text";
            this.CboAssociative.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboAssociative.DropDownHeight = 75;
            this.CboAssociative.FormattingEnabled = true;
            this.CboAssociative.IntegralHeight = false;
            this.CboAssociative.ItemHeight = 14;
            this.CboAssociative.Items.AddRange(new object[] {
            this.Customer,
            this.Supplier});
            this.CboAssociative.Location = new System.Drawing.Point(66, 36);
            this.CboAssociative.Name = "CboAssociative";
            this.CboAssociative.Size = new System.Drawing.Size(157, 20);
            this.CboAssociative.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboAssociative.TabIndex = 6;
            this.CboAssociative.TabStop = false;
            this.CboAssociative.SelectionChangeCommitted += new System.EventHandler(this.CboAssociative_SelectionChangeCommitted);
            this.CboAssociative.SelectedIndexChanged += new System.EventHandler(this.CboAssociative_SelectedIndexChanged);
            // 
            // Customer
            // 
            this.Customer.Text = "Customer";
            // 
            // Supplier
            // 
            this.Supplier.Text = "Supplier";
            // 
            // lblType
            // 
            // 
            // 
            // 
            this.lblType.BackgroundStyle.Class = "";
            this.lblType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblType.Location = new System.Drawing.Point(238, 35);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(75, 23);
            this.lblType.TabIndex = 11;
            this.lblType.Text = "Type";
            this.lblType.Visible = false;
            // 
            // txtType
            // 
            this.txtType.AutoCompleteCustomSource.AddRange(new string[] {
            "Country",
            "Location",
            "MobileNumber",
            "TransactionType"});
            this.txtType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtType.Border.Class = "TextBoxBorder";
            this.txtType.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtType.Location = new System.Drawing.Point(426, 36);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(40, 20);
            this.txtType.TabIndex = 9;
            this.txtType.Visible = false;
            // 
            // lblAssociates
            // 
            // 
            // 
            // 
            this.lblAssociates.BackgroundStyle.Class = "";
            this.lblAssociates.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAssociates.Location = new System.Drawing.Point(7, 33);
            this.lblAssociates.Name = "lblAssociates";
            this.lblAssociates.Size = new System.Drawing.Size(75, 23);
            this.lblAssociates.TabIndex = 7;
            this.lblAssociates.Text = "Associates";
            // 
            // cboType
            // 
            this.cboType.DisplayMember = "Text";
            this.cboType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboType.FormattingEnabled = true;
            this.cboType.ItemHeight = 14;
            this.cboType.Items.AddRange(new object[] {
            this.ALL,
            this.Country,
            new System.Drawing.Point(0, 0),
            this.MobileNumber,
            this.TransactionType});
            this.cboType.Location = new System.Drawing.Point(269, 36);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(56, 20);
            this.cboType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboType.TabIndex = 8;
            this.cboType.TabStop = false;
            this.cboType.Visible = false;
            // 
            // ALL
            // 
            this.ALL.Text = "ALL";
            // 
            // Country
            // 
            this.Country.Text = "Country";
            // 
            // MobileNumber
            // 
            this.MobileNumber.Text = "Mobile Number";
            // 
            // TransactionType
            // 
            this.TransactionType.Text = "Transaction Type";
            // 
            // tcVendorHistory
            // 
            this.tcVendorHistory.Controls.Add(this.tpGeneralInfo);
            this.tcVendorHistory.Controls.Add(this.tpCusSales);
            this.tcVendorHistory.Controls.Add(this.tpCusPayment);
            this.tcVendorHistory.Controls.Add(this.tpCusSalesItems);
            this.tcVendorHistory.Controls.Add(this.tpSupPurchase);
            this.tcVendorHistory.Controls.Add(this.tpSupPayments);
            this.tcVendorHistory.Controls.Add(this.tpSupStock);
            this.tcVendorHistory.Controls.Add(this.tpSupSalesComplaints);
            this.tcVendorHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcVendorHistory.Location = new System.Drawing.Point(0, 68);
            this.tcVendorHistory.Name = "tcVendorHistory";
            this.tcVendorHistory.SelectedIndex = 0;
            this.tcVendorHistory.Size = new System.Drawing.Size(766, 443);
            this.tcVendorHistory.TabIndex = 2;
            // 
            // tpGeneralInfo
            // 
            this.tpGeneralInfo.Controls.Add(this.lblVendorStatusValue);
            this.tpGeneralInfo.Controls.Add(this.lblVendorStatus);
            this.tpGeneralInfo.Controls.Add(this.lblDocuments);
            this.tpGeneralInfo.Controls.Add(this.lblAddress);
            this.tpGeneralInfo.Controls.Add(this.lblRatingValue);
            this.tpGeneralInfo.Controls.Add(this.lblRating);
            this.tpGeneralInfo.Controls.Add(this.lblTransactionTypeValue);
            this.tpGeneralInfo.Controls.Add(this.lblTransactionType);
            this.tpGeneralInfo.Controls.Add(this.lblGenderOrSupClassValue);
            this.tpGeneralInfo.Controls.Add(this.lblGenderOrSupClass);
            this.tpGeneralInfo.Controls.Add(this.lblEmailValue);
            this.tpGeneralInfo.Controls.Add(this.lblEmail);
            this.tpGeneralInfo.Controls.Add(this.lblCodeValue);
            this.tpGeneralInfo.Controls.Add(this.lblCode);
            this.tpGeneralInfo.Controls.Add(this.lblNameValue);
            this.tpGeneralInfo.Controls.Add(this.lblName);
            this.tpGeneralInfo.Controls.Add(this.dgvDocuments);
            this.tpGeneralInfo.Controls.Add(this.dgvContactAddress);
            this.tpGeneralInfo.Controls.Add(this.shapeContainer1);
            this.tpGeneralInfo.Location = new System.Drawing.Point(4, 22);
            this.tpGeneralInfo.Name = "tpGeneralInfo";
            this.tpGeneralInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tpGeneralInfo.Size = new System.Drawing.Size(758, 417);
            this.tpGeneralInfo.TabIndex = 0;
            this.tpGeneralInfo.Text = "Personal Info";
            this.tpGeneralInfo.UseVisualStyleBackColor = true;
            // 
            // lblVendorStatusValue
            // 
            this.lblVendorStatusValue.AutoSize = true;
            this.lblVendorStatusValue.Location = new System.Drawing.Point(645, 66);
            this.lblVendorStatusValue.Name = "lblVendorStatusValue";
            this.lblVendorStatusValue.Size = new System.Drawing.Size(10, 13);
            this.lblVendorStatusValue.TabIndex = 18;
            this.lblVendorStatusValue.Text = " ";
            // 
            // lblVendorStatus
            // 
            this.lblVendorStatus.AutoSize = true;
            this.lblVendorStatus.Location = new System.Drawing.Point(593, 66);
            this.lblVendorStatus.Name = "lblVendorStatus";
            this.lblVendorStatus.Size = new System.Drawing.Size(43, 13);
            this.lblVendorStatus.TabIndex = 17;
            this.lblVendorStatus.Text = "Status :";
            // 
            // lblDocuments
            // 
            this.lblDocuments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDocuments.AutoSize = true;
            this.lblDocuments.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocuments.Location = new System.Drawing.Point(6, 257);
            this.lblDocuments.Name = "lblDocuments";
            this.lblDocuments.Size = new System.Drawing.Size(74, 13);
            this.lblDocuments.TabIndex = 15;
            this.lblDocuments.Text = "Documents ";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.Location = new System.Drawing.Point(6, 95);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(104, 13);
            this.lblAddress.TabIndex = 14;
            this.lblAddress.Text = "Contact Address ";
            // 
            // lblRatingValue
            // 
            this.lblRatingValue.AutoSize = true;
            this.lblRatingValue.Location = new System.Drawing.Point(493, 66);
            this.lblRatingValue.Name = "lblRatingValue";
            this.lblRatingValue.Size = new System.Drawing.Size(10, 13);
            this.lblRatingValue.TabIndex = 13;
            this.lblRatingValue.Text = " ";
            // 
            // lblRating
            // 
            this.lblRating.AutoSize = true;
            this.lblRating.Location = new System.Drawing.Point(381, 66);
            this.lblRating.Name = "lblRating";
            this.lblRating.Size = new System.Drawing.Size(44, 13);
            this.lblRating.TabIndex = 12;
            this.lblRating.Text = "Rating :";
            // 
            // lblTransactionTypeValue
            // 
            this.lblTransactionTypeValue.AutoSize = true;
            this.lblTransactionTypeValue.Location = new System.Drawing.Point(493, 37);
            this.lblTransactionTypeValue.Name = "lblTransactionTypeValue";
            this.lblTransactionTypeValue.Size = new System.Drawing.Size(10, 13);
            this.lblTransactionTypeValue.TabIndex = 11;
            this.lblTransactionTypeValue.Text = " ";
            // 
            // lblTransactionType
            // 
            this.lblTransactionType.AutoSize = true;
            this.lblTransactionType.Location = new System.Drawing.Point(381, 37);
            this.lblTransactionType.Name = "lblTransactionType";
            this.lblTransactionType.Size = new System.Drawing.Size(96, 13);
            this.lblTransactionType.TabIndex = 10;
            this.lblTransactionType.Text = "Transaction Type :";
            // 
            // lblGenderOrSupClassValue
            // 
            this.lblGenderOrSupClassValue.AutoSize = true;
            this.lblGenderOrSupClassValue.Location = new System.Drawing.Point(493, 14);
            this.lblGenderOrSupClassValue.Name = "lblGenderOrSupClassValue";
            this.lblGenderOrSupClassValue.Size = new System.Drawing.Size(10, 13);
            this.lblGenderOrSupClassValue.TabIndex = 9;
            this.lblGenderOrSupClassValue.Text = " ";
            // 
            // lblGenderOrSupClass
            // 
            this.lblGenderOrSupClass.AutoSize = true;
            this.lblGenderOrSupClass.Location = new System.Drawing.Point(381, 14);
            this.lblGenderOrSupClass.Name = "lblGenderOrSupClass";
            this.lblGenderOrSupClass.Size = new System.Drawing.Size(48, 13);
            this.lblGenderOrSupClass.TabIndex = 8;
            this.lblGenderOrSupClass.Text = "Gender :";
            // 
            // lblEmailValue
            // 
            this.lblEmailValue.AutoSize = true;
            this.lblEmailValue.Location = new System.Drawing.Point(78, 66);
            this.lblEmailValue.Name = "lblEmailValue";
            this.lblEmailValue.Size = new System.Drawing.Size(10, 13);
            this.lblEmailValue.TabIndex = 7;
            this.lblEmailValue.Text = " ";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(27, 66);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(38, 13);
            this.lblEmail.TabIndex = 6;
            this.lblEmail.Text = "Email :";
            // 
            // lblCodeValue
            // 
            this.lblCodeValue.AutoSize = true;
            this.lblCodeValue.Location = new System.Drawing.Point(78, 37);
            this.lblCodeValue.Name = "lblCodeValue";
            this.lblCodeValue.Size = new System.Drawing.Size(10, 13);
            this.lblCodeValue.TabIndex = 5;
            this.lblCodeValue.Text = " ";
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(27, 37);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(38, 13);
            this.lblCode.TabIndex = 4;
            this.lblCode.Text = "Code :";
            // 
            // lblNameValue
            // 
            this.lblNameValue.AutoSize = true;
            this.lblNameValue.Location = new System.Drawing.Point(78, 14);
            this.lblNameValue.Name = "lblNameValue";
            this.lblNameValue.Size = new System.Drawing.Size(0, 13);
            this.lblNameValue.TabIndex = 3;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(27, 14);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(41, 13);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "Name :";
            // 
            // dgvDocuments
            // 
            this.dgvDocuments.AllowUserToAddRows = false;
            this.dgvDocuments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDocuments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDocuments.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvDocuments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocuments.Location = new System.Drawing.Point(8, 282);
            this.dgvDocuments.Name = "dgvDocuments";
            this.dgvDocuments.ReadOnly = true;
            this.dgvDocuments.RowHeadersVisible = false;
            this.dgvDocuments.Size = new System.Drawing.Size(742, 133);
            this.dgvDocuments.TabIndex = 1;
            // 
            // dgvContactAddress
            // 
            this.dgvContactAddress.AllowUserToAddRows = false;
            this.dgvContactAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvContactAddress.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvContactAddress.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvContactAddress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvContactAddress.Location = new System.Drawing.Point(8, 118);
            this.dgvContactAddress.Name = "dgvContactAddress";
            this.dgvContactAddress.ReadOnly = true;
            this.dgvContactAddress.RowHeadersVisible = false;
            this.dgvContactAddress.Size = new System.Drawing.Size(742, 133);
            this.dgvContactAddress.TabIndex = 0;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(752, 411);
            this.shapeContainer1.TabIndex = 16;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 73;
            this.lineShape2.X2 = 745;
            this.lineShape2.Y1 = 263;
            this.lineShape2.Y2 = 266;
            // 
            // lineShape1
            // 
            this.lineShape1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 101;
            this.lineShape1.X2 = 745;
            this.lineShape1.Y1 = 100;
            this.lineShape1.Y2 = 102;
            // 
            // tpCusSales
            // 
            this.tpCusSales.Controls.Add(this.dgvCustmerSales);
            this.tpCusSales.Location = new System.Drawing.Point(4, 22);
            this.tpCusSales.Name = "tpCusSales";
            this.tpCusSales.Padding = new System.Windows.Forms.Padding(3);
            this.tpCusSales.Size = new System.Drawing.Size(758, 417);
            this.tpCusSales.TabIndex = 1;
            this.tpCusSales.Text = "Sales";
            this.tpCusSales.UseVisualStyleBackColor = true;
            // 
            // dgvCustmerSales
            // 
            this.dgvCustmerSales.AllowUserToAddRows = false;
            this.dgvCustmerSales.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCustmerSales.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvCustmerSales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustmerSales.Location = new System.Drawing.Point(0, 0);
            this.dgvCustmerSales.Name = "dgvCustmerSales";
            this.dgvCustmerSales.ReadOnly = true;
            this.dgvCustmerSales.RowHeadersVisible = false;
            this.dgvCustmerSales.Size = new System.Drawing.Size(755, 414);
            this.dgvCustmerSales.TabIndex = 0;
            // 
            // tpCusPayment
            // 
            this.tpCusPayment.Controls.Add(this.dgvCustomerPayments);
            this.tpCusPayment.Location = new System.Drawing.Point(4, 22);
            this.tpCusPayment.Name = "tpCusPayment";
            this.tpCusPayment.Padding = new System.Windows.Forms.Padding(3);
            this.tpCusPayment.Size = new System.Drawing.Size(758, 417);
            this.tpCusPayment.TabIndex = 2;
            this.tpCusPayment.Text = "Payments";
            this.tpCusPayment.UseVisualStyleBackColor = true;
            // 
            // dgvCustomerPayments
            // 
            this.dgvCustomerPayments.AllowUserToAddRows = false;
            this.dgvCustomerPayments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCustomerPayments.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvCustomerPayments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustomerPayments.Location = new System.Drawing.Point(0, 0);
            this.dgvCustomerPayments.Name = "dgvCustomerPayments";
            this.dgvCustomerPayments.ReadOnly = true;
            this.dgvCustomerPayments.RowHeadersVisible = false;
            this.dgvCustomerPayments.Size = new System.Drawing.Size(755, 414);
            this.dgvCustomerPayments.TabIndex = 1;
            // 
            // tpCusSalesItems
            // 
            this.tpCusSalesItems.Controls.Add(this.dgvCustomerItems);
            this.tpCusSalesItems.Location = new System.Drawing.Point(4, 22);
            this.tpCusSalesItems.Name = "tpCusSalesItems";
            this.tpCusSalesItems.Padding = new System.Windows.Forms.Padding(3);
            this.tpCusSalesItems.Size = new System.Drawing.Size(758, 417);
            this.tpCusSalesItems.TabIndex = 3;
            this.tpCusSalesItems.Text = "Sale Items";
            this.tpCusSalesItems.UseVisualStyleBackColor = true;
            // 
            // dgvCustomerItems
            // 
            this.dgvCustomerItems.AllowUserToAddRows = false;
            this.dgvCustomerItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCustomerItems.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCustomerItems.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvCustomerItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustomerItems.Location = new System.Drawing.Point(0, 0);
            this.dgvCustomerItems.Name = "dgvCustomerItems";
            this.dgvCustomerItems.ReadOnly = true;
            this.dgvCustomerItems.RowHeadersVisible = false;
            this.dgvCustomerItems.Size = new System.Drawing.Size(755, 414);
            this.dgvCustomerItems.TabIndex = 1;
            // 
            // tpSupPurchase
            // 
            this.tpSupPurchase.Controls.Add(this.dgvSupplierPurchase);
            this.tpSupPurchase.Location = new System.Drawing.Point(4, 22);
            this.tpSupPurchase.Name = "tpSupPurchase";
            this.tpSupPurchase.Padding = new System.Windows.Forms.Padding(3);
            this.tpSupPurchase.Size = new System.Drawing.Size(758, 417);
            this.tpSupPurchase.TabIndex = 4;
            this.tpSupPurchase.Text = "Purchase";
            this.tpSupPurchase.UseVisualStyleBackColor = true;
            // 
            // dgvSupplierPurchase
            // 
            this.dgvSupplierPurchase.AllowUserToAddRows = false;
            this.dgvSupplierPurchase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSupplierPurchase.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvSupplierPurchase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSupplierPurchase.Location = new System.Drawing.Point(0, 0);
            this.dgvSupplierPurchase.Name = "dgvSupplierPurchase";
            this.dgvSupplierPurchase.ReadOnly = true;
            this.dgvSupplierPurchase.RowHeadersVisible = false;
            this.dgvSupplierPurchase.Size = new System.Drawing.Size(755, 414);
            this.dgvSupplierPurchase.TabIndex = 1;
            // 
            // tpSupPayments
            // 
            this.tpSupPayments.Controls.Add(this.dgvSupplierPayments);
            this.tpSupPayments.Location = new System.Drawing.Point(4, 22);
            this.tpSupPayments.Name = "tpSupPayments";
            this.tpSupPayments.Padding = new System.Windows.Forms.Padding(3);
            this.tpSupPayments.Size = new System.Drawing.Size(758, 417);
            this.tpSupPayments.TabIndex = 5;
            this.tpSupPayments.Text = "Payments";
            this.tpSupPayments.UseVisualStyleBackColor = true;
            // 
            // dgvSupplierPayments
            // 
            this.dgvSupplierPayments.AllowUserToAddRows = false;
            this.dgvSupplierPayments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSupplierPayments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSupplierPayments.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvSupplierPayments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSupplierPayments.Location = new System.Drawing.Point(0, 0);
            this.dgvSupplierPayments.Name = "dgvSupplierPayments";
            this.dgvSupplierPayments.ReadOnly = true;
            this.dgvSupplierPayments.RowHeadersVisible = false;
            this.dgvSupplierPayments.Size = new System.Drawing.Size(755, 414);
            this.dgvSupplierPayments.TabIndex = 1;
            // 
            // tpSupStock
            // 
            this.tpSupStock.Controls.Add(this.dgvSupplierItems);
            this.tpSupStock.Location = new System.Drawing.Point(4, 22);
            this.tpSupStock.Name = "tpSupStock";
            this.tpSupStock.Padding = new System.Windows.Forms.Padding(3);
            this.tpSupStock.Size = new System.Drawing.Size(758, 417);
            this.tpSupStock.TabIndex = 6;
            this.tpSupStock.Text = "Purchase Items";
            this.tpSupStock.UseVisualStyleBackColor = true;
            // 
            // dgvSupplierItems
            // 
            this.dgvSupplierItems.AllowUserToAddRows = false;
            this.dgvSupplierItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSupplierItems.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSupplierItems.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvSupplierItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSupplierItems.Location = new System.Drawing.Point(0, 0);
            this.dgvSupplierItems.Name = "dgvSupplierItems";
            this.dgvSupplierItems.ReadOnly = true;
            this.dgvSupplierItems.RowHeadersVisible = false;
            this.dgvSupplierItems.Size = new System.Drawing.Size(755, 414);
            this.dgvSupplierItems.TabIndex = 1;
            // 
            // tpSupSalesComplaints
            // 
            this.tpSupSalesComplaints.Controls.Add(this.dgvSupplierSalesComplaints);
            this.tpSupSalesComplaints.Location = new System.Drawing.Point(4, 22);
            this.tpSupSalesComplaints.Name = "tpSupSalesComplaints";
            this.tpSupSalesComplaints.Padding = new System.Windows.Forms.Padding(3);
            this.tpSupSalesComplaints.Size = new System.Drawing.Size(758, 417);
            this.tpSupSalesComplaints.TabIndex = 7;
            this.tpSupSalesComplaints.Text = "Sales and Complaints";
            this.tpSupSalesComplaints.UseVisualStyleBackColor = true;
            // 
            // dgvSupplierSalesComplaints
            // 
            this.dgvSupplierSalesComplaints.AllowUserToAddRows = false;
            this.dgvSupplierSalesComplaints.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSupplierSalesComplaints.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvSupplierSalesComplaints.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSupplierSalesComplaints.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtColPIIDPSC,
            this.txtColGRNIDPSC,
            this.txtColdDatePSC,
            this.txtColItemIDPSC,
            this.txtColItemNamePSC,
            this.txtColBatchPSC,
            this.txtColSalesPSC,
            this.txtColSalesReturnPSC,
            this.txtColBalanceItemsPSC,
            this.txtColUOMPSC,
            this.txtColComplaintsPSC});
            this.dgvSupplierSalesComplaints.Location = new System.Drawing.Point(0, 0);
            this.dgvSupplierSalesComplaints.Name = "dgvSupplierSalesComplaints";
            this.dgvSupplierSalesComplaints.ReadOnly = true;
            this.dgvSupplierSalesComplaints.RowHeadersVisible = false;
            this.dgvSupplierSalesComplaints.Size = new System.Drawing.Size(755, 414);
            this.dgvSupplierSalesComplaints.TabIndex = 1;
            // 
            // txtColPIIDPSC
            // 
            this.txtColPIIDPSC.HeaderText = "PI ID";
            this.txtColPIIDPSC.Name = "txtColPIIDPSC";
            this.txtColPIIDPSC.ReadOnly = true;
            // 
            // txtColGRNIDPSC
            // 
            this.txtColGRNIDPSC.HeaderText = "GRN ID";
            this.txtColGRNIDPSC.Name = "txtColGRNIDPSC";
            this.txtColGRNIDPSC.ReadOnly = true;
            // 
            // txtColdDatePSC
            // 
            this.txtColdDatePSC.HeaderText = "Date";
            this.txtColdDatePSC.Name = "txtColdDatePSC";
            this.txtColdDatePSC.ReadOnly = true;
            // 
            // txtColItemIDPSC
            // 
            this.txtColItemIDPSC.HeaderText = "Item ID";
            this.txtColItemIDPSC.Name = "txtColItemIDPSC";
            this.txtColItemIDPSC.ReadOnly = true;
            // 
            // txtColItemNamePSC
            // 
            this.txtColItemNamePSC.HeaderText = "Item Name";
            this.txtColItemNamePSC.Name = "txtColItemNamePSC";
            this.txtColItemNamePSC.ReadOnly = true;
            // 
            // txtColBatchPSC
            // 
            this.txtColBatchPSC.HeaderText = "Batch";
            this.txtColBatchPSC.Name = "txtColBatchPSC";
            this.txtColBatchPSC.ReadOnly = true;
            // 
            // txtColSalesPSC
            // 
            this.txtColSalesPSC.HeaderText = "Sales";
            this.txtColSalesPSC.Name = "txtColSalesPSC";
            this.txtColSalesPSC.ReadOnly = true;
            // 
            // txtColSalesReturnPSC
            // 
            this.txtColSalesReturnPSC.HeaderText = "Sales Return";
            this.txtColSalesReturnPSC.Name = "txtColSalesReturnPSC";
            this.txtColSalesReturnPSC.ReadOnly = true;
            // 
            // txtColBalanceItemsPSC
            // 
            this.txtColBalanceItemsPSC.HeaderText = "Balance Items";
            this.txtColBalanceItemsPSC.Name = "txtColBalanceItemsPSC";
            this.txtColBalanceItemsPSC.ReadOnly = true;
            // 
            // txtColUOMPSC
            // 
            this.txtColUOMPSC.HeaderText = "UOM";
            this.txtColUOMPSC.Name = "txtColUOMPSC";
            this.txtColUOMPSC.ReadOnly = true;
            // 
            // txtColComplaintsPSC
            // 
            this.txtColComplaintsPSC.HeaderText = "Complaints";
            this.txtColComplaintsPSC.Name = "txtColComplaintsPSC";
            this.txtColComplaintsPSC.ReadOnly = true;
            // 
            // FrmVendorHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 511);
            this.Controls.Add(this.tcVendorHistory);
            this.Controls.Add(this.pnlExpandVendorHistory);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmVendorHistory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vendor History";
            this.Load += new System.EventHandler(this.FrmVendorHistory_Load);
            this.pnlExpandVendorHistory.ResumeLayout(false);
            this.tcVendorHistory.ResumeLayout(false);
            this.tpGeneralInfo.ResumeLayout(false);
            this.tpGeneralInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContactAddress)).EndInit();
            this.tpCusSales.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustmerSales)).EndInit();
            this.tpCusPayment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomerPayments)).EndInit();
            this.tpCusSalesItems.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomerItems)).EndInit();
            this.tpSupPurchase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplierPurchase)).EndInit();
            this.tpSupPayments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplierPayments)).EndInit();
            this.tpSupStock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplierItems)).EndInit();
            this.tpSupSalesComplaints.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplierSalesComplaints)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcVendorHistory;
        private System.Windows.Forms.TabPage tpGeneralInfo;
        private System.Windows.Forms.TabPage tpCusSales;
        private System.Windows.Forms.TabPage tpCusPayment;
        private System.Windows.Forms.TabPage tpCusSalesItems;
        private System.Windows.Forms.TabPage tpSupPurchase;
        private System.Windows.Forms.TabPage tpSupPayments;
        private System.Windows.Forms.TabPage tpSupStock;
        private System.Windows.Forms.TabPage tpSupSalesComplaints;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboType;
        private DevComponents.Editors.ComboItem ALL;
        private DevComponents.Editors.ComboItem Country;
        private DevComponents.Editors.ComboItem MobileNumber;
        private DevComponents.Editors.ComboItem TransactionType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboAssociative;
        private DevComponents.Editors.ComboItem Supplier;
        private DevComponents.Editors.ComboItem Customer;
        private DevComponents.DotNetBar.LabelX lblType;
        public DevComponents.DotNetBar.Controls.TextBoxX txtType;
        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DevComponents.DotNetBar.LabelX lblAssociates;
        private System.Windows.Forms.DataGridView dgvContactAddress;
        private System.Windows.Forms.DataGridView dgvDocuments;
        private System.Windows.Forms.Label lblNameValue;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblRatingValue;
        private System.Windows.Forms.Label lblRating;
        private System.Windows.Forms.Label lblTransactionTypeValue;
        private System.Windows.Forms.Label lblTransactionType;
        private System.Windows.Forms.Label lblGenderOrSupClassValue;
        private System.Windows.Forms.Label lblGenderOrSupClass;
        private System.Windows.Forms.Label lblEmailValue;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblCodeValue;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Label lblDocuments;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.DataGridView dgvCustmerSales;
        private System.Windows.Forms.DataGridView dgvCustomerPayments;
        private System.Windows.Forms.DataGridView dgvCustomerItems;
        private System.Windows.Forms.DataGridView dgvSupplierPurchase;
        private System.Windows.Forms.DataGridView dgvSupplierPayments;
        private System.Windows.Forms.DataGridView dgvSupplierItems;
        private System.Windows.Forms.DataGridView dgvSupplierSalesComplaints;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtColPIIDPSC;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtColGRNIDPSC;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtColdDatePSC;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtColItemIDPSC;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtColItemNamePSC;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtColBatchPSC;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtColSalesPSC;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtColSalesReturnPSC;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtColBalanceItemsPSC;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtColUOMPSC;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtColComplaintsPSC;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboVendor;
        private DevComponents.DotNetBar.ExpandablePanel pnlExpandVendorHistory;
        private System.Windows.Forms.Label lblVendorStatusValue;
        private System.Windows.Forms.Label lblVendorStatus;
    }
}