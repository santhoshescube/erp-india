﻿namespace MyBooksERP
{
    partial class FrmCurrencyReference
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label ScaleLabel;
            System.Windows.Forms.Label CountryIDLabel;
            System.Windows.Forms.Label ShortDescriptionLabel;
            System.Windows.Forms.Label DescriptionLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCurrencyReference));
            this.CurrencyReferenceBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.CurrencyRefBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BtnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnSave = new System.Windows.Forms.Button();
            this.DgvCurrencyRef = new System.Windows.Forms.DataGridView();
            this.TxtDecimalPlace = new System.Windows.Forms.TextBox();
            this.BtnCountry = new System.Windows.Forms.Button();
            this.CboCountry = new System.Windows.Forms.ComboBox();
            this.TxtShortName = new System.Windows.Forms.TextBox();
            this.TxtCurrency = new System.Windows.Forms.TextBox();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.TmrCurrency = new System.Windows.Forms.Timer(this.components);
            this.TmrFocus = new System.Windows.Forms.Timer(this.components);
            this.ErrCurrency = new System.Windows.Forms.ErrorProvider(this.components);
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.lblcurrefstatus = new DevComponents.DotNetBar.LabelItem();
            ScaleLabel = new System.Windows.Forms.Label();
            CountryIDLabel = new System.Windows.Forms.Label();
            ShortDescriptionLabel = new System.Windows.Forms.Label();
            DescriptionLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.CurrencyReferenceBindingNavigator)).BeginInit();
            this.CurrencyReferenceBindingNavigator.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvCurrencyRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // ScaleLabel
            // 
            ScaleLabel.AutoSize = true;
            ScaleLabel.Location = new System.Drawing.Point(9, 93);
            ScaleLabel.Name = "ScaleLabel";
            ScaleLabel.Size = new System.Drawing.Size(75, 13);
            ScaleLabel.TabIndex = 58;
            ScaleLabel.Text = "Decimal Place";
            // 
            // CountryIDLabel
            // 
            CountryIDLabel.AutoSize = true;
            CountryIDLabel.Location = new System.Drawing.Point(9, 68);
            CountryIDLabel.Name = "CountryIDLabel";
            CountryIDLabel.Size = new System.Drawing.Size(43, 13);
            CountryIDLabel.TabIndex = 57;
            CountryIDLabel.Text = "Country";
            // 
            // ShortDescriptionLabel
            // 
            ShortDescriptionLabel.AutoSize = true;
            ShortDescriptionLabel.Location = new System.Drawing.Point(9, 43);
            ShortDescriptionLabel.Name = "ShortDescriptionLabel";
            ShortDescriptionLabel.Size = new System.Drawing.Size(63, 13);
            ShortDescriptionLabel.TabIndex = 56;
            ShortDescriptionLabel.Text = "Short Name";
            // 
            // DescriptionLabel
            // 
            DescriptionLabel.AutoSize = true;
            DescriptionLabel.Location = new System.Drawing.Point(9, 18);
            DescriptionLabel.Name = "DescriptionLabel";
            DescriptionLabel.Size = new System.Drawing.Size(49, 13);
            DescriptionLabel.TabIndex = 55;
            DescriptionLabel.Text = "Currency";
            // 
            // CurrencyReferenceBindingNavigator
            // 
            this.CurrencyReferenceBindingNavigator.AddNewItem = null;
            this.CurrencyReferenceBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.CurrencyReferenceBindingNavigator.CountItem = null;
            this.CurrencyReferenceBindingNavigator.DeleteItem = null;
            this.CurrencyReferenceBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorAddNewItem,
            this.CurrencyRefBindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BtnClear,
            this.ToolStripSeparator1,
            this.BtnPrint,
            this.BtnEmail,
            this.BtnHelp});
            this.CurrencyReferenceBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.CurrencyReferenceBindingNavigator.MoveFirstItem = null;
            this.CurrencyReferenceBindingNavigator.MoveLastItem = null;
            this.CurrencyReferenceBindingNavigator.MoveNextItem = null;
            this.CurrencyReferenceBindingNavigator.MovePreviousItem = null;
            this.CurrencyReferenceBindingNavigator.Name = "CurrencyReferenceBindingNavigator";
            this.CurrencyReferenceBindingNavigator.PositionItem = null;
            this.CurrencyReferenceBindingNavigator.Size = new System.Drawing.Size(442, 25);
            this.CurrencyReferenceBindingNavigator.TabIndex = 2;
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add ";
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New Currency";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // CurrencyRefBindingNavigatorSaveItem
            // 
            this.CurrencyRefBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CurrencyRefBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("CurrencyRefBindingNavigatorSaveItem.Image")));
            this.CurrencyRefBindingNavigatorSaveItem.Name = "CurrencyRefBindingNavigatorSaveItem";
            this.CurrencyRefBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.CurrencyRefBindingNavigatorSaveItem.Text = "Save";
            this.CurrencyRefBindingNavigatorSaveItem.Click += new System.EventHandler(this.CurrencyRefBindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Remove";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BtnClear
            // 
            this.BtnClear.Image = ((System.Drawing.Image)(resources.GetObject("BtnClear.Image")));
            this.BtnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnClear.Name = "BtnClear";
            this.BtnClear.Size = new System.Drawing.Size(54, 22);
            this.BtnClear.Text = "Clear";
            this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "&Print";
            this.BtnPrint.Visible = false;
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "&Email";
            this.BtnEmail.Visible = false;
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.BtnSave);
            this.panel1.Controls.Add(this.DgvCurrencyRef);
            this.panel1.Controls.Add(this.TxtDecimalPlace);
            this.panel1.Controls.Add(this.BtnCountry);
            this.panel1.Controls.Add(this.CboCountry);
            this.panel1.Controls.Add(ScaleLabel);
            this.panel1.Controls.Add(CountryIDLabel);
            this.panel1.Controls.Add(ShortDescriptionLabel);
            this.panel1.Controls.Add(this.TxtShortName);
            this.panel1.Controls.Add(DescriptionLabel);
            this.panel1.Controls.Add(this.TxtCurrency);
            this.panel1.Location = new System.Drawing.Point(6, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(429, 310);
            this.panel1.TabIndex = 3;
            // 
            // BtnSave
            // 
            this.BtnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSave.Location = new System.Drawing.Point(349, 93);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 5;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // DgvCurrencyRef
            // 
            this.DgvCurrencyRef.AllowUserToAddRows = false;
            this.DgvCurrencyRef.AllowUserToDeleteRows = false;
            this.DgvCurrencyRef.AllowUserToResizeColumns = false;
            this.DgvCurrencyRef.AllowUserToResizeRows = false;
            this.DgvCurrencyRef.BackgroundColor = System.Drawing.Color.White;
            this.DgvCurrencyRef.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvCurrencyRef.Location = new System.Drawing.Point(3, 122);
            this.DgvCurrencyRef.MultiSelect = false;
            this.DgvCurrencyRef.Name = "DgvCurrencyRef";
            this.DgvCurrencyRef.ReadOnly = true;
            this.DgvCurrencyRef.RowHeadersVisible = false;
            this.DgvCurrencyRef.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvCurrencyRef.Size = new System.Drawing.Size(421, 180);
            this.DgvCurrencyRef.TabIndex = 6;
            this.DgvCurrencyRef.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvCurrencyRef_CellClick);
            this.DgvCurrencyRef.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DgvCurrencyRef_KeyDown);
            this.DgvCurrencyRef.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DgvCurrencyRef_KeyUp);
            // 
            // TxtDecimalPlace
            // 
            this.TxtDecimalPlace.BackColor = System.Drawing.SystemColors.Info;
            this.TxtDecimalPlace.Location = new System.Drawing.Point(101, 92);
            this.TxtDecimalPlace.MaxLength = 1;
            this.TxtDecimalPlace.Name = "TxtDecimalPlace";
            this.TxtDecimalPlace.Size = new System.Drawing.Size(82, 20);
            this.TxtDecimalPlace.TabIndex = 4;
            this.TxtDecimalPlace.TextChanged += new System.EventHandler(this.TxtDecimalPlace_TextChanged);
            this.TxtDecimalPlace.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtDecimalPlace_KeyPress);
            // 
            // BtnCountry
            // 
            this.BtnCountry.Location = new System.Drawing.Point(398, 64);
            this.BtnCountry.Name = "BtnCountry";
            this.BtnCountry.Size = new System.Drawing.Size(26, 23);
            this.BtnCountry.TabIndex = 3;
            this.BtnCountry.Text = "...";
            this.BtnCountry.UseVisualStyleBackColor = true;
            this.BtnCountry.Click += new System.EventHandler(this.BtnCountry_Click);
            // 
            // CboCountry
            // 
            this.CboCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCountry.BackColor = System.Drawing.SystemColors.Info;
            this.CboCountry.DropDownHeight = 75;
            this.CboCountry.FormattingEnabled = true;
            this.CboCountry.IntegralHeight = false;
            this.CboCountry.Location = new System.Drawing.Point(101, 66);
            this.CboCountry.Name = "CboCountry";
            this.CboCountry.Size = new System.Drawing.Size(291, 21);
            this.CboCountry.TabIndex = 2;
            this.CboCountry.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.CboCountry.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboCountry_KeyDown);
            // 
            // TxtShortName
            // 
            this.TxtShortName.BackColor = System.Drawing.SystemColors.Info;
            this.TxtShortName.Location = new System.Drawing.Point(101, 41);
            this.TxtShortName.MaxLength = 5;
            this.TxtShortName.Name = "TxtShortName";
            this.TxtShortName.Size = new System.Drawing.Size(82, 20);
            this.TxtShortName.TabIndex = 1;
            this.TxtShortName.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // TxtCurrency
            // 
            this.TxtCurrency.BackColor = System.Drawing.SystemColors.Info;
            this.TxtCurrency.Location = new System.Drawing.Point(101, 16);
            this.TxtCurrency.MaxLength = 40;
            this.TxtCurrency.Name = "TxtCurrency";
            this.TxtCurrency.Size = new System.Drawing.Size(323, 20);
            this.TxtCurrency.TabIndex = 0;
            this.TxtCurrency.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnCancel.Location = new System.Drawing.Point(360, 355);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 8;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnOk.Enabled = false;
            this.BtnOk.Location = new System.Drawing.Point(279, 355);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 7;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // TmrCurrency
            // 
            this.TmrCurrency.Interval = 2000;
            // 
            // TmrFocus
            // 
            this.TmrFocus.Tick += new System.EventHandler(this.TmrFocus_Tick);
            // 
            // ErrCurrency
            // 
            this.ErrCurrency.ContainerControl = this;
            this.ErrCurrency.RightToLeft = true;
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.lblcurrefstatus});
            this.bar1.Location = new System.Drawing.Point(0, 384);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(442, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 18;
            this.bar1.TabStop = false;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "Status:";
            // 
            // lblcurrefstatus
            // 
            this.lblcurrefstatus.Name = "lblcurrefstatus";
            // 
            // FrmCurrencyReference
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 403);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.CurrencyReferenceBindingNavigator);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCurrencyReference";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Currency";
            this.Load += new System.EventHandler(this.FrmCurrencyReference_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCurrencyReference_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmCurrencyReference_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.CurrencyReferenceBindingNavigator)).EndInit();
            this.CurrencyReferenceBindingNavigator.ResumeLayout(false);
            this.CurrencyReferenceBindingNavigator.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvCurrencyRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator CurrencyReferenceBindingNavigator;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton CurrencyRefBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BtnClear;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnOk;
        internal System.Windows.Forms.DataGridView DgvCurrencyRef;
        internal System.Windows.Forms.TextBox TxtDecimalPlace;
        internal System.Windows.Forms.Button BtnCountry;
        internal System.Windows.Forms.ComboBox CboCountry;
        internal System.Windows.Forms.TextBox TxtShortName;
        internal System.Windows.Forms.TextBox TxtCurrency;
        internal System.Windows.Forms.Timer TmrCurrency;
        internal System.Windows.Forms.Timer TmrFocus;
        internal System.Windows.Forms.ErrorProvider ErrCurrency;
        private System.Windows.Forms.ToolStripButton BtnPrint;
        private System.Windows.Forms.ToolStripButton BtnEmail;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.LabelItem lblcurrefstatus;
    }
}