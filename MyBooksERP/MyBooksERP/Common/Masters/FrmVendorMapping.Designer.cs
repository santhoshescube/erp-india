﻿namespace MyBooksERP
{
    partial class FrmVendorMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVendorMapping));
            this.SStripVendor = new System.Windows.Forms.StatusStrip();
            this.LblVendorStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.BnSearch = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.TxtSearchText = new System.Windows.Forms.ToolStripTextBox();
            this.CboSearchOption = new System.Windows.Forms.ToolStripComboBox();
            this.BtnSearch = new System.Windows.Forms.ToolStripButton();
            this.dgvVendor = new System.Windows.Forms.DataGridView();
            this.VendorID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VendorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MobileNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Associate = new DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn();
            this.Remove = new DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn();
            this.TmVendor = new System.Windows.Forms.Timer(this.components);
            this.SStripVendor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BnSearch)).BeginInit();
            this.BnSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendor)).BeginInit();
            this.SuspendLayout();
            // 
            // SStripVendor
            // 
            this.SStripVendor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblVendorStatus});
            this.SStripVendor.Location = new System.Drawing.Point(0, 402);
            this.SStripVendor.Name = "SStripVendor";
            this.SStripVendor.Size = new System.Drawing.Size(694, 22);
            this.SStripVendor.TabIndex = 6;
            this.SStripVendor.Text = "statusStrip1";
            // 
            // LblVendorStatus
            // 
            this.LblVendorStatus.Name = "LblVendorStatus";
            this.LblVendorStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // BnSearch
            // 
            this.BnSearch.AddNewItem = null;
            this.BnSearch.BackColor = System.Drawing.Color.Transparent;
            this.BnSearch.CountItem = null;
            this.BnSearch.DeleteItem = null;
            this.BnSearch.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.CboSearchOption,
            this.TxtSearchText,
            this.BtnSearch});
            this.BnSearch.Location = new System.Drawing.Point(0, 0);
            this.BnSearch.MoveFirstItem = null;
            this.BnSearch.MoveLastItem = null;
            this.BnSearch.MoveNextItem = null;
            this.BnSearch.MovePreviousItem = null;
            this.BnSearch.Name = "BnSearch";
            this.BnSearch.PositionItem = null;
            this.BnSearch.Size = new System.Drawing.Size(694, 25);
            this.BnSearch.TabIndex = 7;
            this.BnSearch.Text = "bindingNavigator1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(55, 22);
            this.toolStripLabel1.Text = "Search In";
            // 
            // TxtSearchText
            // 
            this.TxtSearchText.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.TxtSearchText.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.TxtSearchText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtSearchText.MaxLength = 25;
            this.TxtSearchText.Name = "TxtSearchText";
            this.TxtSearchText.Size = new System.Drawing.Size(150, 25);
            this.TxtSearchText.ToolTipText = "Enter the search text";
            // 
            // CboSearchOption
            // 
            this.CboSearchOption.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSearchOption.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSearchOption.DropDownHeight = 75;
            this.CboSearchOption.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.CboSearchOption.IntegralHeight = false;
            this.CboSearchOption.Items.AddRange(new object[] {
            "Code",
            "Email",
            "Fax No",
            "IBAN/Account No",
            "Mobile No",
            "Name",
            "Telephone No",
            "Website"});
            this.CboSearchOption.Margin = new System.Windows.Forms.Padding(0, 1, 1, 0);
            this.CboSearchOption.Name = "CboSearchOption";
            this.CboSearchOption.Size = new System.Drawing.Size(140, 24);
            this.CboSearchOption.Sorted = true;
            this.CboSearchOption.ToolTipText = "Select a search option";
            this.CboSearchOption.SelectedIndexChanged += new System.EventHandler(this.CboSearchOption_SelectedIndexChanged);
            // 
            // BtnSearch
            // 
            this.BtnSearch.Image = global::MyBooksERP.Properties.Resources.Search;
            this.BtnSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnSearch.Name = "BtnSearch";
            this.BtnSearch.Size = new System.Drawing.Size(62, 22);
            this.BtnSearch.Text = "Search";
            this.BtnSearch.ToolTipText = "Search";
            this.BtnSearch.Click += new System.EventHandler(this.BtnSearch_Click);
            // 
            // dgvVendor
            // 
            this.dgvVendor.AllowUserToAddRows = false;
            this.dgvVendor.AllowUserToDeleteRows = false;
            this.dgvVendor.AllowUserToResizeColumns = false;
            this.dgvVendor.AllowUserToResizeRows = false;
            this.dgvVendor.BackgroundColor = System.Drawing.Color.White;
            this.dgvVendor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVendor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VendorID,
            this.VendorName,
            this.CompanyID,
            this.CompanyName,
            this.MobileNo,
            this.Associate,
            this.Remove});
            this.dgvVendor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvVendor.Location = new System.Drawing.Point(0, 25);
            this.dgvVendor.Name = "dgvVendor";
            this.dgvVendor.RowHeadersWidth = 60;
            this.dgvVendor.Size = new System.Drawing.Size(694, 377);
            this.dgvVendor.TabIndex = 8;
            this.dgvVendor.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVendor_CellClick);
            // 
            // VendorID
            // 
            this.VendorID.DataPropertyName = "VendorID";
            this.VendorID.HeaderText = "VendorID";
            this.VendorID.Name = "VendorID";
            this.VendorID.ReadOnly = true;
            this.VendorID.Visible = false;
            // 
            // VendorName
            // 
            this.VendorName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.VendorName.DataPropertyName = "VendorName";
            this.VendorName.HeaderText = "Vendor";
            this.VendorName.Name = "VendorName";
            this.VendorName.ReadOnly = true;
            // 
            // CompanyID
            // 
            this.CompanyID.DataPropertyName = "CompanyID";
            this.CompanyID.HeaderText = "CompanyID";
            this.CompanyID.Name = "CompanyID";
            this.CompanyID.ReadOnly = true;
            this.CompanyID.Visible = false;
            // 
            // CompanyName
            // 
            this.CompanyName.DataPropertyName = "CompanyName";
            this.CompanyName.HeaderText = "Company";
            this.CompanyName.Name = "CompanyName";
            this.CompanyName.ReadOnly = true;
            this.CompanyName.Width = 150;
            // 
            // MobileNo
            // 
            this.MobileNo.DataPropertyName = "MobileNo";
            this.MobileNo.HeaderText = "Mobile No";
            this.MobileNo.Name = "MobileNo";
            this.MobileNo.ReadOnly = true;
            // 
            // Associate
            // 
            this.Associate.DataPropertyName = "Associate";
            this.Associate.HeaderText = "Associate";
            this.Associate.Image = ((System.Drawing.Image)(resources.GetObject("Associate.Image")));
            this.Associate.Name = "Associate";
            this.Associate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Associate.Text = null;
            this.Associate.Width = 60;
            // 
            // Remove
            // 
            this.Remove.DataPropertyName = "Remove";
            this.Remove.HeaderText = "Remove";
            this.Remove.Image = ((System.Drawing.Image)(resources.GetObject("Remove.Image")));
            this.Remove.Name = "Remove";
            this.Remove.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Remove.Text = null;
            this.Remove.Width = 60;
            // 
            // FrmVendorMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 424);
            this.Controls.Add(this.dgvVendor);
            this.Controls.Add(this.BnSearch);
            this.Controls.Add(this.SStripVendor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmVendorMapping";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vendor Mapping";
            this.Load += new System.EventHandler(this.FrmVendorMapping_Load);
            this.SStripVendor.ResumeLayout(false);
            this.SStripVendor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BnSearch)).EndInit();
            this.BnSearch.ResumeLayout(false);
            this.BnSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip SStripVendor;
        private System.Windows.Forms.ToolStripStatusLabel LblVendorStatus;
        private System.Windows.Forms.BindingNavigator BnSearch;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox CboSearchOption;
        private System.Windows.Forms.ToolStripButton BtnSearch;
        private System.Windows.Forms.DataGridView dgvVendor;
        private System.Windows.Forms.Timer TmVendor;
        private System.Windows.Forms.DataGridViewTextBoxColumn VendorID;
        private System.Windows.Forms.DataGridViewTextBoxColumn VendorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MobileNo;
        private DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn Associate;
        private DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn Remove;
        internal System.Windows.Forms.ToolStripTextBox TxtSearchText;
    }
}