﻿namespace MyBooksERP
{
    partial class FrmCurrency
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label1;
            System.Windows.Forms.Label CurrencyIDLabel;
            System.Windows.Forms.Label ExchangeDateLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCurrency));
            this.CurrencyBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.pnlCurrency = new System.Windows.Forms.Panel();
            this.lblESelectedCurrency = new System.Windows.Forms.Label();
            this.lblECompCrncy = new System.Windows.Forms.Label();
            this.txtExchangeRate = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.DgvCurrency = new System.Windows.Forms.DataGridView();
            this.CboCompany = new System.Windows.Forms.ComboBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.LblGetExchangeRate = new System.Windows.Forms.LinkLabel();
            this.dtpExchangeDate = new System.Windows.Forms.DateTimePicker();
            this.BtnSave = new System.Windows.Forms.Button();
            this.TxtDefaultCurrency = new System.Windows.Forms.TextBox();
            this.BtnCurrency = new System.Windows.Forms.Button();
            this.CboCurrencyType = new System.Windows.Forms.ComboBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.TmCurrency = new System.Windows.Forms.Timer(this.components);
            this.ErrCurrency = new System.Windows.Forms.ErrorProvider(this.components);
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.lblCurrencyStatus = new DevComponents.DotNetBar.LabelItem();
            Label1 = new System.Windows.Forms.Label();
            CurrencyIDLabel = new System.Windows.Forms.Label();
            ExchangeDateLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.CurrencyBindingNavigator)).BeginInit();
            this.CurrencyBindingNavigator.SuspendLayout();
            this.pnlCurrency.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new System.Drawing.Point(11, 39);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(96, 13);
            Label1.TabIndex = 77;
            Label1.Text = "Company Currency";
            // 
            // CurrencyIDLabel
            // 
            CurrencyIDLabel.AutoSize = true;
            CurrencyIDLabel.Location = new System.Drawing.Point(11, 66);
            CurrencyIDLabel.Name = "CurrencyIDLabel";
            CurrencyIDLabel.Size = new System.Drawing.Size(52, 13);
            CurrencyIDLabel.TabIndex = 74;
            CurrencyIDLabel.Text = "Currency ";
            // 
            // ExchangeDateLabel
            // 
            ExchangeDateLabel.AutoSize = true;
            ExchangeDateLabel.Location = new System.Drawing.Point(11, 119);
            ExchangeDateLabel.Name = "ExchangeDateLabel";
            ExchangeDateLabel.Size = new System.Drawing.Size(81, 13);
            ExchangeDateLabel.TabIndex = 76;
            ExchangeDateLabel.Text = "Exchange Date";
            // 
            // CurrencyBindingNavigator
            // 
            this.CurrencyBindingNavigator.AddNewItem = this.BindingNavigatorAddNewItem;
            this.CurrencyBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.CurrencyBindingNavigator.CountItem = null;
            this.CurrencyBindingNavigator.DeleteItem = null;
            this.CurrencyBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.btnClear,
            this.toolStripSeparator4,
            this.btnPrint,
            this.btnEmail,
            this.toolStripSeparator3,
            this.BtnHelp});
            this.CurrencyBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.CurrencyBindingNavigator.MoveFirstItem = null;
            this.CurrencyBindingNavigator.MoveLastItem = null;
            this.CurrencyBindingNavigator.MoveNextItem = null;
            this.CurrencyBindingNavigator.MovePreviousItem = null;
            this.CurrencyBindingNavigator.Name = "CurrencyBindingNavigator";
            this.CurrencyBindingNavigator.PositionItem = null;
            this.CurrencyBindingNavigator.Size = new System.Drawing.Size(445, 25);
            this.CurrencyBindingNavigator.TabIndex = 4;
            this.CurrencyBindingNavigator.Text = "bindingNavigator1";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.ToolTipText = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.ToolTipText = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "toolStripButton1";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click_1);
            // 
            // pnlCurrency
            // 
            this.pnlCurrency.Controls.Add(this.lblESelectedCurrency);
            this.pnlCurrency.Controls.Add(this.lblECompCrncy);
            this.pnlCurrency.Controls.Add(this.txtExchangeRate);
            this.pnlCurrency.Controls.Add(this.Label3);
            this.pnlCurrency.Controls.Add(this.DgvCurrency);
            this.pnlCurrency.Controls.Add(this.CboCompany);
            this.pnlCurrency.Controls.Add(this.Label2);
            this.pnlCurrency.Controls.Add(this.LblGetExchangeRate);
            this.pnlCurrency.Controls.Add(this.dtpExchangeDate);
            this.pnlCurrency.Controls.Add(this.BtnSave);
            this.pnlCurrency.Controls.Add(this.TxtDefaultCurrency);
            this.pnlCurrency.Controls.Add(Label1);
            this.pnlCurrency.Controls.Add(this.BtnCurrency);
            this.pnlCurrency.Controls.Add(this.CboCurrencyType);
            this.pnlCurrency.Controls.Add(CurrencyIDLabel);
            this.pnlCurrency.Controls.Add(ExchangeDateLabel);
            this.pnlCurrency.Controls.Add(this.shapeContainer1);
            this.pnlCurrency.Location = new System.Drawing.Point(0, 28);
            this.pnlCurrency.Name = "pnlCurrency";
            this.pnlCurrency.Size = new System.Drawing.Size(445, 321);
            this.pnlCurrency.TabIndex = 2;
            // 
            // lblESelectedCurrency
            // 
            this.lblESelectedCurrency.AutoSize = true;
            this.lblESelectedCurrency.Location = new System.Drawing.Point(11, 93);
            this.lblESelectedCurrency.Name = "lblESelectedCurrency";
            this.lblESelectedCurrency.Size = new System.Drawing.Size(83, 13);
            this.lblESelectedCurrency.TabIndex = 1004;
            this.lblESelectedCurrency.Text = "ESelectedCrncy";
            // 
            // lblECompCrncy
            // 
            this.lblECompCrncy.AutoSize = true;
            this.lblECompCrncy.Location = new System.Drawing.Point(180, 93);
            this.lblECompCrncy.Name = "lblECompCrncy";
            this.lblECompCrncy.Size = new System.Drawing.Size(48, 13);
            this.lblECompCrncy.TabIndex = 1003;
            this.lblECompCrncy.Text = "ECCrncy";
            // 
            // txtExchangeRate
            // 
            this.txtExchangeRate.BackColor = System.Drawing.SystemColors.Info;
            this.txtExchangeRate.Location = new System.Drawing.Point(110, 90);
            this.txtExchangeRate.MaxLength = 8;
            this.txtExchangeRate.Name = "txtExchangeRate";
            this.txtExchangeRate.Size = new System.Drawing.Size(64, 20);
            this.txtExchangeRate.TabIndex = 5;
            this.txtExchangeRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtExchangeRate.TextChanged += new System.EventHandler(this.txtExchangeRate_TextChanged);
            this.txtExchangeRate.Leave += new System.EventHandler(this.txtExchangeRate_Leave);
            this.txtExchangeRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtExchangeRate_KeyPress);
            this.txtExchangeRate.Enter += new System.EventHandler(this.txtExchangeRate_Enter);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Label3.Location = new System.Drawing.Point(9, 140);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(100, 13);
            this.Label3.TabIndex = 1002;
            this.Label3.Text = "Currency Details";
            // 
            // DgvCurrency
            // 
            this.DgvCurrency.AllowUserToAddRows = false;
            this.DgvCurrency.AllowUserToDeleteRows = false;
            this.DgvCurrency.AllowUserToResizeColumns = false;
            this.DgvCurrency.AllowUserToResizeRows = false;
            this.DgvCurrency.BackgroundColor = System.Drawing.Color.White;
            this.DgvCurrency.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvCurrency.Location = new System.Drawing.Point(14, 158);
            this.DgvCurrency.MultiSelect = false;
            this.DgvCurrency.Name = "DgvCurrency";
            this.DgvCurrency.ReadOnly = true;
            this.DgvCurrency.RowHeadersWidth = 50;
            this.DgvCurrency.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvCurrency.Size = new System.Drawing.Size(419, 158);
            this.DgvCurrency.TabIndex = 11;
            this.DgvCurrency.TabStop = false;
            this.DgvCurrency.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DgvCurrency_RowsAdded);
            this.DgvCurrency.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvCurrency_CellClick);
            this.DgvCurrency.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DgvCurrency_KeyDown);
            this.DgvCurrency.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.DgvCurrency_RowsRemoved);
            this.DgvCurrency.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DgvCurrency_KeyUp);
            // 
            // CboCompany
            // 
            this.CboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompany.BackColor = System.Drawing.SystemColors.Info;
            this.CboCompany.DropDownHeight = 75;
            this.CboCompany.FormattingEnabled = true;
            this.CboCompany.IntegralHeight = false;
            this.CboCompany.Location = new System.Drawing.Point(110, 9);
            this.CboCompany.Name = "CboCompany";
            this.CboCompany.Size = new System.Drawing.Size(294, 21);
            this.CboCompany.TabIndex = 1;
            this.CboCompany.SelectedIndexChanged += new System.EventHandler(this.CboCompany_SelectedIndexChanged);
            this.CboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(11, 12);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(51, 13);
            this.Label2.TabIndex = 78;
            this.Label2.Text = "Company";
            // 
            // LblGetExchangeRate
            // 
            this.LblGetExchangeRate.AutoSize = true;
            this.LblGetExchangeRate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LblGetExchangeRate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblGetExchangeRate.Location = new System.Drawing.Point(244, 93);
            this.LblGetExchangeRate.Name = "LblGetExchangeRate";
            this.LblGetExchangeRate.Size = new System.Drawing.Size(161, 13);
            this.LblGetExchangeRate.TabIndex = 6;
            this.LblGetExchangeRate.TabStop = true;
            this.LblGetExchangeRate.Text = "Click here to get Exchange Rate";
            this.LblGetExchangeRate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LblGetExchangeRate_LinkClicked);
            // 
            // dtpExchangeDate
            // 
            this.dtpExchangeDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpExchangeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExchangeDate.Location = new System.Drawing.Point(110, 116);
            this.dtpExchangeDate.Name = "dtpExchangeDate";
            this.dtpExchangeDate.Size = new System.Drawing.Size(102, 20);
            this.dtpExchangeDate.TabIndex = 7;
            this.dtpExchangeDate.ValueChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(358, 113);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 8;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // TxtDefaultCurrency
            // 
            this.TxtDefaultCurrency.BackColor = System.Drawing.SystemColors.Info;
            this.TxtDefaultCurrency.Enabled = false;
            this.TxtDefaultCurrency.Location = new System.Drawing.Point(110, 36);
            this.TxtDefaultCurrency.Name = "TxtDefaultCurrency";
            this.TxtDefaultCurrency.Size = new System.Drawing.Size(294, 20);
            this.TxtDefaultCurrency.TabIndex = 2;
            this.TxtDefaultCurrency.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // BtnCurrency
            // 
            this.BtnCurrency.Location = new System.Drawing.Point(407, 60);
            this.BtnCurrency.Name = "BtnCurrency";
            this.BtnCurrency.Size = new System.Drawing.Size(26, 23);
            this.BtnCurrency.TabIndex = 4;
            this.BtnCurrency.Text = "...";
            this.BtnCurrency.UseVisualStyleBackColor = true;
            this.BtnCurrency.Click += new System.EventHandler(this.BtnCurrency_Click);
            // 
            // CboCurrencyType
            // 
            this.CboCurrencyType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCurrencyType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCurrencyType.BackColor = System.Drawing.SystemColors.Info;
            this.CboCurrencyType.DropDownHeight = 75;
            this.CboCurrencyType.FormattingEnabled = true;
            this.CboCurrencyType.IntegralHeight = false;
            this.CboCurrencyType.Location = new System.Drawing.Point(110, 62);
            this.CboCurrencyType.Name = "CboCurrencyType";
            this.CboCurrencyType.Size = new System.Drawing.Size(294, 21);
            this.CboCurrencyType.TabIndex = 3;
            this.CboCurrencyType.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.CboCurrencyType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(445, 321);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 81;
            this.LineShape1.X2 = 430;
            this.LineShape1.Y1 = 149;
            this.LineShape1.Y2 = 149;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(359, 352);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 10;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(278, 352);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 9;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // ErrCurrency
            // 
            this.ErrCurrency.ContainerControl = this;
            this.ErrCurrency.RightToLeft = true;
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.lblCurrencyStatus});
            this.bar1.Location = new System.Drawing.Point(0, 382);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(445, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 21;
            this.bar1.TabStop = false;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "Status:";
            // 
            // lblCurrencyStatus
            // 
            this.lblCurrencyStatus.Name = "lblCurrencyStatus";
            // 
            // FrmCurrency
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 401);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.pnlCurrency);
            this.Controls.Add(this.CurrencyBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCurrency";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exchange - Currency ";
            this.Load += new System.EventHandler(this.FrmCurrency_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCurrency_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmCurrency_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.CurrencyBindingNavigator)).EndInit();
            this.CurrencyBindingNavigator.ResumeLayout(false);
            this.CurrencyBindingNavigator.PerformLayout();
            this.pnlCurrency.ResumeLayout(false);
            this.pnlCurrency.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator CurrencyBindingNavigator;
        private System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        private System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton btnClear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton btnEmail;
        private System.Windows.Forms.ToolStripButton btnPrint;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Panel pnlCurrency;
        internal System.Windows.Forms.ComboBox CboCompany;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.LinkLabel LblGetExchangeRate;
        internal System.Windows.Forms.DateTimePicker dtpExchangeDate;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.TextBox TxtDefaultCurrency;
        internal System.Windows.Forms.Button BtnCurrency;
        internal System.Windows.Forms.ComboBox CboCurrencyType;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.DataGridView DgvCurrency;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Timer TmCurrency;
        private System.Windows.Forms.ErrorProvider ErrCurrency;
        private System.Windows.Forms.TextBox txtExchangeRate;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.LabelItem lblCurrencyStatus;
        private System.Windows.Forms.Label lblECompCrncy;
        private System.Windows.Forms.Label lblESelectedCurrency;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
    }
}