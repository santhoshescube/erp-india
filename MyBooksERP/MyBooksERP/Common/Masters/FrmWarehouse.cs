﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;


namespace MyBooksERP
{
    public partial class FrmWarehouse : DevComponents.DotNetBar.Office2007Form
    {
        private bool MblnAddStatus; //To Specify Add/Update mode
        private bool MblnPrintEmailPermission = false;//To Set View Permission
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnClose = false;
        private int MintRecordCnt;
        private string MstrMessageCaption;
        private string MstrMessageCommon;
        private int MintTimerInterval;
        private int MintWarehouseID;
        public int PintWareHouse = 0; //For Setting Warehouse ID Externally
        public int MiWarehouseIDFromPurchase; //laxmi added for purchase
        private string strCondition;
        private ArrayList MArrlstMessage;                  // Error Message display
        private ArrayList MArrlstStatusMessage;
        private MessageBoxIcon MmessageIcon;

        clsBLLWarehouse MObjclsBLLWarehouse;
        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;
        ClsCommonUtility MobjClsCommonUtility;

        public FrmWarehouse()
        {
            InitializeComponent();

            MObjclsBLLWarehouse = new clsBLLWarehouse();
            MObjClsNotification = new ClsNotification();
            MobjClsCommonUtility = new ClsCommonUtility();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MmessageIcon = MessageBoxIcon.Information;
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
        }

        private void FrmWarehouse_Load(object sender, EventArgs e)
        {
            try
            {
                SetPermissions();
                LoadMessage();
                LoadCombos(0);
                
                if (PintWareHouse > 0)
                {
                    MObjclsBLLWarehouse.clsDTOWarehouse.intWarehouseID = PintWareHouse;
                    MintRecordCnt = MObjclsBLLWarehouse.DisplayRecordCount(ClsCommonSettings.CompanyID);
                    LblWarehouseStatus.Text = "";
                    DisplayWarehouseInfo();
                    BindingNavigatorCountItem.Text = "of " + MintRecordCnt.ToString();
                    BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();
                    ChangeStatus();
                }
                else
                    AddNewWarehouse();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on FrmWarehouse_Load " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("FrmWarehouse_Load " + Ex.Message.ToString());
            }
        }

        private void FrmWarehouse_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MObjclsBLLWarehouse.clsDTOWarehouse.intWarehouseID > 0)
                PintWareHouse = MObjclsBLLWarehouse.clsDTOWarehouse.intWarehouseID;

            if (BtnSave.Enabled)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MArrlstMessage, 8, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    MObjClsNotification = null;
                    MObjClsLogWriter = null;
                    MObjclsBLLWarehouse = null;
                    e.Cancel = false;
                }
                else
                    e.Cancel = true;
            }
        }

        private void BtnCountry_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Country", new int[] { 1, 0, MintTimerInterval }, "CountryID,CountryName As Country", "CountryReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intCurrentCountry = CboCountry.SelectedValue.ToInt32();
                LoadCombos(1);

                if (objCommon.NewID != 0)
                    CboCountry.SelectedValue = objCommon.NewID;
                else
                    CboCountry.SelectedValue = intCurrentCountry;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on BtnCountry_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("BtnCountry_Click " + Ex.Message.ToString());
            }
        }

        private void btnInCharge_Click(object sender, EventArgs e)
        {
            try
            {
                int intCurrentEmployee = cboInchange.SelectedValue.ToInt32();

                using (FrmEmployee objFrmEmployee = new FrmEmployee())
                {
                    objFrmEmployee.PintEmployeeID = Convert.ToInt32(cboInchange.SelectedValue);
                    objFrmEmployee.ShowDialog();
                    LoadCombos(3);
                    if (objFrmEmployee.PintEmployeeID != 0)
                        cboInchange.SelectedValue = objFrmEmployee.PintEmployeeID;
                    else
                        cboInchange.SelectedValue = intCurrentEmployee;
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on btnInCharge_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("btnInCharge_Click " + Ex.Message.ToString());
            }
        }

        private bool LoadCombos(int intType)
        {
            // 0 - For Loading All Combo
            // 1 - For Loading CboCountry 
            // 3-For Incharge combo Loading
            try
            {
                DataTable datCombos = new DataTable();

                if (intType == 0 || intType == 1)
                {
                    datCombos = MObjclsBLLWarehouse.FillCombos(new string[] { "CountryID,CountryName", "CountryReference", "" });
                    CboCountry.ValueMember = "CountryID";
                    CboCountry.DisplayMember = "CountryName";
                    CboCountry.DataSource = datCombos;
                }

                if (intType == 0 || intType == 3)
                {
                    cboInchange.DataSource = null;
                    strCondition = "CompanyID IN (SELECT CompanyID FROM CompanyMaster WHERE CompanyID = " + ClsCommonSettings.CompanyID + " OR " +
                        "ParentID = " + ClsCommonSettings.CompanyID + ") AND WorkStatusID >= 6 ";
                    
                    datCombos = null;
                    datCombos = MObjclsBLLWarehouse.FillCombos(new string[] { "EmployeeID,EmployeeFullName + ' - ' + EmployeeNumber AS FirstName", "" +
                        "EmployeeMaster", strCondition });
                    cboInchange.ValueMember = "EmployeeID";
                    cboInchange.DisplayMember = "FirstName";
                    cboInchange.DataSource = datCombos;
                }

                if (intType == 0 || intType == 4)
                {
                    datCombos = MObjclsBLLWarehouse.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" + 
                        "CompanyID = " + ClsCommonSettings.CompanyID + " OR ParentID = " + ClsCommonSettings.CompanyID });

                    clbCompany.DataSource = datCombos;
                    clbCompany.ValueMember = "CompanyID";
                    clbCompany.DisplayMember = "CompanyName";
                }

                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.Warehouse, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                DataTable DtPermission = objClsBLLPermissionSettings.GetMenuPermissions(ClsCommonSettings.RoleID);
                
                if (!(DtPermission.Rows.Count == 0))                
                {
                    DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Employee).ToString();
                    btnInCharge.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);
                }
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void LoadMessage()
        {
            MArrlstMessage = new ArrayList();
            MArrlstStatusMessage = new ArrayList();
            MArrlstMessage = MObjClsNotification.FillMessageArray((int)FormID.Warehouse, ClsCommonSettings.ProductID);
            MArrlstStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.Warehouse, ClsCommonSettings.ProductID);
        }

        private bool AddNewWarehouse()
        {
            ClearAllContorls();
            cboInchange.SelectedIndex = -1;
            DisplayRecordCount();
            BindingNavigatorCountItem.Text = "of " + (MintRecordCnt + 1).ToString();
            BindingNavigatorPositionItem.Text = (MintRecordCnt + 1).ToString();
            MblnAddStatus = true;
            BindingNavigatorAddNewItem.Enabled = BtnPrint.Enabled = BtnEmail.Enabled = false;
            BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = BindingNavigatorDeleteItem.Enabled = false;            
            TxtName.Focus();
            ClearCompanyList();
            TbCreateWarehouse.SelectedTab = TpGeneral;
            return true;
        }

        private void ClearAllContorls()
        {
            MintWarehouseID = 0;
            TxtName.Text = "";
            TxtShortName.Text = "";
            CboCountry.Text = cboInchange.Text = "";
            TxtAddress1.Text = "";
            TxtAddress2.Text = "";
            TxtZipCode.Text = "";
            TxtCity.Text = "";
            TxtDescription.Text = "";
            CboCountry.SelectedIndex = -1;
            cboInchange.SelectedIndex = -1;
            TxtPhoneNo.Text = "";
            ErrProWarehouse.Clear();
            chbActive.Checked = true;
        }

        private void DisplayRecordCount()//Dispalying Total Record Count
        {
            MintRecordCnt = MObjclsBLLWarehouse.DisplayRecordCount(ClsCommonSettings.CompanyID);
            BindingNavigatorCountItem.Text = "of " + MintRecordCnt.ToString();
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();
            BindingEnableDisable();
        }

        private bool FillParameterMaster()
        {
            MObjclsBLLWarehouse.clsDTOWarehouse.intWarehouseID = MintWarehouseID;
            MObjclsBLLWarehouse.clsDTOWarehouse.strWarehouseName = TxtName.Text;
            MObjclsBLLWarehouse.clsDTOWarehouse.strShortName = TxtShortName.Text;
            MObjclsBLLWarehouse.clsDTOWarehouse.intCompanyID = ClsCommonSettings.CompanyID;
            MObjclsBLLWarehouse.clsDTOWarehouse.strAddress1 = TxtAddress1.Text;
            MObjclsBLLWarehouse.clsDTOWarehouse.strAddress2 = TxtAddress2.Text;
            MObjclsBLLWarehouse.clsDTOWarehouse.strZipCode = TxtZipCode.Text;
            MObjclsBLLWarehouse.clsDTOWarehouse.strCityState = TxtCity.Text;
            MObjclsBLLWarehouse.clsDTOWarehouse.intCountryID = CboCountry.SelectedValue.ToInt32();
            MObjclsBLLWarehouse.clsDTOWarehouse.strDescription = TxtDescription.Text;
            MObjclsBLLWarehouse.clsDTOWarehouse.intInChargeID = cboInchange.SelectedValue.ToInt32();
            MObjclsBLLWarehouse.clsDTOWarehouse.strPhone = TxtPhoneNo.Text;

            if (chbActive.Checked)
                MObjclsBLLWarehouse.clsDTOWarehouse.blnActive = true;
            else
                MObjclsBLLWarehouse.clsDTOWarehouse.blnActive = false;

            MObjclsBLLWarehouse.clsDTOWarehouse.lstWarehouseCompanyDetails = new List<ClsDTOWarehouseCompanyDetails>();

            for (int iCounter = 0; iCounter <= clbCompany.Items.Count - 1; iCounter++)
            {
                if (clbCompany.GetItemChecked(iCounter) == true)
                {
                    DataRowView drv = (DataRowView)clbCompany.Items[iCounter];
                    ClsDTOWarehouseCompanyDetails objWarehouseCompanyDetails = new ClsDTOWarehouseCompanyDetails();
                    objWarehouseCompanyDetails.CompanyID = drv.Row[0].ToInt32();
                    MObjclsBLLWarehouse.clsDTOWarehouse.lstWarehouseCompanyDetails.Add(objWarehouseCompanyDetails);
                }
            }

            return true;
        }
        
        private bool SaveWarehouseInfo()//Master table insertion
        {
            try
            {
                if (WarehouseFormValidation() == false)
                    return false;

                if (MessageBox.Show(MObjClsNotification.GetErrorMessage(MArrlstMessage, (MblnAddStatus == true ? 1 : 3), out MmessageIcon).Replace("#", "").Trim(),
                         MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return false;

                if (CheckDuplication())
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MArrlstMessage, 614, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErrProWarehouse.SetError(TxtName, MstrMessageCommon.Replace("#", "").Trim());
                    LblWarehouseStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrWarehouse.Enabled = true;
                    TbCreateWarehouse.SelectedTab = TpGeneral;
                    TxtName.Focus();
                    return false;
                }

                FillParameterMaster();

                if (MObjclsBLLWarehouse.SaveWarehouseGenaralInformation())//Master Saving
                    return true;

                return false;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on SaveWarehouseInfo " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("SaveWarehouseInfo " + Ex.Message.ToString());

                return false;
            }
        }

        private void DisplayWarehouseInfo()
        {
            try
            {
                ClearAllContorls();

                if (MObjclsBLLWarehouse.DispalyWarehouseInfo(BindingNavigatorPositionItem.Text.ToInt32(), ClsCommonSettings.CompanyID))
                {
                    TbCreateWarehouse.SelectedTab = TpGeneral;
                    MintWarehouseID = MObjclsBLLWarehouse.clsDTOWarehouse.intWarehouseID;
                    TxtName.Text = MObjclsBLLWarehouse.clsDTOWarehouse.strWarehouseName;
                    TxtShortName.Text = MObjclsBLLWarehouse.clsDTOWarehouse.strShortName;
                    TxtAddress1.Text = MObjclsBLLWarehouse.clsDTOWarehouse.strAddress1;
                    TxtAddress2.Text = MObjclsBLLWarehouse.clsDTOWarehouse.strAddress2;
                    TxtZipCode.Text = MObjclsBLLWarehouse.clsDTOWarehouse.strZipCode;
                    TxtCity.Text = MObjclsBLLWarehouse.clsDTOWarehouse.strCityState;
                    CboCountry.SelectedValue = MObjclsBLLWarehouse.clsDTOWarehouse.intCountryID;
                    TxtDescription.Text = MObjclsBLLWarehouse.clsDTOWarehouse.strDescription;
                    cboInchange.SelectedValue = MObjclsBLLWarehouse.clsDTOWarehouse.intInChargeID;
                    TxtPhoneNo.Text = MObjclsBLLWarehouse.clsDTOWarehouse.strPhone;
                    chbActive.Checked = MObjclsBLLWarehouse.clsDTOWarehouse.blnActive;

                    if (cboInchange.SelectedValue.ToInt32() == 0)
                    {
                        DataTable datEmployeeDetails = MObjclsBLLWarehouse.FillCombos(new string[] { "EmployeeFullName + ' - ' + EmployeeNumber AS FirstName", "" +
                            "EmployeeMaster", "EmployeeID = " + MObjclsBLLWarehouse.clsDTOWarehouse.intInChargeID });
                        if(datEmployeeDetails.Rows.Count>0)
                         cboInchange.Text = datEmployeeDetails.Rows[0]["FirstName"].ToStringCustom();
                    }
                }

                ClearCompanyList();

                DataTable datTemp = MObjclsBLLWarehouse.getWarehouseCompanyDetails(MintWarehouseID);

                for (int iCounter = 0; iCounter < clbCompany.Items.Count; iCounter++)
                {
                    DataRowView drv = (DataRowView)clbCompany.Items[iCounter];

                    for (int jCounter = 0; jCounter <= datTemp.Rows.Count - 1; jCounter++)
                    {
                        if (drv.Row[0].ToInt32() == datTemp.Rows[jCounter]["CompanyID"].ToInt32())
                        {
                            clbCompany.SetItemChecked(iCounter, true);
                            break;
                        }
                    }
                }

                ChangeStatus();
                // disable in update mode. Enable these controls when edit started
                BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = false; 
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on DisplayWarehouseInfo " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("DisplayWarehouseInfo " + Ex.Message.ToString());
            }
        }

        private bool DeleteWarehouseInformation()
        {
            try
            {
                MObjclsBLLWarehouse.clsDTOWarehouse.intWarehouseID = MintWarehouseID;

                if (MObjclsBLLWarehouse.DeleteWarehouseInformation())
                    return true;
                else
                    return false;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on DeleteWarehouseInformation()" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("DeleteWarehouseInformation()" + Ex.Message.ToString());
                return false;
            }
        }
        
        private void ChangeStatus()
        {
            if (MblnAddStatus)
            {
                BindingNavigatorAddNewItem.Enabled = BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnAddPermission;
                BindingNavigatorDeleteItem.Enabled = BtnPrint.Enabled = BtnEmail.Enabled = false;
            }
            else
            {
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
            }

            ErrProWarehouse.Clear();
        }

        private void TxtValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)13 || e.KeyChar == (char)8)
                    e.Handled = false;
                else if ((!(e.KeyChar >= 48 && e.KeyChar <= 57)))
                    e.Handled = true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on TxtValue_keypress" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("txtValue_Keypress " + Ex.Message.ToString());
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void LoadReport()
        {
            if (MintWarehouseID > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = MintWarehouseID;
                ObjViewer.PiFormID = (int)FormID.Warehouse;
                ObjViewer.ShowDialog();
            }
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
            {
                ObjEmailPopUp.MsSubject = "Warehouse Information";
                ObjEmailPopUp.EmailFormType = EmailFormID.Warehouse;
                ObjEmailPopUp.EmailSource = MObjclsBLLWarehouse.GetWarehouseReport();
                ObjEmailPopUp.ShowDialog();
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveWarehouseInfo())
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MArrlstMessage, 2, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                LblWarehouseStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrWarehouse.Enabled = true;
                AddNewWarehouse();
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (SaveWarehouseInfo())
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MArrlstMessage, 2, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                LblWarehouseStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrWarehouse.Enabled = true;
                AddNewWarehouse();
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (SaveWarehouseInfo())
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MArrlstMessage, 2, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                LblWarehouseStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrWarehouse.Enabled = true;
                MblnClose = true;
                this.Close();
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            AddNewWarehouse();
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            //MblnIsEditMode = true;

            //if (MintCurrentRecCnt > 1)
            //{
            //    MintCurrentRecCnt = 1;
            //    DisplayWarehouseInfo();
            //}

            //SetBindingNavigatorButtons();

            BindingNavigatorPositionItem.Text = "1";

            if (MintRecordCnt > 1)
                BindingNavigatorCountItem.Text = "of " + MintRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = "of 1";

            DisplayWarehouseInfo();
            BindingEnableDisable();
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            //MblnIsEditMode = true;
            //DisplayRecordCount();

            //if (MintCurrentRecCnt > 1)
            //{
            //    MintCurrentRecCnt = MintCurrentRecCnt - 1;
            //    DisplayWarehouseInfo();
            //    SetBindingNavigatorButtons();
            //}

            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) - 1).ToString();

            if (Convert.ToInt32(BindingNavigatorPositionItem.Text) <= 0)
                BindingNavigatorPositionItem.Text = "1";

            if (MintRecordCnt > 1)
                BindingNavigatorCountItem.Text = "of " + MintRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = "of 1";

            DisplayWarehouseInfo();
            BindingEnableDisable();
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            //MblnIsEditMode = true;

            //if (MintCurrentRecCnt < MintRecordCnt)
            //{
            //    MintCurrentRecCnt = MintCurrentRecCnt + 1;
            //    DisplayWarehouseInfo();
            //}

            //SetBindingNavigatorButtons();

            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) + 1).ToString();

            if (MintRecordCnt < Convert.ToInt32(BindingNavigatorPositionItem.Text))
            {
                if (MintRecordCnt > 1)
                    BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();
                else
                    BindingNavigatorPositionItem.Text = "1";
            }

            if (MintRecordCnt > 1)
                BindingNavigatorCountItem.Text = "of " + MintRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = "of 1";

            DisplayWarehouseInfo();
            BindingEnableDisable();
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            //MblnIsEditMode = true;
            //DisplayRecordCount();

            //if (MintCurrentRecCnt < MintRecordCnt)
            //{
            //    MintCurrentRecCnt = MintRecordCnt;
            //    DisplayWarehouseInfo();
            //}

            //SetBindingNavigatorButtons();

            if (MintRecordCnt > 1)
            {
                BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();
                BindingNavigatorCountItem.Text = "of " + MintRecordCnt.ToString();
            }
            else
            {
                BindingNavigatorPositionItem.Text = "1";
                BindingNavigatorCountItem.Text = "of 1";
            }

            DisplayWarehouseInfo();
            BindingEnableDisable();
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewWarehouse();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            MObjclsBLLWarehouse.clsDTOWarehouse.intWarehouseID = MintWarehouseID;

            if (DeleteValidation(MintWarehouseID))//If id not Exist in other table
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MArrlstMessage, 13, out MmessageIcon);//Delete

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (DeleteWarehouseInformation())
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MArrlstMessage, 4, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        LblWarehouseStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrWarehouse.Enabled = true;
                        AddNewWarehouse();
                    }
                }
            }
        }

        private bool DeleteValidation(int MiWarehousePassingId)
        {
            if (MObjclsBLLWarehouse.GetFormsInUse(MiWarehousePassingId))
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MArrlstMessage, 610, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                TmrWarehouse.Enabled = true;
                return false;
            }

            return true;
        }

        private bool DeleteNodeValidation(int intLocId, int intRowID, int intBlockID, int intLotID, int intLevel)
        {
            if (MObjclsBLLWarehouse.GetNodeInUse(MintWarehouseID, intLocId, intRowID, intBlockID, intLotID, intLevel))
            {
                contextMenuStripGenerator.Close();
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MArrlstMessage, 610, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                TmrWarehouse.Enabled = true;
                return false;
            }

            return true;
        }

        private bool WarehouseFormValidation() //Validation
        {

            if (TxtName.Text.Trim() == "")
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MArrlstMessage, 603, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrProWarehouse.SetError(TxtName, MstrMessageCommon.Replace("#", "").Trim());
                LblWarehouseStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrWarehouse.Enabled = true;
                TbCreateWarehouse.SelectedTab = TpGeneral;
                TxtName.Focus();
                return false;
            }

            //if (Convert.ToInt32(CboCountry.SelectedValue) == 0)
            //{
            //    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MArrlstMessage, 606, out MmessageIcon);
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
            //    ErrProWarehouse.SetError(CboCountry, MstrMessageCommon.Replace("#", "").Trim());
            //    LblWarehouseStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            //    TmrWarehouse.Enabled = true;
            //    TbCreateWarehouse.SelectedTab = TpGeneral;
            //    CboCountry.Focus();
            //    return false;
            //}

            //if (Convert.ToInt32(cboInchange.SelectedValue) == 0)
            //{
            //    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MArrlstMessage, 607, out MmessageIcon);
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
            //    ErrProWarehouse.SetError(cboInchange, MstrMessageCommon.Replace("#", "").Trim());
            //    LblWarehouseStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            //    TmrWarehouse.Enabled = true;
            //    TbCreateWarehouse.SelectedTab = TpGeneral;
            //    cboInchange.Focus();
            //    return false;
            //}

            //if (TxtPhoneNo.Text.Trim() == "")
            //{
            //    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MArrlstMessage, 615, out MmessageIcon);
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
            //    ErrProWarehouse.SetError(TxtPhoneNo, MstrMessageCommon.Replace("#", "").Trim());
            //    LblWarehouseStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            //    TmrWarehouse.Enabled = true;
            //    TbCreateWarehouse.SelectedTab = TpGeneral;
            //    TxtPhoneNo.Focus();
            //    return false;
            //}

            if (CheckDuplication())
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MArrlstMessage, 614, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrProWarehouse.SetError(TxtName, MstrMessageCommon.Replace("#", "").Trim());
                LblWarehouseStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrWarehouse.Enabled = true;
                TbCreateWarehouse.SelectedTab = TpGeneral;
                TxtName.Focus();
                return false;
            }

            return true;
        }

        private bool CheckDuplication()
        {
            if (MObjclsBLLWarehouse.CheckDuplication(ClsCommonSettings.CompanyID, Convert.ToString(TxtName.Text.Trim()), MintWarehouseID))
                return true;

            return false;
        }

        private void TVRows_AfterSelect(object sender, TreeViewEventArgs e)
        {
            ChangeStatus();
        }

        private void ChangeStatus(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void TmrWarehouse_Tick(object sender, EventArgs e)
        {
            TmrWarehouse.Enabled = false;
            LblWarehouseStatus.Text = "";
        }

        /// <summary>
        /// Enables/disables binding navigator buttons according to the current position
        /// </summary>
        private void BindingEnableDisable() // Navigator visibility settings
        {
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;

            if (MintRecordCnt == 0)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == Convert.ToDouble(MintRecordCnt))
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                else if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == 1)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
            }
        }

        private void Cbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        private void FrmWarehouse_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    //case Keys.Control | Keys.S:
                    //    btnSave_Click(sender,new EventArgs());//save
                    //  break;
                    //case Keys.Control | Keys.O:
                    //    btnOk_Click(sender, new EventArgs());//OK
                    //    break; 
                    //case Keys.Control | Keys.L:
                    //    btnCancel_Click(sender, new EventArgs());//Cancel
                    //    break;
                    case Keys.Control | Keys.Enter:
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        BtnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        BtnPrint_Click(sender, new EventArgs());//Cancel
                        break;
                    case Keys.Control | Keys.M:
                        BtnEmail_Click(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on FrmWarehouse_KeyDown()" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmWarehouse_KeyDown()" + Ex.Message.ToString());
            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Warehouses";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void FrmWarehouse_Shown(object sender, EventArgs e)
        {
            TxtName.Focus();
        }

        private void ClearCompanyList()
        {
            for (int iCounter = 0; iCounter < clbCompany.Items.Count; iCounter++)
                clbCompany.SetItemChecked(iCounter, false);
        }
    }
}