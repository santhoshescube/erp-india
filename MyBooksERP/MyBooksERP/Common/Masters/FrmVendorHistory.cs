﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace MyBooksERP
{
    public partial class FrmVendorHistory : Form
    {
        int MintAssociateType;
        int MintAssociateID=0;
        int MintPanalVisible = 1;
        bool MbViewPermission = true;
        bool MblnPrintEmailPermission = false;//To Set View Permission
        bool MblnAddPermission = false;//To Set Add Permission
        bool MblnUpdatePermission = false;//To Set Update Permission
        bool MblnDeletePermission = false;//To Set Delete Permission
        TabPage objTpSupPurchase = new TabPage();
        TabPage objTpSupPayments = new TabPage();
        TabPage objTpSupStock = new TabPage();
        TabPage objTpSupSalesComplaints = new TabPage();
        TabPage objTpCusSales = new TabPage();
        TabPage objTpCusPayment = new TabPage();
        TabPage objTpCusSalesItems = new TabPage();
        TabPage objTpCusPersonalInfo = new TabPage();
        TabPage objTpSupPersonalInfo = new TabPage();


        clsBLLReportViewer MobjclsBLLReportViewer;
        public FrmVendorHistory()
        {
            InitializeComponent();
        }
        public FrmVendorHistory(int intAssociateType)
        {
            MintAssociateType = intAssociateType;          
            InitializeComponent();
        }
        public FrmVendorHistory(int intAssociateType, int intAssociateID)
        {
            MintAssociateType = intAssociateType;
            MintAssociateID = intAssociateID;
            InitializeComponent();
            CboAssociative.Enabled = false;
        }
        private void CboAssociative_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable datCombos;

            if (MintPanalVisible == 1)
            {
                if (CboAssociative.SelectedIndex == 0)
                {
                    datCombos = null;
                    datCombos= MobjclsBLLReportViewer.FillCombos(new string[] { "VendorID,VendorName", "VendorInformation", "VendorTypeID=3"});
                    cboVendor.DisplayMember = "VendorName";
                    cboVendor.ValueMember = "VendorID";
                    cboVendor.DataSource = datCombos;
                    this.Text = "Customer History";
                    this.Icon = ((System.Drawing.Icon)(Properties.Resources.Customer_History));
                    if (tcVendorHistory.Contains(tpSupPurchase) && tcVendorHistory.Contains(tpSupPayments) && tcVendorHistory.Contains(tpSupStock) /*&& tcVendorHistory.Contains(tpSupSalesComplaints) */&& tcVendorHistory.Contains(tpGeneralInfo))
                    {
                        //objTpSupPersonalInfo=tpGeneralInfo; 
                        //objTpSupPurchase = tpSupPurchase;
                        //objTpSupPayments = tpSupPayments;
                        //objTpSupStock = tpSupStock;
                        //objTpSupSalesComplaints = tpSupSalesComplaints;
                        tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpGeneralInfo"]);
                        tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpSupPurchase"]);
                        tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpSupPayments"]);
                        tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpSupStock"]);
                        // tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpSupSalesComplaints"]);
                        if (objTpCusSales != null && objTpCusPayment != null && objTpCusSalesItems != null && objTpCusPersonalInfo != null)
                        {
                            tcVendorHistory.TabPages.Add(objTpCusPersonalInfo);
                            tcVendorHistory.TabPages.Add(objTpCusSales);
                            tcVendorHistory.TabPages.Add(objTpCusPayment);
                            tcVendorHistory.TabPages.Add(objTpCusSalesItems);
                            lblGenderOrSupClass.Text = "Gender :";
                        }
                    }

                }
                else
                {
                    datCombos = null;
                    datCombos=MobjclsBLLReportViewer.FillCombos(new string[] { "VendorID,VendorName", "VendorInformation", "VendorTypeID=2"});
                    cboVendor.DisplayMember = "VendorName";
                    cboVendor.ValueMember = "VendorID";
                    cboVendor.DataSource = datCombos;
                    this.Text = "Supplier History";
                    this.Icon = ((System.Drawing.Icon)(Properties.Resources.Vendor_history1));
                    if (tcVendorHistory.Contains(tpCusSales) && tcVendorHistory.Contains(tpCusPayment) && tcVendorHistory.Contains(tpCusSalesItems) && tcVendorHistory.Contains(tpGeneralInfo))
                    {
                        //objTpCusPersonalInfo = tpGeneralInfo;
                        //objTpCusSales = tpCusSales;
                        //objTpCusPayment = tpCusPayment;
                        //objTpCusSalesItems = tpCusSalesItems;
                        tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpCusSales"]);
                        tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpCusPayment"]);
                        tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpCusSalesItems"]);
                        tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpGeneralInfo"]);
                        if (objTpSupPersonalInfo != null && objTpSupPurchase != null && objTpSupPayments != null && objTpSupStock != null/* && objTpSupSalesComplaints != null*/)
                        {
                            tcVendorHistory.TabPages.Add(objTpSupPersonalInfo);
                            tcVendorHistory.TabPages.Add(objTpSupPurchase);
                            tcVendorHistory.TabPages.Add(objTpSupPayments);
                            tcVendorHistory.TabPages.Add(objTpSupStock);
                            //tcVendorHistory.TabPages.Add(objTpSupSalesComplaints);
                            lblGenderOrSupClass.Text = "Supplier Classification :";
                        }
                    }

                }
                cboVendor.Text = null;//to clear combo when datasource null
                cboVendor.SelectedIndex = -1;
            }
        }

        private void FrmVendorHistory_Load(object sender, EventArgs e)
        {
            SetPermissions();
            MobjclsBLLReportViewer = new clsBLLReportViewer();
            objTpSupPersonalInfo = tpGeneralInfo;
            objTpSupPurchase = tpSupPurchase;
            objTpSupPayments = tpSupPayments;
            objTpSupStock = tpSupStock;
            objTpSupSalesComplaints = tpSupSalesComplaints;
            objTpCusPersonalInfo = tpGeneralInfo;
            objTpCusSales = tpCusSales;
            objTpCusPayment = tpCusPayment;
            objTpCusSalesItems = tpCusSalesItems;
            tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpSupSalesComplaints"]);
            if (MintAssociateType == 3)
            {
                tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpSupPurchase"]);
                tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpSupPayments"]);
                tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpSupStock"]);
                //tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpSupSalesComplaints"]);
                CboAssociative.SelectedIndex = 0;
            }
            else
            {
                tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpCusSales"]);
                tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpCusPayment"]);
                tcVendorHistory.TabPages.Remove(tcVendorHistory.TabPages["tpCusSalesItems"]);
                lblGenderOrSupClass.Text = "Supplier Classification :";
                CboAssociative.SelectedIndex = 1;
            }
            if (MintAssociateID != 0)
            {
               
                    cboVendor.SelectedValue = MintAssociateID;
                    MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = MintAssociateID;
                    BtnShow_Click(sender, e);
                    pnlExpandVendorHistory.Expanded = false;
               
            }
        }
        private void SetPermissions()
        {
            //clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            //if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            //{
            //    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.VendorHistory, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            //}
            //else
            //    MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            //MbViewPermission = (MblnAddPermission || MblnPrintEmailPermission || MblnUpdatePermission || MblnDeletePermission);
            //tcVendorHistory.Enabled = pnlExpandVendorHistory.Enabled= MbViewPermission;
        }
        private void BtnShow_Click(object sender, EventArgs e)
        {
            if (CboAssociative.SelectedIndex != -1 && cboVendor.SelectedIndex != -1)
            {
                //General info and Documents info
                DataTable datGeneralInfo = MobjclsBLLReportViewer.DisplayVendorReport();
                if (datGeneralInfo.Rows.Count > 0)
                {
                    lblNameValue.Text = datGeneralInfo.Rows[0]["VendorName"].ToString();
                    lblCodeValue.Text = datGeneralInfo.Rows[0]["VendorCode"].ToString();
                    lblEmailValue.Text = datGeneralInfo.Rows[0]["EMAIL"].ToString();
                    lblTransactionTypeValue.Text = datGeneralInfo.Rows[0]["TransactionType"].ToString();
                    lblRatingValue.Text = datGeneralInfo.Rows[0]["Rating"].ToString();
                    lblVendorStatusValue.Text = datGeneralInfo.Rows[0]["Status"].ToString();
                    string[] strAryColumnDoc = new string[] { "DocumentNo", "DocumentType", "IssueDate", "ExpiryDate" };
                    dgvDocuments.DataSource = datGeneralInfo.DefaultView.ToTable(true, strAryColumnDoc);
                    //dgvDocuments.Columns["DocumentNo"].HeaderText = "Document No";
                    //dgvDocuments.Columns["DocumentType"].HeaderText = "Document Type";
                    //dgvDocuments.Columns["IssueDate"].HeaderText = "Issue Date";
                    //dgvDocuments.Columns["ExpiryDate"].HeaderText = "Expiry Date";

                    //Address
                    DataTable datAddress = MobjclsBLLReportViewer.DisplayVendorAddressReport();
                    string[] strAryColumnAdrs = new string[] { "AddressName", "Address", "Country", "MobileNo" };
                    dgvContactAddress.DataSource = datAddress.DefaultView.ToTable(true, strAryColumnAdrs);
                    dgvContactAddress.Columns["AddressName"].HeaderText = "AddressType";
                    dgvContactAddress.Columns["Country"].HeaderText = "Nationality";

                    if (CboAssociative.SelectedIndex == 0)
                    {
                        lblGenderOrSupClassValue.Text = datGeneralInfo.Rows[0]["Gender"].ToString();

                        //Customer Sales Details
                        DataTable datCustomerSales = MobjclsBLLReportViewer.DisplayCustomerSalesReport();
                        dgvCustmerSales.DataSource = datCustomerSales;
                        dgvCustmerSales.Columns["SalesOrderID"].Visible = false;
                        dgvCustmerSales.Columns["SalesInvoiceID"].Visible = false;

                        //Customer Payment Details
                        DataTable datCustomerPayment = MobjclsBLLReportViewer.DisplayCustomerPaymentReport();
                        dgvCustomerPayments.DataSource = datCustomerPayment;
                        dgvCustomerPayments.Columns["SalesOrderID"].Visible = false;
                        dgvCustomerPayments.Columns["SalesInvoiceID"].Visible = false;

                        //Customer Sales Item Details
                        DataTable datCustomerSalesItems = MobjclsBLLReportViewer.DisplayCustomerItemsReport();
                        dgvCustomerItems.DataSource = datCustomerSalesItems;
                        dgvCustomerItems.Columns["ItemID"].Visible = false;
                        this.Text = "Customer History [ " + lblNameValue.Text + " ]";
                    }
                    if (CboAssociative.SelectedIndex == 1)
                    {
                        lblGenderOrSupClassValue.Text = datGeneralInfo.Rows[0]["VendorClassification"].ToString();


                        //Supplier Purchase Details
                        DataTable datSupplierPurchase = MobjclsBLLReportViewer.DisplaySupplierPurchaseReport();
                        dgvSupplierPurchase.DataSource = datSupplierPurchase;
                        dgvSupplierPurchase.Columns["PurchaseInvoiceID"].Visible = false;
                        dgvSupplierPurchase.Columns["PurchaseOrderID"].Visible = false;

                        //Supplier Payment Details
                        DataTable datSupplierPayment = MobjclsBLLReportViewer.DisplaySupplierPaymentReport();
                        dgvSupplierPayments.DataSource = datSupplierPayment;
                        dgvSupplierPayments.Columns["PurchaseInvoiceID"].Visible = false;
                        dgvSupplierPayments.Columns["PurchaseOrderID"].Visible = false;

                        //Supplier Items Details
                        DataTable datSupplierItems = MobjclsBLLReportViewer.DisplaySupplierItemsReport();
                        dgvSupplierItems.DataSource = datSupplierItems;
                        dgvSupplierItems.Columns["ItemID"].Visible = false;
                        this.Text = "Supplier History [ " + lblNameValue.Text + " ]";
                    }
                }
            }
            else
            {
                ClearControls();
            }


            
        }

        private void cboVendor_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboVendor.DroppedDown = false;
        }

        private void cboVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboVendor.SelectedIndex != -1)
            {
                MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = cboVendor.SelectedValue.ToInt32();
            }
            else
            {
                MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = 0;
            }
        }

      
       private void  ClearControls()
        {
            dgvContactAddress.DataSource = null;
            dgvDocuments.DataSource = null;
            dgvCustmerSales.DataSource = null;
            dgvCustomerItems.DataSource = null;
            dgvCustomerPayments.DataSource = null;
            dgvSupplierItems.DataSource = null;
            dgvSupplierPayments.DataSource = null;
            dgvSupplierPurchase.DataSource = null;
            lblNameValue.Text = null;
            lblCodeValue.Text = null;
            lblEmailValue.Text = null;
            lblTransactionTypeValue.Text = null;
            lblGenderOrSupClassValue.Text = null;
            lblRatingValue.Text = null;
            lblVendorStatusValue.Text = null;
            if (CboAssociative.SelectedIndex == 0)
            {
                this.Text = "Customer History";
            }
            else
            {
                this.Text = "Supplier History";
            }
        }

       private void CboAssociative_SelectionChangeCommitted(object sender, EventArgs e)
       {
           ClearControls();
       }

       private void pnlExpandVendorHistory_ExpandedChanged(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
       {
           MintPanalVisible = 1;
       }

       private void pnlExpandVendorHistory_ExpandedChanging(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
       {
            MintPanalVisible = 0;
       }
 

 


    
     
    }
}
