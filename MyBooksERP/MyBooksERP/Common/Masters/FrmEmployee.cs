﻿using System;
//using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
//using System.Drawing.Imaging;

using System.IO;
/* 
=================================================
Author      :		 <Author,,Sawmya>
Create date :        <Create Date,22 Feb 2011>
Description :	     <Description,, EmployeeInformation Form>
Modified By :		 <Laxmi>
Modified Date :      <06 Apr 2012>
================================================
*/
namespace MyBooksERP
{
    public partial class FrmEmployee :Form
    {
        #region Private variables
        private bool MblnAddStatus; //Add/Update mode 

        // Permissions

       
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnAddUpdatePermission = false; // To set add/update permission
        private bool MblnPrintEmailPermission = false;//To Set PrintEmail Permission


        public bool MblnIsFromExistingForm = false;
        private bool MblnIsEditMode = false;
        private bool MblnChangeStatus;
        //private bool PhotoAttach;
        private int MintCurrentRecCnt;    // Current recourd count
        private int MintRecordCnt;      // Total record count
        private int MintEmployeeID;
        private int MintTimerInterval;    // Time interval of the timer
        private int MintRowNumber = 0;    // Row number
        //private int MintCompanyId;        // Company identification
        //private int MintUserId;         // User identification

        private string MsMessageCommon;
        private string MsMessageCaption;
        //private string RemoveRecPicture;
        //private string RemocePassPicture;
        private string RecentFilePath;
        private string PassportFilPath;
       
        //private string strEmployeeCodePrefix;  //Generate Employee code
        private Image imgImageFile;

        private ArrayList MsMessageArray;  // Error Message display
        private ArrayList MaStatusMessage;

        private DateTime dDOJ ;

        public int PintEmployeeID = 0; //To Set EmployeeID Externally
        private int iLeavePolicyID = 0; // Assigning leave policy to this variable for checking

        ClsBLLEmployeeInformation MObjClsBLLEmployeeInformation;
        ClsLogWriter MObjClsLogWriter;      // Object of the LogWriter class
        ClsNotification MObjClsNotification; // Object of the Notification class
        ClsCommonUtility MobjCommonUtlity;
        MessageBoxIcon MsMessageBoxIcon;
        #endregion // Private variables

        #region Constructor

        public FrmEmployee()
        {
            InitializeComponent();
            // Setting values to the variables
            MintTimerInterval = ClsCommonSettings.TimerInterval; // current companyid
            MsMessageCaption = ClsCommonSettings.MessageCaption; //Message caption
            MObjClsBLLEmployeeInformation = new ClsBLLEmployeeInformation();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);   // Application startup path
            MObjClsNotification = new ClsNotification();
            TmEmployee.Interval = ClsCommonSettings.TimerInterval; // Set time interval of the timer
            MobjCommonUtlity = new ClsCommonUtility();
           // strEmployeeCodePrefix = ClsCommonSettings.strEmployeeNumberPrefix; //set employeenumber for codeprefix
        }
        #endregion // Constructor

        private void FrmEmployee_Load(object sender, EventArgs e)
        {
            try
            {
                SetPermissions();
                LoadCombos(0);            // Method for loading comboboxes  
                LoadMessage();
                AddNewItem();
                dtpDateofBirth.MaxDate = ClsCommonSettings.GetServerDate();
                CboSearchOption.SelectedIndex = 2;

                if (PintEmployeeID > 0)
                {
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID = PintEmployeeID;
                    MintCurrentRecCnt = MObjClsBLLEmployeeInformation.GetRowNumber();
                    DisplayEmployeeInformation();
                    SetBindingNavigatorButtons();
                    MblnAddStatus = false;
                }

                if (!ClsMainSettings.PayrollEnabled)
                    TbEmployee.TabPages.Remove(tpPayroll);
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form Load:FrmEmployee_Load " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmEmployee_Load() " + Ex.Message.ToString());
            }
        }

        private void LoadMessage()
        {
            MsMessageArray = new ArrayList();
            MaStatusMessage = new ArrayList();
            MsMessageArray = MObjClsNotification.FillMessageArray((int)FormID.Employee, ClsCommonSettings.ProductID);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.Employee, ClsCommonSettings.ProductID);
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.General , (Int32)eMenuID.Employee, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void ChangeStatus()
        {            
            if (!MblnIsEditMode)
            {
                BtnOk.Enabled = BtnSave.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = BtnClear.Enabled = MblnAddPermission;
            }
            else
            {
                BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
                BindingNavigatorSaveItem.Enabled = BtnOk.Enabled = BtnSave.Enabled = BtnClear.Enabled = MblnUpdatePermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            }

            ErrEmployee.Clear();
        }

        private void changestatus(object sender, EventArgs e)
        {
            this.SetAutoCompleteList();
            ChangeStatus();                       
        }

        private void Cbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        private void SetBindingNavigatorButtons()
        {
            BindingNavigatornPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
            BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";

            BindingNavigatorMoveNextItem.Enabled = true; ;
            BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveFirstItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatornPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
            }
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            BtnPrint.Enabled = MblnPrintEmailPermission;
            BtnEmail.Enabled = MblnPrintEmailPermission;
            BtnClear.Enabled = MblnUpdatePermission;
            BtnSave.Enabled = false;
            BtnOk.Enabled = false;
            BindingNavigatorSaveItem.Enabled = false;
        }

        /// <summary>
        /// Enable/disable buttons according to the action
        /// </summary>
        private void EnableDisableButtons(bool bEnable)
        {
            BindingNavigatorAddNewItem.Enabled = !bEnable;
            BindingNavigatorDeleteItem.Enabled = !bEnable;
            bnMoreActions.Enabled = !bEnable;          

            if (bEnable)
            {
                BtnPrint.Enabled = !bEnable;
                BtnEmail.Enabled = !bEnable;
                BindingNavigatorSaveItem.Enabled = !bEnable;
                BtnOk.Enabled = !bEnable;
                BtnClear.Enabled = !bEnable;
                BtnSave.Enabled = !bEnable;
                MblnChangeStatus = !bEnable;
            }
            else
            {
                BtnPrint.Enabled = MblnPrintEmailPermission;
                BtnEmail.Enabled = MblnPrintEmailPermission;
                BindingNavigatorSaveItem.Enabled = bEnable;
                BtnOk.Enabled = bEnable;
                BtnClear.Enabled = bEnable;
                BtnSave.Enabled = bEnable;
                MblnChangeStatus = bEnable;
            }
        }

        /// <summary>
        /// Setting the form for adding new Employee
        /// </summary>
        /// <returns></returns>
        private bool AddNewItem()
        {
            try
            {               
                MblnAddStatus = true;
                MblnIsEditMode = false;
                SetEnable();    // Clear all fields in the form
                MintEmployeeID = 0;
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strPassportPhotoPath = null;
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strRecentPhotoPath= null;

                if (File.Exists(Application.StartupPath + "\\Images\\sample.png"))
                    PassportPhotoPictureBox.Image = Image.FromFile(Application.StartupPath + "\\Images\\sample.png");
                
                MintRowNumber = 0;
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.AddNew, out MsMessageBoxIcon);
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                DisplayRecordCount();
                BindingAddNew();
                cboWorkingAt.SelectedValue = ClsCommonSettings.LoginCompanyID;
                GenerateEmployeeCode();
                EnableDisableButtons(true);
                cboWorkingAt.Enabled = true;
                TbEmployee.SelectedTab = TpGeneral;
                CboSalutation.Select();
                cboWorkPolicy.SelectedIndex = cboLeavePolicy.SelectedIndex = cboVacationPolicy.SelectedIndex = cboSettlementPolicy.SelectedIndex = -1;
                cboVisafrom.SelectedIndex = -1;
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form AddNewItem:AddNewItem " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewItem() " + Ex.Message.ToString());
                return false;
            }
        }

        private void DisplayRecordCount()//Dispalying Total Record Count
        {
            MintRecordCnt = MObjClsBLLEmployeeInformation.RecCountNavigate();
        }

        private void GenerateEmployeeCode()
        {
            try
            {
                clsBLLLogin objClsBLLLogin = new clsBLLLogin();
                DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboWorkingAt.SelectedValue));

                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intCompanyID = Convert.ToInt32(cboWorkingAt.SelectedValue);
                TxtEmployeeNumber.Text = ClsCommonSettings.strEmployeeNumberPrefix + (MObjClsBLLEmployeeInformation.GenerateEmployeeCode() + 1).ToString();

                if (ClsCommonSettings.blnEmployeeNumberAutogenerate)
                    TxtEmployeeNumber.ReadOnly = false;
                else
                    TxtEmployeeNumber.ReadOnly = true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in GenerateEmployeeCode() " + ex.Message);
                MObjClsLogWriter.WriteLog("Error in GenerateEmployeeCode) " + ex.Message, 2);
            }
        }

        private void BindingAddNew()
        {
            BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt + 1) + "";
            BindingNavigatornPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
            MintCurrentRecCnt = MintRecordCnt + 1;
            if (MintCurrentRecCnt == 1)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                BindingNavigatorMoveFirstItem.Enabled = true;
                BindingNavigatorMovePreviousItem.Enabled = true;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
        }

        public void GetFullName()
        {
            txtEmployeeFullName.Text = TxtFirstName.Text.Trim() + " " + TxtMiddleName.Text.Trim() + " " + TxtLastName.Text.Trim();
        }

        /// <summary>
        /// Clearing fields in the form
        /// </summary>
        /// 
        private void SetEnable()
        {
            // Setting default values            
            rdbMale.Checked = true;
            TxtFirstName.Text = "";
            TxtMiddleName.Text = "";
            TxtLastName.Text = "";
            txtEmployeeFullName.Text = "";
            txtEmployeeFullName.Text = "";          
            TxtOfficialEmail.Text = "";
            txtEmail.Text = "";
            cboCountryoforgin.Text = "";
            cboCountryoforgin.SelectedIndex = -1;
            txtLocalAddress.Text = "";
            txtMobile.Text = "";
            txtLocalPhone.Text = "";
            DtpDateofJoining.Value = DateTime.Today;
            dtpDateofBirth.Value = DateTime.Today;
            CboSalutation.SelectedIndex = -1;

            if (ClsCommonSettings.CompanyID <= 0)
                ClsCommonSettings.CompanyID = MobjCommonUtlity.GetCompanyID();

            cboWorkingAt.SelectedValue  = ClsCommonSettings.LoginCompanyID;
            
           if (cboWorkStatus.Items.Count >=6) cboWorkStatus.SelectedValue = 6;
           
            cboDepartment.SelectedIndex = -1;
            cboDesignation.SelectedIndex = -1;
            cboEmploymentType.Text = "";                
            cboEmploymentType.SelectedIndex = -1;
            cboNationality.Text = "";
            cboReligion.Text = "";
            cboNationality.SelectedIndex = -1;
            cboReligion.SelectedIndex = -1;           
            ErrEmployee.Clear();

            cboWorkPolicy.SelectedIndex = -1;
            cboLeavePolicy.SelectedIndex = -1;

            BankNameReferenceComboBox.SelectedIndex = -1;
            CboEmployeeBankname.SelectedIndex = -1;
            AccountNumberTextBox.Text = "";
            ComBankAccIdComboBox.SelectedIndex = -1;
            // Disable buttons
            EnableDisableButtons(false);
            TxtSearchText.Text = "";
            PassportPhotoPictureBox.Image = null;
            PassportPhotoPictureBox.Refresh();
            TxtEmployeeNumber.Focus();
        }

        /// <summary>
        /// Loading comboboxes with values
        /// </summary>
        /// <returns></returns>
        /// 
        private bool LoadCombos(int intType)
        {
            try
            {
                DataTable datCombos = new DataTable();

                if (intType == 0 || intType == 1)
                {
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "DepartmentID, Department", "DepartmentReference", "" });
                    cboDepartment.ValueMember = "DepartmentID";
                    cboDepartment.DisplayMember = "Department";
                    cboDepartment.DataSource = datCombos;
                }

                if (intType == 0 || intType == 2)
                {
                    datCombos = null;
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "DesignationID, Designation", "DesignationReference", "" });
                    cboDesignation.ValueMember = "DesignationID";
                    cboDesignation.DisplayMember = "Designation";
                    cboDesignation.DataSource = datCombos;
                }

                if (intType == 0 || intType == 4)
                {
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "ReligionID, Religion", "ReligionReference", "" });
                    cboReligion.ValueMember = "ReligionID";
                    cboReligion.DisplayMember = "Religion";
                    cboReligion.DataSource = datCombos;
                }

                if (intType == 0 || intType == 5)
                {
                    datCombos = null;
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "NationalityID, Nationality", "NationalityReference", "" });
                    cboNationality.ValueMember = "NationalityID";
                    cboNationality.DisplayMember = "Nationality";
                    cboNationality.DataSource = datCombos;
                }

                if (intType == 0 || intType == 6)
                {
                    datCombos = null;
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "SalutationID, Salutation", "SalutationReference", "" });
                    CboSalutation.ValueMember = "SalutationID";
                    CboSalutation.DisplayMember = "Salutation";
                    CboSalutation.DataSource = datCombos;
                }

                if (intType == 0 || intType == 7)
                {
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID });
                    cboWorkingAt.ValueMember = "CompanyID";
                    cboWorkingAt.DisplayMember = "CompanyName";
                    cboWorkingAt.DataSource = datCombos;
                }

                if (intType == 0 || intType == 10)
                {
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "WorkStatusID, WorkStatus", "WorkStatusReference", "" });
                    cboWorkStatus.ValueMember = "WorkStatusID";
                    cboWorkStatus.DisplayMember = "WorkStatus";
                    cboWorkStatus.DataSource = datCombos;
                }
                if (intType == 0 || intType == 11)
                {
                    datCombos = null;
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "TransactionTypeID, TransactionType", "TransactionTypeReference", "" });
                    TransactionTypeReferenceComboBox.ValueMember = "TransactionTypeID";
                    TransactionTypeReferenceComboBox.DisplayMember = "TransactionType";
                    TransactionTypeReferenceComboBox.DataSource = datCombos;
                }
                         
                if (intType == 0 || intType == 16)
                {
                    datCombos = null;
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "EmploymentTypeID,EmploymentType ", "EmploymentTypeReference", "" });
                    cboEmploymentType.ValueMember = "EmploymentTypeID";
                    cboEmploymentType.DisplayMember = "EmploymentType";
                    cboEmploymentType.DataSource = datCombos;
                }

                if (intType == 0 || intType == 17)
                {
                    datCombos = null;
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "CountryID, CountryName", "CountryReference", "" });
                    cboCountryoforgin.ValueMember = "CountryID";
                    cboCountryoforgin.DisplayMember = "CountryName";
                    cboCountryoforgin.DataSource = datCombos;
                }
                if (intType == 0 || intType == 18)
                {
                    datCombos = null;
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "WorkPolicyID, WorkPolicy", "PayWorkPolicyMaster", "" });
                    cboWorkPolicy.ValueMember = "WorkPolicyID";
                    cboWorkPolicy.DisplayMember = "WorkPolicy";
                    cboWorkPolicy.DataSource = datCombos;
                }
                if (intType == 0 || intType == 19)
                {
                    datCombos = null;
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "LeavePolicyID, LeavePolicyName", "PayLeavePolicyMaster", "" });
                    cboLeavePolicy.ValueMember = "LeavePolicyID";
                    cboLeavePolicy.DisplayMember = "LeavePolicyName";
                    cboLeavePolicy.DataSource = datCombos;
                }
                if (intType == 0 || intType == 20)
                {
                    datCombos = null;
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "BankBranchReference.BankBranchID, isnull(BankReference.BankName,'')+ ' - ' + isnull(BankBranchReference.BankBranchName,'')   as Description  ", "BankBranchReference INNER JOIN BankReference ON BankReference.BankID = BankBranchReference.BankID ORDER BY BankBranchReference.BankBranchName  ", "" });
                    CboEmployeeBankname.ValueMember = "BankBranchID";
                    CboEmployeeBankname.DisplayMember = "Description";
                    CboEmployeeBankname.DataSource = datCombos;
                }

                if (intType == 0 || intType == 21)
                {
                    datCombos = null;
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "VacationPolicyID, VacationPolicy", "PayVacationPolicy", "" });
                    cboVacationPolicy.ValueMember = "VacationPolicyID";
                    cboVacationPolicy.DisplayMember = "VacationPolicy";
                    cboVacationPolicy.DataSource = datCombos;
                }

                if (intType == 0 || intType == 22)
                {
                    datCombos = null;
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "SetPolicyID, DescriptionPolicy", "PaySettlementPolicy", "" });
                    cboSettlementPolicy.ValueMember = "SetPolicyID";
                    cboSettlementPolicy.DisplayMember = "DescriptionPolicy";
                    cboSettlementPolicy.DataSource = datCombos;
                }

                if (intType == 0 || intType == 23)
                {
                    datCombos = null;
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                    cboVisafrom.ValueMember = "CompanyID";
                    cboVisafrom.DisplayMember = "CompanyName";
                    cboVisafrom.DataSource = datCombos;
                }
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
                return false;
            }
        }

        /// <summary>
        /// Validating Employee information
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmployeeFields()
        {
            // Employee number
            if (!TxtEmployeeNumber.ReadOnly && MObjClsBLLEmployeeInformation.CheckDuplication(MblnAddStatus, new string[] { TxtEmployeeNumber.Text.Replace("'", "").Trim() }, MintEmployeeID,cboWorkingAt.SelectedValue.ToInt32()))
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 413, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(TxtEmployeeNumber, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmEmployee.Enabled = true;
                TxtEmployeeNumber.Focus();
                return false;
            }
            if (TxtEmployeeNumber.Text.Trim().Length == 0)
            {
                //MsMessageCommon = "Please enter First Name";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.EmpNoValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(TxtEmployeeNumber, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TxtEmployeeNumber.Focus();
                return false;
            }
            // Salutation
            if (Convert.ToInt32(CboSalutation.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select Salutation";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.SalutationValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(CboSalutation, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                CboSalutation.Focus();
                return false;
            }
            // First name
            if (TxtFirstName.Text.Trim().Length == 0)
            {
                //MsMessageCommon = "Please enter First Name";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.FirstNameValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(TxtFirstName, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TxtFirstName.Focus();
                return false;
            }
            // Working company
            if (Convert.ToInt32(cboWorkingAt.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select Working At";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.WorkingAtValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(cboWorkingAt, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                cboWorkingAt.Focus();
                return false;
            }

            if (Convert.ToInt32(cboWorkStatus.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select Working At";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.WorkstatusValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(cboWorkStatus, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                cboWorkStatus.Focus();
                return false;
            }
    
            // Department
            if (Convert.ToInt32(cboDepartment.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select Department";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.DepartmentValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(cboDepartment, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                cboDepartment.Focus();
                return false;
            }
            // Designation
            if (Convert.ToInt32(cboDesignation.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select Designation";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.DesignationValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(cboDesignation, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                cboDesignation.Focus();
                return false;
            }

            if (TxtOfficialEmail.Text.Trim().Length > 0 && !MObjClsBLLEmployeeInformation.CheckValidEmail(TxtOfficialEmail.Text.Trim()))
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 411, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(TxtOfficialEmail, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                TxtOfficialEmail.Focus();
                return false;
            }
            if (txtEmail.Text.Trim().Length > 0 && !MObjClsBLLEmployeeInformation.CheckValidEmail(txtEmail.Text.Trim()))
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 411, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(txtEmail, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TbContacts;
                txtEmail.Focus();
                return false;
            }

            //Age should be greater than or equal to 18 years.
            if (Convert.ToDateTime(DtpDateofJoining.Value.ToString()) < Microsoft.VisualBasic.DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Year, 18, dtpDateofBirth.Value.Date))
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 431, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(dtpDateofBirth, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                dtpDateofBirth.Focus();
                return false;
            }
            //The joining date cannot be before the 'Date of Birth'.(c)
            if (Convert.ToDateTime(DtpDateofJoining.Value.ToString()) < Convert.ToDateTime(dtpDateofBirth.Value.ToString()))
            {

                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 432, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(DtpDateofJoining, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                DtpDateofJoining.Focus();
                return false;
            }

            //The 'Date of Joining' should be less than the 'Current Date'.
            if (Convert.ToDateTime(DtpDateofJoining.Value.Date.ToString()) > ClsCommonSettings.GetServerDate().Date)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 433, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(DtpDateofJoining, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                DtpDateofJoining.Focus();
                return false;
            }
            string strJoingDate=DtpDateofJoining.Value.Date.ToString("dd MMM yyyy");
            string strFinYear=MObjClsBLLEmployeeInformation.GetCompanyFinYearStartDate(cboWorkingAt.SelectedValue.ToInt32());
            if (strFinYear != "" && strJoingDate != "")
            {

                if (Convert.ToDateTime(strJoingDate) < Convert.ToDateTime(strFinYear))
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 445, out MsMessageBoxIcon);

                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    ErrEmployee.SetError(DtpDateofJoining, MsMessageCommon.Replace("#", "").Trim());
                    LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TbEmployee.SelectedTab = TpGeneral;
                    DtpDateofJoining.Focus();
                    return false;

                }
            }
            if (Convert.ToInt32(cboCountryoforgin.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select Religion";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.CountryoforginValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(cboCountryoforgin, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                cboCountryoforgin.Focus();
                return false;
            }           
            // Nationality
            if (Convert.ToInt32(cboNationality.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select Nationality";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.NationalityValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(cboNationality, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                cboNationality.Focus();
                return false;
            }
            // Religion
            if (Convert.ToInt32(cboReligion.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select Religion";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.ReligionValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(cboReligion, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                cboReligion.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// Validating and saving item master details
        /// </summary>
        private void SaveEmployeeInformation()
        {
            // Validation for the fields
            if (ValidateEmployeeFields())
            {
                // Saving new employee details
                if (SaveEmployeeDetails())
                {                    
                    string strMsg = "";

                    if (MblnAddStatus == true)//insert
                        strMsg = MObjClsNotification.GetErrorMessage(MsMessageArray, 2, out MsMessageBoxIcon);
                    else
                        strMsg = MObjClsNotification.GetErrorMessage(MsMessageArray, 21, out MsMessageBoxIcon);

                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                    BindingNavigatorMovePreviousItem_Click(null, null);
                    LblEmployeeStatus.Text = strMsg.Remove(0, strMsg.IndexOf("#") + 1);
                    TmEmployee.Enabled = true;
                }
            }
        }

        /// <summary>
        /// Setting values to the Employee object and saves
        /// </summary>
        /// <returns></returns>
        private bool SaveEmployeeDetails()
        {
            try
            {
                if (MblnAddStatus == true)//insert
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 1, out MsMessageBoxIcon);
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID = 0;
                }
                else
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 3, out MsMessageBoxIcon);
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID = MintEmployeeID;
                }

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;

                if (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID == 0 && MObjClsBLLEmployeeInformation.CheckDuplication(MblnAddStatus, new string[] { TxtEmployeeNumber.Text.Replace("'", "").Trim() }, MintEmployeeID, cboWorkingAt.SelectedValue.ToInt32()))//&& TxtEmployeeNumber.ReadOnly
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 9002, out MsMessageBoxIcon);
                    MsMessageCommon = MsMessageCommon.Replace("* no", "Employee No");
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        GenerateEmployeeCode();
                }
                else if (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID > 0 && MObjClsBLLEmployeeInformation.CheckDuplication(MblnAddStatus, new string[] { TxtEmployeeNumber.Text.Replace("'", "").Trim() }, MintEmployeeID, cboWorkingAt.SelectedValue.ToInt32()))
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 413, out MsMessageBoxIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    ErrEmployee.SetError(TxtEmployeeNumber, MsMessageCommon.Replace("#", "").Trim());
                    LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmEmployee.Enabled = true;
                    TxtEmployeeNumber.Focus();
                    return false;
                }

                // Setting values to the business object
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID = MintEmployeeID;
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeNumber = TxtEmployeeNumber.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intSalutation = Convert.ToInt32(CboSalutation.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strFirstName = TxtFirstName.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLastName = TxtLastName.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strMiddleName = TxtMiddleName.Text.Trim();                
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeFullName = Convert.ToString(txtEmployeeFullName.Text.Trim());
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intCompanyID = Convert.ToInt32(cboWorkingAt.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmploymentType = Convert.ToInt32(cboEmploymentType.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intWorkStatusID = Convert.ToInt32(cboWorkStatus.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intDepartmentID = Convert.ToInt32(cboDepartment.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intDesignationID = Convert.ToInt32(cboDesignation.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strGender = rdbMale.Checked == true ? "Male" : "Female";
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intCountryofOriginID = Convert.ToInt32(cboCountryoforgin.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intNationalityID = Convert.ToInt32(cboNationality.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intReligionID = Convert.ToInt32(cboReligion.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strOfficialEmail = TxtOfficialEmail.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.dtDateofBirth = dtpDateofBirth.Value.ToString("dd MMM yyyy").ToDateTime();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.dtDateofJoining = DtpDateofJoining.Value.ToString("dd MMM yyyy").ToDateTime();

                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strAddress = txtPermanentAddress.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strPhone = txtPhone.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strMobile = txtMobile.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmail = txtEmail.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLocalAddress = txtLocalAddress.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLocalPhone = txtLocalPhone.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmergencyContactAddress = txtEmergencyAddress.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmergencyContactPhone = txtEmergencyPhone.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strNotes = txtRemarks.Text.Trim();

                // Payroll

                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intPolicyID = cboWorkPolicy.SelectedValue.ToInt32();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intLeavePolicyID = cboLeavePolicy.SelectedValue.ToInt32();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intVacationPolicyID = cboVacationPolicy.SelectedValue.ToInt32();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intSettlementPolicyID = cboSettlementPolicy.SelectedValue.ToInt32();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intVisaObtainedFrom = cboVisafrom.SelectedValue.ToInt32();

                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intTransactionTypeID = TransactionTypeReferenceComboBox.SelectedValue.ToInt32();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intBankNameID = BankNameReferenceComboBox.SelectedValue.ToInt32();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intComBankAccId = ComBankAccIdComboBox.SelectedValue.ToInt32();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intBankBranchID = CboEmployeeBankname.SelectedValue.ToInt32();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strAccountNumber = AccountNumberTextBox.Text.Trim();                
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strPersonID = txtPersonID.Text.Trim();
                if (PassportPhotoPictureBox.Image != null)
                {
                    ClsImages GetByteImage = new ClsImages();
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strPassportPhotoPath = GetByteImage.GetImageDataStream(PassportPhotoPictureBox.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                else
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strPassportPhotoPath = null;

                if (MObjClsBLLEmployeeInformation.SaveEmployeeInformation(MblnAddStatus))
                {
                    MintEmployeeID = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on Save:SaveEmployeeDetails() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Saving SaveEmployeeDetails()" + Ex.Message.ToString());
                return false;
            }
        }

        /// <summary>
        /// Displaying item details of the selected row number
        /// </summary>
        private void DisplayEmployeeInformation()
        {
            if (MObjClsBLLEmployeeInformation.DisplayEmployeeInformation(MintCurrentRecCnt, null))
                SetEmployeeInformation();
        }

        /// <summary>
        /// Getting Employee information from the object
        /// </summary>
        private void SetEmployeeInformation()
        {
            MintEmployeeID = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID;
            dtpDateofBirth.Value = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.dtDateofBirth;
            DtpDateofJoining.Value = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.dtDateofJoining;            
            rdbFemale.Checked = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strGender == "Male" ? false : true;
            rdbMale.Checked = !rdbFemale.Checked;
            TxtEmployeeNumber.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeNumber;
            cboDepartment.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intDepartmentID;
            cboDesignation.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intDesignationID;
            TxtOfficialEmail.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strOfficialEmail;
            txtEmployeeFullName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeNumber;
            TxtFirstName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strFirstName;
            TxtLastName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLastName;
            TxtMiddleName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strMiddleName;
            CboSalutation.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intSalutation;
            cboNationality.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intNationalityID;
            cboReligion.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intReligionID;
            txtEmployeeFullName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeFullName;
            MblnAddStatus = false;
            cboWorkingAt.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intCompanyID;
            cboWorkingAt.Enabled = false;    
            cboEmploymentType.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmploymentType;
            txtEmployeeFullName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeFullName;
            cboCountryoforgin.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intCountryofOriginID;

            txtPermanentAddress.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strAddress;
            txtPhone.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strPhone;
            txtMobile.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strMobile;
            txtEmail.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmail;
            txtLocalAddress.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLocalAddress;
            txtLocalPhone.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLocalPhone;
            txtEmergencyAddress.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmergencyContactAddress;
            txtEmergencyPhone.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmergencyContactPhone;
            txtRemarks.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strNotes;

            if (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strPassportPhotoPath != null)
            {
                ClsImages objimg = new ClsImages();
                PassportPhotoPictureBox.Image = objimg.GetImage(MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strPassportPhotoPath);
            }
            else if (File.Exists(Application.StartupPath + "\\Images\\sample.png"))
                PassportPhotoPictureBox.Image = Image.FromFile(Application.StartupPath + "\\Images\\sample.png");
            
            cboWorkStatus.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intWorkStatusID;
            FillPolicyCombo(Convert.ToInt32(cboWorkingAt.SelectedValue));

            cboWorkPolicy.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intPolicyID;
            cboLeavePolicy.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intLeavePolicyID;
            cboVacationPolicy.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intVacationPolicyID;
            cboSettlementPolicy.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intSettlementPolicyID;
            cboVisafrom.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intVisaObtainedFrom;

            TransactionTypeReferenceComboBox.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intTransactionTypeID;
            SetAccountBankDetails();

            BankNameReferenceComboBox.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intBankNameID;
            CboEmployeeBankname.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intBankBranchID;
            AccountNumberTextBox.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strAccountNumber;
            ComBankAccIdComboBox.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intComBankAccId;
            txtPersonID.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strPersonID;
            dDOJ = DtpDateofJoining.Value;
            // Enable buttons
            EnableDisableButtons(false);
            TxtEmployeeNumber.Focus();
            MblnAddStatus = false;
        }
        private void SetAccountBankDetails()
        {
            if (TransactionTypeReferenceComboBox.Text.ToUpper() == "BANK" ||
                    TransactionTypeReferenceComboBox.Text.ToUpper() == "WPS")
            {
                BankNameReferenceComboBox.BackColor = Color.LightYellow;
                CboEmployeeBankname.BackColor = Color.LightYellow;
                ComBankAccIdComboBox.BackColor = Color.LightYellow;
                AccountNumberTextBox.BackColor = Color.LightYellow;
            }
            else
            {
                BankNameReferenceComboBox.BackColor = Color.White;
                CboEmployeeBankname.BackColor = Color.White;
                ComBankAccIdComboBox.BackColor = Color.White;
                AccountNumberTextBox.BackColor = Color.White;
            }

            DataTable datCombos = new DataTable();

            if (TransactionTypeReferenceComboBox.Text != null)
            {
                if (Convert.ToInt32(cboWorkingAt.SelectedValue) != 0)
                {
                    BankNameReferenceComboBox.Enabled = true;
                    ComBankAccIdComboBox.Enabled = true;
                    AccountNumberTextBox.Enabled = true;
                    BankNameReferenceComboBox.DataSource = null;
                    CboEmployeeBankname.DataSource = null;

                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "bnr.BankBranchID,(BB.BankName + '-' + bnr.BankBranchName) as Description", "BankBranchReference bnr  LEFT OUTER JOIN BankReference BB on bnr.BankID=BB.BankID left outer join CompanyBankAccountDetails abd on bnr.BankBranchID=abd.BankBranchID ", "CompanyID=" + cboWorkingAt.SelectedValue + "" });
                    BankNameReferenceComboBox.ValueMember = "BankBranchID";
                    BankNameReferenceComboBox.DisplayMember = "Description";
                    BankNameReferenceComboBox.DataSource = datCombos;

                    datCombos = new DataTable();

                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "BankBranchReference.BankBranchID,isnull(BankReference.BankName,'')+ ' - ' + isnull(BankBranchReference.BankBranchName,'')   as Description", "BankBranchReference INNER JOIN BankReference ON BankBranchReference.BankID = BankReference.BankID ORDER BY BankBranchReference.BankBranchName", "" });   // CboEmployeeBankname, "SELECT   ,  ", "BankBranchID", "BankBranch");
                    CboEmployeeBankname.ValueMember = "BankBranchID";
                    CboEmployeeBankname.DisplayMember = "Description";
                    CboEmployeeBankname.DataSource = datCombos;
                }

                ComBankAccIdComboBox.DataSource = null;
                AccountNumberTextBox.Text = "";
                BankNameReferenceComboBox.Enabled = true;
                AccountNumberTextBox.Enabled = true;
            }

            if (TransactionTypeReferenceComboBox.Text.ToUpper() == "WPS")
                txtPersonID.BackColor = Color.LightYellow;
            else
                txtPersonID.BackColor = Color.White;


            if (BankNameReferenceComboBox.Items.Count > 0)
                BankNameReferenceComboBox.SelectedIndex = -1;

            if (CboEmployeeBankname.Items.Count > 0)
                CboEmployeeBankname.SelectedIndex = -1;

            if (ComBankAccIdComboBox.Items.Count > 0)
                ComBankAccIdComboBox.SelectedIndex = -1;

        }
        private void FillPolicyCombo(int iComID)
        {
            DataTable dtPolicy = new DataTable();

            cboWorkPolicy.DataSource = null;
            cboLeavePolicy.DataSource = null;

            string sQuery = string.Empty;
            sQuery = "SELECT WorkPolicyID,WorkPolicy FROM PayWorkPolicyMaster ";

            //if (cboEmploymentType.Text != "" && Convert.ToInt32(cboEmploymentType.SelectedValue) != 0)
            //    sQuery = sQuery + " And(EmploymentTypeID = " + Convert.ToInt32(cboEmploymentType.SelectedValue) + " OR ISNULL(EmploymentTypeID,0)=0) ";

            //if (cboDepartment.Text != "" && Convert.ToInt32(cboDepartment.SelectedValue) != 0)
            //    sQuery = sQuery + " And(DepartmentID = " + Convert.ToInt32(cboDepartment.SelectedValue) + " OR ISNULL(DepartmentID,0)=0) ";
            //if (cboDesignation.Text != "" && Convert.ToInt32(cboDesignation.SelectedValue) != 0)

            //    sQuery = sQuery + " And(DesignationID = " + Convert.ToInt32(cboDesignation.SelectedValue) + " OR ISNULL(DesignationID,0)=0) ";

            //sQuery = sQuery + " UNION SELECT WorkPolicyID,WorkPolicy FROM PayWorkPolicyMaster WHERE WorkPolicyID=(SELECT WorkPolicyID FROM EmployeeMaster WHERE EmployeeID=" + Convert.ToInt32(TxtEmployeeNumber.Tag) + " )";


            dtPolicy = MObjClsBLLEmployeeInformation.FillPolicyCombo(sQuery);

            cboWorkPolicy.DataSource = dtPolicy;
            cboWorkPolicy.DisplayMember = "WorkPolicy";
            cboWorkPolicy.ValueMember = "WorkPolicyID";

            dtPolicy = new DataTable();

            sQuery = string.Empty;
            sQuery = "SELECT LeavePolicyID,LeavePolicyName FROM PayLeavePolicyMaster ";

            //if (cboEmploymentType.Text != "" && Convert.ToInt32(cboEmploymentType.SelectedValue) != 0)
            //    sQuery = sQuery + " And(EmploymentTypeID = " + Convert.ToInt32(cboEmploymentType.SelectedValue) + " OR ISNULL(EmploymentTypeID,0)=0) ";

            //if (cboDepartment.Text != "" && Convert.ToInt32(cboDepartment.SelectedValue) != 0)
            //    sQuery = sQuery + " And(DepartmentID = " + Convert.ToInt32(cboDepartment.SelectedValue) + " OR ISNULL(DepartmentID,0)=0) ";
            //if (cboDesignation.Text != "" && Convert.ToInt32(cboDesignation.SelectedValue) != 0)

            //    sQuery = sQuery + " And(DesignationID = " + Convert.ToInt32(cboDesignation.SelectedValue) + " OR ISNULL(DesignationID,0)=0) ";

            //sQuery = sQuery + " UNION SELECT LeavePolicyID,LeavePolicyName FROM PayLeavePolicyMaster WHERE LeavePolicyID=(SELECT LeavePolicyID FROM EmployeeMaster WHERE EmployeeID=" + Convert.ToInt32(TxtEmployeeNumber.Tag) + " )";

            dtPolicy = MObjClsBLLEmployeeInformation.FillPolicyCombo(sQuery);

            cboLeavePolicy.DataSource = dtPolicy;
            cboLeavePolicy.DisplayMember = "LeavePolicyName";
            cboLeavePolicy.ValueMember = "LeavePolicyID";

        }


        /// <summary>
        /// Getting previous/first Employee Information
        /// </summary>
        /// <param name="bFirstItem"></param>
        private void GetPreviousEmployeeInformation(bool blnFirstItem)
        {
            DisplayRecordCount();
            if (MintRowNumber > 0)
            {
                if (blnFirstItem)
                    MintRowNumber = 1;
                else
                    MintRowNumber--;

                EventArgs e = new EventArgs();
                BnSearchButton_Click(BtnSearch, e);
            }
            else
            {
                // Getting previous item in the details
                DisplayRecordCount();
                if (MintRecordCnt > 0)
                {
                    if (blnFirstItem)
                    {
                        MintCurrentRecCnt = 1;
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 9, out MsMessageBoxIcon);
                    }
                    else
                    {
                        MintCurrentRecCnt = MintCurrentRecCnt - 1;

                        if (MintCurrentRecCnt <= 0)
                            MintCurrentRecCnt = 1;

                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 10, out MsMessageBoxIcon);
                    }
                }
                else
                    BindingNavigatornPositionItem.Text = "of 0";

                DisplayEmployeeInformation();
                BindingNavigatornPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt);
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                SetBindingNavigatorButtons();                 
                TxtSearchText.Clear();
                MintRowNumber = 0;
                TbEmployee.SelectedTab = TpGeneral;              
            }
        }

        /// <summary>
        /// Getting previous/first Employee Information
        /// </summary>
        /// <param name="bFirstItem"></param>
        private void GetNextExecutiveInformation(bool bLastItem)
        {
            
            if (MintRowNumber > 0)    // Getting next item in the search results
            {
                if (bLastItem)
                    MintRowNumber = MintRecordCnt;
                else if (Convert.ToInt32(BindingNavigatornPositionItem.Text) < MintRecordCnt)
                    MintRowNumber++;

                EventArgs e = new EventArgs();
                // Calling searching event
                BnSearchButton_Click(BnSearch, e);
            }
            else
            {
                DisplayRecordCount();
                MblnAddStatus = false;
                ErrEmployee.Clear();
                DisplayRecordCount();

                if (MintRecordCnt > 0)
                {
                    if (bLastItem)
                    {
                        MintCurrentRecCnt = MintRecordCnt;
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 12, out MsMessageBoxIcon);
                    }
                    else
                    {
                        MintCurrentRecCnt = MintCurrentRecCnt + 1;

                        if (MintCurrentRecCnt >= MintRecordCnt)
                            MintCurrentRecCnt = MintRecordCnt;

                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 11, out MsMessageBoxIcon);
                    }
                }
                else
                    MintCurrentRecCnt = 0;

                DisplayEmployeeInformation();  // Displaying employee information
                BindingNavigatornPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt);
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                SetBindingNavigatorButtons();   // Setting Binding navigator buttons
                TxtSearchText.Clear();
                MintRowNumber = 0;
                TbEmployee.SelectedTab = TpGeneral;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SaveEmployeeInformation();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            //Validating fields before saving
            if (ValidateEmployeeFields())
            {
                // Saving Employee details and closes the form
                if (SaveEmployeeDetails())
                {
                    MblnChangeStatus = false;
                    this.Close();
                }
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BnHelpButton_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Employee";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void BnSearchButton_Click(object sender, EventArgs e)
        {
            if (this.TxtSearchText.Text.Trim() == string.Empty)
                return;

            SearchBy searchBy = this.GetSearchBy();

            int TotalRows;
            // Validating Search option
            if (CboSearchOption.SelectedIndex == -1)
            {
                //MsMessageCommon = "Please select a Search Option";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.SearchOptionValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmEmployee.Enabled = true;
                CboSearchOption.Focus();
                return;
            }
            // Validating Search criteria
            if (TxtSearchText.Text.Trim().Length == 0)
            {
                //MsMessageCommon = "Please enter Search Text";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.SearchTextValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmEmployee.Enabled = true;
                TxtSearchText.Focus();
                return;
            }
            //Searching an Employee according to the Employeename/employee number
            if (MObjClsBLLEmployeeInformation.SearchEmployee(Convert.ToInt32(CboSearchOption.SelectedIndex),
                TxtSearchText.Text.Trim(), MintRowNumber, out TotalRows))
            {
                BindingNavigatornPositionItem.Text = MintRowNumber.ToString();
                BindingNavigatorCountItem.Text = "of " + TotalRows.ToString();

                MintRecordCnt = TotalRows;
                MblnAddStatus = false;

                SetEmployeeInformation();  // Displaying employee information
                BindingNavigatornPositionItem.Text = Convert.ToString(MintRowNumber);
                BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";

                BindingNavigatorMoveNextItem.Enabled = true;
                BindingNavigatorMoveLastItem.Enabled = true;
                BindingNavigatorMovePreviousItem.Enabled = true;
                BindingNavigatorMoveFirstItem.Enabled = true;

                int iCurrentRec = Convert.ToInt32(BindingNavigatornPositionItem.Text); // Current position of the record
                int iRecordCnt = MintRecordCnt;  // Total count of the records

                if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }

                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                BtnPrint.Enabled = MblnPrintEmailPermission;
                BtnEmail.Enabled = MblnPrintEmailPermission;
                BtnClear.Enabled = MblnUpdatePermission;
                BtnSave.Enabled = false;
                BtnOk.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
            }
            else
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.NoResultMessage, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmEmployee.Enabled = true;
                MintRowNumber = 0;
            }
        }

        private void TmExecutive_Tick(object sender, EventArgs e)
        {
            TmEmployee.Enabled = false;
            LblEmployeeStatus.Text = "";
        }

        private void ClearErrorProvider(object sender, EventArgs e)
        {
            ErrEmployee.Clear();
            EnableButtons(sender, e);
        }

        private void EnableButtons(object sender, EventArgs e)
        {
            BtnSave.Enabled = MblnUpdatePermission;
            BtnOk.Enabled = MblnUpdatePermission;
            BtnClear.Enabled = MblnUpdatePermission;
            BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            MblnChangeStatus = true;
        }

        private void ClearSearchCount(object sender, EventArgs e)
        {
           if (TxtSearchText.Text != string.Empty) MintRowNumber = 1;
        }

        private enum ErrorCode
        {
            EmpNoValidation = 401, // Please enter Employee Number
            SalutationValidation = 402, // Please select Salutation
            FirstNameValidation = 403, // Please enter First Name
            WorkingAtValidation = 404, // Please select Working At
            DepartmentValidation = 405, // Please select Department
            DesignationValidation = 406, //	Please select Designation
            MaritalStatusValidation = 407, // Please select Marital Status
            NationalityValidation = 408, // Please select Nationality
            ReligionValidation = 409, // Please select Religion
            DateOfJoinValidation = 410, // Please select valid Date of Joining
            EmailValidation = 411, // Please enter valid Email
            DateOfBirthValidation = 412, // Please select valid Date of Birth
            DuplicateEmpNo = 413, // Duplicate Employee Number not permitted
            SearchOptionValidation = 414, // Please select a Search Option
            SearchTextValidation = 415, // Please enter Search Text
            NoResultMessage = 416, // No result for the search
            SaveConfirm = 417,
            UpdateConfirm = 418,
            AddNew = 419,
            CouldNotDelete = 420,
            SaveMessage = 421, // Executive Information saved successfully
            UpdateMessage = 422, // Executive Information updated successfully
            DeleteConfirm = 423, // Are you sure you want to delete the selected Executive Information ?
            DeleteMessage = 424, // Executive Information deleted successfully
            FormClosing = 425, // Your changes will be lost. Please confirm if you wish to cancel ?  
            WorkstatusValidation = 426,// Please select Workstaus
            CountryoforginValidation = 427, // Please select Countryoforgin
            ReviewDateValidation = 430, //Reviewdate should be greater than joining date
            ProbationEndDatevalidation = 429, //ProbationDate should be greater than joining date
            WorkPolicy=438,
            LeavePolicy =439
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            SaveEmployeeInformation();
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            MblnIsEditMode = true;

            try
            {
                GetPreviousEmployeeInformation(false);
                
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MovePreviousItem:MovePreviousItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MovePreviousItem_Click " + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            MblnIsEditMode = true;

            try
            {
                GetPreviousEmployeeInformation(true);
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MoveFirstItem:MoveFirstItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveFirstItem_Click " + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            MblnIsEditMode = true;

            try
            {
                GetNextExecutiveInformation(false);
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MoveNextItem:MoveNextItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveNextItem_Click " + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            MblnIsEditMode = true;

            try
            {
                GetNextExecutiveInformation(true);
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MoveLastItem:MoveLastItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveLastItem_Click " + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewItem();
            
        }

        private bool DeleteValidation(int intEmployee)
        {
            bool blnretvalue = true;
            DataTable dtIds = MObjClsBLLEmployeeInformation.CheckExistReferences();

            if (dtIds.Rows.Count > 0)
            {
                   MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 420, out MsMessageBoxIcon);
                   MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                   TmEmployee.Enabled = true;
                   blnretvalue = false;
               
            }
            
            return blnretvalue;
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                int intEmployee = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID;
                if (DeleteValidation(intEmployee))
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 13, out MsMessageBoxIcon);

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;

                    if (MObjClsBLLEmployeeInformation.DeleteEmployeeInformation())
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 4, out MsMessageBoxIcon);
                        LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmEmployee.Enabled = true;
                        AddNewItem();
                    }
                }
            }
            catch (SqlException Ex)
            {
                if (Ex.Number == (int)SqlErrorCodes.ForeignKeyErrorNumber)
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 420, out MsMessageBoxIcon);
                else
                    MsMessageCommon = "Error on DeleteItem_Click() " + Ex.Message.ToString();
                              
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                TmEmployee.Enabled = true;               
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on DeleteItem:DeleteItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteItem_Click() " + Ex.Message.ToString());
            }
        }

        private void FrmEmployee_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID > 0)
                PintEmployeeID = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID;
            if (MblnChangeStatus)
            {
                // Checking the changes are not saved and shows warning to the user
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.FormClosing, out MsMessageBoxIcon);
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MObjClsBLLEmployeeInformation = null;
                    MObjClsLogWriter = null;
                    MObjClsNotification = null;
                    e.Cancel = false;
                }
                else
                    e.Cancel = true;
            }
        }

        private void BtnWorkstatus_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("WorkStatus", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "WorkStatusID,IsPredifined,WorkStatus", "WorkStatusReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkStatusID = Convert.ToInt32(cboWorkStatus.SelectedValue);
                LoadCombos(10);
                if (objCommon.NewID != 0)
                    cboWorkStatus.SelectedValue = objCommon.NewID;
                else
                    cboWorkStatus.SelectedValue = intWorkStatusID;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnWorkstatus" + Ex.Message.ToString());
            }
        }

        private void BtnWorkingat_Click(object sender, EventArgs e)
        {
            try
            {
                int intWorkingAt = Convert.ToInt32(cboWorkingAt.SelectedValue);
                using (FrmCompany objCompany = new FrmCompany())
                {
                    objCompany.PintCompany = Convert.ToInt32(cboWorkingAt.SelectedValue);
                    objCompany.ShowDialog();
                    LoadCombos(7);
                    if(objCompany.PintCompany !=0)
                        cboWorkingAt.SelectedValue = objCompany.PintCompany;
                    else
                        cboWorkingAt.SelectedValue = intWorkingAt;
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnWorkingat" + Ex.Message.ToString());
            }
        }

        private void BtnSalutation_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Salutation", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "SalutationID, Salutation", "SalutationReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                int intWorkingAt = Convert.ToInt32(CboSalutation.SelectedValue);
                LoadCombos(6);
                if (objCommon.NewID != 0)
                    CboSalutation.SelectedValue = objCommon.NewID;
                else
                    CboSalutation.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnSalutation" + Ex.Message.ToString());
            }
        }

        private void BtnEmptype_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Employment Type", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "EmploymentTypeID,IsPredefined, EmploymentType As [Employment Type]", "EmploymentTypeReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                int intWorkingAt = Convert.ToInt32(cboEmploymentType.SelectedValue);
                LoadCombos(16);
                if (objCommon.NewID != 0)
                    cboEmploymentType.SelectedValue = objCommon.NewID;
                else
                    cboEmploymentType.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnEmptype" + Ex.Message.ToString());
            }
        }

        private void BtnCountryoforigin_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Country", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "CountryID, CountryName As [Country]", "CountryReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                int intWorkingAt = Convert.ToInt32(cboCountryoforgin.SelectedValue);
                LoadCombos(17);
                if(objCommon.NewID !=0)
                    cboCountryoforgin.SelectedValue = objCommon.NewID;
                else
                    cboCountryoforgin.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnCountryoforigin" + Ex.Message.ToString());
            }
        }

        private void BtnNationality_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Nationality", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "NationalityID,IsPredefined, Nationality", "NationalityReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(cboNationality.SelectedValue);
                LoadCombos(5);
                if (objCommon.NewID != 0)
                    cboNationality.SelectedValue = objCommon.NewID;
                else
                    cboNationality.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on  BtnNationality " + Ex.Message.ToString());
            }
        }

        private void BtnDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                int DepartmentID = Convert.ToInt32(cboDepartment.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Department", new int[] {2,2, ClsCommonSettings.TimerInterval }, "DepartmentID,IsPredefined,Department", "DepartmentReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(cboDepartment.SelectedValue);
                LoadCombos(1);

                if (objCommon.NewID != 0)
                    cboDepartment.SelectedValue = objCommon.NewID;
                else
                    cboDepartment.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnDepartment " + Ex.Message.ToString());
            }
        }

        private void BtnDesignation_Click(object sender, EventArgs e)
        {
            try
            {
                int DesignationID = Convert.ToInt32(cboDesignation.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Designation",
                    new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "DesignationID,IsPredefined, Designation", "DesignationReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(cboDesignation.SelectedValue);
                LoadCombos(2);

                if (objCommon.NewID != 0)
                    cboDesignation.SelectedValue = objCommon.NewID;
                else
                    cboDesignation.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnDesignation " + Ex.Message.ToString());
            }
        }

        private void BtnReligion_Click(object sender, EventArgs e)
        {
            try
            {
                int intReligion = Convert.ToInt32(cboReligion.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Religion",
                    new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "ReligionID,IsPredefined,Religion", "ReligionReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(cboReligion.SelectedValue);
                LoadCombos(4);

                if (objCommon.NewID != 0)
                    cboReligion.SelectedValue = objCommon.NewID;
                else
                    cboReligion.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnReligion " + Ex.Message.ToString());
            }
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            AddNewItem();
        }

        /// <summary>
        /// Loading report viewer with employee information
        /// </summary>
        private void LoadReport()
        {
            if (MintEmployeeID > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = MintEmployeeID;
                ObjViewer.PiFormID = (int)FormID.Employee ;
                ObjViewer.ShowDialog();
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                LoadReport();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmEmployee: LoadReport() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on   LoadReport()" + Ex.Message.ToString());
            }
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Employee Information";
                    ObjEmailPopUp.EmailFormType = EmailFormID.Employee;
                    ObjEmailPopUp.EmailSource = MObjClsBLLEmployeeInformation.GetEmployeeReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmEmployee: BtnEmail_Click " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on  BtnEmail_Click  " + Ex.Message.ToString());
            }
        }

        private void ClearPicture()
        {
            try
            {
                if (!File.Exists(Application.StartupPath + "\\Images\\sample.png"))
                {
                }
                else
                {
                    PassportPhotoPictureBox.Image = Image.FromFile(Application.StartupPath + "\\Images\\sample.png");
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strPassportPhotoPath = null;
                }
                ChangeStatus();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmEmployee:ClearPicture() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on  ClearPicture()  " + Ex.Message.ToString());
            }
        }

        private void ScanDocs()
        {
            try
            {
                if (BtnAttach.Text == ToolStripMenuAttach.Text)
                {
                    OpenFileDialog Dlg = new OpenFileDialog();
                    Dlg.Filter = "Images (*.jpg;*.jpeg;*.tif;*.png;*.bmp)|*.jpg;*.tif;*.jpeg;*.png;*.bmp";
                    Dlg.ShowDialog();
                    PassportFilPath = Dlg.FileName;

                    if (File.Exists(PassportFilPath) == false)
                        return;
                    else
                    {
                        string strExtention = "";
                        FileInfo fInfo = new FileInfo(PassportFilPath);

                        strExtention = fInfo.Extension.ToUpper();

                        if (strExtention == ".jpg".ToUpper() || strExtention == ".jpeg".ToUpper() || strExtention == ".tif".ToUpper() ||
                            strExtention == ".png".ToUpper() || strExtention == ".bmp".ToUpper())
                        {
                            imgImageFile = Image.FromFile(PassportFilPath.ToString());
                        }
                        else
                            return;
                    }

                    PassportPhotoPictureBox.Image = imgImageFile;
                    ResizeEmpImage(imgImageFile);
                }
                else
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 437, out MsMessageBoxIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmEmployee: ScanDocs()" + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on  ScanDocs() " + Ex.Message.ToString());
            }
        }

        private void BtnAttach_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(BtnAttach.Text.Trim()) == "Remove")
                ClearPicture();
            else
                ScanDocs();

            ChangeStatus();
        }

        private void ResizeEmpImage(Image Img)
        {
            //function for resize emp image
            Bitmap bm = new Bitmap(Img);
            int width = 293;
            int height = 192;
            Bitmap thumb = new Bitmap(width, height);
            Graphics g;
            g = Graphics.FromImage(thumb);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.DrawImage(bm, new Rectangle(0, 0, width, height), new Rectangle(0, 0, bm.Width, bm.Height), GraphicsUnit.Pixel);
            PassportPhotoPictureBox.Image = thumb;
            g.Dispose();
            bm.Dispose();
        }

        private void ChangeVisiblityPhoto(int intfrmbtn)
        {
            switch (intfrmbtn)
            {
                case 1:
                    BtnAttach.Text = ToolStripMenuAttach.Text;
                    break;
                case 2:
                    BtnAttach.Text = ToolStripMenuScan.Text;
                    break;
                case 3:
                    BtnAttach.Text = ToolStripMenuRemove.Text;
                    break;
            }
        }

        private void ToolStripMenuAttach_Click(object sender, EventArgs e)
        {
            ChangeStatus();
            ChangeVisiblityPhoto(1);
            ScanDocs();
        }

        private void ToolStripMenuScan_Click(object sender, EventArgs e)
        {
            ChangeStatus();
            ChangeVisiblityPhoto(2);
            ScanDocs();
        }

        private void ToolStripMenuRemove_Click(object sender, EventArgs e)
        {
            ChangeStatus();
            ChangeVisiblityPhoto(3);
            ClearPicture();
        }

        private void BtnContxtMenu1_Click(object sender, EventArgs e)
        {
            ContextMenuStrip2.Show(BtnContxtMenu1, BtnContxtMenu1.PointToClient(System.Windows.Forms.Cursor.Position));
        }

        private void TxtFirstName_TextChanged(object sender, EventArgs e)
        {
            GetFullName();
            ChangeStatus();
        }

        private void TxtMiddleName_TextChanged(object sender, EventArgs e)
        {
            GetFullName();
            ChangeStatus();
        }

        private void TxtLastName_TextChanged(object sender, EventArgs e)
        {
            GetFullName();
            ChangeStatus();
        }

        private void CboSearchOption_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboSearchOption.DroppedDown = false;
        }

        private void mnuItemAttachDocuments_Click(object sender, EventArgs e)
        {
            using (frmDocumentMaster objfrmDocumentMaster = new frmDocumentMaster())
            {
                objfrmDocumentMaster.PintOperationType = Convert.ToInt32(OperationType.Employee);
                objfrmDocumentMaster.PlngOperationID = MintEmployeeID;
                objfrmDocumentMaster.PstrOperationNo = TxtEmployeeNumber.Text.Trim();
                objfrmDocumentMaster.ShowDialog();
            }
        }

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
             ErrEmployee.SetError(cboDepartment, "");
             ChangeStatus();
        }

        private void cboDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErrEmployee.SetError(cboDesignation, "");
            ChangeStatus();
        }

        private void cboEmploymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErrEmployee.SetError(cboEmploymentType, "");
            ChangeStatus();
        }

        private void txtPersonID_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void TxtSearchText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                BnSearchButton_Click(sender,e);
        }

        private void SetAutoCompleteList()
        {
            SearchBy searchBy = this.GetSearchBy();
            this.TxtSearchText.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = MObjClsBLLEmployeeInformation.GetAutoCompleteList(searchBy);
            this.TxtSearchText.AutoCompleteCustomSource = MObjClsBLLEmployeeInformation.ConvertToAutoCompleteCollection(dt);
        }

        private SearchBy GetSearchBy()
        {
            SearchBy searchBy = SearchBy.Department ;

            switch (this.CboSearchOption.SelectedIndex)
            {
                case 1:
                    searchBy = SearchBy.Designation;
                    break;
                case 2:
                    searchBy = SearchBy.EmployeeNumber ;
                    break;
                case 3:
                    searchBy = SearchBy.FirstName;
                    break;
            }

            return searchBy;
        }

        private void BankNameReferenceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BankNameReferenceComboBox.SelectedIndex != -1 && cboWorkingAt.SelectedIndex != -1)
            {
                ComBankAccIdComboBox.DataSource = null;
                DataTable datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "abd.CompanyBankAccountID,abd.BankAccountNo", "BankBranchReference bnr inner join CompanyBankAccountDetails abd on bnr.BankBranchID=abd.BankBranchID ", "CompanyID=" + cboWorkingAt.SelectedValue + " and bnr.BankBranchID=" + Convert.ToInt32(BankNameReferenceComboBox.SelectedValue) + "" });

                ComBankAccIdComboBox.ValueMember = "CompanyBankAccountID";
                ComBankAccIdComboBox.DisplayMember = "BankAccountNo";
                ComBankAccIdComboBox.DataSource = datCombos;

                if (MblnAddStatus == true)
                {
                    ComBankAccIdComboBox.SelectedIndex = -1;
                    ComBankAccIdComboBox.Text = "";
                }

                if (!(TransactionTypeReferenceComboBox.Text.ToUpper() == "BANK" || TransactionTypeReferenceComboBox.Text.ToUpper() == "WPS"))
                    ComBankAccIdComboBox.Text = "";

                if (TransactionTypeReferenceComboBox.Text.ToUpper() == "WPS")
                {
                    CboEmployeeBankname.Enabled = true;
                }
            }

            ErrEmployee.SetError(BankNameReferenceComboBox, "");
            ChangeStatus();
        }

        private void TransactionTypeReferenceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetAccountBankDetails();
            ErrEmployee.SetError(TransactionTypeReferenceComboBox, "");
            ChangeStatus(); 
        }

        private void ComBankAccIdComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErrEmployee.SetError(ComBankAccIdComboBox, "");
            ChangeStatus();
        }

        private void btnWorkPolicy_Click(object sender, EventArgs e)
        {
            using (FrmWorkPolicy objWorkPolicy = new FrmWorkPolicy())
            {
                objWorkPolicy.ShowDialog();
            }
            LoadCombos(18);
        }

        private void btnLeavePolicy_Click(object sender, EventArgs e)
        {
            using (FrmLeavePolicy objLeavePolicy = new FrmLeavePolicy())
            {
                objLeavePolicy.ShowDialog();
            }
            LoadCombos(19);
        }

        private void btnVacationPolicy_Click(object sender, EventArgs e)
        {
            using (frmVacationPolicy objVacationPolicy = new frmVacationPolicy())
            {
                objVacationPolicy.ShowDialog();
            }
            LoadCombos(21);
        }

        private void btnSettlementPolicy_Click(object sender, EventArgs e)
        {
            using (FrmSettlementPolicy objSettlementPolicy = new FrmSettlementPolicy())
            {
                objSettlementPolicy.ShowDialog();
            }
            LoadCombos(22);
        }

        private void cboWorkPolicy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboLeavePolicy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboVacationPolicy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboSettlementPolicy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboVisafrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void CboEmployeeBankname_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void AccountNumberTextBox_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

       
    }
}