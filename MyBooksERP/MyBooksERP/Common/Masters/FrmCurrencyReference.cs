﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;

/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date, 5 April 2011>
Description:	<Description, CurrencyReference Form>
================================================
*/
namespace MyBooksERP
{
    public partial class FrmCurrencyReference : DevComponents.DotNetBar.Office2007Form
    {
        #region Variables Declaration
        public int PintCurrency = 0;

        private bool MblnChangeStatus = false;            //Check state of the page
        private bool MblnAddStatus;
        //private bool MblnViewPermission = true;//To Set View Permission
        private bool MblnAddPermission = true;//To Set Add Permission
        private bool MblnUpdatePermission = true;//To Set Update Permission
        private bool MblnDeletePermission = true;//To Set Delete Permission
        private bool MblnAddUpdatePermission = true;//To Set Add/Update Permission
       
        //private bool MbShowErrorMess;
        //private bool Mbblnfill = true;
        
        private int MiTimerInterval;
        private int MiUserId;
        private int MiCurrencyDetID = 0;
        private int MiCompanyId = 0;

        private string MstrMessageCommon;

        private MessageBoxIcon MmsgMessageIcon;
        
        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;

        clsBLLCurrencyReference MobjclsBLLCurrencyReference;
        ClsLogWriter MobjLogWriter;
        ClsNotification MobjNotification;
        #endregion //Variables Declaration

        #region Constructor
        public FrmCurrencyReference()
        {
            InitializeComponent();
            //MbShowErrorMess = true;
            MiCompanyId = ClsCommonSettings.CompanyID;         // current companyid
            MiUserId = ClsCommonSettings.UserID;            // current userid
            MiTimerInterval = ClsCommonSettings.TimerInterval;   // current companyid
            MmsgMessageIcon = MessageBoxIcon.Information;
            MobjclsBLLCurrencyReference = new clsBLLCurrencyReference();
            MobjLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjNotification = new ClsNotification();
        }
        #endregion //Constructor

        private void FrmCurrencyReference_Load(object sender, EventArgs e)
        {
            try
            {
                MblnChangeStatus = false;
                MblnAddStatus = true;
                LoadMessage();
                //LoadCombos(0);
                if (PintCurrency > 0)
                {
                    DisplayCurrencyDetails();
                    FillCurrencyDetails();
                }
                else
                {
                    FillCurrencyDetails();
                    AddNewCurrency();
                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form Load:FrmCurrencyReference_Load " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmCurrencyReference_Load() " + Ex.Message.ToString());
            }
         }

    private void AddNewCurrency()
    {
        try
        
        {
            LoadCombos(0);
            MblnAddStatus = true;
            ClearControls();
            TmrCurrency.Enabled = true;
            DgvCurrencyRef.ClearSelection();
            MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 3160, out MmsgMessageIcon);
            lblcurrefstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            EnableDisableButtons(true);
            ErrCurrency.Clear();
            if (DgvCurrencyRef.RowCount > 0)
            {
                BtnPrint.Enabled = true;
            }
            else
            {
                BtnPrint.Enabled = false;
            }
            BindingNavigatorAddNewItem.Enabled = false;
            TxtCurrency.Focus();
        }
        catch (Exception Ex)
        {
            MobjLogWriter.WriteLog("Error on form AddNewCurrency:AddNewCurrency() " + this.Name + " " + Ex.Message.ToString(), 2);

            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on AddNewCurrency() " + Ex.Message.ToString());
        }
    }

    private void LoadMessage()
    {
        MaMessageArr = new ArrayList();
        MaStatusMessage = new ArrayList();       
        MaMessageArr = MobjNotification.FillMessageArray((int)FormID.Currency, ClsCommonSettings.ProductID);
        MaStatusMessage = MobjNotification.FillStatusMessageArray((int)FormID.Currency, ClsCommonSettings.ProductID);
    }

    private void ClearControls()
    {
        TxtCurrency.Clear();
        CboCountry.SelectedIndex = -1;
        TxtShortName.Clear();
        TxtDecimalPlace.Clear();
        DgvCurrencyRef.ClearSelection();
        ErrCurrency.Clear();
        BtnClear.Enabled = false;
    }
    
    private void ChangeStatus()
    {
        MblnChangeStatus = true;
        BtnClear.Enabled = true;
        BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
        if (MobjclsBLLCurrencyReference.clsDTOCurrencyReference.intCurrencyID > 0)
        {
            CurrencyRefBindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            BtnSave.Enabled = MblnUpdatePermission;
            BtnOk.Enabled = MblnUpdatePermission;
        }
        else
        {
            CurrencyRefBindingNavigatorSaveItem.Enabled = MblnAddPermission;
            BtnSave.Enabled = MblnAddPermission;
            BtnOk.Enabled = MblnAddPermission;
        }
        ErrCurrency.Clear();
    }

    private void SetPermissions()
    {
        if (MblnAddPermission == true || MblnUpdatePermission == true)
            MblnAddUpdatePermission = true;

        BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
        CurrencyRefBindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
        BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
        BtnOk.Enabled = MblnUpdatePermission;
    }

    public void ChangeStatus(object sender, EventArgs e) // to set the event
    {
        ChangeStatus();
    }

    private void Cbo_KeyPress(object sender, KeyPressEventArgs e)
    {
        ComboBox Cbo = (ComboBox)sender;
        Cbo.DroppedDown = false;
    }

    /// <summary>
    /// Enable/disable buttons according to the action
    /// </summary>
    private void EnableDisableButtons(bool bEnable)
    {
        BindingNavigatorDeleteItem.Enabled = !bEnable;
        BtnPrint.Enabled = !bEnable;
        BtnEmail.Enabled = !bEnable;
        
        if (bEnable)
        {
            CurrencyRefBindingNavigatorSaveItem.Enabled = !bEnable;
            BtnOk.Enabled = !bEnable;
            BtnSave.Enabled = !bEnable;
            MblnChangeStatus = !bEnable;
        }
        else
        {
            CurrencyRefBindingNavigatorSaveItem.Enabled = bEnable;
            BtnOk.Enabled = bEnable;
            BtnSave.Enabled = bEnable;
            MblnChangeStatus = bEnable;
        }
    }

    private void TmrFocus_Tick(object sender, EventArgs e)
    {
        TmrFocus.Enabled = false;
        TxtCurrency.Focus();
    }
    private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
    {
        AddNewCurrency();
    }

    private void CurrencyRefBindingNavigatorSaveItem_Click(object sender, EventArgs e)
    {
        if (Currencyvalidation())
        {
            if (SaveCurrency())
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr , 2,out MmsgMessageIcon );
                lblcurrefstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrFocus.Enabled = true;
                AddNewCurrency();
                FillCurrencyDetails();
            }
        }
    }

    private bool DeleteValidation(int intCurrentID)
    {
        DataTable dtIds = MobjclsBLLCurrencyReference.CurrencyExists(intCurrentID);
        if (dtIds.Rows.Count > 0)
        {
            if (Convert.ToString(dtIds.Rows[0][0]) != "0")
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr,558, out MmsgMessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                TmrCurrency.Enabled = true;
                return false;
            }
        }

        //if (dtIds.Rows.Count > 0)
        //{
        //    if (Convert.ToString(dtIds.Rows[0][0]) != "0")
        //    {
        //        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3059, out MmsgMessageIcon);
        //        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
        //        TmUOM.Enabled = true;
        //        return false;
        //    }
        //}
        return true;
    }     

    private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
    {
        try
        {
            MiCurrencyDetID = Convert.ToInt32(DgvCurrencyRef.CurrentRow.Cells[0].Value);
            if (DeleteValidation(MiCurrencyDetID))
            {

                if (MobjclsBLLCurrencyReference.DeleteCurrency(MiCurrencyDetID))
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 13, out MmsgMessageIcon);
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 4, out MmsgMessageIcon);
                    lblcurrefstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrFocus.Enabled = true;
                    AddNewCurrency();
                    FillCurrencyDetails();
                }
            }
        }
        catch (Exception Ex)
        {
            MobjLogWriter.WriteLog("Error on form DeleteItem:DeleteItem_Click" + this.Name + " " + Ex.Message.ToString(), 2);

            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on DeleteItem_Click " + Ex.Message.ToString());
        }     
       
    }

   
    private void BtnHelp_Click(object sender, EventArgs e)
    {

    }    

    private void BtnSave_Click(object sender, EventArgs e)
    {
        if (Currencyvalidation())
        {
            if (SaveCurrency())
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 2,out MmsgMessageIcon );
                lblcurrefstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrFocus.Enabled = true;
                AddNewCurrency();
                FillCurrencyDetails();
            }
        }
    }

    private void BtnOk_Click(object sender, EventArgs e)
    {
        if (Currencyvalidation())
        {
            if (SaveCurrency())
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 2,out MmsgMessageIcon );
                lblcurrefstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrFocus.Enabled = true;
                AddNewCurrency();
                FillCurrencyDetails();
            }
        }
    }

    private void BtnCancel_Click(object sender, EventArgs e)
    {
        this.Close();
    }

    private void BtnCountry_Click(object sender, EventArgs e)
    {
        try
        {
            MiCompanyId = Convert.ToInt32(CboCountry.SelectedValue);
            FrmCommonRef objCommon = new FrmCommonRef("Country", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "CountryID,CountryName As Country", "CountryReference", "");
            objCommon.ShowDialog();
            objCommon.Dispose();
            int intWorkingAt = Convert.ToInt32(CboCountry.SelectedValue);
            LoadCombos(1);
            if (objCommon.NewID != 0)
                CboCountry.SelectedValue = objCommon.NewID;
            else
                CboCountry.SelectedValue = intWorkingAt;            
        }
        catch (Exception Ex)
        {
            MobjLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on BtnCurrency " + Ex.Message.ToString());
        }
        
    }

    private void DgvCurrencyRef_CellClick(object sender, DataGridViewCellEventArgs e)
    {
        DisplayCurrency();
    } 
   

    // Loading Currency Combo

    private bool LoadCombos(int intType)
    {        
      
       try
        {
            DataTable datCombos = new DataTable();

            if (intType == 0 || intType == 1)
            {
                 datCombos = MobjclsBLLCurrencyReference.FillCombos(new string[] { "CountryID,CountryName", "CountryReference", "CountryID not in ( select isnull(CountryID,0) as CountryID from  CurrencyReference)" });
                //datCombos = MobjclsBLLCurrencyReference.FillCombos(new string[] { "CountryID,Description", "CountryReference", "" });
                CboCountry.ValueMember = "CountryID";
                CboCountry.DisplayMember = "CountryName";
                CboCountry.DataSource = datCombos;
            }
            if (intType == 2)
            {
                datCombos = MobjclsBLLCurrencyReference.FillCombos(new string[] { "CountryID,CountryName", "CountryReference", "" });
                CboCountry.ValueMember = "CountryID";
                CboCountry.DisplayMember = "CountryName";
                CboCountry.DataSource = datCombos;
            }
            MobjLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
            return true;
        }
        catch (Exception Ex)
        {
            MobjLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

            return false;
        }
    }     

      
    //private bool DeleteValidation()
    //{
    //    string sQuery = "";
       
    //    SqlDataReader sReader = MobjclsBLLCurrencyReference.GetCurrencyExists(Convert.ToInt32(TxtCurrency.Tag));
    //    if (sReader.Read())
    //    {
    //        if (Convert.ToInt32(sReader[0]) != 0)
    //        {
    //            sReader.Close();
    //            MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 558,out MmsgMessageIcon );
    //            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
    //            lblcurrefstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
    //            TmrCurrency.Enabled = true;
    //            return false;
    //        }

    //    }
    //    sReader.Close();
    //    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 13,out MmsgMessageIcon );
    //    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
    //        return false;
    //    else
    //        return true;

    //}
   
    private bool Currencyvalidation()
    {
        ErrCurrency.Clear();
        lblcurrefstatus.Text = "";
         if ( TxtCurrency.Text.Trim() == "") 
         {
            // MstrMessageCommon = "Please enter a Currency name.";
             MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 552,out MmsgMessageIcon );
             ErrCurrency.SetError(TxtCurrency, MstrMessageCommon.Replace("#", "").Trim());
             MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(),ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
             lblcurrefstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
             TmrCurrency.Enabled = true;            
             TxtCurrency.Focus();
             return false;
           }
         if (TxtShortName.Text.Trim() == "")
         {
             //MstrMessageCommon = "Please enter a Short name.";
              MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 553,out MmsgMessageIcon );
             ErrCurrency.SetError(TxtShortName, MstrMessageCommon.Replace("#", "").Trim());
             MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
             lblcurrefstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
             TmrCurrency.Enabled = true;
             TxtShortName.Focus();
             return false;
         }
         if (CboCountry.SelectedIndex == -1 || Convert.ToInt32(CboCountry.SelectedValue)<=0)
         {
             //MstrMessageCommon = "Please select a valid Country.";
             MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 5,out MmsgMessageIcon );
             ErrCurrency.SetError(CboCountry, MstrMessageCommon.Replace("#", "").Trim());
             MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
             lblcurrefstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
             TmrCurrency.Enabled = true;
             CboCountry.Focus();
             return false;
         }
         if (TxtDecimalPlace.Text.Trim() == "")
         {
             //MstrMessageCommon = "Please enter decimal place.";
              MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 555,out MmsgMessageIcon );
             ErrCurrency.SetError(TxtDecimalPlace, MstrMessageCommon.Replace("#", "").Trim());
             MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
             lblcurrefstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
             TmrCurrency.Enabled = true;
             TxtDecimalPlace.Focus();
             return false;
         }

         if (Convert.ToInt32(TxtDecimalPlace.Text) > 3)
         {
             //MstrMessageCommon = "Scale should not be greater than 3";
              MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 556,out MmsgMessageIcon );
             ErrCurrency.SetError(TxtDecimalPlace, MstrMessageCommon.Replace("#", "").Trim());
             MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
             lblcurrefstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
             TmrCurrency.Enabled = true;
             TxtDecimalPlace.Focus();
             return false;
         }
         int iCurID = 0;
         int iCountryID = 0;   
         if (MblnAddStatus == true)
         {
             iCurID = MobjclsBLLCurrencyReference.CheckDuplicateCurrency(TxtCurrency.Text.Trim());//duplicate curr name add new mode
         }
         else
         {
             iCurID = MobjclsBLLCurrencyReference.CheckDuplicateCurrency(TxtCurrency.Text.Trim(), MiCurrencyDetID);//duplicate curr name update mode
             iCountryID = MobjclsBLLCurrencyReference.CheckDuplicateCountry(MiCurrencyDetID, Convert.ToInt32(CboCountry.SelectedValue));
             

         }
         if (iCurID > 0)
         {
             MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 559, out MmsgMessageIcon);
             ErrCurrency.SetError(TxtCurrency, MstrMessageCommon.Replace("#", "").Trim());
             MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
             lblcurrefstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
             TmrCurrency.Enabled = true;
             TxtDecimalPlace.Focus();
             return false;
         }
         if (iCountryID > 0)
         {
             MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 560, out MmsgMessageIcon);
             ErrCurrency.SetError(CboCountry, MstrMessageCommon.Replace("#", "").Trim());
             MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
             lblcurrefstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
             TmrCurrency.Enabled = true;
             TxtDecimalPlace.Focus();
             return false;
         }

        return true;
    }

    private bool SaveCurrency()
    {
        try
        {
            if (MblnAddStatus == true)

                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 557, out MmsgMessageIcon);
            else
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 3, out MmsgMessageIcon);


            if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                return false;

            if (MblnAddStatus == true)
            {
                MobjclsBLLCurrencyReference.clsDTOCurrencyReference.intCurrencyID = 0;
            }
            else
            {
                MobjclsBLLCurrencyReference.clsDTOCurrencyReference.intCurrencyID = MiCurrencyDetID;
            }
            MobjclsBLLCurrencyReference.clsDTOCurrencyReference.strDescription = TxtCurrency.Text.Replace("'", "’").Trim();
            MobjclsBLLCurrencyReference.clsDTOCurrencyReference.strShortDescription = TxtShortName.Text.Replace("'", "’").Trim();
            MobjclsBLLCurrencyReference.clsDTOCurrencyReference.intCountryID = Convert.ToInt32(CboCountry.SelectedValue);
            MobjclsBLLCurrencyReference.clsDTOCurrencyReference.shtScale = Convert.ToInt16(TxtDecimalPlace.Text);

            if (MobjclsBLLCurrencyReference.SaveCurrency(MblnAddStatus))
            {
                //Mbblnfill = true;
                TxtCurrency.Tag = MobjclsBLLCurrencyReference.clsDTOCurrencyReference.intCurrencyID;

                return true;

            }
            else
            {
                return false;
            }
        }
        catch (Exception Ex)
        {
            MobjLogWriter.WriteLog("Error on form save:SaveCurrency() " + this.Name + " " + Ex.Message.ToString(), 2);

            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on saving SaveCurrency() " + Ex.Message.ToString());
            return false;
        }
    }
   
        private void FillCurrencyDetails()
        {
            DgvCurrencyRef.DataSource = null;
            DgvCurrencyRef.DataSource = MobjclsBLLCurrencyReference.GetCurrencyDetails();
            DgvCurrencyRef.Columns[0].Visible = false; 
            DgvCurrencyRef.Columns[1].Width = 130;
            DgvCurrencyRef.Columns[2].Width = 70;
            DgvCurrencyRef.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
            DgvCurrencyRef.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;  

        }
      
        private void DisplayCurrency()
        {
            
            if (DgvCurrencyRef.CurrentRow !=null && Convert.ToInt32(DgvCurrencyRef.CurrentRow.Cells[0].Value) != 0)
            {
                DisplayCurrencyDetails();
            }
        }
        private void DisplayCurrencyDetails()
        {
            LoadCombos(2);//loading all Country
            lblcurrefstatus.Text = "";
            ErrCurrency.Clear();
            if (PintCurrency > 0)
            {
                MiCurrencyDetID = PintCurrency;
                PintCurrency = 0;
            }
            else
            {
                MiCurrencyDetID = Convert.ToInt32(DgvCurrencyRef.CurrentRow.Cells[0].Value);
            }
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            if (MobjclsBLLCurrencyReference.DisplayCurrency(MiCurrencyDetID))
            {
                TxtCurrency.Tag = MobjclsBLLCurrencyReference.clsDTOCurrencyReference.intCurrencyID;
                TxtCurrency.Text = MobjclsBLLCurrencyReference.clsDTOCurrencyReference.strDescription;
                TxtShortName.Text = MobjclsBLLCurrencyReference.clsDTOCurrencyReference.strShortDescription;
                CboCountry.SelectedValue = MobjclsBLLCurrencyReference.clsDTOCurrencyReference.intCountryID;
                TxtDecimalPlace.Text = Convert.ToString(MobjclsBLLCurrencyReference.clsDTOCurrencyReference.shtScale);

            }
            CurrencyRefBindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            MblnChangeStatus = false;
            BtnOk.Enabled = MblnChangeStatus;
            BtnSave.Enabled = MblnChangeStatus;
            CurrencyRefBindingNavigatorSaveItem.Enabled = MblnChangeStatus;
            BtnClear.Enabled = true;
            MblnAddStatus = false;
        
        }
             
 

    private void DgvCurrencyRef_KeyDown(object sender, KeyEventArgs e)
    {

        DisplayCurrency();
    }

    private void TxtDecimalPlace_KeyPress(object sender, KeyPressEventArgs e)
    {
        Boolean nonNumericEntered = true;

        if (((e.KeyChar) >= 48 && (e.KeyChar) <= 57) || (e.KeyChar) == 8)
            nonNumericEntered = false;
        if (nonNumericEntered == true)
            e.Handled = true;
        else
            e.Handled = false;
    }

    private void DgvCurrencyRef_KeyUp(object sender, KeyEventArgs e)
    {
        DisplayCurrency();
    }
    private void FrmCurrencyReference_KeyDown(object sender, KeyEventArgs e)
    {
        try
        {
            switch (e.KeyData)
            {
                case Keys.F1:
                    //Dim objHelp As New VisaHelp
                    //objHelp.frmWhere = "nEmp"
                    //objHelp.Show()
                    //objHelp = Nothing
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
                //case Keys.Control | Keys.S:
                //    btnSave_Click(sender,new EventArgs());//save
                //  break;
                //case Keys.Control | Keys.O:
                //    btnOk_Click(sender, new EventArgs());//OK
                //    break; 
                //case Keys.Control | Keys.L:
                //    btnCancel_Click(sender, new EventArgs());//Cancel
                //    break;
                case Keys.Control | Keys.Enter:
                    BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                    break;
                case Keys.Alt | Keys.R:
                    BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                    break;
                case Keys.Control | Keys.E:
                    BtnClear_Click(sender, new EventArgs());//Clear
                    break;
               
                //case Keys.Control | Keys.P:
                //    BtnPrint_Click(sender, new EventArgs());//Cancel
                //    break;
                //case Keys.Control | Keys.M:
                //    BtnEmail_Click(sender, new EventArgs());//Cancel
                //    break;
            }
        }
        catch (Exception)
        {
        }
    }
    private void FrmCurrencyReference_FormClosing(object sender, FormClosingEventArgs e)
    {
        if (MblnChangeStatus == true)
        {
            MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 8, out MmsgMessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
        }        
    }


    private void BtnClear_Click(object sender, EventArgs e)
    {
        AddNewCurrency();
    }

    private void CboCountry_KeyDown(object sender, KeyEventArgs e)
    {
        CboCountry.DroppedDown = false;
    }

    private void TxtDecimalPlace_TextChanged(object sender, EventArgs e)
    {
        if(TxtDecimalPlace.Text != string.Empty)
            TxtDecimalPlace.Text = TxtDecimalPlace.Text.ToInt32().ToString();

        ChangeStatus();
    }

    }
}
