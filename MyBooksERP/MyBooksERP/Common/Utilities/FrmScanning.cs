﻿
#region USING
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using Microsoft.ReportingServices.ReportRendering;
using System.Collections.Generic;
using DevComponents.AdvTree;
//using GoldenAnchor.TwainLib
//Imports GoldenAnchor.GdiPlusLib
#endregion USING


/* 
=================================================
Author:		<Author,,Arun>
Create date: <Create Date,,18 Apr 2012>
Description:	<Description,, Form  for Scanning>
================================================
*/

namespace MyBooksERP
{
    public partial class FrmScanning : Form
    {
        #region DECLARATIONS

        private string RecentFilePath;
        public string pictureimage;
        private bool MblnAddStatus;
        private string PrintImage;
        public int Parentnode;
        private string MstrMessageCommon;
        public string strsavePath;
        public string strFileName = "";
        public string strMetaDetails = "";

        private System.Drawing.Image imgImageFile;

        public int MintNode;
        public string StrDescr;
        TreeNode tn;

        private ArrayList MaMessageArr; //' Error Message display
        private ArrayList MaStatusMessageArr;
        private MessageBoxIcon MmessageIcon;

        public string strFormName;
        public System.Drawing.Image ImgPrintImage;
        public int MintPrintCount;
        public int MintAddNewCount;
        public string strAttachFile = "";
        public int mCurrentNode;

        public int MintDocumentTypeid;
        public int MintOperationTypeID;
        public long MlngReferenceID;
        public string MstrReferenceNo;
        public int MintVendorID;
        public bool PblnEditable = true;


        PrintPreviewDialog MObjPrintPreviewDialog;
        PrintDialog MobjPrintDialog;
        Bitmap MobjBitmap;
        WebBrowser MobjWebBrowser;

        clsBLLScanning MobjclsBLLScanning;
        ClsLogWriter MobjClsLogWriter;
        ClsNotification MObjClsNotification;

        #endregion DECLARTIONS

        #region CONSTRUCTOR
        public FrmScanning()
        {
            InitializeComponent();
            MObjPrintPreviewDialog = new PrintPreviewDialog();
            MobjPrintDialog = new PrintDialog();
            MobjclsBLLScanning = new clsBLLScanning();
            MobjWebBrowser = new WebBrowser();
            MObjClsNotification = new ClsNotification();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
        }
        #endregion CONSTRUCTOR

        #region METHODS
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessageArr = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.Scanning, 4);
            MaStatusMessageArr = MObjClsNotification.FillStatusMessageArray((int)FormID.Scanning, 4);
        }

        private void LoadInitial()
        {
            if (MobjclsBLLScanning.SelectDocumentType(MintDocumentTypeid))
            {
                TrvScan.Nodes[0].Text = MobjclsBLLScanning.clsDTOScanning.strNodeName + "(" + MstrReferenceNo + ")";
            }

            MobjclsBLLScanning.SelectfromTreeMaster(MintDocumentTypeid);
            StrDescr = MobjclsBLLScanning.clsDTOScanning.strNodeName;

            if (MintDocumentTypeid > 0)
            {
                MobjclsBLLScanning.clsDTOScanning.intDocumentTypeID = MintDocumentTypeid;
                MobjclsBLLScanning.clsDTOScanning.strNodeName = TrvScan.Nodes[0].Text.Trim();
                MobjclsBLLScanning.clsDTOScanning.intOperationTypeID = MintOperationTypeID;
                MobjclsBLLScanning.clsDTOScanning.intReferenceID = MlngReferenceID.ToInt32();
                // MobjclsBLLScanning.clsDTOScanning.strDescription = TrvScan.Nodes[0].Text.Trim();
                MobjclsBLLScanning.InsertMaster();

                Parentnode = MobjclsBLLScanning.clsDTOScanning.intDocumentAttachID;
                TrvScan.Nodes[0].Tag = Parentnode.ToString();
            }

            //Display(MintOperationTypeID, MlngReferenceID, Parentnode, MintOperationTypeID, MintVendorID, MintDocumentTypeid);
            Display(MintOperationTypeID, MlngReferenceID.ToInt32(), MintDocumentTypeid, TrvScan.Nodes[0].Text.Trim());
            TrvScan.ExpandAll();
        }

        //private bool Display(int Navid, long CommonId, int Parentnode, int intOperationTypeID, int intVendorID, int intDocumentTypeid)
        //{
        //    //DataTable dtIds = MobjclsBLLScanning.GetparentNodes(Navid, CommonId, Parentnode, intOperationTypeID, intVendorID, intDocumentTypeid);
        //    DataTable dtIds = MobjclsBLLScanning.GetparentNodes(intOperationTypeID, intVendorID, intDocumentTypeid);
        //    foreach (DataRow dr in dtIds.Rows)
        //    {
        //        DataTable datChilds = MobjclsBLLScanning.Display(MintOperationTypeID, Convert.ToInt32(dr[0]));
        //        if (datChilds.Rows.Count > 0)
        //        {
        //            PopulateNodes(datChilds, TrvScan.Nodes[0]);
        //            //return false;
        //        }
        //    }

        //    return true;
        //}
        private bool Display(int intOperationTypeID, int intReferenceID, int intDocumentTypeID, string strNodeName)
        {

            DataTable dtIds = MobjclsBLLScanning.GetparentNodes(intOperationTypeID, intReferenceID, intDocumentTypeID, strNodeName);
            foreach (DataRow dr in dtIds.Rows)
            {
                DataTable datChilds = MobjclsBLLScanning.Display(MintOperationTypeID, Convert.ToInt32(dr[0]));
                if (datChilds.Rows.Count > 0)
                {
                    PopulateNodes(datChilds, TrvScan.Nodes[0]);
                    //return false;
                }
            }

            return true;
        }

        public void PopulateNodes(DataTable dt, TreeNode parentNode)
        {
            foreach (DataRow dr in dt.Rows)
            {
                tn = new TreeNode();

                tn.Text = Convert.ToString(dr[1]);

                tn.Tag = (Convert.ToString(dr[0]));
                parentNode.Nodes.Add(tn);
                PopulateNodes(MobjclsBLLScanning.Display(MintOperationTypeID, Convert.ToInt32(tn.Tag)), tn);
            }
        }


        private void PrintingImage()
        {
            MobjclsBLLScanning.Print();

            if (MintPrintCount > 0)
            {
                //MessageBox("Cannot print,this is not document ", MsgBoxStyle.Information, StrMessageCaption);
            }
            if (TrvScan.SelectedNode.Parent == null || TrvScan.SelectedNode.Parent.Nodes.Count == 0)
            {
                //MsgBox("Cannot print,this is not document", MsgBoxStyle.Information, StrMessageCaption);
            }
            if (SetupThePrinting())
            {
                MyPrintDocument.DocumentName = strFormName;
                MObjPrintPreviewDialog.Document = MyPrintDocument;
                MObjPrintPreviewDialog.Width = 750;
                MObjPrintPreviewDialog.Height = 550;
                MObjPrintPreviewDialog.ShowDialog();
            }
        }

        private bool SetupThePrinting()
        {
            if (MobjPrintDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                return false;
            }

            MyPrintDocument.DefaultPageSettings.Margins = MyPrintDocument.PrinterSettings.DefaultPageSettings.Margins;//New System.Drawing.Printing.Margins(30, 30, 30, 30)
            MyPrintDocument.DefaultPageSettings.PaperSize = MobjPrintDialog.PrinterSettings.DefaultPageSettings.PaperSize;
            MyPrintDocument.DefaultPageSettings.Landscape = MobjPrintDialog.PrinterSettings.DefaultPageSettings.Landscape;
            MyPrintDocument.PrinterSettings = MobjPrintDialog.PrinterSettings;
            if (MyPrintDocument.OriginAtMargins == false)
            {
                ScaleImage(MyPrintDocument.DefaultPageSettings.Bounds.Height, MyPrintDocument.DefaultPageSettings.Bounds.Width);
            }
            return true;
        }

        private void ScaleImage(int intHeight, int intWidth)
        {
            int intImageHeight;
            int intImageWidth;
            Single scale_factor;

            if (TrvScan.SelectedNode.Tag != null)
            {
                StrDescr = TrvScan.SelectedNode.Text;
                TrvScan.Nodes[0].Tag = TrvScan.SelectedNode.Tag;

                MintNode = Convert.ToInt32(TrvScan.Nodes[0].Tag);
                //MobjclsBLLScanning.Selectnode(MintOperationTypeID, StrDescr, MintOperationTypeID);

                MintNode = Convert.ToInt32(TrvScan.Nodes[0].Tag);
                if (MintNode > 0)
                {
                    MobjclsBLLScanning.clsDTOScanning.strFileName = "";
                    MobjclsBLLScanning.SelectDetails(MintNode);
                    pictureimage = MobjclsBLLScanning.clsDTOScanning.strFileName;
                    if (pictureimage != "")
                    {
                        ImageControl1.Image = System.Drawing.Image.FromFile(ClsCommonSettings.strServerPath + @"\DocumentImages" + pictureimage);
                        PrintImage = pictureimage;
                    }
                    else
                    {
                        ImageControl1.Image = null;
                    }

                    ImgPrintImage = System.Drawing.Image.FromFile(ClsCommonSettings.strServerPath + @"\DocumentImages" + pictureimage);
                    intImageHeight = ImgPrintImage.Height;
                    intImageWidth = ImgPrintImage.Width; //Calculate scale_factor
                    scale_factor = 1.0f;  //No scaling by default i.e. image fits in the Box 1) Height

                    if (intImageHeight > intHeight) // Reduce height first  
                    {
                        scale_factor = (float)((float)intHeight / (float)intImageHeight);
                    }
                    if ((intImageWidth * scale_factor) > intWidth)
                    {
                        //Scaled width exceeds Box's width, recalculate scale_factor  
                        scale_factor = (float)((float)intWidth / (float)intImageWidth);
                    }

                    MobjBitmap = new Bitmap(ImgPrintImage);  // Move image to control for resizing,// Get the source bitmap.
                    // Move image to control for resizing,// Get the source bitmap.
                    Bitmap bm_dest;
                    bm_dest = new Bitmap(Convert.ToInt32(MobjBitmap.Width * scale_factor), Convert.ToInt32(MobjBitmap.Height * scale_factor));
                    Graphics gr_dest = Graphics.FromImage(bm_dest);
                    gr_dest.DrawImage(MobjBitmap, 0, 0, bm_dest.Width + 1, bm_dest.Height + 1);
                    ImgPrintImage = bm_dest;
                }
            }
        }


        private TreeNode GetNode(string text, TreeNodeCollection parentcollection)
        {
            TreeNode ret = new TreeNode();
            foreach (TreeNode child in parentcollection)
            {
                if (child.Text == text)
                {
                    ret = child;
                }
                else if (child.GetNodeCount(false) > 0)
                {
                    ret = GetNode(text, child.Nodes);
                }

                if (ret == null)
                    break;
            }
            return ret;
        }



        private bool Save(string insert)
        {
            try
            {

                if (MblnAddStatus == true)//insert
                {
                    MobjclsBLLScanning.clsDTOScanning.intParentNode = Convert.ToInt32(TrvScan.SelectedNode.Parent.Tag);

                    MobjclsBLLScanning.clsDTOScanning.intDocumentTypeID = MintDocumentTypeid;
                    MobjclsBLLScanning.clsDTOScanning.intReferenceID = MlngReferenceID.ToInt32();
                    MobjclsBLLScanning.clsDTOScanning.intOperationTypeID = MintOperationTypeID;
                }
                else
                {
                    MobjclsBLLScanning.clsDTOScanning.intDocumentAttachID = MintNode;
                }

                MobjclsBLLScanning.clsDTOScanning.strNodeName = insert;
                if (MobjclsBLLScanning.Save(MblnAddStatus))
                {
                    MintNode = MobjclsBLLScanning.clsDTOScanning.intDocumentAttachID;

                    return true;
                }
                else
                {
                    return false;
                }
            }

            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form Scanning:Save " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Save " + Ex.Message.ToString());
                return false;
            }
        }




        private void DeleteNodeRecursive(TreeNode node)
        {
            if (node.Nodes.Count > 0)
            {
                foreach (TreeNode nd in node.Nodes)
                {
                    DeleteNodeRecursive(nd);
                }
            }
            else
            {
                MobjclsBLLScanning.DeleteNodes(5, Convert.ToInt32(node.Tag));
            }
        }

        private bool DeleteNodes()
        {
            if (TrvScan.SelectedNode != null)
            {
                if (TrvScan.SelectedNode.Nodes.Count > 0)
                {
                    //DeleteNodeRecursive(TrvScan.SelectedNode);

                    foreach (TreeNode nd in TrvScan.SelectedNode.Nodes)
                    {
                        if (MobjclsBLLScanning.SelectDetails(Convert.ToInt32(nd.Tag)))
                        {

                            // must write Documents exists msg 
                            MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
                            if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            {
                                return false;
                            }
                        }
                    }

                    //Delete
                    //foreach (TreeNode nd in TrvScan.SelectedNode.Nodes)
                    //{
                    //    MobjclsBLLScanning.Deletenodes(18, Convert.ToInt32(nd.Tag));
                    //}
                    ////Delete
                    //foreach (TreeNode nd in TrvScan.SelectedNode.Nodes)
                    //{
                    //    MobjclsBLLScanning.Deletenodes(5, Convert.ToInt32(nd.Tag));
                    //}
                    ////Delete parent
                    //MobjclsBLLScanning.Deletenodes(5, Convert.ToInt32(TrvScan.SelectedNode.Tag));
                    List<Int32> lstNodesToDelete = new List<int>();
                    lstNodesToDelete = GetNodes(TrvScan.SelectedNode, lstNodesToDelete);
                    //foreach(TreeNode nd in TrvScan.SelectedNode.Nodes)
                    //{
                    //    lstNodesToDelete.Add(Convert.ToInt32(nd.Tag));
                    //}
                    for (int i = lstNodesToDelete.Count - 1; i >= 0; i--)
                    {
                        MobjclsBLLScanning.DeleteNodes(5, lstNodesToDelete[i]);
                    }



                    MobjclsBLLScanning.SelectDetails(Convert.ToInt32(TrvScan.SelectedNode.Tag));
                    string FileName = MobjclsBLLScanning.clsDTOScanning.strFileName.Trim();
                    string FilePath = string.Format(@"{0}\{1}\{2}", ClsCommonSettings.strServerPath, "DocumentImages", FileName);
                    if (FilePath.IndexOf(":") == -1)
                        FilePath = "\\" + FilePath;

                    if (System.IO.File.Exists(FilePath))
                    {
                        System.IO.File.Delete(FilePath);
                    }




                    TrvScan.Nodes.Remove(TrvScan.SelectedNode);
                }
                else
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);

                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (MobjclsBLLScanning.DeleteNodes(5, Convert.ToInt32(TrvScan.SelectedNode.Tag)))
                        {
                            MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);

                            MobjclsBLLScanning.SelectDetails(Convert.ToInt32(TrvScan.SelectedNode.Tag));
                            string FileName = MobjclsBLLScanning.clsDTOScanning.strFileName.Trim();
                            string FilePath = string.Format(@"{0}\{1}\{2}", ClsCommonSettings.strServerPath, "DocumentImages", FileName);
                            if (FilePath.IndexOf(":") == -1)
                                FilePath = "\\" + FilePath;

                            if (System.IO.File.Exists(FilePath))
                            {
                                System.IO.File.Delete(FilePath);
                            }



                            TrvScan.Nodes.Remove(TrvScan.SelectedNode);
                            ImageControl1.Image = null;
                        }
                    }
                }
            }

            return true;
        }

        private List<int> GetNodes(TreeNode node, List<int> lstNodes)
        {
            lstNodes.Add(Convert.ToInt32(node.Tag));
            if (node.Nodes.Count > 0)
            {
                foreach (TreeNode tn in node.Nodes)
                    lstNodes = GetNodes(tn, lstNodes);
            }
            return lstNodes;
        }
        private bool CheckFileExtension(string flName)
        {
            try
            {
                string ext = flName.Remove(0, flName.LastIndexOf(".")).Trim();
                if (ext.ToLower() == ".jpg" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".gif" || ext.ToLower() == ".bmp" || ext.ToLower() == ".png" || ext.ToLower() == ".tif")
                    return true;
                else
                    return false;
            }
            catch (Exception) { return false; }

        }

        private bool ShowFileContents(string sFilename)
        {
            BtnOpenApplication.Visible = false;
            LblMess1.Visible = false;
            LblMess2.Visible = false;
            FileInfo fInfo = new FileInfo(sFilename);
            if (CheckFileExtension(sFilename))
            {
                ImageControl1.Visible = true;
                webView.Visible = false;
                ImageControl1.Image = System.Drawing.Image.FromFile(sFilename.ToString());
                PrintTStripButton.Enabled = true;
                webView.Url = null;
                fInfo = null;
                return true;
            }
            else if (fInfo.Extension.ToLower() == ".pdf")
            {
                ImageControl1.Visible = false;
                ImageControl1.Image = null;
                webView.Visible = true;
                webView.Url = new Uri(sFilename);
                PrintTStripButton.Enabled = true;
                fInfo = null;
                return true;
            }
            else if (fInfo.Extension.ToLower() == ".txt")
            {
                ImageControl1.Visible = false;
                ImageControl1.Image = null;
                webView.Visible = true;
                webView.Url = new Uri(sFilename);
                PrintTStripButton.Enabled = true;
                fInfo = null;
                return true;
            }
            else
            {
                fInfo = null;
                ImageControl1.Visible = false;
                ImageControl1.Image = null;
                webView.Visible = false;
                PrintTStripButton.Enabled = false;
                BtnOpenApplication.Visible = true;
                LblMess1.Visible = true;
                LblMess2.Visible = true;
                return false;
            }

        }
        private void ScanDocs()
        {
            string Description;

            Description = TrvScan.SelectedNode.Text;

            MintNode = Convert.ToInt32(TrvScan.SelectedNode.Tag);
            OpenFileDialog Dlg = new OpenFileDialog();

            Dlg.Filter = "Picture files (jpg; jpeg;Tiff; Png; Bitmap)|*.jpg;*.jpeg;*.tif;*.png;*.bmp|All files (*.*)|*.*";

            Dlg.ShowDialog();
            string RecentFilePath = Dlg.FileName;
            if (RecentFilePath.IndexOf(":") == -1)
                RecentFilePath = "\\" + RecentFilePath;

            if (File.Exists(RecentFilePath))
            {
                ShowFileContents(RecentFilePath);
            }
            else
            {
                return;
            }

            string[] sarFileName = Dlg.SafeFileName.Split(Convert.ToChar("."));
            DateTime dtServerDate = ClsCommonSettings.GetServerDateTime();
            Random objRan = new Random();

            string strImageFileName = sarFileName[0] + "_" + dtServerDate.Date.Year +
                dtServerDate.Date.Month + dtServerDate.Date.Day + "_" + dtServerDate.Hour + dtServerDate.Minute + dtServerDate.Second + objRan.Next(999).ToString() +
                "." + sarFileName[1];

            string DestinationDirectoryPath = Path.Combine(ClsCommonSettings.strServerPath, "DocumentImages");
            string DestinationFilePath = Path.Combine(DestinationDirectoryPath, strImageFileName);
            if (!Directory.Exists(DestinationDirectoryPath))
                Directory.CreateDirectory(DestinationDirectoryPath);
            if (DestinationFilePath.IndexOf(":") == -1)
                DestinationFilePath = "\\" + DestinationFilePath;

            File.Copy(Dlg.FileName, DestinationFilePath, false);

            //MobjclsBLLDocuments.clsDTODocuments.strFileName = strImageFileName;
            //strMetaDetails = ClsCommonSettings.strUserName.ToString() + '@' + ClsCommonSettings.UserID.ToString() + '@' + DateTime.Now + '@' + Dlg.FileName;
            //MobjclsBLLDocuments.clsDTODocuments.strMetaData = strMetaDetails;
            //MobjclsBLLDocuments.insertDetails(MintNode, MobjclsBLLDocuments.clsDTODocuments.strFileName, MobjclsBLLDocuments.clsDTODocuments.strMetaData);


            //-------------------------------------------------------------------------------------------------------
            MobjclsBLLScanning.clsDTOScanning.strFileName = strImageFileName;
            strMetaDetails = MintOperationTypeID.ToString() + ' ' + Description;
            MobjclsBLLScanning.clsDTOScanning.strMetaData = strMetaDetails;
            MobjclsBLLScanning.InsertDetails(MintNode, MobjclsBLLScanning.clsDTOScanning.strFileName, MobjclsBLLScanning.clsDTOScanning.strMetaData);
            //ImageControl1.Visible = true;
            //ImageControl1.Image = System.Drawing.Image.FromFile(RecentFilePath.ToString());
            //-------------------------------------------------------------------------------------------------------


            PrintImage = DestinationFilePath;
            BtnOpenApplication.Visible = false;
            LblMess1.Visible = false;
            LblMess2.Visible = false;
            PrintTStripButton.Enabled = true;
        }

        #endregion METHODS

        #region CONTROL EVENTS



        private void FrmScanning_Load(object sender, EventArgs e)
        {
            try
            {
                toolStripContainer1.LeftToolStripPanel.Visible = true;
                toolStripContainer1.RightToolStripPanel.Visible = true;
                TrvScan.Visible = true;
                ImageControl1.Visible = true;
                LoadMessage();
                LoadInitial();

                TrvScan.LabelEdit = PblnEditable;
                ContextMenuStripScan.Visible = PblnEditable;
                ContextMnuAddNewItem.Visible = PblnEditable;
                ContextMnuDelete.Visible = PblnEditable;
                ToolStripMenuItemEdit.Visible = PblnEditable;
                BtnAttach.Enabled = PblnEditable;

                ToolStripDropDownButtonDoc.Enabled = PblnEditable;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form Scanning:FrmScanning_Load" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmScanning_Load" + Ex.Message.ToString());
            }
        }

        private void TrvScan_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                pictureimage = "";

                if (e.Node.Nodes.Count > 0)
                {
                    e.Node.SelectedImageIndex = 1;
                }
                StrDescr = e.Node.Text;

                MintNode = Convert.ToInt32(e.Node.Tag);

                if (MintNode > 0)
                {
                    MobjclsBLLScanning.clsDTOScanning.strFileName = "";
                    MobjclsBLLScanning.SelectDetails(MintNode);
                    string FileName = MobjclsBLLScanning.clsDTOScanning.strFileName.Trim();
                    string FilePath = string.Format(@"{0}\{1}\{2}", ClsCommonSettings.strServerPath, "DocumentImages", FileName);
                    pictureimage = FileName;

                    if (FileName != string.Empty)
                    {
                        if (FilePath.IndexOf(":") == -1)
                            FilePath = "\\" + FilePath;

                        if (File.Exists(FilePath))
                        {
                            ShowFileContents(FilePath);
                            ToolStripButtonFitToScreen_Click(sender, e);
                        }
                        else
                        {
                            ImageControl1.Image = null;
                            webView.Url = null;
                        }
                    }
                    else
                    {
                        ImageControl1.Image = null;
                        webView.Url = null;
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void TrvScan_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    Point ptPoint = new Point(e.X, e.Y);
                    TreeNode TragetNode = (TreeNode)(this.TrvScan.GetNodeAt(ptPoint));
                    this.ContextMenuStripScan.Show(this.TrvScan, ptPoint);
                    this.Refresh();
                    Application.DoEvents();
                }

                if (TrvScan.SelectedNode.Text == "NewItem")
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 7302, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    TrvScan.LabelEdit = true;
                    TrvScan.SelectedNode.BeginEdit();
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }

        }

        private void TrvScan_Leave(object sender, EventArgs e)
        {
            try
            {
                if (TrvScan.SelectedNode.Text == "")
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 7301, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    TrvScan.LabelEdit = true;
                    TrvScan.SelectedNode.BeginEdit();
                }
                if (TrvScan.SelectedNode.Text == "NewItem")
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 7302, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    TrvScan.LabelEdit = true;
                    TrvScan.SelectedNode.BeginEdit();
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }

        }

        private void TrvScan_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            try
            {
                Parentnode = Convert.ToInt32(TrvScan.SelectedNode.Tag);

                if (e.Label != null)
                {
                    if (e.Label != "NewItem")
                    {
                        e.CancelEdit = false;
                        string insert = e.Label.Trim();
                        if (insert == "")
                        {
                            e.CancelEdit = true;
                            return;
                        }




                        MblnAddStatus = true;

                        if (Convert.ToInt32(e.Node.Tag) > 0)
                        {
                            MblnAddStatus = false;
                            Save(insert);
                        }
                        else
                        {
                            if (Save(insert))
                            {
                                e.Node.Tag = MintNode;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void ToolStripMenuItemEdit_Click(object sender, EventArgs e)
        {
            try
            {

                TrvScan.LabelEdit = true;
                TrvScan.SelectedNode.BeginEdit();
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void ContextMnuDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (TrvScan.SelectedNode.Level > 0)
                    DeleteNodes();
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form Scanning:DeleteNodes " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteNodes " + Ex.Message.ToString());
            }

        }

        private void MyPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            try
            {
                if (ImgPrintImage == null)
                    return;
                e.Graphics.DrawImage(ImageControl1.Image, 5, 5, ImgPrintImage.Width, ImgPrintImage.Height);
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }


        private void ContextMnuAddNewItem_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNode trv;
                trv = new TreeNode();

                StrDescr = TrvScan.SelectedNode.Text;

                MintNode = Convert.ToInt32(TrvScan.SelectedNode.Tag);

                if (MobjclsBLLScanning.SelectDetails(MintNode))
                {
                    if (MobjclsBLLScanning.clsDTOScanning.strFileName != "")
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 7305, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    }
                }
                else
                {
                    StrDescr = TrvScan.SelectedNode.Text;
                    trv.Text = "NewItem";
                    TrvScan.SelectedNode.Nodes.Add(trv);
                    trv.SelectedImageIndex = 0;
                    TrvScan.SelectedNode.ImageIndex = 1;
                    trv = GetNode("NewItem", TrvScan.Nodes);
                    TrvScan.LabelEdit = true;
                    TrvScan.SelectedNode = trv;
                    TrvScan.LabelEdit = true;
                    trv.BeginEdit();
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }
        private void PrintTStripButton_Click(object sender, EventArgs e)
        {

                try
                {
                    if (TrvScan.SelectedNode.Tag != null)
                    {
                        if (ImageControl1.Image != null)
                        {
                            PrintingImage();
                        }
                        else
                        {
                            webView.ShowPrintDialog();
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.MobjClsLogWriter.WriteLog(string.Format("Error on PrintTStripButton_Click() Form :{0}, Exception :{1}", this.Name, ex.ToString()), 3);
                }
         
        }


        private void BtnAttach_Click(object sender, EventArgs e)
        {
            try
            {
                if (TrvScan.SelectedNode.Text == TrvScan.Nodes[0].Text)
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 7304, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                }
                else if (TrvScan.SelectedNode.Nodes.Count > 0)
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 7304, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                }
                else
                {
                    ScanDocs();
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void ToolStripMenuItemAdd_Click(object sender, EventArgs e)
        {
            try
            {

                TreeNode trv;
                trv = new TreeNode();

                StrDescr = TrvScan.SelectedNode.Text;
                MintNode = Convert.ToInt32(TrvScan.SelectedNode.Tag);

                if (MobjclsBLLScanning.SelectDetails(MintNode))
                {
                    pictureimage = MobjclsBLLScanning.clsDTOScanning.strFileName;
                    if (pictureimage != "")
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 7305, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    }
                }
                else
                {
                    StrDescr = TrvScan.SelectedNode.Text;
                    trv.Text = "NewItem";
                    TrvScan.SelectedNode.Nodes.Add(trv);
                    trv.SelectedImageIndex = 0;
                    TrvScan.SelectedNode.ImageIndex = 1;
                    trv = GetNode("NewItem", TrvScan.Nodes);
                    TrvScan.SelectedNode = trv;
                    TrvScan.LabelEdit = true;
                    trv.BeginEdit();
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void ToolStripMnuItemEdit_Click(object sender, EventArgs e)
        {
            try
            {
                TrvScan.LabelEdit = true;
                TrvScan.SelectedNode.BeginEdit();

            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void ToolStripMenuItemDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (TrvScan.SelectedNode.Level > 0)
                    DeleteNodes();
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form Scanning:DeleteNodes " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteNodes " + Ex.Message.ToString());
            }
        }

        private void ToolStripButtonFitToScreen_Click(object sender, EventArgs e)
        {
            try
            {
                if (TrvScan.SelectedNode != null)
                {
                    if (TrvScan.SelectedNode != TrvScan.Nodes[0])
                    {
                        if (ImageControl1.Image != null)
                        {
                            ImageControl1.fittoscreen();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }

        }

        private void ToolStripButtonZoomOut_Click(object sender, EventArgs e)
        {
            try
            {
                if (TrvScan.SelectedNode != null)
                {
                    if (TrvScan.SelectedNode != TrvScan.Nodes[0])
                    {
                        if (ImageControl1.Image != null)
                        {
                            ImageControl1.ZoomOut();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

        private void ToolStripButtonZoomIn_Click(object sender, EventArgs e)
        {
            try
            {
                if (TrvScan.SelectedNode != null)
                {
                    if (TrvScan.SelectedNode != TrvScan.Nodes[0])
                    {
                        if (ImageControl1.Image != null)
                        {
                            ImageControl1.ZoomIn();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }


        private void ToolStripButtonSendMail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Scanner/Attached File";
                    if (pictureimage != "")
                    {

                        string filepat = "";
                        filepat = ClsCommonSettings.strServerPath.ToString();

                        if (filepat.IndexOf(":") == -1)
                            filepat = "\\" + filepat;

                        ObjEmailPopUp.mstrscanfilepath = filepat + @"\DocumentImages" + pictureimage;
                    }
                    else
                    {
                        ObjEmailPopUp.mstrscanfilepath = "";
                    }

                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }

        }
        #endregion CONTROL EVENTS

        private void BtnOpenApplication_Click(object sender, EventArgs e)
        {
            string appName = ClsCommonSettings.strServerPath + @"\DocumentImages\" + pictureimage;
            try
            {
                if (appName.IndexOf(":") == -1)
                    appName = "\\" + appName;
                System.Diagnostics.Process objProc = new System.Diagnostics.Process();
                objProc.StartInfo.FileName = appName;
                objProc.Start();

                objProc = null;
            }
            catch (Exception)
            {
                MessageBox.Show("Windows can not open this file. Application Missing.", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }//class
}//namespace