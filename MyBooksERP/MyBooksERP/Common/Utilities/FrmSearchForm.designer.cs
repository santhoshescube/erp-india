﻿namespace MyBooksERP
{
    partial class FrmSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSearchForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlSearch = new DevComponents.DotNetBar.PanelEx();
            this.txtFullCriteria = new System.Windows.Forms.RichTextBox();
            this.lblValue2 = new DevComponents.DotNetBar.LabelX();
            this.BtnShow = new System.Windows.Forms.Button();
            this.txtValue2 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboCriteria = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblValue1 = new DevComponents.DotNetBar.LabelX();
            this.lblCriteria = new DevComponents.DotNetBar.LabelX();
            this.lblFilterOn = new DevComponents.DotNetBar.LabelX();
            this.lblOperationType = new DevComponents.DotNetBar.LabelX();
            this.cboFilterOn = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboOperationType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtValue1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.dtpValue2 = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.dtpValue1 = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.BtnPrevious = new System.Windows.Forms.Button();
            this.BtnNext = new System.Windows.Forms.Button();
            this.BtnOK = new System.Windows.Forms.Button();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.lblAdvanceSearchStatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvData = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnRefine = new System.Windows.Forms.Button();
            this.tcAdvanceSearch = new System.Windows.Forms.TabControl();
            this.tcAllData = new System.Windows.Forms.TabPage();
            this.tpRefinedData = new System.Windows.Forms.TabPage();
            this.dgvRefinedData = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpValue2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpValue1)).BeginInit();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.tcAdvanceSearch.SuspendLayout();
            this.tcAllData.SuspendLayout();
            this.tpRefinedData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRefinedData)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSearch
            // 
            this.pnlSearch.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlSearch.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlSearch.Controls.Add(this.txtFullCriteria);
            this.pnlSearch.Controls.Add(this.lblValue2);
            this.pnlSearch.Controls.Add(this.BtnShow);
            this.pnlSearch.Controls.Add(this.txtValue2);
            this.pnlSearch.Controls.Add(this.cboCriteria);
            this.pnlSearch.Controls.Add(this.lblValue1);
            this.pnlSearch.Controls.Add(this.lblCriteria);
            this.pnlSearch.Controls.Add(this.lblFilterOn);
            this.pnlSearch.Controls.Add(this.lblOperationType);
            this.pnlSearch.Controls.Add(this.cboFilterOn);
            this.pnlSearch.Controls.Add(this.cboOperationType);
            this.pnlSearch.Controls.Add(this.txtValue1);
            this.pnlSearch.Controls.Add(this.dtpValue2);
            this.pnlSearch.Controls.Add(this.dtpValue1);
            this.pnlSearch.Location = new System.Drawing.Point(8, 6);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(558, 144);
            this.pnlSearch.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlSearch.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlSearch.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlSearch.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlSearch.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlSearch.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlSearch.Style.GradientAngle = 90;
            this.pnlSearch.TabIndex = 10;
            // 
            // txtFullCriteria
            // 
            this.txtFullCriteria.BackColor = System.Drawing.Color.White;
            this.txtFullCriteria.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFullCriteria.Location = new System.Drawing.Point(333, 9);
            this.txtFullCriteria.Name = "txtFullCriteria";
            this.txtFullCriteria.ReadOnly = true;
            this.txtFullCriteria.Size = new System.Drawing.Size(221, 123);
            this.txtFullCriteria.TabIndex = 25;
            this.txtFullCriteria.Text = "";
            // 
            // lblValue2
            // 
            // 
            // 
            // 
            this.lblValue2.BackgroundStyle.Class = "";
            this.lblValue2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblValue2.Location = new System.Drawing.Point(12, 106);
            this.lblValue2.Name = "lblValue2";
            this.lblValue2.Size = new System.Drawing.Size(49, 23);
            this.lblValue2.TabIndex = 24;
            this.lblValue2.Text = "Value 2";
            // 
            // BtnShow
            // 
            this.BtnShow.Location = new System.Drawing.Point(230, 106);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(75, 23);
            this.BtnShow.TabIndex = 7;
            this.BtnShow.Text = "Show";
            this.BtnShow.UseVisualStyleBackColor = true;
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // txtValue2
            // 
            // 
            // 
            // 
            this.txtValue2.Border.Class = "TextBoxBorder";
            this.txtValue2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtValue2.Location = new System.Drawing.Point(103, 106);
            this.txtValue2.Name = "txtValue2";
            this.txtValue2.Size = new System.Drawing.Size(121, 22);
            this.txtValue2.TabIndex = 4;
            this.txtValue2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValue2_KeyPress);
            this.txtValue2.TextChanged += new System.EventHandler(this.txtValue2_TextChanged);
            // 
            // cboCriteria
            // 
            this.cboCriteria.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCriteria.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCriteria.DisplayMember = "Text";
            this.cboCriteria.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCriteria.DropDownHeight = 80;
            this.cboCriteria.FormattingEnabled = true;
            this.cboCriteria.IntegralHeight = false;
            this.cboCriteria.ItemHeight = 16;
            this.cboCriteria.Location = new System.Drawing.Point(103, 57);
            this.cboCriteria.Name = "cboCriteria";
            this.cboCriteria.Size = new System.Drawing.Size(224, 22);
            this.cboCriteria.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCriteria.TabIndex = 2;
            this.cboCriteria.SelectedIndexChanged += new System.EventHandler(this.cboCriteria_SelectedIndexChanged);
            this.cboCriteria.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCriteria_KeyPress);
            // 
            // lblValue1
            // 
            // 
            // 
            // 
            this.lblValue1.BackgroundStyle.Class = "";
            this.lblValue1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblValue1.Location = new System.Drawing.Point(12, 81);
            this.lblValue1.Name = "lblValue1";
            this.lblValue1.Size = new System.Drawing.Size(49, 23);
            this.lblValue1.TabIndex = 18;
            this.lblValue1.Text = "Value 1";
            // 
            // lblCriteria
            // 
            // 
            // 
            // 
            this.lblCriteria.BackgroundStyle.Class = "";
            this.lblCriteria.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCriteria.Location = new System.Drawing.Point(12, 57);
            this.lblCriteria.Name = "lblCriteria";
            this.lblCriteria.Size = new System.Drawing.Size(75, 23);
            this.lblCriteria.TabIndex = 15;
            this.lblCriteria.Text = "Criteria";
            // 
            // lblFilterOn
            // 
            // 
            // 
            // 
            this.lblFilterOn.BackgroundStyle.Class = "";
            this.lblFilterOn.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFilterOn.Location = new System.Drawing.Point(12, 33);
            this.lblFilterOn.Name = "lblFilterOn";
            this.lblFilterOn.Size = new System.Drawing.Size(75, 23);
            this.lblFilterOn.TabIndex = 14;
            this.lblFilterOn.Text = "Filter On";
            // 
            // lblOperationType
            // 
            // 
            // 
            // 
            this.lblOperationType.BackgroundStyle.Class = "";
            this.lblOperationType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblOperationType.Location = new System.Drawing.Point(12, 9);
            this.lblOperationType.Name = "lblOperationType";
            this.lblOperationType.Size = new System.Drawing.Size(87, 23);
            this.lblOperationType.TabIndex = 11;
            this.lblOperationType.Text = "Operation Type";
            // 
            // cboFilterOn
            // 
            this.cboFilterOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFilterOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFilterOn.DisplayMember = "Text";
            this.cboFilterOn.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboFilterOn.DropDownHeight = 80;
            this.cboFilterOn.FormattingEnabled = true;
            this.cboFilterOn.IntegralHeight = false;
            this.cboFilterOn.ItemHeight = 16;
            this.cboFilterOn.Location = new System.Drawing.Point(103, 33);
            this.cboFilterOn.Name = "cboFilterOn";
            this.cboFilterOn.Size = new System.Drawing.Size(224, 22);
            this.cboFilterOn.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboFilterOn.TabIndex = 1;
            this.cboFilterOn.SelectedIndexChanged += new System.EventHandler(this.cboFilterOn_SelectedIndexChanged);
            this.cboFilterOn.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboFilterOn_KeyPress);
            // 
            // cboOperationType
            // 
            this.cboOperationType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOperationType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOperationType.DisplayMember = "Text";
            this.cboOperationType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboOperationType.DropDownHeight = 80;
            this.cboOperationType.FormattingEnabled = true;
            this.cboOperationType.IntegralHeight = false;
            this.cboOperationType.ItemHeight = 16;
            this.cboOperationType.Location = new System.Drawing.Point(103, 9);
            this.cboOperationType.Name = "cboOperationType";
            this.cboOperationType.Size = new System.Drawing.Size(224, 22);
            this.cboOperationType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboOperationType.TabIndex = 0;
            this.cboOperationType.SelectedIndexChanged += new System.EventHandler(this.cboOperationType_SelectedIndexChanged);
            this.cboOperationType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboOperationType_KeyPress);
            // 
            // txtValue1
            // 
            // 
            // 
            // 
            this.txtValue1.Border.Class = "TextBoxBorder";
            this.txtValue1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtValue1.Location = new System.Drawing.Point(103, 81);
            this.txtValue1.Name = "txtValue1";
            this.txtValue1.Size = new System.Drawing.Size(121, 22);
            this.txtValue1.TabIndex = 3;
            this.txtValue1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValue1_KeyPress);
            this.txtValue1.TextChanged += new System.EventHandler(this.txtValue1_TextChanged);
            // 
            // dtpValue2
            // 
            // 
            // 
            // 
            this.dtpValue2.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtpValue2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpValue2.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtpValue2.ButtonDropDown.Visible = true;
            this.dtpValue2.CustomFormat = "dd-MMM-yyyy";
            this.dtpValue2.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.dtpValue2.IsPopupCalendarOpen = false;
            this.dtpValue2.Location = new System.Drawing.Point(103, 106);
            // 
            // 
            // 
            this.dtpValue2.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpValue2.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.dtpValue2.MonthCalendar.BackgroundStyle.Class = "";
            this.dtpValue2.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpValue2.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtpValue2.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtpValue2.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpValue2.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtpValue2.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtpValue2.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtpValue2.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtpValue2.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.dtpValue2.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpValue2.MonthCalendar.DisplayMonth = new System.DateTime(2011, 10, 1, 0, 0, 0, 0);
            this.dtpValue2.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dtpValue2.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpValue2.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtpValue2.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpValue2.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtpValue2.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.dtpValue2.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpValue2.MonthCalendar.TodayButtonVisible = true;
            this.dtpValue2.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dtpValue2.Name = "dtpValue2";
            this.dtpValue2.Size = new System.Drawing.Size(121, 22);
            this.dtpValue2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtpValue2.TabIndex = 6;
            this.dtpValue2.Value = new System.DateTime(2011, 12, 19, 17, 35, 52, 0);
            this.dtpValue2.Visible = false;
            this.dtpValue2.ValueChanged += new System.EventHandler(this.dtpValue2_ValueChanged);
            // 
            // dtpValue1
            // 
            // 
            // 
            // 
            this.dtpValue1.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtpValue1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpValue1.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtpValue1.ButtonDropDown.Visible = true;
            this.dtpValue1.CustomFormat = "dd-MMM-yyyy";
            this.dtpValue1.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.dtpValue1.IsPopupCalendarOpen = false;
            this.dtpValue1.Location = new System.Drawing.Point(103, 82);
            // 
            // 
            // 
            this.dtpValue1.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpValue1.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.dtpValue1.MonthCalendar.BackgroundStyle.Class = "";
            this.dtpValue1.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpValue1.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtpValue1.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtpValue1.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpValue1.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtpValue1.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtpValue1.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtpValue1.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtpValue1.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.dtpValue1.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpValue1.MonthCalendar.DisplayMonth = new System.DateTime(2011, 10, 1, 0, 0, 0, 0);
            this.dtpValue1.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dtpValue1.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpValue1.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtpValue1.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpValue1.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtpValue1.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.dtpValue1.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpValue1.MonthCalendar.TodayButtonVisible = true;
            this.dtpValue1.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dtpValue1.Name = "dtpValue1";
            this.dtpValue1.Size = new System.Drawing.Size(121, 22);
            this.dtpValue1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtpValue1.TabIndex = 5;
            this.dtpValue1.Value = new System.DateTime(2011, 12, 19, 17, 35, 34, 0);
            this.dtpValue1.Visible = false;
            this.dtpValue1.ValueChanged += new System.EventHandler(this.dtpValue1_ValueChanged);
            // 
            // BtnPrevious
            // 
            this.BtnPrevious.Location = new System.Drawing.Point(7, 322);
            this.BtnPrevious.Name = "BtnPrevious";
            this.BtnPrevious.Size = new System.Drawing.Size(43, 23);
            this.BtnPrevious.TabIndex = 2;
            this.BtnPrevious.Text = "<<";
            this.BtnPrevious.UseVisualStyleBackColor = true;
            this.BtnPrevious.Click += new System.EventHandler(this.BtnPrevious_Click);
            // 
            // BtnNext
            // 
            this.BtnNext.Location = new System.Drawing.Point(52, 322);
            this.BtnNext.Name = "BtnNext";
            this.BtnNext.Size = new System.Drawing.Size(43, 23);
            this.BtnNext.TabIndex = 3;
            this.BtnNext.Text = ">>";
            this.BtnNext.UseVisualStyleBackColor = true;
            this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // BtnOK
            // 
            this.BtnOK.Location = new System.Drawing.Point(408, 324);
            this.BtnOK.Name = "BtnOK";
            this.BtnOK.Size = new System.Drawing.Size(75, 23);
            this.BtnOK.TabIndex = 1;
            this.BtnOK.Text = "OK";
            this.BtnOK.UseVisualStyleBackColor = true;
            this.BtnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.lblAdvanceSearchStatus);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 352);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(574, 26);
            this.pnlBottom.TabIndex = 252;
            // 
            // lblAdvanceSearchStatus
            // 
            this.lblAdvanceSearchStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblAdvanceSearchStatus.CloseButtonVisible = false;
            this.lblAdvanceSearchStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAdvanceSearchStatus.Image = ((System.Drawing.Image)(resources.GetObject("lblAdvanceSearchStatus.Image")));
            this.lblAdvanceSearchStatus.Location = new System.Drawing.Point(0, 0);
            this.lblAdvanceSearchStatus.Name = "lblAdvanceSearchStatus";
            this.lblAdvanceSearchStatus.OptionsButtonVisible = false;
            this.lblAdvanceSearchStatus.Size = new System.Drawing.Size(572, 24);
            this.lblAdvanceSearchStatus.TabIndex = 20;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "SO No";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Value";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            this.dgvData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle25;
            this.dgvData.BackgroundColor = System.Drawing.Color.White;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle26;
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvData.Location = new System.Drawing.Point(3, 3);
            this.dgvData.Name = "dgvData";
            this.dgvData.ReadOnly = true;
            this.dgvData.RowHeadersWidth = 60;
            this.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvData.Size = new System.Drawing.Size(544, 140);
            this.dgvData.TabIndex = 0;
            this.dgvData.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvData_RowsAdded);
            this.dgvData.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvData_RowsRemoved);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(327, 324);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 4;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnRefine
            // 
            this.btnRefine.Location = new System.Drawing.Point(246, 324);
            this.btnRefine.Name = "btnRefine";
            this.btnRefine.Size = new System.Drawing.Size(75, 23);
            this.btnRefine.TabIndex = 254;
            this.btnRefine.Text = "Refine";
            this.btnRefine.UseVisualStyleBackColor = true;
            this.btnRefine.Visible = false;
            this.btnRefine.Click += new System.EventHandler(this.btnRefine_Click);
            // 
            // tcAdvanceSearch
            // 
            this.tcAdvanceSearch.Controls.Add(this.tcAllData);
            this.tcAdvanceSearch.Controls.Add(this.tpRefinedData);
            this.tcAdvanceSearch.Location = new System.Drawing.Point(8, 144);
            this.tcAdvanceSearch.Name = "tcAdvanceSearch";
            this.tcAdvanceSearch.SelectedIndex = 0;
            this.tcAdvanceSearch.Size = new System.Drawing.Size(558, 173);
            this.tcAdvanceSearch.TabIndex = 255;
            // 
            // tcAllData
            // 
            this.tcAllData.Controls.Add(this.dgvData);
            this.tcAllData.Location = new System.Drawing.Point(4, 23);
            this.tcAllData.Name = "tcAllData";
            this.tcAllData.Padding = new System.Windows.Forms.Padding(3);
            this.tcAllData.Size = new System.Drawing.Size(550, 146);
            this.tcAllData.TabIndex = 1;
            this.tcAllData.Text = "All Data";
            this.tcAllData.UseVisualStyleBackColor = true;
            // 
            // tpRefinedData
            // 
            this.tpRefinedData.Controls.Add(this.dgvRefinedData);
            this.tpRefinedData.Location = new System.Drawing.Point(4, 23);
            this.tpRefinedData.Name = "tpRefinedData";
            this.tpRefinedData.Padding = new System.Windows.Forms.Padding(3);
            this.tpRefinedData.Size = new System.Drawing.Size(550, 146);
            this.tpRefinedData.TabIndex = 0;
            this.tpRefinedData.Text = "Refined Data";
            this.tpRefinedData.UseVisualStyleBackColor = true;
            // 
            // dgvRefinedData
            // 
            this.dgvRefinedData.AllowUserToAddRows = false;
            this.dgvRefinedData.AllowUserToDeleteRows = false;
            this.dgvRefinedData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRefinedData.BackgroundColor = System.Drawing.Color.White;
            this.dgvRefinedData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRefinedData.DefaultCellStyle = dataGridViewCellStyle27;
            this.dgvRefinedData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRefinedData.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvRefinedData.Location = new System.Drawing.Point(3, 3);
            this.dgvRefinedData.Name = "dgvRefinedData";
            this.dgvRefinedData.ReadOnly = true;
            this.dgvRefinedData.RowHeadersWidth = 60;
            this.dgvRefinedData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRefinedData.Size = new System.Drawing.Size(544, 140);
            this.dgvRefinedData.TabIndex = 1;
            this.dgvRefinedData.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvRefinedData_RowsAdded);
            this.dgvRefinedData.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvRefinedData_RowsRemoved);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(489, 324);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FrmSearchForm
            // 
            this.AcceptButton = this.BtnShow;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 378);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.tcAdvanceSearch);
            this.Controls.Add(this.btnRefine);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.BtnOK);
            this.Controls.Add(this.BtnNext);
            this.Controls.Add(this.BtnPrevious);
            this.Controls.Add(this.pnlSearch);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSearchForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Advance Search - Sales Order";
            this.Load += new System.EventHandler(this.FrmSearchForm_Load);
            this.pnlSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtpValue2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpValue1)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.tcAdvanceSearch.ResumeLayout(false);
            this.tcAllData.ResumeLayout(false);
            this.tpRefinedData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRefinedData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx pnlSearch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboFilterOn;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboOperationType;
        private DevComponents.DotNetBar.Controls.TextBoxX txtValue1;
        private DevComponents.DotNetBar.LabelX lblCriteria;
        private DevComponents.DotNetBar.LabelX lblFilterOn;
        private DevComponents.DotNetBar.LabelX lblOperationType;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtpValue2;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtpValue1;
        private DevComponents.DotNetBar.LabelX lblValue1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtValue2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCriteria;
        private System.Windows.Forms.Button BtnPrevious;
        private System.Windows.Forms.Button BtnNext;
        private System.Windows.Forms.Button BtnOK;
        private System.Windows.Forms.Button BtnShow;
        private DevComponents.DotNetBar.LabelX lblValue2;
        private System.Windows.Forms.Panel pnlBottom;
        private DevComponents.DotNetBar.Controls.WarningBox lblAdvanceSearchStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvData;
        private System.Windows.Forms.RichTextBox txtFullCriteria;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnRefine;
        private System.Windows.Forms.TabControl tcAdvanceSearch;
        private System.Windows.Forms.TabPage tpRefinedData;
        private System.Windows.Forms.TabPage tcAllData;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvRefinedData;
        private System.Windows.Forms.Button btnCancel;

    }
}