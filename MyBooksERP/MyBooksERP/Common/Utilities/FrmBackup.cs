﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public partial class FrmBackup : DevComponents.DotNetBar.Office2007Form
    {
        DataLayer objBkupCon = new DataLayer();
        string DBName = ClsCommonSettings.DbName;   

        public FrmBackup()
        {
            InitializeComponent();
        }

        private void BrowsePath_Click(object sender, EventArgs e)
        {
            String StrPath;
            //String UNCPath = "";

            Cursor.Current = Cursors.WaitCursor;

            FolderBrowser1.ShowDialog();
            StrPath = FolderBrowser1.SelectedPath;

            if (StrPath.Trim() == "")
            {
                return ;
            }

            FilePathTextBox.Text = StrPath;
            FileNameTextBox.Text = DateTime.Now.ToString("ddMMMyyyy") + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + DBName;
            Cursor.Current = Cursors.Default;

        }

         private void UpdateBackupDateinRegisry()
         {
             
            //clsConnection oCnn;
            //SqlDataReader sd;
            //ProjectSettings.clsMachineSettings objMachineSettings;
            //ProjectSettings.clsRegistry objReg;

            //int nInterval=3;
            //oCnn= new clsConnection(); 
            //sd = oCnn.ExecutesReader("Select ConfigurationValue from ConfigurationMaster where ConfigurationItem='BackupInterval' and ProductId=2");
            //if (sd.Read())
            //{
            //    nInterval = Convert.ToInt32(sd[0]);
            //}
            //sd.Close();

         

            //clsRegistry objReg ;
            //DateTime dt = ClsCommonSettings.GetServerDate().Date.AddDays(nInterval);
            //objReg.WriteToRegistry("SOFTWARE\\Mindsoft\\MyPayBackup", "MyPayBackup", "MyPayBackupDate", dt.Date.ToString());

        }



         private void BtnBackUp_Click(object sender, EventArgs e)
         {
             try
             {
                 //String GlStrMessageCaption;

                 Cursor.Current = Cursors.WaitCursor;
                 string FilePath;
                 string phyname;
                 string TSQL;
                 int Flag;


                 SqlDataReader dr;
                 phyname = "";
                 Flag = -1;


                 if (Directory.Exists(FilePathTextBox.Text.Trim()) == false)
                 {

                     MessageBox.Show("please select a valid folder", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                     return;
                 }

                 rtfMess.Text = "Database backup started.";
                 rtfMess.Refresh();

                 FileNameTextBox.Text = DateTime.Now.ToString("ddMMMyyyy") + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + DBName;

                 FilePath = FilePathTextBox.Text.Trim();

                 TSQL = "select phyname from Master..sysdevices where name like '" + FileNameTextBox.Text.Trim() + "'";
                 dr = objBkupCon.ExecuteReader(TSQL);

                 if (dr.Read())
                 {
                     phyname = Convert.ToString(dr["phyname"]);
                 }
                 dr.Close();

                 if (phyname.Trim() != "")
                 {
                     TSQL = "sp_dropdevice '" + phyname + "'";
                     objBkupCon.ExecuteQuery(TSQL);
                 }


                 TSQL = "sp_addumpdevice 'disk','" + FileNameTextBox.Text.Trim() + "','" + FilePath.Trim() + "\\" + FileNameTextBox.Text.Trim() + ".Bak'";
                 objBkupCon.ExecuteQuery(TSQL);

                 TSQL = "Backup database [" + DBName + "] To [" + FileNameTextBox.Text.Trim() + "]";
                 Flag = objBkupCon.ExecuteQuery(TSQL);

                 rtfMess.Text = "Backup process successfully completed.";
                 rtfMess.Refresh();
                 MessageBox.Show("Backup process successfully completed.", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                 Cursor.Current = Cursors.Default;

                 //UpdateBackupDateinRegisry();
                 this.Close();

                 Cursor.Current = Cursors.Default;
             }
             catch (Exception)
             {
                 Cursor.Current = Cursors.Default;
                 rtfMess.Text = "";
                 rtfMess.Refresh();
                 MessageBox.Show("You cannot back up database to a network drive if your account have not sufficient permission to access the network drive", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
             }
         }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmBackup_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Backup";
            objHelp.ShowDialog();
            objHelp = null;
        }
    }
}
