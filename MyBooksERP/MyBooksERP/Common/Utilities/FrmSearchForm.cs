﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
/* 
=================================================
   Author:		< Sanju >
   Create date: < 15 Dec 2011 >
   Description:	< Advance Search Form>
================================================
*/
namespace MyBooksERP
{
    public partial class FrmSearchForm : DevComponents.DotNetBar.Office2007Form
    {
        ClsLogWriter MobjLogs;
        clsBLLPermissionSettings MobjClsBLLPermissionSettings;
        ClsNotificationNew mObjNotification;

        clsBLLAdvanceSearch MobjclsBLLAdvanceSearch;
        clsDTOAdvanceSearch MobjclsDTOAdvanceSearch;
        DataTable datData;
        DataTable datCriteriaHistory = new DataTable();

        string strSearchCriteria = "";
        string strDisplaySearchCriteria = "";
        string strCompanyIDs = "";
        string strEmployeeIDs = "";
        int intSearchCounter = 0;
        int MintOperationType = -1;

        public DataLayer objclsConnection { get; set; }
        public DataTable datSerachedData = new DataTable();
        public string PstrAdditionalSearchCondition = "";

        public FrmSearchForm()
        {
            InitializeComponent();
        }

        public FrmSearchForm(int intOperationType)
        {
            InitializeComponent();
            MintOperationType = intOperationType;
        }

        private void FrmSearchForm_Load(object sender, EventArgs e)
        {
            try
            {
                MobjclsBLLAdvanceSearch = new clsBLLAdvanceSearch();
                MobjclsDTOAdvanceSearch = new clsDTOAdvanceSearch();
                MobjLogs = new ClsLogWriter(Application.StartupPath);
                mObjNotification = new ClsNotificationNew();

                MobjClsBLLPermissionSettings = new clsBLLPermissionSettings();
                datCriteriaHistory.Columns.Add("Criteria");
                datCriteriaHistory.Columns.Add("DisplayCriteria");
                datCriteriaHistory.Columns.Add("DisplayColumn");
                LoadCombos(0);

                if (MintOperationType >= 0)
                {
                    cboOperationType.SelectedValue = MintOperationType;
                    cboOperationType.Enabled = false;
                }
                dtpValue1.Value = ClsCommonSettings.GetServerDate();
                dtpValue2.Value = ClsCommonSettings.GetServerDate();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FrmSearchForm_Load() " + ex.Message);
                MobjLogs.WriteLog("Error in FrmSearchForm_Load() " + ex.Message, 2);
            }
        }

        private void LoadCombos(int intFillType)
        {
            try
            {
                DataTable datCombos = new DataTable();
                if (intFillType == 0)
                {
                    datCombos = null;
                    datCombos = MobjclsBLLAdvanceSearch.FillCombos(new string[] { "OperationTypeID,OperationType", "OperationTypeReference", "OperationTypeID in (" + (int)OperationType.CreditNote + "," + (int)OperationType.Receipt + "," + (int)OperationType.Payment + "," + (int)OperationType.DebitNote + "," + (int)OperationType.DeliveryNote + "," + (int)OperationType.GRN + "," + (int)OperationType.POS + "," + (int)OperationType.PurchaseIndent + "," + (int)OperationType.PurchaseInvoice + "," + (int)OperationType.PurchaseOrder + "," + (int)OperationType.PurchaseQuotation + "," + (int)OperationType.RFQ + "," + (int)OperationType.SalesInvoice + "," + (int)OperationType.SalesOrder + "," + (int)OperationType.SalesQuotation + "," + (int)OperationType.StockTransfer + "," + (int)OperationType.StockAdjustment + "," + (int)OperationType.Products + "," + (int)OperationType.MaterialReturn + "," + (int)OperationType.MaterialIssue + ")" });
                    cboOperationType.ValueMember = "OperationTypeID";
                    cboOperationType.DisplayMember = "OperationType";
                    cboOperationType.DataSource = datCombos;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadCombos() " + ex.Message);
                MobjLogs.WriteLog("Error in LoadCombos() " + ex.Message, 2);
            }
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            try
            {
                BtnShow.Enabled = false;

                if (intSearchCounter < datCriteriaHistory.Rows.Count)
                {
                    while (intSearchCounter != datCriteriaHistory.Rows.Count)
                    {
                        datCriteriaHistory.Rows.RemoveAt(datCriteriaHistory.Rows.Count - 1);
                    }
                }
                if (cboOperationType.SelectedIndex != -1)
                {
                    if (datCriteriaHistory.Rows.Count <= 0)
                    {
                        DataRow dr = datCriteriaHistory.NewRow();
                        if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.Products)
                        {
                            dr["Criteria"] = " 1=1 ";
                        }
                        else
                        {
                            dr["Criteria"] = "CompanyID in (" + strCompanyIDs + ") and UserID in (" + strEmployeeIDs + ")";
                        }
                        dr["DisplayCriteria"] = "Searched Criteria :\n";
                        datCriteriaHistory.Rows.Add(dr);
                        intSearchCounter = datCriteriaHistory.Rows.Count;
                    }
                    if (cboCriteria.SelectedIndex != -1 && cboFilterOn.SelectedIndex != -1)
                    {
                        if (cboFilterOn.SelectedValue.ToString() == "System.DateTime")
                        {
                            if (cboCriteria.SelectedValue.ToString() != "Between")
                            {
                                if (dtpValue1.Value.ToDateTime().ToString("yyyy MMM dd") != "0001 Jan 01")
                                {
                                    strSearchCriteria = " and " + "Convert(datetime, Convert(varchar," + cboFilterOn.Text.ToString() + ",106),106)" + " " + cboCriteria.SelectedValue.ToString() + "Convert(datetime, Convert(varchar, '" + dtpValue1.Value.ToString("yyyy MMM dd") + "',106),106)";
                                    strDisplaySearchCriteria = datCriteriaHistory.Rows.Count + ") " + cboFilterOn.Text.ToString() + " " + cboCriteria.Text.ToString() + " '" + dtpValue1.Value.ToString("dd-MMM-yyyy") + "'\n";
                                    DataRow dr2 = datCriteriaHistory.NewRow();
                                    dr2["Criteria"] = strSearchCriteria;
                                    dr2["DisplayCriteria"] = strDisplaySearchCriteria;
                                    dr2["DisplayColumn"] = cboFilterOn.Text.ToString();
                                    datCriteriaHistory.Rows.Add(dr2);
                                    intSearchCounter = datCriteriaHistory.Rows.Count;
                                }
                            }
                            else
                            {
                                if (dtpValue1.Value.ToDateTime().ToString("yyyy MMM dd") != "0001 Jan 01" && dtpValue2.Value.ToDateTime().ToString("yyyy MMM dd") != "0001 Jan 01")
                                {
                                    strSearchCriteria = " and " + " (" + "Convert(datetime, Convert(varchar," + cboFilterOn.Text.ToString() + ",106),106)" + " " + cboCriteria.SelectedValue.ToString() + " Convert(datetime, Convert(varchar ,'" + dtpValue1.Value.ToString("yyyy MMM dd") + "',106),106)" + " and " + "Convert(datetime, Convert(varchar, '" + dtpValue2.Value.ToString("yyyy MMM dd") + "',106),106)" + ")";
                                    strDisplaySearchCriteria = datCriteriaHistory.Rows.Count + ") " + cboFilterOn.Text.ToString() + " " + cboCriteria.Text.ToString() + " '" + dtpValue1.Value.ToString("dd-MMM-yyyy") + "'" + " and " + " '" + dtpValue2.Value.ToString("dd-MMM-yyyy") + "'" + "\n";
                                    DataRow dr2 = datCriteriaHistory.NewRow();
                                    dr2["Criteria"] = strSearchCriteria;
                                    dr2["DisplayCriteria"] = strDisplaySearchCriteria;
                                    dr2["DisplayColumn"] = cboFilterOn.Text.ToString();
                                    datCriteriaHistory.Rows.Add(dr2);
                                    intSearchCounter = datCriteriaHistory.Rows.Count;
                                }
                            }
                        }
                        else if (cboFilterOn.SelectedValue.ToString() == "System.Boolean")
                        {
                            strSearchCriteria = " and " + cboFilterOn.Text.ToString() + " = " + cboCriteria.SelectedValue.ToString();
                            strDisplaySearchCriteria = datCriteriaHistory.Rows.Count + ") " + cboFilterOn.Text.ToString() + " " + cboCriteria.Text.ToString() + "\n";
                            DataRow dr2 = datCriteriaHistory.NewRow();
                            dr2["Criteria"] = strSearchCriteria;
                            dr2["DisplayCriteria"] = strDisplaySearchCriteria;
                            dr2["DisplayColumn"] = cboFilterOn.Text.ToString();
                            datCriteriaHistory.Rows.Add(dr2);
                            intSearchCounter = datCriteriaHistory.Rows.Count;
                        }
                        else if (cboFilterOn.SelectedValue.ToString() == "System.String")
                        {
                            if (cboCriteria.SelectedValue.ToString() == "Between")
                            {
                                if (txtValue1.Text.Trim() != "" && txtValue2.Text.Trim() != "")
                                {
                                    strSearchCriteria = " and " + " ( upper(" + cboFilterOn.Text.ToString() + ") " + cboCriteria.SelectedValue.ToString() + "'" + txtValue1.Text.ToUpper() + "'" + " and " + "'" + txtValue2.Text.ToUpper() + "'" + ")";
                                    strDisplaySearchCriteria = datCriteriaHistory.Rows.Count + ") " + cboFilterOn.Text.ToString() + " " + cboCriteria.Text.ToString() + " '" + txtValue1.Text + "'" + " and " + " '" + txtValue2.Text + "'" + "\n";
                                    DataRow dr2 = datCriteriaHistory.NewRow();
                                    dr2["Criteria"] = strSearchCriteria;
                                    dr2["DisplayCriteria"] = strDisplaySearchCriteria;
                                    dr2["DisplayColumn"] = cboFilterOn.Text.ToString();
                                    datCriteriaHistory.Rows.Add(dr2);
                                    intSearchCounter = datCriteriaHistory.Rows.Count;
                                }
                            }
                            else if (cboCriteria.SelectedValue.ToString() == "Like")
                            {
                                if (txtValue1.Text.Trim() != "")
                                {
                                    strSearchCriteria = " and upper(" + cboFilterOn.Text.ToString() + ") " + cboCriteria.SelectedValue.ToString() + " '%" + txtValue1.Text.ToUpper() + "%'";
                                    strDisplaySearchCriteria = datCriteriaHistory.Rows.Count + ") " + cboFilterOn.Text.ToString() + " " + cboCriteria.Text.ToString() + " '" + txtValue1.Text + "'" + "\n";
                                    DataRow dr2 = datCriteriaHistory.NewRow();
                                    dr2["Criteria"] = strSearchCriteria;
                                    dr2["DisplayCriteria"] = strDisplaySearchCriteria;
                                    dr2["DisplayColumn"] = cboFilterOn.Text.ToString();
                                    datCriteriaHistory.Rows.Add(dr2);
                                    intSearchCounter = datCriteriaHistory.Rows.Count;
                                }
                            }
                            else
                            {
                                if (txtValue1.Text.Trim() != "")
                                {
                                    strSearchCriteria = " and upper(" + cboFilterOn.Text.ToString() + ") " + cboCriteria.SelectedValue.ToString() + "'" + txtValue1.Text.ToUpper() + "'";
                                    strDisplaySearchCriteria = datCriteriaHistory.Rows.Count + ") " + cboFilterOn.Text.ToString() + " " + cboCriteria.Text.ToString() + " '" + txtValue1.Text + "'" + "\n";
                                    DataRow dr2 = datCriteriaHistory.NewRow();
                                    dr2["Criteria"] = strSearchCriteria;
                                    dr2["DisplayCriteria"] = strDisplaySearchCriteria;
                                    dr2["DisplayColumn"] = cboFilterOn.Text.ToString();
                                    datCriteriaHistory.Rows.Add(dr2);
                                    intSearchCounter = datCriteriaHistory.Rows.Count;
                                }
                            }
                        }
                        else
                        {
                            if (cboCriteria.SelectedValue.ToString() == "Between")
                            {
                                if (txtValue1.Text.Trim() != "" && txtValue2.Text.Trim() != "")
                                {
                                    strSearchCriteria = " and " + " (" + cboFilterOn.Text.ToString() + " " + cboCriteria.SelectedValue.ToString() + " '" + txtValue1.Text + "'" + " and " + " '" + txtValue2.Text + "'" + ")";
                                    strDisplaySearchCriteria = datCriteriaHistory.Rows.Count + ") " + cboFilterOn.Text.ToString() + " " + cboCriteria.Text.ToString() + " '" + txtValue1.Text + "'" + " and " + " '" + txtValue2.Text + "'" + "\n";
                                    DataRow dr2 = datCriteriaHistory.NewRow();
                                    dr2["Criteria"] = strSearchCriteria;
                                    dr2["DisplayCriteria"] = strDisplaySearchCriteria;
                                    dr2["DisplayColumn"] = cboFilterOn.Text.ToString();
                                    datCriteriaHistory.Rows.Add(dr2);
                                    intSearchCounter = datCriteriaHistory.Rows.Count;
                                }
                            }
                            else if (cboCriteria.SelectedValue.ToString() == "Like")
                            {
                                if (txtValue1.Text.Trim() != "")
                                {
                                    strSearchCriteria = " and " + cboFilterOn.Text.ToString() + " " + cboCriteria.SelectedValue.ToString() + " '%" + txtValue1.Text + "%'";
                                    strDisplaySearchCriteria = datCriteriaHistory.Rows.Count + ") " + cboFilterOn.Text.ToString() + " " + cboCriteria.Text.ToString() + " '" + txtValue1.Text + "'" + "\n";
                                    DataRow dr2 = datCriteriaHistory.NewRow();
                                    dr2["Criteria"] = strSearchCriteria;
                                    dr2["DisplayCriteria"] = strDisplaySearchCriteria;
                                    dr2["DisplayColumn"] = cboFilterOn.Text.ToString();
                                    datCriteriaHistory.Rows.Add(dr2);
                                    intSearchCounter = datCriteriaHistory.Rows.Count;
                                }
                            }
                            else
                            {
                                if (txtValue1.Text.Trim() != "")
                                {
                                    strSearchCriteria = " and " + cboFilterOn.Text.ToString() + " " + cboCriteria.SelectedValue.ToString() + " '" + txtValue1.Text + "'";
                                    strDisplaySearchCriteria = datCriteriaHistory.Rows.Count + ") " + cboFilterOn.Text.ToString() + " " + cboCriteria.Text.ToString() + " '" + txtValue1.Text + "'" + "\n";
                                    DataRow dr2 = datCriteriaHistory.NewRow();
                                    dr2["Criteria"] = strSearchCriteria;
                                    dr2["DisplayCriteria"] = strDisplaySearchCriteria;
                                    dr2["DisplayColumn"] = cboFilterOn.Text.ToString();
                                    datCriteriaHistory.Rows.Add(dr2);
                                    intSearchCounter = datCriteriaHistory.Rows.Count;
                                }
                            }
                        }
                    }
                }
                DisplaySearchedData();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnShow_Click() " + ex.Message);
                MobjLogs.WriteLog("Error in BtnShow_Click() " + ex.Message, 2);
                ClearControls(0);
            }
        }

        private void cboOperationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ClearControls(1);
                if (cboOperationType.SelectedIndex != -1)
                {
                    //int intControlID = 0;
                    //int intControlIDEmployee = 0;
                    //if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesQuotation)
                    //{
                    //    intControlID = (int)ControlOperationType.SalQuoCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.SalQuoEmployee;
                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesOrder)
                    //{
                    //    intControlID = (int)ControlOperationType.SalOrdCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.SalOrdEmployee;
                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesInvoice)
                    //{
                    //    intControlID = (int)ControlOperationType.SalInvCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.SalInvEmployee;

                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseQuotation)
                    //{
                    //    intControlID = (int)ControlOperationType.PurQuoCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.PurQuoEmployee;

                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseOrder)
                    //{
                    //    intControlID = (int)ControlOperationType.PurOrdCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.PurOrdEmployee;

                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.GRN)
                    //{
                    //    intControlID = (int)ControlOperationType.GRNCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.GRNEmployee;

                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseInvoice)
                    //{
                    //    intControlID = (int)ControlOperationType.PurInvCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.PurInvEmployee;

                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseIndent)
                    //{
                    //    intControlID = (int)ControlOperationType.PurIndCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.PurIndEmployee;

                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.RFQ)
                    //{
                    //    intControlID = (int)ControlOperationType.RFQCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.RFQEmployee;

                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.DebitNote)
                    //{
                    //    intControlID = (int)ControlOperationType.PurRetCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.PurRetEmployee;

                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.CreditNote)
                    //{
                    //    intControlID = (int)ControlOperationType.CreNoteCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.CreNoteEmployee;

                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.POS)
                    //{
                    //    intControlID = (int)ControlOperationType.POSCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.POSEmployee;

                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.DeliveryNote)
                    //{
                    //    intControlID = (int)ControlOperationType.DelNoteCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.DelNoteEmployee;

                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.Products)
                    //{
                    //    //intControlID = (int)ControlOperationType.DelNoteCompany;
                    //    //intControlIDEmployee = (int)ControlOperationType.DelNoteEmployee;

                    //    intControlID = 0;
                    //    intControlIDEmployee = 0;
                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.StockAdjustment)
                    //{
                    //    intControlID = (int)ControlOperationType.StAdjCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.StAdjEmployee;

                    //}
                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.StockTransfer)
                    //{
                    //    intControlID = (int)ControlOperationType.StTranCompany;
                    //    intControlIDEmployee = (int)ControlOperationType.StTranEmployee;

                    //}

                    //DataTable datPermittedCompanies = null;
                    //DataTable datPermittedEmployees = null;
                    strCompanyIDs = "";
                    strEmployeeIDs = "";
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //{
                    DataTable datTemp = new DataTable();
                    datTemp = MobjclsBLLAdvanceSearch.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });

                    for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                    {
                        strCompanyIDs += datTemp.Rows[iCounter]["CompanyID"].ToString() + ",";
                        //strCompanies += datTemp.Rows[iCounter]["Name"].ToString() + ",";
                    }
                    strCompanyIDs = strCompanyIDs.Remove(strCompanyIDs.Length - 1);
                    //strCompanies = strCompanies.Remove(strCompanies.Length - 1);

                    DataTable datTempEmp = new DataTable();
                    datTempEmp = MobjclsBLLAdvanceSearch.FillCombos(new string[] { "UserID,UserName", "UserMaster", "" });

                    for (int iCounter = 0; iCounter <= datTempEmp.Rows.Count - 1; iCounter++)
                    {
                        strEmployeeIDs += datTempEmp.Rows[iCounter]["UserID"].ToString() + ",";
                        //strEmployeeIDs += datTemp.Rows[iCounter]["Name"].ToString() + ",";
                    }
                    strEmployeeIDs = strEmployeeIDs.Remove(strEmployeeIDs.Length - 1);
                    //strCompanies = strCompanies.Remove(strCompanies.Length - 1);

                    //}
                    //else
                    //{
                    //    datPermittedCompanies = MobjclsBLLAdvanceSearch.FillCombos(new string[] { "IsEnabled", "STRoleFieldsDetails", "Controlid=" + intControlID + " and roleID=" + ClsCommonSettings.RoleID + " and companyid=" + ClsCommonSettings.CompanyID + "" });
                    //    if (datPermittedCompanies.Rows.Count > 0)
                    //    {
                    //        strCompanyIDs = datPermittedCompanies.Rows[0]["IsEnabled"].ToString();
                    //    }
                    //    else
                    //    {
                    //        strCompanyIDs = ClsCommonSettings.CompanyID.ToString();
                    //    }


                    //    //datPermittedEmployees = MobjclsBLLAdvanceSearch.FillCombos(new string[] { "IsEnabled", "STRoleFieldsDetails", "Controlid=" + intControlIDEmployee + " and roleID=" + ClsCommonSettings.RoleID + " and companyid=" + ClsCommonSettings.CompanyID + "" });
                    //    //if (datPermittedEmployees.Rows.Count > 0)
                    //    //{
                    //    //    strEmployeeIDs = datPermittedEmployees.Rows[0]["IsEnabled"].ToString();
                    //    //}
                    //    //else
                    //    //{

                    //    //    strEmployeeIDs = ClsCommonSettings.intEmployeeID.ToString();
                    //    //}

                    //    DataTable datTemp = MobjclsBLLAdvanceSearch.GetUserByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, intControlID, intControlIDEmployee, ClsCommonSettings.UserID);
                    //    if (datTemp != null)
                    //    {
                    //        if (datTemp.Rows.Count > 0)
                    //        {
                    //            for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                    //            {
                    //                strEmployeeIDs += datTemp.Rows[iCounter]["UserID"].ToString() + ",";
                    //                //strCompanies += datTemp.Rows[iCounter]["Name"].ToString() + ",";
                    //            }
                    //            strEmployeeIDs = strEmployeeIDs.Remove(strEmployeeIDs.Length - 1);
                    //        }
                    //    }
                    //}

                    this.Text = "Advance Search [ " + cboOperationType.Text + " ]";

                    if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesQuotation)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 2;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesOrder)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 4;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesInvoice)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 6;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseQuotation)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 8;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseOrder)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 10;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.GRN)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 12;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseInvoice)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 14;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseIndent)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 16;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.RFQ)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 18;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.DebitNote)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 20;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.CreditNote)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 22;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.POS)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 24;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.DeliveryNote)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 26;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.Products)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 28;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.StockAdjustment)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 30;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.StockTransfer)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 32;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.MaterialReturn)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 34;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.MaterialIssue)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 36;
                        GetData();
                    }

                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.Receipt)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 44;
                        GetData();
                    }
                    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.Payment)
                    {
                        MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 44;
                        GetData();
                    }
                }
                else
                {
                    this.Text = "Advance Search ";
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in cboOperationType_SelectedIndexChanged() " + ex.Message);
                MobjLogs.WriteLog("Error in cboOperationType_SelectedIndexChanged() " + ex.Message, 2);
            }
        }

        private void DisplaySearchedData()
        {
            try
            {
                MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.strSearchCondition = "";
                int intRowerCounter = 0;
                string strDisplayCriterias = "";

                foreach (DataRow drCriteriaHistory in datCriteriaHistory.Rows)
                {
                    intRowerCounter++;
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.strSearchCondition = MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.strSearchCondition + drCriteriaHistory["Criteria"].ToString();
                    strDisplayCriterias = strDisplayCriterias + drCriteriaHistory["DisplayCriteria"].ToString();

                    if (intRowerCounter == intSearchCounter)
                        break;
                }
                //txtFullCriteria.Text = MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.strSearchCondition;
                //lblAdvanceSearchStatus.Text = MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.strSearchCondition;
                if (PstrAdditionalSearchCondition != "")
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.strSearchCondition = MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.strSearchCondition + " and " + PstrAdditionalSearchCondition;
                }
                txtFullCriteria.Text = strDisplayCriterias;

                if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesQuotation)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 3;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesOrder)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 5;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.SalesInvoice)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 7;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseQuotation)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 9;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseOrder)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 11;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.GRN)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 13;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseInvoice)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 15;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.PurchaseIndent)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 17;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.RFQ)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 19;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.DebitNote)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 21;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.CreditNote)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 23;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.POS)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 25;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.DeliveryNote)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 27;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.Products)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 29;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.StockAdjustment)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 31;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.StockTransfer)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 33;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.MaterialReturn)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 35;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.MaterialIssue)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 37;
                }
            
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.Receipt)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 45;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.Payment)
                {
                    MobjclsBLLAdvanceSearch.objclsDTOAdvanceSearch.intMode = 46;
                }
                datSerachedData = MobjclsBLLAdvanceSearch.GetSearchData();
                //datData.DefaultView.RowFilter = strSearchCriteria;
                if (datSerachedData.Rows.Count > 0)
                {
                    dgvData.DataSource = datSerachedData;
                    dgvData.Columns["ID"].Visible = false;
                    dgvData.Columns["CompanyID"].Visible = false;
                    dgvData.Columns["UserID"].Visible = false;
                    //if (cboFilterOn.SelectedIndex != -1)
                    //    dgvData.Columns[cboFilterOn.Text.ToString()].DisplayIndex = 1;
                    RefinedData();
                }
                else
                {
                    dgvData.DataSource = null;
                    dgvRefinedData.DataSource = null;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplaySearchedData() " + ex.Message);
                MobjLogs.WriteLog("Error in DisplaySearchedData() " + ex.Message, 2);
            }
        }

        public void GetData()
        {
            try
            {
                cboFilterOn.DataSource = null;
                datData = MobjclsBLLAdvanceSearch.GetSearchColumn();

                DataTable datHeaders = new DataTable();
                datHeaders.Columns.Add("ColumnName");
                datHeaders.Columns.Add("Datatype");

                foreach (DataColumn dc in datData.Columns)
                {
                    if (dc.ColumnName.ToString() != "ID")
                    {
                        DataRow dr = datHeaders.NewRow();
                        dr["ColumnName"] = dc.ColumnName.ToString();
                        dr["Datatype"] = dc.DataType.ToString();
                        datHeaders.Rows.Add(dr);
                    }
                }
                cboFilterOn.DisplayMember = "ColumnName";
                cboFilterOn.ValueMember = "Datatype";
                cboFilterOn.DataSource = datHeaders;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in GetData() " + ex.Message);
                MobjLogs.WriteLog("Error in GetData() " + ex.Message, 2);
            }
        }

        public void FillCriteria()
        {
            try
            {
                cboCriteria.DataSource = null;
                DataTable datCriteria = new DataTable();

                datCriteria.Columns.Add("CriteriaDescription");
                datCriteria.Columns.Add("CriteriaValue");

                if (cboFilterOn.SelectedValue.ToString() == "System.Int32" || cboFilterOn.SelectedValue.ToString() == "System.Int64" || cboFilterOn.SelectedValue.ToString() == "System.Decimal" || cboFilterOn.SelectedValue.ToString() == "System.DateTime")
                {
                    DataRow dr = datCriteria.NewRow();
                    dr["CriteriaDescription"] = "Less than";
                    dr["CriteriaValue"] = "<";
                    datCriteria.Rows.Add(dr);

                    DataRow dr2 = datCriteria.NewRow();
                    dr2["CriteriaDescription"] = "Less than or Equal to";
                    dr2["CriteriaValue"] = "<=";
                    datCriteria.Rows.Add(dr2);

                    DataRow dr3 = datCriteria.NewRow();
                    dr3["CriteriaDescription"] = "Greater than";
                    dr3["CriteriaValue"] = ">";
                    datCriteria.Rows.Add(dr3);

                    DataRow dr4 = datCriteria.NewRow();
                    dr4["CriteriaDescription"] = "Greater than or Equal to";
                    dr4["CriteriaValue"] = ">=";
                    datCriteria.Rows.Add(dr4);

                    DataRow dr5 = datCriteria.NewRow();
                    dr5["CriteriaDescription"] = "Equal to";
                    dr5["CriteriaValue"] = "=";
                    datCriteria.Rows.Add(dr5);

                    DataRow dr6 = datCriteria.NewRow();
                    dr6["CriteriaDescription"] = "Not Equal to";
                    dr6["CriteriaValue"] = "<>";
                    datCriteria.Rows.Add(dr6);

                    DataRow dr7 = datCriteria.NewRow();
                    dr7["CriteriaDescription"] = "Between";
                    dr7["CriteriaValue"] = "Between";
                    datCriteria.Rows.Add(dr7);
                }
                //if (cboFilterOn.SelectedValue.ToString() == "System.DateTime")
                //{
                //    DataRow dr7 = datCriteria.NewRow();
                //    dr7["CriteriaDescription"] = "Between";
                //    dr7["CriteriaValue"] = "Between";
                //    datCriteria.Rows.Add(dr7);
                //}
                if (cboFilterOn.SelectedValue.ToString() == "System.String")
                {
                    DataRow dr = datCriteria.NewRow();
                    dr["CriteriaDescription"] = "Equal to";
                    dr["CriteriaValue"] = "=";
                    datCriteria.Rows.Add(dr);

                    DataRow dr2 = datCriteria.NewRow();
                    dr2["CriteriaDescription"] = "Not Equal to";
                    dr2["CriteriaValue"] = "<>";
                    datCriteria.Rows.Add(dr2);

                    DataRow dr3 = datCriteria.NewRow();
                    dr3["CriteriaDescription"] = "Like";
                    dr3["CriteriaValue"] = "Like";
                    datCriteria.Rows.Add(dr3);
                }
                if (cboFilterOn.SelectedValue.ToString() == "System.Boolean")
                {
                    DataRow dr = datCriteria.NewRow();
                    dr["CriteriaDescription"] = "Is True";
                    dr["CriteriaValue"] = "1";
                    datCriteria.Rows.Add(dr);

                    DataRow dr2 = datCriteria.NewRow();
                    dr2["CriteriaDescription"] = "Is False";
                    dr2["CriteriaValue"] = "0";
                    datCriteria.Rows.Add(dr2);
                }
                cboCriteria.DisplayMember = "CriteriaDescription";
                cboCriteria.ValueMember = "CriteriaValue";
                cboCriteria.DataSource = datCriteria;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FillCriteria() " + ex.Message);
                MobjLogs.WriteLog("Error in FillCriteria() " + ex.Message, 2);
            }
        }

        private void cboFilterOn_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ClearControls(1);

                if (cboFilterOn.SelectedIndex != -1)
                {
                    FillCriteria();

                    if (cboFilterOn.SelectedValue.ToString() == "System.DateTime")
                    {
                        dtpValue1.Visible = true;
                        dtpValue2.Visible = true;
                        txtValue1.Visible = false;
                        txtValue2.Visible = false;
                    }
                    //else if (cboFilterOn.SelectedValue.ToString() == "System.Boolean")
                    //{
                    //    dtpValue1.Visible = false;
                    //    dtpValue2.Visible = false;
                    //    txtValue1.Visible = false;
                    //    txtValue2.Visible = false;
                    //}
                    else
                    {
                        dtpValue1.Visible = false;
                        dtpValue2.Visible = false;
                        txtValue1.Visible = true;
                        txtValue2.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in cboFilterOn_SelectedIndexChanged() " + ex.Message);
                MobjLogs.WriteLog("Error in cboFilterOn_SelectedIndexChanged() " + ex.Message, 2);
            }
        }

        private void cboCriteria_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ClearControls(1);

                if (cboCriteria.SelectedIndex != -1 && cboFilterOn.SelectedIndex != -1)
                {
                    if (cboCriteria.SelectedValue.ToString() == "Between")
                    {
                        dtpValue1.Enabled = true;
                        dtpValue2.Enabled = true;
                        txtValue1.Enabled = true;
                        txtValue2.Enabled = true;

                    }
                    else if (cboFilterOn.SelectedValue.ToString() == "System.Boolean")
                    {
                        dtpValue1.Enabled = false;
                        dtpValue2.Enabled = false;
                        txtValue1.Enabled = false;
                        txtValue2.Enabled = false;
                    }
                    else
                    {
                        dtpValue1.Enabled = true;
                        dtpValue2.Enabled = false;
                        txtValue1.Enabled = true;
                        txtValue2.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in cboCriteria_SelectedIndexChanged() " + ex.Message);
                MobjLogs.WriteLog("Error in cboCriteria_SelectedIndexChanged() " + ex.Message, 2);
            }
        }

        private void BtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                if (intSearchCounter > 1)
                {
                    intSearchCounter--;
                    DisplaySearchedData();
                    ClearControls(1);
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnPrevious_Click() " + ex.Message);
                MobjLogs.WriteLog("Error in BtnPrevious_Click() " + ex.Message, 2);
            }
        }

        private void BtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (intSearchCounter < datCriteriaHistory.Rows.Count)
                {
                    intSearchCounter++;
                    DisplaySearchedData();
                    ClearControls(1);
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnNext_Click() " + ex.Message);
                MobjLogs.WriteLog("Error in BtnNext_Click() " + ex.Message, 2);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearControls(0);
        }

        public void ClearControls(int Mode)
        {
            try
            {
                if (Mode == 0)
                {
                    datCriteriaHistory.Clear();
                    dgvData.DataSource = null;
                    datSerachedData = null;
                    dgvRefinedData.DataSource = null;
                    txtFullCriteria.Text = "";
                    intSearchCounter = 0;
                }
                if (Mode == 1 || Mode == 0)
                {
                    BtnShow.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in ClearControls() " + ex.Message);
                MobjLogs.WriteLog("Error in ClearControls() " + ex.Message, 2);
            }
        }

        private void txtValue1_TextChanged(object sender, EventArgs e)
        {
            ClearControls(1);
        }

        private void txtValue2_TextChanged(object sender, EventArgs e)
        {
            ClearControls(1);
        }

        private void dtpValue2_ValueChanged(object sender, EventArgs e)
        {
            ClearControls(1);
        }

        private void dtpValue1_ValueChanged(object sender, EventArgs e)
        {
            ClearControls(1);
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboOperationType_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboOperationType.DroppedDown = false;
        }

        private void cboFilterOn_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboFilterOn.DroppedDown = false;
        }

        private void cboCriteria_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCriteria.DroppedDown = false;
        }

        private void SetSerialNo()
        {
            try
            {
                int intRowNumber = 1;

                foreach (DataGridViewRow row in dgvData.Rows)
                {
                    if (row.Index < dgvData.Rows.Count)
                    {
                        row.HeaderCell.Value = intRowNumber.ToString();
                        intRowNumber++;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SetSerialNo() " + ex.Message);
                MobjLogs.WriteLog("Error in SetSerialNo() " + ex.Message, 2);
            }
        }

        private void dgvData_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            SetSerialNo();
        }

        private void dgvData_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            SetSerialNo();
        }

        private void btnRefine_Click(object sender, EventArgs e)
        {
            try
            {
                //   DataTable datHistoryColumns = datCriteriaHistory.DefaultView.ToTable(true, "DisplayColumn");
                //   string[] strDisplayColumn = new string[datHistoryColumns.Rows.Count - 1];
                //   for (int intICounter = 1; intICounter <= datHistoryColumns.Rows.Count - 1; intICounter++)
                //   {
                //       strDisplayColumn[intICounter - 1] = datHistoryColumns.Rows[intICounter]["DisplayColumn"].ToString();

                //   }
                //   DataTable datRefine = datSerachedData;
                //   datRefine = datRefine.DefaultView.ToTable(true, strDisplayColumn);
                //   dgvData.DataSource = datRefine;
                //// MessageBox.Show(Convert.ToString(dgvData.DisplayedColumnCount(false)));

                //   //dgvData.AutoResizeRowHeadersWidth=System.Windows.Forms
                //   //dgvData.Columns[dgvData.Columns.Count - 1].AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
                //   // dgvData.ScrollBars = ScrollBars.Horizontal;
                //   //dgvData.AutoSizeColumnsMode=DataGridViewAutoSizeColumnMode.AllCells;
                //   //dgvData.Columns[0].AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
                //   for (int intICounter = 0; intICounter <= dgvData.Columns.Count - 1; intICounter++)
                //   {
                //       //dgvData.Columns[intICounter].AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
                //       dgvData.Columns[intICounter].MinimumWidth = 100;

                //   }
                RefinedData();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnRefine_Click() " + ex.Message);
                MobjLogs.WriteLog("Error in btnRefine_Click() " + ex.Message, 2);
            }
        }

        public void RefinedData()
        {
            try
            {
                if (intSearchCounter >= 1)
                {
                    DataTable datHistoryColumnsTemp = datCriteriaHistory.DefaultView.ToTable(false, "DisplayColumn");
                    DataTable datHistoryColumns = datHistoryColumnsTemp.Clone();
                    for (int intKCounter = 0; intKCounter < intSearchCounter; intKCounter++)
                    {
                        datHistoryColumns.ImportRow(datHistoryColumnsTemp.Rows[intKCounter]);

                    }
                    DataRow dr = datHistoryColumns.NewRow();
                    dr["DisplayColumn"] = "No";
                    datHistoryColumns.Rows.InsertAt(dr, 1);
                    DataRow dr2 = datHistoryColumns.NewRow();
                    dr2["DisplayColumn"] = "ID";
                    datHistoryColumns.Rows.InsertAt(dr2, 2);
                    datHistoryColumns = datHistoryColumns.DefaultView.ToTable(true, "DisplayColumn");
                    string[] strDisplayColumn = new string[datHistoryColumns.Rows.Count - 1];

                    for (int intICounter = 1; intICounter <= datHistoryColumns.Rows.Count - 1; intICounter++)
                    {
                        strDisplayColumn[intICounter - 1] = datHistoryColumns.Rows[intICounter]["DisplayColumn"].ToString();
                    }
                    DataTable datRefine = datSerachedData;
                    datRefine = datRefine.DefaultView.ToTable(true, strDisplayColumn);
                    dgvRefinedData.DataSource = datRefine;
                    dgvData.Columns["ID"].Visible = false;
                    dgvRefinedData.Columns["ID"].Visible = false;
                    dgvRefinedData.Columns["No"].DisplayIndex = 0;

                    for (int intICounter = 0; intICounter <= dgvRefinedData.Columns.Count - 1; intICounter++)
                    {
                        //dgvData.Columns[intICounter].AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
                        dgvRefinedData.Columns[intICounter].MinimumWidth = 125;
                    }
                }
                else
                {
                    dgvRefinedData.DataSource = null;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in RefinedData() " + ex.Message);
                MobjLogs.WriteLog("Error in RefinedData() " + ex.Message, 2);
            }
        }

        private void dgvRefinedData_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            SetdgvRefinedDataSerialNo();
        }

        private void dgvRefinedData_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            SetdgvRefinedDataSerialNo();
        }

        private void SetdgvRefinedDataSerialNo()
        {
            try
            {
                int intRowNumber = 1;
                foreach (DataGridViewRow row in dgvRefinedData.Rows)
                {
                    if (row.Index < dgvRefinedData.Rows.Count)
                    {
                        row.HeaderCell.Value = intRowNumber.ToString();
                        intRowNumber++;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SetdgvRefinedDataSerialNo() " + ex.Message);
                MobjLogs.WriteLog("Error in SetdgvRefinedDataSerialNo() " + ex.Message, 2);
            }
        }

        private void txtValue1_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (cboFilterOn.SelectedValue.ToString() != "System.String")
                {
                    if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                    {
                        e.Handled = true;
                    }
                    if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                    {
                        e.Handled = true;
                    }
                }
                if (e.KeyChar == 39)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in txtValue1_KeyPress() " + ex.Message);
                MobjLogs.WriteLog("Error in txtValue1_KeyPress() " + ex.Message, 2);
            }
        }

        private void txtValue2_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (cboFilterOn.SelectedValue.ToString() != "System.String")
                {
                    if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                    {
                        e.Handled = true;
                    }
                    if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                    {
                        e.Handled = true;
                    }
                }
                if (e.KeyChar == 39)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in txtValue2_KeyPress() " + ex.Message);
                MobjLogs.WriteLog("Error in txtValue2_KeyPress() " + ex.Message, 2);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ClearControls(0);
            this.Close();
        }
    }
}