﻿namespace MyBooksERP
{
    partial class FrmScanning
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmScanning));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Documents");
            this.MenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemScan = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripMenuItemPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripMenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolStripDropDownButtonDoc = new System.Windows.Forms.ToolStripDropDownButton();
            this.ToolStripMenuItemAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMnuItemEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.PrintTStripButton = new System.Windows.Forms.ToolStripButton();
            this.btnScanner = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonSendMail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonZoomIn = new System.Windows.Forms.ToolStripButton();
            this.BtnAttach = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonZoomOut = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButtonFitToScreen = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.TrvScan = new System.Windows.Forms.TreeView();
            this.BtnOpenApplication = new System.Windows.Forms.Button();
            this.LblMess2 = new System.Windows.Forms.Label();
            this.LblMess1 = new System.Windows.Forms.Label();
            this.ImageControl1 = new Ic.ImageControl();
            this.webView = new System.Windows.Forms.WebBrowser();
            this.MyPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.ContextMenuStripScan = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ContextMnuAddNewItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMnuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip1.SuspendLayout();
            this.ToolStrip1.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.ContextMenuStripScan.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuStrip1
            // 
            this.MenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem1});
            this.MenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip1.Name = "MenuStrip1";
            this.MenuStrip1.Size = new System.Drawing.Size(937, 24);
            this.MenuStrip1.TabIndex = 2;
            this.MenuStrip1.Text = "MenuStrip1";
            // 
            // ToolStripMenuItem1
            // 
            this.ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem4,
            this.ToolStripMenuItemScan,
            this.ToolStripMenuItemSaveAs,
            this.ToolStripSeparator10,
            this.ToolStripMenuItemPrint,
            this.ToolStripSeparator11,
            this.ToolStripMenuItemExit});
            this.ToolStripMenuItem1.Name = "ToolStripMenuItem1";
            this.ToolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.ToolStripMenuItem1.Text = "&File";
            this.ToolStripMenuItem1.Visible = false;
            // 
            // ToolStripMenuItem4
            // 
            this.ToolStripMenuItem4.Name = "ToolStripMenuItem4";
            this.ToolStripMenuItem4.Size = new System.Drawing.Size(154, 22);
            this.ToolStripMenuItem4.Text = "&Select Source";
            // 
            // ToolStripMenuItemScan
            // 
            this.ToolStripMenuItemScan.Name = "ToolStripMenuItemScan";
            this.ToolStripMenuItemScan.Size = new System.Drawing.Size(154, 22);
            this.ToolStripMenuItemScan.Text = "&Scan";
            // 
            // ToolStripMenuItemSaveAs
            // 
            this.ToolStripMenuItemSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripMenuItemSaveAs.Image")));
            this.ToolStripMenuItemSaveAs.ImageTransparentColor = System.Drawing.Color.Black;
            this.ToolStripMenuItemSaveAs.Name = "ToolStripMenuItemSaveAs";
            this.ToolStripMenuItemSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.ToolStripMenuItemSaveAs.Size = new System.Drawing.Size(154, 22);
            this.ToolStripMenuItemSaveAs.Text = "&Save As";
            // 
            // ToolStripSeparator10
            // 
            this.ToolStripSeparator10.Name = "ToolStripSeparator10";
            this.ToolStripSeparator10.Size = new System.Drawing.Size(151, 6);
            // 
            // ToolStripMenuItemPrint
            // 
            this.ToolStripMenuItemPrint.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripMenuItemPrint.Image")));
            this.ToolStripMenuItemPrint.ImageTransparentColor = System.Drawing.Color.Black;
            this.ToolStripMenuItemPrint.Name = "ToolStripMenuItemPrint";
            this.ToolStripMenuItemPrint.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.ToolStripMenuItemPrint.Size = new System.Drawing.Size(154, 22);
            this.ToolStripMenuItemPrint.Text = "&Print";
            // 
            // ToolStripSeparator11
            // 
            this.ToolStripSeparator11.Name = "ToolStripSeparator11";
            this.ToolStripSeparator11.Size = new System.Drawing.Size(151, 6);
            // 
            // ToolStripMenuItemExit
            // 
            this.ToolStripMenuItemExit.Name = "ToolStripMenuItemExit";
            this.ToolStripMenuItemExit.Size = new System.Drawing.Size(154, 22);
            this.ToolStripMenuItemExit.Text = "E&xit";
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripDropDownButtonDoc,
            this.PrintTStripButton,
            this.btnScanner,
            this.ToolStripButtonSendMail,
            this.ToolStripButtonZoomIn,
            this.BtnAttach,
            this.ToolStripButtonZoomOut,
            this.ToolStripButtonFitToScreen});
            this.ToolStrip1.Location = new System.Drawing.Point(0, 24);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(937, 25);
            this.ToolStrip1.TabIndex = 3;
            this.ToolStrip1.Text = "ToolStrip1";
            // 
            // ToolStripDropDownButtonDoc
            // 
            this.ToolStripDropDownButtonDoc.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemAdd,
            this.ToolStripMnuItemEdit,
            this.ToolStripMenuItemDelete});
            this.ToolStripDropDownButtonDoc.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripDropDownButtonDoc.Image")));
            this.ToolStripDropDownButtonDoc.ImageTransparentColor = System.Drawing.Color.Black;
            this.ToolStripDropDownButtonDoc.Name = "ToolStripDropDownButtonDoc";
            this.ToolStripDropDownButtonDoc.Size = new System.Drawing.Size(97, 22);
            this.ToolStripDropDownButtonDoc.Text = "Documents";
            // 
            // ToolStripMenuItemAdd
            // 
            this.ToolStripMenuItemAdd.Name = "ToolStripMenuItemAdd";
            this.ToolStripMenuItemAdd.Size = new System.Drawing.Size(123, 22);
            this.ToolStripMenuItemAdd.Text = "Add New";
            this.ToolStripMenuItemAdd.Click += new System.EventHandler(this.ToolStripMenuItemAdd_Click);
            // 
            // ToolStripMnuItemEdit
            // 
            this.ToolStripMnuItemEdit.Checked = true;
            this.ToolStripMnuItemEdit.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ToolStripMnuItemEdit.Name = "ToolStripMnuItemEdit";
            this.ToolStripMnuItemEdit.Size = new System.Drawing.Size(123, 22);
            this.ToolStripMnuItemEdit.Text = "Edit";
            this.ToolStripMnuItemEdit.Click += new System.EventHandler(this.ToolStripMnuItemEdit_Click);
            // 
            // ToolStripMenuItemDelete
            // 
            this.ToolStripMenuItemDelete.Name = "ToolStripMenuItemDelete";
            this.ToolStripMenuItemDelete.Size = new System.Drawing.Size(123, 22);
            this.ToolStripMenuItemDelete.Text = "Delete";
            this.ToolStripMenuItemDelete.Click += new System.EventHandler(this.ToolStripMenuItemDelete_Click);
            // 
            // PrintTStripButton
            // 
            this.PrintTStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PrintTStripButton.Name = "PrintTStripButton";
            this.PrintTStripButton.Size = new System.Drawing.Size(58, 22);
            this.PrintTStripButton.Text = "&Print/Fax";
            this.PrintTStripButton.Click += new System.EventHandler(this.PrintTStripButton_Click);
            // 
            // btnScanner
            // 
            this.btnScanner.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnScanner.Name = "btnScanner";
            this.btnScanner.Size = new System.Drawing.Size(53, 22);
            this.btnScanner.Text = "&Scanner";
            this.btnScanner.Visible = false;
            // 
            // ToolStripButtonSendMail
            // 
            this.ToolStripButtonSendMail.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripButtonSendMail.Image")));
            this.ToolStripButtonSendMail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonSendMail.Name = "ToolStripButtonSendMail";
            this.ToolStripButtonSendMail.Size = new System.Drawing.Size(79, 22);
            this.ToolStripButtonSendMail.Text = "Send Mail";
            this.ToolStripButtonSendMail.Click += new System.EventHandler(this.ToolStripButtonSendMail_Click);
            // 
            // ToolStripButtonZoomIn
            // 
            this.ToolStripButtonZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripButtonZoomIn.Image")));
            this.ToolStripButtonZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonZoomIn.Name = "ToolStripButtonZoomIn";
            this.ToolStripButtonZoomIn.Size = new System.Drawing.Size(72, 22);
            this.ToolStripButtonZoomIn.Text = "Zoom In";
            this.ToolStripButtonZoomIn.Click += new System.EventHandler(this.ToolStripButtonZoomIn_Click);
            // 
            // BtnAttach
            // 
            this.BtnAttach.Image = ((System.Drawing.Image)(resources.GetObject("BtnAttach.Image")));
            this.BtnAttach.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnAttach.Name = "BtnAttach";
            this.BtnAttach.Size = new System.Drawing.Size(62, 22);
            this.BtnAttach.Text = "&Attach";
            this.BtnAttach.Click += new System.EventHandler(this.BtnAttach_Click);
            // 
            // ToolStripButtonZoomOut
            // 
            this.ToolStripButtonZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripButtonZoomOut.Image")));
            this.ToolStripButtonZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonZoomOut.Name = "ToolStripButtonZoomOut";
            this.ToolStripButtonZoomOut.Size = new System.Drawing.Size(82, 22);
            this.ToolStripButtonZoomOut.Text = "Zoom Out";
            this.ToolStripButtonZoomOut.Click += new System.EventHandler(this.ToolStripButtonZoomOut_Click);
            // 
            // ToolStripButtonFitToScreen
            // 
            this.ToolStripButtonFitToScreen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ToolStripButtonFitToScreen.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripButtonFitToScreen.Image")));
            this.ToolStripButtonFitToScreen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButtonFitToScreen.Name = "ToolStripButtonFitToScreen";
            this.ToolStripButtonFitToScreen.Size = new System.Drawing.Size(79, 22);
            this.ToolStripButtonFitToScreen.Text = "Fit To Screen";
            this.ToolStripButtonFitToScreen.Click += new System.EventHandler(this.ToolStripButtonFitToScreen_Click);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(937, 412);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 49);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(937, 459);
            this.toolStripContainer1.TabIndex = 4;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(937, 22);
            this.statusStrip1.TabIndex = 0;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel1.Text = "Status";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.TrvScan);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.BtnOpenApplication);
            this.splitContainer1.Panel2.Controls.Add(this.LblMess2);
            this.splitContainer1.Panel2.Controls.Add(this.LblMess1);
            this.splitContainer1.Panel2.Controls.Add(this.ImageControl1);
            this.splitContainer1.Panel2.Controls.Add(this.webView);
            this.splitContainer1.Size = new System.Drawing.Size(937, 412);
            this.splitContainer1.SplitterDistance = 348;
            this.splitContainer1.TabIndex = 0;
            // 
            // TrvScan
            // 
            this.TrvScan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TrvScan.Location = new System.Drawing.Point(0, 0);
            this.TrvScan.Name = "TrvScan";
            treeNode1.Name = "Node0";
            treeNode1.Text = "Documents";
            this.TrvScan.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.TrvScan.SelectedImageKey = "OpenFolder";
            this.TrvScan.Size = new System.Drawing.Size(348, 412);
            this.TrvScan.TabIndex = 1;
            this.TrvScan.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.TrvScan_AfterLabelEdit);
            this.TrvScan.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TrvScan_MouseUp);
            this.TrvScan.Leave += new System.EventHandler(this.TrvScan_Leave);
            this.TrvScan.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.TrvScan_NodeMouseClick);
            // 
            // BtnOpenApplication
            // 
            this.BtnOpenApplication.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnOpenApplication.BackColor = System.Drawing.Color.AliceBlue;
            this.BtnOpenApplication.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnOpenApplication.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnOpenApplication.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnOpenApplication.Location = new System.Drawing.Point(364, 275);
            this.BtnOpenApplication.Name = "BtnOpenApplication";
            this.BtnOpenApplication.Size = new System.Drawing.Size(145, 27);
            this.BtnOpenApplication.TabIndex = 13;
            this.BtnOpenApplication.Text = "  Open Application";
            this.BtnOpenApplication.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnOpenApplication.UseVisualStyleBackColor = false;
            this.BtnOpenApplication.Visible = false;
            this.BtnOpenApplication.Click += new System.EventHandler(this.BtnOpenApplication_Click);
            // 
            // LblMess2
            // 
            this.LblMess2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LblMess2.AutoSize = true;
            this.LblMess2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.LblMess2.Location = new System.Drawing.Point(101, 231);
            this.LblMess2.Name = "LblMess2";
            this.LblMess2.Size = new System.Drawing.Size(407, 32);
            this.LblMess2.TabIndex = 12;
            this.LblMess2.Text = "The document you are trying to view relate to an external application!\r\nUse the b" +
                "utton below to open this document.";
            this.LblMess2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LblMess2.Visible = false;
            // 
            // LblMess1
            // 
            this.LblMess1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LblMess1.AutoSize = true;
            this.LblMess1.Font = new System.Drawing.Font("Tahoma", 11.25F);
            this.LblMess1.ForeColor = System.Drawing.Color.Maroon;
            this.LblMess1.Location = new System.Drawing.Point(228, 198);
            this.LblMess1.Name = "LblMess1";
            this.LblMess1.Size = new System.Drawing.Size(280, 18);
            this.LblMess1.TabIndex = 11;
            this.LblMess1.Text = "Preview for this document is not available!";
            this.LblMess1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LblMess1.Visible = false;
            // 
            // ImageControl1
            // 
            this.ImageControl1.BackColor = System.Drawing.SystemColors.Window;
            this.ImageControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ImageControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImageControl1.Image = null;
            this.ImageControl1.initialimage = null;
            this.ImageControl1.Location = new System.Drawing.Point(0, 0);
            this.ImageControl1.Name = "ImageControl1";
            this.ImageControl1.Origin = new System.Drawing.Point(0, 0);
            this.ImageControl1.PanButton = System.Windows.Forms.MouseButtons.Left;
            this.ImageControl1.PanMode = true;
            this.ImageControl1.ScrollbarsVisible = true;
            this.ImageControl1.Size = new System.Drawing.Size(585, 412);
            this.ImageControl1.StretchImageToFit = false;
            this.ImageControl1.TabIndex = 9;
            this.ImageControl1.ZoomFactor = 1;
            this.ImageControl1.ZoomOnMouseWheel = true;
            // 
            // webView
            // 
            this.webView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webView.Location = new System.Drawing.Point(0, 0);
            this.webView.MinimumSize = new System.Drawing.Size(20, 20);
            this.webView.Name = "webView";
            this.webView.Size = new System.Drawing.Size(585, 412);
            this.webView.TabIndex = 15;
            this.webView.Visible = false;
            // 
            // MyPrintDocument
            // 
            this.MyPrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.MyPrintDocument_PrintPage);
            // 
            // ContextMenuStripScan
            // 
            this.ContextMenuStripScan.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ContextMnuAddNewItem,
            this.ToolStripMenuItemEdit,
            this.ContextMnuDelete});
            this.ContextMenuStripScan.Name = "ContextMenuStrip1";
            this.ContextMenuStripScan.Size = new System.Drawing.Size(151, 70);
            // 
            // ContextMnuAddNewItem
            // 
            this.ContextMnuAddNewItem.Name = "ContextMnuAddNewItem";
            this.ContextMnuAddNewItem.Size = new System.Drawing.Size(150, 22);
            this.ContextMnuAddNewItem.Text = "Add New Item";
            this.ContextMnuAddNewItem.Click += new System.EventHandler(this.ContextMnuAddNewItem_Click);
            // 
            // ToolStripMenuItemEdit
            // 
            this.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit";
            this.ToolStripMenuItemEdit.Size = new System.Drawing.Size(150, 22);
            this.ToolStripMenuItemEdit.Text = "Edit ";
            this.ToolStripMenuItemEdit.Click += new System.EventHandler(this.ToolStripMenuItemEdit_Click);
            // 
            // ContextMnuDelete
            // 
            this.ContextMnuDelete.Name = "ContextMnuDelete";
            this.ContextMnuDelete.Size = new System.Drawing.Size(150, 22);
            this.ContextMnuDelete.Text = "Delete Item";
            this.ContextMnuDelete.Click += new System.EventHandler(this.ContextMnuDelete_Click);
            // 
            // FrmScanning
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 508);
            this.Controls.Add(this.toolStripContainer1);
            this.Controls.Add(this.ToolStrip1);
            this.Controls.Add(this.MenuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmScanning";
            this.Text = "Scanning";
            this.Load += new System.EventHandler(this.FrmScanning_Load);
            this.MenuStrip1.ResumeLayout(false);
            this.MenuStrip1.PerformLayout();
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.ContextMenuStripScan.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.MenuStrip MenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem4;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemScan;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemSaveAs;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator10;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemPrint;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator11;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemExit;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.ToolStripDropDownButton ToolStripDropDownButtonDoc;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemAdd;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMnuItemEdit;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemDelete;
        internal System.Windows.Forms.ToolStripButton PrintTStripButton;
        internal System.Windows.Forms.ToolStripButton btnScanner;
        internal System.Windows.Forms.ToolStripButton ToolStripButtonSendMail;
        internal System.Windows.Forms.ToolStripButton ToolStripButtonZoomIn;
        internal System.Windows.Forms.ToolStripButton ToolStripButtonZoomOut;
        internal System.Windows.Forms.ToolStripButton BtnAttach;
        internal System.Windows.Forms.ToolStripButton ToolStripButtonFitToScreen;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        internal System.Windows.Forms.TreeView TrvScan;
        internal Ic.ImageControl ImageControl1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        internal System.Windows.Forms.Button BtnOpenApplication;
        internal System.Windows.Forms.Label LblMess2;
        internal System.Windows.Forms.Label LblMess1;
        internal System.Drawing.Printing.PrintDocument MyPrintDocument;
        internal System.Windows.Forms.ContextMenuStrip ContextMenuStripScan;
        internal System.Windows.Forms.ToolStripMenuItem ContextMnuAddNewItem;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemEdit;
        internal System.Windows.Forms.ToolStripMenuItem ContextMnuDelete;
        private System.Windows.Forms.WebBrowser webView;

    }
}