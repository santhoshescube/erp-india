﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class frmItemSelection : Form
    {
        DataTable datDataSource = new DataTable();

        public frmItemSelection()
        {
            InitializeComponent();
        }

        public frmItemSelection(DataTable datSource)
        {
            InitializeComponent();
            datDataSource = datSource;
        }

        private void frmItemSelection_Load(object sender, EventArgs e)
        {
            dgvItems.DataSource = datDataSource;
        }
    }
}
