﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;


namespace MyBooksERP//.Utilities.Forms
{
    public partial class FrmBarCode : DevComponents.DotNetBar.Office2007Form
    {
        public string PstrBarcodeValue;
        public long PlngItemID;
        public Image PimgBarcodeImage;
        public bool PblnIsTemplate;
        int MintRecordCount, MintRecordPosition, MintMode = 1;

        bool MblnAddStatus = true, MblnShowErrorMess = true;   // Checking whether error messages are showing or not

        string MstrCommonMessage;

        DataTable MdatMessages;

        MessageBoxIcon MmsgMessageIcon;

        ClsNotificationNew MobjNotification;
        clsBLLBarCode MobjBLLBarCode;
        ClsLogWriter MobjLogWriter;
        ClsImages MobjImages;

        BarcodeLib.Barcode MBarCode;
        private bool MblnPrintEmailPermission = false;    // Set Print and Email Permission
        private bool MblnAddPermission = false;           //Set Add Permission
        private bool MblnUpdatePermission = false;        //Set Update Permission
        private bool MblnDeletePermission = false;        //Set Delete Permission
        private bool MblnAddUpdatePermission = false;     //Set Add Update Permission
        private bool MblnCancelPermission = true;        // Set Cancel Permission


        private bool MblnIsCancel=true;
        public FrmBarCode()
        {
            InitializeComponent();

            MBarCode = new BarcodeLib.Barcode();

            MobjBLLBarCode = new clsBLLBarCode();
            MobjLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjNotification = new ClsNotificationNew();
            MobjImages = new ClsImages();
        }

        private void FormLoad(object sender, EventArgs e)
        {           
            MobjBLLBarCode.objDTOBarCode.blnIsTemplate = PblnIsTemplate;
            SetPermissions();
            LoadMessage();
            LoadCombo();
            ResetForm();
            if(!PblnIsTemplate)
            {
                grpItemInfo.Enabled = false;
                MblnAddPermission = MblnAddUpdatePermission = MblnCancelPermission = MblnUpdatePermission = MblnDeletePermission = false;
                bnAddNewItem.Enabled = MblnAddPermission;
                bnSaveItem.Enabled = MblnAddUpdatePermission;
                bnDeleteItem.Enabled = MblnDeletePermission;
                btnSave.Enabled = MblnAddUpdatePermission;
                btnOk.Enabled = MblnAddUpdatePermission;
            }
        }
        private void SetPermissions()
        {
            // Function for setting permissions
            //clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            //if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            //{
            //    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)ModuleID.Inventory, (Int32)MenuID.Products, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            //    if (MblnAddPermission == true || MblnUpdatePermission == true)
            //        MblnAddUpdatePermission = true;

            //    //if (MblnAddPermission == true && MblnUpdatePermission == true && MblnDeletePermission == true)
            //    //    MblnPrintAndEmailPermission = true;
            //}
            //else
                MblnAddPermission = MblnAddUpdatePermission = MblnCancelPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            bnAddNewItem.Enabled = MblnAddPermission;
            bnSaveItem.Enabled = MblnAddUpdatePermission;
            bnDeleteItem.Enabled = MblnDeletePermission;
            btnSave.Enabled = MblnAddUpdatePermission;
            btnOk.Enabled = MblnAddUpdatePermission;
            bnPrint.Enabled = MblnPrintEmailPermission;
            bnEmail.Enabled = MblnPrintEmailPermission;
        }


        private void LoadBarCode()
        {
            Bitmap temp = new Bitmap(1, 1);
            temp.SetPixel(0, 0, BackColor);
            pbBarcode.Image = (Image)temp;

            cboRotateFlip.DataSource = System.Enum.GetNames(typeof(RotateFlipType));

            int i = 0;
            foreach (object o in cboRotateFlip.Items)
            {
                if (o.ToString().Trim().ToLower() == "rotatenoneflipnone")
                    break;
                i++;
            }
            cboRotateFlip.SelectedIndex = i;

            btnBackColor.BackColor = MBarCode.BackColor;
            btnForeColor.BackColor = MBarCode.ForeColor;
        }

        /// <summary>
        /// Loading the variables for messages
        /// </summary>
        private void LoadMessage()
        {
            MdatMessages = new DataTable();
            MdatMessages = MobjNotification.FillMessageArray((int)FormID.BarCodeCreation, 4);
        }

        private bool LoadCombo()
        {
            DataTable datCombos = new DataTable();
            datCombos = MobjBLLBarCode.FillCombos(new string[] { "ItemID, Code", "InvItemMaster", "" });
            cboItemName.DisplayMember = "Code";
            cboItemName.ValueMember = "ItemID";
            cboItemName.DataSource = datCombos;

            datCombos = null;
            datCombos = MobjBLLBarCode.FillCombos(new string[] { "BarcodeEncodingTypeID, BarcodeEncodingType", "InvBarcodeEncodingTypeReference", "" });
            cboEncodeType.DisplayMember = "BarcodeEncodingType";
            cboEncodeType.ValueMember = "BarcodeEncodingTypeID";
            cboEncodeType.DataSource = datCombos;

            DataTable datAllignment = new DataTable();
            datAllignment.Columns.Add("AllignmentID");
            datAllignment.Columns.Add("Allignment");
            DataRow dr1 = datAllignment.NewRow();
            dr1["AllignmentID"] = (Int32)Allignment.CENTER;
            dr1["Allignment"] = "Center";
            datAllignment.Rows.Add(dr1);
            DataRow dr2 = datAllignment.NewRow();
            dr2["AllignmentID"] = (Int32)Allignment.LEFT;
            dr2["Allignment"] = "Left";
            datAllignment.Rows.Add(dr2);
            DataRow dr3 = datAllignment.NewRow();
            dr3["AllignmentID"] = (Int32)Allignment.RIGHT;
            dr3["Allignment"] = "Right";
            datAllignment.Rows.Add(dr3);
            cboBarcodeAlign.DisplayMember = "Allignment";
            cboBarcodeAlign.ValueMember = "AllignmentID";
            cboBarcodeAlign.DataSource = datAllignment;

            DataTable datLocation = new DataTable();
            datLocation.Columns.Add("LocationID");
            datLocation.Columns.Add("Location");

            DataRow dr4 = datLocation.NewRow();
            dr4["LocationID"] = (Int32)Locations.BOTTOMCENTER;
            dr4["Location"] = "BottomCenter";
            datLocation.Rows.Add(dr4);
            DataRow dr5 = datLocation.NewRow();
            dr5["LocationID"] = (Int32)Locations.BOTTOMLEFT;
            dr5["Location"] = "BottomLeft";
            datLocation.Rows.Add(dr5);
            DataRow dr6 = datLocation.NewRow();
            dr6["LocationID"] = (Int32)Locations.BOTTOMRIGHT;
            dr6["Location"] = "BottomRight";
            datLocation.Rows.Add(dr6);
            DataRow dr7 = datLocation.NewRow();
            dr7["LocationID"] = (Int32)Locations.TOPCENTER;
            dr7["Location"] = "TopCenter";
            datLocation.Rows.Add(dr7);
            DataRow dr8 = datLocation.NewRow();
            dr8["LocationID"] = (Int32)Locations.TOPLEFT;
            dr8["Location"] = "TopLeft";
            datLocation.Rows.Add(dr8);
            DataRow dr9 = datLocation.NewRow();
            dr9["LocationID"] = (Int32)Locations.TOPRIGHT;
            dr9["Location"] = "TopRight";
            datLocation.Rows.Add(dr9);

            cboLabelLocation.DisplayMember = "Location";
            cboLabelLocation.ValueMember = "LocationID";
            cboLabelLocation.DataSource = datLocation;
            datCombos = null;

            return true;
        }

        private void ResetForm()
        {
            try
            {
                txtData.Tag = "0";
                cboEncodeType.SelectedValue = (int)BarcodeEncodingType.CODE128;
                cboBarcodeAlign.SelectedIndex = 0;
                cboLabelLocation.SelectedIndex = 0;
                cboRotateFlip.SelectedIndex = 0;
                pbBarcode.Image = null;
                cboEncodeType.Text = "Code 128";

                txtData.Text = PstrBarcodeValue;
                cboItemName.SelectedValue = PlngItemID;
                if (PlngItemID > 0)
                {
                    cboItemName.Enabled = false;
                    MintMode = 7;
                }

                MstrCommonMessage = MobjNotification.GetErrorMessage(MdatMessages, 701, out MmsgMessageIcon);
                lblBarcodeStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                tmrBarCode.Enabled = true;
                errBarcode.Clear();

                MintRecordPosition = 1;

                LoadBarCode();
                DisplayInfo();
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void ResetForm(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void SaveItem(object sender, EventArgs e)
        {
            try
            {
                MblnIsCancel = false;

                if (ValidateFields())
                {
                    if (SaveBarcode())
                        ResetForm();
                    else
                        return;

                    if (sender.GetType() == typeof(Button))
                    {
                        if (((Button)sender).Name == btnOk.Name)
                            Close();
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private bool ValidateFields()
        {
            bool blnValid = true;
            Control cntrlFocus = null;

            errBarcode.Clear();

            if (Convert.ToInt32(cboItemName.SelectedValue) == 0)
            {
                MstrCommonMessage = MobjNotification.GetErrorMessage(MdatMessages, 702, out MmsgMessageIcon);
                cntrlFocus = cboItemName;
                blnValid = false;
            }
            else if (pbBarcode.Image.Size.Width == 1 && pbBarcode.Image.Size.Height == 1)
            {
                MstrCommonMessage = MobjNotification.GetErrorMessage(MdatMessages, 703, out MmsgMessageIcon);
                cntrlFocus = pbBarcode;
                blnValid = false;
            }
            //else //if (MbAddStatus)//Insert 
            //{
            //    if (MobjBLLBarCode.CheckDuplication(MbAddStatus, new string[] { Convert.ToString(cboItemName.SelectedValue) }, MlngItemID))
            //    {
            //        MstrCommonMessage = "Duplicate values not permitted";
            //        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        ErrorProviderBarcode.SetError(cboItemName, MstrCommonMessage.Replace("#", "").Trim());
            //        LblBarcodeStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
            //        TmrBarcode.Enabled = true;
            //        cboItemName.Focus();
            //        return false;
            //    }
            //}

            if (!blnValid)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                //errBarcode.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                lblBarcodeStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                cntrlFocus.Focus();
            }
            return blnValid;
        }

        private void DisplayInfo()
        {
            MobjBLLBarCode.objDTOBarCode.intRowNumber = MintRecordPosition;

            if (MobjBLLBarCode.GetBarCode(MintMode,PlngItemID, PstrBarcodeValue))
            {
                cboItemName.SelectedValue = MobjBLLBarCode.objDTOBarCode.lngItemID;
                txtData.Text = MobjBLLBarCode.objDTOBarCode.strBarcodeValue;
                txtData.Tag = MobjBLLBarCode.objDTOBarCode.intBarcodeID.ToString();
                cboEncodeType.SelectedValue=MobjBLLBarCode.objDTOBarCode.intEncodingID.ToInt32();
                chkGenerateLabel.Checked = MobjBLLBarCode.objDTOBarCode.blnIsShowlabel.ToBoolean();
                txtWidth.Text = MobjBLLBarCode.objDTOBarCode.intWidth.ToString();
                txtHeight.Text = MobjBLLBarCode.objDTOBarCode.intHeight.ToString();
                this.cboRotateFlip.SelectedIndex = MobjBLLBarCode.objDTOBarCode.intRotationID.ToInt32();
                cboBarcodeAlign.SelectedValue= MobjBLLBarCode.objDTOBarCode.intAlignment.ToInt32();
                cboLabelLocation.SelectedValue = MobjBLLBarCode.objDTOBarCode.intLabelLocation.ToInt32();
                btnBackColor.BackColor = Color.FromName(MobjBLLBarCode.objDTOBarCode.strRGBHexBackColor.ToString());
                btnForeColor.BackColor = Color.FromName(MobjBLLBarCode.objDTOBarCode.strRGBHexForeColor.ToString());


                if (cboEncodeType.SelectedValue.ToInt32() == 25)
                {
                    try
                    {

                        if (txtData.Text.Trim() != "" && (txtData.Text.Trim().Length == 12 || txtData.Text.Trim().Length == 13) && txtData.Text.Trim().ToDecimal() > 0)
                        {
                            Ean13 ean13 = new Ean13();
                            ean13.CountryCode = txtData.Text.Trim().Substring(0, 2);
                            ean13.ManufacturerCode = txtData.Text.Trim().Substring(2, 5);
                            ean13.ProductCode = txtData.Text.Trim().Substring(7, 5);
                            if (txtData.Text.Trim().Length == 13)
                                ean13.ChecksumDigit = txtData.Text.Trim().Substring(12, 1);
                            ean13.Scale = (float)Convert.ToDecimal(1);
                            System.Drawing.Bitmap bmp = ean13.CreateBitmap(txtWidth.Text.ToInt32(), txtHeight.Text.ToInt32());
                            this.pbBarcode.Image = bmp;
                            pbBarcode.SizeMode = PictureBoxSizeMode.Zoom;
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    return;
                }
                else
                {
                    pbBarcode.Image = MobjImages.GetImage(MobjBLLBarCode.objDTOBarCode.imgBarcodeImage);

                }
                //bnSaveItem.Enabled = false;
                //btnOk.Enabled = false;
                //btnSave.Enabled = false;

                //bnAddNewItem.Enabled = true;
                //bnDeleteItem.Enabled = true;
                //bnEmail.Enabled = true;
                //bnPrint.Enabled = true;

                bnSaveItem.Enabled = false;
                btnOk.Enabled = false;
                btnSave.Enabled = false;
                bnAddNewItem.Enabled = MblnAddPermission;
                bnDeleteItem.Enabled = MblnDeletePermission;
                bnEmail.Enabled = MblnPrintEmailPermission;
                bnPrint.Enabled = MblnPrintEmailPermission;



                MblnAddStatus = false;
            }

            MintRecordCount = MobjBLLBarCode.GetBarCodeCount();
            if (MintRecordCount > 0)
            {
                bnPositionItem.Text = MobjBLLBarCode.objDTOBarCode.intRowNumber.ToString();
                bnCountItem.Text = " of " + MintRecordCount.ToString();
            }
            else
            {
                bnPositionItem.Text = "1";
                bnCountItem.Text = " of 1";
            }
        }

        private void MoveFirstItem(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition > 1)
                {
                    MintMode = 1;
                    MintRecordPosition = 1;
                    DisplayInfo();

                    lblBarcodeStatus.Text = MobjNotification.GetErrorMessage(MdatMessages, 9);
                    tmrBarCode.Enabled = true;
                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void MovePreviousItem(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition > 1)
                {
                    MintMode = 1;
                    MintRecordPosition--;
                    DisplayInfo();

                    lblBarcodeStatus.Text = MobjNotification.GetErrorMessage(MdatMessages, 10);
                    tmrBarCode.Enabled = true;
                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void MoveNextItem(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition < MintRecordCount)
                {
                    MintMode = 1;
                    MintRecordPosition++;
                    DisplayInfo();

                    lblBarcodeStatus.Text = MobjNotification.GetErrorMessage(MdatMessages, 11);
                    tmrBarCode.Enabled = true;
                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void MoveLastItem(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition <= MintRecordCount + 1)
                {
                    MintMode = 1;
                    MintRecordPosition = MintRecordCount;
                    DisplayInfo();

                    lblBarcodeStatus.Text = MobjNotification.GetErrorMessage(MdatMessages, 12);
                    tmrBarCode.Enabled = true;
                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void DeleteItem(object sender, EventArgs e)
        {
            try
            {
                MobjBLLBarCode.objDTOBarCode.lngItemID = Convert.ToInt64(cboItemName.SelectedValue);

                MstrCommonMessage = MobjNotification.GetErrorMessage(MdatMessages, 13, out MmsgMessageIcon);//Delete
                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;

                if (MobjBLLBarCode.DeleteBarCode())
                {
                    ResetForm();
                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void Cancel(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ChangeStatus(object sender, EventArgs e)
        {
            //btnOk.Enabled = true;
            //btnSave.Enabled = true;
            //bnSaveItem.Enabled = true;
            bnSaveItem.Enabled = MblnAddUpdatePermission;
            btnOk.Enabled = MblnAddUpdatePermission;
            btnSave.Enabled = MblnAddUpdatePermission;
            bnSaveItem.Enabled = MblnAddUpdatePermission;
            errBarcode.Clear();
        }

        private void TimerTick(object sender, EventArgs e)
        {
            lblBarcodeStatus.Text = "";
            tmrBarCode.Enabled = false;
        }

        private void GenerateBarcode(object sender, EventArgs e)
        {
            errBarcode.Clear();
            if (cboEncodeType.SelectedValue.ToInt32()==25)
            {
                try
                {

                    if (txtData.Text.Trim() != "" && (txtData.Text.Trim().Length == 12 || txtData.Text.Trim().Length == 13) && txtData.Text.Trim().ToDecimal() > 0)
                    {
                        Ean13 ean13 = new Ean13();
                        ean13.CountryCode = txtData.Text.Trim().Substring(0, 2);
                        ean13.ManufacturerCode = txtData.Text.Trim().Substring(2, 5);
                        ean13.ProductCode = txtData.Text.Trim().Substring(7, 5);
                        if (txtData.Text.Trim().Length == 13)
                            ean13.ChecksumDigit = txtData.Text.Trim().Substring(12, 1);
                        ean13.Scale = (float)Convert.ToDecimal(1);
                        System.Drawing.Bitmap bmp = ean13.CreateBitmap(txtWidth.Text.ToInt32(), txtHeight.Text.ToInt32());
                        this.pbBarcode.Image = bmp;
                        //bmp.Save("c:test.bmp");
                        pbBarcode.SizeMode = PictureBoxSizeMode.Zoom;
                        ChangeStatus(sender, e);
                    }
                    else
                    {
                        MessageBox.Show("Invalid data for this Encoding type,Data must be 12 or 13 digit number");
                    }
                }
                catch (Exception ex)
                {
                }
                return;
            }
            pbBarcode.SizeMode = PictureBoxSizeMode.Normal;
            int W = 0;
            int H = 0;
            if (Int32.TryParse(this.txtWidth.Text.Trim(), out  W))
            {
                W = Convert.ToInt32(this.txtWidth.Text.Trim());

            }

            if (Int32.TryParse(this.txtHeight.Text.Trim(), out  H))
            {
                H = Convert.ToInt32(this.txtHeight.Text.Trim());

            }
           
             BarcodeLib.AlignmentPositions Align = BarcodeLib.AlignmentPositions.CENTER;

            //barcode alignment
            switch (cboBarcodeAlign.SelectedValue.ToInt32())
            {
                case (Int32)Allignment.LEFT: Align = BarcodeLib.AlignmentPositions.LEFT; break;
                case (Int32)Allignment.RIGHT: Align = BarcodeLib.AlignmentPositions.RIGHT; break;
                default: Align = BarcodeLib.AlignmentPositions.CENTER; break;
            }

            BarcodeLib.TYPE type = BarcodeLib.TYPE.UNSPECIFIED;

            switch (cboEncodeType.SelectedValue.ToInt32())
            {
                case (Int32)BarcodeEncodingType.UPCA: type = BarcodeLib.TYPE.UPCA; break;
                case (Int32)BarcodeEncodingType.UPCE: type = BarcodeLib.TYPE.UPCE; break;
                case (Int32)BarcodeEncodingType.UPC_SUPPLEMENTAL_2DIGIT: type = BarcodeLib.TYPE.UPC_SUPPLEMENTAL_2DIGIT; break;
                case (Int32)BarcodeEncodingType.UPC_SUPPLEMENTAL_5DIGIT: type = BarcodeLib.TYPE.UPC_SUPPLEMENTAL_5DIGIT; break;
                case (Int32)BarcodeEncodingType.EAN13: type = BarcodeLib.TYPE.EAN13; break;
                case (Int32)BarcodeEncodingType.JAN13: type = BarcodeLib.TYPE.JAN13; break;
                case (Int32)BarcodeEncodingType.EAN8: type = BarcodeLib.TYPE.EAN8; break;
                case (Int32)BarcodeEncodingType.ITF14: type = BarcodeLib.TYPE.ITF14; break;
                case (Int32)BarcodeEncodingType.Codabar: type = BarcodeLib.TYPE.Codabar; break;
                case (Int32)BarcodeEncodingType.PostNet: type = BarcodeLib.TYPE.PostNet; break;
                case (Int32)BarcodeEncodingType.BOOKLAND: type = BarcodeLib.TYPE.BOOKLAND; break;
                case (Int32)BarcodeEncodingType.CODE11: type = BarcodeLib.TYPE.CODE11; break;
                case (Int32)BarcodeEncodingType.CODE39: type = BarcodeLib.TYPE.CODE39; break;
                case (Int32)BarcodeEncodingType.CODE39Extended: type = BarcodeLib.TYPE.CODE39Extended; break;
                case (Int32)BarcodeEncodingType.CODE93: type = BarcodeLib.TYPE.CODE93; break;
                case (Int32)BarcodeEncodingType.LOGMARS: type = BarcodeLib.TYPE.LOGMARS; break;
                case (Int32)BarcodeEncodingType.MSI_Mod10: type = BarcodeLib.TYPE.MSI_Mod10; break;
                case (Int32)BarcodeEncodingType.Interleaved2of5: type = BarcodeLib.TYPE.Interleaved2of5; break;
                case (Int32)BarcodeEncodingType.Standard2of5: type = BarcodeLib.TYPE.Standard2of5; break;
                case (Int32)BarcodeEncodingType.CODE128: type = BarcodeLib.TYPE.CODE128; break;
                case (Int32)BarcodeEncodingType.CODE128A: type = BarcodeLib.TYPE.CODE128A; break;
                case (Int32)BarcodeEncodingType.CODE128B: type = BarcodeLib.TYPE.CODE128B; break;
                case (Int32)BarcodeEncodingType.CODE128C: type = BarcodeLib.TYPE.CODE128C; break;
                case (Int32)BarcodeEncodingType.TELEPEN: type = BarcodeLib.TYPE.TELEPEN; break;
                default: MessageBox.Show("Please specify the encoding type."); break;
            }

            try
            {
                if (type != BarcodeLib.TYPE.UNSPECIFIED)
                {
                    MBarCode.IncludeLabel = this.chkGenerateLabel.Checked;

                    MBarCode.Alignment = Align;
                    MBarCode.RotateFlipType = (RotateFlipType)Enum.Parse(typeof(RotateFlipType), this.cboRotateFlip.SelectedItem.ToString(), true);

                    //label alignment and position
                    switch (this.cboLabelLocation.SelectedValue.ToInt32())
                    {
                        case (Int32)Locations.BOTTOMLEFT: MBarCode.LabelPosition = BarcodeLib.LabelPositions.BOTTOMLEFT; break;
                        case (Int32)Locations.BOTTOMRIGHT: MBarCode.LabelPosition = BarcodeLib.LabelPositions.BOTTOMRIGHT; break;
                        case (Int32)Locations.TOPCENTER: MBarCode.LabelPosition = BarcodeLib.LabelPositions.TOPCENTER; break;
                        case (Int32)Locations.TOPLEFT: MBarCode.LabelPosition = BarcodeLib.LabelPositions.TOPLEFT; break;
                        case (Int32)Locations.TOPRIGHT: MBarCode.LabelPosition = BarcodeLib.LabelPositions.TOPRIGHT; break;
                        default: MBarCode.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER; break;
                    }

                    pbBarcode.Image = MBarCode.Encode(type, this.txtData.Text.Trim(), this.btnForeColor.BackColor, this.btnBackColor.BackColor, W, H);
                    lblEncodingTime.Text = "(" + Math.Round(MBarCode.EncodingTime, 0, MidpointRounding.AwayFromZero).ToString() + "ms)";
                }

                pbBarcode.Width = pbBarcode.Image.Width;
                pbBarcode.Height = pbBarcode.Image.Height;

                //reposition the barcode image to the middle
                pbBarcode.Location = new Point((this.grpBarcodeImage.Location.X + this.grpBarcodeImage.Width / 2) - pbBarcode.Width / 2,
                    (this.grpBarcodeImage.Location.Y + this.grpBarcodeImage.Height / 2) - pbBarcode.Height / 2);
                ChangeStatus(sender, e);
            }
            catch (Exception ex)
            {
                if (MblnShowErrorMess)
                MessageBox.Show(ex.Message);
            }
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            pbBarcode.Location = new Point((this.grpBarcodeImage.Location.X + this.grpBarcodeImage.Width / 2) - pbBarcode.Width / 2,
                (this.grpBarcodeImage.Location.Y + this.grpBarcodeImage.Height / 2) - pbBarcode.Height / 2);
        }

        private void btnBackColor_Click(object sender, EventArgs e)
        {
            using (ColorDialog cdialog = new ColorDialog())
            {
                cdialog.AnyColor = true;
                if (cdialog.ShowDialog() == DialogResult.OK)
                {
                    MBarCode.BackColor = cdialog.Color;
                    btnBackColor.BackColor = this.MBarCode.BackColor;
                }
            }
        }

        private void btnForeColor_Click(object sender, EventArgs e)
        {
            using (ColorDialog cdialog = new ColorDialog())
            {
                cdialog.AnyColor = true;
                if (cdialog.ShowDialog() == DialogResult.OK)
                {
                    MBarCode.ForeColor = cdialog.Color;
                    btnForeColor.BackColor = this.MBarCode.ForeColor;
                }
            }
        }

        private bool SaveBarcode()//Extra detail
        {
            MstrCommonMessage = MobjNotification.GetErrorMessage(MdatMessages, (MblnAddStatus ? 1 : 3), out MmsgMessageIcon);

            if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                return false;

            MobjBLLBarCode.objDTOBarCode.intBarcodeID = Convert.ToInt32(txtData.Tag);
            MobjBLLBarCode.objDTOBarCode.lngItemID = Convert.ToInt64(cboItemName.SelectedValue);
            MobjBLLBarCode.objDTOBarCode.intEncodingID = cboEncodeType.SelectedValue.ToInt32();
            MobjBLLBarCode.objDTOBarCode.blnIsShowlabel = chkGenerateLabel.Checked.ToBoolean();
            MobjBLLBarCode.objDTOBarCode.intWidth = txtWidth.Text.ToInt32();
            MobjBLLBarCode.objDTOBarCode.intHeight = txtHeight.Text.ToInt32();
            MobjBLLBarCode.objDTOBarCode.intRotationID = this.cboRotateFlip.SelectedIndex.ToInt32();
            MobjBLLBarCode.objDTOBarCode.intAlignment = cboBarcodeAlign.SelectedValue.ToInt32();
            MobjBLLBarCode.objDTOBarCode.intLabelLocation = cboLabelLocation.SelectedValue.ToInt32();
            MobjBLLBarCode.objDTOBarCode.strRGBHexBackColor =btnBackColor.BackColor.Name.ToString();
            MobjBLLBarCode.objDTOBarCode.strRGBHexForeColor = btnForeColor.BackColor.Name.ToString();
            MobjBLLBarCode.objDTOBarCode.strBarcodeValue = txtData.Text.Trim();
            MobjBLLBarCode.objDTOBarCode.imgBarcodeImage = MobjImages.GetImageDataStream(pbBarcode.Image, System.Drawing.Imaging.ImageFormat.Bmp);

            if (cboEncodeType.SelectedValue.ToInt32()!=25)
            {
                PimgBarcodeImage = pbBarcode.Image;
            }
            else
            {
                PimgBarcodeImage = null;
            }
            return MobjBLLBarCode.SaveBarCode();
        }

        public void CreateBarCode(int intBarcodeID, long lngItemID, long lngBatchID, string strBatchNumber)
        {
            try
            {
                //cboBarcodeAlign.SelectedIndex = 0;
                //cboRotateFlip.SelectedIndex = 0;
                //cboLabelLocation.SelectedIndex = 0;
                //cboEncodeType.Text = "Code 128";
                //LoadBarCode();

                //MobjBLLBarCode.objDTOBarCode.lngItemID = lngItemID;
                //MobjBLLBarCode.objDTOBarCode.strBarcodeValue = strBatchNumber.Trim();
                //MobjBLLBarCode.objDTOBarCode.lngBatchID = lngBatchID;

                //if (MobjBLLBarCode.objDTOBarCode.strBarcodeValue != MobjBLLBarCode.GetBarCodeValue().Trim())
                //{
                //    MobjBLLBarCode.objDTOBarCode.intBarcodeID = intBarcodeID;
                //    GenerateBarcode(btnEncode, new EventArgs());
                //    MobjBLLBarCode.objDTOBarCode.imgBarcodeImage = MobjImages.GetImageDataStream(pbBarcode.Image, System.Drawing.Imaging.ImageFormat.Jpeg);

                //    MobjBLLBarCode.SaveBarCode();
                LoadCombo();
                //LoadBarCode();
                cboRotateFlip.DataSource = System.Enum.GetNames(typeof(RotateFlipType));
                MobjBLLBarCode.objDTOBarCode.intRowNumber = 1;
                MobjBLLBarCode.objDTOBarCode.blnIsTemplate = true;
                PlngItemID = lngItemID;
                if (MobjBLLBarCode.GetBarCode(MintMode, PlngItemID, PstrBarcodeValue))
                {
                    cboItemName.SelectedValue = MobjBLLBarCode.objDTOBarCode.lngItemID;
                    //txtData.Text = MobjBLLBarCode.objDTOBarCode.strBarcodeValue;
                    pbBarcode.Image = MobjImages.GetImage(MobjBLLBarCode.objDTOBarCode.imgBarcodeImage);
                    txtData.Tag = MobjBLLBarCode.objDTOBarCode.intBarcodeID.ToString();
                    cboEncodeType.SelectedValue = MobjBLLBarCode.objDTOBarCode.intEncodingID.ToInt32();
                    chkGenerateLabel.Checked = MobjBLLBarCode.objDTOBarCode.blnIsShowlabel.ToBoolean();
                    txtWidth.Text = MobjBLLBarCode.objDTOBarCode.intWidth.ToString();
                    txtHeight.Text = MobjBLLBarCode.objDTOBarCode.intHeight.ToString();
                    this.cboRotateFlip.SelectedIndex = MobjBLLBarCode.objDTOBarCode.intRotationID.ToInt32();
                    cboBarcodeAlign.SelectedValue = MobjBLLBarCode.objDTOBarCode.intAlignment.ToInt32();
                    cboLabelLocation.SelectedValue = MobjBLLBarCode.objDTOBarCode.intLabelLocation.ToInt32();
                    btnBackColor.BackColor = Color.FromName(MobjBLLBarCode.objDTOBarCode.strRGBHexBackColor.ToString());
                    btnForeColor.BackColor = Color.FromName(MobjBLLBarCode.objDTOBarCode.strRGBHexForeColor.ToString());
                    txtData.Text = strBatchNumber.Trim();
                    GenerateBarcode(null, null);
                    MobjBLLBarCode.objDTOBarCode.lngItemID = MobjBLLBarCode.objDTOBarCode.lngItemID;
                    MobjBLLBarCode.objDTOBarCode.lngBatchID = lngBatchID;
                    MobjBLLBarCode.objDTOBarCode.strBarcodeValue = txtData.Text.Trim();
                    MobjBLLBarCode.objDTOBarCode.imgBarcodeImage = MobjImages.GetImageDataStream(pbBarcode.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
                    PimgBarcodeImage = pbBarcode.Image;
                    MobjBLLBarCode.SaveBatchBarCode();
                }
            }
            catch (Exception ex)
            {

                MobjLogWriter.WriteLog("Error onm CreateBarCode() " + this.Name + " " + ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on CreateBarCode() " + ex.Message.ToString());
            }
            //}
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                grpBarcodePrintSetup.Visible = true;
                txtRow.Text = (txtWidth.Text.ToInt32() + 26).ToString();
                txtColumn.Text=(txtHeight.Text.ToInt32()+51).ToString();
                if (cboEncodeType.SelectedValue.ToInt32() == 25)
                {
                    txtRow.Text = "145";
                    txtColumn.Text = "110";
                }

            }catch{}
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            if (pbBarcode.Image.Width > 1 && pbBarcode.Image.Height > 1)
            {
                using (Graphics g = e.Graphics)
                {
                    int intWidth = Convert.ToInt32(pbBarcode.Image.Width), intHeight = Convert.ToInt32(pbBarcode.Image.Height);

                    //float zfRightMargin = e.MarginBounds.Right, afBottomMargin = e.MarginBounds.Bottom;
                    float zfRightMargin = e.PageBounds.Right, afBottomMargin = e.PageBounds.Bottom;
                    int zyColumn =  Math.Floor( Convert.ToDecimal((zfRightMargin) / intWidth)).ToInt32();//column
                    int yyRows = Math.Floor(Convert.ToDecimal((afBottomMargin) / intHeight)).ToInt32();//rows

                    intWidth = intWidth + 25;
                    intHeight = intHeight + 50;

                    int j, c;

                    for (int i = 0; i < zyColumn; i++)//X changing
                    {
                        j = (i * intWidth);

                        for (int k = 0; k < yyRows; k++)//Y changing
                        {
                            c = (k * intHeight);

                            if (c < e.MarginBounds.Bottom)
                            {
                                if (c == 0)
                                    g.DrawImage(pbBarcode.Image, j, 10);
                                else
                                    g.DrawImage(pbBarcode.Image, j, c);
                            }
                        }
                    }
                }
            }
        }

        private void Print(object sender, EventArgs e)
        {
            
            if (grpBarcodePrintSetup.Visible == true)
            {
                if (cboEncodeType.SelectedValue.ToInt32() != 25)
                {
                    PrintDialog objPrintdialog = new PrintDialog();
                    objPrintdialog.UseEXDialog = true;
                    if (objPrintdialog.ShowDialog() == DialogResult.OK)
                    {
                        PrintPreviewDialog objPritPreviwDiolog = new PrintPreviewDialog();
                        //pdDocument.DefaultPageSettings.PaperSize = printdialog.PrinterSettings.DefaultPageSettings.PaperSize;
                        pdDocument.PrinterSettings = objPrintdialog.PrinterSettings;
                        pdDocument.DefaultPageSettings.PaperSize = new System.Drawing.Printing.PaperSize("Custom Size", Convert.ToInt32(txtRow.Text), Convert.ToInt32(txtColumn.Text));
                        pdDocument.DefaultPageSettings.Margins.Right = 5;
                        pdDocument.DefaultPageSettings.Margins.Left = 5;
                        pdDocument.DefaultPageSettings.Margins.Bottom = 5;
                        pdDocument.DefaultPageSettings.Margins.Top = 5;
                        objPritPreviwDiolog.Document = pdDocument;

                        objPritPreviwDiolog.Width = objPrintdialog.PrinterSettings.DefaultPageSettings.PaperSize.Width;
                        objPritPreviwDiolog.Height = objPrintdialog.PrinterSettings.DefaultPageSettings.PaperSize.Height;
                        objPritPreviwDiolog.ShowDialog();
                    }
                }
                else
                {

                    PrintDialog objPrintdialog = new PrintDialog();
                    objPrintdialog.UseEXDialog = true;
                    if (objPrintdialog.ShowDialog() == DialogResult.OK)
                    {
                        System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument();
                        pd.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.pd_PrintPage);
                        PrintPreviewDialog objPritPreviwDiolog = new PrintPreviewDialog();
                        //pdDocument.DefaultPageSettings.PaperSize = printdialog.PrinterSettings.DefaultPageSettings.PaperSize;
                        pd.PrinterSettings = objPrintdialog.PrinterSettings;
                        pd.DefaultPageSettings.PaperSize = new System.Drawing.Printing.PaperSize("Custom Size", Convert.ToInt32(txtRow.Text), Convert.ToInt32(txtColumn.Text));
                        objPritPreviwDiolog.Document = pd;

                        objPritPreviwDiolog.Width = objPrintdialog.PrinterSettings.DefaultPageSettings.PaperSize.Width;
                        objPritPreviwDiolog.Height = objPrintdialog.PrinterSettings.DefaultPageSettings.PaperSize.Height;
                        objPritPreviwDiolog.ShowDialog();
                    }
                    //System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument();
                    //pd.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.pd_PrintPage);
                    //pd.Print();

                }
            }
        }
        private void pd_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs ev)
        {
            try
            {
                if (txtData.Text.Trim() != "" && (txtData.Text.Trim().Length == 12 || txtData.Text.Trim().Length == 13) && txtData.Text.Trim().ToDecimal() > 0)
                {
                    Ean13 ean13 = new Ean13();
                    ean13.CountryCode = txtData.Text.Trim().Substring(0, 2);
                    ean13.ManufacturerCode = txtData.Text.Trim().Substring(2, 5);
                    ean13.ProductCode = txtData.Text.Trim().Substring(7, 5);
                    if (txtData.Text.Trim().Length == 13)
                        ean13.ChecksumDigit = txtData.Text.Trim().Substring(12, 1);
                    ean13.Scale = (float)Convert.ToDecimal(1);
                    ean13.DrawEan13Barcode(ev.Graphics, new System.Drawing.Point(0, 0));
                    ev.Graphics.Dispose();
                }
            }
            catch (Exception ex)
            {
            }
            
        }
        private void CancelPrint(object sender, EventArgs e)
        {
            grpBarcodePrintSetup.Visible = false;
        }

        private void FormKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                switch (e.KeyCode)
                {
                    case Keys.Left:
                        MovePreviousItem(sender, e);
                        break;
                    case Keys.Right:
                        MoveNextItem(sender, e);
                        break;
                    case Keys.Up:
                        MoveLastItem(sender, e);
                        break;
                    case Keys.Down:
                        MoveFirstItem(sender, e);
                        break;
                }
            }
        }

        private void ClosingForm(object sender, FormClosingEventArgs e)
        {
            if (!MblnIsCancel)
            {
                //PimgBarcodeImage = pbBarcode.Image;
                PstrBarcodeValue = txtData.Text.Trim();
            }

            if (btnOk.Enabled)
            {
                MstrCommonMessage = MobjNotification.GetErrorMessage(MdatMessages, 8, out MmsgMessageIcon).Replace("#", "").Trim();
                if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.Yes)
                {
                    MobjNotification = null;
                    MobjLogWriter = null;
                    MobjBLLBarCode = null;
                    MobjImages = null;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void Close(object sender, EventArgs e)
        {
            Close();
        }

        private void txtHeight_Leave(object sender, EventArgs e)
        {
            
        }

        private void cboEncodeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus(null,null);
            if (cboEncodeType.SelectedValue.ToInt32() == 25)
            {
                txtWidth.Text = "200";
                txtHeight.Text = "100";
            }
            else
            {
                txtWidth.Text = "160";
                txtHeight.Text = "75";
            }
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    MobjBLLBarCode.objDTOBarCode.imgBarcodeImage = MobjImages.GetImageDataStream(pbBarcode.Image, System.Drawing.Imaging.ImageFormat.Bmp);
        //    pbBarcode.Image.Save("c:test.bmp");
        //    pbBarcode.Image = MobjImages.GetImage(MobjBLLBarCode.objDTOBarCode.imgBarcodeImage);
         
        //}
    }
}