﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Collections;
using System.IO;
using Microsoft.Reporting.WinForms;
using System.Xml.Serialization;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Xml.Schema;
using XMLControls;

namespace MyBooksERP
{
    public partial class FrmInvoiceTemplateDesign : DevComponents.DotNetBar.Office2007Form
    {
        Control m_cntrl = new Control();
        private int MiTimerInterval;
        private int MiCompanyId;
        private int MiUserId;
        private string MsMessageCaption;
        private string sAppPath;//appln path;
        string sttemp;
        string strMain;
        string strLocLeft;
        string strLocTop;
        string strSizWid;
        string strSizHe;
        string strText;
        string sttempgrp;
        string strGrpTextBox;
        string strGrpPicturebox;
        string strGrpDgv;
        string strTblXml;
        string strMainTabl;
        string strRowHedStyl;
        string strTblHeadXml;
        string strTblClnWid;
        string strMainBodyHi;
        string strDataSetName;
        string strAppPathinfo;
        string stTblxmlCls;
        string strTblxmlRws;
        string strTxtTextAlgn;
        ///////////////////////////
        //// </summary>//////////
        ///////<summary>
        private ArrayList MaMessageArr; //' Error Message display
        private ArrayList MaStatusMessageArr;
        ClsControlMove mObjClsContorlMove;//for moving controls
        ClsCommonUtility mObjClsCommonUtility;

        /// <summary>
        /// /////////////
        /// 
        List<Control> ListAllCntrl;
        List<Control> ListGrpNewAllCntrl;
        //////////////////////
       

        double dblSwid;
        double dblLtop;
        double dblLleft;
        double dblSheight;
        double dblRowHeight;
        TextBox objxmlListTxt;

        GroupBox objXmlNodePnlList = new GroupBox();


        Panel pnlTemplate;

        /// </summary>
        /////////////////////////////
        int iCounterTxb = 99;//////////counter for naming darg drop text box


        /// <summary>New simplified code////////using XMLControls commen class
        XMLTxtCntl objXMLTxtCntl;
        XMLTbCntl objXMLTbCntl;
        XMLImgCntl objXMLImgCntl;
        XMLLstCntl objXMLLstCntl = new XMLLstCntl();
        XMLLstCntl objXMLLNWstCntl;
        int intSize = 0;
        double dblSize;
        string strVisibility;
        string strDatasetname;
        string strGrpExprsn;
        string strBgColor;
        string strDefultName;
        string strValue;
        string strFntSize;
        string strFntWeight;
        
        string strTxtName;
        string strBordelStyl;
        string strBorderWidth;

        string strListOnList;
        string strListOnPnl;
        string strTxtLsL;
        /// </summary>
        public FrmInvoiceTemplateDesign()
        {
            InitializeComponent();
            MiCompanyId = ClsCommonSettings.CompanyID; // current companyid
            MiUserId = ClsCommonSettings.UserID; // current userid
            MiTimerInterval = ClsCommonSettings.TimerInterval; // current companyid
            MsMessageCaption = ClsCommonSettings.MessageCaption; //Message caption
            mObjClsContorlMove = new ClsControlMove();
            mObjClsCommonUtility = new ClsCommonUtility();
            
        }
        private void FrmInvoiceTemplateDesign_Load(object sender, EventArgs e)
        {
            //tooll not used now 
            navigationPane1.Items[2].Visible = false;
            tlstrpBtnShowPreview.Visible = false;
            LoadMessage();

            //mObjClsCommonUtility.FillCombos(cboOperationTypes.ComboBox, new string[] { "TypeID,Description", "STTypeReference", "ModuleType IN(3,4)", "Description", "Description" });
            //mObjClsCommonUtility.FillCombos(cboOperationTypes.ComboBox, new string[] { "TypeID,Description", "STTypeReference", "Description IN('SalesOrder','PurchaseOrder','PurchaseInvoice','SalesInvoice')", "Description", "Description" });
            DataTable datTemp = mObjClsCommonUtility.FillCombos(new string[] { "TypeID,Description", "STTypeReference", "Description IN('SalesOrder','PurchaseOrder','PurchaseInvoice','SalesInvoice')" });
            DataRow dr = datTemp.NewRow();
            dr["TypeID"] = "0";
            dr["Description"] = "--Select--";
            datTemp.Rows.InsertAt(dr, 0);
            cboOperationTypes.ComboBox.DataSource = datTemp;
            cboOperationTypes.ComboBox.DisplayMember = "Description";
            cboOperationTypes.ComboBox.ValueMember = "Description";
            sAppPath = System.AppDomain.CurrentDomain.BaseDirectory;
            //cboOperationTypes.SelectedIndex = -1;
            cboSampleTemplates.SelectedIndex = -1;

        }
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessageArr = new ArrayList();

        }
        private void loadcombo()
        {
            DataTable datTempTemplates = new DataTable();
            datTempTemplates.Columns.Add("TemplateID");
            datTempTemplates.Columns.Add("Template");
            int iCounter = 1;

            if (cboOperationTypes.ComboBox.Text == "--Select--")
            {
                datTempTemplates = null;
                cboSampleTemplates.ComboBox.DataSource = datTempTemplates;
                strAppPathinfo = null;
                tvMasterDetails.Nodes.Clear();
                cblMasterDetails.Items.Clear();
            }
            else if (cboOperationTypes.ComboBox.Text == "PurchaseOrder")
            {

                clsBLLReportViewer MobjclsBLLReportViewer = new clsBLLReportViewer();
                DataSet dtSet = MobjclsBLLReportViewer.DisplayPurchaseDetailsReport((int)OperationType.PurchaseOrder);
                              

                DirectoryInfo dir = new DirectoryInfo(@"CustomReports");
                FileInfo[] files = dir.GetFiles("*.rdlc");
                foreach (FileInfo file in files)
                {
                    if (file.Name.Contains("RptPo"))
                    {
                        string strFileName = file.Name.Remove(file.Name.Length - 10);

                        DataRow dr = datTempTemplates.NewRow();
                        dr["TemplateID"] = iCounter++;
                        dr["Template"] = strFileName;
                        datTempTemplates.Rows.Add(dr);
                    }
                }
                cboSampleTemplates.ComboBox.ValueMember = "TemplateID";
                cboSampleTemplates.ComboBox.DisplayMember = "Template";
                cboSampleTemplates.ComboBox.DataSource = datTempTemplates;

                cboSampleTemplates.ComboBox.SelectedValue = 1;
                FillTree(dtSet.Tables[0], "DtSetPurchase_PurchaseOrderMaster");
                //FillCbl(dtSet.Tables[1]);
            }
            else if (cboOperationTypes.ComboBox.Text == "SalesOrder")
            {

                clsBLLReportViewer MobjclsBLLReportViewer = new clsBLLReportViewer();
                //DataSet dtSet = MobjclsBLLReportViewer.DisplayPurchaseDetailsReport((int)OperationType.SalesOrder);

                DataSet dtSet = MobjclsBLLReportViewer.DisplaySalesDetailReport(2);
                DirectoryInfo dir = new DirectoryInfo(@"CustomReports");
                FileInfo[] files = dir.GetFiles("*.rdlc");
                foreach (FileInfo file in files)
                {
                    if (file.Name.Contains("RptSo"))
                    {
                        string strFileName = file.Name.Remove(file.Name.Length - 10);

                        DataRow dr = datTempTemplates.NewRow();
                        dr["TemplateID"] = iCounter++;
                        dr["Template"] = strFileName;
                        datTempTemplates.Rows.Add(dr);


                    }
                }
                cboSampleTemplates.ComboBox.ValueMember = "TemplateID";
                cboSampleTemplates.ComboBox.DisplayMember = "Template";
                cboSampleTemplates.ComboBox.DataSource = datTempTemplates;

                cboSampleTemplates.ComboBox.SelectedValue = 1;

                FillTree(dtSet.Tables[0], "DatasetSales_SalesOrderMaster");
                //FillCbl(dtSet.Tables[1],"DtSetPurchase_PurchaseOrderMaster");
                //FillCbl(dtSet.Tables[1]);

            }
            else if (cboOperationTypes.ComboBox.Text == "PurchaseInvoice")
            {
                clsBLLReportViewer MobjclsBLLReportViewer = new clsBLLReportViewer();
                DataSet dtSet = MobjclsBLLReportViewer.DisplayPurchaseDetailsReport((int)OperationType.PurchaseInvoice);
                DirectoryInfo dir = new DirectoryInfo(@"CustomReports");
                FileInfo[] files = dir.GetFiles("*.rdlc");
                foreach (FileInfo file in files)
                {
                    if (file.Name.Contains("RptPi"))
                    {
                        string strFileName = file.Name.Remove(file.Name.Length - 10);
                        DataRow dr = datTempTemplates.NewRow();
                        dr["TemplateID"] = iCounter++;
                        dr["Template"] = strFileName;
                        datTempTemplates.Rows.Add(dr);


                    }
                }
                cboSampleTemplates.ComboBox.ValueMember = "TemplateID";
                cboSampleTemplates.ComboBox.DisplayMember = "Template";
                cboSampleTemplates.ComboBox.DataSource = datTempTemplates;

                cboSampleTemplates.ComboBox.SelectedValue = 1;
                FillTree(dtSet.Tables[0], "DtSetPurchase_PurchaseInvoiceMaster");
                //FillCbl(dtSet.Tables[1],"DtSetPurchase_PurchaseOrderMaster");
                //FillCbl(dtSet.Tables[1]);
            }
            else if (cboOperationTypes.ComboBox.Text == "SalesInvoice")
            {
                clsBLLReportViewer MobjclsBLLReportViewer = new clsBLLReportViewer();
                DataSet dtSet = MobjclsBLLReportViewer.DisplaySalesDetailReport(3);
                DirectoryInfo dir = new DirectoryInfo(@"CustomReports");
                FileInfo[] files = dir.GetFiles("*.rdlc");
                foreach (FileInfo file in files)
                {
                    if (file.Name.Contains("RptSi"))
                    {
                        string strFileName = file.Name.Remove(file.Name.Length - 10);
                        DataRow dr = datTempTemplates.NewRow();
                        dr["TemplateID"] = iCounter++;
                        dr["Template"] = strFileName;
                        datTempTemplates.Rows.Add(dr);
                    }
                }
                cboSampleTemplates.ComboBox.ValueMember = "TemplateID";
                cboSampleTemplates.ComboBox.DisplayMember = "Template";
                cboSampleTemplates.ComboBox.DataSource = datTempTemplates;

                cboSampleTemplates.ComboBox.SelectedValue = 1;

                FillTree(dtSet.Tables[0], "DatasetSales_SalesInvoiceMaster");
                //FillCbl(dtSet.Tables[1],"DtSetPurchase_PurchaseOrderMaster");
                //FillCbl(dtSet.Tables[1]);
            }
        }
        private void cboOperationTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadcombo();
            pnlAllTemplate.Controls.Clear();
            txtTemplateName.Clear();
            ErrorTemplate.Clear();
            CustomRptViewer.Clear();
            CustomRptViewer.Reset();
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
           
            if (strAppPathinfo == null)
            {
                //MessageBox.Show("Please Select a Template");
                //ErrorTemplate.SetError(cboSampleTemplates.ComboBox , "Please Select a Template");
                ErrorTemplate.SetError(cboOperationTypes.ComboBox , "Please Select Operation Type ");
               
            }
            else
            {
                LoadTemplate(strAppPathinfo);
                cblMasterDetails.Items.Clear();
                //tvMasterDetails.Nodes.Clear();

                if (strAppPathinfo.Contains("PurchaseOrderRptPo") || strAppPathinfo.Contains("PurchaseInvoiceRptPi") || strAppPathinfo.Contains("SalesInvoiceRptSi") || strAppPathinfo.Contains("SalesOrderRptSo"))
                {
                    txtTemplateName.Text = "";
                }
                else
                {
                    //cboSampleTemplates.ComboBox.Text
                    txtTemplateName.Text = cboSampleTemplates.ComboBox.Text;
                }



            }
        }
        private void FillTree(DataTable dattbl, string strDtSet)
        {
            //Reports.Purchase.DtSetPurchase obj = new MyBooksERP.Reports.Purchase.DtSetPurchase();
            //obj.STPurchaseInvoiceExpenseMaster.Columns  
            //dattbl = new DataTable();
            //dattbl = dtSet.Tables[0];
            tvMasterDetails.Nodes.Clear();
            TreeNode nodePODtlsPrnt = new TreeNode(strDtSet);
            nodePODtlsPrnt.Text = strDtSet;
            nodePODtlsPrnt.Name = strDtSet;
            tvMasterDetails.TopNode = nodePODtlsPrnt;
            foreach (DataColumn dtaCln in dattbl.Columns)
            {
                TreeNode node = new TreeNode();
                node.Text = dtaCln.ColumnName.ToString();
                node.Name = dtaCln.ColumnName.ToString();
                node.Tag = "=First(Fields!" + node.Name + ".Value," + "\"" + strDtSet + "\")";
                nodePODtlsPrnt.Nodes.Add(node);

            }
            tvMasterDetails.Nodes.Add(nodePODtlsPrnt);
        }
        private void FillCbl(DataTable dattbl)
        {
            cblMasterDetails.Items.Clear();
            foreach (DataColumn dtaCln in dattbl.Columns)
            {
                cblMasterDetails.Items.Add(dtaCln.ToString());
            }
        }
        private void cboSampleTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboOperationTypes.ComboBox.Text == "PurchaseOrder")
            {
                strAppPathinfo = "CustomReports\\" + cboSampleTemplates.ComboBox.Text + "RptPo" + ".rdlc";
            }
            else if (cboOperationTypes.ComboBox.Text == "SalesOrder")
            {
                strAppPathinfo = "CustomReports\\" + cboSampleTemplates.ComboBox.Text + "RptSo" + ".rdlc";
            }
            else if (cboOperationTypes.ComboBox.Text == "PurchaseInvoice")
            {
                strAppPathinfo = "CustomReports\\" + cboSampleTemplates.ComboBox.Text + "RptPi" + ".rdlc";
            }
            else if (cboOperationTypes.ComboBox.Text == "SalesInvoice")
            {
                strAppPathinfo = "CustomReports\\" + cboSampleTemplates.ComboBox.Text + "RptSi" + ".rdlc";
            }
            //strFileName = ((System.Windows.Forms.ToolStripControlHost)(cboSampleTemplates)).Text;
            //strAppPathinfo = "CustomReports\\PurchaseReports\\" + strFileName + ".rdlc";
            //strAppPathinfo = "CustomReports\\" + strFileName + ".rdlc";
            //strAppPathinfo = AppDomain .CurrentDomain.BaseDirectory + @"MainReports\" + strFileName + ".rdlc";
            //LoadTemplate(strAppPathinfo);

            //pnlAllTemplate.Controls.Clear();
            txtTemplateName.Clear();
            CustomRptViewer.Clear();
            CustomRptViewer.Reset();


        }

        //To load controls from RDLC to Panel
        private void LoadTemplate(string FilePathInfo)
        {
            if (pnlAllTemplate.Controls.Contains(pnlTemplate))
            {
                pnlAllTemplate.Controls.Remove(pnlTemplate);
            }
            pnlTemplate = new Panel();
            pnlTemplate.Controls.Clear();
            pnlTemplate.Parent = pnlAllTemplate;
            pnlTemplate.AllowDrop = true;
            pnlTemplate.DragEnter += new DragEventHandler(this.pnlTemplate_DragEnter);
            pnlTemplate.DragDrop += new DragEventHandler(this.pnlTemplate_DragDrop);
            pnlTemplate.MouseDown += new MouseEventHandler(this.ctl_Mouse);
            pnlTemplate.Width = 550;
            pnlTemplate.Height = 650;
            Point pointNavPnl = navigationPane1.PointToClient(new Point(navigationPane1.Width));
            pnlTemplate.Location = new Point(pointNavPnl.X + 20, 5);

            pnlTemplate.BackColor = Color.White;
            pnlAllTemplate.Controls.Add(pnlTemplate);
            ListAllCntrl = new List<Control>();
            ListGrpNewAllCntrl = new List<Control>();
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(@strAppPathinfo);///////////////////////////////////////////////////////loding the specific rdlc from combo
            XmlNode reportitems = xDoc.GetElementsByTagName("Body").Item(0).FirstChild;
            XmlNode heightbody = xDoc.GetElementsByTagName("Body").Item(0).LastChild;
            XmlNodeList nodelist = reportitems.ChildNodes;
            XmlNode xlist = xDoc.GetElementsByTagName("List").Item(0);
            XmlNode xTab = xDoc.GetElementsByTagName("Table").Item(0);
            foreach (XmlNode xElmAllCntr in nodelist)
            {
                if (xElmAllCntr.Name == "Textbox")
                {
                    LoadXMLTxtBox(xElmAllCntr);
                    objXMLTxtCntl.ContextMenuStrip = contextMenuStrip2;
                    objXMLTxtCntl.Parent = pnlTemplate;
                    pnlTemplate.Controls.Add(objXMLTxtCntl);
                    ListAllCntrl.Add(objXMLTxtCntl);
                    mObjClsContorlMove.WireControl(objXMLTxtCntl);
                }
                if (xElmAllCntr.Name == "Table")
                {
                    XmlNodeList xTableNodeList = xElmAllCntr.ChildNodes;
                    LoadTableFromRDLC(xElmAllCntr, xTableNodeList);
                }
                if (xElmAllCntr.Name == "List")
                {
                    LoadXMLList(xElmAllCntr);

                    if (pnlTemplate.Controls.Contains(objXMLLstCntl))
                    {
                        pnlTemplate.Controls.Remove(objXMLLstCntl);
                    }
                    objXMLLstCntl.Parent = pnlTemplate;
                    
                    pnlTemplate.Controls.Add(objXMLLstCntl);
                    ListGrpNewAllCntrl.Add(objXMLLstCntl);
                    mObjClsContorlMove.WireControl(objXMLLstCntl);
                }
            }
        }
        //To Load textbox from RDLC
        private void LoadXMLTxtBox(XmlNode xElmAllCntr)
        {
            objXMLTxtCntl = new XMLTxtCntl();
            objXMLTxtCntl.Multiline = true;
            objXMLTxtCntl.Name = xElmAllCntr.Attributes[0].Value;
            if (xElmAllCntr.Attributes[0].Value.Contains("txbl"))
            {
                xElmAllCntr.Attributes[0].Value = xElmAllCntr.Attributes[0].Value.Remove(0, 7);
            }
            if (xElmAllCntr.Attributes[0].Value.Contains("txb"))
            {
                xElmAllCntr.Attributes[0].Value = xElmAllCntr.Attributes[0].Value.Remove(0, 6);
            }
            objXMLTxtCntl.Text = xElmAllCntr.Attributes[0].Value;
            XmlNodeList xTxtNodeList = xElmAllCntr.ChildNodes;
            foreach (XmlNode xTxtNode in xTxtNodeList)
            {
                if (xTxtNode.Name == "rd:DefaultName")
                {
                    objXMLTxtCntl.strDefaultName = xTxtNode.InnerText;
                }
                if (xTxtNode.Name == "Top")
                {
                   intSize = InchToPixel(xTxtNode.InnerText);
                    objXMLTxtCntl.Top = intSize;
                }
                if (xTxtNode.Name == "ZIndex")
                {
                    objXMLTxtCntl.intZindex = Convert.ToInt32(xTxtNode.InnerText);
                }
                if (xTxtNode.Name == "Left")
                {
                    intSize = InchToPixel(xTxtNode.InnerText);
                    objXMLTxtCntl.Left = intSize;
                }
                if (xTxtNode.Name == "Height")
                {
                    intSize = InchToPixel(xTxtNode.InnerText);
                    objXMLTxtCntl.Height = intSize;
                }
                if (xTxtNode.Name == "Width")
                {
                    intSize = InchToPixel(xTxtNode.InnerText);
                    objXMLTxtCntl.Width = intSize;
                }
                if (xTxtNode.Name == "Visibility")
                {
                    objXMLTxtCntl.strvisibilty = xTxtNode.InnerText;
                }
                if (xTxtNode.Name == "Style")
                {
                    XmlNodeList xTxtNodeStylList = xTxtNode.ChildNodes;
                    foreach (XmlNode xTxtNodeStNode in xTxtNodeStylList)
                    {
                        if (xTxtNodeStNode.Name == "FontSize")
                        {
                            objXMLTxtCntl.strFontSize = xTxtNodeStNode.InnerText;
                        }
                        if (xTxtNodeStNode.Name == "FontWeight")
                        {
                            objXMLTxtCntl.intFontWeight = Convert.ToInt32(xTxtNodeStNode.InnerText);
                        }
                        if (xTxtNodeStNode.Name == "BackgroundColor")
                        {
                            objXMLTxtCntl.BackColor = Color.LightGray;
                            objXMLTxtCntl.strBgColor = xTxtNodeStNode.InnerText;
                        }
                        if (xTxtNodeStNode.Name == "BorderStyle")
                        {
                            objXMLTxtCntl.strBorderStyle = xTxtNodeStNode.InnerText;
                        }
                        if (xTxtNodeStNode.Name == "BorderWidth")
                        {
                            objXMLTxtCntl.strBorderWidth = xTxtNodeStNode.InnerText;
                        }
                        if (xTxtNodeStNode.Name == "TextAlign")
                        {
                            if (xTxtNodeStNode.InnerText == "Left")
                            {
                                objXMLTxtCntl.TextAlign = HorizontalAlignment.Left;
                                objXMLTxtCntl.strTextAlign = xTxtNodeStNode.InnerText;
                            }
                            if (xTxtNodeStNode.InnerText == "Right")
                            {
                                objXMLTxtCntl.TextAlign = HorizontalAlignment.Right;
                                objXMLTxtCntl.strTextAlign = xTxtNodeStNode.InnerText;
                            }
                            if (xTxtNodeStNode.InnerText == "Center")
                            {
                                objXMLTxtCntl.TextAlign = HorizontalAlignment.Center;
                                objXMLTxtCntl.strTextAlign = xTxtNodeStNode.InnerText;
                            }
                        }
                    }
                }
                if (xTxtNode.Name == "Value")
                {
                    objXMLTxtCntl.strvalue = xTxtNode.InnerText;
                    if ((xTxtNode.InnerText) != null && !xTxtNode.InnerText.Contains("="))
                    {

                        objXMLTxtCntl.Text = xTxtNode.InnerText;
                    }
                }
            }
            objXMLTxtCntl.MouseDown += new MouseEventHandler(this.ctl_Mouse);
            objXMLTxtCntl.KeyDown += new KeyEventHandler(this.ctl_KeyDown);
            objXMLTxtCntl.MouseHover += new EventHandler (this.objXMLTxtCntl_MouseHover);
        }
        //obj
        private void objXMLTxtCntl_MouseHover(object sender, EventArgs e)
        {
            this.tlpTextboxContent.SetToolTip(this.objXMLTxtCntl, objXMLTxtCntl.Text);
        }
        //To load XMlImage  from RDLC
        private void LoadXMLImage(XmlNode xElmAllCntr)
        {
            objXMLImgCntl = new XMLImgCntl();
            objXMLImgCntl.Name = xElmAllCntr.Attributes[0].Value;
            XmlNodeList xListImgNodeList = xElmAllCntr.ChildNodes;
            foreach (XmlNode xListImgNode in xListImgNodeList)
            {
                if (xListImgNode.Name == "Sizing")
                {
                    objXMLImgCntl.strSizing = xListImgNode.InnerText;
                }
                if (xListImgNode.Name == "Width")
                {
                    intSize = InchToPixel(xListImgNode.InnerText);
                    objXMLImgCntl.Width = intSize;
                }
                if (xListImgNode.Name == "Top")
                {
                    intSize = InchToPixel(xListImgNode.InnerText);
                    objXMLImgCntl.Top = intSize;

                }
                if (xListImgNode.Name == "MIMEType")
                {
                    objXMLImgCntl.strMIMETYPE = xListImgNode.InnerText;
                }
                if (xListImgNode.Name == "Source")
                {
                    objXMLImgCntl.strSource = xListImgNode.InnerText;
                }
                if (xListImgNode.Name == "ZIndex")
                {
                    objXMLImgCntl.intZindex = Convert.ToInt32(xListImgNode.InnerText);
                }
                if (xListImgNode.Name == "Height")
                {
                    intSize = InchToPixel(xListImgNode.InnerText);
                    objXMLImgCntl.Height = intSize;
                }
                if (xListImgNode.Name == "Left")
                {
                    intSize = InchToPixel(xListImgNode.InnerText);
                    objXMLImgCntl.Left = intSize;
                }
                if (xListImgNode.Name == " Value")
                {
                    objXMLImgCntl.strvalue = xListImgNode.InnerText;
                }
            }
            objXMLImgCntl.MouseDown += new MouseEventHandler(this.ctl_Mouse);
        }
        //To load Table from RDLC
        private void LoadTableFromRDLC(XmlNode xElmAllCntr, XmlNodeList xTableNodeList)
        {
            objXMLTbCntl = new XMLTbCntl();
            foreach (XmlNode xTblNode in xTableNodeList)
            {
                if (xTblNode.Name == "ZIndex")
                {

                }
                if (xTblNode.Name == "DataSetName")
                {
                    objXMLTbCntl.strDataSetName = xTblNode.InnerText;
                }
                if (xTblNode.Name == "Visibility")
                {
                    objXMLTbCntl.strvisibilty = xTblNode.InnerText;
                }

                if (xTblNode.Name == "Style")
                {
                    XmlNodeList xTblStyNdlist = xTblNode.ChildNodes;
                    foreach (XmlNode xTbStylNd in xTblStyNdlist)
                    {
                        if (xTbStylNd.Name == "BorderWidth")
                        {
                            objXMLTbCntl.strBorderWidth = xTbStylNd.InnerText;
                        }

                    }

                }

                if (xTblNode.Name == "Left")
                {
                    intSize = InchToPixel(xTblNode.InnerText);
                    objXMLTbCntl.Left = intSize;
                }
                if (xTblNode.Name == "Height")
                {
                    intSize = InchToPixel(xTblNode.InnerText);
                    objXMLTbCntl.Height = intSize;
                }
                if (xTblNode.Name == "Top")
                {
                    intSize = InchToPixel(xTblNode.InnerText);
                    objXMLTbCntl.Top = intSize;
                }
                if (xTblNode.Name == "Width")
                {
                    intSize = InchToPixel(xTblNode.InnerText);
                    objXMLTbCntl.Width = intSize;
                }
                if (xTblNode.Name == "Details")
                {
                    XmlDocument xTablDocument = new XmlDocument();

                    XmlNodeList xTblDetailsNodeList = xTblNode.ChildNodes;
                    foreach (XmlNode xTblClnmDtiNode in xTblDetailsNodeList)
                    {
                        if (xTblClnmDtiNode.Name == "TableRows")
                        {
                            XmlNodeList xTableRowsNodeList = xTblClnmDtiNode.ChildNodes;
                            foreach (XmlNode xTblRowNode in xTableRowsNodeList)
                            {
                                if (xTblRowNode.Name == "TableRow")
                                {
                                    XmlNodeList xTableRowNodeList = xTblRowNode.ChildNodes;
                                    foreach (XmlNode xTblRowNWNode in xTableRowNodeList)
                                    {
                                        if (xTblRowNWNode.Name == "TableCells")
                                        {
                                            //////counter////////
                                            int TbClHedTxtTextCounter = 0;
                                            XmlNodeList xTblRowCellsList = xTblRowNWNode.ChildNodes;
                                            foreach (XmlNode xTblRowCellNode in xTblRowCellsList)
                                            {
                                                objXMLTbCntl.Columns.Add("", "");///////adding number of colunms to grid view
                                                if (xTblRowCellNode.Name == "TableCell")
                                                {
                                                    XmlNodeList xTblRowClList = xTblRowCellNode.ChildNodes;
                                                    foreach (XmlNode xTBRWCLnOde in xTblRowClList)
                                                    {
                                                        if (xTBRWCLnOde.Name == "ReportItems")
                                                        {
                                                            XmlNodeList xTBRWCLnOdeList = xTBRWCLnOde.ChildNodes;
                                                            foreach (XmlNode xTBRWCLnOdeTxtNode in xTBRWCLnOdeList)
                                                            {
                                                                if (xTBRWCLnOdeTxtNode.Name == "Textbox")
                                                                {
                                                                    strTxtName = xTBRWCLnOdeTxtNode.Attributes[0].Value;
                                                                    XmlNodeList xTBRWCLnOdeTxtNodeList = xTBRWCLnOdeTxtNode.ChildNodes;
                                                                    foreach (XmlNode xTXTDt in xTBRWCLnOdeTxtNodeList)
                                                                    {
                                                                        if (xTXTDt.Name == "rd:DefaultName")
                                                                        {
                                                                            //strTxtName = xTXTDt.InnerText;
                                                                        }
                                                                        if (xTXTDt.Name == "Value")
                                                                        {
                                                                            strText = xTXTDt.InnerText;
                                                                            objXMLTbCntl.Columns[TbClHedTxtTextCounter].Name = strTxtName;
                                                                            objXMLTbCntl.Rows[0].Cells[TbClHedTxtTextCounter].Value = strTxtName;
                                                                            objXMLTbCntl.Rows[0].Cells[TbClHedTxtTextCounter].Tag = strText;
                                                                           
                                                                            TbClHedTxtTextCounter++;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }///
                                            }
                                        }/////////////
                                        if (xTblRowNWNode.Name == "Height")
                                        {
                                            intSize = InchToPixel(xTblRowNWNode.InnerText);
                                            objXMLTbCntl.Rows[0].Height = intSize;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (xTblNode.Name == "Header")
                {
                    XmlNodeList xTblClmHeaderList = xTblNode.ChildNodes;
                    foreach (XmlNode xTblClmHeadNode in xTblClmHeaderList)
                    {
                        if (xTblClmHeadNode.Name == "TableRows")
                        {
                            XmlNodeList xTblRoWsList = xTblClmHeadNode.ChildNodes;
                            foreach (XmlNode xTblRoWsnode in xTblRoWsList)
                            {
                                if (xTblRoWsnode.Name == "TableRow")
                                {
                                    XmlNodeList xTblRowList = xTblRoWsnode.ChildNodes;
                                    foreach (XmlNode xTblRowNode in xTblRowList)//list contain TableCell and Height
                                    {
                                        if (xTblRowNode.Name == "TableCells")
                                        {
                                            int cLHedCounter = 0;
                                            XmlNodeList xTblCellsList = xTblRowNode.ChildNodes;
                                            foreach (XmlNode xTblCellsNode in xTblCellsList)
                                            {
                                                if (xTblCellsNode.Name == "TableCell")
                                                {
                                                    XmlNodeList xTblCellList = xTblCellsNode.ChildNodes;
                                                    foreach (XmlNode xTblCellNode in xTblCellList)
                                                    {
                                                        if (xTblCellNode.Name == "ReportItems")
                                                        {
                                                            XmlNodeList xTblCellNodeRPTList = xTblCellNode.ChildNodes;
                                                            foreach (XmlNode xTblCellRPtTxtNode in xTblCellNodeRPTList)
                                                            {
                                                                if (xTblCellRPtTxtNode.Name == "Textbox")
                                                                {
                                                                    strTxtName = xTblCellRPtTxtNode.Attributes[0].Value;//////////////giving  Header Cell name 
                                                                    XmlNodeList xTblCellRPtTxtNodeList = xTblCellRPtTxtNode.ChildNodes;
                                                                    foreach (XmlNode xTblCellRPtTxtNodeDtl in xTblCellRPtTxtNodeList)
                                                                    {
                                                                        if (xTblCellRPtTxtNodeDtl.Name == "Style")
                                                                        {

                                                                        }
                                                                        if (xTblCellRPtTxtNodeDtl.Name == "Value")
                                                                        {
                                                                            strText = xTblCellRPtTxtNodeDtl.InnerText;
                                                                        }
                                                                    }
                                                                    objXMLTbCntl.Columns[cLHedCounter].HeaderText = strText;/// Editing header to prevoisly added columns
                                                                    cLHedCounter++;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (xTblRowNode.Name == "Height")
                                        {
                                            intSize = InchToPixel(xTblRowNode.InnerText);
                                            objXMLTbCntl.ColumnHeadersHeight = intSize;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                objXMLTbCntl.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                if (xTblNode.Name == "TableColumns")
                {
                    int iTBcEllCount = 0;
                    XmlNodeList xTblClmnsnodeList = xTblNode.ChildNodes;
                    foreach (XmlNode xTblClmn in xTblClmnsnodeList)
                    {
                        if (xTblClmn.Name == "TableColumn")
                        {
                            XmlNodeList xTblCLList = xTblClmn.ChildNodes;
                            foreach (XmlNode xTBWidNode in xTblCLList)
                            {
                                if (xTBWidNode.Name == "Width")
                                {
                                    intSize = InchToPixel(xTBWidNode.InnerText);
                                    objXMLTbCntl.Columns[iTBcEllCount].Width = intSize;
                                }
                                if (xTBWidNode.Name == "Visibility")
                                {
                                    objXMLTbCntl.Columns[iTBcEllCount].Visible = false;
                                }
                                
                               
                            }
                        }
                        iTBcEllCount++;
                    }
                }
            }
            
            //objXMLTbCntl.DefaultCellStyle= DataGridView.
            objXMLTbCntl.Name = xElmAllCntr.Attributes[0].Value;
            objXMLTbCntl.RowHeadersVisible = false;
            objXMLTbCntl.MouseDown += new MouseEventHandler(this.objXMLTbCntl_MouseDown);
            pnlTemplate.Controls.Add(objXMLTbCntl);
            ListAllCntrl.Add(objXMLTbCntl);
        }
        //To Load List from RDLC
        private void LoadXMLList(XmlNode xElmAllCntr)
        {
            XmlNodeList xListNodeList = xElmAllCntr.ChildNodes;
            objXMLLstCntl = new XMLLstCntl();
            objXMLLstCntl.Controls.Clear();
            objXMLLstCntl.Name = xElmAllCntr.Attributes[0].Value;
            objXMLLstCntl.AllowDrop = true;
            objXMLLstCntl.DragEnter += new DragEventHandler(this.objXmlNodePnlList_DragEnter);
            objXMLLstCntl.DragDrop += new DragEventHandler(this.objXmlNodePnlList_DragDrop);
            objXMLLstCntl.MouseDown += new MouseEventHandler(this.ctl_Mouse);
            foreach (XmlNode xListNode in xListNodeList)
            {
                if (xListNode.Name == "Top")
                {
                    intSize = InchToPixel(xListNode.InnerText);
                    objXMLLstCntl.Top = intSize;
                }
                if (xListNode.Name == "Width")
                {
                    intSize = InchToPixel(xListNode.InnerText);
                    objXMLLstCntl.Width = intSize;
                }
                if (xListNode.Name == "ZIndex")
                {
                    objXMLLstCntl.intZindex = Convert.ToInt32(xListNode.InnerText);
                }
                if (xListNode.Name == "DataSetName")
                {
                    objXMLLstCntl.strDataSetName = xListNode.InnerText;
                }
                if (xListNode.Name == "Grouping")
                {
                    objXMLLstCntl.strGroupingName = xListNode.Attributes[0].Value;
                    objXMLLstCntl.strGroupExpressions = xListNode.InnerText;
                }
                if (xListNode.Name == "KeepTogether")
                {
                    objXMLLstCntl.Tag = xListNode.InnerText;
                }
                if (xListNode.Name == "Visibility")
                {
                    objXMLLstCntl.strvisibilty = xListNode.InnerText;
                }
                if (xListNode.Name == "Style")
                {
                    objXMLLstCntl.strBorderStyle = xListNode.InnerText;
                }

                if (xListNode.Name == "ReportItems")
                {
                    XmlNodeList xListAllCntrlList = xListNode.ChildNodes;
                    foreach (XmlNode xListNL in xListAllCntrlList)
                    {
                        if (xListNL.Name == "Textbox")
                        {
                            LoadXMLTxtBox(xListNL);
                            objXMLTxtCntl.ContextMenuStrip = contextMenuStrip2;
                            objXMLTxtCntl.Parent = objXMLLstCntl;
                            objXMLLstCntl.Controls.Add(objXMLTxtCntl);
                            ListGrpNewAllCntrl.Add(objXMLTxtCntl);
                            mObjClsContorlMove.WireControl(objXMLTxtCntl);
                        }
                        if (xListNL.Name == "Image")
                        {
                            LoadXMLImage(xListNL);
                            objXMLImgCntl.BorderStyle = BorderStyle.FixedSingle;
                            objXMLImgCntl.ContextMenuStrip = contextMenuStrip4;
                            objXMLImgCntl.Parent = objXMLLstCntl;
                            objXMLImgCntl.BringToFront();
                            objXMLLstCntl.Controls.Add(objXMLImgCntl);
                            ListGrpNewAllCntrl.Add(objXMLImgCntl);
                            mObjClsContorlMove.WireControl(objXMLImgCntl);
                        }
                        if (xListNL.Name == "List")
                        {
                            objXMLLNWstCntl = new XMLLstCntl();
                            objXMLLNWstCntl.Name = xListNL.Attributes[0].Value;
                            XmlNodeList XListonList = xListNL.ChildNodes;
                            foreach (XmlNode xNodeitem in XListonList)
                            {
                                if (xNodeitem.Name == "ZIndex")
                                {
                                    objXMLLNWstCntl.intZindex = Convert.ToInt32(xNodeitem.InnerText);
                                }
                                if (xNodeitem.Name == "Left")
                                {
                                    intSize = InchToPixel(xNodeitem.InnerText);
                                    objXMLLNWstCntl.Left = intSize;
                                }
                                if (xNodeitem.Name == "Width")
                                {
                                    intSize = InchToPixel(xNodeitem.InnerText);
                                    objXMLLNWstCntl.Width = intSize;
                                }
                                if (xNodeitem.Name == "DataSetName")
                                {
                                    objXMLLNWstCntl.strDataSetName = xNodeitem.InnerText;
                                }
                                if (xNodeitem.Name == "Visibility")
                                {
                                    objXMLLNWstCntl.strvisibilty = xNodeitem.InnerText;
                                }
                                if (xNodeitem.Name == "ReportItems")
                                {
                                    XmlNodeList XListTxtNdL = xNodeitem.ChildNodes;
                                    foreach (XmlNode XLstTxtNd in XListTxtNdL)
                                    {
                                        if (XLstTxtNd.Name == "Textbox")
                                        {
                                            LoadXMLTxtBox(XLstTxtNd);
                                            objXMLTxtCntl.Parent = objXMLLNWstCntl;
                                            objXMLLNWstCntl.Controls.Add(objXMLTxtCntl);
                                            mObjClsContorlMove.WireControl(objXMLTxtCntl);
                                            ListGrpNewAllCntrl.Add(objXMLTxtCntl);
                                        }
                                    }
                                }
                                if (xNodeitem.Name == "Top")
                                {
                                    intSize = InchToPixel(xNodeitem.InnerText);
                                    objXMLLNWstCntl.Top = intSize;
                                }
                                if (xNodeitem.Name == "Style")
                                {

                                }
                                if (xNodeitem.Name == "Height")
                                {
                                    intSize = InchToPixel(xNodeitem.InnerText);
                                    objXMLLNWstCntl.Height = intSize;
                                }

                            }
                            objXMLLNWstCntl.Parent = objXMLLstCntl;
                            objXMLLstCntl.Controls.Add(objXMLLNWstCntl);
                            mObjClsContorlMove.WireControl(objXMLLNWstCntl);
                            ListGrpNewAllCntrl.Add(objXMLLNWstCntl);
                        }
                        if (xListNL.Name == "Table")
                        {
                            objXMLTbCntl = new XMLTbCntl();
                            objXMLTbCntl.RowHeadersVisible = false;
                            objXMLTbCntl.ColumnHeadersVisible = false;
                            objXMLTbCntl.AllowUserToAddRows = false;
                            //dgvCommon.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
                            objXMLTbCntl.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                            objXMLTbCntl.ScrollBars = ScrollBars.None;
                            objXMLTbCntl.HorizontalScrollingOffset = 35;
                            XmlNodeList xTableNodeList = xListNL.ChildNodes;
                            foreach (XmlNode xTblNode in xTableNodeList)
                            {
                                if (xTblNode.Name == "Top")
                                {
                                    intSize = InchToPixel(xTblNode.InnerText);
                                    objXMLTbCntl.Top = intSize;
                                }
                                if (xTblNode.Name == "ZIndex")
                                {

                                }
                                if (xTblNode.Name == "DataSetName")
                                {

                                    objXMLTbCntl.strDataSetName = xTblNode.InnerText;
                                }
                                if (xTblNode.Name == "TableGroups")
                                {
                                    XmlNodeList xTblNodeList = xTblNode.ChildNodes;
                                    if (xTblNodeList[0].Name == "TableGroup")
                                    {
                                        XmlNodeList xLtblNodeli = xTblNodeList[0].ChildNodes;
                                        foreach (XmlNode xLtblNode in xLtblNodeli)
                                        {
                                            if (xLtblNode.Name == "Grouping")
                                            {
                                                objXMLTbCntl.strGroupingName = xLtblNode.Attributes[0].InnerText;
                                                XmlNodeList xTblGrpList = xLtblNode.ChildNodes;
                                                foreach (XmlNode xTblGrpNd in xTblGrpList)
                                                {
                                                    if (xTblGrpNd.Name == "GroupExpressions")
                                                    {

                                                        objXMLTbCntl.strGroupExpressions = xTblGrpNd.InnerText;
                                                    }
                                                }
                                            }
                                            if (xLtblNode.Name == "Header")
                                            {
                                                XmlNodeList xTblHnodeList = xLtblNode.ChildNodes;
                                                foreach (XmlNode xHnode in xTblHnodeList)
                                                {
                                                    if (xHnode.Name == "TableRows")
                                                    {
                                                        XmlNodeList xTblRowsList = xHnode.ChildNodes;
                                                        foreach (XmlNode xTblRwnd in xTblRowsList)
                                                        {
                                                            if (xTblRwnd.Name == "TableRow")
                                                            {
                                                                XmlNodeList xTblRwL = xTblRwnd.ChildNodes;
                                                                foreach (XmlNode xTbRwnd in xTblRwL)
                                                                {
                                                                    if (xTbRwnd.Name == "TableCells")
                                                                    {
                                                                        int cLHedCounter = 0;
                                                                        XmlNodeList xTbClsList = xTbRwnd.ChildNodes;
                                                                        foreach (XmlNode xTblClsNd in xTbClsList)
                                                                        {
                                                                            objXMLTbCntl.Columns.Add("", "");
                                                                            objXMLTbCntl.Rows.Add("", "");
                                                                            if (xTblClsNd.Name == "TableCell")
                                                                            {
                                                                                XmlNodeList xTblTbClList = xTblClsNd.ChildNodes;
                                                                                foreach (XmlNode xTblTbClNod in xTblTbClList)
                                                                                {
                                                                                    if (xTblTbClNod.Name == "ReportItems")
                                                                                    {
                                                                                        XmlNodeList xTblRptList = xTblTbClNod.ChildNodes;
                                                                                        foreach (XmlNode xTblRptnd in xTblRptList)
                                                                                        {
                                                                                            strTxtName = xTblRptnd.Attributes[0].Value;
                                                                                            if (xTblRptnd.Name == "Textbox")
                                                                                            {
                                                                                                XmlNodeList xTblTxbList = xTblRptnd.ChildNodes;
                                                                                                foreach (XmlNode xTblTxbNd in xTblTxbList)
                                                                                                {
                                                                                                    if (xTblTxbNd.Name == "Value")
                                                                                                    {
                                                                                                        strText = xTblTxbNd.InnerText;
                                                                                                    }
                                                                                                }

                                                                                                objXMLTbCntl.Rows[0].Cells[cLHedCounter].Value = strTxtName;
                                                                                                objXMLTbCntl.Rows[0].Cells[cLHedCounter].Tag = strText;
                                                                                                cLHedCounter++;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    if (xTbRwnd.Name == "Height")
                                                                    {
                                                                        intSize = InchToPixel(xTbRwnd.InnerText);
                                                                        objXMLTbCntl.Rows[0].Height = intSize;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (xLtblNode.Name == "Footer")
                                            {
                                                XmlNodeList xTblFtList = xLtblNode.ChildNodes;
                                                foreach (XmlNode xTblFtNode in xTblFtList)
                                                {
                                                    if (xTblFtNode.Name == "TableRows")
                                                    {
                                                        XmlNodeList xTblFtTblRwsList = xTblFtNode.ChildNodes;
                                                        foreach (XmlNode xTbFtTbRwnod in xTblFtTblRwsList)
                                                        {
                                                            if (xTbFtTbRwnod.Name == "TableRow")
                                                            {
                                                                XmlNodeList xTblFTbClList = xTbFtTbRwnod.ChildNodes;
                                                                foreach (XmlNode xTblFtClNode in xTblFTbClList)
                                                                {
                                                                    if (xTblFtClNode.Name == "TableCells")
                                                                    {
                                                                        int cLHedCounter = 0;
                                                                        XmlNodeList xTblFtTbClsList = xTblFtClNode.ChildNodes;
                                                                        foreach (XmlNode xTblFtTbClnode in xTblFtTbClsList)
                                                                        {
                                                                            if (xTblFtTbClnode.Name == "TableCell")
                                                                            {
                                                                                XmlNodeList xTblFtTbClList = xTblFtTbClnode.ChildNodes;

                                                                                foreach (XmlNode xTblFtTbcLnode in xTblFtTbClList)
                                                                                {
                                                                                    if (xTblFtTbcLnode.Name == "ReportItems")
                                                                                    {
                                                                                        XmlNodeList xTbFtRptList = xTblFtTbcLnode.ChildNodes;
                                                                                        foreach (XmlNode xTblFtRptNode in xTbFtRptList)
                                                                                        {
                                                                                            strTxtName = xTblFtRptNode.Attributes[0].Value;
                                                                                            if (xTblFtRptNode.Name == "Textbox")
                                                                                            {
                                                                                                XmlNodeList xTblFtTxtList = xTblFtRptNode.ChildNodes;
                                                                                                foreach (XmlNode xTblFtTxtNode in xTblFtTxtList)
                                                                                                {
                                                                                                    //if (xTblFtTxtNode.Name == "Style")
                                                                                                    //{

                                                                                                    //}
                                                                                                    if (xTblFtTxtNode.Name == "Value")
                                                                                                    {
                                                                                                        strText = xTblFtTxtNode.InnerText;

                                                                                                    }
                                                                                                }
                                                                                                objXMLTbCntl.Rows[1].Cells[cLHedCounter].Tag = strText;
                                                                                                objXMLTbCntl.Rows[1].Cells[cLHedCounter].Value = strTxtName;
                                                                                                cLHedCounter++;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    if (xTblFtClNode.Name == "Height")
                                                                    {
                                                                        intSize = InchToPixel(xTblFtClNode.InnerText);
                                                                        objXMLTbCntl.Rows[1].Height = intSize;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (xTblNode.Name == "Left")
                                {
                                    intSize = InchToPixel(xTblNode.InnerText);
                                    objXMLTbCntl.Left = intSize;
                                }
                                if (xTblNode.Name == "Height")
                                {
                                    intSize = InchToPixel(xTblNode.InnerText);
                                    objXMLTbCntl.Height = intSize;
                                }
                                if (xTblNode.Name == "Width")
                                {
                                    intSize = InchToPixel(xTblNode.InnerText);
                                    objXMLTbCntl.Width = intSize;

                                }
                                if (xTblNode.Name == "TableColumns")
                                {
                                    int iTBcEllCount = 0;
                                    XmlNodeList xTblClmnsnodeList = xTblNode.ChildNodes;
                                    foreach (XmlNode xTblClmn in xTblClmnsnodeList)
                                    {
                                        if (xTblClmn.Name == "TableColumn")
                                        {
                                            XmlNodeList xTblCLList = xTblClmn.ChildNodes;

                                            foreach (XmlNode xTBWidNode in xTblCLList)
                                            {
                                                if (xTBWidNode.Name == "Width")
                                                {
                                                    intSize = InchToPixel(xTBWidNode.InnerText);
                                                    objXMLTbCntl.Columns[iTBcEllCount].Width = intSize;
                                                }
                                                
                                                
                                                
                                            }
                                        }
                                        iTBcEllCount++;
                                    }
                                }
                            }
                            objXMLTbCntl.Name = xListNL.Attributes[0].Value;
                            objXMLTbCntl.Parent = objXMLLstCntl;
                            objXMLTbCntl.BringToFront();
                            objXMLLstCntl.Controls.Add(objXMLTbCntl);
                            ListGrpNewAllCntrl.Add(objXMLTbCntl);
                        }
                    }
                }
                if (xListNode.Name == "Height")
                {
                    intSize = InchToPixel(xListNode.InnerText);
                    objXMLLstCntl.Height = intSize;
                }
                if (xListNode.Name == "Left")
                {
                    intSize = InchToPixel(xListNode.InnerText);
                    objXMLLstCntl.Left = intSize;
                }
            }
        }
        //Convert Inches to pixet
        private static int   InchToPixel(string strSize)
        {
            int intPixelsize = 0;
            intPixelsize = Convert.ToInt32(Math.Abs(Convert.ToDouble(strSize.Remove(strSize.Length - 2)) * 72));
            return intPixelsize;
        }
        private void PixelToInch(string value)
        {

        }
        private void pnlTemplate_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }
        private void pnlTemplate_DragDrop(object sender, DragEventArgs e)//////drop txt to panel
        {
            if (e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                iCounterTxb++;
                Point clientPoint = pnlTemplate.PointToClient(new Point(e.X, e.Y));
                e.Effect = DragDropEffects.Copy;
                string strDataValue = (string)e.Data.GetData(DataFormats.StringFormat);
                string[] strTempDataValue = strDataValue.Split('^');
                string strTxtname = strTempDataValue[0].ToString();
                string strTxttag = strTempDataValue[1].ToString();
                if (strDataValue.Contains("LogoFile"))
                {
                    //objXMLImgCntl = new XMLImgCntl();
                    //objXMLImgCntl.ContextMenuStrip = contextMenuStrip4;
                    //objXMLImgCntl.BorderStyle = BorderStyle.FixedSingle;
                    //objXMLImgCntl.Name = "img" + iCounterTxb.ToString();
                    //objXMLImgCntl.Parent = objXMLLstCntl;
                    //objXMLImgCntl.Location = new Point(clientPoint.X, clientPoint.Y);
                    //objXMLImgCntl.PreviewKeyDown += new PreviewKeyDownEventHandler(this.objXMLImgCntl_PreviewKeyDownEventHandler);
                    //objXMLImgCntl.MouseDown += new MouseEventHandler(this.ctl_Mouse);
                    //objXMLImgCntl.BringToFront();
                    //pnlTemplate.Controls.Add(objXMLImgCntl);
                    //ListAllCntrl.Add(objXMLImgCntl);
                    //objXMLImgCntl.BringToFront();

                }
                else
                {
                    objXMLTxtCntl = new XMLTxtCntl();
                    objXMLTxtCntl.Multiline = true;
                    objXMLTxtCntl.Name = "txb" + iCounterTxb.ToString() + strTxtname;
                    objXMLTxtCntl.ContextMenuStrip = contextMenuStrip2;
                    objXMLTxtCntl.Parent = pnlTemplate;
                    objXMLTxtCntl.Location = new Point(clientPoint.X, clientPoint.Y);
                    mObjClsContorlMove.WireControl(objXMLTxtCntl);
                    objXMLTxtCntl.Text = strTxtname;
                    objXMLTxtCntl.strvalue = strTxttag;
                    objXMLTxtCntl.MouseDown += new MouseEventHandler(this.ctl_Mouse);
                    objXMLTxtCntl.KeyDown += new KeyEventHandler(this.ctl_KeyDown);
                    pnlTemplate.Controls.Add(objXMLTxtCntl);
                    ListAllCntrl.Add(objXMLTxtCntl);
                    objXMLTxtCntl.BringToFront();

                    objXMLTxtCntl = new XMLTxtCntl();
                    objXMLTxtCntl.Multiline = true;
                    objXMLTxtCntl.Name = "txB" + iCounterTxb.ToString();
                    objXMLTxtCntl.Parent = pnlTemplate;
                    objXMLTxtCntl.MouseDown += new MouseEventHandler(this.ctl_Mouse);
                    objXMLTxtCntl.KeyDown += new KeyEventHandler(this.ctl_KeyDown);
                    objXMLTxtCntl.Location = new Point(clientPoint.X - 110, clientPoint.Y);
                    ListAllCntrl.Add(objXMLTxtCntl);
                    mObjClsContorlMove.WireControl(objXMLTxtCntl);
                    pnlTemplate.Controls.Add(objXMLTxtCntl);
                    objXMLTxtCntl.BringToFront();
                }
            }
        }
        private void objXmlNodePnlList_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }
        private void objXmlNodePnlList_DragDrop(object sender, DragEventArgs e)///drop txt to grpbox on a pnl
        {
            if (e.Data.GetDataPresent(DataFormats.StringFormat))
            {

                iCounterTxb++;
                Point clientPoint = objXMLLstCntl.PointToClient(new Point(e.X, e.Y));
                e.Effect = DragDropEffects.Copy;
                string strDataValue = (string)e.Data.GetData(DataFormats.StringFormat);
                string[] strTempDataValue = strDataValue.Split('^');
                string strTxtname = strTempDataValue[0].ToString();
                string strTxttag = strTempDataValue[1].ToString();
                if (strDataValue.Contains("LogoFile"))
                {
                    objXMLImgCntl = new XMLImgCntl();
                    objXMLImgCntl.ContextMenuStrip = contextMenuStrip4;
                    objXMLImgCntl.BorderStyle = BorderStyle.FixedSingle;
                    objXMLImgCntl.Name = "img" + iCounterTxb.ToString();
                    objXMLImgCntl.Parent = objXMLLstCntl;
                    objXMLImgCntl.Location = new Point(clientPoint.X, clientPoint.Y);
                    objXMLImgCntl.PreviewKeyDown += new PreviewKeyDownEventHandler(this.objXMLImgCntl_PreviewKeyDownEventHandler);
                    objXMLImgCntl.MouseDown += new MouseEventHandler(this.ctl_Mouse);
                    objXMLImgCntl.BringToFront();
                    ListGrpNewAllCntrl.Add(objXMLImgCntl);
                    mObjClsContorlMove.WireControl(objXMLImgCntl);
                    objXMLLstCntl.Controls.Add(objXMLImgCntl);
                }
                else
                {
                    objXMLTxtCntl = new XMLTxtCntl();
                    objXMLTxtCntl.Multiline = true;
                    objXMLTxtCntl.BringToFront();
                    objXMLTxtCntl.Name = "txbl" + iCounterTxb.ToString() + strTxtname;
                    objXMLTxtCntl.Text = strTxtname;
                    objXMLTxtCntl.strvalue = strTxttag;
                    //objXMLTxtCntl.ContextMenuStrip = contextMenuStrip4;
                    objXMLTxtCntl.Parent = objXMLLstCntl;
                    objXMLTxtCntl.MouseDown += new MouseEventHandler(this.ctl_Mouse);
                    objXMLTxtCntl.KeyDown += new KeyEventHandler(this.ctl_KeyDown);
                    objXMLTxtCntl.Location = new Point(clientPoint.X, clientPoint.Y);
                    ListGrpNewAllCntrl.Add(objXMLTxtCntl);
                    mObjClsContorlMove.WireControl(objXMLTxtCntl);
                    objXMLLstCntl.Controls.Add(objXMLTxtCntl);
                    objXMLTxtCntl.BringToFront();
                    objXMLTxtCntl = new XMLTxtCntl();
                    objXMLTxtCntl.Multiline = true;
                    objXMLTxtCntl.Name = "txBl" + iCounterTxb.ToString();
                    objXMLTxtCntl.Parent = objXMLLstCntl;
                    objXMLTxtCntl.MouseDown += new MouseEventHandler(this.ctl_Mouse);
                    objXMLTxtCntl.KeyDown += new KeyEventHandler(this.ctl_KeyDown);
                    objXMLTxtCntl.Location = new Point(clientPoint.X-110, clientPoint.Y);
                    ListGrpNewAllCntrl.Add(objXMLTxtCntl);
                    mObjClsContorlMove.WireControl(objXMLTxtCntl);
                    objXMLLstCntl.Controls.Add(objXMLTxtCntl);
                    objXMLTxtCntl.BringToFront();
                }
            }
        }
        private void xmlListTxt_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (objxmlListTxt != null && sender.GetType() == objxmlListTxt.GetType())
                {
                    objxmlListTxt = (TextBox)sender;
                    m_cntrl = (TextBox)sender;
                }
            }
            catch { }
        }
        private void ctl_Mouse(object sender, MouseEventArgs e)
        {
            try
            {
                if (objXMLTxtCntl != null && sender.GetType() == objXMLTxtCntl.GetType())
                {
                    objXMLTxtCntl = (XMLTxtCntl)sender;
                    m_cntrl = (XMLTxtCntl)sender;
                    cblMasterDetails.Items.Clear();
                    tvMasterDetails.Visible = true;
                }
                if (objXMLImgCntl != null && sender.GetType() == objXMLImgCntl.GetType())
                {
                    objXMLImgCntl = (XMLImgCntl)sender;
                    m_cntrl = (XMLImgCntl)sender;
                    cblMasterDetails.Items.Clear();
                    tvMasterDetails.Visible = true;
                }
                if (objXMLLstCntl != null && sender.GetType() == objXMLLstCntl.GetType())
                {
                    objXMLLstCntl = (XMLLstCntl)sender;
                    m_cntrl = (XMLLstCntl)sender;
                    tvMasterDetails.Visible = true;
                }
                if (pnlTemplate != null && sender.GetType() == pnlTemplate.GetType())
                {
                    pnlTemplate = (Panel)sender;
                    m_cntrl = (Panel)sender;
                    tvMasterDetails.Visible = true;
                    mObjClsContorlMove.ClearHandles();
                    cblMasterDetails.Items.Clear();
                }

            }
            catch { }
        }
        private void ctl_MoseOver(object sender, EventArgs e)
        {
            tlpTextboxContent.Show(objXMLTxtCntl.Text, objXMLTxtCntl);

        }
        private void objXMLTbCntl_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (objXMLTbCntl != null && sender.GetType() == objXMLTbCntl.GetType())
                {
                    objXMLTbCntl = (XMLTbCntl)sender;
                    m_cntrl = (XMLTbCntl)sender;
                    cblMasterDetails.Items.Clear();
                    mObjClsContorlMove.ClearHandles();
                    //tvMasterDetails.Nodes.Clear();
                    tvMasterDetails.Visible = false;
                    for (int i = 0; i < objXMLTbCntl.ColumnCount; i++)
                    {
                        string items = objXMLTbCntl.Columns[i].HeaderText;
                        if (objXMLTbCntl.Columns[i].Visible == false)
                        {
                            cblMasterDetails.Items.Add(items, CheckState.Checked);
                            
                        }
                        else
                        {
                            cblMasterDetails.Items.Add(items);
                        }
                    }
                }
            }
            catch { }


        }
        private void objXMLImgCntl_PreviewKeyDownEventHandler(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (objXMLImgCntl != null && objXMLImgCntl.Parent.Controls.Contains(objXMLImgCntl))
                {
                    if (ListAllCntrl.Contains(objXMLImgCntl))
                    {
                        ListAllCntrl.Remove(objXMLImgCntl);
                    }
                    else if (ListGrpNewAllCntrl.Contains(objXMLImgCntl))
                    {
                        ListGrpNewAllCntrl.Remove(objXMLImgCntl);
                    }
                    objXMLImgCntl.Parent.Controls.Remove(objXMLImgCntl);
                    mObjClsContorlMove.ClearHandles();
                }

            }

        }
        private void ctl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (objXMLTxtCntl != null && objXMLTxtCntl.Parent.Controls.Contains(objXMLTxtCntl))
                {
                    if (ListAllCntrl.Contains(objXMLTxtCntl))
                    {
                        ListAllCntrl.Remove(objXMLTxtCntl);
                    }
                    else if (ListGrpNewAllCntrl.Contains(objXMLTxtCntl))
                    {
                        ListGrpNewAllCntrl.Remove(objXMLTxtCntl);
                    }
                    objXMLTxtCntl.Parent.Controls.Remove(objXMLTxtCntl);
                    mObjClsContorlMove.ClearHandles();
                }
            }
        }
        private void ctl_Drag(object sender, DragEventArgs e)
        {
            m_cntrl = (TextBox)sender;
            ActiveControl = ((TextBox)sender);

            if (e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                e.Effect = DragDropEffects.Copy;
                string strDataValue = (string)e.Data.GetData(DataFormats.StringFormat);
                string[] strTempDataValue = strDataValue.Split('^');
                string strTxtname = strTempDataValue[0].ToString();
                string strTxttag = strTempDataValue[1].ToString();
                ActiveControl.Text = strTxtname;
                ActiveControl.Text.Clone();
                ActiveControl.Tag = "";
                ActiveControl.Tag = strTxttag;
            }
        }
        private void dELETEToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            if (objXMLTxtCntl != null && objXMLTxtCntl.Parent.Controls.Contains(objXMLTxtCntl))
            {
                if (ListAllCntrl.Contains(objXMLTxtCntl))
                {
                    ListAllCntrl.Remove(objXMLTxtCntl);
                }
                else if (ListGrpNewAllCntrl.Contains(objXMLTxtCntl))
                {
                    ListGrpNewAllCntrl.Remove(objXMLTxtCntl);
                }
                objXMLTxtCntl.Parent.Controls.Remove(objXMLTxtCntl);
                mObjClsContorlMove.ClearHandles();
            }
        }

        private void xListTxtBox_MouseDown(object sender, MouseEventArgs e)
        {

        }
        private void deleteTextBoxToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (objXMLImgCntl != null && objXMLImgCntl.Parent.Controls.Contains(objXMLImgCntl))
            {
                ListGrpNewAllCntrl.Remove(objXMLImgCntl);
                objXMLImgCntl.Parent.Controls.Remove(objXMLImgCntl);
                mObjClsContorlMove.ClearHandles();
            }
        }
        private void pnlAllTemplate_MouseClick(object sender, MouseEventArgs e)
        {
            mObjClsContorlMove.ClearHandles();
        }
        private void cblMasterDetails_ItemCheck_1(object sender, ItemCheckEventArgs e)
        {
            try
            {
                if (e.NewValue == CheckState.Checked)
                {
                    objXMLTbCntl.Columns[e.Index].Visible = false;
                }
                else if (e.NewValue == CheckState.Unchecked)
                {
                    objXMLTbCntl.Columns[e.Index].Visible = true;
                }
            }
            catch { }
        }
        private void tvMasterDetails_ItemDrag_1(object sender, ItemDragEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Left)
                {
                    tvMasterDetails.DoDragDrop(tvMasterDetails.SelectedNode.Text + "^" + tvMasterDetails.SelectedNode.Tag, DragDropEffects.Copy);
                }
            }
            catch
            {

            }
        }

        private void tvToolbox_ItemDrag(object sender, ItemDragEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Left)
                {
                    tvMasterDetails.DoDragDrop(tvMasterDetails.SelectedNode.Text, DragDropEffects.Copy);
                }
            }
            catch
            {

            }
        }
        private void tlstrpbtnSave_Click(object sender, EventArgs e)
        {
            string path = "";
            int saveMode = 0;
            bool blnSave = false;
            if (txtTemplateName.Text == "" || txtTemplateName.Text==null )
            {
                //MessageBox.Show("Enter a valide Template name");
                ErrorTemplate.SetError(txtTemplateName.TextBox, "Enter a valide Template name");
            }
            else if (txtTemplateName.Text.Length > 0)
            {

                XmlDocument xmldoc = new XmlDocument();
                if (cboOperationTypes.ComboBox.Text == "PurchaseOrder")
                {
                    saveMode = 1;
                    xmldoc.RemoveAll();
                    path = "CustomReports\\" + txtTemplateName.Text + "RptPo" + ".rdlc";
                    xmldoc.Load(@"CustomReports\\PurchaseOrderRptPo.rdlc");


                }
                else if (cboOperationTypes.ComboBox.Text == "PurchaseInvoice")
                {
                    saveMode = 2;
                    xmldoc.RemoveAll();
                    path = "CustomReports\\" + txtTemplateName.Text + "RptPi" + ".rdlc";
                    xmldoc.Load(@"CustomReports\\PurchaseInvoiceRptPi.rdlc");


                }
                else if (cboOperationTypes.ComboBox.Text == "SalesOrder")
                {
                    saveMode = 3;
                    xmldoc.RemoveAll();
                    path = "CustomReports\\" + txtTemplateName.Text + "RptSo" + ".rdlc";
                    xmldoc.Load(@"CustomReports\\SalesOrderRptSo.rdlc");

                }
                else if (cboOperationTypes.ComboBox.Text == "SalesInvoice")
                {
                    saveMode = 4;
                    xmldoc.RemoveAll();
                    path = "CustomReports\\" + txtTemplateName.Text + "RptSi" + ".rdlc";
                    xmldoc.Load(@"CustomReports\\SalesInvoiceRptSi.rdlc");

                }


                if (pnlAllTemplate.Controls.Contains(pnlTemplate))
                {
                    blnSave = ChekFileExist(path);
                }
                else
                {
                    MessageBox.Show("Show a Template");
                }

                if (blnSave == true)
                {
                    SaveTemplate(xmldoc, path, saveMode);
                }
                else if (blnSave == false)
                {
                    txtTemplateName.Focus();
                }

            }
        }

        private bool ChekFileExist(string path)
        {
           bool blnSave = false;
           if (File.Exists(@path))
           {
               if (path.Contains("PurchaseOrderRptPo") || path.Contains("PurchaseInvoiceRptPi") || path.Contains("SalesInvoiceRptSi") || path.Contains("SalesOrderRptSo"))
               {
                   MessageBox.Show("Default Template Existed With This Name");
                   
                   blnSave = false;
               }
               else
               {
                   if (cboSampleTemplates.ComboBox.Text == txtTemplateName.Text)
                   {

                       if (MessageBox.Show("Are you want to save the Changes?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information ) == DialogResult.Yes)
                       {
                           blnSave = true;
                       }
                       else
                       {
                           blnSave = false;
                       }
                   }
                   else if (cboSampleTemplates.ComboBox.Text != txtTemplateName.Text)
                   {
                       if (MessageBox.Show("File Existed Are you want to repalce?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                       {
                           blnSave = true;
                       }
                       else
                       {
                           blnSave = false;
                       }
                   }
               }
           }
           else
           {
               if (MessageBox.Show("Do you want to save the Template?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
               {
                   blnSave = true;
               }
               else
               {
                   blnSave = false;
               }
               //blnSave = true;
           }

           return blnSave;
        }

        //To Save Template AS RDLC
        private void SaveTemplate(XmlDocument xmldoc, string path, int saveMode)
        {
            strMain = "";
            sttemp = "";

            XmlNode reportitems = xmldoc.GetElementsByTagName("Body").Item(0);
            //strMainBodyHi = reportitems.InnerText;
            XmlNode BodyHight = reportitems.LastChild;
            strMainBodyHi = BodyHight.InnerText;
            XmlNode heightbody = xmldoc.GetElementsByTagName("Body").Item(0).LastChild;
            foreach (XMLTxtCntl textLBox in pnlTemplate.Controls.OfType<XMLTxtCntl>())
            {

                CreateXMLTextBox(textLBox);
                strMain += sttemp;
            }
            foreach (XMLImgCntl ImgOnPnl in pnlTemplate.Controls.OfType<XMLImgCntl>())
            {
                CreateXMLImage(ImgOnPnl);
                strMain += sttemp;
            }
            strMainTabl = "";
            foreach (XMLTbCntl TblOnPnl in pnlTemplate.Controls.OfType<XMLTbCntl>())
            {

                CreateXMLTable(TblOnPnl);
                strMain += strMainTabl;
            }
            strListOnPnl = ""; sttemp = "";
            foreach (XMLLstCntl LstOnPnl in pnlTemplate.Controls.OfType<XMLLstCntl>())
            {
                strGrpTextBox = ""; sttemp = "";
                foreach (XMLTxtCntl TxtOnList in LstOnPnl.Controls.OfType<XMLTxtCntl>())
                {
                    CreateXMLTextBox(TxtOnList);
                    strGrpTextBox += sttemp;
                }
                strListOnList = ""; sttemp = "";
                foreach (XMLLstCntl LstOnList in LstOnPnl.Controls.OfType<XMLLstCntl>())
                {
                    strTxtLsL = ""; sttemp = "";
                    foreach (XMLTxtCntl LsOnLStxt in LstOnList.Controls.OfType<XMLTxtCntl>())
                    {
                        CreateXMLTextBox(LsOnLStxt);
                        strTxtLsL += sttemp;
                    }
                    string strtxtLL = "<ReportItems>" + strTxtLsL + "</ReportItems>";
                    strTxtName = LstOnList.Name;
                    dblLleft = LstOnList.Location.X;
                    dblLleft = Math.Round((dblLleft / 72), 5);
                    dblLtop = LstOnList.Location.Y;
                    dblLtop = Math.Round((dblLtop / 72), 5);
                    dblSwid = LstOnList.Width;
                    dblSwid = Math.Round((dblSwid / 72), 5);
                    dblSheight = LstOnList.Height;
                    dblSheight = Math.Round((dblSheight / 72), 5);
                    strLocLeft = dblLleft.ToString();
                    strLocTop = dblLtop.ToString();
                    strSizWid = dblSwid.ToString();
                    strSizHe = dblSheight.ToString();
                    strDatasetname = "";
                    strVisibility = "";
                    strBordelStyl = "";
                    strGrpExprsn = "";
                    if (LstOnList.strDataSetName != null)
                    {
                        strDatasetname = "<DataSetName>" + LstOnPnl.strDataSetName + "</DataSetName>";
                    }
                    if (LstOnList.strvisibilty != null)
                    {
                        strVisibility = "";
                        strVisibility = LstOnList.strvisibilty;

                        if (strVisibility.Contains(">"))
                        {
                            strVisibility = strVisibility.Replace(">", "&gt;");
                        }
                        if (strVisibility.Contains("<"))
                        {
                            strVisibility = strVisibility.Replace("<", "&lt;");
                        }
                        strVisibility = "<Visibility>" + "<Hidden>" + strVisibility + "</Hidden>" + "</Visibility>";
                    }

                    sttemp = "<List " + "Name=\"" + strTxtName + "\">" +
                            "<Top>" + strLocTop + "in</Top>" +
                          "<Width>" + strSizWid + "in</Width>" +
                              strDatasetname + strVisibility +
                                                    strtxtLL +
                                  "<Left>" + strLocLeft + "in</Left>" +
                                  "<Height>" + strSizHe + "in</Height>" +
                                                      "</List>";
                    strListOnList += sttemp;
                }

                strGrpPicturebox = ""; sttemp = "";
                foreach (XMLImgCntl ImgOnList in LstOnPnl.Controls.OfType<XMLImgCntl>())
                {
                    CreateXMLImage(ImgOnList);
                    strGrpPicturebox += sttemp;
                }
                strGrpDgv = ""; sttemp = "";
                foreach (XMLTbCntl Dgv in LstOnPnl.Controls.OfType<XMLTbCntl>())
                {
                    sttemp = "";
                    strTblXml = "";
                    strMainTabl = "";
                    if (LstOnPnl.Contains(Dgv))
                    {
                        int i = 0;
                        for (i = 0; i < Dgv.RowCount; i++)
                        {
                            sttemp = "";
                            stTblxmlCls = "";
                            for (int j = 0; j < Dgv.Rows[i].Cells.Count; j++)
                            {
                                strTxtName = Dgv.Rows[i].Cells[j].Value.ToString();
                                //strText = Dgv.Rows[i].Cells[j].Tag.ToString();
                                strText = Dgv.Rows[i].Cells[j].Tag.ToString();
                                if (strText.Contains("&"))
                                {
                                    strText = strText.Replace("&", "&amp;");
                                }
                                sttemp = "<TableCell>" +
                                       "<ReportItems>" + "<Textbox " + "Name=\"" + strTxtName + "\">" + "<rd:DefaultName>" + strTxtName +
                                 "</rd:DefaultName>" + "<Style>" + "<BorderStyle>" + "<Default>None</Default>" + "</BorderStyle>" + "<BorderWidth>" +
                               "<Default>0.3pt</Default>" + "</BorderWidth>" + "<FontFamily>Tahoma</FontFamily>" + "<FontSize>8pt</FontSize>"
                               + "<PaddingLeft>2pt</PaddingLeft>" + "<PaddingRight>2pt</PaddingRight>" + "<PaddingTop>2pt</PaddingTop>" +
                               "<PaddingBottom>2pt</PaddingBottom>" + "</Style>" + "<CanGrow>true</CanGrow>" + "<Value>" + strText + "</Value>" +
                               "</Textbox>" + "</ReportItems>" + "</TableCell>";
                                stTblxmlCls += sttemp;
                            }
                            strTblxmlRws = "<TableRows>" + "<TableRow>" + "<TableCells>" + stTblxmlCls + "</TableCells>" + "<Height>0.2in</Height> " +
                                "</TableRow>" + "</TableRows>";
                            if (i == 0)
                            {
                                strTblXml = "<Header>" + strTblxmlRws + "</Header>";
                            }
                            else
                            {
                                strTblXml += "<Footer>" + strTblxmlRws + "</Footer>";

                            }
                        }
                        strTxtName = Dgv.Name.ToString();
                        strGrpExprsn = "";
                        if (Dgv.strGroupExpressions != null)
                        {
                            strGrpExprsn = "<Grouping Name=\"" + Dgv.strGroupingName + "\">" + "<GroupExpressions>" +
                                " <GroupExpression>" + Dgv.strGroupExpressions + "</GroupExpression> " +
                                "</GroupExpressions>" + "</Grouping>";
                        }
                        strTblXml = "<TableGroups>" + "<TableGroup>" + strGrpExprsn +
                                     strTblXml +
                                "</TableGroup>" + "</TableGroups>";

                        strTblClnWid = "";
                        strMainTabl = "";
                        for (int k = 0; k < Dgv.ColumnCount; k++)
                        {
                            double dblCelwid = (Dgv.Columns[k].Width);
                            double newdblCelWid = Math.Round((dblCelwid / 72), 5);

                            strSizWid = newdblCelWid.ToString();
                            sttemp = "<TableColumn>" + "<Width>" + strSizWid + "in" + "</Width>" + "</TableColumn>";
                            strTblClnWid += sttemp;
                        }
                        strTxtName = Dgv.Name;
                        dblLleft = Dgv.Location.X;
                        dblLleft = Math.Round((dblLleft / 72), 5);
                        dblLtop = Dgv.Location.Y;
                        dblLtop = Math.Round((dblLtop / 72), 5);
                        dblSwid = Dgv.Width;
                        dblSwid = Math.Round((dblSwid / 72), 5);
                        dblSheight = Dgv.Height;
                        dblSheight = Math.Round((dblSheight / 72), 5);
                        strLocTop = dblLtop.ToString();
                        strSizWid = dblSwid.ToString();
                        strSizHe = dblSheight.ToString();
                        strLocLeft = dblLleft.ToString();
                        strDataSetName = "";
                        if (Dgv.strDataSetName != null)
                        {
                            strDataSetName = "<DataSetName>" + Dgv.strDataSetName + "</DataSetName>";
                        }
                        strMainTabl = "<Table " + "Name=\"" + strTxtName + "\">" + strDataSetName + "<Top>" + strLocTop + "in" + "</Top>" + strTblXml +
                        "<Width>" + strSizWid + "in" + "</Width> " + "<TableColumns>" + strTblClnWid + "</TableColumns>" + "<Height>0.4in</Height> " +
                        "<Left>0.01in</Left> " + " </Table>";
                    }
                    strGrpDgv += strMainTabl;
                }
                sttempgrp = "<ReportItems>" + strGrpTextBox + strGrpPicturebox + strGrpDgv + strListOnList + "</ReportItems>";
                strGrpDgv = "";
                dblLleft = LstOnPnl.Location.X;
                dblLleft = Math.Round((dblLleft / 72), 5);
                dblLtop = LstOnPnl.Location.Y;
                dblLtop = Math.Round((dblLtop / 72), 5);
                dblSwid = LstOnPnl.Width;
                dblSwid = Math.Round((dblSwid / 72), 5);
                dblSheight = LstOnPnl.Height;
                dblSheight = Math.Round((dblSheight / 72), 5);
                strLocLeft = dblLleft.ToString();
                strLocTop = dblLtop.ToString();
                strSizWid = dblSwid.ToString();
                strSizHe = dblSheight.ToString();
                strTxtName = LstOnPnl.Name;
                strDatasetname = "";
                strVisibility = "";
                strBordelStyl = "";
                strGrpExprsn = "";
                if (LstOnPnl.strDataSetName != null)
                {
                    strDatasetname = "<DataSetName>" + LstOnPnl.strDataSetName + "</DataSetName>";
                }
                if (LstOnPnl.strvisibilty != null)
                {
                    strVisibility = "";
                    strVisibility = LstOnPnl.strvisibilty;

                    if (strVisibility.Contains(">"))
                    {
                        strVisibility = strVisibility.Replace(">", "&gt;");
                    }
                    if (strVisibility.Contains("<"))
                    {
                        strVisibility = strVisibility.Replace("<", "&lt;");
                    }
                    strVisibility = "<Visibility>" + "<Hidden>" + strVisibility + "</Hidden>" + "</Visibility>";
                }
                if ((LstOnPnl.strBorderStyle) != null && ((LstOnPnl.strBorderStyle) != ""))
                {
                    strBordelStyl = "<BorderStyle>" + "<Default>Solid</Default>" + "</BorderStyle>";
                }
                if (LstOnPnl.strGroupingName != null)
                {
                    strGrpExprsn = "<Grouping Name=\"" + LstOnPnl.strGroupingName + "\">" + "<GroupExpressions>" +
                        "<GroupExpression>" + LstOnPnl.strGroupExpressions + "</GroupExpression>" + "</GroupExpressions>" + "</Grouping>";
                }
                sttemp = "<List " + "Name=\"" + strTxtName + "\">" +
                "<Top>" + strLocTop + "in</Top>" +
                "<Width>" + strSizWid + "in</Width>" + strDatasetname + strVisibility +
                                         sttempgrp +
                "<Style>" + strBordelStyl + "</Style>" + strGrpExprsn +
                "<Left>" + strLocLeft + "in</Left>" +
                "<Height>" + strSizHe + "in</Height>" +
                                  "</List>";
                strMain += sttemp;
            }
            string strNewMain = "";
            strNewMain = "<ReportItems>" + strMain + "</ReportItems>" + "<Height>" + strMainBodyHi + "</Height>";
            reportitems.InnerXml = strNewMain;

            switch (saveMode)
            {
                case 1:
                    {

                        clsBLLReportViewer MobjclsBLLReportViewer = new clsBLLReportViewer();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayPurchaseDetailsReport((int)OperationType.PurchaseOrder);
                        dtSet.DataSetName = "DtSetPurchase";
                        dtSet.Tables[0].TableName = "DtSetPurchase_PurchaseOrderMaster";
                        dtSet.Tables[1].TableName = "DtSetPurchase_PurchaseOrderDetails";
                        dtSet.Tables[2].TableName = "DtSetPurchase_STPurchaseOrderExpenseMaster";
                        dtSet.Tables[3].TableName = "DtSetPurchase_STDocumentMaster";
                        dtSet.Tables[4].TableName = "DtSetPurchase_STTermsAndConditions";

                        ChangeDataSetsInnerXML(dtSet, xmldoc);


                        break;
                    }
                case 2:
                    {

                        clsBLLReportViewer MobjclsBLLReportViewer = new clsBLLReportViewer();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayPurchaseDetailsReport((int)OperationType.PurchaseInvoice);
                        dtSet.DataSetName = "DtSetPurchase";
                        dtSet.Tables[0].TableName = "DtSetPurchase_PurchaseInvoiceMaster";
                        dtSet.Tables[1].TableName = "DtSetPurchase_PurchaseInvoiceDetails";
                        dtSet.Tables[2].TableName = "DtSetPurchase_STPurchaseInvoiceExpenseMaster";
                        dtSet.Tables[3].TableName = "DtSetPurchase_GRNExtraItems";
                        dtSet.Tables[4].TableName = "DtSetPurchase_STTermsAndConditions";
                        dtSet.Tables.RemoveAt(3);

                        ChangeDataSetsInnerXML(dtSet, xmldoc);


                        break;
                    }
                case 3:
                    {
                        int companyid = 0;

                        clsBLLReportViewer MobjclsBLLReportViewer = new clsBLLReportViewer();
                        //DataSet dtSet = MobjclsBLLReportViewer.DisplaySalesDetailReport(2);
                        DataSet dtSet = MobjclsBLLReportViewer.DisplaySalesDetailReport(2);
                        DataTable DTCompany = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid).Tables[0].Copy();
                        dtSet.DataSetName = "DatasetSales";
                        dtSet.Tables[0].TableName = "DatasetSales_SalesOrderMaster";
                        dtSet.Tables[1].TableName = "DatasetSales_SalesOrderDetails";
                        dtSet.Tables[2].TableName = "DatasetSales_SalesOrderExpenseDetails";
                        dtSet.Tables[3].TableName = "DatasetSales_STTermsAndConditions";
                        dtSet.Tables.Add(DTCompany);
                        dtSet.Tables[dtSet.Tables.Count - 1].TableName = "DtSetCompanyFormReport_CompanyHeader";

                        ChangeDataSetsInnerXML(dtSet, xmldoc);
                        break;
                    }
                case 4:
                    {
                        int companyid = 0;

                        clsBLLReportViewer MobjclsBLLReportViewer = new clsBLLReportViewer();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplaySalesDetailReport(3);
                        DataTable DTCompany = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid).Tables[0].Copy();
                        dtSet.Tables[0].TableName = "DatasetSales_SalesInvoiceMaster";
                        dtSet.Tables[1].TableName = "DatasetSales_SalesInvoicenDetails";
                        dtSet.Tables[2].TableName = "DatasetSales_SalesInvoiceExpenseDetails";
                        dtSet.Tables[3].TableName = "DatasetSales_STTermsAndConditions";
                        dtSet.Tables.Add(DTCompany);
                        dtSet.Tables[dtSet.Tables.Count - 1].TableName = "DtSetCompanyFormReport_CompanyHeader";

                        ChangeDataSetsInnerXML(dtSet, xmldoc);

                        break;
                    }
            }

            xmldoc.Save(@path);
            MessageBox.Show("Template saved");
            string strTempTemplateName = txtTemplateName.Text;
            loadcombo();

            DataTable datTemp = (DataTable)cboSampleTemplates.ComboBox.DataSource;
            datTemp.DefaultView.RowFilter = "Template='" + strTempTemplateName + "'";
            int iTempID = Convert.ToInt32(datTemp.DefaultView.ToTable().Rows[0]["TemplateID"].ToString());
            loadcombo();
            cboSampleTemplates.ComboBox.SelectedValue = iTempID;

            txtTemplateName.Clear();
        }


        //To Create XML TextBox
        private void CreateXMLTextBox(XMLTxtCntl textLBox)
        {
            strTxtName = textLBox.Name;
            dblLleft = textLBox.Location.X;
            dblLleft = Math.Round((dblLleft / 72), 5);
            dblLtop = textLBox.Location.Y;
            dblLtop = Math.Round((dblLtop / 72), 5);
            dblSwid = textLBox.Width;
            dblSwid = Math.Round((dblSwid / 72), 5);
            dblSheight = textLBox.Height;
            dblSheight = Math.Round((dblSheight / 72), 5);
            strLocLeft = dblLleft.ToString();
            strLocTop = dblLtop.ToString();
            strSizWid = dblSwid.ToString();
            strSizHe = dblSheight.ToString();

            strTxtTextAlgn = "";
            strVisibility = "";
            strBgColor = "";
            strDefultName = "";
            strFntWeight = "";
            strBordelStyl = "";
            strValue = "";

            if (textLBox.strvisibilty != null)
            {

                strVisibility = textLBox.strvisibilty;

                if (strVisibility.Contains(">"))
                {
                    strVisibility = strVisibility.Replace(">", "&gt;");
                }
                if (strVisibility.Contains("<"))
                {
                    strVisibility = strVisibility.Replace("<", "&lt;");
                }
                strVisibility = "<Visibility>" + "<Hidden>" + strVisibility + "</Hidden>" + "</Visibility>";
            }
            if ((textLBox.strBgColor) != null && textLBox.strBgColor == "LightGrey")
            {
                strBgColor = "<BackgroundColor>" + textLBox.strBgColor + "</BackgroundColor>";
            }
            else
            {
                strBgColor = "";
            }
            if (textLBox.TextAlign == HorizontalAlignment.Center)
            {
                strTxtTextAlgn = "<TextAlign>Center</TextAlign>";
            }
            if (textLBox.TextAlign == HorizontalAlignment.Right)
            {
                strTxtTextAlgn = "<TextAlign>Right</TextAlign>";
            }

            if (textLBox.TextAlign == HorizontalAlignment.Left)
            {
                strTxtTextAlgn = "<TextAlign>Left</TextAlign>";
            }
            if (textLBox.strDefaultName != null)
            {
                strDefultName = "<rd:DefaultName>" + textLBox.strDefaultName + "</rd:DefaultName>";
            }
            if ((textLBox.strBorderStyle != null) && ((textLBox.strBorderStyle) != ""))
            {
                strBordelStyl = "<BorderStyle>" + "<Default>" + textLBox.strBorderStyle + "</Default>" + "</BorderStyle>";
            }
            if ((textLBox.strBorderWidth != null) && (textLBox.strBorderWidth != ""))
            {
                strBorderWidth = "<BorderWidth>" + "<Default>" + textLBox.strBorderWidth + "</Default>" + "</BorderWidth>";
            }
            if ((textLBox.strFontSize != null) && (textLBox.strFontSize != ""))
            {
                strFntSize = "<FontSize>" + textLBox.strFontSize + "</FontSize>";
            }
            if (textLBox.intFontWeight > 0)
            {
                strFntWeight = "<FontWeight>" + textLBox.intFontWeight.ToString() + "</FontWeight> ";
            }
            if (textLBox.strvalue == "" || textLBox.strvalue == null)
            {
                strValue = textLBox.Text;
            }
            else if ((textLBox.strvalue) != null && ((textLBox.strvalue) != ""))
            {
                strValue = "";

                strValue = textLBox.strvalue;
                if (strValue.Contains("&"))
                {
                    strValue = strValue.Replace("&", "&amp;");
                }
                if (strValue.Contains("<"))
                {
                    strValue = strValue.Replace("<", "&lt;");
                }
                if (strValue.Contains(">"))
                {
                    strValue = strValue.Replace(">", "&gt;");
                }
                
            }
            if (strValue == "")
            {
                strValue = textLBox.Text;
            }
            sttemp = "<Textbox " + "Name=\"" + strTxtName + "\">" +
                 strDefultName + strVisibility +
               "<Top>" + strLocTop + "in</Top>" +
               "<Width>" + strSizWid + "in</Width>" +
               "<Style>" + strBgColor + strBordelStyl + strBorderWidth +
               "<FontFamily>Tahoma</FontFamily>" +
                strFntSize + strFntWeight + strTxtTextAlgn +
               "<PaddingLeft>2pt</PaddingLeft>" +
               "<PaddingRight>2pt</PaddingRight>" +
               "<PaddingTop>2pt</PaddingTop>" +
               "<PaddingBottom>2pt</PaddingBottom>" +
               "</Style>" +
               "<CanGrow>true</CanGrow> " +
               "<Left>" + strLocLeft + "in</Left>" +
               "<Height>" + strSizHe + "in</Height>" +
               "<Value>" + strValue + "</Value>" +
                                  "</Textbox>";
        }
        //To Create XML Image
        private void CreateXMLImage(XMLImgCntl pictureBox)
        {
            dblLleft = pictureBox.Location.X;
            dblLleft = Math.Round((dblLleft / 72), 5);
            dblLtop = pictureBox.Location.Y;
            dblLtop = Math.Round((dblLtop / 72), 5);
            dblSwid = pictureBox.Width;
            dblSwid = Math.Round((dblSwid / 72), 5);
            dblSheight = pictureBox.Height;
            dblSheight = Math.Round((dblSheight / 72), 5);
            strLocLeft = dblLleft.ToString();
            strLocTop = dblLtop.ToString();
            strSizWid = dblSwid.ToString();
            strSizHe = dblSheight.ToString();
            strTxtName = pictureBox.Name;
            sttemp = "<Image " + "Name=\"" + strTxtName + "\">" +
                            "<Sizing>FitProportional</Sizing> " +
                            "<Width>" + strSizWid + "in</Width>" +
                            "<Top>" + strLocTop + "in</Top>" +
                            "<MIMEType>image/jpeg</MIMEType>" +
                            "<Source>Database</Source>" +
                            "<Style /> " +
                            "<Left>" + strLocLeft + "in</Left>" +
                            "<Height>" + strSizHe + "in</Height>" +
                            "<Value>=Fields!LogoFile.Value</Value>" +
                            "</Image>";
        }
        //To Create XML List
        private void CreateXMLList(XMLLstCntl groupBox)
        {
            sttemp = "<Rectangle " + "Name=\"" + strTxtName + "\">" +
                 "<Left>" + strLocLeft + "pt</Left>" +
                  
                  "<Height>" + strSizHe + "pt</Height>" +
                  "<Top>" + strLocTop + "pt</Top>" +
                  "<Width>" + strSizWid + "pt</Width>" +
                  "</Rectangle>";
        }
        //To create XML Table on panel
        private void CreateXMLTable(XMLTbCntl datagrid)
        {
            sttemp = "";
            strTblXml = "";
            strSizHe = datagrid.Height.ToString();
            int i = 0;
            for (i = 0; i < datagrid.RowCount; i++)
            {
                for (int j = 0; j < datagrid.Rows[i].Cells.Count; j++)
                {
                    strTxtName = datagrid.Columns[j].Name + "Dtls";
                    strText = datagrid.Rows[i].Cells[j].Tag.ToString();
                    if (strText.Contains("&"))
                    {
                        strText = strText.Replace("&", "&amp;");
                    }
                    dblRowHeight = (datagrid.Rows[i].Height);
                    double newDdRowHeight = Math.Round((dblRowHeight / 72), 5);

                    strSizHe = newDdRowHeight.ToString();
                    sttemp = "<TableCell>" +
                                           "<ReportItems>" +
                                               "<Textbox " + "Name=\"" + strTxtName + "\">" + "<rd:DefaultName>" + strTxtName + "</rd:DefaultName>" +
                                                 "<Style>" + "<BorderStyle>" + "<Default>Solid</Default>" + "</BorderStyle>" +
                                           "<BorderWidth>" + "<Default>0.5pt</Default>" + "</BorderWidth>" + "<FontFamily>Tahoma</FontFamily>" +
                         "<FontSize>8pt</FontSize>" + "<TextAlign>Left</TextAlign>" + "<PaddingLeft>2pt</PaddingLeft>" + "<PaddingRight>2pt</PaddingRight>" +
                           "<PaddingTop>2pt</PaddingTop>" +
                      "<PaddingBottom>2pt</PaddingBottom>" + "</Style>" + "<CanGrow>true</CanGrow>" + "<Value>" + strText + "</Value>" +
                                              "</Textbox>" +
                                          "</ReportItems>" +
                                             "</TableCell>";
                    strTblXml += sttemp;
                }
            }
            string strTbRowHead;
            strTbRowHead = "<Details>" + "<TableRows>" + "<TableRow>" + "<TableCells>" + strTblXml +
                "</TableCells>" + "<Height>" + strSizHe + "in" + "</Height>" + "</TableRow>" + "</TableRows>" +
                "</Details>";
            //string strRowHedStyl;
            strRowHedStyl = strTbRowHead + "<Style>" + "<BorderStyle>" + "<Default>Solid</Default>" + "</BorderStyle>" + "<BorderWidth>" +
                "<Default>0.5pt</Default>" + "</BorderWidth>" + "<FontFamily>Tahoma</FontFamily>" + "</Style>";
            strTblHeadXml = "";
            for (int k = 0; k < datagrid.Columns.Count; k++)
            {
                strTxtName = datagrid.Columns[k].Name;
                strText = datagrid.Columns[k].HeaderText;
                sttemp = "<TableCell>" +
                                          "<ReportItems>" +
                                              "<Textbox " + "Name=\"" + strTxtName +
                                                    "\">" +
                                              "<Style>" +
                                        "<BorderStyle>" +
                             "<Default>Solid</Default>" +
                             "</BorderStyle>" +
                             "<BorderWidth>" +
                             "<Default>0.5pt</Default>" +
                             "</BorderWidth>" +
                             "<FontFamily>Tahoma</FontFamily>" +
                             "<FontSize>8pt</FontSize>" +
                             "<FontWeight>700</FontWeight>" +
                             "<PaddingLeft>2pt</PaddingLeft>" +
                             "<PaddingRight>2pt</PaddingRight>" +
                             "<PaddingTop>2pt</PaddingTop>" +
                             "<PaddingBottom>2pt</PaddingBottom>" +
                             "</Style>" +
                             "<CanGrow>true</CanGrow>" +
                             "<Value>" + strText + "</Value>" + "</Textbox>" +
                             "</ReportItems>" +
                             "</TableCell>";

                strTblHeadXml += sttemp;
            }
            strTxtName = datagrid.Name;
            strLocLeft = datagrid.Location.X.ToString();
            //strLocTop  = datagrid.Location.Y.ToString();
            double dblTblTop = datagrid.Location.Y;
            double newdblTblTop = Math.Round((dblTblTop / 72), 5);
            strSizWid = datagrid.Width.ToString();
            strTblClnWid = "";
            strMainTabl = "";
            for (int l = 0; l < datagrid.Columns.Count; l++)
            {
                double dblCelwid = (datagrid.Columns[l].Width);
                double newdblCelWid = Math.Round((dblCelwid / 72), 5);

                strSizWid = newdblCelWid.ToString();
                strVisibility = "";
                if (datagrid.Columns[l].Visible == false)
                {
                    strVisibility = "<Visibility>" + "<Hidden>true</Hidden>" + "</Visibility>";
                }
                else
                {
                    strVisibility = "";
                }
                sttemp = "<TableColumn>" + strVisibility + "<Width>" + strSizWid + "in" + "</Width>" + "</TableColumn>";
                strTblClnWid += sttemp;
            }
            string strTbHead;
            strTbHead = "<Header>" + "<TableRows>" + "<TableRow>" + "<TableCells>" + strTblHeadXml + "</TableCells>" +
                "<Height>0.25in</Height>" +
                "</TableRow>" +
                "</TableRows>" +
                "</Header>";
            dblSheight = datagrid.Height;
            dblSheight = Math.Round((dblSheight / 72), 5);
            dblSwid = datagrid.Width;
            dblSwid = Math.Round((dblSwid / 72), 5);
            strSizWid = dblSwid.ToString();
            strSizHe = dblSheight.ToString();
            strVisibility = "";
            strDatasetname = "";
            if (datagrid.strvisibilty != null)
            {
                strVisibility = "<Visibility>" + "<Hidden>" + datagrid.strvisibilty + "</Hidden>" + "</Visibility>";
            }
            if (datagrid.strDataSetName != null)
            {
                strDatasetname = "<DataSetName>" + datagrid.strDataSetName + "</DataSetName>";

            }
            strMainTabl += "<Table " + "Name=\"" + strTxtName + "\">" + strDatasetname + strVisibility
                 + "<Top>" + newdblTblTop + "in" + "</Top>" + "<Width>" + strSizWid + "in</Width>" +
                strRowHedStyl + strTbHead + "<TableColumns>" + strTblClnWid + "</TableColumns>" + "<Height>" + strSizHe + "in" + "</Height>" + "<Left>0in</Left>" +
                "</Table>";

        }
        //To do:ChangeDataSetsInnerXML and copying new datasets to RDLC xml
        public void ChangeDataSetsInnerXML(DataSet dtSet, XmlDocument xmldoc)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(dtSet.GetXmlSchema());
            string physicalXslt = (@"CustomReports\\RDLCStyle.xslt");
            string xmlSchema = TransformXML(xd, physicalXslt);
            XmlDocument xN = new XmlDocument();
            xN.LoadXml(xmlSchema);
            XmlNode xNnode = xN.LastChild.FirstChild;
            XmlNode xDatasetnode = xmldoc.GetElementsByTagName("DataSets").Item(0);

            xDatasetnode.InnerXml = xNnode.InnerXml;
        }
        //To do :trasform a scheama file to RDLC xlm formate using xslt
        public static string TransformXML(XmlDocument  xml, string xsfile)
        {
            string result = string.Empty;
            {
                XslCompiledTransform transform = new XslCompiledTransform();
                transform.Load(xsfile);
                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                //transform.Transform(@"CustomReports\\sample.xml", "@CustomReports\\sampleText.xml");
                transform.Transform(xml, null,sw );
                result = sw.ToString();
            }
            return result;
        }


        private void tbTemplateDesign_SelectedIndexChanged(object sender, EventArgs e)
        {
            //tabPageDesign
            if (tbTemplateDesign.SelectedTab== tabPageDesign )
            {
                tlstrpBtnShowPreview.Visible = false;
                txtTemplateName.Visible = true;
                tlstrpbtnSave.Visible = true;
                toolStripButton1.Visible = true;
                tlstrpBtnDelete.Visible = true;
            }
             //tbPagePreview
            else if (tbTemplateDesign.SelectedTab == tabPagePreview)
            {
                toolStripButton1.Visible = false;
                txtTemplateName.Visible = false;
                tlstrpbtnSave.Visible = false;
                tlstriplblTmpname.Visible = false;
                tlstrpBtnShowPreview.Visible = true;
                tlstrpBtnDelete.Visible = false;
                if (pnlAllTemplate.Controls.Contains(pnlTemplate))
                {
                    ShowReprot();
                }
                
            }
        }
        private void ShowReprot()
        {
            if (cboOperationTypes.ComboBox.Text == "--Select--")
            {
                CustomRptViewer.Clear();
                CustomRptViewer.Reset();
            }
            else if (cboOperationTypes.ComboBox.Text == "PurchaseOrder")
            {

                    CustomRptViewer.Clear();
                    CustomRptViewer.Reset();
                    sAppPath = Application.StartupPath + "\\CustomReports\\" + cboSampleTemplates.ComboBox.Text + "RptPo" + ".rdlc";
                    CustomRptViewer.LocalReport.ReportPath = sAppPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", "PsReportFooter", false);
                    CustomRptViewer.LocalReport.SetParameters(ReportParameters);
                    CustomRptViewer.LocalReport.DataSources.Clear();
                    CustomRptViewer.DocumentMapCollapsed = true;
                    string strPermittedCompany = null;
                    clsBLLMISPurchase MobjclsBLLMISPurchase = new clsBLLMISPurchase();
                    DataSet dtsetPurchaseOrder = MobjclsBLLMISPurchase.DisplayPurchaseOrder(0, 0, "26 Dec 2011", "26 Dec 2011", 0, 3, 0, 49, 0, 0, 0, 0, strPermittedCompany);
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", dtsetPurchaseOrder.Tables[0]));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", dtsetPurchaseOrder.Tables[1]));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster", dtsetPurchaseOrder.Tables[2]));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", dtsetPurchaseOrder.Tables[3]));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", dtsetPurchaseOrder.Tables[4]));



                    CustomRptViewer.SetDisplayMode(DisplayMode.PrintLayout);
                    CustomRptViewer.ZoomMode = ZoomMode.Percent;
                    this.Cursor = Cursors.Default;

            }
            else if (cboOperationTypes.ComboBox.Text == "SalesOrder")
            {

                    CustomRptViewer.Clear();
                    CustomRptViewer.Reset();
                    sAppPath = Application.StartupPath + "\\CustomReports\\" + cboSampleTemplates.ComboBox.Text + "RptSo" + ".rdlc";
                    CustomRptViewer.LocalReport.ReportPath = sAppPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", "PsReportFooter", false);
                    CustomRptViewer.LocalReport.SetParameters(ReportParameters);
                    CustomRptViewer.LocalReport.DataSources.Clear();
                    string strPermittedCompany = null;
                    clsBLLSales MobjclsBLLSales = new clsBLLSales();
                    DataSet dtSet = MobjclsBLLSales.DisplaySalesOrder(0, 0, "26 Dec 2011", "26 Dec 2011", 8, 0, 148, 0, 0, 0, strPermittedCompany);
                    int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                    DataTable DTCompany = MobjclsBLLSales.DisplayCompanyHeaderReport(companyid);
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderMaster", dtSet.Tables[0]));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderDetails", dtSet.Tables[1]));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderExpenseDetails", dtSet.Tables[2]));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", dtSet.Tables[3]));

                    CustomRptViewer.SetDisplayMode(DisplayMode.PrintLayout);
                    CustomRptViewer.ZoomMode = ZoomMode.Percent;
                    this.Cursor = Cursors.Default;
 

            }
            else if (cboOperationTypes.ComboBox.Text == "PurchaseInvoice")
            {

                    CustomRptViewer.Clear();
                    CustomRptViewer.Reset();
                    sAppPath = Application.StartupPath + "\\CustomReports\\" + cboSampleTemplates.ComboBox.Text + "RptPi" + ".rdlc";
                    CustomRptViewer.LocalReport.ReportPath = sAppPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", "PsReportFooter", false);
                    CustomRptViewer.LocalReport.SetParameters(ReportParameters);
                    CustomRptViewer.LocalReport.DataSources.Clear();
                    string strPermittedCompany = null;
                    clsBLLMISPurchase MobjclsBLLMISPurchase = new clsBLLMISPurchase();
                    DataSet dtsetPurchaseInvoice = MobjclsBLLMISPurchase.DisplayPurchaseInvoice(0, 0, "26 Dec 2011", "26 Dec 2011", 0, 5, 0, 0, 228, 0, 0, 0, 0, strPermittedCompany);
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", dtsetPurchaseInvoice.Tables[0]));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", dtsetPurchaseInvoice.Tables[1]));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", dtsetPurchaseInvoice.Tables[2]));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", dtsetPurchaseInvoice.Tables[3]));


                    CustomRptViewer.SetDisplayMode(DisplayMode.PrintLayout);
                    CustomRptViewer.ZoomMode = ZoomMode.Percent;
                    this.Cursor = Cursors.Default;
 
            }
            else if (cboOperationTypes.ComboBox.Text == "SalesInvoice")
            {

                    CustomRptViewer.Clear();
                    CustomRptViewer.Reset();
                    sAppPath = Application.StartupPath + "\\CustomReports\\" + cboSampleTemplates.ComboBox.Text + "RptSi" + ".rdlc";
                    CustomRptViewer.LocalReport.ReportPath = sAppPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", "PsReportFooter", false);
                    CustomRptViewer.LocalReport.SetParameters(ReportParameters);
                    CustomRptViewer.LocalReport.DataSources.Clear();
                    string strPermittedCompany = null;
                    clsBLLSales MobjclsBLLSales = new clsBLLSales();
                    DataSet dtSet = MobjclsBLLSales.DisplaySalesInvoice(0, 0, "26 Dec 2011", "26 Dec 2011", 9, 0, 127, 148, 0, 0, 0, strPermittedCompany);
                    int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                    DataTable DTCompany = MobjclsBLLSales.DisplayCompanyHeaderReport(companyid);
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceMaster", dtSet.Tables[0]));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoicenDetails", dtSet.Tables[1]));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceExpenseDetails", dtSet.Tables[2]));
                    CustomRptViewer.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", dtSet.Tables[3]));

                    CustomRptViewer.SetDisplayMode(DisplayMode.PrintLayout);
                    CustomRptViewer.ZoomMode = ZoomMode.Percent;
                    this.Cursor = Cursors.Default;


            }
            //CustomRptViewer.RefreshReport();
            //CustomRptViewer.Refresh();
            //CustomRptViewer.SetDisplayMode(DisplayMode.PrintLayout);
            //CustomRptViewer.ZoomMode = ZoomMode.Percent;
            //this.Cursor = Cursors.Default;
        }

        private void txtTemplateName_TextChanged(object sender, EventArgs e)
        {
            ErrorTemplate.Clear();
        }

        private void tlstrpBtnShowPreview_Click(object sender, EventArgs e)
        {
            ShowReprot();
        }

        private void tlstrpBtnDelete_Click(object sender, EventArgs e)
        {
            string strFilePath = null;
            if (cboOperationTypes.ComboBox.Text == "--Select--")
            {
                strFilePath = null;
            }

            else if (cboOperationTypes.ComboBox.Text == "PurchaseOrder")
            {
                
                strFilePath = "CustomReports\\" + cboSampleTemplates.ComboBox.Text + "RptPo" + ".rdlc";

            }
            else if (cboOperationTypes.ComboBox.Text == "PurchaseInvoice")
            {
                strFilePath = "CustomReports\\" + cboSampleTemplates.ComboBox.Text + "RptPi" + ".rdlc";

            }

            else if (cboOperationTypes.ComboBox.Text == "SalesOrder")
            {
                strFilePath = "CustomReports\\" + cboSampleTemplates.ComboBox.Text + "RptSo" + ".rdlc";

            }
            else if (cboOperationTypes.ComboBox.Text == "SalesInvoice")
            {
                strFilePath = "CustomReports\\" + cboSampleTemplates.ComboBox.Text + "RptSi" + ".rdlc";

            }
            if (strFilePath != null)
            {
                DeleteTemplate(strFilePath);
            }
           
        }

        private void DeleteTemplate(string strFilePath)
        {
            if (pnlAllTemplate.Controls.Contains(pnlTemplate))
            {
                if (File.Exists(@strFilePath))
                {
                    if (strFilePath.Contains("PurchaseOrderRptPo") || strFilePath.Contains("PurchaseInvoiceRptPi") || strFilePath.Contains("SalesInvoiceRptSi") || strFilePath.Contains("SalesOrderRptSo"))
                    {
                        MessageBox.Show("Unable to Delete default Template", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {

                        if (MessageBox.Show("Are you sure you want to delete this template?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            File.Delete(@strFilePath);
                            pnlAllTemplate.Controls.Clear();
                            loadcombo();
                        }
                        else
                        {

                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Show a template");
            }

            


        }



    }
}

        

     

