﻿namespace MyBooksERP
{
    partial class FrmInvoiceTemplateDesign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("TextBox");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("GroupBox");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Common Controls", new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode5});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInvoiceTemplateDesign));
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource4 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtNw = new System.Windows.Forms.TextBox();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dELETEToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip4 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteTextBoxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel30 = new System.Windows.Forms.Panel();
            this.dataGridView8 = new System.Windows.Forms.DataGridView();
            this.panel33 = new System.Windows.Forms.Panel();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.panel32 = new System.Windows.Forms.Panel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.panel31 = new System.Windows.Forms.Panel();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.panel26 = new System.Windows.Forms.Panel();
            this.dataGridView7 = new System.Windows.Forms.DataGridView();
            this.panel29 = new System.Windows.Forms.Panel();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.panel28 = new System.Windows.Forms.Panel();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.panel27 = new System.Windows.Forms.Panel();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.panel25 = new System.Windows.Forms.Panel();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.pnlSalesInvoicetemplate2 = new System.Windows.Forms.Panel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtthanks = new System.Windows.Forms.TextBox();
            this.txtAdditionalfield1 = new System.Windows.Forms.TextBox();
            this.txtadditionalfieldbottom = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.grpboxbillto1 = new System.Windows.Forms.GroupBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.txtInvoiceNo1 = new System.Windows.Forms.TextBox();
            this.txtInvoiceDate1 = new System.Windows.Forms.TextBox();
            this.txtAdditionalfieldTop = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtCompanyaddress1 = new System.Windows.Forms.TextBox();
            this.txtYourcompanyname = new System.Windows.Forms.TextBox();
            this.txtInvoice1 = new System.Windows.Forms.TextBox();
            this.pnlInvoiceTemp4 = new System.Windows.Forms.Panel();
            this.chkQty = new System.Windows.Forms.CheckBox();
            this.chkRate = new System.Windows.Forms.CheckBox();
            this.chkDiscount = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tlstrplblOperationType = new System.Windows.Forms.ToolStripLabel();
            this.cboOperationTypes = new System.Windows.Forms.ToolStripComboBox();
            this.tlstrplblw1 = new System.Windows.Forms.ToolStripLabel();
            this.tlstrplblSampleTemplate = new System.Windows.Forms.ToolStripLabel();
            this.cboSampleTemplates = new System.Windows.Forms.ToolStripComboBox();
            this.tlstrplblw = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.tlstrpBtnShowPreview = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tlstriplblTmpname = new System.Windows.Forms.ToolStripLabel();
            this.txtTemplateName = new System.Windows.Forms.ToolStripTextBox();
            this.tlstrpLbl = new System.Windows.Forms.ToolStripLabel();
            this.tlstrpbtnSave = new System.Windows.Forms.ToolStripButton();
            this.tlstrpBtnDelete = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbTemplateDesign = new System.Windows.Forms.TabControl();
            this.tabPageDesign = new System.Windows.Forms.TabPage();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.navigationPane1 = new DevComponents.DotNetBar.NavigationPane();
            this.navigationPanePanel3 = new DevComponents.DotNetBar.NavigationPanePanel();
            this.tvToolbox = new System.Windows.Forms.TreeView();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.navigationPanePanel1 = new DevComponents.DotNetBar.NavigationPanePanel();
            this.tvMasterDetails = new System.Windows.Forms.TreeView();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.navigationPanePanel2 = new DevComponents.DotNetBar.NavigationPanePanel();
            this.cblMasterDetails = new System.Windows.Forms.CheckedListBox();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.pnlAlltemplateDesign = new System.Windows.Forms.Panel();
            this.pnlAllTemplate = new System.Windows.Forms.Panel();
            this.tabPagePreview = new System.Windows.Forms.TabPage();
            this.CustomRptViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.tlpTextboxContent = new System.Windows.Forms.ToolTip(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTotal1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUnit1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDiscount1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColQty1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDescription1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColItemCode1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ErrorTemplate = new System.Windows.Forms.ErrorProvider(this.components);
            this.contextMenuStrip2.SuspendLayout();
            this.contextMenuStrip4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tbTemplateDesign.SuspendLayout();
            this.tabPageDesign.SuspendLayout();
            this.panel2.SuspendLayout();
            this.navigationPane1.SuspendLayout();
            this.navigationPanePanel3.SuspendLayout();
            this.navigationPanePanel1.SuspendLayout();
            this.navigationPanePanel2.SuspendLayout();
            this.pnlAlltemplateDesign.SuspendLayout();
            this.tabPagePreview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNw
            // 
            this.txtNw.Location = new System.Drawing.Point(0, 0);
            this.txtNw.Name = "txtNw";
            this.txtNw.Size = new System.Drawing.Size(100, 20);
            this.txtNw.TabIndex = 0;
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dELETEToolStripMenuItem1});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(114, 26);
            // 
            // dELETEToolStripMenuItem1
            // 
            this.dELETEToolStripMenuItem1.Name = "dELETEToolStripMenuItem1";
            this.dELETEToolStripMenuItem1.Size = new System.Drawing.Size(113, 22);
            this.dELETEToolStripMenuItem1.Text = "DELETE";
            this.dELETEToolStripMenuItem1.Click += new System.EventHandler(this.dELETEToolStripMenuItem1_Click);
            // 
            // contextMenuStrip4
            // 
            this.contextMenuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteTextBoxToolStripMenuItem});
            this.contextMenuStrip4.Name = "contextMenuStrip4";
            this.contextMenuStrip4.Size = new System.Drawing.Size(114, 26);
            // 
            // deleteTextBoxToolStripMenuItem
            // 
            this.deleteTextBoxToolStripMenuItem.Name = "deleteTextBoxToolStripMenuItem";
            this.deleteTextBoxToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.deleteTextBoxToolStripMenuItem.Text = "DELETE";
            this.deleteTextBoxToolStripMenuItem.Click += new System.EventHandler(this.deleteTextBoxToolStripMenuItem_Click);
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.White;
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel30.Location = new System.Drawing.Point(17, 6);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(688, 405);
            this.panel30.TabIndex = 52;
            // 
            // dataGridView8
            // 
            this.dataGridView8.AllowUserToDeleteRows = false;
            this.dataGridView8.AllowUserToResizeColumns = false;
            this.dataGridView8.AllowUserToResizeRows = false;
            this.dataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView8.Location = new System.Drawing.Point(-1, 165);
            this.dataGridView8.Name = "dataGridView8";
            this.dataGridView8.ReadOnly = true;
            this.dataGridView8.RowHeadersVisible = false;
            this.dataGridView8.Size = new System.Drawing.Size(688, 44);
            this.dataGridView8.TabIndex = 1;
            // 
            // panel33
            // 
            this.panel33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel33.Location = new System.Drawing.Point(-1, 254);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(688, 150);
            this.panel33.TabIndex = 2;
            // 
            // textBox64
            // 
            this.textBox64.Location = new System.Drawing.Point(268, 125);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(259, 20);
            this.textBox64.TabIndex = 0;
            this.textBox64.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox63
            // 
            this.textBox63.Location = new System.Drawing.Point(3, 99);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(366, 20);
            this.textBox63.TabIndex = 1;
            // 
            // textBox62
            // 
            this.textBox62.Location = new System.Drawing.Point(279, 20);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(208, 20);
            this.textBox62.TabIndex = 2;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(520, 210);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(175, 13);
            this.label31.TabIndex = 40;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(519, 223);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(171, 13);
            this.label30.TabIndex = 41;
            // 
            // panel32
            // 
            this.panel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel32.Location = new System.Drawing.Point(-1, 89);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(688, 79);
            this.panel32.TabIndex = 0;
            // 
            // groupBox8
            // 
            this.groupBox8.ForeColor = System.Drawing.Color.Green;
            this.groupBox8.Location = new System.Drawing.Point(1, -1);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(215, 60);
            this.groupBox8.TabIndex = 22;
            this.groupBox8.TabStop = false;
            // 
            // textBox61
            // 
            this.textBox61.Location = new System.Drawing.Point(8, 14);
            this.textBox61.Multiline = true;
            this.textBox61.Name = "textBox61";
            this.textBox61.ReadOnly = true;
            this.textBox61.Size = new System.Drawing.Size(166, 37);
            this.textBox61.TabIndex = 17;
            // 
            // textBox60
            // 
            this.textBox60.ForeColor = System.Drawing.Color.Green;
            this.textBox60.Location = new System.Drawing.Point(461, 3);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(185, 20);
            this.textBox60.TabIndex = 36;
            // 
            // textBox59
            // 
            this.textBox59.ForeColor = System.Drawing.Color.Green;
            this.textBox59.Location = new System.Drawing.Point(461, 23);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(185, 20);
            this.textBox59.TabIndex = 37;
            // 
            // textBox58
            // 
            this.textBox58.Location = new System.Drawing.Point(222, 12);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(163, 20);
            this.textBox58.TabIndex = 38;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(519, 236);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(174, 13);
            this.label29.TabIndex = 42;
            // 
            // panel31
            // 
            this.panel31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel31.Location = new System.Drawing.Point(-1, -1);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(688, 88);
            this.panel31.TabIndex = 39;
            // 
            // textBox57
            // 
            this.textBox57.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox57.Location = new System.Drawing.Point(4, 29);
            this.textBox57.Multiline = true;
            this.textBox57.Name = "textBox57";
            this.textBox57.ReadOnly = true;
            this.textBox57.Size = new System.Drawing.Size(215, 39);
            this.textBox57.TabIndex = 34;
            // 
            // textBox56
            // 
            this.textBox56.Location = new System.Drawing.Point(3, 3);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(211, 20);
            this.textBox56.TabIndex = 23;
            this.textBox56.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox55
            // 
            this.textBox55.Location = new System.Drawing.Point(269, 0);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(186, 20);
            this.textBox55.TabIndex = 35;
            this.textBox55.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.White;
            this.panel26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel26.Location = new System.Drawing.Point(19, 3);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(696, 404);
            this.panel26.TabIndex = 50;
            // 
            // dataGridView7
            // 
            this.dataGridView7.AllowUserToDeleteRows = false;
            this.dataGridView7.AllowUserToResizeColumns = false;
            this.dataGridView7.AllowUserToResizeRows = false;
            this.dataGridView7.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView7.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView7.ColumnHeadersHeight = 25;
            this.dataGridView7.Location = new System.Drawing.Point(-1, 149);
            this.dataGridView7.Name = "dataGridView7";
            this.dataGridView7.ReadOnly = true;
            this.dataGridView7.RowHeadersVisible = false;
            this.dataGridView7.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dataGridView7.Size = new System.Drawing.Size(696, 48);
            this.dataGridView7.TabIndex = 25;
            // 
            // panel29
            // 
            this.panel29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel29.Location = new System.Drawing.Point(-1, 238);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(696, 165);
            this.panel29.TabIndex = 34;
            // 
            // textBox54
            // 
            this.textBox54.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox54.Location = new System.Drawing.Point(258, 3);
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(134, 23);
            this.textBox54.TabIndex = 31;
            this.textBox54.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox53
            // 
            this.textBox53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox53.Location = new System.Drawing.Point(27, 73);
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(156, 20);
            this.textBox53.TabIndex = 30;
            // 
            // textBox52
            // 
            this.textBox52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox52.Location = new System.Drawing.Point(27, 47);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(156, 20);
            this.textBox52.TabIndex = 29;
            // 
            // textBox51
            // 
            this.textBox51.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox51.Location = new System.Drawing.Point(11, 140);
            this.textBox51.Multiline = true;
            this.textBox51.Name = "textBox51";
            this.textBox51.ReadOnly = true;
            this.textBox51.Size = new System.Drawing.Size(146, 23);
            this.textBox51.TabIndex = 33;
            // 
            // textBox50
            // 
            this.textBox50.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.textBox50.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox50.Location = new System.Drawing.Point(562, 138);
            this.textBox50.Multiline = true;
            this.textBox50.Name = "textBox50";
            this.textBox50.ReadOnly = true;
            this.textBox50.Size = new System.Drawing.Size(129, 24);
            this.textBox50.TabIndex = 32;
            this.textBox50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox49
            // 
            this.textBox49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox49.Location = new System.Drawing.Point(27, 97);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(156, 20);
            this.textBox49.TabIndex = 27;
            // 
            // textBox48
            // 
            this.textBox48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox48.Location = new System.Drawing.Point(27, 21);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(156, 20);
            this.textBox48.TabIndex = 28;
            // 
            // panel28
            // 
            this.panel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel28.Location = new System.Drawing.Point(-1, 62);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(696, 89);
            this.panel28.TabIndex = 35;
            // 
            // textBox47
            // 
            this.textBox47.ForeColor = System.Drawing.Color.Green;
            this.textBox47.Location = new System.Drawing.Point(416, 3);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(186, 20);
            this.textBox47.TabIndex = 22;
            // 
            // textBox46
            // 
            this.textBox46.ForeColor = System.Drawing.Color.Green;
            this.textBox46.Location = new System.Drawing.Point(416, 55);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(186, 20);
            this.textBox46.TabIndex = 24;
            // 
            // textBox45
            // 
            this.textBox45.ForeColor = System.Drawing.Color.Green;
            this.textBox45.Location = new System.Drawing.Point(416, 29);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(186, 20);
            this.textBox45.TabIndex = 23;
            // 
            // groupBox7
            // 
            this.groupBox7.ForeColor = System.Drawing.Color.Green;
            this.groupBox7.Location = new System.Drawing.Point(3, 17);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(180, 60);
            this.groupBox7.TabIndex = 21;
            this.groupBox7.TabStop = false;
            // 
            // textBox44
            // 
            this.textBox44.Location = new System.Drawing.Point(8, 14);
            this.textBox44.Multiline = true;
            this.textBox44.Name = "textBox44";
            this.textBox44.ReadOnly = true;
            this.textBox44.Size = new System.Drawing.Size(166, 37);
            this.textBox44.TabIndex = 17;
            // 
            // groupBox6
            // 
            this.groupBox6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox6.ForeColor = System.Drawing.Color.Green;
            this.groupBox6.Location = new System.Drawing.Point(189, 17);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(180, 63);
            this.groupBox6.TabIndex = 20;
            this.groupBox6.TabStop = false;
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(8, 16);
            this.textBox43.Multiline = true;
            this.textBox43.Name = "textBox43";
            this.textBox43.ReadOnly = true;
            this.textBox43.Size = new System.Drawing.Size(166, 38);
            this.textBox43.TabIndex = 0;
            // 
            // panel27
            // 
            this.panel27.Location = new System.Drawing.Point(-1, -1);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(696, 61);
            this.panel27.TabIndex = 36;
            // 
            // textBox42
            // 
            this.textBox42.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox42.Location = new System.Drawing.Point(259, 5);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(147, 29);
            this.textBox42.TabIndex = 26;
            this.textBox42.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(273, 34);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(120, 20);
            this.textBox41.TabIndex = 27;
            this.textBox41.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(51, 5);
            this.textBox40.Multiline = true;
            this.textBox40.Name = "textBox40";
            this.textBox40.ReadOnly = true;
            this.textBox40.Size = new System.Drawing.Size(91, 23);
            this.textBox40.TabIndex = 35;
            // 
            // textBox39
            // 
            this.textBox39.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.textBox39.Location = new System.Drawing.Point(563, 2);
            this.textBox39.Multiline = true;
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(129, 24);
            this.textBox39.TabIndex = 34;
            this.textBox39.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(520, 197);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(232, 13);
            this.label28.TabIndex = 37;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(520, 210);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(171, 13);
            this.label27.TabIndex = 38;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(520, 223);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(174, 13);
            this.label26.TabIndex = 39;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(4, 221);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(241, 13);
            this.label25.TabIndex = 40;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.White;
            this.panel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel23.Location = new System.Drawing.Point(4, 8);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(660, 390);
            this.panel23.TabIndex = 51;
            // 
            // dataGridView6
            // 
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Location = new System.Drawing.Point(3, 171);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.RowHeadersVisible = false;
            this.dataGridView6.Size = new System.Drawing.Size(657, 46);
            this.dataGridView6.TabIndex = 0;
            // 
            // panel25
            // 
            this.panel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel25.Location = new System.Drawing.Point(3, 3);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(654, 166);
            this.panel25.TabIndex = 1;
            // 
            // textBox38
            // 
            this.textBox38.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox38.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox38.Location = new System.Drawing.Point(522, 144);
            this.textBox38.MaximumSize = new System.Drawing.Size(129, 19);
            this.textBox38.MinimumSize = new System.Drawing.Size(129, 19);
            this.textBox38.Multiline = true;
            this.textBox38.Name = "textBox38";
            this.textBox38.ReadOnly = true;
            this.textBox38.Size = new System.Drawing.Size(129, 19);
            this.textBox38.TabIndex = 35;
            // 
            // textBox37
            // 
            this.textBox37.ForeColor = System.Drawing.Color.Green;
            this.textBox37.Location = new System.Drawing.Point(61, 52);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(185, 20);
            this.textBox37.TabIndex = 39;
            // 
            // textBox36
            // 
            this.textBox36.ForeColor = System.Drawing.Color.Green;
            this.textBox36.Location = new System.Drawing.Point(61, 35);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(185, 20);
            this.textBox36.TabIndex = 40;
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(3, 3);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(186, 20);
            this.textBox35.TabIndex = 38;
            this.textBox35.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox5
            // 
            this.groupBox5.ForeColor = System.Drawing.Color.Green;
            this.groupBox5.Location = new System.Drawing.Point(3, 100);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(185, 62);
            this.groupBox5.TabIndex = 43;
            this.groupBox5.TabStop = false;
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(8, 14);
            this.textBox34.Multiline = true;
            this.textBox34.Name = "textBox34";
            this.textBox34.ReadOnly = true;
            this.textBox34.Size = new System.Drawing.Size(166, 37);
            this.textBox34.TabIndex = 17;
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(436, 2);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(100, 20);
            this.textBox33.TabIndex = 44;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(484, 216);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(175, 13);
            this.label24.TabIndex = 43;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(481, 229);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(174, 13);
            this.label23.TabIndex = 45;
            // 
            // panel24
            // 
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel24.Location = new System.Drawing.Point(1, 245);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(657, 145);
            this.panel24.TabIndex = 46;
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(34, 39);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(118, 20);
            this.textBox32.TabIndex = 0;
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(34, 56);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(178, 20);
            this.textBox31.TabIndex = 1;
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(2, 3);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(100, 20);
            this.textBox30.TabIndex = 5;
            // 
            // groupBox4
            // 
            this.groupBox4.ForeColor = System.Drawing.Color.Green;
            this.groupBox4.Location = new System.Drawing.Point(236, 35);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(175, 54);
            this.groupBox4.TabIndex = 44;
            this.groupBox4.TabStop = false;
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(6, 12);
            this.textBox29.Multiline = true;
            this.textBox29.Name = "textBox29";
            this.textBox29.ReadOnly = true;
            this.textBox29.Size = new System.Drawing.Size(122, 33);
            this.textBox29.TabIndex = 17;
            // 
            // pnlSalesInvoicetemplate2
            // 
            this.pnlSalesInvoicetemplate2.BackColor = System.Drawing.Color.White;
            this.pnlSalesInvoicetemplate2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSalesInvoicetemplate2.Location = new System.Drawing.Point(19, 3);
            this.pnlSalesInvoicetemplate2.Name = "pnlSalesInvoicetemplate2";
            this.pnlSalesInvoicetemplate2.Size = new System.Drawing.Size(688, 405);
            this.pnlSalesInvoicetemplate2.TabIndex = 46;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(-1, 165);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(688, 44);
            this.dataGridView2.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Location = new System.Drawing.Point(-1, 254);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(688, 150);
            this.panel5.TabIndex = 2;
            // 
            // txtthanks
            // 
            this.txtthanks.Location = new System.Drawing.Point(268, 125);
            this.txtthanks.Name = "txtthanks";
            this.txtthanks.Size = new System.Drawing.Size(259, 20);
            this.txtthanks.TabIndex = 0;
            this.txtthanks.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtAdditionalfield1
            // 
            this.txtAdditionalfield1.Location = new System.Drawing.Point(3, 99);
            this.txtAdditionalfield1.Name = "txtAdditionalfield1";
            this.txtAdditionalfield1.Size = new System.Drawing.Size(366, 20);
            this.txtAdditionalfield1.TabIndex = 1;
            // 
            // txtadditionalfieldbottom
            // 
            this.txtadditionalfieldbottom.Location = new System.Drawing.Point(279, 20);
            this.txtadditionalfieldbottom.Name = "txtadditionalfieldbottom";
            this.txtadditionalfieldbottom.Size = new System.Drawing.Size(208, 20);
            this.txtadditionalfieldbottom.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(520, 210);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(175, 13);
            this.label9.TabIndex = 40;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(519, 223);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(171, 13);
            this.label8.TabIndex = 41;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Location = new System.Drawing.Point(-1, 89);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(688, 79);
            this.panel4.TabIndex = 0;
            // 
            // grpboxbillto1
            // 
            this.grpboxbillto1.ForeColor = System.Drawing.Color.Green;
            this.grpboxbillto1.Location = new System.Drawing.Point(1, -1);
            this.grpboxbillto1.Name = "grpboxbillto1";
            this.grpboxbillto1.Size = new System.Drawing.Size(215, 60);
            this.grpboxbillto1.TabIndex = 22;
            this.grpboxbillto1.TabStop = false;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(8, 14);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(166, 37);
            this.textBox4.TabIndex = 17;
            // 
            // txtInvoiceNo1
            // 
            this.txtInvoiceNo1.ForeColor = System.Drawing.Color.Green;
            this.txtInvoiceNo1.Location = new System.Drawing.Point(461, 3);
            this.txtInvoiceNo1.Name = "txtInvoiceNo1";
            this.txtInvoiceNo1.Size = new System.Drawing.Size(185, 20);
            this.txtInvoiceNo1.TabIndex = 36;
            // 
            // txtInvoiceDate1
            // 
            this.txtInvoiceDate1.ForeColor = System.Drawing.Color.Green;
            this.txtInvoiceDate1.Location = new System.Drawing.Point(461, 23);
            this.txtInvoiceDate1.Name = "txtInvoiceDate1";
            this.txtInvoiceDate1.Size = new System.Drawing.Size(185, 20);
            this.txtInvoiceDate1.TabIndex = 37;
            // 
            // txtAdditionalfieldTop
            // 
            this.txtAdditionalfieldTop.Location = new System.Drawing.Point(222, 12);
            this.txtAdditionalfieldTop.Name = "txtAdditionalfieldTop";
            this.txtAdditionalfieldTop.Size = new System.Drawing.Size(163, 20);
            this.txtAdditionalfieldTop.TabIndex = 38;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(519, 236);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(174, 13);
            this.label7.TabIndex = 42;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Location = new System.Drawing.Point(-1, -1);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(688, 88);
            this.panel8.TabIndex = 39;
            // 
            // txtCompanyaddress1
            // 
            this.txtCompanyaddress1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.txtCompanyaddress1.Location = new System.Drawing.Point(4, 29);
            this.txtCompanyaddress1.Multiline = true;
            this.txtCompanyaddress1.Name = "txtCompanyaddress1";
            this.txtCompanyaddress1.ReadOnly = true;
            this.txtCompanyaddress1.Size = new System.Drawing.Size(215, 39);
            this.txtCompanyaddress1.TabIndex = 34;
            // 
            // txtYourcompanyname
            // 
            this.txtYourcompanyname.Location = new System.Drawing.Point(3, 3);
            this.txtYourcompanyname.Name = "txtYourcompanyname";
            this.txtYourcompanyname.Size = new System.Drawing.Size(211, 20);
            this.txtYourcompanyname.TabIndex = 23;
            this.txtYourcompanyname.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtInvoice1
            // 
            this.txtInvoice1.Location = new System.Drawing.Point(269, 0);
            this.txtInvoice1.Name = "txtInvoice1";
            this.txtInvoice1.Size = new System.Drawing.Size(186, 20);
            this.txtInvoice1.TabIndex = 35;
            this.txtInvoice1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pnlInvoiceTemp4
            // 
            this.pnlInvoiceTemp4.AllowDrop = true;
            this.pnlInvoiceTemp4.BackColor = System.Drawing.Color.LavenderBlush;
            this.pnlInvoiceTemp4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlInvoiceTemp4.Location = new System.Drawing.Point(13, 10);
            this.pnlInvoiceTemp4.Name = "pnlInvoiceTemp4";
            this.pnlInvoiceTemp4.Size = new System.Drawing.Size(510, 516);
            this.pnlInvoiceTemp4.TabIndex = 47;
            this.pnlInvoiceTemp4.TabStop = true;
            // 
            // chkQty
            // 
            this.chkQty.AutoSize = true;
            this.chkQty.Location = new System.Drawing.Point(11, 32);
            this.chkQty.Name = "chkQty";
            this.chkQty.Size = new System.Drawing.Size(59, 17);
            this.chkQty.TabIndex = 68;
            this.chkQty.Text = "Quntity";
            this.chkQty.UseVisualStyleBackColor = true;
            // 
            // chkRate
            // 
            this.chkRate.AutoSize = true;
            this.chkRate.Location = new System.Drawing.Point(10, 58);
            this.chkRate.Name = "chkRate";
            this.chkRate.Size = new System.Drawing.Size(49, 17);
            this.chkRate.TabIndex = 69;
            this.chkRate.Text = "Rate";
            this.chkRate.UseVisualStyleBackColor = true;
            // 
            // chkDiscount
            // 
            this.chkDiscount.AutoSize = true;
            this.chkDiscount.Location = new System.Drawing.Point(11, 82);
            this.chkDiscount.Name = "chkDiscount";
            this.chkDiscount.Size = new System.Drawing.Size(68, 17);
            this.chkDiscount.TabIndex = 70;
            this.chkDiscount.Text = "Discount";
            this.chkDiscount.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(11, 14);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(102, 13);
            this.label21.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlstrplblOperationType,
            this.cboOperationTypes,
            this.tlstrplblw1,
            this.tlstrplblSampleTemplate,
            this.cboSampleTemplates,
            this.tlstrplblw,
            this.toolStripButton1,
            this.tlstrpBtnShowPreview,
            this.toolStripSeparator1,
            this.tlstriplblTmpname,
            this.txtTemplateName,
            this.tlstrpLbl,
            this.tlstrpbtnSave,
            this.tlstrpBtnDelete});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1037, 25);
            this.toolStrip1.TabIndex = 70;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tlstrplblOperationType
            // 
            this.tlstrplblOperationType.Name = "tlstrplblOperationType";
            this.tlstrplblOperationType.Size = new System.Drawing.Size(88, 22);
            this.tlstrplblOperationType.Text = "Opertion Types";
            // 
            // cboOperationTypes
            // 
            this.cboOperationTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOperationTypes.Name = "cboOperationTypes";
            this.cboOperationTypes.Size = new System.Drawing.Size(121, 25);
            this.cboOperationTypes.SelectedIndexChanged += new System.EventHandler(this.cboOperationTypes_SelectedIndexChanged);
            // 
            // tlstrplblw1
            // 
            this.tlstrplblw1.AutoSize = false;
            this.tlstrplblw1.Name = "tlstrplblw1";
            this.tlstrplblw1.RightToLeftAutoMirrorImage = true;
            this.tlstrplblw1.Size = new System.Drawing.Size(30, 22);
            // 
            // tlstrplblSampleTemplate
            // 
            this.tlstrplblSampleTemplate.Name = "tlstrplblSampleTemplate";
            this.tlstrplblSampleTemplate.Size = new System.Drawing.Size(104, 22);
            this.tlstrplblSampleTemplate.Text = "Sample Templates";
            // 
            // cboSampleTemplates
            // 
            this.cboSampleTemplates.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSampleTemplates.Name = "cboSampleTemplates";
            this.cboSampleTemplates.Size = new System.Drawing.Size(121, 25);
            this.cboSampleTemplates.SelectedIndexChanged += new System.EventHandler(this.cboSampleTemplates_SelectedIndexChanged);
            // 
            // tlstrplblw
            // 
            this.tlstrplblw.AutoSize = false;
            this.tlstrplblw.Name = "tlstrplblw";
            this.tlstrplblw.RightToLeftAutoMirrorImage = true;
            this.tlstrplblw.Size = new System.Drawing.Size(30, 22);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::MyBooksERP.Properties.Resources.View;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Show Template";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // tlstrpBtnShowPreview
            // 
            this.tlstrpBtnShowPreview.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tlstrpBtnShowPreview.Image = global::MyBooksERP.Properties.Resources.Reports;
            this.tlstrpBtnShowPreview.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tlstrpBtnShowPreview.Name = "tlstrpBtnShowPreview";
            this.tlstrpBtnShowPreview.Size = new System.Drawing.Size(23, 22);
            this.tlstrpBtnShowPreview.Text = "Generate Report";
            this.tlstrpBtnShowPreview.Click += new System.EventHandler(this.tlstrpBtnShowPreview_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tlstriplblTmpname
            // 
            this.tlstriplblTmpname.Name = "tlstriplblTmpname";
            this.tlstriplblTmpname.Size = new System.Drawing.Size(92, 22);
            this.tlstriplblTmpname.Text = "Template Name";
            // 
            // txtTemplateName
            // 
            this.txtTemplateName.AutoSize = false;
            this.txtTemplateName.Name = "txtTemplateName";
            this.txtTemplateName.Size = new System.Drawing.Size(150, 25);
            this.txtTemplateName.TextChanged += new System.EventHandler(this.txtTemplateName_TextChanged);
            // 
            // tlstrpLbl
            // 
            this.tlstrpLbl.AutoSize = false;
            this.tlstrpLbl.Name = "tlstrpLbl";
            this.tlstrpLbl.RightToLeftAutoMirrorImage = true;
            this.tlstrpLbl.Size = new System.Drawing.Size(30, 22);
            // 
            // tlstrpbtnSave
            // 
            this.tlstrpbtnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tlstrpbtnSave.Image = global::MyBooksERP.Properties.Resources.save;
            this.tlstrpbtnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tlstrpbtnSave.Name = "tlstrpbtnSave";
            this.tlstrpbtnSave.Size = new System.Drawing.Size(23, 22);
            this.tlstrpbtnSave.Text = "Save Template";
            this.tlstrpbtnSave.Click += new System.EventHandler(this.tlstrpbtnSave_Click);
            // 
            // tlstrpBtnDelete
            // 
            this.tlstrpBtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tlstrpBtnDelete.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.tlstrpBtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tlstrpBtnDelete.Name = "tlstrpBtnDelete";
            this.tlstrpBtnDelete.Size = new System.Drawing.Size(23, 22);
            this.tlstrpBtnDelete.Text = "Delete Template";
            this.tlstrpBtnDelete.Click += new System.EventHandler(this.tlstrpBtnDelete_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tbTemplateDesign);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1037, 454);
            this.panel1.TabIndex = 71;
            // 
            // tbTemplateDesign
            // 
            this.tbTemplateDesign.Controls.Add(this.tabPageDesign);
            this.tbTemplateDesign.Controls.Add(this.tabPagePreview);
            this.tbTemplateDesign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbTemplateDesign.Location = new System.Drawing.Point(0, 0);
            this.tbTemplateDesign.Name = "tbTemplateDesign";
            this.tbTemplateDesign.SelectedIndex = 0;
            this.tbTemplateDesign.Size = new System.Drawing.Size(1037, 454);
            this.tbTemplateDesign.TabIndex = 48;
            this.tbTemplateDesign.SelectedIndexChanged += new System.EventHandler(this.tbTemplateDesign_SelectedIndexChanged);
            // 
            // tabPageDesign
            // 
            this.tabPageDesign.Controls.Add(this.expandableSplitterLeft);
            this.tabPageDesign.Controls.Add(this.panel2);
            this.tabPageDesign.Controls.Add(this.pnlAlltemplateDesign);
            this.tabPageDesign.Location = new System.Drawing.Point(4, 22);
            this.tabPageDesign.Name = "tabPageDesign";
            this.tabPageDesign.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDesign.Size = new System.Drawing.Size(1029, 428);
            this.tabPageDesign.TabIndex = 1;
            this.tabPageDesign.Text = "Design";
            this.tabPageDesign.UseVisualStyleBackColor = true;
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.panel2;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(239, 3);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 422);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 120;
            this.expandableSplitterLeft.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.navigationPane1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(236, 422);
            this.panel2.TabIndex = 49;
            // 
            // navigationPane1
            // 
            this.navigationPane1.Controls.Add(this.navigationPanePanel2);
            this.navigationPane1.Controls.Add(this.navigationPanePanel3);
            this.navigationPane1.Controls.Add(this.navigationPanePanel1);
            this.navigationPane1.Dock = System.Windows.Forms.DockStyle.Left;
            this.navigationPane1.ItemPaddingBottom = 2;
            this.navigationPane1.ItemPaddingTop = 2;
            this.navigationPane1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem1,
            this.buttonItem2,
            this.buttonItem3});
            this.navigationPane1.Location = new System.Drawing.Point(0, 0);
            this.navigationPane1.Name = "navigationPane1";
            this.navigationPane1.Padding = new System.Windows.Forms.Padding(1);
            this.navigationPane1.Size = new System.Drawing.Size(235, 422);
            this.navigationPane1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPane1.TabIndex = 52;
            // 
            // 
            // 
            this.navigationPane1.TitlePanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPane1.TitlePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.navigationPane1.TitlePanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navigationPane1.TitlePanel.Location = new System.Drawing.Point(1, 1);
            this.navigationPane1.TitlePanel.Name = "panelTitle";
            this.navigationPane1.TitlePanel.Size = new System.Drawing.Size(233, 24);
            this.navigationPane1.TitlePanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.navigationPane1.TitlePanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.navigationPane1.TitlePanel.Style.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.navigationPane1.TitlePanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPane1.TitlePanel.Style.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom;
            this.navigationPane1.TitlePanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.navigationPane1.TitlePanel.Style.GradientAngle = 90;
            this.navigationPane1.TitlePanel.Style.MarginLeft = 4;
            this.navigationPane1.TitlePanel.TabIndex = 0;
            this.navigationPane1.TitlePanel.Text = "Master Details";
            // 
            // navigationPanePanel3
            // 
            this.navigationPanePanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPanePanel3.Controls.Add(this.tvToolbox);
            this.navigationPanePanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPanePanel3.Location = new System.Drawing.Point(1, 1);
            this.navigationPanePanel3.Name = "navigationPanePanel3";
            this.navigationPanePanel3.ParentItem = this.buttonItem3;
            this.navigationPanePanel3.Size = new System.Drawing.Size(233, 420);
            this.navigationPanePanel3.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.navigationPanePanel3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPanePanel3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.navigationPanePanel3.Style.GradientAngle = 90;
            this.navigationPanePanel3.TabIndex = 4;
            // 
            // tvToolbox
            // 
            this.tvToolbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvToolbox.Location = new System.Drawing.Point(0, 0);
            this.tvToolbox.Name = "tvToolbox";
            treeNode4.Name = "Node2";
            treeNode4.Text = "TextBox";
            treeNode5.Name = "Node3";
            treeNode5.Text = "GroupBox";
            treeNode6.Name = "Node0";
            treeNode6.Text = "Common Controls";
            this.tvToolbox.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode6});
            this.tvToolbox.Size = new System.Drawing.Size(233, 420);
            this.tvToolbox.TabIndex = 0;
            this.tvToolbox.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tvToolbox_ItemDrag);
            // 
            // buttonItem3
            // 
            this.buttonItem3.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem3.Image")));
            this.buttonItem3.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.OptionGroup = "navBar";
            this.buttonItem3.Text = "Toolbox";
            // 
            // navigationPanePanel1
            // 
            this.navigationPanePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPanePanel1.Controls.Add(this.tvMasterDetails);
            this.navigationPanePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPanePanel1.Location = new System.Drawing.Point(1, 1);
            this.navigationPanePanel1.Name = "navigationPanePanel1";
            this.navigationPanePanel1.ParentItem = this.buttonItem1;
            this.navigationPanePanel1.Size = new System.Drawing.Size(233, 420);
            this.navigationPanePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.navigationPanePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPanePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.navigationPanePanel1.Style.GradientAngle = 90;
            this.navigationPanePanel1.TabIndex = 2;
            // 
            // tvMasterDetails
            // 
            this.tvMasterDetails.AllowDrop = true;
            this.tvMasterDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tvMasterDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvMasterDetails.FullRowSelect = true;
            this.tvMasterDetails.HideSelection = false;
            this.tvMasterDetails.Location = new System.Drawing.Point(0, 0);
            this.tvMasterDetails.Name = "tvMasterDetails";
            this.tvMasterDetails.Size = new System.Drawing.Size(233, 420);
            this.tvMasterDetails.TabIndex = 51;
            this.tvMasterDetails.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tvMasterDetails_ItemDrag_1);
            // 
            // buttonItem1
            // 
            this.buttonItem1.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem1.Image")));
            this.buttonItem1.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.OptionGroup = "navBar";
            this.buttonItem1.Text = "Master";
            // 
            // navigationPanePanel2
            // 
            this.navigationPanePanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPanePanel2.Controls.Add(this.cblMasterDetails);
            this.navigationPanePanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPanePanel2.Location = new System.Drawing.Point(1, 25);
            this.navigationPanePanel2.Name = "navigationPanePanel2";
            this.navigationPanePanel2.ParentItem = this.buttonItem2;
            this.navigationPanePanel2.Size = new System.Drawing.Size(233, 364);
            this.navigationPanePanel2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.navigationPanePanel2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPanePanel2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.navigationPanePanel2.Style.GradientAngle = 90;
            this.navigationPanePanel2.TabIndex = 3;
            // 
            // cblMasterDetails
            // 
            this.cblMasterDetails.CheckOnClick = true;
            this.cblMasterDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cblMasterDetails.FormattingEnabled = true;
            this.cblMasterDetails.Location = new System.Drawing.Point(0, 0);
            this.cblMasterDetails.Name = "cblMasterDetails";
            this.cblMasterDetails.Size = new System.Drawing.Size(233, 364);
            this.cblMasterDetails.TabIndex = 52;
            this.cblMasterDetails.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.cblMasterDetails_ItemCheck_1);
            // 
            // buttonItem2
            // 
            this.buttonItem2.Checked = true;
            this.buttonItem2.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem2.Image")));
            this.buttonItem2.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.OptionGroup = "navBar";
            this.buttonItem2.Text = "Master Details";
            // 
            // pnlAlltemplateDesign
            // 
            this.pnlAlltemplateDesign.BackColor = System.Drawing.Color.DimGray;
            this.pnlAlltemplateDesign.Controls.Add(this.pnlAllTemplate);
            this.pnlAlltemplateDesign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAlltemplateDesign.Location = new System.Drawing.Point(3, 3);
            this.pnlAlltemplateDesign.Name = "pnlAlltemplateDesign";
            this.pnlAlltemplateDesign.Size = new System.Drawing.Size(1023, 422);
            this.pnlAlltemplateDesign.TabIndex = 45;
            // 
            // pnlAllTemplate
            // 
            this.pnlAllTemplate.AutoScroll = true;
            this.pnlAllTemplate.BackColor = System.Drawing.Color.Transparent;
            this.pnlAllTemplate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAllTemplate.Location = new System.Drawing.Point(0, 0);
            this.pnlAllTemplate.Name = "pnlAllTemplate";
            this.pnlAllTemplate.Size = new System.Drawing.Size(1023, 422);
            this.pnlAllTemplate.TabIndex = 0;
            this.pnlAllTemplate.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlAllTemplate_MouseClick);
            // 
            // tabPagePreview
            // 
            this.tabPagePreview.Controls.Add(this.CustomRptViewer);
            this.tabPagePreview.Location = new System.Drawing.Point(4, 22);
            this.tabPagePreview.Name = "tabPagePreview";
            this.tabPagePreview.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePreview.Size = new System.Drawing.Size(1029, 428);
            this.tabPagePreview.TabIndex = 0;
            this.tabPagePreview.Text = "Preview";
            this.tabPagePreview.UseVisualStyleBackColor = true;
            // 
            // CustomRptViewer
            // 
            this.CustomRptViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource3.Name = "DtSetSales_STSalesInvoiceDetails";
            reportDataSource3.Value = null;
            reportDataSource4.Name = "DtSetSales_STSalesInvoiceMaster";
            reportDataSource4.Value = null;
            this.CustomRptViewer.LocalReport.DataSources.Add(reportDataSource3);
            this.CustomRptViewer.LocalReport.DataSources.Add(reportDataSource4);
            this.CustomRptViewer.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.InvoiceTemplates.temp1.rdlc";
            this.CustomRptViewer.Location = new System.Drawing.Point(3, 3);
            this.CustomRptViewer.Name = "CustomRptViewer";
            this.CustomRptViewer.Size = new System.Drawing.Size(1023, 422);
            this.CustomRptViewer.TabIndex = 31;
            // 
            // tlpTextboxContent
            // 
            this.tlpTextboxContent.Tag = "";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn1.HeaderText = "QTY";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Item";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "UNIT";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 40;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.HeaderText = "Description";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.HeaderText = "Discount";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.HeaderText = "Tax";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Unit Price";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 40;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn8.HeaderText = "Total";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn9.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "Description";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 150;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn11.HeaderText = "QTY";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn12.HeaderText = "Unit";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 92;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "Discount";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 80;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn14.HeaderText = "Total";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn15.HeaderText = "Item";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn16.HeaderText = "Description";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Width = 70;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "Unit Price";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Width = 80;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn19.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn20.HeaderText = "Item";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn21.HeaderText = "Description";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.HeaderText = "Unit Price";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn24.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn25.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn25.HeaderText = "QTY";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.HeaderText = "Item";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.Width = 80;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.HeaderText = "UNIT";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.Width = 80;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn28.HeaderText = "Description";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.HeaderText = "Discount";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.HeaderText = "Tax";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.Width = 70;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn31.HeaderText = "Item";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn32.HeaderText = "Description";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn33.FillWeight = 116.9422F;
            this.dataGridViewTextBoxColumn33.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.HeaderText = "Unit Price";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.Width = 150;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn35.FillWeight = 116.9422F;
            this.dataGridViewTextBoxColumn35.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            // 
            // dataGridViewTextBoxColumn36
            // 
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn36.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn36.HeaderText = "QTY";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.Width = 92;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.HeaderText = "Item";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.Width = 80;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn38.FillWeight = 66.1157F;
            this.dataGridViewTextBoxColumn38.HeaderText = "UNIT";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn39.HeaderText = "Description";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.HeaderText = "Discount";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.HeaderText = "Tax";
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.ReadOnly = true;
            this.dataGridViewTextBoxColumn41.Width = 70;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.HeaderText = "Unit Price";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.ReadOnly = true;
            this.dataGridViewTextBoxColumn42.Width = 80;
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.HeaderText = "Total";
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.ReadOnly = true;
            this.dataGridViewTextBoxColumn43.Width = 95;
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn49.HeaderText = "Total";
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.HeaderText = "Discount";
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.HeaderText = "QTY";
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.HeaderText = "Description";
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.ReadOnly = true;
            this.dataGridViewTextBoxColumn45.Width = 150;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn44.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.ReadOnly = true;
            // 
            // ColTotal1
            // 
            this.ColTotal1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColTotal1.HeaderText = "Total";
            this.ColTotal1.Name = "ColTotal1";
            this.ColTotal1.ReadOnly = true;
            // 
            // ColUnit1
            // 
            this.ColUnit1.HeaderText = "Rate";
            this.ColUnit1.Name = "ColUnit1";
            this.ColUnit1.ReadOnly = true;
            // 
            // ColDiscount1
            // 
            this.ColDiscount1.HeaderText = "Discount";
            this.ColDiscount1.Name = "ColDiscount1";
            this.ColDiscount1.ReadOnly = true;
            // 
            // ColQty1
            // 
            this.ColQty1.HeaderText = "QTY";
            this.ColQty1.Name = "ColQty1";
            this.ColQty1.ReadOnly = true;
            // 
            // ColDescription1
            // 
            this.ColDescription1.HeaderText = "Description";
            this.ColDescription1.Name = "ColDescription1";
            this.ColDescription1.ReadOnly = true;
            this.ColDescription1.Width = 150;
            // 
            // ColItemCode1
            // 
            this.ColItemCode1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColItemCode1.HeaderText = "Item Code";
            this.ColItemCode1.Name = "ColItemCode1";
            this.ColItemCode1.ReadOnly = true;
            // 
            // ErrorTemplate
            // 
            this.ErrorTemplate.ContainerControl = this;
            // 
            // FrmInvoiceTemplateDesign
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1037, 479);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmInvoiceTemplateDesign";
            this.RightToLeftLayout = true;
            this.Text = "Invoice Template Design";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmInvoiceTemplateDesign_Load);
            this.contextMenuStrip2.ResumeLayout(false);
            this.contextMenuStrip4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tbTemplateDesign.ResumeLayout(false);
            this.tabPageDesign.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.navigationPane1.ResumeLayout(false);
            this.navigationPanePanel3.ResumeLayout(false);
            this.navigationPanePanel1.ResumeLayout(false);
            this.navigationPanePanel2.ResumeLayout(false);
            this.pnlAlltemplateDesign.ResumeLayout(false);
            this.tabPagePreview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorTemplate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.TextBox txtNw;
        //private System.Windows.Forms.Panel pnlSalesInvoice4;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem dELETEToolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip4;
        private System.Windows.Forms.ToolStripMenuItem deleteTextBoxToolStripMenuItem;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.DataGridView dataGridView8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.DataGridView dataGridView7;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.Panel pnlSalesInvoicetemplate2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTotal1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColUnit1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDiscount1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColQty1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDescription1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColItemCode1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtthanks;
        private System.Windows.Forms.TextBox txtAdditionalfield1;
        private System.Windows.Forms.TextBox txtadditionalfieldbottom;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox grpboxbillto1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox txtInvoiceNo1;
        private System.Windows.Forms.TextBox txtInvoiceDate1;
        private System.Windows.Forms.TextBox txtAdditionalfieldTop;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox txtCompanyaddress1;
        private System.Windows.Forms.TextBox txtYourcompanyname;
        private System.Windows.Forms.TextBox txtInvoice1;
        private System.Windows.Forms.Panel pnlInvoiceTemp4;
        private System.Windows.Forms.CheckBox chkQty;
        private System.Windows.Forms.CheckBox chkRate;
        private System.Windows.Forms.CheckBox chkDiscount;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel tlstrplblOperationType;
        private System.Windows.Forms.ToolStripComboBox cboOperationTypes;
        private System.Windows.Forms.ToolStripLabel tlstrplblSampleTemplate;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tbTemplateDesign;
        private System.Windows.Forms.TabPage tabPageDesign;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterLeft;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlAlltemplateDesign;
        private System.Windows.Forms.TabPage tabPagePreview;
        private Microsoft.Reporting.WinForms.ReportViewer CustomRptViewer;
        private System.Windows.Forms.Panel pnlAllTemplate;
        private System.Windows.Forms.ToolStripComboBox cboSampleTemplates;
        private DevComponents.DotNetBar.NavigationPane navigationPane1;
        private DevComponents.DotNetBar.NavigationPanePanel navigationPanePanel2;
        private System.Windows.Forms.CheckedListBox cblMasterDetails;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.NavigationPanePanel navigationPanePanel1;
        private System.Windows.Forms.TreeView tvMasterDetails;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.NavigationPanePanel navigationPanePanel3;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private System.Windows.Forms.TreeView tvToolbox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel tlstriplblTmpname;
        private System.Windows.Forms.ToolStripTextBox txtTemplateName;
        private System.Windows.Forms.ToolStripButton tlstrpbtnSave;
        private System.Windows.Forms.ToolTip tlpTextboxContent;
        private System.Windows.Forms.ErrorProvider ErrorTemplate;
        private System.Windows.Forms.ToolStripLabel tlstrpLbl;
        private System.Windows.Forms.ToolStripButton tlstrpBtnShowPreview;
        private System.Windows.Forms.ToolStripLabel tlstrplblw;
        private System.Windows.Forms.ToolStripLabel tlstrplblw1;
        private System.Windows.Forms.ToolStripButton tlstrpBtnDelete;
        //private System.Windows.Forms.TextBox TempName;
    }
}