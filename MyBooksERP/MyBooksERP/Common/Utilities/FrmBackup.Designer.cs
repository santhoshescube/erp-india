﻿namespace MyBooksERP
{
    partial class FrmBackup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBackup));
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.OpenFile1 = new System.Windows.Forms.OpenFileDialog();
            this.FolderBrowser1 = new System.Windows.Forms.FolderBrowserDialog();
            this.pnlBackUp = new DevComponents.DotNetBar.PanelEx();
            this.lblFileName = new DevComponents.DotNetBar.LabelX();
            this.FileNameTextBox = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.BrowsePath = new DevComponents.DotNetBar.ButtonX();
            this.lblBackupPath = new DevComponents.DotNetBar.LabelX();
            this.FilePathTextBox = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.BtnBackUp = new DevComponents.DotNetBar.ButtonX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.toolStripStatusLabel11 = new DevComponents.DotNetBar.LabelItem();
            this.rtfMess = new DevComponents.DotNetBar.LabelItem();
            this.pnlBackUp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // Timer1
            // 
            this.Timer1.Enabled = true;
            this.Timer1.Interval = 1000;
            // 
            // pnlBackUp
            // 
            this.pnlBackUp.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBackUp.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBackUp.Controls.Add(this.lblFileName);
            this.pnlBackUp.Controls.Add(this.FileNameTextBox);
            this.pnlBackUp.Controls.Add(this.BrowsePath);
            this.pnlBackUp.Controls.Add(this.lblBackupPath);
            this.pnlBackUp.Controls.Add(this.FilePathTextBox);
            this.pnlBackUp.Location = new System.Drawing.Point(8, 7);
            this.pnlBackUp.Name = "pnlBackUp";
            this.pnlBackUp.Size = new System.Drawing.Size(335, 80);
            this.pnlBackUp.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBackUp.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBackUp.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBackUp.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBackUp.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBackUp.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBackUp.Style.GradientAngle = 90;
            this.pnlBackUp.TabIndex = 14;
            // 
            // lblFileName
            // 
            // 
            // 
            // 
            this.lblFileName.BackgroundStyle.Class = "";
            this.lblFileName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFileName.Location = new System.Drawing.Point(15, 39);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(66, 23);
            this.lblFileName.TabIndex = 4;
            this.lblFileName.Text = "File Name";
            // 
            // FileNameTextBox
            // 
            // 
            // 
            // 
            this.FileNameTextBox.Border.Class = "TextBoxBorder";
            this.FileNameTextBox.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.FileNameTextBox.Location = new System.Drawing.Point(84, 41);
            this.FileNameTextBox.Name = "FileNameTextBox";
            this.FileNameTextBox.ReadOnly = true;
            this.FileNameTextBox.Size = new System.Drawing.Size(239, 20);
            this.FileNameTextBox.TabIndex = 3;
            // 
            // BrowsePath
            // 
            this.BrowsePath.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BrowsePath.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BrowsePath.Location = new System.Drawing.Point(300, 15);
            this.BrowsePath.Name = "BrowsePath";
            this.BrowsePath.Size = new System.Drawing.Size(23, 20);
            this.BrowsePath.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BrowsePath.TabIndex = 2;
            this.BrowsePath.Text = "...";
            this.BrowsePath.Tooltip = "Browse for file";
            this.BrowsePath.Click += new System.EventHandler(this.BrowsePath_Click);
            // 
            // lblBackupPath
            // 
            // 
            // 
            // 
            this.lblBackupPath.BackgroundStyle.Class = "";
            this.lblBackupPath.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblBackupPath.Location = new System.Drawing.Point(15, 14);
            this.lblBackupPath.Name = "lblBackupPath";
            this.lblBackupPath.Size = new System.Drawing.Size(66, 23);
            this.lblBackupPath.TabIndex = 1;
            this.lblBackupPath.Text = "Backup Path";
            // 
            // FilePathTextBox
            // 
            // 
            // 
            // 
            this.FilePathTextBox.Border.Class = "TextBoxBorder";
            this.FilePathTextBox.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.FilePathTextBox.Location = new System.Drawing.Point(84, 15);
            this.FilePathTextBox.Name = "FilePathTextBox";
            this.FilePathTextBox.Size = new System.Drawing.Size(210, 20);
            this.FilePathTextBox.TabIndex = 0;
            // 
            // BtnBackUp
            // 
            this.BtnBackUp.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnBackUp.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnBackUp.Location = new System.Drawing.Point(187, 93);
            this.BtnBackUp.Name = "BtnBackUp";
            this.BtnBackUp.Size = new System.Drawing.Size(75, 23);
            this.BtnBackUp.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnBackUp.TabIndex = 5;
            this.BtnBackUp.Text = "BackUp";
            this.BtnBackUp.Tooltip = "BackUp";
            this.BtnBackUp.Click += new System.EventHandler(this.BtnBackUp_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.Location = new System.Drawing.Point(268, 93);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Tooltip = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.toolStripStatusLabel11,
            this.rtfMess});
            this.bar1.Location = new System.Drawing.Point(0, 123);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(353, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 82;
            this.bar1.TabStop = false;
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Text = "Status:";
            // 
            // rtfMess
            // 
            this.rtfMess.Name = "rtfMess";
            // 
            // FrmBackup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 142);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.BtnBackUp);
            this.Controls.Add(this.pnlBackUp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmBackup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Back up";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.FrmBackup_HelpButtonClicked);
            this.pnlBackUp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Timer Timer1;
        internal System.Windows.Forms.OpenFileDialog OpenFile1;
        internal System.Windows.Forms.FolderBrowserDialog FolderBrowser1;
        private DevComponents.DotNetBar.PanelEx pnlBackUp;
        private DevComponents.DotNetBar.ButtonX BtnBackUp;
        private DevComponents.DotNetBar.LabelX lblFileName;
        private DevComponents.DotNetBar.Controls.TextBoxX FileNameTextBox;
        private DevComponents.DotNetBar.ButtonX BrowsePath;
        private DevComponents.DotNetBar.LabelX lblBackupPath;
        private DevComponents.DotNetBar.Controls.TextBoxX FilePathTextBox;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel11;
        private DevComponents.DotNetBar.LabelItem rtfMess;
    }
}