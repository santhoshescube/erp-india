﻿namespace MyBooksERP
{
    partial class FrmBarCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBarCode));
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblBarcodeStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.grpItemInfo = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cboEncodeType = new System.Windows.Forms.ComboBox();
            this.cboItemName = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cboRotateFlip = new System.Windows.Forms.ComboBox();
            this.lblLabelLocation = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnEncode = new System.Windows.Forms.Button();
            this.txtHeight = new System.Windows.Forms.TextBox();
            this.cboLabelLocation = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtWidth = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnBackColor = new System.Windows.Forms.Button();
            this.cboBarcodeAlign = new System.Windows.Forms.ComboBox();
            this.btnForeColor = new System.Windows.Forms.Button();
            this.lblEncodingTime = new System.Windows.Forms.Label();
            this.chkGenerateLabel = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.grpBarcodeImage = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.pbBarcode = new System.Windows.Forms.PictureBox();
            this.grpBarcodePrintSetup = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.txtColumn = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtRow = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.BarCodeBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bnMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bnMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bnPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bnCancel = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.bnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnHelp = new System.Windows.Forms.ToolStripButton();
            this.errBarcode = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrBarCode = new System.Windows.Forms.Timer(this.components);
            this.pdDocument = new System.Drawing.Printing.PrintDocument();
            this.ssStatus.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.grpItemInfo.SuspendLayout();
            this.grpBarcodeImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBarcode)).BeginInit();
            this.grpBarcodePrintSetup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarCodeBindingNavigator)).BeginInit();
            this.BarCodeBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errBarcode)).BeginInit();
            this.SuspendLayout();
            // 
            // ssStatus
            // 
            this.ssStatus.BackColor = System.Drawing.Color.Transparent;
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel2,
            this.lblBarcodeStatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 362);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(739, 22);
            this.ssStatus.TabIndex = 0;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(48, 17);
            this.toolStripStatusLabel2.Text = "Status : ";
            this.toolStripStatusLabel2.ToolTipText = "Status";
            // 
            // lblBarcodeStatus
            // 
            this.lblBarcodeStatus.Name = "lblBarcodeStatus";
            this.lblBarcodeStatus.Size = new System.Drawing.Size(0, 17);
            this.lblBarcodeStatus.ToolTipText = "Status";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(4, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.grpItemInfo);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.grpBarcodeImage);
            this.splitContainer1.Panel2.Controls.Add(this.grpBarcodePrintSetup);
            this.splitContainer1.Size = new System.Drawing.Size(730, 302);
            this.splitContainer1.SplitterDistance = 340;
            this.splitContainer1.TabIndex = 44;
            // 
            // grpItemInfo
            // 
            this.grpItemInfo.Controls.Add(this.label14);
            this.grpItemInfo.Controls.Add(this.cboEncodeType);
            this.grpItemInfo.Controls.Add(this.cboItemName);
            this.grpItemInfo.Controls.Add(this.label11);
            this.grpItemInfo.Controls.Add(this.label10);
            this.grpItemInfo.Controls.Add(this.cboRotateFlip);
            this.grpItemInfo.Controls.Add(this.lblLabelLocation);
            this.grpItemInfo.Controls.Add(this.label8);
            this.grpItemInfo.Controls.Add(this.btnEncode);
            this.grpItemInfo.Controls.Add(this.txtHeight);
            this.grpItemInfo.Controls.Add(this.cboLabelLocation);
            this.grpItemInfo.Controls.Add(this.label4);
            this.grpItemInfo.Controls.Add(this.label6);
            this.grpItemInfo.Controls.Add(this.txtWidth);
            this.grpItemInfo.Controls.Add(this.label7);
            this.grpItemInfo.Controls.Add(this.label5);
            this.grpItemInfo.Controls.Add(this.btnBackColor);
            this.grpItemInfo.Controls.Add(this.cboBarcodeAlign);
            this.grpItemInfo.Controls.Add(this.btnForeColor);
            this.grpItemInfo.Controls.Add(this.lblEncodingTime);
            this.grpItemInfo.Controls.Add(this.chkGenerateLabel);
            this.grpItemInfo.Controls.Add(this.label3);
            this.grpItemInfo.Controls.Add(this.txtData);
            this.grpItemInfo.Controls.Add(this.label1);
            this.grpItemInfo.Controls.Add(this.label9);
            this.grpItemInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpItemInfo.Location = new System.Drawing.Point(0, 0);
            this.grpItemInfo.Name = "grpItemInfo";
            this.grpItemInfo.Size = new System.Drawing.Size(340, 302);
            this.grpItemInfo.TabIndex = 0;
            this.grpItemInfo.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(106, 13);
            this.label14.TabIndex = 56;
            this.label14.Text = "Item Information";
            // 
            // cboEncodeType
            // 
            this.cboEncodeType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEncodeType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEncodeType.BackColor = System.Drawing.SystemColors.Info;
            this.cboEncodeType.DropDownHeight = 134;
            this.cboEncodeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEncodeType.FormattingEnabled = true;
            this.cboEncodeType.IntegralHeight = false;
            this.cboEncodeType.Location = new System.Drawing.Point(6, 120);
            this.cboEncodeType.MaxDropDownItems = 10;
            this.cboEncodeType.Name = "cboEncodeType";
            this.cboEncodeType.Size = new System.Drawing.Size(124, 21);
            this.cboEncodeType.TabIndex = 3;
            this.cboEncodeType.SelectedIndexChanged += new System.EventHandler(this.cboEncodeType_SelectedIndexChanged);
            // 
            // cboItemName
            // 
            this.cboItemName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboItemName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboItemName.BackColor = System.Drawing.SystemColors.Info;
            this.cboItemName.DropDownHeight = 134;
            this.cboItemName.FormattingEnabled = true;
            this.cboItemName.IntegralHeight = false;
            this.cboItemName.Location = new System.Drawing.Point(93, 17);
            this.cboItemName.MaxDropDownItems = 10;
            this.cboItemName.Name = "cboItemName";
            this.cboItemName.Size = new System.Drawing.Size(232, 21);
            this.cboItemName.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 54;
            this.label11.Text = "Item Name";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 153);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Rotate";
            // 
            // cboRotateFlip
            // 
            this.cboRotateFlip.BackColor = System.Drawing.SystemColors.Info;
            this.cboRotateFlip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRotateFlip.FormattingEnabled = true;
            this.cboRotateFlip.Items.AddRange(new object[] {
            "Center",
            "Left",
            "Right"});
            this.cboRotateFlip.Location = new System.Drawing.Point(139, 149);
            this.cboRotateFlip.Name = "cboRotateFlip";
            this.cboRotateFlip.Size = new System.Drawing.Size(194, 21);
            this.cboRotateFlip.TabIndex = 7;
            // 
            // lblLabelLocation
            // 
            this.lblLabelLocation.AutoSize = true;
            this.lblLabelLocation.Location = new System.Drawing.Point(6, 207);
            this.lblLabelLocation.Name = "lblLabelLocation";
            this.lblLabelLocation.Size = new System.Drawing.Size(77, 13);
            this.lblLabelLocation.TabIndex = 48;
            this.lblLabelLocation.Text = "Label Location";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 50;
            this.label8.Text = "Alignment";
            // 
            // btnEncode
            // 
            this.btnEncode.BackColor = System.Drawing.SystemColors.Control;
            this.btnEncode.Location = new System.Drawing.Point(233, 116);
            this.btnEncode.Name = "btnEncode";
            this.btnEncode.Size = new System.Drawing.Size(100, 28);
            this.btnEncode.TabIndex = 6;
            this.btnEncode.Text = "&GenerateBarcode";
            this.btnEncode.UseVisualStyleBackColor = true;
            this.btnEncode.Click += new System.EventHandler(this.GenerateBarcode);
            // 
            // txtHeight
            // 
            this.txtHeight.Location = new System.Drawing.Point(193, 120);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(28, 20);
            this.txtHeight.TabIndex = 5;
            this.txtHeight.Text = "75";
            this.txtHeight.Leave += new System.EventHandler(this.txtHeight_Leave);
            // 
            // cboLabelLocation
            // 
            this.cboLabelLocation.BackColor = System.Drawing.SystemColors.Info;
            this.cboLabelLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLabelLocation.FormattingEnabled = true;
            this.cboLabelLocation.Items.AddRange(new object[] {
            "BottomCenter",
            "BottomLeft",
            "BottomRight",
            "TopCenter",
            "TopLeft",
            "TopRight"});
            this.cboLabelLocation.Location = new System.Drawing.Point(139, 203);
            this.cboLabelLocation.Name = "cboLabelLocation";
            this.cboLabelLocation.Size = new System.Drawing.Size(194, 21);
            this.cboLabelLocation.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 232);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "Barcode Fore Color";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(189, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 42;
            this.label6.Text = "Height";
            // 
            // txtWidth
            // 
            this.txtWidth.Location = new System.Drawing.Point(139, 120);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.Size = new System.Drawing.Size(32, 20);
            this.txtWidth.TabIndex = 4;
            this.txtWidth.Text = "160";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(140, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 41;
            this.label7.Text = "Width";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 264);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 39;
            this.label5.Text = "Background Color";
            // 
            // btnBackColor
            // 
            this.btnBackColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackColor.Location = new System.Drawing.Point(139, 259);
            this.btnBackColor.Name = "btnBackColor";
            this.btnBackColor.Size = new System.Drawing.Size(195, 23);
            this.btnBackColor.TabIndex = 12;
            this.btnBackColor.UseVisualStyleBackColor = true;
            this.btnBackColor.Click += new System.EventHandler(this.btnBackColor_Click);
            // 
            // cboBarcodeAlign
            // 
            this.cboBarcodeAlign.BackColor = System.Drawing.SystemColors.Info;
            this.cboBarcodeAlign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBarcodeAlign.FormattingEnabled = true;
            this.cboBarcodeAlign.Location = new System.Drawing.Point(139, 176);
            this.cboBarcodeAlign.Name = "cboBarcodeAlign";
            this.cboBarcodeAlign.Size = new System.Drawing.Size(96, 21);
            this.cboBarcodeAlign.TabIndex = 8;
            // 
            // btnForeColor
            // 
            this.btnForeColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnForeColor.Location = new System.Drawing.Point(139, 230);
            this.btnForeColor.Name = "btnForeColor";
            this.btnForeColor.Size = new System.Drawing.Size(195, 23);
            this.btnForeColor.TabIndex = 11;
            this.btnForeColor.UseVisualStyleBackColor = true;
            this.btnForeColor.Click += new System.EventHandler(this.btnForeColor_Click);
            // 
            // lblEncodingTime
            // 
            this.lblEncodingTime.AutoSize = true;
            this.lblEncodingTime.Location = new System.Drawing.Point(83, 207);
            this.lblEncodingTime.Name = "lblEncodingTime";
            this.lblEncodingTime.Size = new System.Drawing.Size(0, 13);
            this.lblEncodingTime.TabIndex = 9;
            // 
            // chkGenerateLabel
            // 
            this.chkGenerateLabel.AutoSize = true;
            this.chkGenerateLabel.Location = new System.Drawing.Point(233, 76);
            this.chkGenerateLabel.Name = "chkGenerateLabel";
            this.chkGenerateLabel.Size = new System.Drawing.Size(95, 17);
            this.chkGenerateLabel.TabIndex = 2;
            this.chkGenerateLabel.Text = "Generate label";
            this.chkGenerateLabel.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "Encoding";
            // 
            // txtData
            // 
            this.txtData.Location = new System.Drawing.Point(9, 74);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(219, 20);
            this.txtData.TabIndex = 1;
            this.txtData.Text = "038000356216";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "BarCode Value to Encode";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(178, 124);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 13);
            this.label9.TabIndex = 51;
            this.label9.Text = "x";
            // 
            // grpBarcodeImage
            // 
            this.grpBarcodeImage.Controls.Add(this.label15);
            this.grpBarcodeImage.Controls.Add(this.pbBarcode);
            this.grpBarcodeImage.Location = new System.Drawing.Point(9, 3);
            this.grpBarcodeImage.Name = "grpBarcodeImage";
            this.grpBarcodeImage.Size = new System.Drawing.Size(378, 194);
            this.grpBarcodeImage.TabIndex = 36;
            this.grpBarcodeImage.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(93, 13);
            this.label15.TabIndex = 57;
            this.label15.Text = "Barcode Image";
            // 
            // pbBarcode
            // 
            this.pbBarcode.BackColor = System.Drawing.Color.Transparent;
            this.pbBarcode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pbBarcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbBarcode.Location = new System.Drawing.Point(3, 16);
            this.pbBarcode.Name = "pbBarcode";
            this.pbBarcode.Size = new System.Drawing.Size(372, 175);
            this.pbBarcode.TabIndex = 13;
            this.pbBarcode.TabStop = false;
            // 
            // grpBarcodePrintSetup
            // 
            this.grpBarcodePrintSetup.BackColor = System.Drawing.SystemColors.Info;
            this.grpBarcodePrintSetup.Controls.Add(this.btnCancel);
            this.grpBarcodePrintSetup.Controls.Add(this.btnPrint);
            this.grpBarcodePrintSetup.Controls.Add(this.txtColumn);
            this.grpBarcodePrintSetup.Controls.Add(this.label12);
            this.grpBarcodePrintSetup.Controls.Add(this.label2);
            this.grpBarcodePrintSetup.Controls.Add(this.label13);
            this.grpBarcodePrintSetup.Controls.Add(this.txtRow);
            this.grpBarcodePrintSetup.Location = new System.Drawing.Point(5, 207);
            this.grpBarcodePrintSetup.Name = "grpBarcodePrintSetup";
            this.grpBarcodePrintSetup.Size = new System.Drawing.Size(380, 81);
            this.grpBarcodePrintSetup.TabIndex = 14;
            this.grpBarcodePrintSetup.TabStop = false;
            this.grpBarcodePrintSetup.Text = "Barcode Print Setup";
            this.grpBarcodePrintSetup.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.SystemColors.Control;
            this.btnCancel.Location = new System.Drawing.Point(223, 23);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(98, 35);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.CancelPrint);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.SystemColors.Control;
            this.btnPrint.Location = new System.Drawing.Point(120, 23);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(98, 35);
            this.btnPrint.TabIndex = 15;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.Print);
            // 
            // txtColumn
            // 
            this.txtColumn.Location = new System.Drawing.Point(76, 32);
            this.txtColumn.Name = "txtColumn";
            this.txtColumn.Size = new System.Drawing.Size(35, 20);
            this.txtColumn.TabIndex = 14;
            this.txtColumn.Text = "150";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(18, 15);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 13);
            this.label12.TabIndex = 56;
            this.label12.Text = "Row";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(73, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "Column";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(60, 36);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(12, 13);
            this.label13.TabIndex = 60;
            this.label13.Text = "x";
            // 
            // txtRow
            // 
            this.txtRow.Location = new System.Drawing.Point(21, 32);
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(35, 20);
            this.txtRow.TabIndex = 13;
            this.txtRow.Text = "200";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(658, 334);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 19;
            this.btnClose.Text = "&Cancel";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.Close);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.BackColor = System.Drawing.SystemColors.Control;
            this.btnOk.Enabled = false;
            this.btnOk.Location = new System.Drawing.Point(577, 334);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 18;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.SaveItem);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(4, 334);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.SaveItem);
            // 
            // BarCodeBindingNavigator
            // 
            this.BarCodeBindingNavigator.AddNewItem = null;
            this.BarCodeBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.BarCodeBindingNavigator.CountItem = this.bnCountItem;
            this.BarCodeBindingNavigator.DeleteItem = null;
            this.BarCodeBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnMoveFirstItem,
            this.bnMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.bnPositionItem,
            this.bnCountItem,
            this.BindingNavigatorSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnDeleteItem,
            this.bnCancel,
            this.ToolStripSeparator1,
            this.bnPrint,
            this.bnEmail,
            this.ToolStripSeparator2,
            this.bnHelp});
            this.BarCodeBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.BarCodeBindingNavigator.MoveFirstItem = null;
            this.BarCodeBindingNavigator.MoveLastItem = null;
            this.BarCodeBindingNavigator.MoveNextItem = null;
            this.BarCodeBindingNavigator.MovePreviousItem = null;
            this.BarCodeBindingNavigator.Name = "BarCodeBindingNavigator";
            this.BarCodeBindingNavigator.PositionItem = this.bnPositionItem;
            this.BarCodeBindingNavigator.Size = new System.Drawing.Size(739, 25);
            this.BarCodeBindingNavigator.TabIndex = 21;
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.Size = new System.Drawing.Size(35, 22);
            this.bnCountItem.Text = "of {0}";
            this.bnCountItem.ToolTipText = "Total number of items";
            // 
            // bnMoveFirstItem
            // 
            this.bnMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            this.bnMoveFirstItem.Name = "bnMoveFirstItem";
            this.bnMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveFirstItem.Text = "Move first";
            this.bnMoveFirstItem.Click += new System.EventHandler(this.MoveFirstItem);
            // 
            // bnMovePreviousItem
            // 
            this.bnMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem.Image")));
            this.bnMovePreviousItem.Name = "bnMovePreviousItem";
            this.bnMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bnMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bnMovePreviousItem.Text = "Move previous";
            this.bnMovePreviousItem.Click += new System.EventHandler(this.MovePreviousItem);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPositionItem
            // 
            this.bnPositionItem.AccessibleName = "Position";
            this.bnPositionItem.AutoSize = false;
            this.bnPositionItem.Name = "bnPositionItem";
            this.bnPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bnPositionItem.Text = "0";
            this.bnPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Move next";
            this.bnMoveNextItem.Click += new System.EventHandler(this.MoveNextItem);
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Move last";
            this.bnMoveLastItem.Click += new System.EventHandler(this.MoveLastItem);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewItem.Text = "Add";
            this.bnAddNewItem.Visible = false;
            this.bnAddNewItem.Click += new System.EventHandler(this.ResetForm);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bnSaveItem.Text = "Save";
            this.bnSaveItem.Click += new System.EventHandler(this.SaveItem);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bnDeleteItem.Image")));
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.Text = "Remove";
            this.bnDeleteItem.Click += new System.EventHandler(this.DeleteItem);
            // 
            // bnCancel
            // 
            this.bnCancel.Image = ((System.Drawing.Image)(resources.GetObject("bnCancel.Image")));
            this.bnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnCancel.Name = "bnCancel";
            this.bnCancel.Size = new System.Drawing.Size(54, 22);
            this.bnCancel.Text = "Clear";
            this.bnCancel.Visible = false;
            this.bnCancel.Click += new System.EventHandler(this.ResetForm);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPrint
            // 
            this.bnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnPrint.Image = ((System.Drawing.Image)(resources.GetObject("bnPrint.Image")));
            this.bnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(23, 22);
            this.bnPrint.Text = "Print/Fax";
            this.bnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // bnEmail
            // 
            this.bnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnEmail.Image = ((System.Drawing.Image)(resources.GetObject("bnEmail.Image")));
            this.bnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Size = new System.Drawing.Size(23, 22);
            this.bnEmail.Text = "Email";
            this.bnEmail.Visible = false;
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnHelp
            // 
            this.bnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnHelp.Image = ((System.Drawing.Image)(resources.GetObject("bnHelp.Image")));
            this.bnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnHelp.Name = "bnHelp";
            this.bnHelp.Size = new System.Drawing.Size(23, 22);
            this.bnHelp.Text = "He&lp";
            // 
            // errBarcode
            // 
            this.errBarcode.ContainerControl = this;
            this.errBarcode.RightToLeft = true;
            // 
            // tmrBarCode
            // 
            this.tmrBarCode.Interval = 2000;
            this.tmrBarCode.Tick += new System.EventHandler(this.TimerTick);
            // 
            // pdDocument
            // 
            this.pdDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // FrmBarCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 384);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.BarCodeBindingNavigator);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmBarCode";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BarCode Generation";
            this.Load += new System.EventHandler(this.FormLoad);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClosingForm);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormKeyDown);
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.grpItemInfo.ResumeLayout(false);
            this.grpItemInfo.PerformLayout();
            this.grpBarcodeImage.ResumeLayout(false);
            this.grpBarcodeImage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBarcode)).EndInit();
            this.grpBarcodePrintSetup.ResumeLayout(false);
            this.grpBarcodePrintSetup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarCodeBindingNavigator)).EndInit();
            this.BarCodeBindingNavigator.ResumeLayout(false);
            this.BarCodeBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errBarcode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        internal System.Windows.Forms.ToolStripStatusLabel lblBarcodeStatus;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox grpItemInfo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboRotateFlip;
        private System.Windows.Forms.Label lblLabelLocation;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnEncode;
        private System.Windows.Forms.TextBox txtHeight;
        private System.Windows.Forms.ComboBox cboLabelLocation;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtWidth;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnBackColor;
        private System.Windows.Forms.ComboBox cboBarcodeAlign;
        private System.Windows.Forms.Button btnForeColor;
        private System.Windows.Forms.Label lblEncodingTime;
        private System.Windows.Forms.CheckBox chkGenerateLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox grpBarcodeImage;
        private System.Windows.Forms.PictureBox pbBarcode;
        private System.Windows.Forms.Label label11;
        internal System.Windows.Forms.BindingNavigator BarCodeBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel bnCountItem;
        internal System.Windows.Forms.ToolStripButton bnMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton bnMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox bnPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton bnMoveNextItem;
        internal System.Windows.Forms.ToolStripButton bnMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton bnAddNewItem;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        internal System.Windows.Forms.ToolStripButton bnCancel;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton bnPrint;
        internal System.Windows.Forms.ToolStripButton bnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton bnHelp;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnClose;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.ErrorProvider errBarcode;
        internal System.Windows.Forms.Timer tmrBarCode;
        internal System.Windows.Forms.ComboBox cboItemName;
        internal System.Windows.Forms.ComboBox cboEncodeType;
        private System.Drawing.Printing.PrintDocument pdDocument;
        private System.Windows.Forms.GroupBox grpBarcodePrintSetup;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.TextBox txtColumn;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtRow;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;


    }
}