﻿using System;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLCreateFeatures
    {
        private clsDALCreateFeatures objDALCreateFeatures = null;
        public clsDALCreateFeatures DALCreateFeature
        {
            get
            {
                if (this.objDALCreateFeatures == null)
                    this.objDALCreateFeatures = new clsDALCreateFeatures();

                return this.objDALCreateFeatures;
            }
        }

        public DataTable GetFeatures()
        {
            return this.DALCreateFeature.GetFeatures();
        }

        public DataTable GetFeatureValues(int intFeatureID)
        {
            return this.DALCreateFeature.GetFeatureValues(intFeatureID);
        }
    }
}
