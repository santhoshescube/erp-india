﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,8 Mar 2011>
   Description:	<Description,,Item Location Details BLL>
================================================
*/
namespace MyBooksERP
{
    public class clsBLLItemLocationDetails
    {
        private DataLayer MobjDataLayer;//obj of DataLayer
        private clsDALItemLocationDetails MobjClsDALItemLocationDetails;//obj Of clsDALItemLocationDetails

        public clsDTOItemLocationMaster PobjClsDTOItemLocationMaster { get; set; }//property for clsDTOItemLocation Master

        public clsBLLItemLocationDetails()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjClsDALItemLocationDetails = new clsDALItemLocationDetails(MobjDataLayer);
            PobjClsDTOItemLocationMaster = new clsDTOItemLocationMaster();
            MobjClsDALItemLocationDetails.PobjClsDTOItemLocationMaster = PobjClsDTOItemLocationMaster;
        }

        public bool SaveItemLocationDetails()
        {
            //Saving Information
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALItemLocationDetails.SaveItemLocationDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public decimal GetItemBatchQuantity(int intItemID, int intBatchID, int intWarehouseID)
        {
            //function for getting item batch quantity
            return MobjClsDALItemLocationDetails.GetItemBatchQuantity(intItemID, intBatchID, intWarehouseID);
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting dataTable for filling combo
            return MobjClsDALItemLocationDetails.FillCombos(saFieldValues);
        }

        public DataTable GetGRNsAndInvoices(int intReferenceID, bool blnIsGRN)
        {
            //Function for getting GRNs And Invoices With Pending Location Setting
            return MobjClsDALItemLocationDetails.GetGRNsAndInvoices(intReferenceID, blnIsGRN);
        }

        public DataTable FindAllItemsWithoutLocation(bool blnIsGRN, int intReferenceID)
        {
            return MobjClsDALItemLocationDetails.FindAllItemsWithoutLocation(blnIsGRN, intReferenceID);
        }
    }
}
