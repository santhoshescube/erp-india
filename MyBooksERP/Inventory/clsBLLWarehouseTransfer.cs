﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
namespace MyBooksERP
{
    public class clsBLLWarehouseTransfer
    {

        private DataLayer MobjDataLayer;
        private clsDALWarehouseTransfer MobjClsDALWarehouseTransfer;
        public clsDTOWarehouseTransfer PobjClsDTOWarehouseTransfer { get; set; }
        public clsBLLWarehouseTransfer()
        {
            MobjDataLayer = new DataLayer();
            MobjClsDALWarehouseTransfer = new clsDALWarehouseTransfer(MobjDataLayer);
            PobjClsDTOWarehouseTransfer = new clsDTOWarehouseTransfer();
            MobjClsDALWarehouseTransfer.MobjclsDTOWarehouseTransfer = PobjClsDTOWarehouseTransfer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            return MobjClsDALWarehouseTransfer.FillCombos(saFieldValues);
        }
        //----------------------------------------------------
        public bool SaveStockTransfer(bool blnAddStatus)
        {//Save and update stocktransfer order
            bool blnRetValue = false;
             try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALWarehouseTransfer.SaveStockTransfer(blnAddStatus);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public bool DisplayStockTransferInfo(Int64 iOrderID)
        {//Displaying Stock Transfer Info
            return MobjClsDALWarehouseTransfer.DisplayStockTransferInfo(iOrderID);
        }
        public bool DeleteStockTransferInfo()
        {//Delete Stock transfer info 
            return MobjClsDALWarehouseTransfer.DeleteStockTransferInfo();
        }

        public DataTable SearchStockTransfers(string strSearchCondition)
        {
            // fill search grid
            return MobjClsDALWarehouseTransfer.SearchStockTransfers(strSearchCondition);
        }
        public DataTable GetStockTransferItemDetails()
        {// Getting Stock transfer Item Details
            return MobjClsDALWarehouseTransfer.GetStockTransferItemDetails();
        }

        public bool UpdateStockTransferStatus()
        {//Updating Stocktrransfer Status
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALWarehouseTransfer.UpdateStockTransferStatus();
                if (blnRetValue)
                {
                    if (MobjClsDALWarehouseTransfer.MobjclsDTOWarehouseTransfer.intStatusID == (int)OperationStatusType.STCancelled)
                        blnRetValue = SaveCancellationDetails();
                    else if (MobjClsDALWarehouseTransfer.MobjclsDTOWarehouseTransfer.intStatusID == (int)OperationStatusType.STOpened)
                        blnRetValue = DeleteCancellationDetails();
                }
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public bool Suggest(int intOperationTypeID)
        {// Verification History insertion when suggest or deny
            return MobjClsDALWarehouseTransfer.Suggest(intOperationTypeID);
        }
        public bool SaveCancellationDetails()
        {//Save Cancellation details
            return MobjClsDALWarehouseTransfer.SaveCancellationDetails();
        }
        public bool DeleteCancellationDetails()
        { //Delete Cancelation details
            return MobjClsDALWarehouseTransfer.DeleteCancellationDetails();
        }
        //------------------------------------------------------------------------------------
        public Int64 GetLastStockTransferNo(int intCompanyID)
        {
            // get Last Stock Transfer No against each company
            return MobjClsDALWarehouseTransfer.GetLastStockTransferNo(intCompanyID);
        }
        public string DisplayAddressInformation(int intVendorAdd)
        {
            //getting vendor Address info
            return MobjClsDALWarehouseTransfer.DisplayAddressInformation(intVendorAdd);
        }
        public DataTable GetItemUOMs(int intItemID)
        {
            //Get ItemUoms
            
            return MobjClsDALWarehouseTransfer.GetItemUOMs(intItemID);
        }
        public DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {//get uom conversions
            return MobjClsDALWarehouseTransfer.GetUomConversionValues(intUOMID, intItemID);

        }
        public DataTable GetItemWiseDiscountDetails(int intItemID, decimal decQty, decimal decRate, string strCurrentDate,int intCurrencyID)
        {
            //get uom conversions
            return MobjClsDALWarehouseTransfer.GetItemWiseDiscountDetails(intItemID, decQty, decRate, strCurrentDate,intCurrencyID);
        }

        public bool IsDiscountForAmount(int intDiscountID)
        {
            return MobjClsDALWarehouseTransfer.IsDiscountForAmount(intDiscountID);
        }

        public double GetItemDiscountAmount(int iMode, int iItemID, int DiscountID, double Rate)
        {
            return MobjClsDALWarehouseTransfer.GetItemDiscountAmount(iMode, iItemID, DiscountID, Rate);
        }
        public bool GetExchangeCurrencyRate(int intCompanyID, int intCurrencyID)
        {
            return MobjClsDALWarehouseTransfer.GetExchangeCurrencyRate(intCompanyID, intCurrencyID);
        }
        public string GetCompanyCurrency(int intCompanyID, out int intScale,out int intCurrencyID)
        {
            return MobjClsDALWarehouseTransfer.GetCompanyCurrency(intCompanyID, out intScale,out intCurrencyID);
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALWarehouseTransfer.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetOperationTypeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALWarehouseTransfer.GetOperationTypeByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALWarehouseTransfer.GetEmployeeByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataSet GetStockTransferReport()
        {
            return MobjClsDALWarehouseTransfer.GetStockTransferReport();
        }

        public bool UpdateGrandTotalAmount()
        {
            return MobjClsDALWarehouseTransfer.UpdateGrandTotalAmount();
        }

        public DataTable GetDataForItemSelection(int intCompanyID, int intCurrencyID,int intWarehouseID,bool blnWarehouseToWarehouse)
        {
            return MobjClsDALWarehouseTransfer.GetDataForItemSelection(intCompanyID, intCurrencyID,intWarehouseID,blnWarehouseToWarehouse);
        }

        public bool UpdationVerificationTableRemarks(Int64 intReferenceID, string strRemarks)
        {
            return MobjClsDALWarehouseTransfer.UpdationVerificationTableRemarks(intReferenceID, strRemarks);
        }

        public bool IsReferenceExistsInReceipt(Int64 intReferenceID)
        {
            return MobjClsDALWarehouseTransfer.IsReferenceExistsInReceipt(intReferenceID);      
        }
        public bool IsDuplicateStockTranferNo(Int64 intCompanyID, string strStockTransferNo)
        {
            return MobjClsDALWarehouseTransfer.IsDuplicateStockTranferNo(intCompanyID, strStockTransferNo);
        }
        public bool IsValidStockTransferID(Int64 intCompanyID, Int64 intStockTransferID)
        {
            return MobjClsDALWarehouseTransfer.IsValidStockTransferID(intCompanyID, intStockTransferID);
        }
        public bool ExistCurrencyReference(int FromWarehouse, int ToWarehouse)
        {
            return MobjClsDALWarehouseTransfer.ExistCurrencyReference(FromWarehouse, ToWarehouse);
        }
    }
}
