﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;




/***********************************************
 * Created By       : Ratheesh
 * Creation Date    : 18 June 2011
 * Description      : Handle stock adjustments
 * *********************************************/

namespace MyBooksERP
{
    public class clsBLLStockAdjustments
    {
        #region Declarations

        // Data Access Layer
        private clsDALStockAdjustments objStockAdjustmentDAL;

        #endregion

        #region Properties

        public clsDTOStockAdjustment StockAdjustment {
            get { return this.objStockAdjustmentDAL.StockAdjustment; }
            set { this.objStockAdjustmentDAL.StockAdjustment = value; }
        }

        public clsPager Pager {
            get { return this.objStockAdjustmentDAL.Pager; }
            set { this.objStockAdjustmentDAL.Pager = value; }
        }

        #endregion

        #region  Constructor

        public clsBLLStockAdjustments()
        { 
            // Instantiate objects
            this.objStockAdjustmentDAL = new clsDALStockAdjustments();
        }

        #endregion

        public bool InsertStockAdjustments()
        {
            try
            {
                // variables to track database updation status
                bool MasterInserted = false, DetailsInserted = false,LocationDetailsInserted = false,StockUpdated = false;

                // here we begin our stock adjustment transaction
                this.objStockAdjustmentDAL.db.BeginTransaction();

                // insert master record
                MasterInserted = this.objStockAdjustmentDAL.InsertStockAdjustment();

                if (MasterInserted)
                    // insert detail records
                    DetailsInserted = this.objStockAdjustmentDAL.InsertAjustedItems();

                if (MasterInserted && DetailsInserted)
                {
                    // insert location details
                    if (LocationDetailsInserted)
                        LocationDetailsInserted = this.objStockAdjustmentDAL.InsertAdjustedLocationDetails();
                    else
                        LocationDetailsInserted = true;
                }

                //if (MasterInserted && DetailsInserted && LocationDetailsInserted)
                //    StockUpdated = this.objStockAdjustmentDAL.UpdateSaleOpStockAccountDetails(objStockAdjustmentDAL.StockAdjustment.StockAdjustmentID);

                if (!MasterInserted || !DetailsInserted || !LocationDetailsInserted)// || !StockUpdated)
                    // Either master or detail has not been inserted, so throw an exception to roll back
                    throw new ApplicationException("Stock adjustment insersion failed.");

                // stock adjustments have been inserted successfully. Commit transaction
                this.objStockAdjustmentDAL.db.CommitTransaction();

                return true;
            }
            catch 
            {
                // something went wrong. roll back
                this.objStockAdjustmentDAL.db.RollbackTransaction();
                
                // re-throw the very same exception the catch statement has caught.
                throw;
            }
        }

        public bool UpdateStockAdjustments()
        {
            try
            {
                // variables to track database updation status
                bool MasterUpdated = false, DetailsUpdated = false, DetailsDeleted = false;

                // here we begin our stock adjustment transaction
                this.objStockAdjustmentDAL.db.BeginTransaction();

                // insert master record
                MasterUpdated = this.objStockAdjustmentDAL.UpdateStockAdjustment();

               // if (MasterUpdated)
               // {
                    // delete existing detail records 
                //    DetailsDeleted = this.objStockAdjustmentDAL.DeleteStockAdjustmentDetails();

                //    if(DetailsDeleted)
                        // re-insert detail records having updated data
                //        DetailsUpdated = this.objStockAdjustmentDAL.InsertAjustedItems();
               // }

                if (!MasterUpdated)// || !DetailsUpdated)
                    // Either master or detail has not been updated, so throw an exception to roll back
                    throw new ApplicationException("Stock adjustment updation failed.");

                // stock adjustments have been inserted successfully. Commit transaction
                this.objStockAdjustmentDAL.db.CommitTransaction();

                return true;
            }
            catch
            {
                // something went wrong. roll back
                this.objStockAdjustmentDAL.db.RollbackTransaction();

                // re-throw the very same exception the catch statement has caught.
                throw;
            }
        }

        public DataTable GetRateAndCurrentStock(long BatchID, int ItemID, int WarehouseID)
        {
            return this.objStockAdjustmentDAL.GetRateAndCurrentStock(BatchID, ItemID, WarehouseID);
        }

        public bool GetStockAdjustmentDetails(long lngStockAdjustmentID)
        {
            return this.objStockAdjustmentDAL.GetStockAdjustmentDetails(lngStockAdjustmentID);
        }

        public void GetRecordCount()
        {
            this.objStockAdjustmentDAL.GetRecordCount();
        }

        public bool DeleteStockAdjustment()
        {
            return this.objStockAdjustmentDAL.DeleteStockAdjustment();
        }

        public bool IsAdjustmentNumberExists()
        {
            return this.objStockAdjustmentDAL.IsAdjustmentNumberExists();
        }

        public string GenerateAdjustmentNo(int intCompanyID)
        {
            return this.objStockAdjustmentDAL.GenerateAdjustmentNo(intCompanyID);
        }
        public DataSet DispalyStockAdjustmentReport(int intStockAdjustmentID)
        {
            return this.objStockAdjustmentDAL.DispalyStockAdjustmentReport(intStockAdjustmentID);
        }
        public DataSet DisplayStockAdjustmentEmail(int intStockAdjustmentID) // Stock Adjustment Email
        {
            return this.objStockAdjustmentDAL.DisplayStockAdjustmentEmail(intStockAdjustmentID);
        }


        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
          return this.objStockAdjustmentDAL.GetItemDefaultLocation(intItemID,intWarehouseID);
        }

        public decimal GetItemLocationQuantity(clsDTOAdjustedLocationDetails objLocationDetails, int intWarehouseID)
        {
            return this.objStockAdjustmentDAL.GetItemLocationQuantity(objLocationDetails, intWarehouseID);
        }

        public DataTable GetAllAdjustments()
        {
            return this.objStockAdjustmentDAL.GetAllAdjustments();
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return this.objStockAdjustmentDAL.GetEmployeeByPermission(intRoleID, intCompanyID, intControlID);
        }
    }
}
