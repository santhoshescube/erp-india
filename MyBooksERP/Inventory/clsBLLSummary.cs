﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;


namespace MyBooksERP 
{
    public class clsBLLSummary
    {
        private DataLayer MobjDataLayer;                                  //object of datalayer
        private clsDALSummary MobjClsDALSummary;            //object of clsDALSummary
        public clsDTOSummary MobjclsDTOSummary { get; set; }

        public clsBLLSummary()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDTOSummary = new clsDTOSummary();
            MobjClsDALSummary = new clsDALSummary(MobjDataLayer);
            MobjClsDALSummary.objclsDTOSummary = MobjclsDTOSummary;
        }


        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            return MobjClsDALSummary.FillCombos(saFieldValues);
        }

        public DataTable GetCompanyByPermission(int RoleID, int MenuID, int ControlID)
        {
            return MobjClsDALSummary.GetCompanyByPermission(RoleID, MenuID, ControlID);
        }

        public DataTable GetStockDetails()
        {
            return MobjClsDALSummary.GetStockDetails();
        }
    }
}
