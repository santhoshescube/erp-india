﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLMaterialIssueMaster
    {
        public clsDTOMaterialIssueMaster objClsDTOMaterialIssueMaster { get; set; }
        clsDALMaterialIssueMaster objclsDALMaterialIssueMaster;
        private DataLayer MobjDataLayer;


        public clsBLLMaterialIssueMaster()
        {
            MobjDataLayer = new DataLayer();
            objclsDALMaterialIssueMaster = new clsDALMaterialIssueMaster(MobjDataLayer);
            objClsDTOMaterialIssueMaster = new clsDTOMaterialIssueMaster();
            objclsDALMaterialIssueMaster .PobjclsDTOMaterialIssueMaster  = objClsDTOMaterialIssueMaster ;
        }

        public bool SaveMaterialIssue()
        {
            bool blnRetValue =false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = objclsDALMaterialIssueMaster.SaveMaterialIssueMaster();
                MobjDataLayer.CommitTransaction();

            }
            catch
            {
                MobjDataLayer.RollbackTransaction();
            }
            return blnRetValue;
        }

        public bool DeleteMaterialIssue()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = objclsDALMaterialIssueMaster.DeleteMaterialIssue();
                MobjDataLayer.CommitTransaction();
            }
            catch
            {
                MobjDataLayer.RollbackTransaction();
            }

            return blnRetValue;
        }

        public bool FillMaterialIssueMasterInfo()
        {
            return objclsDALMaterialIssueMaster.FillMaterialIssueMasterInfo();
        }

        public DataTable GetMaterialIssueDetails()
        {
            return objclsDALMaterialIssueMaster.GetMaterialIssueDetails();
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            return objclsDALMaterialIssueMaster.GetItemUOMs(intItemID);
        }

        public DataTable GetItems(int intCompanyID, int intWarehouseID)
        {
            return objclsDALMaterialIssueMaster.GetItems(intCompanyID, intWarehouseID);
        }

        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            return objclsDALMaterialIssueMaster.GetItemDefaultLocation(intItemID, intWarehouseID);
        }

        public DataTable GetAllMaterialIssues(string strCondition)
        {
            return objclsDALMaterialIssueMaster.GetAllMaterialIssues(strCondition);
        }

        public DataSet GetMaterialIssueReport() //Material Issue Report
        {
            return objclsDALMaterialIssueMaster.GetMaterialIssueReport();
        }

        public DataTable GetProductionTemplateDetails(long lngTemplateID)
        {
            return objclsDALMaterialIssueMaster.GetProductionTemplateDetails(lngTemplateID);
        }
    }
}
