﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace MyBooksERP
{
    public class clsBLLLocationTransfer
    {
        private clsDTOLLocationTransfer MobjclsDTOLLocationTransfer;
        private ClsDALLocationTransfer MobjClsDALLocationTransfer;
        private DataLayer MobjDataLayer;
        
        public clsBLLLocationTransfer()
        {
            MobjclsDTOLLocationTransfer = new clsDTOLLocationTransfer();
            MobjClsDALLocationTransfer = new ClsDALLocationTransfer();
            MobjDataLayer = new DataLayer();
            MobjClsDALLocationTransfer.clsDTOLLocationTransfer = clsDTOLLocationTransfer;
        }

        public clsDTOLLocationTransfer clsDTOLLocationTransfer
        {
            get { return MobjclsDTOLLocationTransfer; }
            set { MobjclsDTOLLocationTransfer = value; }
        }

        public bool SaveItemMovements(bool blnStaus, bool blnDamagedQty)
        {//Save
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.SaveItemMovements(blnStaus, blnDamagedQty);
        }
        //---------filter ItemLocation
       
        public DataTable GetItemLocationDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID, bool blnDamagedQty)
        {
            //filter ItemLocation Location
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetItemLocationDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID,blnDamagedQty);
        }
        public DataTable GetItemRowDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID, bool blnDamagedQty)
        {
            //filter ItemLocation Rows
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetItemRowDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID, blnDamagedQty);
        }
        public DataTable GetItemBlockDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID, bool blnDamagedQty)
        {//filter ItemLocation Blocks

            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetItemBlockDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID,  blnDamagedQty);
        }
        public DataTable GetItemLotDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID, bool blnDamagedQty)
        {
            //filter ItemLocation Lots
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetItemLotDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID,  blnDamagedQty);
        }
        public DataTable GetItemItemDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID,bool blnDamagedQty)
        {//filter ItemLocation item

            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetItemItemDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID,blnDamagedQty);
        }
        public DataTable GetItemBatchDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID, bool blnDamagedQty)
        {
            //filter ItemLocation Batch
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetItemBatchDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID,blnDamagedQty);
        }
        //-----------filter ware house---------
        public DataTable GetWarehouseLocInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)
        {//filter ware house lOcation
           MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
           return MobjClsDALLocationTransfer.GetWarehouseLocInfoByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID);

        }
        public DataTable GetWarehouseRowInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)
        {//filter ware house rows
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetWarehouseRowInfoByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID);
        }
        public DataTable GetWarehouseBlockInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)
        {
            //filter ware house blocks
             MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
             return MobjClsDALLocationTransfer.GetWarehouseBlockInfoByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID);
        }
        public DataTable GetWarehouseLotInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)
        {//filter ware house Lots
             MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
             return MobjClsDALLocationTransfer.GetWarehouseLotInfoByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID);
        }
        //-----------filter ware house---------
        //-----------fill combo
        public DataTable FillCombos(string[] sarFieldValues)// filling combo box
        {
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.FillCombos(sarFieldValues);
        }

        public DataTable GetCompanyByPermission(int intRoleID,int intCompanyID, int intControlID)// filling combo box
        {
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
        }
    }
}
/*
 
 using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace MyBooksERP
{
    public class clsBLLLocationTransfer
    {
        private clsDTOLLocationTransfer MobjclsDTOLLocationTransfer;
        private ClsDALLocationTransfer MobjClsDALLocationTransfer;
        private DataLayer MobjDataLayer;
        
        public clsBLLLocationTransfer()
        {
            MobjclsDTOLLocationTransfer = new clsDTOLLocationTransfer();
            MobjClsDALLocationTransfer = new ClsDALLocationTransfer();
            MobjDataLayer = new DataLayer();
            MobjClsDALLocationTransfer.clsDTOLLocationTransfer = clsDTOLLocationTransfer;
        }

        public clsDTOLLocationTransfer clsDTOLLocationTransfer
        {
            get { return MobjclsDTOLLocationTransfer; }
            set { MobjclsDTOLLocationTransfer = value; }
        }

        public DataTable DisplayLocationInfo(int intWarehouseID)//Selecting Locations
        {
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.DisplayLocationInfo( intWarehouseID);

        }
        public DataTable DisplayRowinfo(int intWarehouseID, int intLocID)//Getting Row Details
        {

             MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
             return MobjClsDALLocationTransfer.DisplayRowinfo(intWarehouseID, intLocID);

        }
        public DataTable DisplayBlockInfo(int intWarehouseID, int intLocID, int intRowID)//Blocks
        {
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.DisplayBlockInfo(intWarehouseID, intLocID, intRowID);
           
        }
        public DataTable DisplayLotInfo(int intWarehouseID, int intLocID, int intRowID, int intBlockID)//Selecting Lots
        {
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.DisplayLotInfo(intWarehouseID, intLocID, intRowID, intBlockID);
        }
        public DataTable DisplayItemInfo(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)//Selecting Lots
        {
             MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
             return MobjClsDALLocationTransfer.DisplayItemInfo(intWarehouseID, intLocID, intRowID, intBlockID, intLotID);

        }
        public DataTable DisplayBatchInfo(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID)//Selecting Lots
        {
            // Selecting Batch Details
            
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.DisplayBatchInfo(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID);
        }
        //----------------------------------------------------------------------------------------------------------------------
        public DataTable DisplayWarehouseInformationDetail(int pWarehouseID)
        {

            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.DisplayWarehouseInformationDetail(pWarehouseID);
            //return true;
        }
        public DataTable DisplayWarehouseInformationDetailRow(int pWarehouseID, string psLocation, int LocID)
        {
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.DisplayWarehouseInformationDetailRow(pWarehouseID, psLocation, LocID);
           
            //return true;
        }
        public DataTable DisplayWarehouseInformationDetailBLOCK(int pWarehouseID, string psLocation, int LocID, string psRows, int RowID)
        {

           
             MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
             return MobjClsDALLocationTransfer.DisplayWarehouseInformationDetailBLOCK( pWarehouseID,  psLocation,  LocID,  psRows,  RowID);
            //return true;
        }
        public DataTable DisplayWarehouseInformationDetailLOTS(int pWarehouseID, string psLocation, int LocID, string psRows, int RowID, string psBlocks, int BlockID)
        {
             MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
             return MobjClsDALLocationTransfer.DisplayWarehouseInformationDetailLOTS( pWarehouseID,  psLocation,  LocID,  psRows,  RowID,  psBlocks,  BlockID);
            //return true;
        }

        //----------------------------------------------------------------------------------------------------------------------
        //Save
        public bool Save(bool blnStaus)
        {
             MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
             return MobjClsDALLocationTransfer.Save(blnStaus);

        }
        //---------filter
        public DataTable GetItemLocationDetailByFilter(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID)
        {
            //Filter By all
             MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
             return MobjClsDALLocationTransfer.GetItemLocationDetailByFilter(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID);
        }
        public DataTable GetItemLocationDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID)
        {

            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetItemLocationDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID);
        }
        public DataTable GetItemRowDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID)
        {

            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetItemRowDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID);
        }
        public DataTable GetItemBlockDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID)
        {

            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetItemBlockDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID);
        }
        public DataTable GetItemLotDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID)
        {

            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetItemLotDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID);
        }
        public DataTable GetItemItemDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID)
        {

            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetItemItemDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID);
        }
        public DataTable GetItemBatchDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID)
        {

            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetItemBatchDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID);
        }
        //-----------filter ware house---------
        public DataTable GetWarehouseLocInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)
        {
           MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
           return MobjClsDALLocationTransfer.GetWarehouseLocInfoByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID);

        }
        public DataTable GetWarehouseRowInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)
        {
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.GetWarehouseRowInfoByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID);
        }
        public DataTable GetWarehouseBlockInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)
        {
             MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
             return MobjClsDALLocationTransfer.GetWarehouseBlockInfoByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID);
        }
        public DataTable GetWarehouseLotInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)
        {
             MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
             return MobjClsDALLocationTransfer.GetWarehouseLotInfoByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID);
        }
        //-----------filter ware house---------
        //-----------fill combo
        public DataTable FillCombos(string[] sarFieldValues)// filling combo box
        {
            MobjClsDALLocationTransfer.MobjclsConnection = MobjDataLayer;
            return MobjClsDALLocationTransfer.FillCombos(sarFieldValues);
        }

    }
}

*/
