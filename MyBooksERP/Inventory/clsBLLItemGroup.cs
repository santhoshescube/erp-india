﻿using System;
using System.Data;



namespace MyBooksERP
{
    public class clsBLLItemGroup
    {
        public clsDTOItemGroup objClsDTOItemGroup { get; set; }
        clsDALItemGroup objClsDALItemGroup;

        public clsBLLItemGroup()
        {
            objClsDALItemGroup = new clsDALItemGroup();
            objClsDTOItemGroup = new clsDTOItemGroup();
            objClsDALItemGroup.objClsDTOItemGroup = objClsDTOItemGroup;
            objClsDALItemGroup.objClsConnection = new DataLayer();
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            return objClsDALItemGroup.FillCombos(sarFieldValues);
        }

        public bool DisplayItemGroup()
        {
            return objClsDALItemGroup.DisplayItemGroup();
        }

        public bool SaveItemGroup()
        {
            bool blnSaved = false;
            try
            {
                objClsDALItemGroup.objClsConnection.BeginTransaction();
                blnSaved = objClsDALItemGroup.SaveItemGroup();
                objClsDALItemGroup.objClsConnection.CommitTransaction();
            }
            catch (Exception ex)
            {
                objClsDALItemGroup.objClsConnection.RollbackTransaction();
                throw ex;
            }

            return blnSaved;
        }

        public bool DeleteItemGroup()
        {
            return objClsDALItemGroup.DeleteItemGroup();
        }

        public int GetItemGroupCount()
        {
            return objClsDALItemGroup.GetItemGroupCount();
        }

        public DataSet GetItemGroupingReport()
        {
            return objClsDALItemGroup.GetItemGroupingReport();
        }

        public int GenerateItemGroupCode()
        {
            return objClsDALItemGroup.GenerateItemGroupCode();
        }
        public int CheckDupliateGroupCodeName(string strGroupCode,Int64 intItemGroupId)
        {
            return objClsDALItemGroup.CheckDupliateGroupCodeName(strGroupCode, intItemGroupId);
        }
        public DataTable GetItemGroupRowNumber(Int64 intItemGroupID)
        {
            return objClsDALItemGroup.GetItemGroupRowNumber(intItemGroupID);
        }

    }
}
