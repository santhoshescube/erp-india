﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLMaterialReturnMaster
    {
        public clsDTOMaterialReturnMaster objClsDTOMaterialReturnMaster { get; set; }
        clsDALMaterialReturnMaster objclsDALMaterialReturnMaster;
        private DataLayer MobjDataLayer;


        public clsBLLMaterialReturnMaster()
        {
            MobjDataLayer = new DataLayer();
            objclsDALMaterialReturnMaster = new clsDALMaterialReturnMaster(MobjDataLayer);
            objClsDTOMaterialReturnMaster = new clsDTOMaterialReturnMaster();
            objclsDALMaterialReturnMaster.PobjclsDTOMaterialReturnMaster  = objClsDTOMaterialReturnMaster ;
        }

        public bool SaveMaterialReturn()
        {
            bool blnRetValue =false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = objclsDALMaterialReturnMaster.SaveMaterialReturnMaster();
                MobjDataLayer.CommitTransaction();

            }
            catch
            {
                MobjDataLayer.RollbackTransaction();
            }
            return blnRetValue;
        }

        public bool DeleteMaterialReturn()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = objclsDALMaterialReturnMaster.DeleteMaterialReturn();
                MobjDataLayer.CommitTransaction();
            }
            catch
            {
                MobjDataLayer.RollbackTransaction();
            }

            return blnRetValue;
        }

        public bool FillMaterialReturnMasterInfo()
        {
            return objclsDALMaterialReturnMaster.FillMaterialReturnMasterInfo();
        }

        public DataTable GetMaterialReturnDetails()
        {
            return objclsDALMaterialReturnMaster.GetMaterialReturnDetails();
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            return objclsDALMaterialReturnMaster.GetItemUOMs(intItemID);
        }

        public DataTable GetItems(int intCompanyID,int intWarehouseID,int intProjectID)
        {
            return objclsDALMaterialReturnMaster.GetItems(intCompanyID,intWarehouseID,intProjectID);
        }

        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            return objclsDALMaterialReturnMaster.GetItemDefaultLocation(intItemID, intWarehouseID);
        }

        public DataTable GetAllMaterialReturns(string strCondition)
        {
            return objclsDALMaterialReturnMaster.GetAllMaterialReturns(strCondition);
        }

        public DataSet GetMaterialReturnReport() //Material Return Report
        {
            return objclsDALMaterialReturnMaster.GetMaterialReturnReport();
        }
    }
}
