﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,11 Apr 2011>
   Description:	<Description,,Opening Stock BLL>
================================================
*/
namespace MyBooksERP
{
    public class clsBLLOpeningStock
    {
        private DataLayer MobjDataLayer;                                  //object of datalayer
        private clsDALOpeningStock MobjClsDALOpeningStock;            //object of clsDALOpeningStock
        public clsDTOOpeningStock PobjClsDTOOpeningStock { get; set; }//Property for clsDTOOpening Stock

        public clsBLLOpeningStock()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjClsDALOpeningStock = new clsDALOpeningStock(MobjDataLayer);
            PobjClsDTOOpeningStock = new clsDTOOpeningStock();
            MobjClsDALOpeningStock.objDTOOpeningStock = PobjClsDTOOpeningStock;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = MobjDataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool GetOpeningStockInfo()
        {
            return MobjClsDALOpeningStock.GetOpeningStockInfo();
        }

        public DataTable GetOpeningStockDetails()
        {
            return MobjClsDALOpeningStock.GetOpeningStockDetails();
        }
        public Int32 GetCostingMethod(Int64 intItemID)
        {
            return MobjClsDALOpeningStock.GetCostingMethod(intItemID);
        }
        public bool SaveOpeningStockInfo()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALOpeningStock.SaveOpeningStockInfo();
                if (blnRetValue)
                    blnRetValue = MobjClsDALOpeningStock.DeleteUnwantedBatchDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }


        public bool DeleteOpeningStockInfo()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALOpeningStock.DeleteOpeningStockInfo();
                if (blnRetValue)
                    blnRetValue = MobjClsDALOpeningStock.DeleteUnwantedBatchDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeleteOpeningStockDetails()
        {
            return MobjClsDALOpeningStock.DeleteOpeningStockDetails();
        }

        public int GetRecordCount()
        {
            return MobjClsDALOpeningStock.GetRecordCount();
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            return MobjClsDALOpeningStock.GetItemUOMs(intItemID);
        }

        public string StockValidation(bool blnIsDeletion,out bool blnIsLocation,out bool blnIsSalesDone)
        {
            return MobjClsDALOpeningStock.StockValidation(blnIsDeletion,out blnIsLocation,out blnIsSalesDone);
        }

        //email

        public DataSet GetOpeningStockReport()
        {
            return MobjClsDALOpeningStock.GetOpeningStockReport();
        }


        public bool IsStockAdjustmentDone(int intItemID, Int64 intBatchID,int intWarehouseID)
        {
            return MobjClsDALOpeningStock.IsStockAdjusmentDone(intItemID, intBatchID,intWarehouseID);
        }

        public decimal GetDefaultLocationQuantity(int intItemID, long lngBatchID, int intWarehouseID)
        {
            return GetDefaultLocationQuantity(intItemID, lngBatchID, intWarehouseID);
        }

        public decimal GetUsedQuantity(int intItemID, long lngBatchID)
        {
            return MobjClsDALOpeningStock.GetUsedQuantity(intItemID, lngBatchID);
        }
    }
}
