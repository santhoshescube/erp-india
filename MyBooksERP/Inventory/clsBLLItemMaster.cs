﻿using System.Data;



namespace MyBooksERP
{
    /// <summary>
    /// Created by  : Devi
    /// Created on  : 18.02.2011
    /// Description : Business layer for ItemMaster
    /// </summary>
    public class clsBLLItemMaster 
    {
        public clsDTOItemMaster objClsDTOItemMaster { get; set; }
        clsDALItemMaster objclsDALItemMaster;
        
        public clsBLLItemMaster()
        {
            objclsDALItemMaster = new clsDALItemMaster();
            objClsDTOItemMaster = new clsDTOItemMaster();
            objclsDALItemMaster.objClsDTOItemMaster = objClsDTOItemMaster;
            objclsDALItemMaster.objClsConnection = new DataLayer();
        }

        public bool SaveItemInfo()
        {
            try
            {
                objclsDALItemMaster.objClsConnection.BeginTransaction();
                if (objclsDALItemMaster.SaveItemInfo())
                {                    
                    objclsDALItemMaster.SaveItemLocations();
                    objclsDALItemMaster.SaveItemUom();
                }
                objclsDALItemMaster.objClsConnection.CommitTransaction();
                return true;
            }
            catch(System.Exception ex)
            {
                this.objclsDALItemMaster.objClsConnection.RollbackTransaction();
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                return false;
            }

        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            return objclsDALItemMaster.FillCombos(sarFieldValues);
        }

        public bool DuplicateItemCodeExists(string strItemCode,int CompanyID)
        {
            return objclsDALItemMaster.DuplicateItemCodeExists(strItemCode, CompanyID);
        }

        public DataTable SearchItem(int intCategory, int intSubCategory, string strDescription, string strCode,int CompanyID, out int intTotalRows)
        {
            return objclsDALItemMaster.SearchItem(intCategory, intSubCategory, strDescription, strCode,CompanyID, out intTotalRows);
        }

        public bool DisplayItemInfo()
        {
            return objclsDALItemMaster.DisplayItemInfo();
        }

        public bool CheckItemExists()
        {
            return objclsDALItemMaster.CheckItemExists();
        }

        public bool CheckItemAndUOMExists(int intItemID, int intUomID,int intUomTypeID)
        {
            return objclsDALItemMaster.CheckItemAndUOMExists(intItemID, intUomID,intUomTypeID);
        }

        public bool DeleteItemInfo()
        {
            return objclsDALItemMaster.DeleteItemInfo();
        }

        public DataTable GetItemUOM(int intItemID)
        {
            return objclsDALItemMaster.GetItemUOM(intItemID);
        }

        public DataSet GetProductReport()
        {
            return objclsDALItemMaster.GetProductReport();
        }

        public DataSet GetProductBatchDetails()
        {
            return objclsDALItemMaster.GetProductBatchDetails();
        }

        public int GenerateItemCode()
        {
            return objclsDALItemMaster.GenerateItemCode();
        }

        public string GetSaleRate(int intMethodID, int intItemId,int intCompanyID)
        {
            return objclsDALItemMaster.GetSaleRate(intMethodID, intItemId, intCompanyID);
        }

        public DataTable GetSaleRateForBatch(int intItemId, int intCompanyID, int intPricingSchemeID)
        {
            return objclsDALItemMaster.GetSaleRateForBatch(intItemId, intCompanyID, intPricingSchemeID);
        }

        public DataTable GetBatchDetailsFromCostingPricing(int intItemId, int intCompanyID)
        {
            return objclsDALItemMaster.GetBatchDetailsFromCostingPricing(intItemId, intCompanyID);
        }

        public static DataTable GetUOMsByItemID(long ItemID)
        {
            return clsDALItemMaster.GetUOMsByItemID(ItemID);
        }

        public static int GetBaseUomByItem(long ItemID)
        {
            return clsDALItemMaster.GetBaseUomByItem(ItemID);
        }

        public DataTable ItemSaleHistory(long ItemID)
        {
            return objclsDALItemMaster.ItemSaleHistory(ItemID);
        }
    }
}
