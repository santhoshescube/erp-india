﻿using System;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLDimensionReference
    {
        clsDALDimensionReference objDALDimenionReference = null;

        private clsDALDimensionReference DALDimensionReference
        {
            get
            {
                if (this.objDALDimenionReference == null)
                    this.objDALDimenionReference = new clsDALDimensionReference();

                return this.objDALDimenionReference;
            }
        }

        /// <summary>
        /// Returns all dimensions
        /// </summary>
        /// <returns></returns>
        public DataTable GetDimensions()
        {
            return this.DALDimensionReference.GetDimensions();
        }
    }
}
