﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace MyBooksERP
{
    static class Program
    {
        public static FrmMain objMain ;
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            objMain = new FrmMain();
            Application.Run(objMain);
        }
    }
}
