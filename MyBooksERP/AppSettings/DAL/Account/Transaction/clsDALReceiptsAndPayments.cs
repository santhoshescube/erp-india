﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace MyBooksERP
{
    public class clsDALReceiptsAndPayments
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOReceiptsAndPayments objDTOReceiptsAndPayments { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;

        public clsDALReceiptsAndPayments(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public DateTime GetServerDate()
        {
            parameters = new ArrayList();
            return Convert.ToDateTime(MobjDataLayer.ExecuteScalar("spGetServerDate", parameters));
        }

        public DataTable getDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@CompanyID", objDTOReceiptsAndPayments.intCompanyID));
            parameters.Add(new SqlParameter("@OperationTypeID", objDTOReceiptsAndPayments.intOperationTypeID));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentTypeID", objDTOReceiptsAndPayments.intReceiptAndPaymentTypeID));
            parameters.Add(new SqlParameter("@VendorID", objDTOReceiptsAndPayments.intVendorID));
            return MobjDataLayer.ExecuteDataTable("spAccReceiptsAndPayments", parameters);
        }

        public bool SaveReceiptAndPaymentMaster(DataTable datDetails)
        {
            int intStatus = 0;
            //if (IsExistAllReferenceIDs(datDetails))
            //{
                if (objDTOReceiptsAndPayments.intReceiptAndPaymentID > 0)
                    DeleteReceiptAndPaymentDetails();

                parameters = new ArrayList();
                if (objDTOReceiptsAndPayments.intReceiptAndPaymentID == 0)
                {
                    parameters.Add(new SqlParameter("@Mode", 3)); // Insert
                    intStatus = 0;
                }
                else
                {
                    parameters.Add(new SqlParameter("@Mode", 5)); // Update
                    intStatus = 1;
                }

                parameters.Add(new SqlParameter("@ReceiptAndPaymentID", objDTOReceiptsAndPayments.intReceiptAndPaymentID));
                parameters.Add(new SqlParameter("@PaymentNumber", objDTOReceiptsAndPayments.strPaymentNumber));
                parameters.Add(new SqlParameter("@PaymentDate", objDTOReceiptsAndPayments.dtpPaymentDate));
                parameters.Add(new SqlParameter("@OperationTypeID", objDTOReceiptsAndPayments.intOperationTypeID));
                parameters.Add(new SqlParameter("@ReceiptAndPaymentTypeID", objDTOReceiptsAndPayments.intReceiptAndPaymentTypeID));
                parameters.Add(new SqlParameter("@TransactionTypeID", objDTOReceiptsAndPayments.intTransactionTypeID));
                if (objDTOReceiptsAndPayments.intTransactionTypeID == (int)PaymentModes.Bank)
                {
                    parameters.Add(new SqlParameter("@ChequeNumber", objDTOReceiptsAndPayments.strChequeNumber));
                    parameters.Add(new SqlParameter("@ChequeDate", objDTOReceiptsAndPayments.dtpChequeDate));
                    parameters.Add(new SqlParameter("@AccountID", objDTOReceiptsAndPayments.intAccountID));
                }
                parameters.Add(new SqlParameter("@TotalAmount", objDTOReceiptsAndPayments.decTotalAmount));
                parameters.Add(new SqlParameter("@CurrencyID", objDTOReceiptsAndPayments.intCurrencyID));
                parameters.Add(new SqlParameter("@CompanyID", objDTOReceiptsAndPayments.intCompanyID));
                parameters.Add(new SqlParameter("@CreatedBy", objDTOReceiptsAndPayments.intCreatedBy));
                parameters.Add(new SqlParameter("@CreatedDate", objDTOReceiptsAndPayments.dtpCreatedDate));
                parameters.Add(new SqlParameter("@Remarks", objDTOReceiptsAndPayments.strRemarks));
                parameters.Add(new SqlParameter("@IsReceipt", objDTOReceiptsAndPayments.blnIsReceipt));
                parameters.Add(new SqlParameter("@IsDirect", objDTOReceiptsAndPayments.blnIsDirect));
                if (objDTOReceiptsAndPayments.intVendorID > 0)
                    parameters.Add(new SqlParameter("@VendorID", objDTOReceiptsAndPayments.intVendorID));

                SqlParameter objParam = new SqlParameter("@OutReceiptAndPaymentID", SqlDbType.BigInt);
                objParam.Direction = ParameterDirection.Output;
                parameters.Add(objParam);

                object objOutintReceiptAndPaymentID = null;

                if (MobjDataLayer.ExecuteNonQueryWithTran("spAccReceiptsAndPayments", parameters, out objOutintReceiptAndPaymentID) != 0)
                {
                    if (objOutintReceiptAndPaymentID.ToInt64() > 0)
                    {
                        objDTOReceiptsAndPayments.intReceiptAndPaymentID = objOutintReceiptAndPaymentID.ToInt64();
                        if (datDetails != null) // Save ReceiptAndPayment Details
                        {
                            if (datDetails.Rows.Count > 0)
                            {
                                SaveReceiptAndPaymentDetails(datDetails);
                                VoucherUpdation(intStatus);
                            }
                        }
                        return true;
                    }
                }
                return false;
            //}
            //return false;
        }

        private bool SaveReceiptAndPaymentDetails(DataTable datDetails)
        {
            bool blnStatus = false;
            for (int iCounter = 0; iCounter <= datDetails.Rows.Count - 1; iCounter++)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 4));
                parameters.Add(new SqlParameter("@ReceiptAndPaymentID", objDTOReceiptsAndPayments.intReceiptAndPaymentID));
                parameters.Add(new SqlParameter("@ReferenceID", datDetails.Rows[iCounter]["ReferenceID"].ToInt64()));
                if (datDetails.Rows[iCounter]["Amount"].ToDecimal() > 0)
                    parameters.Add(new SqlParameter("@Amount", datDetails.Rows[iCounter]["Amount"].ToDecimal()));
                else
                    parameters.Add(new SqlParameter("@Amount", 0));
                if (MobjDataLayer.ExecuteNonQueryWithTran("spAccReceiptsAndPayments", parameters) != 0)
                    blnStatus = true;
                else
                    blnStatus = false;
            }
            return blnStatus;
        }

        public bool IsExistAllReferenceIDs(DataTable datDetails)
        {
            bool blnStatus = false;
            int jCounter = 0;
            if (datDetails.Rows.Count > 0)
            {
                for (int iCounter = 0; iCounter <= datDetails.Rows.Count - 1; iCounter++)
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 32));
                    parameters.Add(new SqlParameter("@ReceiptAndPaymentID", objDTOReceiptsAndPayments.intReceiptAndPaymentID));
                    parameters.Add(new SqlParameter("@OperationTypeID", objDTOReceiptsAndPayments.intOperationTypeID));
                    parameters.Add(new SqlParameter("@ReferenceID", datDetails.Rows[iCounter]["ReferenceID"].ToInt64()));
                    if (MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters).ToInt32() != 0)
                        jCounter = jCounter + 1;
                }
                if (datDetails.Rows.Count  == jCounter)
                    blnStatus = true;
                else
                    blnStatus = false;

                return blnStatus;
            }
            return blnStatus;
        }


        private bool VoucherUpdation(int intStatus)
        {
            bool blnStatus = false;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 24));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentID", objDTOReceiptsAndPayments.intReceiptAndPaymentID));
            parameters.Add(new SqlParameter("@IsReceipt", objDTOReceiptsAndPayments.blnIsReceipt));
            parameters.Add(new SqlParameter("@Status", intStatus));
            if (MobjDataLayer.ExecuteNonQueryWithTran("spAccReceiptsAndPayments", parameters) != 0)
                blnStatus = true;
            else
                blnStatus = false;
            return blnStatus;
        }

        public bool DeleteReceiptAndPaymentMaster()
        {
            if (DeleteReceiptAndPaymentDetails() == true)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 6));
                parameters.Add(new SqlParameter("@ReceiptAndPaymentID", objDTOReceiptsAndPayments.intReceiptAndPaymentID));
                parameters.Add(new SqlParameter("@OperationTypeID", objDTOReceiptsAndPayments.intOperationTypeID));
                if (MobjDataLayer.ExecuteNonQueryWithTran("spAccReceiptsAndPayments", parameters) != 0)
                {
                    VoucherUpdation(2); //Delete
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public bool DeleteReceiptAndPaymentDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentID", objDTOReceiptsAndPayments.intReceiptAndPaymentID));
            parameters.Add(new SqlParameter("@IsReceipt", objDTOReceiptsAndPayments.blnIsReceipt));
            if (MobjDataLayer.ExecuteNonQueryWithTran("spAccReceiptsAndPayments", parameters) != 0)
                return true;
            else
                return false;
        }

        public string GetPaymentNo()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@CompanyID", objDTOReceiptsAndPayments.intCompanyID));
            parameters.Add(new SqlParameter("@IsReceipt", objDTOReceiptsAndPayments.blnIsReceipt));
            return Convert.ToString(MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }

        public int GetCompanyCurrency()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@CompanyID", objDTOReceiptsAndPayments.intCompanyID));
            return Convert.ToInt32(MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }

        public int GetSupplierCurrency()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@VendorID", objDTOReceiptsAndPayments.intVendorID));
            return Convert.ToInt32(MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }

        public long GetRecordCount(int intModuleID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@IsReceipt", objDTOReceiptsAndPayments.blnIsReceipt));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@ReceiptsAndPaymentsMode", intModuleID));
            return Convert.ToInt64(MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }

        public bool DisplayReceiptsAndPayments(long intRowNum, int intModuleID)
        {
            bool blnStatus = false;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@RowNum", intRowNum));
            parameters.Add(new SqlParameter("@IsReceipt", objDTOReceiptsAndPayments.blnIsReceipt));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@ReceiptsAndPaymentsMode", intModuleID));
            DataTable datTemp = MobjDataLayer.ExecuteDataTable("spAccReceiptsAndPayments", parameters);

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    objDTOReceiptsAndPayments.intReceiptAndPaymentID = datTemp.Rows[0]["ReceiptAndPaymentID"].ToInt64();
                    objDTOReceiptsAndPayments.strPaymentNumber = datTemp.Rows[0]["PaymentNumber"].ToString();
                    objDTOReceiptsAndPayments.dtpPaymentDate = datTemp.Rows[0]["PaymentDate"].ToDateTime();
                    objDTOReceiptsAndPayments.intOperationTypeID = datTemp.Rows[0]["OperationTypeID"].ToInt32();
                    objDTOReceiptsAndPayments.intReceiptAndPaymentTypeID = datTemp.Rows[0]["ReceiptAndPaymentTypeID"].ToInt32();
                    objDTOReceiptsAndPayments.intTransactionTypeID = datTemp.Rows[0]["TransactionTypeID"].ToInt32();
                    if (datTemp.Rows[0]["ChequeNumber"].ToString() != "")
                    {
                        objDTOReceiptsAndPayments.strChequeNumber = datTemp.Rows[0]["ChequeNumber"].ToString();
                        objDTOReceiptsAndPayments.dtpChequeDate = datTemp.Rows[0]["ChequeDate"].ToDateTime();
                        objDTOReceiptsAndPayments.intAccountID = datTemp.Rows[0]["AccountID"].ToInt32();
                    }
                    objDTOReceiptsAndPayments.decTotalAmount = datTemp.Rows[0]["TotalAmount"].ToDecimal();
                    objDTOReceiptsAndPayments.intCurrencyID = datTemp.Rows[0]["CurrencyID"].ToInt32();
                    objDTOReceiptsAndPayments.intCompanyID = datTemp.Rows[0]["CompanyID"].ToInt32();
                    objDTOReceiptsAndPayments.strRemarks = datTemp.Rows[0]["Remarks"].ToString();
                    objDTOReceiptsAndPayments.blnIsDirect = datTemp.Rows[0]["IsDirect"].ToBoolean();
                   // if (datTemp.Rows[0]["VendorID"].ToInt32() > 0)
                        objDTOReceiptsAndPayments.intVendorID = datTemp.Rows[0]["VendorID"].ToInt32();

                    blnStatus = true;
                }
            }
            return blnStatus;
        }

        public DataTable DisplayReceiptsAndPaymentsDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 13));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentID", objDTOReceiptsAndPayments.intReceiptAndPaymentID));
            return MobjDataLayer.ExecuteDataTable("spAccReceiptsAndPayments", parameters);
        }

        public DataTable getReferenceIDDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 14));
            parameters.Add(new SqlParameter("@CompanyID", objDTOReceiptsAndPayments.intCompanyID));
            parameters.Add(new SqlParameter("@OperationTypeID", objDTOReceiptsAndPayments.intOperationTypeID));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentTypeID", objDTOReceiptsAndPayments.intReceiptAndPaymentTypeID));
            parameters.Add(new SqlParameter("@PaymentDate", objDTOReceiptsAndPayments.dtpPaymentDate));
            parameters.Add(new SqlParameter("@VendorID", objDTOReceiptsAndPayments.intVendorID));            
            return MobjDataLayer.ExecuteDataTable("spAccReceiptsAndPayments", parameters);
        }

        public string getReferenceNo()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 15));
            parameters.Add(new SqlParameter("@OperationTypeID", objDTOReceiptsAndPayments.intOperationTypeID));
            parameters.Add(new SqlParameter("@ReferenceID", objDTOReceiptsAndPayments.intReferenceID));
            return Convert.ToString(MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }

        public decimal getBillAmount()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 16));
            parameters.Add(new SqlParameter("@OperationTypeID", objDTOReceiptsAndPayments.intOperationTypeID));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentTypeID", objDTOReceiptsAndPayments.intReceiptAndPaymentTypeID));
            parameters.Add(new SqlParameter("@ReferenceID", objDTOReceiptsAndPayments.intReferenceID));
            parameters.Add(new SqlParameter("@PostingEnabled", ClsMainSettings.ExpensePosted));
            return Convert.ToDecimal(MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }

        public decimal getBalanceAmount()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 17));
            parameters.Add(new SqlParameter("@OperationTypeID", objDTOReceiptsAndPayments.intOperationTypeID));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentTypeID", objDTOReceiptsAndPayments.intReceiptAndPaymentTypeID));
            parameters.Add(new SqlParameter("@ReferenceID", objDTOReceiptsAndPayments.intReferenceID));
            parameters.Add(new SqlParameter("@PostingEnabled", ClsMainSettings.ExpensePosted));
            return Convert.ToDecimal(MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }

        public int DuplicatePaymentNo()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 18));
            parameters.Add(new SqlParameter("@CompanyID", objDTOReceiptsAndPayments.intCompanyID));
            parameters.Add(new SqlParameter("@PaymentNumber", objDTOReceiptsAndPayments.strPaymentNumber));
            return Convert.ToInt32(MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }

        public int DuplicatePaymentNo(long intRPID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 19));
            parameters.Add(new SqlParameter("@CompanyID", objDTOReceiptsAndPayments.intCompanyID));
            parameters.Add(new SqlParameter("@PaymentNumber", objDTOReceiptsAndPayments.strPaymentNumber));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentID", intRPID));
            return Convert.ToInt32(MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }

        public int CheckReferenceNoStatus()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 20));
            parameters.Add(new SqlParameter("@ReferenceID", objDTOReceiptsAndPayments.intReferenceID));
            parameters.Add(new SqlParameter("@OperationTypeID", objDTOReceiptsAndPayments.intOperationTypeID));
            return Convert.ToInt32(MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }

        public int DuplicateChequeNo(long intRPID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 21));
            parameters.Add(new SqlParameter("@ChequeNumber", objDTOReceiptsAndPayments.strChequeNumber));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentID", intRPID));
            return Convert.ToInt32(MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }

        public DataSet DisplayPaymentDetailsReport(long intRPID) //Payment Details Report
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 22));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentID", intRPID));
            return MobjDataLayer.ExecuteDataSet("spAccReceiptsAndPayments", parameters);
        }

        public int GetCurrencyScale(int intCurrencyID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 23));
            parameters.Add(new SqlParameter("@CurrencyID", intCurrencyID));
            return Convert.ToInt32(MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }

        public long GetRowNumber(long lngRPID, bool blnReceipt)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 25));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentID", lngRPID));
            parameters.Add(new SqlParameter("@IsReceipt", blnReceipt));
            return Convert.ToInt64(MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }

        public DataTable GetAllInvoiceIDandAmount()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 27));
            parameters.Add(new SqlParameter("@OperationTypeID", objDTOReceiptsAndPayments.intOperationTypeID));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentTypeID", objDTOReceiptsAndPayments.intReceiptAndPaymentTypeID));
            parameters.Add(new SqlParameter("@ReferenceID", objDTOReceiptsAndPayments.intReferenceID));
            return MobjDataLayer.ExecuteDataTable("spAccReceiptsAndPayments", parameters);
        }
        public decimal GetAdvanceAmountByInvoiceID()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 28));
            parameters.Add(new SqlParameter("@OperationTypeID", objDTOReceiptsAndPayments.intOperationTypeID));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentTypeID", objDTOReceiptsAndPayments.intReceiptAndPaymentTypeID));
            parameters.Add(new SqlParameter("@ReferenceID", objDTOReceiptsAndPayments.intReferenceID));
            return Convert.ToDecimal(MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }
        public DataTable getReferenceIDDetailstoDisplay()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 29));
            parameters.Add(new SqlParameter("@CompanyID", objDTOReceiptsAndPayments.intCompanyID));
            parameters.Add(new SqlParameter("@OperationTypeID", objDTOReceiptsAndPayments.intOperationTypeID));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentTypeID", objDTOReceiptsAndPayments.intReceiptAndPaymentTypeID));
            parameters.Add(new SqlParameter("@PaymentDate", objDTOReceiptsAndPayments.dtpPaymentDate));
            parameters.Add(new SqlParameter("@VendorID", objDTOReceiptsAndPayments.intVendorID));
            return MobjDataLayer.ExecuteDataTable("spAccReceiptsAndPayments", parameters);
        }

        public int GetPaymentTerms(int intReferenceNo)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 30));
            parameters.Add(new SqlParameter("@CompanyID", objDTOReceiptsAndPayments.intCompanyID));
            parameters.Add(new SqlParameter("@OperationTypeID", objDTOReceiptsAndPayments.intOperationTypeID));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentTypeID", objDTOReceiptsAndPayments.intReceiptAndPaymentTypeID));
            parameters.Add(new SqlParameter("@ReferenceID", intReferenceNo));
            return Convert.ToInt32( MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters));
        }
        public DataTable GetPreviousAmountAndType(long ReceiptAndPaymentID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 31));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentID", ReceiptAndPaymentID));
            return MobjDataLayer.ExecuteDataTable("spAccReceiptsAndPayments", parameters);
        }
        public decimal GetReceiptAmount(int ReferenceID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 33));
            parameters.Add(new SqlParameter("@ReferenceID", ReferenceID));
            return MobjDataLayer.ExecuteScalar("spAccReceiptsAndPayments", parameters).ToDecimal();
        }
    }
}
