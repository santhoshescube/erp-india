﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace MyBooksERP
{       
    public class clsDALJournalVoucher
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOJournalVoucher objDTOJournalVoucher { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;

        public clsDALJournalVoucher(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public DateTime GetServerDate()
        {
            parameters = new ArrayList();
            return Convert.ToDateTime(MobjDataLayer.ExecuteScalar("spGetServerDate", parameters));
        }

        public bool DisplayJournalVoucher(SqlDecimal intVoucherID)
        {
            bool blnStatus = false;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@VoucherID", intVoucherID));
            DataTable datTemp = MobjDataLayer.ExecuteDataTable("spAccJournalVoucher", parameters);

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    objDTOJournalVoucher.dlVoucherID = (datTemp.Rows[0]["VoucherID"].ToString()).ToDecimal();
                    objDTOJournalVoucher.strVoucherNo = datTemp.Rows[0]["VoucherNo"].ToString();
                    objDTOJournalVoucher.intVoucherTypeID = (datTemp.Rows[0]["VoucherTypeID"].ToString()).ToInt32();
                    objDTOJournalVoucher.intCompanyID = (datTemp.Rows[0]["CompanyID"].ToString()).ToInt32();
                    objDTOJournalVoucher.dtpVoucherDate = (datTemp.Rows[0]["VoucherDate"].ToString()).ToDateTime();
                    objDTOJournalVoucher.strChNo = datTemp.Rows[0]["ChNo"].ToString();
                    objDTOJournalVoucher.dtpChDate = (datTemp.Rows[0]["ChDate"].ToString()).ToDateTime();
                    objDTOJournalVoucher.strRemarks = datTemp.Rows[0]["Remarks"].ToString();
                    objDTOJournalVoucher.intJournalTypeID = (datTemp.Rows[0]["JournalTypeID"].ToString()).ToInt32();
                    objDTOJournalVoucher.intOperationTypeID = (datTemp.Rows[0]["OperationTypeID"].ToString()).ToInt32();
                    objDTOJournalVoucher.intReferenceID = (datTemp.Rows[0]["ReferenceID"].ToString()).ToInt32();
                    objDTOJournalVoucher.blnIsPDC = (datTemp.Rows[0]["IsPDC"].ToString()).ToBoolean();
                    objDTOJournalVoucher.intBankAccountID = datTemp.Rows[0]["BankAccountID"].ToInt32();

                    if (objDTOJournalVoucher.intJournalTypeID == (int)JournalTypes.Automated && (!ClsMainSettings.AutoVoucherNo))
                        objDTOJournalVoucher.strVoucherNo = datTemp.Rows[0]["ReferenceNo"].ToString();

                    blnStatus = true;
                }
            }
            return blnStatus;
        }

        public DataTable DisplayJournalVoucherDetails(SqlDecimal dlTempVoucherID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 2));
            parameters.Add(new SqlParameter("@VoucherID", dlTempVoucherID));
            return MobjDataLayer.ExecuteDataTable("spAccJournalVoucher", parameters);  
        }

        public DataTable DisplayCostCenterDetails(SqlDecimal dlTempVoucherID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 3));
            parameters.Add(new SqlParameter("@VoucherID", dlTempVoucherID));
            return MobjDataLayer.ExecuteDataTable("spAccJournalVoucher", parameters);
        }

        public bool SaveJournalVoucher(DataTable datJVDetails, DataTable datCCDetails)
        {
            if (objDTOJournalVoucher.dlVoucherID > 0)
                DeleteJournalVoucherDetails();

            parameters = new ArrayList();
            if (objDTOJournalVoucher.dlVoucherID == 0)
                parameters.Add(new SqlParameter("@Mode", 4)); // Insert
            else
                parameters.Add(new SqlParameter("@Mode", 7)); // Update

            parameters.Add(new SqlParameter("@VoucherID", objDTOJournalVoucher.dlVoucherID));
            parameters.Add(new SqlParameter("@VoucherNo", objDTOJournalVoucher.strVoucherNo));
            parameters.Add(new SqlParameter("@VoucherTypeID", objDTOJournalVoucher.intVoucherTypeID));
            parameters.Add(new SqlParameter("@CompanyID", objDTOJournalVoucher.intCompanyID));
            parameters.Add(new SqlParameter("@VoucherDate", objDTOJournalVoucher.dtpVoucherDate));
            parameters.Add(new SqlParameter("@ChNo", objDTOJournalVoucher.strChNo));
            parameters.Add(new SqlParameter("@ChDate", objDTOJournalVoucher.dtpChDate));
            parameters.Add(new SqlParameter("@Remarks", objDTOJournalVoucher.strRemarks));
            if (objDTOJournalVoucher.intJournalTypeID > 0)
                parameters.Add(new SqlParameter("@JournalTypeID", objDTOJournalVoucher.intJournalTypeID));
            if (objDTOJournalVoucher.intOperationTypeID > 0)
                parameters.Add(new SqlParameter("@OperationTypeID", objDTOJournalVoucher.intOperationTypeID));
            if (objDTOJournalVoucher.intReferenceID > 0)
                parameters.Add(new SqlParameter("@ReferenceID", objDTOJournalVoucher.intReferenceID));
            parameters.Add(new SqlParameter("@CreatedBy", objDTOJournalVoucher.intCreatedBy));
            parameters.Add(new SqlParameter("@CreatedDate", objDTOJournalVoucher.dtpCreatedDate));

            SqlParameter objParam = new SqlParameter("@OutVoucherID", SqlDbType.Decimal);
            objParam.Direction = ParameterDirection.Output;
            parameters.Add(objParam);

            object objOutVoucherID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran("spAccJournalVoucher", parameters, out objOutVoucherID) != 0)
            {
                if (objOutVoucherID.ToDecimal() > 0)
                {
                    objDTOJournalVoucher.dlVoucherID = objOutVoucherID.ToDecimal();
                    if (datJVDetails != null) // Save Journal Voucher Details
                    {
                        if (datJVDetails.Rows.Count > 0)
                        {
                            SaveJournalVoucherDetails(datJVDetails);

                            //if (objDTOJournalVoucher.dtpChDate.Date > ClsCommonSettings.GetServerDate())
                            //{
                            //    parameters = new ArrayList();
                            //    parameters.Add(new SqlParameter("@Mode", 19));
                            //    parameters.Add(new SqlParameter("@VoucherID", objDTOJournalVoucher.dlVoucherID));
                            //    MobjDataLayer.ExecuteNonQueryWithTran("spAccJournalVoucher", parameters);
                            //}
                        }
                    }
                    if (datCCDetails != null) // Save Cost Center Details
                    {
                        if (datCCDetails.Rows.Count > 0)
                            SaveCostCenterDetails(datCCDetails);
                    }
                    return true;
                }
            }
            return false;
        }

        private bool SaveJournalVoucherDetails(DataTable datJVDetails)
        {
            bool blnStatus = false;
            for (int iCounter = 0; iCounter <= datJVDetails.Rows.Count - 1; iCounter++)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 5));
                parameters.Add(new SqlParameter("@VoucherID", objDTOJournalVoucher.dlVoucherID));
                parameters.Add(new SqlParameter("@VoucherTypeID", objDTOJournalVoucher.intVoucherTypeID));
                parameters.Add(new SqlParameter("@CompanyID", objDTOJournalVoucher.intCompanyID));
                parameters.Add(new SqlParameter("@VoucherDate", objDTOJournalVoucher.dtpVoucherDate));
                parameters.Add(new SqlParameter("@AccountID", (datJVDetails.Rows[iCounter]["AccountID"].ToString()).ToInt32()));

                parameters.Add(new SqlParameter("@RemarksDet", (datJVDetails.Rows[iCounter]["Remarks"].ToString())));

                parameters.Add(new SqlParameter("@SerialNo", (datJVDetails.Rows[iCounter]["SerialNo"].ToString()).ToInt32()));
                if (datJVDetails.Rows[iCounter]["DebitAmount"].ToDecimal() > 0)
                    parameters.Add(new SqlParameter("@DebitAmount", datJVDetails.Rows[iCounter]["DebitAmount"].ToDecimal()));
                else
                    parameters.Add(new SqlParameter("@DebitAmount", 0));
                if (datJVDetails.Rows[iCounter]["CreditAmount"].ToDecimal() > 0)
                    parameters.Add(new SqlParameter("@CreditAmount", datJVDetails.Rows[iCounter]["CreditAmount"].ToDecimal()));
                else
                    parameters.Add(new SqlParameter("@CreditAmount", 0));
                parameters.Add(new SqlParameter("@CashAccountID", (int)Accounts.CashAccount));
                if (MobjDataLayer.ExecuteNonQueryWithTran("spAccJournalVoucher", parameters) != 0)
                    blnStatus = true;
                else
                    blnStatus = false;
            }
            return blnStatus;
        }

        private bool SaveCostCenterDetails(DataTable datCCDetails)
        {            
            bool blnStatus = false;
            for (int iCounter = 0; iCounter <= datCCDetails.Rows.Count - 1; iCounter++)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 6));
                parameters.Add(new SqlParameter("@CostCenterID", (datCCDetails.Rows[iCounter]["CostCenterID"].ToString()).ToInt32()));
                parameters.Add(new SqlParameter("@CostCenterAccountID", (datCCDetails.Rows[iCounter]["CostCenterAccountID"].ToString()).ToInt32()));
                parameters.Add(new SqlParameter("@VoucherID", objDTOJournalVoucher.dlVoucherID));
                parameters.Add(new SqlParameter("@VoucherDate", objDTOJournalVoucher.dtpVoucherDate));
                parameters.Add(new SqlParameter("@Amount", (datCCDetails.Rows[iCounter]["Amount"].ToString()).ToDecimal()));
                if (MobjDataLayer.ExecuteNonQueryWithTran("spAccJournalVoucher", parameters) != 0)
                    blnStatus = true;
                else
                    blnStatus = false;
            }
            return blnStatus;
        }

        public bool DeleteJournalVoucher()
        {
            if (DeleteJournalVoucherDetails() == true)
            {
                parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@VoucherID", objDTOJournalVoucher.dlVoucherID));
            if (MobjDataLayer.ExecuteNonQueryWithTran("spAccJournalVoucher", parameters) != 0)
                return true;
            else
                return false;
            }
            else
                return false;
        }

        public bool DeleteJournalVoucherDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@VoucherID", objDTOJournalVoucher.dlVoucherID));
            if (MobjDataLayer.ExecuteNonQueryWithTran("spAccJournalVoucher", parameters) != 0)
                return true;
            else
                return false;
        }

        public int GetRecordCount(int intTempCompanyID, int intTempVTypeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@CompanyID", intTempCompanyID));
            parameters.Add(new SqlParameter("@VoucherTypeID", intTempVTypeID));
            return Convert.ToInt32(MobjDataLayer.ExecuteScalar("spAccJournalVoucher", parameters));
        }

        public string GetVoucherNo(int intTempCompID, int intTempVTypeID, DateTime dtpTempVDate)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@CompanyID", intTempCompID));
            parameters.Add(new SqlParameter("@VoucherTypeID", intTempVTypeID));
            parameters.Add(new SqlParameter("@VoucherDate", dtpTempVDate));
            return Convert.ToString(MobjDataLayer.ExecuteScalar("spAccJournalVoucher", parameters));
        }

        public bool CheckAccount(int intTempAccountID) // Check Selected Account is under Cash or Bank Group
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@AccountID", intTempAccountID));
            return Convert.ToBoolean(MobjDataLayer.ExecuteScalar("spAccJournalVoucher", parameters));
        }

        public bool CheckAccountForCheque(int intTempAccountID) // Check Selected Account is under Bank Group for cheque
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 13));
            parameters.Add(new SqlParameter("@AccountID", intTempAccountID));
            return Convert.ToBoolean(MobjDataLayer.ExecuteScalar("spAccJournalVoucher", parameters));
        }

        public bool CheckAccountCostCenterApplicable(int intTempAccountID) // Check Selected Account is Cost Center Applicable
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 14));
            parameters.Add(new SqlParameter("@AccountID", intTempAccountID));
            return Convert.ToBoolean(MobjDataLayer.ExecuteScalar("spAccJournalVoucher", parameters));
        }

        public DataTable getVoucherNos(int intTempVoucherTypeID, int intTempCompanyID, string strTempVNo, string strFromDate, string strToDate)
        {
            parameters = new ArrayList();
            if (strTempVNo == "")
                parameters.Add(new SqlParameter("@Mode", 15));
            else
                parameters.Add(new SqlParameter("@Mode", 16));
            parameters.Add(new SqlParameter("@VoucherTypeID", intTempVoucherTypeID));
            parameters.Add(new SqlParameter("@CompanyID", intTempCompanyID));
            parameters.Add(new SqlParameter("@FromDate", strFromDate));
            parameters.Add(new SqlParameter("@ToDate", strToDate));
            parameters.Add(new SqlParameter("@VoucherNo", strTempVNo));
            parameters.Add(new SqlParameter("@AutoVoucherNo", ClsMainSettings.AutoVoucherNo));
            return MobjDataLayer.ExecuteDataTable("spAccJournalVoucher", parameters);
        }

        public decimal getAccountBalance(int intTempAccountID, int intTempCompanyID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 17));
            parameters.Add(new SqlParameter("@AccountID", intTempAccountID));
            parameters.Add(new SqlParameter("@CompanyID", intTempCompanyID));
            return Convert.ToDecimal(MobjDataLayer.ExecuteScalar("spAccJournalVoucher", parameters));
        }

        public DataTable getRecurringVoucherMasterDetails(int intRecurringJournalSetUpID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 20));
            parameters.Add(new SqlParameter("@ReferenceID", intRecurringJournalSetUpID));
            return MobjDataLayer.ExecuteDataTable("spAccJournalVoucher", parameters);
        }

        public DataTable getRecurringVoucherDetails(int intRecurringJournalSetUpID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 21));
            parameters.Add(new SqlParameter("@ReferenceID", intRecurringJournalSetUpID));
            return MobjDataLayer.ExecuteDataTable("spAccJournalVoucher", parameters);
        }

        public DataSet DisplayGeneralReceiptsAndPayements(long lngVoucherID)// Display report for Jounrnal Voucher
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 18));
            parameters.Add(new SqlParameter("@VoucherID", lngVoucherID));
            return MobjDataLayer.ExecuteDataSet("spAccJournalVoucher", parameters);
        }

        public DataTable getInnerGridDetails(int intCompID, string strTemp)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 22));
            parameters.Add(new SqlParameter("@CompanyID", intCompID));
            parameters.Add(new SqlParameter("@SearchCondition", strTemp));
            return MobjDataLayer.ExecuteDataTable("spAccJournalVoucher", parameters);
        }

        // For Group JV
        public bool SaveVoucherMaster(DataTable datJVDetails)
        {
            if (objDTOJournalVoucher.dlVoucherID > 0)
                DeleteJournalVoucherDetails();
            else
                objDTOJournalVoucher.strVoucherNo = GetVoucherNo(ClsCommonSettings.LoginCompanyID, (int)VoucherType.Journal, 
                    objDTOJournalVoucher.dtpVoucherDate);
            

            parameters = new ArrayList();

            if (objDTOJournalVoucher.dlVoucherID == 0)
                parameters.Add(new SqlParameter("@Mode", 4)); // Insert
            else
                parameters.Add(new SqlParameter("@Mode", 7)); // Update

            parameters.Add(new SqlParameter("@VoucherID", objDTOJournalVoucher.dlVoucherID));
            parameters.Add(new SqlParameter("@VoucherNo", objDTOJournalVoucher.strVoucherNo));
            parameters.Add(new SqlParameter("@VoucherTypeID", (int)VoucherType.Journal));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@VoucherDate", objDTOJournalVoucher.dtpVoucherDate));
            parameters.Add(new SqlParameter("@Remarks", objDTOJournalVoucher.strRemarks));
            parameters.Add(new SqlParameter("@JournalTypeID", 3)); // Automatted
            parameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDateTime()));

            SqlParameter objParam = new SqlParameter("@OutVoucherID", SqlDbType.Decimal);
            objParam.Direction = ParameterDirection.Output;
            parameters.Add(objParam);

            object objOutVoucherID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran("spAccJournalVoucher", parameters, out objOutVoucherID) != 0)
            {
                if (objOutVoucherID.ToDecimal() > 0)
                {
                    objDTOJournalVoucher.dlVoucherID = objOutVoucherID.ToDecimal();

                    if (datJVDetails != null) // Save Journal Voucher Details
                    {
                        if (datJVDetails.Rows.Count > 0)
                            SaveVoucherDetails(datJVDetails);
                    }

                    return true;
                }
            }
            return false;
        }

        private bool SaveVoucherDetails(DataTable datJVDetails)
        {
            bool blnStatus = false;
            for (int iCounter = 0; iCounter <= datJVDetails.Rows.Count - 1; iCounter++)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 5));
                parameters.Add(new SqlParameter("@VoucherID", objDTOJournalVoucher.dlVoucherID));
                parameters.Add(new SqlParameter("@VoucherTypeID", (int)VoucherType.Journal));
                parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
                parameters.Add(new SqlParameter("@VoucherDate", objDTOJournalVoucher.dtpVoucherDate));
                parameters.Add(new SqlParameter("@AccountID", (datJVDetails.Rows[iCounter]["AccountID"].ToString()).ToInt32()));

                parameters.Add(new SqlParameter("@RemarksDet", (datJVDetails.Rows[iCounter]["Remarks"].ToString())));

                parameters.Add(new SqlParameter("@SerialNo", (datJVDetails.Rows[iCounter]["SerialNo"].ToString()).ToInt32()));
                if (datJVDetails.Rows[iCounter]["DebitAmount"].ToDecimal() > 0)
                    parameters.Add(new SqlParameter("@DebitAmount", datJVDetails.Rows[iCounter]["DebitAmount"].ToDecimal()));
                else
                    parameters.Add(new SqlParameter("@DebitAmount", 0));
                if (datJVDetails.Rows[iCounter]["CreditAmount"].ToDecimal() > 0)
                    parameters.Add(new SqlParameter("@CreditAmount", datJVDetails.Rows[iCounter]["CreditAmount"].ToDecimal()));
                else
                    parameters.Add(new SqlParameter("@CreditAmount", 0));
                parameters.Add(new SqlParameter("@CashAccountID", (int)Accounts.CashAccount));
                if (MobjDataLayer.ExecuteNonQueryWithTran("spAccJournalVoucher", parameters) != 0)
                    blnStatus = true;
                else
                    blnStatus = false;
            }
            return blnStatus;
        }

        public bool SaveGroupJVMaster(DataTable datJVDetails, DataTable datGJVDetails)
        {
            if (objDTOJournalVoucher.dlVoucherID > 0)
            {
                DeleteGroupJVDetails();
                DeleteGroupJVDetDetails();
            }

            parameters = new ArrayList();

            if (objDTOJournalVoucher.dlVoucherID == 0)
                parameters.Add(new SqlParameter("@Mode", 1)); // Insert
            else
                parameters.Add(new SqlParameter("@Mode", 2)); // Update

            parameters.Add(new SqlParameter("@GroupJVID", objDTOJournalVoucher.dlVoucherID));
            parameters.Add(new SqlParameter("@JVNo", objDTOJournalVoucher.strVoucherNo));            
            parameters.Add(new SqlParameter("@JVDate", objDTOJournalVoucher.dtpVoucherDate));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@Remarks", objDTOJournalVoucher.strRemarks));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            object objOutVoucherID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran("spAccGroupJV", parameters, out objOutVoucherID) != 0)
            {
                if (objOutVoucherID.ToDecimal() > 0)
                {
                    objDTOJournalVoucher.dlVoucherID = objOutVoucherID.ToInt64();

                    if (datJVDetails != null) // Save Journal Voucher Details
                    {
                        if (datJVDetails.Rows.Count > 0)
                        {
                            SaveGroupJVDetails(datJVDetails);
                            SaveGroupJVDetDetails(datGJVDetails);
                        }
                    }

                    return true;
                }
            }
            return false;
        }

        public bool DeleteGroupJV()
        {
            if (DeleteGroupJVDetails() == true)
            {
                DeleteGroupJVDetDetails();
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 3));
                parameters.Add(new SqlParameter("@GroupJVID", objDTOJournalVoucher.dlVoucherID));
                if (MobjDataLayer.ExecuteNonQueryWithTran("spAccGroupJV", parameters) != 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        private bool SaveGroupJVDetails(DataTable datJVDetails)
        {
            bool blnStatus = false;
            for (int iCounter = 0; iCounter <= datJVDetails.Rows.Count - 1; iCounter++)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 4));
                parameters.Add(new SqlParameter("@GroupJVID", objDTOJournalVoucher.dlVoucherID));
                parameters.Add(new SqlParameter("@JVSerialNo", datJVDetails.Rows[iCounter]["JVSerialNo"].ToInt32()));
                parameters.Add(new SqlParameter("@AccountID", datJVDetails.Rows[iCounter]["AccountID"].ToInt32()));
                parameters.Add(new SqlParameter("@SerialNo", datJVDetails.Rows[iCounter]["SerialNo"].ToInt32()));

                if (datJVDetails.Rows[iCounter]["DebitAmount"].ToDecimal() > 0)
                    parameters.Add(new SqlParameter("@DebitAmount", datJVDetails.Rows[iCounter]["DebitAmount"].ToDecimal()));
                else
                    parameters.Add(new SqlParameter("@DebitAmount", 0));
                if (datJVDetails.Rows[iCounter]["CreditAmount"].ToDecimal() > 0)
                    parameters.Add(new SqlParameter("@CreditAmount", datJVDetails.Rows[iCounter]["CreditAmount"].ToDecimal()));
                else
                    parameters.Add(new SqlParameter("@CreditAmount", 0));

                parameters.Add(new SqlParameter("@Remarks", datJVDetails.Rows[iCounter]["Remarks"].ToStringCustom()));
                parameters.Add(new SqlParameter("@VoucherID", datJVDetails.Rows[iCounter]["VoucherID"].ToInt64()));

                if (MobjDataLayer.ExecuteNonQueryWithTran("spAccGroupJV", parameters) != 0)
                    blnStatus = true;
                else
                    blnStatus = false;
            }

            return blnStatus;
        }

        public bool DeleteGroupJVDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@GroupJVID", objDTOJournalVoucher.dlVoucherID));
            if (MobjDataLayer.ExecuteNonQueryWithTran("spAccGroupJV", parameters) != 0)
                return true;
            else
                return false;
        }

        public string GetGroupJVNo()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            return Convert.ToString(MobjDataLayer.ExecuteScalar("spAccGroupJV", parameters));
        }

        public DataTable getGroupJVNos(string strTempVNo)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@JVNo", strTempVNo));
            return MobjDataLayer.ExecuteDataTable("spAccGroupJV", parameters);
        }

        public DataTable getGroupJVNos(string strTempVNo, string strFromDate, string strToDate)
        {
            parameters = new ArrayList();
            if (strTempVNo != "")
                parameters.Add(new SqlParameter("@Mode", 7));
            else
                parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@FromDate", strFromDate));
            parameters.Add(new SqlParameter("@ToDate", strToDate));
            parameters.Add(new SqlParameter("@JVNo", strTempVNo));
            return MobjDataLayer.ExecuteDataTable("spAccGroupJV", parameters);
        }

        public bool DisplayGroupJV(SqlDecimal intVoucherID)
        {
            bool blnStatus = false;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@GroupJVID", intVoucherID));
            DataTable datTemp = MobjDataLayer.ExecuteDataTable("spAccGroupJV", parameters);

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    objDTOJournalVoucher.dlVoucherID = datTemp.Rows[0]["GroupJVID"].ToInt64();
                    objDTOJournalVoucher.strVoucherNo = datTemp.Rows[0]["JVNo"].ToString();
                    objDTOJournalVoucher.dtpVoucherDate = datTemp.Rows[0]["JVDate"].ToDateTime();
                    objDTOJournalVoucher.strRemarks = datTemp.Rows[0]["Remarks"].ToString();
                    blnStatus = true;
                }
            }
            return blnStatus;
        }

        public DataTable DisplayGroupJVDetails(SqlDecimal dlTempVoucherID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@GroupJVID", dlTempVoucherID));
            return MobjDataLayer.ExecuteDataTable("spAccGroupJV", parameters);
        }

        public string getMainVoucherNo()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@VoucherID", objDTOJournalVoucher.dlVoucherID));
            return MobjDataLayer.ExecuteScalar("spAccGroupJV", parameters).ToStringCustom();
        }

        public DataSet DisplayEmailGroupJV(long lngVoucherID)// Display report for Jounrnal Voucher
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@GroupJVID", lngVoucherID));
            return MobjDataLayer.ExecuteDataSet("spAccGroupJV", parameters);
        }

        private bool SaveGroupJVDetDetails(DataTable datGJVDetails)
        {
            bool blnStatus = false;
            for (int iCounter = 0; iCounter <= datGJVDetails.Rows.Count - 1; iCounter++)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 13));
                parameters.Add(new SqlParameter("@GroupJVID", objDTOJournalVoucher.dlVoucherID));
                parameters.Add(new SqlParameter("@JVSerialNo", datGJVDetails.Rows[iCounter]["JVSerialNo"].ToInt32()));
                parameters.Add(new SqlParameter("@AccountID", datGJVDetails.Rows[iCounter]["AccountID"].ToInt32()));
                parameters.Add(new SqlParameter("@SerialNo", datGJVDetails.Rows[iCounter]["SerialNo"].ToInt32()));

                if (datGJVDetails.Rows[iCounter]["DebitAmount"].ToDecimal() > 0)
                    parameters.Add(new SqlParameter("@DebitAmount", datGJVDetails.Rows[iCounter]["DebitAmount"].ToDecimal()));
                else
                    parameters.Add(new SqlParameter("@DebitAmount", 0));
                if (datGJVDetails.Rows[iCounter]["CreditAmount"].ToDecimal() > 0)
                    parameters.Add(new SqlParameter("@CreditAmount", datGJVDetails.Rows[iCounter]["CreditAmount"].ToDecimal()));
                else
                    parameters.Add(new SqlParameter("@CreditAmount", 0));

                parameters.Add(new SqlParameter("@Remarks", datGJVDetails.Rows[iCounter]["Remarks"].ToStringCustom()));
                parameters.Add(new SqlParameter("@VoucherID", datGJVDetails.Rows[iCounter]["VoucherID"].ToInt64()));

                if (MobjDataLayer.ExecuteNonQueryWithTran("spAccGroupJV", parameters) != 0)
                    blnStatus = true;
                else
                    blnStatus = false;
            }

            return blnStatus;
        }

        public bool DeleteGroupJVDetDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 14));
            parameters.Add(new SqlParameter("@GroupJVID", objDTOJournalVoucher.dlVoucherID));
            if (MobjDataLayer.ExecuteNonQueryWithTran("spAccGroupJV", parameters) != 0)
                return true;
            else
                return false;
        }

        public DataTable DisplayGroupJVDetDetails(SqlDecimal dlTempVoucherID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 15));
            parameters.Add(new SqlParameter("@GroupJVID", dlTempVoucherID));
            return MobjDataLayer.ExecuteDataTable("spAccGroupJV", parameters);
        }
    }
}