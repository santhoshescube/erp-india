﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
namespace MyBooksERP
{
    public class clsDALChartOfAccounts
    { 
        public DataLayer MObjConnection;
        public clsDTOChartOfAccounts ObjclsDTOChartOfAccounts;
        string spAccChartOfAccounts = "spAccChartOfAccounts";

        enum Mode
        {
            FillAccountHeads = 1,
            FillAccountMaster = 2,
            SaveAccountHead = 3,
            SaveAccountMaster = 4,
            UpdateAccountHead = 5,
            UpdateAccountMaster = 6,
            SelectAccountMasterDetail = 7,
            SelectAccountHeadDetails = 8
        }
        int CostCenterApplicable = 0;
        
        public DataTable LoadComboboxes(string[] saFields)
        {
            try
            {
                if (saFields.Length == 3)
                {
                    using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                    {
                        objCommonUtility.PobjDataLayer = this.MObjConnection;
                        return objCommonUtility.FillCombos(saFields);
                    }
                }
                return null;
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable FillAccountsHead()
        {
            try
            {
                ArrayList prmAccounts = new ArrayList();
                prmAccounts.Add(new SqlParameter("@Mode", (int)Mode.FillAccountHeads));

                DataTable DtAccount = MObjConnection.ExecuteDataTable(spAccChartOfAccounts, prmAccounts);
                return DtAccount;
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable FillAccounts(int iParentID, int iCompanyID)
        {
            try
            {
                ArrayList prmAccounts = new ArrayList();
                prmAccounts.Add(new SqlParameter("@Mode", (int)Mode.FillAccountMaster));
                prmAccounts.Add(new SqlParameter("@ParentID", iParentID));
                prmAccounts.Add(new SqlParameter("@CompanyID", iCompanyID));
                DataTable dat = MObjConnection.ExecuteDataTable(spAccChartOfAccounts, prmAccounts);
                return dat;
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable GetChildGroups(int iGroupID)
        {
            try
            {
                ArrayList prmAccounts = new ArrayList();
                prmAccounts.Add(new SqlParameter("@Mode", 13));
                prmAccounts.Add(new SqlParameter("@ParentID", iGroupID));
                prmAccounts.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
                DataTable datGroups = MObjConnection.ExecuteDataTable(spAccChartOfAccounts, prmAccounts);
                return datGroups;
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable GetChildAccounts(int iGroupID,int iCompanyID)
        {
            try
            {
                ArrayList prmAccounts = new ArrayList();
                prmAccounts.Add(new SqlParameter("@Mode", 14));
                prmAccounts.Add(new SqlParameter("@AccountGroupID", iGroupID));
                prmAccounts.Add(new SqlParameter("@CompanyID", iCompanyID));
               
                DataTable datAccounts = MObjConnection.ExecuteDataTable(spAccChartOfAccounts, prmAccounts);
                return datAccounts;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool SaveAccount(bool MbAccountHead, bool MbAddStatus)
        {
            try
            {
                ArrayList prmAccounts = new ArrayList();
                if (MbAddStatus)
                    prmAccounts.Add(new SqlParameter("@Mode", (MbAccountHead == true ? (int)Mode.SaveAccountHead : (int)Mode.SaveAccountMaster)));
                else
                    prmAccounts.Add(new SqlParameter("@Mode", (MbAccountHead == true ? (int)Mode.UpdateAccountHead : (int)Mode.UpdateAccountMaster)));

                if (!MbAccountHead)
                    prmAccounts.Add(new SqlParameter("@IsCostCenterApplicable", (this.ObjclsDTOChartOfAccounts.IsCostCenterApplicable == true ? CostCenterApplicable = 1 : CostCenterApplicable = 0)));
                
                prmAccounts.Add(new SqlParameter("@AccountGroupName", this.ObjclsDTOChartOfAccounts.Description.Trim()));
                prmAccounts.Add(new SqlParameter("@AccountName", this.ObjclsDTOChartOfAccounts.Description.Trim()));
                prmAccounts.Add(new SqlParameter("@Code", this.ObjclsDTOChartOfAccounts.Code));
                if (MbAddStatus && !MbAccountHead)
                    prmAccounts.Add(new SqlParameter("@AccountGroupID", this.ObjclsDTOChartOfAccounts.GLAccountID));
                if (MbAddStatus && MbAccountHead)
                    prmAccounts.Add(new SqlParameter("@ParentID", this.ObjclsDTOChartOfAccounts.GLAccountID));
                if (!MbAddStatus && MbAccountHead)
                {
                    prmAccounts.Add(new SqlParameter("@AccountGroupID", this.ObjclsDTOChartOfAccounts.GLAccountID));
                    prmAccounts.Add(new SqlParameter("@ParentID", this.ObjclsDTOChartOfAccounts.ParentID ));
                }
                if (!MbAddStatus && !MbAccountHead)
                {
                    prmAccounts.Add(new SqlParameter("@AccountID", this.ObjclsDTOChartOfAccounts.GLAccountID));
                    prmAccounts.Add(new SqlParameter("@AccountGroupID", this.ObjclsDTOChartOfAccounts.ParentID ));
                }
                prmAccounts.Add(new SqlParameter("@BSOrPL", this.ObjclsDTOChartOfAccounts.BSOrPL));
                prmAccounts.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
                prmAccounts.Add(new SqlParameter("@AccValue", this.ObjclsDTOChartOfAccounts.AccValue));

                if (MObjConnection.ExecuteNonQuery(spAccChartOfAccounts, prmAccounts) != 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool DeleteAccountHead(int iAccountID)
        {
            try
            {
                ArrayList prmAccounts = new ArrayList();
                prmAccounts.Add(new SqlParameter("@Mode", 12));
                prmAccounts.Add(new SqlParameter("@AccountID", iAccountID));

                if (MObjConnection.ExecuteNonQuery(spAccChartOfAccounts, prmAccounts) != 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool DeleteAccountGroup(int iGLAccountID)
        {
            try
            {
                ArrayList prmAccounts = new ArrayList();
                prmAccounts.Add(new SqlParameter("@Mode", 11));
                prmAccounts.Add(new SqlParameter("@AccountGroupID", iGLAccountID));

                if (MObjConnection.ExecuteNonQuery(spAccChartOfAccounts, prmAccounts) != 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool GetAccountInfo(bool MbAccountHead, int iAccountID)
        {
            try
            {
                ArrayList prmAccounts = new ArrayList();
                prmAccounts.Add(new SqlParameter("@Mode", (MbAccountHead == true ? (int)Mode.SelectAccountHeadDetails : (int)Mode.SelectAccountMasterDetail)));
                prmAccounts.Add(new SqlParameter("@AccountGroupID", iAccountID));
                prmAccounts.Add(new SqlParameter("@AccountID", iAccountID));
                SqlDataReader DrAccount = this.MObjConnection.ExecuteReader(spAccChartOfAccounts, prmAccounts, CommandBehavior.SingleResult);
                if (DrAccount.Read())
                {
                    this.ObjclsDTOChartOfAccounts = new clsDTOChartOfAccounts
                    {
                        Description = DrAccount[1].ToString(),
                        Code = DrAccount["Code"].ToString(),
                        Predefined = Convert.ToBoolean(DrAccount["IsPredefined"]),
                        GLAccountID = Convert.ToInt32(DrAccount["AccountGroupID"]),
                         
                    };
                    if (!MbAccountHead)
                        ObjclsDTOChartOfAccounts.IsCostCenterApplicable = Convert.ToBoolean(DrAccount["IsCostCenterApplicable"]);
                    ObjclsDTOChartOfAccounts.AccValue = DrAccount["AccValue"].ToDecimal();
                    DrAccount.Close();
                    return true;
                }
                else
                {
                    DrAccount.Close();
                    return false;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public bool CanDeleteAccountGroup(int iGLAccountID)
        {
            try
            {
                ArrayList prmAccounts = new ArrayList();
                prmAccounts.Add(new SqlParameter("@Mode", 9));
                prmAccounts.Add(new SqlParameter("@AccountGroupID", iGLAccountID));
                return Convert.ToBoolean(this.MObjConnection.ExecuteScalar(spAccChartOfAccounts, prmAccounts));
            }
            catch (Exception ex) { throw ex; }
        }

        public bool CanDeleteAccount(int iAccountID, bool blnChkSetting)
        {
            try
            {
                ArrayList prmAccounts = new ArrayList();
                prmAccounts.Add(new SqlParameter("@Mode", blnChkSetting == false ? 10 : 19));// 10 check AccoutID exists on AccVoucherDetails 19 check AccoutID exists on Accout Settings
                prmAccounts.Add(new SqlParameter("@AccountID", iAccountID));
                return Convert.ToBoolean(this.MObjConnection.ExecuteScalar(spAccChartOfAccounts, prmAccounts));
            }
            catch (Exception ex) { throw ex; }
        }

        public bool CheckDuplication(bool bAddStatus, string[] saValues, long iId, int iCompany, bool bAccountHead)
        {
            try
            {
                string sField = "AccountGroupID";
                string sTable = bAccountHead == true ? "AccAccountGroup" : "AccAccountMaster";
                string sCondition = "";

                if (bAddStatus && bAccountHead) // Add Account head
                    sCondition = "AccountGroupName = '" + saValues[0] + "' AND (CompanyID = " + iCompany + " OR CompanyID IS NULL)";
                else if (!bAddStatus && bAccountHead) // Edit Account head
                    sCondition = "AccountGroupName = '" + saValues[0] + "' And AccountGroupID <> " + iId + " AND (CompanyID = " + iCompany + " OR CompanyID IS NULL)";
                else if (bAddStatus && !bAccountHead)    // Add Account
                    sCondition = "AccountName = '" + saValues[0] + "' AND (CompanyID = " + iCompany + " OR CompanyID IS NULL)";
                else if (!bAddStatus && !bAccountHead)    // Edit Account
                    sCondition = "AccountName = '" + saValues[0] + "' And AccountID <> " + iId + " AND (CompanyID = " + iCompany + " OR CompanyID IS NULL)";

                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility
                {
                    PobjDataLayer = this.MObjConnection
                })
                {
                    return objCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public void DeleteAllRowsFromSTTmpAccounts()
        {
            try
            {
                string sQuery = "delete from STTmpAccounts";
                this.MObjConnection.ExecuteNonQuery(sQuery);
            }
            catch (Exception ex) { throw ex; }
        }

        public void InsertIntoSTTmpAccount(int iImageIndex, string sHead)
        {
            try
            {
                string sQuery = "insert into STTmpAccounts(AccountType, GLAccountID, Description)" +
                                    " values(" + iImageIndex + ",'" + this.ObjclsDTOChartOfAccounts.CompanyID + "','" + sHead + "')";
                this.MObjConnection.ExecuteNonQuery(sQuery);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool CheckIsPredefined(long lngRefID, bool IsGroup)
        {
            ArrayList prmAccounts = new ArrayList();
            prmAccounts.Add(new SqlParameter("@Mode", 20));

            if (IsGroup)
                prmAccounts.Add(new SqlParameter("@AccountGroupID", lngRefID));
            else
                prmAccounts.Add(new SqlParameter("@AccountID", lngRefID));

            prmAccounts.Add(new SqlParameter("@IsGroup", IsGroup));
            return Convert.ToBoolean(this.MObjConnection.ExecuteScalar(spAccChartOfAccounts, prmAccounts));
        }

        public string GetAccountCode(int intAccountGroupID)
        {
            ArrayList prmAccounts = new ArrayList();
            prmAccounts.Add(new SqlParameter("@Mode", 21));
            prmAccounts.Add(new SqlParameter("@AccountGroupID", intAccountGroupID));
            prmAccounts.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            return MObjConnection.ExecuteScalar(spAccChartOfAccounts, prmAccounts).ToStringCustom();
        }

        public bool CheckAccountCodeAutogenerated(int intAccountGroupID)
        {
            ArrayList prmAccounts = new ArrayList();
            prmAccounts.Add(new SqlParameter("@Mode", 22));
            prmAccounts.Add(new SqlParameter("@AccountGroupID", intAccountGroupID));
            prmAccounts.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            return Convert.ToBoolean(MObjConnection.ExecuteScalar(spAccChartOfAccounts, prmAccounts));
        }

        public bool CheckAccountCodeExists(int intAccountID, string strCode)
        {
            ArrayList prmAccounts = new ArrayList();
            prmAccounts.Add(new SqlParameter("@Mode", 23));
            prmAccounts.Add(new SqlParameter("@AccountID", intAccountID));
            prmAccounts.Add(new SqlParameter("@Code", strCode));
            return Convert.ToBoolean(MObjConnection.ExecuteScalar(spAccChartOfAccounts, prmAccounts));
        }
    }
}