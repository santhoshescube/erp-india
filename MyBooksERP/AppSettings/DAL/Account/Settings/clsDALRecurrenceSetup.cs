﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALRecurrenceSetup 
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTORecurrenceSetup objDTORecurrenceSetup { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;

        public clsDALRecurrenceSetup(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public void DisplayRecurranceSetup(int intRowNum)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@RowNum", intRowNum));
            DataTable datTemp = MobjDataLayer.ExecuteDataTable("spAccRecurranceSetup", parameters);

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    objDTORecurrenceSetup.intRecurrenceSetupID = (datTemp.Rows[0]["RecurrenceSetupID"].ToString()).ToInt32();
                    objDTORecurrenceSetup.strRecurrenceType = datTemp.Rows[0]["RecurrenceType"].ToString();
                    objDTORecurrenceSetup.intRecurringPatternID = (datTemp.Rows[0]["RecurringPatternID"].ToString()).ToInt32();

                    if (objDTORecurrenceSetup.intRecurrenceSetupID > 0)
                    {
                        parameters = new ArrayList();
                        parameters.Add(new SqlParameter("@Mode", 2));
                        parameters.Add(new SqlParameter("@RecurrenceSetupID", objDTORecurrenceSetup.intRecurrenceSetupID));
                        parameters.Add(new SqlParameter("@RecurringPatternID", objDTORecurrenceSetup.intRecurringPatternID));
                        datTemp = MobjDataLayer.ExecuteDataTable("spAccRecurranceSetup", parameters);

                        if (objDTORecurrenceSetup.intRecurringPatternID == 1)
                        {
                            objDTORecurrenceSetup.intDayInterval = (datTemp.Rows[0]["DayInterval"].ToString()).ToInt32();
                            objDTORecurrenceSetup.blnIsDaysDaily = (datTemp.Rows[0]["IsDays"].ToString()).ToBoolean();
                        }
                        else if (objDTORecurrenceSetup.intRecurringPatternID == 2)
                        {
                            objDTORecurrenceSetup.intWeekInterval = (datTemp.Rows[0]["WeekInterval"].ToString()).ToInt32();
                            objDTORecurrenceSetup.blnIsSunday = (datTemp.Rows[0]["IsSunday"].ToString()).ToBoolean();
                            objDTORecurrenceSetup.blnIsMonday = (datTemp.Rows[0]["IsMonday"].ToString()).ToBoolean();
                            objDTORecurrenceSetup.blnIsTuesday = (datTemp.Rows[0]["IsTuesday"].ToString()).ToBoolean();
                            objDTORecurrenceSetup.blnIsWednesday = (datTemp.Rows[0]["IsWednesday"].ToString()).ToBoolean();
                            objDTORecurrenceSetup.blnIsThursday = (datTemp.Rows[0]["IsThursday"].ToString()).ToBoolean();
                            objDTORecurrenceSetup.blnIsFriday = (datTemp.Rows[0]["IsFriday"].ToString()).ToBoolean();
                            objDTORecurrenceSetup.blnIsSaturday = (datTemp.Rows[0]["IsSaturday"].ToString()).ToBoolean();
                        }
                        else if (objDTORecurrenceSetup.intRecurringPatternID == 3)
                        {
                            objDTORecurrenceSetup.blnIsDaysMonthly = (datTemp.Rows[0]["IsDays"].ToString()).ToBoolean();
                            objDTORecurrenceSetup.intDayofMonth = (datTemp.Rows[0]["DayofMonth"].ToString()).ToInt32();
                            objDTORecurrenceSetup.intMonthInterval = (datTemp.Rows[0]["MonthInterval"].ToString()).ToInt32();
                            objDTORecurrenceSetup.intWeekDayOrderID = (datTemp.Rows[0]["WeekDayOrderID"].ToString()).ToInt32();
                            objDTORecurrenceSetup.intDayID = (datTemp.Rows[0]["DayID"].ToString()).ToInt32();
                        }
                    }
                }
            }
        }

        public bool SaveRecurrenceSetup()
        {
            parameters = new ArrayList();
            if (objDTORecurrenceSetup.intRecurrenceSetupID == 0)
                parameters.Add(new SqlParameter("@Mode", 3)); // Insert
            else
                parameters.Add(new SqlParameter("@Mode", 6)); // Update

            parameters.Add(new SqlParameter("@RecurrenceSetupID", objDTORecurrenceSetup.intRecurrenceSetupID));
            parameters.Add(new SqlParameter("@RecurrenceType", objDTORecurrenceSetup.strRecurrenceType));
            parameters.Add(new SqlParameter("@RecurringPatternID", objDTORecurrenceSetup.intRecurringPatternID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objOutRecurrenceSetupID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran("spAccRecurranceSetup", parameters, out objOutRecurrenceSetupID) != 0)
            {
                if (Convert.ToInt32(objOutRecurrenceSetupID) > 0)
                {
                    objDTORecurrenceSetup.intRecurrenceSetupID= Convert.ToInt32(objOutRecurrenceSetupID);
                    SaveRecurrenceSetupDetails();
                    return true;
                }
            }
            return false;
        }

        private bool SaveRecurrenceSetupDetails()
        {
            DeleteRecurrenceSetupDetails();

            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@RecurrenceSetupID", objDTORecurrenceSetup.intRecurrenceSetupID));
            parameters.Add(new SqlParameter("@RecurringPatternID", objDTORecurrenceSetup.intRecurringPatternID));
            if (objDTORecurrenceSetup.intRecurringPatternID == 1)
            {
                parameters.Add(new SqlParameter("@DayInterval", objDTORecurrenceSetup.intDayInterval));
                parameters.Add(new SqlParameter("@IsDaysDaily", objDTORecurrenceSetup.blnIsDaysDaily));
            }
            else if (objDTORecurrenceSetup.intRecurringPatternID == 2)
            {
                parameters.Add(new SqlParameter("@WeekInterval", objDTORecurrenceSetup.intWeekInterval));
                parameters.Add(new SqlParameter("@IsSunday", objDTORecurrenceSetup.blnIsSunday));
                parameters.Add(new SqlParameter("@IsMonday", objDTORecurrenceSetup.blnIsMonday));
                parameters.Add(new SqlParameter("@IsTuesday", objDTORecurrenceSetup.blnIsTuesday));
                parameters.Add(new SqlParameter("@IsWednesday", objDTORecurrenceSetup.blnIsWednesday));
                parameters.Add(new SqlParameter("@IsThursday", objDTORecurrenceSetup.blnIsThursday));
                parameters.Add(new SqlParameter("@IsFriday", objDTORecurrenceSetup.blnIsFriday));
                parameters.Add(new SqlParameter("@IsSaturday", objDTORecurrenceSetup.blnIsSaturday));
            }
            else if (objDTORecurrenceSetup.intRecurringPatternID == 3)
            {
                parameters.Add(new SqlParameter("@IsDaysMonthly", objDTORecurrenceSetup.blnIsDaysMonthly));
                if (objDTORecurrenceSetup.blnIsDaysMonthly == true)
                {
                    parameters.Add(new SqlParameter("@DayofMonth", objDTORecurrenceSetup.intDayofMonth));
                    parameters.Add(new SqlParameter("@MonthInterval", objDTORecurrenceSetup.intMonthInterval));
                }
                else
                {
                    parameters.Add(new SqlParameter("@WeekDayOrderID", objDTORecurrenceSetup.intWeekDayOrderID));
                    parameters.Add(new SqlParameter("@DayID", objDTORecurrenceSetup.intDayID));
                    parameters.Add(new SqlParameter("@MonthInterval", objDTORecurrenceSetup.intMonthInterval));
                }
            }
            if (MobjDataLayer.ExecuteNonQueryWithTran("spAccRecurranceSetup", parameters) != 0)
                return true;
            else
                return false;
        }
        
        private bool DeleteRecurrenceSetupDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@RecurrenceSetupID", objDTORecurrenceSetup.intRecurrenceSetupID));
            if (MobjDataLayer.ExecuteNonQueryWithTran("spAccRecurranceSetup", parameters) != 0)
                return true;
            else
                return false;
        }

        public bool DeleteRecurrenceSetup()
        {
            if (DeleteRecurrenceSetupDetails() == true)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 7));
                parameters.Add(new SqlParameter("@RecurrenceSetupID", objDTORecurrenceSetup.intRecurrenceSetupID));
                if (MobjDataLayer.ExecuteNonQueryWithTran("spAccRecurranceSetup", parameters) != 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public int GetRecordCount()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            return Convert.ToInt32(MobjDataLayer.ExecuteScalar("spAccRecurranceSetup", parameters));
        }
    }
}
