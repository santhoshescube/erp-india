﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlTypes;

namespace MyBooksERP
{
   /*************************************************************************
  * Author       : Thasni
  * Created On   : 17 Apr 2012
  * Purpose      : Create Data access layer for Journal Recurrence setup form
  * ***********************************************************************/
   public  class clsDALReurringJournalSetup:DataLayer
    {
        clsDTOReurringJournalSetup objclsDTOReurringJournalSetup = null;
       

        private string strProcedureName = "spAccJournalRecurrenceSetup";
        private List<SqlParameter> sqlParameters = null;

        public clsPager objPager { get; set; }

        public clsDALReurringJournalSetup()
        {
            objPager = new clsPager();
        }
        public clsDTOReurringJournalSetup DTOReurringJournalSetup
        {
            get
            {
                if (this.objclsDTOReurringJournalSetup == null)
                    this.objclsDTOReurringJournalSetup = new clsDTOReurringJournalSetup();
                return objclsDTOReurringJournalSetup;
            }
            set
            {
                this.objclsDTOReurringJournalSetup = value;
            }
        }

        /// <summary>
        /// To Get all Vouchers nos of journal type direct
        /// </summary>
        /// <returns>datatable </returns>
        public DataTable FillComboVoucherNo()
        {
            return this.ExecuteDataTable(this.strProcedureName, new List<SqlParameter>{
            new SqlParameter("@Mode",1),
            new SqlParameter("@CompanyID",DTOReurringJournalSetup.CompanyId),
            new SqlParameter("@VoucherTypeID",DTOReurringJournalSetup.VoucherTypeId)
           });
        }
        /// <summary>
        /// To Get all alerts
        /// </summary>
        /// <returns>datatable </returns>
        public DataTable FillAlerts(int UserId)
        {
            return this.ExecuteDataTable(this.strProcedureName, new List<SqlParameter>{
            new SqlParameter("@Mode",11),
            new SqlParameter("@UserID",UserId)
            });
        }
        /// <summary>
        /// To Get no of recurrences thar are occured
        /// </summary>
        /// <returns>datatable </returns>
        public int GetRecurredCount()
        {
            
            return this.ExecuteScalar(this.strProcedureName, new List<SqlParameter>{
            new SqlParameter("@Mode",10),
            new SqlParameter("@RecurringJournalSetupID",DTOReurringJournalSetup.RecurringJournalSetupID)
                          
           }).ToInt32();
        }
        /// <summary>
        /// To Get all Vouchers nos of journal type direct
        /// </summary>
        /// <returns>datatable </returns>
        public int SaveRecurrenceFournalSetup()
        {

            return this.ExecuteScalar(this.strProcedureName, new List<SqlParameter>{
            new SqlParameter("@Mode",2),
            new SqlParameter("@RecurringJournalSetupID",DTOReurringJournalSetup.RecurringJournalSetupID),
            new SqlParameter("@RecurrenceSetupID",DTOReurringJournalSetup.RecurrenceSetupID),           
            new SqlParameter("@VoucherID",DTOReurringJournalSetup.VoucherID),
            new SqlParameter("@StartDate",DTOReurringJournalSetup.StartDate),
            new SqlParameter("@NoofOccurance",DTOReurringJournalSetup.NoofOccurance),
            new SqlParameter("@IsActive",DTOReurringJournalSetup.Status)                
           }).ToInt32();
        }
        public void BindRecurrenceSetupInfo()
        {
            this.sqlParameters = new List<SqlParameter>();
            this.sqlParameters.Add(new SqlParameter("@Mode", 5));
            this.sqlParameters.Add(new SqlParameter("@RowIndex", objPager.RowIndex));

            DataSet ds = this.ExecuteDataSet(strProcedureName, this.sqlParameters);

            if (ds.Tables[0].Rows.Count > 0)
            {
                objPager.TotalRecords = ds.Tables[0].Rows[0]["TotalRecords"].ToInt64();
            }
            if (ds.Tables[1].Rows.Count > 0)
            {
                MapProperties(ds.Tables[1].Rows[0]);
            }

        }

        private void MapProperties(DataRow dr)
        {
            this.DTOReurringJournalSetup.RecurringJournalSetupID = dr["RecurringJournalSetupID"].ToInt32();
            this.DTOReurringJournalSetup.CompanyId = dr["CompanyID"].ToInt32();
            this.DTOReurringJournalSetup.VoucherID = (SqlDecimal)(dr["VoucherID"].ToDecimal());
            this.DTOReurringJournalSetup.VoucherTypeId = dr["VoucherTypeID"].ToInt32();
            this.DTOReurringJournalSetup.RecurrenceSetupID = dr["RecurrenceSetupID"].ToInt32();
            this.DTOReurringJournalSetup.StartDate = dr["StartDate"].ToDateTime();
            this.DTOReurringJournalSetup.NoofOccurance = dr["NoofOccurance"].ToInt32();
            this.DTOReurringJournalSetup.Status = Convert.ToBoolean(dr["IsActive"]);

        }

        public bool DeleteRecuuringJournalSetup()
        {
            this.sqlParameters = new List<SqlParameter>();
            this.sqlParameters.Add(new SqlParameter("@Mode", 5));
            this.sqlParameters.Add(new SqlParameter("@AlertSettingID", AlertSettingsTypes.RecurringJournalAlert));
            this.sqlParameters.Add(new SqlParameter("@RecurringJournalSetupID", DTOReurringJournalSetup.RecurringJournalSetupID));
            return (this.ExecuteNonQuery(this.strProcedureName,this.sqlParameters) > 0);
        }
        /// <summary>
        /// To Get Vouher details
        /// </summary>
        /// <returns>datatable </returns>
        public DataTable GetVoucherDetails()
        {
            return this.ExecuteDataTable(this.strProcedureName, new List<SqlParameter>{
            new SqlParameter("@Mode",4),
            new SqlParameter("@VoucherID",DTOReurringJournalSetup.VoucherID)           
           });
        }
        /// <summary>
        /// To Is Recurring exists
        /// </summary>
        /// <returns>datatable </returns>
        public bool IsRecurringExists()
        {
            return (this.ExecuteScalar(this.strProcedureName, new List<SqlParameter>{
            new SqlParameter("@Mode",7),
            new SqlParameter("@VoucherID",DTOReurringJournalSetup.VoucherID) ,
            new SqlParameter("@RecurringJournalSetupID",DTOReurringJournalSetup.RecurringJournalSetupID)
           })).ToInt32()>0;
        }
        /// <summary>
        /// To Is Recurring exists
        /// </summary>
        /// <returns>datatable </returns>
        public bool IsAlertUserSettingsExists()
        {
            return (this.ExecuteScalar(this.strProcedureName, new List<SqlParameter>{
            new SqlParameter("@Mode",8)           
           })).ToInt32() > 0;
        }

       

       


       

    }
}
