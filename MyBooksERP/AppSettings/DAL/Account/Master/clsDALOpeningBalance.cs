﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
namespace MyBooksERP
{
    /***********************************************************
    * Author       : Junaid
    * Created On   : 13 APRL 2012
    * Purpose      : Create Opening Balance for Accounts
    *                
    * **********************************************************/
    public class clsDALOpeningBalance
    {
        public DataLayer MObjConnection { get; set; }
        public clsDTOOpeningBalance ObjclsDTOOpeningBalance { get; set; }
        string spOpeningBalance = "spAccOpeningBalance";
        ArrayList parameters;
        enum Mode
        {
            Save=1,
            Update=2,
            select=3,
            Delete=4
        }

        public DataTable LoadComboboxes(string[] saFields)
        {
            try
            {
                if (saFields.Length == 3)
                {
                    using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                    {
                        objCommonUtility.PobjDataLayer = this.MObjConnection;
                        return objCommonUtility.FillCombos(saFields);
                    }
                }
                return null;
            }
            catch (Exception ex) { throw ex; }
        }
        public DataTable FillcboAccount()
        {
            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 9));

                return MObjConnection.ExecuteDataTable(spOpeningBalance, parameters);
            }
            catch (Exception ex) { throw ex; }
        }
        public DataTable FillcboAccount(int CompanyID)
        {
            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 10));
                parameters.Add(new SqlParameter("@CompanyID", CompanyID));

                return MObjConnection.ExecuteDataTable(spOpeningBalance, parameters);
            }
            catch (Exception ex) { throw ex; }
        }
        public DataTable FillcboAccount(int CompanyID, int AccountID)
        {
            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 11));
                parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                parameters.Add(new SqlParameter("@AccountID", AccountID));
                return MObjConnection.ExecuteDataTable(spOpeningBalance, parameters);
            }
            catch (Exception ex) { throw ex; }
        }
        public string BookStartDate(int CompanyID)
        {
            try
            {
                using (MObjConnection = new DataLayer ())
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 5));
                    parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                    return Convert.ToString(this.MObjConnection.ExecuteScalar(spOpeningBalance, parameters));
                }

            }
            catch (Exception ex) { throw ex; }
        }
        public bool SaveOpeningBalance(bool blnAddStatus,bool Credit,int OldAccountId)
        {
            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", (blnAddStatus==true ?(int)Mode.Save :(int)Mode.Update )));
                parameters.Add(new SqlParameter("@AccountID", this.ObjclsDTOOpeningBalance.AccountID));
                parameters.Add(new SqlParameter("@OldAccountId", OldAccountId));
                parameters.Add(new SqlParameter("@CompanyID", this.ObjclsDTOOpeningBalance.CompanyID));
                parameters.Add(new SqlParameter("@DebitAmount", (Credit == false ? this.ObjclsDTOOpeningBalance.OpeningBalance : 0)));
                parameters.Add(new SqlParameter("@CreditAmount", (Credit == true ? this.ObjclsDTOOpeningBalance.OpeningBalance : 0)));
                parameters.Add(new SqlParameter("@BookStartDate", this.ObjclsDTOOpeningBalance.BookStartDate));
                if (MObjConnection.ExecuteNonQuery(spOpeningBalance, parameters) != 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex) 
            {
                return false;
                throw ex; 
            }
        }
        public DataTable DisplayGridInformation()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", (int)Mode.select ));
            parameters.Add(new SqlParameter("@CompanyID",this.ObjclsDTOOpeningBalance.CompanyID ));
            return MObjConnection.ExecuteDataTable(spOpeningBalance, parameters);


        }
        public bool DeleteOpeningBalance(int AccountID, int CompanyID)
        {
            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", (int)Mode.Delete ));
                parameters.Add(new SqlParameter("@AccountID", AccountID));
                parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                if (MObjConnection.ExecuteNonQuery(spOpeningBalance, parameters) != 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }

        }
        public bool CheckDuplicate()
        {
            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 7));
                parameters.Add(new SqlParameter("@AccountID", this.ObjclsDTOOpeningBalance.AccountID));
                parameters.Add(new SqlParameter("@CompanyID", this.ObjclsDTOOpeningBalance.CompanyID));
                return Convert.ToBoolean(this.MObjConnection.ExecuteScalar(spOpeningBalance, parameters));
            }
            catch (Exception ex) { throw ex; }
        }
        public DataTable fillBalance()
        {
            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 8));
                parameters.Add(new SqlParameter("@AccountID", this.ObjclsDTOOpeningBalance.AccountID));
                parameters.Add(new SqlParameter("@CompanyID", this.ObjclsDTOOpeningBalance.CompanyID));
                return MObjConnection.ExecuteDataTable(spOpeningBalance, parameters);
            }
            catch (Exception ex) { throw ex; }
        }
    }
    
}
