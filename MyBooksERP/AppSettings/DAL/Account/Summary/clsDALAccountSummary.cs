﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyBooksERP
{
    public class clsDALAccountSummary
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOAccountSummary objDTOAccountSummary { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;
        
        public clsDALAccountSummary(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public DataSet DisplayAccountsSummaryReoprt()
        {
            string strProc = "spAccRptAccounts";
            parameters = new ArrayList();

            if (this.objDTOAccountSummary.intTempMenuID == (int)eMenuID.GeneralLedger)
                //parameters.Add(new SqlParameter("@Mode", 1));
                strProc = "spAccRptGeneralLedger";
            else if (this.objDTOAccountSummary.intTempMenuID == (int)eMenuID.DayBook)
                parameters.Add(new SqlParameter("@Mode", 2));
            else if (this.objDTOAccountSummary.intTempMenuID == (int)eMenuID.CashBook)
                parameters.Add(new SqlParameter("@Mode", 3));
            else if (this.objDTOAccountSummary.intTempMenuID == (int)eMenuID.AccountsSummary)
                parameters.Add(new SqlParameter("@Mode", 4));
            else if (this.objDTOAccountSummary.intTempMenuID == (int)eMenuID.TrialBalance)
                parameters.Add(new SqlParameter("@Mode", 5));
            else if (this.objDTOAccountSummary.intTempMenuID == (int)eMenuID.BalanceSheet)
                parameters.Add(new SqlParameter("@Mode", 16));
            else if (this.objDTOAccountSummary.intTempMenuID == (int)eMenuID.ProfitAndLossAccount)
                parameters.Add(new SqlParameter("@Mode", 15));  
            parameters.Add(new SqlParameter("@AccountID", this.objDTOAccountSummary.intTempAccountID ));
            parameters.Add(new SqlParameter("@CompanyID", this.objDTOAccountSummary .intTempCompanyID ));
            parameters.Add(new SqlParameter("@FromDate", this.objDTOAccountSummary.dtpTempFromDate ));
            parameters.Add(new SqlParameter("@ToDate", this.objDTOAccountSummary .dtpTempToDate ));
            parameters.Add(new SqlParameter("@IsGroup",this.objDTOAccountSummary.IsTempGroup ));
            return MobjDataLayer.ExecuteDataSet(strProc, parameters);
        }

        public DataTable GetChildAccounts(int intGroupID, int intSelectedCriteria)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 18));
            parameters.Add(new SqlParameter("@AccountGroupID", intGroupID));
            parameters.Add(new SqlParameter("@SelectedCriteria", intSelectedCriteria));
            return MobjDataLayer.ExecuteDataTable("spAccChartOfAccounts", parameters);
        }

        public string GetCompanyCurrency(int intCompanyID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            return Convert.ToString(MobjDataLayer.ExecuteScalar("spAccRptAccounts", parameters));
        }

        public DateTime GetCompanyBookStartDate(int intCompanyID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 13));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            return Convert.ToDateTime(MobjDataLayer.ExecuteScalar("spAccRptAccounts", parameters));
        }

        public DateTime GetFinancialStartDate(int intCompanyID, DateTime dtpDate)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 14));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ToDate", dtpDate));
            return Convert.ToDateTime(MobjDataLayer.ExecuteScalar("spAccRptAccounts", parameters));
        }
    }
}
