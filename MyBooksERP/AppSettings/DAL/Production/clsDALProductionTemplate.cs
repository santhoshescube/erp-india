﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALProductionTemplate
    {
        public clsDTOProductionTemplate objDTOProductionTemplate { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;
        string strProcName = "spInvProductionTemplate";

        public clsDALProductionTemplate(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
        }

        public bool SaveProductionTemplate(DataTable datDetails, DataTable datOther)
        {
            if (objDTOProductionTemplate.lngTemplateID > 0)
                DeleteProductionTemplateDetails();

            parameters = new ArrayList();

            if (objDTOProductionTemplate.lngTemplateID == 0)
                parameters.Add(new SqlParameter("@Mode", 1)); // Insert
            else
                parameters.Add(new SqlParameter("@Mode", 2)); // Update

            parameters.Add(new SqlParameter("@TemplateID", objDTOProductionTemplate.lngTemplateID));
            parameters.Add(new SqlParameter("@Template", objDTOProductionTemplate.strName));
            parameters.Add(new SqlParameter("@Code", objDTOProductionTemplate.strCode));
            parameters.Add(new SqlParameter("@ProductID", objDTOProductionTemplate.intProductID));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@Remarks", objDTOProductionTemplate.strRemarks));
            parameters.Add(new SqlParameter("@Total", objDTOProductionTemplate.decTotal));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));

            SqlParameter objParam = new SqlParameter("@OutTemplateID", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objOutTemplateID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters, out objOutTemplateID) != 0)
            {
                if (objOutTemplateID.ToInt64() > 0)
                {
                    objDTOProductionTemplate.lngTemplateID = objOutTemplateID.ToInt64();

                    if (datDetails != null) // Save Production Template Details
                    {
                        if (datDetails.Rows.Count > 0)
                        {
                            SaveProductionTemplateDetails(datDetails, datOther);
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        public bool DeleteProductionTemplate()
        {
            if (DeleteProductionTemplateDetails() == true)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 3));
                parameters.Add(new SqlParameter("@TemplateID", objDTOProductionTemplate.lngTemplateID));

                if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        private bool SaveProductionTemplateDetails(DataTable datDetails, DataTable datOther)
        {
            bool blnStatus = false;

            for (int iCounter = 0; iCounter <= datDetails.Rows.Count - 1; iCounter++)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 4));
                parameters.Add(new SqlParameter("@TemplateID", objDTOProductionTemplate.lngTemplateID));
                parameters.Add(new SqlParameter("@ItemID", datDetails.Rows[iCounter]["ItemID"].ToInt32()));
                parameters.Add(new SqlParameter("@UOMID", datDetails.Rows[iCounter]["UOMID"].ToInt32()));
                parameters.Add(new SqlParameter("@Quantity", datDetails.Rows[iCounter]["Quantity"].ToDecimal()));
                parameters.Add(new SqlParameter("@Rate", datDetails.Rows[iCounter]["Rate"].ToDecimal()));
                parameters.Add(new SqlParameter("@ItemTotal", datDetails.Rows[iCounter]["Total"].ToDecimal()));
                parameters.Add(new SqlParameter("@SerialNo", iCounter + 1));

                if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                    blnStatus = true;
                else
                    blnStatus = false;
            }

            for (int iCounter = 0; iCounter <= datOther.Rows.Count - 1; iCounter++)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 6));
                parameters.Add(new SqlParameter("@TemplateID", objDTOProductionTemplate.lngTemplateID));
                parameters.Add(new SqlParameter("@Particulars", datOther.Rows[iCounter]["Particulars"].ToString()));
                parameters.Add(new SqlParameter("@Amount", datOther.Rows[iCounter]["Amount"].ToDecimal()));
                parameters.Add(new SqlParameter("@SerialNo", iCounter + 1));

                if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                    blnStatus = true;
                else
                    blnStatus = false;
            }

            return blnStatus;
        }

        public bool DeleteProductionTemplateDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@TemplateID", objDTOProductionTemplate.lngTemplateID));
            MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters);

            return true;
        }

        public DataTable getSearchTemplate()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public DataSet getTemplateDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@TemplateID", objDTOProductionTemplate.lngTemplateID));
            return MobjDataLayer.ExecuteDataSet(strProcName, parameters);
        }

        public string getTemplateCode()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToStringCustom();
        }

        public bool checkTemplateCodeDuplication(long lngTempID, string strCode)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@TemplateID", lngTempID));
            parameters.Add(new SqlParameter("@Code", strCode));
            return Convert.ToBoolean(MobjDataLayer.ExecuteScalar(strProcName, parameters));
        }
    }
}
