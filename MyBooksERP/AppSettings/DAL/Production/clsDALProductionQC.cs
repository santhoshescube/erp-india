﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALProductionQC
    {
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;
        string strProcName = "spInvProductionQC";

        public clsDALProductionQC(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
        }
        public long SaveQC(DataTable datDetails, long QCID, string QCNo, string QCDate, long ProductionID, string Remarks, int InchargeID, decimal SampleQty, decimal PassedQty)
        {
            parameters = new ArrayList();

            if (QCID == 0)
                parameters.Add(new SqlParameter("@Mode", 1)); // Insert
            else
                parameters.Add(new SqlParameter("@Mode", 3)); // Update

            parameters.Add(new SqlParameter("@QCID", QCID));
            parameters.Add(new SqlParameter("@QCNo", QCNo));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@QCDate", QCDate));
            parameters.Add(new SqlParameter("@ProductionID", ProductionID));

            parameters.Add(new SqlParameter("@Remarks", Remarks));
            if (InchargeID > 0)
                parameters.Add(new SqlParameter("@InchargeID", InchargeID));
            parameters.Add(new SqlParameter("@CreatedBY", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@SampleQty", SampleQty));
            parameters.Add(new SqlParameter("@PassedQty", PassedQty));
            parameters.Add(new SqlParameter("@QCEnabled", ClsMainSettings.QCEnabled));

            SqlParameter objParam = new SqlParameter("@OutQCID", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objOutQCID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters, out objOutQCID) != 0)
            {
                if (objOutQCID.ToInt64() > 0)
                {
                    DeleteQCDetails(objOutQCID.ToInt64());
                    if (datDetails != null) // Save Production Template Details
                    {
                        if (datDetails.Rows.Count > 0)
                        {
                            SaveQCDetails(datDetails, objOutQCID.ToInt64());
                        }
                    }
                    return objOutQCID.ToInt64();
                }
            }
            return 0;
        }
        private void SaveQCDetails(DataTable datDetails, long QCID)
        {
            bool blnStatus = false;

            for (int iCounter = 0; iCounter <= datDetails.Rows.Count - 1; iCounter++)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 2));
                parameters.Add(new SqlParameter("@QCID", QCID));

                parameters.Add(new SqlParameter("@BatchNo", datDetails.Rows[iCounter]["BatchNo"].ToStringCustom()));
                parameters.Add(new SqlParameter("@SerialNo", datDetails.Rows[iCounter]["SerialNo"].ToStringCustom()));
                parameters.Add(new SqlParameter("@QCStatus", (datDetails.Rows[iCounter]["QCStatus"])));
                parameters.Add(new SqlParameter("@Remarks", datDetails.Rows[iCounter]["Remarks"].ToStringCustom()));

                if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                    blnStatus = true;
                else
                    blnStatus = false;
            }
        }
        public bool DeleteQC(long QCID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@QCID", QCID));
            parameters.Add(new SqlParameter("@QCEnabled", ClsMainSettings.QCEnabled));
            MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters);

            return true;
        }
        public bool DeleteQCDetails(long QCID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@QCID", QCID));
            MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters);

            return true;
        }
        public DataTable GetAllQC(DateTime dtFromdate, DateTime dtToDate, long ProductionID, long QCID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@QCID", QCID));
            parameters.Add(new SqlParameter("@ProductionID", ProductionID));
            parameters.Add(new SqlParameter("@FromDate", dtFromdate.Date));
            parameters.Add(new SqlParameter("@ToDate", dtToDate.Date));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);

        }
        public DataTable GetQCDetails(long QCID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@QCID", QCID));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);

        }
        public string GetQCNo()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToStringCustom();
        }
    }


}
