﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;

namespace MyBooksERP
{
    public class clsDALProductionProcess
    {
        public clsDTOProductionProcess objDTOProductionProcess { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;
        string strProcName = "spInvProductionProcess";

        public clsDALProductionProcess(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
        }

        public bool SaveProductionProcess(DataTable datDetails)
        {
            if (objDTOProductionProcess.lngProductionID > 0)
            {
                SaveProductionQtyStockUpdation(objDTOProductionProcess.lngProductionID, true);
                DeleteProductionProcessDetails();
            }

            parameters = new ArrayList();

            if (objDTOProductionProcess.lngProductionID == 0)
                parameters.Add(new SqlParameter("@Mode", 1)); // Insert
            else
                parameters.Add(new SqlParameter("@Mode", 2)); // Update

            parameters.Add(new SqlParameter("@ProductionID", objDTOProductionProcess.lngProductionID));            
            parameters.Add(new SqlParameter("@ProductionNo", objDTOProductionProcess.strProductionNo));
            parameters.Add(new SqlParameter("@ProductionDate", objDTOProductionProcess.dtProductionDate));
            parameters.Add(new SqlParameter("@TemplateID", objDTOProductionProcess.lngTemplateID));
            parameters.Add(new SqlParameter("@BatchNo", objDTOProductionProcess.strBatchNo));
            parameters.Add(new SqlParameter("@ProductID", objDTOProductionProcess.intProductID));
            parameters.Add(new SqlParameter("@Quantity", objDTOProductionProcess.decQuantity));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@InChargeID", objDTOProductionProcess.intInChargeID));
            parameters.Add(new SqlParameter("@WarehouseID", objDTOProductionProcess.intWarehouseID));
            parameters.Add(new SqlParameter("@Remarks", objDTOProductionProcess.strRemarks));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));

            SqlParameter objParam = new SqlParameter("@OutProcessID", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objOutProcessID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters, out objOutProcessID) != 0)
            {
                if (objOutProcessID.ToInt64() > 0)
                {
                    objDTOProductionProcess.lngProductionID = objOutProcessID.ToInt64();

                    if (datDetails != null) // Save Production Process Details
                    {
                        if (datDetails.Rows.Count > 0)
                        {
                            SaveProductionProcessDetails(datDetails);
                            SaveProductionQtyStockUpdation(objDTOProductionProcess.lngProductionID, false);
                            SaveUpdateStockValueUpdation(objDTOProductionProcess.lngProductionID);
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        public bool DeleteProductionProcess()
        {
            SaveProductionQtyStockUpdation(objDTOProductionProcess.lngProductionID, true);

            if (DeleteProductionProcessDetails() == true)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 3));
                parameters.Add(new SqlParameter("@ProductionID", objDTOProductionProcess.lngProductionID));
                parameters.Add(new SqlParameter("@QCEnabled", ClsMainSettings.QCEnabled));

                if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        private bool SaveProductionProcessDetails(DataTable datDetails)
        {
            bool blnStatus = false;

            for (int iCounter = 0; iCounter <= datDetails.Rows.Count - 1; iCounter++)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 4));
                parameters.Add(new SqlParameter("@ProductionID", objDTOProductionProcess.lngProductionID));
                parameters.Add(new SqlParameter("@ItemID", datDetails.Rows[iCounter]["ItemID"].ToInt32()));
                parameters.Add(new SqlParameter("@UOMID", datDetails.Rows[iCounter]["UOMID"].ToInt32()));
                parameters.Add(new SqlParameter("@ActualQuantity", datDetails.Rows[iCounter]["ActualQuantity"].ToDecimal()));
                parameters.Add(new SqlParameter("@RequiredQuantity", datDetails.Rows[iCounter]["RequiredQuantity"].ToDecimal()));
                parameters.Add(new SqlParameter("@WastageQuantity", datDetails.Rows[iCounter]["WastageQuantity"].ToDecimal()));

                if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                    blnStatus = true;
                else
                    blnStatus = false;
            }

            return blnStatus;
        }

        public bool DeleteProductionProcessDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@ProductionID", objDTOProductionProcess.lngProductionID));
            MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters);

            return true;
        }

        public DataTable getSearchProductionProcess(string strPrdNo, DateTime dtFromDate, DateTime dtToDate, long lngMID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@ProductionNo", strPrdNo));
            parameters.Add(new SqlParameter("@FromDate", dtFromDate));
            parameters.Add(new SqlParameter("@ToDate", dtToDate));
            parameters.Add(new SqlParameter("@TemplateID", lngMID));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public DataSet getProductionProcessDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@ProductionID", objDTOProductionProcess.lngProductionID));
            return MobjDataLayer.ExecuteDataSet(strProcName, parameters);
        }

        public string getProductionProcessNo()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToStringCustom();
        }

        public bool checkProductionProcessNoDuplication(long lngProductionID, string strProductionNo)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@ProductionID", lngProductionID));
            parameters.Add(new SqlParameter("@ProductionNo", strProductionNo));
            return Convert.ToBoolean(MobjDataLayer.ExecuteScalar(strProcName, parameters));
        }

        public void SaveUpdateStockValueUpdation(long lngProductionID)
        {
            if (!ClsMainSettings.QCEnabled)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 11));
                parameters.Add(new SqlParameter("@ProductionID", lngProductionID));
                parameters.Add(new SqlParameter("@QCEnabled", ClsMainSettings.QCEnabled));
                MobjDataLayer.ExecuteScalar(strProcName, parameters);
            }
        }

        public void SaveProductionQtyStockUpdation(long lngProductionID, bool blnDelete)
        {
            if (!ClsMainSettings.QCEnabled)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@ProductionID", lngProductionID));
                parameters.Add(new SqlParameter("@Delete", blnDelete));
                parameters.Add(new SqlParameter("@QCEnabled", ClsMainSettings.QCEnabled));
                MobjDataLayer.ExecuteScalar("spInvProductionQtyStockUpdation", parameters);
            }
        }

        public DataTable getBOMDetails(long lngTemplateID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@TemplateID", lngTemplateID));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }
    }
}
