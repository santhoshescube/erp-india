﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.ComponentModel;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,28 Feb 2011>
Description:	<Description,DAL for ConfigurationSettings>
================================================
*/

namespace MyBooksERP
{
   public class clsDALConfigurationSettings:IDisposable
    {
       ArrayList Parameter;

       public clsDTOConfigurationSettings ObjclsDTOConfigurationSettings { get; set; }
       public DataLayer ObjClsconnection { get; set; }
       private string strProcedureName = "spConfigurationSetting";

       //  Display information on grid

       public DataTable DisplayInformation(int ModuleID)
       {
           Parameter = new ArrayList();
           Parameter.Add(new SqlParameter("@Mode", 1));
           Parameter.Add(new SqlParameter("@ModuleID", ModuleID));
           return ObjClsconnection.ExecuteDataTable(strProcedureName, Parameter);
       }

       // To update

       public bool Update()
       {
          
           Parameter = new ArrayList();
           Parameter.Add(new SqlParameter("@Mode", 3));
           Parameter.Add(new SqlParameter("@ConfigurationID",ObjclsDTOConfigurationSettings.intConfigurationID));
           Parameter.Add(new SqlParameter("@ConfigurationValue", ObjclsDTOConfigurationSettings.strConfigurationValue));

           if (ObjClsconnection.ExecuteNonQuery(strProcedureName, Parameter) != 0)
           {
               return true;
           }
           return false;          
       }

       #region IDisposable Members

       void IDisposable.Dispose()
       {
           if (Parameter != null)
               Parameter = null;
       }

       #endregion
    }
}
