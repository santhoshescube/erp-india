﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,4 April 2011>
Description:	<Description,,DAL for Currency>
================================================
*/

namespace MyBooksERP
{
   public  class ClsDALCurrency
    {
        ArrayList parameters;
        SqlDataReader sdrDr;

        public ClsDTOCurrency objClsDTOCurrency { get; set; }
        public DataLayer objclsconnection { get; set; }

        // To fill combo

        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsconnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }

        //   SAVE AND UPDATE

        public bool SaveCurrency(bool AddStatus)
        {
            object objOutCurrencyID = 0;
            //(@CurrencyID,@CompanyID,@ExchangeRate,@ExchangeDate)		
            parameters = new ArrayList();
            if (AddStatus)
                parameters.Add(new SqlParameter("@Mode", 2));//insert
            else
                parameters.Add(new SqlParameter("@Mode", 3));//update
            parameters.Add(new SqlParameter("@CurrencyDetailID", objClsDTOCurrency.intCurrencyDetailID));
            parameters.Add(new SqlParameter("@CurrencyID",objClsDTOCurrency.intCurrencyID));
            parameters.Add(new SqlParameter("@CompanyID", objClsDTOCurrency.intCompanyID));
            parameters.Add(new SqlParameter("@ExchangeRate", objClsDTOCurrency.dblExchangeRate));
            parameters.Add(new SqlParameter("@ExchangeDate", objClsDTOCurrency.dtpExchangeDate));          
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            if (objclsconnection.ExecuteNonQuery("spExchangeCurrency", parameters, out objOutCurrencyID) != 0)
            {
                if ((int)objOutCurrencyID > 0)
                {
                    objClsDTOCurrency.intCurrencyDetailID = (int)objOutCurrencyID;
                    return true;
                }

            }
            return false;
        }
        // Display

        public bool DisplayCurrency(int intCurrencyDetailID)
        {

            parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@CurrencyDetailID ", intCurrencyDetailID));
            sdrDr = objclsconnection.ExecuteReader("spExchangeCurrency", parameters, CommandBehavior.SingleRow);

            if (sdrDr.Read())
            {
                objClsDTOCurrency.intCompanyID = Convert.ToInt32(sdrDr["CompanyID"]);
                objClsDTOCurrency.strCompanyCurrency = Convert.ToString(sdrDr["CompanyCurrency"]);
                objClsDTOCurrency.intCurrencyID = Convert.ToInt32(sdrDr["CurrencyID"]);
                objClsDTOCurrency.strCurrency = Convert.ToString(sdrDr["Currency"]);
                objClsDTOCurrency.dblExchangeRate = Convert.ToDouble(sdrDr["ExchangeRate"]);
                objClsDTOCurrency.dtpExchangeDate = Convert.ToDateTime(sdrDr["ExchangeDate"]);
                sdrDr.Close();
                return true;
            }
            return false;
        }


        public bool DisplayCompanyCurrency(int CompanyId)
        {

            parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode",5));
            parameters.Add(new SqlParameter("@CompanyID",CompanyId));
            sdrDr = objclsconnection.ExecuteReader("spExchangeCurrency", parameters, CommandBehavior.SingleRow);

            if (sdrDr.Read())
            {
                objClsDTOCurrency.strCompanyCurrency = Convert.ToString(sdrDr["CompanyCurrency"]);
                objClsDTOCurrency.intCurrencyID = Convert.ToInt32(sdrDr["CurrencyId"]);
               sdrDr.Close();
                return true;
            }
            return false;
        }


        public bool CheckDuplication (int CompanyId,int CurrencyID,string date)
        {
            parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode",8));
            parameters.Add(new SqlParameter("@CompanyID",CompanyId));
            parameters.Add(new SqlParameter("@CurrencyID",CurrencyID));
            parameters.Add(new SqlParameter("@ExchangeDate", date));
            sdrDr = objclsconnection.ExecuteReader("spExchangeCurrency", parameters, CommandBehavior.SingleRow);

            if (sdrDr.Read())
            {
                objClsDTOCurrency.dblExchangeRate = Convert.ToDouble(sdrDr["ExchangeRate"]);
                //objClsDTOCurrency.intCompanyID = Convert.ToInt32(sdrDr["CompanyID"]);
                //objClsDTOCurrency.dtpExchangeDate = Convert.ToDateTime(sdrDr["date"]);

               sdrDr.Close();
                return true;
            }
            return false;
        }

        public bool DeleteCurrency(int intCurrencyDetailID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@CurrencyDetailID ", intCurrencyDetailID));
            if (objclsconnection.ExecuteNonQuery("spExchangeCurrency", parameters) != 0)
                return true;
            else return false;
        }    

        // TO FILL Grid on loading

        public DataTable GetCurrencyDetails(int intCompanyID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objclsconnection.ExecuteDataTable("spExchangeCurrency", parameters);

        }

       //email
        public DataSet GetExchangeCurrencyReport()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@CompanyID", objClsDTOCurrency.intCompanyID));
            return objclsconnection.ExecuteDataSet("spExchangeCurrency", parameters);
        }

        public int GetCurrencyExists(int intCurrDetailId)
        {
            int intReturnvlaue = 0;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@CurrencyDetailID", intCurrDetailId));
            SqlDataReader sdr = objclsconnection.ExecuteReader("spExchangeCurrency", parameters);
            if (sdr.Read())
            {

                intReturnvlaue = Convert.ToInt32(sdr["IDExists"]);
            }
            sdr.Close();
            return intReturnvlaue;
        }
        public int GetCurrencyExists1(int intCurrDetailId)
        {
            int intReturnvlaue = 0;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@CurrencyDetailID", intCurrDetailId));
            
            SqlDataReader sdr = objclsconnection.ExecuteReader("spExchangeCurrency", parameters);
            if (sdr.Read())
            {

                intReturnvlaue = Convert.ToInt32(sdr["IDExists"]);
            }
            sdr.Close();
            return intReturnvlaue;
        }


       /* 
        =================================================
            Author:		<Author,Amal>
            Create date: <Create Date,16 August 2011>
            Description:	<Description,,Modification in Currency label>
        ================================================
       */
        public bool FillERCompanyCurrencyLabel(int intCompanyID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            SqlDataReader sdr = objclsconnection.ExecuteReader("spExchangeCurrency", parameters);
            if (sdr.Read())
            {
                objClsDTOCurrency.strShortDescription = Convert.ToString(sdr["Code"]);
                sdr.Close();
                return true;
            }
            return false;
        }
        public bool FillERSelectedCurrencyLabel(int intCurrencyID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@CurrencyID", intCurrencyID));
            SqlDataReader sdr = objclsconnection.ExecuteReader("spExchangeCurrency", parameters);
            if (sdr.Read())
            {
                objClsDTOCurrency.strShortDescription = Convert.ToString(sdr["Code"]);
                sdr.Close();
                return true;
            }
            return false;
        }
        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList Parameter = new ArrayList();
            Parameter.Add(new SqlParameter("@Mode", 4));
            Parameter.Add(new SqlParameter("@RoleID", intRoleID));
            Parameter.Add(new SqlParameter("@CompanyID", intCompanyID));
            Parameter.Add(new SqlParameter("@ControlID", intControlID));
            return objclsconnection.ExecuteDataTable("STPermissionSettings", Parameter);
        }
    }
}
