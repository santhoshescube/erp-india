﻿using System;

using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALSubCategory
    {
        public clsDTOSubCategory objDTOSubCategory { get; set; }
        public DataLayer objConnection { get; set; }

        ArrayList prmSubcategory;
        string spInvItemSubCategory = "spInvItemSubCategory";

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool GetSubCategoryInfo()
        {
            bool blnRead = false;

            prmSubcategory = new ArrayList();
            prmSubcategory.Add(new SqlParameter("@Mode", 1));
            prmSubcategory.Add(new SqlParameter("@SubCategoryID", objDTOSubCategory.intSubCategoryID));

            using (SqlDataReader sdr = objConnection.ExecuteReader(spInvItemSubCategory, prmSubcategory, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    objDTOSubCategory.intSubCategoryID = Convert.ToInt32(sdr["SubCategoryID"]);
                    objDTOSubCategory.intCategoryID = Convert.ToInt32(sdr["CategoryID"]);
                    objDTOSubCategory.strSubCategoryName = Convert.ToString(sdr["SubCategoryName"]);
                    objDTOSubCategory.strDescription = Convert.ToString(sdr["Description"]);
                    blnRead = true;
                }
                sdr.Close();
            }

            return blnRead;
        }

        public bool SaveSubCategoryInfo()
        {
            prmSubcategory = new ArrayList();
            prmSubcategory.Add(new SqlParameter("@Mode", (objDTOSubCategory.intSubCategoryID == 0 ? 2 : 3)));
            prmSubcategory.Add(new SqlParameter("@SubCategoryID", objDTOSubCategory.intSubCategoryID));
            prmSubcategory.Add(new SqlParameter("@CategoryID", objDTOSubCategory.intCategoryID));
            prmSubcategory.Add(new SqlParameter("@SubCategoryName", objDTOSubCategory.strSubCategoryName));
            prmSubcategory.Add(new SqlParameter("@Description", objDTOSubCategory.strDescription));

            SqlParameter prmReturnValue = new SqlParameter("ReturnVal", SqlDbType.Int);
            prmReturnValue.Direction = ParameterDirection.ReturnValue;
            prmSubcategory.Add(prmReturnValue);

            object objOutSubCategoryID = null;

            if (objConnection.ExecuteNonQuery(spInvItemSubCategory, prmSubcategory, out objOutSubCategoryID) != 0)
            {
                objDTOSubCategory.intSubCategoryID = Convert.ToInt32(objOutSubCategoryID);
                if (objDTOSubCategory.intSubCategoryID > 0)
                    return true;
            }
            return false;
        }
        public DataTable GetFormsInUse(int iId)
        {
            ArrayList fmParameters = new ArrayList();
            string sCondition = "name <>'InvSubCategoryReference'";
            fmParameters.Add(new SqlParameter("@PrimeryID", iId));
            fmParameters.Add(new SqlParameter("@Condition", sCondition));
            fmParameters.Add(new SqlParameter("@FieldName", "SubCategoryID"));
            return objConnection.ExecuteDataTable("spCheckIDExists", fmParameters);
        }
        public bool DeleteSubCategoryInfo()
        {
            prmSubcategory = new ArrayList();
            prmSubcategory.Add(new SqlParameter("@Mode", 4));
            prmSubcategory.Add(new SqlParameter("@SubCategoryID", objDTOSubCategory.intSubCategoryID));

            if (objConnection.ExecuteNonQuery(spInvItemSubCategory, prmSubcategory) != 0)
                return true;
            else return false;
        }

        public int GetSubCategoryCount()
        {
            int intRecordCount = 0;

            prmSubcategory = new ArrayList();
            prmSubcategory.Add(new SqlParameter("@Mode", 5));

            using (SqlDataReader sdr = objConnection.ExecuteReader(spInvItemSubCategory, prmSubcategory, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    intRecordCount = Convert.ToInt32(sdr[0]);
                }
                sdr.Close();
            }

            return intRecordCount;
        }

        public bool CheckDuplicateSubCategory()
        {
            bool blnExists = false;

            prmSubcategory = new ArrayList();
            prmSubcategory.Add(new SqlParameter("@Mode", 6));
            prmSubcategory.Add(new SqlParameter("@SubCategoryID", objDTOSubCategory.intSubCategoryID));
            prmSubcategory.Add(new SqlParameter("@CategoryID", objDTOSubCategory.intCategoryID));
            prmSubcategory.Add(new SqlParameter("@SubCategoryName", objDTOSubCategory.strSubCategoryName));

            using (SqlDataReader sdr = objConnection.ExecuteReader(spInvItemSubCategory, prmSubcategory, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    blnExists = (Convert.ToInt32(sdr[0]) == 0 ? false : true);
                }
                sdr.Close();
            }

            return blnExists;
        }

        public DataSet GetSubCategoryReport()
        {
            prmSubcategory = new ArrayList();
            prmSubcategory.Add(new SqlParameter("@Mode", 7));
            prmSubcategory.Add(new SqlParameter("@CategoryID", objDTOSubCategory.intCategoryID));

            return objConnection.ExecuteDataSet(spInvItemSubCategory, prmSubcategory);
        }
    }
}
