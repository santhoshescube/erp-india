﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,16 Feb 2011>
   Description:	<Description,,CommonRef DAL Class>
================================================
*/
namespace MyBooksERP
{
    public class clsDALCommonRef : IDisposable
    {
        ArrayList prmCommon;//for setting sql paramters

        public clsDTOCommonRef PobjclsDTOCommonRef {get; set;} // DTO Common Ref Property
        public DataLayer PobjDataLayer {get; set;} // DataLayer Property

        public DataTable DisplayInformation()
        {
            // Display Information
                prmCommon = new ArrayList();
                prmCommon.Add(new SqlParameter("@Mode", "0"));
                prmCommon.Add(new SqlParameter("@Fields", PobjclsDTOCommonRef.strFields));
                prmCommon.Add(new SqlParameter("@TableName", PobjclsDTOCommonRef.strTableName));
                prmCommon.Add(new SqlParameter("@Condition", PobjclsDTOCommonRef.strCondition));
                return PobjDataLayer.ExecuteDataSet("spCommonReferenceTransaction", prmCommon).Tables[0];
        }

        public bool AddInformation(out int intPmId)
        {
            // Adding Data
            object objPmId = 0;

                ArrayList prmCommon = new ArrayList();
                prmCommon.Add(new SqlParameter("@Mode", "1"));
                prmCommon.Add(new SqlParameter("@Fields", PobjclsDTOCommonRef.strFields));
                prmCommon.Add(new SqlParameter("@FieldValue", PobjclsDTOCommonRef.strFieldValues));
                prmCommon.Add(new SqlParameter("@TableName", PobjclsDTOCommonRef.strTableName));
                SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
                objParam.Direction = ParameterDirection.ReturnValue;
                prmCommon.Add(objParam);

                if (PobjDataLayer.ExecuteNonQuery("spCommonReferenceTransaction", prmCommon, out objPmId) != 0)
                {
                    intPmId = (int)objPmId;
                    return true;
                }
                else
                {
                    intPmId = (int)objPmId;
                    return false;
                }
        }

        public bool UpdateInformation()
        {
            // Updating Data
                prmCommon = new ArrayList();
                prmCommon.Add(new SqlParameter("@Mode", "2"));
                prmCommon.Add(new SqlParameter("@Fields", PobjclsDTOCommonRef.strFields));
                prmCommon.Add(new SqlParameter("@FieldValue", PobjclsDTOCommonRef.strFieldValues));
                prmCommon.Add(new SqlParameter("@TableName", PobjclsDTOCommonRef.strTableName));

                if (PobjDataLayer.ExecuteNonQuery("spCommonReferenceTransaction", prmCommon) != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
        }

        public bool DeleteInformation()
        {
            //Deleting data
                prmCommon = new ArrayList();
                prmCommon.Add(new SqlParameter("@Mode", "3"));
                prmCommon.Add(new SqlParameter("@Fields", PobjclsDTOCommonRef.strCondition));
                prmCommon.Add(new SqlParameter("@TableName", PobjclsDTOCommonRef.strTableName));

                if (PobjDataLayer.ExecuteNonQuery("spCommonReferenceTransaction", prmCommon) != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
        }

        #region IDisposable Members

        public void Dispose()
        {
            //Disposing 
            if (prmCommon != null)
                prmCommon = null;
        }

        #endregion
    }
}
