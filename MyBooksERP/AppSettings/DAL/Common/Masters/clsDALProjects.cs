﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using System.Collections;


namespace MyBooksERP 
{
    public class clsDALProjects
    {

        public clsDTOProjects objDTOProjects { get; set; }
        //public DataLayer objclsConnection { get; set; }

        ClsCommonUtility MObjClsCommonUtility;
        DataLayer MobjDataLayer;
        ArrayList prmProjects;
        DataTable dt;
        

        public clsDALProjects(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            //objclsConnection = new DataLayer();
           
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility MObjClsCommonUtility = new ClsCommonUtility())
                {
                    MObjClsCommonUtility.PobjDataLayer = MobjDataLayer;
                    return MObjClsCommonUtility.FillCombos(saFieldValues);
                }
            }
            else
                return null;
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", parameters);
        }

        public int GenerateCode(int intComapnyID)
        {
            int intRetValue = 0;
            string strProcedureName = string.Empty;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@CompanyID", intComapnyID));

            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spInvProjects", parameters);     
            if (sdr.Read())
            {
                if (sdr["MaxSerialNo"] != DBNull.Value)
                    intRetValue = Convert.ToInt32(sdr["MaxSerialNo"]);
                else
                    intRetValue = 0;
            }
            sdr.Close();
            return intRetValue+1;
        }

        public int AddProjects()
        {
            object objID = 0;    

            prmProjects = new ArrayList();
            prmProjects.Add(new SqlParameter("@Mode", 2));
            prmProjects.Add(new SqlParameter("@ProjectCode", objDTOProjects.strProjectCode));
            prmProjects.Add(new SqlParameter("@Description", objDTOProjects.strDscription));
            prmProjects.Add(new SqlParameter("@StartDate", objDTOProjects.strStartDate));
            prmProjects.Add(new SqlParameter("@EndDate", objDTOProjects.strEndDate));
            prmProjects.Add(new SqlParameter("@ProjectInchargeID", objDTOProjects.intProjectInchargeID));
            prmProjects.Add(new SqlParameter("@Remarks", objDTOProjects.strRemarks));
            prmProjects.Add(new SqlParameter("@EstimatedAmount", objDTOProjects.decEstimatedAmount));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmProjects.Add(objParam);
            //if (objclsConnection.ExecuteNonQuery("spInvProjects", prmProjects, out objOutVendorID) != 0)
            MobjDataLayer.ExecuteNonQuery("spInvProjects", prmProjects, out objID);
            return 0;
            
        }
        public int RecCountNavigate()
        {
            int intRecordCnt = 0;

            dt = new DataTable();
            prmProjects = new ArrayList();

            prmProjects.Add(new SqlParameter("@Mode", 3));

            dt = MobjDataLayer.ExecuteDataTable("spInvProjects", prmProjects);

            if (dt.Rows.Count > 0)
                intRecordCnt = Convert.ToInt32(dt.Rows[0]["RecordCount"]);

            return intRecordCnt;
        }

        public bool DisplayProjectsInfo(int intRowNum)
        {
            dt = new DataTable();
            prmProjects = new ArrayList();

            prmProjects.Add(new SqlParameter("@Mode", 4));
            prmProjects.Add(new SqlParameter("@RowNumber", intRowNum));
            dt = MobjDataLayer.ExecuteDataTable("spInvProjects", prmProjects);

            if(dt.Rows.Count>0)
            {
                objDTOProjects.intProjectID = Convert.ToInt32(dt.Rows[0]["ProjectID"]);
                objDTOProjects.strProjectCode = Convert.ToString(dt.Rows[0]["ProjectCode"]);
                objDTOProjects.strDscription = Convert.ToString(dt.Rows[0]["ProjectName"]);
                objDTOProjects.strStartDate = Convert.ToString(dt.Rows[0]["StartDate"]);
                objDTOProjects.strEndDate = Convert.ToString(dt.Rows[0]["EndDate"]);
                objDTOProjects.intProjectInchargeID = Convert.ToInt32(dt.Rows[0]["ProjectInchargeID"]); 
                objDTOProjects.strRemarks = Convert.ToString(dt.Rows[0]["Remarks"]);
                objDTOProjects.decEstimatedAmount = Convert.ToString(dt.Rows[0]["EstimatedAmount"]);

                return true;
            }
            return false;
        }

        public int UpdateProjectsInfo()
        {
            object objID = 0;

            prmProjects = new ArrayList();
            prmProjects.Add(new SqlParameter("@Mode", 5));
            prmProjects.Add(new SqlParameter("@ProjectID", objDTOProjects.intProjectID));
            prmProjects.Add(new SqlParameter("@ProjectCode", objDTOProjects.strProjectCode));
            prmProjects.Add(new SqlParameter("@Description", objDTOProjects.strDscription));
            prmProjects.Add(new SqlParameter("@StartDate", objDTOProjects.strStartDate));
            prmProjects.Add(new SqlParameter("@EndDate", objDTOProjects.strEndDate));
            prmProjects.Add(new SqlParameter("@ProjectInchargeID", objDTOProjects.intProjectInchargeID));
            prmProjects.Add(new SqlParameter("@Remarks", objDTOProjects.strRemarks));
            prmProjects.Add(new SqlParameter("@EstimatedAmount", objDTOProjects.decEstimatedAmount));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmProjects.Add(objParam);

            MobjDataLayer.ExecuteNonQuery("spInvProjects", prmProjects, out objID);
            return 0;

        }

        public bool DeleteProjectsInfo()
        {
            prmProjects = new ArrayList();
            prmProjects.Add(new SqlParameter("@Mode", 6));
            prmProjects.Add(new SqlParameter("ProjectID", objDTOProjects.intProjectID));

            if (MobjDataLayer.ExecuteNonQuery("spInvProjects", prmProjects)!=0)
                return true;
            else
                return false;
        }

        public bool GetProjectDetailsExists()
        {
            bool RetValue = false;
            dt = new DataTable();
            prmProjects = new ArrayList();
            prmProjects.Add(new SqlParameter("@Mode", 7));
            prmProjects.Add(new SqlParameter("@ReferenceID ", objDTOProjects.intProjectID));

            dt = MobjDataLayer.ExecuteDataTable("spInvProjects", prmProjects);

            if (dt.Rows.Count > 0)
                RetValue = true;
            else
                RetValue = false;

             return RetValue;

           
        }


        public int RecCountNavigate1(int intRoleID,int intControlID)
        {
            int intRecordCnt = 0;

            dt = new DataTable();
            prmProjects = new ArrayList();

            prmProjects.Add(new SqlParameter("@Mode", 8));
            prmProjects.Add(new SqlParameter("@RoleID",intRoleID));
            prmProjects.Add(new SqlParameter("@ControlID", intControlID));

            dt = MobjDataLayer.ExecuteDataTable("spInvProjects", prmProjects);

            if (dt.Rows.Count > 0)
                intRecordCnt = Convert.ToInt32(dt.Rows[0]["RecordCount"]);

            return intRecordCnt;
        }


        public bool DisplayProjectsInfo1(int intRoleID,int intControlID, int intRowNum)
        {
            dt = new DataTable();
            prmProjects = new ArrayList();

            prmProjects.Add(new SqlParameter("@Mode", 9));
            prmProjects.Add(new SqlParameter("@RoleID", intRoleID));
            prmProjects.Add(new SqlParameter("@ControlID", intControlID));
            prmProjects.Add(new SqlParameter("@RowNumber1", intRowNum));
            
            dt = MobjDataLayer.ExecuteDataTable("spInvProjects", prmProjects);
            if (dt.Rows.Count > 0)
            {
                objDTOProjects.intProjectID = Convert.ToInt32(dt.Rows[0]["ProjectID"]);
                objDTOProjects.strProjectCode = Convert.ToString(dt.Rows[0]["ProjectCode"]);
                objDTOProjects.strDscription = Convert.ToString(dt.Rows[0]["ProjectName"]);
                objDTOProjects.strStartDate = Convert.ToString(dt.Rows[0]["StartDate"]);
                objDTOProjects.strEndDate = Convert.ToString(dt.Rows[0]["EndDate"]);
                objDTOProjects.intProjectInchargeID = Convert.ToInt32(dt.Rows[0]["ProjectInchargeID"]);
                objDTOProjects.strRemarks = Convert.ToString(dt.Rows[0]["Remarks"]);
                objDTOProjects.decEstimatedAmount = Convert.ToString(dt.Rows[0]["EstimatedAmount"]);
                return true;
            }

            return false;
        }

        public DataSet GetEmailInfo()  //For Getting Email Information
        {
            ArrayList prmProject = new ArrayList();
            prmProject.Add(new SqlParameter("@Mode", 10));
            prmProject.Add(new SqlParameter("@ProjectID", objDTOProjects.intProjectID));
            return MobjDataLayer.ExecuteDataSet("spInvProjects", prmProject);
        }



    }
}
