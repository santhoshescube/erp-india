﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,21 Feb 2011>
Description:	<Description,,DAL for EmployeeInformation>
================================================
*/
namespace MyBooksERP 
{
    public class clsDALEmployeeInformation:IDisposable
    {
        ArrayList parameters;
        ArrayList Param;
        SqlDataReader sdrdr;

        public clsDTOEmployeeInformation objclsDTOEmployeeInformation { get; set; }
        
        public DataLayer objclsConnection { get; set; }


        // To Save and Update

        public bool SaveEmployeeInformation(bool AddStatus)
        {
            object objoutEmployeeId = 0;

            parameters = new ArrayList();
            if (AddStatus)
                parameters.Add(new SqlParameter("@Mode", 2));
            else
            parameters.Add(new SqlParameter("@Mode", 3));

            parameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));            
            parameters.Add(new SqlParameter("@EmployeeNumber",objclsDTOEmployeeInformation.strEmployeeNumber));
            parameters.Add(new SqlParameter("@SalutationID", objclsDTOEmployeeInformation.intSalutation));
            parameters.Add(new SqlParameter("@FirstName", objclsDTOEmployeeInformation.strFirstName));
            parameters.Add(new SqlParameter("@MiddleName", objclsDTOEmployeeInformation.strMiddleName));
            parameters.Add(new SqlParameter("@LastName", objclsDTOEmployeeInformation.strLastName));
            parameters.Add(new SqlParameter("@EmployeeFullName", objclsDTOEmployeeInformation.strEmployeeFullName));
            parameters.Add(new SqlParameter("@CompanyID", objclsDTOEmployeeInformation.intCompanyID));
            parameters.Add(new SqlParameter("@EmploymentTypeID", objclsDTOEmployeeInformation.intEmploymentType));
            parameters.Add(new SqlParameter("@WorkStatusID", objclsDTOEmployeeInformation.intWorkStatusID));
            parameters.Add(new SqlParameter("@DepartmentID", objclsDTOEmployeeInformation.intDepartmentID));
            parameters.Add(new SqlParameter("@DesignationID", objclsDTOEmployeeInformation.intDesignationID));
            parameters.Add(new SqlParameter("@Gender", objclsDTOEmployeeInformation.strGender));
            parameters.Add(new SqlParameter("@CountryID", objclsDTOEmployeeInformation.intCountryofOriginID));
            parameters.Add(new SqlParameter("@NationalityID", objclsDTOEmployeeInformation.intNationalityID));
            parameters.Add(new SqlParameter("@ReligionID", objclsDTOEmployeeInformation.intReligionID));
            parameters.Add(new SqlParameter("@OfficialEmailID", objclsDTOEmployeeInformation.strOfficialEmail));
            parameters.Add(new SqlParameter("@DateofBirth",objclsDTOEmployeeInformation.dtDateofBirth));
            parameters.Add(new SqlParameter("@DateofJoining", objclsDTOEmployeeInformation.dtDateofJoining));

            parameters.Add(new SqlParameter("@Address", objclsDTOEmployeeInformation.strAddress));
            parameters.Add(new SqlParameter("@Phone", objclsDTOEmployeeInformation.strPhone));
            parameters.Add(new SqlParameter("@Mobile", objclsDTOEmployeeInformation.strMobile));
            parameters.Add(new SqlParameter("@EmailID", objclsDTOEmployeeInformation.strEmail));
            parameters.Add(new SqlParameter("@LocalAddress", objclsDTOEmployeeInformation.strLocalAddress));
            parameters.Add(new SqlParameter("@LocalPhone",objclsDTOEmployeeInformation.strLocalPhone ));
            parameters.Add(new SqlParameter("@EmergencyContactAddress", objclsDTOEmployeeInformation.strEmergencyContactAddress));
            parameters.Add(new SqlParameter("@EmergencyContactPhone", objclsDTOEmployeeInformation.strEmergencyContactPhone));
            parameters.Add(new SqlParameter("@Remarks", objclsDTOEmployeeInformation.strNotes));

            // Payroll
            parameters.Add(new SqlParameter("@TransactionTypeID", objclsDTOEmployeeInformation.intTransactionTypeID));
            parameters.Add(new SqlParameter("@BankID", objclsDTOEmployeeInformation.intBankNameID));
            parameters.Add(new SqlParameter("@BankBranchID", objclsDTOEmployeeInformation.intBankBranchID));
            parameters.Add(new SqlParameter("@AccountNumber", objclsDTOEmployeeInformation.strAccountNumber));
            parameters.Add(new SqlParameter("@CompanyBankAccountID", objclsDTOEmployeeInformation.intComBankAccId));
            parameters.Add(new SqlParameter("@PersonID", objclsDTOEmployeeInformation.strPersonID));

            parameters.Add(new SqlParameter("@WorkPolicyID", objclsDTOEmployeeInformation.intPolicyID));
            parameters.Add(new SqlParameter("@LeavePolicyID", objclsDTOEmployeeInformation.intLeavePolicyID));
            parameters.Add(new SqlParameter("@VacationPolicyID", objclsDTOEmployeeInformation.intVacationPolicyID));
            parameters.Add(new SqlParameter("@SettlementPolicyID", objclsDTOEmployeeInformation.intSettlementPolicyID));
            parameters.Add(new SqlParameter("@VisaObtainedFrom", objclsDTOEmployeeInformation.intVisaObtainedFrom));

            SqlParameter objSqlParameter = new SqlParameter();
            objSqlParameter.DbType = DbType.Binary;
            objSqlParameter.Value = objclsDTOEmployeeInformation.strPassportPhotoPath;
            objSqlParameter.ParameterName = "@PassportPhoto";
            parameters.Add(objSqlParameter); 

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            if (objclsConnection.ExecuteNonQueryWithTran("spEmployee", parameters, out objoutEmployeeId) != 0)
            {
                objclsDTOEmployeeInformation.intEmployeeID = (int)objoutEmployeeId;

                if ((int)objoutEmployeeId > 0)
                {
                    objclsDTOEmployeeInformation.intEmployeeID = (int)objoutEmployeeId;
                    return true;
                }
            }
            return false;
                                    
        }

        private void UpdatePolicyHistory(int intEmpId, int intPolicyID, int intCompanyID)
        {//
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@PolicyID", intPolicyID));
            parameters.Add(new SqlParameter("@EmpID", intEmpId));
            parameters.Add(new SqlParameter("@ComID", intCompanyID));
        
            objclsConnection.ExecuteNonQueryWithTran("spPayPolicyHistoryMaster", parameters);
        }

        public bool SaveLeavePolicySummary()
        {//
            try
            {
                ArrayList parameters = new ArrayList();
                parameters.Add(new SqlParameter("@CompanyID", objclsDTOEmployeeInformation.intCompanyID));
                parameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));
                parameters.Add(new SqlParameter("@LeavePolicyID", objclsDTOEmployeeInformation.intLeavePolicyID));
                parameters.Add(new SqlParameter("@Fromdate", objclsDTOEmployeeInformation.dtDateofJoining.ToString("dd-MMM-yyyy")));
                parameters.Add(new SqlParameter("@Type", 0.ToInt32()));

                objclsConnection.ExecuteNonQueryWithTran("spPayEmployeeLeaveSummary", parameters);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int RecCountNavigate()
        {
             int intRecordcnt=0;
             parameters=new ArrayList();
             parameters.Add(new SqlParameter("@Mode",5));
             parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
             sdrdr=objclsConnection.ExecuteReader("spEmployee",parameters);
             if (sdrdr.Read())
             {
                intRecordcnt=Convert.ToInt32(sdrdr["cnt"]);
            
            }
             sdrdr.Close();
             return intRecordcnt;
        }
        public int RecCountNavigateByCompany(int intRoleID, int intCompanyID, int intControlID)
        {
            int intRecordCnt = 0;

            DataTable dt = new DataTable();
            parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 13));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));

            dt = objclsConnection.ExecuteDataTable("spEmployee", parameters);

            if (dt.Rows.Count > 0)
                intRecordCnt = Convert.ToInt32(dt.Rows[0]["Count"]);

            return intRecordCnt;
        }

        public int GetRowNumber()
        {
            int intRowNumber = 0;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode",11));
            parameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            sdrdr = objclsConnection.ExecuteReader("spEmployee", parameters);
            if (sdrdr.Read())
            {
                intRowNumber = Convert.ToInt32(sdrdr["RowNumber"]);

            }
            sdrdr.Close();
            return intRowNumber;
        }

        public bool CheckExistence(int EmployeeId)
        {

            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeId));
            sdrdr = objclsConnection.ExecuteReader("spEmployee", parameters, CommandBehavior.SingleRow);
            if (sdrdr.Read())
            {
                objclsDTOEmployeeInformation.intEmployeeID = Convert.ToInt32(sdrdr["AssignedTo"]);
                sdrdr.Close();
                return false;
            }
            return true;
        }

        // Display 

        public bool DisplayEmployeeInformation(int introwno, string strPermittedCompany)
        {
            ArrayList parameters = new ArrayList();
            string strSearchCondition = "";
            string strSearchCondition1 ="";
            strSearchCondition = " Where EM.CompanyID = " + ClsCommonSettings.LoginCompanyID + " AND";
           
            if (introwno != 0) strSearchCondition1 = " E.RowNumber=" + introwno + " AND";
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 4);
            if (strSearchCondition1.Length > 3) strSearchCondition1 = strSearchCondition1.Substring(0, strSearchCondition1.Length - 4);

            parameters.Add(new SqlParameter("@Mode", 14));
            parameters.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            parameters.Add(new SqlParameter("@SearchCondition2", strSearchCondition1));
            sdrdr = objclsConnection.ExecuteReader("spEmployee", parameters, CommandBehavior.SingleRow);
            bool blnReturnValue = ReadEmployeeInformation(sdrdr);
            sdrdr.Close();
            return blnReturnValue;                     
        }

        // Function to display employee

        public bool ReadEmployeeInformation(SqlDataReader sdrEmployee)
        {
            bool blnReturnValue;

            if (sdrEmployee.Read())
            {
                objclsDTOEmployeeInformation.intEmployeeID = Convert.ToInt32(sdrdr["EmployeeID"]);
                objclsDTOEmployeeInformation.strEmployeeNumber = Convert.ToString(sdrdr["EmployeeNumber"]);
                objclsDTOEmployeeInformation.intSalutation = Convert.ToInt32(sdrdr["SalutationID"]);
                objclsDTOEmployeeInformation.strFirstName = Convert.ToString(sdrdr["FirstName"]);
                objclsDTOEmployeeInformation.strMiddleName = Convert.ToString(sdrdr["MiddleName"]);
                objclsDTOEmployeeInformation.strLastName = Convert.ToString(sdrdr["LastName"]);
                objclsDTOEmployeeInformation.strEmployeeFullName = Convert.ToString(sdrdr["EmployeeFullName"]);
                objclsDTOEmployeeInformation.intCompanyID = Convert.ToInt32(sdrdr["CompanyID"]);
                objclsDTOEmployeeInformation.intEmploymentType = Convert.ToInt32(sdrdr["EmploymentTypeID"]);
                objclsDTOEmployeeInformation.intWorkStatusID = Convert.ToInt32(sdrdr["WorkStatusID"]);
                objclsDTOEmployeeInformation.intDepartmentID = Convert.ToInt32(sdrdr["DepartmentID"]);
                objclsDTOEmployeeInformation.intDesignationID = Convert.ToInt32(sdrdr["DesignationID"]);
                objclsDTOEmployeeInformation.strGender = Convert.ToString(sdrdr["Gender"]);
                objclsDTOEmployeeInformation.intCountryofOriginID = Convert.ToInt32(sdrdr["CountryID"]);
                objclsDTOEmployeeInformation.intNationalityID = Convert.ToInt32(sdrdr["NationalityID"]);
                objclsDTOEmployeeInformation.intReligionID = Convert.ToInt32(sdrdr["ReligionID"]);
                objclsDTOEmployeeInformation.strOfficialEmail = Convert.ToString(sdrdr["OfficialEmailID"]);
                objclsDTOEmployeeInformation.dtDateofBirth = sdrdr["DateofBirth"].ToString().ToDateTime();
                objclsDTOEmployeeInformation.dtDateofJoining = sdrdr["DateofJoining"].ToString().ToDateTime();

                objclsDTOEmployeeInformation.strAddress = Convert.ToString(sdrdr["Address"]);
                objclsDTOEmployeeInformation.strPhone = Convert.ToString(sdrdr["Phone"]);
                objclsDTOEmployeeInformation.strMobile = Convert.ToString(sdrdr["Mobile"]);
                objclsDTOEmployeeInformation.strEmail = Convert.ToString(sdrdr["EmailID"]);
                objclsDTOEmployeeInformation.strLocalAddress = Convert.ToString(sdrdr["LocalAddress"]);
                objclsDTOEmployeeInformation.strLocalPhone = Convert.ToString(sdrdr["LocalPhone"]);
                objclsDTOEmployeeInformation.strEmergencyContactAddress = Convert.ToString(sdrdr["EmergencyContactAddress"]);
                objclsDTOEmployeeInformation.strEmergencyContactPhone = Convert.ToString(sdrdr["EmergencyContactPhone"]);
                objclsDTOEmployeeInformation.strNotes = Convert.ToString(sdrdr["Remarks"]);

                //Payroll
                objclsDTOEmployeeInformation.intPolicyID = sdrdr["WorkPolicyID"].ToInt32();
                objclsDTOEmployeeInformation.intLeavePolicyID = sdrdr["LeavePolicyID"].ToInt32();
                objclsDTOEmployeeInformation.intVacationPolicyID = sdrdr["VacationPolicyID"].ToInt32();
                objclsDTOEmployeeInformation.intSettlementPolicyID = sdrdr["SettlementPolicyID"].ToInt32();
                objclsDTOEmployeeInformation.intVisaObtainedFrom = sdrdr["VisaObtainedFrom"].ToInt32();
                objclsDTOEmployeeInformation.intTransactionTypeID = sdrdr["TransactionTypeID"].ToInt32();
                objclsDTOEmployeeInformation.intBankNameID = sdrdr["BankID"].ToInt32();
                objclsDTOEmployeeInformation.intBankBranchID = sdrdr["BankBranchID"].ToInt32();
                objclsDTOEmployeeInformation.strAccountNumber = Convert.ToString(sdrdr["AccountNumber"]);
                objclsDTOEmployeeInformation.intComBankAccId = sdrdr["CompanyBankAccountID"].ToInt32();
                objclsDTOEmployeeInformation.strPersonID = Convert.ToString(sdrdr["PersonID"]);

                if ((sdrdr["PassportPhoto"]) != DBNull.Value)
                    objclsDTOEmployeeInformation.strPassportPhotoPath = (byte[])sdrdr["PassportPhoto"];
                else
                    objclsDTOEmployeeInformation.strPassportPhotoPath = null;

                blnReturnValue = true;
            }
            else
            {
                blnReturnValue = false;
            }
            return blnReturnValue;
        }


        public bool SearchItem(int SearchOption, string SearchCriteria,int RowNum, out int TotalRows)
        {           
            parameters = new ArrayList();
            TotalRows = 0;
            parameters.Add(new SqlParameter("@Mode", 6));     
            parameters.Add(new SqlParameter("@SearchOption", SearchOption));
            parameters.Add(new SqlParameter("@SearchCriteria", SearchCriteria.Replace("'", "''").ToLower()));
            parameters.Add(new SqlParameter("@RowNum", RowNum));
            sdrdr = objclsConnection.ExecuteReader("spEmployee", parameters, CommandBehavior.SingleRow);
            bool blnReturnValue = ReadEmployeeInformation(sdrdr);
            sdrdr.NextResult();   // Go to the next query resultfor getting record count
            if (sdrdr.Read())
            {
                TotalRows = Convert.ToInt32(sdrdr["RecordCount"]);
            }
            sdrdr.Close();
            return blnReturnValue;
        }


        // Delete EmployeeInformation

        public bool DeleteEmployeeInformation()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode",4));
            parameters.Add(new SqlParameter("@EmployeeID",objclsDTOEmployeeInformation.intEmployeeID));
            if (objclsConnection.ExecuteNonQueryWithTran("spEmployee",parameters)!=0)
            {
            
            return true;
            }
            return false;
        }

        // To check the Existence of EmployeeID

        public DataTable CheckExistReferences()
        {

            string strCondition1 = "'EmployeeMaster','EmployeeDetails','PayWorkPolicyHistoryMaster','PayEmployeeLeaveSummary'";
            string strCondition2 = "'AssignedTo','EmployeeID','InChargeID'";
            
            Param = new ArrayList();
            Param.Add(new SqlParameter("@Mode", 1));
            Param.Add(new SqlParameter("@Value",objclsDTOEmployeeInformation.intEmployeeID));

            Param.Add(new SqlParameter("@ColumnsIn",strCondition2));
            Param.Add(new SqlParameter("@TablesNotIn",strCondition1));

            return objclsConnection.ExecuteDataSet("spCheckValueExists", Param).Tables[0];
        }
            
        
         //To Fill Combobox
         public DataTable FillCombos(string[] sarFieldValues)
         {
             if (sarFieldValues.Length == 3)
             {
                 using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                 {
                     objCommonUtility.PobjDataLayer = objclsConnection;
                     return objCommonUtility.FillCombos(sarFieldValues);
                 }

             }
             return null;
         }
         public DataTable FillPolicyCombo(string sQuery)
         {
             return objclsConnection.ExecuteDataTable(sQuery);
         }

        // To check Validemail 

         public bool CheckValidEmail(string sEmailAddress)
         {
             using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
             {
                 return objCommonUtility.CheckValidEmail(sEmailAddress);
             }
         }

         public bool CheckDuplication(bool blnAddStatus, string[] saValues, int intId, int intCompanyID)
         {
             string sField = "EmployeeNumber";
             string sTable = "EmployeeMaster";
             string sCondition = "";

             if (blnAddStatus) // Add mode
                 sCondition = "EmployeeNumber='" + saValues[0] + "' and CompanyID= " + intCompanyID + "";
             else // Edit mode
                 sCondition = "EmployeeNumber='" + saValues[0] + "' and CompanyID= " + intCompanyID  +" and EmployeeID <> " + intId + "";

             using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
             {
                 objCommonUtility.PobjDataLayer = objclsConnection;
                 return objCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
             }
         }

         public DataSet GetEmployeeReport()
         {
             parameters = new ArrayList();
             parameters.Add(new SqlParameter("@Mode", 7));
             parameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));
             return objclsConnection.ExecuteDataSet("spEmployee", parameters);
         }

         public int GenerateEmployeeCode()
         {
             int intRetValue = 0;
             ArrayList parameters = new ArrayList();
             parameters.Add(new SqlParameter("@Mode", 9));
             parameters.Add(new SqlParameter("@CompanyID", objclsDTOEmployeeInformation.intCompanyID));
             SqlDataReader sdr = objclsConnection.ExecuteReader("spEmployee", parameters);
             if (sdr.Read())
             {
                 if (sdr["ReturnValue"] != DBNull.Value)
                     intRetValue = Convert.ToInt32(sdr["ReturnValue"]);
                 else 
                     intRetValue = 0;
             }
             sdr.Close();
             return intRetValue;
         }
         public int GetCurRowNumber()
         {


             ArrayList prmWorkPolicy = new ArrayList();
             prmWorkPolicy = new ArrayList();
             prmWorkPolicy.Add(new SqlParameter("@Mode", "GCRC"));
             prmWorkPolicy.Add(new SqlParameter("@PolicyID", objclsDTOEmployeeInformation.intPolicyID ));

             return Convert.ToInt32(objclsConnection.ExecuteScalar("spPayWorkPolicy", prmWorkPolicy));


         }
         public void SaveEmployeeAccount(int intEmpId, int intCompID, string strEmpName)
         {
             ArrayList parameters = new ArrayList();
             parameters.Add(new SqlParameter("@Mode", 12));
             parameters.Add(new SqlParameter("@EmployeeID", intEmpId));
             parameters.Add(new SqlParameter("@CompanyID", intCompID));
             parameters.Add(new SqlParameter("@FirstName", strEmpName));
             objclsConnection.ExecuteNonQueryWithTran("spEmployee", parameters);
         }
         public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
         {
             ArrayList prmEmployee = new ArrayList();
             prmEmployee.Add(new SqlParameter("@Mode", 4));
             prmEmployee.Add(new SqlParameter("@RoleID", intRoleID));
             prmEmployee.Add(new SqlParameter("@CompanyID", intCompanyID));
             prmEmployee.Add(new SqlParameter("@ControlID", intControlID));
             return objclsConnection.ExecuteDataTable("STPermissionSettings", prmEmployee);
         }
         public DataTable GetAutoCompleteList(SearchBy searchBy)
         {
             return this.objclsConnection.ExecuteDataTable("spEmployee", new List<SqlParameter> { 
                new SqlParameter("@Mode", 15),
                new SqlParameter("@SearchIndex", (int)searchBy)
            });
         }

         public bool DeleteLeavePolicy()
         {
             try
             {
                 ArrayList parameters = new ArrayList();
                 parameters.Add(new SqlParameter("@Mode", 16));
                 parameters.Add(new SqlParameter("@CompanyID", objclsDTOEmployeeInformation.intCompanyID));
                 parameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));

                 if (objclsConnection.ExecuteNonQuery("spEmployee", parameters) != 0)
                    return true;
                    else
                    return false;

             }
             catch (Exception ex)
             {
                 return false;
             }
         }

         public DataTable CheckCarryForwardLeaveExists()
         {
             return this.objclsConnection.ExecuteDataTable("spEmployee", new List<SqlParameter> {
                 new SqlParameter("@Mode",16),new SqlParameter("@EmployeeID",objclsDTOEmployeeInformation.intEmployeeID)});
         }
         public DataSet UpdateLeaveSummary(int iLeaveTypeID,int iLeavePolicyID)
         {

             ArrayList prmEmployee = new ArrayList();
             ArrayList rEmployee = new ArrayList();

             prmEmployee.Add(new SqlParameter("@Type", 0.ToInt32()));
             prmEmployee.Add(new SqlParameter("@CompanyID", objclsDTOEmployeeInformation.intCompanyID ));
             prmEmployee.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID ));
             prmEmployee.Add(new SqlParameter("@LeavetypeID", iLeaveTypeID));
             prmEmployee.Add(new SqlParameter("@Leavepolicyid", iLeavePolicyID));
             prmEmployee.Add(new SqlParameter("@Fromdate", new clsConnection().GetSysDate()));
             prmEmployee.Add(new SqlParameter("@Todate", new clsConnection().GetSysDate()));
             return objclsConnection.ExecuteDataSet("spPayEmployeeLeaveSummaryFromLeaveEntry", prmEmployee);
         }


         public string GetCompanyFinYearStartDate(int intCompanyID)
         {
             string strFinYearDate = "";
             strFinYearDate = this.objclsConnection.ExecuteScalar("spEmployee", new List<SqlParameter>{
                new SqlParameter("@Mode",17),new SqlParameter("@CompanyID",intCompanyID)}).ToStringCustom();
             return strFinYearDate;
         }


        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (parameters != null)
                parameters = null;

            if (sdrdr != null)
                sdrdr.Dispose();

            if (Param != null)
                Param = null;
                       
        }

        #endregion
       }
       

    }

