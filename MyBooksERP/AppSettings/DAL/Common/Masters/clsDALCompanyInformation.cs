﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Data;

/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,16 Feb 2011>
   Description:	<Description,,CompanyInformation DAL Class>
================================================
*/
namespace MyBooksERP
{
    public class clsDALCompanyInformation : IDisposable
    {
        ArrayList prmCompany; // for setting Sql parameters
        ClsCommonUtility MObjClsCommonUtility;
        DataLayer MobjDataLayer;

        public clsDTOCompanyInformation PobjclsDTOCompanyInformation {get; set;} // DTO Company Information Property

        public clsDALCompanyInformation(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public int RecCountNavigate(int intCompanyID)
        {
            //Function for recount navigate
            int intRecordCnt;
            intRecordCnt = 0;
            string strTSQL;
            SqlDataReader sdr;

            if (intCompanyID == 0)
                strTSQL = "Select isnull(count(1),0) as cnt from CompanyMaster";
            else
                strTSQL = "Select isnull(count(1),0) as cnt from CompanyMaster WHERE CompanyID = " + intCompanyID;

            sdr = MobjDataLayer.ExecuteReader(strTSQL);
         
            if (sdr.Read())
            {
                intRecordCnt = Convert.ToInt32(sdr["cnt"]);
            }
            else
            {
                intRecordCnt = 0;
            }
            sdr.Close();
            return intRecordCnt;
        }

        public bool SaveCompany(bool AddStatus)
        {
            // Function for adding and updating
            object objOutCompanyID = 0;
            prmCompany = new ArrayList();

            if (AddStatus)
                prmCompany.Add(new SqlParameter("@Mode", "2"));
            else
                prmCompany.Add(new SqlParameter("@Mode", "3"));
            prmCompany.Add(new SqlParameter("@CompanyID", PobjclsDTOCompanyInformation.intCompanyID));
            prmCompany.Add(new SqlParameter("@ParentID", PobjclsDTOCompanyInformation.intParentID));
            prmCompany.Add(new SqlParameter("@IsBranch ", PobjclsDTOCompanyInformation.blnCompanyBranchIndicator));
            prmCompany.Add(new SqlParameter("@CompanyName", PobjclsDTOCompanyInformation.strName));
            prmCompany.Add(new SqlParameter("@ShortName", PobjclsDTOCompanyInformation.strShortName));
            prmCompany.Add(new SqlParameter("@POBox", PobjclsDTOCompanyInformation.strPOBox));
            prmCompany.Add(new SqlParameter("@Road", PobjclsDTOCompanyInformation.strRoad));
            prmCompany.Add(new SqlParameter("@Area", PobjclsDTOCompanyInformation.strArea));
            prmCompany.Add(new SqlParameter("@Block", PobjclsDTOCompanyInformation.strBlock));
            prmCompany.Add(new SqlParameter("@City", PobjclsDTOCompanyInformation.strCity));
            prmCompany.Add(new SqlParameter("@ProvinceID", PobjclsDTOCompanyInformation.intProvinceID));
            prmCompany.Add(new SqlParameter("@CountryID",PobjclsDTOCompanyInformation.intCountryID));
            prmCompany.Add(new SqlParameter("@PrimaryEmail", PobjclsDTOCompanyInformation.strPrimaryEmail));
            prmCompany.Add(new SqlParameter("@SecondaryEmail", PobjclsDTOCompanyInformation.strSecondaryEmail));
            prmCompany.Add(new SqlParameter("@ContactPerson", PobjclsDTOCompanyInformation.strContactPerson));
            prmCompany.Add(new SqlParameter("@DesignationID", PobjclsDTOCompanyInformation.intContactPersonDesignationID));
            prmCompany.Add(new SqlParameter("@ContactPersonPhone", PobjclsDTOCompanyInformation.strContactPersonPhone));
            prmCompany.Add(new SqlParameter("@FaxNumber", PobjclsDTOCompanyInformation.strPABXNumber));
            prmCompany.Add(new SqlParameter("@WebSite", PobjclsDTOCompanyInformation.strWebSite));
            prmCompany.Add(new SqlParameter("@CompanyIndustryID", PobjclsDTOCompanyInformation.intCompanyIndustryID));
            prmCompany.Add(new SqlParameter("@CompanyTypeID", PobjclsDTOCompanyInformation.intCompanyTypeID));
            //prmCompany.Add(new SqlParameter("@LogoFile", PobjclsDTOCompanyInformation.byteLogoFile));
            if (PobjclsDTOCompanyInformation.intUnernedPolicyID > 0)
                prmCompany.Add(new SqlParameter("@UnearnedPolicyID", PobjclsDTOCompanyInformation.intUnernedPolicyID));
            SqlParameter objSqlParameter = new SqlParameter();
            objSqlParameter.DbType = DbType.Binary;
            objSqlParameter.Value = PobjclsDTOCompanyInformation.byteLogoFile;
            objSqlParameter.ParameterName = "@LogoFile";
            prmCompany.Add(objSqlParameter); 

            prmCompany.Add(new SqlParameter("@OtherInfo", PobjclsDTOCompanyInformation.strOtherInfo));
            prmCompany.Add(new SqlParameter("@CurrencyId",PobjclsDTOCompanyInformation.intCurrencyId));
            prmCompany.Add(new SqlParameter("@FinYearStartDate1", PobjclsDTOCompanyInformation.strFinYearStartDate));
            prmCompany.Add(new SqlParameter("@WorkingDaysInMonth", PobjclsDTOCompanyInformation.intWorkingDaysInMonth));
            prmCompany.Add(new SqlParameter("@DayIDs", PobjclsDTOCompanyInformation.strOffDayIDs));
            prmCompany.Add(new SqlParameter("@BookStartDate1", PobjclsDTOCompanyInformation.strBookStartDate));
            prmCompany.Add(new SqlParameter("@IsMonth",PobjclsDTOCompanyInformation.blnIsMonth));
            prmCompany.Add(new SqlParameter("@EPID", PobjclsDTOCompanyInformation.strEPID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmCompany.Add(objParam);

            if (MobjDataLayer.ExecuteNonQuery("spCompany", prmCompany, out objOutCompanyID) != 0)
            {
                if ((int)objOutCompanyID > 0)
                {

                    PobjclsDTOCompanyInformation.intCompanyID = (int)objOutCompanyID;
                    SaveCompanyDetails(AddStatus, (int)objOutCompanyID);
                    if (PobjclsDTOCompanyInformation.lstCompanyBankDetails.Count > 0)
                    {
                      SaveBankInformation();
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


        public bool SaveCompanyDetails(bool AddStatus, int iComID)
        {
            // function for adding and updating company details
            object objOutCompanyID = 0;
            prmCompany = new ArrayList();

            //if (AddStatus)
                prmCompany.Add(new SqlParameter("@Mode", "6"));
            //else
            //    prmCompany.Add(new SqlParameter("@Mode", "7"));
            prmCompany.Add(new SqlParameter("@CompanyID", PobjclsDTOCompanyInformation.intCompanyID));
            prmCompany.Add(new SqlParameter("@TaxablePersonName", PobjclsDTOCompanyInformation.TaxablePersonName));
            prmCompany.Add(new SqlParameter("@TaxablePersonNameArb ", PobjclsDTOCompanyInformation.TaxablePersonNameArb));
            prmCompany.Add(new SqlParameter("@TRN", PobjclsDTOCompanyInformation.TRN));
            prmCompany.Add(new SqlParameter("@TaxAgencyName", PobjclsDTOCompanyInformation.TaxAgencyName));
            prmCompany.Add(new SqlParameter("@TAN", PobjclsDTOCompanyInformation.TAN));
            prmCompany.Add(new SqlParameter("@TaxAgentName", PobjclsDTOCompanyInformation.TaxAgentName));
            prmCompany.Add(new SqlParameter("@TAAN", PobjclsDTOCompanyInformation.TAAN));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmCompany.Add(objParam);

            if (MobjDataLayer.ExecuteNonQuery("spCompany", prmCompany, out objOutCompanyID) != 0)
            {
                if ((int)objOutCompanyID > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }




        //public bool SaveCompanyDetails(bool AddStatus, int iComID)
        //{
        //    // function for adding and updating company details
        //    object objOutCompanyID = 0;
        //    prmCompany = new ArrayList();

        //    if (AddStatus)
        //        prmCompany.Add(new SqlParameter("@Mode", "6"));
        //    else
        //        prmCompany.Add(new SqlParameter("@Mode", "7"));
        //    prmCompany.Add(new SqlParameter("@CompanyID", PobjclsDTOCompanyInformation.intCompanyID));
        //    prmCompany.Add(new SqlParameter("@CompanyServiceTaxNo", PobjclsDTOCompanyInformation.strCompanyServiceTaxNo));
        //    prmCompany.Add(new SqlParameter("@CompanyCSTNo ", PobjclsDTOCompanyInformation.strCompanyCSTNo));
        //    prmCompany.Add(new SqlParameter("@CompanyTINNo", PobjclsDTOCompanyInformation.strCompanyTINNo));
        //    prmCompany.Add(new SqlParameter("@CompanyPANNo", PobjclsDTOCompanyInformation.strCompanyPANNo));
        //    prmCompany.Add(new SqlParameter("@VATRegistrationNo", PobjclsDTOCompanyInformation.strVATRegistrationNo));
        //    SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
        //    objParam.Direction = ParameterDirection.ReturnValue;
        //    prmCompany.Add(objParam);

        //    if (MobjDataLayer.ExecuteNonQuery("spCompany", prmCompany, out objOutCompanyID) != 0)
        //    {
        //        if ((int)objOutCompanyID > 0)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        public bool SaveBankInformation()
        {
            //function for adding and updating bank details
            ArrayList prmBanks;
           
            foreach (clsDTOCompanyBankDetails objClsDtoCompanyBankDetails in PobjclsDTOCompanyInformation.lstCompanyBankDetails)
            {
                prmBanks = new ArrayList();
                if (objClsDtoCompanyBankDetails.intCompanyBankId == 0)
                    prmBanks.Add(new SqlParameter("@Mode", "9"));
                else
                    prmBanks.Add(new SqlParameter("@Mode", "25"));
                prmBanks.Add(new SqlParameter("@CompanyBankId", objClsDtoCompanyBankDetails.intCompanyBankId));
                prmBanks.Add(new SqlParameter("@CompanyID", PobjclsDTOCompanyInformation.intCompanyID));
                prmBanks.Add(new SqlParameter("@BankID", objClsDtoCompanyBankDetails.intBankId));
                prmBanks.Add(new SqlParameter("@AccountNumber ", objClsDtoCompanyBankDetails.strAccountNumber));
                MobjDataLayer.ExecuteNonQuery("spCompany",prmBanks);
            }
            return true;
        }

        public bool DisplayCompanyInfo(int Rownum, int intCompanyID)
        {
            //function for displaying company information
            prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@Mode", "1"));
            prmCompany.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmCompany.Add(new SqlParameter("@RowNum", Rownum));
            SqlDataReader sdrCompany = MobjDataLayer.ExecuteReader("spCompany", prmCompany, CommandBehavior.SingleRow);

            if (sdrCompany.Read())
            {
                PobjclsDTOCompanyInformation.intCompanyID = Convert.ToInt32(sdrCompany["CompanyID"]);
                PobjclsDTOCompanyInformation.intParentID = Convert.ToInt32(sdrCompany["ParentID"]);
                PobjclsDTOCompanyInformation.blnCompanyBranchIndicator = Convert.ToBoolean(sdrCompany["IsBranch"]);
                PobjclsDTOCompanyInformation.strName = Convert.ToString(sdrCompany["CompanyName"]);

                if (sdrCompany["FinYearStartDate"] != DBNull.Value)
                {
                    PobjclsDTOCompanyInformation.strFinYearStartDate = Convert.ToString(sdrCompany["FinYearStartDate"]);
                }

                if (sdrCompany["BookStartDate"] != DBNull.Value)
                {
                    PobjclsDTOCompanyInformation.strBookStartDate = Convert.ToString(sdrCompany["BookStartDate"]);
                }

                //if (sdrCompany["AccountID"] != DBNull.Value) 
                //    PobjclsDTOCompanyInformation.intAccountID = Convert.ToInt32(sdrCompany["AccountID"]);
                //else 
                //    PobjclsDTOCompanyInformation.intAccountID = 0;

                if (sdrCompany["EPID"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.strEPID = Convert.ToString(sdrCompany["EPID"]); 
                else 
                    PobjclsDTOCompanyInformation.strEPID = "";

                PobjclsDTOCompanyInformation.strShortName = Convert.ToString(sdrCompany["ShortName"]);
                PobjclsDTOCompanyInformation.strPOBox = Convert.ToString(sdrCompany["POBox"]);

                if (sdrCompany["CountryID"] != DBNull.Value) 
                    PobjclsDTOCompanyInformation.intCountryID = Convert.ToInt32(sdrCompany["CountryID"]);
                else PobjclsDTOCompanyInformation.intCountryID = 0;

                if (sdrCompany["CurrencyId"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.intCurrencyId = Convert.ToInt32(sdrCompany["CurrencyId"]); 
                else
                    PobjclsDTOCompanyInformation.intCurrencyId = 0;

                if (sdrCompany["CompanyIndustryID"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.intCompanyIndustryID = Convert.ToInt32(sdrCompany["CompanyIndustryID"]);
                else 
                    PobjclsDTOCompanyInformation.intCompanyIndustryID = 0;

                if (sdrCompany["WorkingDaysInMonth"] != DBNull.Value) 
                    PobjclsDTOCompanyInformation.intWorkingDaysInMonth = Convert.ToInt32(sdrCompany["WorkingDaysInMonth"]);

                if (sdrCompany["UnernedPolicyID"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.intUnernedPolicyID = Convert.ToInt32(sdrCompany["UnernedPolicyID"]);
                else
                    PobjclsDTOCompanyInformation.intUnernedPolicyID = 0;

                if (sdrCompany["CompanyTypeID"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.intCompanyTypeID = Convert.ToInt32(sdrCompany["CompanyTypeID"]); 
                else 
                    PobjclsDTOCompanyInformation.intCompanyTypeID = 0;

                if (sdrCompany["ProvinceID"] != DBNull.Value) 
                    PobjclsDTOCompanyInformation.intProvinceID = Convert.ToInt32(sdrCompany["ProvinceID"]); 
                else 
                    PobjclsDTOCompanyInformation.intProvinceID = 0;

                if (sdrCompany["Area"] != DBNull.Value) 
                    PobjclsDTOCompanyInformation.strArea = Convert.ToString(sdrCompany["Area"]); 
                else 
                    PobjclsDTOCompanyInformation.strArea = "";

                if (sdrCompany["Block"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.strBlock = Convert.ToString(sdrCompany["Block"]); 
                else 
                    PobjclsDTOCompanyInformation.strBlock = "";

                if (sdrCompany["Road"] != DBNull.Value) 
                    PobjclsDTOCompanyInformation.strRoad = Convert.ToString(sdrCompany["Road"]); 
                else 
                    PobjclsDTOCompanyInformation.strRoad = "";

                if (sdrCompany["City"] != DBNull.Value) 
                    PobjclsDTOCompanyInformation.strCity = Convert.ToString(sdrCompany["City"]);
                else 
                    PobjclsDTOCompanyInformation.strCity = "";

                if (sdrCompany["PrimaryEmail"] != DBNull.Value) 
                    PobjclsDTOCompanyInformation.strPrimaryEmail = Convert.ToString(sdrCompany["PrimaryEmail"]); 
                else 
                    PobjclsDTOCompanyInformation.strPrimaryEmail = "";

                if (sdrCompany["SecondaryEmail"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.strSecondaryEmail = Convert.ToString(sdrCompany["SecondaryEmail"]);
                else
                    PobjclsDTOCompanyInformation.strSecondaryEmail = "";

                if (sdrCompany["ContactPerson"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.strContactPerson = Convert.ToString(sdrCompany["ContactPerson"]);
                else
                    PobjclsDTOCompanyInformation.strContactPerson = "";

                if (sdrCompany["DesignationID"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.intContactPersonDesignationID = Convert.ToInt32(sdrCompany["DesignationID"]);
                else
                    PobjclsDTOCompanyInformation.intContactPersonDesignationID = 0;

                if (sdrCompany["ContactPersonPhone"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.strContactPersonPhone = Convert.ToString(sdrCompany["ContactPersonPhone"]);
                else 
                    PobjclsDTOCompanyInformation.strContactPersonPhone = "";

                if (sdrCompany["FaxNumber"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.strPABXNumber = Convert.ToString(sdrCompany["FaxNumber"]);
                else
                    PobjclsDTOCompanyInformation.strPABXNumber = "";

                if (sdrCompany["WebSite"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.strWebSite = Convert.ToString(sdrCompany["WebSite"]); 
                else 
                    PobjclsDTOCompanyInformation.strWebSite = "";

                if (sdrCompany["OtherInfo"] != DBNull.Value) 
                    PobjclsDTOCompanyInformation.strOtherInfo = Convert.ToString(sdrCompany["OtherInfo"]); 
                else
                    PobjclsDTOCompanyInformation.strOtherInfo = "";

                if ((sdrCompany["LogoFile"]) != DBNull.Value)
                {
                    PobjclsDTOCompanyInformation.byteLogoFile = (byte[])sdrCompany["logofile"];
                }
                else
                {
                    PobjclsDTOCompanyInformation.byteLogoFile = null;
                }

                if (sdrCompany["IsMonth"] != DBNull.Value)
                {
                    PobjclsDTOCompanyInformation.blnIsMonth = Convert.ToBoolean(sdrCompany["IsMonth"]);
                }

                if (sdrCompany["DayIDs"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.strOffDayIDs = Convert.ToString(sdrCompany["DayIDs"]);
                else
                    PobjclsDTOCompanyInformation.strOffDayIDs = string.Empty;

                sdrCompany.Close();
                DisplayCompanyDetailInfo(PobjclsDTOCompanyInformation.intCompanyID);
                return true;
            }
            else
            {
                sdrCompany.Close();
                return false;
            }
        }

        public bool DisplayCompanyDetailInfo(int iComID)
        {
            // function for displaying company detail information
            prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@Mode", "5"));
            prmCompany.Add(new SqlParameter("@CompanyID", iComID));
            SqlDataReader sdrCompany = MobjDataLayer.ExecuteReader("spCompany", prmCompany, CommandBehavior.SingleRow);

            if (sdrCompany.Read())
            {
                if (sdrCompany["TaxablePersonName"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.TaxablePersonName = Convert.ToString(sdrCompany["TaxablePersonName"]); 
                else
                    PobjclsDTOCompanyInformation.TaxablePersonName = "";

                if (sdrCompany["TaxablePersonNameArb"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.TaxablePersonNameArb = Convert.ToString(sdrCompany["TaxablePersonNameArb"]);
                else
                    PobjclsDTOCompanyInformation.TaxablePersonNameArb = "";

                if (sdrCompany["TRN"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.TRN = Convert.ToString(sdrCompany["TRN"]);
                else
                    PobjclsDTOCompanyInformation.TRN = "";

                if (sdrCompany["TaxAgencyName"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.TaxAgencyName = Convert.ToString(sdrCompany["TaxAgencyName"]);
                else
                    PobjclsDTOCompanyInformation.TaxAgencyName = "";

                if (sdrCompany["TAN"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.TAN = Convert.ToString(sdrCompany["TAN"]); 
                else
                    PobjclsDTOCompanyInformation.TAN = "";

                if (sdrCompany["TaxAgentName"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.TaxAgentName = Convert.ToString(sdrCompany["TaxAgentName"]);
                else
                    PobjclsDTOCompanyInformation.TaxAgentName = "";

                if (sdrCompany["TAAN"] != DBNull.Value)
                    PobjclsDTOCompanyInformation.TAAN = Convert.ToString(sdrCompany["TAAN"]);
                else
                    PobjclsDTOCompanyInformation.TAAN = "";

                sdrCompany.Close();
                return true;
            }
            else
            {
                sdrCompany.Close();
                PobjclsDTOCompanyInformation.strCompanyServiceTaxNo = "";
                PobjclsDTOCompanyInformation.strCompanyCSTNo = "";
                PobjclsDTOCompanyInformation.strCompanyTINNo = "";
                PobjclsDTOCompanyInformation.strCompanyPANNo = "";
                PobjclsDTOCompanyInformation.strVATRegistrationNo = "";
                return false;
            }
        }

        public DataTable DisplayBankInformation(int Rownum)
        {
            //function for displaying bank details
            prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@Mode", "8"));
            prmCompany.Add(new SqlParameter("@CompanyID", Rownum));
            return MobjDataLayer.ExecuteDataSet("spCompany", prmCompany).Tables[0];
        }

        public bool GetCompanyExists(int iComID)
        {
            //function for checking company exists or not
            bool blnRetValue = false;
            prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@Mode","16"));
            prmCompany.Add(new SqlParameter("@CompanyID",iComID));
            //string strQuery = "EXEC STGetCompanyIDExists " + iComID;
             SqlDataReader sdr = MobjDataLayer.ExecuteReader("spCompany",prmCompany);
             if (sdr.Read())
             {
                 if (Convert.ToInt32(sdr[0]) != 0)
                 {
                     blnRetValue = true;
                 }
                 sdr.Close();
             }
             return blnRetValue;
        }

        public bool CheckCompanyCurrencyUsed(int intCompanyID,int intCurrencyID)
        {
            //prmCompany = new ArrayList();
            //bool blnIsExists = false;
            //prmCompany.Add(new SqlParameter("@ID", intCompanyID ));
            //prmCompany.Add(new SqlParameter("@Table", "CompanyMaster"));
            //prmCompany.Add(new SqlParameter("@DetailsTable", "CompanyBankAccountDetails','CompanySettings', 'CurrencyDetails','InvWarehouse"));

            //if (Convert.ToString(MobjDataLayer.ExecuteScalar("spCheckIDExistsNew", prmCompany)).Trim().Length > 0)
            //    blnIsExists = true;
        
            //return blnIsExists;
            prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@Mode", 3));
            prmCompany.Add(new SqlParameter("@Value", intCompanyID));
            return Convert.ToBoolean(MobjDataLayer.ExecuteScalar("spCheckValueExists", prmCompany));
        }

        public bool DeleteCompany(int Rownum)
        {
            //function for deleting company
            prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@Mode", "4"));
            prmCompany.Add(new SqlParameter("@CompanyID", Rownum));

            if (MobjDataLayer.ExecuteNonQuery("spCompany",prmCompany) != 0)
                return true;
            else
                return false;
        }
        public bool DeleteCompanyAccountBankDetails(List<int> lstCompanyBankIds)
        {
            //function for deleting bank details
            int intRowsAffected = 0;

            foreach (int intCompanyBankId in lstCompanyBankIds)
            {
                prmCompany = new ArrayList();
                prmCompany.Add(new SqlParameter("@Mode", "12"));
                prmCompany.Add(new SqlParameter("CompanyBankId",intCompanyBankId));
                if (MobjDataLayer.ExecuteNonQuery("spCompany",prmCompany) != 0)
                    intRowsAffected++;
            }

            if (intRowsAffected == lstCompanyBankIds.Count)
                return true;
            else
                return false;
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MObjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }


        public bool CheckDuplication(bool bAddStatus, string[] saValues, int iId, int iType)
        {
            // function for checking duplicate item
            string sField = "CompanyID";
            string sTable = "CompanyMaster";
            string sCondition = "";

            if (bAddStatus)
            {
                if (iType == 1)
                {
                    sCondition = "CompanyName='" + saValues[0] + "'";
                }
                else
                {
                    sCondition = "EPID='" + saValues[0] + "'";
                }
            }
            else
            {
                if (iType == 1)
                {
                    sCondition = "CompanyName='" + saValues[0] + "'   and CompanyID <>" + iId + "";
                }
                else if (iType == 2)
                {
                    sCondition = "EPID='" + saValues[0] + "'   and CompanyID<>" + iId + "";
                }
            }
                return MObjClsCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
        }

        public bool CheckVoucherExists(string[] saValues, out int iReturnID)
        {
            //function for checking voucher exists or not
            string sField = "VoucherID";
            string sTable = "Vouchers";
            string sCondition = "CompanyID=" + saValues[0] + "";

                return MObjClsCommonUtility.GetRecordValue(new string[] { sField, sTable, sCondition }, out iReturnID);
        }

        public bool CheckAccountNoExists(bool bAddStatus, string[] saValues, int iId, out int iReturnID)
        {
            //function for checking account no exists or not
            string sField = "CompanyMaster.CompanyID";
            string sTable = "CompanyMaster INNER JOIN CompanyBankAccountDetails ON CompanyMaster.CompanyID = CompanyBankAccountDetails.CompanyID ";
            string sCondition = "";

            if (bAddStatus)
            {
                sCondition = "CompanyBankAccountDetails.CompanyBankAccountID =" + saValues[0] + " AND CompanyBankAccountDetails.BankAccountNo='" + saValues[1] + "' ";
            }
            else
            {
                sCondition = "CompanyBankAccountDetails.CompanyBankAccountID =" + saValues[0] + " AND CompanyBankAccountDetails.BankAccountNo='" + saValues[1] + "' and CompanyMaster.CompanyID <> " + iId + " ";
            }

                return MObjClsCommonUtility.GetRecordValue(new string[] { sField, sTable, sCondition }, out iReturnID);
        }

        public bool CheckDBSdateExists(string[] saValues, out DateTime dBSdate)
        {
            // function for checking book start date exists or not
            string[] sOutDate;
            string sField = "BookStartDate";
            string sTable = "CompanyMaster";
            string sCondition = "CompanyID=" + saValues[0] + " and BookStartDate IS NOT NULL";

                if (MObjClsCommonUtility.GetRecordValue(new string[] { sField, sTable, sCondition }, out sOutDate))
                {
                    dBSdate = Convert.ToDateTime(sOutDate[0]);
                    return true;
                }
                else
                {
                    dBSdate = Convert.ToDateTime(DBNull.Value);
                    return false;
                }
        }

        public bool IsLeapYearDate(int Year)
        {
            // function for checking leap year or not
            bool leapyear = false;
            leapyear = (((Year % 4) == 0) && ((Year % 100) != 0) || ((Year % 400) == 0));
            if (leapyear.Equals(true))
                return true;
            else
                return false;
        }

        public bool CheckValidEmail(string sEmail) 
        {
            // function for checking valid email or not
                return MObjClsCommonUtility.CheckValidEmail(sEmail);
        }

        public DataSet GetCompanyReport()
        {
            prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@Mode", 19));
            prmCompany.Add(new SqlParameter("@CompanyID", PobjclsDTOCompanyInformation.intCompanyID));

            return MobjDataLayer.ExecuteDataSet("spCompany", prmCompany);
        }
        public int GetRowNumber()
        {
            DataTable dat = new DataTable();
            prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@Mode", 21));
            prmCompany.Add(new SqlParameter("@CompanyID", PobjclsDTOCompanyInformation.intCompanyID));
            dat = MobjDataLayer.ExecuteDataTable("spCompany", prmCompany);

            if (dat.Rows.Count > 0)
                return Convert.ToInt32(dat.Rows[0]["RowNumber"]);
            else
                return 0;
        }

        //a

        public bool GetBankBranchExistsInEmployee(int intBankBranchID)
        {
            //function for checking BankBranchID exists or not
            bool blnRetValue = false;
            prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@Mode", "23"));
            prmCompany.Add(new SqlParameter("@BankBranchID", intBankBranchID));
            prmCompany.Add(new SqlParameter("AccountNumber", PobjclsDTOCompanyInformation.AccountNumber));
            prmCompany.Add(new SqlParameter("@CompanyID", PobjclsDTOCompanyInformation.intCompanyID));
            //string strQuery = "EXEC STGetCompanyIDExists " + iComID;
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spCompany", prmCompany);
            if (sdr.Read())
            {
                if (Convert.ToInt32(sdr[0]) != 0)
                {
                    blnRetValue = true;
                }
                sdr.Close();
            }
            return blnRetValue;
        }

        public bool GetVoucherDetailsExists(int intBankBranchID)
        {
            bool blnRetValue = false;
            prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@Mode", "24"));
            prmCompany.Add(new SqlParameter("@BankBranchID", intBankBranchID));
            prmCompany.Add(new SqlParameter("@CompanyID", PobjclsDTOCompanyInformation.intCompanyID));
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spCompany", prmCompany);
            if (sdr.Read())
            {
                if (Convert.ToInt32(sdr[0]) != 0)
                {
                    blnRetValue = true;
                }
                sdr.Close();
            }
            return blnRetValue;
        }
      

        #region IDisposable Members

        public void Dispose()
        {
            //disposing
            if (prmCompany != null)
                prmCompany = null;
            if (MObjClsCommonUtility != null)
                MObjClsCommonUtility = null;
        }

        #endregion


    public int GetAccountID(int intBankBranchID)
    {
        throw new NotImplementedException();
    }
    }
}
