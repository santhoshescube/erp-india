﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyBooksERP
{
    public class clsDALHeirarchy
    {
        ArrayList prmHeirarchy;  

        public clsDTOHeirarchy objclsDTOHeirarchy { get; set; }

        public DataLayer objclsConnection { get; set; }

        public bool Insert()
        {
            prmHeirarchy = new ArrayList();
            prmHeirarchy.Add(new SqlParameter("@Mode", "I"));
            prmHeirarchy.Add(new SqlParameter("@EmployeeID", objclsDTOHeirarchy.EmployeeID));
            prmHeirarchy.Add(new SqlParameter("@ParentId", objclsDTOHeirarchy.ParentId));

            objclsConnection.ExecuteNonQuery("STHierarchy", prmHeirarchy);
            return true;

        }

        public DataSet GetAllParents()
        {
            prmHeirarchy = new ArrayList();

            prmHeirarchy.Add(new SqlParameter("@Mode", "GAP"));

            return objclsConnection.ExecuteDataSet("STHierarchy", prmHeirarchy);
        }

        public DataSet GetAllEmployees()
        {
            prmHeirarchy = new ArrayList();

            prmHeirarchy.Add(new SqlParameter("@Mode", "GAE"));

            return objclsConnection.ExecuteDataSet("STHierarchy", prmHeirarchy);
        }

        public DataSet GetAllChildren(int iParentId)
        {
            prmHeirarchy = new ArrayList();

            prmHeirarchy.Add(new SqlParameter("@Mode", "GC"));
            prmHeirarchy.Add(new SqlParameter("@ParentId", iParentId));

            return objclsConnection.ExecuteDataSet("STHierarchy", prmHeirarchy);
        }

        public DataTable GetAllTreeParents()
        {
            prmHeirarchy = new ArrayList();

            prmHeirarchy.Add(new SqlParameter("@Mode", "GP"));

            return objclsConnection.ExecuteDataTable("STHierarchy", prmHeirarchy);
        }

        public DataTable GetAllNodes()
        {
            prmHeirarchy = new ArrayList();

            prmHeirarchy.Add(new SqlParameter("@Mode", "GPC"));
            prmHeirarchy.Add(new SqlParameter("@HierarchyId", objclsDTOHeirarchy.HierarchyId));

            return objclsConnection.ExecuteDataTable("STHierarchy", prmHeirarchy);
        }

        public void Delete(int iHeirarchyID)
        {
            prmHeirarchy = new ArrayList();

            prmHeirarchy.Add(new SqlParameter("@Mode", "D"));
            prmHeirarchy.Add(new SqlParameter("@HierarchyId", iHeirarchyID));

            objclsConnection.ExecuteNonQuery("STHierarchy",prmHeirarchy);
        }



    }

}
