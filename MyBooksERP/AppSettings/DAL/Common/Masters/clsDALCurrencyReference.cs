﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date, 5 April 2011>
Description:	<Description,DAL for CurrencyReference>
================================================
*/

namespace MyBooksERP
{
    public class clsDALCurrencyReference
    {

        ArrayList parameters;
        SqlDataReader sdrDr;

        public clsDTOCurrencyReference objclsDTOCurrencyReference { get; set; }
        public DataLayer objclsconnection { get; set; }

        // To fill combo

        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsconnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }


        //  SAVE AND UPDATE
        public bool SaveCurrency(bool AddStatus)
        {
            object objOutCurrencyID = 0;
            parameters = new ArrayList();
            if (AddStatus)
                parameters.Add(new SqlParameter("@Mode", 2));//insert
            else
                parameters.Add(new SqlParameter("@Mode", 3));//update
            parameters.Add(new SqlParameter("@CurrencyID", objclsDTOCurrencyReference.intCurrencyID));
            parameters.Add(new SqlParameter("@Code", objclsDTOCurrencyReference.strShortDescription));
            parameters.Add(new SqlParameter("@CurrencyName", objclsDTOCurrencyReference.strDescription));
            parameters.Add(new SqlParameter("@CountryID", objclsDTOCurrencyReference.intCountryID));
            parameters.Add(new SqlParameter("@Scale", objclsDTOCurrencyReference.shtScale));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            if (objclsconnection.ExecuteNonQuery("spCurrencyReference", parameters, out objOutCurrencyID) != 0)
            {
                if ((int)objOutCurrencyID > 0)
                {
                    objclsDTOCurrencyReference.intCurrencyID = (int)objOutCurrencyID;
                    return true;
                }

            }
            return false;
        }


        // Display
        public bool DisplayCurrency(int intCurrencyID)
        {
            parameters = new ArrayList();
            //ShortDescription,Description,CountryID,Scale
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@CurrencyID", intCurrencyID));
            sdrDr = objclsconnection.ExecuteReader("spCurrencyReference", parameters, CommandBehavior.SingleRow);
            if (sdrDr.Read())
            {
                objclsDTOCurrencyReference.strShortDescription = Convert.ToString(sdrDr["Code"]);
                objclsDTOCurrencyReference.strDescription = Convert.ToString(sdrDr["CurrencyName"]);
                objclsDTOCurrencyReference.intCountryID = Convert.ToInt32(sdrDr["CountryID"]);
                objclsDTOCurrencyReference.shtScale = Convert.ToInt16(sdrDr["Scale"]);
                sdrDr.Close();
                return true;
            }
            return false;
        }

        public bool DeleteCurrency(int intCurrencyDID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@CurrencyID", intCurrencyDID));
            if (objclsconnection.ExecuteNonQuery("spCurrencyReference", parameters) != 0)
                return true;
            else return false;
        }

        // TO FILL Grid on loading

        public DataTable GetCurrencyDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode",5));
            return objclsconnection.ExecuteDataTable("spCurrencyReference", parameters);

        }

        //public  GetCurrencyExists(int iCurID)
        //{
        //    string sQuery = "EXEC STGetCurrencyIDExists " + iCurID;
        //    return MobjCurRefCon.ExecutesReader(sQuery);

        //} 
        public int CheckDuplicateCurrency(string strCurrencyName)
        {
            int iCurrencyId=0;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode",6));
            parameters.Add(new SqlParameter("@CurrencyName", strCurrencyName));
            SqlDataReader sdrCurrency = objclsconnection.ExecuteReader("spCurrencyReference", parameters);
            if (sdrCurrency.Read())
            {
                iCurrencyId=Convert.ToInt32(sdrCurrency["CurrencyID"]);

            }
            sdrCurrency.Close();
            return iCurrencyId;
        }

        public int CheckDuplicateCurrency(string strCurrencyName,int intCurrID)
        {
            int iCurrencyId = 0;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@CurrencyName", strCurrencyName));
            parameters.Add(new SqlParameter("@CurrencyID", intCurrID));
            SqlDataReader sdrCurrency = objclsconnection.ExecuteReader("spCurrencyReference", parameters);
            if (sdrCurrency.Read())
            {
                iCurrencyId = Convert.ToInt32(sdrCurrency["CurrencyID"]);

            }
            sdrCurrency.Close();
            return iCurrencyId;
        }

        public int CheckDuplicateCountry( int intCurrID,int intCountryID)
        {
            int iCurrencyId = 0;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@CurrencyID", intCurrID));
            parameters.Add(new SqlParameter("@CountryID", intCountryID));
            SqlDataReader sdrCurrency = objclsconnection.ExecuteReader("spCurrencyReference", parameters);
            if (sdrCurrency.Read())
            {
                iCurrencyId = Convert.ToInt32(sdrCurrency["CurrencyID"]);

            }
            sdrCurrency.Close();
            return iCurrencyId;
        }
        public DataTable GetCurrencyExists(int iCurID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@CurrencyID",iCurID));
            return objclsconnection.ExecuteDataTable("spGetCurrencyIDExists", parameters);
        }

        



    }
}
