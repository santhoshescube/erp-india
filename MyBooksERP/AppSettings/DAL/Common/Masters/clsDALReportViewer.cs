﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,25 Feb 2011>
Description:	<Description,,DAL for ReportViewer>
================================================
*/
namespace MyBooksERP
{
    public class clsDALReportViewer : IDisposable
    {
        ArrayList prmReportDetails;
        DataTable dtpreportTable;

        public clsDTOReportviewer objclsDTOReportviewer { get; set; }
        public DataLayer objclsconnection { get; set; }


        public DataTable DisplayCompanyReport() // for company report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 17));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spCompany", prmReportDetails);
        }

        public DataTable DisplayCompanyBankReport() //company Bank Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 18));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spCompany", prmReportDetails);
        }

        public DataTable DisplayBankReport()  //Bank Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            prmReportDetails.Add(new SqlParameter("@BankBranchID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spBankDetails",prmReportDetails);
        }

        public DataTable DisplayEmployeeReport()  //Employee  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@EmployeeID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spEmployee", prmReportDetails);
        }

        public DataTable DisplayVendorReport()  //Vendor  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 12));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }

        public DataTable DisplayVendorAddressReport() //VendorAddress  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode",13));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }
        public DataTable DisplayCustomerSalesReport()  //Customer  Sales History
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 34));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }
        public DataTable DisplayCustomerPaymentReport()  //Customer  Payment History
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 35));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }
        public DataTable DisplayCustomerItemsReport()  //Customer  Sales item History
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 36));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }
        public DataTable DisplaySupplierPurchaseReport()  //Supplier Purchase History
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 37));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }
        public DataTable DisplaySupplierPaymentReport()  //Supplier Payment History
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 38));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }
        public DataTable DisplaySupplierItemsReport()  //Supplier Items History
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 39));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }
        public DataTable DisplaySupplierNearestNeighbourReport()  // Nearest Neighbour Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 30));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }

        public DataTable DisplayWareHouseReport() //WareHouse Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 15));
            prmReportDetails.Add(new SqlParameter("@WarehouseID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvWarehouseMasterTransactions", prmReportDetails);
        }

        public DataTable DisplayWareHouseDetailsReport() //WareHouseDetails  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 16));
            prmReportDetails.Add(new SqlParameter("@WarehouseID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvWarehouseMasterTransactions", prmReportDetails);
        }
        public DataSet DisplayCompanySettingsReport() //  CompanySettings Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 12));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spCompanySettings", prmReportDetails);
        }
        public DataSet DisplayRoleSettingsReport() //  CompanySettings Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 3));
            prmReportDetails.Add(new SqlParameter("@RoleID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("STRoleDetailsTransaction", prmReportDetails);
        }

        public DataSet DisplayProductReport() //  Product Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@ItemID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvItemMaster", prmReportDetails);
        }

        public DataSet DisplayAlertSettingsReport() //  AlertSetting Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 11));
            prmReportDetails.Add(new SqlParameter("@AlertSettingID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAlertSetting", prmReportDetails);
        }

        public DataTable DisplayUOMReport() //UOM  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@UOMTypeID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvUnitOfMeasurement", prmReportDetails);
        }

        public DataTable DisplayPaymentTermsReport() //UOM  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 5));
            return objclsconnection.ExecuteDataTable("STPaymentTermsTransaction", prmReportDetails);
        }

        public DataTable DisplaySTDiscountTypeReferenceReport() //STDiscountTypeReference Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode",14));
            prmReportDetails.Add(new SqlParameter("@DiscountTypeID",objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvDiscounts", prmReportDetails);
        }

        public DataTable DisplaySTDiscountItemWiseDetailsReport() //STDiscountItemWiseDetails  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode",15));
            prmReportDetails.Add(new SqlParameter("@DiscountTypeID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvDiscounts", prmReportDetails);
        }

        public DataTable DisplaySTDiscountCustomerWiseDetailsReport() //STDiscountCustomerWiseDetails  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode",16));
            prmReportDetails.Add(new SqlParameter("@DiscountTypeID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvDiscounts", prmReportDetails);
        }

        public DataSet DisplayPaymentDetailsReport() //Payment Details Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 22));
            prmReportDetails.Add(new SqlParameter("@ReceiptAndPaymentID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAccReceiptsAndPayments", prmReportDetails);
        }

        public DataSet DisplayExchangeCurrencyReport() //Exchange currency Details Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spExchangeCurrency", prmReportDetails);
        }

        public DataSet DisplayDeliveryNoteReport() //Delivery Note Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 15));
            //prmReportDetails.Add(new SqlParameter("@Mode", 25));
            prmReportDetails.Add(new SqlParameter("@ItemIssueID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvItemIssue", prmReportDetails);
        }

        public DataSet DisplayCustomerWiseDeliveryNoteReport()//Customerwise Delivery Note report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 25));
            prmReportDetails.Add(new SqlParameter("@ItemIssueID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvItemIssue", prmReportDetails);

        }

        public DataSet DisplayMaterialIssueReport() //Material Issue Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 11));
            prmReportDetails.Add(new SqlParameter("@MaterialIssueID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvMaterialIssue", prmReportDetails);
        }

        public DataSet DisplayMaterialReturnReport() //Material Return Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 11));
            prmReportDetails.Add(new SqlParameter("@MaterialReturnID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvMaterialReturn", prmReportDetails);
        }
  
        public DataSet DisplayExpenseReport() //Delivery Expense Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            prmReportDetails.Add(new SqlParameter("@ExpenseID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvExpense", prmReportDetails);
        }

        public DataSet DisplayItemGroupReport() //Item Group Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode",6));
            prmReportDetails.Add(new SqlParameter("@ItemGroupID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvItemGroupMaster", prmReportDetails);
        }
        public DataSet DisplayPurchaseDetailsReport(int intFormType)
        {
            // 1 - PurchaseIndent, 2 - PurchaseQuotation, 3 - PurchaseOrder, 4 - GRN, 5 - PurchaseInvoice
            switch (intFormType)
            {
                case 1:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 19));
                    prmReportDetails.Add(new SqlParameter("@PurchaseIndentID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("InvPurchaseIndent", prmReportDetails);
                case 2:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 19));
                    prmReportDetails.Add(new SqlParameter("@PurchaseQuotationID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spInvPurchaseQuotation", prmReportDetails);
                case 3:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 20));
                    prmReportDetails.Add(new SqlParameter("@PurchaseOrderID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spPurchaseOrder", prmReportDetails);
                case 4:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 18));
                    prmReportDetails.Add(new SqlParameter("@GRNID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spGRN", prmReportDetails);
                case 5:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 18));
                    prmReportDetails.Add(new SqlParameter("@PurchaseInvoiceID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spPurchaseInvoice", prmReportDetails);
                case 6:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 20));
                    prmReportDetails.Add(new SqlParameter("@GRNID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spInvDirectGRN", prmReportDetails);
                default:
                    return null;
            }
        }
        public DataSet DisplayPILocationReport()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 29));
            prmReportDetails.Add(new SqlParameter("@PurchaseInvoiceID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvPurchaseInvoice", prmReportDetails);
        }
        public DataSet DisplayGRNLocationReport()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 23));
            prmReportDetails.Add(new SqlParameter("@GRNID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvGRN", prmReportDetails);
        }
        public DataTable DisplaySalesInvoice1Report() //sales invoice detail report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 23));
            prmReportDetails.Add(new SqlParameter("@TermsID", 1));
            prmReportDetails.Add(new SqlParameter("@OperationTypeID", 9));
            return objclsconnection.ExecuteDataTable("spInvSalesInvoice", prmReportDetails);
        }

        public DataSet DisplaySalesDetailReport(int intFormType)
        {
            switch (intFormType)
            {
                case 1:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 16));
                    prmReportDetails.Add(new SqlParameter("@SalesQuotationID ", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spInvSalesQuotation", prmReportDetails);
                case 2:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 18));
                    prmReportDetails.Add(new SqlParameter("@SalesOrderID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spInvSalesOrder", prmReportDetails);
                case 3:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 19));
                    prmReportDetails.Add(new SqlParameter("@SalesInvoiceID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spInvSalesInvoice", prmReportDetails);
                case 4:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 19));
                    prmReportDetails.Add(new SqlParameter("@SalesReturnID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spInvSalesReturn", prmReportDetails);
                default:
                    return null;
            }
        }

        public DataSet DisplayCompanyReportHeader(int intCompanyID) //CompnayReportHeader
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode",31));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objclsconnection.ExecuteDataSet("spInvPurchaseModuleFunctions", prmReportDetails);
        }

        public DataTable DisplayCompanyDefaultHeader() //Company Default Header
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 22));
            return objclsconnection.ExecuteDataTable("spCompany", prmReportDetails);
        }
        public DataSet DisplayOpeningStockReport() //Opening Stock Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 9));
            prmReportDetails.Add(new SqlParameter("@OpeningStockID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvOpeningStock", prmReportDetails);
        }      
       
        public DataSet DisplayAccountSettingsReport() //Account Settings Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAccSettings", prmReportDetails);
        }
        public DataSet DisplayRecurrentSetupReport() //Account RecurrentSetup Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 9));
            prmReportDetails.Add(new SqlParameter("@RecurrenceSetupID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAccRecurranceSetup", prmReportDetails);
        }

        public DataSet DisplayRFQReport() //RFQ  Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 19));
            prmReportDetails.Add(new SqlParameter("@RFQID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvRFQ", prmReportDetails);
        }

        public DataSet DisplayUOMConversionReport(int intUomClassID, int intUomBaseID) // UOM Conversion
        {
            prmReportDetails = new ArrayList();
            if(intUomClassID == 0)
                prmReportDetails.Add(new SqlParameter("@Mode", 4));
            else if (intUomClassID > 0 && intUomBaseID == 0)
                prmReportDetails.Add(new SqlParameter("@Mode", 6));
            else if (intUomClassID > 0 && intUomBaseID > 0)
                prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@UOMTypeID", intUomClassID));
            prmReportDetails.Add(new SqlParameter("@UOMID", intUomBaseID));
            return objclsconnection.ExecuteDataSet("spInvUomConversion", prmReportDetails);
        }
        public DataSet DisplayStockAdjustmentReport() //Stock Adjustment Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "10"));
            prmReportDetails.Add(new SqlParameter("@StockAdjustmentID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvStockAdjustment", prmReportDetails);
        }

        public DataTable DisplayTermsAndConditionReport(int operation, int intCompanyID) // for all TermsAndConditionreport
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 4));
            prmReportDetails.Add(new SqlParameter("@OperationTypeID", operation));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objclsconnection.ExecuteDataTable("spTermsAndConditions", prmReportDetails);

        }
        public DataSet DisplayGeneralReceiptsAndPayements()// Display report for Jounrnal Voucher
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 18));
            prmReportDetails.Add(new SqlParameter("@VoucherID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAccJournalVoucher", prmReportDetails);
        }
        public DataSet DisplayAccountsOpeningBalance()// Display report for OpeningBalance
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            //prmReportDetails.Add(new SqlParameter("@AccountID", objclsDTOReportviewer.intRecId));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intCompany));
            return objclsconnection.ExecuteDataSet("spAccOpeningBalance", prmReportDetails);
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsconnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }


        public DataSet DisplayProjectReport() //Project Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 10));
            prmReportDetails.Add(new SqlParameter("@ProjectID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvProjects", prmReportDetails);
        }

        public DataSet DisplayStockTransfer() //Project Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 16));
            prmReportDetails.Add(new SqlParameter("@StockTransferID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvStockTransfer", prmReportDetails);
        }
        public DataSet DisplayDebitNoteReport()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 23));
            prmReportDetails.Add(new SqlParameter("@PurchaseReturnID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvPurchaseReturn", prmReportDetails);
        }

        public DataSet DisplayDebitNoteLocationReport()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 24));
            prmReportDetails.Add(new SqlParameter("@PurchaseReturnID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvPurchaseReturn", prmReportDetails);
        }

        public DataSet DisplayItemIssueLocationReport()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 29));
            prmReportDetails.Add(new SqlParameter("@ItemIssueID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvItemIssue", prmReportDetails);
        }
        public DataSet DisplayPricingSchemeReport() //Delivery Expense Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 14));
            prmReportDetails.Add(new SqlParameter("@RowNumber", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("[spInvPricingScheme]", prmReportDetails);
        }

        public DataSet DisplayPOSReport() // POS Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 17));
            prmReportDetails.Add(new SqlParameter("@POSID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvPOS", prmReportDetails);

        }
        public DataSet DisplayLeavePolicy() //Leave Policy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 20));
            prmReportDetails.Add(new SqlParameter("@LeavePolicyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayLeavePolicy", prmReportDetails);

        }

        public DataSet DisplayShiftPolicy() //Shift Policy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 13));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayShiftPolicy", prmReportDetails);

        }
        public DataSet DisplayWorkPolicy() //Work Policy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "WP"));
            prmReportDetails.Add(new SqlParameter("@PolicyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayWorkPolicy", prmReportDetails);

        }

        public DataSet DisplayOvertimePolicy() //OvertimePolicy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "11"));
            prmReportDetails.Add(new SqlParameter("@OTPolicyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayOvertimePolicy", prmReportDetails);

        }
        public DataSet DisplayReceiptIssue() //OvertimePolicy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "5"));
            prmReportDetails.Add(new SqlParameter("@DocumentID", objclsDTOReportviewer.intRecId));
            prmReportDetails.Add(new SqlParameter("@StatusID", objclsDTOReportviewer.intStatusID));
            return objclsconnection.ExecuteDataSet("spDocuments", prmReportDetails);

        }
        public DataSet DisplayAccRecurringJournalSetup() //AccRecurringJournalSetup
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "6"));
            prmReportDetails.Add(new SqlParameter("@RecurringJournalSetupID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAccJournalRecurrenceSetup", prmReportDetails);

        }

        public DataSet DisplaySalaryStructure() //OvertimePolicy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "19"));
            prmReportDetails.Add(new SqlParameter("@SalaryStructureID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPaySalaryStructure", prmReportDetails);

        }


        public DataSet DisplaySalaryRelease() //SalaryRelease Form
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@ProcessDate1", objclsDTOReportviewer.strDate));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intCompany));
            return objclsconnection.ExecuteDataSet("spPayEmployeePaymentDetailRelease", prmReportDetails);

        }

        public DataSet DisplayCompanyHeader()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 31));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intCompany));
            return objclsconnection.ExecuteDataSet("spInvPurchaseModuleFunctions", prmReportDetails);
        }

        public DataSet GetJobOrderDetails()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 8));
            prmReportDetails.Add(new SqlParameter("@JobOrderID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPrdCompanywiseProduction", prmReportDetails);
        }
        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (prmReportDetails != null)
                prmReportDetails = null;

            if (dtpreportTable != null)
                dtpreportTable.Dispose();


        }

        #endregion

    }
}
