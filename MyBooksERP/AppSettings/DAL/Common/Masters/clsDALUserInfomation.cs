﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.ComponentModel;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,28 Feb 2011>
Description:	<Description,,DAL for UserInformation>
================================================
*/

namespace MyBooksERP
{
    public class clsDALUserInfomation : IDisposable
    {

        ArrayList parameters;
        ArrayList fmParameters;
        SqlDataReader sdrDr;

        public clsDTOUserInformation objclsDTOUserInformation { get; set; }
        public DataLayer objclsconnection { get; set; }
        private string strProcedureName = "spUser";
        private string strProcedureCheckIDExists = "spCheckIDExists";

        // Save And Update

        public bool SaveUser(bool AddStatus)
        {
            object objOutUserID = 0;

            parameters = new ArrayList();

            if (AddStatus)
                parameters.Add(new SqlParameter("@Mode", 2));// Insert
            else
                parameters.Add(new SqlParameter("@Mode", 3));// Update
            parameters.Add(new SqlParameter("@UserID", objclsDTOUserInformation.intUserID));
            parameters.Add(new SqlParameter("@RoleID", objclsDTOUserInformation.intRoleID));

            if (objclsDTOUserInformation.intEmployeeID > 0)
                parameters.Add(new SqlParameter("@EmployeeID ", objclsDTOUserInformation.intEmployeeID));

            parameters.Add(new SqlParameter("@UserName", objclsDTOUserInformation.strUserName));
            parameters.Add(new SqlParameter("@Password", objclsDTOUserInformation.strPassword));
            parameters.Add(new SqlParameter("@EmailID", objclsDTOUserInformation.strEmailID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            if (objclsconnection.ExecuteNonQuery(strProcedureName, parameters, out objOutUserID) != 0)//[STUserInformations]
            {
                if ((int)objOutUserID > 0)
                {
                    objclsDTOUserInformation.intUserID = (int)objOutUserID;
                    SaveUserCompanyDetails();
                    return true;
                }
            }
            return false;
        }

        private void SaveUserCompanyDetails()
        {
            DeleteUserCompany();

            object objoutRowsAffected = 2;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 13));
            parameters.Add(new SqlParameter("@UserID", objclsDTOUserInformation.intUserID));
            parameters.Add(new SqlParameter("@XmlCompanyDetails", objclsDTOUserInformation.lstUserInformation.ToXml()));
            objclsconnection.ExecuteNonQueryWithTran(strProcedureName, parameters, out objoutRowsAffected);
        }

        private void DeleteUserCompany()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 14));
            parameters.Add(new SqlParameter("@UserID", objclsDTOUserInformation.intUserID));
            objclsconnection.ExecuteNonQuery(strProcedureName, parameters);
        }

        public int RecCountNavigate()
        {
            int intRecordCnt = 0;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            sdrDr = objclsconnection.ExecuteReader(strProcedureName, parameters);

            if (sdrDr.Read())
            {
                intRecordCnt = Convert.ToInt32(sdrDr["cnt"]);
            }

            sdrDr.Close();
            return intRecordCnt;
        }

        // Display

        public bool DisplayUser(int rowno)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1)); // Select
            parameters.Add(new SqlParameter("@RowNum", rowno));
            sdrDr = objclsconnection.ExecuteReader(strProcedureName, parameters, CommandBehavior.SingleRow);

            if (sdrDr.Read())
            {
                objclsDTOUserInformation.intUserID = Convert.ToInt32(sdrDr["UserID"]);
                objclsDTOUserInformation.intRoleID = Convert.ToInt32(sdrDr["RoleID"]);

                if (sdrDr["EmployeeID"] != DBNull.Value)
                    objclsDTOUserInformation.intEmployeeID = Convert.ToInt32(sdrDr["EmployeeID"]);

                objclsDTOUserInformation.strUserName = Convert.ToString(sdrDr["UserName"]);
                objclsDTOUserInformation.strPassword = Convert.ToString(sdrDr["Password"]);
                objclsDTOUserInformation.strEmailID = Convert.ToString(sdrDr["EmailID"]);
                sdrDr.Close();
                return true;
            }
            return false;
        }

        // Delete
        public bool DeleteUserInformation()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));// Delete
            parameters.Add(new SqlParameter("@UserID", objclsDTOUserInformation.intUserID));

            if (objclsconnection.ExecuteNonQuery(strProcedureName, parameters) != 0)
            {
                return true;
            }
            return false;
        }

        //To Fill Combobox

        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsconnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }
            }
            return null;
        }

        //  check Valid Email

        public bool CheckValidEmail(string sEmailAddress)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                return objCommonUtility.CheckValidEmail(sEmailAddress);
            }
        }

        //  To check Duplication

        public bool CheckDuplication(bool blnAddStatus, string[] saValues, int intId)
        {
            string sField = "UserName";
            string sTable = "UserMaster";
            string sCondition = "";

            if (blnAddStatus) // Add mode           
                sCondition = "UserName='" + saValues[0] + "'";
            else // Edit mode                
                sCondition = "UserName='" + saValues[0] + "' and  UserID <>" + intId + "";

            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = objclsconnection;
                return objCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
            }
        }

        public DataTable CheckExistReferences(int intId)
        {
            fmParameters = new ArrayList();
            string strCondition = "name <>'UserMaster'";
            fmParameters.Add(new SqlParameter("@PrimeryID", intId));
            fmParameters.Add(new SqlParameter("@Condition", strCondition));
            fmParameters.Add(new SqlParameter("@FieldName", "UserID"));
            return objclsconnection.ExecuteDataSet(strProcedureCheckIDExists, fmParameters).Tables[0];
        }

        public string GetFormsInUse(string strCaption, int intPrimeryId, string strCondition, string strFileds)
        {
            return new ClsWebform().GetUsedFormsInformation(strCaption, intPrimeryId, strCondition, strFileds);
        }

        /// <summary>
        /// Created By   : Laxmi
        /// Created date : 09-03-2011
        /// Purpose      : Check user exists from login page
        /// </summary>
        /// 

        public SqlDataReader IsExistUserName()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@UserName", objclsDTOUserInformation.strUserName));
            parameters.Add(new SqlParameter("@Password", objclsDTOUserInformation.strPassword));

            return objclsconnection.ExecuteReader(strProcedureName, parameters);
        }

        public string GetWelcomeText()
        {
            parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@UserID", objclsDTOUserInformation.intUserID));

            return Convert.ToString(objclsconnection.ExecuteScalar(strProcedureName, parameters));
        }

        public DataTable getUserCompanyDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 15));
            parameters.Add(new SqlParameter("@UserID", objclsDTOUserInformation.intUserID));
            return objclsconnection.ExecuteDataTable(strProcedureName, parameters);
        }

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (parameters != null)
                parameters = null;

            if (fmParameters != null)
                fmParameters = null;

            if (sdrDr != null)
                sdrDr.Dispose();

        }

        #endregion
    }
}