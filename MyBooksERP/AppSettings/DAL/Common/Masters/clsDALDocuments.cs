﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,11 Mar 2011>
Description:	<Description,DAL for Scanning>
================================================
*/

namespace MyBooksERP 
{
  public class clsDALDocuments
    {
      ArrayList parameters;
      SqlDataReader sdrDr;

      public clsDTODocuments objclsDTODocuments { get; set; }
      public DataLayer objclsconnection { get; set; }

      // count for  Print checking
       public int Print() 
        {
          int intPrintCount=0;
          parameters = new ArrayList();
          parameters.Add(new SqlParameter("@Mode", 1));
          sdrDr = objclsconnection.ExecuteReader("STScanning", parameters, CommandBehavior.SingleRow);
          if (sdrDr.Read())
          {
              intPrintCount = Convert.ToInt32(sdrDr["cnt"]);            

          }
          sdrDr.Close();
          return intPrintCount;
           }

       // count for  Addnew checking

       public int AddNew() 
       {
           int intAddNewCount = 0;
           parameters = new ArrayList();
           parameters.Add(new SqlParameter("@Mode", 2));
           sdrDr = objclsconnection.ExecuteReader("STScanning", parameters, CommandBehavior.SingleRow);
           if (sdrDr.Read())
           {
               intAddNewCount = Convert.ToInt32(sdrDr["cnt"]);
           }
           sdrDr.Close();
           return intAddNewCount;
       }
         
       // To Save and Update
       public bool Save(bool AddStatus)
       {
           object objoutNodeId = 0;

           parameters = new ArrayList();
           if (AddStatus)
           {
               parameters.Add(new SqlParameter("@Mode", 3));
               parameters.Add(new SqlParameter("@ParentNode", objclsDTODocuments.intParentNode));
               parameters.Add(new SqlParameter("@DocumentTypeID", objclsDTODocuments.intDocumentTypeID));
               parameters.Add(new SqlParameter("@CommonId", objclsDTODocuments.lngCommonId));
               parameters.Add(new SqlParameter("@NavID", objclsDTODocuments.intNavID));
           }
           else
           {
               parameters.Add(new SqlParameter("@Mode", 14));
               parameters.Add(new SqlParameter("@Node", objclsDTODocuments.intNode));
           }

           parameters.Add(new SqlParameter("@Description", objclsDTODocuments.strDescription));

           SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
           objParam.Direction = ParameterDirection.ReturnValue;
           parameters.Add(objParam);

           if (objclsconnection.ExecuteNonQuery("STScanning", parameters, out objoutNodeId) != 0)
           {
               if ((int)objoutNodeId > 0)
               {
                   objclsDTODocuments.intNode = (int)objoutNodeId;
                   return true;
               }

           }
           return false;
       }

       public bool insertDetails(int intNode,string strFilename,string strMeta)
       {
           parameters = new ArrayList();
           parameters.Add(new SqlParameter("@Mode", 11));
           parameters.Add(new SqlParameter("@Node", intNode));
           parameters.Add(new SqlParameter("@FileName", strFilename));
           parameters.Add(new SqlParameter("@MetaData", strMeta));      
           objclsconnection.ExecuteNonQuery("STScanning", parameters);
           return true;
       }

       public void Insert()
       {
           parameters = new ArrayList();
           parameters.Add(new SqlParameter("@Mode", 9));
           parameters.Add(new SqlParameter("@DocumentTypeID", objclsDTODocuments.intDocumentTypeID));
           parameters.Add(new SqlParameter("@CommonId", objclsDTODocuments.lngCommonId));
           parameters.Add(new SqlParameter("@NavID", objclsDTODocuments.intNavID));
           parameters.Add(new SqlParameter("@Description", objclsDTODocuments.strDescription));

           SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
           objParam.Direction = ParameterDirection.ReturnValue;
           parameters.Add(objParam);

           object objoutNodeId;

           if (objclsconnection.ExecuteNonQuery("STScanning", parameters, out objoutNodeId) != 0)
           {
               objclsDTODocuments.intNode = Convert.ToInt32(objoutNodeId);
           }
       }

       public bool selectforupdate(string description)
       {
           parameters = new ArrayList();
           parameters.Add(new SqlParameter("@Mode", 15));
           parameters.Add(new SqlParameter("@Description", description));
           sdrDr = objclsconnection.ExecuteReader("STScanning", parameters);
           if (sdrDr.Read())
           {
               objclsDTODocuments.intNode = Convert.ToInt32(sdrDr["Node"]);
               objclsDTODocuments.strDescription = Convert.ToString(sdrDr["Description"]);
               sdrDr.Close();
               return true;
           }
           return false;
       }


       public bool Selectnode(long lngCommonID, string strDescription, int intNavid)
       {
           parameters = new ArrayList();
           parameters.Add(new SqlParameter("@Mode", 7));
           parameters.Add(new SqlParameter("@CommonId", lngCommonID));
           parameters.Add(new SqlParameter("@Description", strDescription));
           parameters.Add(new SqlParameter("@NavID", intNavid));
           sdrDr = objclsconnection.ExecuteReader("STScanning", parameters);
           if (sdrDr.Read())
           {
               objclsDTODocuments.intNode = Convert.ToInt32(sdrDr["Node"]);
               sdrDr.Close();
               return true;
           }
           return false;

       }
       public bool selectDocumentType(int Document)
       {
           parameters = new ArrayList();
           parameters.Add(new SqlParameter("@Mode",6));
           parameters.Add(new SqlParameter("@DocumentTypeID",Document));
           sdrDr = objclsconnection.ExecuteReader("STScanning", parameters);
           if (sdrDr.Read())
            {
               objclsDTODocuments.strDescription = Convert.ToString(sdrDr["description"]);
               sdrDr.Close();
               return true;
           }
           return false;
       }


       public bool SelectDetails(int intNode)
       {
           parameters = new ArrayList();
           parameters.Add(new SqlParameter("@Mode", 12));
           parameters.Add(new SqlParameter("@Node",intNode));           
           sdrDr = objclsconnection.ExecuteReader("STScanning", parameters);
           if (sdrDr.Read())
           {
               objclsDTODocuments.strFileName = Convert.ToString(sdrDr["FileName"]);
               sdrDr.Close();
               return true;
           }
           sdrDr.Close();
           return false;

       }


       public bool SelectfromTreeMaster(int documentid)
       {
           objclsDTODocuments.strDescription = "";
           parameters = new ArrayList();
           parameters.Add(new SqlParameter("@Mode", 13));
           parameters.Add(new SqlParameter("@DocumentTypeID", documentid));
           sdrDr = objclsconnection.ExecuteReader("STScanning", parameters);
           if (sdrDr.Read())
           {
               objclsDTODocuments.strDescription = Convert.ToString(sdrDr["Description"]);
               sdrDr.Close();
               return true;
           }
           return false;

       }

       public bool Deletenodes(int mode, int Nodes)
       {
           parameters = new ArrayList();
           parameters.Add(new SqlParameter("@mode", mode));
           parameters.Add(new SqlParameter("@Node ", Nodes));
           if (objclsconnection.ExecuteNonQueryWithTran("STScanning", parameters) != 0)
           {

               return true;
           }
           return false;
       }



       public DataTable Display(int Navid, long CommonId, int Parentnode)
       {
           parameters = new ArrayList();
           parameters.Add(new SqlParameter("@Mode", 10));
           parameters.Add(new SqlParameter("@NavID", Navid));
           parameters.Add(new SqlParameter("@CommonId", CommonId));
           parameters.Add(new SqlParameter("@parentNode", Parentnode));
           return objclsconnection.ExecuteDataTable("STScanning", parameters);
       }


       public DataTable Fill(int CompanyId,string strDescription,int Navid,int parentnode)
       {
           parameters = new ArrayList();
           parameters.Add(new SqlParameter("@Mode", 8));
           parameters.Add(new SqlParameter("@CommonId", CompanyId));
           parameters.Add(new SqlParameter("@Description", strDescription));
           parameters.Add(new SqlParameter("@NavID", Navid));
           parameters.Add(new SqlParameter("@ParentNode", parentnode));
           return objclsconnection.ExecuteDataTable("STScanning", parameters);

       }

       public DataTable GetparentNodes(int Navid, long CommonId, int Parentnode, int intOperationTypeID, int intVendorID, int intDocumentTypeid)
       {
           parameters = new ArrayList();
           parameters.Add(new SqlParameter("@Mode", 20));
           parameters.Add(new SqlParameter("@NavID", Navid));
           parameters.Add(new SqlParameter("@CommonId", CommonId));
           parameters.Add(new SqlParameter("@OperationTypeID", intOperationTypeID));
           parameters.Add(new SqlParameter("@VendorID", intVendorID));
           parameters.Add(new SqlParameter("@DocumentTypeID", intDocumentTypeid));
           parameters.Add(new SqlParameter("@parentNode", Parentnode));
           return objclsconnection.ExecuteDataTable("STScanning", parameters);
       }
    }
}
