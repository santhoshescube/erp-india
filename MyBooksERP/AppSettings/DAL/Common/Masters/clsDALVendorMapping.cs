﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALVendorMapping
    {
        public clsDTOVendorInformation objDTOVendorInformation { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;

        public clsDALVendorMapping(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
        }

        public DataTable getVendorDetails(int intType, string strSearchText, int intVendorTypeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@Type", intType));
            parameters.Add(new SqlParameter("@SearchText", strSearchText));
            parameters.Add(new SqlParameter("@VendorTypeID", intVendorTypeID));
            return MobjDataLayer.ExecuteDataTable("spInvVendorMapping", parameters);
        }

        public bool SaveVendorInfo()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 2));
            parameters.Add(new SqlParameter("@VendorID", objDTOVendorInformation.intVendorID));
            parameters.Add(new SqlParameter("@CompanyID", objDTOVendorInformation.intCompanyID));
            MobjDataLayer.ExecuteNonQuery("spInvVendorMapping", parameters);
            return true;
        }

        public bool DeleteVendorInfo()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 3));
            parameters.Add(new SqlParameter("@VendorID", objDTOVendorInformation.intVendorID));
            parameters.Add(new SqlParameter("@CompanyID", objDTOVendorInformation.intCompanyID));
            MobjDataLayer.ExecuteNonQuery("spInvVendorMapping", parameters);
            return true;
        }

        public DataTable SetAutoCompleteList(int intType, string strSearchText, int intVendorTypeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@Type", intType));
            parameters.Add(new SqlParameter("@SearchText", strSearchText));
            parameters.Add(new SqlParameter("@VendorTypeID", intVendorTypeID));
            return MobjDataLayer.ExecuteDataTable("spInvVendorMapping", parameters);
        }
    }
}
