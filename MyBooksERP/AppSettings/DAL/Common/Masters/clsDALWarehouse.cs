﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyBooksERP
{
    /// <summary>
    /// Author:Arun
    /// Date:17/02/2011
    /// Purpose:For WareHouseTransactions
    /// </summary>
    public class clsDALWarehouse : IDisposable
    {
        ArrayList prmWarehouse;

        public clsDTOWarehouse clsDTOWarehouse { get; set; }
        public DataLayer objclsConnection { get; set; }

        public int DisplayRecordCount(int intCompanyID)// Get Record Count
        {
            int intRecordCount = 0;
            prmWarehouse = new ArrayList();
            SqlDataReader sdrReader;
            prmWarehouse.Add(new SqlParameter("@Mode", 5));
            prmWarehouse.Add(new SqlParameter("@CompanyID", intCompanyID));
            sdrReader = objclsConnection.ExecuteReader("spInvWarehouseMasterTransactions", prmWarehouse, CommandBehavior.CloseConnection);
           
            if (sdrReader.Read())
                intRecordCount = Convert.ToInt32(sdrReader["Count"]);

            sdrReader.Close();
            return intRecordCount;
        }
        public int DisplayCompanyRecordCount(int intRoleID, int intCompanyID, int intControlID)
        {
            int intRecordCount = 0;
            prmWarehouse = new ArrayList();
            SqlDataReader sdrReader;
            prmWarehouse.Add(new SqlParameter("@Mode", 20));
            prmWarehouse.Add(new SqlParameter("@RoleID", intRoleID));
            prmWarehouse.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmWarehouse.Add(new SqlParameter("@ControlID", intControlID));
            sdrReader = objclsConnection.ExecuteReader("spInvWarehouseMasterTransactions", prmWarehouse, CommandBehavior.CloseConnection);

            if (sdrReader.Read())
                intRecordCount = Convert.ToInt32(sdrReader["Count"]);

            sdrReader.Close();
            return intRecordCount; 
        }
        public bool DispalyWarehouseInfo(int RowNum, int intCompanyID)//Dispalying Master Data
        {
            prmWarehouse = new ArrayList();
            prmWarehouse.Add(new SqlParameter("@Mode", 1));
            prmWarehouse.Add(new SqlParameter("@RowNumber", RowNum));
            prmWarehouse.Add(new SqlParameter("@CompanyID", intCompanyID));
            SqlDataReader drWarehouse = objclsConnection.ExecuteReader("spInvWarehouseMasterTransactions", prmWarehouse, CommandBehavior.SingleRow);

            if (drWarehouse.Read())
            {
                clsDTOWarehouse.intWarehouseID = Convert.ToInt32(drWarehouse["WarehouseID"]);
                clsDTOWarehouse.strWarehouseName = Convert.ToString(drWarehouse["WarehouseName"]);
                clsDTOWarehouse.strShortName = Convert.ToString(drWarehouse["ShortName"]);
                clsDTOWarehouse.intCompanyID = Convert.ToInt32(drWarehouse["CompanyID"]);
                clsDTOWarehouse.strAddress1 = Convert.ToString(drWarehouse["Address1"]);
                clsDTOWarehouse.strAddress2 = Convert.ToString(drWarehouse["Address2"]);
                clsDTOWarehouse.strZipCode = Convert.ToString(drWarehouse["ZipCode"]);
                clsDTOWarehouse.strCityState = Convert.ToString(drWarehouse["CityState"]);
                clsDTOWarehouse.intCountryID = drWarehouse["CountryID"].ToInt32();
                clsDTOWarehouse.strDescription = Convert.ToString(drWarehouse["Remarks"]);
                clsDTOWarehouse.intInChargeID = drWarehouse["InChargeID"].ToInt32();
                clsDTOWarehouse.strPhone = Convert.ToString(drWarehouse["Phone"]);
                clsDTOWarehouse.blnActive = Convert.ToBoolean(drWarehouse["IsActive"]);
                drWarehouse.Close();
                return true;
            }
            else
            {
                drWarehouse.Close();
                return false;
            }
        }

        public bool DisplayCompanyWarehouseInfo(int intRoleID, int intCompanyID, int intControlID, int RowNo)
        {

            prmWarehouse = new ArrayList();
            prmWarehouse.Add(new SqlParameter("@Mode", 19));
            prmWarehouse.Add(new SqlParameter("@RoleID", intRoleID));
            prmWarehouse.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmWarehouse.Add(new SqlParameter("@ControlID", intControlID));
            prmWarehouse.Add(new SqlParameter("@RowNo", RowNo));
            SqlDataReader drWarehouse = objclsConnection.ExecuteReader("spInvWarehouseMasterTransactions", prmWarehouse, CommandBehavior.SingleRow);

            if (drWarehouse.Read())
            {
                clsDTOWarehouse.intWarehouseID = Convert.ToInt32(drWarehouse["WarehouseID"]);
                clsDTOWarehouse.strWarehouseName = Convert.ToString(drWarehouse["WarehouseName"]);
                clsDTOWarehouse.strShortName = Convert.ToString(drWarehouse["ShortName"]);
                clsDTOWarehouse.intCompanyID = Convert.ToInt32(drWarehouse["CompanyID"]);
                clsDTOWarehouse.strAddress1 = Convert.ToString(drWarehouse["Address1"]);
                clsDTOWarehouse.strAddress2 = Convert.ToString(drWarehouse["Address2"]);
                clsDTOWarehouse.strZipCode = Convert.ToString(drWarehouse["ZipCode"]);
                clsDTOWarehouse.strCityState = Convert.ToString(drWarehouse["CityState"]);
                clsDTOWarehouse.intCountryID = Convert.ToInt32(drWarehouse["CountryID"]);
                clsDTOWarehouse.strDescription = Convert.ToString(drWarehouse["Description"]);
                clsDTOWarehouse.intInChargeID = Convert.ToInt32(drWarehouse["InChargeID"]);
                clsDTOWarehouse.strPhone = Convert.ToString(drWarehouse["Phone"]);
                clsDTOWarehouse.blnActive = Convert.ToBoolean(drWarehouse["Active"]);
                drWarehouse.Close();
                return true;
            }
            else
            {
                drWarehouse.Close();
                return false;
            }
        }
        // for email
        public DataSet GetWarehouseReport()
        {
            prmWarehouse = new ArrayList();
            prmWarehouse.Add(new SqlParameter("@Mode", 17));
            prmWarehouse.Add(new SqlParameter("@WarehouseID", clsDTOWarehouse.intWarehouseID));

            return objclsConnection.ExecuteDataSet("spInvWarehouseMasterTransactions", prmWarehouse);
        }

        //-----------------------------------------------
        public DataTable DisplayWarehouseInformationDetail(int pWarehouseID)
        {
            prmWarehouse = new ArrayList();
            prmWarehouse.Add(new SqlParameter("@Mode", 7));
            prmWarehouse.Add(new SqlParameter("@WarehouseID", pWarehouseID));
            return objclsConnection.ExecuteDataSet("spInvWarehouseMasterTransactions", prmWarehouse).Tables[0];
            //return true;
        }

        //Rows
        public DataTable DisplayWarehouseInformationDetailRow(int pWarehouseID, string psLocation, int LocID)
        {

            prmWarehouse = new ArrayList();
            prmWarehouse.Add(new SqlParameter("@Mode", 11));
            prmWarehouse.Add(new SqlParameter("@WarehouseID", pWarehouseID));
            prmWarehouse.Add(new SqlParameter("@Location", psLocation));
            prmWarehouse.Add(new SqlParameter("@LocationID", LocID));
            return objclsConnection.ExecuteDataSet("spInvWarehouseMasterTransactions", prmWarehouse).Tables[0];
            //return true;
        }

        //Blocks
        public DataTable DisplayWarehouseInformationDetailBLOCK(int pWarehouseID, string psLocation, int LocID, string psRows, int RowID)
        {

            prmWarehouse = new ArrayList();
            prmWarehouse.Add(new SqlParameter("@Mode", 12));
            prmWarehouse.Add(new SqlParameter("@WarehouseID", pWarehouseID));
            prmWarehouse.Add(new SqlParameter("@Location", psLocation));
            prmWarehouse.Add(new SqlParameter("@LocationID", LocID));
            prmWarehouse.Add(new SqlParameter("@RowNumbers", psRows));
            prmWarehouse.Add(new SqlParameter("@RowID", RowID));
            return objclsConnection.ExecuteDataSet("spInvWarehouseMasterTransactions", prmWarehouse).Tables[0];
            //return true;
        }

        //LOTS
        public DataTable DisplayWarehouseInformationDetailLOTS(int pWarehouseID, string psLocation, int LocID, string psRows, int RowID, string psBlocks, int BlockID)
        {
            prmWarehouse = new ArrayList();
            prmWarehouse.Add(new SqlParameter("@Mode", 13));
            prmWarehouse.Add(new SqlParameter("@WarehouseID", pWarehouseID));
            prmWarehouse.Add(new SqlParameter("@Location", psLocation));
            prmWarehouse.Add(new SqlParameter("@LocationID", LocID));
            prmWarehouse.Add(new SqlParameter("@RowNumbers", psRows));
            prmWarehouse.Add(new SqlParameter("@RowID", RowID));
            prmWarehouse.Add(new SqlParameter("@BlockNumber", psBlocks));
            prmWarehouse.Add(new SqlParameter("@BlockID", BlockID));
            return objclsConnection.ExecuteDataSet("spInvWarehouseMasterTransactions", prmWarehouse).Tables[0];
            //return true;
        }

        public bool SaveWarehouseGenaralInformation()//Master Data Saving
        {
            object objOutWarehouseID = 0;
            prmWarehouse = new ArrayList();

            if (clsDTOWarehouse.intWarehouseID > 0)
            {
                prmWarehouse.Add(new SqlParameter("@Mode", 3));//3 - update
                prmWarehouse.Add(new SqlParameter("@WarehouseID", clsDTOWarehouse.intWarehouseID));
            }
            else
            {
                prmWarehouse.Add(new SqlParameter("@Mode", 2));
            }//2 - Insert }

            prmWarehouse.Add(new SqlParameter("@WarehouseName", clsDTOWarehouse.strWarehouseName));
            prmWarehouse.Add(new SqlParameter("@ShortName", clsDTOWarehouse.strShortName));
            prmWarehouse.Add(new SqlParameter("@CompanyID", clsDTOWarehouse.intCompanyID));
            prmWarehouse.Add(new SqlParameter("@Address1", clsDTOWarehouse.strAddress1));
            prmWarehouse.Add(new SqlParameter("@Address2", clsDTOWarehouse.strAddress2));
            prmWarehouse.Add(new SqlParameter("@ZipCode", clsDTOWarehouse.strZipCode));
            prmWarehouse.Add(new SqlParameter("@CityState", clsDTOWarehouse.strCityState));
            if(clsDTOWarehouse.intCountryID>0)
            prmWarehouse.Add(new SqlParameter("@CountryID", clsDTOWarehouse.intCountryID));
            prmWarehouse.Add(new SqlParameter("@Description", clsDTOWarehouse.strDescription));
            if(clsDTOWarehouse.intInChargeID>0)
            prmWarehouse.Add(new SqlParameter("@InChargeID", clsDTOWarehouse.intInChargeID));
            prmWarehouse.Add(new SqlParameter("@Phone", clsDTOWarehouse.strPhone));
            prmWarehouse.Add(new SqlParameter("@Active", clsDTOWarehouse.blnActive));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmWarehouse.Add(objParam);

            if (objclsConnection.ExecuteNonQuery("spInvWarehouseMasterTransactions", prmWarehouse, out objOutWarehouseID) != 0)
            {
                if ((int)objOutWarehouseID > 0)
                    clsDTOWarehouse.intWarehouseID = (int)objOutWarehouseID;// Convert.ToInt32(objParam.Value);

                SaveWarehouseCompanyDetails();
                return true;
            }
            return false;
        }

        public bool DeleteWarehouseInformation()//Delete Information in the warehouse
        {
            prmWarehouse = new ArrayList();
            prmWarehouse.Add(new SqlParameter("@Mode", 4));
            prmWarehouse.Add(new SqlParameter("@WarehouseID", clsDTOWarehouse.intWarehouseID));
            objclsConnection.ExecuteNonQuery("spInvWarehouseMasterTransactions", prmWarehouse);
            return true;
        }

        public bool CheckDuplication(int ComID, string WareHouseName, int WID)
        {
            prmWarehouse = new ArrayList();
            prmWarehouse.Add(new SqlParameter("@Mode", 6));
            prmWarehouse.Add(new SqlParameter("@CompanyID", ComID));
            prmWarehouse.Add(new SqlParameter("@WarehouseName", WareHouseName));
            prmWarehouse.Add(new SqlParameter("@WarehouseID", WID));
            SqlDataReader drWarehouse = objclsConnection.ExecuteReader("spInvWarehouseMasterTransactions", prmWarehouse);

            if (drWarehouse.Read())
                return true;

            return false;
        }

        public int GetWarehouseCount(int intCompanyID)
        {
            int intRecordCount = 0;
            prmWarehouse = new ArrayList();
            SqlDataReader sdrReader;
            prmWarehouse.Add(new SqlParameter("@Mode", 21));
            prmWarehouse.Add(new SqlParameter("@CompanyID", intCompanyID));
            sdrReader = objclsConnection.ExecuteReader("spInvWarehouseMasterTransactions", prmWarehouse, CommandBehavior.CloseConnection);

            if (sdrReader.Read())
                intRecordCount = Convert.ToInt32(sdrReader["Cnt"]);

            sdrReader.Close();

            return intRecordCount;
        }

        public string GetFormsInUse(string strCaption, int intPrimeryId, string strCondition, string strFileds)
        {
            return new ClsWebform().GetUsedFormsInformation(strCaption, intPrimeryId, strCondition, strFileds);
        }

        public bool DeleteWarehouseDetailInformation()//Delete Detail Information  for Update
        {
            prmWarehouse = new ArrayList();

            prmWarehouse.Add(new SqlParameter("@Mode", 10));
            prmWarehouse.Add(new SqlParameter("@WarehouseID", clsDTOWarehouse.intWarehouseID));
            objclsConnection.ExecuteNonQuery("spInvWarehouseMasterTransactions", prmWarehouse);
            return true;
        }

        public DataTable GetFormsInUse(int iId)
        {
            ArrayList fmParameters = new ArrayList();
            string sCondition = "name <>'InvWarehouse' and name <>'InvWarehouseDetails'";
            fmParameters.Add(new SqlParameter("@PrimeryID", iId));
            fmParameters.Add(new SqlParameter("@Condition", sCondition));
            fmParameters.Add(new SqlParameter("@FieldName", "WarehouseID"));
            return objclsConnection.ExecuteDataTable("spCheckIDExists", fmParameters);
        }
        public bool GetNodeInUse(int intWarehouseID, int intLocId, int intRowID, int intBlockID, int intLotID, int intLevel)
        {

            string sField = "ItemID";
            string sTable = "STItemLocationDetails";
            string sCondition = "";

            if (intLevel==1)
                sCondition = "WarehouseID=" + intWarehouseID + " and LocationID=" + intLocId +" and Quantity > " + 0 + " and ReferenceID= " + 163 + "";
            if (intLevel == 2)
                sCondition = "WarehouseID=" + intWarehouseID + " and LocationID=" + intLocId + " and RowID =" + intRowID + " and Quantity > " + 0 + " and ReferenceID= " + 163 + "";
            if (intLevel == 3)
                sCondition = "WarehouseID=" + intWarehouseID + " and LocationID=" + intLocId + " and RowID =" + intRowID + " and BlockID = "+ intBlockID + " and Quantity > " + 0 + " and ReferenceID= " + 163 + "";
            if (intLevel == 4)
                sCondition = "WarehouseID=" + intWarehouseID + " and LocationID=" + intLocId + " and RowID =" + intRowID + " and BlockID = " + intBlockID + " and LotID= " + intLotID + " and Quantity > " + 0 + " and ReferenceID= " + 163 + "";

            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = objclsConnection;
                return objCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
            }
        }
        
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }
        public int GetRowNumber()
        {
            DataTable dat = new DataTable();
            prmWarehouse = new ArrayList();
            prmWarehouse.Add(new SqlParameter("@Mode", 18));
            prmWarehouse.Add(new SqlParameter("@WarehouseID",clsDTOWarehouse.intWarehouseID));
            dat = objclsConnection.ExecuteDataTable("spInvWarehouseMasterTransactions", prmWarehouse);

            if (dat.Rows.Count > 0)
                return Convert.ToInt32(dat.Rows[0]["RowNumber"]);
            else
                return 0;
        }
        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmWarehouse = new ArrayList();
            prmWarehouse.Add(new SqlParameter("@Mode", 4));
            prmWarehouse.Add(new SqlParameter("@RoleID", intRoleID));
            prmWarehouse.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmWarehouse.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable("STPermissionSettings", prmWarehouse);
        }
        void IDisposable.Dispose()
        {
            if (prmWarehouse != null)
                prmWarehouse = null;
        }

        private void SaveWarehouseCompanyDetails()
        {
            DeleteWarehouseCompanyDetails();

            object objoutRowsAffected = 2;
            prmWarehouse = new ArrayList();
            prmWarehouse.Add(new SqlParameter("@Mode", 22));
            prmWarehouse.Add(new SqlParameter("@WarehouseID", clsDTOWarehouse.intWarehouseID));
            prmWarehouse.Add(new SqlParameter("@XmlCompanyDetails", clsDTOWarehouse.lstWarehouseCompanyDetails.ToXml()));
            objclsConnection.ExecuteNonQueryWithTran("spInvWarehouseMasterTransactions", prmWarehouse, out objoutRowsAffected);
        }

        private void DeleteWarehouseCompanyDetails()
        {
            prmWarehouse = new ArrayList();
            prmWarehouse.Add(new SqlParameter("@Mode", 23));
            prmWarehouse.Add(new SqlParameter("@WarehouseID", clsDTOWarehouse.intWarehouseID));
            objclsConnection.ExecuteNonQuery("spInvWarehouseMasterTransactions", prmWarehouse);
        }

        public DataTable getWarehouseCompanyDetails(int intWarehouseID)
        {
            prmWarehouse = new ArrayList();
            prmWarehouse.Add(new SqlParameter("@Mode", 24));
            prmWarehouse.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            return objclsConnection.ExecuteDataTable("spInvWarehouseMasterTransactions", prmWarehouse);
        }
    }
}