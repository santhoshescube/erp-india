using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
/* 
   =================================================
   Created By:		< Midhun >
   Create date:     < 05 Apr 2012>
   ================================================
*/
namespace MyBooksERP
{
    public class clsDALVendorInformation:  IDisposable 
    {
        ArrayList prmVendor;
        ArrayList prmGetAccount;
        DataTable dat;
        DataTable sRecordValues;
        string strProcedureName = "spInvVendorInformation";

        public clsDTOVendorInformation objclsDTOVendorInformation { get; set; }
        public DataLayer objclsConnection { get; set; }
       
         

        public int AddVendorInfo()
        {
            object objOutVendorID = 0;

            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 2));
            prmVendor.Add(new SqlParameter("@VendorCode", objclsDTOVendorInformation.strVendorCode));
            prmVendor.Add(new SqlParameter("@SerialNo",objclsDTOVendorInformation.intSerialNo));
            prmVendor.Add(new SqlParameter("@Description", objclsDTOVendorInformation.strVendorName));
            prmVendor.Add(new SqlParameter("@GLAccountID", objclsDTOVendorInformation.intGLAccountID));
            prmVendor.Add(new SqlParameter("@BSOrPL", objclsDTOVendorInformation.chrBSOrPL));
            prmVendor.Add(new SqlParameter("@Predefined", objclsDTOVendorInformation.blnPredefined));
            prmVendor.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID ));
                //objclsDTOVendorInformation.intCompanyID));
            //prmVendor.Add(new SqlParameter("@OpeningBalance", objclsDTOVendorInformation.dblOpeningBalance));
            prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));
            prmVendor.Add(new SqlParameter("@VendorAddID", objclsDTOVendorInformation.intVendorAddID));
            prmVendor.Add(new SqlParameter("@VendorTypeID", objclsDTOVendorInformation.intVendorType));
            prmVendor.Add(new SqlParameter("@VendorName", objclsDTOVendorInformation.strVendorName));
            //prmVendor.Add(new SqlParameter("@ShortName", objclsDTOVendorInformation.strShortName));
            prmVendor.Add(new SqlParameter("@TransactionTypeID", objclsDTOVendorInformation.intTransType));
            prmVendor.Add(new SqlParameter("@TRNNo", objclsDTOVendorInformation.strTRNNo));
           
            if (objclsDTOVendorInformation.intBranchID > 0)
                prmVendor.Add(new SqlParameter("@BankBranchID", objclsDTOVendorInformation.intBranchID));
            if (objclsDTOVendorInformation.intBankID > 0)
                prmVendor.Add(new SqlParameter("@BankID", objclsDTOVendorInformation.intBankID));
            
            prmVendor.Add(new SqlParameter("@AccountNumber", objclsDTOVendorInformation.strAccountNumber));
            prmVendor.Add(new SqlParameter("@Email", objclsDTOVendorInformation.strEmail));
            prmVendor.Add(new SqlParameter("@Website", objclsDTOVendorInformation.strWebsite));
            prmVendor.Add(new SqlParameter("@VendorClassificationID", objclsDTOVendorInformation.intVendorClassificationID));
            prmVendor.Add(new SqlParameter("@RatingRemarks", objclsDTOVendorInformation.strRatingRemarks));
            prmVendor.Add(new SqlParameter("@Rating", objclsDTOVendorInformation.dblRatting));
            if (objclsDTOVendorInformation.intStatusID > 0)
                prmVendor.Add(new SqlParameter("@StatusID", objclsDTOVendorInformation.intStatusID));
           
            if (objclsDTOVendorInformation.decAdvanceLimit > 0)
                prmVendor.Add(new SqlParameter("@AdvanceLimit", objclsDTOVendorInformation.decAdvanceLimit));
            if (objclsDTOVendorInformation.decCreditLimit > 0)
                prmVendor.Add(new SqlParameter("@CreditLimit", objclsDTOVendorInformation.decCreditLimit));

           
            if (objclsDTOVendorInformation.intSourceMediaID > 0)
                prmVendor.Add(new SqlParameter("@SourceMediaID", objclsDTOVendorInformation.intSourceMediaID));

            if (objclsDTOVendorInformation.intSupplierCurrencyID> 0)
                prmVendor.Add(new SqlParameter("@CurrencyID", objclsDTOVendorInformation.intSupplierCurrencyID));

            if (objclsDTOVendorInformation.intCreditDaysLimit > 0)
                prmVendor.Add(new SqlParameter("@CreditLimitDays", objclsDTOVendorInformation.intCreditDaysLimit));

            //if (objclsDTOVendorInformation.intAccountTypeID > 0)
            //    prmVendor.Add(new SqlParameter("@SalaryAccountTypeID", objclsDTOVendorInformation.intAccountTypeID));

            //if (objclsDTOVendorInformation.decSalaryOrIncome > 0)
            //    prmVendor.Add(new SqlParameter("@SalaryOrIncome", objclsDTOVendorInformation.decSalaryOrIncome));

            //prmVendor.Add(new SqlParameter("@Gender", objclsDTOVendorInformation.strGender));
            //if (objclsDTOVendorInformation.intDepartmentID > 0)
            //    prmVendor.Add(new SqlParameter("@DepartmentID", objclsDTOVendorInformation.intDepartmentID));
            //if (objclsDTOVendorInformation.intDesignationID > 0)
            //    prmVendor.Add(new SqlParameter("@DesignationID", objclsDTOVendorInformation.intDesignationID));
            //if (objclsDTOVendorInformation.intEmploymentTypeID > 0)
            //    prmVendor.Add(new SqlParameter("@EmploymentTypeID", objclsDTOVendorInformation.intEmploymentTypeID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmVendor.Add(objParam);

            if (objclsConnection.ExecuteNonQuery(strProcedureName, prmVendor, out objOutVendorID) != 0)
            {
                if ((int)objOutVendorID > 0)
                {
                    objclsDTOVendorInformation.intVendorID = (int)objOutVendorID;

                    if (objclsDTOVendorInformation.intVendorType == (int)VendorType.Supplier)
                    {
                        AddNearestCompetitor();
                    }
                    return objclsDTOVendorInformation.intVendorID;
                }
            }
            return 0;
        }

         public int AddVendorInfoRegular()
         {
             object objOutVendorID = 0;

             prmVendor = new ArrayList();
             prmVendor.Add(new SqlParameter("@Mode", 25));
             prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));
             prmVendor.Add(new SqlParameter("@VendorAddID", objclsDTOVendorInformation.intVendorAddID));
             prmVendor.Add(new SqlParameter("@VendorTypeID", objclsDTOVendorInformation.intVendorType));
             prmVendor.Add(new SqlParameter("@VendorCode", objclsDTOVendorInformation.strVendorCode));
             prmVendor.Add(new SqlParameter("@SerialNo", objclsDTOVendorInformation.intSerialNo));
             prmVendor.Add(new SqlParameter("@VendorName", objclsDTOVendorInformation.strVendorName));
             //prmVendor.Add(new SqlParameter("@ShortName", objclsDTOVendorInformation.strShortName));
             prmVendor.Add(new SqlParameter("@TransactionTypeID", objclsDTOVendorInformation.intTransType));
             prmVendor.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
             if (objclsDTOVendorInformation.intBranchID > 0)
                 prmVendor.Add(new SqlParameter("@BankBranchID", objclsDTOVendorInformation.intBranchID));
             if (objclsDTOVendorInformation.intBankID > 0)
                 prmVendor.Add(new SqlParameter("@BankID", objclsDTOVendorInformation.intBankID));
            
             prmVendor.Add(new SqlParameter("@AccountNumber", objclsDTOVendorInformation.strAccountNumber));

             if (objclsDTOVendorInformation.intAccountHead > 0)
                 prmVendor.Add(new SqlParameter("@AccountID", objclsDTOVendorInformation.intAccountHead));

            
             prmVendor.Add(new SqlParameter("@Email", objclsDTOVendorInformation.strEmail));
             prmVendor.Add(new SqlParameter("@Website", objclsDTOVendorInformation.strWebsite));
             prmVendor.Add(new SqlParameter("@VendorClassificationID", objclsDTOVendorInformation.intVendorClassificationID));
             prmVendor.Add(new SqlParameter("@RatingRemarks", objclsDTOVendorInformation.strRatingRemarks));
             prmVendor.Add(new SqlParameter("@Rating", objclsDTOVendorInformation.dblRatting));
             if (objclsDTOVendorInformation.intStatusID > 0)
                 prmVendor.Add(new SqlParameter("@StatusID", objclsDTOVendorInformation.intStatusID));
             
             if (objclsDTOVendorInformation.decAdvanceLimit > 0)
                 prmVendor.Add(new SqlParameter("@AdvanceLimit", objclsDTOVendorInformation.decAdvanceLimit));
             if (objclsDTOVendorInformation.decCreditLimit > 0)
                 prmVendor.Add(new SqlParameter("@CreditLimit", objclsDTOVendorInformation.decCreditLimit));

             //if (objclsDTOVendorInformation.decSalaryOrIncome > 0)
             //    prmVendor.Add(new SqlParameter("@SalaryOrIncome", objclsDTOVendorInformation.decSalaryOrIncome));
             //prmVendor.Add(new SqlParameter("@Gender", objclsDTOVendorInformation.strGender));
             //if (objclsDTOVendorInformation.intDepartmentID > 0)
             //    prmVendor.Add(new SqlParameter("@DepartmentID", objclsDTOVendorInformation.intDepartmentID));
             //if (objclsDTOVendorInformation.intDesignationID > 0)
             //    prmVendor.Add(new SqlParameter("@DesignationID", objclsDTOVendorInformation.intDesignationID));
             //if (objclsDTOVendorInformation.intEmploymentTypeID > 0)
             //    prmVendor.Add(new SqlParameter("@EmploymentTypeID", objclsDTOVendorInformation.intEmploymentTypeID));
             //if (objclsDTOVendorInformation.intAccountTypeID > 0)
             //   prmVendor.Add(new SqlParameter("@SalaryAccountTypeID", objclsDTOVendorInformation.intAccountTypeID));
          
             if (objclsDTOVendorInformation.intSourceMediaID > 0)
                 prmVendor.Add(new SqlParameter("@SourceMediaID", objclsDTOVendorInformation.intSourceMediaID));

             if (objclsDTOVendorInformation.intSupplierCurrencyID > 0)
                 prmVendor.Add(new SqlParameter("@CurrencyID", objclsDTOVendorInformation.intSupplierCurrencyID));
             if (objclsDTOVendorInformation.intCreditDaysLimit > 0)
                 prmVendor.Add(new SqlParameter("@CreditLimitDays", objclsDTOVendorInformation.intCreditDaysLimit));
             SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
             objParam.Direction = ParameterDirection.ReturnValue;
             prmVendor.Add(objParam);

             if (objclsConnection.ExecuteNonQuery(strProcedureName, prmVendor, out objOutVendorID) != 0)
             {
                 if ((int)objOutVendorID > 0)
                 {
                     objclsDTOVendorInformation.intVendorID = (int)objOutVendorID;
                     if (objclsDTOVendorInformation.intVendorType == (int)VendorType.Supplier)
                     {
                         AddNearestCompetitor();
                     }
                     return objclsDTOVendorInformation.intVendorID;
                 }
             }
             return 0;
         }
         public void AddNearestCompetitor()
         {
             foreach (clsDTOVendorNearestCompetitor objVendorNearestCompetitor in objclsDTOVendorInformation.lstNearestCompetitor)
             {
                 prmVendor = new ArrayList();
                 prmVendor.Add(new SqlParameter("@Mode", 31));
                 prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));
                 prmVendor.Add(new SqlParameter("@NearestCompetitorID", objVendorNearestCompetitor.intNearestCompetitorID));
                 objclsConnection.ExecuteNonQuery(strProcedureName, prmVendor);
             }
         }

        public bool UpdateVendorInfo()
        {
            object objOutVendorID = 0;

            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 3));
            prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));
            prmVendor.Add(new SqlParameter("@VendorAddID", objclsDTOVendorInformation.intVendorAddID));
            prmVendor.Add(new SqlParameter("@VendorTypeID", objclsDTOVendorInformation.intVendorType));
            prmVendor.Add(new SqlParameter("@VendorCode", objclsDTOVendorInformation.strVendorCode));
            prmVendor.Add(new SqlParameter("@SerialNo", objclsDTOVendorInformation.intSerialNo));
            prmVendor.Add(new SqlParameter("@VendorName", objclsDTOVendorInformation.strVendorName));
            //prmVendor.Add(new SqlParameter("@ShortName", objclsDTOVendorInformation.strShortName));
            prmVendor.Add(new SqlParameter("@TransactionTypeID", objclsDTOVendorInformation.intTransType));

            if (objclsDTOVendorInformation.intBranchID > 0)
                prmVendor.Add(new SqlParameter("@BankBranchID", objclsDTOVendorInformation.intBranchID));
            if (objclsDTOVendorInformation.intBankID > 0)
                prmVendor.Add(new SqlParameter("@BankID", objclsDTOVendorInformation.intBankID));
            
            prmVendor.Add(new SqlParameter("@AccountNumber", objclsDTOVendorInformation.strAccountNumber));

            if (objclsDTOVendorInformation.intAccountHead > 0)
                prmVendor.Add(new SqlParameter("@AccountID", objclsDTOVendorInformation.intAccountHead));

            prmVendor.Add(new SqlParameter("@Email", objclsDTOVendorInformation.strEmail));
            prmVendor.Add(new SqlParameter("@Website", objclsDTOVendorInformation.strWebsite));
            prmVendor.Add(new SqlParameter("@VendorClassificationID", objclsDTOVendorInformation.intVendorClassificationID));
            prmVendor.Add(new SqlParameter("@RatingRemarks", objclsDTOVendorInformation.strRatingRemarks));
            prmVendor.Add(new SqlParameter("@Rating", objclsDTOVendorInformation.dblRatting));
            prmVendor.Add(new SqlParameter("@TRNNo", objclsDTOVendorInformation.strTRNNo));
            if (objclsDTOVendorInformation.intStatusID > 0)
                prmVendor.Add(new SqlParameter("@StatusID", objclsDTOVendorInformation.intStatusID));
            //if (objclsDTOVendorInformation.decSalaryOrIncome > 0)
            //    prmVendor.Add(new SqlParameter("@SalaryOrIncome", objclsDTOVendorInformation.decSalaryOrIncome));
            if (objclsDTOVendorInformation.decAdvanceLimit > 0)
                prmVendor.Add(new SqlParameter("@AdvanceLimit", objclsDTOVendorInformation.decAdvanceLimit));
            if (objclsDTOVendorInformation.decCreditLimit > 0)
                prmVendor.Add(new SqlParameter("@CreditLimit", objclsDTOVendorInformation.decCreditLimit));
            //prmVendor.Add(new SqlParameter("@Gender", objclsDTOVendorInformation.strGender));
            //if (objclsDTOVendorInformation.intDepartmentID > 0)
            //    prmVendor.Add(new SqlParameter("@DepartmentID", objclsDTOVendorInformation.intDepartmentID));
            //if (objclsDTOVendorInformation.intDesignationID > 0)
            //    prmVendor.Add(new SqlParameter("@DesignationID", objclsDTOVendorInformation.intDesignationID));
            //if (objclsDTOVendorInformation.intEmploymentTypeID > 0)
            //    prmVendor.Add(new SqlParameter("@EmploymentTypeID", objclsDTOVendorInformation.intEmploymentTypeID));
            //if (objclsDTOVendorInformation.intAccountTypeID > 0)
            //    prmVendor.Add(new SqlParameter("@SalaryAccountTypeID", objclsDTOVendorInformation.intAccountTypeID));

            if (objclsDTOVendorInformation.intSourceMediaID > 0)
                prmVendor.Add(new SqlParameter("@SourceMediaID", objclsDTOVendorInformation.intSourceMediaID));

            if (objclsDTOVendorInformation.intSupplierCurrencyID > 0)
                prmVendor.Add(new SqlParameter("@CurrencyID", objclsDTOVendorInformation.intSupplierCurrencyID));
            if (objclsDTOVendorInformation.intCreditDaysLimit > 0)
                prmVendor.Add(new SqlParameter("@CreditLimitDays", objclsDTOVendorInformation.intCreditDaysLimit));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmVendor.Add(objParam);

            if (objclsConnection.ExecuteNonQuery(strProcedureName, prmVendor, out objOutVendorID) != 0)
            {
                if (objclsDTOVendorInformation.intVendorType == (int)VendorType.Supplier)
                {
                    DeleteNearestCompitetor();
                    AddNearestCompetitor();
                }
                if ((int)objOutVendorID > 0)
                {
                    objclsDTOVendorInformation.intVendorID = (int)objOutVendorID;
                    return true;
                }
            }
            return false;
        }

        public bool UpdatePermanantVendorAddress() // update Permanant address 
        {
            object objOutVendorAddID = 0;

            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 17));
            prmVendor.Add(new SqlParameter("@VendorAddID", objclsDTOVendorInformation.intVendorAddID));
            prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));
            prmVendor.Add(new SqlParameter("@AddressName", objclsDTOVendorInformation.strAddressName));
            prmVendor.Add(new SqlParameter("@ContactPerson", objclsDTOVendorInformation.strContactPerson));

            if (objclsDTOVendorInformation.intCountry > 0)
                prmVendor.Add(new SqlParameter("@CountryID", objclsDTOVendorInformation.intCountry));

            prmVendor.Add(new SqlParameter("@State", objclsDTOVendorInformation.strState));
            prmVendor.Add(new SqlParameter("@Address", objclsDTOVendorInformation.strAddress));
            prmVendor.Add(new SqlParameter("@Telephone", objclsDTOVendorInformation.strTelephoneNo));
            prmVendor.Add(new SqlParameter("@Fax", objclsDTOVendorInformation.strFax));
            prmVendor.Add(new SqlParameter("@ZipCode", objclsDTOVendorInformation.strZipCode));


            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmVendor.Add(objParam);

            if (objclsConnection.ExecuteNonQuery(strProcedureName, prmVendor, out objOutVendorAddID) != 0)
            {
                if ((int)objOutVendorAddID > 0)
                {
                    objclsDTOVendorInformation.intVendorAddID = (int)objOutVendorAddID;
                    return true;
                }
            }
            return false;
        }
        
        public bool DeleteVendorInfo()
        {
            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 4));
            prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));

            if (objclsConnection.ExecuteNonQuery(strProcedureName, prmVendor) != 0)
                return true;
            else
                return false;
        }
        public bool DeleteNearestCompitetor()
        {
            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 32));
            prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));

            if (objclsConnection.ExecuteNonQuery(strProcedureName, prmVendor) != 0)
                return true;
            else
                return false;
        
        }
        public bool FindVendorInfo(int intRowNum, int MintVendorType)
        {
            dat = new DataTable();
            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 1));
            prmVendor.Add(new SqlParameter("@RowNum", intRowNum));
            prmVendor.Add(new SqlParameter("@VendorTypeID", MintVendorType));
            prmVendor.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
            dat = objclsConnection.ExecuteDataTable(strProcedureName, prmVendor);

            if (dat.Rows.Count > 0)
            {
                objclsDTOVendorInformation.intVendorID = Convert.ToInt32(dat.Rows[0]["VendorID"]);
                objclsDTOVendorInformation.strVendorName = Convert.ToString(dat.Rows[0]["VendorName"]);
                //objclsDTOVendorInformation.strShortName = Convert.ToString(dat.Rows[0]["ShortName"]);
                objclsDTOVendorInformation.intVendorType = Convert.ToInt32(dat.Rows[0]["VendorTypeID"]);
                objclsDTOVendorInformation.strContactPerson = Convert.ToString(dat.Rows[0]["ContactPerson"]);
                objclsDTOVendorInformation.strAddressName = Convert.ToString(dat.Rows[0]["AddressName"]);
                objclsDTOVendorInformation.strAddress = Convert.ToString(dat.Rows[0]["Address"]);
                objclsDTOVendorInformation.strEmail = Convert.ToString(dat.Rows[0]["Email"]);
                objclsDTOVendorInformation.strFax = Convert.ToString(dat.Rows[0]["Fax"]);
                objclsDTOVendorInformation.strWebsite = Convert.ToString(dat.Rows[0]["Website"]);
                objclsDTOVendorInformation.strTelephoneNo = Convert.ToString(dat.Rows[0]["Telephone"]);
                objclsDTOVendorInformation.strMobile1 = Convert.ToString(dat.Rows[0]["MobileNo"]);
                objclsDTOVendorInformation.strAccountNumber = Convert.ToString(dat.Rows[0]["AccountNumber"]);

                objclsDTOVendorInformation.intAccountHead = dat.Rows[0]["AccountID"].ToInt32();

                objclsDTOVendorInformation.intBranchID = dat.Rows[0]["BankBranchID"].ToInt32();

                objclsDTOVendorInformation.intBankID = dat.Rows[0]["BankID"].ToInt32();

                objclsDTOVendorInformation.strVendorCode = Convert.ToString(dat.Rows[0]["VendorCode"]);
                objclsDTOVendorInformation.intSerialNo = Convert.ToInt32(dat.Rows[0]["SerialNo"]);
                objclsDTOVendorInformation.intTransType = Convert.ToInt32(dat.Rows[0]["TransactionTypeID"]);
                objclsDTOVendorInformation.intCountry = Convert.ToInt32(dat.Rows[0]["CountryID"]);
                objclsDTOVendorInformation.strZipCode = Convert.ToString(dat.Rows[0]["ZipCode"]);
                objclsDTOVendorInformation.strState = Convert.ToString(dat.Rows[0]["State"]);
                objclsDTOVendorInformation.intVendorAddID = Convert.ToInt32(dat.Rows[0]["VendorAddID"]);

                objclsDTOVendorInformation.intVendorClassificationID = dat.Rows[0]["VendorClassificationID"].ToInt32();

                objclsDTOVendorInformation.strRatingRemarks = Convert.ToString(dat.Rows[0]["RatingRemarks"]);

                objclsDTOVendorInformation.dblRatting = dat.Rows[0]["Rating"].ToDouble();

                objclsDTOVendorInformation.intStatusID = Convert.ToInt32(dat.Rows[0]["StatusID"]);

                //if (dat.Rows[0]["SalaryOrIncome"] != DBNull.Value)
                //    objclsDTOVendorInformation.decSalaryOrIncome = Convert.ToDecimal(dat.Rows[0]["SalaryOrIncome"]);
                //else
                //    objclsDTOVendorInformation.decSalaryOrIncome = 0;

                if (dat.Rows[0]["AdvanceLimit"] != DBNull.Value)
                    objclsDTOVendorInformation.decAdvanceLimit = dat.Rows[0]["AdvanceLimit"].ToDecimal();
                else
                    objclsDTOVendorInformation.decAdvanceLimit = 0;

                if (dat.Rows[0]["CreditLimit"] != DBNull.Value)
                    objclsDTOVendorInformation.decCreditLimit = dat.Rows[0]["CreditLimit"].ToDecimal();
                else
                    objclsDTOVendorInformation.decCreditLimit = 0;
                if (dat.Rows[0]["CreditLimitDays"] != DBNull.Value)
                    objclsDTOVendorInformation.intCreditDaysLimit = dat.Rows[0]["CreditLimitDays"].ToInt32();
                else
                    objclsDTOVendorInformation.intCreditDaysLimit = 0;
                //objclsDTOVendorInformation.strGender = Convert.ToString(dat.Rows[0]["Gender"]);

                //objclsDTOVendorInformation.intDepartmentID = dat.Rows[0]["DepartmentID"].ToInt32();
                //objclsDTOVendorInformation.intDesignationID = dat.Rows[0]["DesignationID"].ToInt32();
                //objclsDTOVendorInformation.intEmploymentTypeID = dat.Rows[0]["EmploymentTypeID"].ToInt32();
                //objclsDTOVendorInformation.intAccountTypeID = dat.Rows[0]["SalaryAccountTypeID"].ToInt32();

                objclsDTOVendorInformation.intSourceMediaID = dat.Rows[0]["SourceMediaID"].ToInt32();

                objclsDTOVendorInformation.intSupplierCurrencyID = dat.Rows[0]["CurrencyID"].ToInt32();
                objclsDTOVendorInformation.strTRNNo = dat.Rows[0]["TRNNo"].ToStringCustom();

                FindVendorAddress();
                return true;
            }
            return false;
        }

        public DataTable DtGetAddressName()
        {
            sRecordValues = new DataTable();

            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = objclsConnection;
                objCommonUtility.GetRecordValue(new string[] { "VendorAddID,AddressName", "InvVendorAddress", "VendorID=" + objclsDTOVendorInformation.intVendorID + "" }, out sRecordValues);
                return sRecordValues;
            }
        }

        public string GetFormsInUse(string strCaption, int intPrimeryId, string strCondition, string strFileds)
        {
            return new ClsWebform().GetUsedFormsInformation(strCaption, intPrimeryId, strCondition, strFileds);
        }

        public bool CheckValidEmail(string sEmailAddress)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                return objCommonUtility.CheckValidEmail(sEmailAddress);
            }
        }

        public bool CheckDuplication(bool blnAddStatus, string[] sarValues, int intId,int intVendorTypeID)
        {
            string sField = "VendorID";
            string sTable = "InvVendorInformation";
            string sCondition = "";

            if (blnAddStatus)
                sCondition = "VendorCode='" + sarValues[0] + "' And VendorTypeID = "+intVendorTypeID+"";
            else
                sCondition = "VendorCode='" + sarValues[0] + "' and VendorID<>" + intId + " And VendorTypeID = "+intVendorTypeID+"";

            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = objclsConnection;
                return objCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
            }
        }
        public bool IsPermanentAddressExist(bool blnAddStatus, string[] sarValues, int intId, int vendorAddID)
        {
            string sField = "VendorID";
            string sTable = "InvVendorAddress";
            string sCondition = "";

            if (blnAddStatus)
                //sCondition = "AddressName='" + sarValues[0] + "'";
                sCondition = "AddressName='" + sarValues[0] + "' and VendorID=" + intId + "";
            else
                sCondition = "AddressName='" + sarValues[0] + "' and VendorID='" + intId + "' and VendorAddID<>" + vendorAddID + "";

            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = objclsConnection;
                return objCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
            }
        }
        public bool CheckMobileNoDuplication(bool blnAddStatus, string[] sarValues, int vendorID)
        {
            string sField = "VendorAddID";
            string sTable = "InvVendorAddress INNER JOIN InvVendorInformation on InvVendorAddress.VendorID=dbo.InvVendorInformation.VendorID";
            string sCondition = "";

            if (blnAddStatus)
                sCondition = "Telephone='" + sarValues[0] + "' and VendorTypeID=" + (int)VendorType.Customer + "";
            else
                sCondition = "Telephone='" + sarValues[0] + "' and InvVendorAddress.VendorID<>'" + vendorID + "' and VendorTypeID=" + (int)VendorType.Customer + "";

            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = objclsConnection;
                return objCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
            }
        }

        public bool CheckLocationTypeDuplication(bool blnAddStatus, string[] sarValues, int vendorID, int vendorAddID)
        {
            return false;
        }
        
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }
        
        public DataTable GetAccount(int intGrpCode)
        {
            prmGetAccount = new ArrayList();
            prmGetAccount.Add(new SqlParameter("@ParentID", intGrpCode));
           prmGetAccount.Add(new SqlParameter("@CompID", 0 ));
            return objclsConnection.ExecuteDataTable("spAccGetAccounts", prmGetAccount);
        }
        public DataTable FillNearestCompetitor()
        {
            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 33));
            prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));
            return objclsConnection.ExecuteDataTable(strProcedureName, prmVendor);
        }
        public DataTable GetNearestCompetitor(int intVendorID)
        {
            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 29));
            prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));
            return objclsConnection.ExecuteDataTable(strProcedureName, prmVendor);
        }
        public DataTable GetNearestCompetitorRating(int iVendorID)
        {
            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 28));
            prmVendor.Add(new SqlParameter("@VendorID",iVendorID));
            return objclsConnection.ExecuteDataTable(strProcedureName, prmVendor);
        }
        public bool CheckExistReferences()
        {
            dat = new DataTable();
            prmVendor = new ArrayList();
            string strCondition = "name <> 'InvVendorInformation' and name <> 'InvVendorAddress' and name <> 'InvVendorCompanyDetails'";
            prmVendor.Add(new SqlParameter("@PrimeryID", objclsDTOVendorInformation.intVendorID));
            prmVendor.Add(new SqlParameter("@Condition", strCondition));
            prmVendor.Add(new SqlParameter("@FieldName", "VendorID"));
            dat = objclsConnection.ExecuteDataSet("spCheckIDExists", prmVendor).Tables[0];
            if (dat.Rows.Count > 0 && dat.Rows[0]["FormName"].ToString() != "0" )
                return true;
            else
                return false;
        }

        public int RecCountNavigate(int MintVendorType)
        {
            int intRecordCnt = 0;

            dat = new DataTable();
            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 11));
            prmVendor.Add(new SqlParameter("@VendorTypeID", MintVendorType));
            prmVendor.Add(new SqlParameter("@CompanyID",ClsCommonSettings.CompanyID ));

            dat = objclsConnection.ExecuteDataTable(strProcedureName, prmVendor);

            if (dat.Rows.Count > 0)
                intRecordCnt = Convert.ToInt32(dat.Rows[0]["cnt"]);

            return intRecordCnt;
        }

        public bool AddAddress()
        {
            object objOutVendorAddID = 0;

            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 5));
            prmVendor.Add(new SqlParameter("@VendorAddID", objclsDTOVendorInformation.intVendorAddID));
            prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));
            prmVendor.Add(new SqlParameter("@AddressName", objclsDTOVendorInformation.strAddressName));
            prmVendor.Add(new SqlParameter("@ContactPerson", objclsDTOVendorInformation.strContactPerson));

            if ( objclsDTOVendorInformation.intCountry > 0) 
             prmVendor.Add(new SqlParameter("@CountryID", objclsDTOVendorInformation.intCountry));
         
            prmVendor.Add(new SqlParameter("@State", objclsDTOVendorInformation.strState));
            prmVendor.Add(new SqlParameter("@Address", objclsDTOVendorInformation.strAddress));
            prmVendor.Add(new SqlParameter("@Telephone", objclsDTOVendorInformation.strTelephoneNo));
            prmVendor.Add(new SqlParameter("@Fax", objclsDTOVendorInformation.strFax));
            prmVendor.Add(new SqlParameter("@ZipCode", objclsDTOVendorInformation.strZipCode));
            prmVendor.Add(new SqlParameter("@MobileNo", objclsDTOVendorInformation.strMobile1));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmVendor.Add(objParam);

            if (objclsConnection.ExecuteNonQuery(strProcedureName, prmVendor, out objOutVendorAddID) != 0)
            {
                if ((int)objOutVendorAddID > 0)
                {
                    objclsDTOVendorInformation.intVendorAddID = (int)objOutVendorAddID;
                    return true;
                }
            }
            return false;
        }

        public bool UpdateAddress()
        {
            object objOutVendorAddID = 0;

            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 6));
            prmVendor.Add(new SqlParameter("@VendorAddID", objclsDTOVendorInformation.intVendorAddID));
            prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));
            prmVendor.Add(new SqlParameter("@AddressName", objclsDTOVendorInformation.strAddressName));
            prmVendor.Add(new SqlParameter("@ContactPerson", objclsDTOVendorInformation.strContactPerson));

            if (objclsDTOVendorInformation.intCountry > 0)
                prmVendor.Add(new SqlParameter("@CountryID", objclsDTOVendorInformation.intCountry));
         
            prmVendor.Add(new SqlParameter("@State", objclsDTOVendorInformation.strState));
            prmVendor.Add(new SqlParameter("@Address", objclsDTOVendorInformation.strAddress));
            prmVendor.Add(new SqlParameter("@Telephone", objclsDTOVendorInformation.strTelephoneNo));
            prmVendor.Add(new SqlParameter("@MobileNo", objclsDTOVendorInformation.strMobile1));
            prmVendor.Add(new SqlParameter("@Fax", objclsDTOVendorInformation.strFax));
            prmVendor.Add(new SqlParameter("@ZipCode", objclsDTOVendorInformation.strZipCode));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmVendor.Add(objParam);

            if (objclsConnection.ExecuteNonQuery(strProcedureName, prmVendor, out objOutVendorAddID) != 0)
            {
                if ((int)objOutVendorAddID > 0)
                {
                    objclsDTOVendorInformation.intVendorAddID = (int)objOutVendorAddID;
                    return true;
                }
            }
            return false;
        }

        public bool DeleteAddress()
        {
            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 8));
            prmVendor.Add(new SqlParameter("@VendorAddID", objclsDTOVendorInformation.intVendorAddID));

            if (objclsConnection.ExecuteNonQuery(strProcedureName, prmVendor) != 0)
                return true;
            else
                return false;
        }

        public bool FindVendorAddress()
        {
            dat = new DataTable();
            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 7));
            prmVendor.Add(new SqlParameter("@VendorAddID", objclsDTOVendorInformation.intVendorAddID));

            dat = objclsConnection.ExecuteDataTable(strProcedureName, prmVendor);

            if (dat.Rows.Count > 0)
            {
                objclsDTOVendorInformation.strContactPerson = Convert.ToString(dat.Rows[0]["ContactPerson"]);
                    objclsDTOVendorInformation.intCountry = dat.Rows[0]["CountryID"].ToInt32();
                objclsDTOVendorInformation.strAddress = Convert.ToString(dat.Rows[0]["Address"]);
                objclsDTOVendorInformation.strTelephoneNo = Convert.ToString(dat.Rows[0]["Telephone"]);
                objclsDTOVendorInformation.strFax = Convert.ToString(dat.Rows[0]["Fax"]);
                objclsDTOVendorInformation.strState = Convert.ToString(dat.Rows[0]["State"]);
                objclsDTOVendorInformation.strZipCode = Convert.ToString(dat.Rows[0]["ZipCode"]);
                objclsDTOVendorInformation.strAddressName = Convert.ToString(dat.Rows[0]["AddressName"]);
                objclsDTOVendorInformation.strMobile1 = Convert.ToString(dat.Rows[0]["MobileNo"]);
                return true;
            }
            return false;
        }

        public int GetRowNumber()
        {
            dat = new DataTable();
            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 10));
            prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));
            prmVendor.Add(new SqlParameter("@VendorTypeID", objclsDTOVendorInformation.intVendorType));
            prmVendor.Add(new SqlParameter("@CompanyID",ClsCommonSettings.CompanyID ));

            dat = objclsConnection.ExecuteDataTable(strProcedureName, prmVendor);

            if (dat.Rows.Count > 0)
                return Convert.ToInt32(dat.Rows[0]["RowNumber"]);
            else
                return 0;
        }
        /// <summary>
        /// Created By   : Laxmi
        /// Created date :04-03-2011
        /// Purpose      : Get all associates from associates page control
        /// </summary>
        //public DataSet GetAllAssociates() 
        //{
        //    prmVendor = new ArrayList();
        //    prmVendor.Add(new SqlParameter("@mode", 14));
        //    return objclsConnection.ExecuteDataSet(strProcedureName, prmVendor);
        //}
        //public int GetSingleAssociates() 
        //{
        //    prmVendor = new ArrayList();
        //    prmVendor.Add(new SqlParameter("@mode", 20));
        //    prmVendor.Add(new SqlParameter("@SearchKey", objclsDTOVendorInformation.strSearchKey));
        //    return Convert.ToInt32( objclsConnection.ExecuteScalar(strProcedureName, prmVendor));
        //}
        
        //public DataSet GetAssociateAddress(int intRowNum) // All address
        //{
        //    prmVendor = new ArrayList();
        //    prmVendor.Add(new SqlParameter("@mode", 15));
        //    prmVendor.Add(new SqlParameter("@VendorID", intRowNum));
        //    return objclsConnection.ExecuteDataSet(strProcedureName, prmVendor);
        //}

        //public SqlDataReader GetSingleAssociateAddress(int intVendorAddressID,int intVendorID)  //Single address
        //{
        //    prmVendor = new ArrayList();
        //    prmVendor.Add(new SqlParameter("@mode", 16));
        //    prmVendor.Add(new SqlParameter("@VendorAddID", intVendorAddressID));
        //    prmVendor.Add(new SqlParameter("@VendorID", intVendorID));
        //    return objclsConnection.ExecuteReader(strProcedureName, prmVendor);
        //}

        public bool IsVendorExists() // duplication of vendor
        {
            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 19));
            prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));
            prmVendor.Add(new SqlParameter("@VendorName", objclsDTOVendorInformation.strVendorName));
            prmVendor.Add(new SqlParameter("@Telephone", objclsDTOVendorInformation.strTelephoneNo));
            return (Convert.ToInt32(objclsConnection.ExecuteScalar(strProcedureName, prmVendor)) > 0 ? true : false);
        }
        /// <summary>
        /// Created By   : Thasni Latheef
        /// Created date :07-03-2011
        /// Purpose      : Get all customers from associates page control
        /// </summary>
        //public DataSet GetAllCustomers()
        //{
        //    prmVendor = new ArrayList();
        //    prmVendor.Add(new SqlParameter("@mode", 18));
        //    prmVendor.Add(new SqlParameter("@PageIndex", objclsDTOVendorInformation.intPageIndex));
        //    prmVendor.Add(new SqlParameter("@PageSize", objclsDTOVendorInformation.intPageSize));
         
        //    return objclsConnection.ExecuteDataSet(strProcedureName, prmVendor);
        //}

        public DataSet GetVendorReport()
        {
            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 21));
            prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));
            return objclsConnection.ExecuteDataSet(strProcedureName, prmVendor);
        }

        //public bool IsPermanentAddressExist(bool blnAddStatus, string[] sarValues, int intvendorId)
        //{
        //    prmVendor = new ArrayList();
        //    if (blnAddStatus)
        //    {
        //        prmVendor.Add(new SqlParameter("@Mode", 22));
        //    }
        //    else
        //    {
        //        prmVendor.Add(new SqlParameter("@Mode", 23));
        //    }
        //    prmVendor.Add(new SqlParameter("@VendorID", intvendorId));
        //    prmVendor.Add(new SqlParameter("@VendorAddID", intAddressId));
        //    return (Convert.ToInt32(objclsConnection.ExecuteScalar(strProcedureName, prmVendor)) > 0 ? true : false);
        //}

        public string GetVendorMobileNo(long iVendorID)
        {
            try
            {
                prmVendor = new ArrayList();
                prmVendor.Add(new SqlParameter("@Mode", 24));
                prmVendor.Add(new SqlParameter("@VendorID", iVendorID));
                return objclsConnection.ExecuteScalar(strProcedureName, prmVendor).ToString();
            }
            catch (Exception ex) { throw ex; }
        }
        public int GenerateCode(int intFormType)
        {
            int intRetValue = 0;
        //    string strProcedureName = string.Empty;
            ArrayList parameters=new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 26));
            parameters.Add(new SqlParameter("@VendorTypeID", intFormType));
            SqlDataReader sdr = objclsConnection.ExecuteReader(strProcedureName, parameters);
            if (sdr.Read())
            {
                if (sdr["ReturnValue"] != DBNull.Value)
                    intRetValue = Convert.ToInt32(sdr["ReturnValue"]);
                else 
                    intRetValue = 0;
            }
            sdr.Close();
            return intRetValue;
        }
        public bool SearchVendor(int SearchOption, string SearchCriteria, int RowNum,int intVendorTypeID, out int TotalRows)
        {
            ArrayList parameters = new ArrayList();
            TotalRows = 0;
            parameters.Add(new SqlParameter("@Mode", 27));
            parameters.Add(new SqlParameter("@SearchOption", SearchOption));
            parameters.Add(new SqlParameter("@SearchCriteria", SearchCriteria.Replace("'", "''")));
            parameters.Add(new SqlParameter("@RowNum", RowNum));
            parameters.Add(new SqlParameter("@VendorTypeID", intVendorTypeID));
            SqlDataReader sdr = objclsConnection.ExecuteReader(strProcedureName, parameters, CommandBehavior.SingleRow);
            bool blnReturnValue = ReadVendorInformation(sdr);
            sdr.NextResult();
            if (sdr.Read())
            {
                TotalRows = Convert.ToInt32(sdr["RecordCount"]);  
            }
            sdr.Close();
            return blnReturnValue;
        }
        public bool ReadVendorInformation(SqlDataReader sdr)
        {
            bool blnReturnValue;

            if (sdr.Read())
            {
                objclsDTOVendorInformation.intVendorID = Convert.ToInt32(sdr["VendorID"]);
                objclsDTOVendorInformation.intVendorType = Convert.ToInt32(sdr["VendorTypeID"]);
                objclsDTOVendorInformation.strVendorCode = Convert.ToString(sdr["VendorCode"]);
                objclsDTOVendorInformation.strVendorName = Convert.ToString(sdr["VendorName"]);
                //objclsDTOVendorInformation.strShortName = Convert.ToString(sdr["ShortName"]);
                if (sdr["TransactionTypeID"] != DBNull.Value)
                    objclsDTOVendorInformation.intTransType = Convert.ToInt32(sdr["TransactionTypeID"]);
                if (sdr["BankBranchID"] != DBNull.Value)
                    objclsDTOVendorInformation.intBranchID = Convert.ToInt32(sdr["BankBranchID"]);
                if (sdr["BankId"] != DBNull.Value)
                    objclsDTOVendorInformation.intBankID = Convert.ToInt32(sdr["BankId"]);
                objclsDTOVendorInformation.strEmail = Convert.ToString(sdr["Email"]);
                objclsDTOVendorInformation.strWebsite = Convert.ToString(sdr["Website"]);
                if (sdr["StatusID"] != DBNull.Value)
                    objclsDTOVendorInformation.intStatusID = Convert.ToInt32(sdr["StatusID"]);
                if (sdr["Rating"] != DBNull.Value) 
                    objclsDTOVendorInformation.dblRatting = sdr["Rating"].ToDouble();
                objclsDTOVendorInformation.strRatingRemarks = Convert.ToString(sdr["RatingRemarks"]);
                if (sdr["VendorClassificationID"] != DBNull.Value) 
                    objclsDTOVendorInformation.intVendorClassificationID = Convert.ToInt32(sdr["VendorClassificationID"]);
                //if (sdr["EmploymentTypeID"] != DBNull.Value)
                //    objclsDTOVendorInformation.intEmploymentTypeID = Convert.ToInt32(sdr["EmploymentTypeID"]);
                //if (sdr["DepartmentID"] != DBNull.Value)
                //    objclsDTOVendorInformation.intDepartmentID = Convert.ToInt32(sdr["DepartmentID"]);
                //if (sdr["DesignationID"] != DBNull.Value)
                //    objclsDTOVendorInformation.intDesignationID = Convert.ToInt32(sdr["DesignationID"]);
                //objclsDTOVendorInformation.strGender = Convert.ToString(sdr["Gender"]);
                //if (sdr["SalaryAccountTypeID"] != DBNull.Value)
                //    objclsDTOVendorInformation.intAccountTypeID = Convert.ToInt32(sdr["SalaryAccountTypeID"]);
                if (sdr["AccountID"] != DBNull.Value)
                    objclsDTOVendorInformation.intAccountHead = Convert.ToInt32(sdr["AccountID"]);
                else
                    objclsDTOVendorInformation.intAccountHead = 0;
                //if (sdr["SalaryOrIncome"] != DBNull.Value)
                //    objclsDTOVendorInformation.decSalaryOrIncome = sdr["SalaryOrIncome"].ToDecimal();
                //else
                //    objclsDTOVendorInformation.decSalaryOrIncome = 0;
                if (sdr["AdvanceLimit"] != DBNull.Value)
                    objclsDTOVendorInformation.decAdvanceLimit = sdr["AdvanceLimit"].ToDecimal();
                else
                    objclsDTOVendorInformation.decAdvanceLimit = 0;
                if (sdr["CreditLimit"] != DBNull.Value)
                    objclsDTOVendorInformation.decCreditLimit = sdr["CreditLimit"].ToDecimal();
                else
                    objclsDTOVendorInformation.decCreditLimit = 0;
                if (sdr["CreditLimitDays"] != DBNull.Value)
                    objclsDTOVendorInformation.intCreditDaysLimit = sdr["CreditLimitDays"].ToInt32();
                else
                    objclsDTOVendorInformation.intCreditDaysLimit = 0;
                objclsDTOVendorInformation.intVendorAddID = Convert.ToInt32(sdr["VendorAddID"]);
                objclsDTOVendorInformation.strAddressName = Convert.ToString(sdr["AddressName"]);
                objclsDTOVendorInformation.strContactPerson = Convert.ToString(sdr["ContactPerson"]);
                objclsDTOVendorInformation.strAddress = Convert.ToString(sdr["Address"]);
                if (sdr["CountryId"] != DBNull.Value)
                    objclsDTOVendorInformation.intCountry = Convert.ToInt32(sdr["CountryId"]);
                objclsDTOVendorInformation.strState = Convert.ToString(sdr["State"]);
                objclsDTOVendorInformation.strZipCode = Convert.ToString(sdr["ZipCode"]);
                objclsDTOVendorInformation.strTelephoneNo = Convert.ToString(sdr["Telephone"]);
                objclsDTOVendorInformation.strMobile1 = Convert.ToString(sdr["MobileNo"]);
                objclsDTOVendorInformation.strFax = Convert.ToString(sdr["Fax"]);
                objclsDTOVendorInformation.intSupplierCurrencyID = sdr["CurrencyID"].ToInt32();
                objclsDTOVendorInformation.strTRNNo = sdr["TRNNo"].ToString();
                blnReturnValue = true;
            }
            else
            {
                blnReturnValue = false;
            }
            return blnReturnValue;
        
        }


        public bool CheckVendorAlreadyUsed() // duplication of vendor
        {
            prmVendor = new ArrayList();
            prmVendor.Add(new SqlParameter("@Mode", 40));
            prmVendor.Add(new SqlParameter("@VendorID", objclsDTOVendorInformation.intVendorID));
            return (Convert.ToInt32(objclsConnection.ExecuteScalar(strProcedureName, prmVendor)) > 0 ? true : false);
        }
        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (dat != null)
                dat.Dispose();

            if (sRecordValues != null)
                sRecordValues.Dispose();

            if (prmVendor != null)
                prmVendor = null;

            if (prmGetAccount != null)
                prmGetAccount = null;
        }

        #endregion
    }
}