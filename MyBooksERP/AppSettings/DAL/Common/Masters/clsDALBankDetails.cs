﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,17 Feb 2011>
Description:	<Description,,DAL for Bank>
================================================
*/

namespace MyBooksERP
{
       
   public class clsDALBankDetails:IDisposable
    {
        ArrayList parameters;
        SqlDataReader sdrdr;
        ArrayList fmParameters;

        public clsDTOBankDetails objclsDTOBankDetails { get; set; }
        public DataLayer objclsConnection { get; set; }


        // To Save and Update

         public bool SaveBankDetails(bool AddStatus)
        {
            object objoutBankNameID = 0;

                parameters = new ArrayList();
                if (AddStatus)
                    parameters.Add(new SqlParameter("@Mode", 2));
                else
                    parameters.Add(new SqlParameter("@Mode", 3));


                parameters.Add(new SqlParameter("@BankBranchID", objclsDTOBankDetails.intBankNameID));
                parameters.Add(new SqlParameter("@BankBranchName", objclsDTOBankDetails.strDescription));
                parameters.Add(new SqlParameter("@BankBranchCode ", objclsDTOBankDetails.strBankCode));
                parameters.Add(new SqlParameter("@Address", objclsDTOBankDetails.strAddress));
                parameters.Add(new SqlParameter("@Telephone", objclsDTOBankDetails.strTelephone));
                parameters.Add(new SqlParameter("@Fax", objclsDTOBankDetails.strFax));
                parameters.Add(new SqlParameter("@CountryID", objclsDTOBankDetails.intCountryID));
                parameters.Add(new SqlParameter("@Email", objclsDTOBankDetails.strEmail));
                parameters.Add(new SqlParameter("@Website", objclsDTOBankDetails.strWebsite));
                //parameters.Add(new SqlParameter("@AccountID", objclsDTOBankDetails.intAccountID));
                parameters.Add(new SqlParameter("@BankID", objclsDTOBankDetails.intBankBranchID));
                parameters.Add(new SqlParameter("@UAEBankCode", objclsDTOBankDetails.lngUAEBankCode));
                parameters.Add(new SqlParameter("@IsAgent", objclsDTOBankDetails.blnIsAgent));
                parameters.Add(new SqlParameter("@BankSortCode", objclsDTOBankDetails.strBankSortCode));

                SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
                objParam.Direction = ParameterDirection.ReturnValue;
                parameters.Add(objParam);

                if (objclsConnection.ExecuteNonQuery("spBankDetails", parameters, out objoutBankNameID) != 0)
                {
                    if ((int)objoutBankNameID > 0)
                    {
                        objclsDTOBankDetails.intBankNameID = (int)objoutBankNameID;
                        return true;
                    }
                   
                    }

                return false;
                                    
                }
       
        // Delete

         public bool DeleteBankDetails()
         {
            
                 ArrayList parameters = new ArrayList();
                 parameters.Add(new SqlParameter("@Mode", 4));
                 parameters.Add(new SqlParameter("@BankBranchID", objclsDTOBankDetails.intBankNameID));
                 if (objclsConnection.ExecuteNonQuery("spBankDetails", parameters) != 0)
                 {
                     return true;
                 }
                 
                     return false;                           
                          
         }

         // Display 

         public bool DisplayBankDetails(int rowno)
         {
            
                 ArrayList parameters = new ArrayList();
                 parameters.Add(new SqlParameter("@Mode", 1));
                 parameters.Add(new SqlParameter("@RowNum", rowno));
                 sdrdr = objclsConnection.ExecuteReader("spBankDetails", parameters, CommandBehavior.SingleRow);

                 if (sdrdr.Read())
                 {
                     objclsDTOBankDetails.intBankNameID = Convert.ToInt32(sdrdr["BankBranchID"]);
                     objclsDTOBankDetails.strDescription = Convert.ToString(sdrdr["BankBranchName"]);
                     objclsDTOBankDetails.strBankCode = Convert.ToString(sdrdr["BankBranchCode"]);
                     objclsDTOBankDetails.strAddress = Convert.ToString(sdrdr["Address"]);
                     objclsDTOBankDetails.strTelephone = Convert.ToString(sdrdr["Telephone"]);
                     objclsDTOBankDetails.strFax = Convert.ToString(sdrdr["Fax"]);
                     objclsDTOBankDetails.intCountryID = Convert.ToInt32(sdrdr["CountryID"]);
                     objclsDTOBankDetails.strEmail = Convert.ToString(sdrdr["Email"]);
                     objclsDTOBankDetails.strWebsite = Convert.ToString(sdrdr["Website"]);
                     objclsDTOBankDetails.intBankBranchID = Convert.ToInt32(sdrdr["BankID"]);
                     //objclsDTOBankDetails.intAccountID = Convert.ToInt32(sdrdr["AccountID"]);
                     objclsDTOBankDetails.lngUAEBankCode = Convert.ToInt64(sdrdr["UAEBankCode"]);
                     
                 

                     sdrdr.Close();
                     return true;
                 }
                 return false;
             
         }

         public bool DisplayBankIDDetails(int rowno,int bankID)
         {

             ArrayList parameters = new ArrayList();
             parameters.Add(new SqlParameter("@Mode", 9));
             parameters.Add(new SqlParameter("@RowNum", rowno));
             parameters.Add(new SqlParameter("@BankBranchID", bankID));
             sdrdr = objclsConnection.ExecuteReader("spBankDetails", parameters, CommandBehavior.SingleRow);

             if (sdrdr.Read())
             {
                 objclsDTOBankDetails.intBankNameID = Convert.ToInt32(sdrdr["BankBranchID"]);
                 objclsDTOBankDetails.strDescription = Convert.ToString(sdrdr["Description"]);
                 objclsDTOBankDetails.strBankCode = Convert.ToString(sdrdr["BankCode"]);
                 objclsDTOBankDetails.strAddress = Convert.ToString(sdrdr["Address"]);
                 objclsDTOBankDetails.strTelephone = Convert.ToString(sdrdr["Telephone"]);
                 objclsDTOBankDetails.strFax = Convert.ToString(sdrdr["Fax"]);
                 objclsDTOBankDetails.intCountryID = Convert.ToInt32(sdrdr["CountryID"]);
                 objclsDTOBankDetails.strEmail = Convert.ToString(sdrdr["Email"]);
                 objclsDTOBankDetails.strWebsite = Convert.ToString(sdrdr["Website"]);
                 objclsDTOBankDetails.intBankBranchID = Convert.ToInt32(sdrdr["BankID"]);
                 //objclsDTOBankDetails.intAccountID = Convert.ToInt32(sdrdr["AccountID"]);
                 objclsDTOBankDetails.lngUAEBankCode = Convert.ToInt64(sdrdr["UAEBankCode"]);

                 sdrdr.Close();
                 return true;
             }
             return false;

         }
         public int RecCountNavigate()
         {
            
                 int intRecordCnt = 0;                                
                 parameters = new ArrayList();
                 parameters.Add(new SqlParameter("@Mode", 5));
                 sdrdr = objclsConnection.ExecuteReader("spBankDetails", parameters, CommandBehavior.SingleRow);
                 if (sdrdr.Read()) 
                 {
                     intRecordCnt=Convert.ToInt32(sdrdr["Cnt"]);              
                 
             }
                 sdrdr.Close();
             return  intRecordCnt;             
         }

         public int RecCountBankNavigate(int MintBankID)
         {
            
                 int intRecordCnt = 0;                                
                 parameters = new ArrayList();
                 parameters.Add(new SqlParameter("@Mode", 8));
                 parameters.Add(new SqlParameter("@BankID", MintBankID));
                 sdrdr = objclsConnection.ExecuteReader("spBankDetails", parameters, CommandBehavior.SingleRow);
                 if (sdrdr.Read()) 
                 {
                     intRecordCnt=Convert.ToInt32(sdrdr["Cnt"]);              
                 
             }
                 sdrdr.Close();
             return  intRecordCnt;             
         }
         //To Fill Combobox
         public  DataTable FillCombos(string[] sarFieldValues)
         {
             if (sarFieldValues.Length == 3)
             {
                 using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                 {
                     objCommonUtility.PobjDataLayer = objclsConnection;
                     return objCommonUtility.FillCombos( sarFieldValues);
                 }
                
             }
             return null;
         }


         public DataTable AccountDetails()
         {
             ArrayList parameters = new ArrayList();
             parameters.Add(new SqlParameter("@ParentID", 33));
             return objclsConnection.ExecuteDataTable("PayGetAccounts", parameters);
         }

                      
          public DataTable  CheckExistReferences(int intId)
         {
            fmParameters = new ArrayList();
             string strCondition = "name <>'BankBranchReference'";
             fmParameters.Add(new SqlParameter("@PrimeryID", intId));
             fmParameters.Add(new SqlParameter("@Condition", strCondition));
             fmParameters.Add(new SqlParameter("@FieldName", "BankBranchID"));
             return objclsConnection.ExecuteDataSet("spCheckIDExists", fmParameters).Tables[0];
             
         }

         public string GetFormsInUse(string strCaption, int intPrimeryId, string strCondition, string strFileds)
         {
             return new ClsWebform().GetUsedFormsInformation(strCaption, intPrimeryId, strCondition, strFileds);
         }

         public bool CheckValidEmail(string sEmailAddress)
         {
             using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
             {
                 return objCommonUtility.CheckValidEmail(sEmailAddress);
             }
         }

         public DataSet GetBankReport()
         {
             parameters = new ArrayList();
             parameters.Add(new SqlParameter("@Mode", 7));
             parameters.Add(new SqlParameter("@BankBranchID", objclsDTOBankDetails.intBankNameID));
             return objclsConnection.ExecuteDataSet("spBankDetails", parameters);
         }




         #region IDisposable Members

         void IDisposable.Dispose()
         {
             if (parameters != null)
                 parameters = null;

             if (fmParameters != null)
                 fmParameters = null;

             if (sdrdr != null)
                 sdrdr.Dispose();
         }

         #endregion

    }
}
