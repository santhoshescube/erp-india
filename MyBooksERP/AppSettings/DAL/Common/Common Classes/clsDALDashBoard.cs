﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALDashBoard
    {
        ArrayList parameters;
        public DataLayer MobjDataLayer;
        string strProc = "spDashBoard";

        public clsDALDashBoard(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
        }

        public DataTable GetChartDetails(DateTime dtMonthYear, int intMenuID, int intItemID)
        {
            parameters = new ArrayList();
            switch (intMenuID)
            {
                case (1):
                    parameters.Add(new SqlParameter("@Mode", 1));
                    break;
                case (2):
                    parameters.Add(new SqlParameter("@Mode", 2));
                    break;
                case (3):
                    parameters.Add(new SqlParameter("@Mode", 3));
                    break;
                case (4):
                    parameters.Add(new SqlParameter("@Mode", 4));
                    break;
                case (5):
                    parameters.Add(new SqlParameter("@Mode", 5));
                    break;
                case (6):
                    parameters.Add(new SqlParameter("@Mode", 6));
                    break;
                case (7):
                    parameters.Add(new SqlParameter("@Mode", 18));
                    break;
                case (8):
                    parameters.Add(new SqlParameter("@Mode", 20));
                    break;
            }
                
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
            parameters.Add(new SqlParameter("@MonthYear", dtMonthYear));
            parameters.Add(new SqlParameter("@ItemID", intItemID));
            return MobjDataLayer.ExecuteDataTable(strProc, parameters);
        }

        public DataTable GetDetails(DateTime dtMonthYear, int intMenuID, int intItemID)
        {
            parameters = new ArrayList();
            switch (intMenuID)
            {
                case (1):
                    parameters.Add(new SqlParameter("@Mode", 7));
                    break;
                case (2):
                    parameters.Add(new SqlParameter("@Mode", 8));
                    break;
                case (3):
                    parameters.Add(new SqlParameter("@Mode", 9));
                    break;
                case (4):
                    parameters.Add(new SqlParameter("@Mode", 10));
                    break;
                case (5):
                    parameters.Add(new SqlParameter("@Mode", 11));
                    break;
                case (6):
                    parameters.Add(new SqlParameter("@Mode", 12));
                    break;
                case (7):
                    parameters.Add(new SqlParameter("@Mode", 19));
                    break;
                case (8):
                    parameters.Add(new SqlParameter("@Mode", 21));
                    break;
            }
            
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
            parameters.Add(new SqlParameter("@MonthYear", dtMonthYear));
            parameters.Add(new SqlParameter("@ItemID", intItemID));
            return MobjDataLayer.ExecuteDataTable(strProc, parameters);
        }

        public DataTable GetChartSummary(DateTime dtMonthYear, int intMenuID, int intItemID)
        {
            parameters = new ArrayList();
            switch (intMenuID)
            {
                case (1):
                    parameters.Add(new SqlParameter("@Mode", 13));
                    break;
                case (2):
                    parameters.Add(new SqlParameter("@Mode", 14));
                    break;
                case (3):
                case (4):
                case (7):
                case (8):
                    parameters.Add(new SqlParameter("@Mode", 15));
                    break;
                case (5):
                case (6):
                    parameters.Add(new SqlParameter("@Mode", 16));
                    break;
            }

            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
            parameters.Add(new SqlParameter("@MonthYear", dtMonthYear));
            parameters.Add(new SqlParameter("@ItemID", intItemID));
            return MobjDataLayer.ExecuteDataTable(strProc, parameters);
        }

        public DataTable getItems(DateTime dtMonthYear)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 17));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
            parameters.Add(new SqlParameter("@MonthYear", dtMonthYear));
            return MobjDataLayer.ExecuteDataTable(strProc, parameters);
        }
    }
}
