﻿using System;
using System.Collections.Generic;

using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALMailSettings
    {
        public DataLayer objClsConnection { get; set; }
        public clsDTOMailSetting objclsDTOMailSetting;

        public void GetMailSetting(int iAccountType)
        {
            ArrayList prmSetting = null;
            
            try
            {
                prmSetting = new ArrayList();
                prmSetting.Add(new SqlParameter("@Mode", 1));
                prmSetting.Add(new SqlParameter("@AccountType", iAccountType));

                SqlDataReader sdrReader = this.objClsConnection.ExecuteReader("spMailSettings", prmSetting);
                if (sdrReader != null)
                {
                    if (sdrReader.Read())
                    {
                        this.objclsDTOMailSetting = new clsDTOMailSetting
                        {
                            AccountType = sdrReader["AccountType"].ToString(),
                            UserName = sdrReader["UserName"].ToString(),
                            PassWord = sdrReader["PassWord"].ToString(),
                            OutgoingServer = sdrReader["OutgoingServer"].ToString(),
                            PortNumber = Convert.ToInt32(sdrReader["PortNumber"]),
                            EnableSsl = Convert.ToBoolean(sdrReader["EnableSsl"]),
                            IncomingServer = sdrReader["IncomingServer"].ToString(),
                            MailId = Convert.ToInt32(sdrReader["MailSettingsID"])
                        };
                    }
                    sdrReader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmSetting != null) { prmSetting.Clear(); prmSetting = null; }
            }
            //return null;
        }
    }
}
