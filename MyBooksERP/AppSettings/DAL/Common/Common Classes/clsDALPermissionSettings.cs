﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;

namespace MyBooksERP
{
    public class clsDALPermissionSettings
    {
        ArrayList prmPermissions;                                          // array list for storing parameters
        public DataLayer MobjDataLayer;                                  // obj of datalayer    
        private string strProcedurePermissionSettings = "spPermissionSettings";

        public clsDALPermissionSettings(DataLayer objDataLayer)
        {
            //constructor
            MobjDataLayer = objDataLayer;
        }

        public void GetPermissions(int intRoleID,int intCompanyID,int intModuleID,int intMenuID,out bool blnPrintEmailPermission,out bool blnCreatePermission,out bool blnUpdatePermission,out bool blnDeletePermission)
        {
            blnCreatePermission = blnPrintEmailPermission = blnUpdatePermission = blnDeletePermission = false;
            prmPermissions = new ArrayList();
            prmPermissions.Add(new SqlParameter("@Mode",1));
            prmPermissions.Add(new SqlParameter("@RoleID", intRoleID));
            prmPermissions.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPermissions.Add(new SqlParameter("@ModuleID",intModuleID));
            prmPermissions.Add(new SqlParameter("@MenuID",intMenuID));
            DataTable dtPermissions = new DataTable();
            dtPermissions = MobjDataLayer.ExecuteDataTable(strProcedurePermissionSettings, prmPermissions);
            if (dtPermissions.Rows.Count > 0)
            {
                    blnCreatePermission = Convert.ToBoolean(dtPermissions.Rows[0]["IsCreate"]);
                    blnPrintEmailPermission = Convert.ToBoolean(dtPermissions.Rows[0]["IsPrintEmail"]);
                    blnUpdatePermission = Convert.ToBoolean(dtPermissions.Rows[0]["IsUpdate"]);
                    blnDeletePermission = Convert.ToBoolean(dtPermissions.Rows[0]["IsDelete"]);
            }
        }

        public DataTable GetControlsPermissions(int intRoleID, int intMenuID, int intCompanyID)
        {
            prmPermissions = new ArrayList();
            prmPermissions.Add(new SqlParameter("@Mode", 2));
            prmPermissions.Add(new SqlParameter("@RoleID", intRoleID));
            prmPermissions.Add(new SqlParameter("@MenuID", intMenuID));
            prmPermissions.Add(new SqlParameter("@CompanyID", intCompanyID));
            return MobjDataLayer.ExecuteDataTable(strProcedurePermissionSettings, prmPermissions); ;
        }
        public DataTable GetMenuPermissions(int intRoleID)
        {
            prmPermissions = new ArrayList();
            prmPermissions.Add(new SqlParameter("@Mode", 2));
            prmPermissions.Add(new SqlParameter("@RoleID", intRoleID));
            return MobjDataLayer.ExecuteDataTable(strProcedurePermissionSettings, prmPermissions); ;
        }
        public DataTable GetMenuPermissions(int intRoleID, int intCompanyID)
        {
            prmPermissions = new ArrayList();
            prmPermissions.Add(new SqlParameter("@Mode", 3));
            prmPermissions.Add(new SqlParameter("@RoleID", intRoleID));
            prmPermissions.Add(new SqlParameter("@CompanyID", intCompanyID));
            return MobjDataLayer.ExecuteDataTable(strProcedurePermissionSettings, prmPermissions); ;
        }
        public DataTable GetPermissionsForCompany(int intRoleID)
        {
            prmPermissions = new ArrayList();
            prmPermissions.Add(new SqlParameter("@Mode", 4));
            prmPermissions.Add(new SqlParameter("@RoleID", intRoleID));
            return MobjDataLayer.ExecuteDataTable(strProcedurePermissionSettings, prmPermissions); ;
        }
        public DataTable GetPermittedCompany(int intRoleID, int intCompanyID, int intControlID)// Only fro report by AMAL
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 13));
            prmReportDetails.Add(new SqlParameter("@RoleID", intRoleID));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmReportDetails.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable(strProcedurePermissionSettings, prmReportDetails);
        }
        public DataTable GetControlPermissions1(int intRoleID, int intCompanyID, int intControlID)  // For Getting Individual Control Permissions
        {
            prmPermissions = new ArrayList();
            prmPermissions.Add(new SqlParameter("@Mode", 16));
            prmPermissions.Add(new SqlParameter("@RoleID", intRoleID));
            prmPermissions.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPermissions.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable(strProcedurePermissionSettings, prmPermissions);
        }

        public DataTable GetPermittedCompany(int intUserID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 4));
            prmReportDetails.Add(new SqlParameter("@UserID", intUserID));
            return MobjDataLayer.ExecuteDataTable(strProcedurePermissionSettings, prmReportDetails);
        }
    }
}
