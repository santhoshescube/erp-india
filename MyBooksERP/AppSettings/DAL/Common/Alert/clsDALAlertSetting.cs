﻿using System;
using System.Collections.Generic;

using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALAlertSetting
    {
        public DataLayer objClsConnection { get; set; }
        public clsDTOAlertSettings objclsDTOAlertSettings;

        private string AlertTransaction = "spAlert";
        private string STAlertSettingTransaction = "spAlertSetting";
        private string STAlertRoleSettingTransaction = "spAlertRoleSetting";

        public DataTable FillCombos(string[] saFieldValues)
        {
            try
            {
                if (saFieldValues.Length == 3)
                {
                    using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                    {
                        objCommonUtility.PobjDataLayer = objClsConnection;
                        return objCommonUtility.FillCombos(saFieldValues);
                    }
                }
                return null;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool SaveAlertSettings()// insert
        {
            ArrayList parameters;
            object iOutSettingsID = 0;

            try
            {
                parameters = new ArrayList();
                if (this.objclsDTOAlertSettings.lngAlertSettingID > 0)
                {
                    parameters.Add(new SqlParameter("@Mode", 2));//update
                    parameters.Add(new SqlParameter("@AlertSettingID", this.objclsDTOAlertSettings.lngAlertSettingID));
                }
                else
                {
                    parameters.Add(new SqlParameter("@Mode", 1));//insert
                }

                parameters.Add(new SqlParameter("@AlertTypeID", this.objclsDTOAlertSettings.intAlertTypeID));
                parameters.Add(new SqlParameter("@OperationTypeID", this.objclsDTOAlertSettings.intOperationTypeID));
                parameters.Add(new SqlParameter("@Description", this.objclsDTOAlertSettings.strDescription));
                SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
                objParam.Direction = ParameterDirection.ReturnValue;
                parameters.Add(objParam);

                if (this.objClsConnection.ExecuteNonQuery(this.STAlertSettingTransaction, parameters, out iOutSettingsID) != 0)
                {
                    if (Convert.ToInt32(iOutSettingsID) > 0)
                    {
                        this.objclsDTOAlertSettings.lngAlertSettingID = Convert.ToInt64(iOutSettingsID);
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool DeleteAlertSetting(long lngAlertSettingID)
        {
            ArrayList parameters;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 4));//delete
                parameters.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));

                if (this.objClsConnection.ExecuteNonQuery(this.STAlertSettingTransaction, parameters) != 0)
                    return true;
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public int RecCountNavigate()
        {
            int intRecordCnt = 0;
            ArrayList parameters;
            SqlDataReader sdrDr = null;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 8));
                parameters.Add(new SqlParameter("@ModuleID", this.objclsDTOAlertSettings.intModuleID));

                sdrDr = this.objClsConnection.ExecuteReader(this.STAlertSettingTransaction, parameters);

                if (sdrDr.Read())
                {
                    intRecordCnt = Convert.ToInt32(sdrDr["cnt"]);
                }
                sdrDr.Close();
                return intRecordCnt;
            }
            catch (Exception ex) { throw ex; }
        }

        // Display

        public bool DisplayAlertSetting(int rowno)
        {
            ArrayList parameters;
            SqlDataReader sdrDr = null;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 9)); // Select
                parameters.Add(new SqlParameter("@RowNum", rowno));
                parameters.Add(new SqlParameter("@ModuleID",this.objclsDTOAlertSettings.intModuleID));

                sdrDr = this.objClsConnection.ExecuteReader(this.STAlertSettingTransaction, parameters, CommandBehavior.SingleRow);
                if (sdrDr.Read())
                {
                    this.objclsDTOAlertSettings.lngAlertSettingID = Convert.ToInt32(sdrDr["AlertSettingID"]);
                    this.objclsDTOAlertSettings.intAlertTypeID = Convert.ToInt32(sdrDr["AlertTypeID"]);
                    this.objclsDTOAlertSettings.intOperationTypeID = Convert.ToInt32(sdrDr["OperationTypeID"]);
                    this.objclsDTOAlertSettings.strDescription = sdrDr["AlertSettings"].ToString();

                    sdrDr.Close();
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool GetAlertSetting(long lngAlertSettingID)
        {
            ArrayList parameters;
            SqlDataReader sdrDr = null;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 5)); // Select
                parameters.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));

                sdrDr = this.objClsConnection.ExecuteReader(this.STAlertSettingTransaction, parameters, CommandBehavior.SingleRow);
                if (sdrDr.Read())
                {
                    this.objclsDTOAlertSettings = new clsDTOAlertSettings();
                    this.objclsDTOAlertSettings.lngAlertSettingID = Convert.ToInt32(sdrDr["AlertSettingID"]);
                    this.objclsDTOAlertSettings.intAlertTypeID = Convert.ToInt32(sdrDr["AlertTypeID"]);
                    this.objclsDTOAlertSettings.intOperationTypeID = Convert.ToInt32(sdrDr["OperationTypeID"]);
                    this.objclsDTOAlertSettings.strDescription = sdrDr["AlertSettings"].ToString();

                    sdrDr.Close();
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable GetAlertRoleSetting(long lngAlertSettingID)
        {
            ArrayList prmSetting = null;
            DataTable datSetting = null;

            try
            {
                prmSetting = new ArrayList();
                prmSetting.Add(new SqlParameter("@Mode", 6));
                prmSetting.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));
                datSetting = objClsConnection.ExecuteDataTable(this.STAlertRoleSettingTransaction, prmSetting);

                return datSetting;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmSetting != null) { prmSetting.Clear(); prmSetting = null; }
            }
        }

        public int GetAlertRoleSettingCount(long lngAlertSettingID)
        {
            ArrayList prmSetting = null;

            try
            {
                prmSetting = new ArrayList();
                prmSetting.Add(new SqlParameter("@Mode", 10));
                prmSetting.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));
                return Convert.ToInt32(objClsConnection.ExecuteScalar(this.STAlertRoleSettingTransaction, prmSetting));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmSetting != null) { prmSetting.Clear(); prmSetting = null; }
            }
        }

        public DataSet DisplayAlertSettingsEmail() //  AlertSetting Email
        {
            ArrayList prmSetting = null;
            prmSetting = new ArrayList();
            prmSetting.Add(new SqlParameter("@Mode", 11));
            prmSetting.Add(new SqlParameter("@AlertSettingID", objclsDTOAlertSettings.lngAlertSettingID));
            return objClsConnection.ExecuteDataSet(this.STAlertSettingTransaction, prmSetting);
        }
        public bool DeleteAlertRolesInGrid(List<long> lnglstAlertRoleSettingID)
        {
            /* Delete alert from two tables*/
            ArrayList prmSetting = null;
           
            try
            { 
                prmSetting = new ArrayList();
                foreach (long AlertRoleSettingID in lnglstAlertRoleSettingID)
                {
                   
                    prmSetting.Add(new SqlParameter("@Mode", 12));
                    prmSetting.Add(new SqlParameter("@AlertRoleSettingID", AlertRoleSettingID));
                    Convert.ToInt32(objClsConnection.ExecuteNonQuery(this.STAlertRoleSettingTransaction, prmSetting));
                    prmSetting.Clear();
                }
                return true;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmSetting != null) { prmSetting.Clear(); prmSetting = null; }
            }
        }
       
    }
}