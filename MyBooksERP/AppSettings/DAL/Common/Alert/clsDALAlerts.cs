﻿using System;
using System.Collections.Generic;

using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALAlerts
    {
        public DataLayer objClsConnection { get; set; }
        public clsDTOAlerts objclsDTOAlerts;
        private string AlertTransaction = "spAlert";
        private string AlertRoleSettingTransaction = "spAlertRoleSetting";
        private string AlertUserSettingTransaction = "spAlertUserSetting";

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objClsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool SaveAlert()// insert
        {
            ArrayList parameters;
            object iOutAletID = 0;

            try
            {
                parameters = new ArrayList();
                if (this.objclsDTOAlerts.lngAlertID > 0)
                {
                    parameters.Add(new SqlParameter("@Mode", 2));//update
                    parameters.Add(new SqlParameter("@AlertID", this.objclsDTOAlerts.lngAlertID));
                }
                else
                {
                    parameters.Add(new SqlParameter("@Mode", 1));//insert
                }

                parameters.Add(new SqlParameter("@AlertUserSettingID", this.objclsDTOAlerts.lngAlertUserSettingID));
                parameters.Add(new SqlParameter("@ReferenceID", this.objclsDTOAlerts.lngReferenceID));
                parameters.Add(new SqlParameter("@StartDate", this.objclsDTOAlerts.dtmStartDate));
                parameters.Add(new SqlParameter("@AlertMessage", this.objclsDTOAlerts.strAlertMessage));
                parameters.Add(new SqlParameter("@Status", this.objclsDTOAlerts.shrStatus));

                SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
                objParam.Direction = ParameterDirection.ReturnValue;
                parameters.Add(objParam);

                if (this.objClsConnection.ExecuteNonQuery(this.AlertTransaction, parameters, out iOutAletID) != 0)
                {
                    if (Convert.ToInt32(iOutAletID) > 0)
                    {
                        this.objclsDTOAlerts.lngAlertID = Convert.ToInt64(iOutAletID);
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable GetUserDayAlerts(int iUserID, DateTime dtmDate)
        {
            ArrayList prmSetting = null;
            DataTable datSetting = null;

            try
            {
                prmSetting = new ArrayList();
                prmSetting.Add(new SqlParameter("@Mode", 7));
                prmSetting.Add(new SqlParameter("@CurrentDate", dtmDate));
                prmSetting.Add(new SqlParameter("@UserID", iUserID));
                datSetting = objClsConnection.ExecuteDataTable(this.AlertTransaction, prmSetting);

                return datSetting;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmSetting != null) { prmSetting.Clear(); prmSetting = null; }
            }
        }

        public int GetPurchaseOrderStatus(long lngPurchaseOrderID)
        {
            ArrayList prmSetting = null;

            try
            {
                prmSetting = new ArrayList();
                prmSetting.Add(new SqlParameter("@Mode", 8));
                prmSetting.Add(new SqlParameter("@ReferenceID", lngPurchaseOrderID));

                return Convert.ToInt32(objClsConnection.ExecuteScalar(this.AlertTransaction, prmSetting));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmSetting != null) { prmSetting.Clear(); prmSetting = null; }
            }
        }
                
        public bool CloseAlert(Queue<long> queAlertID)// insert
        {
            ArrayList parameters;
            object iOutSettingsID = 0;

            try
            {
                parameters = new ArrayList();
                foreach (long lngAlertID in queAlertID)
                {
                    parameters.Add(new SqlParameter("@Mode", 9));
                    parameters.Add(new SqlParameter("@AlertID", lngAlertID));
                    parameters.Add(new SqlParameter("@Status", Convert.ToInt32(AlertStatus.Close)));

                    if (this.objClsConnection.ExecuteNonQuery(this.AlertTransaction, parameters) == 0)
                    {
                        return false;
                    }
                    parameters.Clear();
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable GetItems()
        {
            ArrayList prmSetting = null;
            DataTable datSetting = null;

            try
            {
                prmSetting = new ArrayList();
                prmSetting.Add(new SqlParameter("@Mode", 10));
                prmSetting.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
                datSetting = objClsConnection.ExecuteDataTable(this.AlertTransaction, prmSetting);

                return datSetting;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmSetting != null) { prmSetting.Clear(); prmSetting = null; }
            }
        }

        
        public DataTable GetItemsReoederLevel()
        {
            ArrayList prmSetting = null;
            DataTable datSetting = null;

            try
            {
                prmSetting = new ArrayList();
                prmSetting.Add(new SqlParameter("@Mode", 11));
                datSetting = objClsConnection.ExecuteDataTable(this.AlertTransaction, prmSetting);

                return datSetting;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmSetting != null) { prmSetting.Clear(); prmSetting = null; }
            }
        }

        public bool SaveAlert(DataTable datAlert)
        {
            ArrayList parameters;
            object iOutAletID = 0;

            try
            {
                foreach (DataRow dtRow in datAlert.Rows)
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 1));
                    parameters.Add(new SqlParameter("@AlertUserSettingID", dtRow["AlertUserSettingID"]));
                    parameters.Add(new SqlParameter("@ReferenceID", dtRow["ReferenceID"]));
                    parameters.Add(new SqlParameter("@StartDate", Convert.ToDateTime(dtRow["StartDate"]).ToString("dd MMM yyyy")));
                    parameters.Add(new SqlParameter("@AlertMessage", dtRow["AlertMessage"]));
                    parameters.Add(new SqlParameter("@Status", dtRow["Status"]));

                    SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
                    objParam.Direction = ParameterDirection.ReturnValue;
                    parameters.Add(objParam);

                    if (this.objClsConnection.ExecuteNonQuery(this.AlertTransaction, parameters, out iOutAletID) != 0)
                    {
                        if (Convert.ToInt32(iOutAletID) > 0)
                        {
                            dtRow["AlertID"] = Convert.ToInt64(iOutAletID);
                        }
                    }
                    else
                        return false;
                    if (parameters != null) { parameters.Clear(); parameters = null; }
                }
                datAlert.AcceptChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool DeleteAlert(int iOperationID,long lngAlertSettingID,int iUserID)
        {
            ArrayList parameters;
            object iOutSettingsID = 0;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 12));
                parameters.Add(new SqlParameter("@OperationTypeID",iOperationID));
                parameters.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));
                parameters.Add(new SqlParameter("@UserID", iUserID));
                if (this.objClsConnection.ExecuteNonQuery(this.AlertTransaction, parameters) != 0)
                {
                    return true;
                }

                return false;

            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable GetAlertRoleSetting(long lngAlertSettingID)
        {
            ArrayList parameters = null;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 6));
                parameters.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));

                return this.objClsConnection.ExecuteDataTable(AlertRoleSettingTransaction, parameters);
            }
            catch (Exception ex) { throw ex; }
            finally { if (parameters != null) { parameters.Clear(); parameters = null; } }
        }

        public DataTable GetAlertUserSetting(long lngAlertSettingID)
        {
            ArrayList parameters = null;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 9));
                parameters.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));

                return this.objClsConnection.ExecuteDataTable(AlertUserSettingTransaction, parameters);
            }
            catch (Exception ex) { throw ex; }
            finally { if (parameters != null) { parameters.Clear(); parameters = null; } }
        }

        public bool GetAlertUserSettingRepeat(long lngAlertUserSettingID)
        {
            ArrayList prmSetting = null;

            try
            {
                prmSetting = new ArrayList();
                prmSetting.Add(new SqlParameter("@Mode", 10));
                prmSetting.Add(new SqlParameter("@AlertUserSettingID", lngAlertUserSettingID));

                return Convert.ToBoolean(objClsConnection.ExecuteScalar(AlertUserSettingTransaction, prmSetting));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmSetting != null) { prmSetting.Clear(); prmSetting = null; }
            }
        }

        public DataTable GetUserAlertEntries(string TName, string Condition)
        {
            ArrayList prmCommon = new ArrayList();
            try
            {                
                prmCommon.Add(new SqlParameter("@Mode", "0"));
                prmCommon.Add(new SqlParameter("@Fields", "*"));
                prmCommon.Add(new SqlParameter("@TableName", TName));
                prmCommon.Add(new SqlParameter("@Condition", Condition));

                return this.objClsConnection.ExecuteDataTable("spCommonReferenceTransaction", prmCommon);
            }
            catch (Exception ex) { throw ex; }
            finally { if (prmCommon != null) { prmCommon.Clear(); prmCommon = null; } }
        }

        public bool DeleteAlertForUser(int AlertID)
        {
            ArrayList prmCommon = new ArrayList();
            try
            {
                prmCommon = new ArrayList();
                prmCommon.Add(new SqlParameter("@Mode", 4));
                prmCommon.Add(new SqlParameter("@AlertID", AlertID));
                return Convert.ToBoolean(objClsConnection.ExecuteScalar(AlertTransaction, prmCommon));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmCommon != null) { prmCommon.Clear(); prmCommon = null; }
            }
        }

        public DataTable GetCustomerCollectionAlert()
        {
            ArrayList prmCommon = new ArrayList();
            try
            {
                prmCommon.Add(new SqlParameter("@Mode", 16));
                return this.objClsConnection.ExecuteDataTable(AlertTransaction, prmCommon);
            }
            catch (Exception ex) { throw ex; }
            finally { if (prmCommon != null) { prmCommon.Clear(); prmCommon = null; } }
        }

        public bool SetReadStatus(int intAlertID,int intStatusID)
        {
            object objOutintRowCount = 0;
            ArrayList parameters;

            parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 19));
            parameters.Add(new SqlParameter("@AlertID", intAlertID));
            parameters.Add(new SqlParameter("@Status", intStatusID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            if (objClsConnection.ExecuteNonQuery(this.AlertTransaction, parameters, out objOutintRowCount) != 0)
            {
                if ((int)objOutintRowCount > 0)
                    return true;
            }
            return false;
        }
        /// <summary>
        /// Delete Alerts for a particular ReferenceID and AlertType
        /// </summary>
        /// <param name="ReferenceID"></param>
        /// <param name="AlertTypeID"></param>
        /// <returns>bool</returns>
        public bool DeleteAlert(int ReferenceID, int AlertTypeID)
        {
            int intResult = objClsConnection.ExecuteNonQuery("spCommonUtility", new List<SqlParameter>{
                new SqlParameter("@Mode","2"),
                new SqlParameter("@ReferenceID",ReferenceID),
                new SqlParameter("@OperationTypeID",AlertTypeID)
            });
            return intResult > 0;
        }
    }
}