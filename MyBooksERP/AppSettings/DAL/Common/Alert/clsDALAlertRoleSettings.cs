﻿using System;
using System.Collections.Generic;

using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALAlertRoleSettings
    {
        public DataLayer objClsConnection { get; set; }
        public clsDTOAlertRoleSettings objclsDTOAlertRoleSettings;

        private string STAlertRoleSettingTransaction = "spAlertRoleSetting";

        public DataTable FillCombos(string[] saFieldValues)
        {
            try
            {
                if (saFieldValues.Length == 3)
                {
                    using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                    {
                        objCommonUtility.PobjDataLayer = objClsConnection;
                        return objCommonUtility.FillCombos(saFieldValues);
                    }
                }
                return null;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool SaveAlertRoleSettings()// insert
        {
            ArrayList parameters;
            object iOutRoleSettingsID = 0;

            try
            {
                parameters = new ArrayList();
                if (this.objclsDTOAlertRoleSettings.lngAlertRoleSettingID > 0)
                {
                    parameters.Add(new SqlParameter("@Mode", 2));//update
                    parameters.Add(new SqlParameter("@AlertRoleSettingID", this.objclsDTOAlertRoleSettings.lngAlertRoleSettingID));
                }
                else
                {
                    parameters.Add(new SqlParameter("@Mode", 1));//insert
                }

                parameters.Add(new SqlParameter("@AlertSettingID", this.objclsDTOAlertRoleSettings.lngAlertSettingID));
                parameters.Add(new SqlParameter("@RoleID", this.objclsDTOAlertRoleSettings.intRoleID));
                parameters.Add(new SqlParameter("@ProcessingInterval", this.objclsDTOAlertRoleSettings.intProcessingInterval));
                parameters.Add(new SqlParameter("@RepeatInterval", this.objclsDTOAlertRoleSettings.intRepeatInterval));
                parameters.Add(new SqlParameter("@IsRepeat", this.objclsDTOAlertRoleSettings.blnIsRepeat));
                parameters.Add(new SqlParameter("@ValidPeriod", this.objclsDTOAlertRoleSettings.intValidPeriod));                
                parameters.Add(new SqlParameter("@IsAlertON", this.objclsDTOAlertRoleSettings.blnIsAlertOn));
                parameters.Add(new SqlParameter("@IsEmailAlertON", this.objclsDTOAlertRoleSettings.blnIsEmailAlertOn));
                parameters.Add(new SqlParameter("@AlertMessage", this.objclsDTOAlertRoleSettings.strAlertMessage));
                SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
                objParam.Direction = ParameterDirection.ReturnValue;
                parameters.Add(objParam);

                if (this.objClsConnection.ExecuteNonQuery(this.STAlertRoleSettingTransaction, parameters, out iOutRoleSettingsID) != 0)
                {
                    if (Convert.ToInt32(iOutRoleSettingsID) > 0)
                    {
                        this.objclsDTOAlertRoleSettings.lngAlertRoleSettingID = Convert.ToInt64(iOutRoleSettingsID);
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool GetAlertRoleSetting(long lngAlertSettingID, int iRoleID)
        {

            ArrayList prmSetting = null;
            SqlDataReader sdrReader;
            bool blnRes = false;
            try
            {
                prmSetting = new ArrayList();
                prmSetting.Add(new SqlParameter("@Mode", 7));
                prmSetting.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));
                prmSetting.Add(new SqlParameter("@RoleID", iRoleID));

                sdrReader = objClsConnection.ExecuteReader(this.STAlertRoleSettingTransaction, prmSetting, CommandBehavior.SingleRow);
                if (sdrReader != null)
                {
                    if (sdrReader.Read())
                    {
                        blnRes = true;
                        this.objclsDTOAlertRoleSettings.blnIsAlertOn = Convert.ToBoolean(sdrReader["IsAlertON"]);
                        this.objclsDTOAlertRoleSettings.blnIsEmailAlertOn = Convert.ToBoolean(sdrReader["IsEmailAlertON"]);
                        this.objclsDTOAlertRoleSettings.blnIsRepeat = Convert.ToBoolean(sdrReader["IsRepeat"]);
                        this.objclsDTOAlertRoleSettings.lngAlertRoleSettingID = Convert.ToInt32(sdrReader["AlertRoleSettingID"]);
                        this.objclsDTOAlertRoleSettings.lngAlertSettingID = Convert.ToInt32(sdrReader["AlertSettingID"]);
                        this.objclsDTOAlertRoleSettings.intProcessingInterval = Convert.ToInt32(sdrReader["ProcessingInterval"]);
                        this.objclsDTOAlertRoleSettings.intRepeatInterval = Convert.ToInt32(sdrReader["RepeatInterval"]);
                        this.objclsDTOAlertRoleSettings.intValidPeriod = Convert.ToInt32(sdrReader["ValidPeriod"]);
                        this.objclsDTOAlertRoleSettings.intRoleID = Convert.ToInt32(sdrReader["RoleID"]);
                        this.objclsDTOAlertRoleSettings.strRoleName = sdrReader["RoleName"].ToString();
                        this.objclsDTOAlertRoleSettings.strAlertMessage = sdrReader["AlertMessage"].ToString();
                    }
                    sdrReader.Close();
                }
                return blnRes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmSetting != null) { prmSetting.Clear(); prmSetting = null; }
                sdrReader = null;
            }
        }

        public bool GetAlertRoleSetting(long lngAlertRoleSettingID)
        {

            ArrayList prmSetting = null;
            SqlDataReader sdrReader;
            bool blnRes = false;
            try
            {
                prmSetting = new ArrayList();
                prmSetting.Add(new SqlParameter("@Mode", 5));
                prmSetting.Add(new SqlParameter("@AlertRoleSettingID", lngAlertRoleSettingID));

                sdrReader = objClsConnection.ExecuteReader(this.STAlertRoleSettingTransaction, prmSetting, CommandBehavior.SingleRow);
                if (sdrReader != null)
                {
                    if (sdrReader.Read())
                    {
                        blnRes = true;
                        this.objclsDTOAlertRoleSettings.blnIsAlertOn = Convert.ToBoolean(sdrReader["IsAlertON"]);
                        this.objclsDTOAlertRoleSettings.blnIsEmailAlertOn = Convert.ToBoolean(sdrReader["IsEmailAlertON"]);
                        this.objclsDTOAlertRoleSettings.blnIsRepeat = Convert.ToBoolean(sdrReader["IsRepeat"]);
                        this.objclsDTOAlertRoleSettings.lngAlertRoleSettingID = Convert.ToInt32(sdrReader["AlertRoleSettingID"]);
                        this.objclsDTOAlertRoleSettings.lngAlertSettingID = Convert.ToInt32(sdrReader["AlertSettingID"]);
                        this.objclsDTOAlertRoleSettings.intProcessingInterval = Convert.ToInt32(sdrReader["ProcessingInterval"]);
                        this.objclsDTOAlertRoleSettings.intRepeatInterval = Convert.ToInt32(sdrReader["RepeatInterval"]);
                        this.objclsDTOAlertRoleSettings.intValidPeriod = Convert.ToInt32(sdrReader["ValidPeriod"]);
                        this.objclsDTOAlertRoleSettings.intRoleID = Convert.ToInt32(sdrReader["RoleID"]);
                        this.objclsDTOAlertRoleSettings.strRoleName = "";// sdrReader["RoleName"].ToString();
                        this.objclsDTOAlertRoleSettings.strAlertMessage = sdrReader["AlertMessage"].ToString();
                    }
                    sdrReader.Close();
                }
                return blnRes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmSetting != null) { prmSetting.Clear(); prmSetting = null; }
                sdrReader = null;
            }
        }

        public bool CheckDuplicate(long lngAlertSettingID,int iRoleID,out int iCount)
        {

            ArrayList prmSetting = null;
            SqlDataReader sdrReader;
            bool blnRes = false;
            iCount = 0;
            try
            {
                 prmSetting = new ArrayList();
                 prmSetting.Add(new SqlParameter("@Mode",11));
                 prmSetting.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));
                 prmSetting.Add(new SqlParameter("@RoleID", iRoleID));
                sdrReader = objClsConnection.ExecuteReader(this.STAlertRoleSettingTransaction, prmSetting, CommandBehavior.SingleRow);
                if (sdrReader != null)
                {
                    if (sdrReader.Read())
                    {
                        iCount = Convert.ToInt32(sdrReader["AlertCount"]);
                        if (iCount > 0)
                        {
                            blnRes = true;
                        }
                    }
                    sdrReader.Close();
                }
                return blnRes;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmSetting != null) { prmSetting.Clear(); prmSetting = null; }
                sdrReader = null;
            }


        }
    }
}