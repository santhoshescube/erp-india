﻿using System;
using System.Collections.Generic;

using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALAlertUserSettings
    {
        public DataLayer objClsConnection { get; set; }
        public Queue<clsDTOAlertUserSettings> queclsDTOAlertUserSettings;

        private string STAlertUserSettingTransaction = "spAlertUserSetting";

        public DataTable GetAlertSettingUsers(long lngAlertSetingID, int intRoleID)
        {
            ArrayList prmAlert = null;
            DataTable datAlertSettingUsers = null;

            try
            {
                prmAlert = new ArrayList();
                prmAlert.Add(new SqlParameter("@Mode", 7));
                prmAlert.Add(new SqlParameter("@AlertSettingID", lngAlertSetingID));
                prmAlert.Add(new SqlParameter("@RoleID", intRoleID));
                datAlertSettingUsers = objClsConnection.ExecuteDataTable(STAlertUserSettingTransaction, prmAlert);

                return datAlertSettingUsers;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmAlert != null) { prmAlert.Clear(); prmAlert = null; }
            }
        }

        public DataTable GetUsers(long lngAlertSetingID, int intRoleID)
        {
            ArrayList prmAlert = null;
            DataTable datUsers = null;

            try
            {
                prmAlert = new ArrayList();
                prmAlert.Add(new SqlParameter("@Mode", 8));
                prmAlert.Add(new SqlParameter("@AlertSettingID", lngAlertSetingID));
                prmAlert.Add(new SqlParameter("@RoleID", intRoleID));
                datUsers = objClsConnection.ExecuteDataTable(STAlertUserSettingTransaction, prmAlert);

                return datUsers;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmAlert != null) { prmAlert.Clear(); prmAlert = null; }
            }
        }

        public bool SaveAlertUserSettings()// insert
        {
            ArrayList parameters;
            object iOutAlertUserSettingID = 0;

            try
            {
                parameters = new ArrayList();
                foreach (clsDTOAlertUserSettings objclsDTOAlertUserSettings in this.queclsDTOAlertUserSettings)
                {
                    //if (objclsDTOAlertUserSettings.lngAlertUserSettingID > 0)
                    //{
                    //    parameters.Add(new SqlParameter("@Mode", 2));//update
                    //    parameters.Add(new SqlParameter("@AlertUserSettingID", objclsDTOAlertUserSettings.lngAlertUserSettingID));
                    //}
                    //else
                    //{
                        parameters.Add(new SqlParameter("@Mode", 1));//insert
                    ////}

                    parameters.Add(new SqlParameter("@AlertSettingID", objclsDTOAlertUserSettings.lngAlertSettingID));
                    parameters.Add(new SqlParameter("@UserID", objclsDTOAlertUserSettings.intUserID));
                    parameters.Add(new SqlParameter("@ProcessingInterval", objclsDTOAlertUserSettings.intProcessingInterval));
                    parameters.Add(new SqlParameter("@RepeatInterval", objclsDTOAlertUserSettings.intRepaetInterval));                    
                    parameters.Add(new SqlParameter("@IsRepeat", objclsDTOAlertUserSettings.blnIsRepeat));
                    parameters.Add(new SqlParameter("@ValidPeriod", objclsDTOAlertUserSettings.intValidPeriod));
                    parameters.Add(new SqlParameter("@IsAlertON", objclsDTOAlertUserSettings.blnIsAlertOn));
                    parameters.Add(new SqlParameter("@IsEmailAlertON", objclsDTOAlertUserSettings.blnIsEmailAlertOn));
                    SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
                    objParam.Direction = ParameterDirection.ReturnValue;
                    parameters.Add(objParam);

                    if (this.objClsConnection.ExecuteNonQuery(this.STAlertUserSettingTransaction, parameters, out iOutAlertUserSettingID) != 0)
                    {
                        if (Convert.ToInt32(iOutAlertUserSettingID) > 0)
                        {
                            objclsDTOAlertUserSettings.lngAlertUserSettingID = Convert.ToInt64(iOutAlertUserSettingID);
                        }
                    }
                    else
                        return false;
                    parameters.Clear();
                }
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool DeleteAlertRoleSetting(long lngAlertSetingID, int intRoleID)
        {
            /* For Deleteing alert (delete and save) */
             
           
            ArrayList prmAlert = null;
            try
            {

                prmAlert = new ArrayList();
                prmAlert.Add(new SqlParameter("@Mode", 11));
                prmAlert.Add(new SqlParameter("@AlertSettingID", lngAlertSetingID));
                prmAlert.Add(new SqlParameter("@RoleID", intRoleID));
                if (this.objClsConnection.ExecuteNonQuery(this.STAlertUserSettingTransaction, prmAlert) != 0)
                {
                    
                }
                else
                    return false;
                prmAlert.Clear();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
