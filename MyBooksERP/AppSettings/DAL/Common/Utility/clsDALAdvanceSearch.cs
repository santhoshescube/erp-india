﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
/* 
=================================================
   Author:		< Sanju >
   Create date: < 15 Dec 2011 >
   Description:	< Advance Search DAL>
================================================
*/
namespace MyBooksERP
{
    public class clsDALAdvanceSearch
    {
        ArrayList prmAdvanceSearch;
        DataTable dat;
        public clsDTOAdvanceSearch objclsDTOAdvanceSearch { get; set; }
        public DataLayer objclsConnection { get; set; }
        ClsCommonUtility MobjClsCommonUtility; // object of clsCommonUtility

        public clsDALAdvanceSearch(DataLayer objDataLayer)
        {
            MobjClsCommonUtility = new ClsCommonUtility();
            objclsConnection = objDataLayer;
            MobjClsCommonUtility.PobjDataLayer = objclsConnection;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataTable GetSearchColumn()
        {//Get all Columns Name
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", objclsDTOAdvanceSearch.intMode));
            return objclsConnection.ExecuteDataTable("spInvAdvanceSearch", prmCommon);
        }

        public DataTable GetSearchData()
        {//Get searched data
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", objclsDTOAdvanceSearch.intMode));
            prmCommon.Add(new SqlParameter("@SearchCondition", objclsDTOAdvanceSearch.strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvAdvanceSearch", prmCommon);
        }

        public DataTable GetUserByPermission(int intRoleID, int intCompanyID, int intControlID, int intEmpControlID, int intUserID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 1));
            prmCommon.Add(new SqlParameter("@RoleID", intRoleID));
            prmCommon.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmCommon.Add(new SqlParameter("@ControlID", intControlID));
            prmCommon.Add(new SqlParameter("@EmpControlID", intEmpControlID));
            prmCommon.Add(new SqlParameter("@UserID", intUserID));
            return objclsConnection.ExecuteDataTable("spInvAdvanceSearch", prmCommon);
        }
    }
}