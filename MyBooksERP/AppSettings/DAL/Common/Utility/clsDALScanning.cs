﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/* 
=================================================
Author:		<Author,Arun>
Create date: <Create Date,18 Apr 2012>
Description:	<Description,DAL for Document Attach>
================================================
*/

namespace MyBooksERP
{
    public class clsDALScanning
    {
        ArrayList parameters;
        SqlDataReader sdrDr;

        public clsDTOScanning objclsDTOScanning { get; set; }
        public DataLayer objclsconnection { get; set; }
        private string strProcedureName = "spDocDocumentAttach";
        // count for  Print checking
        public int Print()
        {
            int intPrintCount = 0;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            intPrintCount = objclsconnection.ExecuteScalar(strProcedureName, parameters).ToInt32();

            return intPrintCount;
        }

        // count for  Addnew checking

        public int AddNew()
        {
            int intAddNewCount = 0;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 2));
            intAddNewCount = objclsconnection.ExecuteScalar(strProcedureName, parameters).ToInt32();

            return intAddNewCount;
        }

        // To Save and Update
        public bool Save(bool AddStatus)
        {
            object objoutNodeId = 0;

            parameters = new ArrayList();
            if (AddStatus)
            {
                parameters.Add(new SqlParameter("@Mode", 3));
                parameters.Add(new SqlParameter("@ParentNode", objclsDTOScanning.intParentNode));
                parameters.Add(new SqlParameter("@DocumentTypeID", objclsDTOScanning.intDocumentTypeID));
                parameters.Add(new SqlParameter("@ReferenceID", objclsDTOScanning.intReferenceID));
                parameters.Add(new SqlParameter("@OperationTypeID", objclsDTOScanning.intOperationTypeID));
            }
            else
            {
                parameters.Add(new SqlParameter("@Mode", 14));
                parameters.Add(new SqlParameter("@DocumentAttachID", objclsDTOScanning.intDocumentAttachID));
            }

            parameters.Add(new SqlParameter("@NodeName", objclsDTOScanning.strNodeName));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            if (objclsconnection.ExecuteNonQuery(strProcedureName, parameters, out objoutNodeId) != 0)
            {
                if ((int)objoutNodeId > 0)
                {
                    objclsDTOScanning.intDocumentAttachID = (int)objoutNodeId;
                    return true;
                }

            }
            return false;
        }

        public bool InsertDetails(int intNode, string strFilename, string strMeta)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@DocumentAttachID", intNode));
            parameters.Add(new SqlParameter("@FileName", strFilename));
            parameters.Add(new SqlParameter("@MetaData", strMeta));
            objclsconnection.ExecuteNonQuery(strProcedureName, parameters);
            return true;
        }

        public void InsertMaster()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@DocumentTypeID", objclsDTOScanning.intDocumentTypeID));
            parameters.Add(new SqlParameter("@ReferenceID", objclsDTOScanning.intReferenceID));
            parameters.Add(new SqlParameter("@OperationTypeID", objclsDTOScanning.intOperationTypeID));
            parameters.Add(new SqlParameter("@NodeName", objclsDTOScanning.strNodeName));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objoutNodeId;

            if (objclsconnection.ExecuteNonQuery(strProcedureName, parameters, out objoutNodeId) != 0)
            {
                objclsDTOScanning.intDocumentAttachID = Convert.ToInt32(objoutNodeId);
            }
        }

        public bool SelectForUpdate(string strNodeName)
        {
            bool blnRetValue = false;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 15));
            parameters.Add(new SqlParameter("@NodeName", strNodeName));
            sdrDr = objclsconnection.ExecuteReader(strProcedureName, parameters);
            if (sdrDr.Read())
            {
                objclsDTOScanning.intDocumentAttachID = sdrDr["DocumentAttachID"].ToInt32();
                objclsDTOScanning.strNodeName = sdrDr["NodeName"].ToStringCustom();
                blnRetValue = true;
            }
            sdrDr.Close();
            return blnRetValue;
        }


        public bool SelectNode(int iReferenceID, string strNodeName, int intOpertaionTypeID)
        {
            bool blnRetValue = false;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@ReferenceID", iReferenceID));
            parameters.Add(new SqlParameter("@NodeName", strNodeName));
            parameters.Add(new SqlParameter("@OperationTypeID", intOpertaionTypeID));
            sdrDr = objclsconnection.ExecuteReader(strProcedureName, parameters);
            if (sdrDr.Read())
            {
                objclsDTOScanning.intDocumentAttachID = sdrDr["DocumentAttachID"].ToInt32();
                blnRetValue = true;
            }
            sdrDr.Close();
            return blnRetValue;

        }
        public bool SelectDocumentType(int iDocumentTypeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@DocumentTypeID", iDocumentTypeID));
            objclsDTOScanning.strNodeName = objclsconnection.ExecuteScalar(strProcedureName, parameters).ToStringCustom();
            return (objclsDTOScanning.strNodeName != string.Empty);

        }


        public bool SelectDetails(int intDocAttachID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@DocumentAttachID", intDocAttachID));
            objclsDTOScanning.strFileName = objclsconnection.ExecuteScalar(strProcedureName, parameters).ToStringCustom();
            return (objclsDTOScanning.strFileName != string.Empty);

        }


        public bool SelectfromTreeMaster(int intDocTypeID)
        {
            objclsDTOScanning.strNodeName = "";
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 13));
            parameters.Add(new SqlParameter("@DocumentTypeID", intDocTypeID));
            objclsDTOScanning.strNodeName = objclsconnection.ExecuteScalar(strProcedureName, parameters).ToStringCustom();
            return (objclsDTOScanning.strNodeName != string.Empty);

        }

        public bool DeleteNodes(int iMode, int iDocAttachID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", iMode));
            parameters.Add(new SqlParameter("@DocumentAttachID ", iDocAttachID));
            if (objclsconnection.ExecuteNonQueryWithTran(strProcedureName, parameters) != 0)
            {
                return true;
            }
            return false;
        }



        public DataTable Display(int iOperationTypeID, int iParentNode)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@OperationTypeID", iOperationTypeID));
            parameters.Add(new SqlParameter("@ParentNode", iParentNode));
            return objclsconnection.ExecuteDataTable(strProcedureName, parameters);
        }


        public DataTable Fill(int iCompanyID, string strNodeName, int iOperationTypID, int iParentNode)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@ReferenceID", iCompanyID));
            parameters.Add(new SqlParameter("@NodeName", strNodeName));
            parameters.Add(new SqlParameter("@OperationTypeID", iOperationTypID));
            return objclsconnection.ExecuteDataTable(strProcedureName, parameters);

        }

        public DataTable GetparentNodes(int intOperationTypeID, int intReferenceID, int intDocumentTypeID,string strNodeName)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 20));
            parameters.Add(new SqlParameter("@OperationTypeID", intOperationTypeID));
            parameters.Add(new SqlParameter("@ReferenceID", intReferenceID));
            parameters.Add(new SqlParameter("@DocumentTypeID", intDocumentTypeID));
            parameters.Add(new SqlParameter("@NodeName", strNodeName));
            return objclsconnection.ExecuteDataTable(strProcedureName, parameters);
        }
    }
}
