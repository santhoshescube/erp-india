﻿using System;

using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALBarCode
    {
        public clsDTOBarCode objDTOBarCode { get; set; }
        public DataLayer objConnection { get; set; }

        ArrayList prmBarCode;

        string STBarCodeTransaction = "spInvBarCode";

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool GetBarCode(int MintMode, long PlngItemID, string PstrBarcodeValue)
        {
            bool blnRead = false;

            prmBarCode = new ArrayList();
            prmBarCode.Add(new SqlParameter("@Mode", MintMode));
            prmBarCode.Add(new SqlParameter("@RowNumber", objDTOBarCode.intRowNumber));
            prmBarCode.Add(new SqlParameter("@IsTemplate", objDTOBarCode.blnIsTemplate));
            prmBarCode.Add(new SqlParameter("@ItemID", PlngItemID));
            prmBarCode.Add(new SqlParameter("@BarcodeValue", PstrBarcodeValue));

            using (SqlDataReader sdr = objConnection.ExecuteReader(STBarCodeTransaction, prmBarCode, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    objDTOBarCode.lngItemID = Convert.ToInt64(sdr["ItemID"]);
                    //objDTOBarCode.lngBatchID = Convert.ToInt64(sdr["BatchID"]);\
                    objDTOBarCode.intEncodingID=sdr["EncodingID"].ToInt32();
                    objDTOBarCode.blnIsShowlabel=sdr["IsShowlabel"].ToBoolean();
                    objDTOBarCode.intWidth=sdr["Width"].ToInt32();
                    objDTOBarCode.intHeight=sdr["Height"].ToInt32();
                    objDTOBarCode.intRotationID=sdr["RotationID"].ToInt32();
                    objDTOBarCode.intAlignment=sdr["Alignment"].ToInt32();
                    objDTOBarCode.intLabelLocation=sdr["LabelLocation"].ToInt32();
                    objDTOBarCode.strRGBHexBackColor=sdr["RGBHexBackColor"].ToString();
                    objDTOBarCode.strRGBHexForeColor = sdr["RGBHexForeColor"].ToString();
                    objDTOBarCode.strBarcodeValue = Convert.ToString(sdr["BarcodeValue"]);
                    objDTOBarCode.imgBarcodeImage = (byte[])(sdr["BarcodeImage"]);
                    objDTOBarCode.intBarcodeID = Convert.ToInt32(sdr["BarcodeID"]);
                    blnRead = true;
                }
                sdr.Close();
            }

            return blnRead;
        }

        public bool SaveBarCode()
        {
            prmBarCode = new ArrayList();
            prmBarCode.Add(new SqlParameter("@Mode", (objDTOBarCode.intBarcodeID == 0 ? 2 : 3)));
            prmBarCode.Add(new SqlParameter("@BarcodeID", objDTOBarCode.intBarcodeID));
            prmBarCode.Add(new SqlParameter("@ItemID", objDTOBarCode.lngItemID));
            prmBarCode.Add(new SqlParameter("@BatchID", objDTOBarCode.lngBatchID));
            prmBarCode.Add(new SqlParameter("@EncodingID", objDTOBarCode.intEncodingID));
            prmBarCode.Add(new SqlParameter("@IsShowlabel", objDTOBarCode.blnIsShowlabel));
            prmBarCode.Add(new SqlParameter("@Width", objDTOBarCode.intWidth));
            prmBarCode.Add(new SqlParameter("@Height", objDTOBarCode.intHeight));
            prmBarCode.Add(new SqlParameter("@RotationID", objDTOBarCode.intRotationID));
            prmBarCode.Add(new SqlParameter("@Alignment", objDTOBarCode.intAlignment));
            prmBarCode.Add(new SqlParameter("@LabelLocation", objDTOBarCode.intLabelLocation));
            prmBarCode.Add(new SqlParameter("@RGBHexBackColor", objDTOBarCode.strRGBHexBackColor));
            prmBarCode.Add(new SqlParameter("@RGBHexForeColor", objDTOBarCode.strRGBHexForeColor));
            prmBarCode.Add(new SqlParameter("@BarcodeValue", objDTOBarCode.strBarcodeValue));
            prmBarCode.Add(new SqlParameter("@BarcodeImage", objDTOBarCode.imgBarcodeImage));
  
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmBarCode.Add(objParam);

            object objOutBarcodeID;

            objConnection.ExecuteNonQuery(STBarCodeTransaction, prmBarCode, out objOutBarcodeID);
            objDTOBarCode.intBarcodeID = Convert.ToInt32(objOutBarcodeID);

            if (objDTOBarCode.intBarcodeID > 0)
                return true;
            else return false;
        }
        public void SaveBatchBarCode()
        {
            prmBarCode = new ArrayList();
            prmBarCode.Add(new SqlParameter("@Mode", 8));
            prmBarCode.Add(new SqlParameter("@ItemID", objDTOBarCode.lngItemID));
            prmBarCode.Add(new SqlParameter("@BatchID", objDTOBarCode.lngBatchID));
            prmBarCode.Add(new SqlParameter("@BarcodeValue", objDTOBarCode.strBarcodeValue));
            prmBarCode.Add(new SqlParameter("@BarcodeImage", objDTOBarCode.imgBarcodeImage));
            objConnection.ExecuteNonQuery(STBarCodeTransaction, prmBarCode);
        }
        public bool DeleteBarCode()
        {
            prmBarCode = new ArrayList();
            prmBarCode.Add(new SqlParameter("@Mode", 4));
            prmBarCode.Add(new SqlParameter("@BarcodeID", objDTOBarCode.intBarcodeID));
            prmBarCode.Add(new SqlParameter("@ItemID", objDTOBarCode.lngItemID));
            if (objConnection.ExecuteNonQuery(STBarCodeTransaction, prmBarCode) != 0)
                return true;
            else return false;
        }

        public int GetBarCodeCount()
        {
            int intRecordCount = 0;

            prmBarCode = new ArrayList();
            prmBarCode.Add(new SqlParameter("@Mode", 5));
            prmBarCode.Add(new SqlParameter("@IsTemplate", objDTOBarCode.blnIsTemplate));
            prmBarCode.Add(new SqlParameter("@ItemID", objDTOBarCode.lngItemID));

            using (SqlDataReader sdr = objConnection.ExecuteReader(STBarCodeTransaction, prmBarCode, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    intRecordCount = Convert.ToInt32(sdr[0]);
                }
                sdr.Close();
            }

            return intRecordCount;
        }

        public string GetBarCodeValue()
        {
            string strBarcodeValue = string.Empty;

            prmBarCode = new ArrayList();
            prmBarCode.Add(new SqlParameter("@Mode", 6));
            prmBarCode.Add(new SqlParameter("@BatchID", objDTOBarCode.lngBatchID));
            prmBarCode.Add(new SqlParameter("@BarcodeValue", objDTOBarCode.strBarcodeValue));

            using (SqlDataReader sdr = objConnection.ExecuteReader(STBarCodeTransaction, prmBarCode, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    strBarcodeValue = Convert.ToString(sdr["BarcodeValue"]);
                    objDTOBarCode.intBarcodeID = Convert.ToInt32(sdr["BarcodeID"]);
                }
                sdr.Close();
            }

            return strBarcodeValue;
        }
    }
}