﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.SqlClient;
using System.Data;


namespace MyBooksERP
{
    public class clsDALUomConversion : IDisposable
    {
        public DataLayer objConnection { get; set; }
        public clsDTOUOMConversion objDTOUOMConversion { get; set; }
        ArrayList parameters;
        SqlDataReader sdrDr;

        void IDisposable.Dispose()
        {
            if (parameters != null)
                parameters = null;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool CheckDuplication(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objConnection;
                    return objCommonUtility.CheckDuplication(saFieldValues);
                }
            }
            return false;
        }

        public bool SaveUOM(bool AddStatus)
        {
            object objOutUOMConversionID = 0;

            parameters = new ArrayList();
            if (AddStatus)
                parameters.Add(new SqlParameter("@Mode", 1));//insert
            else
            {
                parameters.Add(new SqlParameter("@Mode", 2));//update
                parameters.Add(new SqlParameter("@UOMConversionID", objDTOUOMConversion.intUomConversionID));
            }
            parameters.Add(new SqlParameter("@UOMTypeID", objDTOUOMConversion.intUOMClassID));
            parameters.Add(new SqlParameter("@UOMID", objDTOUOMConversion.intUOMBaseID));
            parameters.Add(new SqlParameter("@OtherUOMID", objDTOUOMConversion.intUOMID));
            parameters.Add(new SqlParameter("@ConversionFactorID", objDTOUOMConversion.intConversionFactorID));
            parameters.Add(new SqlParameter("@ConversionRate", objDTOUOMConversion.dblConversionValue));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            if (objConnection.ExecuteNonQuery("spInvUomConversion", parameters, out objOutUOMConversionID) != 0)
            {
                if ((int)objOutUOMConversionID > 0)
                {
                    objDTOUOMConversion.intUomConversionID = (int)objOutUOMConversionID;
                    return true;
                }
            }
            return false;
        }

        public bool DeleteUOM(int UOMConversionID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 3));
            parameters.Add(new SqlParameter("@UOMConversionID", UOMConversionID));
            if (objConnection.ExecuteNonQuery("spInvUomConversion", parameters) != 0)
                return true;
            else return false;
        }

        public DataTable GetUOMDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            return objConnection.ExecuteDataTable("spInvUomConversion", parameters);
        }

        public bool DisplayUOM(int UOMConversionID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@UOMConversionID", UOMConversionID));
            sdrDr = objConnection.ExecuteReader("spInvUomConversion", parameters, CommandBehavior.SingleRow);
            if (sdrDr.Read())
            {
                objDTOUOMConversion.intUomConversionID = Convert.ToInt32(sdrDr["UOMConversionID"]);
                objDTOUOMConversion.intUOMClassID = Convert.ToInt32(sdrDr["UOMTypeID"]);
                objDTOUOMConversion.intUOMBaseID = Convert.ToInt32(sdrDr["UOMID"]);
                objDTOUOMConversion.intUOMID = Convert.ToInt32(sdrDr["OtherUOMID"]);
                objDTOUOMConversion.intConversionFactorID = Convert.ToInt32(sdrDr["ConversionFactorID"]);
                objDTOUOMConversion.dblConversionValue = Convert.ToDouble(sdrDr["ConversionRate"]);
                sdrDr.Close();
                return true;
            }
            return false;
        }

        public DataSet GetUOMReport()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            return objConnection.ExecuteDataSet("spInvUomConversion", parameters);
        }

        public DataTable GetUOMDetails(int UomBaseClassID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@UOMTypeID", UomBaseClassID));
            return objConnection.ExecuteDataTable("spInvUomConversion", parameters);
        }

        public DataTable GetUOMDetails(int UomBaseClassID, int UomBaseID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@UOMTypeID", UomBaseClassID));
            parameters.Add(new SqlParameter("@UOMID", UomBaseID));
            return objConnection.ExecuteDataTable("spInvUomConversion", parameters);
        }

        public DataTable GetUOMExistingOrNot(int intUomID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@UOMID", intUomID));
            return objConnection.ExecuteDataTable("spInvUomConversion", parameters);
        }
        public DataSet DisplayUOMConversionReport(int intUomClassID, int intUomBaseID) // UOM Conversion
        {
            parameters = new ArrayList();
            if (intUomClassID == 0)
                parameters.Add(new SqlParameter("@Mode", 4));
            else if (intUomClassID > 0 && intUomBaseID == 0)
                parameters.Add(new SqlParameter("@Mode", 6));
            else if (intUomClassID > 0 && intUomBaseID > 0)
                parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@UOMTypeID", intUomClassID));
            parameters.Add(new SqlParameter("@UOMID", intUomBaseID));
            return objConnection.ExecuteDataSet("spInvUomConversion", parameters);
        }
    }
}
