﻿using System;
using System.Collections.Generic;

using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALWorkFlowSettingsSetting
    {
        public clsDTOWorkFlowSettingsSetting objclsDTOWorkFlowSettingsSetting { get; set; }
        public Queue<clsDTOWorkFlowSettingsSetting> queclsDTOWorkFlowSettingsSetting { get; set; }

        public DataLayer objClsConnection { get; set; }
        private string spWorkFlowSettings = "spWorkFlowSettings";

        /// <summary>
        /// Loading all companies
        /// </summary>
        /// <returns></returns>
        public DataTable LoadRoles()
        {
            string strSQL;
            try
            {
                strSQL = "Select WF.WorkFlowTypeID,WF.WorkFlowType,UM.RoleID,RR.RoleName,OH.UserID,UM.UserName From WorkFlowTypeReference WF " +
                " Left Join WorkFlowSettings OH ON WF.WorkFlowTypeID = OH.WorkFlowTypeID " +
                " Left Join UserMaster UM ON UM.UserID = OH.UserID  " +
                " Left Join RoleReference RR ON RR.RoleID = UM.RoleID  ";

                //strSQL = "Select WF.WorkFlowTypeID,WF.WorkFlowType FROM " +
                //        "  WorkFlowTypeReference WF  ";

                return this.objClsConnection.ExecuteDataSet(strSQL).Tables[0];
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Loading all companies
        /// </summary>
        /// <returns></returns>
        public DataTable LoadUsers()
        {
            string strSQL;
            try
            {
                strSQL = "SELECT UserID, UserName,R.RoleID,R.RoleName " +
                            " FROM UserMaster U Inner Join RoleReference R ON R.RoleID = U.RoleID Order By R.RoleID";
                return this.objClsConnection.ExecuteDataSet(strSQL).Tables[0];
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Get all rows form Hierarchy Settings
        /// </summary>
        /// <returns></returns>
        public DataTable GetHierarchySettings()
        {
            ArrayList prmRole = null;
            DataTable datHierarchySetting = null;

            try
            {
                prmRole = new ArrayList();
                prmRole.Add(new SqlParameter("@Mode", 7));
                datHierarchySetting = objClsConnection.ExecuteDataTable(spWorkFlowSettings, prmRole);

                return datHierarchySetting;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmRole != null) { prmRole.Clear(); prmRole = null; }
            }
        }

        /// <summary>
        /// Get all rows form Hierarchy Settings by RoleID
        /// </summary>
        /// <param name="iRoleID"></param>
        /// <returns></returns>
        public DataTable GetHierarchySettings(int iRoleID)
        {
            ArrayList prmRole = null;

            try
            {
                prmRole = new ArrayList();
                prmRole.Add(new SqlParameter("@Mode", 6));
                prmRole.Add(new SqlParameter("@WorkFlowTypeID", 7));
                return objClsConnection.ExecuteDataTable(spWorkFlowSettings, prmRole);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (prmRole != null) { prmRole.Clear(); prmRole = null; }
            }
        }

        public bool SaveWorkFlowSettingsSettings()// insert
        {
            ArrayList parameters = null;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 4));//Delete All

                this.objClsConnection.ExecuteNonQuery(this.spWorkFlowSettings, parameters);

                parameters.Clear();
                foreach (clsDTOWorkFlowSettingsSetting _clsDTOSetting in queclsDTOWorkFlowSettingsSetting)
                {
                    parameters.Add(new SqlParameter("@Mode", 1));//Insert New
                    parameters.Add(new SqlParameter("@WorkFlowTypeID", _clsDTOSetting.intWorkFlowTypeID));
                    parameters.Add(new SqlParameter("@UserID", _clsDTOSetting.intUserID));

                    this.objClsConnection.ExecuteNonQuery(this.spWorkFlowSettings, parameters);
                    parameters.Clear();
                }
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

    }
}