﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALChangepassword
    {
        public clsDTOChangepassword MobjclsDTOChangepassword { get; set; }

        public DataLayer MobjDataLayer { get; set; }
        ArrayList prmChangePassword;
        private string strProcedureName = "spUser";

        public bool GetUserDetails()
        {
            bool blnRetValue=false;
            SqlDataReader sdrUser;
            prmChangePassword = new ArrayList();
            prmChangePassword.Add(new SqlParameter("@Mode", 11));
            prmChangePassword.Add(new SqlParameter("@UserID", MobjclsDTOChangepassword.intUserID));
            sdrUser = MobjDataLayer.ExecuteReader(strProcedureName, prmChangePassword);

            if (sdrUser.Read())
            {
                MobjclsDTOChangepassword.strUserName = Convert.ToString(sdrUser["UserName"]);
                MobjclsDTOChangepassword.strCurrentPassword = Convert.ToString(sdrUser["Password"]);
                blnRetValue = true;
            }
            sdrUser.Close();
            return blnRetValue;

        }
        public bool ChangePassword()
        {//Updating With NEWpassword
            prmChangePassword = new ArrayList();
            prmChangePassword.Add(new SqlParameter("@Mode", 12));
            prmChangePassword.Add(new SqlParameter("@Password",MobjclsDTOChangepassword.strNewPassword ));
            prmChangePassword.Add(new SqlParameter("@UserID", MobjclsDTOChangepassword.intUserID));
            MobjDataLayer.ExecuteNonQuery(strProcedureName, prmChangePassword);
           return true;
        }
    }
}
