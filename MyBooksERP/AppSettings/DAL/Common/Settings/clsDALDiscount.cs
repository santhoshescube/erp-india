﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/* 
================================================
Author:		<Author,Sawmya>
Create date: <Create Date,4 Mar 2011>
Description:	<Description,DAL for Discount>
================================================
*/
namespace MyBooksERP
{
     public class clsDALDiscount:IDisposable
    {
         ArrayList parameters; 
         SqlDataReader sdrDr;

         public clsDTODiscount objclsDTODiscount { get; set; }
         public DataLayer objclsconnection { get; set; }

         
         // To fill combo

         public DataTable FillCombos(string[] sarFieldValues)
         {
             if (sarFieldValues.Length == 3)
             {
                 using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                 {
                     objCommonUtility.PobjDataLayer = objclsconnection;
                     return objCommonUtility.FillCombos(sarFieldValues);
                 }

             }
             return null;
         }

         //   SAVE AND UPDATE

         public bool SaveDiscount(bool AddStatus)
         {
             object objOutintDiscountTypeId = 0;

             parameters = new ArrayList();
             if (AddStatus)
                 parameters.Add(new SqlParameter("@Mode", 2));//insert
             else
                 parameters.Add(new SqlParameter("@Mode", 3));//update
             //(DiscountShortName,Description,DiscountMode,DiscountForAmount,PercentOrAmount,PercOrAmtVAlue,IsSaleType)
             parameters.Add(new SqlParameter("@DiscountTypeID", objclsDTODiscount.intDiscountTypeId));
             parameters.Add(new SqlParameter("@DiscountShortName",objclsDTODiscount.strDiscountShortName));
             parameters.Add(new SqlParameter("@Description",objclsDTODiscount.strDescription));
             parameters.Add(new SqlParameter("@IsSaleType",objclsDTODiscount.blnIsSaleType));
             parameters.Add(new SqlParameter("@DiscountForAmount",objclsDTODiscount.blnDiscountForAmount));
             parameters.Add(new SqlParameter("@DiscountMode",objclsDTODiscount.charDiscountMode));
             parameters.Add(new SqlParameter("@PercentOrAmount",objclsDTODiscount.blnPercentOrAmount));
             parameters.Add(new SqlParameter("@PercOrAmtValue", objclsDTODiscount.dblPercOrAmtValue));
             parameters.Add(new SqlParameter("@PeriodApplicable", objclsDTODiscount.blnPeriodApplicable));
             parameters.Add(new SqlParameter("@PeriodFrom", objclsDTODiscount.dteFromPeriod));
             parameters.Add(new SqlParameter("@PeriodTo", objclsDTODiscount.dteToPeriod));
             parameters.Add(new SqlParameter("@SlabApplicable", objclsDTODiscount.blnSlabApplicable));
             parameters.Add(new SqlParameter("@MinSlab", objclsDTODiscount.decMinSlab));
             parameters.Add(new SqlParameter("@Unit", objclsDTODiscount.strUnit));
             parameters.Add(new SqlParameter("@IsActive", objclsDTODiscount.blnActive));
             parameters.Add(new SqlParameter("@CurrencyID", objclsDTODiscount.intCurrencyID));
             SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
             objParam.Direction = ParameterDirection.ReturnValue;
             parameters.Add(objParam);
             if (objclsconnection.ExecuteNonQuery("spInvDiscounts", parameters, out objOutintDiscountTypeId) != 0)
             {
                 if ((int)objOutintDiscountTypeId > 0)
                 {
                     objclsDTODiscount.intDiscountTypeId = (int)objOutintDiscountTypeId;
                    
                                                            
                 }
                if (AddStatus == false)
                  {
                   DeleteDiscountDetailsforupdating(objclsDTODiscount.charDiscountMode);
                  }
                if (objclsDTODiscount.charDiscountMode == 'I' || objclsDTODiscount.charDiscountMode == 'C')
                {
                    SaveDiscountDetails(objclsDTODiscount.charDiscountMode);
                }
                                  return true; 
             }
             return false;
         }

         // save itemwise in grid
         public bool SaveDiscountDetails(char mode)
         {           

             foreach (clsDTODiscountDetails objclsDTODiscountDetails in objclsDTODiscount.lstDiscountDetails)
             {
                parameters = new ArrayList();
                if (mode == Convert.ToChar("I"))
                {
                    parameters.Add(new SqlParameter("@Mode", "5"));
                    parameters.Add(new SqlParameter("DiscountTypeID", objclsDTODiscount.intDiscountTypeId));
                    parameters.Add(new SqlParameter("@ItemOrderId", objclsDTODiscountDetails.intItemOrderId));
                    parameters.Add(new SqlParameter("@ItemID", objclsDTODiscountDetails.intItemID));
                    parameters.Add(new SqlParameter("@ModePercentOrAmt", objclsDTODiscountDetails.charModePercentOrAmt));
                    parameters.Add(new SqlParameter("@Value", objclsDTODiscountDetails.decValue));
                    parameters.Add(new SqlParameter("@SlabApplicableGrid", objclsDTODiscountDetails.blnSlabApplicableGrid));
                    parameters.Add(new SqlParameter("@MinSlabGrid", objclsDTODiscountDetails.decMinSlabGrid));
                    if (objclsDTODiscountDetails.intUOMID != 0)
                    parameters.Add(new SqlParameter("@UnitGrid", objclsDTODiscountDetails.intUOMID));
                }
                else
                {
                    parameters.Add(new SqlParameter("@Mode", "6"));
                    parameters.Add(new SqlParameter("DiscountTypeID", objclsDTODiscount.intDiscountTypeId));
                    parameters.Add(new SqlParameter("@CustomerOrderID", objclsDTODiscountDetails.intCustomerOrderID));
                    parameters.Add(new SqlParameter("@VendorID", objclsDTODiscountDetails.intVendorID));
                    parameters.Add(new SqlParameter("@ModePercentOrAmt", objclsDTODiscountDetails.charModePercentOrAmt));
                    parameters.Add(new SqlParameter("@Value", objclsDTODiscountDetails.decValue));
                    parameters.Add(new SqlParameter("@SlabApplicableGrid", objclsDTODiscountDetails.blnSlabApplicableGrid));
                    parameters.Add(new SqlParameter("@MinSlabGrid", objclsDTODiscountDetails.decMinSlabGrid));
                    if(objclsDTODiscountDetails.intUOMID !=0)
                    parameters.Add(new SqlParameter("@UnitGrid", objclsDTODiscountDetails.intUOMID));
                }
                if (objclsconnection.ExecuteNonQuery("spInvDiscounts", parameters) != 0)
                {
                   
                }
             }
             return true;
         }

         //delete itemwise grid for updating
         public bool DeleteDiscountDetailsforupdating(char mode)
         {
             parameters = new ArrayList();
             if (mode == Convert.ToChar("I"))
             {
                 parameters.Add(new SqlParameter("@Mode", 9));
             }
             else
             {

                 parameters.Add(new SqlParameter("@Mode", 10));
             }
             parameters.Add(new SqlParameter("@DiscountTypeID",objclsDTODiscount.intDiscountTypeId));
             if (objclsconnection.ExecuteNonQuery("spInvDiscounts", parameters) != 0)
             {
                 return true;
             }

             return false;

         }       
       
         //To display grid information for itemwise and customer

         public DataTable DisplayGridInformation(int intDiscountTypeID, char mode)
         {
             if (mode == Convert.ToChar("I"))
             {
                 parameters = new ArrayList();
                 parameters.Add(new SqlParameter("@Mode", 7));
                 parameters.Add(new SqlParameter("@DiscountTypeID", intDiscountTypeID));
                 return objclsconnection.ExecuteDataTable("spInvDiscounts", parameters);
             }
             else
             {
                 parameters = new ArrayList();
                 parameters.Add(new SqlParameter("@Mode", 8));
                 parameters.Add(new SqlParameter("@DiscountTypeID", intDiscountTypeID));
                 return objclsconnection.ExecuteDataTable("spInvDiscounts", parameters);
             }
         }


         public int RecCountNavigate()
         {

             int intRecordCnt = 0;
             parameters = new ArrayList();
             parameters.Add(new SqlParameter("@Mode", 11));
             sdrDr = objclsconnection.ExecuteReader("spInvDiscounts", parameters, CommandBehavior.SingleRow);
             if (sdrDr.Read())
             {
                 intRecordCnt = Convert.ToInt32(sdrDr["Cnt"]);

             }
             sdrDr.Close();
             return intRecordCnt;
         }

         public DataTable GetItemUOMs(int intItemID,int intUOMTypeID)
         {
             ArrayList prmItems = new ArrayList();
             prmItems.Add(new SqlParameter("@Mode", 10));
             prmItems.Add(new SqlParameter("@ItemID", intItemID));
             prmItems.Add(new SqlParameter("@UomTypeID", intUOMTypeID));
             DataTable dtItemUoms = new DataTable();
             dtItemUoms = objclsconnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmItems);

             prmItems = new ArrayList();
             prmItems.Add(new SqlParameter("@Mode", 12));
             prmItems.Add(new SqlParameter("@ItemID", intItemID));
             DataTable dtItemBaseUnit = objclsconnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmItems);
             DataRow dr = dtItemUoms.NewRow();
             dr["UOMID"] = dtItemBaseUnit.Rows[0]["UOMID"];
             dr["Description"] = dtItemBaseUnit.Rows[0]["Description"];
             dr["ItemID"] = intItemID;
             dtItemUoms.Rows.InsertAt(dr, 0);
             return dtItemUoms;
         }
         // Display

         public bool DisplayNormalDiscount(int RowNo)
         {
                         
                 parameters = new ArrayList();

                 parameters.Add(new SqlParameter("@Mode", 1));
                 parameters.Add(new SqlParameter("RowNum", RowNo));
                 sdrDr = objclsconnection.ExecuteReader("spInvDiscounts", parameters, CommandBehavior.SingleRow);

                 if (sdrDr.Read())
                 {
                     objclsDTODiscount.intDiscountTypeId = Convert.ToInt32(sdrDr["DiscountID"]);
                     objclsDTODiscount.strDiscountShortName = Convert.ToString(sdrDr["DiscountShortName"]);
                     objclsDTODiscount.strDescription = Convert.ToString(sdrDr["DiscountName"]);
                     objclsDTODiscount.blnIsSaleType = Convert.ToBoolean(sdrDr["IsSaleType"]);
                     objclsDTODiscount.blnDiscountForAmount = Convert.ToBoolean(sdrDr["DiscountForAmount"]);
                     objclsDTODiscount.charDiscountMode = Convert.ToChar(sdrDr["DiscountMode"]);
                     objclsDTODiscount.blnPercentOrAmount = Convert.ToBoolean(sdrDr["PercentOrAmount"]);
                     objclsDTODiscount.dblPercOrAmtValue = Convert.ToDouble(sdrDr["PercOrAmtVAlue"]);
                     objclsDTODiscount.blnPeriodApplicable = Convert.ToBoolean(sdrDr["IsPeriodApplicable"]);
                     objclsDTODiscount.dteFromPeriod = Convert.ToDateTime(sdrDr["PeriodFrom"]);
                     objclsDTODiscount.dteToPeriod = Convert.ToDateTime(sdrDr["PeriodTo"]);
                     objclsDTODiscount.blnSlabApplicable = Convert.ToBoolean(sdrDr["IsSlabApplicable"]);
                     objclsDTODiscount.decMinSlab = Convert.ToDecimal(sdrDr["MinSlab"]);
                     objclsDTODiscount.strUnit = Convert.ToString(sdrDr["Unit"]);
                     objclsDTODiscount.blnActive = Convert.ToBoolean(sdrDr["IsActive"]);
                     objclsDTODiscount.intCurrencyID = Convert.ToInt32(sdrDr["CurrencyID"]);

                     sdrDr.Close();
                     return true;
                 }
                        return false;
         }

         // Delete

         public bool DeleteAll()
         {

             ArrayList parameters = new ArrayList();
             if (objclsDTODiscount.charDiscountMode == Convert.ToChar("N"))
             {
                 parameters.Add(new SqlParameter("@Mode", 4));
             }
             if (objclsDTODiscount.charDiscountMode == Convert.ToChar("I"))
             {
                 parameters.Add(new SqlParameter("@Mode", 12));
             }
             if (objclsDTODiscount.charDiscountMode == Convert.ToChar("C"))
             {
                 parameters.Add(new SqlParameter("@Mode", 13));
             }
             parameters.Add(new SqlParameter("@DiscountTypeID",objclsDTODiscount.intDiscountTypeId));
             if (objclsconnection.ExecuteNonQuery("spInvDiscounts", parameters) != 0)
             {
                 return true;
             }

             return false;

         }



         public bool CheckDuplication(bool blnAddStatus, string[] saValues, int intId)
         {
             string sField = "DiscountShortName";
             string sTable = "InvDiscountReference";
             string sCondition = "";

             if (blnAddStatus) // Add mode
                 sCondition = "DiscountShortName='" + saValues[0] + "'";
             else // Edit mode
                 sCondition = "DiscountShortName='" + saValues[0] + "' and DiscountID <> " + intId + "";

             using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
             {
                 objCommonUtility.PobjDataLayer = objclsconnection;
                 return objCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
             }
         }

         public DataSet GetNormalReport()
         {
             parameters = new ArrayList();
             parameters.Add(new SqlParameter("@Mode", 14));
             parameters.Add(new SqlParameter("@DiscountTypeID",objclsDTODiscount.intDiscountTypeId));
             return objclsconnection.ExecuteDataSet("spInvDiscounts", parameters);
         }

         public DataSet GetItemReport()
         {
             parameters = new ArrayList();
             parameters.Add(new SqlParameter("@Mode", 17));
             parameters.Add(new SqlParameter("@DiscountTypeID", objclsDTODiscount.intDiscountTypeId));
             return objclsconnection.ExecuteDataSet("spInvDiscounts", parameters);
         }
         public DataSet GetCustomerReport()
         {
             parameters = new ArrayList();
             parameters.Add(new SqlParameter("@Mode", 18));
             parameters.Add(new SqlParameter("@DiscountTypeID", objclsDTODiscount.intDiscountTypeId));
             return objclsconnection.ExecuteDataSet("spInvDiscounts", parameters);
         }
         
         public DataTable GetDiscountIDExists(int MiDiscountTypeId)
         {
             parameters = new ArrayList();
             parameters.Add(new SqlParameter("@DiscountTypeID", MiDiscountTypeId));
             return objclsconnection.ExecuteDataSet("spInvGetDiscountIDExists", parameters).Tables[0];
         }


         public string GetFormsInUse(string sCaption, int iPrimeryId, string sCondition, string sFileds)
         {
             return new ClsWebform().GetUsedFormsInformation(sCaption, iPrimeryId, sCondition, sFileds);
         }


         #region IDisposable Members

         void IDisposable.Dispose()
         {
             if (parameters != null)
                 parameters = null;

             if (sdrDr != null)
                 sdrDr.Dispose();

         }

         #endregion


    }
}
