﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,1 Mar 2011>
Description:	<Description,,DAL for UOM>
================================================
*/
namespace MyBooksERP
{
    public class clsDALUnitsOfMeasurement:IDisposable
    {
        ArrayList parameters;
        SqlDataReader sdrDr;


        public clsDTOUnitsOfMeasurement objDTOUnitsOfMeasurement { get; set; }
        public  DataLayer objConnection { get; set; }

      

        // To fill combo

        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility ())
                {
                    objCommonUtility.PobjDataLayer=objConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }


        //   SAVE AND UPDATE

        public bool SaveUOM(bool AddStatus)
        {
            object  objOutUOMID = 0;

            parameters = new ArrayList();
            if (AddStatus)
                parameters.Add(new SqlParameter("@Mode", 2));//insert
            else
                parameters.Add(new SqlParameter("@Mode", 3));//update
            parameters.Add(new SqlParameter("@UOMID", objDTOUnitsOfMeasurement.intUOMID));
            parameters.Add(new SqlParameter("@UOMName", objDTOUnitsOfMeasurement.strDescription));
            parameters.Add(new SqlParameter("@Code", objDTOUnitsOfMeasurement.strShortName));
            parameters.Add(new SqlParameter("@UOMTypeID", objDTOUnitsOfMeasurement.intUOMClassID));
            //parameters.Add(new SqlParameter("@QuantityPerUOM", objDTOUnitsOfMeasurement.dblQuantityPerUOM));
            parameters.Add(new SqlParameter("@Scale", objDTOUnitsOfMeasurement.intScale));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            if (objConnection.ExecuteNonQuery( "spInvUnitOfMeasurement",parameters, out objOutUOMID)!=0)
            {
                if ((int)objOutUOMID > 0)
                {
                    objDTOUnitsOfMeasurement.intUOMID = (int)objOutUOMID;
                    return true;
                }
               
            }
            return false;
        }
              


        // Display

        public bool DisplayUOM(int UOMID)
        {

           parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@UOMID", UOMID));
            sdrDr = objConnection.ExecuteReader("spInvUnitOfMeasurement", parameters, CommandBehavior.SingleRow);

            if (sdrDr.Read())
            {
                objDTOUnitsOfMeasurement.intUOMClassID = Convert.ToInt32(sdrDr["UOMTypeID"]);
                objDTOUnitsOfMeasurement.strDescription = Convert.ToString(sdrDr["UOMName"]);
                objDTOUnitsOfMeasurement.strShortName = Convert.ToString(sdrDr["Code"]);
                //objDTOUnitsOfMeasurement.dblQuantityPerUOM = Convert.ToDouble(sdrDr["QuantityPerUOM"]);
                objDTOUnitsOfMeasurement.intScale = Convert.ToInt32(sdrDr["Scale"]);
                sdrDr.Close();
                return true;
            }           
              return false;            
        }

        // TO FILL DataTable

        public DataTable GetUOMDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            return objConnection.ExecuteDataTable("spInvUnitOfMeasurement", parameters);

        }     


        //DELETE

        public bool DeleteUOM(int UOMID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@UOMID",UOMID));
            if (objConnection.ExecuteNonQuery("spInvUnitOfMeasurement", parameters) != 0)
                return true;
            else return false;
        }    
               
              
        // check UOMID exists
        public DataTable UOMIDExists(int intUOMID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@UOMID", intUOMID));
            return objConnection.ExecuteDataTable("spInvUnitOfMeasurement", parameters);
        }

        // Get TableName

        public DataTable GetFormNameTest(string TableName)
        {       
            
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode",6));
            parameters.Add(new SqlParameter("@TableName", TableName));
            return objConnection.ExecuteDataTable("spInvUnitOfMeasurement", parameters);
                       
        }

        public DataSet GetUOMReport()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            return objConnection.ExecuteDataSet("spInvUnitOfMeasurement", parameters);
        }



        public bool CheckDuplication(bool blnAddStatus, string[] saValues, int intId)
        {
            string sField = "UOMName";
            string sTable = "InvUOMReference";
            string sCondition = "";

            if (blnAddStatus) // Add mode
                sCondition = "UOMName='" + saValues[0] + "'";
            else // Edit mode
                sCondition = "UOMName='" + saValues[0] + "' and UOMID <> " + intId + "";

            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = objConnection;
                return objCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
            }
        }




        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (parameters != null)
                parameters = null;

           
        }

        #endregion

        


    }
}
