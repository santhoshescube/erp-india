﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;

/*
 ========================================================
 
 Author:<Author,,Amal Raj> 
 Create Date:<Create Date,,06 June 2011> 
 Description:<Description,,Terms And Condition DAL Class>
 
 ========================================================
 */

namespace MyBooksERP
{
    public class clsDALTermsAndCond : IDisposable
    {
        ArrayList parameters;
        public clsDTOTermsAndCond objclsDTOTermsAndCond { get; set; }
        public DataLayer objclsConnection { get; set; }
        
        //DataLayer MobjDataLayer;

        public bool SaveTermsAndConditions(bool AddStatus)   //SAVE AND UPDATE
        {
            object objOutTermsID = 0;
            parameters = new ArrayList();
            if (AddStatus)
            {
                parameters.Add(new SqlParameter("@Mode", 1));    //INSERT
            }
            else
            {
                parameters.Add(new SqlParameter("@Mode", 2));    //UPDATE
            }
            parameters.Add(new SqlParameter("@CompanyID", objclsDTOTermsAndCond.intCompanyID));
            parameters.Add(new SqlParameter("@TermsID", objclsDTOTermsAndCond.intTermsID));
            parameters.Add(new SqlParameter("@OperationTypeID", objclsDTOTermsAndCond.intTypeID));
            parameters.Add(new SqlParameter("@DescriptionEnglish", objclsDTOTermsAndCond.strDescriptionEnglish));
            parameters.Add(new SqlParameter("@DescriptionArabic", objclsDTOTermsAndCond.strDescriptionArabic));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            if (objclsConnection.ExecuteNonQuery("spTermsAndConditions", parameters, out objOutTermsID) != 0)
            {
                if ((int)objOutTermsID > 0)
                {
                    objclsDTOTermsAndCond.intTermsID = (int)objOutTermsID;
                }    
                return true;
            }
               return false;
           }
              
        public DataTable FillCombos(string[] saFieldValues)   //To fill Combo
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }
        public DataTable GetTermsAndCondn(int intTypeID, int intCompanyID)
        {
            // Function for Displaying Informations By Company
            ArrayList prmTAC = new ArrayList();
            string strSearchCondition = "";
            if (intCompanyID != 0) strSearchCondition = " TAC.CompanyID=" + intCompanyID + " AND";
            if (intTypeID != 0) strSearchCondition = strSearchCondition + " TR.OperationTypeID=" + intTypeID + " AND";
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 4);

            prmTAC.Add(new SqlParameter("@Mode", 3));          //Select
            prmTAC.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spTermsAndConditions", prmTAC);
        }
 
        public DataSet GetTermsAndConditionsReport(int intOperationType, int intCompanyID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@OperationTypeID", intOperationType));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objclsConnection.ExecuteDataSet("spTermsAndConditions", parameters);
        }

        public bool DeleteTAC(int intTermsID)
        {
            ArrayList prmTAC = new ArrayList();
            prmTAC.Add(new SqlParameter("@Mode", 5));
            prmTAC.Add(new SqlParameter("@TermsID",intTermsID));

            if (objclsConnection.ExecuteNonQuery("spTermsAndConditions", prmTAC) != 0)
                return true;
            else return false;
        }
        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (parameters != null)
                parameters = null;
        }
        #endregion
       }
    }
    

