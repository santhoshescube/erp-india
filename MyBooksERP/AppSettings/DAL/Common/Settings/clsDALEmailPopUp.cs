﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,9 Mar 2011>
Description:	<Description,DAL for EmailPopUp>
================================================
*/

namespace MyBooksERP
{
   public  class clsDALEmailPopUp:IDisposable
    {
       ArrayList parameters;
       SqlDataReader sdrDr;

       public clsDTOEmailPopUp objclsDTOEmailPopUp { get; set; }
       public DataLayer objconnection { get; set; }


       // To fill combo

       public DataTable FillCombos(string[] sarFieldValues)
       {
           if (sarFieldValues.Length == 3)
           {
               using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
               {
                   objCommonUtility.PobjDataLayer = objconnection;
                   return objCommonUtility.FillCombos(sarFieldValues);
               }

           }
           return null;
       }

       public bool Send(out string FromMailID, out  string PWD, out string SmtpServerAddress, out int PortNumber)
         {
              FromMailID = "";
              PWD = "";
             SmtpServerAddress = "";
             PortNumber = 0;  

             parameters = new ArrayList();
             parameters.Add(new SqlParameter("@Mode", 1));
             sdrDr = objconnection.ExecuteReader("STMailSettings", parameters, CommandBehavior.SingleRow);
             if (sdrDr.Read())
             {
                 FromMailID = Convert.ToString(sdrDr[0]);
                 PWD = Convert.ToString(sdrDr[1]);
                 SmtpServerAddress = Convert.ToString(sdrDr[2]);
                 PortNumber = Convert.ToInt32(sdrDr[3]);
                 sdrDr.Close();
                 return true;

             }
             sdrDr.Close();
             return false;
             
         }

       #region IDisposable Members

       void IDisposable.Dispose()
       {
           if (parameters != null)
               parameters = null;

           if (sdrDr != null)
               sdrDr.Dispose();

       }

       #endregion


       


    }
}
