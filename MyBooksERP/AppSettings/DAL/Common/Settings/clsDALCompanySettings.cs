﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

/* ===============================================
  Author:		<Author,,Arun>
  Create date: <Create Date,,23 Feb 2011>
  Description:	<Description,,DTO for CompanySettings>
================================================*/
namespace MyBooksERP
{
    public class clsDALCompanySettings : IDisposable
    {
        public DataLayer objclsConnection { get; set; }
        string STCompanySettingsTransactions = "spCompanySettings";

        public DataTable DisplayCompanysettings(int CompanyID, int iMode, string sConfigurationItem)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", iMode));//mode=6
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            parameters.Add(new SqlParameter("@ConfigurationItem", sConfigurationItem));
            return objclsConnection.ExecuteDataTable(STCompanySettingsTransactions, parameters);
        }

        public bool Reset(int intType)  //RESET (A-Advanced,C-ChequeReceipt,P,P,P,R,S,S)
        {
            ArrayList prmcompany = new ArrayList();
            prmcompany.Add(new SqlParameter("@Mode", 11));
            prmcompany.Add(new SqlParameter("CompanyID", intType));

            if (objclsConnection.ExecuteNonQuery(STCompanySettingsTransactions, prmcompany) != 0)
            {
                return true;
            }
            return false;
        }

        //Function to insert all the details  according to company
        public bool Insert(int intID)
        {
            ArrayList prmcompany = new ArrayList();
            prmcompany.Add(new SqlParameter("@Mode", 10));
            prmcompany.Add(new SqlParameter("@CompanyID", intID));

            if (objclsConnection.ExecuteNonQuery(STCompanySettingsTransactions, prmcompany) != 0)
            {
                return true;
            }
            return false;
        }

        public bool SavePro(int SettingsId, int CompanyID, string ConfigurationValue)
        {
            ArrayList prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@Mode", 3));  // SAVE USING PROPERTYDESCRIPTOR
            prmCompany.Add(new SqlParameter("@SettingsID", SettingsId));
            prmCompany.Add(new SqlParameter("@CompanyID", CompanyID));
            prmCompany.Add(new SqlParameter("@ConfigurationValue", ConfigurationValue));

            if (objclsConnection.ExecuteNonQuery(STCompanySettingsTransactions, prmCompany) != 0)
            {
                return true;
            }
            return false;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataSet GetCompanySettingsReport(int CompanyID)
        {
            ArrayList prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@Mode", 12));
            prmCompany.Add(new SqlParameter("@CompanyID", CompanyID));
            return objclsConnection.ExecuteDataSet("spCompanySettings", prmCompany);
        }

        void IDisposable.Dispose()
        { }
    }
}