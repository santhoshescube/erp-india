﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
/* 
=================================================
   Author:		<Author,,Arun>
   Create date: <Create Date,,24 Feb 2011>
   Description:	<Description,,DAL for AccountSettings>
================================================
*/
namespace MyBooksERP
{
   public  class clsDALAccountSettings
    {
       public clsDTOAccountSettings objclsDTOAccountSettings { get; set; }

        public DataLayer objclsConnection { get; set; }
        ArrayList parameters;
        string spAccSettings = "spAccSettings";

        public bool SaveAccountSettings()// insert
        {
             object iOutSettingsID = 0;

            parameters= new ArrayList();
            if (objclsDTOAccountSettings.intSettingsID > 0)
            {
                parameters.Add(new SqlParameter("@Mode", 2));//update
                parameters.Add(new SqlParameter("@AccountSettingsID", objclsDTOAccountSettings.intSettingsID));
            }
            else
            {
                parameters.Add(new SqlParameter("@Mode", 1));//insert
            }
           
            parameters.Add(new SqlParameter("@CompanyID", objclsDTOAccountSettings.intCompanyID));
            parameters.Add(new SqlParameter("@AccountTransactionTypeID", objclsDTOAccountSettings.intOperationModId));
            parameters.Add(new SqlParameter("@AccountID", objclsDTOAccountSettings.intAccountID));
            parameters.Add(new SqlParameter("@AccountGroupID", objclsDTOAccountSettings.intGLAccountID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            if (objclsConnection.ExecuteNonQuery("spAccSettings", parameters, out iOutSettingsID) != 0)
            {
                if (Convert.ToInt32(iOutSettingsID) > 0)
                {
                    objclsDTOAccountSettings.intSettingsID = (int)iOutSettingsID;
                }
                return true;


            }
            else
            {
                return false;
            }
        }


        public DataTable DisplayGridInformation()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 3));//select
            parameters.Add(new SqlParameter("@CompanyID", objclsDTOAccountSettings.intCompanyID));
           return objclsConnection.ExecuteDataTable(spAccSettings,parameters);

             
        }
      
        public bool DeleteInfo()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));//Delete
            parameters.Add(new SqlParameter("@SettingsID", objclsDTOAccountSettings.intSettingsID));
            if (objclsConnection.ExecuteNonQuery(spAccSettings, parameters) != 0)
            {
                return true;
            }
            return false;
        }
        public bool CheckDuplication(bool bAddStatus, string[] saValues, int iId)
        {
            //string sField = "CompanyID,AccountTransactionTypeID,AccountGroupID,AccountID ";
            //string sTable = "AccAccountSettings";
            //string sCondition = "";
            //if (bAddStatus)
            //    sCondition = "CompanyID=" + saValues[0] + " and AccountTransactionTypeID=" + saValues[1] + " and AccountID=" + saValues[3];
            //else
            //    sCondition = "CompanyID=" + saValues[0] + " and AccountTransactionTypeID=" + saValues[1] + " and AccountID=" + saValues[3] + " and AccountSettingsID <>" + iId + "";



            //using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            //{
            //    objCommonUtility.PobjDataLayer = objclsConnection;
            //    return objCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
            //}
            if (bAddStatus)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 5));
                parameters.Add(new SqlParameter("@CompanyID", saValues[0]));
                parameters.Add(new SqlParameter("@AccountTransactionTypeID ", saValues[1]));
                parameters.Add(new SqlParameter("@AccountID ", saValues[3]));
                return Convert.ToBoolean(this.objclsConnection.ExecuteScalar("spAccSettings", parameters));
            }
            return false;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        //Email
        public DataSet GetAccountSettingsReport()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@CompanyID", objclsDTOAccountSettings.intCompanyID));
            return objclsConnection.ExecuteDataSet(spAccSettings, parameters);
        }

       //For Checking If Account Settings Exists
        public DataTable CheckAccountIDExists(int MiAccountID)
        {
            string strCondition1 = "'AccountSettings'";
            //string strCondition2 = "'AccountID','CreditHeadID','DebitHeadID'";
            string strCondition2 = "'AccountID'";

            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@Value", MiAccountID));

            parameters.Add(new SqlParameter("@ColumnsIn", strCondition2));
            parameters.Add(new SqlParameter("@TablesNotIn", strCondition1));

            return objclsConnection.ExecuteDataSet("STCheckValueExists", parameters).Tables[0];
            
          }
    }
}
