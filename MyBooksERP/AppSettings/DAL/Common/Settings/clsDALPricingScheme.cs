﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.SqlClient;
using System.Data;


namespace MyBooksERP
{
    public class clsDALPricingScheme : IDisposable
    {
        public DataLayer objConnection { get; set; }
        public clsDTOPricingScheme objDTOPricingScheme { get; set; }
        ArrayList parameters;

        void IDisposable.Dispose()
        {
            if (parameters != null)
                parameters = null;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool CheckDuplication(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objConnection;
                    return objCommonUtility.CheckDuplication(saFieldValues);
                }
            }
            return false;
        }

        public bool SavePricingSchemeDetails(bool AddStatus)
        {
            object objOutPricingSchemeID = 0;

            parameters = new ArrayList();
            if (AddStatus)
                parameters.Add(new SqlParameter("@Mode", 1));//insert
            else
            {
                parameters.Add(new SqlParameter("@Mode", 2));//update
                parameters.Add(new SqlParameter("@PricingSchemeID", objDTOPricingScheme.intPricingSchemeID));
            }
            parameters.Add(new SqlParameter("@PricingShortName", objDTOPricingScheme.strPricingShortName));
            parameters.Add(new SqlParameter("@Description", objDTOPricingScheme.strDescription));            
            parameters.Add(new SqlParameter("@PercentOrAmount", objDTOPricingScheme.intPercentOrAmount));
            parameters.Add(new SqlParameter("@CurrencyID", objDTOPricingScheme.intCurrencyID));
            parameters.Add(new SqlParameter("@PercOrAmtValue", objDTOPricingScheme.dblPercOrAmtValue));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            if (objConnection.ExecuteNonQuery("spInvPricingScheme", parameters, out objOutPricingSchemeID) != 0)
            {
                if ((int)objOutPricingSchemeID > 0)
                {
                    objDTOPricingScheme.intPricingSchemeID = (int)objOutPricingSchemeID;
                    return true;
                }
            }
            return false;
        }

        public DataTable DisplayPricingScheme()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@RowNumber", objDTOPricingScheme.intRowNumber));
            return objConnection.ExecuteDataTable("spInvPricingScheme", parameters);
        }

        public bool DeletePricingScheme()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@PricingSchemeID", objDTOPricingScheme.intPricingSchemeID));
            if (objConnection.ExecuteNonQuery("spInvPricingScheme", parameters) != 0)
            {
                return true;
            }
            return false;
        }

        public DataSet GetPricingSchemeReport()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 14));
            return objConnection.ExecuteDataSet("[spInvPricingScheme]", parameters);
        }

    }
}
