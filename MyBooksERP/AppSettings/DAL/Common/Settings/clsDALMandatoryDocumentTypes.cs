﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

using System.Data;
namespace MyBooksERP
{
    public class clsDALMandatoryDocumentTypes
    {
        public clsDTOMandatoryDocumentTypes MobjclsDTOMandatoryDocumentTypes { get; set; }

        public DataLayer MobjDataLayer { get; set; }
        ArrayList prmMandatoryDocumentTypes;
        private string strProcName = "spMandatoryDocuments";

        public DataTable GetMandatoryDocumentTypes(int intOperationTypeID,int intCompanyID)
        {
            prmMandatoryDocumentTypes = new ArrayList();
            prmMandatoryDocumentTypes.Add(new SqlParameter("@Mode", 1));
            prmMandatoryDocumentTypes.Add(new SqlParameter("@OperationTypeID", intOperationTypeID));
            prmMandatoryDocumentTypes.Add(new SqlParameter("@CompanyID", intCompanyID));
            return MobjDataLayer.ExecuteDataTable(strProcName, prmMandatoryDocumentTypes);
        }
        public bool InsertMandatoryDocumentTypes()
        {
            DeleteMandatoryDocumentTypes();
            foreach (clsDTOMandatoryDocumentType objMandatoryDocumentType in MobjclsDTOMandatoryDocumentTypes.lstMandatoryDocumentTypes)
            {
                prmMandatoryDocumentTypes = new ArrayList();
                prmMandatoryDocumentTypes.Add(new SqlParameter("@Mode", 2));
                prmMandatoryDocumentTypes.Add(new SqlParameter("@OperationTypeID", MobjclsDTOMandatoryDocumentTypes.intOperationTypeID));
                prmMandatoryDocumentTypes.Add(new SqlParameter("@CompanyID", MobjclsDTOMandatoryDocumentTypes.intCompanyID));
                prmMandatoryDocumentTypes.Add(new SqlParameter("@DocumentTypeID", objMandatoryDocumentType.intDocumentTypeID));
                MobjDataLayer.ExecuteNonQueryWithTran(strProcName, prmMandatoryDocumentTypes);
            }
           return true;
        }
        public string ValidationInsertMandatoryDocumentTypes()
        {
            string strDoc ="";
            int intModuleType=0;
            if (MobjclsDTOMandatoryDocumentTypes.intOperationTypeID == 2 || MobjclsDTOMandatoryDocumentTypes.intOperationTypeID == 3)
            {
                intModuleType = 3;
            }
            else if (MobjclsDTOMandatoryDocumentTypes.intOperationTypeID == 8)
            {
                intModuleType = 4;
            }
            else if (MobjclsDTOMandatoryDocumentTypes.intOperationTypeID == 20)
            {
                intModuleType = 2;
            }
            foreach (clsDTOMandatoryDocumentType objMandatoryDocumentType in MobjclsDTOMandatoryDocumentTypes.lstMandatoryDocumentTypes)
            {
                prmMandatoryDocumentTypes = new ArrayList();
                prmMandatoryDocumentTypes.Add(new SqlParameter("@Mode", 4));
                prmMandatoryDocumentTypes.Add(new SqlParameter("@OperationTypeID", MobjclsDTOMandatoryDocumentTypes.intOperationTypeID));
                prmMandatoryDocumentTypes.Add(new SqlParameter("@CompanyID", MobjclsDTOMandatoryDocumentTypes.intCompanyID));
                prmMandatoryDocumentTypes.Add(new SqlParameter("@DocumentTypeID", objMandatoryDocumentType.intDocumentTypeID));
                prmMandatoryDocumentTypes.Add(new SqlParameter("@ModuleType",intModuleType));

                object objDoc = "";
                 objDoc = MobjDataLayer.ExecuteScalar(strProcName, prmMandatoryDocumentTypes);
                if ((string)objDoc != "")
                    strDoc = strDoc + (string)objDoc+",";
            }
            if (strDoc != "")
               strDoc= strDoc.Remove(strDoc.Length - 1);
            return strDoc;
        }


        public bool DeleteMandatoryDocumentTypes()
        {
            prmMandatoryDocumentTypes = new ArrayList();
            prmMandatoryDocumentTypes.Add(new SqlParameter("@Mode", 3));
            prmMandatoryDocumentTypes.Add(new SqlParameter("@OperationTypeID", MobjclsDTOMandatoryDocumentTypes.intOperationTypeID));
            prmMandatoryDocumentTypes.Add(new SqlParameter("@CompanyID", MobjclsDTOMandatoryDocumentTypes.intCompanyID));
            MobjDataLayer.ExecuteNonQueryWithTran(strProcName, prmMandatoryDocumentTypes);
            return true;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = MobjDataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

    }
}
