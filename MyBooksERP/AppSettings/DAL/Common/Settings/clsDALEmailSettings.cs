﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.ComponentModel;

namespace MyBooksERP
{
    public class clsDALEmailSettings
    {
        ArrayList prmEmail;

        public clsDTOEmailSettings ObjclsDTOEmailSettings { get; set; }
        public DataLayer ObjClsconnection { get; set; }
        private string spProcedureName = "spMailSettings";

        public bool GetEmailSmsSettings(int intAccountType)
        {
            ArrayList prmEmail = new ArrayList();
            prmEmail.Add(new SqlParameter("@Mode", (int)Mode.Select));
            prmEmail.Add(new SqlParameter("@AccountType", intAccountType));
            SqlDataReader RdEmail = ObjClsconnection.ExecuteReader(spProcedureName, prmEmail, CommandBehavior.SingleRow);

            if (RdEmail.HasRows && RdEmail != null)
            {
                RdEmail.Read();
                ObjclsDTOEmailSettings.UserName = RdEmail["UserName"].ToString();
                ObjclsDTOEmailSettings.PassWord = RdEmail["PassWord"].ToString();
                ObjclsDTOEmailSettings.IncomingServer = RdEmail["IncomingServer"].ToString();
                ObjclsDTOEmailSettings.OutgoingServer = RdEmail["OutgoingServer"].ToString();
                ObjclsDTOEmailSettings.PortNumber = Convert.ToInt32(RdEmail["PortNumber"]);
                ObjclsDTOEmailSettings.AccountType = RdEmail["AccountType"].ToString();
                RdEmail.Close();
                return true;
            }
            RdEmail.Close();
            return false;
        }

        public bool SaveEmailSettings(int intAccountType)
        {
            ArrayList prmEmail = new ArrayList();
            prmEmail.Add(new SqlParameter("@Mode", (int)Mode.Save));
            prmEmail.Add(new SqlParameter("@UserName", ObjclsDTOEmailSettings.UserName));
            prmEmail.Add(new SqlParameter("@Password", ObjclsDTOEmailSettings.PassWord));
            prmEmail.Add(new SqlParameter("@IncomingServer", ObjclsDTOEmailSettings.IncomingServer));
            prmEmail.Add(new SqlParameter("@OutgoingServer", ObjclsDTOEmailSettings.OutgoingServer));
            prmEmail.Add(new SqlParameter("@PortNumber", ObjclsDTOEmailSettings.PortNumber));
            prmEmail.Add(new SqlParameter("@AccountType", (ObjclsDTOEmailSettings.AccountType == "Email") ? 1 : 0));

            if (ObjClsconnection.ExecuteNonQuery(spProcedureName, prmEmail) != 0)
                return true;
            else return false;
        }

        public bool FillCombos(System.Windows.Forms.ComboBox cmb, string[] saFieldValues)
        {
            if (saFieldValues.Length == 5)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    //objCommonUtility.FillCombos(cmb, saFieldValues);
                }
                return true;
            }
            else
                return false;
        }

        public DataTable GetAllData()
        {
            DataTable Dt = new DataTable();

            // Code here	

            return Dt;
        }

        public DataTable GetDataByPrimaryKey()
        {
            DataTable Dt = new DataTable();

            // Code here

            return Dt;
        }

        public bool DeleteData()
        {
            // Code here

            return true;
        }

        public enum Mode
        {
            Select = 1,
            Save = 2
        }
    }
}