﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace MyBooksERP
{
   public class clsDALRoleSettings
    {
       DataLayer objDataLayer;
       public clsDTORoleSettings RoleSettings { get; set; }

       public clsDALRoleSettings()
       {
           this.objDataLayer = new DataLayer(); 
       }

       public DataTable BindRoles()
       {
           IList<SqlParameter> lstSqlParameters = new List<SqlParameter>();

           lstSqlParameters.Add(new SqlParameter("@Mode", "SR"));

           return this.objDataLayer.ExecuteDataTable("spRoleSettings", lstSqlParameters);

       }

       public DataTable BindModules()
       {
           IList<SqlParameter> lstSqlParameters = new List<SqlParameter>();

           lstSqlParameters.Add(new SqlParameter("@Mode", "SM"));

           return this.objDataLayer.ExecuteDataTable("spRoleSettings", lstSqlParameters);

       }

       public DataTable BindRoleDetails()
       {
           IList<SqlParameter> lstSqlParameters = new List<SqlParameter>();

           lstSqlParameters.Add(new SqlParameter("@Mode", "GRP"));
           lstSqlParameters.Add(new SqlParameter("@RoleID", RoleSettings.RoleID));

           return this.objDataLayer.ExecuteDataTable("spRoleSettings", lstSqlParameters);

       }

       public bool SaveRoleDetails()
       {
           //if (this.RoleSettings.lstRoleDetails.Count > 0)
           //{
               this.objDataLayer.BeginTransaction();
               try
               {
                   foreach (clsDTORoleSettings objclsRoleSettings in RoleSettings.lstRoleDetails)
                   {
                       IList<SqlParameter> lstSqlParameters = new List<SqlParameter>();
                       lstSqlParameters.Add(new SqlParameter("@Mode", "I"));

                       lstSqlParameters.Add(new SqlParameter("@RoleID", objclsRoleSettings.RoleID));
                       lstSqlParameters.Add(new SqlParameter("@ModuleID", objclsRoleSettings.ModuleID));
                       lstSqlParameters.Add(new SqlParameter("@MenuID", objclsRoleSettings.MenuID));
                       lstSqlParameters.Add(new SqlParameter("@IsCreate", objclsRoleSettings.IsCreate));
                       lstSqlParameters.Add(new SqlParameter("@IsView", objclsRoleSettings.IsView));
                       lstSqlParameters.Add(new SqlParameter("@IsUpdate", objclsRoleSettings.IsUpdate));
                       lstSqlParameters.Add(new SqlParameter("@IsDelete", objclsRoleSettings.IsDelete));
                       lstSqlParameters.Add(new SqlParameter("@IsPrintEmail", objclsRoleSettings.IsPrintEmail));

                       this.objDataLayer.ExecuteScalar("spRoleSettings", lstSqlParameters);
                   }
                   this.objDataLayer.CommitTransaction();
                   return true;
               }
               catch (SqlException ex)
               {
                   throw ex;
                   this.objDataLayer.RollbackTransaction();
                   return false;
               }
          // }
           
       }

       public DataTable GetModuleIDs()
       {
           IList<SqlParameter> lstSqlParameters = new List<SqlParameter>();

           lstSqlParameters.Add(new SqlParameter("@Mode", "GMI"));
           lstSqlParameters.Add(new SqlParameter("@RoleID", RoleSettings.RoleID));
           return this.objDataLayer.ExecuteDataTable("spRoleSettings", lstSqlParameters);

       }

       public DataTable BindaModulePermissions()
       {
           IList<SqlParameter> lstSqlParameters = new List<SqlParameter>();

           lstSqlParameters.Add(new SqlParameter("@Mode", "G"));
           lstSqlParameters.Add(new SqlParameter("@RoleID", RoleSettings.RoleID));
           lstSqlParameters.Add(new SqlParameter("@ModuleID", RoleSettings.ModuleID));

           return this.objDataLayer.ExecuteDataTable("spRoleSettings", lstSqlParameters);
       }

       public void DeleteModulePermissions(int iRoleID, int iModuleID)
       {
           IList<SqlParameter> lstSqlParameters = new List<SqlParameter>();

           lstSqlParameters.Add(new SqlParameter("@Mode", "DP"));
           lstSqlParameters.Add(new SqlParameter("@RoleID", iRoleID));
           lstSqlParameters.Add(new SqlParameter("@ModuleID", iModuleID));

           this.objDataLayer.ExecuteNonQuery("spRoleSettings", lstSqlParameters);

       }
    }
}
