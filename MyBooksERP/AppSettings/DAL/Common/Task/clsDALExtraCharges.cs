﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALExtraCharges
    {
        ArrayList prmExtraCharges;

        public clsDTOExtraCharges objDTOExtraCharges { get; set; }
        public DataLayer objConnection { get; set; }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataTable GetCompanyByPermission(int RoleID, int MenuID, int ControlID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 4));
            prmExtraCharges.Add(new SqlParameter("@RoleID", RoleID));
            prmExtraCharges.Add(new SqlParameter("@MenuID", MenuID));
            prmExtraCharges.Add(new SqlParameter("@ControlID", ControlID));
            return objConnection.ExecuteDataTable("STPermissionSettings", prmExtraCharges);
        }

        public bool SaveExtraCharge(DataTable datTemp)
        {
            if (objDTOExtraCharges.lngExtraChargeID > 0)
                DeleteExtraChargeDetails(false, 0);

            prmExtraCharges = new ArrayList();
            if (objDTOExtraCharges.lngExtraChargeID == 0)
                prmExtraCharges.Add(new SqlParameter("@Mode", 1)); // Insert
            else
                prmExtraCharges.Add(new SqlParameter("@Mode", 3)); // Update

            prmExtraCharges.Add(new SqlParameter("@ExtraChargeID", objDTOExtraCharges.lngExtraChargeID));
            prmExtraCharges.Add(new SqlParameter("@ExtraChargeNo", objDTOExtraCharges.strExtraChargeNo));
            prmExtraCharges.Add(new SqlParameter("@ExtraChargeDate", objDTOExtraCharges.dtExtraChargeDate));
            prmExtraCharges.Add(new SqlParameter("@OperationTypeID", objDTOExtraCharges.intOperationTypeID));
            prmExtraCharges.Add(new SqlParameter("@ReferenceID", objDTOExtraCharges.lngReferenceID));
            prmExtraCharges.Add(new SqlParameter("@TotalAmount", objDTOExtraCharges.decTotalAmount));
            prmExtraCharges.Add(new SqlParameter("@CompanyID", objDTOExtraCharges.intCompanyID));
            prmExtraCharges.Add(new SqlParameter("@CurrencyID", objDTOExtraCharges.intCurrencyID));
            prmExtraCharges.Add(new SqlParameter("@Description", objDTOExtraCharges.strDescription));
            prmExtraCharges.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmExtraCharges.Add(objParam);

            object OutExtraChargeID = null;

            if (objConnection.ExecuteNonQuery("spExtraCharges", prmExtraCharges, out OutExtraChargeID) != 0)
            {
                if (OutExtraChargeID.ToInt64() > 0)
                {
                    objDTOExtraCharges.lngExtraChargeID = OutExtraChargeID.ToInt64();
                    if (datTemp != null) // Save Extra Charge Details
                    {
                        if (datTemp.Rows.Count > 0)
                            SaveExtraChargeDetails(datTemp);
                    }
                    return true;
                }
            }
            return false;
        }

        private bool SaveExtraChargeDetails(DataTable datTemp)
        {
            int count = 0;
            for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
            {
                prmExtraCharges = new ArrayList();
                prmExtraCharges.Add(new SqlParameter("@Mode", 2));
                prmExtraCharges.Add(new SqlParameter("@TempSerialNo", ++count));
                prmExtraCharges.Add(new SqlParameter("@ExtraChargeID", objDTOExtraCharges.lngExtraChargeID));
                prmExtraCharges.Add(new SqlParameter("@ExpenseTypeID", datTemp.Rows[iCounter]["ExpenseTypeID"].ToInt32()));
                prmExtraCharges.Add(new SqlParameter("@VendorID", datTemp.Rows[iCounter]["VendorID"].ToInt32()));
                prmExtraCharges.Add(new SqlParameter("@Amount", datTemp.Rows[iCounter]["Amount"].ToDecimal()));
                prmExtraCharges.Add(new SqlParameter("@Description", datTemp.Rows[iCounter]["Description"].ToString()));
                prmExtraCharges.Add(new SqlParameter("@VoucherID", datTemp.Rows[iCounter]["VoucherID"].ToDecimal()));
                objConnection.ExecuteNonQuery("spExtraCharges", prmExtraCharges);
            }
            return true;
        }

        public bool DeleteExtraCharge()
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 4));
            prmExtraCharges.Add(new SqlParameter("@ExtraChargeID", objDTOExtraCharges.lngExtraChargeID));
            objConnection.ExecuteNonQuery("spExtraCharges", prmExtraCharges);
            return true;
        }

        public bool DeleteExtraChargeDetails(bool blnIsRowDelete, int intSerialNo)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 5));
            prmExtraCharges.Add(new SqlParameter("@ExtraChargeID", objDTOExtraCharges.lngExtraChargeID));
            prmExtraCharges.Add(new SqlParameter("@TempSerialNo", intSerialNo));
            prmExtraCharges.Add(new SqlParameter("@IsRowDelete", blnIsRowDelete));
            objConnection.ExecuteNonQuery("spExtraCharges", prmExtraCharges);
            return true;
        }

        public DataTable DisplayExtraChargeMaster(long lngRowNum)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 6));
            prmExtraCharges.Add(new SqlParameter("@RowNum", lngRowNum));
            DataTable datTemp = objConnection.ExecuteDataTable("spExtraCharges", prmExtraCharges);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    objDTOExtraCharges.lngExtraChargeID = datTemp.Rows[0]["ExtraChargeID"].ToInt64();
                    objDTOExtraCharges.strExtraChargeNo = datTemp.Rows[0]["ExtraChargeNo"].ToString();
                    objDTOExtraCharges.dtExtraChargeDate = datTemp.Rows[0]["ExtraChargeDate"].ToDateTime();
                    objDTOExtraCharges.intOperationTypeID = datTemp.Rows[0]["OperationTypeID"].ToInt32();
                    objDTOExtraCharges.lngReferenceID = datTemp.Rows[0]["ReferenceID"].ToInt64();
                    objDTOExtraCharges.decTotalAmount = datTemp.Rows[0]["TotalAmount"].ToDecimal();
                    objDTOExtraCharges.intCompanyID = datTemp.Rows[0]["CompanyID"].ToInt32();
                    objDTOExtraCharges.intCurrencyID = datTemp.Rows[0]["CurrencyID"].ToInt32();
                    objDTOExtraCharges.strDescription = datTemp.Rows[0]["Description"].ToString();

                    datTemp = null;
                    datTemp = getExtraChargeDetails(objDTOExtraCharges.lngExtraChargeID);
                }
            }
            return datTemp;
        }

        public string getExtraChargeNo(int intCompanyID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 7));
            prmExtraCharges.Add(new SqlParameter("@CompanyID", intCompanyID));
            return Convert.ToString(objConnection.ExecuteScalar("spExtraCharges", prmExtraCharges));
        }

        public int getCurrency(int intCompanyID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 8));
            prmExtraCharges.Add(new SqlParameter("@CompanyID", intCompanyID));
            return Convert.ToInt32(objConnection.ExecuteScalar("spExtraCharges", prmExtraCharges));
        }

        public DataTable getReferenceNo(int intMode, int intCompanyID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", intMode)); // 9 - PO & 10 - PI & 25 - SO & 26 - SI
            prmExtraCharges.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objConnection.ExecuteDataTable("spExtraCharges", prmExtraCharges);
        }

        public long getRecordCount()
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 11));
            return Convert.ToInt64(objConnection.ExecuteScalar("spExtraCharges", prmExtraCharges));
        }

        public DataTable getExtraChargeDetails(long lngExtraChargeID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 12));
            prmExtraCharges.Add(new SqlParameter("@ExtraChargeID", lngExtraChargeID));
            return objConnection.ExecuteDataTable("spExtraCharges", prmExtraCharges);
        }

        public string CheckExtraChargeAutoGenerate(int intCompanyID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 13));
            prmExtraCharges.Add(new SqlParameter("@CompanyID", intCompanyID));
            return Convert.ToString(objConnection.ExecuteScalar("spExtraCharges", prmExtraCharges));
        }

        public bool CheckExtraChargeDuplication(int intCompanyID, long lngExChID, string strExChNo)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 14));
            prmExtraCharges.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmExtraCharges.Add(new SqlParameter("@ExtraChargeID", lngExChID));
            prmExtraCharges.Add(new SqlParameter("@ExtraChargeNo", strExChNo.Trim()));
            return Convert.ToBoolean(objConnection.ExecuteScalar("spExtraCharges", prmExtraCharges));
        }

        public DataSet PrintExtraCharges(long ExtraChargeID)
        {
            return new DataLayer().ExecuteDataSet("spExtraCharges", new List<SqlParameter>{
                new SqlParameter("@Mode",15),
                new SqlParameter("@ExtraChargeID",ExtraChargeID)
            });
        }

        public bool CheckIDExists(long lngExChID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 17));
            prmExtraCharges.Add(new SqlParameter("@ExtraChargeID", lngExChID));
            return Convert.ToBoolean(objConnection.ExecuteScalar("spExtraCharges", prmExtraCharges));
        }

        public string ConvertToWord(string strAmount, int intCurrencyID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 19));
            prmExtraCharges.Add(new SqlParameter("@TempTotalAmount", strAmount));
            prmExtraCharges.Add(new SqlParameter("@CurrencyID", intCurrencyID));
            return Convert.ToString(objConnection.ExecuteScalar("spExtraChargePayment", prmExtraCharges));
        }

        public long getRecordCountForSpecificValue(int intCompID, int OpTypeID, long lngRefID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 18));
            prmExtraCharges.Add(new SqlParameter("@CompanyID", intCompID));
            prmExtraCharges.Add(new SqlParameter("@OperationTypeID", OpTypeID));
            prmExtraCharges.Add(new SqlParameter("@ReferenceID", lngRefID));
            return Convert.ToInt64(objConnection.ExecuteScalar("spExtraCharges", prmExtraCharges));
        }

        public DataTable DisplayExtraChargeMasterForSpecificValue(long lngRowNum, int intCompID, int OpTypeID, long lngRefID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 19));
            prmExtraCharges.Add(new SqlParameter("@RowNum", lngRowNum));
            prmExtraCharges.Add(new SqlParameter("@CompanyID", intCompID));
            prmExtraCharges.Add(new SqlParameter("@OperationTypeID", OpTypeID));
            prmExtraCharges.Add(new SqlParameter("@ReferenceID", lngRefID));
            DataTable datTemp = objConnection.ExecuteDataTable("spExtraCharges", prmExtraCharges);

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    objDTOExtraCharges.lngExtraChargeID = datTemp.Rows[0]["ExtraChargeID"].ToInt64();
                    objDTOExtraCharges.strExtraChargeNo = datTemp.Rows[0]["ExtraChargeNo"].ToString();
                    objDTOExtraCharges.dtExtraChargeDate = datTemp.Rows[0]["ExtraChargeDate"].ToDateTime();
                    objDTOExtraCharges.intOperationTypeID = datTemp.Rows[0]["OperationTypeID"].ToInt32();
                    objDTOExtraCharges.lngReferenceID = datTemp.Rows[0]["ReferenceID"].ToInt64();
                    objDTOExtraCharges.decTotalAmount = datTemp.Rows[0]["TotalAmount"].ToDecimal();
                    objDTOExtraCharges.intCompanyID = datTemp.Rows[0]["CompanyID"].ToInt32();
                    objDTOExtraCharges.intCurrencyID = datTemp.Rows[0]["CurrencyID"].ToInt32();
                    objDTOExtraCharges.strDescription = datTemp.Rows[0]["Description"].ToString();

                    datTemp = null;
                    datTemp = getExtraChargeDetails(objDTOExtraCharges.lngExtraChargeID);
                }
            }
            return datTemp;
        }

        public bool CheckPaymentExist(int intExtraChargeID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 21));
            prmExtraCharges.Add(new SqlParameter("@ExtraChargeID", intExtraChargeID));
            object objResult = objConnection.ExecuteScalar("spExtraCharges", prmExtraCharges);

            if (objResult.ToInt32() > 0)
                return true;
            else 
                return false;
        }

        public bool CheckPurchaseInvoiceStatus(int intReferenceID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 22));
            prmExtraCharges.Add(new SqlParameter("@ReferenceID", intReferenceID));
            object objResult = objConnection.ExecuteScalar("spExtraCharges", prmExtraCharges);

            if (objResult.ToInt32() == (int)OperationStatusType.PInvoiceCancelled)
                return true;
            else
                return false;
        }

        public bool CheckPurchaseOrderStatus(int intReferenceID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 23));
            prmExtraCharges.Add(new SqlParameter("@ReferenceID", intReferenceID));
            object objResult = objConnection.ExecuteScalar("spExtraCharges", prmExtraCharges);

            if (objResult.ToInt32() == (int)OperationStatusType.POrderCancelled)
                return true;
            else
                return false;
        }

        public bool GetCompanyAccount(int intCompanyID, int intTransactionType)
        {
            // function for Geting Company Account
            object objOutAccountID = 0;
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 24));
            prmExtraCharges.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmExtraCharges.Add(new SqlParameter("@TransactionType", intTransactionType));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmExtraCharges.Add(objParam);

            if (objConnection.ExecuteNonQuery("spExtraCharges", prmExtraCharges, out objOutAccountID) != 0)
            {
                if ((int)objOutAccountID > 0) return true;
            }
            return false;
        }

        public bool CheckSalesInvoiceStatus(int intReferenceID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 27));
            prmExtraCharges.Add(new SqlParameter("@ReferenceID", intReferenceID));
            object objResult = objConnection.ExecuteScalar("spExtraCharges", prmExtraCharges);

            if (objResult.ToInt32() == (int)OperationStatusType.SInvoiceCancelled)
                return true;
            else
                return false;
        }

        public bool CheckSalesOrderStatus(int intReferenceID)
        {
            prmExtraCharges = new ArrayList();
            prmExtraCharges.Add(new SqlParameter("@Mode", 28));
            prmExtraCharges.Add(new SqlParameter("@ReferenceID", intReferenceID));
            object objResult = objConnection.ExecuteScalar("spExtraCharges", prmExtraCharges);

            if (objResult.ToInt32() == (int)OperationStatusType.SOrderCancelled)
                return true;
            else
                return false;
        }
    }
}