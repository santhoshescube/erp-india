﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using System.Data;
using System.Data.SqlClient;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,16 Mar 2011>
   Description:	<Description,,Payments DAL>
================================================
*/
namespace MyBooksERP
{
    public class clsDALPaymentMaster : IDisposable
    {
        ArrayList prmPayments;                                      // array list for storing parameters
        public clsDTOPaymentMaster objDTOPaymentMaster { get; set; }// property of clsDTOPaymentMaster
        public DataLayer objDataLayer { get; set; }                 // property of datalayer    

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objDataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool GetPaymentsInfo(int intModuleID)
        {
            // function for getting payments information
            bool blnRead = false;
            prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", 1));
            prmPayments.Add(new SqlParameter("@RowNumber", objDTOPaymentMaster.intRowNumber));
            prmPayments.Add(new SqlParameter("@IsReceipt", objDTOPaymentMaster.blnIsReceipt));
            prmPayments.Add(new SqlParameter("@ModuleID", intModuleID));

            using (SqlDataReader sdr = objDataLayer.ExecuteReader("spInvReceiptAndPayment", prmPayments, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    objDTOPaymentMaster.intPaymentID = Convert.ToInt64(sdr["ReceiptAndPaymentID"]);
                    objDTOPaymentMaster.intSerialNo = Convert.ToInt32(sdr["SerialNo"]); 
                    objDTOPaymentMaster.strPaymentNumber = Convert.ToString(sdr["PaymentNumber"]);
                    objDTOPaymentMaster.strPaymentDate = Convert.ToString(sdr["PaymentDate"]);
                    objDTOPaymentMaster.intOperationTypeID = Convert.ToInt32(sdr["OperationTypeID"]);
                    objDTOPaymentMaster.intPaymentTypeID = Convert.ToInt32(sdr["ReceiptAndPaymentTypeID"]);
                    objDTOPaymentMaster.intPaymentModeID = Convert.ToInt32(sdr["TransactionTypeID"]);
                    objDTOPaymentMaster.intReferenceID = Convert.ToInt64(sdr["ReferenceID"]);

                    if (sdr["ChequeNumber"] != DBNull.Value)
                        objDTOPaymentMaster.strChequeNumber = Convert.ToString(sdr["ChequeNumber"]);
                    else
                        objDTOPaymentMaster.strChequeNumber = null;

                    if (sdr["ChequeDate"] != DBNull.Value)
                        objDTOPaymentMaster.strChequeDate = Convert.ToString(sdr["ChequeDate"]);
                    else
                        objDTOPaymentMaster.strChequeDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");

                    objDTOPaymentMaster.strCreatedDate = Convert.ToString(sdr["CreatedDate"]);

                    //objDTOPaymentMaster.intCreditHeadID = Convert.ToInt32(sdr["CreditHeadID"]);
                    //objDTOPaymentMaster.intDebitHeadID = Convert.ToInt32(sdr["DebitHeadID"]);
                    objDTOPaymentMaster.decAmount = Convert.ToDecimal(sdr["Amount"]);

                    objDTOPaymentMaster.intAccountID = sdr["AccountID"].ToInt32();

                    //if(sdr["VoucherNo"] != DBNull.Value)
                    //    objDTOPaymentMaster.strVoucherNo = Convert.ToString(sdr["VoucherNo"]);
                    
                    //if (sdr["VoucherIds"] != DBNull.Value)
                    //        objDTOPaymentMaster.strVoucherIds = Convert.ToString(sdr["VoucherIds"]);
                    
                    if(sdr["IsDirect"] != DBNull.Value)
                        objDTOPaymentMaster.blnPostToAccount = Convert.ToBoolean(sdr["IsDirect"]);
                    objDTOPaymentMaster.intCurrencyID = Convert.ToInt32(sdr["CurrencyID"]);
                    objDTOPaymentMaster.intCompanyID = Convert.ToInt32(sdr["CompanyID"]);
                    
                    if(sdr["Remarks"] != DBNull.Value)
                        objDTOPaymentMaster.strDescription = Convert.ToString(sdr["Remarks"]);
                    
                    if(sdr["StatusID"] !=DBNull.Value)
                        objDTOPaymentMaster.intStatusID = Convert.ToInt32(sdr["StatusID"]);
                    objDTOPaymentMaster.intRowNumber = Convert.ToInt32(sdr["RowNumber"]);
                    objDTOPaymentMaster.blnIsReceipt = Convert.ToBoolean(sdr["IsReceipt"]);
                    blnRead = true;
                }
                sdr.Close();
            }
            return blnRead;
        }

        public bool SavePayments()
        {
            //Saving Payments
            prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", objDTOPaymentMaster.intPaymentID == 0 ? 2 : 3));
            prmPayments.Add(new SqlParameter("@ReceiptAndPaymentID", objDTOPaymentMaster.intPaymentID));
            prmPayments.Add(new SqlParameter("@PaymentNumber", objDTOPaymentMaster.strPaymentNumber));
            prmPayments.Add(new SqlParameter("@SerialNo", objDTOPaymentMaster.intSerialNo));
            prmPayments.Add(new SqlParameter("@PaymentDate", objDTOPaymentMaster.strPaymentDate));
            prmPayments.Add(new SqlParameter("@OperationTypeID", objDTOPaymentMaster.intOperationTypeID));
            prmPayments.Add(new SqlParameter("@ReceiptAndPaymentTypeID", objDTOPaymentMaster.intPaymentTypeID));
            prmPayments.Add(new SqlParameter("@TransactionTypeID", objDTOPaymentMaster.intPaymentModeID));
            prmPayments.Add(new SqlParameter("@ReferenceID", objDTOPaymentMaster.intReferenceID));

            if (objDTOPaymentMaster.intPaymentModeID != (int)PaymentModes.Cash)
            {
                prmPayments.Add(new SqlParameter("@AccountID", objDTOPaymentMaster.intAccountID));
                prmPayments.Add(new SqlParameter("@ChequeNumber", objDTOPaymentMaster.strChequeNumber));
                prmPayments.Add(new SqlParameter("@ChequeDate", objDTOPaymentMaster.strChequeDate));
            }
            //prmPayments.Add(new SqlParameter("@DebitHeadID", objDTOPaymentMaster.intDebitHeadID));
            prmPayments.Add(new SqlParameter("@Amount", objDTOPaymentMaster.decAmount));
           // prmPayments.Add(new SqlParameter("@VoucherNo", objDTOPaymentMaster.strVoucherNo));
           // prmPayments.Add(new SqlParameter("@VoucherIds", objDTOPaymentMaster.strVoucherIds));
            prmPayments.Add(new SqlParameter("@IsDirect", objDTOPaymentMaster.blnPostToAccount));
            prmPayments.Add(new SqlParameter("@CurrencyID", objDTOPaymentMaster.intCurrencyID));
            prmPayments.Add(new SqlParameter("@CompanyID", objDTOPaymentMaster.intCompanyID));
            prmPayments.Add(new SqlParameter("@CreatedBy", objDTOPaymentMaster.intCreatedBy));
            prmPayments.Add(new SqlParameter("@CreatedDate", objDTOPaymentMaster.strCreatedDate));
            prmPayments.Add(new SqlParameter("@Remarks", objDTOPaymentMaster.strDescription));
          //  prmPayments.Add(new SqlParameter("@StatusID", objDTOPaymentMaster.intStatusID));
            prmPayments.Add(new SqlParameter("@IsReceipt", objDTOPaymentMaster.blnIsReceipt));

            if (objDTOPaymentMaster.blnIsReceipt)
                prmPayments.Add(new SqlParameter("@OperationType", (int)OperationType.Receipt));
            else
                prmPayments.Add(new SqlParameter("@OperationType", (int)OperationType.Payment));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPayments.Add(objParam);

            object objOutPaymentID = 0;

            if (objDataLayer.ExecuteNonQueryWithTran("spInvReceiptAndPayment", prmPayments, out objOutPaymentID) != 0)
            {
                if (Convert.ToInt64(objOutPaymentID) > 0)
                {
                    objDTOPaymentMaster.intPaymentID = Convert.ToInt64(objOutPaymentID);
                    return true;
                }
            }
            return false;
        }

        public bool DeletePayments()
        {
            //Delete Payments
            prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", 4));

            if (objDTOPaymentMaster.blnIsReceipt)
                prmPayments.Add(new SqlParameter("@OperationType", (int)OperationType.Receipt));
            else
                prmPayments.Add(new SqlParameter("@OperationType", (int)OperationType.Payment));

            prmPayments.Add(new SqlParameter("@ReceiptAndPaymentID", objDTOPaymentMaster.intPaymentID));

            if (objDataLayer.ExecuteNonQueryWithTran("spInvReceiptAndPayment", prmPayments) != 0)
            {
                return true;
            }
            return false;
        }

        public int GetRecordCount(int intRoleID, int intCompanyID, int intControlID,int intModuleID)
        {
            //function for getting record count
            prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", 6));
            prmPayments.Add(new SqlParameter("@IsReceipt", objDTOPaymentMaster.blnIsReceipt));
            //prmPayments.Add(new SqlParameter("@CompanyID", objDTOPaymentMaster.intCompanyID));
            prmPayments.Add(new SqlParameter("@RoleID", intRoleID));
            prmPayments.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPayments.Add(new SqlParameter("@ControlID", intControlID));
            prmPayments.Add(new SqlParameter("@ModuleID", intModuleID));
            int intRecordCount = -1;
            object objValue = objDataLayer.ExecuteScalar("spInvReceiptAndPayment", prmPayments);
            if (objValue != DBNull.Value)
                intRecordCount = Convert.ToInt32(objValue);
            
            return intRecordCount;
        }

        public Int64 GetMaxSerialNo(int intCompanyID)
        {
            // function for getting max of paymentsID
            prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", 5));
            prmPayments.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPayments.Add(new SqlParameter("@IsReceipt", objDTOPaymentMaster.blnIsReceipt));
            SqlDataReader sdr;
            sdr = objDataLayer.ExecuteReader("spInvReceiptAndPayment", prmPayments);
            Int64 intRetValue = 0;
            
            if (sdr.Read())
            {
                if (sdr["MaxSerialNo"] != DBNull.Value)
                {
                    intRetValue = Convert.ToInt32(sdr["MaxSerialNo"]);
                }
            }
            sdr.Close();
            
            return intRetValue + 1;
        }

        public bool GetCompanyAccount(int intCompanyID,bool blnIsPayment)
        {
            // function for Geting Company Account
            object objOutAccountID = 0;
            prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", 18));
            prmPayments.Add(new SqlParameter("@CompanyID", intCompanyID));
            
            if(blnIsPayment)
            prmPayments.Add(new SqlParameter("@TransactionType", Convert.ToInt32(TransactionTypes.Payment)));
            else
                prmPayments.Add(new SqlParameter("@TransactionType",Convert.ToInt32(TransactionTypes.Receipts)));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPayments.Add(objParam);

            if (objDataLayer.ExecuteNonQuery("spInvPurchaseModuleFunctions", prmPayments, out objOutAccountID) != 0)
            {
                if (blnIsPayment)
                    ClsCommonSettings.PintPaymentDebitHeadID = (int)objOutAccountID;
                else
                    ClsCommonSettings.PintReceiptsCreditHeadID = (int)objOutAccountID;

                if ((int)objOutAccountID > 0) return true;
            }
            return false;
        }

        public DataTable GetAccounts()
        {
            // function for getting data for filling credit head and debit head
            prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@ParentID", objDTOPaymentMaster.intPaymentHeadID));
            prmPayments.Add(new SqlParameter("@CompID", objDTOPaymentMaster.intCompanyID));

            return objDataLayer.ExecuteDataSet("spAccGetAccounts", prmPayments).Tables[0];
        }

        public DataSet GetPaymentReport()
        {
            // function for getting report details
            prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", 7));
            prmPayments.Add(new SqlParameter("@ReceiptAndPaymentID", objDTOPaymentMaster.intPaymentID));
            return objDataLayer.ExecuteDataSet("spInvReceiptAndPayment", prmPayments);
        }

        public decimal GetBalanceAmount(int intOperationTypeID, Int64 intReferenceID,int intPaymentType)
        {
            // function for getting BalanceAmount for Reference No
            decimal decBalAmount = 0;
            decimal decInvAmt = 0, decExpenseAmt = 0, decPaidAmt = 0, decAdvPayment = 0;
            prmPayments = new ArrayList();

            switch(intOperationTypeID)
            {
                case (int)OperationType.PurchaseOrder:
                case (int)OperationType.SalesOrder:
                //case (int)OperationType.Complaints:
                //case (int)OperationType.DeliverySchedule:
                //case (int)OperationType.ComplaintScheduling:
                    prmPayments.Add(new SqlParameter("@Mode", 8));
                    prmPayments.Add(new SqlParameter("@OperationTypeID", intOperationTypeID));
                    prmPayments.Add(new SqlParameter("@ReferenceID", intReferenceID));
                    prmPayments.Add(new SqlParameter("@ReceiptAndPaymentTypeID", intPaymentType));
                    SqlDataReader sdr = objDataLayer.ExecuteReader("spInvReceiptAndPayment", prmPayments);

                    if (sdr.Read())
                    {
                        if (sdr["Amount"] != DBNull.Value)
                        {
                            decBalAmount = Convert.ToDecimal(sdr["Amount"]);
                        }
                    }
                    sdr.Close();
                    break;
                case (int)OperationType.PurchaseInvoice:
                   
                    decBalAmount = 0;
                    if (intPaymentType == (int)PaymentTypes.DirectExpense)
                    {
                        DataTable datExpenses = FillCombos(new string[] { "Sum(TotalAmount) as TotalAmount", "InvExpenseMaster", "OperationTypeID=" + (int)OperationType.PurchaseInvoice + " And ReferenceID = " + intReferenceID+" And IsDirectExpense = 0" });
                        if (datExpenses.Rows.Count > 0)
                            decExpenseAmt = datExpenses.Rows[0]["TotalAmount"].ToDecimal();
                        DataTable datPayments = FillCombos(new string[] { "Sum(RPD.Amount) as TotalAmount", "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", "Payments.OperationTypeID = " + (int)OperationType.PurchaseInvoice + " And RPD.ReferenceID = " + intReferenceID + " And Payments.ReceiptAndPaymentTypeID = " + (int)PaymentTypes.DirectExpense });
                        if (datPayments.Rows.Count > 0)
                            decPaidAmt = datPayments.Rows[0]["TotalAmount"].ToDecimal();
                        decBalAmount = decExpenseAmt - decPaidAmt;
                    }
                    else if(intPaymentType == (int)PaymentTypes.InvoicePayment)
                    {
                        DataTable datInvDetails = FillCombos(new string[] { "OrderTypeID,PurchaseOrderID,NetAmount", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + intReferenceID });
                        if (datInvDetails.Rows.Count > 0)
                        {
                            int intInvoiceType = datInvDetails.Rows[0]["OrderTypeID"].ToInt32();
                            int intPurchaseOrderID = datInvDetails.Rows[0]["PurchaseOrderID"].ToInt32();
                            decInvAmt = datInvDetails.Rows[0]["NetAmount"].ToDecimal();

                            if (intInvoiceType == (int)OperationOrderType.PInvoiceFromOrder)
                            {
                                decInvAmt = 0;
                                DataTable datPayments = null;
                                datInvDetails = FillCombos(new string[] { "PurchaseInvoiceID,NetAmount", "InvPurchaseInvoiceMaster", "OrderTypeID = " + (int)OperationOrderType.PInvoiceFromOrder + " And PurchaseOrderID = " + intPurchaseOrderID });
                                foreach (DataRow dr in datInvDetails.Rows)
                                {
                                    decInvAmt += dr["NetAmount"].ToDecimal();
                                    datPayments = FillCombos(new string[] { "Sum(RPD.Amount) as TotalAmount", "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", "Payments.OperationTypeID = " + (int)OperationType.PurchaseInvoice + " And RPD.ReferenceID = " + dr["PurchaseInvoiceID"].ToInt64() + " And Payments.ReceiptAndPaymentTypeID = " + (int)PaymentTypes.InvoicePayment });
                                    if (datPayments.Rows.Count > 0)
                                        decPaidAmt += datPayments.Rows[0]["TotalAmount"].ToDecimal();
                                }
                                datPayments = FillCombos(new string[] { "Sum(RPD.Amount) as TotalAmount", "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", "Payments.OperationTypeID = " + (int)OperationType.PurchaseOrder + " And RPD.ReferenceID = " + intPurchaseOrderID + " And Payments.ReceiptAndPaymentTypeID = " + (int)PaymentTypes.AdvancePayment });
                                if (datPayments.Rows.Count > 0)
                                    decAdvPayment = datPayments.Rows[0]["TotalAmount"].ToDecimal();

                                decBalAmount = decInvAmt - decAdvPayment - decPaidAmt;
                            }
                            else
                            {
                                DataTable datPayments = FillCombos(new string[] { "Sum(RPD.Amount) as TotalAmount", "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", "Payments.OperationTypeID = " + (int)OperationType.PurchaseInvoice + " And RPD.ReferenceID = " + intReferenceID + " And RPD.ReceiptAndPaymentTypeID = " + (int)PaymentTypes.InvoicePayment });
                                if (datPayments.Rows.Count > 0)
                                    decPaidAmt = datPayments.Rows[0]["TotalAmount"].ToDecimal();

                                decBalAmount = decInvAmt - decPaidAmt;
                            }
                        }
                    }
                    break;
                case (int)OperationType.SalesInvoice:

                    decBalAmount = 0;
                    if (intPaymentType == (int)PaymentTypes.DirectExpense)
                    {
                        DataTable datExpenses = FillCombos(new string[] { "Sum(TotalAmount) as TotalAmount", "InvExpenseMaster", "OperationTypeID=" + (int)OperationType.SalesInvoice + " And ReferenceID = " + intReferenceID + " And IsDirectExpense = 0 " });
                        if (datExpenses.Rows.Count > 0)
                            decExpenseAmt = datExpenses.Rows[0]["TotalAmount"].ToDecimal();
                        DataTable datPayments = FillCombos(new string[] { "Sum(RPD.Amount) as TotalAmount", "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", "Payments.OperationTypeID = " + (int)OperationType.SalesInvoice + " And RPD.ReferenceID = " + intReferenceID + " And Payments.ReceiptAndPaymentTypeID = " + (int)PaymentTypes.DirectExpense });
                        if (datPayments.Rows.Count > 0)
                            decPaidAmt = datPayments.Rows[0]["TotalAmount"].ToDecimal();
                        decBalAmount = decExpenseAmt - decPaidAmt;
                    }
                    else
                    {
                        DataTable datInvDetails = FillCombos(new string[] { "SalesOrderID,NetAmount", "InvSalesInvoiceMaster", "SalesInvoiceID = " + intReferenceID });
                        if (datInvDetails.Rows.Count > 0)
                        {
                            int intSalesOrderID = datInvDetails.Rows[0]["SalesOrderID"].ToInt32();
                            decInvAmt = datInvDetails.Rows[0]["NetAmount"].ToDecimal();
                            DataTable datPayments = null;
                            datPayments = FillCombos(new string[] { "Sum(Amount) as TotalAmount", "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", "Payments.OperationTypeID = " + (int)OperationType.SalesInvoice + " And RPD.ReferenceID = " + intReferenceID + " And Payments.ReceiptAndPaymentTypeID = " + (int)PaymentTypes.InvoicePayment });
                            if (datPayments.Rows.Count > 0)
                                decPaidAmt += datPayments.Rows[0]["TotalAmount"].ToDecimal();
                            datPayments = FillCombos(new string[] { "Sum(RPD.Amount) as TotalAmount", "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", "Payments.OperationTypeID = " + (int)OperationType.SalesOrder + " And RPD.ReferenceID = " + intSalesOrderID + " And Payments.ReceiptAndPaymentTypeID = " + (int)PaymentTypes.AdvancePayment });
                            if (datPayments.Rows.Count > 0)
                                decAdvPayment = datPayments.Rows[0]["TotalAmount"].ToDecimal();

                            decBalAmount = decInvAmt - decAdvPayment - decPaidAmt;
                        }
                    }
                    break;
                case (int)OperationType.StockTransfer:

                    decBalAmount = 0;
                    if (intPaymentType == (int)PaymentTypes.DirectExpense)
                    {
                        DataTable datExpenses = FillCombos(new string[] { "Sum(TotalAmount) as TotalAmount", "InvExpenseMaster", "OperationTypeID=" + (int)OperationType.StockTransfer + " And ReferenceID = " + intReferenceID + " And IsDirectExpense = 0" });
                        if (datExpenses.Rows.Count > 0)
                            decExpenseAmt = datExpenses.Rows[0]["TotalAmount"].ToDecimal();
                        DataTable datPayments = FillCombos(new string[] { "Sum(RPD.Amount) as TotalAmount", "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", " Payments.OperationTypeID = " + (int)OperationType.StockTransfer + " And RPD.ReferenceID = " + intReferenceID + " And  Payments.ReceiptAndPaymentTypeID = " + (int)PaymentTypes.DirectExpense });
                        if (datPayments.Rows.Count > 0)
                            decPaidAmt = datPayments.Rows[0]["TotalAmount"].ToDecimal();
                        decBalAmount = decExpenseAmt - decPaidAmt;
                    }
                    else
                    {
                        DataTable datTransferDetails = FillCombos(new string[] { "ExpenseAmount", "InvStockTransferMaster", "StockTransferID = " + intReferenceID });
                        decInvAmt = datTransferDetails.Rows[0]["ExpenseAmount"].ToDecimal();

                        DataTable datPayments = FillCombos(new string[] { "Sum(RPD.Amount) as TotalAmount", "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", "Payments.OperationTypeID = " + (int)OperationType.StockTransfer + " And RPD.ReferenceID = " + intReferenceID + " And Payments.ReceiptAndPaymentTypeID = " + (int)PaymentTypes.InvoiceExpense });
                            if (datPayments.Rows.Count > 0)
                                decPaidAmt = datPayments.Rows[0]["TotalAmount"].ToDecimal();

                            decBalAmount = decInvAmt - decPaidAmt;
                    }
                    break;
            }
            return decBalAmount;
        }

        public DataSet GetFilterDetails(string FilterStr)
        {
            //function for getting pending invoices and pos for delivery
            prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", 9));
            prmPayments.Add(new SqlParameter("@StrFilter", FilterStr));
            return objDataLayer.ExecuteDataSet("spInvReceiptAndPayment", prmPayments);
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return objDataLayer.ExecuteDataTable("STPermissionSettings", parameters);
        }

        public DataTable GetOperationTypeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return objDataLayer.ExecuteDataTable("STPermissionSettings", parameters);
        }

        public DataTable GetPendingInvoices(int intCompanyID,int intType)
        {
            // 1 - Purchase Invoice
            // 2 - Sales Invoice
            ArrayList alParameters = new ArrayList();
            if (intType == 1)
                alParameters.Add(new SqlParameter("@Mode", 10));
            else if (intType == 2)
                alParameters.Add(new SqlParameter("@Mode", 11));

            alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objDataLayer.ExecuteDataTable("spInvReceiptAndPayment", alParameters);
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (prmPayments != null)
                prmPayments = null;
        }

        #endregion
    }
}
