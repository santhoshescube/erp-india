﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Data;
using System.Data.SqlClient;


namespace MyBooksERP
{
    public class clsDALExpenseMaster
    {
        ArrayList prmExpense;

        public clsDTOExpenseMaster objDTOExpenseMaster { get; set; }
        public DataLayer objConnection { get; set; }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool GetExpenseInfo(int PintOperationType, long PintOperationID,int intModuleID)
        {
            bool blnRead = false;
            prmExpense = new ArrayList();
            prmExpense.Add(new SqlParameter("@Mode", 1));
            prmExpense.Add(new SqlParameter("@RowNumber", objDTOExpenseMaster.intRowNumber));
            prmExpense.Add(new SqlParameter("@IsDirectExpense", objDTOExpenseMaster.blnPostToAccount));
            prmExpense.Add(new SqlParameter("@OperationTypeID", PintOperationType));
            prmExpense.Add(new SqlParameter("@ReferenceID", PintOperationID));
            prmExpense.Add(new SqlParameter("@ModuleID", intModuleID));

            using (DataTable datExpense = objConnection.ExecuteDataTable("spInvExpense", prmExpense))
            {
                if (datExpense.Rows.Count >0)
                {
                    objDTOExpenseMaster.decTotalAmount = Convert.ToDecimal(datExpense.Rows[0]["TotalAmount"]);
                    objDTOExpenseMaster.intCompanyID = Convert.ToInt32(datExpense.Rows[0]["CompanyID"]);
                    objDTOExpenseMaster.intCurrencyID = Convert.ToInt32(datExpense.Rows[0]["CurrencyID"]);
                    objDTOExpenseMaster.lngExpenseID = Convert.ToInt32(datExpense.Rows[0]["ExpenseID"]);
                    objDTOExpenseMaster.intOperationType = Convert.ToInt32(datExpense.Rows[0]["OperationTypeID"]);
                    objDTOExpenseMaster.lngReferenceID = Convert.ToInt32(datExpense.Rows[0]["ReferenceID"]);
                    objDTOExpenseMaster.intRowNumber = Convert.ToInt32(datExpense.Rows[0]["RowNumber"]);
                    objDTOExpenseMaster.strRemarks = Convert.ToString(datExpense.Rows[0]["Remarks"]);
                    //objDTOExpenseMaster.intCreditHeadID = Convert.ToInt32(datExpense.Rows[0]["CreditHeadID"]);
                    //objDTOExpenseMaster.intDebitHeadID = Convert.ToInt32(datExpense.Rows[0]["DebitHeadID"]);
                    //objDTOExpenseMaster.strVoucherNo = Convert.ToString(datExpense.Rows[0]["VoucherNo"]);
                    //objDTOExpenseMaster.strVoucherIds = Convert.ToString(datExpense.Rows[0]["VoucherIds"]);
                    objDTOExpenseMaster.blnPostToAccount = Convert.ToBoolean(datExpense.Rows[0]["IsDirectExpense"]);
                 //   objDTOExpenseMaster.strAccountDate = Convert.ToString(datExpense.Rows[0]["AccountDate"]);

                    blnRead = true;
                }
                GetExpenseDetails();
            }
            return blnRead;
        }

        private void GetExpenseDetails()
        {
            prmExpense = new ArrayList();
            prmExpense.Add(new SqlParameter("@Mode", 8));
            prmExpense.Add(new SqlParameter("@ExpenseID", objDTOExpenseMaster.lngExpenseID));

            objDTOExpenseMaster.objDTOExpenseDetails = new List<clsDTOExpenseDetails>();

            using (DataTable datExpense = objConnection.ExecuteDataTable("spInvExpense", prmExpense))
            {
                for(int i=0;i<datExpense.Rows.Count;i++)
                {
                    clsDTOExpenseDetails objExpenseDetails = new clsDTOExpenseDetails();

                    objExpenseDetails.intExpenseTypeID = Convert.ToInt32(datExpense.Rows[i]["ExpenseTypeID"]);
                    objExpenseDetails.decQuantity = Convert.ToDecimal(datExpense.Rows[i]["Quantity"]);
                    objExpenseDetails.decAmount = Convert.ToDecimal(datExpense.Rows[i]["Amount"]);
                    objExpenseDetails.strDescription = Convert.ToString(datExpense.Rows[i]["Remarks"]);
                    objExpenseDetails.intSerialNo = Convert.ToInt32(datExpense.Rows[i]["SerialNo"]);
                    objExpenseDetails.intCreatedBy = Convert.ToInt32(datExpense.Rows[i]["CreatedBy"]);
                    objDTOExpenseMaster.objDTOExpenseDetails.Add(objExpenseDetails);
                }
            }
        }

        public bool SaveExpenseInfo()
        {
            prmExpense = new ArrayList();
            prmExpense.Add(new SqlParameter("@Mode", objDTOExpenseMaster.lngExpenseID == 0 ? 2 : 3));
            prmExpense.Add(new SqlParameter("@ExpenseID", objDTOExpenseMaster.lngExpenseID));
            prmExpense.Add(new SqlParameter("@OperationTypeID", objDTOExpenseMaster.intOperationType));
            prmExpense.Add(new SqlParameter("@ReferenceID", objDTOExpenseMaster.lngReferenceID));
            prmExpense.Add(new SqlParameter("@CompanyID", objDTOExpenseMaster.intCompanyID));
            prmExpense.Add(new SqlParameter("@CurrencyID", objDTOExpenseMaster.intCurrencyID));
            prmExpense.Add(new SqlParameter("@TotalAmount", objDTOExpenseMaster.decTotalAmount));
            prmExpense.Add(new SqlParameter("@Remarks", objDTOExpenseMaster.strRemarks));
            prmExpense.Add(new SqlParameter("@PostingEnabled", objDTOExpenseMaster.intOperationType == (int)OperationType.StockTransfer || objDTOExpenseMaster.intOperationType == (int)OperationType.SalesQuotation | objDTOExpenseMaster.intOperationType == (int)OperationType.PurchaseQuotation || objDTOExpenseMaster.intOperationType == (int)OperationType.SalesOrder || objDTOExpenseMaster.intOperationType == (int)OperationType.PurchaseOrder ? false : ClsMainSettings.ExpensePosted));

            //prmExpense.Add(new SqlParameter("@CreditHeadID", objDTOExpenseMaster.intCreditHeadID));
            //prmExpense.Add(new SqlParameter("@DebitHeadID", objDTOExpenseMaster.intDebitHeadID));
            //if (objDTOExpenseMaster.strVoucherNo != null)
             //   prmExpense.Add(new SqlParameter("@VoucherNo", objDTOExpenseMaster.strVoucherNo));
           // if (objDTOExpenseMaster.strVoucherIds != null)
          //      prmExpense.Add(new SqlParameter("@VoucherIds", objDTOExpenseMaster.strVoucherIds));
            prmExpense.Add(new SqlParameter("@IsDirectExpense", objDTOExpenseMaster.blnPostToAccount));
           // if (!(objDTOExpenseMaster.strAccountDate).Equals(string.Empty))
          //      prmExpense.Add(new SqlParameter("@AccountDate", objDTOExpenseMaster.strAccountDate));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmExpense.Add(objParam);

            object objOutExpenseID = null;

            if (objConnection.ExecuteNonQueryWithTran("spInvExpense", prmExpense, out objOutExpenseID) != 0)
            {
                if ((int)objOutExpenseID > 0)
                {
                    objDTOExpenseMaster.lngExpenseID = (int)objOutExpenseID;
                    SaveExpenseDetails();
                    return true;
                }
            }
            return false;
        }

        private void SaveExpenseDetails()
        {
            foreach (clsDTOExpenseDetails objExpenseDetails in objDTOExpenseMaster.objDTOExpenseDetails)
            {
                prmExpense = new ArrayList();
                prmExpense.Add(new SqlParameter("@Mode", (objExpenseDetails.intSerialNo == 0 ? 9 : 10)));
                prmExpense.Add(new SqlParameter("@SerialNo", objExpenseDetails.intSerialNo));
                prmExpense.Add(new SqlParameter("@ExpenseID", objDTOExpenseMaster.lngExpenseID));
                prmExpense.Add(new SqlParameter("@ExpenseTypeID", objExpenseDetails.intExpenseTypeID));
                prmExpense.Add(new SqlParameter("@Quantity", objExpenseDetails.decQuantity));
                prmExpense.Add(new SqlParameter("@Amount", objExpenseDetails.decAmount));
                prmExpense.Add(new SqlParameter("@CreatedBy", objExpenseDetails.intCreatedBy));
                prmExpense.Add(new SqlParameter("@Remarks", objExpenseDetails.strDescription));
                prmExpense.Add(new SqlParameter("@VoucherID", objExpenseDetails.intVoucherID));
                prmExpense.Add(new SqlParameter("@PostingEnabled", objDTOExpenseMaster.intOperationType == (int)OperationType.StockTransfer || objDTOExpenseMaster.intOperationType == (int)OperationType.SalesQuotation | objDTOExpenseMaster.intOperationType == (int)OperationType.PurchaseQuotation || objDTOExpenseMaster.intOperationType == (int)OperationType.SalesOrder || objDTOExpenseMaster.intOperationType == (int)OperationType.PurchaseOrder ? false : ClsMainSettings.ExpensePosted));
                objConnection.ExecuteNonQueryWithTran("spInvExpense", prmExpense);
            }
        }

        public bool DeleteExpenseInfo()
        {
            prmExpense = new ArrayList();

            prmExpense.Add(new SqlParameter("@Mode", 4));
            prmExpense.Add(new SqlParameter("@ExpenseID", objDTOExpenseMaster.lngExpenseID));
            prmExpense.Add(new SqlParameter("@OperationTypeID", objDTOExpenseMaster.intOperationType));
            prmExpense.Add(new SqlParameter("@ReferenceID", objDTOExpenseMaster.lngReferenceID));
            prmExpense.Add(new SqlParameter("@PostingEnabled", ClsMainSettings.ExpensePosted));
            if (objConnection.ExecuteNonQuery("spInvExpense", prmExpense) != 0)
            {
                return true;
            }
            return false;
        }

        public bool DeleteExpenseDetails(int intSerialNo,decimal decTotalAmount)
        {
            prmExpense = new ArrayList();

            prmExpense.Add(new SqlParameter("@Mode", 11));
            prmExpense.Add(new SqlParameter("@ExpenseID", objDTOExpenseMaster.lngExpenseID));
            prmExpense.Add(new SqlParameter("@TotalAmount", decTotalAmount));
            prmExpense.Add(new SqlParameter("@SerialNo", intSerialNo));
            prmExpense.Add(new SqlParameter("@PostingEnabled", ClsMainSettings.ExpensePosted));

            if (objConnection.ExecuteNonQuery("spInvExpense", prmExpense) != 0)
                return true;
            else return false;
        }

        public int GetRecordCount(int PintOperationType, long PlngOperationID, int intRoleID, int intCompanyID, int intControlID,int intModuleID)
        {
            prmExpense = new ArrayList();
            prmExpense.Add(new SqlParameter("@Mode", 20));
            prmExpense.Add(new SqlParameter("@IsDirectExpense", objDTOExpenseMaster.blnPostToAccount));
            prmExpense.Add(new SqlParameter("@RoleID", intRoleID));
            prmExpense.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmExpense.Add(new SqlParameter("@ControlID", intControlID));
            prmExpense.Add(new SqlParameter("@ModuleID", intModuleID));
            return Convert.ToInt32(objConnection.ExecuteScalar("spInvExpense", prmExpense));
        }

        public DataTable GetAccounts()
        {
            prmExpense = new ArrayList();
            prmExpense.Add(new SqlParameter("@ParentID", objDTOExpenseMaster.intExpenseHeadID));
            prmExpense.Add(new SqlParameter("@CompID", objDTOExpenseMaster.intCompanyID));

            return objConnection.ExecuteDataSet("spAccGetAccounts", prmExpense).Tables[0];
        }
        public DataTable GetAccount(int intGrpCode)
        {
            prmExpense = new ArrayList();

            prmExpense.Add(new SqlParameter("@ParentID", intGrpCode));
            prmExpense.Add(new SqlParameter("@CompID", ClsCommonSettings.CompanyID));
            return objConnection.ExecuteDataTable("spAccGetAccounts", prmExpense);
        }
        public DataSet GetExpenseReport()
        {
            prmExpense = new ArrayList();
            prmExpense.Add(new SqlParameter("@Mode", 6));
            prmExpense.Add(new SqlParameter("@ExpenseID", objDTOExpenseMaster.lngExpenseID));

            return objConnection.ExecuteDataSet("spInvExpense", prmExpense);
        }

        public bool CheckSalesOrderEditable(int intMode, Int64 intReferenceID,string strSearchCondition)
        {
            bool blnEditable = false;
            prmExpense = new ArrayList();
            prmExpense.Add(new SqlParameter("@Mode", intMode));
            prmExpense.Add(new SqlParameter("@ReferenceID", intReferenceID));
            prmExpense.Add(new SqlParameter("@SearchCondition", strSearchCondition));

            using (SqlDataReader sdr = objConnection.ExecuteReader("spInvExpense", prmExpense, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    blnEditable = Convert.ToInt32(sdr[0]) > 0 ? true : false;
                }
                sdr.Close();
            }

            return blnEditable;
        }

        public double GetExpenseAmount(int intOperationType,Int64 intReferenceID,long intExpenseID)
        {
            double decExpenseAmount = 0;

            ArrayList prmExpenseAmount = new ArrayList();
            if (intExpenseID == 0)
            {
                prmExpenseAmount.Add(new SqlParameter("@Mode", 17));
                prmExpenseAmount.Add(new SqlParameter("@IsDirectExpense", true));
            }
            else
            {
                prmExpenseAmount.Add(new SqlParameter("@Mode", 22));
                prmExpenseAmount.Add(new SqlParameter("@ExpenseID", intExpenseID));
                prmExpenseAmount.Add(new SqlParameter("@IsDirectExpense", false));
            }
            prmExpenseAmount.Add(new SqlParameter("@OperationTypeID", intOperationType));
            prmExpenseAmount.Add(new SqlParameter("@ReferenceID", intReferenceID));
            

            DataTable datExpenseAmount = objConnection.ExecuteDataTable("spInvExpense", prmExpenseAmount);

            if (datExpenseAmount.Rows.Count > 0)
            {
                if (datExpenseAmount.Rows[0]["ExpenseAmount"] != DBNull.Value)
                {
                    decExpenseAmount = Convert.ToDouble(datExpenseAmount.Rows[0]["ExpenseAmount"]);
                }
                if (decExpenseAmount >= 0)
                    return decExpenseAmount;
            }

            return decExpenseAmount;
        }

        public double GetPaidAmount(int intOperationType, Int64 intReferenceID)
        {
            double  decPaidAmount = 0;
            ArrayList prmPaidAmount = new ArrayList();
            prmPaidAmount.Add(new SqlParameter("@Mode", 21));
            prmPaidAmount.Add(new SqlParameter("@OperationTypeID", intOperationType));
            prmPaidAmount.Add(new SqlParameter("@ReferenceID", intReferenceID));
            DataTable datPaidAmount = objConnection.ExecuteDataTable("spInvExpense", prmPaidAmount);

            if (datPaidAmount.Rows.Count > 0)
            {
                if (datPaidAmount.Rows[0]["TotalAmount"] != DBNull.Value)
                    decPaidAmount = datPaidAmount.Rows[0]["TotalAmount"].ToDouble();
            }
            return decPaidAmount;
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));            
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return objConnection.ExecuteDataTable("STPermissionSettings", parameters);
        }

        public DataTable GetOperationTypeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return objConnection.ExecuteDataTable("STPermissionSettings", parameters);
        }
    }
}
