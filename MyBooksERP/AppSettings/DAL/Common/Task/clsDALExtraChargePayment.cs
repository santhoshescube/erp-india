﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALExtraChargePayment
    {
        ArrayList prmExtraChargePayment;

        public clsDTOExtraChargePayment objDTOExtraChargePayment { get; set; }
        public DataLayer objConnection { get; set; }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataTable GetCompanyByPermission(int RoleID, int MenuID, int ControlID)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 4));
            prmExtraChargePayment.Add(new SqlParameter("@RoleID", RoleID));
            prmExtraChargePayment.Add(new SqlParameter("@MenuID", MenuID));
            prmExtraChargePayment.Add(new SqlParameter("@ControlID", ControlID));
            return objConnection.ExecuteDataTable("STPermissionSettings", prmExtraChargePayment);
        }

        public bool SaveExtraChargePayment(DataTable datTemp)
        {
            if (objDTOExtraChargePayment.lngExtraChargePaymentID > 0)
                DeleteExtraChargePaymentDetails();

            prmExtraChargePayment = new ArrayList();
            if (objDTOExtraChargePayment.lngExtraChargePaymentID == 0)
                prmExtraChargePayment.Add(new SqlParameter("@Mode", 1)); // Insert
            else
                prmExtraChargePayment.Add(new SqlParameter("@Mode", 3)); // Update

            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargePaymentID", objDTOExtraChargePayment.lngExtraChargePaymentID));
            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargePaymentNo", objDTOExtraChargePayment.strExtraChargePaymentNo));
            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargePaymentDate", objDTOExtraChargePayment.dtExtraChargePaymentDate));
            prmExtraChargePayment.Add(new SqlParameter("@CompanyID", objDTOExtraChargePayment.intCompanyID));
            prmExtraChargePayment.Add(new SqlParameter("@CurrencyID", objDTOExtraChargePayment.intCurrencyID));
            prmExtraChargePayment.Add(new SqlParameter("@OperationTypeID", objDTOExtraChargePayment.intOperationTypeID));
            prmExtraChargePayment.Add(new SqlParameter("@ReferenceID", objDTOExtraChargePayment.lngReferenceID));
            prmExtraChargePayment.Add(new SqlParameter("@VendorID", objDTOExtraChargePayment.intVendorID));
            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargeID", objDTOExtraChargePayment.lngExtraChargeID));
            prmExtraChargePayment.Add(new SqlParameter("@TotalAmount", objDTOExtraChargePayment.decTotalAmount));
            prmExtraChargePayment.Add(new SqlParameter("@PaymentModeID", objDTOExtraChargePayment.intPaymentModeID));
            prmExtraChargePayment.Add(new SqlParameter("@AccountID", objDTOExtraChargePayment.intAccountID));
            prmExtraChargePayment.Add(new SqlParameter("@ChNo", objDTOExtraChargePayment.strChNo));
            prmExtraChargePayment.Add(new SqlParameter("@ChDate", objDTOExtraChargePayment.dtChDate));
            prmExtraChargePayment.Add(new SqlParameter("@Description", objDTOExtraChargePayment.strDescription));
            prmExtraChargePayment.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmExtraChargePayment.Add(objParam);

            object OutExtraChargePaymentID = null;

            if (objConnection.ExecuteNonQuery("spExtraChargePayment", prmExtraChargePayment, out OutExtraChargePaymentID) != 0)
            {
                if (OutExtraChargePaymentID.ToInt64() > 0)
                {
                    objDTOExtraChargePayment.lngExtraChargePaymentID = OutExtraChargePaymentID.ToInt64();
                    if (datTemp != null) // Save Extra Charge Payment Details
                    {
                        if (datTemp.Rows.Count > 0)
                            SaveExtraChargePaymentDetails(datTemp);
                    }
                    return true;
                }
            }
            return false;
        }

        private bool SaveExtraChargePaymentDetails(DataTable datTemp)
        {
            int count = 0;
            for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
            {
                prmExtraChargePayment = new ArrayList();
                prmExtraChargePayment.Add(new SqlParameter("@Mode", 2));
                prmExtraChargePayment.Add(new SqlParameter("@TempSerialNo", ++count));
                prmExtraChargePayment.Add(new SqlParameter("@ExtraChargePaymentID", objDTOExtraChargePayment.lngExtraChargePaymentID));
                prmExtraChargePayment.Add(new SqlParameter("@ExpenseTypeID", datTemp.Rows[iCounter]["ExpenseTypeID"].ToInt32()));
                prmExtraChargePayment.Add(new SqlParameter("@Amount", datTemp.Rows[iCounter]["Amount"].ToDecimal()));
                objConnection.ExecuteNonQuery("spExtraChargePayment", prmExtraChargePayment);
            }
            return true;
        }

        public bool DeleteExtraChargePayment()
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 4));
            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargePaymentID", objDTOExtraChargePayment.lngExtraChargePaymentID));
            objConnection.ExecuteNonQuery("spExtraChargePayment", prmExtraChargePayment);
            return true;
        }

        public bool DeleteExtraChargePaymentDetails()
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 5));
            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargePaymentID", objDTOExtraChargePayment.lngExtraChargePaymentID));
            objConnection.ExecuteNonQuery("spExtraChargePayment", prmExtraChargePayment);
            return true;
        }

        public DataTable DisplayExtraChargePaymentMaster(long lngRowNum)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 6));
            prmExtraChargePayment.Add(new SqlParameter("@RowNum", lngRowNum));
            DataTable datTemp = objConnection.ExecuteDataTable("spExtraChargePayment", prmExtraChargePayment);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    objDTOExtraChargePayment.lngExtraChargePaymentID = datTemp.Rows[0]["ExtraChargePaymentID"].ToInt64();
                    objDTOExtraChargePayment.strExtraChargePaymentNo = datTemp.Rows[0]["ExtraChargePaymentNo"].ToString();
                    objDTOExtraChargePayment.dtExtraChargePaymentDate = datTemp.Rows[0]["ExtraChargePaymentDate"].ToDateTime();
                    objDTOExtraChargePayment.intCompanyID = datTemp.Rows[0]["CompanyID"].ToInt32();
                    objDTOExtraChargePayment.intCurrencyID = datTemp.Rows[0]["CurrencyID"].ToInt32();
                    objDTOExtraChargePayment.intOperationTypeID = datTemp.Rows[0]["OperationTypeID"].ToInt32();
                    objDTOExtraChargePayment.lngReferenceID = datTemp.Rows[0]["ReferenceID"].ToInt64();
                    objDTOExtraChargePayment.intVendorID = datTemp.Rows[0]["VendorID"].ToInt32();
                    objDTOExtraChargePayment.lngExtraChargeID = datTemp.Rows[0]["ExtraChargeID"].ToInt64();
                    objDTOExtraChargePayment.decTotalAmount = datTemp.Rows[0]["TotalAmount"].ToDecimal();
                    objDTOExtraChargePayment.intPaymentModeID = datTemp.Rows[0]["PaymentModeID"].ToInt32();
                    objDTOExtraChargePayment.intAccountID = datTemp.Rows[0]["AccountID"].ToInt32();
                    objDTOExtraChargePayment.strChNo = datTemp.Rows[0]["ChNo"].ToString();
                    objDTOExtraChargePayment.dtChDate = datTemp.Rows[0]["ChDate"].ToDateTime();
                    objDTOExtraChargePayment.strDescription = datTemp.Rows[0]["Description"].ToString();

                    datTemp = null;
                    datTemp = getExtraChargePaymentDetails(objDTOExtraChargePayment.lngExtraChargePaymentID);
                }
            }
            return datTemp;
        }

        public string getExtraChargePaymentNo(int intCompanyID)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 7));
            prmExtraChargePayment.Add(new SqlParameter("@CompanyID", intCompanyID));
            return Convert.ToString(objConnection.ExecuteScalar("spExtraChargePayment", prmExtraChargePayment));
        }

        public int getCurrency(long lngExtrChID)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 8));
            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargeID", lngExtrChID));
            return Convert.ToInt32(objConnection.ExecuteScalar("spExtraChargePayment", prmExtraChargePayment));
        }

        public DataTable getReferenceNo(int intMode, int intCompanyID)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", intMode)); // 9 - PO & 10 - PI & 24 - SO & 25 - SI
            prmExtraChargePayment.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objConnection.ExecuteDataTable("spExtraChargePayment", prmExtraChargePayment);
        }

        public long getRecordCount()
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 11));
            return Convert.ToInt64(objConnection.ExecuteScalar("spExtraChargePayment", prmExtraChargePayment));
        }

        public DataTable getExtraChargePaymentDetails(long lngExtraChargeID)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 12));
            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargePaymentID", lngExtraChargeID));
            return objConnection.ExecuteDataTable("spExtraChargePayment", prmExtraChargePayment);
        }

        public string CheckExtraChargePaymentNoAutoGenerate(int intCompanyID)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 13));
            prmExtraChargePayment.Add(new SqlParameter("@CompanyID", intCompanyID));
            return Convert.ToString(objConnection.ExecuteScalar("spExtraChargePayment", prmExtraChargePayment));
        }

        public bool CheckExtraChargePaymentNoDuplication(int intCompanyID, long lngExChID, string strExChNo)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 14));
            prmExtraChargePayment.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargePaymentID", lngExChID));
            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargePaymentNo", strExChNo.Trim()));
            return Convert.ToBoolean(objConnection.ExecuteScalar("spExtraChargePayment", prmExtraChargePayment));
        }

        public DataTable getExtraChargeNo(int intCompanyID, int intOpTypeID, long lngRefID, int intVendorID)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 15));
            prmExtraChargePayment.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmExtraChargePayment.Add(new SqlParameter("@OperationTypeID", intOpTypeID));
            prmExtraChargePayment.Add(new SqlParameter("@ReferenceID", lngRefID));
            prmExtraChargePayment.Add(new SqlParameter("@VendorID", intVendorID));
            return objConnection.ExecuteDataTable("spExtraChargePayment", prmExtraChargePayment);
        }

        public DataTable getShipmentVendor(int intCompanyID, int intOpTypeID, long lngRefID)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 16));
            prmExtraChargePayment.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmExtraChargePayment.Add(new SqlParameter("@OperationTypeID", intOpTypeID));
            prmExtraChargePayment.Add(new SqlParameter("@ReferenceID", lngRefID));
            return objConnection.ExecuteDataTable("spExtraChargePayment", prmExtraChargePayment);
        }

        public DataTable getExtraChargeDetails(int intVendorID, long lngExtraChargeID)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 17));
            prmExtraChargePayment.Add(new SqlParameter("@VendorID", intVendorID));
            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargeID", lngExtraChargeID));
            return objConnection.ExecuteDataTable("spExtraChargePayment", prmExtraChargePayment);
        }

        public decimal getCurrentBalance(long lngExtraChargeID, int intExpenseTypeID, int intVendorID)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 18));
            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargeID", lngExtraChargeID));
            prmExtraChargePayment.Add(new SqlParameter("@ExpenseTypeID", intExpenseTypeID));
            prmExtraChargePayment.Add(new SqlParameter("@VendorID", intVendorID));
            return Convert.ToDecimal(objConnection.ExecuteScalar("spExtraChargePayment", prmExtraChargePayment));
        }

        public string ConvertToWord(string strAmount, int intCurrencyID)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 19));
            prmExtraChargePayment.Add(new SqlParameter("@TempTotalAmount", strAmount));
            prmExtraChargePayment.Add(new SqlParameter("@CurrencyID", intCurrencyID));
            return Convert.ToString(objConnection.ExecuteScalar("spExtraChargePayment", prmExtraChargePayment));
        }

        public DataSet DisplayExtraChargePayment(long ExtraChargePaymentID)
        {
            return new DataLayer().ExecuteDataSet("spExtraChargePayment", new List<SqlParameter>{
                new SqlParameter("@Mode",20),
                new SqlParameter("@ExtraChargePaymentID",ExtraChargePaymentID)
            });
        }

        public long getRecordCountForSpecificValue(int intCompID, int OpTypeID, long lngRefID, long lngExChID)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 21));
            prmExtraChargePayment.Add(new SqlParameter("@CompanyID", intCompID));
            prmExtraChargePayment.Add(new SqlParameter("@OperationTypeID", OpTypeID));
            prmExtraChargePayment.Add(new SqlParameter("@ReferenceID", lngRefID));
            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargeID", lngExChID));
            return Convert.ToInt64(objConnection.ExecuteScalar("spExtraChargePayment", prmExtraChargePayment));
        }

        public DataTable DisplayExtraChargePaymentMasterForSpecificValue(long lngRowNum, int intCompID, int OpTypeID, long lngRefID, long lngExChID)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 22));
            prmExtraChargePayment.Add(new SqlParameter("@RowNum", lngRowNum));
            prmExtraChargePayment.Add(new SqlParameter("@CompanyID", intCompID));
            prmExtraChargePayment.Add(new SqlParameter("@OperationTypeID", OpTypeID));
            prmExtraChargePayment.Add(new SqlParameter("@ReferenceID", lngRefID));
            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargeID", lngExChID));
            DataTable datTemp = objConnection.ExecuteDataTable("spExtraChargePayment", prmExtraChargePayment);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    objDTOExtraChargePayment.lngExtraChargePaymentID = datTemp.Rows[0]["ExtraChargePaymentID"].ToInt64();
                    objDTOExtraChargePayment.strExtraChargePaymentNo = datTemp.Rows[0]["ExtraChargePaymentNo"].ToString();
                    objDTOExtraChargePayment.dtExtraChargePaymentDate = datTemp.Rows[0]["ExtraChargePaymentDate"].ToDateTime();
                    objDTOExtraChargePayment.intCompanyID = datTemp.Rows[0]["CompanyID"].ToInt32();
                    objDTOExtraChargePayment.intCurrencyID = datTemp.Rows[0]["CurrencyID"].ToInt32();
                    objDTOExtraChargePayment.intOperationTypeID = datTemp.Rows[0]["OperationTypeID"].ToInt32();
                    objDTOExtraChargePayment.lngReferenceID = datTemp.Rows[0]["ReferenceID"].ToInt64();
                    objDTOExtraChargePayment.intVendorID = datTemp.Rows[0]["VendorID"].ToInt32();
                    objDTOExtraChargePayment.lngExtraChargeID = datTemp.Rows[0]["ExtraChargeID"].ToInt64();
                    objDTOExtraChargePayment.decTotalAmount = datTemp.Rows[0]["TotalAmount"].ToDecimal();
                    objDTOExtraChargePayment.intPaymentModeID = datTemp.Rows[0]["PaymentModeID"].ToInt32();
                    objDTOExtraChargePayment.intAccountID = datTemp.Rows[0]["AccountID"].ToInt32();
                    objDTOExtraChargePayment.strChNo = datTemp.Rows[0]["ChNo"].ToString();
                    objDTOExtraChargePayment.dtChDate = datTemp.Rows[0]["ChDate"].ToDateTime();
                    objDTOExtraChargePayment.strDescription = datTemp.Rows[0]["Description"].ToString();

                    datTemp = null;
                    datTemp = getExtraChargePaymentDetails(objDTOExtraChargePayment.lngExtraChargePaymentID);
                }
            }
            return datTemp;
        }

        public DataTable getShipmentVendorWithExtraChargeNo(int intCompanyID, int intOpTypeID, long lngRefID, long lngExChID)
        {
            prmExtraChargePayment = new ArrayList();
            prmExtraChargePayment.Add(new SqlParameter("@Mode", 23));
            prmExtraChargePayment.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmExtraChargePayment.Add(new SqlParameter("@OperationTypeID", intOpTypeID));
            prmExtraChargePayment.Add(new SqlParameter("@ReferenceID", lngRefID));
            prmExtraChargePayment.Add(new SqlParameter("@ExtraChargeID", lngExChID));
            return objConnection.ExecuteDataTable("spExtraChargePayment", prmExtraChargePayment);
        }
    }
}
