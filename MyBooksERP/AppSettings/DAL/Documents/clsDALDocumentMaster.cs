﻿
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System;
using System.Collections.Generic;

namespace MyBooksERP
{

    /*=================================================
  Description :	     <Document Transactions>
  Modified By :		 <Laxmi>
  Modified Date :      <13 Apr 2012>
  ================================================*/


    public class clsDALDocumentMaster
    {
        public clsDTODocumentMaster objClsDTODocumentMaster { get; set; }
        public DataLayer objConnection { get; set; }

        ArrayList prmDocuments;
        string STDocumentMasterTransactions = "spDocuments";

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool GetDocumentInfo()
        {
            bool blnRead = false;

            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 3));
            prmDocuments.Add(new SqlParameter("@RowNumber", objClsDTODocumentMaster.intRowNumber));
            prmDocuments.Add(new SqlParameter("@DocumentID", objClsDTODocumentMaster.intDocumentID));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", objClsDTODocumentMaster.intOperationTypeID));
            prmDocuments.Add(new SqlParameter("@ReferenceID", objClsDTODocumentMaster.lngReferenceID));

            using (SqlDataReader sdr = objConnection.ExecuteReader(STDocumentMasterTransactions, prmDocuments, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    objClsDTODocumentMaster.intDocumentID = sdr["DocumentID"].ToInt32();
                    objClsDTODocumentMaster.intDocumentTypeID = sdr["DocumentTypeID"].ToInt32();
                    objClsDTODocumentMaster.intOperationTypeID = sdr["OperationTypeID"].ToInt32();
                    objClsDTODocumentMaster.lngReferenceID = sdr["ReferenceID"].ToInt64();
                    objClsDTODocumentMaster.strReferenceNumber = sdr["ReferenceNumber"].ToStringCustom();
                    objClsDTODocumentMaster.strExpiryDate = sdr["ExpiryDate"].ToStringCustom();
                    objClsDTODocumentMaster.intStatusID = sdr["StatusID"].ToInt32();
                    objClsDTODocumentMaster.intRowNumber = sdr["RowNumber"].ToInt32();
                    objClsDTODocumentMaster.strReceiptIssueDate = sdr["ReceiptIssueDate"].ToStringCustom();
                    objClsDTODocumentMaster.intCustodian =sdr["Custodian"].ToInt32();
                    objClsDTODocumentMaster.intDocumentStatusID = sdr["DocumentStatusID"].ToInt32();
                    objClsDTODocumentMaster.intIssueReasonID = sdr["IssueReasonID"].ToInt32();
                    objClsDTODocumentMaster.strExpectedReturnDate = sdr["ExpectedReturnDate"].ToStringCustom();
                    objClsDTODocumentMaster.intBinID = sdr["BinID"].ToInt32();
                    objClsDTODocumentMaster.strRemarks = sdr["Remarks"].ToStringCustom();
                    objClsDTODocumentMaster.intOrderNo = sdr["OrderNumber"].ToInt32();

                    //objClsDTODocumentMaster.strOperationNumber = Convert.ToString(sdr["OperationNumber"]);
                    //if (sdr["VendorID"]!=DBNull.Value)
                    //    objClsDTODocumentMaster.intVendorID = Convert.ToInt32(sdr["VendorID"]);
                    blnRead = true;
                }
                sdr.Close();
            }

            return blnRead;
        }

        public int SaveDocumentInfo()
        {
            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 2));
            prmDocuments.Add(new SqlParameter("@DocumentID", objClsDTODocumentMaster.intDocumentID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", objClsDTODocumentMaster.intDocumentTypeID));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", objClsDTODocumentMaster.intOperationTypeID));
            prmDocuments.Add(new SqlParameter("@ReferenceID", objClsDTODocumentMaster.lngReferenceID));
            prmDocuments.Add(new SqlParameter("@ReferenceNumber", objClsDTODocumentMaster.strReferenceNumber));
            prmDocuments.Add(new SqlParameter("@IsReceipted", objClsDTODocumentMaster.blnIsReceipted));
            prmDocuments.Add(new SqlParameter("@CreatedBy", objClsDTODocumentMaster.intCreatedBy));
            prmDocuments.Add(new SqlParameter("@StatusID", objClsDTODocumentMaster.intStatusID));
            prmDocuments.Add(new SqlParameter("@OrderNo", objClsDTODocumentMaster.intOrderNo));
            prmDocuments.Add(new SqlParameter("@ReceiptIssueDate", objClsDTODocumentMaster.strReceiptIssueDate));
            prmDocuments.Add(new SqlParameter("@Custodian", objClsDTODocumentMaster.intCustodian));
            prmDocuments.Add(new SqlParameter("@DocumentStatusID", objClsDTODocumentMaster.intDocumentStatusID));
            prmDocuments.Add(new SqlParameter("@BinID", objClsDTODocumentMaster.intBinID));
            prmDocuments.Add(new SqlParameter("@IssueReasonID", objClsDTODocumentMaster.intIssueReasonID));
            prmDocuments.Add(new SqlParameter("@ExpectedReturnDate", objClsDTODocumentMaster.strExpectedReturnDate));
            prmDocuments.Add(new SqlParameter("@Remarks", objClsDTODocumentMaster.strRemarks));
            if (objClsDTODocumentMaster.strExpiryDate != "")
                prmDocuments.Add(new SqlParameter("@ExpiryDate", objClsDTODocumentMaster.strExpiryDate));

            SqlParameter prmReturnValue = new SqlParameter("ReturnVal", SqlDbType.Int);
            prmReturnValue.Direction = ParameterDirection.ReturnValue;
            prmDocuments.Add(prmReturnValue);

            object objOutDocumentID = null;

            if (objConnection.ExecuteNonQuery(STDocumentMasterTransactions, prmDocuments, out objOutDocumentID) != 0)
            {
                objClsDTODocumentMaster.intDocumentID = Convert.ToInt32(objOutDocumentID);
                return objClsDTODocumentMaster.intDocumentID;

            }
            else
                return 0;

        }

        public int SaveAlert(DataTable datAlert)// insert
        {
            ArrayList parameters;
            object iOutAletID = 0;

            //try
            //{
            parameters = new ArrayList();
            foreach (DataRow dtAlertRow in datAlert.Rows)
            {

                parameters.Add(new SqlParameter("@Mode", 18));//insert
                parameters.Add(new SqlParameter("@AlertUserSettingID", dtAlertRow["AlertUserSettingID"]));
                parameters.Add(new SqlParameter("@ReferenceID", dtAlertRow["ReferenceID"]));
                parameters.Add(new SqlParameter("@StartDate", dtAlertRow["StartDate"]));
                parameters.Add(new SqlParameter("@AlertMessage", dtAlertRow["AlertMessage"]));
                parameters.Add(new SqlParameter("@Status", dtAlertRow["Status"]));

                SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
                objParam.Direction = ParameterDirection.ReturnValue;
                parameters.Add(objParam);

                if (this.objConnection.ExecuteNonQuery("spAlert", parameters, out iOutAletID) != 0)
                {
                    if (Convert.ToInt32(iOutAletID) > 0)
                    {
                        dtAlertRow["AlertID"] = iOutAletID;
                    }
                }
                else
                    return 0;
                parameters.Clear();
            }
            return Convert.ToInt32(iOutAletID);
            // }
            // catch (Exception ex) { throw ex; }
        }


        public bool DeleteDocumentInfo()
        {
            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 8));

            prmDocuments.Add(new SqlParameter("@DocumentID", objClsDTODocumentMaster.intDocumentID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", objClsDTODocumentMaster.intDocumentTypeID));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", objClsDTODocumentMaster.intOperationTypeID));
            prmDocuments.Add(new SqlParameter("@ReferenceID", objClsDTODocumentMaster.lngReferenceID));

            if (objConnection.ExecuteNonQuery(STDocumentMasterTransactions, prmDocuments) != 0)
                return true;
            else return false;
        }

        public bool CheckDuplicateDocument(int intVendorID)
        {
            bool blnExists = false;

            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 5));
            prmDocuments.Add(new SqlParameter("@DocumentID", objClsDTODocumentMaster.intDocumentID));
            prmDocuments.Add(new SqlParameter("@ReferenceNumber", objClsDTODocumentMaster.strReferenceNumber));
            prmDocuments.Add(new SqlParameter("@VendorID", intVendorID));
            using (SqlDataReader sdr = objConnection.ExecuteReader(STDocumentMasterTransactions, prmDocuments, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    blnExists = (Convert.ToInt32(sdr[0]) == 0 ? false : true);
                }
                sdr.Close();
            }

            return blnExists;
        }

        public bool CheckDuplicateDocumentDetailsForSameVendor(int intReferenceID, int intDocumentType)
        {
            bool blnExists = false;

            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 11));
            prmDocuments.Add(new SqlParameter("@ReferenceID", intReferenceID));
            //prmDocuments.Add(new SqlParameter("@CurrentDate",ClsCommonSettings.GetServerDate()));
            prmDocuments.Add(new SqlParameter("@DocumentID", objClsDTODocumentMaster.intDocumentID));
            prmDocuments.Add(new SqlParameter("@ReferenceNumber", objClsDTODocumentMaster.strReferenceNumber));
            using (SqlDataReader sdr = objConnection.ExecuteReader(STDocumentMasterTransactions, prmDocuments, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    blnExists = (Convert.ToInt32(sdr[0]) == 0 ? false : true);
                }
                sdr.Close();
            }

            return blnExists;
        }

        public int GetDocumentCount(int PintOperationType, long intReferenceID)
        {
            int intRecordCount = 0;

            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 1));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", PintOperationType));
            prmDocuments.Add(new SqlParameter("@ReferenceID", intReferenceID));

            using (SqlDataReader sdr = objConnection.ExecuteReader(STDocumentMasterTransactions, prmDocuments, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    intRecordCount = Convert.ToInt32(sdr[0]);
                }
                sdr.Close();
            }

            return intRecordCount;
        }

        public DataSet GetDocumentReport()
        {
            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 7));
            prmDocuments.Add(new SqlParameter("@DocumentID", objClsDTODocumentMaster.intDocumentID));

            return objConnection.ExecuteDataSet(STDocumentMasterTransactions, prmDocuments);
        }

        public int GetOperationStatus(int PintOperationType, long PlngOperationID)
        {
            int intOperationStatus = 0;

            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 8));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", PintOperationType));
            prmDocuments.Add(new SqlParameter("@ReferenceID", PlngOperationID));

            using (SqlDataReader sdr = objConnection.ExecuteReader(STDocumentMasterTransactions, prmDocuments, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    intOperationStatus = Convert.ToInt32(sdr[0]);
                }
                sdr.Close();
            }

            return intOperationStatus;
        }

        public bool GetExistingDocumentInfo(int PintVendorReferenceId, int intDocumentTypeID)
        {
            bool blnRead = false;

            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 9));
            prmDocuments.Add(new SqlParameter("@RowNumber", 1));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", intDocumentTypeID));
            prmDocuments.Add(new SqlParameter("@ReferenceID", PintVendorReferenceId));

            using (SqlDataReader sdr = objConnection.ExecuteReader(STDocumentMasterTransactions, prmDocuments, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    objClsDTODocumentMaster.intDocumentID = sdr["DocumentID"].ToInt32();
                    objClsDTODocumentMaster.intDocumentTypeID = sdr["DocumentTypeID"].ToInt32();
                    objClsDTODocumentMaster.intRowNumber =sdr["RowNumber"].ToInt32();
                    objClsDTODocumentMaster.intStatusID = sdr["StatusID"].ToInt32();
                    objClsDTODocumentMaster.lngReferenceID = sdr["ReferenceID"].ToInt64();
                    objClsDTODocumentMaster.strExpiryDate = sdr["ExpiryDate"].ToStringCustom();
                    objClsDTODocumentMaster.strReceiptIssueDate = sdr["IssuedDate"].ToStringCustom();
                    objClsDTODocumentMaster.strReferenceNumber = sdr["ReferenceNumber"].ToStringCustom();
                    objClsDTODocumentMaster.strRemarks = sdr["Remarks"].ToStringCustom();
                    blnRead = true;
                }
                sdr.Close();
            }

            return blnRead;

        }

        public bool GetStatusAndOrderNo()
        {
            bool blnRead = false;

            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 4));
            prmDocuments.Add(new SqlParameter("@DocumentID", objClsDTODocumentMaster.intDocumentID));

            using (SqlDataReader sdr = objConnection.ExecuteReader(STDocumentMasterTransactions, prmDocuments, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    objClsDTODocumentMaster.intStatusID = Convert.ToInt32(sdr["StatusID"]);
                    objClsDTODocumentMaster.intOrderNo = Convert.ToInt32(sdr["OrderNo"]);
                    blnRead = true;
                }
                sdr.Close();
            }
            return blnRead;
        }

        public DataTable GetStockDocuments()
        {
            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 6));
            prmDocuments.Add(new SqlParameter("@StatusID", objClsDTODocumentMaster.intStatusID));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", objClsDTODocumentMaster.intOperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", objClsDTODocumentMaster.intDocumentTypeID));

            return objConnection.ExecuteDataTable(STDocumentMasterTransactions, prmDocuments);
        }

        public DataTable GetStockDocumentDetails()
        {
            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 7));
            prmDocuments.Add(new SqlParameter("@DocumentID", objClsDTODocumentMaster.intDocumentID));
            return objConnection.ExecuteDataTable(STDocumentMasterTransactions, prmDocuments);
        }


        public bool DeleteSingleDocument()
        {
            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 9));

            prmDocuments.Add(new SqlParameter("@DocumentID", objClsDTODocumentMaster.intDocumentID));
            prmDocuments.Add(new SqlParameter("@StatusID", objClsDTODocumentMaster.intStatusID));
            prmDocuments.Add(new SqlParameter("@OrderNo", objClsDTODocumentMaster.intOrderNo));

            if (objConnection.ExecuteNonQuery(STDocumentMasterTransactions, prmDocuments) != 0)
                return true;
            else return false;
        }

        public long CheckAlertSetting()
        {
            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 10));
            return Convert.ToInt64(this.objConnection.ExecuteScalar(STDocumentMasterTransactions, prmDocuments));
        }

        public DataTable GetAlertSetting()
        {
            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 11));
            return objConnection.ExecuteDataTable(STDocumentMasterTransactions, prmDocuments);
        }

        public DataTable GetAlertDocs()
        {
            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 12));
            prmDocuments.Add(new SqlParameter("@DocumentID", objClsDTODocumentMaster.intDocumentID));
            return objConnection.ExecuteDataTable(STDocumentMasterTransactions, prmDocuments);

        }
        public int GetCurStatus()
        {
            int iOutStatus = 0;

            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 15));
            prmDocuments.Add(new SqlParameter("@DocumentID", objClsDTODocumentMaster.intDocumentID));

            using (SqlDataReader sdr = objConnection.ExecuteReader(STDocumentMasterTransactions, prmDocuments, CommandBehavior.CloseConnection))
            {
                if (sdr.Read())
                {
                    iOutStatus = Convert.ToInt32(sdr[0]);
                }
                sdr.Close();
            }

            return iOutStatus;
        }
        public bool CheckDocumentExists()
        {
            prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 16));
            prmDocuments.Add(new SqlParameter("@DocumentID", objClsDTODocumentMaster.intDocumentID));
            prmDocuments.Add(new SqlParameter("@ReferenceNumber", objClsDTODocumentMaster.strReferenceNumber));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", objClsDTODocumentMaster.intDocumentTypeID));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", objClsDTODocumentMaster.intOperationTypeID));
            prmDocuments.Add(new SqlParameter("@ReferenceID", objClsDTODocumentMaster.lngReferenceID));

            if (Convert.ToInt32(objConnection.ExecuteScalar(STDocumentMasterTransactions, prmDocuments)) != 0)
            {
                return true;
            }
            else return false;

        }
        /// <summary>
        /// Delete Alerts on Document deletion or Updation without entering Expiry date
        /// </summary>
        /// <param name="ReferenceID">Document ID</param>
        /// <param name="AlertsettingID"></param>
        /// <returns>bool</returns>
        public void DeleteAlert(int ReferenceID, int AlertsettingID)
        {
            int Result = objConnection.ExecuteNonQuery("spCommonUtility", new List<SqlParameter>{
                new SqlParameter("@Mode","2"),
                new SqlParameter("@ReferenceID",ReferenceID),
                new SqlParameter("@OperationTypeID",AlertsettingID)
            });
        }
    }
}
