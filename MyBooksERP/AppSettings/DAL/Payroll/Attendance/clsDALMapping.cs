﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using DTO;

namespace DAL
{
    public class clsDALMapping
    {
        ArrayList prmMapping; // for setting Sql parameters
        ClsCommonUtility  MObjClsCommonUtility;
        DataLayer MobjDataLayer;

        public clsDTOMapping PobjclsDTOMapping { get; set; } // DTO Company Information Property

        public clsDALMapping(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MObjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }

        public DataTable GetAllData()
        {
            prmMapping  = new ArrayList();

            prmMapping.Add(new SqlParameter("@Mode", "GAD"));

            return MobjDataLayer.ExecuteDataTable("spPayAttendanceMappingWizard", prmMapping);
        }

        public DataTable GetMappingEmployeeList()
        {
            prmMapping  = new ArrayList();

            prmMapping.Add(new SqlParameter("@Mode", "GAMD"));
            prmMapping.Add(new SqlParameter("@TemplateName", PobjclsDTOMapping.TemplateName));
            return MobjDataLayer.ExecuteDataTable("spPayAttendanceMappingWizard", prmMapping);
        }

        public void Execute(string sQuery)
        {
             MobjDataLayer.ExecuteQuery(sQuery);
        }

        public bool IsExists()
        {
            prmMapping = new ArrayList();

            prmMapping.Add(new SqlParameter("@Mode", "GTN"));
            prmMapping.Add(new SqlParameter("@TemplateName", PobjclsDTOMapping.TemplateName));

            return (Convert.ToInt32(MobjDataLayer.ExecuteScalar("spPayAttendanceMappingWizard", prmMapping)) > 0 ? true : false);
        }

        public int SaveTemplate()
        {
            prmMapping = new ArrayList();
            prmMapping.Add(new SqlParameter("@Mode", "I"));
            prmMapping.Add(new SqlParameter("@TemplateName", PobjclsDTOMapping.TemplateName ));
            prmMapping.Add(new SqlParameter("@IsHeaderExists", PobjclsDTOMapping.HeaderExists));
            prmMapping.Add(new SqlParameter("@Punchingcount", PobjclsDTOMapping.Punchingcount));
            prmMapping.Add(new SqlParameter("@Dateformat", PobjclsDTOMapping.Dateformat ));
            prmMapping.Add(new SqlParameter("@IsTimewithdate", PobjclsDTOMapping.Timewithdate));
            prmMapping.Add(new SqlParameter("@FileTypeID", PobjclsDTOMapping.intFileTypeID));

            return Convert.ToInt32(MobjDataLayer.ExecuteScalar("spPayAttendanceMappingWizard", prmMapping));
        }

        public int SaveTemplateDetails()
        {
            prmMapping = new ArrayList();
            prmMapping.Add(new SqlParameter("@Mode", "ID"));
            prmMapping.Add(new SqlParameter("@TemplateMasterID", PobjclsDTOMapping.TemplateMasterID));
            prmMapping.Add(new SqlParameter("@Templatefield", PobjclsDTOMapping.Templatefield));
            prmMapping.Add(new SqlParameter("@Actualfield", PobjclsDTOMapping.Actualfield));

            return Convert.ToInt32(MobjDataLayer.ExecuteScalar("spPayAttendanceMappingWizard", prmMapping));
        }
    }
}
