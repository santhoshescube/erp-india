﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
namespace MyBooksERP
{
    /*****************************************************
 * Created By       : Arun
 * Creation Date    : 16 Apr 2012
 * Description      : Handle Attendance
 * ***************************************************/
/*****************************************************
* Modified By      : Sanju
* Creation Date    : 25 Aug 2013
* Description      : Code optimization
* FORM ID          : 121
* ***************************************************/
    public class clsDALAttendance
    {
        ArrayList prmAtnSummary;
        ClsCommonUtility MobjClsCommonUtility;
        public clsDTOAttendance PobjclsDTOAttendance { get; set; }
        public clsDTOAttendanceDeviceLogs PobjclsDTOAttendanceDeviceLogs { get; set; }

        public DataLayer objConnection { get; set; }

        public clsDALAttendance()
        {
            MobjClsCommonUtility = new ClsCommonUtility();
        }

        public DataTable GetFingerTechDeviceSettings(int intDeviceID)
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 1));
            prmAtnSummary.Add(new SqlParameter("@DeviceID", intDeviceID));
            return objConnection.ExecuteDataTable("spPayAttendanceMaster", prmAtnSummary);
        }

        public bool SaveAttendanceInfo()
        {
            object intOutAttendanceID = 0;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 7));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", PobjclsDTOAttendance.intEmployeeID));
            prmAtnSummary.Add(new SqlParameter("@Date", PobjclsDTOAttendance.strDate));
            prmAtnSummary.Add(new SqlParameter("@ShiftID", PobjclsDTOAttendance.intShiftID));
            prmAtnSummary.Add(new SqlParameter("@WorkTime", PobjclsDTOAttendance.strWorkTime));
            prmAtnSummary.Add(new SqlParameter("@BreakTime", PobjclsDTOAttendance.strBreakTime));
            prmAtnSummary.Add(new SqlParameter("@Late", PobjclsDTOAttendance.strLate));
            prmAtnSummary.Add(new SqlParameter("@Early", PobjclsDTOAttendance.strEarly));
            prmAtnSummary.Add(new SqlParameter("@HalfDay", PobjclsDTOAttendance.blnIsHalfDay));
            prmAtnSummary.Add(new SqlParameter("@OT", PobjclsDTOAttendance.strOT));
            prmAtnSummary.Add(new SqlParameter("@LOP", PobjclsDTOAttendance.blnIsLOP));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", PobjclsDTOAttendance.intCompanyId));
            if (PobjclsDTOAttendance.intConsequenceID > 0)
                prmAtnSummary.Add(new SqlParameter("@ConsequenceID", PobjclsDTOAttendance.intConsequenceID));

            prmAtnSummary.Add(new SqlParameter("@PolicyID", PobjclsDTOAttendance.intPolicyID));
            prmAtnSummary.Add(new SqlParameter("@AmountDed", PobjclsDTOAttendance.decAmountDed));
            //prmAtnSummary.Add(new SqlParameter("@ProjectID", PobjclsDTOAttendance.intProjectID));
            //prmAtnSummary.Add(new SqlParameter("@WorkLocationID", PobjclsDTOAttendance.intWorkLocationID));
            prmAtnSummary.Add(new SqlParameter("@AttendenceStatusID", PobjclsDTOAttendance.intAttendenceStatusID));
            prmAtnSummary.Add(new SqlParameter("@AbsentTime", PobjclsDTOAttendance.intAbsentTime));
            //prmAtnSummary.Add(new SqlParameter("@LeaveID",PobjclsDTOAttendance.intLeaveID));
            if (PobjclsDTOAttendance.intLeaveID > 0)
                prmAtnSummary.Add(new SqlParameter("@LeaveID", PobjclsDTOAttendance.intLeaveID));

            if (PobjclsDTOAttendance.intShiftOrderID > 0)
            {
                prmAtnSummary.Add(new SqlParameter("@ShiftOrderNo", PobjclsDTOAttendance.intShiftOrderID));
            }
            if (PobjclsDTOAttendance.strRemarks != "")
            {
                prmAtnSummary.Add(new SqlParameter("@Remarks", PobjclsDTOAttendance.strRemarks));
            }

            if (PobjclsDTOAttendance.intProjectID > 0)
                prmAtnSummary.Add(new SqlParameter("@ProjectID", PobjclsDTOAttendance.intProjectID));


            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmAtnSummary.Add(objParam);
            if (objConnection.ExecuteNonQuery("spPayAttendanceMaster", prmAtnSummary, out intOutAttendanceID) != 0)
            {
                if ((int)intOutAttendanceID > 0)
                {
                    PobjclsDTOAttendance.intAttendanceID = (int)intOutAttendanceID;
                    SaveAttendanceDetails();
                }

            }
            return true;
        }

        public bool SaveAttendanceDetails()
        {

            if (PobjclsDTOAttendance.lstclsDTOAttendanceDetails != null)
            {
                foreach (clsDTOAttendanceDetails objclsDTOAttendanceDetails in PobjclsDTOAttendance.lstclsDTOAttendanceDetails)
                {
                    prmAtnSummary = new ArrayList();
                    prmAtnSummary.Add(new SqlParameter("@Mode", 3));
                    prmAtnSummary.Add(new SqlParameter("@AttendanceID", PobjclsDTOAttendance.intAttendanceID));
                    prmAtnSummary.Add(new SqlParameter("@Time", objclsDTOAttendanceDetails.strTime));
                    prmAtnSummary.Add(new SqlParameter("@OrderNo", objclsDTOAttendanceDetails.intOrderNo));
                    objConnection.ExecuteNonQuery("spPayAttendanceMaster", prmAtnSummary);
                }
            }
            return true;
        }

        public bool UpdateAttendanceInfo()
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 8));
            prmAtnSummary.Add(new SqlParameter("@AttendanceID", PobjclsDTOAttendance.intAttendanceID));
            prmAtnSummary.Add(new SqlParameter("@WorkTime", PobjclsDTOAttendance.strWorkTime));
            prmAtnSummary.Add(new SqlParameter("@BreakTime", PobjclsDTOAttendance.strBreakTime));
            prmAtnSummary.Add(new SqlParameter("@Late", PobjclsDTOAttendance.strLate));
            prmAtnSummary.Add(new SqlParameter("@Early", PobjclsDTOAttendance.strEarly));
            prmAtnSummary.Add(new SqlParameter("@HalfDay", PobjclsDTOAttendance.blnIsHalfDay));
            prmAtnSummary.Add(new SqlParameter("@OT", PobjclsDTOAttendance.strOT));
            prmAtnSummary.Add(new SqlParameter("@LOP", PobjclsDTOAttendance.blnIsLOP));
            prmAtnSummary.Add(new SqlParameter("@ConsequenceID", PobjclsDTOAttendance.intConsequenceID));
            prmAtnSummary.Add(new SqlParameter("@PolicyID", PobjclsDTOAttendance.intPolicyID));
            prmAtnSummary.Add(new SqlParameter("@AmountDed", PobjclsDTOAttendance.decAmountDed));
            prmAtnSummary.Add(new SqlParameter("@AbsentTime", PobjclsDTOAttendance.intAbsentTime));

            if (objConnection.ExecuteNonQuery("spPayAttendanceMaster", prmAtnSummary) != 0)
            {
                SaveAttendanceDetails();
                return true;
            }
            return false;
        }


        public bool DeleteAttendanceDetails(int iAttendanceID)
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 2));
            prmAtnSummary.Add(new SqlParameter("@AttendanceID", iAttendanceID));
            if (objConnection.ExecuteNonQuery("spPayAttendanceMaster", prmAtnSummary) != 0)
            {
                return true;
            }
            return false;
        }

        public int IsAttendanceExists(int iEmpID, int iCompanyID, int iShiftID, string sDate)
        {
            int iAttendanceID = 0;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 9));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpID));
            //prmAtnSummary.Add(new SqlParameter("@ShiftID", iShiftID));
            prmAtnSummary.Add(new SqlParameter("@Date", sDate));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", iCompanyID));
            SqlDataReader sdr = objConnection.ExecuteReader("spPayAttendanceMaster", prmAtnSummary);
            if (sdr.Read())
            {
                iAttendanceID = Convert.ToInt32(sdr["AttendanceID"]);
            }
            sdr.Close();
            return iAttendanceID;

        }

        public bool IsEmployeeInService(int iEmpID)
        {
            int iAttendanceID = 0;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 14));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpID));
            iAttendanceID = objConnection.ExecuteScalar("spPayAttendanceMaster", prmAtnSummary).ToInt32();
            return (iAttendanceID > 0);

        }

        public DataTable GetEmployeeInfo(int intMode, string sWorkID, int intTemplateMasterID, int dayID, string sFromDate, string sToDate, int intCmpID,
                                          int EmpID)
        {

            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", intMode));
            if (intMode == 1 || intMode == 2)
            {
                prmAtnSummary.Add(new SqlParameter("@WorkID", sWorkID));
                prmAtnSummary.Add(new SqlParameter("@TemplateMasterID", intTemplateMasterID));
                prmAtnSummary.Add(new SqlParameter("@DayID", dayID));
            }

            else if (intMode == 3)
            {
                prmAtnSummary.Add(new SqlParameter("@CompanyID", intCmpID));
                prmAtnSummary.Add(new SqlParameter("@EmployeeID", EmpID));

                prmAtnSummary.Add(new SqlParameter("@FromDate", sFromDate));
                prmAtnSummary.Add(new SqlParameter("@ToDate", sToDate));
            }

            return objConnection.ExecuteDataTable("spPayAttendancePolicy", prmAtnSummary);
        }
        public DataTable GetShiftInfo(int intMode, int intEmpID, int intShiftID, int intDayID, string sFromDate, string sToDate, int intCmpID)
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", intMode));
            if (intMode == 4)
            {
                prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));
                prmAtnSummary.Add(new SqlParameter("@ShiftID", intShiftID));
                prmAtnSummary.Add(new SqlParameter("@DayID", intDayID));

            }
            else if (intMode == 5)
            {
                prmAtnSummary.Add(new SqlParameter("@ShiftID", intShiftID));
            }
            else if (intMode == 3)
            {
                prmAtnSummary.Add(new SqlParameter("@CompanyID", intCmpID));
                prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));

                prmAtnSummary.Add(new SqlParameter("@FromDate", sFromDate));
                prmAtnSummary.Add(new SqlParameter("@ToDate", sToDate));
            }

            return objConnection.ExecuteDataTable("spPayAttendancePolicy", prmAtnSummary);
        }
        public bool CheckHoliDay()
        {
            bool blnIsHoliday = false;
            SqlDataReader sdrHoliday;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 6));
            sdrHoliday = objConnection.ExecuteReader("spPayAttendancePolicy", prmAtnSummary);
            if (sdrHoliday.Read())
            {
                blnIsHoliday = true; //Holiday
            }
            sdrHoliday.Close();

            if (blnIsHoliday == false)
            {
                prmAtnSummary = new ArrayList();
                prmAtnSummary.Add(new SqlParameter("@Mode", 7));
                sdrHoliday = objConnection.ExecuteReader("spPayAttendancePolicy", prmAtnSummary);
                if (sdrHoliday.Read())//Workday
                    blnIsHoliday = false;
                else// 
                    blnIsHoliday = true;//Holiday
                sdrHoliday.Close();
            }
            return blnIsHoliday;
        }

        public string GetIP(int ID)
        {
            string IP = "";
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 10));
            prmAtnSummary.Add(new SqlParameter("@DeviceID", ID));

            IP = objConnection.ExecuteScalar("spPayAttendanceMaster", prmAtnSummary).ToStringCustom();

            return IP;
        }

        public int GetEmpPolicyID(int intEmpID)
        {
            int intPolicyID = 0;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 8));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));
            intPolicyID = objConnection.ExecuteScalar("spPayAttendancePolicy", prmAtnSummary).ToInt32();
            return intPolicyID;

        }
        public int GetEmployee(string strWorkID, ref string strEmpName)
        {
            int intEmployeeID = 0;
            strEmpName = "";
            SqlDataReader sdrEmployee;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 5));
            prmAtnSummary.Add(new SqlParameter("@WorkiD", strWorkID));

            sdrEmployee = objConnection.ExecuteReader("spPayAttendanceMaster", prmAtnSummary);
            if (sdrEmployee.Read())
            {
                intEmployeeID = Convert.ToInt32(sdrEmployee["EmployeeID"]);
                strEmpName = Convert.ToString(sdrEmployee["FirstName"]);

            }
            sdrEmployee.Close();
            return intEmployeeID;
        }
        public double GetMonthlyLeave(int intEmpID, int intCompID, string atnDate)
        {
            double dblMonthlyLeve = 0;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 21));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", intCompID));
            prmAtnSummary.Add(new SqlParameter("@AtnDate", atnDate));
            dblMonthlyLeve = objConnection.ExecuteScalar("spPayAttendancePolicy", prmAtnSummary).ToDouble();
            if (dblMonthlyLeve <= 0)
            {

                prmAtnSummary = new ArrayList();
                prmAtnSummary.Add(new SqlParameter("@Mode", 10));
                prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));
                dblMonthlyLeve = objConnection.ExecuteScalar("spPayAttendancePolicy", prmAtnSummary).ToDouble();

            }
            return dblMonthlyLeve;
        }
        public double GetTakenleaves(int iEId, DateTime dt, int intLeaveTypeID)
        {

            double dTaken = 0;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 3));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEId));
            prmAtnSummary.Add(new SqlParameter("@Date", dt.ToString("dd MMM yyyy")));
            prmAtnSummary.Add(new SqlParameter("@LeaveTypeID", intLeaveTypeID));
            dTaken = objConnection.ExecuteScalar("spPayAttendnacePolicyConseqInfo", prmAtnSummary).ToDouble();
            return dTaken;

        }

        public bool GetShiftInfo(int intShiftID, ref int intShiftType, ref string sDuration, ref string sMinWorkHours, ref int intPolBreak, ref int intLatemin, ref int intEarlyMin)
        {
            intShiftType = 0;
            sDuration = "";
            sMinWorkHours = "";
            intPolBreak = 0;
            intLatemin = 0;
            intEarlyMin = 0;

            SqlDataReader sdrEmployee;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 5));
            prmAtnSummary.Add(new SqlParameter("@ShiftID", intShiftID));

            sdrEmployee = objConnection.ExecuteReader("spPayAttendancePolicy", prmAtnSummary);
            if (sdrEmployee.Read())
            {
                intShiftType = Convert.ToInt32(sdrEmployee["ShiftTypeID"]);
                sDuration = Convert.ToString(sdrEmployee["Duration"]);
                sMinWorkHours = Convert.ToString(sdrEmployee["MinWorkingHours"]);
                intPolBreak = Convert.ToInt32(sdrEmployee["AllowedBreakTime"]);
                intLatemin = Convert.ToInt32(sdrEmployee["LateAfter"]);
                intEarlyMin = Convert.ToInt32(sdrEmployee["EarlyBefore"]);
            }
            sdrEmployee.Close();
            return true;
        }
        public bool GetEmployeePolicy(int intEmpID, int intDayID, ref int iPolicyID)//, ref string sAllowedBreak, ref int iLate, ref int iEarly)
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 4));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));
            prmAtnSummary.Add(new SqlParameter("@DayID", intDayID));
            SqlDataReader sdEmp = objConnection.ExecuteReader("spPayAttendnacePolicyConseqInfo", prmAtnSummary);
            if (sdEmp.Read())
            {
                iPolicyID = Convert.ToInt32(sdEmp["WorkPolicyID"]);
                //sAllowedBreak = Convert.ToString(sdEmp["AllowedBreakTime"]);
                //iLate = Convert.ToInt32(sdEmp["LateAfter"]);
                //iEarly = Convert.ToInt32(sdEmp["EarlyBefore"]);
            }
            sdEmp.Close();
            return true;

        }
        public int GetLeaveExAddMinutes(int intEmpID, DateTime dtAtnDate)
        {

            int intAdditinalminutes = 0;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 13));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));
            prmAtnSummary.Add(new SqlParameter("@AtnDate", dtAtnDate.ToString("dd MMM yyyy")));
            intAdditinalminutes = objConnection.ExecuteScalar("spPayAttendancePolicy", prmAtnSummary).ToInt32();
            return intAdditinalminutes;
        }

        public bool GetEmployeeIsHalfDay(int intEmpID, string strFromDate)//'--------------------------Is employee on half day
        {
            double dblNoOfDays = 0;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 14));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));
            prmAtnSummary.Add(new SqlParameter("@FromDate", strFromDate));
            dblNoOfDays = objConnection.ExecuteScalar("spPayAttendancePolicy", prmAtnSummary).ToDouble();
            return (dblNoOfDays > 0);

        }

        public bool GetPolicyConsequenceWorktime(int iEmpId, ref int iLop, ref int iCasual, ref double dConseqAmt, ref int iConseqID)
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 5));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpId));
            prmAtnSummary.Add(new SqlParameter("@ConsequenceID", 2));
            SqlDataReader sdrPolicy = objConnection.ExecuteReader("spPayAttendnacePolicyConseqInfo", prmAtnSummary);
            if (sdrPolicy.Read())
            {
                iLop = Convert.ToInt32(sdrPolicy["LOP"]);
                iCasual = Convert.ToInt32(sdrPolicy["Casual"]);
                dConseqAmt = Convert.ToDouble(sdrPolicy["Amount"]);
                iConseqID = 2;
            }
            sdrPolicy.Close();
            return true;
        }


        public bool GetPolicyConsequenceBreaktime(int iPolicyID, ref int iLop, ref int iCasual, ref double dConseqAmt, ref int iConseqID)
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 6));
            prmAtnSummary.Add(new SqlParameter("@PolicyID", iPolicyID));
            SqlDataReader sdrPolicy = objConnection.ExecuteReader("spPayAttendnacePolicyConseqInfo", prmAtnSummary);
            if (sdrPolicy.Read())
            {
                iLop = Convert.ToInt32(sdrPolicy["LOP"]);
                iCasual = Convert.ToInt32(sdrPolicy["Casual"]);
                dConseqAmt = Convert.ToDouble(sdrPolicy["Amount"]);
                iConseqID = 1;
            }
            sdrPolicy.Close();
            return true;
        }



        public bool GetPolicyConsequenceLateComing(int iEmpID, ref int iLop, ref int iCasual, ref double dConseqAmt, ref int iConseqID)
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 5));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpID));
            prmAtnSummary.Add(new SqlParameter("@ConsequenceID", 3));
            SqlDataReader sdrPolicy = objConnection.ExecuteReader("spPayAttendnacePolicyConseqInfo", prmAtnSummary);
            if (sdrPolicy.Read())
            {
                iLop = Convert.ToInt32(sdrPolicy["LOP"]);
                iCasual = Convert.ToInt32(sdrPolicy["Casual"]);
                dConseqAmt = Convert.ToDouble(sdrPolicy["Amount"]);
                iConseqID = 3;
            }
            sdrPolicy.Close();
            return true;

        }
        public bool GetPolicyConsequenceEarlyGoing(int iEmpID, ref int iLop, ref int iCasual, ref double dConseqAmt, ref int iConseqID)
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 5));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpID));
            prmAtnSummary.Add(new SqlParameter("@ConsequenceID", 4));
            SqlDataReader sdrPolicy = objConnection.ExecuteReader("spPayAttendnacePolicyConseqInfo", prmAtnSummary);
            if (sdrPolicy.Read())
            {
                iLop = Convert.ToInt32(sdrPolicy["LOP"]);
                iCasual = Convert.ToInt32(sdrPolicy["Casual"]);
                dConseqAmt = Convert.ToDouble(sdrPolicy["Amount"]);
                iConseqID = 4;
            }
            sdrPolicy.Close();
            return true;
        }

        public int LeaveFromPolicyConsequence(int iCmpID, int iEmpId, int iHalfday, DateTime DtDate, int Mode)
        {
            object iLeaveid;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@CompanyId", iCmpID));
            parameters.Add(new SqlParameter("@EmployeeId", iEmpId));
            parameters.Add(new SqlParameter("@HalfDay", iHalfday));//
            parameters.Add(new SqlParameter("@Ldate", DtDate.ToString("dd MMM yyyy")));
            parameters.Add(new SqlParameter("@Mode", Mode));
            SqlParameter sQlparam = new SqlParameter("@ReturnVal", SqlDbType.Int);
            sQlparam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(sQlparam);
            if (objConnection.ExecuteNonQuery("spPayGenerateLeave", parameters, out iLeaveid) != 0)
            {
                if (Convert.ToInt32(iLeaveid) > 0)
                {
                    return Convert.ToInt32(iLeaveid);
                }
            }
            return 0;
        }

        public bool IsHoliday(int empId, int iCmpID, DateTime dtDate, int iDayID)
        {
            bool bHoliday = false;
            int ID = 0;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 7));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", iCmpID));
            prmAtnSummary.Add(new SqlParameter("@Date", dtDate.ToString("dd MMM yyyy")));
            ID = objConnection.ExecuteScalar("spPayAttendnacePolicyConseqInfo", prmAtnSummary).ToInt32();
            if (ID > 0)
                bHoliday = true;

            if (bHoliday == false)
            {
                ID = 0;
                prmAtnSummary = new ArrayList();
                prmAtnSummary.Add(new SqlParameter("@Mode", 8));
                prmAtnSummary.Add(new SqlParameter("@EmployeeID", empId));
                prmAtnSummary.Add(new SqlParameter("@DayID", iDayID));
                ID = objConnection.ExecuteScalar("spPayAttendnacePolicyConseqInfo", prmAtnSummary).ToInt32();

                if (ID > 0)
                {
                    bHoliday = false;
                }
                else
                {
                    bHoliday = true;
                }

            }
            return bHoliday;
        }


        public string GetLastSaveDate()
        {
            string sLastSavedate = "";
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 13));
            sLastSavedate = objConnection.ExecuteScalar("spPayAttendanceMaster", prmAtnSummary).ToStringCustom();

            return sLastSavedate;
        }
        public bool SaveAllLogsFromFingerTek()
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 6));
            prmAtnSummary.Add(new SqlParameter("@DeviceID", PobjclsDTOAttendanceDeviceLogs.intDeviceID));
            //prmAtnSummary.Add(new SqlParameter("@Date", objclsDTOAtnAllLogs.strDate));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", PobjclsDTOAttendanceDeviceLogs.intEmployeeID));
            prmAtnSummary.Add(new SqlParameter("@WorkiD", PobjclsDTOAttendanceDeviceLogs.strWorkID));
            prmAtnSummary.Add(new SqlParameter("@AtnDate", PobjclsDTOAttendanceDeviceLogs.strAtnDate));

            prmAtnSummary.Add(new SqlParameter("@Time", PobjclsDTOAttendanceDeviceLogs.strAtnTime));
            prmAtnSummary.Add(new SqlParameter("@Status", PobjclsDTOAttendanceDeviceLogs.strAtnStatus));

            prmAtnSummary.Add(new SqlParameter("@Result", PobjclsDTOAttendanceDeviceLogs.strAtnResult));
            prmAtnSummary.Add(new SqlParameter("@Format", PobjclsDTOAttendanceDeviceLogs.strAtnFormat));
            prmAtnSummary.Add(new SqlParameter("@DeviceTypeID", PobjclsDTOAttendanceDeviceLogs.intDeviceTypeID));

            object i = objConnection.ExecuteScalar("MSAtnFingerTechTransactions", prmAtnSummary);
            i = i == null || i == DBNull.Value ? 0 : i;
            return (((int)i) > 0) ? true : false;
        }
        public DataTable GetDeviceLogs()
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 11));
            return objConnection.ExecuteDataTable("spPayAttendanceMaster", prmAtnSummary);
        }
        public DataTable GetDynamicShiftDetails(int iShiftId)
        {
            // gettting Dynamic and Split Shift  details 
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 16));
            prmAtnSummary.Add(new SqlParameter("@ShiftID", iShiftId));
            return objConnection.ExecuteDataTable("spPayAttendancePolicy", prmAtnSummary);
        }
        public bool isAfterMinWrkHrsAsOT(int iShiftID)
        {
            bool retValue = false;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 20));
            prmAtnSummary.Add(new SqlParameter("@ShiftID", iShiftID));
            retValue = objConnection.ExecuteScalar("spPayAttendancePolicy", prmAtnSummary).ToBoolean();
            return retValue;
        }
        public string GetDynamicShiftDuration(int iShiftID, int iShiftOrderNo, ref string sMinWorkHours, ref string sAllowedBreakTime)
        {
            string sDuration = "";
            sMinWorkHours = "0";
            sAllowedBreakTime = "0";
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 15));
            prmAtnSummary.Add(new SqlParameter("@ShiftID", iShiftID));
            prmAtnSummary.Add(new SqlParameter("@ShiftOrderNo", iShiftOrderNo));
            SqlDataReader sdrEmployee = objConnection.ExecuteReader("spPayAttendancePolicy", prmAtnSummary);
            if (sdrEmployee.Read())
            {
                sDuration = Convert.ToString(sdrEmployee["Duration"]);
                sMinWorkHours = Convert.ToString(sdrEmployee["MinWorkingHours"]);
                sAllowedBreakTime = Convert.ToString(sdrEmployee["AllowedBreakTime"]);
            }
            sdrEmployee.Close();
            return sDuration;
        }
        public bool DeleteAbsentMarked(int iEmpID, int iShiftID, string sDate, int iCompanyID, int iStatusID)
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 12));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpID));
            //prmAtnSummary.Add(new SqlParameter("@ShiftID", iShiftID));
            prmAtnSummary.Add(new SqlParameter("@Date", sDate));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", iCompanyID));
            prmAtnSummary.Add(new SqlParameter("@AttendenceStatusID", iStatusID));
            objConnection.ExecuteNonQuery("spPayAttendanceMaster", prmAtnSummary);
            return true;
        }
        public DataTable GetAbsentDays(string sStartDate, string sEnddate, int iEmpID)
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", "ARD"));
            prmAtnSummary.Add(new SqlParameter("@FinYearFrom", Convert.ToDateTime(sStartDate)));
            prmAtnSummary.Add(new SqlParameter("@FinYearTo", Convert.ToDateTime(sEnddate)));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpID));
            return objConnection.ExecuteDataTable("spPayGetEmployeeAbsentDays", prmAtnSummary);

        }
        public bool GetEmployeeInfoForAbsentMarking(int iEmpID, int iDayID, string sDate, ref int iShiftID, ref int iPolicyId, ref int IcomID,
                                                     ref string sDuration, ref string sMinWorkHrs, ref  int iShiftType, ref string sAllowedBreak)
        {
            iShiftID = 0;
            iPolicyId = 0;
            IcomID = 0;
            sDuration = "0";
            sMinWorkHrs = "0";
            iShiftType = 0;
            sAllowedBreak = "0";

            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 17));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpID));
            prmAtnSummary.Add(new SqlParameter("@DayID", iDayID));
            prmAtnSummary.Add(new SqlParameter("@Date", sDate));
            SqlDataReader SdrEmployee = objConnection.ExecuteReader("spPayAttendancePolicy", prmAtnSummary);
            if (SdrEmployee.Read())
            {
                iShiftID = Convert.ToInt32(SdrEmployee["ShiftID"]);
                iPolicyId = Convert.ToInt32(SdrEmployee["WorkPolicyID"]);
                IcomID = Convert.ToInt32(SdrEmployee["CompanyID"]);
                sDuration = Convert.ToString(SdrEmployee["Duration"]);
                sMinWorkHrs = Convert.ToString(SdrEmployee["MinWorkingHours"]);
                iShiftType = Convert.ToInt32(SdrEmployee["ShiftTypeID"]);
                sAllowedBreak = Convert.ToString(SdrEmployee["AllowedBreakTime"]);
            }
            SdrEmployee.Close();

            if (iShiftID == 0)
            {
                prmAtnSummary = new ArrayList();
                prmAtnSummary.Add(new SqlParameter("@Mode", 18));
                prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpID));
                SdrEmployee = objConnection.ExecuteReader("spPayAttendancePolicy", prmAtnSummary);
                if (SdrEmployee.Read())
                {
                    iShiftID = Convert.ToInt32(SdrEmployee["ShiftID"]);
                    iPolicyId = Convert.ToInt32(SdrEmployee["WorkPolicyID"]);
                    IcomID = Convert.ToInt32(SdrEmployee["CompanyID"]);
                    sDuration = Convert.ToString(SdrEmployee["Duration"]);
                    sMinWorkHrs = Convert.ToString(SdrEmployee["MinWorkingHours"]);
                    iShiftType = Convert.ToInt32(SdrEmployee["ShiftTypeID"]);
                }
                SdrEmployee.Close();
            }
            if (IcomID > 0)
            {
                prmAtnSummary = new ArrayList();
                prmAtnSummary.Add(new SqlParameter("@Mode", 3));
                prmAtnSummary.Add(new SqlParameter("@CompanyID", IcomID));
                prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpID));
                prmAtnSummary.Add(new SqlParameter("@FromDate", sDate));
                prmAtnSummary.Add(new SqlParameter("@ToDate", sDate));
                SdrEmployee = objConnection.ExecuteReader("spPayAttendancePolicy", prmAtnSummary);
                if (SdrEmployee.Read())
                {
                    iShiftID = Convert.ToInt32(SdrEmployee["ShiftID"]);
                    sDuration = Convert.ToString(SdrEmployee["Duration"]);
                    sMinWorkHrs = Convert.ToString(SdrEmployee["MinWorkingHours"]);
                    iShiftType = Convert.ToInt32(SdrEmployee["ShiftTypeID"]);
                }
                SdrEmployee.Close();
            }

            return true;


        }
        public int AbsentSaving(int iEmployeeId, string sAtndate, int ishiftID, int iShiftOrderID,
                                               int iabsent, int iworkpolicyId, int iCompanyID)
        {


            prmAtnSummary = new ArrayList();

            object iReturnid = 0;

            prmAtnSummary.Add(new SqlParameter("@Mode", 7));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmployeeId));
            prmAtnSummary.Add(new SqlParameter("@Date", sAtndate));
            prmAtnSummary.Add(new SqlParameter("@ShiftID", ishiftID));
            prmAtnSummary.Add(new SqlParameter("@WorkTime", "00:00:00"));
            prmAtnSummary.Add(new SqlParameter("@BreakTime", ""));
            prmAtnSummary.Add(new SqlParameter("@Late", "00:00:00"));
            prmAtnSummary.Add(new SqlParameter("@Early", "00:00:00"));
            prmAtnSummary.Add(new SqlParameter("@OT", ""));
            prmAtnSummary.Add(new SqlParameter("@AbsentTime", iabsent));
            prmAtnSummary.Add(new SqlParameter("@HalfDay", false));
            prmAtnSummary.Add(new SqlParameter("@LOP", false));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", iCompanyID));
            prmAtnSummary.Add(new SqlParameter("@ConsequenceID", 0));
            prmAtnSummary.Add(new SqlParameter("@PolicyID", iworkpolicyId));
            prmAtnSummary.Add(new SqlParameter("@AmountDed", 0));
            prmAtnSummary.Add(new SqlParameter("@AttendenceStatusID", Convert.ToInt32(AttendanceStatus.Absent)));

            if (iShiftOrderID > 0)
            {
                prmAtnSummary.Add(new SqlParameter("@ShiftOrderNo", iShiftOrderID));
            }

            SqlParameter sQlparam = new SqlParameter("@ReturnVal", SqlDbType.Int);
            sQlparam.Direction = ParameterDirection.ReturnValue;
            prmAtnSummary.Add(sQlparam);

            objConnection.ExecuteNonQuery("spPayAttendanceMaster", prmAtnSummary, out  iReturnid);
            return Convert.ToInt32(iReturnid);

        }


        //----------------------------------------------------Apply Polcy Consequence End -----------------------------------

        //--------------------------------------------Manual------------------
        public int GetMaxColumnCount(int intEmpID, int intCMPID, DateTime dtDate, bool blnMonthly, bool blnbreakExceed,
                                        string strSearchConditionEmployee, string strSearchConditionAttendance)
        {
            int intMaxCount = 0;
            int iIsMonthly = blnMonthly ? 1 : 0;
            int iIsbreakExceed = blnbreakExceed ? 1 : 0;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 3));
            prmAtnSummary.Add(new SqlParameter("@IsBreakExceed", iIsbreakExceed));
            prmAtnSummary.Add(new SqlParameter("@IsMonthly", iIsMonthly));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", intCMPID));
            prmAtnSummary.Add(new SqlParameter("@Date", dtDate.ToString("dd-MMM-yyyy")));
            prmAtnSummary.Add(new SqlParameter("@SearchConditionEmployee", strSearchConditionEmployee));
            prmAtnSummary.Add(new SqlParameter("@SearchConditionAttendance", strSearchConditionAttendance));

            intMaxCount = objConnection.ExecuteScalar("spPayAttendanceDetails", prmAtnSummary).ToInt32();

            return intMaxCount;
        }

        public DataTable GetAttendance(string strInOut1, string strInOut, int EmpID, int CmpID, DateTime dtDate, bool blnMonthly,
                                                 string strSearchConditionEmployee, string strSearchConditionAttendance)
        {
            int iIsMonthly = blnMonthly ? 1 : 0;

            prmAtnSummary = new ArrayList();
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    prmAtnSummary.Add(new SqlParameter("@Mode", 20));
            //}
            //else
            //{
                prmAtnSummary.Add(new SqlParameter("@Mode", 4));
            //}
            prmAtnSummary.Add(new SqlParameter("@InOutOrder", strInOut1));
            prmAtnSummary.Add(new SqlParameter("@InOutNo", strInOut));
            prmAtnSummary.Add(new SqlParameter("@IsMonthly", iIsMonthly));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", EmpID));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", CmpID));
            prmAtnSummary.Add(new SqlParameter("@Date", dtDate.ToString("dd-MMM-yyyy")));
            prmAtnSummary.Add(new SqlParameter("@SearchConditionEmployee", strSearchConditionEmployee));
            prmAtnSummary.Add(new SqlParameter("@SearchConditionAttendance", strSearchConditionAttendance));
            return objConnection.ExecuteDataTable("spPayAttendanceDetails", prmAtnSummary);


        }

        public bool CheckOffDay(int intEmpID, string dayOffWeek,DateTime dtDate)
        {
            bool returnvalue = false;
            int ID = 0;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@EmployeeID", intEmpID));
            parameters.Add(new SqlParameter("@DayOfWeek", dayOffWeek));
            parameters.Add(new SqlParameter("@Date", dtDate));

            ID = objConnection.ExecuteScalar("spPayAttendnacePolicyConseqInfo", parameters).ToInt32();
            if (ID > 0)
            {
                returnvalue = true;

            }

            return returnvalue;
        }
        public bool CheckHoliday(int intCmpID, string strDate)
        {
            bool returnvalue = false;
            ArrayList PrmHoliday = new ArrayList();
            int ID = 0;
            PrmHoliday.Add(new SqlParameter("@Mode", 6));
            PrmHoliday.Add(new SqlParameter("@FromDate", strDate));
            PrmHoliday.Add(new SqlParameter("@CompanyID", intCmpID));
            ID = objConnection.ExecuteScalar("spPayAttendancePolicy", PrmHoliday).ToInt32();
            if (ID > 0)
            {
                returnvalue = true;
            }

            return returnvalue;

        }
        public bool CheckLeave(int intEmpID, string strDate, ref bool leaveflag)
        {
            leaveflag = false;
            bool returnvalue = false;
            SqlDataReader sdr;
            ArrayList PrmHoliday = new ArrayList();
            PrmHoliday.Add(new SqlParameter("@Mode", 12));
            PrmHoliday.Add(new SqlParameter("@Date", strDate));
            PrmHoliday.Add(new SqlParameter("@EmployeeID", intEmpID));
            sdr = objConnection.ExecuteReader("spPayAttendnacePolicyConseqInfo", PrmHoliday);
            if (sdr.Read())
            {
                returnvalue = true;
                leaveflag = true;

            }
            sdr.Close();
            return returnvalue;
        }
        public DataTable GetEmployeeShiftInfo(int intEmpID, int intCmpID, string strDate)
        {
            ArrayList prmShift = new ArrayList();
            prmShift.Add(new SqlParameter("@Mode", 3));

            prmShift.Add(new SqlParameter("@EmployeeID", intEmpID));
            prmShift.Add(new SqlParameter("@CompanyID", intCmpID));
            prmShift.Add(new SqlParameter("@FromDate", strDate));
            prmShift.Add(new SqlParameter("@ToDate", strDate));
            return objConnection.ExecuteDataTable("spPayAttendancePolicy", prmShift);
        }
        public bool CheckDay(int IntEmpID, int DayID, DateTime dtDate)
        {

            bool returnvalue = false;
            SqlDataReader sdr;
            ArrayList prmShift = new ArrayList();
            prmShift = new ArrayList();
            prmShift.Add(new SqlParameter("@Mode", 7));
            prmShift.Add(new SqlParameter("@EmployeeID", IntEmpID));
            prmShift.Add(new SqlParameter("@DayID", DayID));
            prmShift.Add(new SqlParameter("@Date", dtDate));
            sdr = objConnection.ExecuteReader("spPayAttendancePolicy", prmShift);
            if (sdr.Read())
            {
                returnvalue = true;
            }
            sdr.Close();
            return returnvalue;
        }
        public DataTable GetDailyShift(int intEmpID, int intDayId,DateTime dtDate)
        {
            ArrayList prmShift = new ArrayList();
            prmShift = new ArrayList();
            prmShift.Add(new SqlParameter("@Mode", 13));
            prmShift.Add(new SqlParameter("@EmployeeID", intEmpID));
            prmShift.Add(new SqlParameter("@DayID", intDayId));
            prmShift.Add(new SqlParameter("@Date", dtDate));
            return objConnection.ExecuteDataTable("spPayAttendnacePolicyConseqInfo", prmShift);
        }
        public DataTable GetOffDayShift(int intEmpID, int DayID, DateTime dtDate)
        {
            ArrayList prmShift = new ArrayList();
            prmShift = new ArrayList();
            prmShift.Add(new SqlParameter("@Mode", 14));
            prmShift.Add(new SqlParameter("@EmployeeID", intEmpID));
            prmShift.Add(new SqlParameter("@DayID", DayID));
            prmShift.Add(new SqlParameter("@Date", dtDate));
            return objConnection.ExecuteDataTable("spPayAttendnacePolicyConseqInfo", prmShift);
        }

        public DataTable FillEmployes(string sEmpIds, DateTime dtDate, string strSearchConditionEmployee, string strSearchConditionAttendance)
        {

            DataTable dtEmp;

            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 2));
            prmAtnSummary.Add(new SqlParameter("@Date", dtDate.ToString("dd-MMM-yyyy")));
            prmAtnSummary.Add(new SqlParameter("@SearchConditionEmployee", strSearchConditionEmployee));
            prmAtnSummary.Add(new SqlParameter("@EmpIDS", sEmpIds));


            dtEmp = objConnection.ExecuteDataTable("spPayAttendanceDetails", prmAtnSummary);

            return dtEmp;

        }
        public bool IsEmployeeOnLeave(int iEmpId, int iCompID, DateTime dCurrentDate)
        {
            bool blnReturnValue = false;
            ArrayList prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 18));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpId));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", iCompID));
            prmAtnSummary.Add(new SqlParameter("@Date", dCurrentDate.Date.ToString("dd MMM yyyy")));


            SqlDataReader sdrShift = objConnection.ExecuteReader("spPayAttendnacePolicyConseqInfo", prmAtnSummary);
            if (sdrShift.Read())
            {

                blnReturnValue = true;
            }
            sdrShift.Close();
            return blnReturnValue;
        }
        public bool CheckOffDayShift(int intEmpID, int DayID, DateTime dtDate)
        {
            bool blnReturnValue = false;
            ArrayList prmShift = new ArrayList();
            prmShift = new ArrayList();
            prmShift.Add(new SqlParameter("@Mode", 14));
            prmShift.Add(new SqlParameter("@EmployeeID", intEmpID));
            prmShift.Add(new SqlParameter("@DayID", DayID));
            prmShift.Add(new SqlParameter("@Date", dtDate));
            SqlDataReader sdrShift = objConnection.ExecuteReader("spPayAttendnacePolicyConseqInfo", prmShift);
            if (sdrShift.Read())
            {
                blnReturnValue = true;//''offday with shift
            }
            sdrShift.Close();
            return blnReturnValue;
        }

        public DataTable LoadEmployeeGrid(string strEmpFilterType, string strEmpWorkstatus, int intEmpFilter, int intCmpID, int intEmpFilterTypeValue, int intWorkStatusID)
        {

            DataTable dtEmp;

            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 1));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", intCmpID));
            prmAtnSummary.Add(new SqlParameter("@EmpFilterTypeValue", intEmpFilterTypeValue));
            prmAtnSummary.Add(new SqlParameter("@WorkStatusID", intWorkStatusID));
            prmAtnSummary.Add(new SqlParameter("@EmpFilterType", intEmpFilter));
            prmAtnSummary.Add(new SqlParameter("@IsArabic", 0));
            dtEmp = objConnection.ExecuteDataTable("spPayAttendanceDetails", prmAtnSummary);

            return dtEmp;

        }

        public bool IsSalaryReleased(int iEid, int iCmpID, DateTime dtdate)
        {
            bool IsSalaryReleased = false;
            try
            {
                int iIsSalaryReleased = 0;
                prmAtnSummary = new ArrayList();
                prmAtnSummary.Add(new SqlParameter("@Mode", 10));
                prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEid));
                prmAtnSummary.Add(new SqlParameter("@CompanyID", iCmpID));
                prmAtnSummary.Add(new SqlParameter("@Date", dtdate.ToString("dd-MMM-yyyy")));
                iIsSalaryReleased = objConnection.ExecuteScalar("spPayAttendanceDetails", prmAtnSummary).ToInt32();

                if (iIsSalaryReleased > 0)
                    IsSalaryReleased = true;

                return IsSalaryReleased;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool DeleteAttendanceWithLeave(int iEid, int iCmpID, DateTime dtdate, int iLeaveID)
        {
            try
            {
                int intIsDelete = 0;
                prmAtnSummary = new ArrayList();
                prmAtnSummary.Add(new SqlParameter("@Mode", 18));
                prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEid));
                prmAtnSummary.Add(new SqlParameter("@CompanyID", iCmpID));
                prmAtnSummary.Add(new SqlParameter("@Date", dtdate.ToString("dd-MMM-yyyy")));
                prmAtnSummary.Add(new SqlParameter("@LeaveID", iLeaveID));

                intIsDelete = objConnection.ExecuteScalar("spPayAttendanceDetails", prmAtnSummary).ToInt32();

                if (intIsDelete > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string GetJoiningDate(int empid)
        {
            string sJoiningDate = "";
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 15));
            parameters.Add(new SqlParameter("@EmployeeID", empid));
            SqlDataReader sdrPolicy = objConnection.ExecuteReader("spPayAttendnacePolicyConseqInfo", parameters);
            if (sdrPolicy.Read())
            {
                sJoiningDate = Convert.ToString(sdrPolicy["JoiningDate"]);
            }
            sdrPolicy.Close();
            return sJoiningDate;

        }
        public DataTable GetShift(int iShiftId, int intEmpID)
        {
            ArrayList prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 17));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));
            prmAtnSummary.Add(new SqlParameter("@ShiftID", iShiftId));
            return objConnection.ExecuteDataTable("spPayAttendnacePolicyConseqInfo", prmAtnSummary);
        }
        public bool DeleteLeaveEntry(int iEmpId, int Cmpid, DateTime dDate)
        {
            int iHalfDay = 0;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@CompanyId", Cmpid));
            parameters.Add(new SqlParameter("@EmployeeId", iEmpId));
            parameters.Add(new SqlParameter("@HalfDay", iHalfDay));//Convert.ToInt32(0)
            parameters.Add(new SqlParameter("@Ldate", dDate.ToString("dd MMM yyyy")));
            parameters.Add(new SqlParameter("@Mode", 1));
            objConnection.ExecuteNonQuery("spPayGenerateLeave", parameters);
            return true;

        }
        public bool DeleteLeaveFromAttendance(int iEmpID, int iCmpID, DateTime dtDate, int iLeaveID)//
        {//'for deleting leaves from attendance
            int iHalfDay = 0;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@CompanyId", iCmpID));
            parameters.Add(new SqlParameter("@EmployeeId", iEmpID));
            parameters.Add(new SqlParameter("@HalfDay", iHalfDay));//Convert.ToInt32(0)
            parameters.Add(new SqlParameter("@Ldate", dtDate.ToString("dd MMM yyyy")));// 'DTPDate.Value.ToString("dd MMM yyyy")
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@LID", iLeaveID));
            objConnection.ExecuteNonQuery("spPayGenerateLeave", parameters);
            return true;
        }
        public bool DeleteAttendance(int intEmpID, int intCmpID, DateTime dtDates)
        {
            ArrayList prmShift = new ArrayList();
            prmShift.Add(new SqlParameter("@Mode", 19));
            prmShift.Add(new SqlParameter("@EmployeeID", intEmpID));
            prmShift.Add(new SqlParameter("@CompanyID", intCmpID));
            prmShift.Add(new SqlParameter("@Date", dtDates.ToString("dd MMM yyyy")));
            objConnection.ExecuteNonQuery("spPayAttendnacePolicyConseqInfo", prmShift);

            return true;
        }
        public bool IsAttendanceExistsManual(int iCmpID, int iEmpID, DateTime DtDates)
        {
            bool AttendanceExists = false;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 20));
            parameters.Add(new SqlParameter("@CompanyID", iCmpID));
            parameters.Add(new SqlParameter("@EmployeeID", iEmpID));
            parameters.Add(new SqlParameter("@Date", DtDates.ToString("dd MMM yyyy")));

            SqlDataReader DR = objConnection.ExecuteReader("spPayAttendnacePolicyConseqInfo", parameters);
            if (DR.Read())
            {
                AttendanceExists = true;
            }

            DR.Close();
            return AttendanceExists;

        }
        public string GetEmployeeJoiningDate(int intEmpID)
        {
            string strJoining = "";
            ArrayList prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 15));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));
            SqlDataReader sdrjoing = objConnection.ExecuteReader("spPayAttendnacePolicyConseqInfo", prmAtnSummary);
            if (sdrjoing.Read())
            {
                strJoining = Convert.ToString(sdrjoing["JoiningDate"]);
            }

            sdrjoing.Close();
            return strJoining;

        }
        public int LeaveEntry(int iCmpID, int iEmpId, int intHalf, DateTime dtDate, int intLeavetype, int intPaid)
        {

            ArrayList parameters = new ArrayList();
            object iLeaveid = 0;
            parameters.Add(new SqlParameter("@CompanyId", iCmpID));
            parameters.Add(new SqlParameter("@EmployeeId", iEmpId));
            parameters.Add(new SqlParameter("@HalfDay", intHalf));
            parameters.Add(new SqlParameter("@Ldate", dtDate.ToString("dd MMM yyyy")));

            parameters.Add(new SqlParameter("@LeaveTypeID", intLeavetype));
            parameters.Add(new SqlParameter("@Paid", intPaid));

            parameters.Add(new SqlParameter("@Mode", 3));
            SqlParameter sQlparam = new SqlParameter("@ReturnVal", SqlDbType.Int);
            sQlparam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(sQlparam);
            if (objConnection.ExecuteNonQuery("spPayGenerateLeave", parameters, out iLeaveid) != 0)
            {

            }
            return Convert.ToInt32(iLeaveid);

        }

        public bool GetLeaveDetails(int iCmpID, int iEmpID, int ileaveTypeID, int iLeavePolicyId, string strFromDate)
        {
            bool blnRetValue = false;
            int iType = 0;//Mode
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@CompanyID", iCmpID));
            parameters.Add(new SqlParameter("@EmployeeID", iEmpID));
            parameters.Add(new SqlParameter("@LeavetypeID", ileaveTypeID));
            parameters.Add(new SqlParameter("@Leavepolicyid", iLeavePolicyId));
            parameters.Add(new SqlParameter("@Fromdate", strFromDate));
            parameters.Add(new SqlParameter("@Todate", strFromDate));
            parameters.Add(new SqlParameter("@Type", iType));
            DataSet dtLeave = objConnection.ExecuteDataSet("spPayEmployeeLeaveSummaryFromLeaveEntry", parameters);
            if (dtLeave != null)
            {
                if (dtLeave.Tables[0].Rows.Count > 0)
                    blnRetValue = true;
            }
            return blnRetValue;
        }



        public bool GetAbsentTime(int iShiftid, int iWorkPolicy, int dayid, ref string sBreakTime, ref string sMinWorkHours)
        {

            sBreakTime = "0";
            sMinWorkHours = "0";
            if (iShiftid > 0)
            {
                ArrayList prmAtnSummary = new ArrayList();
                prmAtnSummary.Add(new SqlParameter("@Mode", 19));
                prmAtnSummary.Add(new SqlParameter("@DayID", dayid));
                prmAtnSummary.Add(new SqlParameter("@ShiftID", iShiftid));
                prmAtnSummary.Add(new SqlParameter("@PolicyID", iWorkPolicy));
                SqlDataReader sdrShift = objConnection.ExecuteReader("spPayAttendancePolicy", prmAtnSummary);

                if (sdrShift.Read())
                {
                    sBreakTime = Convert.ToString(sdrShift["AllowedBreakTime"]);
                    sMinWorkHours = Convert.ToString(sdrShift["MinWorkingHours"]);

                }
                sdrShift.Close();
                return true;
            }
            return false;

        }

        public double GetEmployeeLeaveStatus(int iEmpId, int iCmpID, string atnDate)
        {
            SqlDataReader sdrPolicy;
            double MintMonthlyleave = 0;

            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 9));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpId));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", iCmpID));
            prmAtnSummary.Add(new SqlParameter("@Date", atnDate));
            sdrPolicy = objConnection.ExecuteReader("spPayAttendnacePolicyConseqInfo", prmAtnSummary);
            if (sdrPolicy.Read())
            {
                MintMonthlyleave = Convert.ToDouble(sdrPolicy["MonthLeave"]);
                sdrPolicy.Close();
            }
            else
            {
                sdrPolicy.Close();
                prmAtnSummary = new ArrayList();
                prmAtnSummary.Add(new SqlParameter("@Mode", 1));
                prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpId));
                sdrPolicy = objConnection.ExecuteReader("spPayAttendnacePolicyConseqInfo", prmAtnSummary);
                if (sdrPolicy.Read())
                {
                    MintMonthlyleave = Convert.ToDouble(sdrPolicy["MonthLeave"]);
                }
                sdrPolicy.Close();
            }
            return MintMonthlyleave;
        }


        public double GetEmployeeLeaveStatus(int iEmpId, int intCompID, string atnDate, int intLeaveTypeID)
        {
            SqlDataReader sdrPolicy;
            double MintMonthlyleave = 0;

            ArrayList prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 21));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpId));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", intCompID));
            prmAtnSummary.Add(new SqlParameter("@LeaveTypeID", intLeaveTypeID));
            prmAtnSummary.Add(new SqlParameter("@AtnDate", atnDate));
            sdrPolicy = objConnection.ExecuteReader("spPayAttendancePolicy", prmAtnSummary);
            if (sdrPolicy.Read())
            {
                MintMonthlyleave = Convert.ToDouble(sdrPolicy["MonthLeave"]);
                sdrPolicy.Close();
            }
            else
            {
                sdrPolicy.Close();
                prmAtnSummary = new ArrayList();
                prmAtnSummary.Add(new SqlParameter("@Mode", 10));
                prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpId));
                prmAtnSummary.Add(new SqlParameter("@LeaveTypeID", intLeaveTypeID));
                sdrPolicy = objConnection.ExecuteReader("spPayAttendancePolicy", prmAtnSummary);
                if (sdrPolicy.Read())
                {
                    MintMonthlyleave = Convert.ToDouble(sdrPolicy["MonthLeave"]);
                }
                sdrPolicy.Close();
            }
            return MintMonthlyleave;
        }
        public DataSet GetBalanceAndHolidaysdetails(int IntCompany, int IntEmployeeID, int IntLeaveTypeID,string strDate)
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@CompanyID", IntCompany));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", IntEmployeeID));
            prmAtnSummary.Add(new SqlParameter("@LeaveTypeID", IntLeaveTypeID));
            prmAtnSummary.Add(new SqlParameter("@AtnDate", strDate));
            prmAtnSummary.Add(new SqlParameter("@Mode", 23));
            DataSet DtSet = objConnection.ExecuteDataSet("spPayAttendancePolicy", prmAtnSummary);

            return DtSet;
        }
        public DataSet ShowCount(DateTime dtpFromDate, int intEmployeeID, string strSearchConditionEmployee, bool blnIsMonthly)
        {
            DataSet dtEmp;
            int iIsMonthly = blnIsMonthly ? 1 : 0;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 5));
            prmAtnSummary.Add(new SqlParameter("@IsMonthly", iIsMonthly));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            prmAtnSummary.Add(new SqlParameter("@Date", dtpFromDate.ToString("dd-MMM-yyyy")));
            prmAtnSummary.Add(new SqlParameter("@SearchConditionEmployee", strSearchConditionEmployee));

            dtEmp = objConnection.ExecuteDataSet("spPayAttendanceDetails", prmAtnSummary);

            return dtEmp;


        }

        public string ShowConsequence(int iEmpID, DateTime dtDate)
        {
            string StrConsequenceReason = "";
            DataTable dtEmp;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 6));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpID));
            prmAtnSummary.Add(new SqlParameter("@Date", dtDate.ToString("dd-MMM-yyyy")));
            dtEmp = objConnection.ExecuteDataTable("spPayAttendanceDetails", prmAtnSummary);
            if (dtEmp != null)
            {
                if (dtEmp.Rows.Count > 0)
                {
                    if (dtEmp.Rows[0]["Reason"].ToStringCustom() != string.Empty && dtEmp.Rows[0]["Consequence"].ToStringCustom() != string.Empty)
                        StrConsequenceReason = dtEmp.Rows[0]["Reason"].ToStringCustom() + "-" + dtEmp.Rows[0]["Consequence"].ToStringCustom();
                }
            }
            return StrConsequenceReason;
        }

        public bool IsConsiderBufferTimeForOT(int iShiftID, ref int iBufferTime, ref int iMinOtMinutes)
        {
            //Checking buffer Time is consider for Ot ,if it is true then after (ToTime+ buffer time) Employee Is worked consider ot other wise after Totime, ot is Consider 
            bool blnIsConsiderBufferTimeForOT = false;
            iBufferTime = 0;
            iMinOtMinutes = 0;
            DataTable dtEmp;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 7));
            prmAtnSummary.Add(new SqlParameter("@ShiftID", iShiftID));
            dtEmp = objConnection.ExecuteDataTable("spPayAttendanceDetails", prmAtnSummary);
            if (dtEmp != null)
            {
                if (dtEmp.Rows.Count > 0)
                {
                    iBufferTime = dtEmp.Rows[0]["Buffer"].ToInt32();
                    blnIsConsiderBufferTimeForOT = dtEmp.Rows[0]["IsBufferForOT"].ToInt32() > 0 ? true : false;
                    iMinOtMinutes = dtEmp.Rows[0]["MinimumOT"].ToInt32();
                }
            }
            return blnIsConsiderBufferTimeForOT;
        }
        public bool CheckHalfdayLeave(int iCompID, int iEmpID, DateTime dCurrentDate)
        {
            int intEmpID = 0;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 8));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpID));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", iCompID));
            prmAtnSummary.Add(new SqlParameter("@Date", dCurrentDate.ToString("dd MMM yyyy")));
            intEmpID = objConnection.ExecuteScalar("spPayAttendanceDetails", prmAtnSummary).ToInt32();


            return (intEmpID > 0);
        }
        public int CheckHalfdayLeaveNew(int iCompID, int iEmpID, DateTime dCurrentDate)
        {

            int iLeaveTypeID = 0;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 9));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpID));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", iCompID));
            prmAtnSummary.Add(new SqlParameter("@Date", dCurrentDate.ToString("dd MMM yyyy")));
            iLeaveTypeID = objConnection.ExecuteScalar("spPayAttendanceDetails", prmAtnSummary).ToInt32();

            return iLeaveTypeID;
        }
        public DataTable GetLeaveRemarks(int iCompID, int iEmpID, DateTime dCurrentDate)
        {
            //get leave remarks,leavetype,halfday,leaveid
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 17));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", iEmpID));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", iCompID));
            prmAtnSummary.Add(new SqlParameter("@Date", dCurrentDate.ToString("dd MMM yyyy")));
            return objConnection.ExecuteDataTable("spPayAttendanceDetails", prmAtnSummary);
        }

        public DataTable GetOtDetails()
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 10));
            return objConnection.ExecuteDataTable("spPayAttendanceDetails", prmAtnSummary);

        }
        public int GetTemplateFileType(int intTemplateID)
        {
            int intFileType = 0;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@TemplateMasterID", intTemplateID));
            intFileType = objConnection.ExecuteScalar("spPayAttendanceDetails", parameters).ToInt32();
            return intFileType;
        }
        public bool GetTemplateDetails(int intTemplateID, ref bool IsHeaderExists, ref int PunchingCount, ref string DateFormat, ref bool IsTimeWithDate)
        {
            IsHeaderExists = false;
            PunchingCount = 0;
            DateFormat = "";
            IsTimeWithDate = false;
            bool blnRetValue = false;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 13));
            parameters.Add(new SqlParameter("@TemplateMasterID", intTemplateID));
            DataTable DtDetails = objConnection.ExecuteDataTable("spPayAttendanceDetails", parameters);
            if (DtDetails != null)
            {
                if (DtDetails.Rows.Count > 0)
                {
                    IsHeaderExists = DtDetails.Rows[0]["IsHeaderExists"].ToInt32() == 1 ? true : false;
                    PunchingCount = DtDetails.Rows[0]["PunchingCount"].ToInt32();
                    DateFormat = DtDetails.Rows[0]["DateFormat"].ToStringCustom();
                    IsTimeWithDate = DtDetails.Rows[0]["IsTimeWithDate"].ToBoolean();
                    blnRetValue = true;
                }
            }
            return blnRetValue;
        }
        public DataTable GetTemplateFieldDetails(int intTemplateID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 14));
            parameters.Add(new SqlParameter("@TemplateMasterID", intTemplateID));
            return objConnection.ExecuteDataTable("spPayAttendanceDetails", parameters);
        }

        public bool AttendanceAllReadyExists(int intTemplateID, string strWorkID, string strDate)
        {
            int intEmpID = 0;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 15));
            parameters.Add(new SqlParameter("@TemplateMasterID", intTemplateID));
            parameters.Add(new SqlParameter("@WorkID", strWorkID));
            parameters.Add(new SqlParameter("@Date", strDate));
            parameters.Add(new SqlParameter("@AttendenceStatusID", (int)AttendanceStatus.Absent));
            intEmpID = objConnection.ExecuteScalar("spPayAttendanceDetails", parameters).ToInt32();
            return (intEmpID > 0);
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MobjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }

        public bool ClearAllPools()
        {

            objConnection.ClearAllPools();
            return true;
        }
        public DataTable getAttendanceLive(int iEmpID, int DeviceID, DateTime AttDate)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 16));
            parameters.Add(new SqlParameter("@EmployeeID", iEmpID));
            parameters.Add(new SqlParameter("@DeviceID", DeviceID));
            parameters.Add(new SqlParameter("@AttDate", AttDate));
            return objConnection.ExecuteDataTable("spPayAttendanceDetails", parameters);
        }

        public bool SaveAttendanceSummaryConsequence(int EmployeeID, int Month, int Year)
        {
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 16));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", EmployeeID));
            prmAtnSummary.Add(new SqlParameter("@Month", Month));
            prmAtnSummary.Add(new SqlParameter("@Year", Year));
            objConnection.ExecuteNonQuery("spPayAttendanceMaster", prmAtnSummary);

            return true;
        }

        public DataTable CheckConsequence(int EmployeeID, int Month, int Year)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 18));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@Month", Month));
            parameters.Add(new SqlParameter("@Year", Year));
            return objConnection.ExecuteDataTable("spPayAttendanceMaster", parameters);
        }


    }
}
