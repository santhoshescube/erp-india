﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
namespace MyBooksERP//.DAL.FingerTech
{
    /*****************************************************
 * Created By       : Arun
 * Creation Date    : 17 Apr 2012
 * Description      : Handle Attendance Device Settings
 * ***************************************************/

    public class clsDALAttendanceDeviceSettings:IDisposable
    {
        ArrayList prmDeviceSettings ;
        //ClsCommonUtility MobjClsCommonUtility;
        //DataLayer MobjDataLayer;

        public clsDTOAttendanceDeviceSettings PobjclsDTOAttendanceDeviceSettings { get; set; }
        public DataLayer objConnection { get; set; }

        public bool SaveDeviceSettings()
        {
            prmDeviceSettings = new ArrayList();
            object objOutSLID = 0;

            if (PobjclsDTOAttendanceDeviceSettings.intDeviceID > 0)//Update
            {
                prmDeviceSettings.Add(new SqlParameter("@Mode", 2));
                prmDeviceSettings.Add(new SqlParameter("@DeviceID", PobjclsDTOAttendanceDeviceSettings.intDeviceID));
            }
            else
            { prmDeviceSettings.Add(new SqlParameter("@Mode", 1)); } //Insert

            prmDeviceSettings.Add(new SqlParameter("@DeviceName", PobjclsDTOAttendanceDeviceSettings.strDevicename));
            prmDeviceSettings.Add(new SqlParameter("@IPAddress", PobjclsDTOAttendanceDeviceSettings.strIPAddress));
            prmDeviceSettings.Add(new SqlParameter("@Port", PobjclsDTOAttendanceDeviceSettings.dblPort));
            prmDeviceSettings.Add(new SqlParameter("@CommKey", PobjclsDTOAttendanceDeviceSettings.intCommKey));
            prmDeviceSettings.Add(new SqlParameter("@Model", PobjclsDTOAttendanceDeviceSettings.strModel));
            prmDeviceSettings.Add(new SqlParameter("@Delay", PobjclsDTOAttendanceDeviceSettings.intDelay));
            prmDeviceSettings.Add(new SqlParameter("@TimeOut",PobjclsDTOAttendanceDeviceSettings.intTimeOut));
            prmDeviceSettings.Add(new SqlParameter("@DeviceNo",PobjclsDTOAttendanceDeviceSettings.intDeviceNo));
            prmDeviceSettings.Add(new SqlParameter("@DeviceTypeID", PobjclsDTOAttendanceDeviceSettings.intDeviceTypeID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmDeviceSettings.Add(objParam);
            //object objID = objConnection.ExecuteScalar("spPayAttendanceDeviceSettings", prmDeviceSettings);

            //int intID = objID == null ? -1 : Convert.ToInt32(objID);

            if (objConnection.ExecuteNonQueryWithTran("spPayAttendanceDeviceSettings", prmDeviceSettings, out objOutSLID) != 0)
            {
                if ((int)objOutSLID > 0)
                {
                    PobjclsDTOAttendanceDeviceSettings.intDeviceID = (int)objOutSLID;
                }
                return true;
            }
            //return intID > 0;
            return false;
        }

        public DataTable DisplaySettings()
        {
            prmDeviceSettings = new ArrayList();
            prmDeviceSettings.Add(new SqlParameter("@Mode", 3));
            prmDeviceSettings.Add(new SqlParameter("@DeviceTypeID", PobjclsDTOAttendanceDeviceSettings.intDeviceTypeID));
            return objConnection.ExecuteDataTable("spPayAttendanceDeviceSettings", prmDeviceSettings);
        }
        public bool DeleteDeviceSettings()
        {
            prmDeviceSettings = new ArrayList();
            prmDeviceSettings.Add(new SqlParameter("@Mode", 4));
            prmDeviceSettings.Add(new SqlParameter("@DeviceID", PobjclsDTOAttendanceDeviceSettings.intDeviceID));
            if (objConnection.ExecuteNonQuery("spPayAttendanceDeviceSettings", prmDeviceSettings) != 0)
            {
                return true;
            }
            return false;
        }
        //public DataTable GetDeviceSettings(int intConectionType)
        //{
        //    prmDeviceSettings = new ArrayList();
        //    if (intConectionType == 0)
        //    {
        //        prmDeviceSettings.Add(new SqlParameter("@Mode",5));
                
        //    }
        //    else if(intConectionType==1)
        //    {
        ////        prmDeviceSettings.Add(new SqlParameter("@Mode", 6));
        //        prmDeviceSettings.Add(new SqlParameter("@Devicename", 6));
        //    }
           
        //    return objConnection.ExecuteDataTable("spPayAttendanceDeviceSettings", prmDeviceSettings);
        //}
        #region IDisposable Members

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
    


}
