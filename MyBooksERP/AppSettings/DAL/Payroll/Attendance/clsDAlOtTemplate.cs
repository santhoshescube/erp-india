﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace MyBooksERP
{/*********************************************
       * Author       : Arun
       * Created On   : 19 Apr 2012
       * Purpose      : Ot template   DAL 
       * ******************************************/


    public class clsDALOtTemplate
    {


        private clsDTOOtTemplate MobjDTOtTemplate = null;
        private string strProcedureName = "spPayAttendanceOtTemplate";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALOtTemplate() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
        public clsDTOOtTemplate DTOOtTemplate
        {
            get
            {
                if (this.MobjDTOtTemplate == null)
                    this.MobjDTOtTemplate = new clsDTOOtTemplate();

                return this.MobjDTOtTemplate;
            }
            set
            {
                this.MobjDTOtTemplate = value;
            }
        }


        public DataTable GetOtTemplateDetails()
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 2)
                };
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);

        }
        public bool SaveOtTemplateDetails()
        {
            bool blnRetValue = false;
            foreach (clsDTOOtTemplateDetails clsDTOOtTemplateDet in DTOOtTemplate.DTOOtTemplateDetails)
            {
                this.sqlParameters = new List<SqlParameter>();
                this.sqlParameters.Add(new SqlParameter("@Mode", 1));
                this.sqlParameters.Add(new SqlParameter("@StartMin", clsDTOOtTemplateDet.StartMin));
                this.sqlParameters.Add(new SqlParameter("@EndMin", clsDTOOtTemplateDet.EndMin));
                this.sqlParameters.Add(new SqlParameter("@OTMin", clsDTOOtTemplateDet.OTMin));
                if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters) != 0)
                {
                    blnRetValue = true;
                }
            }

            return blnRetValue;
        }
        public bool DeleteOtTemplate()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",3)
                                  };
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }


    }

}
