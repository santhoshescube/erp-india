﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
/*
 * Created By       : Tijo
 * Created Date     : 22 Aug 2013
 * Purpose          : For Shift Schedule
*/
namespace MyBooksERP
{
    public class clsDALShiftSchedule
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOShiftSchedule objclsDTOShiftSchedule { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;
        string strProcName = "spPayShiftScheduleTransaction";

        public clsDALShiftSchedule(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public long GetRecordCount()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTOShiftSchedule.strSearchKey));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return Convert.ToInt64(MobjDataLayer.ExecuteScalar(strProcName, parameters));
        }

        public DataTable getEmployeeDetails(int intCompanyID, int intDepartmentID, int intDesignationID)
        {
            
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 2));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@DepartmentID", intDepartmentID));
            parameters.Add(new SqlParameter("@DesignationID", intDesignationID));
            parameters.Add(new SqlParameter("@IsArabicView", 0));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public int SaveShiftScheduleMaster()
        {
            if (objclsDTOShiftSchedule.intShiftScheduleID > 0)
                DeleteShiftScheduleDetails(objclsDTOShiftSchedule.intShiftScheduleID);

            parameters = new ArrayList();
            if (objclsDTOShiftSchedule.intShiftScheduleID == 0)
                parameters.Add(new SqlParameter("@Mode", 3)); // Insert
            else
                parameters.Add(new SqlParameter("@Mode", 5)); // Update

            parameters.Add(new SqlParameter("@ShiftScheduleID", objclsDTOShiftSchedule.intShiftScheduleID));
            parameters.Add(new SqlParameter("@ScheduleName", objclsDTOShiftSchedule.strScheduleName));
            parameters.Add(new SqlParameter("@ShiftID", objclsDTOShiftSchedule.intShiftID));
            parameters.Add(new SqlParameter("@FromDate", objclsDTOShiftSchedule.dtFromDate));
            parameters.Add(new SqlParameter("@ToDate", objclsDTOShiftSchedule.dtToDate));
            parameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objShiftScheduleID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters, out objShiftScheduleID) != 0)
            {
                objclsDTOShiftSchedule.intShiftScheduleID = objShiftScheduleID.ToInt32();

                if (objclsDTOShiftSchedule.intShiftScheduleID > 0)
                {
                    if (SaveShiftScheduleDetails(objclsDTOShiftSchedule.intShiftScheduleID) > 0)
                        return objclsDTOShiftSchedule.intShiftScheduleID;
                    else
                        return 0;
                }
            }
            return 0;
        }

        public int SaveShiftScheduleDetails(int intShiftScheduleID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@ShiftScheduleID", intShiftScheduleID));
            parameters.Add(new SqlParameter("@XmlShiftScheduleDetails", objclsDTOShiftSchedule.IlstclsDTOShiftScheduleDetails.ToXml()));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objoutRowsAffected = 0;
            if (MobjDataLayer.ExecuteNonQuery(strProcName, parameters, out objoutRowsAffected) != 0)
                return objoutRowsAffected.ToInt32();

            return 0;
        }

        public bool DeleteShiftScheduleDetails(int intShiftScheduleID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@ShiftScheduleID", intShiftScheduleID));
            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                return true;

            return false;
        }

        public bool DeleteShiftScheduleMaster()
        {
            if (DeleteShiftScheduleDetails(objclsDTOShiftSchedule.intShiftScheduleID))
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 6));
                parameters.Add(new SqlParameter("@ShiftScheduleID", objclsDTOShiftSchedule.intShiftScheduleID));
                if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public int getRecordRowNumber(int intShiftScheduleID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@ShiftScheduleID", intShiftScheduleID));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTOShiftSchedule.strSearchKey));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt32();
        }

        public DataTable DisplayShiftScheduleMaster(int intRowNum)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@RowNum", intRowNum));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTOShiftSchedule.strSearchKey));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public DataTable DisplayShiftScheduleDetails(int intShiftScheduleID, int intCompanyID, int intDepartmentID, int intDesignationID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@ShiftScheduleID", intShiftScheduleID));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@DepartmentID", intDepartmentID));
            parameters.Add(new SqlParameter("@DesignationID", intDesignationID));
            parameters.Add(new SqlParameter("@IsArabicView", 0));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public DataSet GetPrintShiftSchedule(int intShiftScheduleID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@ShiftScheduleID", intShiftScheduleID));
            return MobjDataLayer.ExecuteDataSet(strProcName, parameters);
        }

        public bool CheckShiftScheduleNameExists(int intShiftScheduleID, string strScheduleName)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@ShiftScheduleID", intShiftScheduleID));
            parameters.Add(new SqlParameter("@ScheduleName", strScheduleName));
            return Convert.ToBoolean(MobjDataLayer.ExecuteScalar(strProcName, parameters));
        }

        public void CheckShiftExists(DateTime dtFromDate, DateTime dtToDate, int intShiftScheduleID, string strScheduleName, long lngEmployeeID, 
            ref bool blnShiftExists, ref bool blnPaymentExists, ref bool blnAttendanceExists)
        {
            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 13));
                parameters.Add(new SqlParameter("@ShiftScheduleID", intShiftScheduleID));
                parameters.Add(new SqlParameter("@ScheduleName", strScheduleName));
                parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
                parameters.Add(new SqlParameter("@FromDate", dtFromDate));
                parameters.Add(new SqlParameter("@ToDate", dtToDate));

                DataSet dsTemp = MobjDataLayer.ExecuteDataSet(strProcName, parameters);

                if (dsTemp.Tables[0].Rows.Count > 0)
                {
                    blnShiftExists = (dsTemp.Tables[0].Rows[0][0].ToInt32() > 0 ? true : false);
                }

                if (dsTemp.Tables[1].Rows.Count > 0)
                {
                    blnPaymentExists = (dsTemp.Tables[1].Rows[0][0].ToInt32() > 0 ? true : false);
                }

                if (dsTemp.Tables[2].Rows.Count > 0)
                {
                    blnAttendanceExists = (dsTemp.Tables[2].Rows[0][0].ToInt32() > 0 ? true : false);
                }
            }
            catch 
            {
                blnShiftExists = blnPaymentExists = blnAttendanceExists = true; 
            }
        }

        public bool CheckIsSalaryExistsForDelete(long lngEmployeeID, DateTime dtFromDate, DateTime dtToDate)
        {
            bool blnIsSalaryExists = false;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 14));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            parameters.Add(new SqlParameter("@FromDate", dtFromDate));
            parameters.Add(new SqlParameter("@ToDate", dtToDate));
            DataTable datTemp = MobjDataLayer.ExecuteDataTable(strProcName, parameters);

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    blnIsSalaryExists = true;
            }

            return blnIsSalaryExists;
        }

        public bool CheckIsShiftScheduleExistsForDelete(int intShiftScheduleID, long lngEmployeeID, DateTime dtFromDate, DateTime dtToDate)
        {
            bool blnIsShiftScheduleExists = false;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 15));
            parameters.Add(new SqlParameter("@ShiftScheduleID", intShiftScheduleID));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            parameters.Add(new SqlParameter("@FromDate", dtFromDate));
            parameters.Add(new SqlParameter("@ToDate", dtToDate));
            DataTable datTemp = MobjDataLayer.ExecuteDataTable(strProcName, parameters);

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    blnIsShiftScheduleExists = true;
            }

            return blnIsShiftScheduleExists;
        }
    }
}