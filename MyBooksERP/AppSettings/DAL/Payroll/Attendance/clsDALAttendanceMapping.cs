﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
namespace MyBooksERP//.DAL.FingerTech
{ /*****************************************************
 * Created By       : Arun
 * Creation Date    : 17 Apr 2012
 * Description      : Handle Employee WorkCode Mapping for Attendance
 * ***************************************************/


     public class clsDALAttendanceMapping
    {


        ArrayList prmAtnMapping;
        ClsCommonUtility MobjClsCommonUtility;
        public List<clsDTOAttendanceMapping> lstobjclsDTOAtnMapping{get;set;}
        //public list<clsDTOAtnMapping PobjclsDTOAtnMapping { get; set; }
        public DataLayer objConnection { get; set; }

        public clsDALAttendanceMapping()
        {
            MobjClsCommonUtility = new ClsCommonUtility();
        }
        public bool SaveMappingInfo()
        {
            //prmAtnMapping = new ArrayList();
            //prmAtnMapping.Add(new SqlParameter("@Mode", 1));
            //prmAtnMapping.Add(new SqlParameter("@EmployeeID", PobjclsDTOAtnMapping.intEmployeeID));
            //prmAtnMapping.Add(new SqlParameter("@EquipmentID", PobjclsDTOAtnMapping.intEquipmentID));
            //prmAtnMapping.Add(new SqlParameter("@WorkID", PobjclsDTOAtnMapping.strWorkID));
            //prmAtnMapping.Add(new SqlParameter("@Active",PobjclsDTOAtnMapping.blnActive));
            //prmAtnMapping.Add(new SqlParameter("@TemplateMasterID", PobjclsDTOAtnMapping.intTemplateMasterID));
            //prmAtnMapping.Add(new SqlParameter("@SettingsID",PobjclsDTOAtnMapping.intsettingsID));
            //prmAtnMapping.Add(new SqlParameter("@Type", PobjclsDTOAtnMapping.intType));
            //prmAtnMapping.Add(new SqlParameter("@CompanyID",PobjclsDTOAtnMapping.intComapnyID));
            //if (objConnection.ExecuteNonQueryWithTran("spPayAttendanceMapping", prmAtnMapping) != 0)
            //{
            //    return true;
            //}
            //return false;
            foreach (clsDTOAttendanceMapping objclsDTOAtnMapping in lstobjclsDTOAtnMapping)
            {
                prmAtnMapping = new ArrayList();
                prmAtnMapping.Add(new SqlParameter("@Mode", 7));
                prmAtnMapping.Add(new SqlParameter("@TemplateMasterID", objclsDTOAtnMapping.intTemplateMasterID));
                prmAtnMapping.Add(new SqlParameter("@CompanyID", objclsDTOAtnMapping.intComapnyID));
                objConnection.ExecuteNonQuery("spPayAttendanceMapping", prmAtnMapping);
                break;
            }
            foreach (clsDTOAttendanceMapping objclsDTOAtnMapping in lstobjclsDTOAtnMapping)
            {
                prmAtnMapping = new ArrayList();
                prmAtnMapping.Add(new SqlParameter("@Mode", 1));
                prmAtnMapping.Add(new SqlParameter("@EmployeeID", objclsDTOAtnMapping.intEmployeeID));
                prmAtnMapping.Add(new SqlParameter("@EquipmentID", objclsDTOAtnMapping.intEquipmentID));
                prmAtnMapping.Add(new SqlParameter("@WorkID", objclsDTOAtnMapping.strWorkID));
                prmAtnMapping.Add(new SqlParameter("@Active", objclsDTOAtnMapping.blnActive));
                prmAtnMapping.Add(new SqlParameter("@TemplateMasterID", objclsDTOAtnMapping.intTemplateMasterID));
                prmAtnMapping.Add(new SqlParameter("@SettingsID", objclsDTOAtnMapping.intsettingsID));
                prmAtnMapping.Add(new SqlParameter("@CompanyID", objclsDTOAtnMapping.intComapnyID));
                objConnection.ExecuteNonQuery("spPayAttendanceMapping", prmAtnMapping);

            }
            return true;
        }

        public DataTable GetEmployees(int iEquipmentID, int iSettingsID,int intTemplateMasterID )
        {
            prmAtnMapping = new ArrayList();
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    prmAtnMapping.Add(new SqlParameter("@Mode", 8));
            //}
            //else
            //{
                prmAtnMapping.Add(new SqlParameter("@Mode", 5));
            //}
            prmAtnMapping.Add(new SqlParameter("@EquipmentID", iEquipmentID));
            prmAtnMapping.Add(new SqlParameter("@SettingsID", iSettingsID));
            prmAtnMapping.Add(new SqlParameter("@TemplateMasterID",intTemplateMasterID)); 
            return objConnection.ExecuteDataTable("spPayAttendanceMapping", prmAtnMapping);

        }
        public DataTable GetFingerTechDeviceSettings(int intDeviceTypeID)
        {
            prmAtnMapping = new ArrayList();
            prmAtnMapping.Add(new SqlParameter("@Mode", 3));
            prmAtnMapping.Add(new SqlParameter("@DeviceTypeID", intDeviceTypeID));
            return objConnection.ExecuteDataTable("spPayAttendanceMapping", prmAtnMapping);
        }
        public bool DeleteMappings(int iEquipmentID, int iSettingsID, int intTemplateMasterID)
        {
            prmAtnMapping = new ArrayList();
            prmAtnMapping.Add(new SqlParameter("@Mode", 4));
            prmAtnMapping.Add(new SqlParameter("@EquipmentID", iEquipmentID));
            prmAtnMapping.Add(new SqlParameter("@SettingsID", iSettingsID));
            prmAtnMapping.Add(new SqlParameter("@TemplateMasterID", intTemplateMasterID)); 
            if (objConnection.ExecuteNonQuery("spPayAttendanceMapping", prmAtnMapping) != 0)
            {
                return true;
            }
            return false;
        }
        public bool DeleteEmployeeMapping(int iEmpID, int iSettingsID, int intTemplateMasterID)
        {
            prmAtnMapping = new ArrayList();
            prmAtnMapping.Add(new SqlParameter("@Mode", 6));
            prmAtnMapping.Add(new SqlParameter("@EmployeeID", iEmpID));
            prmAtnMapping.Add(new SqlParameter("@SettingsID", iSettingsID));
            prmAtnMapping.Add(new SqlParameter("@TemplateMasterID", intTemplateMasterID));
            if (objConnection.ExecuteNonQuery("spPayAttendanceMapping", prmAtnMapping) != 0)
            {
                return true;
            }
            return false;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {

                return MobjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }
    }
}
