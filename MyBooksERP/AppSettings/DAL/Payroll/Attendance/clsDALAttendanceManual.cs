﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
namespace MyBooksERP
{
    public class clsDALAttendanceManual
    {
        ArrayList ParameterArray;
        public DataLayer objConnection { get; set; }
        public clsDTOAttendanceManual MobjclsDTOAttendanceManual { get; set; }
        ClsCommonUtility MobjClsCommonUtility;

        string strProcedureName = "spPayAttendanceManual";

        public clsDALAttendanceManual()
        {
            MobjClsCommonUtility = new ClsCommonUtility();
        }
        public DataTable GetAttendanceDetails(int intCurrentPage, int intPageRowCount)
        {
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 1));
            ParameterArray.Add(new SqlParameter("@CompanyID", MobjclsDTOAttendanceManual.intCompanyID));
            ParameterArray.Add(new SqlParameter("@WorkLocationID", MobjclsDTOAttendanceManual.intWorkLocationID));
            ParameterArray.Add(new SqlParameter("@DepartmentID", MobjclsDTOAttendanceManual.intDepartmentID));
            ParameterArray.Add(new SqlParameter("@Date", MobjclsDTOAttendanceManual.strDate));
            ParameterArray.Add(new SqlParameter("@PageIndex", intCurrentPage));
            ParameterArray.Add(new SqlParameter("@PageRowCount", intPageRowCount));
            return this.objConnection.ExecuteDataTable(strProcedureName, ParameterArray,1000);
        }

        public DataTable GetAttendanceDetailsPageCount(int intCurrentPage, int intPageRowCount)
        {
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 15));
            ParameterArray.Add(new SqlParameter("@CompanyID", MobjclsDTOAttendanceManual.intCompanyID));
            ParameterArray.Add(new SqlParameter("@WorkLocationID", MobjclsDTOAttendanceManual.intWorkLocationID));
            ParameterArray.Add(new SqlParameter("@DepartmentID", MobjclsDTOAttendanceManual.intDepartmentID));
            ParameterArray.Add(new SqlParameter("@Date", MobjclsDTOAttendanceManual.strDate));
            ParameterArray.Add(new SqlParameter("@PageIndex", intCurrentPage));
            ParameterArray.Add(new SqlParameter("@PageRowCount", intPageRowCount));
            return this.objConnection.ExecuteDataTable(strProcedureName, ParameterArray,1000);
        }

        public bool SaveAttendanceManual()
        {
            object iReturnID;

            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 2));
            //ParameterArray.Add(new SqlParameter("@XmlAttenDanceDetails", lstClsAttendanceManualDetails.ToXml()););
            ParameterArray.Add(new SqlParameter("@XmlAttenDanceDetails", MobjclsDTOAttendanceManual.strXmlDoc));
            SqlParameter sQlparam = new SqlParameter("@ReturnVal", SqlDbType.Int);
            sQlparam.Direction = ParameterDirection.ReturnValue;
            this.ParameterArray.Add(sQlparam);
            this.objConnection.ExecuteNonQuery(this.strProcedureName, this.ParameterArray, out iReturnID);
            return ((int)iReturnID > 0);
        }
        public bool SaveManualAttendanceNew(int AttendanceID, int EmployeeID, int CompanyID, string AttDate, int PolicyID, int ShiftID, string WorkTime, string OT, int AbsentTime, int AttendenceStatusID, int LeaveID, int LeaveType, int IsHalfDayLeave, int IsPaidLeave, int ConsequenceID, int WorkLocationID)
        {
            object iReturnID = 0;
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 2));
            ParameterArray.Add(new SqlParameter("@AttendanceID", AttendanceID));
            ParameterArray.Add(new SqlParameter("@EmployeeID", EmployeeID));
            ParameterArray.Add(new SqlParameter("@CompanyID", CompanyID));
            ParameterArray.Add(new SqlParameter("@Date", AttDate));
            ParameterArray.Add(new SqlParameter("@PolicyID", PolicyID));
            ParameterArray.Add(new SqlParameter("@ShiftID", ShiftID));
            ParameterArray.Add(new SqlParameter("@WorkTime", WorkTime));
            ParameterArray.Add(new SqlParameter("@OT", OT));
            ParameterArray.Add(new SqlParameter("@AbsentTime", AbsentTime));
            ParameterArray.Add(new SqlParameter("@AttendenceStatusID", AttendenceStatusID));
            ParameterArray.Add(new SqlParameter("@LeaveID", LeaveID));
            ParameterArray.Add(new SqlParameter("@LeaveTypeID", LeaveType));
            ParameterArray.Add(new SqlParameter("@IsHalfDay", IsHalfDayLeave));
            ParameterArray.Add(new SqlParameter("@IsPaid", IsPaidLeave));
            ParameterArray.Add(new SqlParameter("@ConsequenceID", ConsequenceID));
            ParameterArray.Add(new SqlParameter("@WorkLocationID", WorkLocationID));
            SqlParameter sQlparam = new SqlParameter("@ReturnVal", SqlDbType.Int);
            sQlparam.Direction = ParameterDirection.ReturnValue;
            this.ParameterArray.Add(sQlparam);
            this.objConnection.ExecuteNonQuery(this.strProcedureName, this.ParameterArray, out iReturnID);
            return ((int)iReturnID > 0);
        }


        public bool SaveManualAttendanceOT(int EmployeeID,string strDate, string sOTHour, decimal dOTDays)
        {
            object iReturnID = 0;
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 16));
            ParameterArray.Add(new SqlParameter("@EmployeeID", EmployeeID));
            ParameterArray.Add(new SqlParameter("@Date", strDate));
            ParameterArray.Add(new SqlParameter("@OTHour", sOTHour));
            ParameterArray.Add(new SqlParameter("@OTDays", dOTDays));
            this.objConnection.ExecuteNonQuery(this.strProcedureName, this.ParameterArray);
            return true;
        }

        public bool DeleteAttendanceManual()
        {
            object iReturnID = 0;
            this.ParameterArray = new ArrayList();
            this.ParameterArray.Add(new SqlParameter("@Mode", 4));
            this.ParameterArray.Add(new SqlParameter("@XmlAttenDanceDetails", MobjclsDTOAttendanceManual.strXmlDoc));
            SqlParameter sQlparam = new SqlParameter("@ReturnVal", SqlDbType.Int);
            sQlparam.Direction = ParameterDirection.ReturnValue;
            this.ParameterArray.Add(sQlparam);
            this.objConnection.ExecuteNonQuery(strProcedureName, ParameterArray, out iReturnID);
            return ((int)iReturnID > 0);
        }

        public DataTable GetEmployeeShiftInfo(int iEmpID, string sDate)
        {
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 3));
            ParameterArray.Add(new SqlParameter("@EmployeeID", iEmpID));
            ParameterArray.Add(new SqlParameter("@Date", sDate));
            return this.objConnection.ExecuteDataTable(strProcedureName, ParameterArray);
        }

        public bool IsSalaryReleased(int iEmpID, string sDate)
        {
            //Checking Salary Is Released or processed
            int iReturnID = 0;
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 6));
            ParameterArray.Add(new SqlParameter("@EmployeeID", iEmpID));
            ParameterArray.Add(new SqlParameter("@Date", sDate));
            iReturnID = Convert.ToInt32(this.objConnection.ExecuteScalar(strProcedureName, ParameterArray));
            return (iReturnID > 0);
        }

        //public bool IsVacationExists(int iEmpID, string sDate)
        //{
        //    //Checking Vaction Is Exists Or not
        //    int iReturnID = 0;
        //    ParameterArray = new ArrayList();
        //    ParameterArray.Add(new SqlParameter("@Mode", 11));
        //    ParameterArray.Add(new SqlParameter("@EmployeeID", iEmpID));
        //    ParameterArray.Add(new SqlParameter("@Date", sDate));
        //    iReturnID = Convert.ToInt32(this.objConnection.ExecuteScalar(strProcedureName, ParameterArray));
        //    return (iReturnID > 0);
        //}

        public DateTime GetEmployeeJoiningDate(int iEmpID)
        {
            DateTime dtJoiningDate;
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 7));
            ParameterArray.Add(new SqlParameter("@EmployeeID", iEmpID));
            dtJoiningDate = Convert.ToDateTime(this.objConnection.ExecuteScalar(strProcedureName, ParameterArray));
            return dtJoiningDate;
        }

        public int GetTotalWorkTime(int iEmpID, string strDate, int iAttendanceID)
        {
            int iTotalWorkTime = 0;
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 8));
            ParameterArray.Add(new SqlParameter("@EmployeeID", iEmpID));
            ParameterArray.Add(new SqlParameter("@Date", strDate));
            ParameterArray.Add(new SqlParameter("@AttendanceID", iAttendanceID));
            iTotalWorkTime = Convert.ToInt32(this.objConnection.ExecuteScalar(strProcedureName, ParameterArray));
            return iTotalWorkTime;
        }

        public int GetTotalTime(int iEmpID, string strDate, int iAttendanceID)
        {
            int iTotalTime = 0;
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 9));
            ParameterArray.Add(new SqlParameter("@EmployeeID", iEmpID));
            ParameterArray.Add(new SqlParameter("@Date", strDate));
            ParameterArray.Add(new SqlParameter("@AttendanceID", iAttendanceID));
            iTotalTime = Convert.ToInt32(this.objConnection.ExecuteScalar(strProcedureName, ParameterArray));
            return iTotalTime;
        }

        public bool IsOTExists(int iEmpID, string strDate, int iAttendanceID)
        {
            int iExists = 0;
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 10));
            ParameterArray.Add(new SqlParameter("@EmployeeID", iEmpID));
            ParameterArray.Add(new SqlParameter("@Date", strDate));
            ParameterArray.Add(new SqlParameter("@AttendanceID", iAttendanceID));
            iExists = Convert.ToInt32(this.objConnection.ExecuteScalar(strProcedureName, ParameterArray));
            return (iExists > 0);
        }

        public bool IsAttendanceManualExists(int iCmpID, string sDate, int iPrjID)
        {
            int iExists = 0;
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 12));
            ParameterArray.Add(new SqlParameter("@CompanyID", iCmpID));
            ParameterArray.Add(new SqlParameter("@Date", sDate));
            iExists = Convert.ToInt32(this.objConnection.ExecuteScalar(strProcedureName, ParameterArray));
            return (iExists > 0);
        }

        public bool IsWorkTimeExists(int iEmpID, string sDate, int iAtnID)
        {
            int iExists = 0;
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 13));
            ParameterArray.Add(new SqlParameter("@EmployeeID", iEmpID));
            ParameterArray.Add(new SqlParameter("@Date", sDate));
            ParameterArray.Add(new SqlParameter("@AttendanceID", iAtnID));
            iExists = Convert.ToInt32(this.objConnection.ExecuteScalar(strProcedureName, ParameterArray));
            return (iExists > 0);
        }

        public DataTable GetShiftOTDetails(int iShiftID)
        {
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 14));
            ParameterArray.Add(new SqlParameter("@ShiftID", iShiftID));
            return this.objConnection.ExecuteDataTable(strProcedureName, ParameterArray);
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MobjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }
    }
}