﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
namespace MyBooksERP
{
    public class clsDALLeaveEntry
    {

        #region DECLARATIONS 
        private List<SqlParameter> sqlParameters = null;
        #endregion
        #region PROPERTIES   
        public DataLayer objclsConnection { get; set; }
        public clsDTOLeaveEntry objclsDTOLeaveEntry { get; set; }
        #endregion
        #region METHODS      
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public int GetRowNumber(int iEmpID)
        {
            int intRownum = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 24), new SqlParameter("@EmployeeID", iEmpID) };
            intRownum = objclsConnection.ExecuteScalar("spPayEmployeeLeave", this.sqlParameters).ToInt32();
            return intRownum;
        }

        public int GetRecordCount()
        {
            int intRecordCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 23),
                new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID)};
            intRecordCount = objclsConnection.ExecuteScalar("spPayEmployeeLeave", this.sqlParameters).ToInt32();
            return intRecordCount;
        }

        public DateTime GetFinYearStartDate(int intCompanyID, string Currentdate)
        {
            DateTime Curfinyer = ClsCommonSettings.GetServerDate();
            string testdate;
            this.sqlParameters = new List<SqlParameter>();
             sqlParameters.Add(new SqlParameter("@Mode",27 ));
             sqlParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
             sqlParameters.Add(new SqlParameter("@CurrentDate", Currentdate));

            SqlDataReader sdrfinyer = objclsConnection.ExecuteReader("spPayEmployeeLeave", this.sqlParameters);
            if ((sdrfinyer != null && sdrfinyer.HasRows))
            {
                if (sdrfinyer.Read())
                {
                    Curfinyer = Convert.ToDateTime(sdrfinyer[0]);
                    testdate = sdrfinyer[0].ToString();
                }
            }
            sdrfinyer.Close();
            return Curfinyer;
        }

        public decimal GetLeaveTaken(int intCompanyID, int intLeavePolicyID, int IntEmployeeID, int intLeaveTypeID, string TestDate, out decimal DecBalanceleave)
        {

            decimal takenLeave = 0;
            this.sqlParameters = new List<SqlParameter> ();
             sqlParameters.Add(new SqlParameter("@Mode",28));
             sqlParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
             sqlParameters.Add(new SqlParameter("@LeavePolicyID", intLeavePolicyID));
             sqlParameters.Add(new SqlParameter("@EmployeeID", IntEmployeeID));
             sqlParameters.Add(new SqlParameter("@LeaveTypeID", intLeaveTypeID));
             sqlParameters.Add(new SqlParameter("@testDate", TestDate.ToDateTime()));
            SqlDataReader sdrfinyer = objclsConnection.ExecuteReader("spPayEmployeeLeave", this.sqlParameters);
            if (sdrfinyer.Read())
            {
                DecBalanceleave = sdrfinyer[0].ToDecimal();
                takenLeave = sdrfinyer["TakenLeaves"].ToDecimal();
            }
            else
                DecBalanceleave = 0;
            sdrfinyer.Close();
            return takenLeave;
        }
        public decimal GetCurrentLeaveTaken(int intLeaveID)
        {
            decimal takenLeave = 0;
            this.sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode", 38));
            sqlParameters.Add(new SqlParameter("@LeaveID", intLeaveID));
            SqlDataReader sdrfinyer = objclsConnection.ExecuteReader("spPayEmployeeLeave", this.sqlParameters);
            if (sdrfinyer.Read())
            {
                takenLeave = sdrfinyer["NoOfDays"].ToDecimal();
            }
            sdrfinyer.Close();
            return takenLeave;
        }
        public bool SaveLeaveEntry(bool bStatus, object iLeaveID1)
        {
            this.sqlParameters = new List<SqlParameter>();

             sqlParameters.Add(new SqlParameter("@EmployeeID", objclsDTOLeaveEntry.intEmployeeID));
             sqlParameters.Add(new SqlParameter("@LeaveTypeID", objclsDTOLeaveEntry.intLeaveTypeID));
             sqlParameters.Add(new SqlParameter("@HalfDay", objclsDTOLeaveEntry.blnHalfDay));
             sqlParameters.Add(new SqlParameter("@LeaveDateFrom", Convert.ToDateTime(objclsDTOLeaveEntry.strLeaveDateFrom)));
             sqlParameters.Add(new SqlParameter("@LeaveDateTo", Convert.ToDateTime(objclsDTOLeaveEntry.strLeaveDateTo)));
             sqlParameters.Add(new SqlParameter("@RejoinDate", Convert.ToDateTime(objclsDTOLeaveEntry.strRejoinDate)));
             sqlParameters.Add(new SqlParameter("@Remarks", objclsDTOLeaveEntry.strRemarks));
             sqlParameters.Add(new SqlParameter("@CompanyID", objclsDTOLeaveEntry.intCompanyID));
            if (bStatus == false)
            {
                 sqlParameters.Add(new SqlParameter("@LeaveID", objclsDTOLeaveEntry.intLeaveID));
            }
             sqlParameters.Add(new SqlParameter("@NoOfDays", objclsDTOLeaveEntry.dblNoOfDays));
             sqlParameters.Add(new SqlParameter("@NoOfHolidays", objclsDTOLeaveEntry.dblNoOfHolidays));
             sqlParameters.Add(new SqlParameter("@PaidLeave", objclsDTOLeaveEntry.blnPaidLeave));

            if (bStatus == true)
                 sqlParameters.Add(new SqlParameter("@Mode", 13));
            else
                 sqlParameters.Add(new SqlParameter("@Mode", 14));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            this.sqlParameters.Add(objParam);
            objclsConnection.ExecuteNonQuery("spPayEmployeeLeave", this.sqlParameters, out iLeaveID1);
            if (iLeaveID1.ToInt32() > 0)
            {
                return true;

            }
            else
            {
                return false;
            }


        }

        public bool DeleteAbsentEntryFromAttendance()
        {
            bool blnRetValue = false;

            this.sqlParameters = new List<SqlParameter>();
             sqlParameters.Add(new SqlParameter("@Mode", 31));
             sqlParameters.Add(new SqlParameter("@EmployeeID", objclsDTOLeaveEntry.intEmployeeID));
             sqlParameters.Add(new SqlParameter("@LeaveDateFromDate", Convert.ToDateTime(objclsDTOLeaveEntry.strLeaveDateFrom).ToString("dd-MMM-yyyy")));
             sqlParameters.Add(new SqlParameter("@LeaveDateToDate", Convert.ToDateTime(objclsDTOLeaveEntry.strLeaveDateTo).ToString("dd-MMM-yyyy")));
            if (objclsConnection.ExecuteNonQuery("spPayEmployeeLeave", this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }

        public bool UpdateAttendance()   //IF full day attendance is Exists Then Leave Entry is HAlfday then Attendance Status is Updated AS halfday
        {
            bool blnRetValue = false;

             this.sqlParameters = new List<SqlParameter>();
             sqlParameters.Add(new SqlParameter("@Mode", 32));
             sqlParameters.Add(new SqlParameter("@EmployeeID", objclsDTOLeaveEntry.intEmployeeID));
             sqlParameters.Add(new SqlParameter("@LeaveDateFromDate", Convert.ToDateTime(objclsDTOLeaveEntry.strLeaveDateFrom).ToString("dd-MMM-yyyy")));
            if (objclsConnection.ExecuteNonQuery("spPayEmployeeLeave", this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }

        public bool DeleteEmployeeLeaveSummaryDetails(int intCompanyID, DateTime dFinYearDate, int IntLeavePolicyID, int IntEmployeeID, int IntLeaveTypeID)
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode", 33));
            sqlParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            sqlParameters.Add(new SqlParameter("@dFinYearDate ", Convert.ToDateTime(dFinYearDate.ToString("dd-MMM-yyyy"))));
            sqlParameters.Add(new SqlParameter("@LeavePolicyID", IntLeavePolicyID));
            sqlParameters.Add(new SqlParameter("@EmployeeID", IntEmployeeID));
            sqlParameters.Add(new SqlParameter("@LeaveTypeID", IntLeaveTypeID));
            if (objclsConnection.ExecuteNonQuery("spPayEmployeeLeave", this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;

        }

        public DataSet GetBalanceAndHolidaysdetails(int IntCompany, int IntEmployeeID, int IntLeaveTypeID, int MiLeavePolicyID, string s1, string s2, bool MbAddStatus)
        {
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@CompanyID", IntCompany));
            sqlParameters.Add(new SqlParameter("@EmployeeID", IntEmployeeID));
            sqlParameters.Add(new SqlParameter("@LeavetypeID", IntLeaveTypeID));
            sqlParameters.Add(new SqlParameter("@Leavepolicyid", MiLeavePolicyID));
            sqlParameters.Add(new SqlParameter("@Fromdate", s1));
            sqlParameters.Add(new SqlParameter("@Todate", s2));
            if (MbAddStatus == true)
            {
                sqlParameters.Add(new SqlParameter("@Type", 0));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@Type", 1));
            }
            DataSet DtSet = objclsConnection.ExecuteDataSet("spPayEmployeeLeaveSummaryFromLeaveEntry", sqlParameters);

            return DtSet;

        }

        public decimal GetMonthLeaveTaken(int IntEmployeeID, string IntLeaveDateFrom, string IntLeaveDateTo, int IntCompany, int IntLeaveTypeID)
        {

            decimal dFulldayTaken = 0.0M;

            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode", 34));
            sqlParameters.Add(new SqlParameter("@EmployeeID", IntEmployeeID));
            sqlParameters.Add(new SqlParameter("@FromDate1", IntLeaveDateFrom));
            sqlParameters.Add(new SqlParameter("@ToDate1", IntLeaveDateTo));
            sqlParameters.Add(new SqlParameter("@CompanyID", IntCompany));
            sqlParameters.Add(new SqlParameter("@LeavetypeID", IntLeaveTypeID));

            DataTable dt = objclsConnection.ExecuteDataTable("spPayEmployeeLeave", this.sqlParameters);
            if (dt.Rows.Count > 0)
            {
                dFulldayTaken = dt.Rows[0][0].ToDecimal(); ;
            }
            return dFulldayTaken;
        }

        public int GetHoildaysWithOffDays(string strLeaveDateFrom, string strLeaveDateTo, int IntEmployeeID)
        {

            int Holidaycnt = 0;
 
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode", 35));
            sqlParameters.Add(new SqlParameter("@EmployeeID", IntEmployeeID));
            sqlParameters.Add(new SqlParameter("@FromDate1", strLeaveDateFrom));
            sqlParameters.Add(new SqlParameter("@ToDate1", strLeaveDateTo));
             DataTable dt = objclsConnection.ExecuteDataTable("spPayEmployeeLeave", this.sqlParameters);
             if (dt.Rows.Count > 0)
             {
                 Holidaycnt = dt.Rows[0][0].ToInt32();
             }
             return Holidaycnt;
        }

        public bool SearchLeave(int IntRowNum, string strSearchText,out  int IntTotalCount)
        {
            IntTotalCount = 0;
            //if (datCompany.Rows[0]["VendorID"] != DBNull.Value) objclsDTOSTSalesMaster.intVendorID = Convert.ToInt32(datCompany.Rows[0]["VendorID"]); else objclsDTOSTSalesMaster.intVendorID = 0;
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode", 21));
            sqlParameters.Add(new SqlParameter("@RowNum", IntRowNum));
            sqlParameters.Add(new SqlParameter("@SearchText", strSearchText));
            sqlParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            DataSet DtSet = objclsConnection.ExecuteDataSet("spPayEmployeeLeave", sqlParameters);
            if (DtSet.Tables.Count == 2)
            {  
                if (DtSet.Tables[0].Rows.Count > 0)
                {
                
                    if (DtSet.Tables[0].Rows[0]["LeaveID"] != DBNull.Value) objclsDTOLeaveEntry.intLeaveID = DtSet.Tables[0].Rows[0]["LeaveID"].ToInt32(); else objclsDTOLeaveEntry.intLeaveID = 0;
                    if (DtSet.Tables[0].Rows[0]["EmployeeID"] != DBNull.Value) objclsDTOLeaveEntry.intEmployeeID = DtSet.Tables[0].Rows[0]["EmployeeID"].ToInt32(); else objclsDTOLeaveEntry.intEmployeeID = 0;
                    if (DtSet.Tables[0].Rows[0]["LeaveTypeID"] != DBNull.Value) objclsDTOLeaveEntry.intLeaveTypeID = DtSet.Tables[0].Rows[0]["LeaveTypeID"].ToInt32(); else objclsDTOLeaveEntry.intLeaveTypeID = 0;
                    if (DtSet.Tables[0].Rows[0]["HalfDay"] != DBNull.Value) objclsDTOLeaveEntry.blnHalfDay = DtSet.Tables[0].Rows[0]["HalfDay"].ToBoolean(); else objclsDTOLeaveEntry.blnHalfDay = false;
                    if (DtSet.Tables[0].Rows[0]["LeaveDateFrom"] != DBNull.Value) objclsDTOLeaveEntry.strLeaveDateFrom = DtSet.Tables[0].Rows[0]["LeaveDateFrom"].ToString(); else objclsDTOLeaveEntry.strLeaveDateFrom = "";
                    if (DtSet.Tables[0].Rows[0]["LeaveDateTo"] != DBNull.Value) objclsDTOLeaveEntry.strLeaveDateTo = DtSet.Tables[0].Rows[0]["LeaveDateTo"].ToString(); else objclsDTOLeaveEntry.strLeaveDateTo = "";
                    if (DtSet.Tables[0].Rows[0]["RejoinDate"] != DBNull.Value) objclsDTOLeaveEntry.strRejoinDate = DtSet.Tables[0].Rows[0]["RejoinDate"].ToString(); else objclsDTOLeaveEntry.strRejoinDate = "";
                    if (DtSet.Tables[0].Rows[0]["Remarks"] != DBNull.Value) objclsDTOLeaveEntry.strRemarks = DtSet.Tables[0].Rows[0]["Remarks"].ToString(); else objclsDTOLeaveEntry.strRemarks = "";
                    if (DtSet.Tables[0].Rows[0]["CompanyID"] != DBNull.Value) objclsDTOLeaveEntry.intCompanyID = DtSet.Tables[0].Rows[0]["CompanyID"].ToInt32(); else objclsDTOLeaveEntry.intCompanyID = 0;
                    if (DtSet.Tables[0].Rows[0]["NoOfDays"] != DBNull.Value) objclsDTOLeaveEntry.dblNoOfDays = DtSet.Tables[0].Rows[0]["NoOfDays"].ToInt32(); else objclsDTOLeaveEntry.dblNoOfDays = 0;
                    if (DtSet.Tables[0].Rows[0]["NoOfHolidays"] != DBNull.Value) objclsDTOLeaveEntry.dblNoOfHolidays = DtSet.Tables[0].Rows[0]["NoOfHolidays"].ToInt32(); else objclsDTOLeaveEntry.dblNoOfHolidays = 0;
                    if (DtSet.Tables[0].Rows[0]["PaidLeave"] != DBNull.Value) objclsDTOLeaveEntry.blnPaidLeave = DtSet.Tables[0].Rows[0]["PaidLeave"].ToBoolean(); else objclsDTOLeaveEntry.blnPaidLeave = false;
                    if (DtSet.Tables[0].Rows[0]["IsFromAttendence"] != DBNull.Value) objclsDTOLeaveEntry.blnFromAttendence = DtSet.Tables[0].Rows[0]["IsFromAttendence"].ToBoolean(); else objclsDTOLeaveEntry.blnFromAttendence = false;

                }
                if (DtSet.Tables[1].Rows.Count > 0)
                {
                    if (DtSet.Tables[1].Rows[0]["RecordCount"] != DBNull.Value) IntTotalCount = DtSet.Tables[1].Rows[0]["RecordCount"].ToInt32(); else IntTotalCount = 0;
                }
          
            }
            return (IntTotalCount > 0);
        }

        public bool DisplayLeave(int IntRecordID, int IntEmpID)

        {
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode", 15));
            sqlParameters.Add(new SqlParameter("@RowNum", IntRecordID));
            sqlParameters.Add(new SqlParameter("@EmployeeID", IntEmpID));
            sqlParameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            sqlParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            DataSet DtSet = objclsConnection.ExecuteDataSet("spPayEmployeeLeave", sqlParameters);
            if (DtSet.Tables[0].Rows.Count > 0)
            {
                if (DtSet.Tables[0].Rows[0]["LeaveID"] != DBNull.Value) objclsDTOLeaveEntry.intLeaveID = DtSet.Tables[0].Rows[0]["LeaveID"].ToInt32(); else objclsDTOLeaveEntry.intLeaveID = 0;
                if (DtSet.Tables[0].Rows[0]["EmployeeID"] != DBNull.Value) objclsDTOLeaveEntry.intEmployeeID = DtSet.Tables[0].Rows[0]["EmployeeID"].ToInt32(); else objclsDTOLeaveEntry.intEmployeeID = 0;
                if (DtSet.Tables[0].Rows[0]["LeaveTypeID"] != DBNull.Value) objclsDTOLeaveEntry.intLeaveTypeID = DtSet.Tables[0].Rows[0]["LeaveTypeID"].ToInt32(); else objclsDTOLeaveEntry.intLeaveTypeID = 0;
                if (DtSet.Tables[0].Rows[0]["HalfDay"] != DBNull.Value) objclsDTOLeaveEntry.blnHalfDay = DtSet.Tables[0].Rows[0]["HalfDay"].ToBoolean(); else objclsDTOLeaveEntry.blnHalfDay = false;
                if (DtSet.Tables[0].Rows[0]["LeaveDateFrom"] != DBNull.Value) objclsDTOLeaveEntry.strLeaveDateFrom = DtSet.Tables[0].Rows[0]["LeaveDateFrom"].ToString(); else objclsDTOLeaveEntry.strLeaveDateFrom = "";
                if (DtSet.Tables[0].Rows[0]["LeaveDateTo"] != DBNull.Value) objclsDTOLeaveEntry.strLeaveDateTo = DtSet.Tables[0].Rows[0]["LeaveDateTo"].ToString(); else objclsDTOLeaveEntry.strLeaveDateTo = "";
                if (DtSet.Tables[0].Rows[0]["RejoinDate"] != DBNull.Value) objclsDTOLeaveEntry.strRejoinDate = DtSet.Tables[0].Rows[0]["RejoinDate"].ToString(); else objclsDTOLeaveEntry.strRejoinDate = "";
                if (DtSet.Tables[0].Rows[0]["Remarks"] != DBNull.Value) objclsDTOLeaveEntry.strRemarks = DtSet.Tables[0].Rows[0]["Remarks"].ToString(); else objclsDTOLeaveEntry.strRemarks = "";
                if (DtSet.Tables[0].Rows[0]["CompanyID"] != DBNull.Value) objclsDTOLeaveEntry.intCompanyID = DtSet.Tables[0].Rows[0]["CompanyID"].ToInt32(); else objclsDTOLeaveEntry.intCompanyID = 0;
                if (DtSet.Tables[0].Rows[0]["NoOfDays"] != DBNull.Value) objclsDTOLeaveEntry.dblNoOfDays = DtSet.Tables[0].Rows[0]["NoOfDays"].ToInt32(); else objclsDTOLeaveEntry.dblNoOfDays = 0;
                if (DtSet.Tables[0].Rows[0]["NoOfHolidays"] != DBNull.Value) objclsDTOLeaveEntry.dblNoOfHolidays = DtSet.Tables[0].Rows[0]["NoOfHolidays"].ToInt32(); else objclsDTOLeaveEntry.dblNoOfHolidays = 0;
                if (DtSet.Tables[0].Rows[0]["PaidLeave"] != DBNull.Value) objclsDTOLeaveEntry.blnPaidLeave = DtSet.Tables[0].Rows[0]["PaidLeave"].ToBoolean(); else objclsDTOLeaveEntry.blnPaidLeave = false;
                if (DtSet.Tables[0].Rows[0]["IsFromAttendence"] != DBNull.Value) objclsDTOLeaveEntry.blnFromAttendence = DtSet.Tables[0].Rows[0]["IsFromAttendence"].ToBoolean(); else objclsDTOLeaveEntry.blnFromAttendence = false;
            }
            return true;
        }

        public bool DeleteLeave(int IntLeaveID)
        {
            int iresultID;
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add( new SqlParameter("@Mode", 16));
            sqlParameters.Add(new SqlParameter("@LeaveID", IntLeaveID));
            iresultID= objclsConnection.ExecuteNonQuery("spPayEmployeeLeave", sqlParameters);
            return (iresultID>0);

        }

        public void CheckLeavestructures(int IntCompanyID, int IntEmployeeID,int IntLeaveTypeID,int MiLeavepolicyID, string s1 )
            
        {
                sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("@CompanyID", IntCompanyID));
                sqlParameters.Add(new SqlParameter("@EmployeeID",IntEmployeeID));
                sqlParameters.Add(new SqlParameter("@LeavetypeID", IntLeaveTypeID));
                sqlParameters.Add(new SqlParameter("@Leavepolicyid", MiLeavepolicyID));
                sqlParameters.Add( new SqlParameter("@Fromdate", s1));
                sqlParameters.Add(new SqlParameter("@type", 1));
                objclsConnection.ExecuteNonQuery("PayEmployeeLeaveSummary",sqlParameters);

        }

        public DataSet DisplayEmployeeLeaveEntry()
        {
            sqlParameters = new List<SqlParameter>();

            sqlParameters.Add(new SqlParameter("@Mode", 36));
            sqlParameters.Add(new SqlParameter("@LeaveID", objclsDTOLeaveEntry.intLeaveID));
            return objclsConnection.ExecuteDataSet("spPayEmployeeLeave", sqlParameters);
        }

        #endregion
   
    }
}