﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Data;

namespace MyBooksERP
{
    public class clsDALLeaveOpening
    {

        private clsDTOLeaveOpening MobjDTOLeaveOpening= null;
        private string strProcedureName = "spEmployeeLeaveOpening";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALLeaveOpening() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
        public clsDTOLeaveOpening DTOLeaveOpening
        {
            get
            {
                if (this.MobjDTOLeaveOpening == null)
                    this.MobjDTOLeaveOpening = new clsDTOLeaveOpening();

                return this.MobjDTOLeaveOpening;
            }
            set
            {
                this.MobjDTOLeaveOpening = value;
            }
        }



        public DataTable getDetailsInComboBox(int intType)
        {

            this.sqlParameters = new List<SqlParameter> 
           { 
            new SqlParameter("@Mode", intType) ,
            new SqlParameter("@UserID",  ClsCommonSettings.UserID),
           };
            return this.DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }

            return null;
        }



        public DataTable GetEmployeeList(int UserID,int CompanyID)
        {
            this.sqlParameters = new List<SqlParameter> 
           { 
            new SqlParameter("@Mode", 2) ,
            new SqlParameter("@UserID", UserID),
            new SqlParameter("@CompanyID", CompanyID),
           };
            return this.DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);

        }

        public void SaveEmployeeLeave()
        {

            foreach (clsDTOLeaveOpeningDetail objclsDTOLeaveOpeningDetail in MobjDTOLeaveOpening.lstclsDTOLeaveOpeningDetail)
                {
                    ArrayList alParameters = new ArrayList();
                    alParameters.Add(new SqlParameter("@Mode", objclsDTOLeaveOpeningDetail.Mode));
                    alParameters.Add(new SqlParameter("@EmployeeID", Convert.ToInt32(objclsDTOLeaveOpeningDetail.EmployeeID)));
                    alParameters.Add(new SqlParameter("@TakenLeaves", Convert.ToDouble(objclsDTOLeaveOpeningDetail.TakenLeaves)));
                    alParameters.Add(new SqlParameter("@PaidDays", Convert.ToDouble(objclsDTOLeaveOpeningDetail.PaidDays)));
                    alParameters.Add(new SqlParameter("@TakenTickets", Convert.ToInt32(objclsDTOLeaveOpeningDetail.TakenTickets)));
                    alParameters.Add(new SqlParameter("@PaidTicketAmount", Convert.ToDouble(objclsDTOLeaveOpeningDetail.PaidTicketAmount)));
                    this.DataLayer.ExecuteDataTable("spEmployeeLeaveOpening",alParameters);
               }


              

        }


    }
}
