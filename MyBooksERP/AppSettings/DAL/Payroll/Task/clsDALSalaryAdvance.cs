﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    /*********************************************
      * Author       : Arun
      * Created On   : 12 Apr 2012
      * Purpose      : Salary Advance Database Transactions 
      * ******************************************/
   public class clsDALSalaryAdvance
    {
        private clsDTOSalaryAdvance MobjDTOSalaryAdvance= null;
        private string strProcedureName = "spPaySalaryAdvance";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALSalaryAdvance() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
        public clsDTOSalaryAdvance DTOSalaryAdvance 
        {
            get
            {
                if (this.MobjDTOSalaryAdvance == null)
                    this.MobjDTOSalaryAdvance = new clsDTOSalaryAdvance();

                return this.MobjDTOSalaryAdvance;
            }
            set
            {
                this.MobjDTOSalaryAdvance = value;
            }
        }
       /// <summary>
       /// Get RowNumber Of Corresponding ID
       /// </summary>
       /// <param name="intSalaryAdvanceID"></param>
       /// <returns></returns>
        public int GetRowNumber(int intSalaryAdvanceID)
        {
            int intRownum = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 6), new SqlParameter("@SalaryAdvanceID", intSalaryAdvanceID) };
            intRownum = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRownum;
        }
        /// <summary>
        /// Get the total record Count
        /// </summary>
        /// <returns></returns>
        public int GetRecordCount()
        {
            int intRecordCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 5),
                new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID)};
            intRecordCount = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRecordCount;
        }



        /// <summary>
        /// Salary advancce master Save
        /// </summary>
        /// <returns></returns>
        public bool SalaryAdvanceSave()
        {
            bool blnRetValue = false;
            object objSalaryAdvanceID = 0;
            this.sqlParameters = new List<SqlParameter>();

            if (DTOSalaryAdvance.SalaryAdvanceID > 0)
            {

                this.sqlParameters.Add(new SqlParameter("@Mode", 2));
                this.sqlParameters.Add(new SqlParameter("@SalaryAdvanceID", this.DTOSalaryAdvance.SalaryAdvanceID));
            }
            else
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", 1));
            }

            this.sqlParameters.Add(new SqlParameter("@EmployeeID", this.DTOSalaryAdvance.EmployeeID));
            this.sqlParameters.Add(new SqlParameter("@Date", this.DTOSalaryAdvance.Date));
            this.sqlParameters.Add(new SqlParameter("@Amount", this.DTOSalaryAdvance.Amount));
            this.sqlParameters.Add(new SqlParameter("@Remarks", this.DTOSalaryAdvance.Remarks));
            this.sqlParameters.Add(new SqlParameter("@TransactionTypeID", this.DTOSalaryAdvance.TransactionTypeID));
            if (DTOSalaryAdvance.TransactionTypeID == (int)PaymentModes.Bank)
            {
                this.sqlParameters.Add(new SqlParameter("@AccountID", this.DTOSalaryAdvance.AccountID));
                this.sqlParameters.Add(new SqlParameter("@ChequeNumber", this.DTOSalaryAdvance.ChequeNumber));
                this.sqlParameters.Add(new SqlParameter("@ChequeDate", this.DTOSalaryAdvance.ChequeDate));
            }

            this.sqlParameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            this.sqlParameters.Add(objParam);
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out objSalaryAdvanceID) != 0)
            {
                this.DTOSalaryAdvance.SalaryAdvanceID = (int)objSalaryAdvanceID;
                blnRetValue = (int)objSalaryAdvanceID>0? true:false ;
            }
            return blnRetValue;

        }

        public bool DisplaySalaryAdvanceinfo(int intRowNumber)
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 3),
                new SqlParameter("@RowNumber", intRowNumber),
                new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID)};
            SqlDataReader sdr = this.DataLayer.ExecuteReader(this.strProcedureName, this.sqlParameters);
            if (sdr.Read())
            {
                DTOSalaryAdvance.SalaryAdvanceID = sdr["SalaryAdvanceID"].ToInt32();
                DTOSalaryAdvance.EmployeeID = sdr["EmployeeID"].ToInt32();
                DTOSalaryAdvance.Date = sdr["Date"].ToStringCustom();
                DTOSalaryAdvance.Amount = sdr["Amount"].ToDecimal();
                DTOSalaryAdvance.Remarks = sdr["Remarks"].ToStringCustom();
                DTOSalaryAdvance.IsSettled = sdr["IsSettled"].ToBoolean();
                DTOSalaryAdvance.TransactionTypeID = sdr["TransactionTypeID"].ToInt32();
                DTOSalaryAdvance.AccountID = sdr["AccountID"].ToInt32();
                DTOSalaryAdvance.ChequeNumber = sdr["ChequeNumber"].ToStringCustom();
                DTOSalaryAdvance.ChequeDate = sdr["ChequeDate"].ToStringCustom();
                blnRetValue = true;
            }
            sdr.Close();
            return blnRetValue;
        }
        public bool DeleteSalaryAdvanceInfo()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",4),
                                    new SqlParameter("@SalaryAdvanceID", this.DTOSalaryAdvance.SalaryAdvanceID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }
        public bool IsSalaryAdavanceIsSettled(int intSalaryAdvanceID)
        {
            bool blnIsSettled = false;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 7), new SqlParameter("@SalaryAdvanceID", intSalaryAdvanceID) };
            blnIsSettled = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToBoolean();
            return blnIsSettled;
        }

        public bool IsSalaryProcessed(int iEmpID,string strDate)
        {
            
            int intEmpID = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 8), new SqlParameter("@Date", strDate),
            new SqlParameter("@EmployeeID", iEmpID)};
            intEmpID = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intEmpID>0);
        }
        public bool GetEmployeeJoiningDate(int iEmpID,ref string strDate, ref decimal decBasicPay ,ref string strSalAdvPercent)
        {
            //, ref decimal decBasicPay ,ref string strSalAdvPercent
            strDate = "";
            decBasicPay = 0;
            strSalAdvPercent = "";
            
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 9),
                new SqlParameter("@EmployeeID", iEmpID)};
            DataTable dt = this.DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    strDate = dt.Rows[0]["DateofJoining"].ToStringCustom();
                    decBasicPay=dt.Rows[0]["BasicPay"].ToDecimal();
                    strSalAdvPercent = dt.Rows[0]["SalAdvPercent"].ToStringCustom();
                }
            }
            return (dt.Rows.Count > 0);
        }

        public bool GetCompanyFinancialYearStartDate(int iEmpID, ref string strDate)
        {
            strDate = "";
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 10),
                new SqlParameter("@EmployeeID", iEmpID)};
            strDate = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToStringCustom();
            return (strDate != string.Empty);
        }

        public bool IsSalAdvAccountExists(int intEmpID)
        {
            int intIsExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 16), new SqlParameter("@EmployeeID", intEmpID) };
            intIsExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intIsExists>0);
        }
        public int GetEmployeeWorkStatus(int intEmpID)
        {
            int intWorkStausID = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 11) ,
            new SqlParameter("@EmployeeID", intEmpID)};
            intWorkStausID = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intWorkStausID;
        }
        public bool GetEmployeeSalaryStructreDetails(int intEmpID, ref int intPayCalculationTypeID,ref int intPaymentClassificationID,ref string strDate )
        {
            intPayCalculationTypeID = 0;
            intPaymentClassificationID = 0;
            strDate = "";
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 12) ,
            new SqlParameter("@EmployeeID", intEmpID)};
            SqlDataReader sdr = this.DataLayer.ExecuteReader(this.strProcedureName, this.sqlParameters);
            if (sdr.Read())
            {
                intPayCalculationTypeID = sdr["PayCalculationTypeID"].ToInt32();
                intPaymentClassificationID = sdr["PaymentClassificationID"].ToInt32();
                strDate = sdr["DateofJoining"].ToStringCustom();
                blnRetValue = true;

            }
            sdr.Close();
            return blnRetValue;
        }
        public decimal GetEmployeeBasicPay(int intEmpID)
        {
            decimal decBasicPay = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 13) ,
            new SqlParameter("@EmployeeID", intEmpID)};
            decBasicPay = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToDecimal();
            return decBasicPay;
        }
        public bool GetSalaryAdvancePercentage(ref decimal decDefaultValue, ref decimal decConfigurationValue)
        {
            decDefaultValue = 0;
            decConfigurationValue = 0;

            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 14) ,
            };
            SqlDataReader sdr = this.DataLayer.ExecuteReader(this.strProcedureName, this.sqlParameters);
            if (sdr.Read())
            {
                decDefaultValue = sdr["DefaultValue"].ToDecimal();
                decConfigurationValue = sdr["ConfigurationValue"].ToDecimal();
                blnRetValue = true;

            }
            sdr.Close();
            return blnRetValue;
        }
        public decimal GetMonthlyTotalAdvanceAmount(int intAdvanceID,int intEmpID,string strDate)
        {
            decimal decTotalAmount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 15) ,
            new SqlParameter("@SalaryAdvanceID", intAdvanceID),
            new SqlParameter("@EmployeeID", intEmpID),
            new SqlParameter("@Date", strDate)};
            decTotalAmount = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToDecimal();
            return decTotalAmount;

        }
        public decimal GetExchangeRate(int intEmpID,ref string strCmpCurrency)
        {//Getting Exchange rate between EmployeeCurrency and CompanyCurrency
            decimal decExchangeRate = 0;
            strCmpCurrency = "";
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 17) ,
                new SqlParameter("@EmployeeID", intEmpID)
            };
            SqlDataReader sdr = this.DataLayer.ExecuteReader(this.strProcedureName, this.sqlParameters);
            if (sdr.Read())
            {
                decExchangeRate = sdr["ExchangeRate"].ToDecimal();
                strCmpCurrency = sdr["CompanyCurrency"].ToStringCustom();
                

            }
            sdr.Close();
            return decExchangeRate;

        }
        /// <summary>
        /// 
        /// return datatable  for filling combos
        /// </summary>
        /// <param name="saFieldValues"></param>
        /// <returns></returns>
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }


    }
}
