﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyBooksERP
{
    public class clsDALLeaveExtension : DataLayer
    {
        #region DECLARATIONS
        //private clsDTOLeaveExtension MobjDTOLeaveExtension = null;
        private string strProcedureName = "spEmployeeLeaveExtension";
        ArrayList parameters = null;
        #endregion
        #region PROPERTIES
        public clsDTOLeaveExtension PobjDTOLeaveExtension { get; set; }
        #endregion
        #region METHODS
        #region SaveLeaveExtension
        public bool SaveLeaveExtension(bool MAddStatus)
        {
            parameters = new ArrayList();
            if (MAddStatus)
            {
                parameters.Add(new SqlParameter("@Mode", "InsertLeaveExtension"));
            }
            else
            {
                parameters.Add(new SqlParameter("@Mode", "UpdateLeaveExtension"));
                parameters.Add(new SqlParameter("@ExtensionID", PobjDTOLeaveExtension.ExtensionID));
            }
            parameters.Add(new SqlParameter("@EmployeeID", PobjDTOLeaveExtension.EmployeeID));
            parameters.Add(new SqlParameter("@MonthYear", PobjDTOLeaveExtension.MonthYear));
            parameters.Add(new SqlParameter("@LeaveExtensionTypeID", PobjDTOLeaveExtension.LeaveExtensionTypeID));
            if (PobjDTOLeaveExtension.LeaveTypeID > 0)
                parameters.Add(new SqlParameter("@LeaveTypeID", PobjDTOLeaveExtension.LeaveTypeID));
            parameters.Add(new SqlParameter("@NoOfDays", PobjDTOLeaveExtension.NoOfDays));
            parameters.Add(new SqlParameter("@AdditionalMinutes", PobjDTOLeaveExtension.AdditionalMinutes));
            parameters.Add(new SqlParameter("@Reason", PobjDTOLeaveExtension.Reason));
            parameters.Add(new SqlParameter("@LastModifiedUserID", ClsCommonSettings.UserID));

            return ExecuteNonQuery(strProcedureName, parameters) > 0 ? true : false;
        }
        #endregion SaveLeaveExtension

        #region GetJoiningDate
        public DateTime GetJoiningDate(int intEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "GetJoiningDate"));
            parameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            return Convert.ToDateTime(ExecuteScalar(strProcedureName, parameters));
        }
        #endregion GetJoiningDate

        #region GetShiftDuration
        public string GetShiftDuration(int intEmployeeID, int intDayID,DateTime dteDate)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "GetShiftDuration"));
            parameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            parameters.Add(new SqlParameter("@DayID", intDayID));
            parameters.Add(new SqlParameter("@Date", dteDate));
            return Convert.ToString(ExecuteScalar(strProcedureName, parameters));
        }
        #endregion GetShiftDuration

        #region CheckIfPolicyExists
        public bool CheckIfPolicyExists(int intEmployeeID, int intDayID,DateTime dteDate)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "CheckIfPolicyExists"));
            parameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            parameters.Add(new SqlParameter("@DayID", intDayID));
            parameters.Add(new SqlParameter("@Date", dteDate));
            return Convert.ToBoolean(ExecuteScalar(strProcedureName, parameters));
        }
        #endregion CheckIfPolicyExists

        #region GetRecordCount
        public int GetRecordCount(string strSearchString, Int32 PintEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "GetRecordCount"));
            parameters.Add(new SqlParameter("@SearchString", strSearchString));
            parameters.Add(new SqlParameter("@EmployeeID", PintEmployeeID));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return Convert.ToInt32(ExecuteScalar(strProcedureName, parameters));
        }
        #endregion GetRecordCount

        #region GetLeaveExtensionDetails
        public DataTable GetLeaveExtensionDetails(int intRowNum, int LeaveExtensionTypeID, string strSearchString, Int32 PintEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "GetLeaveExtensionDetails"));
            parameters.Add(new SqlParameter("@RowNum", intRowNum));
            parameters.Add(new SqlParameter("@LeaveExtensionTypeID", LeaveExtensionTypeID));
            parameters.Add(new SqlParameter("@SearchString", strSearchString));
            parameters.Add(new SqlParameter("@EmployeeID", PintEmployeeID));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return ExecuteDataTable(strProcedureName, parameters);
        }
        #endregion GetLeaveExtensionDetails

        #region CheckLeaveDuplicate
        public bool CheckLeaveDuplicate(string MonthYear, int EmployeeID, int LeaveTypeID, int LeaveExtensionTypeID, int MAddStatus, int ExtensionID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "DuplicatChecking_Leave"));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@MonthYear", MonthYear));
            parameters.Add(new SqlParameter("@LeaveTypeID", LeaveTypeID));
            parameters.Add(new SqlParameter("@LeaveExtensionTypeID", LeaveExtensionTypeID));
            parameters.Add(new SqlParameter("@MAddStatus", MAddStatus));
            parameters.Add(new SqlParameter("@ExtensionID", ExtensionID));
            return Convert.ToBoolean(ExecuteScalar(strProcedureName, parameters));
        }
        #endregion CheckLeaveDuplicate

        #region CheckTimeDuplicate
        public bool CheckTimeDuplicate(string MonthYear, int EmployeeID, int LeaveExtensionTypeID, int MAddStatus, int ExtensionID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "DuplicatChecking_Time"));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@MonthYear", MonthYear));
            parameters.Add(new SqlParameter("@LeaveExtensionTypeID", LeaveExtensionTypeID));
            parameters.Add(new SqlParameter("@MAddStatus", MAddStatus));
            parameters.Add(new SqlParameter("@ExtensionID", ExtensionID));
            return Convert.ToBoolean(ExecuteScalar(strProcedureName, parameters));
        }
        #endregion CheckTimeDuplicate

        #region CheckPaymentRelease
        public bool CheckPaymentRelease(int EmployeeID, string EndDate, int intMode)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", intMode));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@TransDate1", EndDate));
            return Convert.ToInt32(ExecuteScalar("spPayPaymentReleaseChecking", parameters)) == 1 ? true : false;
        }
        #endregion CheckPaymentRelease

        #region GetEmployeesWithLeaveExtension
        public DataTable GetEmployeesWithLeaveExtension(int ExtensionID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "GetEmployees"));
            parameters.Add(new SqlParameter("@ExtensionID", ExtensionID));
            return ExecuteDataTable(strProcedureName, parameters);
        }
        #endregion GetEmployeesWithLeaveExtension

        #region GetEmployeeCompany
        public int GetEmployeeCompany(int EmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "GetCompanyID"));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            return Convert.ToInt32(ExecuteScalar(strProcedureName, parameters));
        }
        #endregion GetEmployeeCompany

        #region GetFinYearStartDate
        public string GetFinYearStartDate(int CompanyID, string MonthYear)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "GetFinYearStartDate"));
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            parameters.Add(new SqlParameter("@MonthYear", MonthYear));
            return Convert.ToString(ExecuteScalar(strProcedureName, parameters));
        }
        #endregion GetFinYearStartDate

        #region GetLeavePolicy
        public int GetLeavePolicy(int EmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "GetLeavePolicy"));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            return Convert.ToInt32(ExecuteScalar(strProcedureName, parameters));
        }
        #endregion GetLeavePolicy

        #region GetBalanceLeave
        public DataTable GetBalanceLeave(int EmployeeID, int CompanyID, string MonthYear, int LeavePolicyID, int LeaveTypeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "GetBalanceLeave"));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            parameters.Add(new SqlParameter("@MonthYear", MonthYear));
            parameters.Add(new SqlParameter("@LeavePolicyID", LeavePolicyID));
            parameters.Add(new SqlParameter("@LeaveTypeID", LeaveTypeID));
            return ExecuteDataTable(strProcedureName, parameters);
        }
        #endregion GetBalanceLeave

        #region CheckMaleOrFemale
        public bool CheckMaleOrFemale(int EmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "CheckMaleOrFemale"));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

            return Convert.ToBoolean(ExecuteScalar(strProcedureName, parameters));
        }
        #endregion CheckMaleOrFemale

        #region CheckLeavesTakenForEdit
        public bool CheckLeavesTakenForEdit(string MonthYear, int EmployeeID, int CompanyID, int LeaveTypeID, double ExceededEdit)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "CheckLeavesTakenForEdit"));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            parameters.Add(new SqlParameter("@MonthYear", MonthYear));
            parameters.Add(new SqlParameter("@LeaveTypeID", LeaveTypeID));
            parameters.Add(new SqlParameter("@ExceededEdit", ExceededEdit));
            return Convert.ToBoolean(ExecuteScalar(strProcedureName, parameters));
        }
        #endregion CheckLeavesTakenForEdit

        #region CheckLeavesTakenForDelete
        public bool CheckLeavesTakenForDelete(string MonthYear, int EmployeeID, int CompanyID, int LeaveTypeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "CheckLeavesTakenForDelete"));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            parameters.Add(new SqlParameter("@MonthYear", MonthYear));
            parameters.Add(new SqlParameter("@LeaveTypeID", LeaveTypeID));
            return Convert.ToBoolean(ExecuteScalar(strProcedureName, parameters));
        }
        #endregion CheckLeavesTakenForDelete

        #region CheckIfExtensionIDExists
        public bool CheckIfExtensionIDExists(int ExtensionID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "CheckIfExtensionIDExists"));
            parameters.Add(new SqlParameter("@ExtensionID", ExtensionID));

            return Convert.ToInt32(ExecuteScalar(strProcedureName, parameters)) > 0 ? true : false;
        }
        #endregion CheckIfExtensionIDExists

        #region DeleteLeaveExtension
        public bool DeleteLeaveExtension(int ExtensionID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "DeleteLeaveExtension"));
            parameters.Add(new SqlParameter("@ExtensionID", ExtensionID));

            return ExecuteNonQuery(strProcedureName, parameters) > 0 ? true : false;
        }
        #endregion DeleteLeaveExtension

        #region GetEmployeeLeaveSummary
        public DataTable GetEmployeeLeaveSummary(int EmployeeID, string MonthYear, int LeaveTypeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "GetEmployeeLeaveSummary"));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@MonthYear", MonthYear));
            parameters.Add(new SqlParameter("@LeaveTypeID", LeaveTypeID));
            return ExecuteDataTable(strProcedureName, parameters);
        }
        #endregion GetEmployeeLeaveSummary

        #region ShowReport
        public DataSet ShowReport(int EmployeeID, int CompanyID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "ShowReport"));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            return ExecuteDataSet(strProcedureName, parameters);
        }
        #endregion ShowReport
        #endregion
    }
}
