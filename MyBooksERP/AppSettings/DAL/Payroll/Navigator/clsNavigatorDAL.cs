﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{

    /*******************************************************
  * Author       : Sreelekshmi
  * Created On   : 06 Oct 2011
  * Purpose      : Create data access layer for Navigator
  * *****************************************************/

    public class clsNavigatorDAL:DataLayer 
    {
        private clsNavigator  objNavigator= null;
        private string strProcedureName = "spNavigator";
        private List<SqlParameter> sqlParameters = null;

        /// <summary>
        /// Constructor 
        /// </summary>
        public clsNavigatorDAL() { }

        public clsNavigator Navigator
        {
            get
            {
                if (this.objNavigator == null)
                    this.objNavigator = new clsNavigator();
                return this.objNavigator;
            }
            set { this.objNavigator = value; }
        }

        /// <summary>
        /// Combo filling company
        /// </summary>
        /// <returns></returns>
        public DataTable GetNavCompanyTree()
        {
            return this.ExecuteDataTable(this.strProcedureName, new List<SqlParameter>() { 
                new SqlParameter("@Mode", "GC")
            });
        }
        public DataTable GetNavCamp()
        {
            return this.ExecuteDataTable(this.strProcedureName, new List<SqlParameter>() { 
                 new SqlParameter("@Mode", "GCP"),
                 new SqlParameter("@ParentID", this.Navigator.parentID)
            });
        }
        public DataTable GetNavClient()
        {
            return this.ExecuteDataTable(this.strProcedureName, new List<SqlParameter>() { 
                 new SqlParameter("@Mode", "GAC"),
                 new SqlParameter("@ParentID", this.Navigator.parentID)
            });
        }
        public DataTable GetNavEmployee()
        {
            return this.ExecuteDataTable(this.strProcedureName, new List<SqlParameter>() { 
                 new SqlParameter("@Mode", "GE"),
                 new SqlParameter("@ParentID", this.Navigator.parentID)
            });
        }
        public DataTable GetNavBuilding()
        {
            return this.ExecuteDataTable(this.strProcedureName, new List<SqlParameter>() { 
                 new SqlParameter("@Mode", "GB")
                 
            });
        }
        public DataTable GetNavMess()
        {
            return this.ExecuteDataTable(this.strProcedureName, new List<SqlParameter>() { 
                 new SqlParameter("@Mode", "GM")
                 
            });
        }
        public DataSet GetNavCompanyEmail()
        {
            return this.ExecuteDataSet(this.strProcedureName, new List<SqlParameter>() { 
                 new SqlParameter("@Mode", "GCN"),
                 new SqlParameter("@ParentID", this.Navigator.parentID)
            });
        }
        public DataSet GetNavCampEmail()
        {
            return this.ExecuteDataSet(this.strProcedureName, new List<SqlParameter>() { 
                 new SqlParameter("@Mode", "GCPN"),
                 new SqlParameter("@ParentID", this.Navigator.parentID)
            });
        }
        public DataSet GetNavClientEmail()
        {
            return this.ExecuteDataSet(this.strProcedureName, new List<SqlParameter>() { 
                 new SqlParameter("@Mode", "GCCN"),
                 new SqlParameter("@ParentID", this.Navigator.parentID)
            });
        }
        public DataSet GetNavEmployeeEmail()
        {
            return this.ExecuteDataSet(this.strProcedureName, new List<SqlParameter>() { 
                 new SqlParameter("@Mode", "GEN"),
                 new SqlParameter("@ParentID", this.Navigator.parentID)
            });
        }

    
     

        public DataSet Search(NavSearch  searchBy, string searchKey)
        {
            return this.ExecuteDataSet(strProcedureName, new List<SqlParameter>() { 
                new SqlParameter("@Mode", "SEAR"),
                new SqlParameter("@SearchIndex", (int)searchBy),
                new SqlParameter("@SearchKey", searchKey)
            });
        }
  
        
    
     
        public DataSet GetEmployeeReport()
        {
            return this.ExecuteDataSet("spEmployee", new List<SqlParameter>() { 
                new SqlParameter("@Mode", 7),new SqlParameter("@EmployeeID", this.Navigator.parentID)
               
            });
        }
        public DataSet GetSalaryStructureReport()
        {
            return this.ExecuteDataSet("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@Mode","GES"),new SqlParameter("@ParentID", this.Navigator.parentID)
               
            });
        }

        public DataSet GetPreSalaryStructureReport()
        {
            return this.ExecuteDataSet("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@Mode","GEPS"),new SqlParameter("@ParentID", this.Navigator.parentID)
               
            });
        }
        
        public DataSet GetShiftPolicy()
        {

            return this.ExecuteDataSet("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@Mode","GSD"),new SqlParameter("@ParentID", this.Navigator.parentID)
               
            });
        }
        public DataTable  LoadShifts()
        {
             return this.ExecuteDataTable("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@Mode","GAS"),new SqlParameter("@ParentID", this.Navigator.parentID)});
        }
        public DataTable LoadCompany()
        {
            return this.ExecuteDataTable("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@Mode","GACC"),new SqlParameter("@ParentID", this.Navigator.parentID)});
        }

        public DataTable LoadWorkPolicies()
        {
            return this.ExecuteDataTable("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@Mode","GAW"),new SqlParameter("@ParentID", this.Navigator.parentID)});
        }

        public DataTable LoadLeavePolicies()
        {
            return this.ExecuteDataTable("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@Mode","GAL"),new SqlParameter("@ParentID", this.Navigator.parentID)});
        }

        public DataSet GetWorkPolicy()
        {
            return this.ExecuteDataSet("spPayWorkPolicy", new List<SqlParameter>() { 
                new SqlParameter("@Mode","WP"),new SqlParameter("@PolicyID", this.Navigator.parentID)});

        }
        public DataSet GetLeavePolicy() //Leave Policy
        {
            return this.ExecuteDataSet("spPayLeavePolicy", new List<SqlParameter>() { 
                new SqlParameter("@Mode",20),new SqlParameter("@LeavePolicyID", this.Navigator.parentID)});

        }

        public DataTable LoadEmployeeGrid(int intEmpFilterTypeValue, int intEmpFilter, int intWorkStatusID,int intCmpID)
        {
            return this.ExecuteDataTable("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@Mode","GEL"),new SqlParameter("@EmpFilterTypeValue",intEmpFilterTypeValue),new SqlParameter("@EmpFilter",intEmpFilter),new SqlParameter("@CompanyID",intCmpID),new SqlParameter("@WorkStatusID",intWorkStatusID)});

        }

        public DataTable FillData() 
        {
            return this.ExecuteDataTable("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@Mode","GRD"),new SqlParameter("@SearchType", this.Navigator.SearchType),new SqlParameter("@ParentID", this.Navigator.parentID)});
        }

        public DataTable GetAlerts() 
        {
            return this.ExecuteDataTable("spDocuments", new List<SqlParameter>() { 
                new SqlParameter("@Mode",13),new SqlParameter("@CreatedBy", this.Navigator.parentID)});
        }
        
    }
}
