﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using BLL;
using DTO;
using MyBooksERP;
namespace DAL
{
    /// <summary>
    ///  Modified By        : Sachin
    ///  Modified Date      : 8/14/2013
    ///  Purpose            : Performance tuning
    /// </summary>
    public class clsDALVacationEntry
    {
        ClsCommonUtility MObjClsCommonUtility;
        private clsDTOVacationEntry MobjDTOVacationEntry = null;
        DataLayer MobjDataLayer = null;
        private List<SqlParameter> sqlParameters = null;
        public clsBLLSalaryPayment PobjBLLSalaryPaymentt;
        public clsDTOSalaryPayment PobjclsDTOSalaryPayment { get; set; } // DTO Salary Process Property
        public clsDALVacationEntry(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MobjDataLayer;
            PobjBLLSalaryPaymentt = new clsBLLSalaryPayment();
        }

        public DataLayer DataLayer
        {
            get

            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
        public clsDTOVacationEntry DTOVacationEntry
        {
            get
            {
                if (this.MobjDTOVacationEntry == null)
                    this.MobjDTOVacationEntry = new clsDTOVacationEntry();

                return this.MobjDTOVacationEntry;
            }
            set
            {
                this.MobjDTOVacationEntry = value;
            }
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MObjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }
        /// <summary>
        /// To get Record Count
        /// </summary>
        /// <returns>int</returns>
        public int RecCount()
        {
            int RecordCnt = 0;
            string TSQL;
            DataTable DT;
            TSQL = "SELECT ISNULL(COUNT(1),0) FROM [PayVacationMaster] where  CompanyID IN(select CompanyID from dbo.UserCompanyDetails where UserID="+ClsCommonSettings.UserID +")";
            DT = MobjDataLayer.ExecuteDataTable(TSQL);
            if (DT.Rows.Count > 0)
            {
                RecordCnt = Convert.ToInt32(DT.Rows[0][0]);
            }
            else
            {
                RecordCnt = 0;
            }
            return RecordCnt;
        }
        /// <summary>
        /// To get Record count as per Conditions Passed
        /// </summary>
        /// <returns>int</returns>
        public int GetRecCount()
        {
            int RecordCnt = 0;
            DataTable DT;
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 18));
            alparameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            alparameters.Add(new SqlParameter("@SearchKey", MobjDTOVacationEntry.strSearchKey));
            alparameters.Add(new SqlParameter("@CompID", ClsCommonSettings.LoginCompanyID));
            alparameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            DT = MobjDataLayer.ExecuteDataTable("spPayVacationProcessTransactions", alparameters);
            if (DT.Rows.Count > 0)
            {
                RecordCnt = Convert.ToInt32(DT.Rows[0][0]);
            }
            else
            {
                RecordCnt = 0;
            }
            return RecordCnt;
        }
        /// <summary>
        /// To save Vacation Master
        /// </summary>
        /// <returns>bool</returns>
        public bool SaveVacationMaster()
        {
            int intReturnID;
            ArrayList alparameters;
            string TSQL="";

            alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@IsClosed", MobjDTOVacationEntry.IsClosed));
            if (MobjDTOVacationEntry.VacationID > 0)
            {
                DeleteVacationDetails();

                alparameters.Add(new SqlParameter("@Mode", "3"));
                alparameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            }
            else
            {
                alparameters.Add(new SqlParameter("@Mode", "2"));
            }

            alparameters.Add(new SqlParameter("@EmployeeID", MobjDTOVacationEntry.EmployeeID));
            alparameters.Add(new SqlParameter("@NetAmount", MobjDTOVacationEntry.NetAmount));
            alparameters.Add(new SqlParameter("@GivenAmount", MobjDTOVacationEntry.GivenAmount));
            alparameters.Add(new SqlParameter("@CurrencyID", MobjDTOVacationEntry.CurrencyID));
            alparameters.Add(new SqlParameter("@FromDate1", MobjDTOVacationEntry.FromDate));
            alparameters.Add(new SqlParameter("@ToDate1", MobjDTOVacationEntry.ToDate));
            alparameters.Add(new SqlParameter("@TakenLeaves", MobjDTOVacationEntry.TakenLeaves));
            alparameters.Add(new SqlParameter("@OvertakenLeaves", MobjDTOVacationEntry.OvertakenLeaves));
            alparameters.Add(new SqlParameter("@ActualRejoinDate", MobjDTOVacationEntry.ActualDateTime));
            alparameters.Add(new SqlParameter("@IssuedTickets", MobjDTOVacationEntry.IssuedTickets));
            alparameters.Add(new SqlParameter("@TicketExpense", MobjDTOVacationEntry.TicketExpense));
            alparameters.Add(new SqlParameter("@CompID", MobjDTOVacationEntry.CompanyID));
            alparameters.Add(new SqlParameter("@Remarks", MobjDTOVacationEntry.Remarks));
            alparameters.Add(new SqlParameter("@CreatedBy", MobjDTOVacationEntry.CreatedBy));
            alparameters.Add(new SqlParameter("@IsProcessSalaryForCurrentMonth", MobjDTOVacationEntry.IsProcessSalaryForCurrentMonth));
            alparameters.Add(new SqlParameter("@ExperienceDays", MobjDTOVacationEntry.ExperienceDays));
            alparameters.Add(new SqlParameter("@AbsentDays", MobjDTOVacationEntry.AbsentDays));
            alparameters.Add(new SqlParameter("@EncashComboOffDay", MobjDTOVacationEntry.EncashComboOffDay));
            alparameters.Add(new SqlParameter("@IsConsiderAbsentDays", MobjDTOVacationEntry.IsConsiderAbsentDays));
            alparameters.Add(new SqlParameter("@EligibleLeavePayDays", MobjDTOVacationEntry.EligibleLeavePayDays));
            alparameters.Add(new SqlParameter("@IsPaidLeave", MobjDTOVacationEntry.PaidLeave));
            alparameters.Add(new SqlParameter("@RejoinDate", MobjDTOVacationEntry.RejoinDate));
            alparameters.Add(new SqlParameter("@IsEncashOnly", MobjDTOVacationEntry.IsEncashOnly));
            if (MobjDTOVacationEntry.AccountID > 0)
                alparameters.Add(new SqlParameter("@AccountID", MobjDTOVacationEntry.AccountID));
            alparameters.Add(new SqlParameter("@TransactionTypeID", MobjDTOVacationEntry.TransactionTypeID));
            alparameters.Add(new SqlParameter("@ChequeNumber", MobjDTOVacationEntry.ChequeNumber));
            alparameters.Add(new SqlParameter("@ChequeDate", MobjDTOVacationEntry.ChequeDate));
            alparameters.Add(new SqlParameter("@RePaymentID", MobjDTOVacationEntry.RePaymentID));
            alparameters.Add(new SqlParameter("@NeedVoucherEntry", MobjDTOVacationEntry.NeedVoucherEntry));

            SqlParameter sQlparam = new SqlParameter("@ReturnVal", SqlDbType.Int);
            sQlparam.Direction = ParameterDirection.ReturnValue;
            alparameters.Add(sQlparam);
            intReturnID = Convert.ToInt32(MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", alparameters));

            if (Convert.ToInt32(intReturnID) > 0)
            {

                TSQL = "Delete PayTempEmployeeIDForPaymentRelease Where EmployeeID=" + MobjDTOVacationEntry.EmployeeID + " and MachineName='"+ System.Net.Dns.GetHostName()  +"'";
                MobjDataLayer.ExecuteQuery(TSQL);


                if (MobjDTOVacationEntry.IsClosed == true && MobjDTOVacationEntry.RePaymentID!=0)
                {

                    TSQL = " INSERT INTO PayTempEmployeeIDForPaymentRelease(EmployeeID,MachineName,PaymentID,EmpVenFlagRele) " +
                           " SELECT " + MobjDTOVacationEntry.EmployeeID + ",'" + System.Net.Dns.GetHostName() + "'," + MobjDTOVacationEntry.RePaymentID + ",1";

                    MobjDataLayer.ExecuteQuery(TSQL);


                    
                    alparameters = new ArrayList();
                    alparameters.Add(new SqlParameter("@UserID", MobjDTOVacationEntry.CreatedBy));
                    alparameters.Add(new SqlParameter("@GCompanyID", MobjDTOVacationEntry.CompanyID));
                    alparameters.Add(new SqlParameter("@MachineName", System.Net.Dns.GetHostName()));
                    MobjDataLayer.ExecuteScalar("spPayNetAmtCorrection", alparameters);


                    DataTable Dt;
                    Double dblNetAmount=0;
                    TSQL = "SELECT NetAmount FROM PayEmployeePayment WHERE PaymentID=" + MobjDTOVacationEntry.RePaymentID + "";
                    Dt = MobjDataLayer.ExecuteDataTable(TSQL);

                    if (Dt.Rows.Count > 0)
                    {
                        dblNetAmount =Convert.ToDouble(Dt.Rows[0][0]); 
                    }
                     
                    PobjBLLSalaryPaymentt.clsDTOSalaryPayment.intUserId=2;
                    PobjBLLSalaryPaymentt.clsDTOSalaryPayment.intCompanyID = MobjDTOVacationEntry.CompanyID;
                    PobjBLLSalaryPaymentt.clsDTOSalaryPayment.strGetHostName = System.Net.Dns.GetHostName();
                    PobjBLLSalaryPaymentt.clsDTOSalaryPayment.strPaymentDate = System.DateTime.Now.Date.ToString("dd-MMM-yyyy");
                    PobjBLLSalaryPaymentt.clsDTOSalaryPayment.dblNetAmount = dblNetAmount;
                    PobjBLLSalaryPaymentt.ConfirmAll();  
                }                

                if (intReturnID > 0)
                {
                    MobjDTOVacationEntry.VacationID = intReturnID;
                    SaveVacationDetails();

                    if (MobjDTOVacationEntry.IsEncashOnly == false)
                    {
                        DeleteAttendance();
                    }
                }
                //In case of HRPower enabled
                //if (ClsCommonSettings.IsHrPowerEnabled)
                //{
                //    //TSQL = " UPDATE HRLeaveRequest SET VacationID=" + MobjDTOVacationEntry.VacationID + " WHERE EmployeeID=" + MobjDTOVacationEntry.EmployeeID + " AND StatusId=5 AND IsVacation=1";
                //    //MobjDataLayer.ExecuteQuery(TSQL);
                //}
            }

            return true;
        }
        /// <summary>
        /// To save Vacation Details
        /// </summary>
        /// <returns>bool</returns>
        public bool SaveVacationDetails()
        {
            int intOrderNo = 0;

            foreach (clsDTOVacationEntryDetail objclsDTOVacationEntryDetail in MobjDTOVacationEntry.lstclsDTOVacationEntryDetail)
            {
                ArrayList alparameters = new ArrayList();
                intOrderNo = intOrderNo + 1;
                alparameters.Add(new SqlParameter("@Mode", 5));
                alparameters.Add(new SqlParameter("@OrderNo", ++intOrderNo));
                alparameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
                alparameters.Add(new SqlParameter("@AdditionDeductionID", objclsDTOVacationEntryDetail.AdditionDeductionID));
                alparameters.Add(new SqlParameter("@Amount", objclsDTOVacationEntryDetail.Amount));
                alparameters.Add(new SqlParameter("@NoOfDays", objclsDTOVacationEntryDetail.NoOfDays));
                alparameters.Add(new SqlParameter("@ReleaseLater", objclsDTOVacationEntryDetail.IsReleaseLater));
                alparameters.Add(new SqlParameter("@CompID", MobjDTOVacationEntry.CompanyID));
                alparameters.Add(new SqlParameter("@CurrencyID", MobjDTOVacationEntry.CurrencyID));
                MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", alparameters);
            }
            return true;
        }
        /// <summary>
        /// To delete Leave Entry Details
        /// </summary>
        /// <returns>bool</returns>
        public bool DeleteLeaveEntryDetails()
        {
            ArrayList alParamters = new ArrayList();
            alParamters.Add(new SqlParameter("@Mode", 10));
            alParamters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            alParamters.Add(new SqlParameter("@EmployeeID", MobjDTOVacationEntry.EmployeeID));
            MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", alParamters);
            return true;
        }
        /// <summary>
        /// To Delete Attendance
        /// </summary>
        /// <returns>bool</returns>
        public bool DeleteAttendance()
        {
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 13));
            alparameters.Add(new SqlParameter("@EmployeeID", MobjDTOVacationEntry.EmployeeID));
            alparameters.Add(new SqlParameter("@FromDate1", MobjDTOVacationEntry.FromDate));
            alparameters.Add(new SqlParameter("@ToDate1", MobjDTOVacationEntry.ToDate));
            alparameters.Add(new SqlParameter("@OvertakenLeaves", MobjDTOVacationEntry.OvertakenLeaves));
            MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", alparameters);
            return true;
        }
        /// <summary>
        /// To delete Vacation Details
        /// </summary>
        /// <returns>bool</returns>
        public bool DeleteVacationDetails()
        {
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 6));
            alparameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", alparameters);
            return true;
        }
        /// <summary>
        /// To delete Vacation Master
        /// </summary>
        /// <returns>bool</returns>
        public bool DeleteVacationMaster()
        {
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 4));
            alparameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            alparameters.Add(new SqlParameter("@EmployeeID", MobjDTOVacationEntry.EmployeeID));
            //if(ClsCommonSettings.IsHrPowerEnabled)
            //    alparameters.Add(new SqlParameter("@IsHRPower", 1));
            MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", alparameters);
            return true;
        }
        /// <summary>
        /// To delete Payment Details
        /// </summary>
        /// <returns></returns>
        public bool DeletePaymentDetails()
        {
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 16));
            alparameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", alparameters);
            return true;
        }
        /// <summary>
        /// To get vacation Master Details
        /// </summary>
        /// <param name="iRowNum"></param>
        /// <returns>bool</returns>
        public bool GetVacationMaster(int iRowNum)
        {
            DataSet dtVacation = new DataSet();
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", "1"));
            alparameters.Add(new SqlParameter("@RowNum", MobjDTOVacationEntry.RowNum));
            alparameters.Add(new SqlParameter("@SearchKey", MobjDTOVacationEntry.strSearchKey));
            alparameters.Add(new SqlParameter("@CompID", ClsCommonSettings.LoginCompanyID));
            alparameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            dtVacation = MobjDataLayer.ExecuteDataSet("spPayVacationProcessTransactions", alparameters);

            if (dtVacation.Tables.Count > 0)
            {
                if (dtVacation.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = dtVacation.Tables[0].Rows[0];


                    MobjDTOVacationEntry.VacationID = Convert.ToInt32(dr["VacationID"]);
                    MobjDTOVacationEntry.RowNum = Convert.ToInt32(dr["RowNumber"]);
                    MobjDTOVacationEntry.Remarks = Convert.ToString(dr["Remarks"]);
                    MobjDTOVacationEntry.NetAmount = Convert.ToDecimal(dr["NetAmount"]);
                    MobjDTOVacationEntry.GivenAmount = Convert.ToDecimal(dr["GivenAmount"]);
                    MobjDTOVacationEntry.EmployeeID = Convert.ToInt32(dr["EmployeeID"]);
                    MobjDTOVacationEntry.PaidLeave = Convert.ToBoolean(dr["IsPaidLeave"]);
                    MobjDTOVacationEntry.IsEncashOnly = Convert.ToBoolean(dr["IsEncashOnly"]);
                    MobjDTOVacationEntry.FromDate = Convert.ToString(dr["FromDate"]);
                    MobjDTOVacationEntry.ToDate = Convert.ToString(dr["ToDate"]);
                    MobjDTOVacationEntry.TakenLeaves = Convert.ToDecimal(dr["TakenLeaves"]);
                    MobjDTOVacationEntry.OvertakenLeaves = Convert.ToInt32(dr["OvertakenLeaves"]);
                    MobjDTOVacationEntry.ActualDateTime = Convert.ToDateTime(dr["ActualRejoinDate"]);
                    MobjDTOVacationEntry.IssuedTickets = Convert.ToInt32(dr["IssuedTickets"]);
                    MobjDTOVacationEntry.TicketExpense = Convert.ToDecimal(dr["TicketExpense"]);
                    MobjDTOVacationEntry.CreatedDate = Convert.ToString(dr["CreatedDate"]);
                    MobjDTOVacationEntry.IsClosed = Convert.ToBoolean(dr["IsClosed"]);
                    MobjDTOVacationEntry.ExperienceDays = Convert.ToDecimal(dr["ExperienceDays"]);
                    MobjDTOVacationEntry.AbsentDays = Convert.ToDecimal(dr["AbsentDays"]);
                    MobjDTOVacationEntry.EncashComboOffDay = dr["EncashComboOffDay"].ToDecimal();
                    MobjDTOVacationEntry.EligibleLeavePayDays = Convert.ToDecimal(dr["EligibleLeavePayDays"]);
                    MobjDTOVacationEntry.RejoinDate = Convert.ToString(dr["RejoinDate"]);
                    MobjDTOVacationEntry.IsProcessSalaryForCurrentMonth = Convert.ToBoolean(dr["IsProcessSalaryForCurrentMonth"]);
                    MobjDTOVacationEntry.IsConsiderAbsentDays = Convert.ToBoolean(dr["IsConsiderAbsentDays"]);
                    if (dr["AccountID"] == System.DBNull.Value)
                    {
                        MobjDTOVacationEntry.AccountID = 0;
                    }
                    else
                    {
                        MobjDTOVacationEntry.AccountID = Convert.ToInt32(dr["AccountID"]);
                    }
                    MobjDTOVacationEntry.ChequeDate = Convert.ToDateTime(dr["ChequeDate"]);
                    MobjDTOVacationEntry.ChequeNumber = Convert.ToString(dr["ChequeNumber"]);
                    MobjDTOVacationEntry.TransactionTypeID = Convert.ToInt32(dr["TransactionTypeID"]);
                    MobjDTOVacationEntry.RePaymentID = Convert.ToInt32(dr["PaymentID"]);

                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// search vacation proceass
        /// </summary>
        /// <param name="iRowNum"></param>
        /// <param name="iSerachType"></param>
        /// <param name="sSerchText"></param>
        /// <param name="iTotalCount"></param>
        /// <returns>bool</returns>
        public bool SearchVaction(int iRowNum, int iSerachType, string sSerchText, int iTotalCount)
        {
            DataSet dtVacation = new DataSet();

            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 12));
            alparameters.Add(new SqlParameter("@RowNum", iRowNum));
            alparameters.Add(new SqlParameter("@SearchType", iSerachType));
            alparameters.Add(new SqlParameter("@SearchText", sSerchText));
            alparameters.Add(new SqlParameter("@CompID", ClsCommonSettings.LoginCompanyID));
            dtVacation = MobjDataLayer.ExecuteDataSet("spPayVacationProcessTransactions", alparameters);

            if (dtVacation.Tables.Count > 0)
            {
                if (dtVacation.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = dtVacation.Tables[0].Rows[0];
                    MobjDTOVacationEntry.VacationID = Convert.ToInt32(dr["VacationID"]);
                    MobjDTOVacationEntry.RowNum = Convert.ToInt32(dr["RowNumber"]);
                    MobjDTOVacationEntry.Remarks = Convert.ToString(dr["Remarks"]);
                    MobjDTOVacationEntry.NetAmount = Convert.ToDecimal(dr["NetAmount"]);
                    MobjDTOVacationEntry.GivenAmount = Convert.ToDecimal(dr["GivenAmount"]);
                    MobjDTOVacationEntry.EmployeeID = Convert.ToInt32(dr["EmployeeID"]);
                    MobjDTOVacationEntry.FromDate = Convert.ToDateTime(dr["FromDate"]).ToString();
                    MobjDTOVacationEntry.ToDate = Convert.ToDateTime(dr["ToDate"]).ToString();
                    MobjDTOVacationEntry.TakenLeaves = Convert.ToDecimal(dr["TakenLeaves"]);
                    MobjDTOVacationEntry.OvertakenLeaves = Convert.ToInt32(dr["OvertakenLeaves"]);
                    MobjDTOVacationEntry.IssuedTickets = Convert.ToInt32(dr["IssuedTickets"]);
                    MobjDTOVacationEntry.TicketExpense = Convert.ToDecimal(dr["TicketExpense"]);
                    MobjDTOVacationEntry.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]).ToString();
                    MobjDTOVacationEntry.IsClosed = Convert.ToBoolean(dr["IsClosed"]);
                    MobjDTOVacationEntry.ExperienceDays = Convert.ToInt32(dr["ExperienceDays"]);
                    MobjDTOVacationEntry.AbsentDays = Convert.ToInt32(dr["AbsentDays"]);
                    MobjDTOVacationEntry.EligibleLeavePayDays = Convert.ToDecimal(dr["EligibleLeavePayDays"]);
                    MobjDTOVacationEntry.RejoinDate = Convert.ToDateTime(dr["RejoinDate"]).ToString();
                    MobjDTOVacationEntry.IsProcessSalaryForCurrentMonth = Convert.ToBoolean(dr["IsProcessSalaryForCurrentMonth"]);
                    MobjDTOVacationEntry.IsConsiderAbsentDays = Convert.ToBoolean(dr["IsConsiderAbsentDays"]);
                    MobjDTOVacationEntry.EncashComboOffDay = dr["EncashComboOffDay"].ToDecimal();
                    MobjDTOVacationEntry.ActualDateTime = Convert.ToDateTime(dr["ActualRejoinDate"].ToString());
                    MobjDTOVacationEntry.AccountID = Convert.ToInt32(dr["AccountID"]);
                    MobjDTOVacationEntry.ChequeDate = Convert.ToDateTime(dr["ChequeDate"]);
                    MobjDTOVacationEntry.ChequeNumber = Convert.ToString(dr["ChequeNumber"]);
                    MobjDTOVacationEntry.TransactionTypeID = Convert.ToInt32(dr["TransactionTypeID"]);
                }

                if (dtVacation.Tables[1].Rows.Count > 0)
                {
                    iTotalCount = Convert.ToInt32(dtVacation.Tables[1].Rows[0]["RecordCount"]);
                }
            }
            return (iTotalCount > 0);
        }
        /// <summary>
        /// To get Vacation Details
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetVacationDetails()
        {
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 7));
            alparameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            alparameters.Add(new SqlParameter("@CompID", ClsCommonSettings.LoginCompanyID));
            return MobjDataLayer.ExecuteDataSet("spPayVacationProcessTransactions", alparameters);
        }
        /// <summary>
        /// To update over taken leaves
        /// </summary>
        /// <returns>bool</returns>
        public bool UpdateOverTakenLeaves()
        {
            string strQuery;
            if (MobjDTOVacationEntry.ActualDateTime != "01-Jan-1900".ToDateTime())
            {
                strQuery = "UPDATE PayVacationMaster SET ActualRejoinDate='" + MobjDTOVacationEntry.ActualDateTime.ToString("dd/MMM/yyyy")  + "', IsPaidLeave=" + Convert.ToInt32(MobjDTOVacationEntry.PaidLeave) + " ,OvertakenLeaves = " + MobjDTOVacationEntry.OvertakenLeaves + " , Remarks = '" + MobjDTOVacationEntry.Remarks.Trim().Replace("'", "’") + "' WHERE VacationID = " + MobjDTOVacationEntry.VacationID;
            }
            else
            {
                strQuery = "UPDATE PayVacationMaster SET  IsPaidLeave=" + Convert.ToInt32(MobjDTOVacationEntry.PaidLeave) + " ,OvertakenLeaves = " + MobjDTOVacationEntry.OvertakenLeaves + " , Remarks = '" + MobjDTOVacationEntry.Remarks.Trim().Replace("'", "’") + "' WHERE VacationID = " + MobjDTOVacationEntry.VacationID;
            }
            MobjDataLayer.ExecuteQuery(strQuery);
            return true;
        }
        //----------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// To check whether Addition or Deduction
        /// </summary>
        /// <param name="AddDedID"></param>
        /// <returns>bool</returns>
        public bool IsAddition(int AddDedID)
        {
            DataTable DT;
            string TSQL = "SELECT IsAddition FROM PayAdditionDeductionReference WHERE ISNULL(IsAddition,0)=1 AND AdditionDeductionID=" + AddDedID + "";
            DT = MobjDataLayer.ExecuteDataTable(TSQL);
            if (DT.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// To check duplicate entey or not
        /// </summary>
        /// <param name="MbAddStatus"></param>
        /// <returns>bool</returns>
        public bool chkdupicate(bool MbAddStatus)
        {
            try
            {
                string TSQL;
                if (MbAddStatus == true)
                {

                    TSQL = "SELECT VacationID  FROM PayVacationMaster WHERE  EmployeeID=" + MobjDTOVacationEntry.EmployeeID + "  AND (CONVERT(DATETIME,'" + MobjDTOVacationEntry.FromDate + "',106)  BETWEEN FromDate  AND DATEADD(DAY,ISNULL(OverTakenLeaves,0),ToDate)  OR CONVERT(DATETIME,'" + MobjDTOVacationEntry.ToDate + "',106) BETWEEN FromDate  AND DATEADD(DAY,ISNULL(OverTakenLeaves,0),ToDate))" +
                            " UNION ALL " +
                            "SELECT LeaveID  FROM PayEmployeeLeaveDetails WHERE  EmployeeID=" + MobjDTOVacationEntry.EmployeeID + "  AND (CONVERT(DATETIME,'" + MobjDTOVacationEntry.FromDate + "',106)  BETWEEN LeaveDateFrom  AND LeaveDateTo  OR CONVERT(datetime,'" + MobjDTOVacationEntry.ToDate + "',106) BETWEEN LeaveDateFrom  AND LeaveDateTo)";
                }
                else
                {
                    TSQL = "SELECT VacationID  FROM PayVacationMaster WHERE  VacationID <> " + MobjDTOVacationEntry.VacationID + " AND  EmployeeID=" + MobjDTOVacationEntry.EmployeeID + "  AND (CONVERT(DATETIME,'" + MobjDTOVacationEntry.FromDate + "',106)  BETWEEN FromDate  AND DATEADD(DAY,ISNULL(OverTakenLeaves,0),ToDate)  OR CONVERT(datetime,'" + MobjDTOVacationEntry.ToDate + "',106) BETWEEN FromDate  AND DATEADD(DAY,ISNULL(OverTakenLeaves,0),ToDate))" +
                           " UNION ALL " +
                           "SELECT LeaveID  FROM PayEmployeeLeaveDetails WHERE  EmployeeID=" + MobjDTOVacationEntry.EmployeeID + "  AND (CONVERT(DATETIME,'" + MobjDTOVacationEntry.FromDate  + "',106)  BETWEEN LeaveDateFrom  AND LeaveDateTo  OR CONVERT(DATETIME,'" + MobjDTOVacationEntry.ToDate + "',106) BETWEEN LeaveDateFrom  AND LeaveDateTo)";
                }


                DataTable DT = MobjDataLayer.ExecuteDataTable(TSQL);
                if (DT.Rows.Count > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Is Vacation Policy Exists
        /// </summary>
        /// <returns>bool</returns>
        public bool IsVacationPolicyExists()
        {
            DataTable DT;
            string sSqlQuery = "SELECT ISNULL(EM.VacationPolicyID,0) AS SetPolicyID FROM PaySalaryStructure SH INNER JOIN EmployeeMaster EM ON EM.EmployeeID = SH.EmployeeID   WHERE ISNULL(EM.VacationPolicyID,0)<>0 AND SH.EmployeeID=" + MobjDTOVacationEntry.EmployeeID + "";

            DT = MobjDataLayer.ExecuteDataTable(sSqlQuery);

            if (DT.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }





        /// <summary>
        /// To chech whether Salary Processed or Closed
        /// </summary>
        /// <returns>bool</returns>
        public bool IsSalaryIsProcessedOrClosed()
        {
            DataTable DT;
            string sSqlQuery;
            sSqlQuery = " SELECT * FROM PayVacationMaster WHERE isnull(IsClosed,0)=1 AND  EmployeeID=" + MobjDTOVacationEntry.EmployeeID + "AND " +
                        " ((CONVERT(DATETIME,CONVERT(VARCHAR,FromDate,112))<=convert(DATETIME,CONVERT(VARCHAR,'" + string.Format(MobjDTOVacationEntry.FromDate, "dd MMM yyyy") + "',112)) AND  " +
                        " CONVERT(DATETIME,CONVERT(VARCHAR,ToDate,112))>=convert(DATETIME,CONVERT(VARCHAR,'" + string.Format(MobjDTOVacationEntry.FromDate, "dd MMM yyyy") + "',112))) OR " +
                        " (CONVERT(DATETIME,CONVERT(VARCHAR,FromDate,112))<=convert(DATETIME,CONVERT(VARCHAR,'" + string.Format(MobjDTOVacationEntry.ToDate, "dd MMM yyyy") + "',112)) AND " +
                        " CONVERT(DATETIME,CONVERT(VARCHAR,ToDate,112))>=convert(DATETIME,CONVERT(VARCHAR,'" + string.Format(MobjDTOVacationEntry.ToDate, "dd MMM yyyy") + "',112)))) ";


            DT = MobjDataLayer.ExecuteDataTable(sSqlQuery);
            if (DT.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// To check whether Employee In Service or not
        /// </summary>
        /// <returns>bool</returns>
        public bool IsEmployeeInService()
        {
            DataTable DT;
            string sSqlQuery;
            sSqlQuery = "SELECT EmployeeID FROM EmployeeMaster  WHERE EmployeeID=" + MobjDTOVacationEntry.EmployeeID + " AND WorkStatusID<6 ";

            DT = MobjDataLayer.ExecuteDataTable(sSqlQuery);
            if (DT.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// To check whether Employee has Pending vacationa process
        /// </summary>
        /// <returns>bool</returns>
        public bool IsPendingVacationProcessed()
        {
            DataTable DT;
            string sSqlQuery;
            sSqlQuery = "SELECT 1 FROM PayVacationMaster WHERE EmployeeID = " + MobjDTOVacationEntry.EmployeeID + " AND IsClosed = 0 AND VacationID <> " + MobjDTOVacationEntry.VacationID + "";

            DT = MobjDataLayer.ExecuteDataTable(sSqlQuery);
            if (DT.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// To get Employees Company
        /// </summary>
        /// <returns>int</returns>
        public int GetEmpCompanyID()
        {
            DataTable DT;
            string sSqlQuery;
            sSqlQuery = "SELECT ISNULL(CompanyID,0) AS CompanyID FROM EmployeeMaster  WHERE EmployeeID=" + MobjDTOVacationEntry.EmployeeID + " ";

            DT = MobjDataLayer.ExecuteDataTable(sSqlQuery);
            if (DT.Rows.Count > 0)
            {
                return DT.Rows[0][0].ToInt32();
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        ///  To check whether Vacation Already Processed
        /// </summary>
        /// <param name="FromDateStr"></param>
        /// <param name="ToDateStr"></param>
        /// <returns>bool</returns>
        public bool IsVacationAlreadyProcessed(string FromDateStr, string ToDateStr)
        {

            DataTable DT;
            string sSqlQuery;
            sSqlQuery = " SELECT 1 FROM PayVacationMaster WHERE isnull(IsClosed,0)=1 AND  EmployeeID=" + MobjDTOVacationEntry.EmployeeID + "AND " +
                        " ((CONVERT(DATETIME,CONVERT(VARCHAR,FromDate,112))<=CONVERT(DATETIME,CONVERT(VARCHAR,'" + string.Format(MobjDTOVacationEntry.FromDate, "dd MMM yyyy") + "',112)) AND  " +
                        " CONVERT(DATETIME,CONVERT(VARCHAR,ToDate,112))>=CONVERT(DATETIME,CONVERT(VARCHAR,'" + string.Format(MobjDTOVacationEntry.FromDate, "dd MMM yyyy") + "',112))) OR " +
                        " (CONVERT(DATETIME,CONVERT(VARCHAR,FromDate,112))<=CONVERT(DATETIME,CONVERT(VARCHAR,'" + string.Format(MobjDTOVacationEntry.ToDate, "dd MMM yyyy") + "',112)) AND " +
                        " CONVERT(DATETIME,CONVERT(VARCHAR,ToDate,112))>=CONVERT(DATETIME,CONVERT(VARCHAR,'" + string.Format(MobjDTOVacationEntry.ToDate, "dd MMM yyyy") + "',112)))) ";


            DT = MobjDataLayer.ExecuteDataTable(sSqlQuery);
            if (DT.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// To check whether Is Vacation Process Salary Processed
        /// </summary>
        /// <param name="FromDateStr"></param>
        /// <param name="ToDateStr"></param>
        /// <returns>bool</returns>
        public bool IsVacationProcessSalaryProcessed(string FromDateStr, string ToDateStr)
        {

            DataTable DT;
            string sSqlQuery;
            sSqlQuery = " SELECT * FROM PayEmployeePayment WHERE EmployeeID=" + MobjDTOVacationEntry.EmployeeID + "AND " +
                        " (((CONVERT(DATETIME,CONVERT(VARCHAR,'" + string.Format(MobjDTOVacationEntry.FromDate, "dd MMM yyyy") + "',112))) BETWEEN (CONVERT(DATETIME,CONVERT(VARCHAR,PeriodFrom,112))) AND CONVERT(DATETIME,CONVERT(VARCHAR,PeriodTo,112))) " +
                        "  OR (CONVERT(DATETIME,CONVERT(VARCHAR,'" + string.Format(MobjDTOVacationEntry.ToDate, "dd MMM yyyy") + "',112)) BETWEEN (CONVERT(DATETIME,CONVERT(VARCHAR,PeriodFrom,112))) AND CONVERT(DATETIME,CONVERT(VARCHAR,PeriodTo,112))))";

            DT = MobjDataLayer.ExecuteDataTable(sSqlQuery);
            if (DT.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// To get Last Employee Transfer of particular Employee
        /// </summary>
        /// <returns>bool</returns>
        public bool LastTransferDateCheck()
        {
            DataTable DT;
            string sSqlQuery;
            sSqlQuery = "SELECT  TOP 1 TransferID FROM EmployeeTransfers  WHERE  EmployeeID=" + MobjDTOVacationEntry.EmployeeID + " AND " +
                        " (CONVERT(DATETIME,CONVERT(VARCHAR,TransferDate,106),106) > CONVERT(DATETIME,CONVERT(VARCHAR,'" + string.Format(MobjDTOVacationEntry.FromDate, "dd MMM yyyy") + "',106),106)) AND TransferTypeID=1    ORDER BY TransferID DESC";


            DT = MobjDataLayer.ExecuteDataTable(sSqlQuery);
            if (DT.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// To Get Date of Joining
        /// </summary>
        /// <returns>string</returns>
        public string GetDateofJoining()
        {
            DataTable DT;
            string sSqlQuery;
            sSqlQuery = "SELECT CONVERT(VARCHAR(15),DateofJoining,106) AS  DateofJoining  FROM EmployeeMaster WHERE EmployeeID=" + MobjDTOVacationEntry.EmployeeID + "   ORDER BY EmployeeID ";
            DT = MobjDataLayer.ExecuteDataTable(sSqlQuery);
            if (DT.Rows.Count > 0)
            {
                return DT.Rows[0][0].ToString();
            }
            else
            {
                return "";
            }
        }
        /// <summary>
        /// To get SalarystructureID for Employee
        /// </summary>
        /// <returns>bool</returns>
        public bool SalaryStructureCheck()
        {
            DataTable DT;
            string sSqlQuery;
            sSqlQuery = "SELECT  SalaryStructureID FROM PaySalaryStructure WHERE EmployeeID=" + MobjDTOVacationEntry.EmployeeID + " ";

            DT = MobjDataLayer.ExecuteDataTable(sSqlQuery);
            if (DT.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// To Fill Process Details
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable FillProcessDetails()
        {
            string TSQL = "";

            DeletePaymentDetails();

            TSQL = "DELETE FROM PayVacationDetail WHERE VacationID = " + MobjDTOVacationEntry.VacationID + "";
            MobjDataLayer.ExecuteQuery(TSQL);

            DataTable dtVacation;
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", "0"));
            parameters.Add(new SqlParameter("@EmployeeID", MobjDTOVacationEntry.EmployeeID));
            parameters.Add(new SqlParameter("@CompID", MobjDTOVacationEntry.CompanyID));
            parameters.Add(new SqlParameter("@FromDate1", MobjDTOVacationEntry.FromDate));
            parameters.Add(new SqlParameter("@ToDate1", MobjDTOVacationEntry.ToDate));
            parameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            parameters.Add(new SqlParameter("@IsProcessSalaryForCurrentMonth", MobjDTOVacationEntry.IsProcessSalaryForCurrentMonth));
            parameters.Add(new SqlParameter("@IsConsiderAbsentDays", MobjDTOVacationEntry.IsConsiderAbsentDays));
            parameters.Add(new SqlParameter("@EligibleLeavePayDays", MobjDTOVacationEntry.EligibleLeavePayDays));
            parameters.Add(new SqlParameter("@IsEncashOnly", MobjDTOVacationEntry.IsEncashOnly));
            parameters.Add(new SqlParameter("@EncashComboOffDay", MobjDTOVacationEntry.EncashComboOffDay));
            dtVacation = MobjDataLayer.ExecuteDataTable("spPayVacationProcessTransactions", parameters);
            return dtVacation;

        }


        public bool IsSalVacationAccountExists(int intEmpID)
        {
            int intIsExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 14), new SqlParameter("@EmployeeID", intEmpID) };
            intIsExists = this.DataLayer.ExecuteScalar("spPayVacationProcessTransactions", this.sqlParameters).ToInt32();
            return (intIsExists > 0);
        }

        /// <summary>
        /// To get Employee Details
        /// </summary>
        public void EmpDetail()
        {
            String sSqlQuery = " SELECT   EM.DateofJoining AS DateofJoining,cur.Code as ShortDescription, EM.CompanyID,H.CurrencyId AS CurrencyID,ISNULL(cur.Scale,0) AS Scale,ISNULL(EM.LeavePolicyID,0) AS LeavePolicyID,ISNULL(EM.TransactionTypeID,0) TransactionTypeID " +
                                " FROM EmployeeMaster AS EM " +
                                " INNER JOIN PaySalaryStructure AS H ON EM.EmployeeID = H.EmployeeID " +
                                " INNER JOIN CurrencyReference AS cur ON H.CurrencyId = cur.CurrencyID   " +
                                " WHERE EM.EmployeeID=" + MobjDTOVacationEntry.EmployeeID + " ";

            DataTable dt = MobjDataLayer.ExecuteDataTable(sSqlQuery);
            DataTable dtPolicyDetails = MobjDataLayer.ExecuteDataTable("SELECT VP.IsRateOnly FROM  PayVacationPolicy VP INNER JOIN EmployeeMaster EM ON EM.VacationPolicyID = VP.VacationPolicyID WHERE EM.EmployeeID = " + MobjDTOVacationEntry.EmployeeID + "");

            if ((dtPolicyDetails.Rows.Count > 0))
            {
                MobjDTOVacationEntry.IsRateOnly = Convert.ToBoolean(dtPolicyDetails.Rows[0]["IsRateOnly"]);
            }

            if (dt.Rows.Count > 0)
            {
                MobjDTOVacationEntry.CurrencyID = Convert.ToInt32(dt.Rows[0]["CurrencyID"]);
                MobjDTOVacationEntry.Scale = Convert.ToInt32(dt.Rows[0]["Scale"]);
                MobjDTOVacationEntry.CompanyID = Convert.ToInt32(dt.Rows[0]["CompanyID"]);
                MobjDTOVacationEntry.LeavePolicyID = Convert.ToInt32(dt.Rows[0]["LeavePolicyID"]);
                MobjDTOVacationEntry.ShortDescription = Convert.ToString(dt.Rows[0]["ShortDescription"]);
                MobjDTOVacationEntry.JoiningDate = Convert.ToDateTime(dt.Rows[0]["DateofJoining"]);
                MobjDTOVacationEntry.TransactionTypeID = Convert.ToInt32(dt.Rows[0]["TransactionTypeID"]);

                System.DateTime tJoiningDate = MobjDTOVacationEntry.JoiningDate;
                System.DateTime tToDate = Convert.ToDateTime(MobjDTOVacationEntry.FromDate);
                TimeSpan TS;
                TimeSpan TS2;

                TS2 = MobjDTOVacationEntry.ToDate.ToDateTime().Subtract(MobjDTOVacationEntry.FromDate.ToDateTime());

                MobjDTOVacationEntry.NoOfDays = TS2.Days.ToInt32() + 1;

                TS = tToDate.Subtract(tJoiningDate);
                MobjDTOVacationEntry.ExperienceDays = TS.Days.ToDecimal() + 1;



                //if (MobjDTOVacationEntry.IsConsiderAbsentDays == true)
                //{
                //    String TSQL = " SELECT COUNT(ISNULL(T.AttendanceID,0)) AS  AbsentDays FROM (SELECT AttendanceID,AttendenceStatusID,EmployeeID,[Date]  FROM  " +
                //                  " PayAttendanceMaster UNION  SELECT AttendanceID,AttendenceStatusID,EmployeeID,[Date]  FROM PayAttendanceMaster) " +
                //                  " T WHERE T.AttendenceStatusID = 4 AND T.EmployeeID = " + MobjDTOVacationEntry.EmployeeID + " AND CONVERT(DATETIME,CONVERT(CHAR(12),T.Date,106),106) < '" + MobjDTOVacationEntry.FromDate + "'";
                //    SqlDataReader sdr = MobjDataLayer.ExecuteReader(TSQL);
                //    if (sdr.Read())
                //    {
                //        if (sdr["AbsentDays"] != System.DBNull.Value)
                //        {
                //            MobjDTOVacationEntry.AbsentDays = Convert.ToDecimal(sdr["AbsentDays"]);
                //        }
                //    }
                //    sdr.Close();




                //}
                MobjDTOVacationEntry.AbsentDays = GetAbsentDays();

            }
        }

        public string GetEligibleCombo(bool blnIsLeavePay)
        {

            DataTable dtVacation;
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", "27"));
            parameters.Add(new SqlParameter("@EmployeeID", MobjDTOVacationEntry.EmployeeID));
            dtVacation = MobjDataLayer.ExecuteDataTable("spPayVacationProcessTransactions", parameters);


            if (dtVacation.Rows.Count > 0)
            {
                if (dtVacation.Rows[0]["EncashComboOffDay"] != System.DBNull.Value)
                {
                    return dtVacation.Rows[0]["EncashComboOffDay"].ToString();
                }
            }

            return "0";

        }

        /// <summary>
        /// To Get Eligible Leave Pay Days
        /// </summary>
        /// <param name="blnIsLeavePay"></param>
        /// <returns>string</returns>
        public string GetEligibleLeavePayDays(bool blnIsLeavePay)
        {

            DataTable dtVacation;
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", "11"));
            parameters.Add(new SqlParameter("@EmployeeID", MobjDTOVacationEntry.EmployeeID));
            parameters.Add(new SqlParameter("@CompID", MobjDTOVacationEntry.CompanyID));
            parameters.Add(new SqlParameter("@FromDate1", MobjDTOVacationEntry.FromDate));
            parameters.Add(new SqlParameter("@ToDate1", MobjDTOVacationEntry.ToDate));
            parameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            parameters.Add(new SqlParameter("@IsConsiderAbsentDays", MobjDTOVacationEntry.IsConsiderAbsentDays));
            parameters.Add(new SqlParameter("@IsLeavePay", blnIsLeavePay));
            dtVacation = MobjDataLayer.ExecuteDataTable("spPayVacationProcessTransactions", parameters);


            if (dtVacation.Rows.Count > 0)
            {
                if (dtVacation.Rows[0]["EligibleLeavePayDays"] != System.DBNull.Value)
                {
                    return dtVacation.Rows[0]["EligibleLeavePayDays"].ToString();
                }
            }

            return "0";

        }
        /// <summary>
        /// To get Employee History Details
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable EmployeeHistoryDetail()
        {
            DataTable datTempSet;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 17));
            parameters.Add(new SqlParameter("@EmployeeID", MobjDTOVacationEntry.EmployeeID));
            parameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            datTempSet = MobjDataLayer.ExecuteDataTable("spPayVacationProcessTransactions", parameters);
            return datTempSet;
        }
        /// <summary>
        /// To check whether leave exists in this period
        /// </summary>
        /// <returns>bool</returns>
        public bool CheckLeaveExists()
        {
            bool blnRetValue = false;

            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 19));
            parameters.Add(new SqlParameter("@EmployeeID", MobjDTOVacationEntry.EmployeeID));
            parameters.Add(new SqlParameter("@FromDate1", MobjDTOVacationEntry.FromDate));
            parameters.Add(new SqlParameter("@ToDate1", MobjDTOVacationEntry.ToDate));
            int result = MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", parameters).ToInt32();
            if (result > 0)
                blnRetValue = true;
            else
                blnRetValue = false;

            return blnRetValue;
        }
        /// <summary>
        /// Vacation Entry Email
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet EmailDetail()
        {
            DataSet dsEmail;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 20));
            parameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            dsEmail = MobjDataLayer.ExecuteDataSet("spPayVacationProcessTransactions", parameters);
            return dsEmail;
        }
        /// <summary>
        /// Check Employee Salary is Processed between this period
        /// </summary>
        /// <param name="lngEmployeeID"></param>
        /// <param name="dtDate"></param>
        /// <returns>long</returns>
        public long CheckEmployeeSalaryIsProcessed(long lngEmployeeID, DateTime dtDate)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 21));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            parameters.Add(new SqlParameter("@FromDate1", dtDate));
            parameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            return MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", parameters).ToInt64();
        }
        /// <summary>
        /// To save Vacation Details
        /// </summary>
        /// <returns>bool</returns>
        public bool InsertParticulars(int intAdditionDeductionID, decimal decAmount)
        {
            int intOrderNo = 0;

            string sSqlQuery;
            sSqlQuery = "SELECT  MAX(SerialNo) FROM PayVacationDetail WHERE VacationID=" + MobjDTOVacationEntry.VacationID + " ";

            intOrderNo = MobjDataLayer.ExecuteScalar(sSqlQuery).ToInt32();
        
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 5));
            alparameters.Add(new SqlParameter("@OrderNo", ++intOrderNo));
            alparameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            alparameters.Add(new SqlParameter("@AdditionDeductionID", intAdditionDeductionID));
            alparameters.Add(new SqlParameter("@Amount", decAmount));
            alparameters.Add(new SqlParameter("@NoOfDays", 0));
            alparameters.Add(new SqlParameter("@ReleaseLater", 0));
            alparameters.Add(new SqlParameter("@CompID", MobjDTOVacationEntry.CompanyID));
            alparameters.Add(new SqlParameter("@CurrencyID", MobjDTOVacationEntry.CurrencyID));
            MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", alparameters);
            return true;
        }
        /// <summary>
        /// To delete Payment Details
        /// </summary>
        /// <returns></returns>
        public bool DeleteVacationDetailParticular(int intAdditionDeductionID)
        {
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 22));
            alparameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            alparameters.Add(new SqlParameter("@AdditionDeductionID", intAdditionDeductionID));
            MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", alparameters);
            return true;
        }
        public bool UpdateParticularAmount(decimal decAmount, int intAdditionDeductionID)
        {
            bool blnRetValue = false;

            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 23));
            parameters.Add(new SqlParameter("@Amount", decAmount));
            parameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            parameters.Add(new SqlParameter("@AdditionDeductionID", intAdditionDeductionID));
            int result = MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", parameters).ToInt32();
            if (result > 0)
                blnRetValue = true;
            else
                blnRetValue = false;

            return blnRetValue;
        }
        /// <summary>
        /// To check whether 
        /// </summary>
        /// <returns>bool</returns>
        public bool CheckEmployeeVacationProcessedAfterDate()
        {
            bool blnRetValue = false;

            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 24));
            parameters.Add(new SqlParameter("@EmployeeID", MobjDTOVacationEntry.EmployeeID));
            parameters.Add(new SqlParameter("@FromDate1", MobjDTOVacationEntry.FromDate));
            parameters.Add(new SqlParameter("@ToDate1", MobjDTOVacationEntry.ToDate));
            int result = MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", parameters).ToInt32();
            if (result > 0)
                blnRetValue = true;
            else
                blnRetValue = false;

            return blnRetValue;
        }
        public decimal GetAbsentDays()
        {
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 25));
            parameters.Add(new SqlParameter("@EmployeeID", MobjDTOVacationEntry.EmployeeID));
            parameters.Add(new SqlParameter("@FromDate1", MobjDTOVacationEntry.FromDate));
            parameters.Add(new SqlParameter("@ToDate1", MobjDTOVacationEntry.ToDate));
            parameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            decimal result = MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", parameters).ToDecimal();
            return result;
        }
        public bool UpdateNetAmount(decimal decNetAmount, decimal decGivenAmount)
        {
          
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 26));
            alparameters.Add(new SqlParameter("@VacationID", MobjDTOVacationEntry.VacationID));
            alparameters.Add(new SqlParameter("@NetAmount", decNetAmount));
            alparameters.Add(new SqlParameter("@GivenAmount", decGivenAmount));
            int result = MobjDataLayer.ExecuteScalar("spPayVacationProcessTransactions", alparameters).ToInt32();
            return true;
        }
    }
}
