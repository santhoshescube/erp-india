﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;

namespace MyBooksERP
{
    class clsDALSalaryProcess : IDisposable
    {
        ArrayList prmProcess; // for setting Sql parameters
        ClsCommonUtility MObjClsCommonUtility;
        DataLayer MobjDataLayer;

        public clsDTOSalaryProcess PobjclsDTOSalaryProcess { get; set; } // DTO Salary Process Property

        public clsDALSalaryProcess(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }
  
        public void SaveSalary()
        {

            prmProcess = new ArrayList();
            prmProcess.Add(new SqlParameter("@FromDate1", PobjclsDTOSalaryProcess.strFromDate1));
            prmProcess.Add(new SqlParameter("@ToDate1", PobjclsDTOSalaryProcess.strToDate1));
            prmProcess.Add(new SqlParameter("@EmployeeID", PobjclsDTOSalaryProcess.intEmployeeID));
            prmProcess.Add(new SqlParameter("@CompanyID", PobjclsDTOSalaryProcess.intCompanyID));
            prmProcess.Add(new SqlParameter("@SettleFlag", PobjclsDTOSalaryProcess.intSettleFlag));
            prmProcess.Add(new SqlParameter("@UserID", PobjclsDTOSalaryProcess.intUserID));
            prmProcess.Add(new SqlParameter("@ProcessTypeID", PobjclsDTOSalaryProcess.intProcessTypeID));
            prmProcess.Add(new SqlParameter("@SalaryDayPar", PobjclsDTOSalaryProcess.intSalaryDay));
            prmProcess.Add(new SqlParameter("@WorkLocationID", PobjclsDTOSalaryProcess.intWorkLocationID));
            MobjDataLayer.ExecuteNonQuery("spPaySalaryProcess", prmProcess);

        }

        public DataSet GetProcessEmployeeList()  
        {

            prmProcess = new ArrayList();
            prmProcess.Add(new SqlParameter("@EmployeeID", PobjclsDTOSalaryProcess.intEmployeeID));
            prmProcess.Add(new SqlParameter("@CompanyID", PobjclsDTOSalaryProcess.intCompanyID));
            prmProcess.Add(new SqlParameter("@FromDate1", PobjclsDTOSalaryProcess.strFromDate1));
            prmProcess.Add(new SqlParameter("@ToDate1", PobjclsDTOSalaryProcess.strToDate1));
            prmProcess.Add(new SqlParameter("@MachineName", PobjclsDTOSalaryProcess.strHostName));
            prmProcess.Add(new SqlParameter("@UserID", PobjclsDTOSalaryProcess.intUserId));
            prmProcess.Add(new SqlParameter("@BranchId", PobjclsDTOSalaryProcess.intBranchIndicator));
            prmProcess.Add(new SqlParameter("@WorkLocationID", PobjclsDTOSalaryProcess.intWorkLocationID));
            return   MobjDataLayer.ExecuteDataSet("spPayEmployeeListWithOutData", prmProcess);

        }
        public int GetCurrentMonth()
        {
            DataTable dat = new DataTable();
            dat = MobjDataLayer.ExecuteDataTable("SELECT month(getdate()) as CurrentMonth , year(getdate()) CurrentYear");

            if (dat.Rows.Count > 0)
                return Convert.ToInt32(dat.Rows[0]["CurrentMonth"]);
            else
                return 0;
        }

        public int GetCurrentYear()
        {
            DataTable dat = new DataTable();
            dat = MobjDataLayer.ExecuteDataTable("SELECT month(getdate()) as CurrentMonth , year(getdate()) CurrentYear");

            if (dat.Rows.Count > 0)
                return Convert.ToInt32(dat.Rows[0]["CurrentYear"]);
            else
                return 0;
        }

        public string GetCurrentDate()
        {
            DataTable dat = new DataTable();
            dat = MobjDataLayer.ExecuteDataTable("select 'Current Date :' +  convert(char(15),getdate(),106) as CurrentDate");

            if (dat.Rows.Count > 0)
                return Convert.ToString(dat.Rows[0]["CurrentDate"]);
            else
                return "";
        }

        public int PayCheckForEmployee()
        {

            DataTable DT;
            prmProcess = new ArrayList();
            prmProcess.Add(new SqlParameter("@CompanyID", PobjclsDTOSalaryProcess.intCompanyID));
            prmProcess.Add(new SqlParameter("@EmployeeID", PobjclsDTOSalaryProcess.intEmployeeID));
            prmProcess.Add(new SqlParameter("@PaymentClassificationID", PobjclsDTOSalaryProcess.intPaymentClassificationID));
            prmProcess.Add(new SqlParameter("@FromDate1", PobjclsDTOSalaryProcess.strFromDate1));
            prmProcess.Add(new SqlParameter("@ToDate1", PobjclsDTOSalaryProcess.strToDate1));
            DT = MobjDataLayer.ExecuteDataTable("spPayCheckForEmployee", prmProcess);


            if (DT.Rows.Count > 0)
            {
                if (Convert.ToInt32(DT.Rows[0]["FLag"]) == 1)
                {
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
            else
            {
                return 3;
            }
        }



        public bool IsEmployeeExists()
        {

            DataTable DT;

            DT = MobjDataLayer.ExecuteDataTable("Select 1 from EmployeeMaster Where  WorkStatusID > 6 And CompanyID=" + PobjclsDTOSalaryProcess.intCompanyID);


            if (DT.Rows.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
       
        }


        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MObjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }

        public DataTable FillCombos(string sQuery)
        {
            // function for getting datatable for filling combo

            return MObjClsCommonUtility.FillCombos(sQuery);
           
        }


        #region IDisposable Members

        public void Dispose()
        {
            //disposing
            if (prmProcess != null)
                prmProcess = null;
            if (MObjClsCommonUtility != null)
                MObjClsCommonUtility = null;
        }

        #endregion
    }
}
