﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALAdditionDeduction
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOAdditionDeduction  objDTOAdditionDeduction { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        private List<SqlParameter> sqlParameters = null;

        private string strProcedureName = "spPayAddDedTransaction";
       

        public clsDALAdditionDeduction(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }
        public bool CheckDuplication(bool blnAddStatus, string[] saValues, int intId)
        {
            string sField = "AdditionDeduction,AdditionDeductionArb";
            string sTable = "PayAdditionDeductionReference";
            string sCondition = "";

            if (blnAddStatus) // Add mode
                sCondition = "(AdditionDeduction='" + saValues[0] + "' OR AdditionDeductionArb='" + saValues[0] + "')";
            else // Edit mode
                sCondition = "(AdditionDeduction='" + saValues[0] + "' OR AdditionDeductionArb='" + saValues[0] + "') and AdditionDeductionID <> " + intId + "";

            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
            }
        }
        public int AddDedIDExists(int iAddDedID)
        {
           
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", "CHKDEL"),
                new SqlParameter("@AdditionDeductionID", iAddDedID)
                 };
            return  this.MobjDataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            

        }

        /// <summary>
        /// Addition Deductionr Save
        /// </summary>
        /// <returns></returns>
        public bool AddDedSave()
        {
            bool blnRetValue = false;
            object objAddDedID = 0;
            this.sqlParameters = new List<SqlParameter>();

            if (objDTOAdditionDeduction.AdditionDeductionID  > 0)
            {

                this.sqlParameters.Add(new SqlParameter("@Mode", "UPD"));
                this.sqlParameters.Add(new SqlParameter("@AdditionDeductionID", this.objDTOAdditionDeduction.AdditionDeductionID));
            }
            else
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", "INS"));
            }

            this.sqlParameters.Add(new SqlParameter("@AdditionDeduction", this.objDTOAdditionDeduction.AdditionDeduction));
            this.sqlParameters.Add(new SqlParameter("@AdditionDeductionArb", this.objDTOAdditionDeduction.AdditionDeductionArb));
            this.sqlParameters.Add(new SqlParameter("@IsAddition", this.objDTOAdditionDeduction.IsAddition));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            this.sqlParameters.Add(objParam);
            if (this.MobjDataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out objAddDedID) != 0)
            {
                this.objDTOAdditionDeduction.AdditionDeductionID  = (int)objAddDedID;
                blnRetValue = true;
            }
            return blnRetValue;

        }

        public DataTable DisplayAdditionDeduction()
        {

            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", "SEL")
            };
            return MobjDataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        public bool DeleteAdditionDeduction()
        {
            bool blnRetValue = false;

            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode","DEL"),
                                    new SqlParameter("@AdditionDeductionID", this.objDTOAdditionDeduction.AdditionDeductionID)};
            if (this.MobjDataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }
        /// <summary>
        /// Check this addition deduction used in salary Structure
        /// </summary>
        /// <param name="iAddDedID"></param>
        /// <returns></returns>
        public bool AddDedIDExistInSalary(int iAddDedID)
        {
            long  intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", "CHKSAL"),
                new SqlParameter("@AdditionDeductionID", iAddDedID)
                 };
            intExists = this.MobjDataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt64();
            return (intExists > 0);

        }

    }
}
