﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Collections;

namespace MyBooksERP
{
    public class clsDALSalaryStructure
    {

        private clsDTOSalaryStructure MobjDTOSalaryStructure = null;
        private string strProcedureName = "spPaySalaryStructure";
        private List<SqlParameter> sqlParameters = null;

        DataLayer objDataLayer = null;

        public clsDALSalaryStructure() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.objDataLayer == null)
                    this.objDataLayer = new DataLayer();

                return this.objDataLayer;
            }
            set
            {
                this.objDataLayer = value;
            }
        }

        public clsDTOSalaryStructure DTOSalaryStructure
        {
            get
            {
                if (this.MobjDTOSalaryStructure == null)
                    this.MobjDTOSalaryStructure = new clsDTOSalaryStructure();

                return this.MobjDTOSalaryStructure;
            }
            set
            {
                this.MobjDTOSalaryStructure = value;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }


        public bool SalaryStructureSave(bool blnEditMode)
        {
            bool blnRetValue = false;
            object SalaryStructureID = 0;
            this.sqlParameters = new List<SqlParameter>();

            if (blnEditMode == false)
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", 1));

            }
            else
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", 2));
                this.sqlParameters.Add(new SqlParameter("@SalaryStructureID", this.DTOSalaryStructure.intSalaryStructureID));
            }

            this.sqlParameters.Add(new SqlParameter("@EmployeeID", this.DTOSalaryStructure.intEmployeeID));
            this.sqlParameters.Add(new SqlParameter("@PaymentClassificationID", this.DTOSalaryStructure.intPaymentClassificationID));
            this.sqlParameters.Add(new SqlParameter("@PayCalculationTypeID", this.DTOSalaryStructure.intPayCalculationTypeID));
            this.sqlParameters.Add(new SqlParameter("@Remarks", this.DTOSalaryStructure.strRemarks));
            this.sqlParameters.Add(new SqlParameter("@SalaryDay", this.DTOSalaryStructure.intSalaryDay));
            this.sqlParameters.Add(new SqlParameter("@CurrencyId", this.DTOSalaryStructure.intCurrencyID));

            if (this.DTOSalaryStructure.intOTPolicyID > 0)
            {
                this.sqlParameters.Add(new SqlParameter("@OTPolicyID", this.DTOSalaryStructure.intOTPolicyID));
            }
            if (this.DTOSalaryStructure.intAbsentPolicyID > 0)
            {
                this.sqlParameters.Add(new SqlParameter("@AbsentPolicyID", this.DTOSalaryStructure.intAbsentPolicyID));
            }
            this.sqlParameters.Add(new SqlParameter("@NetAmount", this.DTOSalaryStructure.decNetAmount));
            if (this.DTOSalaryStructure.intSetPolicyID>0)
               this.sqlParameters.Add(new SqlParameter("@SetPolicyID", this.DTOSalaryStructure.intSetPolicyID));
            //this.sqlParameters.Add(new SqlParameter("@IncentiveTypeID", this.DTOSalaryStructure.intIncentiveTypeID));
            //this.sqlParameters.Add(new SqlParameter("@LastPayment", this.DTOSalaryStructure.dateLastPayment));
            //this.sqlParameters.Add(new SqlParameter("@IsCurrencyForBP", this.DTOSalaryStructure.boolIsCurrencyForBP));
            if (this.DTOSalaryStructure.intHolidayPolicyID > 0)
               this.sqlParameters.Add(new SqlParameter("@HolidayPolicyID", this.DTOSalaryStructure.intHolidayPolicyID));
            if (this.DTOSalaryStructure.intEncashPolicyID > 0)
               this.sqlParameters.Add(new SqlParameter("@EncashPolicyID", this.DTOSalaryStructure.intEncashPolicyID));
            //this.sqlParameters.Add(new SqlParameter("@SettlementPolicyID", this.DTOSalaryStructure.intSettlementPolicyID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            this.sqlParameters.Add(objParam);
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out SalaryStructureID) != 0)
            {
                DTOSalaryStructure.intSalaryStructureID = (int)SalaryStructureID;
                blnRetValue = true;
            }
            return blnRetValue;

        }

        public bool SalaryStructureDetailSave(bool blnEditMode)
        {
            bool blnRetValue = false;

            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",13),
                                    new SqlParameter("@SalaryStructureID", this.DTOSalaryStructure.intSalaryStructureID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }


            foreach (clsDTOSalaryStructureDetail objDTOSalaryStructureDetail in DTOSalaryStructure.DTOSalaryStructureDetail)
            {
                this.sqlParameters = new List<SqlParameter>();
                if (blnEditMode == false || blnEditMode == true)
                {
                    this.sqlParameters.Add(new SqlParameter("@Mode", 3));

                }

                this.sqlParameters.Add(new SqlParameter("@SalaryStructureID", DTOSalaryStructure.intSalaryStructureID));
                this.sqlParameters.Add(new SqlParameter("@AdditionDeductionID", objDTOSalaryStructureDetail.intAdditionDeductionID));
                this.sqlParameters.Add(new SqlParameter("@Amount", objDTOSalaryStructureDetail.decAmount));
                this.sqlParameters.Add(new SqlParameter("@AdditionDeductionPolicyID", objDTOSalaryStructureDetail.intAdditionDeductionPolicyID));
                this.sqlParameters.Add(new SqlParameter("@CategoryID", objDTOSalaryStructureDetail.intCategoryID));

                if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters) != 0)
                {
                    blnRetValue = true;
                }
                SaveRemuneration(blnEditMode);

            }

            return blnRetValue;

        }

        public bool SaveRemuneration(bool blnEditMode)
        {
            bool blnRetValue = false;

            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",26),
                                    new SqlParameter("@SalaryStructureID", this.DTOSalaryStructure.intSalaryStructureID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }


            foreach (clsDTORemunerationDetail objDTORemunerationDetail in DTOSalaryStructure.DTORemunerationDetail)
            {
                this.sqlParameters = new List<SqlParameter>();
                
                this.sqlParameters.Add(new SqlParameter("@Mode", 24));
                this.sqlParameters.Add(new SqlParameter("@SalaryID", DTOSalaryStructure.intSalaryStructureID));
                this.sqlParameters.Add(new SqlParameter("@OtherRemunerationID", objDTORemunerationDetail.OtherRemunerationDetailID));
                this.sqlParameters.Add(new SqlParameter("@AmountRemuneration", objDTORemunerationDetail.AmountRemuneration));


                if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters) != 0)
                {
                    blnRetValue = true;
                }

            }

            return blnRetValue;
        }


        public bool DisplaySalaryStructureMaster(int intRowNumber,string strSearch,int iCompanyID)
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 9),
                new SqlParameter("@CompanyID", iCompanyID),
                new SqlParameter("@SearchText", strSearch),
                new SqlParameter("@RowNumber", intRowNumber) };
            SqlDataReader sdr = this.DataLayer.ExecuteReader(this.strProcedureName, this.sqlParameters);
            if (sdr.Read())
            {

                DTOSalaryStructure.intSalaryStructureID = sdr["SalaryStructureID"].ToInt32();
                DTOSalaryStructure.intEmployeeID = sdr["EmployeeID"].ToInt32();
                DTOSalaryStructure.intPaymentClassificationID = sdr["PaymentClassificationID"].ToInt32();
                DTOSalaryStructure.intPayCalculationTypeID = sdr["PayCalculationTypeID"].ToInt32();
                DTOSalaryStructure.strRemarks = sdr["Remarks"].ToString();
                DTOSalaryStructure.intSalaryDay = sdr["SalaryDay"].ToInt32();
                DTOSalaryStructure.intCurrencyID = sdr["CurrencyId"].ToInt32();
                DTOSalaryStructure.intCompanyID = sdr["CompanyID"].ToInt32();

                if (sdr["OTPolicyID"] != null)
                {
                    DTOSalaryStructure.intOTPolicyID = sdr["OTPolicyID"].ToInt32();
                }
                else
                {
                    DTOSalaryStructure.intOTPolicyID = -1;
                }
                if (sdr["AbsentPolicyID"] != null)
                {
                    DTOSalaryStructure.intAbsentPolicyID = sdr["AbsentPolicyID"].ToInt32();
                }
                else
                {
                    DTOSalaryStructure.intAbsentPolicyID = -1;
                }
                if (sdr["SetPolicyID"] != null)
                {
                    DTOSalaryStructure.intSetPolicyID = sdr["SetPolicyID"].ToInt32();
                }
                else
                {
                    DTOSalaryStructure.intSetPolicyID = -1;
                }
                if (sdr["HolidayPolicyID"] != null)
                {
                    DTOSalaryStructure.intHolidayPolicyID = sdr["HolidayPolicyID"].ToInt32();
                }
                else
                {
                    DTOSalaryStructure.intHolidayPolicyID = -1;
                }


                if (sdr["EncashPolicyID"] != null)
                {
                    DTOSalaryStructure.intEncashPolicyID = sdr["EncashPolicyID"].ToInt32();
                }
                else
                {
                    DTOSalaryStructure.intEncashPolicyID = -1;
                }

                blnRetValue = true;
            }
            sdr.Close();
            return blnRetValue;
        }


        public DataTable DispalySalaryStructureDetail(int SalaryStructureID)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 10),
                new SqlParameter("@SalaryStructureID", SalaryStructureID)};
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }



        public int GetRecordCount(string strSearch, int iCompanyID)
        {
            int intRecordCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@CompanyID", iCompanyID),
                new SqlParameter("@SearchText", strSearch),
                new SqlParameter("@Mode", 11) };
            intRecordCount = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRecordCount;
        }

        //public int GetCompanyIDForEmployee(int intEmployeeID)
        //{
        //    int intCompanyID = 0;
        //    this.sqlParameters = new List<SqlParameter> { 
        //        new SqlParameter("@Mode", 12),
        //        new SqlParameter("@EmployeeID", intEmployeeID)
        //    };
        //    intCompanyID = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
        //    return intCompanyID;
        //}

        public bool CheckSalaryReleased()
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 14),
               new SqlParameter("@EmployeeID", this.DTOSalaryStructure.intEmployeeID)
                };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);

        }
        public bool Updatevalidation()
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 20),
               new SqlParameter("@EmployeeID", this.DTOSalaryStructure.intEmployeeID)
                };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);

        }

        public bool DeleteSalaryStructure()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",15),
                                    new SqlParameter("@EmployeeID", this.DTOSalaryStructure.intEmployeeID),
                                    new SqlParameter("@SalaryStructureID", this.DTOSalaryStructure.intSalaryStructureID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }


        public bool SalaryStructureHistorySave()
        {
            bool blnRetValue = false;
            object SalaryStructureHistoryID = 0;
            this.sqlParameters = new List<SqlParameter>();


            this.sqlParameters.Add(new SqlParameter("@Mode", 16));
            this.sqlParameters.Add(new SqlParameter("@SalaryStructureID", this.DTOSalaryStructure.intSalaryStructureID));
            this.sqlParameters.Add(new SqlParameter("@EmployeeID", this.DTOSalaryStructure.intEmployeeID));


            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            this.sqlParameters.Add(objParam);
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out SalaryStructureHistoryID) != 0)
            {
                DTOSalaryStructure.intSalaryStructureHistoryID = (int)SalaryStructureHistoryID;
                blnRetValue = true;
            }
            return blnRetValue;

        }

        public int GetAdditionOrDeduction(int AdditionDeductionID)
        {
            int intIsAdddition = 0;
            bool blnIsAddition = false;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 17), 
                new SqlParameter("@AdditionDeductionID", AdditionDeductionID)
            };
            blnIsAddition = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToBoolean();
            intIsAdddition = blnIsAddition == true ? 1 : 0;
            return intIsAdddition;
        }


        public bool SearchItem(string SearchCriteria, int RowNum, out int TotalRows, int CompanyID)
        {
            TotalRows = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 18),
                new SqlParameter("@CompanyID", CompanyID),
                new SqlParameter("@RowNumber", RowNum),
                new SqlParameter("@SearchText", SearchCriteria)
            };
            SqlDataReader sdr = this.DataLayer.ExecuteReader(this.strProcedureName, this.sqlParameters);

            bool blnReturnValue = ReadSalarayStructureInfo(sdr);
            sdr.NextResult();

            if (sdr.Read())
            {
                TotalRows = Convert.ToInt32(sdr["RecordCount"]);
            }

            blnReturnValue = TotalRows > 0 ? true : false;
            sdr.Close();
            return blnReturnValue;

        }


        public bool ReadSalarayStructureInfo(SqlDataReader sdrSalaryStructure)
        {
            bool blnReturnValue = false;

            if (sdrSalaryStructure.Read())
            {
                DTOSalaryStructure.intSalaryStructureID = sdrSalaryStructure["SalaryStructureID"].ToInt32();//sdr["SalaryStructureID"].ToInt32();
                DTOSalaryStructure.intEmployeeID = sdrSalaryStructure["EmployeeID"].ToInt32();
                DTOSalaryStructure.intPaymentClassificationID = sdrSalaryStructure["PaymentClassificationID"].ToInt32();
                DTOSalaryStructure.intPayCalculationTypeID = sdrSalaryStructure["PayCalculationTypeID"].ToInt32();
                DTOSalaryStructure.strRemarks = sdrSalaryStructure["Remarks"].ToString();
                DTOSalaryStructure.intSalaryDay = sdrSalaryStructure["SalaryDay"].ToInt32();
                DTOSalaryStructure.intCurrencyID = sdrSalaryStructure["CurrencyId"].ToInt32();
                DTOSalaryStructure.intCompanyID = sdrSalaryStructure["CompanyID"].ToInt32();

                if (sdrSalaryStructure["OTPolicyID"] != null)
                {
                    DTOSalaryStructure.intOTPolicyID = sdrSalaryStructure["OTPolicyID"].ToInt32();
                }
                else
                {
                    DTOSalaryStructure.intOTPolicyID = -1;
                }
                if (sdrSalaryStructure["AbsentPolicyID"] != null)
                {
                    DTOSalaryStructure.intAbsentPolicyID = sdrSalaryStructure["AbsentPolicyID"].ToInt32();
                }
                else
                {
                    DTOSalaryStructure.intAbsentPolicyID = -1;
                }


                if (sdrSalaryStructure["EncashPolicyID"] != null)
                {
                    DTOSalaryStructure.intEncashPolicyID = sdrSalaryStructure["EncashPolicyID"].ToInt32();
                }
                else
                {
                    DTOSalaryStructure.intEncashPolicyID = -1;
                }

                if (sdrSalaryStructure["HolidayPolicyID"] != null)
                {
                    DTOSalaryStructure.intHolidayPolicyID = sdrSalaryStructure["HolidayPolicyID"].ToInt32();
                }
                else
                {
                    DTOSalaryStructure.intHolidayPolicyID = -1;
                }

                if (sdrSalaryStructure["SetPolicyID"] != null)
                {
                    DTOSalaryStructure.intSetPolicyID = sdrSalaryStructure["SetPolicyID"].ToInt32();
                }
                else
                {
                    DTOSalaryStructure.intSetPolicyID = -1;
                }


                blnReturnValue = true;
                //sdr.Close();

            }
            return blnReturnValue;
        }

        public int GetRowNumber(int iCompanyID, string strSearch)
        {
            int intRowNumber = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 21),
                 new SqlParameter("@CompanyID", iCompanyID),
                new SqlParameter("@SearchText", strSearch),
                 new SqlParameter("@EmployeeID", this.DTOSalaryStructure.intEmployeeID),};

            intRowNumber = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRowNumber;


        }

        public bool EmployeeIDExists()
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 22),
               new SqlParameter("@EmployeeID", this.DTOSalaryStructure.intEmployeeID)
                };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);

        }

        public DataTable DispalyRemunerationDetail(int SalaryStructureID)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 27),
                new SqlParameter("@SalaryID", SalaryStructureID)};
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }

        /// <summary>
        /// Get the details of a deduction policy
        /// </summary>
        /// <param name="iDeductionPolicyID">DeductionPolicyID</param>
        /// <returns>datatable of details</returns>
        public DataSet GetDeductionPolicyDetails(int iDeductionPolicyID)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 30),
                new SqlParameter("@AdditionDeductionPolicyId", iDeductionPolicyID)};
            return DataLayer.ExecuteDataSet(this.strProcedureName, this.sqlParameters);
        }


        public DataTable GetPolicyPermissions(int RoleID)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 28),
                new SqlParameter("@RoleID", RoleID)};
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }

        public DataTable GetAutoCompleteList(int intSearchIndex, int intCompanyID)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 29),
                new SqlParameter("@SearchIndex", intSearchIndex),
                new SqlParameter("@CompanyID", intCompanyID)};
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }

        /// <summary>
        /// email source
        /// </summary>
        /// <param name="iSalaryStructureID">salarystructureid</param>
        /// <returns>dataset of salarystructure details</returns>
        public DataSet DisplaySalaryStructure(int iSalaryStructureID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "19"));
            prmReportDetails.Add(new SqlParameter("@EmployeeID",0));
            prmReportDetails.Add(new SqlParameter("@SalaryStructureID", iSalaryStructureID));
            return DataLayer.ExecuteDataSet(this.strProcedureName, prmReportDetails);

        }

        /// <summary>
        /// Display salary structure by employeeid
        /// </summary>
        /// <param name="iSalaryStructureID"></param>
        /// <returns></returns>
        public static DataSet GetSalaryStructureEmail(int EmployeeID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "19"));
            prmReportDetails.Add(new SqlParameter("@EmployeeID", EmployeeID));
            return new  DataLayer().ExecuteDataSet("spPaySalaryStructure", prmReportDetails);

        }




        /// <summary>
        /// Check if employee workstatus is active
        /// </summary>
        /// <param name="iEmployeeID">employeeid</param>
        /// <returns>true/false</returns>
        public bool CheckIfEmployeeIsActive(int iEmployeeID)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 31),
                new SqlParameter("@EmployeeID", iEmployeeID)};
            return DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32()>0?true:false;
        }

        /// <summary>
        /// Check existance of salary processed without release
        /// </summary>
        /// <param name="iEmployeeID">employeeid</param>
        /// <returns>true/false</returns>
        public bool CheckIfEmployeeSalaryProcessed(int iEmployeeID)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 32),
                new SqlParameter("@EmployeeID", iEmployeeID)};
            return DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32() > 0 ? true : false;
        }


        //Created By Rajesh on 20-May-2014
        public static DataSet GetAllEmployeesSalaryStructure(int CompanyID, int DepartmentID, int EmployeeID)
        {
            List<SqlParameter> Params = new List<SqlParameter> { 
                new SqlParameter("@CompanyID", CompanyID ),
                  new SqlParameter("@DepartmentID", DepartmentID ),
                new SqlParameter("@EmployeeID", EmployeeID)};
            return new DataLayer().ExecuteDataSet("spPayAllEmployeesSalaryStructure", Params);

        }

    }
}
