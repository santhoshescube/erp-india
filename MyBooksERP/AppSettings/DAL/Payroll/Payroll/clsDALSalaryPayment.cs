﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;

namespace MyBooksERP
{

      public class clsDALSalaryPayment
    {

      ArrayList prmPayment; // for setting Sql parameters
      ClsCommonUtility MObjClsCommonUtility;
      DataLayer MobjDataLayer;
      //ClsCommonSettings MobjCommonStting;
  
      public clsDTOSalaryPayment PobjclsDTOSalaryPayment { get; set; } // DTO Salary Process Property

      public clsDALSalaryPayment(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MobjDataLayer;
            //MobjCommonStting = new ClsCommonSettings();  
        }


      public DataTable FillCombos(string[] saFieldValues)
      {
          //function for getting datatable for filling combo
          return MObjClsCommonUtility.FillCombos(saFieldValues);
      }

      public DataTable FillCombos(string sQuery)
      {
          //function for getting datatable for filling combo
          return MObjClsCommonUtility.FillCombos(sQuery);
      }
        public int GetCurrencyScale()
          {
              string sQuery;
              DataTable DT;
              sQuery = "SELECT  Y.CurrencyID, Y.CurrencyName, isnull(Y.Scale,2) as Scale FROM  CurrencyReference AS Y INNER JOIN CompanyMaster AS C ON C.CurrencyId = Y.CurrencyID WHERE C.CompanyID = " + PobjclsDTOSalaryPayment.intCompanyID + "";


              DT=MobjDataLayer.ExecuteDataTable(sQuery);

             if (DT.Rows.Count >0)
             {
                 return Convert.ToInt32(DT.Rows[0]["Scale"]);
             }
             else
             {
                 return 0;
             }

          }


        public int RecCountNavigate()
        {
            //Function for recount navigate
            int intRecordCnt;
            int iPaymentID;
            string sHostName;
            intRecordCnt = 0;
            string strTSQL;
            DataTable DT;

            iPaymentID = Convert.ToInt32(PobjclsDTOSalaryPayment.intPaymentID.ToString().Trim());

            sHostName = Convert.ToString(PobjclsDTOSalaryPayment.strGetHostName.ToString().Trim());


            strTSQL = "SELECT isnull(count(1),0) FROM PayTempEmployeeIDForPaymentRelease Where (PaymentID = " + iPaymentID + "or " + iPaymentID + "=0) AND MachineName = '" + sHostName + "'";

            DT = MobjDataLayer.ExecuteDataTable(strTSQL);

            if (DT.Rows.Count >0) 
            {
                intRecordCnt = Convert.ToInt32(DT.Rows[0][0]);
            }
            else
            {
                intRecordCnt = 0;
            }
           
            return intRecordCnt;
        }

        public DataTable GetSalaryPaymentDetail()
          {
            prmPayment = new ArrayList();
            prmPayment.Add(new SqlParameter("@Mode", 5));
            prmPayment.Add(new SqlParameter("@PaymentID", PobjclsDTOSalaryPayment.intPaymentID));
            return MobjDataLayer.ExecuteDataTable("spPayEmployeePayment", prmPayment);
          }


          public DataTable NetAmount()
          {
            prmPayment = new ArrayList();
            prmPayment.Add(new SqlParameter("@UserID", 5));
            prmPayment.Add(new SqlParameter("@GCompanyID", PobjclsDTOSalaryPayment.intCompanyID));
            prmPayment.Add(new SqlParameter("@MachineName", PobjclsDTOSalaryPayment.strGetHostName));
            return   MobjDataLayer.ExecuteDataTable ("spPayNetAmtCorrection", prmPayment);
          }


          public  bool FillPaymentID()
          {
              string sQuery="";

              sQuery = "Delete from PayTempEmployeeIDForPaymentRelease where  rtrim(ltrim(MachineName))='" + PobjclsDTOSalaryPayment.strGetHostName.ToString().Trim()   + "'";
              MobjDataLayer.ExecuteQuery(sQuery);

              foreach (clsDTOEmployeePayList objClsDTOEmployeePayList in PobjclsDTOSalaryPayment.lstEmployeePayList)
              {
                  sQuery = "Insert Into PayTempEmployeeIDForPaymentRelease(EmployeeID,ProcessDate,MachineName,PaymentID)  Select  " + objClsDTOEmployeePayList.intEmployeeID + ",'" + objClsDTOEmployeePayList.strProcessDate + "','" + objClsDTOEmployeePayList.strMachineName + "'," + objClsDTOEmployeePayList.intPaymentID + "";
                  MobjDataLayer.ExecuteQuery(sQuery);

              }
              return true;
          }



          public bool SalarySlipIsEditable()
          {
            String  sQuery= "";
            sQuery = "Select rtrim(ltrim(UPPER(ConfigurationValue))) from ConfigurationMaster where  ltrim(rtrim(ConfigurationItem))='SalarySlipIsEditable'";
            DataTable  datTemp;
            datTemp=MobjDataLayer.ExecuteDataTable(sQuery);

              if (datTemp!=null)
              {
                if (datTemp.Rows.Count > 0)
                {
                    if (datTemp.Rows[0][0].ToString() == "YES")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
              }
                if (sQuery == "YES" )
                {
                    return true;
                }
                else
                {
                    return false;
                }
          }

          public int GetPaymentID() 
        {
            DataTable  datTemp;
            datTemp = MobjDataLayer.ExecuteDataTable ("SELECT TEID.PaymentID FROM (SELECT ROW_NUMBER() OVER(ORDER BY PaymentID,MachineName) AS RowNumber,PaymentID,MachineName FROM PayTempEmployeeIDForPaymentRelease Where MachineName = '" + Convert.ToString(PobjclsDTOSalaryPayment.strGetHostName) + "')TEID where TEID.RowNumber=" + Convert.ToInt32(PobjclsDTOSalaryPayment.strRowCount) + " AND MachineName = '" + Convert.ToString(PobjclsDTOSalaryPayment.strGetHostName) + "'");

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    return Convert.ToInt32(datTemp.Rows[0][0].ToString());
                }
            }
            else
            {
                return 0;
            }
            return 0;
        }

          public bool ConfirmAll()
          {
              try
              {
                  prmPayment = new ArrayList();
                  prmPayment.Add(new SqlParameter("@UserID", PobjclsDTOSalaryPayment.intUserId));
                  prmPayment.Add(new SqlParameter("@GCompanyID", PobjclsDTOSalaryPayment.intCompanyID));
                  prmPayment.Add(new SqlParameter("@machinename", PobjclsDTOSalaryPayment.strGetHostName));
                  prmPayment.Add(new SqlParameter("@Today1", PobjclsDTOSalaryPayment.strPaymentDate));
                  prmPayment.Add(new SqlParameter("@NetAmountVou", PobjclsDTOSalaryPayment.dblNetAmount));
                  prmPayment.Add(new SqlParameter("@GroupFlag", "0"));
                  SqlParameter sqlParam = new SqlParameter("@SuccessFlag", SqlDbType.Bit);
                  sqlParam.Direction = ParameterDirection.Output;
                  prmPayment.Add(sqlParam);
                  object obj;

                  MobjDataLayer.ExecuteNonQuery("spPaySalaryRelease", prmPayment, out obj);
                  return obj.ToBoolean();
              }
              catch (Exception)
              {
                  return false;
              }

          }

          
       public bool ParticularsUpdate()
       {
            prmPayment = new ArrayList();
            prmPayment.Add(new SqlParameter("@Mode", 2));
            prmPayment.Add(new SqlParameter("@Today1", PobjclsDTOSalaryPayment.strPaymentDate  ));
            prmPayment.Add(new SqlParameter("@machinename", PobjclsDTOSalaryPayment.strGetHostName));
            prmPayment.Add(new SqlParameter("@GroupID", 0));
            MobjDataLayer.ExecuteNonQuery ("spPayParticularsList", prmPayment);
            return true;
       }
       public DataSet ParticularsSelect()
       {
           prmPayment = new ArrayList();
           prmPayment.Add(new SqlParameter("@Mode", 1));
           prmPayment.Add(new SqlParameter("@Today1", PobjclsDTOSalaryPayment.strPaymentDate));
           prmPayment.Add(new SqlParameter("@machinename", PobjclsDTOSalaryPayment.strGetHostName));
           prmPayment.Add(new SqlParameter("@GroupID", 0));
           return MobjDataLayer.ExecuteDataSet("spPayParticularsList", prmPayment);
           
       }
       public DataTable LoadReportBody()
       {
           prmPayment = new ArrayList();
           prmPayment.Add(new SqlParameter("@Mode", 7));
           prmPayment.Add(new SqlParameter("@MachineName", PobjclsDTOSalaryPayment.strGetHostName));
           return  MobjDataLayer.ExecuteDataTable("spPayEmployeePayment", prmPayment);
       }
       public bool DelTempTable()
       {
           string sQuery = "";
           sQuery = "Delete from PayTempEmployeeIDForPaymentRelease where  rtrim(ltrim(MachineName))='" + PobjclsDTOSalaryPayment.strGetHostName + "'";
           MobjDataLayer.ExecuteQuery(sQuery);
           return true;

       }

       public int IsAdditionFun()
       {
            DataTable DT;
            string sQuery = "Select isnull(IsAddition,0) as Addition from PayAdditionDeductionReference where AdditionDeductionID=" + PobjclsDTOSalaryPayment.intAddDedID + "";

            DT = MobjDataLayer.ExecuteDataTable(sQuery);

            if (DT.Rows.Count >0) 
            {
                return Convert.ToInt32(DT.Rows[0][0]);

            }
            else
            {
                return 0;
            }
             return 0;

       }

       public bool IsAccSet()
       {
           DataTable DT;
           string sQuery = "select 1 from AccAccountSettings where CompanyID=" + PobjclsDTOSalaryPayment.intCompanyID  + " and AccountTransactionTypeID=9";
           DT = MobjDataLayer.ExecuteDataTable(sQuery);

           if (DT.Rows.Count > 0)
           {
               return true;

           }
           else
           {
               return false;
           }
           return false;

       }



       public bool SavePayment()
       {
           //function for adding and updating bank details
           ArrayList prmPay;

           foreach (clsDTOSalaryPaymentList objDTOSalaryPaymentList in PobjclsDTOSalaryPayment.lstSalaryPaymentList)
           {
                prmPay = new ArrayList();
                prmPay.Add(new SqlParameter("@Mode", "3"));
                prmPay.Add(new SqlParameter("@PaymentDetailID", objDTOSalaryPaymentList.intPaymentDetailID));
                prmPay.Add(new SqlParameter("@PaymentID", objDTOSalaryPaymentList.intPaymentID));
                prmPay.Add(new SqlParameter("@AdditionDeductionID", objDTOSalaryPaymentList.intAddDedID));
                prmPay.Add(new SqlParameter("@Amount", objDTOSalaryPaymentList.dblAmount));
                prmPay.Add(new SqlParameter("@Remarks", objDTOSalaryPaymentList.strRemarks));
                prmPay.Add(new SqlParameter("@LoanID", objDTOSalaryPaymentList.intLoanID));
                prmPay.Add(new SqlParameter("@EmpVenFlag", objDTOSalaryPaymentList.intEmpVenFlag));
                prmPay.Add(new SqlParameter("@AddFlag", objDTOSalaryPaymentList.intAddFlag));
                prmPay.Add(new SqlParameter("@IsEditable", objDTOSalaryPaymentList.intIsEditable));
                prmPay.Add(new SqlParameter("@CompanyID", objDTOSalaryPaymentList.intCompanyID));
                MobjDataLayer.ExecuteNonQuery("spPayEmployeePayment", prmPay);
               
           }
           return true;
       }


       public bool SaveUpdatePayment()
       {
           //function for adding and updating bank details
               ArrayList prmPay;

               foreach (clsDTOSalaryPaymentList objDTOSalaryPaymentList in PobjclsDTOSalaryPayment.lstSalaryPaymentList)
               {
                   prmPay = new ArrayList();
                   prmPay.Add(new SqlParameter("@Mode", "5"));
                   prmPay.Add(new SqlParameter("@PaymentDetailID", objDTOSalaryPaymentList.intPaymentDetailID));
                   prmPay.Add(new SqlParameter("@PaymentID", objDTOSalaryPaymentList.intPaymentID));
                   prmPay.Add(new SqlParameter("@AdditionDeductionID", objDTOSalaryPaymentList.intAddDedID));
                   prmPay.Add(new SqlParameter("@Amount", objDTOSalaryPaymentList.dblAmount));
                   prmPay.Add(new SqlParameter("@Remarks", objDTOSalaryPaymentList.strRemarks));
                   prmPay.Add(new SqlParameter("@LoanID", objDTOSalaryPaymentList.intLoanID));
                   prmPay.Add(new SqlParameter("@EmpVenFlag", objDTOSalaryPaymentList.intEmpVenFlag));
                   prmPay.Add(new SqlParameter("@AddFlag", objDTOSalaryPaymentList.intAddFlag));
                   prmPay.Add(new SqlParameter("@IsEditable", objDTOSalaryPaymentList.intIsEditable));
                   prmPay.Add(new SqlParameter("@CompanyID", objDTOSalaryPaymentList.intCompanyID));
                   MobjDataLayer.ExecuteNonQuery("spPayEmployeePayment", prmPay);

               }
           return true;
       }


       public bool DeleteRow()
       {
           string sQuery="";

           sQuery = "Delete  from PayEmployeePaymentDetail where PaymentID=" + PobjclsDTOSalaryPayment.intPaymentID  + " and  PaymentDetailID=" + PobjclsDTOSalaryPayment.intPaymentDetailID + " ";
           MobjDataLayer.ExecuteQuery(sQuery);
           return true;
       }
       public string  GetBookStartDate()
       {
           DataTable DT;
           string DDate = ""; string sQuery;
           sQuery = "select convert(char(15),BookStartDate,106) as BookStartDate from CompanyMaster where CompanyID=" + PobjclsDTOSalaryPayment.intCompanyID + " ";
           DT= MobjDataLayer.ExecuteDataTable(sQuery);
           if (DT.Rows.Count >0)
           {
               DDate = Convert.ToString(DT.Rows[0][0]);
           }
           return DDate;
       }
    }
}
