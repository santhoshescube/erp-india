﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualBasic;

namespace MyBooksERP
{
    /* 
================================================
   Author:		<Author, Lince P Thomas>
   Create date: <Create Date,,8 Jan 2010>
   Description:	<Description,,Company Form>
 *
 *  Modified :	   <Saju>
    Modified date: <Modified Date,19 August 2013>
    Description:	<Description,Settlement Process Form>
================================================
*/
    public class clsDALSettlementProcess
    {


        ClsCommonUtility MObjClsCommonUtility;

        private clsDTOSettlementProcess MobjDTOSettlementProcess = null;
        private string strProcedureName = "spPayVacationProcessTransactions";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALSettlementProcess(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
        public clsDTOSettlementProcess DTOSettlementProcess
        {
            get
            {
                if (this.MobjDTOSettlementProcess == null)
                    this.MobjDTOSettlementProcess = new clsDTOSettlementProcess();

                return this.MobjDTOSettlementProcess;
            }
            set
            {
                this.MobjDTOSettlementProcess = value;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MObjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }
        public int RecCount()
        {
            int RecordCnt = 0;
            string TSQL;
            DataTable DT;

            TSQL = "SELECT COUNT(1) FROM PayEmployeeSettlement AS P " +
            " Where CompanyID in(select CompanyID from  UserCompanyDetails WHERE (U.UserID = " + ClsCommonSettings.UserID + "))";

            DT = MobjDataLayer.ExecuteDataTable(TSQL);
            if (DT.Rows.Count > 0)
            {
                RecordCnt = Convert.ToInt32(DT.Rows[0][0]);
            }
            else
            {
                RecordCnt = 0;
            }
            return RecordCnt;
        }
        public int GetRecordCount()
        {
            DataTable DT; int RecordCnt = 0;

            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 40));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@RoleID", ClsCommonSettings.RoleID));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            parameters.Add(new SqlParameter("@SearchKey", MobjDTOSettlementProcess.strSearchKey.ToStringCustom()));
            DT = MobjDataLayer.ExecuteDataTable("spPayEmployeeSettlementTransaction", parameters);

            if (DT.Rows.Count > 0)
            {
                if (DT.Rows[0][0].ToInt32() > 0)
                {
                    RecordCnt = Convert.ToInt32(DT.Rows[0][0]);
                }
                else
                {
                    RecordCnt = 0;
                }
            }
            else
            {
                RecordCnt = 0;
            }
            return RecordCnt;



        }
        public int IsSalaryProcessed()
        {
            try
            {
                DataTable datTemp; 
                datTemp = MobjDataLayer.ExecuteDataTable("SELECT * FROM PayEmployeePayment WHERE EmployeeID=" + MobjDTOSettlementProcess.EmployeeID + " AND Released=0 AND " +
                                                   " convert(datetime,convert(varchar,ProcessDate,106),106)='" + MobjDTOSettlementProcess.Date + "'");


                if (datTemp.Rows.Count > 0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int IsTransferDateGreater()
        {
            try
            {
                DataTable datTemp; string sSqlQuery = "";


                sSqlQuery = "select  top 1 TransferID from PayEmployeeTransfers  where  EmployeeID=" + MobjDTOSettlementProcess.EmployeeID + " and " +
                                " (convert(datetime,convert(varchar,TransferDate,106),106) > convert(datetime,convert(varchar,'" + MobjDTOSettlementProcess.Date + "',106),106)) and TransferTypeID=1   order by TransferID desc";
                datTemp = datTemp = MobjDataLayer.ExecuteDataTable(sSqlQuery);


                if (datTemp.Rows.Count > 0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int IsDateOfJoinGreater()
        {
            try
            {
                DataTable datTemp; string sSqlQuery = "";


                sSqlQuery = "select convert(varchar,DateofJoining,106) As  DateofJoining  from employeemaster where employeeid=" + MobjDTOSettlementProcess.EmployeeID + "   order by EmployeeID ";
                datTemp = datTemp = MobjDataLayer.ExecuteDataTable(sSqlQuery);


                if (datTemp.Rows.Count > 0)
                {
                    MobjDTOSettlementProcess.dateofjoining = Convert.ToDateTime(datTemp.Rows[0]["DateofJoining"]).ToString("dd-MMM-yyyy");
                    return 1;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception)
            {
                return 0;
            }
        }


        public int IsEmployeeSettled()
        {
            try
            {
                DataTable datTemp; string sSqlQuery = "";
                if (MobjDTOSettlementProcess.AddEditMode == 1)
                {
                    sSqlQuery = "select 1 from PayEmployeeSettlement where SettlementType<=6 and EmployeeID=" + MobjDTOSettlementProcess.EmployeeID + " and " +
                    " (convert(datetime,convert(char(15),ToDate,106),106) " +
                    "between '" + MobjDTOSettlementProcess.FromDate + "' and convert(datetime,'" + MobjDTOSettlementProcess.ToDate + "',106) " +
                    "or convert(datetime,convert(char(15),FromDate,106),106) between '" + MobjDTOSettlementProcess.FromDate + "' " +
                    "and convert(datetime,'" + MobjDTOSettlementProcess.ToDate + "',106) )  and CompanyID=" + MobjDTOSettlementProcess.CompanyID + "";
                }
                else
                {
                    sSqlQuery = "select 1 from PayEmployeeSettlement where SettlementID<>" + MobjDTOSettlementProcess.SettlementID + " and EmployeeID=" + MobjDTOSettlementProcess.EmployeeID + " and " +
                    "SettlementType=" + MobjDTOSettlementProcess.SettlementType + " and (convert(datetime,convert(char(15),ToDate,106),106) " +
                    "between '" + MobjDTOSettlementProcess.FromDate + "' and convert(datetime,'" + MobjDTOSettlementProcess.ToDate + "',106) " +
                    "or convert(datetime,convert(char(15),FromDate,106),106) between '" + MobjDTOSettlementProcess.FromDate + "' " +
                    "and convert(datetime,'" + MobjDTOSettlementProcess.ToDate + "',106) )  and CompanyID=" + MobjDTOSettlementProcess.CompanyID + "";
                }
                datTemp = MobjDataLayer.ExecuteDataTable(sSqlQuery);
                if (datTemp.Rows.Count > 0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception)
            {
                return 0;
            }
        }


        public bool DeleteSettlementDetail()
        {
                    string sSqlQuery;



                    sSqlQuery = "DELETE FROM PayEmployeePaymentDetail WHERE PaymentID IN (SELECT PaymentID FROM PayEmployeePayment " +
                                    "WHERE EmployeeID=" + MobjDTOSettlementProcess.EmployeeID + " and isnull(IsSet,0)=1 AND Released=0)";

                    MobjDataLayer.ExecuteQuery(sSqlQuery);


                    sSqlQuery = "DELETE FROM PayEmployeePaymentDeductionDtls WHERE PaymentID IN (SELECT PaymentID FROM PayEmployeePayment " +
                        "WHERE EmployeeID=" + MobjDTOSettlementProcess.EmployeeID + " and isnull(IsSet,0)=1 AND  Released=0)";

                    MobjDataLayer.ExecuteQuery(sSqlQuery);


                    sSqlQuery = "DELETE FROM PayEmployeePaymentEncash WHERE PaymentID IN (SELECT PaymentID FROM PayEmployeePayment " +
                    "WHERE EmployeeID=" + MobjDTOSettlementProcess.EmployeeID + " and isnull(IsSet,0)=1 AND  Released=0)";

                    MobjDataLayer.ExecuteQuery(sSqlQuery);

                    sSqlQuery = "DELETE FROM PaySlipWorkInfo WHERE PaymentID IN (SELECT PaymentID FROM PayEmployeePayment " +
                        "WHERE EmployeeID=" + MobjDTOSettlementProcess.EmployeeID + " and isnull(IsSet,0)=1 AND  Released=0)";

                    MobjDataLayer.ExecuteQuery(sSqlQuery);

                    //sSqlQuery = "Delete LR from PayEmployeeSettlementDetail S "+
                    //            "INNER JOIN LoanRepayment LR ON S.RepaymentID= LR.RepaymentID  AND S.LoanID=LR.LoanID"+
                    //            " where S.SettlementID= " +MobjDTOSettlementProcess.SettlementID +"";
                                      
                    //MobjDataLayer.ExecuteQuery(sSqlQuery);

                    //sSqlQuery = "DELETE FROM PayProjectPaymentDetails WHERE  PaymentID IN (SELECT PaymentID FROM PayEmployeePayment " +
                    //    "WHERE EmployeeID=" + MobjDTOSettlementProcess.EmployeeID + " and isnull(IsSet,0)=1 AND  Released=0)";
                    //MobjDataLayer.ExecuteQuery(sSqlQuery);

                    sSqlQuery = "DELETE FROM PayEmployeePayment WHERE PaymentID IN (SELECT PaymentID FROM PayEmployeePayment " +
                        "WHERE EmployeeID=" + MobjDTOSettlementProcess.EmployeeID + " and isnull(IsSet,0)=1 AND  Released=0)";
                    MobjDataLayer.ExecuteQuery(sSqlQuery);

                    ArrayList parameterst = new ArrayList();
                    parameterst.Add(new SqlParameter("@Mode", 35));
                    parameterst.Add(new SqlParameter("@SettlementID", MobjDTOSettlementProcess.SettlementID));
                    parameterst.Add(new SqlParameter("@StatusSettlementDetail", 1));
                    MobjDataLayer.ExecuteNonQuery("spPayEmployeeSettlementTransaction", parameterst);

                return true ;
        }
        public int RecCountNavigate()
        {
            int RecordCnt = 0;
            string TSQL;
            DataTable DT;

            TSQL = "Select isnull(count(1),0) from [PayEmployeeSettlement]";
            DT = MobjDataLayer.ExecuteDataTable(TSQL);
            if (DT.Rows.Count > 0)
            {
                RecordCnt = Convert.ToInt32(DT.Rows[0][0]);
            }
            else
            {
                RecordCnt = 0;
            }

            return RecordCnt;
        }
        public bool SaveEmployeeSettlement()
        {
            try
             {
                 object IntSettlementID; string sSqlQuery; bool blnSalaryExists =false;
                ArrayList ParametersSet = new ArrayList();
                if (MobjDTOSettlementProcess.AddEditMode == 1)
                {
                    ParametersSet.Add(new SqlParameter("@Mode", 2));
                }
                else
                {
                    ParametersSet.Add(new SqlParameter("@Mode", 3));
                    IntSettlementID = MobjDTOSettlementProcess.SettlementID;
                }

                ParametersSet.Add(new SqlParameter("@SettlementID", MobjDTOSettlementProcess.SettlementID ));
                ParametersSet.Add(new SqlParameter("@EmployeeID", MobjDTOSettlementProcess.EmployeeID ));
                ParametersSet.Add(new SqlParameter("@Date", MobjDTOSettlementProcess.Date));

                if (MobjDTOSettlementProcess.DocumentReturn == true)
                {
                    ParametersSet.Add(new SqlParameter("@DocumentReturn", 1));
                    ParametersSet.Add(new SqlParameter("@AssetReturn", 1));
                }
                else
                {
                    ParametersSet.Add(new SqlParameter("@DocumentReturn", 0));
                    ParametersSet.Add(new SqlParameter("@AssetReturn", 0));
                }

                ParametersSet.Add(new SqlParameter("@Remarks", MobjDTOSettlementProcess.Remarks ));
                ParametersSet.Add(new SqlParameter("@NetAmount", MobjDTOSettlementProcess.NetAmount ));
                ParametersSet.Add(new SqlParameter("@CurrencyID", MobjDTOSettlementProcess.CurrencyID));
                ParametersSet.Add(new SqlParameter("@SettlementType", MobjDTOSettlementProcess.SettlementType ));
                ParametersSet.Add(new SqlParameter("@FromDate", MobjDTOSettlementProcess.FromDate));
                ParametersSet.Add(new SqlParameter("@ToDate", MobjDTOSettlementProcess.ToDate));
                ParametersSet.Add(new SqlParameter("@Closed", MobjDTOSettlementProcess.Closed));
                ParametersSet.Add(new SqlParameter("@CompanyID ", MobjDTOSettlementProcess.CompanyID));
                ParametersSet.Add(new SqlParameter("@StatusSettlementDetail", 0));
                ParametersSet.Add(new SqlParameter("@PaymentID", MobjDTOSettlementProcess.PaymentID));
                ParametersSet.Add(new SqlParameter("@ExperienceDays", MobjDTOSettlementProcess.ExperienceDays));
                ParametersSet.Add(new SqlParameter("@EligibleLeavePayDays", (MobjDTOSettlementProcess.EligibleLeavePayDays == 0 ? 0 : MobjDTOSettlementProcess.EligibleLeavePayDays)));
                ParametersSet.Add(new SqlParameter("@TakenLeavePayDays", (MobjDTOSettlementProcess.TakenLeavePayDays == 0 ? 0 : MobjDTOSettlementProcess.TakenLeavePayDays)));
                ParametersSet.Add(new SqlParameter("@VacationDays", MobjDTOSettlementProcess.VacationDays));
                ParametersSet.Add(new SqlParameter("@AbsentDays", MobjDTOSettlementProcess.AbsentDays ));
                ParametersSet.Add(new SqlParameter("@ConsiderAbsentDays", MobjDTOSettlementProcess.ConsiderAbsentDays));
                ParametersSet.Add(new SqlParameter("@AccountID", MobjDTOSettlementProcess.AccountID));
                ParametersSet.Add(new SqlParameter("@TransactionTypeID", MobjDTOSettlementProcess.TransactionTypeID));
                ParametersSet.Add(new SqlParameter("@ChequeNumber", MobjDTOSettlementProcess.ChequeNumber));
                ParametersSet.Add(new SqlParameter("@ChequeDate", MobjDTOSettlementProcess.ChequeDate));
                ParametersSet.Add(new SqlParameter("@CreatedBy", MobjDTOSettlementProcess.CreatedBy));


                SqlParameter sqlParam = new SqlParameter("@RetVal", SqlDbType.Int);
                sqlParam.Direction = ParameterDirection.Output;
                ParametersSet.Add(sqlParam);
                IntSettlementID = MobjDataLayer.ExecuteScalar("spPayEmployeeSettlementTransaction", ParametersSet);
                if (Convert.ToInt32(IntSettlementID) > 0)
                {

                    MobjDTOSettlementProcess.SettlementID = Convert.ToInt32(IntSettlementID);
                    if (MobjDTOSettlementProcess.SettlementID>0)
                    {
                        if (MobjDTOSettlementProcess.lstclsDTOSettlementDetails != null)
                        {
                            sSqlQuery = "DELETE FROM PayEmployeeSettlementDetail WHERE SettlementID=" + MobjDTOSettlementProcess.SettlementID + "";
                            MobjDataLayer.ExecuteQuery(sSqlQuery);

                            foreach (clsDTOSettlementDetails objclsDTOSettlementDetails in MobjDTOSettlementProcess.lstclsDTOSettlementDetails)
                            {
                                if (objclsDTOSettlementDetails.Particulars != "")
                                {
                                    ArrayList parameters = new ArrayList();
                                    parameters.Add(new SqlParameter("@EmployeeSettlementDetailID", Convert.ToInt32(objclsDTOSettlementDetails.EmployeeSettlementDetailID)));
                                    parameters.Add(new SqlParameter("@SettlementID", MobjDTOSettlementProcess.SettlementID));
                                    parameters.Add(new SqlParameter("@Particulars", Convert.ToString(objclsDTOSettlementDetails.Particulars)));
                                    if (objclsDTOSettlementDetails.IsAddition == true)
                                    {
                                        parameters.Add(new SqlParameter("@Amount", objclsDTOSettlementDetails.AmountCredit));
                                    }
                                    else
                                    {
                                        parameters.Add(new SqlParameter("@Amount", objclsDTOSettlementDetails.AmountDebit));
                                    }
                                    parameters.Add(new SqlParameter("@IsAddition", objclsDTOSettlementDetails.IsAddition));
                                    parameters.Add(new SqlParameter("@LoanID", objclsDTOSettlementDetails.LoanID));
                                    parameters.Add(new SqlParameter("@Mode", 2));
                                    parameters.Add(new SqlParameter("@StatusSettlementDetail", 1));
                                    if (MobjDTOSettlementProcess.Closed == true)
                                    {
                                        parameters.Add(new SqlParameter("@Confirm", 1));
                                    }
                                    else
                                    {
                                        parameters.Add(new SqlParameter("@Confirm",0));
                                    }

                                    parameters.Add(new SqlParameter("@EmployeeID", MobjDTOSettlementProcess.EmployeeID));
                                    parameters.Add(new SqlParameter("@Date", MobjDTOSettlementProcess.Date));
                                    parameters.Add(new SqlParameter("@PaymentID", objclsDTOSettlementDetails.SalaryPaymentID));

                                    if (objclsDTOSettlementDetails.SalaryPaymentID > 0)
                                    {
                                        blnSalaryExists = true;
                                    }
                                    MobjDataLayer.ExecuteNonQuery("spPayEmployeeSettlementTransaction", parameters);
                                }
                            }

                        }
                    }


                    if (MobjDTOSettlementProcess.lstclsDTOGratuityDetails != null)
                    {
                        sSqlQuery = "DELETE FROM PayEmployeeSettlementGratuity WHERE SettlementID=" + MobjDTOSettlementProcess.SettlementID + "";
                        MobjDataLayer.ExecuteQuery(sSqlQuery);

                        foreach (clsDTOGratuityDetails objclsGratuityDetails in MobjDTOSettlementProcess.lstclsDTOGratuityDetails)
                        {

                                ArrayList parameters = new ArrayList();
                                parameters.Add(new SqlParameter("@Mode", 43));
                                parameters.Add(new SqlParameter("@SettlementID", MobjDTOSettlementProcess.SettlementID));
                                parameters.Add(new SqlParameter("@Years", objclsGratuityDetails.ColYears));
                                parameters.Add(new SqlParameter("@CalculationDays", objclsGratuityDetails.ColCalDays));
                                parameters.Add(new SqlParameter("@GratuityDays", objclsGratuityDetails.ColGRTYDays));
                                parameters.Add(new SqlParameter("@GratuityAmount", objclsGratuityDetails.ColGratuityAmt));
                                MobjDataLayer.ExecuteNonQuery("spPayEmployeeSettlementTransaction", parameters);
                            
                        }
                    }

                    if (blnSalaryExists == false)
                    {
                        sSqlQuery = "	UPDATE PayEmployeeSettlement SET PaymentID=NULL WHERE SettlementID  = "+MobjDTOSettlementProcess.SettlementID + "";
                        MobjDataLayer.ExecuteQuery(sSqlQuery);
                    }

                    sSqlQuery = "update EmployeeMaster set WorkStatusID=" + MobjDTOSettlementProcess.SettlementType + " where employeeid=" + MobjDTOSettlementProcess.EmployeeID + "";
                    MobjDataLayer.ExecuteQuery(sSqlQuery);
                }
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public int IsPaymentReleaseChecking()
        {
            try
            {
                DataTable datTemp; string sSqlQuery = "";


                sSqlQuery = "EXEC spPayPaymentReleaseChecking  " + MobjDTOSettlementProcess.EmployeeID + ",'" + MobjDTOSettlementProcess.ToDate + "'";
                datTemp = MobjDataLayer.ExecuteDataTable(sSqlQuery);


                if (datTemp.Rows.Count > 0)
                {
                    if (datTemp.Rows[0][0].ToInt32()>0)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int IsPaymentReleaseCheckingPrevious()
        {
            try
            {
                DataTable datTemp; string sSqlQuery = "";

                sSqlQuery = "EXEC spPayPaymentReleaseChecking  " + MobjDTOSettlementProcess.EmployeeID + ",'" + MobjDTOSettlementProcess.ToDate + "',3";
                datTemp = MobjDataLayer.ExecuteDataTable(sSqlQuery);

                if (datTemp.Rows.Count > 0)
                {
                    if (datTemp.Rows[0][0].ToInt32() > 0)
                        return 1;
                    else
                        return 0;
                }
                else
                    return 0;

            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int IsAttendanceExisting()
        {
            try
            {
                DataTable datTemp; string sSqlQuery = "";


                sSqlQuery = "EXEC spPayEmployeeSettlementTransaction @EmployeeID=" + MobjDTOSettlementProcess.EmployeeID + ",@Todate='" + MobjDTOSettlementProcess.ToDate + "',@Mode=8,@StatusSettlementDetail=0";
                datTemp = MobjDataLayer.ExecuteDataTable(sSqlQuery);


                if (datTemp.Rows.Count > 0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }


            }
            catch (Exception)
            {
                return 0;
            }
        }

        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";
            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }
            return Months;
        }
        public int IsValidSet(int SetTypeID, int EmpID)
        {
            try
            {
                //if (ClsCommonSettings.IsHrPowerEnabled)
                //{
                    DataTable dt;
                    ArrayList parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 44));
                    parameters.Add(new SqlParameter("@SettlementType", SetTypeID));
                    parameters.Add(new SqlParameter("@EmployeeID", EmpID));
                    dt= MobjDataLayer.ExecuteDataTable("spPayEmployeeSettlementTransaction", parameters);

                    if (dt.Rows.Count > 0)
                    {
                        return dt.Rows[0][0].ToInt32(); 
                    }
                    else
                    {
                        return 0;
                    }

                //}
                //else
                //{
                //    return 1;
                //}
            }
            catch
            {
                return 0;
            }
        }

        public DataTable SettlementProcess()
        {
            DataTable datTemp; int TempTypeID = 0; int intSalaryDay = 0; string ParaProcessDateFrom="";string ParaProcessDateTo="";DateTime  PreviousMonthDate;
            decimal  PaymentID=0;
            string TSQL = ""; int intTransferDay = 0;

                TSQL = "select isnull(SalaryDay,1) as SalaryDay from PaySalaryStructure where EmployeeID=" + MobjDTOSettlementProcess.EmployeeID + "";
                datTemp = MobjDataLayer.ExecuteDataTable(TSQL);

                if (datTemp.Rows.Count > 0)
                {
                    intSalaryDay = Convert.ToInt32(datTemp.Rows[0]["SalaryDay"]);
                }
                else
                {
                    intSalaryDay = 1;
                }
                PreviousMonthDate = DateAndTime.DateAdd(DateInterval.Month, -1,Convert.ToDateTime(MobjDTOSettlementProcess.ToDate));
                if (intSalaryDay < 1)
                {
                    intSalaryDay = 1;
                }
                intTransferDay = DateAndTime.Day(Convert.ToDateTime(MobjDTOSettlementProcess.ToDate));

                if (intSalaryDay <= intTransferDay)
                {
                    ParaProcessDateFrom = intSalaryDay.ToString() + "-" + GetCurrentMonth(DateAndTime.Month(MobjDTOSettlementProcess.ToDate.ToDateTime())) + "-" + DateAndTime.Year(MobjDTOSettlementProcess.ToDate.ToDateTime()) + "";
                }
                else
                {
                    ParaProcessDateFrom = intSalaryDay.ToString() + "-" + GetCurrentMonth(DateAndTime.Month(PreviousMonthDate)) + "-" + DateAndTime.Year(PreviousMonthDate) + "";
                }

                ParaProcessDateTo = MobjDTOSettlementProcess.ToDate;



                TSQL = "SELECT dbo.fnGetAttendanceType(" + MobjDTOSettlementProcess.EmployeeID.ToString() + ",'" + ParaProcessDateFrom + "','" + ParaProcessDateTo + "')";
            datTemp = MobjDataLayer.ExecuteDataTable(TSQL);

            if (datTemp.Rows.Count > 0)
            {
                TempTypeID = Convert.ToInt32(datTemp.Rows[0][0]);
            }
         
                ArrayList parameters = new ArrayList();
                //if (TempTypeID != 0)
                //{
                parameters.Add(new SqlParameter("@CompanyID", MobjDTOSettlementProcess.CompanyID));
                parameters.Add(new SqlParameter("@FromDate1", ParaProcessDateFrom));
                parameters.Add(new SqlParameter("@ToDate1", ParaProcessDateTo));
                parameters.Add(new SqlParameter("@EmployeeID", MobjDTOSettlementProcess.EmployeeID));
                parameters.Add(new SqlParameter("@IsFromTransfer", 0));
                parameters.Add(new SqlParameter("@TransferFromDate1", "Null"));
                parameters.Add(new SqlParameter("@IsPartial", 0));
                parameters.Add(new SqlParameter("@IsVacation", 0));
                parameters.Add(new SqlParameter("@SettleFlag", 3));
                parameters.Add(new SqlParameter("@UserID", 2));
                parameters.Add(new SqlParameter("@SettleDate1", MobjDTOSettlementProcess.ToDate));
                parameters.Add(new SqlParameter("@ProcessTypeID", TempTypeID));
                parameters.Add(new SqlParameter("@AccrualSettlement", 0));

                SqlParameter sqlParam2 = new SqlParameter("@PaymentIDRet", SqlDbType.Decimal);
                sqlParam2.Direction = ParameterDirection.Output;
                parameters.Add(sqlParam2);
                object obj;
                DataLayer.ExecuteNonQuery("spPaySalaryProcess", parameters, out obj);
                PaymentID = obj.ToDecimal();

          

                //}

                parameters = null;
            //'' For Salary Details
            DataTable datTempSet;
            //DataTable datTempSalary;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@StatusSettlementDetail", 0));
            parameters.Add(new SqlParameter("@EmployeeID", MobjDTOSettlementProcess.EmployeeID));
            parameters.Add(new SqlParameter("@FromDate", ParaProcessDateFrom));
            parameters.Add(new SqlParameter("@ToDate", ParaProcessDateTo));
            parameters.Add(new SqlParameter("@PaymentID", PaymentID));
            datTempSet = MobjDataLayer.ExecuteDataTable("spPayEmployeeSettlementTransaction", parameters);



            if (datTempSet.Rows.Count > 0)
            {
                MobjDTOSettlementProcess.PaymentAmount = datTempSet.Rows[0]["Amt"].ToDecimal(); // Salary Amount
                MobjDTOSettlementProcess.PaymentIdTemp = datTempSet.Rows[0]["PaymentID"].ToDecimal(); // Salary PaymentID
                MobjDTOSettlementProcess.PaymentID  = datTempSet.Rows[0]["PaymentID"].ToDecimal(); // Salary PaymentID
            }

            datTempSet.Dispose(); 
             datTempSet=new  DataTable();
             datTempSet = MobjDataLayer.ExecuteDataTable("SELECT * FROM PayEmployeePayment WHERE PaymentID=" + MobjDTOSettlementProcess.PaymentID + " ");


            return datTempSet;

        }


        public DataSet SettlementProcessDetail()
        {
            DataSet datTempSet;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@EmployeeID", MobjDTOSettlementProcess.EmployeeID));
            parameters.Add(new SqlParameter("@CompID", MobjDTOSettlementProcess.CompanyID));
            parameters.Add(new SqlParameter("@FromDate1", MobjDTOSettlementProcess.FromDate));
            parameters.Add(new SqlParameter("@ToDate1", MobjDTOSettlementProcess.ToDate));
            parameters.Add(new SqlParameter("@SepType", MobjDTOSettlementProcess.SettlementType));
            //parameters.Add(new SqlParameter("@SettlementExp", MobjDTOSettlementProcess.ExperienceDays));
            parameters.Add(new SqlParameter("@SettlementExp", MobjDTOSettlementProcess.decEligibleDays));
            parameters.Add(new SqlParameter("@BalanceLeavePay", MobjDTOSettlementProcess.LeavePayDays));
            datTempSet = MobjDataLayer.ExecuteDataSet("spPayFinalSettlementProcess", parameters);
            return datTempSet;
        }

        public DataTable EmployeeHistoryDetail()
        {
            DataTable datTempSet;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@EmployeeID", MobjDTOSettlementProcess.EmployeeID));
            parameters.Add(new SqlParameter("@Mode",39));
            datTempSet = MobjDataLayer.ExecuteDataTable("spPayEmployeeSettlementTransaction", parameters);
            return datTempSet;
        }

        public bool GetSalaryReleaseDate(int intEmpID, ref DateTime DtpToDate)
        {
            try
            {

                if (intEmpID > 0)
                {
                    string sQuery;
                    DataTable DT;
                    sQuery = "SELECT isnull(Max(PeriodTo),getdate()) AS PeriodTo FROM PayEmployeePayment WHERE EmployeeID=" + intEmpID + "";
                    DT = MobjDataLayer.ExecuteDataTable(sQuery);
                    if (DT.Rows.Count > 0)
                    {
                        DtpToDate = Convert.ToDateTime(DT.Rows[0]["PeriodTo"]);
                    }
                    else
                    {
                        DtpToDate = ClsCommonSettings.GetServerDate();
                    }
                }
                else
                {
                    DtpToDate = ClsCommonSettings.GetServerDate();

                }
                return true;
            }
            catch (Exception)
            {
                DtpToDate = ClsCommonSettings.GetServerDate();
                return true;
            }
        }






        public bool DisplayCurrencyEmployee(int EmpID, ref int miCurrencyId, ref int MiCompanyID, ref string LblCurrency, ref string DtpFromDate, ref int TransactionTypeID)
        {
            try
            {
                string sSqlQuery = "";
                DataTable dt;



                sSqlQuery = "SELECT EM.DateofJoining, cur.Code as ShortDescription, EM.CompanyID, SH.CurrencyId,ISNULL(EM.TransactionTypeID,0) TransactionTypeID" +
                            " FROM   EmployeeMaster AS EM " +
                            " INNER JOIN PaySalaryStructure AS SH ON EM.EmployeeID = SH.EmployeeID " +
                            " INNER JOIN CurrencyReference AS cur ON SH.CurrencyId = cur.CurrencyID" +
                            " WHERE  EM.EmployeeID =" + EmpID + " ";
                dt = MobjDataLayer.ExecuteDataTable(sSqlQuery);

                if (dt.Rows.Count > 0)
                {
                    miCurrencyId = Convert.ToInt32(dt.Rows[0]["CurrencyID"]);
                    MiCompanyID = Convert.ToInt32(dt.Rows[0]["CompanyID"]);
                    LblCurrency = Convert.ToString(dt.Rows[0]["ShortDescription"]);
                    DtpFromDate = Convert.ToString(dt.Rows[0]["DateofJoining"]);
                    TransactionTypeID = Convert.ToInt32(dt.Rows[0]["TransactionTypeID"]);

                }
                return true;
            }
            catch (Exception)
            {
                return true;
            }
        }



        public bool GetSettlementPolicyAndExperience(int EmpID, string DtpFromDate, string DtpToDate, ref string SetPolicyDesc, ref string lblNoofMonths, ref string txtNofDaysExperience, ref string txtLeavePayDays, ref string lblTakenLeavePayDays, ref string lblEligibleLeavePayDays, ref string txtAbsentDays)
        {
            string sSqlQuery = "";
            DataTable dt;


            if (EmpID > -1)
            {
                sSqlQuery = "SELECT isnull(DescriptionPolicy,'') AS Policy FROM EmployeeMaster H INNER JOIN PaySettlementPolicy P " +
                                    "ON H.SettlementPolicyID=P.SetPolicyID WHERE H.EmployeeID=" + EmpID + " ";
                dt = MobjDataLayer.ExecuteDataTable(sSqlQuery);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        SetPolicyDesc = dt.Rows[0]["Policy"].ToString();
                    }
                }

                sSqlQuery = "SELECT (isnull(datediff(day,'" + DtpFromDate + "','" + DtpToDate + "'),0) + 1) / (CAST(365 AS Float)/CAST(12 AS Float)) AS Experience, " +
                 "(isnull(datediff(day,'" + DtpFromDate + "','" + DtpToDate + "'),0)) + 1 AS ExperienceDays";

                dt = MobjDataLayer.ExecuteDataTable(sSqlQuery);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["Experience"].ToString() == "0")
                        {
                            lblNoofMonths = "0";
                        }
                        else
                        {
                            lblNoofMonths = (dt.Rows[0]["Experience"].ToDecimal()).ToString("N2");

                            if (lblNoofMonths.Split('.').Last().ToDecimal() == 0)
                                lblNoofMonths = lblNoofMonths.Split('.').First();
                        }

                        if (dt.Rows[0]["ExperienceDays"].ToString() == "0")
                        {
                            txtNofDaysExperience = "0";
                            txtLeavePayDays = "0";
                        }
                        else
                        {
                            txtNofDaysExperience = dt.Rows[0]["ExperienceDays"].ToString();
                        }
                    }
                }


                sSqlQuery = "SELECT ISNULL(SUM(EligibleLeavePayDays),0) AS EligibleLeavePayDays FROM PayVacationMaster WHERE EmployeeID=" + EmpID + " ";
                
                dt = MobjDataLayer.ExecuteDataTable(sSqlQuery);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["EligibleLeavePayDays"].ToString() == "0")
                            lblTakenLeavePayDays = "0";
                        else
                            lblTakenLeavePayDays = dt.Rows[0]["EligibleLeavePayDays"].ToString();
                    }
                }

                lblEligibleLeavePayDays = GetEligibleLeavePayDays(true, EmpID, DtpToDate);

                if (lblEligibleLeavePayDays == "")
                    lblEligibleLeavePayDays = "0";

                if (lblTakenLeavePayDays == "")
                    lblTakenLeavePayDays = "0";

                if (lblEligibleLeavePayDays != "0" && lblTakenLeavePayDays != "0")
                    txtLeavePayDays = (Convert.ToDouble(lblEligibleLeavePayDays) - Convert.ToDouble(lblTakenLeavePayDays)).ToString();

                sSqlQuery = "Select count(isnull(T.AttendanceID,0)) as  AbsentDays from (Select AttendanceID,AttendenceStatusID,EmployeeID,[Date]  From  " +
                                      " PayAttendanceMaster) " +
                                      " T Where T.AttendenceStatusID = 4 And T.EmployeeID = " + EmpID + " And convert(datetime,convert(char(12),T.Date,106),106) < '" + DtpToDate + "'";

                dt = MobjDataLayer.ExecuteDataTable(sSqlQuery);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["AbsentDays"].ToString() == "0")
                            txtAbsentDays = "0";
                        else
                            txtAbsentDays = dt.Rows[0]["AbsentDays"].ToString();
                    }
                }

              
            }

            return true;
        }

        public string GetEligibleLeavePayDays(bool blnIsLeavePay, int EmpID, string strToDate) // From Vacation Process
        {
            DataTable dtVacation;
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", "11"));
            parameters.Add(new SqlParameter("@EmployeeID", EmpID));
            parameters.Add(new SqlParameter("@CompID", ClsCommonSettings.CompanyID));
            parameters.Add(new SqlParameter("@FromDate1", strToDate));
            parameters.Add(new SqlParameter("@ToDate1", strToDate));
            parameters.Add(new SqlParameter("@VacationID", 0));
            parameters.Add(new SqlParameter("@IsConsiderAbsentDays", 0));
            parameters.Add(new SqlParameter("@IsLeavePay", blnIsLeavePay));

            dtVacation = MobjDataLayer.ExecuteDataTable(strProcedureName, parameters);

            if (dtVacation.Rows.Count > 0)
            {
                if (dtVacation.Rows[0]["EligibleLeavePayDays"] != System.DBNull.Value)
                    return dtVacation.Rows[0]["EligibleLeavePayDays"].ToString();
            }
            return "0";
        }

        public void DisplayInfo()
        {
       try
       {
           DataTable DT;
            ArrayList prmSettlement = new ArrayList();

            prmSettlement.Add(new SqlParameter("@Mode", 1));
            prmSettlement.Add(new SqlParameter("@RowNum", MobjDTOSettlementProcess.RowNum));
            prmSettlement.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            prmSettlement.Add(new SqlParameter("@SearchKey", MobjDTOSettlementProcess.strSearchKey));
            prmSettlement.Add(new SqlParameter("@StatusSettlementDetail", 0));
            prmSettlement.Add(new SqlParameter("@RoleID", ClsCommonSettings.RoleID));
            DT = MobjDataLayer.ExecuteDataTable("spPayEmployeeSettlementTransaction", prmSettlement);

            if (DT.Rows.Count >0)
            {
                MobjDTOSettlementProcess.Closed = Convert.ToBoolean(DT.Rows[0]["Closed"] == null ? "0" : DT.Rows[0]["Closed"]);
                MobjDTOSettlementProcess.SettlementID =Convert.ToInt32(DT.Rows[0]["SettlementID"]==null ?  "0" : DT.Rows[0]["SettlementID"]);

                MobjDTOSettlementProcess.EmployeeID  = Convert.ToInt32(DT.Rows[0]["EmployeeID"]);
                MobjDTOSettlementProcess.SettlementType  = Convert.ToInt32(DT.Rows[0]["SettlementType"]);

                MobjDTOSettlementProcess.FromDate = Convert.ToDateTime(DT.Rows[0]["FromDate"] == null ? System.DateTime.Now.Date : DT.Rows[0]["FromDate"]).ToString("dd-MMM-yyyy");
                MobjDTOSettlementProcess.ToDate = Convert.ToDateTime(DT.Rows[0]["ToDate"] == null ? System.DateTime.Now.Date : DT.Rows[0]["ToDate"]).ToString("dd-MMM-yyyy");
                MobjDTOSettlementProcess.Date = Convert.ToDateTime(DT.Rows[0]["Date"] == null ? System.DateTime.Now.Date : DT.Rows[0]["Date"]).ToString("dd-MMM-yyyy");

                MobjDTOSettlementProcess.DocumentReturn = (DT.Rows[0]["DocumentReturn"] == null || DT.Rows[0]["DocumentReturn"] == System.DBNull.Value ? false : Convert.ToBoolean(DT.Rows[0]["DocumentReturn"]));
                MobjDTOSettlementProcess.Remarks = Convert.ToString (DT.Rows[0]["Remarks"]==null ?  "0" : DT.Rows[0]["Remarks"]);
                MobjDTOSettlementProcess.ExperienceDays = Convert.ToDecimal(DT.Rows[0]["ExperienceDays"] == null ? "0" : DT.Rows[0]["ExperienceDays"]);
                MobjDTOSettlementProcess.LeavePayDays = Convert.ToDecimal(DT.Rows[0]["LeavePayDays"] == null ? "0" : DT.Rows[0]["LeavePayDays"]);
                MobjDTOSettlementProcess.AbsentDays = Convert.ToDecimal(DT.Rows[0]["AbsentDays"].ToDecimal()   ==  0 ? "0" : DT.Rows[0]["AbsentDays"]);


                MobjDTOSettlementProcess.AccountID = Convert.ToInt32(DT.Rows[0]["AccountID"].ToInt32() == 0 ? 0 : DT.Rows[0]["AccountID"]);
                MobjDTOSettlementProcess.TransactionTypeID = Convert.ToInt32(DT.Rows[0]["TransactionTypeID"]);
                MobjDTOSettlementProcess.ChequeNumber = Convert.ToString(DT.Rows[0]["ChequeNumber"].ToString() == "0" ? "" : DT.Rows[0]["ChequeNumber"]);
                MobjDTOSettlementProcess.ChequeDate = Convert.ToDateTime(DT.Rows[0]["ChequeDate"] == null ? System.DateTime.Now.Date : DT.Rows[0]["ChequeDate"]).ToString("dd-MMM-yyyy");
                MobjDTOSettlementProcess.PaymentID = Convert.ToInt32(DT.Rows[0]["PaymentID"].ToInt32() == 0 ? 0 : DT.Rows[0]["PaymentID"]);
            }

            FillDetails(MobjDTOSettlementProcess.SettlementID, MobjDTOSettlementProcess.EmployeeID);

          
       }
        catch (Exception)
            {

            }
        
  }


        public string GetEligibleLeavePayDays(int Empid, string DtpFromDate, string DtpToDate, string txtAbsentDays)
        {
            try
            {

                DataTable dtSettlement;
                ArrayList parameters = new ArrayList();

                parameters.Add(new SqlParameter("@Mode", 9));
                parameters.Add(new SqlParameter("@EmployeeID", Empid));
                parameters.Add(new SqlParameter("@FromDate", DtpToDate));
                parameters.Add(new SqlParameter("@ToDate", DtpToDate));
                parameters.Add(new SqlParameter("@AbsentDays", txtAbsentDays));
                dtSettlement = MobjDataLayer.ExecuteDataTable("spPayEmployeeSettlementTransaction", parameters);
                if (dtSettlement.Rows.Count > 0)
                {
                    if (dtSettlement.Rows[0]["EligibleLeavePayDays"] != null)
                    {
                        return Math.Abs(Convert.ToDouble(dtSettlement.Rows[0]["EligibleLeavePayDays"])).ToString();
                    }
                }
                return "0";
            }
            catch (Exception)
            {
            }
            return "NIL";
        }
        public DataSet EmailDetail(int SettlementID, int EmpID)
        {
            try
            {
                DataSet dtSettlement;
                ArrayList parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 38));
                parameters.Add(new SqlParameter("@EmployeeID", EmpID));
                parameters.Add(new SqlParameter("@SettlementID", SettlementID));
                dtSettlement = MobjDataLayer.ExecuteDataSet("spPayEmployeeSettlementTransaction", parameters);

                return dtSettlement;
            }
            catch (Exception)
            {
                DataSet dtSettlement1=new DataSet() ;
                return dtSettlement1;
            }
        }
        public void FillDetails(int SettlementID,int EmpID)
        {
        try
        {
               ArrayList prmSettlement;
                int i=0;

                //'' For Salary Process Details
                DataSet datTempSet;
      
                MobjDTOSettlementProcess.lstclsDTOSettlementDetails = new System.Collections.Generic.List<clsDTOSettlementDetails>();

                datTempSet=new DataSet(); 
                //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                prmSettlement = new ArrayList();
                prmSettlement.Add(new SqlParameter("@Mode", 1));
                prmSettlement.Add(new SqlParameter("@SettlementID", SettlementID));
                prmSettlement.Add(new SqlParameter("@StatusSettlementDetail", 1));
                prmSettlement.Add(new SqlParameter("@RoleID", ClsCommonSettings.RoleID));
                datTempSet = MobjDataLayer.ExecuteDataSet("spPayEmployeeSettlementTransaction", prmSettlement);

                for (i = 0; datTempSet.Tables[0].Rows.Count - 1 >= i; ++i)
                {
                    clsDTOSettlementDetails objclsDTOSettlementDetails = new clsDTOSettlementDetails();
                    objclsDTOSettlementDetails.EmployeeSettlementDetailID = Convert.ToInt32(datTempSet.Tables[0].Rows[i]["EmployeeSettlementDetailID"]);
                    objclsDTOSettlementDetails.Particulars = Convert.ToString(datTempSet.Tables[0].Rows[i]["Particulars"]);
                    objclsDTOSettlementDetails.IsAddition = Convert.ToBoolean(datTempSet.Tables[0].Rows[i]["IsAddition"]);
                        //objclsDTOSettlementDetails.AmountCredit  = Convert.ToDecimal(datTempPay.Rows[i]["Credit"]);
                       
                        if (Convert.ToDouble(objclsDTOSettlementDetails.IsAddition) > 0)
                        {
                            objclsDTOSettlementDetails.Amount = Convert.ToDecimal(datTempSet.Tables[0].Rows[i]["Credit"]);
                            objclsDTOSettlementDetails.AmountCredit = Convert.ToDecimal(datTempSet.Tables[0].Rows[i]["Credit"]);
                            objclsDTOSettlementDetails.AmountDebit = 0;
                        }
                        else
                        {
                            objclsDTOSettlementDetails.Amount = Convert.ToDecimal(datTempSet.Tables[0].Rows[i]["Debit"]);
                            objclsDTOSettlementDetails.AmountDebit = Convert.ToDecimal(datTempSet.Tables[0].Rows[i]["Debit"]);
                            objclsDTOSettlementDetails.AmountCredit = 0;
                        }
                        objclsDTOSettlementDetails.LoanID = Convert.ToInt32(datTempSet.Tables[0].Rows[i]["LoanID"]);
                        objclsDTOSettlementDetails.flag = 1;
                        objclsDTOSettlementDetails.SalaryPaymentID = Convert.ToInt32(datTempSet.Tables[0].Rows[i]["paymentID"]); ;
                        MobjDTOSettlementProcess.lstclsDTOSettlementDetails.Add(objclsDTOSettlementDetails);
                }

                MobjDTOSettlementProcess.lstclsDTOGratuityDetails = new System.Collections.Generic.List<clsDTOGratuityDetails>();

                for (i = 0; datTempSet.Tables[1].Rows.Count - 1 >= i; ++i)
                {
                    clsDTOGratuityDetails objclsDTOGratuityDetails = new clsDTOGratuityDetails();
                    objclsDTOGratuityDetails.ColYears = Convert.ToString(datTempSet.Tables[1].Rows[i]["Years"]); ;
                    objclsDTOGratuityDetails.ColCalDays = Convert.ToDecimal(datTempSet.Tables[1].Rows[i]["CalculationDays"]); ;
                    objclsDTOGratuityDetails.ColGRTYDays = Convert.ToDecimal(datTempSet.Tables[1].Rows[i]["GratuityDays"]); ;
                    objclsDTOGratuityDetails.ColGratuityAmt = Convert.ToDecimal(datTempSet.Tables[1].Rows[i]["GratuityAmount"]); ;
                    MobjDTOSettlementProcess.lstclsDTOGratuityDetails.Add(objclsDTOGratuityDetails);
                }
        }
        catch(Exception)
        {

        }
}

        public bool IsSettlementAccountExists(int intEmpID)
        {
            int intIsExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 34), new SqlParameter("@EmployeeID", intEmpID) };
            intIsExists = this.DataLayer.ExecuteScalar("spPayEmployeeSettlementTransaction", this.sqlParameters).ToInt32();
            return (intIsExists > 0);
        }
        public int GetEmpCompanyID(int intEmpID)
        {

            int RecordCnt = 0;
            string TSQL;
            DataTable DT;

            TSQL = "Select CompanyID from EmployeeMaster where EmployeeID=" + intEmpID + "";
            DT = MobjDataLayer.ExecuteDataTable(TSQL);
            if (DT.Rows.Count > 0)
            {
                RecordCnt = Convert.ToInt32(DT.Rows[0][0]);
            }
            else
            {
                RecordCnt = 0;
            }
            return RecordCnt;


        }

        public bool DeletePaymentDetails()
        {
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 41));
            alparameters.Add(new SqlParameter("@SettlementID", MobjDTOSettlementProcess.SettlementID));
            MobjDataLayer.ExecuteScalar("spPayEmployeeSettlementTransaction", alparameters);
            return true;
        }

        public DataTable GetEmployeeLeaveHolidays(int EmpID, string strFromDate, string strToDate) // Employee Leave days & Holidays
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 42));
            parameters.Add(new SqlParameter("@EmployeeID", EmpID));
            parameters.Add(new SqlParameter("@FromDate", strFromDate));
            parameters.Add(new SqlParameter("@ToDate", strToDate));
            return MobjDataLayer.ExecuteDataTable("spPayEmployeeSettlementTransaction", parameters);
        }


        public void EmployeeSetAttTransaction(int EmpID, string strToDate) 
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 45));
            parameters.Add(new SqlParameter("@EmployeeID", EmpID));
            parameters.Add(new SqlParameter("@ToDate", strToDate));
            MobjDataLayer.ExecuteDataTable("spPayEmployeeSettlementTransaction", parameters);
        }

    }
}