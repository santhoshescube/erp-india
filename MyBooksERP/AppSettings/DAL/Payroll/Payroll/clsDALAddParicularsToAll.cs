﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyBooksERP
{
    class clsDALAddParicularsToAll
    {
        ClsCommonUtility MobjClsCommonUtility; // object of clsCommonUtility
        DataLayer MobjDataLayer;
        ArrayList prmAddPariculars;

        public clsDTOAddParicularsToAll PobjClsDTOAddParicularsToAll { get; set; }

        public clsDALAddParicularsToAll(DataLayer objDataLayer)
        {
            MobjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MobjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MobjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }
        /// <summary>
        /// To get Employee Payment detail
        /// </summary>
        /// <param name="intAdditionDeductionID"></param>
        /// <param name="strProcessDate"></param>
        /// <param name="intCompanyID"></param>
        /// <param name="intDepartmentID"></param>
        /// <returns>DataTable</returns>
        public DataTable GetEmployeeDetails(int intAdditionDeductionID, string strProcessDate,int intCompanyID, int intDepartmentID)// To get Employee Payment detail
        {
            ArrayList prmAddPariculars = new ArrayList();
            prmAddPariculars.Add(new SqlParameter("@Mode", 3));
            prmAddPariculars.Add(new SqlParameter("@AdditionDeductionID", intAdditionDeductionID));
            prmAddPariculars.Add(new SqlParameter("@ProcessDate", strProcessDate));
            prmAddPariculars.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmAddPariculars.Add(new SqlParameter("@DepartmentID", intDepartmentID));
            return MobjDataLayer.ExecuteDataTable("spPayAddAdditionDeduction", prmAddPariculars);
        }
        /// <summary>
        /// function for saving Employee Payment details
        /// </summary>
        /// <returns>bool</returns>
        public bool SaveEmployeeDetails()
        {
            //function for saving Employee Payment details

            foreach (clsDTOAddParicularsToAllDetail objclsDTOAddParicularsToAll in PobjClsDTOAddParicularsToAll.DTOAddParicularsToAllDetail)
            {
                prmAddPariculars = new ArrayList();

                prmAddPariculars.Add(new SqlParameter("@Mode", 1));
                prmAddPariculars.Add(new SqlParameter("@PaymentID", objclsDTOAddParicularsToAll.PaymentID));
                prmAddPariculars.Add(new SqlParameter("@CurrencyID", objclsDTOAddParicularsToAll.CurrencyID));
                prmAddPariculars.Add(new SqlParameter("@AddDedID", objclsDTOAddParicularsToAll.AddDedID));
                prmAddPariculars.Add(new SqlParameter("@Amount", objclsDTOAddParicularsToAll.Amount));
                prmAddPariculars.Add(new SqlParameter("@IsAddition", objclsDTOAddParicularsToAll.IsAddition));
             
                MobjDataLayer.ExecuteNonQueryWithTran("spPayAddAdditionDeduction", prmAddPariculars);

            }
            return true;
        }
    }
}
