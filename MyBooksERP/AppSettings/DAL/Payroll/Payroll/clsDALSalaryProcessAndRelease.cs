﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualBasic.Devices;

namespace MyBooksERP
{
    public class clsDALSalaryProcessAndRelease
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOSalaryProcessAndRelease objDTOSalaryProcessAndRelease { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;
        string strProcedureName = "spPayNewSalaryProcessAndRelease"; 

        public clsDALSalaryProcessAndRelease(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public DataTable getDetailsInComboBox(int intType, int intCompanyID)
        {
            DataTable datTemp = new DataTable();
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", intType));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            datTemp = MobjDataLayer.ExecuteDataTable(strProcedureName, parameters);
            return datTemp;
        }

        public DataSet GetProcessEmployee(int EmployeeID, int CompanyID, string FromDate, string ToDate, string HostName, int UserId,
            int BranchIndicator, int intTempDept, int intTempDesg, int BranchID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 13));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            parameters.Add(new SqlParameter("@FromDate1", FromDate));
            parameters.Add(new SqlParameter("@ToDate1", ToDate));
            parameters.Add(new SqlParameter("@MachineName", HostName));
            parameters.Add(new SqlParameter("@UserID", UserId));
            parameters.Add(new SqlParameter("@BranchID", BranchID));
            parameters.Add(new SqlParameter("@DepartmentID", intTempDept));
            parameters.Add(new SqlParameter("@DesignationID", intTempDesg));
            parameters.Add(new SqlParameter("@BranchIndicator", BranchIndicator));
            parameters.Add(new SqlParameter("@FilterTransactionTypeID", 0));

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    parameters.Add(new SqlParameter("@FlgArb", 1));
            //}
            //else
            //{
                parameters.Add(new SqlParameter("@FlgArb", 0));
            //}


            return MobjDataLayer.ExecuteDataSet(strProcedureName, parameters);
        }

        public void DeletePayment(string FromDate, string HostName, string ToDate, int CompanyID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@ProcessDate1", FromDate));
            parameters.Add(new SqlParameter("@MachineName", HostName));
            parameters.Add(new SqlParameter("@FromDate", FromDate));
            parameters.Add(new SqlParameter("@ToDate", ToDate));
            parameters.Add(new SqlParameter("@Flag", 1));
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            MobjDataLayer.ExecuteNonQuery("spPayDeletePayment", parameters);
        }

        public bool SalaryProcess(int ParaEmployeeID, int CompanyID, string FromDate, string ToDate, bool IsPartial, int TypeIndex)
        {
            bool blnStatus = false;

            if (ParaEmployeeID > 0)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@FromDate1", FromDate));
                parameters.Add(new SqlParameter("@ToDate1", ToDate));
                parameters.Add(new SqlParameter("@EmployeeID", ParaEmployeeID));
                parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                parameters.Add(new SqlParameter("@SettleFlag", 0));
                if (IsPartial)
                    parameters.Add(new SqlParameter("@IsPartial", 1));
                else
                    parameters.Add(new SqlParameter("@IsPartial", 0));
                parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
                parameters.Add(new SqlParameter("@IsFromTransfer", 0));
                parameters.Add(new SqlParameter("@ProcessTypeID", TypeIndex));
                parameters.Add(new SqlParameter("@IsVacation", 0));
                parameters.Add(new SqlParameter("@TransferFromDate1", "Null"));
                parameters.Add(new SqlParameter("@AccrualSettlement", 0));

                SqlParameter sqlParam1 = new SqlParameter("@VacationPayAmt", SqlDbType.Float);
                sqlParam1.Direction = ParameterDirection.Output;
                parameters.Add(sqlParam1);

                SqlParameter sqlParam2 = new SqlParameter("@PaymentIDRet", SqlDbType.Float);
                sqlParam2.Direction = ParameterDirection.Output;
                parameters.Add(sqlParam2);


                MobjDataLayer.ExecuteNonQuery("spPaySalaryProcess", parameters);
                blnStatus = true;
            }
            return blnStatus;
        }

        public DataTable getTemplate()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 15));
            return MobjDataLayer.ExecuteDataTable(strProcedureName, parameters);
        }

        public DataTable CheckPaymentIsPartial(long iPaymentID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 16));
            parameters.Add(new SqlParameter("@PaymentID", iPaymentID));
            return MobjDataLayer.ExecuteDataTable(strProcedureName, parameters);
        }

        public DataSet FillSalaryInfogrid(string ProcessDate, int TemplateID, int BankID, int TransactionTypeID, int ProcessYear, int CompanyID, int CurrencyID,int DesignationID,int DepartmentID)
        {
            parameters = new ArrayList();
            DataSet datTemp = new DataSet();

            parameters.Add(new SqlParameter("@Mode", 17));
            parameters.Add(new SqlParameter("@FilterProcessDate", ProcessDate));
            parameters.Add(new SqlParameter("@FilterTemplateID", TemplateID));
            parameters.Add(new SqlParameter("@FilterBankID", BankID));
            parameters.Add(new SqlParameter("@FilterTransactionTypeID", TransactionTypeID));
            parameters.Add(new SqlParameter("@FilterProcessYear", ProcessYear));
            parameters.Add(new SqlParameter("@FilterCompanyID", CompanyID));
            parameters.Add(new SqlParameter("@FiterCurrencyID", CurrencyID));
            parameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
            parameters.Add(new SqlParameter("@DesignationID", DesignationID));
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    parameters.Add(new SqlParameter("@FlgArb", 1));
            //}
            //else
            //{
                parameters.Add(new SqlParameter("@FlgArb", 0));
            //}
            datTemp = MobjDataLayer.ExecuteDataSet(strProcedureName, parameters);

            return datTemp;
        }

        public bool DeleteTempEmployeeIDForPaymentRelease(string MachineName)
        {            
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 18));
            parameters.Add(new SqlParameter("@MachineName", MachineName));
            return (MobjDataLayer.ExecuteNonQuery(strProcedureName, parameters)).ToBoolean();
        }

        public bool InsertTempEmployeeIDForPaymentRelease(int EmployeeID, string ProcessDate, string MachineName, long PaymentID)
        {   
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 19));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@ProcessDate", ProcessDate));
            parameters.Add(new SqlParameter("@MachineName", MachineName));
            parameters.Add(new SqlParameter("@PaymentID", PaymentID));
            return (MobjDataLayer.ExecuteNonQuery(strProcedureName, parameters)).ToBoolean();
        }

        public DataTable checkPaymentIDUse(string MachineName)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 20));
            parameters.Add(new SqlParameter("@MachineName", MachineName));
            return MobjDataLayer.ExecuteDataTable(strProcedureName, parameters);
        }

        public int DeletePayment(string FillDate, string MachineName, int intCompanyID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 21));
            parameters.Add(new SqlParameter("@FromDate1", FillDate));
            parameters.Add(new SqlParameter("@MachineName", MachineName));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            return MobjDataLayer.ExecuteScalar(strProcedureName, parameters).ToInt32();
        }

        public bool InsertTempEmployeeIDForPaymentRelease(string MachineName, long PaymentID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 22));
            parameters.Add(new SqlParameter("@MachineName", MachineName));
            parameters.Add(new SqlParameter("@PaymentID", PaymentID));
            return (MobjDataLayer.ExecuteNonQuery(strProcedureName, parameters)).ToBoolean();
        }


        public DataTable getEmployeePaymentDetail(int intTemplateID, int ProcessYear)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 23));
            parameters.Add(new SqlParameter("@TemplateID", intTemplateID));
            parameters.Add(new SqlParameter("@FilterProcessYear", ProcessYear));
            parameters.Add(new SqlParameter("@FlgArb", 0));
            return MobjDataLayer.ExecuteDataTable(strProcedureName, parameters);
        }

        public bool DeleteSingleRow(double PaymentID)
        {

            String sQuery = "Select 1  from PayEmployeePayment where isnull(IsVacationSettle,0)=0 and PaymentID= " + PaymentID + " and isnull(Released,0) = 0";
            DataTable DA = MobjDataLayer.ExecuteDataTable(sQuery) ;
            if (DA.Rows.Count > 0)
            {
                 sQuery = "Delete from PayEmployeePaymentDetail where PaymentID in(select PaymentID from PayEmployeePayment where isnull(IsVacationSettle,0)=0 and PaymentID= " + PaymentID + " and isnull(Released,0) = 0)";
                MobjDataLayer.ExecuteQuery(sQuery);
                sQuery = "Delete from PayEmployeePaymentDeductionDtls where PaymentID in(select PaymentID from PayEmployeePayment where isnull(IsVacationSettle,0)=0 and PaymentID= " + PaymentID + " and isnull(Released,0) = 0)";
                MobjDataLayer.ExecuteQuery(sQuery);
                sQuery = "Delete from PayEmployeePaymentEncash where PaymentID in(select PaymentID from PayEmployeePayment where isnull(IsVacationSettle,0)=0 and PaymentID= " + PaymentID + " and isnull(Released,0) = 0)";
                MobjDataLayer.ExecuteQuery(sQuery);
                //sQuery = "Delete from PayProjectPaymentDetails where PaymentID in(select PaymentID from PayEmployeePayment where isnull(IsVacationSettle,0)=0 and PaymentID= " + PaymentID + " and isnull(Released,0) = 0)";
                //MobjDataLayer.ExecuteQuery(sQuery);
                sQuery = "Delete from PaySlipWorkInfo where PaymentID in(select PaymentID from PayEmployeePayment where isnull(IsVacationSettle,0)=0 and PaymentID= " + PaymentID + " and isnull(Released,0) = 0)";
                MobjDataLayer.ExecuteQuery(sQuery);
                sQuery = "Delete  from PayEmployeePayment where isnull(IsVacationSettle,0)=0 and PaymentID= " + PaymentID + " and isnull(Released,0) = 0";
                MobjDataLayer.ExecuteQuery(sQuery);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void InsertIntoFileTables(string strDate, string strpath, int CompanyID, int CompanyBankAccountID,int IsVisa)
        {
            DataTable DA;
            string GetHostName = System.Net.Dns.GetHostName();
            DA = MobjDataLayer.ExecuteDataTable("exec spPayInsertIntoFileTables  '" + GetHostName + "','" + strDate + "'," + CompanyID.ToString() + "," + CompanyBankAccountID.ToString() + "," + IsVisa.ToString());

            for (int i = 0; DA.Rows.Count - 1 >= i; ++i)
            {
                this.ExportToCSV(Convert.ToString(DA.Rows[i]["SIFName"]), Convert.ToUInt32(DA.Rows[i]["SIFID"]), strpath);
            }
        }

        public void ExportToCSV(string csvName, Int64 sifID, string strpath)
        {
            try
            {

                int RowCnt = 0;
                string csvFile = strpath + "\\" + csvName;
                System.IO.StreamWriter outFile;
                Computer myC = new Computer();
                outFile = myC.FileSystem.OpenTextFileWriter(csvFile, false);

                string TSQL = "";

                DataTable dt ;
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 27));
                parameters.Add(new SqlParameter("@SIFID", sifID));
                dt = MobjDataLayer.ExecuteDataTable(strProcedureName, parameters);


                if (dt.Rows.Count > 0)
                {

                   
                    for (int i = 0; dt.Rows.Count - 1 >= i; i++)
                    {
                        string strEDRRow = "";
                        for (int j = 0; dt.Columns.Count > j; ++j)
                        {
                            if (j == Convert.ToInt32(dt.Columns.Count) - 1)
                            {
                                strEDRRow += Convert.ToString(dt.Rows[i][j]);
                            }
                            else
                            {
                                strEDRRow += Convert.ToString(dt.Rows[i][j]) + ",";
                            }
                        }
                        outFile.WriteLine(strEDRRow);
                    }

                    RowCnt = Convert.ToInt32(dt.Rows.Count.ToInt32()); 
                }


                TSQL = "SELECT cast(isnull(RecordType,'')  as varchar(50)) RecordType ," +
                " cast(isnull(EPersonID,0) as varchar(50)) EPersonID,cast(isnull(BankCode,0)  as varchar(50)) BankCode, " +
                " cast(isnull(FCreateDate,0)  as varchar(50)) FCreateDate ,cast(isnull(FCreateTime,0)  as varchar(50))  " +
                " FCreateTime,cast(isnull(SalaryMonth,0)  as varchar(50)) SalaryMonth ,cast(isnull('" + RowCnt.ToString()  +" ',0)  as varchar(50)) " +
                " EDRCount,cast(isnull(TotalSalary,0)  as varchar(50)) TotalSalary ,cast(isnull(PaymentCurrency,0)  as varchar(50)) " +
                " PaymentCurrency ,cast(isnull(EmployerReference,0)  as varchar(50)) EmployerReference  FROM SalaryControlRecord " +
                " WHERE SIFID=" + sifID;


                DataTable dtt = MobjDataLayer.ExecuteDataTable(TSQL);

                if (dtt.Rows.Count > 0)
                {
                    for (int i = 0; dtt.Rows.Count - 1 >= i; i++)
                    {
                        string strEDRRow = "";
                        for (int j = 0; dtt.Columns.Count > j; ++j)
                        {
                            if (j == Convert.ToInt32(dtt.Columns.Count) - 1)
                            {
                                strEDRRow += Convert.ToString(dtt.Rows[i][j]);
                            }
                            else
                            {
                                strEDRRow += Convert.ToString(dtt.Rows[i][j]) + ",";
                            }
                        }
                        outFile.WriteLine(strEDRRow);
                    }
                }

                outFile.Close();

            }
            catch (Exception ex)
            {
            }
        }
    }
}
