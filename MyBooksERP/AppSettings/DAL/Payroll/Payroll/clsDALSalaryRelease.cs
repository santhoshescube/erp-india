﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;


namespace MyBooksERP
{
    public class clsDALSalaryRelease
    {

      ArrayList prmRelease; // for setting Sql parameters
      ClsCommonUtility MObjClsCommonUtility;
      DataLayer MobjDataLayer;

      public clsDTOSalaryRelease PobjclsDTOSalaryRelease { get; set; } // DTO Salary Process Property

      public clsDALSalaryRelease(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }


      public DataTable FillCombos(string[] saFieldValues)
      {
          //function for getting datatable for filling combo
          return MObjClsCommonUtility.FillCombos(saFieldValues);
      }

      public DataTable FillCombos(string sQuery)
      {
          //function for getting datatable for filling combo
          return MObjClsCommonUtility.FillCombos(sQuery);
      }

      public DataTable DisplayProcessYear()
      {
          //function for Display Process Year details
          prmRelease = new ArrayList();
          prmRelease.Add(new SqlParameter("@Mode", "2"));
          prmRelease.Add(new SqlParameter("@ProcessYear", PobjclsDTOSalaryRelease.intProcessYear));
          prmRelease.Add(new SqlParameter("@TemplateID", PobjclsDTOSalaryRelease.intTemplateID));
          return MobjDataLayer.ExecuteDataSet("spPayEmployeePaymentDetail", prmRelease).Tables[0];
      }


      public DataSet GetPaymentDetail()
      {
          //function for Payment Detail

          prmRelease = new ArrayList();
          prmRelease.Add(new SqlParameter("@Mode", "1"));
          prmRelease.Add(new SqlParameter("@ProcessDate1", PobjclsDTOSalaryRelease.strProcessDate));
          prmRelease.Add(new SqlParameter("@RepTableId", PobjclsDTOSalaryRelease.intTemplateID));
          prmRelease.Add(new SqlParameter("@BankNameID", PobjclsDTOSalaryRelease.intBankNameID));
          prmRelease.Add(new SqlParameter("@TransactionTypeID", PobjclsDTOSalaryRelease.intTransactionTypeID));
          prmRelease.Add(new SqlParameter("@YearInt", PobjclsDTOSalaryRelease.intYearInt));
          prmRelease.Add(new SqlParameter("@CompanyID", PobjclsDTOSalaryRelease.intCompanyID));
          prmRelease.Add(new SqlParameter("@DepartmentID", PobjclsDTOSalaryRelease.intDepartmentID));
          prmRelease.Add(new SqlParameter("@ParaCurrencyID", PobjclsDTOSalaryRelease.intCurrencyID));
          return MobjDataLayer.ExecuteDataSet("spPayEmployeePaymentDetail", prmRelease);
      }
      public DataTable  PayDeletePayment()
      {
          //function for Payment Delete

          prmRelease = new ArrayList();
          prmRelease.Add(new SqlParameter("@ProcessDate1", PobjclsDTOSalaryRelease.strProcessDate));
          prmRelease.Add(new SqlParameter("@MachineName", PobjclsDTOSalaryRelease.strHostName));
          prmRelease.Add(new SqlParameter("@FromDate", PobjclsDTOSalaryRelease.strFromDate1));
          prmRelease.Add(new SqlParameter("@ToDate", PobjclsDTOSalaryRelease.strToDate1));
          prmRelease.Add(new SqlParameter("@Flag", 0));
          prmRelease.Add(new SqlParameter("@CompanyID", PobjclsDTOSalaryRelease.intCompanyID));
          return MobjDataLayer.ExecuteDataTable("spPayDeletePayment", prmRelease);

      }
      public DataTable GetPaymentClassificationCalculationType()
      {
          string sQuery;
          sQuery = "select isnull(PaymentClassificationID,0) as PaymentClassificationID,isnull(PayCalculationTypeID,0)  as PayCalculationTypeID from PayEmployeePayment where PaymentID=" + Convert.ToInt32(PobjclsDTOSalaryRelease.intPaymentID) + "";
          return  MobjDataLayer.ExecuteDataTable(sQuery);
      }

      public int GetCompanyID()
      {
          string sQuery;
          DataTable DT;
          sQuery = "select CompanyID  from PayEmployeePayment where PaymentID=" + Convert.ToInt32(PobjclsDTOSalaryRelease.intPaymentID) + "";
          DT= MobjDataLayer.ExecuteDataTable(sQuery);


          if (DT.Rows.Count > 0)
          {
              return Convert.ToInt32(DT.Rows[0]["CompanyID"]);
          }
          else
          {
              return 0;
          }

          return 0;

      }
      public int GetCurrencyScale()
      {
          string sQuery;
          DataTable DT;
          sQuery = "SELECT  Y.CurrencyID, Y.CurrencyName, isnull(Y.Scale,2) as Scale FROM  CurrencyReference AS Y INNER JOIN CompanyMaster AS C ON C.CurrencyId = Y.CurrencyID WHERE C.CompanyID = " + PobjclsDTOSalaryRelease.intCompanyID + "";


          DT=MobjDataLayer.ExecuteDataTable(sQuery);

         if (DT.Rows.Count >0)
         {
             return Convert.ToInt32(DT.Rows[0]["Scale"]);
         }
         else
         {
             return 0;
         }

         return 0;
      }
      public bool DeleteSingleRow(double PaymentID)
      {
        String sQuery = "Delete from PayEmployeePaymentDetail where PaymentID in(select PaymentID from PayEmployeePayment where PaymentID= " + PaymentID + " and isnull(Released,0) = 0)";
        MobjDataLayer.ExecuteQuery(sQuery);
        sQuery = "Delete from PaySlipWorkInfo where PaymentID in(select PaymentID from PayEmployeePayment where PaymentID= " + PaymentID + " and isnull(Released,0) = 0)";
        MobjDataLayer.ExecuteQuery(sQuery);
        sQuery = "Delete  from PayEmployeePayment where PaymentID= " + PaymentID + " and isnull(Released,0) = 0";
        MobjDataLayer.ExecuteQuery(sQuery);
        return true;
      }
        public bool CheckReleased()
        {
            DataTable  DT;
            String sQuery = "Select  PaymentID from PayEmployeePayment where PaymentID= isnull(" + PobjclsDTOSalaryRelease.intPaymentID + ",0) and isnull(Released,0) = 1";
            DT=MobjDataLayer.ExecuteDataTable(sQuery);
            if (DT.Rows.Count >0) 
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}
