﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    class clsDALEmployeeLeaveStructure
    {
        private clsConnection objConnection;
        private clsDTOEmployeeLeaveStructure objDTOEmployeeLeaveStructure;
        private string strProcedureName = "spPayEmployeeLeave";
        DataLayer objDataLayer;
        ArrayList alparameters;


        public clsDALEmployeeLeaveStructure()
        {
            //Constructor
            objDTOEmployeeLeaveStructure = new clsDTOEmployeeLeaveStructure();
            //objBLLEmployeeLeaveStructure = new clsBLLEmployeeLeaveStructure();
            objConnection = new clsConnection();
            objDataLayer = new DataLayer();
        }

        public DataTable GetEmployeeWorkStatusID(int intEmployeeID)
        {
            alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 25));
            alparameters.Add(new SqlParameter("@EmployeeID", objDTOEmployeeLeaveStructure.intEmployeeID));
            return objDataLayer.ExecuteDataTable(strProcedureName, alparameters);
        }

        //public DataTable GetLeaveTypeIDByEmployeeID(int intEmployeeID)
        //{
        //    alparameters = new ArrayList();
        //    alparameters.Add(new SqlParameter("@Mode", 26));
        //    alparameters.Add(new SqlParameter("@EmployeeID", objDTOEmployeeLeaveStructure.intEmployeeID));
        //    return objDataLayer.ExecuteDataTable(strProcedureName, alparameters);
        //}

        public bool LeaveSummaryFromLeaveEntry(string[] str,int intEmployeeID,int intCompanyID)
        {
            alparameters = new ArrayList();
            DataTable dt = new DataTable();
            alparameters.Add(new SqlParameter("@Mode", 26));
            alparameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            dt = objDataLayer.ExecuteDataTable(strProcedureName, alparameters);//Get leave details of corresponding employee leave policy
            int iCounter = 0;
            int jCounter = 0;
            if (dt.Rows.Count > 0)
            {
                for (int ICounter = 0; ICounter <= dt.Rows.Count - 1; ICounter++)
                {
                    iCounter++;
                    ArrayList alparameters1 = new ArrayList();
                    ArrayList alparameters2 = new ArrayList();
                    alparameters1.Add(new SqlParameter("@CompanyID", intCompanyID));
                    alparameters1.Add(new SqlParameter("@EmployeeID", intEmployeeID));
                    alparameters1.Add(new SqlParameter("@LeavetypeID", Convert.ToInt32(dt.Rows[ICounter]["leaveTypeID"])));
                    alparameters1.Add(new SqlParameter("@Leavepolicyid", Convert.ToInt32(dt.Rows[ICounter]["LeavePolicyID"])));
                    alparameters1.Add(new SqlParameter("@Fromdate", str[0]));
                    alparameters1.Add(new SqlParameter("@Todate", str[1]));
                    alparameters1.Add(new SqlParameter("@Type", 0));

                    if(objDataLayer.ExecuteDataSet("spPayEmployeeLeaveSummaryFromLeaveEntry", alparameters1) !=null)
                        jCounter++;
                    
                }

                if ((iCounter == jCounter) && iCounter != 0)
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public bool SaveLeaveStructure(List<clsDTOEmployeeLeaveStructure> lstDTOEmployeeLeaveStructure)
        {
            int iCounter = 0;
            int jCounter = 0;
            foreach (clsDTOEmployeeLeaveStructure objjDTOEmployeeLeaveStructure in lstDTOEmployeeLeaveStructure)
            {
                iCounter++;
                alparameters = new ArrayList();
                alparameters.Add(new SqlParameter("@Mode", 12));
                alparameters.Add(new SqlParameter("@EmployeeID", objjDTOEmployeeLeaveStructure.intEmployeeID));
                alparameters.Add(new SqlParameter("@FinYearStartDate",objjDTOEmployeeLeaveStructure.strFinYearStartDate));
                alparameters.Add(new SqlParameter("@LeaveTypeID",objjDTOEmployeeLeaveStructure.intLeaveTypeID));
                alparameters.Add(new SqlParameter("@BalanceLeaves",objjDTOEmployeeLeaveStructure.BalanceLeaves));
                alparameters.Add(new SqlParameter("@CompanyID",objjDTOEmployeeLeaveStructure.intCompanyID));
                alparameters.Add(new SqlParameter("@LeavePolicyID",objjDTOEmployeeLeaveStructure.intLeavePolicyID));
                alparameters.Add(new SqlParameter("@CarryForwardDays",objjDTOEmployeeLeaveStructure.CarryForwardDays));
                alparameters.Add(new SqlParameter("@TakenLeaves",objjDTOEmployeeLeaveStructure.TakenLeaves));
                alparameters.Add(new SqlParameter("@Exceeded",objjDTOEmployeeLeaveStructure.Exceeded));
                alparameters.Add(new SqlParameter("@MonthLeaves",objjDTOEmployeeLeaveStructure.MonthLeaves));
                alparameters.Add(new SqlParameter("@EncashLeaves",objjDTOEmployeeLeaveStructure.EncashLeaves));

                if (objConnection.ExecutesQuery(alparameters, strProcedureName))
                    jCounter++;
            }
            if (iCounter == jCounter && iCounter != 0)
            {
                return true;
            }
            else
                return false;
            /*
              For intICounter = 0 To DgvEmployeeLeaveStructure.Rows.Count - 1
                parameters = New ArrayList
                parameters.Add(New SqlParameter("@Mode", 12))
                parameters.Add(New SqlParameter("@EmployeeID", Convert.ToInt32(cboEmployee.SelectedValue)))
                parameters.Add(New SqlParameter("@FinYearStartDate", str(0)))
                parameters.Add(New SqlParameter("@LeaveTypeID", Convert.ToInt32(DgvEmployeeLeaveStructure.Rows(intICounter).Cells("leaveTypeID").Value)))
                parameters.Add(New SqlParameter("@BalanceLeaves", Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows(intICounter).Cells("BalanceLeaves").Value)))
                parameters.Add(New SqlParameter("@CompanyID", Convert.ToInt32(cboCompany.SelectedValue)))
                parameters.Add(New SqlParameter("@LeavePolicyID", Convert.ToInt32(DgvEmployeeLeaveStructure.Rows(intICounter).Cells("LeavePolicyID").Value)))
                parameters.Add(New SqlParameter("@CarryForwardDays", Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows(intICounter).Cells("CarryForwardDays").Value)))
                parameters.Add(New SqlParameter("@TakenLeaves", Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows(intICounter).Cells("TakenLeaves").Value)))
                parameters.Add(New SqlParameter("@Exceeded", Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows(intICounter).Cells("Extended").Value)))
                parameters.Add(New SqlParameter("@MonthLeaves", Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows(intICounter).Cells("MonthLeave").Value)))
                parameters.Add(New SqlParameter("@EncashLeaves", Convert.ToDecimal(DgvEmployeeLeaveStructure.Rows(intICounter).Cells("EncashLeaves").Value)))

                objCon.ExecutesQuery(parameters, "PayEmployeeLeave")
             */

        }

        public DataTable GetDisplayData(string date1, int intEmployeeID, int intCompanyID, out int intLeavePolicyID)
        {

            alparameters = new ArrayList();
            DataTable dt = new DataTable();
            DataTable dtLeavePolicyID = new DataTable();
            alparameters.Add(new SqlParameter("@Mode", 29));
            alparameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            alparameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            alparameters.Add(new SqlParameter("@Date1", Convert.ToDateTime(date1)));
            alparameters.Add(new SqlParameter("@IsArabicView", 0));
            dt = objDataLayer.ExecuteDataTable(strProcedureName, alparameters);
            if (dt.Rows.Count == 0)
            {
                ArrayList alparameters1 = new ArrayList();
                alparameters1.Add(new SqlParameter("@Mode", 30));
                alparameters1.Add(new SqlParameter("@EmployeeID", intEmployeeID));
                alparameters1.Add(new SqlParameter("@CompanyID", intCompanyID));
                alparameters1.Add(new SqlParameter("@Date1", Convert.ToDateTime(date1)));
                alparameters1.Add(new SqlParameter("@IsArabicView", 0));
                dt = objDataLayer.ExecuteDataTable(strProcedureName, alparameters1);
            }


            if (dt.Rows.Count > 0)
            {
                dtLeavePolicyID = objConnection.FillDataSet("select LeavePolicyID from Employeemaster where employeeID =" + intEmployeeID).Tables[0];
            }
            if (dtLeavePolicyID.Rows.Count > 0)
            {
                intLeavePolicyID = dtLeavePolicyID.Rows[0][0].ToInt32();
            }
            else
                intLeavePolicyID = 0;

            return dt;

            //if (dtLeavePolicyID.Rows.Count > 0)
            //{
            //    for (int iCounter = 0; iCounter <= dt.Rows.Count - 1; iCounter++)
            //    {
            //        objDTOEmployeeLeaveStructure.intLeavePolicyID = Convert.ToInt32(dt.Rows[iCounter]["LeavePolicyID"]);
            //        objDTOEmployeeLeaveStructure.intLeaveTypeID = Convert.ToInt32(dt.Rows[iCounter]["LeaveType"]);
            //        objDTOEmployeeLeaveStructure.BalanceLeaves = Convert.ToDecimal(dt.Rows[iCounter]["BalanceLeaves"]);
            //        objDTOEmployeeLeaveStructure.CarryForwardDays = Convert.ToDecimal(dt.Rows[iCounter]["CarryForwardDays"]);
            //        objDTOEmployeeLeaveStructure.TakenLeaves = Convert.ToDecimal(dt.Rows[iCounter]["TakenLeaves"]);
            //        decTakenLeaves = (Convert.ToDecimal(dt.Rows[iCounter]["BalanceLeaves"]) + Convert.ToDecimal(dt.Rows[iCounter]["CarryForwardDays"]) + Convert.ToDecimal(dt.Rows[iCounter]["Exceeded"])) - Convert.ToDecimal(dt.Rows[iCounter]["TakenLeaves"]);
            //        if (decTakenLeaves < 0)
            //            decTakenLeaves = 0;
            //        objDTOEmployeeLeaveStructure.Exceeded = Convert.ToDecimal(dt.Rows[iCounter]["Exceeded"]);
            //        objDTOEmployeeLeaveStructure.BalanceLeaves = decTakenLeaves;
            //        objDTOEmployeeLeaveStructure.TakenLeaves = Convert.ToDecimal(dt.Rows[iCounter]["TakenLeaves"]);
            //        objDTOEmployeeLeaveStructure.MonthLeaves = Convert.ToDecimal(dt.Rows[iCounter]["MonthLeaves"]);
            //        objDTOEmployeeLeaveStructure.EncashLeaves = Convert.ToDecimal(dt.Rows[iCounter]["EncashLeaves"]);
            //    }
            //    return true;
            //}
            //else
            //    return false;

        }

        public DataSet GetLeaveStructureEmail(int intEmployeeID, int intCompanyID, string date1,string CurrentFinYear)
        {
            alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 37));
            alparameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            alparameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            alparameters.Add(new SqlParameter("@Date1", Convert.ToDateTime(date1)));
            alparameters.Add(new SqlParameter("@CurrentFinYear", CurrentFinYear));
            return objDataLayer.ExecuteDataSet(strProcedureName, alparameters);
        }
    }
}
