﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using DTO;

namespace MyBooksERP
{
    public class clsDALSettlementPolicy
    {

         ClsCommonUtility MObjClsCommonUtility;

        private clsDTOSettlementPolicy MobjDTOSettlementPolicy = null;
        private string strProcedureName = "spPaySettlementPolicy";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALSettlementPolicy(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
        public clsDTOSettlementPolicy DTOSettlementPolicy
        {
            get
            {
                if (this.MobjDTOSettlementPolicy == null)
                    this.MobjDTOSettlementPolicy = new clsDTOSettlementPolicy();

                return this.MobjDTOSettlementPolicy;
            }
            set
            {
                this.MobjDTOSettlementPolicy = value;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MObjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }

   

        public int RecCountNavigate()
        {
            int RecordCnt = 0;
            string TSQL;
            DataTable    DS;
            TSQL = "Select isnull(count(1),0) from PaySettlementPolicy";
            DS = MobjDataLayer.ExecuteDataTable(TSQL); 
            if (DS.Rows.Count>0  )
            {
                RecordCnt = Convert.ToInt32(DS.Rows[0][0]);
               
            }
            else
            {
                RecordCnt = 0;
            }

            return RecordCnt;
       }
        public void DisplayDetails(int RowNum)
        
       {
   
            DataSet DtSet;
            ArrayList parameters ;

            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 3)) ; 
            parameters.Add(new SqlParameter("@SetPolicyID", 0));
            parameters.Add(new SqlParameter("@RowNum", RowNum));
            DtSet = MobjDataLayer.ExecuteDataSet(strProcedureName, parameters);

            if (DtSet.Tables[0].Rows.Count > 0)
            {
                this.MobjDTOSettlementPolicy.SetPolicyID =Convert.ToInt32(DtSet.Tables[0].Rows[0]["SetPolicyID"]);
                this.MobjDTOSettlementPolicy.DescriptionPolicy  =Convert.ToString( DtSet.Tables[0].Rows[0]["DescriptionPolicy"]);
                this.MobjDTOSettlementPolicy.IsRateOnly = Convert.ToInt32(DtSet.Tables[0].Rows[0]["IsRateOnlyG"]);
                this.MobjDTOSettlementPolicy.blnIsIncludeLeave = Convert.ToBoolean(DtSet.Tables[0].Rows[0]["IsIncludeLeave"]);
                this.MobjDTOSettlementPolicy.RatePerDay = Convert.ToDouble(DtSet.Tables[0].Rows[0]["RatePerDayG"]);
                this.MobjDTOSettlementPolicy.CalculationID = Convert.ToInt32(DtSet.Tables[0].Rows[0]["CalculationIDG"]);
            }



                MobjDTOSettlementPolicy.lstclsDTOSettlementPolicyDetail = new System.Collections.Generic.List<DTO.clsDTOSettlementPolicyDetail>();
                MobjDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFive = new System.Collections.Generic.List<DTO.clsDTOSettlementPolicyDetailFive>();
                DataSet DtSetPol;

                DataTable DtablePolicy;
                DataTable DtablePolicyFive;
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 5));
                parameters.Add(new SqlParameter("@SetPolicyID", this.MobjDTOSettlementPolicy.SetPolicyID));
                DtSetPol = MobjDataLayer.ExecuteDataSet(strProcedureName, parameters);

                DtablePolicy = DtSetPol.Tables[0];
                DtablePolicyFive = DtSetPol.Tables[1]; 

                if (DtablePolicy.Rows.Count > 0)
                {
                   for (int i = 0; DtablePolicy.Rows.Count - 1 >= i; ++i)
                   {
                       clsDTOSettlementPolicyDetail objclsDTOSettlementPolicyDetail = new clsDTOSettlementPolicyDetail();
                       objclsDTOSettlementPolicyDetail.ParameterID = Convert.ToInt32(DtablePolicy.Rows[i]["ParameterID"]);
                       objclsDTOSettlementPolicyDetail.NoOfMonths = Convert.ToDecimal(DtablePolicy.Rows[i]["NoOfMonths"]);
                       objclsDTOSettlementPolicyDetail.NoOfDays = Convert.ToDecimal(DtablePolicy.Rows[i]["NoOfDays"]);
                       objclsDTOSettlementPolicyDetail.decTerminationDays = Convert.ToDecimal(DtablePolicy.Rows[i]["TerminationDays"].ToDecimal());
                       objclsDTOSettlementPolicyDetail.DtlsID = Convert.ToInt32(DtablePolicy.Rows[i]["DtlsID"]);
                       MobjDTOSettlementPolicy.lstclsDTOSettlementPolicyDetail.Add(objclsDTOSettlementPolicyDetail);
                   }
                }

                if (DtablePolicyFive.Rows.Count > 0)
                {
                    for (int i = 0; DtablePolicyFive.Rows.Count - 1 >= i; ++i)
                    {
                        clsDTOSettlementPolicyDetailFive objclsDTOSettlementPolicyDetailFive = new clsDTOSettlementPolicyDetailFive();
                        objclsDTOSettlementPolicyDetailFive.ParameterID = Convert.ToInt32(DtablePolicyFive.Rows[i]["ParameterID"]);
                        objclsDTOSettlementPolicyDetailFive.NoOfMonths = Convert.ToDecimal(DtablePolicyFive.Rows[i]["NoOfMonths"]);
                        objclsDTOSettlementPolicyDetailFive.NoOfDays = Convert.ToDecimal(DtablePolicyFive.Rows[i]["NoOfDays"]);
                        objclsDTOSettlementPolicyDetailFive.decTerminationDays = Convert.ToDecimal(DtablePolicyFive.Rows[i]["TerminationDays"]);
                        objclsDTOSettlementPolicyDetailFive.DtlsID = Convert.ToInt32(DtablePolicyFive.Rows[i]["DtlsID"]);
                        MobjDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFive.Add(objclsDTOSettlementPolicyDetailFive);
                    }
                }
                DisplayExcludeInclude();

       }



            public DataTable  FillAdddetailAddMode() 
            {
                //try
                //{
                string  TSQL = "";
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    TSQL = "SELECT A.AdditionDeductionID AddDedID, A. AdditionDeductionArb  as DescriptionStr,0 as sel " +
                //         " FROM PayAdditionDeductionReference AS A  WHERE(A.IsAddition = 1 And isnull(IsPredefined,0)=0) " +
                //         " UNION SELECT A.AdditionDeductionID AddDedID,  A. AdditionDeductionArb  as DescriptionStr,0 as sel  " +
                //         " FROM PayAdditionDeductionReference AS A WHERE AdditionDeductionID=1";
                //}
                //else
                //{
                    TSQL = "SELECT A.AdditionDeductionID AddDedID, A. AdditionDeduction  as DescriptionStr,0 as sel " +
                     " FROM PayAdditionDeductionReference AS A  WHERE(A.IsAddition = 1 And isnull(IsPredefined,0)=0) " +
                     " UNION SELECT A.AdditionDeductionID AddDedID,  A. AdditionDeduction  as DescriptionStr,0 as sel  " +
                     " FROM PayAdditionDeductionReference AS A WHERE AdditionDeductionID=1";
                //}
                return MobjDataLayer.ExecuteDataTable(TSQL);
                //}
                //catch(Exception ex)
                //{

                //}
        
            }


      public void DisplayExcludeInclude()
      {
        try
        {
            DataTable DtablePolicy;
            ArrayList parameters;
            //'' For Grad
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@SetPolicyID", Convert.ToInt32(this.MobjDTOSettlementPolicy.SetPolicyID)));
            parameters.Add(new SqlParameter("@PolicyType", 7));
            parameters.Add(new SqlParameter("@IsArabicView", 0));
            DtablePolicy = MobjDataLayer.ExecuteDataTable(strProcedureName, parameters);

            MobjDTOSettlementPolicy.lstclsDTOInExGra = new System.Collections.Generic.List<DTO.clsDTOInExGra>();

            if ((DtablePolicy.Rows.Count > 0))
            {
                for (int i = 0; DtablePolicy.Rows.Count - 1 >= i; ++i)
                {
                    clsDTOInExGra objclsInExGra = new clsDTOInExGra();
                    objclsInExGra.AdditionDeductionID = Convert.ToInt32(DtablePolicy.Rows[i]["AdditionDeductionID"]);
                    objclsInExGra.DescriptionStr = Convert.ToString(DtablePolicy.Rows[i]["DescriptionStr"]);
                    objclsInExGra.Sel = Convert.ToBoolean(DtablePolicy.Rows[i]["Sel"]);
                    MobjDTOSettlementPolicy.lstclsDTOInExGra.Add(objclsInExGra);
                }
            }

            //'' For LeavePay/Encash
            //parameters = new ArrayList();
            //parameters.Add(new SqlParameter("@Mode", 11));
            //parameters.Add(new SqlParameter("@SetPolicyID", Convert.ToInt32(this.MobjDTOSettlementPolicy.SetPolicyID)));
            //parameters.Add(new SqlParameter("@PolicyType", 8));
            //parameters.Add(new SqlParameter("@IsArabicView", 0));
            //DtablePolicy = MobjDataLayer.ExecuteDataTable(strProcedureName, parameters);

              //MobjDTOSettlementPolicy.lstclsDTOInExLeave  = new System.Collections.Generic.List<DTO.clsDTOInExLeave>();

               //if ((DtablePolicy.Rows.Count > 0) )
               // {
               //     for (int i = 0; DtablePolicy.Rows.Count - 1 >= i; ++i)
               //     {
               //         clsDTOInExLeave  objclsInExLeave = new clsDTOInExLeave();
               //         objclsInExLeave.AdditionDeductionID= Convert.ToInt32(DtablePolicy.Rows[i]["AdditionDeductionID"]);
               //         objclsInExLeave.DescriptionStr= Convert.ToString(DtablePolicy.Rows[i]["DescriptionStr"]);
               //         objclsInExLeave.Sel= Convert.ToBoolean(DtablePolicy.Rows[i]["Sel"]);
               //         MobjDTOSettlementPolicy.lstclsDTOInExLeave.Add(objclsInExLeave);
               //     }
               // }
        }
        catch (Exception ex)
             {

             }
        
         }







      public int RecCount()
      {
          int RecordCnt = 0;
          string TSQL;
          DataTable DT;
          TSQL = "Select (isnull(count(1),0)+1) from [PaySettlementPolicy]";
          DT = MobjDataLayer.ExecuteDataTable(TSQL);
          if (DT.Rows.Count > 0)
          {
              RecordCnt = Convert.ToInt32(DT.Rows[0][0]);
          }
          else
          {
              RecordCnt = 0;
          }
          return RecordCnt;
      }
      public bool DeleteCompanySettlementPolicy()
      {
            string sSqlQuery;

            sSqlQuery = " delete from   PaySettlementPolicyDetail where SetPolicyID=" + this.MobjDTOSettlementPolicy.SetPolicyID + "";
            MobjDataLayer.ExecuteQuery(sSqlQuery);

            sSqlQuery = " delete from   PaySettlementPolicyDetailFive where SetPolicyID=" + this.MobjDTOSettlementPolicy.SetPolicyID + "";
            MobjDataLayer.ExecuteQuery(sSqlQuery);

            sSqlQuery = "DELETE FROM PaySalaryPolicyDetail WHERE  isnull(PolicyType,0) in(5,9)  and SalPolicyID=" + this.MobjDTOSettlementPolicy.SetPolicyID + "";
            MobjDataLayer.ExecuteQuery(sSqlQuery);

            sSqlQuery = " delete from PaySettlementEncashPolicy where SetPolicyID=" + this.MobjDTOSettlementPolicy.SetPolicyID + "";
            MobjDataLayer.ExecuteQuery(sSqlQuery);

            sSqlQuery = " delete from PaySettlementPolicy where SetPolicyID=" + this.MobjDTOSettlementPolicy.SetPolicyID + "";
            MobjDataLayer.ExecuteQuery(sSqlQuery);
            return true;
      }
      public int ExistsCompanySettlementPolicy()
      {
          int RecordCnt = 0;
          string TSQL;
          DataTable DT;
          TSQL = "Select 1 From PaySalaryStructure Where SetPolicyID = '" + this.MobjDTOSettlementPolicy.SetPolicyID + "'";
          DT = MobjDataLayer.ExecuteDataTable(TSQL);
          if (DT.Rows.Count > 0)
          {
              RecordCnt = 1;
          }
          else
          {
              RecordCnt = 0;
          }
          return RecordCnt;
      }

      public Int32 GetRowNum(int RecID)
      {
          int RecordCnt = 0;
          string TSQL = "select T.RowNo from (select ROW_NUMBER() OVER(ORDER BY SetPolicyID ASC) AS RowNo,SetPolicyID from PaySettlementPolicy)T where SetPolicyID=" + RecID + "";
          DataTable  DT = MobjDataLayer.ExecuteDataTable(TSQL);
          if (DT.Rows.Count > 0)
          {
              RecordCnt = Convert.ToInt32(DT.Rows[0][0]);
          }
          else
          {
              RecordCnt = 0;
          }
          return RecordCnt;

      }

    public bool SaveCompanySettlementPolicy() 
    {
    try
    {
                object intReturnid ;
                ArrayList parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode",this.MobjDTOSettlementPolicy.ModeAddEdit));

                if (this.MobjDTOSettlementPolicy.ModeAddEdit == 2)
                {
                    parameters.Add(new SqlParameter("@SetPolicyID", this.MobjDTOSettlementPolicy.SetPolicyID));
                }

                parameters.Add(new SqlParameter("@DescriptionPolicy", this.MobjDTOSettlementPolicy.DescriptionPolicy ));
                parameters.Add(new SqlParameter("@IsRateOnlyG", this.MobjDTOSettlementPolicy.IsRateOnly));
                parameters.Add(new SqlParameter("@IsIncludeLeave", this.MobjDTOSettlementPolicy.blnIsIncludeLeave));

                if (this.MobjDTOSettlementPolicy.IsRateOnly==1)
                {
                    parameters.Add(new SqlParameter("@RatePerDayG", this.MobjDTOSettlementPolicy.RatePerDay));
                }
                else
                {
                    parameters.Add(new SqlParameter("@CalculationIDG", this.MobjDTOSettlementPolicy.CalculationID));
                }

                SqlParameter sqlParam = new SqlParameter("@ReturnValue", SqlDbType.Int);
                sqlParam.Direction = ParameterDirection.ReturnValue;
                parameters.Add(sqlParam);
               intReturnid = MobjDataLayer.ExecuteScalar(strProcedureName, parameters);
               if (intReturnid != null)
               {
                   this.MobjDTOSettlementPolicy.SetPolicyID = intReturnid.ToInt32();
               }

                foreach (clsDTOSettlementPolicyDetail  objclsDTOSettlementPolicyDetail in MobjDTOSettlementPolicy.lstclsDTOSettlementPolicyDetail)
                {
                    if (this.MobjDTOSettlementPolicy.SetPolicyID != 0)
                    {
                        if (objclsDTOSettlementPolicyDetail.ParameterID != null)
                        {
                            if (objclsDTOSettlementPolicyDetail.ParameterID != 0)
                            {
                                parameters = new ArrayList();
                                parameters.Add(new SqlParameter("@Mode ", 4));
                                parameters.Add(new SqlParameter("@SetPolicyID ", this.DTOSettlementPolicy.SetPolicyID));
                                parameters.Add(new SqlParameter("@ParameterID", objclsDTOSettlementPolicyDetail.ParameterID));
                                parameters.Add(new SqlParameter("@NoOfMonths", objclsDTOSettlementPolicyDetail.NoOfMonths));
                                parameters.Add(new SqlParameter("@NoOfDays", objclsDTOSettlementPolicyDetail.NoOfDays));
                                parameters.Add(new SqlParameter("@TerminationDays", objclsDTOSettlementPolicyDetail.decTerminationDays));
                                parameters.Add(new SqlParameter("@PolicyType", 5));
                                MobjDataLayer.ExecuteScalar(strProcedureName, parameters);
                            }
                        }
                    }
                }



                foreach (clsDTOSettlementPolicyDetailFive objclsDTOSettlementPolicyDetailFive in MobjDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFive)
                {
                    if (this.MobjDTOSettlementPolicy.SetPolicyID != 0)
                    {
                        if (objclsDTOSettlementPolicyDetailFive.ParameterID != null)
                        {
                            if (objclsDTOSettlementPolicyDetailFive.ParameterID != 0)
                            {
                                parameters = new ArrayList();
                                parameters.Add(new SqlParameter("@Mode ", 13));
                                parameters.Add(new SqlParameter("@SetPolicyID ", this.DTOSettlementPolicy.SetPolicyID));
                                parameters.Add(new SqlParameter("@ParameterID", objclsDTOSettlementPolicyDetailFive.ParameterID));
                                parameters.Add(new SqlParameter("@NoOfMonths", objclsDTOSettlementPolicyDetailFive.NoOfMonths));
                                parameters.Add(new SqlParameter("@NoOfDays", objclsDTOSettlementPolicyDetailFive.NoOfDays));
                                parameters.Add(new SqlParameter("@TerminationDays", objclsDTOSettlementPolicyDetailFive.decTerminationDays));
                                parameters.Add(new SqlParameter("@PolicyType", 5));
                                MobjDataLayer.ExecuteScalar(strProcedureName, parameters);
                            }
                        }
                    }
                }


                //MobjDataLayer.ExecuteQuery("DELETE FROM PaySettlementEncashPolicy WHERE SetPolicyID=" + this.MobjDTOSettlementPolicy.SetPolicyID + "");

                if (this.DTOSettlementPolicy.ModeAddEdit  == 2)
                {
                MobjDataLayer.ExecuteQuery("DELETE FROM PaySalaryPolicyDetail WHERE  PolicyType in(7,8) AND SalPolicyID=" + DTOSettlementPolicy.SetPolicyID + "");
                }

                foreach (clsDTOInExGra   objclsDTOInExGrain in MobjDTOSettlementPolicy.lstclsDTOInExGra)
                {
                  if (Convert.ToBoolean(objclsDTOInExGrain.Sel) == true)
                  {
                      string TSQL = "Insert into PaySalaryPolicyDetail select isnull(max(isnull(SerialNo,0)),0)+1, " +
                             " " + DTOSettlementPolicy.SetPolicyID + ", " + Convert.ToInt32(objclsDTOInExGrain.AdditionDeductionID) + ", 7 from PaySalaryPolicyDetail ";
                      MobjDataLayer.ExecuteQuery(TSQL);
                  }
                }
                return true;
        }
        catch(Exception ex)
        {
            return false;
        }
        

    }

    public bool CheckploicyNameDup(int intPolicyID, string strPolicyName)
    {
        ArrayList parameters;

        parameters = new ArrayList();

        parameters.Add(new SqlParameter("@Mode", 12));
        parameters.Add(new SqlParameter("@DescriptionPolicy", strPolicyName));
        parameters.Add(new SqlParameter("@SetPolicyID", this.DTOSettlementPolicy.SetPolicyID));

        int value = MobjDataLayer.ExecuteScalar("spPaySettlementPolicy", parameters).ToInt32();

        if (value > 0)
        {
            return true;
        }
        else
            return false;
    }




        /////////////////////////////////////////////////////////////////////////////////////////////
    }

}