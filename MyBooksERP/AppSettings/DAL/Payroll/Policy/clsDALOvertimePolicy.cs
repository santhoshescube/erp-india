﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace MyBooksERP
{
    public class clsDALOvertimePolicy
    {
        private clsDTOOvertimePolicy MobjDTOOvertimePolicy = null;
        private string strProcedureName = "spPayOvertimePolicy";
        private List<SqlParameter> sqlParameters = null;

        DataLayer objDataLayer = null;

        public clsDALOvertimePolicy() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.objDataLayer == null)
                    this.objDataLayer = new DataLayer();

                return this.objDataLayer;
            }
            set
            {
                this.objDataLayer = value;
            }
        }

        public clsDTOOvertimePolicy DTOOvertimePolicy
        {
            get
            {
                if (this.MobjDTOOvertimePolicy == null)
                    this.MobjDTOOvertimePolicy = new clsDTOOvertimePolicy();

                return this.MobjDTOOvertimePolicy;
            }
            set
            {
                this.MobjDTOOvertimePolicy = value;
            }
        }

        public DataTable GetAdditionDeductionDetails()
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@IsArabicView", 0)};
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool OTPolicyMasterSave(bool blnEditMode)
        {
            bool blnRetValue = false;
            object OTPolicyID = 0;
            this.sqlParameters = new List<SqlParameter>();

            if (blnEditMode ==false)
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", 3));
                
            }
            else
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", 7));
                this.sqlParameters.Add(new SqlParameter("@OTPolicyID", DTOOvertimePolicy.intOTPolicyID));
            }

            this.sqlParameters.Add(new SqlParameter("@OTPolicy", this.DTOOvertimePolicy.strOTPolicyName));
            if (this.DTOOvertimePolicy.intCalculationID > 0)
                this.sqlParameters.Add(new SqlParameter("@CalculationID", this.DTOOvertimePolicy.intCalculationID));
            this.sqlParameters.Add(new SqlParameter("@TotalWorkingHours", this.DTOOvertimePolicy.decTotalWorkingHours));
            this.sqlParameters.Add(new SqlParameter("@IsRateOnly", this.DTOOvertimePolicy.boolIsRateOnly));
            this.sqlParameters.Add(new SqlParameter("@IsCompanyBasedOn", this.DTOOvertimePolicy.intCompanyBasedOn));
            this.sqlParameters.Add(new SqlParameter("@OTRate", this.DTOOvertimePolicy.decOTRate));
            this.sqlParameters.Add(new SqlParameter("@CalculationPercentage", this.DTOOvertimePolicy.CalculationPercentage));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            this.sqlParameters.Add(objParam);
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out OTPolicyID) != 0)
            {
                DTOOvertimePolicy.intOTPolicyID = (int)OTPolicyID;
                blnRetValue = true;
            }
            return blnRetValue;

        }

        public bool SalrayPolicyDetailsSave(bool blnEditMode)
        {
            bool blnRetValue = false;

            
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",8),
                                    new SqlParameter("@SalPolicyID", this.DTOOvertimePolicy.intOTPolicyID)};
            if (this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32() != 0)
            {
                blnRetValue = true;
            }

            if (this.DTOOvertimePolicy.intCalculationID != 1 && this.DTOOvertimePolicy.intCalculationID != 0)
            {
 
            foreach(clsDTOSalaryPolicyDetail DTOSalaryPolicyDetail in DTOOvertimePolicy.DTOSalaryPolicyDetail)
            {
                this.sqlParameters = new List<SqlParameter>();
                if (blnEditMode == false || blnEditMode == true)
                {
                    this.sqlParameters.Add(new SqlParameter("@Mode",4));

                }
                
                if (DTOSalaryPolicyDetail.intAdditionDeductionID > 0)
                {
                    this.sqlParameters.Add(new SqlParameter("@SalPolicyID", this.DTOOvertimePolicy.intOTPolicyID));
                    this.sqlParameters.Add(new SqlParameter("@AdditionDeductionID", DTOSalaryPolicyDetail.intAdditionDeductionID));
                    this.sqlParameters.Add(new SqlParameter("@PolicyType", 3));
                    if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters) != 0)
                    {
                        blnRetValue = true;
                    }
                }
            }
            }

            return blnRetValue;

        }

        public bool DisplayOTPolicyMaster(int intRowNumber)
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                //new SqlParameter("@LeavePolicyID", intLeavePolicyID),
                new SqlParameter("@RowNumber", intRowNumber) };
            SqlDataReader sdr = this.DataLayer.ExecuteReader(this.strProcedureName, this.sqlParameters);
            if (sdr.Read())
            {
                DTOOvertimePolicy.intOTPolicyID = sdr["OTPolicyID"].ToInt32();
                DTOOvertimePolicy.strOTPolicyName = sdr["OTPolicy"].ToString();
                DTOOvertimePolicy.intCalculationID = sdr["CalculationID"].ToInt32();
                DTOOvertimePolicy.decTotalWorkingHours = sdr["TotalWorkingHours"].ToDecimal();
                DTOOvertimePolicy.boolIsRateOnly = sdr["IsRateOnly"].ToBoolean();
                DTOOvertimePolicy.intCompanyBasedOn = sdr["IsCompanyBasedOn"].ToInt32();
                DTOOvertimePolicy.decOTRate = sdr["OTRate"].ToDecimal();
                DTOOvertimePolicy.CalculationPercentage = sdr["CalculationPercentage"].ToDecimal();

                blnRetValue = true;
            }
            sdr.Close();
            return blnRetValue;
        }

        public DataTable DispalySalaryPolicyDetail(int intOTPolicyID)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 6),
                new SqlParameter("@SalPolicyID", intOTPolicyID),
                new SqlParameter("@IsArabicView", 0)};
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }

        public int GetRecordCount()
        {
            int intRecordCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 5) };
            intRecordCount = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRecordCount;
        }

        public bool DeleteOTPolicy()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",9),
                                    new SqlParameter("@OTPolicyID", this.DTOOvertimePolicy.intOTPolicyID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }

        public bool PolicyIDExists()
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 10),
               new SqlParameter("@OTPolicyID", this.DTOOvertimePolicy.intOTPolicyID)
                };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);

        }

        public int GetRowNumber()
        {
            int intRowNumber = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 12),
                 new SqlParameter("@OTPolicyID", this.DTOOvertimePolicy.intOTPolicyID),};
            intRowNumber = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRowNumber;

             
        }
        public bool CheckDuplicate(int iOTPolicyID, string strPolicyName)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 13),
                new SqlParameter("@OTPolicy", strPolicyName),
                new SqlParameter("@OTPolicyID", iOTPolicyID)
                 };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);
        }




    }
}
