﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Data;


namespace MyBooksERP
{
    public class clsDALPFPolicy
    {
        //ArrayList prmDynamicdeductionPolicy;
        ArrayList prmdeductionPolicy; // for setting Sql parameters

        ClsCommonUtility MObjClsCommonUtility;
        DataLayer MObjClsDataLayer;

        public clsDTOPFPolicy objClsDTOPFPolicy { get; set; } // DTO Company Information Property

        public clsDALPFPolicy(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MObjClsDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MObjClsDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MObjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }
        public DataTable FillDeductiondetailAddMode()
        {

            prmdeductionPolicy = new ArrayList();

            prmdeductionPolicy.Add(new SqlParameter("@Mode", 4));

            return MObjClsDataLayer.ExecuteDataTable("spPayDeductionPolicyDetTransaction", prmdeductionPolicy);
           
        }
        public int SavePolicy(bool blnAddStatus)
        {
            int iDeductionPolicyId=0;
            prmdeductionPolicy = new ArrayList();
        if(blnAddStatus)
            prmdeductionPolicy.Add(new SqlParameter("@Mode", 1));
        else
            prmdeductionPolicy.Add(new SqlParameter("@Mode", 2));
     

        prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId",objClsDTOPFPolicy.intDeductionPolicyId));
        prmdeductionPolicy.Add(new SqlParameter("@PolicyName",objClsDTOPFPolicy.strPolicyName));
        prmdeductionPolicy.Add(new SqlParameter("@AdditionID", objClsDTOPFPolicy.intAdditionID));
        if (objClsDTOPFPolicy.intParameterId > 0) 
            prmdeductionPolicy.Add(new SqlParameter("@ParameterId", objClsDTOPFPolicy.intParameterId));

        if (objClsDTOPFPolicy.decAmountLimit > 0) 
            prmdeductionPolicy.Add(new SqlParameter("@AmountLimit", objClsDTOPFPolicy.decAmountLimit));
        
        prmdeductionPolicy.Add(new SqlParameter("@EmployerPart", objClsDTOPFPolicy.dEmployerPart));
        prmdeductionPolicy.Add(new SqlParameter("@EmployeePart", objClsDTOPFPolicy.dEmployeePart));
        prmdeductionPolicy.Add(new SqlParameter("@WithEffect", objClsDTOPFPolicy.strWithEffect));
        prmdeductionPolicy.Add(new SqlParameter("@RateOnly", objClsDTOPFPolicy.intRateOnly));
        prmdeductionPolicy.Add(new SqlParameter("@EPSEmployer", objClsDTOPFPolicy.decEPSEmployer));
        prmdeductionPolicy.Add(new SqlParameter("@EDLI", objClsDTOPFPolicy.decEDLI));
        prmdeductionPolicy.Add(new SqlParameter("@EPFAdmin", objClsDTOPFPolicy.decEPFAdmin));
        prmdeductionPolicy.Add(new SqlParameter("@EDLIAdmin", objClsDTOPFPolicy.decEDLIAdmin));
        prmdeductionPolicy.Add(new SqlParameter("@FixedAmount", objClsDTOPFPolicy.decFixedAmount));
        prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionID", objClsDTOPFPolicy.intAdditionDeductionID));
       SqlParameter sQlparam =new SqlParameter("@ReturnVal", SqlDbType.Int);
        sQlparam.Direction = ParameterDirection.ReturnValue;
        prmdeductionPolicy.Add(sQlparam);
        iDeductionPolicyId = Convert.ToInt32(MObjClsDataLayer.ExecuteScalar("spPayPFPolicyTransaction", prmdeductionPolicy));
        if (iDeductionPolicyId > 0)
        {
            DeletePolicyDetails(iDeductionPolicyId);
            SavePolicyDetails(iDeductionPolicyId);

            return iDeductionPolicyId;
        }
        return iDeductionPolicyId;
        }
        private void DeletePolicyDetails(int iOutPolicyID)
        {
            prmdeductionPolicy = new ArrayList();
            prmdeductionPolicy.Add(new SqlParameter("@Mode", 5));
            prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId", iOutPolicyID));

            MObjClsDataLayer.ExecuteNonQuery("spPayDeductionPolicyDetTransaction", prmdeductionPolicy);
        }
        private void SavePolicyDetails(int iOutPolicyID)
        {
            foreach (clsDTOPFPolicyDetails objClsDTOPFPolicyDetails in   objClsDTOPFPolicy.lstclsDTOPFPolicyDetails)
            {
                prmdeductionPolicy = new ArrayList();
                prmdeductionPolicy.Add(new SqlParameter("@Mode", 1));
                prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId", iOutPolicyID));
                prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionID", objClsDTOPFPolicyDetails.intDetAddDedID));

                MObjClsDataLayer.ExecuteNonQuery("spPayDeductionPolicyDetTransaction", prmdeductionPolicy);
            }
        }
        // To get the Record Count

        public int RecCountNavigate()
        {

            prmdeductionPolicy = new ArrayList();
            prmdeductionPolicy.Add(new SqlParameter("@Mode", 5));

            return Convert.ToInt32(MObjClsDataLayer.ExecuteScalar("spPayPFPolicyTransaction", prmdeductionPolicy));


        }
        public bool Getpolicy(int Rownum)
        {
            prmdeductionPolicy = new ArrayList();

            prmdeductionPolicy.Add(new SqlParameter("@Mode", 3));
            prmdeductionPolicy.Add(new SqlParameter("@RowNumber", Rownum));

            SqlDataReader sdrPolicy = MObjClsDataLayer.ExecuteReader("spPayPFPolicyTransaction", prmdeductionPolicy, CommandBehavior.SingleRow);

            if (sdrPolicy.Read())
            {
                objClsDTOPFPolicy.intDeductionPolicyId = Convert.ToInt32(sdrPolicy["AdditionDeductionPolicyId"]);
                objClsDTOPFPolicy.strPolicyName = Convert.ToString(sdrPolicy["PolicyName"]);
                objClsDTOPFPolicy.intAdditionID = sdrPolicy["AdditionID"].ToInt32();
                objClsDTOPFPolicy.intParameterId = sdrPolicy["ParameterId"].ToInt32();
                objClsDTOPFPolicy.decAmountLimit = Convert.ToDecimal(sdrPolicy["AmountLimit"]);
                objClsDTOPFPolicy.dEmployeePart = Convert.ToDecimal(sdrPolicy["EmployeePart"]);
                objClsDTOPFPolicy.dEmployerPart = Convert.ToDecimal(sdrPolicy["EmployerPart"]);
                objClsDTOPFPolicy.intAdditionDeductionID = Convert.ToInt32(sdrPolicy["AdditionDeductionID"]);
                objClsDTOPFPolicy.strWithEffect = Convert.ToString(sdrPolicy["WithEffect"]);
                objClsDTOPFPolicy.intRateOnly = Convert.ToInt32(sdrPolicy["RateOnly"]);
                objClsDTOPFPolicy.decEPSEmployer = sdrPolicy["EPSEmployer"].ToDecimal();
                objClsDTOPFPolicy.decEDLI = sdrPolicy["EDLI"].ToDecimal();
                objClsDTOPFPolicy.decEPFAdmin =sdrPolicy["EPFAdmin"].ToDecimal();
                objClsDTOPFPolicy.decEDLIAdmin = sdrPolicy["EDLIAdmin"].ToDecimal();
                objClsDTOPFPolicy.decFixedAmount = sdrPolicy["FixedAmount"].ToDecimal();

                //if (sdrPolicy["EmployerPart"] != DBNull.Value)
                //    objclsDTOWorkPolicy.OffDayShiftID = Convert.ToInt32(sdrPolicy["ShiftID"]);
                //else
                //    objclsDTOWorkPolicy.OffDayShiftID = 0;

                sdrPolicy.Close();
                return true;
            }
            else
                sdrPolicy.Close();
            return false;
        }
        public DataTable FillDeductiondetail(int intDeductionID)
        {

            prmdeductionPolicy = new ArrayList();

            prmdeductionPolicy.Add(new SqlParameter("@Mode", 3));
            prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId", intDeductionID)); 
            return MObjClsDataLayer.ExecuteDataTable("spPayDeductionPolicyDetTransaction", prmdeductionPolicy);

        }
        public bool IsExists()
        {
            prmdeductionPolicy = new ArrayList();

            prmdeductionPolicy.Add(new SqlParameter("@Mode", 6));
            prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId", objClsDTOPFPolicy.intDeductionPolicyId));

            return (Convert.ToInt32(MObjClsDataLayer.ExecuteScalar("spPayPFPolicyTransaction", prmdeductionPolicy)) > 0 ? true : false);
        }
        public bool Delete()
        {
            prmdeductionPolicy = new ArrayList();

            prmdeductionPolicy.Add(new SqlParameter("@Mode", 4));
            prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId", objClsDTOPFPolicy.intDeductionPolicyId));

            MObjClsDataLayer.ExecuteNonQuery("spPayPFPolicyTransaction", prmdeductionPolicy);
            return true;
        }
        public bool CheckDuplicate(int iPolicyID, string strPolicyName, bool blnAddStatus)
        {
            int intExists = 0;
               prmdeductionPolicy = new ArrayList();
               if (blnAddStatus)
               {
                   prmdeductionPolicy.Add(new SqlParameter("@Mode", 8));
               }
               else
               {
                   prmdeductionPolicy.Add(new SqlParameter("@Mode", 9));
               }
                   prmdeductionPolicy.Add(new SqlParameter("@PolicyName", strPolicyName));
                prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId", iPolicyID));
                 
            intExists = Convert.ToInt32(MObjClsDataLayer.ExecuteScalar("spPayPFPolicyTransaction",prmdeductionPolicy));
            return (intExists > 0);
        }
        public DataSet DisplayDeductionPolicy() //Work Policy
        {
            prmdeductionPolicy = new ArrayList();
            prmdeductionPolicy.Add(new SqlParameter("@Mode", 7));
            prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId", objClsDTOPFPolicy.intDeductionPolicyId));
            return  MObjClsDataLayer.ExecuteDataSet("spPayPFPolicyTransaction", prmdeductionPolicy);

        }
    }
}
