﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALHolidayType
    {

        
        private clsDTOHolidayType MobjDTOHolidayType = null;
        private string strProcedureName = "spPayHolidayTypeReference";
        private List<SqlParameter> sqlParameters = null;

        DataLayer objDataLayer = null;

        public clsDALHolidayType() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.objDataLayer == null)
                    this.objDataLayer = new DataLayer();

                return this.objDataLayer;
            }
            set
            {
                this.objDataLayer = value;
            }
        }

        public clsDTOHolidayType DTOHolidayType 
        {
            get
            {
                if (this.MobjDTOHolidayType == null)
                    this.MobjDTOHolidayType = new clsDTOHolidayType();

                return this.MobjDTOHolidayType;
            }
            set
            {
                this.MobjDTOHolidayType = value;
            }
        }

        public bool DisplayHolidayType(int intRowNumber)
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@RowNumber", intRowNumber) };
            SqlDataReader sdr = this.DataLayer.ExecuteReader(this.strProcedureName, this.sqlParameters);
            if (sdr.Read())
            {
                DTOHolidayType.intHolidayTypeID = sdr["HolidayTypeID"].ToInt32();
                DTOHolidayType.strHolidayType = sdr["HolidayType"].ToString();
                DTOHolidayType.strColor = sdr["Color"].ToString();
                
                blnRetValue = true;
            }
            sdr.Close();
            return blnRetValue;
        }

        public int GetRecordCount()
        {
            int intRecordCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 5) };
            intRecordCount = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRecordCount;
        }


        public bool HolidayTypeSave(bool blnEditMode)
        {
            bool blnRetValue = false;
            object HolidayTypeID = 0;
            this.sqlParameters = new List<SqlParameter>();

            if (blnEditMode == false)
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", 2));

            }
            else
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", 3));
                this.sqlParameters.Add(new SqlParameter("@HolidayTypeID", this.DTOHolidayType.intHolidayTypeID));
            }

            this.sqlParameters.Add(new SqlParameter("@HolidayType", this.DTOHolidayType.strHolidayType));
            this.sqlParameters.Add(new SqlParameter("@Color", this.DTOHolidayType.strColor));
           
            
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            this.sqlParameters.Add(objParam);
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out HolidayTypeID) != 0)
            {
                DTOHolidayType.intHolidayTypeID = (int)HolidayTypeID;
                blnRetValue = true;
            }
            return blnRetValue;

        }

        public bool HolidayTypeIDExists()
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 6),
               new SqlParameter("@HolidayTypeID", this.DTOHolidayType.intHolidayTypeID)
                };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);

        }

        public bool DeleteHolidayType()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",4),
                                    new SqlParameter("@HolidayTypeID", this.DTOHolidayType.intHolidayTypeID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }

        public bool CheckDuplicate(int iHolidayTypeID, string strHolidayType)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 7),
                new SqlParameter("@HolidayType", strHolidayType),
                new SqlParameter("@HolidayTypeID", iHolidayTypeID)
                 };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);
        }







    }
}
