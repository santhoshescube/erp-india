﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DTO;
namespace MyBooksERP
{
    public class clsDALVacationPolicy
    {
        #region Declartions

        ClsCommonUtility MObjClsCommonUtility;

        private clsDTOVacationPolicy MobjDTOVacationPolicy = null;
        private string strProcedureName = "spPayVacationPolicy";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALVacationPolicy(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
       

        public clsDTOVacationPolicy DTOVacationPolicy
        {
            get
            {
                if (this.MobjDTOVacationPolicy == null)
                    this.MobjDTOVacationPolicy = new clsDTOVacationPolicy();

                return this.MobjDTOVacationPolicy;
            }
            set
            {
                this.MobjDTOVacationPolicy = value;
            }
        }
        #endregion Declartions

        #region checkDuplication
        public bool  checkDuplication(string strName)
         {
                int blnReturn =0;
                DataTable DT;
                ArrayList parameters;
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 10));
                parameters.Add(new SqlParameter("@Description", strName));
                SqlParameter sQlparam = new SqlParameter("@ReturnVal", SqlDbType.Bit);
                sQlparam.Direction = ParameterDirection.ReturnValue;
                parameters.Add(sQlparam);

                DT = MobjDataLayer.ExecuteDataTable("spPayVacationPolicy", parameters);
                if (DT.Rows.Count >0) 
                {
                    return true;
                }
                else
                {
                    return false;
                }
                                 return false;
         }

        #endregion checkDuplication

        #region getParticular
        public DataTable getParticular()
         {
            ArrayList parameters;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@IsArabicView", 0));
            return MobjDataLayer.ExecuteDataTable("spPayVacationPolicy", parameters);
         }
        #endregion getParticular

        #region FillCombos
        public DataTable FillCombos(string[] saFieldValues)
         {
             // function for getting datatable for filling combo
             if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
             {
                 return MObjClsCommonUtility.FillCombos(saFieldValues);
             }
             else
                 return null;
         }
        #endregion FillCombos

        #region checkVacationPolicyUsed

        public bool checkVacationPolicyUsed() 
         {
            bool blnReturn  = false;


            ArrayList parameters = new ArrayList(); 
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@VacationPolicyID", MobjDTOVacationPolicy.VacationPolicyID ));

            SqlParameter sQlparam = new SqlParameter("@ReturnVal", SqlDbType.Bit);
            sQlparam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(sQlparam);
            blnReturn =Convert.ToBoolean (  MobjDataLayer.ExecuteScalar("spPayVacationPolicy", parameters));

            if (Convert.ToBoolean(blnReturn)==true) 
            {
                return Convert.ToBoolean(blnReturn);
            }
            return false;

         }
        #endregion checkVacationPolicyUsed

        #region DeleteVacationPolicy
        public bool DeleteVacationPolicy() 
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 8));
        parameters.Add(new SqlParameter("@VacationPolicyID", MobjDTOVacationPolicy.VacationPolicyID ));
        MobjDataLayer.ExecuteNonQuery("spPayVacationPolicy",parameters);
        return true;

    }
        #endregion DeleteVacationPolicy

        #region InsertVacationPolicyMaster

        public bool InsertVacationPolicyMaster()
    {
        object intReturnid;
        //int intReturnidRet=0;
        ArrayList parameters;
        //intReturnid=new intReturnid();
        MobjDTOVacationPolicy.VacationPolicyID = 0;
        parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 2));
        parameters.Add(new SqlParameter("@Description", MobjDTOVacationPolicy.VacationPolicy ));
        parameters.Add(new SqlParameter("@IsRateOnly", MobjDTOVacationPolicy.IsRateOnly ));
        parameters.Add(new SqlParameter("@RatePerDay", MobjDTOVacationPolicy.RatePerDay));
        parameters.Add(new SqlParameter("@CalculationID", MobjDTOVacationPolicy.CalculationID));
        parameters.Add(new SqlParameter("@NoOfTickets", MobjDTOVacationPolicy.NoOfTickets));
        parameters.Add(new SqlParameter("@TicketAmount", MobjDTOVacationPolicy.TicketAmount));
        parameters.Add(new SqlParameter("@OnTimeRejoinBenefit", MobjDTOVacationPolicy.dblOnTimeRejoinBenefit));
        parameters.Add(new SqlParameter("@IsLeaveOnly", MobjDTOVacationPolicy.chkLeaveOnly));
        parameters.Add(new SqlParameter("@IsFullSalary", MobjDTOVacationPolicy.chkFullSalary));
        SqlParameter sQlparam =new SqlParameter("@ReturnVal", SqlDbType.Int);
        sQlparam.Direction = ParameterDirection.ReturnValue;
        parameters.Add(sQlparam);

        intReturnid = MobjDataLayer.ExecuteScalar("spPayVacationPolicy", parameters);


        if (Convert.ToInt32(intReturnid) != 0)
        {
            if (Convert.ToInt32(intReturnid) > 0)
            {
                MobjDTOVacationPolicy.VacationPolicyID = Convert.ToInt32(intReturnid);
            }
            else
            {
                MobjDTOVacationPolicy.VacationPolicyID = 0;
            }
            return true;
        }
        return false;

    }
        #endregion InsertVacationPolicyMaster

        #region UpdateVacationPolicyMaster
        public bool UpdateVacationPolicyMaster() 
        {
            //int intReturnid  = 0;
            ArrayList parameters;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@VacationPolicyID", MobjDTOVacationPolicy.VacationPolicyID));
            parameters.Add(new SqlParameter("@Description", MobjDTOVacationPolicy.VacationPolicy ));
            parameters.Add(new SqlParameter("@IsRateOnly", MobjDTOVacationPolicy.IsRateOnly));
            parameters.Add(new SqlParameter("@RatePerDay", MobjDTOVacationPolicy.RatePerDay));
            parameters.Add(new SqlParameter("@CalculationID", MobjDTOVacationPolicy.CalculationID));
            parameters.Add(new SqlParameter("@NoOfTickets", MobjDTOVacationPolicy.NoOfTickets));
            parameters.Add(new SqlParameter("@TicketAmount", MobjDTOVacationPolicy.TicketAmount ));
            parameters.Add(new SqlParameter("@OnTimeRejoinBenefit", MobjDTOVacationPolicy.dblOnTimeRejoinBenefit));
            parameters.Add(new SqlParameter("@IsLeaveOnly", MobjDTOVacationPolicy.chkLeaveOnly));
            parameters.Add(new SqlParameter("@IsFullSalary", MobjDTOVacationPolicy.chkFullSalary));
            MobjDataLayer.ExecuteNonQuery("spPayVacationPolicy", parameters);
            return true;

        }
        #endregion UpdateVacationPolicyMaster

        #region InsertVacationPolicyDetails
        public bool InsertVacationPolicyDetails() 
        {

            MobjDataLayer.ExecuteQuery("Delete from [PayVacationPolicyDetail] where [VacationPolicyID]=" + MobjDTOVacationPolicy.VacationPolicyID + "");

                ArrayList parameters ;
                foreach (clsDTOVacationPolicyDetail objclsDTOVacationPolicyDetail in MobjDTOVacationPolicy.lstclsDTOVacationPolicyDetail)
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 3));
                    parameters.Add(new SqlParameter("@VacationPolicyID", MobjDTOVacationPolicy.VacationPolicyID));
                    parameters.Add(new SqlParameter("@Parameter", objclsDTOVacationPolicyDetail.ParameterID  ));
                    parameters.Add(new SqlParameter("@NoOfMonths", objclsDTOVacationPolicyDetail.NoOfMonths ));
                    parameters.Add(new SqlParameter("@NoOfDays", objclsDTOVacationPolicyDetail.NoOfDays ));
                    parameters.Add(new SqlParameter("@NoOfTickets", objclsDTOVacationPolicyDetail.NoOfTickets ));
                    parameters.Add(new SqlParameter("@TicketAmount", objclsDTOVacationPolicyDetail.TicketAmount ));
                    MobjDataLayer.ExecuteNonQuery("spPayVacationPolicy", parameters);
                }
      
            return true;
        }
        #endregion InsertVacationPolicyDetails

        #region SelectVacationPolicy
        public DataSet SelectVacationPolicy(int intRowNumber) 
        {
            DataTable DtMaster;

            ArrayList parameters;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@RowNumber", intRowNumber));
            DataSet  DtSet;
            DtSet = MobjDataLayer.ExecuteDataSet("spPayVacationPolicy", parameters);

            DtMaster = DtSet.Tables[0];

            if (DtMaster.Rows.Count > 0)
            {
                MobjDTOVacationPolicy.CalculationID =Convert.ToInt32(DtMaster.Rows[0]["CalculationID"]);
                MobjDTOVacationPolicy.dblOnTimeRejoinBenefit = Convert.ToDecimal(DtMaster.Rows[0]["OnTimeRejoinBenefit"]);
                MobjDTOVacationPolicy.IsRateOnly = Convert.ToBoolean(DtMaster.Rows[0]["IsRateOnly"]);
                MobjDTOVacationPolicy.NoOfTickets = Convert.ToDecimal(DtMaster.Rows[0]["NoOfTickets"]);
                MobjDTOVacationPolicy.RatePerDay = Convert.ToDecimal(DtMaster.Rows[0]["RatePerDay"]);
                MobjDTOVacationPolicy.TicketAmount = Convert.ToDecimal(DtMaster.Rows[0]["TicketAmount"]);
                MobjDTOVacationPolicy.VacationPolicy =Convert.ToString( DtMaster.Rows[0]["VacationPolicy"]);
                MobjDTOVacationPolicy.VacationPolicyID = Convert.ToInt32(DtMaster.Rows[0]["VacationPolicyID"]);
                MobjDTOVacationPolicy.chkLeaveOnly = Convert.ToBoolean(DtMaster.Rows[0]["IsLeaveOnly"]);
                MobjDTOVacationPolicy.chkFullSalary = Convert.ToBoolean(DtMaster.Rows[0]["IsFullSalary"]);
            }
            return DtSet;
        }
        #endregion SelectVacationPolicy

        #region SelectVacationPolicyByVacationPolicyID
        public DataSet SelectVacationPolicyByVacationPolicyID(int intVacationPolicyID)
        {
            DataTable DtMaster;

            ArrayList parameters;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 22));
            parameters.Add(new SqlParameter("@VacationPolicyID", intVacationPolicyID));
            DataSet DtSet;
            DtSet = MobjDataLayer.ExecuteDataSet("spPayVacationPolicy", parameters);

            DtMaster = DtSet.Tables[0];

            if (DtMaster.Rows.Count > 0)
            {
                MobjDTOVacationPolicy.CalculationID = Convert.ToInt32(DtMaster.Rows[0]["CalculationID"]);
                MobjDTOVacationPolicy.dblOnTimeRejoinBenefit = Convert.ToDecimal(DtMaster.Rows[0]["OnTimeRejoinBenefit"]);
                MobjDTOVacationPolicy.IsRateOnly = Convert.ToBoolean(DtMaster.Rows[0]["IsRateOnly"]);
                MobjDTOVacationPolicy.NoOfTickets = Convert.ToDecimal(DtMaster.Rows[0]["NoOfTickets"]);
                MobjDTOVacationPolicy.RatePerDay = Convert.ToDecimal(DtMaster.Rows[0]["RatePerDay"]);
                MobjDTOVacationPolicy.TicketAmount = Convert.ToDecimal(DtMaster.Rows[0]["TicketAmount"]);
                MobjDTOVacationPolicy.VacationPolicy = Convert.ToString(DtMaster.Rows[0]["VacationPolicy"]);
                MobjDTOVacationPolicy.VacationPolicyID = Convert.ToInt32(DtMaster.Rows[0]["VacationPolicyID"]);
                MobjDTOVacationPolicy.chkLeaveOnly = Convert.ToBoolean(DtMaster.Rows[0]["IsLeaveOnly"]);
                MobjDTOVacationPolicy.chkFullSalary = Convert.ToBoolean(DtMaster.Rows[0]["IsFullSalary"]);
            }
            return DtSet;
        }
        #endregion SelectVacationPolicyByVacationPolicyID

        #region getVacationPolicyRowNumber
        public int getVacationPolicyRowNumber(int intPolicyID)
        {
            object intReturnid;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 13));
            parameters.Add(new SqlParameter("@VacationPolicyID", intPolicyID));
            SqlParameter sQlparam = new SqlParameter("@ReturnVal", SqlDbType.Int);
            sQlparam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(sQlparam);
            intReturnid = MobjDataLayer.ExecuteScalar("spPayVacationPolicy", parameters);
            return Convert.ToInt32(intReturnid);

        }
        #endregion getVacationPolicyRowNumber

  //private bool UpdateCompanyVacationPolicy() 
  //{
  //      If UpdateVacationPolicyMaster() Then
  //          MobjclsVacationPolicy.DeleteSalaryPolicyDetail()
  //          FillAndInsertParticularExcludeParameter()
  //          MobjclsVacationPolicy.DeleteVacationPolicyDetail()
  //          FillAndInsertDetailsParameter()
  //          Return True
  //      End If
  //      Return False
  //}
        #region RecCountNavigate
        public int RecCountNavigate()
        {
            int RecordCnt = 0;
            string TSQL;
            DataTable DT;

            TSQL = "Select isnull(count(1),0) from [PayVacationPolicy]";
            DT = MobjDataLayer.ExecuteDataTable(TSQL);
            if (DT.Rows.Count > 0)
            {
                RecordCnt = Convert.ToInt32(DT.Rows[0][0]);
            }
            else
            {
                RecordCnt = 0;
            }

            return RecordCnt;
        }
        #endregion RecCountNavigate

        #region RecCount
        public int RecCount()
        {
            int RecordCnt = 0;
            string TSQL;
            DataTable DT;
            TSQL = "Select (isnull(count(1),0)+1) from [PayVacationPolicy]";
            DT = MobjDataLayer.ExecuteDataTable(TSQL);
            if (DT.Rows.Count > 0)
            {
                RecordCnt = Convert.ToInt32(DT.Rows[0][0]);
            }
            else
            {
                RecordCnt = 0;
            }
            return RecordCnt;
        }
        #endregion RecCount

        #region InsertSalaryPolicyDetail
        public bool InsertSalaryPolicyDetail() 
           {

            

            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@VacationPolicyID", MobjDTOVacationPolicy.VacationPolicyID));
            parameters.Add(new SqlParameter("@ParticularID", MobjDTOVacationPolicy.intParticularID));
            MobjDataLayer.ExecuteScalar("spPayVacationPolicy", parameters); 
            return true;


           }
        #endregion InsertSalaryPolicyDetail

        #region DeleteSalaryPolicyDetail
        public bool DeleteSalaryPolicyDetail()
           {
               ArrayList parameters = new ArrayList();
               if (MobjDTOVacationPolicy.VacationPolicyID > 0)
               {
                   parameters.Add(new SqlParameter("@Mode", 6));
                   parameters.Add(new SqlParameter("@VacationPolicyID", MobjDTOVacationPolicy.VacationPolicyID));
                   MobjDataLayer.ExecuteScalar("spPayVacationPolicy", parameters);
                   return true;
               }
               return true;
           }
        #endregion DeleteSalaryPolicyDetail

        #region CheckploicyNameDup
        public bool CheckploicyNameDup(int intPolicyID, string strPolicyName)
           {
               ArrayList parameters;

               parameters = new ArrayList();

               parameters.Add(new SqlParameter("@Mode", 23));
               parameters.Add(new SqlParameter("@Description", strPolicyName));
               parameters.Add(new SqlParameter("@VacationPolicyID", MobjDTOVacationPolicy.VacationPolicyID));

               int value = MobjDataLayer.ExecuteScalar("spPayVacationPolicy", parameters).ToInt32();

               if (value > 0)
               {
                   return true;
               }
               else
                   return false;
           }
        #endregion CheckploicyNameDup


    }
}
