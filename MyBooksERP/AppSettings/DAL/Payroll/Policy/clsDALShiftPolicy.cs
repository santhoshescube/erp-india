﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using DTO;
/* 
=================================================
   Author:		<Author,,Laxmi>
   Create date: <Create Date,,21 March 2011>
   Description:	<Description,,Shift Policy DAL Class>
================================================
*/
namespace DAL
{
    public class clsDALShiftPolicy
    {
        ArrayList prmDynamicShiftPolicy;
        ArrayList prmShiftPolicy; // for setting Sql parameters

        ClsCommonUtility MObjClsCommonUtility;
        DataLayer MObjClsDataLayer;

        public clsDTOShiftPolicy objClsDTOShiftPolicy { get; set; } // DTO Company Information Property

        public clsDALShiftPolicy(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MObjClsDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MObjClsDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MObjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }
        /// <summary>
        /// Insert and Update
        /// </summary>
        /// <param name="AddStatus"></param>
        /// <returns></returns>
        /// 
        public int Insert(bool AddStatus)
        {
            int iOutPolicyID = 0;
            prmShiftPolicy = new ArrayList();

            if (AddStatus == true)
                prmShiftPolicy.Add(new SqlParameter("@Mode", 1));
            else
                prmShiftPolicy.Add(new SqlParameter("@Mode", 2));
            prmShiftPolicy.Add(new SqlParameter("@ShiftID", objClsDTOShiftPolicy.ShiftID));
            prmShiftPolicy.Add(new SqlParameter("@CompanyID", objClsDTOShiftPolicy.CompanyID));
            prmShiftPolicy.Add(new SqlParameter("@ShiftName", objClsDTOShiftPolicy.Description));
            prmShiftPolicy.Add(new SqlParameter("@FromTime", objClsDTOShiftPolicy.FromTime));
            prmShiftPolicy.Add(new SqlParameter("@ToTime", objClsDTOShiftPolicy.ToTime));
            prmShiftPolicy.Add(new SqlParameter("@ShiftTypeID", objClsDTOShiftPolicy.ShiftType));
            prmShiftPolicy.Add(new SqlParameter("@Duration", objClsDTOShiftPolicy.Duration));
            prmShiftPolicy.Add(new SqlParameter("@NoOfTimings", objClsDTOShiftPolicy.NoOfTimings));
            prmShiftPolicy.Add(new SqlParameter("@MinWorkingHours", objClsDTOShiftPolicy.MinWorkingHours));
            prmShiftPolicy.Add(new SqlParameter("@AllowedBreakTime", objClsDTOShiftPolicy.AllowedBreakTime));
            prmShiftPolicy.Add(new SqlParameter("@LateAfter", objClsDTOShiftPolicy.LateAfter));
            prmShiftPolicy.Add(new SqlParameter("@EarlyBefore", objClsDTOShiftPolicy.EarlyBefore));
            prmShiftPolicy.Add(new SqlParameter("@IsMinimumWorkOT", objClsDTOShiftPolicy.IsMinWorkOT));
            prmShiftPolicy.Add(new SqlParameter("@Buffer", objClsDTOShiftPolicy.intBuffer));
            prmShiftPolicy.Add(new SqlParameter("@IsBufferForOT", objClsDTOShiftPolicy.intIsBufferForOT));
            prmShiftPolicy.Add(new SqlParameter("@MinimumOT", objClsDTOShiftPolicy.intMinimumOT));
            prmShiftPolicy.Add(new SqlParameter("@WeekWrkHrs", objClsDTOShiftPolicy.WeekWrkHrs));

            iOutPolicyID = Convert.ToInt32(MObjClsDataLayer.ExecuteScalar("spPayShiftPolicy", prmShiftPolicy));
            if (iOutPolicyID > 0)
            {
                if (Convert.ToInt32(objClsDTOShiftPolicy.ShiftType) == 3 || Convert.ToInt32(objClsDTOShiftPolicy.ShiftType) == 4)
                {
                    DeleteDynamicPolicy(iOutPolicyID);
                    SaveDynamicPolicy(iOutPolicyID);
                    return iOutPolicyID;
                }
            }
            return iOutPolicyID;


        }
        /// <summary>
        /// Saving ShiftDynamic Details to Table
        /// </summary>
        /// <param name="iOutShiftID"></param>
        /// 
        private void SaveDynamicPolicy(int iOutShiftID)
        {

            foreach (clsDTOShiftPolicyDynamic objclsDTOShiftPolicyDynamic in objClsDTOShiftPolicy.lstclsDTOShiftPolicyDynamic)
            {
                prmDynamicShiftPolicy = new ArrayList();
                prmDynamicShiftPolicy.Add(new SqlParameter("@Mode", 5));
                prmDynamicShiftPolicy.Add(new SqlParameter("@OrderNo", objclsDTOShiftPolicyDynamic.OrderNo));
                prmDynamicShiftPolicy.Add(new SqlParameter("@ShiftID", iOutShiftID));
                prmDynamicShiftPolicy.Add(new SqlParameter("@FromTime", objclsDTOShiftPolicyDynamic.FromTime));
                prmDynamicShiftPolicy.Add(new SqlParameter("@ToTime", objclsDTOShiftPolicyDynamic.ToTime));
                prmDynamicShiftPolicy.Add(new SqlParameter("@AllowedBreakTime", objclsDTOShiftPolicyDynamic.AllowedBreakTime));
                prmDynamicShiftPolicy.Add(new SqlParameter("@MinWorkingHours", objclsDTOShiftPolicyDynamic.MinWorkingHours));
                prmDynamicShiftPolicy.Add(new SqlParameter("@BufferTime", objclsDTOShiftPolicyDynamic.BufferTime));
                prmDynamicShiftPolicy.Add(new SqlParameter("@Duration", objclsDTOShiftPolicyDynamic.Duration));
                prmDynamicShiftPolicy.Add(new SqlParameter("@DynamicDescription", objclsDTOShiftPolicyDynamic.DynamicDescription));

                MObjClsDataLayer.ExecuteNonQuery("spPayShiftPolicy", prmDynamicShiftPolicy);
            }

        }
        /// <summary>
        /// To Delete Dynamic policy
        /// </summary>
        /// <param name="iOutPolicyID"></param>
        /// 
        private void DeleteDynamicPolicy(int iOutPolicyID)
        {
            prmDynamicShiftPolicy = new ArrayList();
            prmDynamicShiftPolicy.Add(new SqlParameter("@Mode", 4));
            prmDynamicShiftPolicy.Add(new SqlParameter("@ShiftID", iOutPolicyID));

            MObjClsDataLayer.ExecuteNonQuery("spPayShiftPolicy", prmDynamicShiftPolicy);
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <returns></returns>
        public bool Delete()
        {
            prmShiftPolicy = new ArrayList();

            prmShiftPolicy.Add(new SqlParameter("@Mode", 3));
            prmShiftPolicy.Add(new SqlParameter("@ShiftID", objClsDTOShiftPolicy.ShiftID));

            MObjClsDataLayer.ExecuteNonQuery("spPayShiftPolicy", prmShiftPolicy);
            return true;
        }

        /// <summary>
        /// To get all Shift Policy
        /// </summary>
        /// <returns></returns>
        /// 
        public DataSet GetAllShifts()
        {
            ArrayList prmGetAllShiftPolicy;

            prmGetAllShiftPolicy = new ArrayList();

            prmGetAllShiftPolicy.Add(new SqlParameter("@Mode", 7));
            prmGetAllShiftPolicy.Add(new SqlParameter("@IsArabicView", 0));
            return MObjClsDataLayer.ExecuteDataSet("spPayShiftPolicy", prmGetAllShiftPolicy);
        }
        /// <summary>
        ///  to Get Min from hours
        /// </summary>
        /// <param name="decMint"></param>
        /// <returns></returns>

        public DataSet GetMinFromHour(decimal decMint)
        {
            return MObjClsDataLayer.ExecuteDataSet("select dbo.fnPayGetMin(" + decMint + ")");
        }
        /// <summary>
        /// To GetHourFrom Min 
        /// </summary>
        /// <param name="decMint"></param>
        /// <returns></returns>
        /// 
        public DataSet GetHourFromMin(decimal decMint)
        {
            return MObjClsDataLayer.ExecuteDataSet("select dbo.fnPayGetHourFromMinute(" + decMint + ")");
        }

        /// <summary>
        /// To GetShiftDetails
        /// </summary>
        /// <returns></returns>
        public SqlDataReader GetShiftDetails()
        {
            prmShiftPolicy = new ArrayList();

            prmShiftPolicy.Add(new SqlParameter("@Mode", 8));
            prmShiftPolicy.Add(new SqlParameter("@ShiftID", objClsDTOShiftPolicy.ShiftID));

            return MObjClsDataLayer.ExecuteReader("spPayShiftPolicy", prmShiftPolicy);
        }
        /// <summary>
        /// To GetDynamicShiftDetails
        /// </summary>
        /// <returns></returns>
        /// 
        public DataSet GetDynamicShiftDetails()
        {
            prmShiftPolicy = new ArrayList();

            prmShiftPolicy.Add(new SqlParameter("@Mode", 6));
            prmShiftPolicy.Add(new SqlParameter("@ShiftID", objClsDTOShiftPolicy.ShiftID));

            return MObjClsDataLayer.ExecuteDataSet("spPayShiftPolicy", prmShiftPolicy);
        }
        /// <summary>
        /// To Check Is Exists
        /// </summary>
        /// <returns></returns>
        public bool IsExists(int intMode)
        {
            prmShiftPolicy = new ArrayList();

            prmShiftPolicy.Add(new SqlParameter("@Mode", intMode));
            prmShiftPolicy.Add(new SqlParameter("@ShiftID", objClsDTOShiftPolicy.ShiftID));

            return (Convert.ToInt32(MObjClsDataLayer.ExecuteScalar("spPayShiftPolicy", prmShiftPolicy)) > 0 ? true : false);
        }

        /// <summary>
        /// To CheckDuplication
        /// </summary>
        /// <returns></returns>
        public bool CheckDuplication(bool bAddStatus, string[] saValues, int iId, int iType)
        {
            // function for checking duplicate item
            string sField = "ShiftID";
            string sTable = "PayShiftReference";
            string sCondition = "";

            if (bAddStatus)
            {
                if (iType == 1)
                    sCondition = "ShiftName='" + saValues[0] + "'";
                else if (iType == 2)
                    sCondition = "FromTime='" + saValues[0] + " and ToTime ='" + saValues[1] + "'";

            }
            else
            {
                if (iType == 1)
                    sCondition = "ShiftName='" + saValues[0] + "'  and ShiftID <>" + iId + "";
                else if (iType == 2)
                    sCondition = "FromTime='" + saValues[0] + "' and ToTime='" + saValues[1] + " and ShiftID <>" + iId + "";

            }
            return MObjClsCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
        }

    }
}
