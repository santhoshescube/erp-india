﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Data;


namespace MyBooksERP
{
    public class clsDALdeductionPolicy
    {
        //ArrayList prmDynamicdeductionPolicy;
        ArrayList prmdeductionPolicy; // for setting Sql parameters

        ClsCommonUtility MObjClsCommonUtility;
        DataLayer MObjClsDataLayer;

        public clsDTOdeductionPolicy objClsDTOdeductionPolicy { get; set; } // DTO Company Information Property

        public clsDALdeductionPolicy(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MObjClsDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MObjClsDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MObjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }
        public DataTable FillDeductiondetailAddMode()
        {

            prmdeductionPolicy = new ArrayList();

            prmdeductionPolicy.Add(new SqlParameter("@Mode", 4));
            prmdeductionPolicy.Add(new SqlParameter("@IsArabicView", 0));
            return MObjClsDataLayer.ExecuteDataTable("spPayDeductionPolicyDetTransaction", prmdeductionPolicy);
           
        }
        public int SavePolicy(bool blnAddStatus)
        {
            int iDeductionPolicyId=0;
            prmdeductionPolicy = new ArrayList();
        if(blnAddStatus)
            prmdeductionPolicy.Add(new SqlParameter("@Mode", 1));
        else
            prmdeductionPolicy.Add(new SqlParameter("@Mode", 2));
     

        prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId",objClsDTOdeductionPolicy.intDeductionPolicyId));
        prmdeductionPolicy.Add(new SqlParameter("@PolicyName",objClsDTOdeductionPolicy.strPolicyName));
        prmdeductionPolicy.Add(new SqlParameter("@AdditionID", objClsDTOdeductionPolicy.intAdditionID));
        if (objClsDTOdeductionPolicy.intParameterId > 0) 
            prmdeductionPolicy.Add(new SqlParameter("@ParameterId", objClsDTOdeductionPolicy.intParameterId));

        if (objClsDTOdeductionPolicy.decAmountLimit > 0) 
            prmdeductionPolicy.Add(new SqlParameter("@AmountLimit", objClsDTOdeductionPolicy.decAmountLimit));
        
        prmdeductionPolicy.Add(new SqlParameter("@EmployerPart", objClsDTOdeductionPolicy.dEmployerPart));
        prmdeductionPolicy.Add(new SqlParameter("@EmployeePart", objClsDTOdeductionPolicy.dEmployeePart));
        prmdeductionPolicy.Add(new SqlParameter("@WithEffect", objClsDTOdeductionPolicy.strWithEffect));
        prmdeductionPolicy.Add(new SqlParameter("@RateOnly", objClsDTOdeductionPolicy.intRateOnly));
        prmdeductionPolicy.Add(new SqlParameter("@FixedAmount", objClsDTOdeductionPolicy.decFixedAmount));

        prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionID", objClsDTOdeductionPolicy.intAdditionDeductionID));
       SqlParameter sQlparam =new SqlParameter("@ReturnVal", SqlDbType.Int);
        sQlparam.Direction = ParameterDirection.ReturnValue;
        prmdeductionPolicy.Add(sQlparam);
        iDeductionPolicyId = Convert.ToInt32(MObjClsDataLayer.ExecuteScalar("spPayDeductionPolicyTransaction", prmdeductionPolicy));
        if (iDeductionPolicyId > 0)
        {
            DeletePolicyDetails(iDeductionPolicyId);
            SavePolicyDetails(iDeductionPolicyId);

            return iDeductionPolicyId;
        }
        return iDeductionPolicyId;
        }
        private void DeletePolicyDetails(int iOutPolicyID)
        {
            prmdeductionPolicy = new ArrayList();
            prmdeductionPolicy.Add(new SqlParameter("@Mode", 5));
            prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId", iOutPolicyID));

            MObjClsDataLayer.ExecuteNonQuery("spPayDeductionPolicyDetTransaction", prmdeductionPolicy);
        }
        private void SavePolicyDetails(int iOutPolicyID)
        {
            foreach (clsDTOdeductionPolicyDetails objclsDTOdeductionPolicyDetails in   objClsDTOdeductionPolicy.lstclsDTOdeductionPolicyDetails)
            {
                prmdeductionPolicy = new ArrayList();
                prmdeductionPolicy.Add(new SqlParameter("@Mode", 1));
                prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId", iOutPolicyID));
                prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionID", objclsDTOdeductionPolicyDetails.intDetAddDedID));

                MObjClsDataLayer.ExecuteNonQuery("spPayDeductionPolicyDetTransaction", prmdeductionPolicy);
            }
        }
        // To get the Record Count

        public int RecCountNavigate()
        {

            prmdeductionPolicy = new ArrayList();
            prmdeductionPolicy.Add(new SqlParameter("@Mode", 5));

            return Convert.ToInt32(MObjClsDataLayer.ExecuteScalar("spPayDeductionPolicyTransaction", prmdeductionPolicy));


        }
        public bool Getpolicy(int Rownum)
        {
            prmdeductionPolicy = new ArrayList();

            prmdeductionPolicy.Add(new SqlParameter("@Mode", 3));
            prmdeductionPolicy.Add(new SqlParameter("@RowNumber", Rownum));

            SqlDataReader sdrPolicy = MObjClsDataLayer.ExecuteReader("spPayDeductionPolicyTransaction", prmdeductionPolicy, CommandBehavior.SingleRow);

            if (sdrPolicy.Read())
            {
                objClsDTOdeductionPolicy.intDeductionPolicyId = Convert.ToInt32(sdrPolicy["AdditionDeductionPolicyId"]);
                objClsDTOdeductionPolicy.strPolicyName = Convert.ToString(sdrPolicy["PolicyName"]);
                objClsDTOdeductionPolicy.intAdditionID =sdrPolicy["AdditionID"].ToInt32();
                objClsDTOdeductionPolicy.intParameterId = Convert.ToInt32(sdrPolicy["ParameterId"]);
                objClsDTOdeductionPolicy.decAmountLimit = Convert.ToDecimal(sdrPolicy["AmountLimit"]);
                objClsDTOdeductionPolicy.dEmployeePart = Convert.ToDecimal(sdrPolicy["EmployeePart"]);
                objClsDTOdeductionPolicy.dEmployerPart = Convert.ToDecimal(sdrPolicy["EmployerPart"]);
                objClsDTOdeductionPolicy.intAdditionDeductionID = Convert.ToInt32(sdrPolicy["AdditionDeductionID"]);
                objClsDTOdeductionPolicy.strWithEffect = Convert.ToString(sdrPolicy["WithEffect"]);
                objClsDTOdeductionPolicy.intRateOnly = Convert.ToInt32(sdrPolicy["RateOnly"]);
                objClsDTOdeductionPolicy.decFixedAmount = sdrPolicy["FixedAmount"].ToDecimal();
                //if (sdrPolicy["EmployerPart"] != DBNull.Value)
                //    objclsDTOWorkPolicy.OffDayShiftID = Convert.ToInt32(sdrPolicy["ShiftID"]);
                //else
                //    objclsDTOWorkPolicy.OffDayShiftID = 0;

                sdrPolicy.Close();
                return true;
            }
            else
                sdrPolicy.Close();
            return false;
        }
        public DataTable FillDeductiondetail(int intDeductionID)
        {

            prmdeductionPolicy = new ArrayList();

            prmdeductionPolicy.Add(new SqlParameter("@Mode", 3));
            prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId", intDeductionID));
            prmdeductionPolicy.Add(new SqlParameter("@IsArabicView", 0));
            return MObjClsDataLayer.ExecuteDataTable("spPayDeductionPolicyDetTransaction", prmdeductionPolicy);

        }
        public bool IsExists()
        {
            prmdeductionPolicy = new ArrayList();

            prmdeductionPolicy.Add(new SqlParameter("@Mode", 6));
            prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId", objClsDTOdeductionPolicy.intDeductionPolicyId));

            return (Convert.ToInt32(MObjClsDataLayer.ExecuteScalar("spPayDeductionPolicyTransaction", prmdeductionPolicy)) > 0 ? true : false);
        }
        public bool Delete()
        {
            prmdeductionPolicy = new ArrayList();

            prmdeductionPolicy.Add(new SqlParameter("@Mode", 4));
            prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId", objClsDTOdeductionPolicy.intDeductionPolicyId));

            MObjClsDataLayer.ExecuteNonQuery("spPayDeductionPolicyTransaction", prmdeductionPolicy);
            return true;
        }
        public bool CheckDuplicate(int iPolicyID, string strPolicyName, bool blnAddStatus)
        {
            int intExists = 0;
               prmdeductionPolicy = new ArrayList();
               if (blnAddStatus)
               {
                   prmdeductionPolicy.Add(new SqlParameter("@Mode", 8));
               }
               else
               {
                   prmdeductionPolicy.Add(new SqlParameter("@Mode", 9));
               }
                   prmdeductionPolicy.Add(new SqlParameter("@PolicyName", strPolicyName));
                prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId", iPolicyID));
                 
            intExists = Convert.ToInt32(MObjClsDataLayer.ExecuteScalar("spPayDeductionPolicyTransaction",prmdeductionPolicy));
            return (intExists > 0);
        }
        public DataSet DisplayDeductionPolicy() //Work Policy
        {
            prmdeductionPolicy = new ArrayList();
            prmdeductionPolicy.Add(new SqlParameter("@Mode", 7));
            prmdeductionPolicy.Add(new SqlParameter("@AdditionDeductionPolicyId", objClsDTOdeductionPolicy.intDeductionPolicyId));
            return  MObjClsDataLayer.ExecuteDataSet("spPayDeductionPolicyTransaction", prmdeductionPolicy);

        }
    }
}
