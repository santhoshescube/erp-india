﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using DTO;

namespace DAL
{
    public class clsDALWorkPolicy
    {

        ArrayList prmWorkPolicy; // for setting Sql parameters

        ClsCommonUtility MObjClsCommonUtility;
        DataLayer MObjClsDataLayer;

        SqlDataReader sdrdr;

        public clsDTOWorkPolicy objclsDTOWorkPolicy {get; set;} // DTO Work policy Information Property

        public clsDALWorkPolicy(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MObjClsDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MObjClsDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MObjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }

        public bool CheckDuplication(bool bAddStatus, string[] saValues, int iId, int iType)
        {
            // function for checking duplicate item
            string sField = "WorkPolicyID";
            string sTable = "PayWorkPolicyMaster";
            string sCondition = "";

            if (bAddStatus)
            {
                if (iType == 1)
                    sCondition = "WorkPolicy='" + saValues[0] + "'";//' and CompanyID='" + saValues[1] + "'
               
            }
            else
            {
                if (iType == 1)
                    sCondition = "WorkPolicy='" + saValues[0] + "' and WorkPolicyID <>" + iId + "";//"' and CompanyID='" + saValues[1] + "'"+
              
            }
            return MObjClsCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
        }

        public bool IsExistsAttendanceWithoutProcess(int intPolicyID)
        {
            prmWorkPolicy = new ArrayList();
            prmWorkPolicy.Add(new SqlParameter("@Mode", "CAE"));
            prmWorkPolicy.Add(new SqlParameter("@PolicyID", intPolicyID));
            return MObjClsDataLayer.ExecuteScalar("spPayWorkPolicy", prmWorkPolicy).ToInt32() == 1 ? true : false;

        }
        public bool IsExistsAttendance(int intPolicyID)
        {
            prmWorkPolicy = new ArrayList();
            prmWorkPolicy.Add(new SqlParameter("@Mode", "CPD"));
            prmWorkPolicy.Add(new SqlParameter("@PolicyID", intPolicyID));
            return MObjClsDataLayer.ExecuteScalar("spPayWorkPolicy", prmWorkPolicy).ToInt32() > 0 ? true : false;

        }

        public DataSet GetAllWorkPolicies()
        {
            prmWorkPolicy = new ArrayList();

            prmWorkPolicy.Add(new SqlParameter("@Mode", "GAWP"));

            return MObjClsDataLayer.ExecuteDataSet("spPayWorkPolicy", prmWorkPolicy);
        }

        public DataSet GetPolicyDetails()
        {
            prmWorkPolicy = new ArrayList();

            prmWorkPolicy.Add(new SqlParameter("@Mode", "GPD"));
            prmWorkPolicy.Add(new SqlParameter("@IsArabicView", 0));
            prmWorkPolicy.Add(new SqlParameter("@PolicyID", objclsDTOWorkPolicy.PolicyID));

            return MObjClsDataLayer.ExecuteDataSet("spPayWorkPolicy", prmWorkPolicy);
        }

        public DataSet GetPolicyConsequences()
        {
            prmWorkPolicy = new ArrayList();

            prmWorkPolicy.Add(new SqlParameter("@Mode", "GAPC"));
            prmWorkPolicy.Add(new SqlParameter("@PolicyID", objclsDTOWorkPolicy.PolicyID));

            return MObjClsDataLayer.ExecuteDataSet("spPayWorkPolicy", prmWorkPolicy);
        }

        public bool GetWorkpolicy(int Rownum)
        {
            prmWorkPolicy = new ArrayList();

            prmWorkPolicy.Add(new SqlParameter("@Mode", "GWP"));
            prmWorkPolicy.Add(new SqlParameter("@Rownum", Rownum));

            SqlDataReader sdrPolicy = MObjClsDataLayer.ExecuteReader("spPayWorkPolicy", prmWorkPolicy, CommandBehavior.SingleRow);

            if (sdrPolicy.Read())
            {
                objclsDTOWorkPolicy.PolicyID = Convert.ToInt32(sdrPolicy["WorkPolicyID"]);
                objclsDTOWorkPolicy.Description = Convert.ToString(sdrPolicy["WorkPolicy"]);
                //objclsDTOWorkPolicy.CompanyID = Convert.ToInt32(sdrPolicy["CompanyID"]);
                //objclsDTOWorkPolicy.DefaultPolicy = Convert.ToBoolean(sdrPolicy["DefaultPolicy"]);
                //objclsDTOWorkPolicy.Active = Convert.ToBoolean(sdrPolicy["Active"]);
                if (sdrPolicy["ShiftID"] != DBNull.Value)
                    objclsDTOWorkPolicy.OffDayShiftID = Convert.ToInt32(sdrPolicy["ShiftID"]);
                else
                    objclsDTOWorkPolicy.OffDayShiftID = 0;

                sdrPolicy.Close();
                return true;
            }
            else
                sdrPolicy.Close();
            return false;
        }

        #region RecCountNavigate
        /// <summary>
        /// Get the number of records
        /// </summary>
        /// <returns>Number of records as integer</returns>
        public int RecCountNavigate()
        {          
            prmWorkPolicy = new ArrayList();
            prmWorkPolicy.Add(new SqlParameter("@Mode", "GRC"));
            return Convert.ToInt32(MObjClsDataLayer.ExecuteScalar("spPayWorkPolicy", prmWorkPolicy));
        }
        #endregion RecCountNavigate

        public bool SaveWorkPolicy(bool blnAddStatus)
        {
            int iOutPolicyID = 0;
            prmWorkPolicy = new ArrayList();

            if (blnAddStatus)
                prmWorkPolicy.Add(new SqlParameter("@Mode", "IPM"));
            else
            {             
                prmWorkPolicy.Add(new SqlParameter("@Mode", "UPM"));
            }
            prmWorkPolicy.Add(new SqlParameter("@PolicyID", objclsDTOWorkPolicy.PolicyID ));
            prmWorkPolicy.Add(new SqlParameter("@Description", objclsDTOWorkPolicy.Description ));
            prmWorkPolicy.Add(new SqlParameter("@ShiftID", objclsDTOWorkPolicy.OffDayShiftID));
            //prmWorkPolicy.Add(new SqlParameter("@DefaultPolicy", objclsDTOWorkPolicy.DefaultPolicy));
            //prmWorkPolicy.Add(new SqlParameter("@Active", objclsDTOWorkPolicy.Active ));

            iOutPolicyID =Convert.ToInt32( MObjClsDataLayer.ExecuteScalar("spPayWorkPolicy", prmWorkPolicy));
            if (iOutPolicyID > 0)
            {
                objclsDTOWorkPolicy.PolicyID = iOutPolicyID;
                DeletePolicyDetails(iOutPolicyID);
                SavePolicyDetails(iOutPolicyID);
                SavePolicyConsequences(iOutPolicyID);
                if (!blnAddStatus)
                {
                    SavePolicyHistory(iOutPolicyID);
                }
                return true;
            }
            return false;
            
        }

        private void DeletePolicyDetails(int iOutPolicyID)
        {
            prmWorkPolicy =new ArrayList();
            prmWorkPolicy.Add(new SqlParameter("@Mode", "DPD"));
            prmWorkPolicy.Add(new SqlParameter("@PolicyID", iOutPolicyID));

            MObjClsDataLayer.ExecuteNonQuery("spPayWorkPolicy", prmWorkPolicy);
        }

        private void  SavePolicyDetails(int iOutPolicyID)
        {
            foreach (clsDTOWorkPolicyDetails objclsDTOWorkPolicyDetail in objclsDTOWorkPolicy.lstclsDTOWorkPolicyDetails )
            {
                 prmWorkPolicy =new ArrayList();
                 prmWorkPolicy.Add(new SqlParameter("@Mode", "IPD"));
                 prmWorkPolicy.Add(new SqlParameter("@PolicyID", iOutPolicyID));
                 prmWorkPolicy.Add(new SqlParameter("@DayID", objclsDTOWorkPolicyDetail.DayID ));
                 prmWorkPolicy.Add(new SqlParameter("@ShiftDetailID", objclsDTOWorkPolicyDetail.ShiftID));

                 MObjClsDataLayer.ExecuteNonQuery("spPayWorkPolicy", prmWorkPolicy);
            }
        }

        private void SavePolicyConsequences(int iOutPolicyID)
        {
            foreach (clsDTOWorkPolicyConsequences objclsDTOWorkPolicyConsequences in objclsDTOWorkPolicy.lstclsDTOWorkPolicyConsequences)
            {
                prmWorkPolicy = new ArrayList();
                prmWorkPolicy.Add(new SqlParameter("@Mode", "IPC"));
                prmWorkPolicy.Add(new SqlParameter("@PolicyID", iOutPolicyID));
                prmWorkPolicy.Add(new SqlParameter("@ConsequenceID", objclsDTOWorkPolicyConsequences.ConsequenceID));
                prmWorkPolicy.Add(new SqlParameter("@LOP", objclsDTOWorkPolicyConsequences.LOP));
                prmWorkPolicy.Add(new SqlParameter("@AllowedDaysPerMonth", objclsDTOWorkPolicyConsequences.AllowedDaysPerMonth));
                prmWorkPolicy.Add(new SqlParameter("@Casual", objclsDTOWorkPolicyConsequences.Casual));
                prmWorkPolicy.Add(new SqlParameter("@Amount", objclsDTOWorkPolicyConsequences.Amount));
            
                MObjClsDataLayer.ExecuteNonQuery("spPayWorkPolicy", prmWorkPolicy);
            }
        }

        public bool IsExists()
        {
            prmWorkPolicy = new ArrayList();

            prmWorkPolicy.Add(new SqlParameter("@Mode", "IE"));
            prmWorkPolicy.Add(new SqlParameter("@PolicyID", objclsDTOWorkPolicy.PolicyID));

            return (Convert.ToInt32(MObjClsDataLayer.ExecuteScalar("spPayWorkPolicy", prmWorkPolicy)) > 0 ? true : false);
        }

        public bool Delete()
        {
            prmWorkPolicy = new ArrayList();

            prmWorkPolicy.Add(new SqlParameter("@Mode", "D"));
            prmWorkPolicy.Add(new SqlParameter("@PolicyID", objclsDTOWorkPolicy.PolicyID));

            MObjClsDataLayer.ExecuteNonQuery("spPayWorkPolicy", prmWorkPolicy);
            return true;
        }

        private void SavePolicyHistory(int iOutPolicyID)
        {
            prmWorkPolicy = new ArrayList();
            prmWorkPolicy.Add(new SqlParameter("@Mode", "SPH"));
            prmWorkPolicy.Add(new SqlParameter("@PolicyID", iOutPolicyID));
            MObjClsDataLayer.ExecuteNonQuery("spPayWorkPolicy", prmWorkPolicy);
        }

    }
}
