﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    /*********************************************
      * Author       : Arun
      * Created On   : 10 Apr 2012
      * Purpose      : Encash Policy Database transactions 
      * ******************************************/
    public class clsDALEncashPolicy
    {
        private clsDTOEncashPolicy MobjDTOEncashPolicy = null;
        private string strProcedureName = "spPayEncashPolicy";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALEncashPolicy() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
        public clsDTOEncashPolicy DTOEncashPolicy
        {
            get
            {
                if (this.MobjDTOEncashPolicy == null)
                    this.MobjDTOEncashPolicy = new clsDTOEncashPolicy();

                return this.MobjDTOEncashPolicy;
            }
            set
            {
                this.MobjDTOEncashPolicy = value;
            }
        }

        /// <summary>
        /// Get The RowNumber of given Encash policy
        /// </summary>
        /// <param name="intEncashPolicyID"></param>
        /// <returns></returns>

        public int GetRowNumber(int intEncashPolicyID)
        {
            int intRownum = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 11), new SqlParameter("@EncashPolicyID", intEncashPolicyID) };
            intRownum = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRownum;
        }
        /// <summary>
        /// Get the total record Count
        /// </summary>
        /// <returns></returns>
        public int GetRecordCount()
        {
            int intRecordCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 10) };
            intRecordCount = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRecordCount;
        }
        /// <summary>
        /// Encash Policy master Save
        /// </summary>
        /// <returns></returns>
        public bool EncashPolicyMasterSave()
        {
            bool blnRetValue = false;
            object objEncashPolicyID = 0;
            this.sqlParameters = new List<SqlParameter>();

            if (DTOEncashPolicy.EncashPolicyID > 0)
            {

                this.sqlParameters.Add(new SqlParameter("@Mode", 2));
                this.sqlParameters.Add(new SqlParameter("@EncashPolicyID", this.DTOEncashPolicy.EncashPolicyID));
            }
            else
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", 1));
            }
            this.sqlParameters.Add(new SqlParameter("@EncashPolicy", this.DTOEncashPolicy.EncashPolicy));
            if (DTOEncashPolicy.CalculationID > 0)
                this.sqlParameters.Add(new SqlParameter("@CalculationID", DTOEncashPolicy.CalculationID));
            this.sqlParameters.Add(new SqlParameter("@CalculationPercentage", DTOEncashPolicy.CalculationPercentage));
            this.sqlParameters.Add(new SqlParameter("@IsCompanyBasedOn", DTOEncashPolicy.CompanyBasedOn));
            this.sqlParameters.Add(new SqlParameter("@IsRateOnly", DTOEncashPolicy.IsRateOnly));
            this.sqlParameters.Add(new SqlParameter("@EncashRate", DTOEncashPolicy.EncashRate));
            this.sqlParameters.Add(new SqlParameter("@IsRateBasedOnHour", DTOEncashPolicy.IsRateBasedOnHour));
            this.sqlParameters.Add(new SqlParameter("@HoursPerDay", DTOEncashPolicy.HoursPerDay));
            //this.sqlParameters.Add(new SqlParameter("@IsHourBasedShift", DTOEncashPolicy.IsHourBasedShift));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            this.sqlParameters.Add(objParam);
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out objEncashPolicyID) != 0)
            {
                this.DTOEncashPolicy.EncashPolicyID = (int)objEncashPolicyID;
                blnRetValue = true;
            }
            return blnRetValue;

        }

        /// <summary>
        /// Save SalaryPolicy Detils info Save
        /// </summary>
        /// <returns></returns>
        public bool SalaryPolicyDetailsSave()
        {
            bool blnRetValue = false;
            foreach (clsDTOSalaryPolicyDetailEncash clsDTOEncashSalDet in DTOEncashPolicy.DTOSalaryPolicyDetailEncash)
            {
                this.sqlParameters = new List<SqlParameter>();
                this.sqlParameters.Add(new SqlParameter("@Mode", 7));
                this.sqlParameters.Add(new SqlParameter("@EncashPolicyID", this.DTOEncashPolicy.EncashPolicyID));
                this.sqlParameters.Add(new SqlParameter("@AdditionDeductionID", clsDTOEncashSalDet.AdditionDeductionID));
                this.sqlParameters.Add(new SqlParameter("@PolicyType", clsDTOEncashSalDet.PolicyType));
                if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters) != 0)
                {
                    blnRetValue = true;
                }
            }

            return blnRetValue;
        }

        public DataTable DisplayEncashPolicy(int intRowNumber)
        {
           
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 3),
                new SqlParameter("@RowNumber", intRowNumber) };
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        public DataTable DisplayAddDedDetails(int iPolicyID, int iType)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 9),
                new SqlParameter("@EncashPolicyID", iPolicyID),
                new SqlParameter("@PolicyType", iType),
                new SqlParameter("@IsArabicView", 0)};
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        public bool DeleteEncashPolicy()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",4),
                                    new SqlParameter("@EncashPolicyID", this.DTOEncashPolicy.EncashPolicyID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }
        public bool DeleteEncashPolicyDetails()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",6),
                                    new SqlParameter("@EncashPolicyID", this.DTOEncashPolicy.EncashPolicyID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }

        /// <summary>
        /// Selecting the Addition Deduction refernce
        /// </summary>
        /// <returns></returns>
        public DataTable GetAdditionDeductions()
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 8),
                new SqlParameter("@IsArabicView", 0)};
             return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        public bool CheckDuplicate(int iPolicyID,string strPolicyName)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 12),
                new SqlParameter("@EncashPolicy", strPolicyName),
                new SqlParameter("@EncashPolicyID", iPolicyID)
                 };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);
        }
        public bool PolicyIDExists(int iPolicyID)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 13),
                new SqlParameter("@EncashPolicyID", iPolicyID)
                 };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);

        }
        /// <summary>
        /// 
        /// return datatable  for filling combos
        /// </summary>
        /// <param name="saFieldValues"></param>
        /// <returns></returns>
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }
    }
}
