﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    /*********************************************
      * Author       : Arun
      * Created On   : 10 Apr 2012
      * Purpose      : Holiday Policy Database transactions 
      * ******************************************/
    public class clsDALHolidayPolicy
    {
        private clsDTOHolidayPolicy MobjDTOHolidayPolicy = null;
        private string strProcedureName = "spPayHolidayPolicy";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALHolidayPolicy() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
        public clsDTOHolidayPolicy DTOHolidayPolicy
        {
            get
            {
                if (this.MobjDTOHolidayPolicy == null)
                    this.MobjDTOHolidayPolicy = new clsDTOHolidayPolicy();

                return this.MobjDTOHolidayPolicy;
            }
            set
            {
                this.MobjDTOHolidayPolicy = value;
            }
        }

        /// <summary>
        /// Get The RowNumber of given Holiday policy
        /// </summary>
        /// <param name="intHolidayPolicyID"></param>
        /// <returns></returns>

        public int GetRowNumber(int intHolidayPolicyID)
        {
            int intRownum = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 11), new SqlParameter("@HolidayPolicyID", intHolidayPolicyID) };
            intRownum = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRownum;
        }
        /// <summary>
        /// Get the total record Count
        /// </summary>
        /// <returns></returns>
        public int GetRecordCount()
        {
            int intRecordCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 10) };
            intRecordCount = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRecordCount;
        }
        /// <summary>
        /// Holiday Policy master Save
        /// </summary>
        /// <returns></returns>
        public bool HolidayPolicyMasterSave()
        {
            bool blnRetValue = false;
            object objHolidayPolicyID = 0;
            this.sqlParameters = new List<SqlParameter>();

            if (DTOHolidayPolicy.HolidayPolicyID > 0)
            {

                this.sqlParameters.Add(new SqlParameter("@Mode", 2));
                this.sqlParameters.Add(new SqlParameter("@HolidayPolicyID", this.DTOHolidayPolicy.HolidayPolicyID));
            }
            else
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", 1));
            }
            this.sqlParameters.Add(new SqlParameter("@HolidayPolicy", this.DTOHolidayPolicy.HolidayPolicy));
            if (DTOHolidayPolicy.CalculationID > 0)
                this.sqlParameters.Add(new SqlParameter("@CalculationID", DTOHolidayPolicy.CalculationID));
            this.sqlParameters.Add(new SqlParameter("@CalculationPercentage", DTOHolidayPolicy.CalculationPercentage));
            this.sqlParameters.Add(new SqlParameter("@IsCompanyBasedOn", DTOHolidayPolicy.CompanyBasedOn));
            this.sqlParameters.Add(new SqlParameter("@IsRateOnly", DTOHolidayPolicy.IsRateOnly));
            this.sqlParameters.Add(new SqlParameter("@HolidayRate", DTOHolidayPolicy.HolidayRate));
            this.sqlParameters.Add(new SqlParameter("@IsRateBasedOnHour", DTOHolidayPolicy.IsRateBasedOnHour));
            this.sqlParameters.Add(new SqlParameter("@HoursPerDay", DTOHolidayPolicy.HoursPerDay));
            this.sqlParameters.Add(new SqlParameter("@IsHourBasedShift", DTOHolidayPolicy.IsHourBasedShift));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            this.sqlParameters.Add(objParam);
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out objHolidayPolicyID) != 0)
            {
                this.DTOHolidayPolicy.HolidayPolicyID = (int)objHolidayPolicyID;
                blnRetValue = true;
            }
            return blnRetValue;

        }

        /// <summary>
        /// Save SalaryPolicy Detils info Save
        /// </summary>
        /// <returns></returns>
        public bool SalaryPolicyDetailsSave()
        {
            bool blnRetValue = false;
            foreach (clsDTOSalaryPolicyDetailHoliday clsDTOHolidaySalDet in DTOHolidayPolicy.DTOSalaryPolicyDetailHoliday)
            {
                this.sqlParameters = new List<SqlParameter>();
                this.sqlParameters.Add(new SqlParameter("@Mode", 7));
                this.sqlParameters.Add(new SqlParameter("@HolidayPolicyID", this.DTOHolidayPolicy.HolidayPolicyID));
                this.sqlParameters.Add(new SqlParameter("@AdditionDeductionID", clsDTOHolidaySalDet.AdditionDeductionID));
                this.sqlParameters.Add(new SqlParameter("@PolicyType", clsDTOHolidaySalDet.PolicyType));
                if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters) != 0)
                {
                    blnRetValue = true;
                }
            }

            return blnRetValue;
        }

        public DataTable DisplayHolidayPolicy(int intRowNumber)
        {
           
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 3),
                new SqlParameter("@RowNumber", intRowNumber) };
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        public DataTable DisplayAddDedDetails(int iPolicyID, int iType)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 9),
                new SqlParameter("@HolidayPolicyID", iPolicyID),
                new SqlParameter("@PolicyType", iType),
                new SqlParameter("@IsArabicView", 0)};
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        public bool DeleteHolidayPolicy()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",4),
                                    new SqlParameter("@HolidayPolicyID", this.DTOHolidayPolicy.HolidayPolicyID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }
        public bool DeleteHolidayPolicyDetails()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",6),
                                    new SqlParameter("@HolidayPolicyID", this.DTOHolidayPolicy.HolidayPolicyID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }

        /// <summary>
        /// Selecting the Addition Deduction refernce
        /// </summary>
        /// <returns></returns>
        public DataTable GetAdditionDeductions()
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 8),
                new SqlParameter("@IsArabicView", 0)};
             return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        public bool CheckDuplicate(int iPolicyID,string strPolicyName)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 12),
                new SqlParameter("@HolidayPolicy", strPolicyName),
                new SqlParameter("@HolidayPolicyID", iPolicyID)
                 };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);
        }
        public bool PolicyIDExists(int iPolicyID)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 13),
                new SqlParameter("@HolidayPolicyID", iPolicyID)
                 };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);

        }
        /// <summary>
        /// 
        /// return datatable  for filling combos
        /// </summary>
        /// <param name="saFieldValues"></param>
        /// <returns></returns>
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }
    }
}
