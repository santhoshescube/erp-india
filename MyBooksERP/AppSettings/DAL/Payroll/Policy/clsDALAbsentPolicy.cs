﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    /*********************************************
      * Author       : Arun
      * Created On   : 10 Apr 2012
      * Purpose      : Absent Policy Database transactions 
      * ******************************************/
    public class clsDALAbsentPolicy
    {

        #region Declartions

        private clsDTOAbsentPolicy MobjDTOAbsentPolicy = null;
        private string strProcedureName = "spPayAbsentPolicy";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALAbsentPolicy() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
        public clsDTOAbsentPolicy DTOAbsentPolicy
        {
            get
            {
                if (this.MobjDTOAbsentPolicy == null)
                    this.MobjDTOAbsentPolicy = new clsDTOAbsentPolicy();

                return this.MobjDTOAbsentPolicy;
            }
            set
            {
                this.MobjDTOAbsentPolicy = value;
            }
        }

        #endregion Declartions

        #region GetRowNumber

        /// <summary>
        /// Get The RowNumber of given Absent policy
        /// </summary>
        /// <param name="intAbsentPolicyID"></param>
        /// <returns></returns>


        public int GetRowNumber(int intAbsentPolicyID)
        {
            int intRownum = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 11), new SqlParameter("@AbsentPolicyID", intAbsentPolicyID) };
            intRownum = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRownum;
        }
        #endregion GetRowNumber

        #region GetRecordCount

        /// <summary>
        /// Get the total record Count
        /// </summary>
        /// <returns></returns>
        public int GetRecordCount()
        {
            int intRecordCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 10) };
            intRecordCount = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRecordCount;
        }

        #endregion GetRecordCount

        #region AbsentPolicyMasterSave

        /// <summary>
        /// Absent Policy master Save
        /// </summary>
        /// <returns></returns>
        public bool AbsentPolicyMasterSave()
        {
            bool blnRetValue = false;
            object objAbsentPolicyID = 0;
            this.sqlParameters = new List<SqlParameter>();

            if (DTOAbsentPolicy.AbsentPolicyID > 0)
            {

                this.sqlParameters.Add(new SqlParameter("@Mode", 2));
                this.sqlParameters.Add(new SqlParameter("@AbsentPolicyID", this.DTOAbsentPolicy.AbsentPolicyID));
            }
            else
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", 1));
            }
            		
		    int iIsMergeBoth= this.DTOAbsentPolicy.IsMergeBoth?1:0;
            this.sqlParameters.Add(new SqlParameter("@AbsentPolicy", this.DTOAbsentPolicy.AbsentPolicy));
            this.sqlParameters.Add(new SqlParameter("@IsMergeBoth", iIsMergeBoth));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            this.sqlParameters.Add(objParam);
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out objAbsentPolicyID) != 0)
            {
                this.DTOAbsentPolicy.AbsentPolicyID = (int)objAbsentPolicyID;
                blnRetValue = true;
            }
            return blnRetValue;

        }
        #endregion AbsentPolicyMasterSave

        #region AbsentPolicyDetailSave

        /// <summary>
        /// Details Save
        /// </summary>
        /// <returns></returns>
        public bool AbsentPolicyDetailSave()
        {
            bool blnRetValue = false;

            foreach (clsDTOAbsentPolicyDeteails clsDTOAbsentDet in DTOAbsentPolicy.DTOAbsentPolicyDeteails)
            {
                this.sqlParameters = new List<SqlParameter>();
                this.sqlParameters.Add(new SqlParameter("@Mode", 5));
                this.sqlParameters.Add(new SqlParameter("@AbsentPolicyID", this.DTOAbsentPolicy.AbsentPolicyID));
                if (clsDTOAbsentDet.CalculationID > 0)
                    this.sqlParameters.Add(new SqlParameter("@CalculationID", clsDTOAbsentDet.CalculationID));
                this.sqlParameters.Add(new SqlParameter("@CompanyBasedOn", clsDTOAbsentDet.CompanyBasedOn));
                this.sqlParameters.Add(new SqlParameter("@IsRateOnly", clsDTOAbsentDet.IsRateOnly));
                this.sqlParameters.Add(new SqlParameter("@AbsentRate", clsDTOAbsentDet.AbsentRate));
                this.sqlParameters.Add(new SqlParameter("@IsRateBasedOnHour", clsDTOAbsentDet.IsRateBasedOnHour));
                this.sqlParameters.Add(new SqlParameter("@HoursPerDay", clsDTOAbsentDet.HoursPerDay));
                this.sqlParameters.Add(new SqlParameter("@AbsentType", clsDTOAbsentDet.AbsentType));

                if(clsDTOAbsentDet.AbsentType == (int)AbsentType.Absent)
                    this.sqlParameters.Add(new SqlParameter("@PolicyType",1));
                else
                    this.sqlParameters.Add(new SqlParameter("@PolicyType", 2));

                if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters) != 0)
                {
                    blnRetValue = true;
                }
            }

            return blnRetValue;
        }

        #endregion AbsentPolicyDetailSave

        #region SalaryPolicyDetailsSave

        /// <summary>
        /// Save SalaryPolicy Detils info Save
        /// </summary>
        /// <returns></returns>
        public bool SalaryPolicyDetailsSave()
        {
            bool blnRetValue = false;
            foreach (clsDTOSalaryPolicyDetailAbsent clsDTOAbsentSalDet in DTOAbsentPolicy.DTOSalaryPolicyDetailAbsent)
            {
                this.sqlParameters = new List<SqlParameter>();
                this.sqlParameters.Add(new SqlParameter("@Mode", 7));
                this.sqlParameters.Add(new SqlParameter("@AbsentPolicyID", this.DTOAbsentPolicy.AbsentPolicyID));
                this.sqlParameters.Add(new SqlParameter("@AdditionDeductionID", clsDTOAbsentSalDet.AdditionDeductionID));
                this.sqlParameters.Add(new SqlParameter("@PolicyType", clsDTOAbsentSalDet.PolicyType));
                if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters) != 0)
                {
                    blnRetValue = true;
                }
            }

            return blnRetValue;
        }

        #endregion SalaryPolicyDetailsSave

        #region DisplayAbsentPolicy
        public DataTable DisplayAbsentPolicy(int intRowNumber)
        {

            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 3),
                new SqlParameter("@RowNumber", intRowNumber) };
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        #endregion DisplayAbsentPolicy

        #region DisplayAddDedDetails
        public DataTable DisplayAddDedDetails(int iPolicyID, int iType)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 9),
                new SqlParameter("@AbsentPolicyID", iPolicyID),
                new SqlParameter("@PolicyType", iType) };
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        #endregion DisplayAddDedDetails

        #region DeleteAbsentPolicy
        public bool DeleteAbsentPolicy()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",4),
                                    new SqlParameter("@AbsentPolicyID", this.DTOAbsentPolicy.AbsentPolicyID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }
        #endregion DeleteAbsentPolicy

        #region DeleteAbsentPolicyDetails
        public bool DeleteAbsentPolicyDetails()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",6),
                                    new SqlParameter("@AbsentPolicyID", this.DTOAbsentPolicy.AbsentPolicyID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }
        #endregion DeleteAbsentPolicyDetails

        #region GetAdditionDeductions
   
        /// <summary>
        /// Selecting the Addition Deduction refernce
        /// </summary>
        /// <returns></returns>
        public DataTable GetAdditionDeductions()
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 8) };
             return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }

        #endregion GetAdditionDeductions

        #region CheckDuplicate
        public bool CheckDuplicate(int iPolicyID, string strPolicyName)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 12),
                new SqlParameter("@AbsentPolicy", strPolicyName),
                new SqlParameter("@AbsentPolicyID", iPolicyID)
                 };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);
        }
        #endregion CheckDuplicate

        #region PolicyIDExists

        public bool PolicyIDExists(int iPolicyID)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 13),
                new SqlParameter("@AbsentPolicyID", iPolicyID)
                 };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);

        }
        #endregion PolicyIDExists

        #region FillCombos

        /// <summary>
        /// 
        /// return datatable  for filling combos
        /// </summary>
        /// <param name="saFieldValues"></param>
        /// <returns></returns>
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }
        #endregion FillCombos
    }
}
