﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    /*********************************************
     * Author       : Ranju Mathew
     * Created On   : 12 Aug 2013
     * Purpose      : Unearned Policy Database transactions 
     * ******************************************/

 public class clsDALUnearnedPolicy
 {
       #region Declaration
    
        private clsDTOUnearnedPolicy MobjDTOUnearnedPolicy = null;
        private string strProcedureName = "spPayUnearnedPolicy";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALUnearnedPolicy() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
        public clsDTOUnearnedPolicy DTOUnearnedPolicy
        {
            get
            {
                if (this.MobjDTOUnearnedPolicy == null)
                    this.MobjDTOUnearnedPolicy = new clsDTOUnearnedPolicy();

                return this.MobjDTOUnearnedPolicy;
            }
            set
            {
                this.MobjDTOUnearnedPolicy = value;
            }
        }
        
        #endregion Declaration

       #region Declaration
        #endregion Declaration

       #region GetRowNumber
        public int GetRowNumber(int IntUnearnedID)
        {
            int intRownum = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 11), new SqlParameter("@UnearnedPolicyID ", IntUnearnedID) };
            intRownum = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRownum;
        }
        #endregion GetRowNumber

       #region GetRecordCount
       
        /// <summary>
        /// Get the total record Count
        /// </summary>
        /// <returns></returns>
        public int GetRecordCount()
        {
            int intRecordCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 10) };
            intRecordCount = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRecordCount;
        }
        #endregion GetRecordCount

       #region UnearnedPolicyMasterSave
 

        /// <summary>
        /// Unearned Policy master Save
        /// </summary>
        /// <returns></returns>
        /// 
        public bool UnearnedPolicyMasterSave()
        {
            bool blnRetValue = false;
            object objUnearnedPolicyID = 0;
            this.sqlParameters = new List<SqlParameter>();

            if (DTOUnearnedPolicy.UnearnedPolicyID > 0)
            {

                this.sqlParameters.Add(new SqlParameter("@Mode", 2));
                this.sqlParameters.Add(new SqlParameter("@UnearnedPolicyID", this.DTOUnearnedPolicy.UnearnedPolicyID));
            }
            else
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", 1));
            }


            this.sqlParameters.Add(new SqlParameter("@UnearnedPolicy", this.DTOUnearnedPolicy.UnearnedPolicy));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            this.sqlParameters.Add(objParam);
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out objUnearnedPolicyID) != 0)
            {
                this.DTOUnearnedPolicy.UnearnedPolicyID = (int)objUnearnedPolicyID;
                blnRetValue = true;
            }
            return blnRetValue;

        }
        #endregion Declaration

       #region UnearnedPolicyDetailSave
     
        /// <summary>
        /// Details Save
        /// </summary>
        /// <returns></returns>
        public bool UnearnedPolicyDetailSave()
        {
            bool blnRetValue = false;

            foreach (clsDTOUnearnedPolicyDetails clsDTOUnearnedDet in DTOUnearnedPolicy.DTOUnearnedPolicyDetails)
            {
                this.sqlParameters = new List<SqlParameter>();
                this.sqlParameters.Add(new SqlParameter("@Mode", 5));
                this.sqlParameters.Add(new SqlParameter("@UnearnedPolicyID", this.DTOUnearnedPolicy.UnearnedPolicyID));
                if (clsDTOUnearnedDet.CalculationID > 0)
                    this.sqlParameters.Add(new SqlParameter("@CalculationID", clsDTOUnearnedDet.CalculationID));
                this.sqlParameters.Add(new SqlParameter("@CompanyBasedOn", clsDTOUnearnedDet.CompanyBasedOn));
                this.sqlParameters.Add(new SqlParameter("@IsRateOnly", clsDTOUnearnedDet.IsRateOnly));
                this.sqlParameters.Add(new SqlParameter("@UnearnedRate", clsDTOUnearnedDet.UnearnedRate));
                this.sqlParameters.Add(new SqlParameter("@IsRateBasedOnHour", clsDTOUnearnedDet.IsRateBasedOnHour));
                this.sqlParameters.Add(new SqlParameter("@HoursPerDay", clsDTOUnearnedDet.HoursPerDay));
               
                if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters) != 0)
                {
                    blnRetValue = true;
                }
            }

            return blnRetValue;
        }
        #endregion UnearnedPolicyDetailSave

       #region SalaryPolicyDetailsSave

       
        /// <summary>
        /// Save SalaryPolicy Detils info Save
        /// </summary>
        /// <returns></returns>
        public bool SalaryPolicyDetailsSave()
        {
            bool blnRetValue = false;
            foreach (  clsDTOSalaryPolicyUNDetails clsDTOUnearnedSalDet in DTOUnearnedPolicy.DTOSalaryPolicyDetail)
            {
                this.sqlParameters = new List<SqlParameter>();
                this.sqlParameters.Add(new SqlParameter("@Mode", 7));
                this.sqlParameters.Add(new SqlParameter("@UnearnedPolicyID", this.DTOUnearnedPolicy.UnearnedPolicyID));
                this.sqlParameters.Add(new SqlParameter("@AdditionDeductionID", clsDTOUnearnedSalDet.AdditionDeductionID));
                this.sqlParameters.Add(new SqlParameter("@PolicyType", clsDTOUnearnedSalDet.PolicyType));
                if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters) != 0)
                {
                    blnRetValue = true;
                }
            }

            return blnRetValue;
        }
        #endregion SalaryPolicyDetailsSave

       #region DisplayUnearnedPolicy
        public DataTable DisplayUnearnedPolicy(int intRowNumber)
        {

            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 3),
                new SqlParameter("@RowNumber", intRowNumber)};
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        #endregion DisplayUnearnedPolicy

       #region DisplayAddDedDetails
        public DataTable DisplayAddDedDetails(int iPolicyID, int iType)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 9),
                new SqlParameter("@UnearnedPolicyID", iPolicyID),
                new SqlParameter("@PolicyType", iType),
                new SqlParameter("@IsArabicView", 0)};
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        #endregion DisplayAddDedDetails

       #region DeleteUnearnedPolicy
        public bool DeleteUnearnedPolicy()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",4),
                                    new SqlParameter("@UnearnedPolicyID", this.DTOUnearnedPolicy.UnearnedPolicyID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }
        #endregion DeleteUnearnedPolicy

       #region DeleteUnearnedPolicyDetails
        public bool DeleteUnearnedPolicyDetails()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",6),
                                    new SqlParameter("@UnearnedPolicyID", this.DTOUnearnedPolicy.UnearnedPolicyID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }
        #endregion DeleteUnearnedPolicyDetails

       #region GetAdditionDeductions

        /// <summary>
        /// Selecting the Addition Deduction refernce
        /// </summary>
        /// <returns></returns>
        public DataTable GetAdditionDeductions()
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 8),
                new SqlParameter("@IsArabicView", 0)};
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        #endregion GetAdditionDeductions

       #region CheckDuplicate
        public bool CheckDuplicate(int iPolicyID, string strPolicyName)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 12),
                new SqlParameter("@UnearnedPolicy", strPolicyName),
                new SqlParameter("@UnearnedPolicyID", iPolicyID)
                 };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);
        }
        #endregion CheckDuplicate

       #region PolicyIDExists
        public bool PolicyIDExists(int iPolicyID)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 13),
                new SqlParameter("@UnearnedPolicyID", iPolicyID)
                 };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);

        }
        #endregion PolicyIDExists

       #region FillCombos
      
        /// <summary>
        /// 
        /// return datatable  for filling combos
        /// </summary>
        /// <param name="saFieldValues"></param>
        /// <returns></returns>
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }
        #endregion FillCombos
 }
}
