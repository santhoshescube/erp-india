﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace MyBooksERP
{
    /*********************************************
      * Author         : Arun
      * Created On     : 11 Apr 2012
      * Purpose        : Holiday Calendar Database Transactions 
      * Modified by    : Siny
      * Modified date  : 16 Aug 2013
      * Description    : Tuning performance improvement
      * ******************************************/

    public class clsDALHolidayCalendar
    {
         private clsDTOHolidayCalendar MobjDTOHolidayCalendar = null;
        private string strProcedureName = "spPayHolidayCalendar";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALHolidayCalendar() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }

        public clsDTOHolidayCalendar DTOHolidayCalendar
        {
            get
            {
                if (this.MobjDTOHolidayCalendar == null)
                    this.MobjDTOHolidayCalendar = new clsDTOHolidayCalendar();

                return this.MobjDTOHolidayCalendar;
            }
            set
            {
                this.MobjDTOHolidayCalendar = value;
            }
        }

        #region InsertHoiday
        /// <summary>
        /// insert holiday details
        /// </summary>
        /// <returns>success/failure</returns>
        public bool InsertHoiday()
        {

            object objHoildayID = 0;
            this.sqlParameters = new List<SqlParameter>();
            this.sqlParameters.Add(new SqlParameter("@Mode", 1));
            this.sqlParameters.Add(new SqlParameter("@CompanyID", this.DTOHolidayCalendar.CompanyID));
            if (this.DTOHolidayCalendar.HolidayTypeID > 0)
                this.sqlParameters.Add(new SqlParameter("@HolidayTypeID", this.DTOHolidayCalendar.HolidayTypeID));
            this.sqlParameters.Add(new SqlParameter("@HolidayDate", this.DTOHolidayCalendar.HolidayDate));
            this.sqlParameters.Add(new SqlParameter("@Remarks", this.DTOHolidayCalendar.Remarks));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            this.sqlParameters.Add(objParam);
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out objHoildayID) != 0)
            {
                this.DTOHolidayCalendar.HolidayID = (int)objHoildayID;

            }
            {
                this.DTOHolidayCalendar.HolidayID = 0;
            }
            return (this.DTOHolidayCalendar.HolidayID > 0);

        }
        #endregion InsertHoiday

        #region UpdateHoliday
        /// <summary>
        /// Update holiday details
        /// </summary>
        /// <param name="iCmpID">companyid</param>
        /// <param name="strDate">date</param>
        /// <param name="iHolidayType">holiday type</param>
        /// <param name="strRemarks">remarks</param>
        /// <returns>success/failure</returns>
        public bool UpdateHoliday(int iCmpID, string strDate,int iHolidayType,string strRemarks)
        {

            object objHoildayID = 0;
            this.sqlParameters = new List<SqlParameter>();
            this.sqlParameters.Add(new SqlParameter("@Mode", 2));
            if (iHolidayType > 0)
                this.sqlParameters.Add(new SqlParameter("@HolidayTypeID", iHolidayType));
            this.sqlParameters.Add(new SqlParameter("@Remarks", strRemarks));
            this.sqlParameters.Add(new SqlParameter("@HolidayDate", strDate));
            this.sqlParameters.Add(new SqlParameter("@CompanyID", iCmpID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            this.sqlParameters.Add(objParam);
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out objHoildayID) != 0)
            {}
            return (objHoildayID.ToInt32() > 0);
        }
        #endregion UpdateHoliday

        #region DeleteHoliday
        /// <summary>
        /// Delete holiday details
        /// </summary>
        /// <param name="iCompID">companyID</param>
        /// <param name="strDate">date</param>
        /// <returns></returns>
        public bool DeleteHoliday(int iCompID,string strDate)
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",3),
                                    new SqlParameter("@CompanyID", iCompID),
                                    new SqlParameter("@HolidayDate", strDate)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }
        #endregion DeleteHoliday

        #region GetMonths
        /// <summary>
        /// Get months
        /// </summary>
        /// <param name="strYear">year</param>
        /// <returns>datatable of months</returns>
        public DataTable GetMonths(string strYear)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 4),
                new SqlParameter("@Year", strYear),
                new SqlParameter("@IsArabicView", 0)};
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        #endregion GetMonths

        #region GetCompanyOffDay
        /// <summary>
        /// Get company off days
        /// </summary>
        /// <param name="intCmpID">companyid</param>
        /// <returns>string array of offdays</returns>
        public string[] GetCompanyOffDay(int intCmpID)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 5),
                new SqlParameter("@CompanyID", intCmpID)
                 };
            string[] strOffDayID = Convert.ToString(this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters)).Split(',');
            return strOffDayID;
        }
        #endregion GetCompanyOffDay

        /// <summary>
        /// get ll the details of company holiday of a specific day
        /// </summary>
        /// <param name="intCmpID">companyid</param>
        /// <param name="strDate">date</param>
        /// <param name="strRemarks">returns remarks </param>
        /// <param name="strColor">returns color name</param>
        /// <param name="intHolidayType">returns holiday type</param>
        /// <returns>holiday details exists or not</returns>
        public bool GetCompanyHolidayDetails(int intCmpID, string strDate, ref string strRemarks, ref string strColor, ref int intHolidayType)
        {
            bool blnRetValue = false;
                       
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 6),
                new SqlParameter("@CompanyID", intCmpID),
                new SqlParameter("@HolidayDate", strDate)
                 };
            SqlDataReader sdrHoliday = this.DataLayer.ExecuteReader(this.strProcedureName, this.sqlParameters);
            if (sdrHoliday.Read())
            {
                strRemarks = sdrHoliday["Remarks"].ToStringCustom();
                strColor = sdrHoliday["Color"].ToStringCustom();
                intHolidayType = sdrHoliday["HolidayTypeID"].ToInt32();
                blnRetValue = true;
            }
            sdrHoliday.Close();

            return blnRetValue;
        }

        /// <summary>
        /// Check if holiday exixts
        /// </summary>
        /// <param name="intCmpID">companyid</param>
        /// <param name="strDate">date</param>
        /// <returns>holiday exists/not</returns>
        public bool CheckHolidayExists(int intCmpID,string strDate)
        {
            int intCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 7),
                new SqlParameter("@CompanyID", intCmpID),
               new SqlParameter("@HolidayDate", strDate)
                 };
            intCount = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intCount>0);

        }

        /// <summary>
        /// Check if payment for that month exists
        /// </summary>
        /// <param name="intCmpID">companyid</param>
        /// <param name="strDate">date</param>
        /// <returns>payment exists/not</returns>
        public bool CheckPaymentExists(int intCmpID, string strDate)
        {
            int intCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 9),
                new SqlParameter("@CompanyID", intCmpID),
               new SqlParameter("@HolidayDate", strDate)
                 };
            intCount = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intCount > 0);

        }

        /// <summary>
        /// Get all holiday types
        /// </summary>
        /// <returns>datatable of holidays</returns>
        public DataTable GetHolidayTypes()
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 8)
                 };
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }

        /// <summary>
        /// Get Holiday Type Details
        /// </summary>
        /// <param name="strHolidayType">holiday type</param>
        /// <param name="strColr">returns color name</param>
        /// <param name="intHolidayTypeID">returns typeid</param>
        /// <returns></returns>
        public bool GetHolidayTypeDetails(string strHolidayType,ref string strColr,ref int intHolidayTypeID)
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 10),
                new SqlParameter("@HolidayType", strHolidayType) 
                 };
            SqlDataReader sdrHoliday = this.DataLayer.ExecuteReader(this.strProcedureName, sqlParameters);
            if (sdrHoliday.Read())
            {
                strColr = sdrHoliday["Color"].ToStringCustom();
                intHolidayTypeID = sdrHoliday["HolidayTypeID"].ToInt32();
                blnRetValue = true;
            }
            sdrHoliday.Close();
            return blnRetValue;

        }
        /// <summary>
        /// return datatable  for filling combos
        /// </summary>
        /// <param name="saFieldValues"></param>
        /// <returns></returns>
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

    }
}
