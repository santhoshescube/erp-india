﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    /*********************************************
      * Author       : Arun
      * Created On   : 5 Apr 2012
      * Purpose      : Leave Policy Database Transactions 
      * ******************************************/
   public  class clsDALLeavePolicy
    {

        private clsDTOLeavePolicy MobjDTOLeavePolicy = null;
        private string strProcedureName = "spPayLeavePolicy";
        private List<SqlParameter> sqlParameters = null;

       
        DataLayer objDataLayer = null;
        /// <summary>
       /// Constructor
       /// </summary>
       
        public clsDALLeavePolicy() {}
        public DataLayer DataLayer
        {
            get
            {
                if (this.objDataLayer == null)
                    this.objDataLayer = new DataLayer();

                return this.objDataLayer;
            }
            set
            {
                this.objDataLayer = value;
            }
        }
        public clsDTOLeavePolicy DTOLeavePolicy
        {
            get
            {
                if (this.MobjDTOLeavePolicy == null)
                    this.MobjDTOLeavePolicy = new clsDTOLeavePolicy();

                return this.MobjDTOLeavePolicy;
            }
            set
            {
                this.MobjDTOLeavePolicy = value;
            }
        }

        /// <summary>
        /// Get Row Number
        /// </summary>
        /// <param name="intLeavePolicyID">Leave policyID as int</param>
        /// <returns>row number</returns>
        public int GetRowNumber(int intLeavePolicyID)
       {
            int intRownum =0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 12), new SqlParameter("@LeavePolicyID", intLeavePolicyID) };
            intRownum = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRownum;         
       }

        /// <summary>
        /// Get total record count
        /// </summary>
        /// <returns>record count</returns>
        public int GetRecordCount()
        {
            int intRecordCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 11) };
            intRecordCount = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRecordCount;
        }

        /// <summary>
        /// Display master details
        /// </summary>
        /// <param name="intRowNumber">row number</param>
        /// <returns>whether details exists as bool</returns>
       public bool DisplayLeavePolicyMaster(int intRowNumber)
       {
           bool blnRetValue = false;
           this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 3),
                new SqlParameter("@RowNumber", intRowNumber) };
           SqlDataReader sdr = this.DataLayer.ExecuteReader(this.strProcedureName, this.sqlParameters);
           if (sdr.Read())
           {
               DTOLeavePolicy.LeavePolicyID = sdr["LeavePolicyID"].ToInt32();
               DTOLeavePolicy.LeavePolicyName = sdr["LeavePolicyName"].ToStringCustom();
               DTOLeavePolicy.ApplicableFrom = sdr["ApplicableFrom"].ToStringCustom();
               blnRetValue = true;
           }
           sdr.Close();
           return blnRetValue;
       }

       /// <summary>
       /// Save policy details
       /// </summary>
       /// <returns>success/failure</returns>
       public bool LeavePolicyMasterSave()
       {
           bool blnRetValue = false;
           object Leavepolicyid=0 ;
           this.sqlParameters = new List<SqlParameter>();

           if (DTOLeavePolicy.LeavePolicyID > 0)
           {

               this.sqlParameters.Add(new SqlParameter("@Mode", 2));
               this.sqlParameters.Add(new SqlParameter("@LeavePolicyID", DTOLeavePolicy.LeavePolicyID));
           }
           else
           {
               this.sqlParameters.Add(new SqlParameter("@Mode", 1));
           }
           
           this.sqlParameters.Add(new SqlParameter("@CompanyID", this.DTOLeavePolicy.CompanyID));
           this.sqlParameters.Add(new SqlParameter("@LeavePolicyName", this.DTOLeavePolicy.LeavePolicyName));
           this.sqlParameters.Add(new SqlParameter("@ApplicableFrom", this.DTOLeavePolicy.ApplicableFrom));
           SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
           objParam.Direction = ParameterDirection.ReturnValue;
           this.sqlParameters.Add(objParam);
           if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out Leavepolicyid)!=0)
           {
               DTOLeavePolicy.LeavePolicyID = (int)Leavepolicyid;
               blnRetValue = true;
           }
           return blnRetValue;
           
       }
       public bool LeavePolicyDetailsSave()
       {
           bool blnRetValue = false;
           
           foreach (clsDTOLeavePolicyDetail clsDtoDetail in DTOLeavePolicy.DTOLeavePolicyDetail)
           {
               this.sqlParameters = new List<SqlParameter>();
               if (clsDtoDetail.SerialNo > 0)
               {
                   this.sqlParameters.Add(new SqlParameter("@Mode", 6));
                   this.sqlParameters.Add(new SqlParameter("@SerialNo", clsDtoDetail.SerialNo));
               }
               else
               {
                   this.sqlParameters.Add(new SqlParameter("@Mode", 5));
               }

               this.sqlParameters.Add(new SqlParameter("@LeavePolicyID", this.DTOLeavePolicy.LeavePolicyID));
               this.sqlParameters.Add(new SqlParameter("@LeaveTypeID", clsDtoDetail.LeaveTypeID));
               this.sqlParameters.Add(new SqlParameter("@NoOfLeave", clsDtoDetail.NoOfLeave));
               this.sqlParameters.Add(new SqlParameter("@MonthLeave", clsDtoDetail.MonthLeave));
               this.sqlParameters.Add(new SqlParameter("@CarryForwardLeave", clsDtoDetail.CarryForwardLeave));
               this.sqlParameters.Add(new SqlParameter("@EncashDays", clsDtoDetail.EncashDays));
               if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters) != 0)
               {
                   blnRetValue = true;
               }
           }
           
           return blnRetValue;

       }
       public bool LeavePolicyConsequenceSave()
       {
           bool blnRetValue = false;

           foreach (clsDTOLeavePolicyConsequenceDetail clsDtoConseq in DTOLeavePolicy.DTOLeavePolicyConsequenceDetail)
           {
               this.sqlParameters = new List<SqlParameter>();
               if (clsDtoConseq.SerialNo > 0)
               {
                   this.sqlParameters.Add(new SqlParameter("@Mode",9));
                   this.sqlParameters.Add(new SqlParameter("@SerialNo", clsDtoConseq.SerialNo));
               }
               else 
               {
                   this.sqlParameters.Add(new SqlParameter("@Mode", 8));
               }
               this.sqlParameters.Add(new SqlParameter("@LeavePolicyID", this.DTOLeavePolicy.LeavePolicyID));
               this.sqlParameters.Add(new SqlParameter("@LeaveTypeID", clsDtoConseq.LeaveTypeID));
               this.sqlParameters.Add(new SqlParameter("@CalculationID", clsDtoConseq.CalculationID));
               this.sqlParameters.Add(new SqlParameter("@CalculationPercentage", clsDtoConseq.CalculationPercentage));
               this.sqlParameters.Add(new SqlParameter("@ApplicableDays", clsDtoConseq.ApplicableDays));
               this.sqlParameters.Add(new SqlParameter("@OrderNo", clsDtoConseq.OrderNo));
               if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters) != 0)
               {
                   blnRetValue = true;
               }
           }

           return blnRetValue;

       }

       /// <summary>
       /// Delete policy
       /// </summary>
       /// <returns>success/failure</returns>
       public bool DeleteLeavePolicy()
       {
           bool blnRetValue = false;
           this.sqlParameters=new List<SqlParameter>{new SqlParameter("@Mode",4),
                                    new SqlParameter("@LeavePolicyID", this.DTOLeavePolicy.LeavePolicyID)};
           if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
           {
               blnRetValue = true;
           }
           return blnRetValue; 
       }

       /// <summary>
       /// Get policy details
       /// </summary>
       /// <param name="intLeavePolicyID">policy id</param>
       /// <returns>datatable of details</returns>
       public DataTable DispalyPolicyDetail(int intLeavePolicyID)
       {          
           this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 7),
                new SqlParameter("@LeavePolicyID", intLeavePolicyID)};
           return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
       }
       public DataTable DispalyPolicyConsequenceDetail(int intLeavePolicyID)
       {
           this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 10),
                new SqlParameter("@LeavePolicyID", intLeavePolicyID)};
           return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
       }

       /// <summary>
       /// Check if duplicate name exists
       /// </summary>
       /// <param name="iPolicyID">policy id</param>
       /// <param name="strPolicyName">policy name</param>
       /// <returns>duplicate exists/not</returns>
       public bool CheckDuplicatePolicyName(int iPolicyID, string strPolicyName)
       {
           int intExists = 0;
           this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 13),
                new SqlParameter("@LeavePolicyID", iPolicyID),
                new SqlParameter("@LeavePolicyName", strPolicyName),
                    };
           intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
           return (intExists > 0);

       }

       /// <summary>
       /// Check if salary is processed against a policyid
       /// </summary>
       /// <param name="iPolicyID">policy id</param>
       /// <returns>Salary Processed/not</returns>
       public bool CheckSalaryProcessed(int iPolicyID)
       {
           int intExists = 0;
           this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 14),
                new SqlParameter("@LeavePolicyID", iPolicyID),
                };
           intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
           return (intExists > 0);

       }

       /// <summary>
       /// check if policy updation is permitted /not
       /// </summary>
       /// <param name="iPolicyID">policyid</param>
       /// <param name="iCmpID">companyid</param>
       /// <param name="ileaveTypeID">leavetypeid</param>
       /// <param name="EnCashDays">encashdays</param>
       /// <param name="CarryForwardDays">carryforward days</param>
       /// <param name="balLeaves">balance day</param>
       /// <param name="monthleave">monthly leave</param>
       /// <returns></returns>
       public bool CheckPolicyUpdation (int iPolicyID,int iCmpID,int ileaveTypeID,decimal EnCashDays,decimal CarryForwardDays,decimal balLeaves,decimal monthleave)
       {
           int intExists = 0;
           this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 15),
                new SqlParameter("@CompanyID", iCmpID),
                new SqlParameter("@LeaveTypeID", ileaveTypeID),
                new SqlParameter("@LeavePolicyID", iPolicyID),
                new SqlParameter("@BalanceLeaves", balLeaves),
                new SqlParameter("@EncashLeave", EnCashDays),
                new SqlParameter("@CarryForwardLeave", CarryForwardDays),
                new SqlParameter("@MonthLeave", monthleave),
                };
           intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
           return (intExists > 0);

       }

       /// <summary>
       /// Checks if policy is used by an employee
       /// </summary>
       /// <param name="iPolicyID">policy id</param>
       /// <returns>yes/no</returns>
       public bool PolicyIDExists(int iPolicyID)
       {
           int intExists = 0;
           this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 16),
               new SqlParameter("@LeavePolicyID", iPolicyID)
                };
           intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
           return (intExists > 0);

       }

       /// <summary>
       /// Get days allotted for a leavetype in a policy
       /// </summary>
       /// <param name="iPolicyID">policyid</param>
       /// <param name="iLeaveTypeid">leavetypeid</param>
       /// <returns>number of days</returns>
       public int GetNoOfDays(int iPolicyID,int iLeaveTypeid)
       {
           int intExists = 0;
           this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 17),
               new SqlParameter("@LeavePolicyID", iPolicyID),
               new SqlParameter("@LeaveTypeID", iLeaveTypeid)
                };
           intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
           return intExists;
               
       }
       public int GetMinDiff(int iPolicyID, int iLeaveTypeid)
       {
           int intExists = 0;
           this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 18),
               new SqlParameter("@LeavePolicyID", iPolicyID),
               new SqlParameter("@LeaveTypeID", iLeaveTypeid)
                };
           intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
           return intExists;

       }
       public int DeletLeavePolicyDetails(int iPolicyID, int iSerialNo)
       {
           int intExists = 0;
           this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 19),
               new SqlParameter("@LeavePolicyID", iPolicyID),
               new SqlParameter("@SerialNo", iSerialNo)
                };
           intExists = this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters).ToInt32();
           return intExists;

       }
       public int DeletLeaveConseqDetails(int iPolicyID, int iSerialNo)
       {
           int intExists = 0;
           this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 21),
               new SqlParameter("@LeavePolicyID", iPolicyID),
               new SqlParameter("@SerialNo", iSerialNo)
                };
           intExists = this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters).ToInt32();
           return intExists;

       }

       /// <summary>
       /// Fill all the combos
       /// </summary>
       /// <param name="saFieldValues">query</param>
       /// <returns>datatable of details</returns>
           public DataTable FillCombos(string[] saFieldValues)
       {
           if (saFieldValues.Length == 3)
           {
               using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
               {
                   objCommonUtility.PobjDataLayer = DataLayer;
                   return objCommonUtility.FillCombos(saFieldValues);
               }
           }
           return null;
       }
   
   }
}
