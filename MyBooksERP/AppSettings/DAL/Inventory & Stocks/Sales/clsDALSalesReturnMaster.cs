﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;

using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALSalesReturnMaster : IDisposable
    {
        ArrayList alParameters;
        DataTable dat;
        public clsDTOSalesReturnMaster objclsDTOSalesReturnMaster { get; set; }
        public DataLayer objclsConnection;

        public clsDALSalesReturnMaster(DataLayer objDataLayer)
        {
            objclsConnection = objDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public int GetLastReturnNo(int intType,int intCompanyID)
        {
            int intRecordCnt = 0;

            dat = new DataTable();
            alParameters = new ArrayList();

            alParameters.Add(new SqlParameter("@Mode", 6));
            alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));

            dat = objclsConnection.ExecuteDataTable("spInvSalesReturn", alParameters);

            if (dat.Rows.Count > 0)
                intRecordCnt = Convert.ToInt32(dat.Rows[0]["LastReturnNo"]);

            if (intType == 1)
                return intRecordCnt + 1;
            else
                return intRecordCnt;
        }


        public DataTable GetReturnNumbers(string strReturnNo)
        {   // Get Return Number For searching
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 5));
          //  prmCommon.Add(new SqlParameter("@CompanyID", objclsDTOSalesReturnMaster.intCompanyID));
        //    prmCommon.Add(new SqlParameter("@VendorID", objclsDTOSalesReturnMaster.intVendorID));
       //     prmCommon.Add(new SqlParameter("@SearchNo", strReturnNo));
            return objclsConnection.ExecuteDataTable("spInvSalesReturn", prmCommon);
        }

        public bool GetSalesReturnMasterDetails(long intSalesReturnID)
        {   //Sales Return
            ArrayList prmSalesReturn = new ArrayList();
            prmSalesReturn.Add(new SqlParameter("@Mode", 1));

            prmSalesReturn.Add(new SqlParameter("@SalesReturnID", intSalesReturnID));
            DataTable datSalesReturn = objclsConnection.ExecuteDataTable("spInvSalesReturn", prmSalesReturn);

            if (datSalesReturn.Rows.Count > 0)
            {
                objclsDTOSalesReturnMaster.intSalesReturnID = intSalesReturnID;
                objclsDTOSalesReturnMaster.strSalesReturnNo = Convert.ToString(datSalesReturn.Rows[0]["SalesReturnNo"]);
                if (datSalesReturn.Rows[0]["ReturnDate"] != DBNull.Value) objclsDTOSalesReturnMaster.strReturnDate = Convert.ToString(datSalesReturn.Rows[0]["ReturnDate"]); else objclsDTOSalesReturnMaster.strReturnDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                if (datSalesReturn.Rows[0]["CompanyID"] != DBNull.Value) objclsDTOSalesReturnMaster.intCompanyID = Convert.ToInt32(datSalesReturn.Rows[0]["CompanyID"]); else objclsDTOSalesReturnMaster.strRemarks = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                if (datSalesReturn.Rows[0]["VendorID"] != DBNull.Value) objclsDTOSalesReturnMaster.intVendorID = Convert.ToInt32(datSalesReturn.Rows[0]["VendorID"]); else objclsDTOSalesReturnMaster.intVendorID = 0;
                if (datSalesReturn.Rows[0]["TaxSchemeID"] != DBNull.Value) objclsDTOSalesReturnMaster.intTaxSchemeID = Convert.ToInt32(datSalesReturn.Rows[0]["TaxSchemeID"]); else objclsDTOSalesReturnMaster.intTaxSchemeID = 0;
                //if (datSalesReturn.Rows[0]["GrandDiscountID"] != DBNull.Value) objclsDTOSalesReturnMaster.intGrandDiscountID = Convert.ToInt32(datSalesReturn.Rows[0]["GrandDiscountID"]); else objclsDTOSalesReturnMaster.intGrandDiscountID = 0;
                if (datSalesReturn.Rows[0]["GrandDiscountAmount"] != DBNull.Value) objclsDTOSalesReturnMaster.decGrandDiscountAmount = Convert.ToDecimal(datSalesReturn.Rows[0]["GrandDiscountAmount"]); else objclsDTOSalesReturnMaster.decGrandDiscountAmount = 0;
                if (datSalesReturn.Rows[0]["GrandReturnAmount"] != DBNull.Value) objclsDTOSalesReturnMaster.decGrandReturnAmount = Convert.ToDecimal(datSalesReturn.Rows[0]["GrandReturnAmount"]); else objclsDTOSalesReturnMaster.decGrandReturnAmount = 0;
                if (datSalesReturn.Rows[0]["GrandAmount"] != DBNull.Value) objclsDTOSalesReturnMaster.decGrandAmount = Convert.ToDecimal(datSalesReturn.Rows[0]["GrandAmount"]); else objclsDTOSalesReturnMaster.decGrandAmount = 0;
                if (datSalesReturn.Rows[0]["NetAmount"] != DBNull.Value)
                    objclsDTOSalesReturnMaster.decNetAmount = Convert.ToDecimal(datSalesReturn.Rows[0]["NetAmount"]); else objclsDTOSalesReturnMaster.decNetAmount = 0;
                if (datSalesReturn.Rows[0]["TaxAmount"] != DBNull.Value)
                    objclsDTOSalesReturnMaster.decTaxAmount = Convert.ToDecimal(datSalesReturn.Rows[0]["TaxAmount"]);
                else objclsDTOSalesReturnMaster.decNetAmount = 0;

                if (datSalesReturn.Rows[0]["CurrencyID"] != DBNull.Value) objclsDTOSalesReturnMaster.intCurrencyID = Convert.ToInt32(datSalesReturn.Rows[0]["CurrencyID"]); else objclsDTOSalesReturnMaster.intCurrencyID = 0;
                if (datSalesReturn.Rows[0]["SalesInvoiceID"] != DBNull.Value) objclsDTOSalesReturnMaster.intSalesInvoiceID = Convert.ToInt32(datSalesReturn.Rows[0]["SalesInvoiceID"]); else objclsDTOSalesReturnMaster.intSalesInvoiceID = 0;
                if (datSalesReturn.Rows[0]["StatusID"] != DBNull.Value) objclsDTOSalesReturnMaster.intStatusID = Convert.ToInt32(datSalesReturn.Rows[0]["StatusID"]); else objclsDTOSalesReturnMaster.intStatusID = 0;

                objclsDTOSalesReturnMaster.strReturnDate = Convert.ToString(datSalesReturn.Rows[0]["ReturnDate"]);
                if (datSalesReturn.Rows[0]["Remarks"] != DBNull.Value) objclsDTOSalesReturnMaster.strRemarks = Convert.ToString(datSalesReturn.Rows[0]["Remarks"]); else objclsDTOSalesReturnMaster.strRemarks = "";

                if (datSalesReturn.Rows[0]["UserName"] != DBNull.Value) objclsDTOSalesReturnMaster.strCreatedBy = Convert.ToString(datSalesReturn.Rows[0]["UserName"]); else objclsDTOSalesReturnMaster.strCreatedBy = "";
                if (datSalesReturn.Rows[0]["AccountID"] != DBNull.Value) objclsDTOSalesReturnMaster.intAccountID = Convert.ToInt32(datSalesReturn.Rows[0]["AccountID"]); else objclsDTOSalesReturnMaster.intAccountID = 0;

                DataTable datCancellationDetails = DisplayCancellationDetails();

                if (datCancellationDetails != null && datCancellationDetails.Rows.Count > 0)
                {
                    objclsDTOSalesReturnMaster.strCancellationDescription = Convert.ToString(datCancellationDetails.Rows[0]["Remarks"]);
                    objclsDTOSalesReturnMaster.strCancellationDate = Convert.ToString(datCancellationDetails.Rows[0]["Date"]);
                    objclsDTOSalesReturnMaster.intCancelledBy = datCancellationDetails.Rows[0]["CancelledBy"].ToInt32();
                    objclsDTOSalesReturnMaster.strCancelledBy = datCancellationDetails.Rows[0]["CancelledByName"].ToString();
                }

                objclsDTOSalesReturnMaster.intCreatedBy = Convert.ToInt32(datSalesReturn.Rows[0]["CreatedBy"]);
                objclsDTOSalesReturnMaster.strCreatedDate = Convert.ToString(datSalesReturn.Rows[0]["CreatedDate"]);

                return true;
            }
            return false;
        }

        public DataTable GetReturnDetDetails(long intSalesReturnID)
        {   // Display Return detail
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode",9 ));
            prmCommon.Add(new SqlParameter("@SalesReturnID", intSalesReturnID));
            return objclsConnection.ExecuteDataTable("spInvSalesReturn", prmCommon);
        }

        public bool SaveSalesReturnMaster()
        {
            alParameters = new ArrayList();
            if (objclsDTOSalesReturnMaster.intSalesReturnID != 0)
            {
                DeleteSalesReturnDetails();
                DeleteCancellationDetails();
            }
            alParameters.Add(new SqlParameter("@Mode", objclsDTOSalesReturnMaster.intSalesReturnID == 0 ? 2 : 3));
            alParameters.Add(new SqlParameter("@CompanyID", objclsDTOSalesReturnMaster.intCompanyID));
            alParameters.Add(new SqlParameter("@SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));
            alParameters.Add(new SqlParameter("@SalesInvoiceID", objclsDTOSalesReturnMaster.intSalesInvoiceID));
            alParameters.Add(new SqlParameter("@SalesReturnNo", objclsDTOSalesReturnMaster.strSalesReturnNo));
            alParameters.Add(new SqlParameter("@ReturnDate", objclsDTOSalesReturnMaster.strReturnDate));
            alParameters.Add(new SqlParameter("@Remarks", objclsDTOSalesReturnMaster.strRemarks));
            alParameters.Add(new SqlParameter("@CurrencyID", objclsDTOSalesReturnMaster.intCurrencyID));
            alParameters.Add(new SqlParameter("@AccountID", objclsDTOSalesReturnMaster.intAccountID));
            alParameters.Add(new SqlParameter("@TaxSchemeID", objclsDTOSalesReturnMaster.intTaxSchemeID));
            alParameters.Add(new SqlParameter("@TaxAmount", objclsDTOSalesReturnMaster.decTaxAmount));
            if (objclsDTOSalesReturnMaster.intGrandDiscountID > 0)
            {
                alParameters.Add(new SqlParameter("@GrandDiscountID", objclsDTOSalesReturnMaster.intGrandDiscountID));
            }
            alParameters.Add(new SqlParameter("@GrandDiscountAmount", objclsDTOSalesReturnMaster.decGrandDiscountAmount));
            alParameters.Add(new SqlParameter("@GrandReturnAmount", objclsDTOSalesReturnMaster.decGrandReturnAmount));
            alParameters.Add(new SqlParameter("@GrandAmount", objclsDTOSalesReturnMaster.decGrandAmount));
            alParameters.Add(new SqlParameter("@NetAmount", objclsDTOSalesReturnMaster.decNetAmount));
            alParameters.Add(new SqlParameter("@CreatedBy", objclsDTOSalesReturnMaster.intCreatedBy));
            alParameters.Add(new SqlParameter("@CreatedDate", objclsDTOSalesReturnMaster.strCreatedDate));
            alParameters.Add(new SqlParameter("@StatusID", objclsDTOSalesReturnMaster.intStatusID));
            //alParameters.Add(new SqlParameter("@CreditHeadID", objclsDTOSalesReturnMaster.intCreditHeadID));
            //alParameters.Add(new SqlParameter("@DebitHeadID", objclsDTOSalesReturnMaster.intDebitHeadID));
            alParameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.CreditNote));

            SqlParameter objParameter = new SqlParameter("RetValue", SqlDbType.Int);
            objParameter.Direction = ParameterDirection.ReturnValue;
            alParameters.Add(objParameter);

            object objSalesReturnID=0;
            if (objclsConnection.ExecuteNonQueryWithTran("spInvSalesReturn", alParameters, out objSalesReturnID) != 0)
            {
                if((Int32)objSalesReturnID >0)
                {
                    objclsDTOSalesReturnMaster.intSalesReturnID = (Int32)objSalesReturnID;
                    if (objclsDTOSalesReturnMaster.intStatusID == (Int32)OperationStatusType.SSalesReturnCancelled)
                        SaveCancellationDetails();
                    SaveSalesReturnDetails();
                }
            }
            return true;
        }

        public bool SaveSalesReturnDetails()
        {
            ArrayList alDetParameters;
            int intSerialNo=0;
            foreach(clsDTOSalesReturnDetails objSalesReturnDetails in objclsDTOSalesReturnMaster.lstSalesReturnDetails)
            {
                alDetParameters = new ArrayList();
                alDetParameters.Add(new SqlParameter("@Mode", 7));
                alDetParameters.Add(new SqlParameter("@SerialNo", ++intSerialNo));
                alDetParameters.Add(new SqlParameter("@SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));
                alDetParameters.Add(new SqlParameter("@ReferenceSerialNo", objSalesReturnDetails.intReferenceSerialNo));
                alDetParameters.Add(new SqlParameter("@BatchID", objSalesReturnDetails.intBatchID));
                alDetParameters.Add(new SqlParameter("@ReturnQuantity", objSalesReturnDetails.decReturnQuantity));
                alDetParameters.Add(new SqlParameter("@Rate", objSalesReturnDetails.decRate));
                alDetParameters.Add(new SqlParameter("@WarehouseID", objSalesReturnDetails.intWarehouseID));
                alDetParameters.Add(new SqlParameter("@ReasonID", objSalesReturnDetails.intReasonID));
                alDetParameters.Add(new SqlParameter("@UOMID", objSalesReturnDetails.intUOMID));
                //if (objSalesReturnDetails.intDiscountID>0)
                //{
                //    alDetParameters.Add(new SqlParameter("@DiscountID", objSalesReturnDetails.intDiscountID));
                //}
                alDetParameters.Add(new SqlParameter("@DiscountAmount", objSalesReturnDetails.decDiscountAmount));
                alDetParameters.Add(new SqlParameter("@ReturnAmount", objSalesReturnDetails.decReturnAmount));
                alDetParameters.Add(new SqlParameter("@GrandAmount", objSalesReturnDetails.decGrandAmount));
                alDetParameters.Add(new SqlParameter("@NetAmount", objSalesReturnDetails.decNetAmount));
                alDetParameters.Add(new SqlParameter("@CompanyID", objclsDTOSalesReturnMaster.intCompanyID));
                alDetParameters.Add(new SqlParameter("@CurrencyID", objclsDTOSalesReturnMaster.intCurrencyID));
                alDetParameters.Add(new SqlParameter("@ReturnDate", objclsDTOSalesReturnMaster.strReturnDate));
                alDetParameters.Add(new SqlParameter("@StatusID", objclsDTOSalesReturnMaster.intStatusID));
                alDetParameters.Add(new SqlParameter("@ItemID", objSalesReturnDetails.intItemID));
                alDetParameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.CreditNote));
                alDetParameters.Add(new SqlParameter("@IsGroup", objSalesReturnDetails.blnIsGroup));

                objclsConnection.ExecuteNonQueryWithTran("spInvSalesReturn", alDetParameters);

                if (objclsDTOSalesReturnMaster.intStatusID != (Int32)OperationStatusType.SSalesReturnCancelled)
                {
                    int intWarehouseID = 0;
                    //DataTable datIssueDetails = FillCombos(new string[] { "IM.WarehouseID", "InvItemIssueMaster IM  INNER JOIN InvItemIssueReferenceDetails IRD ON IM.ItemIssueID=IRD.ItemIssueID", "IM.OrderTypeID = " + (int)OperationOrderType.DNOTSalesInvoice + " And IRD.ReferenceID = " + objclsDTOSalesReturnMaster.intSalesInvoiceID });
                    //intWarehouseID = datIssueDetails.Rows[0]["WarehouseID"].ToInt32();
                    intWarehouseID = objSalesReturnDetails.intWarehouseID;

                    decimal decSoldQty = 0, decDamagedQty = 0;
                    bool blnIsDamaged = false;
                    if (objSalesReturnDetails.blnIsGroup)
                    {
                        objSalesReturnDetails.intSerialNo = intSerialNo;
                        foreach (clsDTOItemGroupReturnDetails objDTOItemGroupReturnDetails in objSalesReturnDetails.lstItemGroupReturnDetails)
                        {
                            decSoldQty = 0; decDamagedQty = 0;
                            blnIsDamaged = false;
                            ArrayList prmItemGroupReturnDetails = new ArrayList();
                            prmItemGroupReturnDetails.Add(new SqlParameter("@Mode", 31));
                            prmItemGroupReturnDetails.Add(new SqlParameter("@SerialNo", ++intSerialNo));
                            prmItemGroupReturnDetails.Add(new SqlParameter("@SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));
                            prmItemGroupReturnDetails.Add(new SqlParameter("@SalesReturnDetailsSerialNo", objSalesReturnDetails.intItemID));
                            prmItemGroupReturnDetails.Add(new SqlParameter("@ItemID", objDTOItemGroupReturnDetails.intItemID));
                            prmItemGroupReturnDetails.Add(new SqlParameter("@BatchID", objDTOItemGroupReturnDetails.intBatchID));
                            prmItemGroupReturnDetails.Add(new SqlParameter("@UOMID", objDTOItemGroupReturnDetails.intUOMID));
                            prmItemGroupReturnDetails.Add(new SqlParameter("@Quantity", objDTOItemGroupReturnDetails.decQuantity));
                            objclsConnection.ExecuteNonQueryWithTran("spInvSalesReturn", prmItemGroupReturnDetails);

                            clsDTOSalesReturnDetails objItemRetDetails = new clsDTOSalesReturnDetails();
                            objItemRetDetails.decReturnQuantity = objDTOItemGroupReturnDetails.decQuantity;
                            objItemRetDetails.intBatchID = objDTOItemGroupReturnDetails.intBatchID;
                            objItemRetDetails.intItemID = objDTOItemGroupReturnDetails.intItemID;
                            objItemRetDetails.intUOMID = objDTOItemGroupReturnDetails.intUOMID;

                            GetStockDetailsQuantity(objItemRetDetails.intItemID, objItemRetDetails.intBatchID, objSalesReturnDetails.intWarehouseID, out decSoldQty, out decDamagedQty);
                            if (objSalesReturnDetails.intReasonID == (Int32)ReasonReferences.Damaged)
                            {
                                blnIsDamaged = true;
                                decDamagedQty = decDamagedQty + ConvertQtyToBaseUnitQty(objItemRetDetails.intUOMID, objItemRetDetails.intItemID, objItemRetDetails.decReturnQuantity);
                            }
                            decSoldQty = decSoldQty - ConvertQtyToBaseUnitQty(objItemRetDetails.intUOMID, objItemRetDetails.intItemID, objItemRetDetails.decReturnQuantity);
                            UpdateStockDetailsQuantity(objItemRetDetails.intItemID, objItemRetDetails.intBatchID, objSalesReturnDetails.intWarehouseID, decSoldQty, decDamagedQty);

                            GetStockMasterQuantity(objItemRetDetails.intItemID, out decSoldQty, out decDamagedQty);
                            if (blnIsDamaged)
                                decDamagedQty = decDamagedQty + ConvertQtyToBaseUnitQty(objItemRetDetails.intUOMID, objItemRetDetails.intItemID, objItemRetDetails.decReturnQuantity );
                            decSoldQty = decSoldQty - ConvertQtyToBaseUnitQty(objItemRetDetails.intUOMID, objItemRetDetails.intItemID, objItemRetDetails.decReturnQuantity);
                            UpdateStockMasterQuantity(objItemRetDetails.intItemID, decSoldQty, decDamagedQty);
                        }
                    }
                    else
                    {

                        GetStockDetailsQuantity(objSalesReturnDetails.intItemID, objSalesReturnDetails.intBatchID, objSalesReturnDetails.intWarehouseID, out decSoldQty, out decDamagedQty);
                        if (objSalesReturnDetails.intReasonID == (Int32)ReasonReferences.Damaged)
                        {
                            blnIsDamaged = true;
                            decDamagedQty = decDamagedQty + ConvertQtyToBaseUnitQty(objSalesReturnDetails.intUOMID, objSalesReturnDetails.intItemID, objSalesReturnDetails.decReturnQuantity);
                        }
                        decSoldQty = decSoldQty - ConvertQtyToBaseUnitQty(objSalesReturnDetails.intUOMID, objSalesReturnDetails.intItemID, objSalesReturnDetails.decReturnQuantity);
                        UpdateStockDetailsQuantity(objSalesReturnDetails.intItemID, objSalesReturnDetails.intBatchID, objSalesReturnDetails.intWarehouseID, decSoldQty, decDamagedQty);

                        GetStockMasterQuantity(objSalesReturnDetails.intItemID, out decSoldQty, out decDamagedQty);
                        if (blnIsDamaged)
                            decDamagedQty = decDamagedQty + ConvertQtyToBaseUnitQty(objSalesReturnDetails.intUOMID, objSalesReturnDetails.intItemID, objSalesReturnDetails.decReturnQuantity);
                        decSoldQty = decSoldQty - ConvertQtyToBaseUnitQty(objSalesReturnDetails.intUOMID, objSalesReturnDetails.intItemID, objSalesReturnDetails.decReturnQuantity);
                        UpdateStockMasterQuantity(objSalesReturnDetails.intItemID, decSoldQty, decDamagedQty);

                        //clsDTOLocationDetails objLocationDetails = new clsDTOLocationDetails();
                        //objLocationDetails.decQuantity = ConvertQtyToBaseUnitQty(objSalesReturnDetails.intUOMID, objSalesReturnDetails.intItemID, objSalesReturnDetails.decReturnQuantity);
                        //objLocationDetails.intItemID = objSalesReturnDetails.intItemID;
                        //objLocationDetails.lngBatchID = objSalesReturnDetails.intBatchID;
                        //DataRow drLocation = GetItemDefaultLocation(objSalesReturnDetails.intItemID, intWarehouseID);
                        //objLocationDetails.intLocationID = drLocation["LocationID"].ToInt32();
                        //objLocationDetails.intRowID = drLocation["RowID"].ToInt32();
                        //objLocationDetails.intBlockID = drLocation["BlockID"].ToInt32();
                        //objLocationDetails.intLotID = drLocation["LotID"].ToInt32();
                        //UpdateItemLocationDetails(objLocationDetails, intWarehouseID, false, blnIsDamaged);
                    }
                }
                SaveItemSummary(objSalesReturnDetails);
            }
            UpdateStockAndAccountsValue();

            return true;
        }
        private void SaveItemSummary(clsDTOSalesReturnDetails objSalesReturnDetails)
        {
            if (objSalesReturnDetails.lstItemGroupReturnDetails != null && objSalesReturnDetails.lstItemGroupReturnDetails.Count > 0)
            {
                foreach (clsDTOItemGroupReturnDetails objclsDTOItemGroupReturnDetails in objSalesReturnDetails.lstItemGroupReturnDetails)
                {
                    ArrayList prmItemGroupReturnDetails = new ArrayList();
                    prmItemGroupReturnDetails.Add(new SqlParameter("@Mode", 37));
                    prmItemGroupReturnDetails.Add(new SqlParameter("@SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));
                    prmItemGroupReturnDetails.Add(new SqlParameter("@ItemID", objclsDTOItemGroupReturnDetails.intItemID));
                    prmItemGroupReturnDetails.Add(new SqlParameter("@BatchID", objclsDTOItemGroupReturnDetails.intBatchID));
                    prmItemGroupReturnDetails.Add(new SqlParameter("@ReturnQuantity", objclsDTOItemGroupReturnDetails.decQuantity));
                    prmItemGroupReturnDetails.Add(new SqlParameter("@StatusID", objclsDTOSalesReturnMaster.intStatusID));
                    prmItemGroupReturnDetails.Add(new SqlParameter("@CompanyID", objclsDTOSalesReturnMaster.intCompanyID));
                    prmItemGroupReturnDetails.Add(new SqlParameter("@ReturnDate", Convert.ToDateTime(objclsDTOSalesReturnMaster.strReturnDate)));
                    prmItemGroupReturnDetails.Add(new SqlParameter("@WarehouseID", objSalesReturnDetails.intWarehouseID));
                    prmItemGroupReturnDetails.Add(new SqlParameter("@ReturnAmount", objSalesReturnDetails.decReturnAmount));
                    prmItemGroupReturnDetails.Add(new SqlParameter("@DiscountAmount", objSalesReturnDetails.decDiscountAmount));
                    objclsConnection.ExecuteNonQueryWithTran("spInvSalesReturn", prmItemGroupReturnDetails);
                }
            }
        }
        private bool UpdateStockAndAccountsValue()
        {
            //This is to Update Stock Value

            ArrayList prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation.Add(new SqlParameter("@Mode", 23));
            prmItemStockValueUpdation.Add(new SqlParameter("@SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));
            prmItemStockValueUpdation.Add(new SqlParameter("@StatusID", objclsDTOSalesReturnMaster.intStatusID));
            prmItemStockValueUpdation.Add(new SqlParameter("@OperationTypeID", (int)OperationType.CreditNote));

            if (objclsConnection.ExecuteNonQueryWithTran("spInvSalesReturn", prmItemStockValueUpdation) != 0)
                return true;
            return false;
        }

        public bool DeleteSalesReturnDetails()
        {
            DeleteItemSummary();
            DataTable datSalesReturnDetails = GetReturnDetDetails(objclsDTOSalesReturnMaster.intSalesReturnID);
            DataTable dtSalesReturn = FillCombos(new string[] {"StatusID","InvSalesReturnMaster","SalesReturnID = "+objclsDTOSalesReturnMaster.intSalesReturnID});
            int intOldStatusID = Convert.ToInt32(dtSalesReturn.Rows[0]["StatusID"]);
            if (intOldStatusID != (Int32)OperationStatusType.SSalesReturnCancelled)
            {
                int intWarehouseID = 0;
                //DataTable datIssueDetails = FillCombos(new string[] { "IM.WarehouseID", "InvItemIssueMaster IM  INNER JOIN InvItemIssueReferenceDetails IRD ON IM.ItemIssueID=IRD.ItemIssueID", "IM.OrderTypeID = " + (int)OperationOrderType.DNOTSalesInvoice + " And IRD.ReferenceID = " + objclsDTOSalesReturnMaster.intSalesInvoiceID });
                

                foreach (DataRow dr in datSalesReturnDetails.Rows)
                {
                    bool blnIsDamaged = false;
                    decimal decSoldQuantity = 0, decDamagedQuantity = 0; decimal TotalQtyBatchless = 0; decimal AvailbleQtyinBatch = 0;

                    intWarehouseID = dr["WarehouseID"].ToInt32();
                 
                    if (dr["IsGroup"].ToBoolean() == true)
                    {
                        
                        DataTable dtGrpReturnDet = GetItemGroupReturnDetails();
                        foreach (DataRow dtGrp in dtGrpReturnDet.Rows)
                        {


                            DataTable BatchlessItems = new DataTable();
                            if (Convert.ToInt32(dr["ReasonID"]) == (Int32)ReasonReferences.Damaged)
                                
                                blnIsDamaged = true;
                            if (blnIsDamaged)
                            {
                                BatchlessItems = BatchLessItemsDamaged(Convert.ToInt32(dtGrp["ItemID"]), intWarehouseID);
                                
                            }
                            else
                            {
                                BatchlessItems = BatchLessItems(Convert.ToInt32(dtGrp["ItemID"]), intWarehouseID);
                            }

                            TotalQtyBatchless = (Convert.ToDecimal(dtGrp["Quantity"]));
                            decimal QtySelectedGrp = 0;
                            foreach (DataRow drBatchLess in BatchlessItems.Rows)
                            {
                                decSoldQuantity = 0; decDamagedQuantity = 0;
                                blnIsDamaged = false;


                                if ((Convert.ToDecimal(dtGrp["Quantity"])) == QtySelectedGrp)
                                {
                                    break;
                                }
                                clsDTOSalesReturnDetails objItemReturnDetailsDelete = new clsDTOSalesReturnDetails();
                                //clsDTOBatchlessItemIssueDetails objBatchless = new clsDTOBatchlessItemIssueDetails();
                                objItemReturnDetailsDelete.intItemID = Convert.ToInt32(drBatchLess["ItemID"]);

                                objItemReturnDetailsDelete.intUOMID = Convert.ToInt32(dr["UOMID"]);



                                objItemReturnDetailsDelete.intBatchID = Convert.ToInt32(drBatchLess["BatchID"]);

                                AvailbleQtyinBatch = Convert.ToDecimal(drBatchLess["AvailableQty"]);


                                if (AvailbleQtyinBatch < TotalQtyBatchless)
                                {

                                    QtySelectedGrp = QtySelectedGrp + AvailbleQtyinBatch;
                                    TotalQtyBatchless = TotalQtyBatchless - AvailbleQtyinBatch;
                                    objItemReturnDetailsDelete.decReturnQuantity = AvailbleQtyinBatch;
                                }
                                else
                                {
                                    QtySelectedGrp = QtySelectedGrp + TotalQtyBatchless;

                                    objItemReturnDetailsDelete.decReturnQuantity = TotalQtyBatchless;
                                } 


                                //objItemReturnDetailsDelete.decReturnQuantity = Convert.ToDecimal(dtGrp["Quantity"]);
                                //objItemReturnDetailsDelete.intBatchID = Convert.ToInt64(dtGrp["BatchID"]);
                                //objItemReturnDetailsDelete.intItemID = Convert.ToInt32(dtGrp["ItemID"]);
                                //objItemReturnDetailsDelete.intUOMID = Convert.ToInt32(dtGrp["UomID"]);


                                GetStockDetailsQuantity(objItemReturnDetailsDelete.intItemID, objItemReturnDetailsDelete.intBatchID, Convert.ToInt32(dr["WarehouseID"]), out decSoldQuantity, out decDamagedQuantity);
                                if (Convert.ToInt32(dr["ReasonID"]) == (Int32)ReasonReferences.Damaged)
                                {
                                    blnIsDamaged = true;
                                    decDamagedQuantity = decDamagedQuantity - ConvertQtyToBaseUnitQty(objItemReturnDetailsDelete.intUOMID, objItemReturnDetailsDelete.intItemID, objItemReturnDetailsDelete.decReturnQuantity);
                                }
                                decSoldQuantity = decSoldQuantity + ConvertQtyToBaseUnitQty(Convert.ToInt32(dr["UOMID"]), Convert.ToInt32(dr["ItemID"]), objItemReturnDetailsDelete.decReturnQuantity);
                                UpdateStockDetailsQuantity(objItemReturnDetailsDelete.intItemID, objItemReturnDetailsDelete.intBatchID, Convert.ToInt32(dr["WarehouseID"]), decSoldQuantity, decDamagedQuantity);

                                GetStockMasterQuantity(objItemReturnDetailsDelete.intItemID, out decSoldQuantity, out decDamagedQuantity);
                                if (blnIsDamaged)
                                {
                                    decDamagedQuantity = decDamagedQuantity - ConvertQtyToBaseUnitQty(objItemReturnDetailsDelete.intUOMID, objItemReturnDetailsDelete.intItemID, objItemReturnDetailsDelete.decReturnQuantity);
                                }
                                decSoldQuantity = decSoldQuantity + ConvertQtyToBaseUnitQty(objItemReturnDetailsDelete.intUOMID, objItemReturnDetailsDelete.intItemID, objItemReturnDetailsDelete.decReturnQuantity);
                                UpdateStockMasterQuantity(objItemReturnDetailsDelete.intItemID, decSoldQuantity, decDamagedQuantity);


                            }
                        }
                        ArrayList prmItemGroupReturnDetails = new ArrayList();
                        prmItemGroupReturnDetails = new ArrayList();
                        prmItemGroupReturnDetails.Add(new SqlParameter("@Mode", 33));
                        prmItemGroupReturnDetails.Add(new SqlParameter("@SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));

                        objclsConnection.ExecuteNonQueryWithTran("spInvSalesReturn", prmItemGroupReturnDetails);
                    }
                    else if (dr["IsGroup"].ToBoolean() == false)
                    {
                        //DataTable dtGrpReturnDet = GetItemGroupReturnDetails();
                        //foreach (DataRow dtGrp in dtGrpReturnDet.Rows)
                        //{


                        //    DataTable BatchlessItems = BatchLessItems(Convert.ToInt32(dr["ItemID"]), intWarehouseID);

                        //    TotalQtyBatchless = (Convert.ToDecimal(dr["ReturnQuantity"]));
                        //    decimal QtySelectedGrp = 0;
                        //    foreach (DataRow drBatchLess in BatchlessItems.Rows)
                        //    {
                        //        decSoldQuantity = 0; decDamagedQuantity = 0;
                        //        blnIsDamaged = false;


                        //        if ((Convert.ToDecimal(dr["ReturnQuantity"])) == QtySelectedGrp)
                        //        {
                        //            break;
                        //        }
                        //        clsDTOSalesReturnDetails objItemReturnDetailsDelete = new clsDTOSalesReturnDetails();
                        //        //clsDTOBatchlessItemIssueDetails objBatchless = new clsDTOBatchlessItemIssueDetails();
                        //        objItemReturnDetailsDelete.intItemID = Convert.ToInt32(drBatchLess["ItemID"]);

                        //        objItemReturnDetailsDelete.intUOMID = Convert.ToInt32(dr["UOMID"]);



                        //        objItemReturnDetailsDelete.intBatchID = Convert.ToInt32(drBatchLess["BatchID"]);

                        //        AvailbleQtyinBatch = Convert.ToDecimal(drBatchLess["AvailableQty"]);


                        //        if (AvailbleQtyinBatch < TotalQtyBatchless)
                        //        {

                        //            QtySelectedGrp = QtySelectedGrp + AvailbleQtyinBatch;
                        //            TotalQtyBatchless = TotalQtyBatchless - AvailbleQtyinBatch;
                        //            objItemReturnDetailsDelete.decReturnQuantity = AvailbleQtyinBatch;
                        //        }
                        //        else
                        //        {
                        //            QtySelectedGrp = QtySelectedGrp + TotalQtyBatchless;

                        //            objItemReturnDetailsDelete.decReturnQuantity = TotalQtyBatchless;
                        //        }


                        //        //objItemReturnDetailsDelete.decReturnQuantity = Convert.ToDecimal(dtGrp["Quantity"]);
                        //        //objItemReturnDetailsDelete.intBatchID = Convert.ToInt64(dtGrp["BatchID"]);
                        //        //objItemReturnDetailsDelete.intItemID = Convert.ToInt32(dtGrp["ItemID"]);
                        //        //objItemReturnDetailsDelete.intUOMID = Convert.ToInt32(dtGrp["UomID"]);


                        //        GetStockDetailsQuantity(objItemReturnDetailsDelete.intItemID, objItemReturnDetailsDelete.intBatchID, Convert.ToInt32(dr["WarehouseID"]), out decSoldQuantity, out decDamagedQuantity);
                        //        if (Convert.ToInt32(dr["ReasonID"]) == (Int32)ReasonReferences.Damaged)
                        //        {
                        //            blnIsDamaged = true;
                        //            decDamagedQuantity = decDamagedQuantity - ConvertQtyToBaseUnitQty(objItemReturnDetailsDelete.intUOMID, objItemReturnDetailsDelete.intItemID, objItemReturnDetailsDelete.decReturnQuantity);
                        //        }
                        //        decSoldQuantity = decSoldQuantity + ConvertQtyToBaseUnitQty(Convert.ToInt32(dr["UOMID"]), Convert.ToInt32(dr["ItemID"]), objItemReturnDetailsDelete.decReturnQuantity);
                        //        UpdateStockDetailsQuantity(objItemReturnDetailsDelete.intItemID, objItemReturnDetailsDelete.intBatchID, Convert.ToInt32(dr["WarehouseID"]), decSoldQuantity, decDamagedQuantity);

                        //        GetStockMasterQuantity(objItemReturnDetailsDelete.intItemID, out decSoldQuantity, out decDamagedQuantity);
                        //        if (blnIsDamaged)
                        //        {
                        //            decDamagedQuantity = decDamagedQuantity - ConvertQtyToBaseUnitQty(objItemReturnDetailsDelete.intUOMID, objItemReturnDetailsDelete.intItemID, objItemReturnDetailsDelete.decReturnQuantity);
                        //        }
                        //        decSoldQuantity = decSoldQuantity + ConvertQtyToBaseUnitQty(objItemReturnDetailsDelete.intUOMID, objItemReturnDetailsDelete.intItemID, objItemReturnDetailsDelete.decReturnQuantity);
                        //        UpdateStockMasterQuantity(objItemReturnDetailsDelete.intItemID, decSoldQuantity, decDamagedQuantity);


                        //    }
                        ////}
                        //ArrayList prmItemGroupReturnDetails = new ArrayList();
                        //prmItemGroupReturnDetails = new ArrayList();
                        //prmItemGroupReturnDetails.Add(new SqlParameter("@Mode", 33));
                        //prmItemGroupReturnDetails.Add(new SqlParameter("@SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));

                        //objclsConnection.ExecuteNonQueryWithTran("spInvSalesReturn", prmItemGroupReturnDetails);



                        intWarehouseID = dr["WarehouseID"].ToInt32();
                        GetStockDetailsQuantity(Convert.ToInt32(dr["ItemID"]), Convert.ToInt64(dr["BatchID"]), Convert.ToInt32(dr["WarehouseID"]), out decSoldQuantity, out decDamagedQuantity);
                        if (Convert.ToInt32(dr["ReasonID"]) == (Int32)ReasonReferences.Damaged)
                        {
                            blnIsDamaged = true;
                            decDamagedQuantity = decDamagedQuantity - ConvertQtyToBaseUnitQty(Convert.ToInt32(dr["UOMID"]), Convert.ToInt32(dr["ItemID"]), Convert.ToDecimal(dr["ReturnQuantity"]));
                        }
                        decSoldQuantity = decSoldQuantity + ConvertQtyToBaseUnitQty(Convert.ToInt32(dr["UOMID"]), Convert.ToInt32(dr["ItemID"]), Convert.ToDecimal(dr["ReturnQuantity"]));
                        UpdateStockDetailsQuantity(Convert.ToInt32(dr["ItemID"]), Convert.ToInt64(dr["BatchID"]), Convert.ToInt32(dr["WarehouseID"]), decSoldQuantity, decDamagedQuantity);

                        GetStockMasterQuantity(Convert.ToInt32(dr["ItemID"]), out decSoldQuantity, out decDamagedQuantity);
                        if (blnIsDamaged)
                        {
                            decDamagedQuantity = decDamagedQuantity - ConvertQtyToBaseUnitQty(Convert.ToInt32(dr["UOMID"]), Convert.ToInt32(dr["ItemID"]), Convert.ToDecimal(dr["ReturnQuantity"]));
                        }
                        decSoldQuantity = decSoldQuantity + ConvertQtyToBaseUnitQty(Convert.ToInt32(dr["UOMID"]), Convert.ToInt32(dr["ItemID"]), Convert.ToDecimal(dr["ReturnQuantity"]));
                        UpdateStockMasterQuantity(Convert.ToInt32(dr["ItemID"]), decSoldQuantity, decDamagedQuantity);

                    }

                
                }
            } 
            ArrayList alDetParameters = new ArrayList();
            alDetParameters.Add(new SqlParameter("@Mode", 8));
            alDetParameters.Add(new SqlParameter("@SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));
            alDetParameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.CreditNote));
            alDetParameters.Add(new SqlParameter("@StatusID", intOldStatusID));
            objclsConnection.ExecuteNonQueryWithTran("spInvSalesReturn", alDetParameters);
            return true;
        }

        private void DeleteItemSummary()
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 38));
            alParameters.Add(new SqlParameter("@SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));
            objclsConnection.ExecuteNonQueryWithTran("spInvSalesReturn", alParameters);
        }

        public bool DeleteSalesReturnMaster()
        {

            DeleteSalesReturnDetails();
            DeleteCancellationDetails();
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 4));
            alParameters.Add(new SqlParameter("@SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));
            alParameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.CreditNote));
            objclsConnection.ExecuteNonQueryWithTran("spInvSalesReturn", alParameters);
            return true;
        }

        public DataTable GetSalesInvoiceItemDetails(int intSalesInvoiceID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 10));
            alParameters.Add(new SqlParameter("@SalesInvoiceID", intSalesInvoiceID));
            return objclsConnection.ExecuteDataTable("spInvSalesReturn", alParameters);
        }


        public DataTable GetItemUOMs(int intItemID)
        {
            ArrayList prmSalesReturn = new ArrayList();
            prmSalesReturn.Add(new SqlParameter("@Mode", 10));
            prmSalesReturn.Add(new SqlParameter("@ItemID", intItemID));
           // prmSalesReturn.Add(new SqlParameter("@UomTypeID", 2));
            DataTable dtItemUoms = new DataTable();
            dtItemUoms = objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmSalesReturn);

            prmSalesReturn = new ArrayList();
            prmSalesReturn.Add(new SqlParameter("@Mode", 12));
            prmSalesReturn.Add(new SqlParameter("@ItemID", intItemID));
            DataTable dtItemBaseUnit = objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmSalesReturn);
            DataRow dr = dtItemUoms.NewRow();
            dr["UOMID"] = dtItemBaseUnit.Rows[0]["UOMID"];
            dr["Description"] = dtItemBaseUnit.Rows[0]["Description"];
            dr["ShortName"] = dtItemBaseUnit.Rows[0]["ShortName"];
            dr["ItemID"] = intItemID;
            dtItemUoms.Rows.InsertAt(dr, 0);
            return dtItemUoms;
        }
        public DataTable GetUomConversionValues(int intUOMID, Int64 intItemID)
        {
            ArrayList prmUomDetails = new ArrayList();
            prmUomDetails.Add(new SqlParameter("@Mode", 11));
            prmUomDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmUomDetails.Add(new SqlParameter("@UnitID", intUOMID));
          //  prmUomDetails.Add(new SqlParameter("@UomTypeID", 1));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmUomDetails);
        }

        public void GetItemDiscountAmount(int intMode, int intItemID, int intDiscountID, int intVendorID)
        {
            ArrayList prmItemDiscount = new ArrayList();
            prmItemDiscount.Add(new SqlParameter("@Mode", intMode));
            prmItemDiscount.Add(new SqlParameter("@ItemID", intItemID));
            prmItemDiscount.Add(new SqlParameter("@DiscountID", intDiscountID));
            prmItemDiscount.Add(new SqlParameter("@VendorID", intVendorID));

            DataTable datDiscountAmount = objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmItemDiscount);

            if (datDiscountAmount.Rows.Count > 0)
            {
                objclsDTOSalesReturnMaster.blnDiscountForAmount = Convert.ToBoolean(datDiscountAmount.Rows[0]["IsDiscountForAmount"]);
                objclsDTOSalesReturnMaster.blnPercentCalculation = Convert.ToBoolean(datDiscountAmount.Rows[0]["PercentOrAmount"]);
                objclsDTOSalesReturnMaster.decDiscountValue = Convert.ToDecimal(datDiscountAmount.Rows[0]["PercOrAmtValue"]);
            }
        }
        public bool IsReturnNoExists(string strReturnNo,int intCompanyID)
        {
            bool blnIsExists = false;
            DataTable dtSalesReturn = new DataTable();
            ArrayList prmSalesReturn = new ArrayList();
            prmSalesReturn.Add(new SqlParameter("@Mode", 20));
            prmSalesReturn.Add(new SqlParameter("@SalesReturnNo", strReturnNo));
            prmSalesReturn.Add(new SqlParameter("@CompanyID", intCompanyID));
            dtSalesReturn = objclsConnection.ExecuteDataTable("spInvSalesReturn", prmSalesReturn);
            if (dtSalesReturn.Rows.Count > 0 && Convert.ToInt64(dtSalesReturn.Rows[0]["SalesReturnID"]) != objclsDTOSalesReturnMaster.intSalesReturnID)
                blnIsExists = true;
            return blnIsExists;
        }
        public string GetVendorAddress(int intReferenceID,bool blnIsVendorID,out string strAddressName)
        {
            if (blnIsVendorID)
            {
                DataTable datVendor = FillCombos(new string[] { "VendorAddID", "InvVendorAddress", "VendorID = " + intReferenceID });
                if (datVendor.Rows.Count > 0)
                {
                    intReferenceID = Convert.ToInt32(datVendor.Rows[0]["VendorAddID"]);
                }
            }
            string sAddress = "";
            sAddress += Environment.NewLine;
            ArrayList prmVendor = new ArrayList();

            prmVendor.Add(new SqlParameter("@Mode", 11));
            prmVendor.Add(new SqlParameter("@VendorAddID", intReferenceID));
            SqlDataReader drVendor = objclsConnection.ExecuteReader("spInvSalesReturn", prmVendor, CommandBehavior.SingleRow);
            strAddressName = "Address";
            if (drVendor.Read())
            {
                sAddress = Environment.NewLine;
                if(!string.IsNullOrEmpty(Convert.ToString(drVendor["AddressName"])))
                    strAddressName = Convert.ToString(drVendor["AddressName"]);

                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["ContactPerson"])))
                {
                    sAddress += Convert.ToString(drVendor["ContactPerson"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Address"])))
                {
                    sAddress += Convert.ToString(drVendor["Address"]);
                    sAddress += Environment.NewLine;
                }

                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["State"])))
                {
                    sAddress += Convert.ToString(drVendor["State"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Country"])))
                {
                    sAddress += Convert.ToString(drVendor["Country"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["ZipCode"])))
                {
                    sAddress += "ZipCode : " + Convert.ToString(drVendor["ZipCode"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Telephone"])))
                {
                    sAddress += "Telephone : " + Convert.ToString(drVendor["Telephone"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["MobileNo"])))
                {
                    sAddress += "MobileNo : " + Convert.ToString(drVendor["MobileNo"]);
                    sAddress += Environment.NewLine;
                }

                drVendor.Close();
                return sAddress;
            }
            else
            {
                drVendor.Close();
                return sAddress;
            }
        }

        public bool GetStockDetailsQuantity(int intItemID, long intBatchID, int intWarehouseID,out decimal decSoldQuantity,out decimal decDamagedQuantity)
        {
            decSoldQuantity = 0; decDamagedQuantity = 0;
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 12));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", intBatchID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@CompanyID", objclsDTOSalesReturnMaster.intCompanyID));
            DataTable datStock = objclsConnection.ExecuteDataTable("spInvSalesReturn", alParameters);
            if (datStock.Rows.Count > 0)
            {
                if (datStock.Rows[0]["SoldQuantity"] != DBNull.Value)
                    decSoldQuantity = Convert.ToDecimal(datStock.Rows[0]["SoldQuantity"]);
                if (datStock.Rows[0]["DamagedQuantity"] != DBNull.Value)
                    decDamagedQuantity = Convert.ToDecimal(datStock.Rows[0]["DamagedQuantity"]);
            }
            return true;
        }

        public bool GetStockMasterQuantity(int intItemID,out decimal decSoldQuantity, out decimal decDamagedQuantity)
        {
            decSoldQuantity = 0; decDamagedQuantity = 0;
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 13));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            DataTable  datStock = objclsConnection.ExecuteDataTable("spInvSalesReturn", alParameters);
            if (datStock.Rows.Count >0)
            {
                if (datStock.Rows[0]["SoldQuantity"] != DBNull.Value)
                    decSoldQuantity = Convert.ToDecimal(datStock.Rows[0]["SoldQuantity"]);
                if (datStock.Rows[0]["DamagedQuantity"] != DBNull.Value)
                    decDamagedQuantity = Convert.ToDecimal(datStock.Rows[0]["DamagedQuantity"]);
            }
            return true;
        }

        public bool UpdateStockDetailsQuantity(int intItemID, long intBatchID, int intWarehouseID, decimal decSoldQuantity, decimal decDamagedQuantity)
        {
            decimal decReceivedFromTransfer = 0, decTransferedQty = 0, decGRNQty = 0, decDemoQty = 0, decInvoicedQty = 0, decOpeningStock = 0;
            DataTable datStockdetails = new DataTable();
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 4));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", intBatchID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@CompanyID", objclsDTOSalesReturnMaster.intCompanyID));
            datStockdetails = objclsConnection.ExecuteDataTable("spInvStockMaster", alParameters);
            if (datStockdetails.Rows.Count > 0)
            {
                decReceivedFromTransfer = datStockdetails.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                decOpeningStock = datStockdetails.Rows[0]["OpeningStock"].ToDecimal();
                decGRNQty = datStockdetails.Rows[0]["GRNQuantity"].ToDecimal();
                decTransferedQty = datStockdetails.Rows[0]["TransferedQuantity"].ToDecimal();
                decInvoicedQty = datStockdetails.Rows[0]["InvoicedQuantity"].ToDecimal();
                decDemoQty = datStockdetails.Rows[0]["DemoQuantity"].ToDecimal();
            }
             alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 6));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", intBatchID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@CompanyID", objclsDTOSalesReturnMaster.intCompanyID));
            alParameters.Add(new SqlParameter("@SoldQuantity", decSoldQuantity));
            alParameters.Add(new SqlParameter("@DamagedQuantity", decDamagedQuantity));
            alParameters.Add(new SqlParameter("@GRNQuantity", decGRNQty));
            alParameters.Add(new SqlParameter("@InvoicedQuantity", decInvoicedQty));
            alParameters.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
            alParameters.Add(new SqlParameter("@OpeningStock", decOpeningStock));
            alParameters.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));
            alParameters.Add(new SqlParameter("@DemoQuantity", decDemoQty));
            objclsConnection.ExecuteNonQueryWithTran("spInvStockMaster", alParameters);
            return true;
        }

        public bool UpdateStockMasterQuantity(int intItemID,decimal decSoldQuantity, decimal decDamagedQuantity)
        {
            decimal decReceivedFromTransfer = 0, decTransferedQty = 0, decGRNQty = 0, decDemoQty = 0, decInvoicedQty = 0, decOpeningStock = 0;
            DataTable datStockdetails = new DataTable();
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            datStockdetails = objclsConnection.ExecuteDataTable("spInvStockMaster", alParameters);
            if (datStockdetails.Rows.Count > 0)
            {
                decReceivedFromTransfer = datStockdetails.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                decOpeningStock = datStockdetails.Rows[0]["OpeningStock"].ToDecimal();
                decGRNQty = datStockdetails.Rows[0]["GRNQuantity"].ToDecimal();
                decTransferedQty = datStockdetails.Rows[0]["TransferedQuantity"].ToDecimal();
                decInvoicedQty = datStockdetails.Rows[0]["InvoicedQuantity"].ToDecimal();
                decDemoQty = datStockdetails.Rows[0]["DemoQuantity"].ToDecimal();
            }
             alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 3));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@SoldQuantity", decSoldQuantity));
            alParameters.Add(new SqlParameter("@DamagedQuantity", decDamagedQuantity));
            alParameters.Add(new SqlParameter("@GRNQuantity", decGRNQty));
            alParameters.Add(new SqlParameter("@InvoicedQuantity", decInvoicedQty));
            alParameters.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
            alParameters.Add(new SqlParameter("@OpeningStock", decOpeningStock));
            alParameters.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));
            alParameters.Add(new SqlParameter("@DemoQuantity", decDemoQty));
            alParameters.Add(new SqlParameter("@LastModifiedBy", objclsDTOSalesReturnMaster.intCreatedBy));
            alParameters.Add(new SqlParameter("@LastModifiedDate", ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")));
            objclsConnection.ExecuteNonQueryWithTran("spInvStockMaster", alParameters);
            return true;
        }

        public decimal ConvertQtyToBaseUnitQty(int intUomID, int intItemId, decimal decQuantity)
        {
            DataTable dtUomDetails = GetUomConversionValues(intUomID, intItemId);
            if (dtUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                    decQuantity = decQuantity / decConversionValue;
                else if (intConversionFactor == 2)
                    decQuantity = decQuantity * decConversionValue;
            }
            return decQuantity;
        }

        private DataTable DisplayCancellationDetails()
        {
            //function for displaying cancellation details

            ArrayList prmSalesReturn = new ArrayList();
            prmSalesReturn.Add(new SqlParameter("Mode", 18));
            prmSalesReturn.Add(new SqlParameter("SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));
            return objclsConnection.ExecuteDataTable("spInvSalesReturn", prmSalesReturn);
        }

        public void SaveCancellationDetails()
        {
            // function for saving cancellation details

            ArrayList prmSalesReturn = new ArrayList();

            prmSalesReturn.Add(new SqlParameter("CancelledBy", objclsDTOSalesReturnMaster.intCancelledBy));
            prmSalesReturn.Add(new SqlParameter("CancellationDate", objclsDTOSalesReturnMaster.strCancellationDate));
            prmSalesReturn.Add(new SqlParameter("Description", objclsDTOSalesReturnMaster.strCancellationDescription));
            prmSalesReturn.Add(new SqlParameter("CompanyID", objclsDTOSalesReturnMaster.intCompanyID));
            prmSalesReturn.Add(new SqlParameter("Mode", 16));
            prmSalesReturn.Add(new SqlParameter("SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));
            objclsConnection.ExecuteNonQueryWithTran("spInvSalesReturn", prmSalesReturn);
        }

        private void DeleteCancellationDetails()
        {
            // function for deleting cancellation details
            ArrayList prmSalesReturn = new ArrayList();
            prmSalesReturn.Add(new SqlParameter("Mode", 17));
            prmSalesReturn.Add(new SqlParameter("SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));
            objclsConnection.ExecuteNonQueryWithTran("spInvSalesReturn", prmSalesReturn);
        }
               
        public DataSet GetSalesReturnReport()//email
        {
            ArrayList prmSalesReturn = new ArrayList();
            prmSalesReturn.Add(new SqlParameter("@Mode", 19));
            prmSalesReturn.Add(new SqlParameter("@SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));
            return objclsConnection.ExecuteDataSet("spInvSalesReturn", prmSalesReturn);
        }
        public bool GetCompanyAccount(int intTransactionType)
        {
            // function for Geting Company Account
            object objOutAccountID = 0;
            ArrayList prmSales = new ArrayList();
            prmSales.Add(new SqlParameter("@Mode", 18));
            prmSales.Add(new SqlParameter("@CompanyID", objclsDTOSalesReturnMaster.intCompanyID));
            prmSales.Add(new SqlParameter("@TransactionType", intTransactionType));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmSales.Add(objParam);

            if (objclsConnection.ExecuteNonQuery("spInvPurchaseModuleFunctions", prmSales, out objOutAccountID) != 0)
            {
                ClsCommonSettings.PintSIDebitHeadID = (int)objOutAccountID;
                if ((int)objOutAccountID > 0) return true;
            }
            return false;
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 4));
            prmCommon.Add(new SqlParameter("@RoleID", intRoleID));
            prmCommon.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmCommon.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable("STPermissionSettings", prmCommon);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 12));
            prmCommon.Add(new SqlParameter("@RoleID", intRoleID));
            prmCommon.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmCommon.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable("STPermissionSettings", prmCommon);
        }

        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 14));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@LocationWise", ClsCommonSettings.PblnIsLocationWise));
            return objclsConnection.ExecuteDataTable("spInvItemLocationTransfer", alParameters).Rows[0];
        }

        public decimal GetDefaultLocationQuantity(int intItemID,long lngBatchID, int intWarehouseID,bool blnIsDamaged)
        {
            ArrayList alParameters = new ArrayList();
            DataRow dr = GetItemDefaultLocation(intItemID, intWarehouseID);
            decimal decQuantity = 0;
            alParameters.Add(new SqlParameter("@Mode", 13));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", lngBatchID));
            alParameters.Add(new SqlParameter("@LocationID", dr["LocationID"].ToInt32()));
            alParameters.Add(new SqlParameter("@RowID", dr["RowID"].ToInt32()));
            alParameters.Add(new SqlParameter("@BlockID", dr["BlockID"].ToInt32()));
            alParameters.Add(new SqlParameter("@LotID", dr["LotID"].ToInt32()));
            DataTable datLocationDetails = objclsConnection.ExecuteDataTable("spInvItemLocationTransfer", alParameters);
            if (datLocationDetails.Rows.Count >0)
            {
                if (!blnIsDamaged)
                {
                    if (datLocationDetails.Rows[0]["Quantity"] != DBNull.Value)
                        decQuantity = datLocationDetails.Rows[0]["Quantity"].ToDecimal();
                }
                else
                {
                    if (datLocationDetails.Rows[0]["DamagedQuantity"] != DBNull.Value)
                        decQuantity = datLocationDetails.Rows[0]["DamagedQuantity"].ToDecimal();
                }
            }
            return decQuantity;
        }

        private bool UpdateItemLocationDetails(clsDTOLocationDetails objLocationDetails, int intWarehouseID, bool blnIsDeletion, bool blnIsDamaged)
        {
            ArrayList alParameters = new ArrayList();
            if (!blnIsDeletion)
                alParameters.Add(new SqlParameter("@Mode", 1));
            else
                alParameters.Add(new SqlParameter("@Mode", 2));
            if (!blnIsDamaged)
                alParameters.Add(new SqlParameter("@Type", 1));
            else
                alParameters.Add(new SqlParameter("@Type", 2));
            alParameters.Add(new SqlParameter("@CompanyID", objclsDTOSalesReturnMaster.intCompanyID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.intItemID));
            alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.lngBatchID));
            alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.intLocationID));
            alParameters.Add(new SqlParameter("@RowID", objLocationDetails.intRowID));
            alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.intBlockID));
            alParameters.Add(new SqlParameter("@LotID", objLocationDetails.intLotID));
            alParameters.Add(new SqlParameter("@Quantity", objLocationDetails.decQuantity));
            alParameters.Add(new SqlParameter("@DamagedQuantity", objLocationDetails.decQuantity));
            objclsConnection.ExecuteNonQueryWithTran("spInvItemLocationTransfer", alParameters);
            return true;
        }
        public string StockValidation(bool isDeletion)
        {
            DataTable dtSalesReturnDetails = new DataTable();

            int intWarehouseID = 0;
            //DataTable datIssueDetails = FillCombos(new string[] { "IM.WarehouseID", "InvItemIssueMaster IM  INNER JOIN InvItemIssueReferenceDetails IRD ON IM.ItemIssueID=IRD.ItemIssueID", "IM.OrderTypeID = " + (int)OperationOrderType.DNOTSalesInvoice + " And IRD.ReferenceID = " + objclsDTOSalesReturnMaster.intSalesInvoiceID });
            
            dtSalesReturnDetails = GetReturnDetDetails(objclsDTOSalesReturnMaster.intSalesReturnID);
            List<int> lstupdateIndices = new List<int>();
            if (isDeletion)
            {
                DataTable datQtyBatchWise = new DataTable();
                datQtyBatchWise.Columns.Add("ItemID");
                datQtyBatchWise.Columns.Add("BatchID");
                datQtyBatchWise.Columns.Add("AvailableQuantity",typeof(decimal));
                datQtyBatchWise.Columns.Add("ReturnQuantity", typeof(decimal));
                datQtyBatchWise.Columns.Add("ItemName");
                for (int i = 0; i < dtSalesReturnDetails.Rows.Count; i++)
                {
                    clsDTOSalesReturnDetails objDTOSalesReturnDetails = new clsDTOSalesReturnDetails();
                   
                    bool blnIsDamaged = false;
                    if (Convert.ToBoolean(dtSalesReturnDetails.Rows[i]["IsGroup"]) == true)
                    {
                        DataTable dtGroupreturnDetails = GetItemGroupReturnDetails();
                        foreach (DataRow drGrp in dtGroupreturnDetails.Rows)
                        {
                            objDTOSalesReturnDetails.intBatchID = Convert.ToInt64(drGrp["BatchID"]);
                            objDTOSalesReturnDetails.intItemID = Convert.ToInt32(drGrp["ItemID"]);
                            intWarehouseID = dtSalesReturnDetails.Rows[i]["WarehouseID"].ToInt32();

                            DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(drGrp["UOMID"]), Convert.ToInt64(drGrp["ItemID"]));
                            decimal decOldQuantity = Convert.ToDecimal(drGrp["Quantity"]);
                            if (dtGetUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                    decOldQuantity = decOldQuantity / decConversionValue;
                                else if (intConversionFactor == 2)
                                    decOldQuantity = decOldQuantity * decConversionValue;
                            }

                            if (dtSalesReturnDetails.Rows[i]["ReasonID"].ToInt32() == (int)ReasonReferences.Damaged)
                                blnIsDamaged = true;
                         
                            decimal AvailableQty = GetAvailableQtyForReturn(objDTOSalesReturnDetails.intItemID, objDTOSalesReturnDetails.intBatchID, intWarehouseID, blnIsDamaged);
                            if (blnIsDamaged)
                            {
                                AvailableQty = AvailableQty + decOldQuantity;
                            }
                            else
                            {
                                AvailableQty = GetAvailableQtyForReturn(objDTOSalesReturnDetails.intItemID, objDTOSalesReturnDetails.intBatchID, intWarehouseID, blnIsDamaged);
                            }
                             datQtyBatchWise.Rows.Add();
                            datQtyBatchWise.Rows[datQtyBatchWise.Rows.Count - 1]["ItemID"] = objDTOSalesReturnDetails.intItemID;
                            datQtyBatchWise.Rows[datQtyBatchWise.Rows.Count - 1]["BatchID"] = objDTOSalesReturnDetails.intBatchID;
                            datQtyBatchWise.Rows[datQtyBatchWise.Rows.Count - 1]["AvailableQuantity"] = AvailableQty;
                            datQtyBatchWise.Rows[datQtyBatchWise.Rows.Count - 1]["ReturnQuantity"] = decOldQuantity;
                            datQtyBatchWise.Rows[datQtyBatchWise.Rows.Count - 1]["ItemName"] = dtSalesReturnDetails.Rows[i]["ItemName"];
                            if (AvailableQty - decOldQuantity < 0)
                            {

                                return Convert.ToString(dtSalesReturnDetails.Rows[i]["ItemName"]);
                            }
                        }

                    }
                    else
                    {
                        objDTOSalesReturnDetails.intBatchID = Convert.ToInt64(dtSalesReturnDetails.Rows[i]["BatchID"]);
                        objDTOSalesReturnDetails.intItemID = Convert.ToInt32(dtSalesReturnDetails.Rows[i]["ItemID"]);
                        intWarehouseID = dtSalesReturnDetails.Rows[i]["WarehouseID"].ToInt32();

                        DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtSalesReturnDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtSalesReturnDetails.Rows[i]["ItemID"]));
                        decimal decOldQuantity = Convert.ToDecimal(dtSalesReturnDetails.Rows[i]["ReturnQuantity"]);
                        if (dtGetUomDetails.Rows.Count > 0)
                        {
                            int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                            decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                            if (intConversionFactor == 1)
                                decOldQuantity = decOldQuantity / decConversionValue;
                            else if (intConversionFactor == 2)
                                decOldQuantity = decOldQuantity * decConversionValue;
                        }
                      
                        if (dtSalesReturnDetails.Rows[i]["ReasonID"].ToInt32() == (int)ReasonReferences.Damaged)
                            blnIsDamaged = true;
                        decimal AvailableQty = GetAvailableQtyForReturn(objDTOSalesReturnDetails.intItemID, objDTOSalesReturnDetails.intBatchID, intWarehouseID, blnIsDamaged);
                        if (blnIsDamaged)
                        {
                            AvailableQty = AvailableQty + decOldQuantity;
                        }
                        else
                        {
                             AvailableQty = GetAvailableQtyForReturn(objDTOSalesReturnDetails.intItemID, objDTOSalesReturnDetails.intBatchID, intWarehouseID, blnIsDamaged);
                        }
                         datQtyBatchWise.Rows.Add();
                        datQtyBatchWise.Rows[datQtyBatchWise.Rows.Count - 1]["ItemID"] = objDTOSalesReturnDetails.intItemID;
                        datQtyBatchWise.Rows[datQtyBatchWise.Rows.Count - 1]["BatchID"] = objDTOSalesReturnDetails.intBatchID;
                        datQtyBatchWise.Rows[datQtyBatchWise.Rows.Count - 1]["AvailableQuantity"] = AvailableQty;
                        datQtyBatchWise.Rows[datQtyBatchWise.Rows.Count - 1]["ReturnQuantity"] = decOldQuantity;
                        datQtyBatchWise.Rows[datQtyBatchWise.Rows.Count - 1]["ItemName"] = dtSalesReturnDetails.Rows[i]["ItemName"];
                        //if (GetAvailableQtyForReturn(objDTOSalesReturnDetails.intItemID, objDTOSalesReturnDetails.intBatchID, intWarehouseID, blnIsDamaged) - decOldQuantity < 0)
                        if (AvailableQty - decOldQuantity < 0)
                        {
                            return Convert.ToString(dtSalesReturnDetails.Rows[i]["ItemName"]);
                        }
                    }
                   
                }
                if (datQtyBatchWise != null && datQtyBatchWise.Rows.Count > 0)
                {
                    foreach (DataRow drQty in datQtyBatchWise.Rows)
                    {
                        decimal decReturnQuantity = datQtyBatchWise.Compute("SUM(ReturnQuantity)", "ItemID=" + Convert.ToInt32(drQty["ItemID"]) + " AND BatchID=" + Convert.ToInt64(drQty["BatchID"]) + "").ToDecimal();
                        //decimal decAvailableQty = Convert.ToDecimal(drQty["AvailableQuantity"]);
                        decimal decAvailableQty=datQtyBatchWise.Compute("SUM(AvailableQuantity)", "ItemID=" + Convert.ToInt32(drQty["ItemID"]) + " AND BatchID=" + Convert.ToInt64(drQty["BatchID"]) + "").ToDecimal();
                        if (decAvailableQty - decReturnQuantity < 0)
                        {
                            return Convert.ToString(drQty["ItemName"]);
                        }
                    }
                }

            }
            else
            {
                for (int i = 0; i < dtSalesReturnDetails.Rows.Count; i++)
                {
                    bool blnExists = false;
                    for (int j = 0; j < objclsDTOSalesReturnMaster.lstSalesReturnDetails.Count; j++)
                    {
                        if (objclsDTOSalesReturnMaster.lstSalesReturnDetails[j].intBatchID == dtSalesReturnDetails.Rows[i]["BatchID"].ToInt64() && objclsDTOSalesReturnMaster.lstSalesReturnDetails[j].intReasonID == dtSalesReturnDetails.Rows[i]["ReasonID"].ToInt32())
                        {
                            if (objclsDTOSalesReturnMaster.lstSalesReturnDetails[j].blnIsGroup == true)
                            {
                                for (int k = 0; k < objclsDTOSalesReturnMaster.lstSalesReturnDetails[j].lstItemGroupReturnDetails.Count; k++)
                                {
                                    intWarehouseID = dtSalesReturnDetails.Rows[j]["WarehouseID"].ToInt32();
                                    DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtSalesReturnDetails.Rows[j]["UOMID"]), Convert.ToInt32(dtSalesReturnDetails.Rows[j]["ItemID"]));
                                    decimal decOldQuantity = Convert.ToDecimal(dtSalesReturnDetails.Rows[j]["ReturnQuantity"]);
                                    if (dtGetUomDetails.Rows.Count > 0)
                                    {
                                        int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                        decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                        if (intConversionFactor == 1)
                                            decOldQuantity = decOldQuantity / decConversionValue;
                                        else if (intConversionFactor == 2)
                                            decOldQuantity = decOldQuantity * decConversionValue;
                                    }
                                    blnExists = true;

                                    bool blnIsDamaged = false;
                                    if (dtSalesReturnDetails.Rows[i]["ReasonID"].ToInt32() == (int)ReasonReferences.Damaged)
                                        blnIsDamaged = true;
                                    if ((GetAvailableQtyForReturn(objclsDTOSalesReturnMaster.lstSalesReturnDetails[j].intItemID, objclsDTOSalesReturnMaster.lstSalesReturnDetails[j].intBatchID, intWarehouseID, blnIsDamaged) - decOldQuantity) < 0)
                                    {
                                        //if ((GetDefaultLocationQuantity(objclsDTOSalesReturnMaster.lstSalesReturnDetails[j].intItemID, objclsDTOSalesReturnMaster.lstSalesReturnDetails[j].intBatchID, intWarehouseID, blnIsDamaged) - decOldQuantity) < 0)
                                        //{
                                        return Convert.ToString(dtSalesReturnDetails.Rows[i]["ItemName"]);
                                    }
                                }


                            }
                            else
                            {
                                intWarehouseID = dtSalesReturnDetails.Rows[j]["WarehouseID"].ToInt32();
                                DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtSalesReturnDetails.Rows[j]["UOMID"]), Convert.ToInt32(dtSalesReturnDetails.Rows[j]["ItemID"]));
                                decimal decOldQuantity = Convert.ToDecimal(dtSalesReturnDetails.Rows[j]["ReturnQuantity"]);
                                if (dtGetUomDetails.Rows.Count > 0)
                                {
                                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                    if (intConversionFactor == 1)
                                        decOldQuantity = decOldQuantity / decConversionValue;
                                    else if (intConversionFactor == 2)
                                        decOldQuantity = decOldQuantity * decConversionValue;
                                }
                                blnExists = true;

                                bool blnIsDamaged = false;
                                if (dtSalesReturnDetails.Rows[i]["ReasonID"].ToInt32() == (int)ReasonReferences.Damaged)
                                    blnIsDamaged = true;
                                if ((GetAvailableQtyForReturn(objclsDTOSalesReturnMaster.lstSalesReturnDetails[j].intItemID, objclsDTOSalesReturnMaster.lstSalesReturnDetails[j].intBatchID, intWarehouseID, blnIsDamaged) - decOldQuantity) < 0)
                               {
                                //if ((GetDefaultLocationQuantity(objclsDTOSalesReturnMaster.lstSalesReturnDetails[j].intItemID, objclsDTOSalesReturnMaster.lstSalesReturnDetails[j].intBatchID, intWarehouseID, blnIsDamaged) - decOldQuantity) < 0)
                                //{
                                    return Convert.ToString(dtSalesReturnDetails.Rows[i]["ItemName"]);
                                }
                            }
                        }
                    }
                    if (!blnExists)
                    {
                        clsDTOSalesReturnDetails objDTOSalesReturnDetails = new clsDTOSalesReturnDetails();
                        intWarehouseID = dtSalesReturnDetails.Rows[i]["WarehouseID"].ToInt32();
                        objDTOSalesReturnDetails.intBatchID = Convert.ToInt64(dtSalesReturnDetails.Rows[i]["BatchID"]);
                        objDTOSalesReturnDetails.intItemID = Convert.ToInt32(dtSalesReturnDetails.Rows[i]["ItemID"]);
                        DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtSalesReturnDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtSalesReturnDetails.Rows[i]["ItemID"]));
                        decimal decOldQuantity = Convert.ToDecimal(dtSalesReturnDetails.Rows[i]["ReturnQuantity"]);
                        if (dtGetUomDetails.Rows.Count > 0)
                        {
                            int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                            decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                            if (intConversionFactor == 1)
                                decOldQuantity = decOldQuantity / decConversionValue;
                            else if (intConversionFactor == 2)
                                decOldQuantity = decOldQuantity * decConversionValue;
                        }

                        bool blnIsDamaged = false;
                        if (dtSalesReturnDetails.Rows[i]["ReasonID"].ToInt32() == (int)ReasonReferences.Damaged)
                            blnIsDamaged = true;

                        if ((GetDefaultLocationQuantity(dtSalesReturnDetails.Rows[i]["ItemID"].ToInt32(), dtSalesReturnDetails.Rows[i]["BatchID"].ToInt64(), intWarehouseID, blnIsDamaged) - decOldQuantity)< 0)
                        {
                            return Convert.ToString(dtSalesReturnDetails.Rows[i]["ItemName"]);
                        }
                    }
                }
            }
            return null;
        }

        public string GetAlreadyReturnedQty(long lngSalesInvoiceID, int intItemID,long lngBatchID,bool blnIsGroup)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 21));
            alParameters.Add(new SqlParameter("@SalesInvoiceID", lngSalesInvoiceID));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", lngBatchID));
            alParameters.Add(new SqlParameter("@IsGroup", blnIsGroup));
            DataTable datReturnDetails = objclsConnection.ExecuteDataTable("spInvSalesReturn", alParameters);
            if(datReturnDetails.Rows.Count > 0)
            return datReturnDetails.Rows[0]["ReturnedQty"].ToString();
            return "";
        }

        public DataTable GetBatchWiseIssueDetails(long lngSalesInvoiceID, int intItemID, long lngBatchID, bool blnIsGroup)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 22));
            alParameters.Add(new SqlParameter("@SalesInvoiceID", lngSalesInvoiceID));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", lngBatchID));
            alParameters.Add(new SqlParameter("@IsGroup", blnIsGroup));
            return objclsConnection.ExecuteDataTable("spInvSalesReturn", alParameters);
        }
        public bool GetCompanyAccount(int intTransactionType, int intCompanyID)
        {
            // function for Geting Company Account
            ArrayList prmPurchase = new ArrayList();
            object objOutAccountID = 0;
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 18));
            prmPurchase.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPurchase.Add(new SqlParameter("@TransactionType", intTransactionType));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPurchase.Add(objParam);

            if (objclsConnection.ExecuteNonQuery("spInvPurchaseModuleFunctions", prmPurchase, out objOutAccountID) != 0)
            {
                ClsCommonSettings.PintSIDebitHeadID = (int)objOutAccountID;
                if ((int)objOutAccountID > 0) return true;
            }
            return false;
        }
        public Int32 GetCompanyAccountID(int intTransactionType, int intCompanyID)
        {
            // function for Geting Company Account
            ArrayList prmPurchase = new ArrayList();
            object objOutAccountID = 0;
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 18));
            prmPurchase.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPurchase.Add(new SqlParameter("@TransactionType", intTransactionType));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPurchase.Add(objParam);

            if (objclsConnection.ExecuteNonQuery("spInvPurchaseModuleFunctions", prmPurchase, out objOutAccountID) != 0)
            {
                ClsCommonSettings.PintSIDebitHeadID = (int)objOutAccountID;
                if ((int)objOutAccountID > 0) return (int)objOutAccountID;
            }
            return 0;
        }
        public DataTable GetBatch(int intSalesInvoiceID, int intItemID)
        {
            ArrayList prmBATCH = new ArrayList();
            prmBATCH.Add(new SqlParameter("@Mode", 26));
            prmBATCH.Add(new SqlParameter("@SalesInvoiceID", intSalesInvoiceID));
            prmBATCH.Add(new SqlParameter("@ItemID", intItemID));

            return objclsConnection.ExecuteDataTable("spInvSalesReturn", prmBATCH);
        }
        public DataTable GetWareHouse(long lngSaleInvoiceID, int intItemID, long lngBatchID)
        {
            ArrayList prmWareHouse = new ArrayList();
            prmWareHouse.Add(new SqlParameter("@Mode",28));
            prmWareHouse.Add(new SqlParameter("@SalesInvoiceID", lngSaleInvoiceID));
            prmWareHouse.Add(new SqlParameter("@ItemID",intItemID));
            prmWareHouse.Add(new SqlParameter("@BatchID", lngBatchID));

             return objclsConnection.ExecuteDataTable("spInvSalesReturn",prmWareHouse);
        }
        public DataTable GetItemGroupDetails(int intItemGroupID)
        {
            ArrayList prmItemGroupIssueDetails = new ArrayList();
            prmItemGroupIssueDetails.Add(new SqlParameter("@Mode", 29));
            prmItemGroupIssueDetails.Add(new SqlParameter("@ItemID", intItemGroupID));
            return objclsConnection.ExecuteDataTable("spInvSalesReturn", prmItemGroupIssueDetails);
        }
        public DataTable BatchLessItems(int intItemID, int intWareHouseID)
        {
            ArrayList prmReturn = new ArrayList();
            prmReturn.Add(new SqlParameter("@Mode", 30));
            prmReturn.Add(new SqlParameter("@ItemID", intItemID));

            prmReturn.Add(new SqlParameter("@WarehouseID", intWareHouseID));

            return objclsConnection.ExecuteDataTable("spInvSalesReturn", prmReturn);
        }
        public DataTable BatchLessItemsDamaged(int intItemID, int intWareHouseID)
        {
            ArrayList prmReturn = new ArrayList();
            prmReturn.Add(new SqlParameter("@Mode", 40));
            prmReturn.Add(new SqlParameter("@ItemID", intItemID));

            prmReturn.Add(new SqlParameter("@WarehouseID", intWareHouseID));

            return objclsConnection.ExecuteDataTable("spInvSalesReturn", prmReturn);
        }
        public DataTable GetItemGroupReturnDetails()
        {
            //function for getting ItemGroup return Details
            ArrayList prmItemGroupReturnDetails = new ArrayList();
            prmItemGroupReturnDetails.Add(new SqlParameter("@Mode", 32));
            prmItemGroupReturnDetails.Add(new SqlParameter("@SalesReturnID", objclsDTOSalesReturnMaster.intSalesReturnID));
            return objclsConnection.ExecuteDataTable("spInvSalesReturn", prmItemGroupReturnDetails);
        }
        //GET GROUP ISSUE DETAILS CORRESPONDING TO A SALES INVOICE ID
        public DataTable GetGroupIssueDetails(int intGroupItemID ,int intGroupID)
        {
            //function for getting ItemGroup return Details
            ArrayList prmGroupIssueDetails = new ArrayList();
            prmGroupIssueDetails.Add(new SqlParameter("@Mode", 34));
            prmGroupIssueDetails.Add(new SqlParameter("@SalesInvoiceID", objclsDTOSalesReturnMaster.intSalesInvoiceID));
            prmGroupIssueDetails.Add(new SqlParameter("@GroupItemID", intGroupItemID));

            prmGroupIssueDetails.Add(new SqlParameter("@GroupID", intGroupID));
            return objclsConnection.ExecuteDataTable("spInvSalesReturn", prmGroupIssueDetails);
        }
        //get issue details of  MIN item CORRESPONDING TO A SALES INVOICE ID
        public DataTable GetIssueDetailsBatchlessItems(int intItemID)
        {
            //function for getting ItemGroup return Details
            ArrayList prmGroupIssueDetails = new ArrayList();
            prmGroupIssueDetails.Add(new SqlParameter("@Mode", 35));
            prmGroupIssueDetails.Add(new SqlParameter("@SalesInvoiceID", objclsDTOSalesReturnMaster.intSalesInvoiceID));
            prmGroupIssueDetails.Add(new SqlParameter("@ItemID", intItemID));

            return objclsConnection.ExecuteDataTable("spInvSalesReturn", prmGroupIssueDetails);
        }
        public decimal GetAvailableQtyForReturn(int intItemID, long lngBatchID, int intWarehouseID, bool blnIsDamaged)
        {
            ArrayList alParameters = new ArrayList();

            decimal decQuantity = 0;
            alParameters.Add(new SqlParameter("@Mode", 36));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", lngBatchID));

            DataTable datDetails = objclsConnection.ExecuteDataTable("spInvSalesReturn", alParameters);
            if (datDetails.Rows.Count > 0)
            {

                decQuantity = datDetails.Rows[0]["AvailableQty"].ToDecimal();



            }
            return decQuantity;
        }
        public DataTable GetGroupIssueDetailsEditMode(int intGroupItemID, int intGroupID)
        {
            //function for getting ItemGroup return Details
            ArrayList prmGroupIssueDetails = new ArrayList();
            prmGroupIssueDetails.Add(new SqlParameter("@Mode", 39));
            prmGroupIssueDetails.Add(new SqlParameter("@SalesInvoiceID", objclsDTOSalesReturnMaster.intSalesInvoiceID));
            prmGroupIssueDetails.Add(new SqlParameter("@GroupItemID", intGroupItemID));

            prmGroupIssueDetails.Add(new SqlParameter("@GroupID", intGroupID));
            return objclsConnection.ExecuteDataTable("spInvSalesReturn", prmGroupIssueDetails);
        }
        public bool IsReturnExists(long lngSalesInvoiceID, long lngSalesReturnID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 41));
            alParameters.Add(new SqlParameter("@SalesInvoiceID", lngSalesInvoiceID));
            alParameters.Add(new SqlParameter("@SalesReturnID", lngSalesReturnID));
            object obj = objclsConnection.ExecuteScalar("spInvSalesReturn", alParameters);
            return obj.ToInt32() > 0;
        }
        public decimal getTaxValue(int TaxScheme)
        {
            return this.objclsConnection.ExecuteScalar("Select AccValue From AccAccountMaster where AccountID='" + TaxScheme + "'").ToDecimal();
        }
       
        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (dat != null)
                dat.Dispose();

            if (alParameters != null)
                alParameters = null;
        }
        #endregion
    }
}