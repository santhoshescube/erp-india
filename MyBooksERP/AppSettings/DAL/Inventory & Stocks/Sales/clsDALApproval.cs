﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;

using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALApproval
    {
        public clsDTOApproval objclsDTOApproval { get; set; }

        public DataLayer objclsConnection { get; set; }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataTable GetApprovalDetails(int intFormType,string strCondition,int intStatusID)
        {
            ArrayList prmApproval = new ArrayList();
            prmApproval.Add(new SqlParameter("@Mode", intFormType));
            prmApproval.Add(new SqlParameter("@CompanyID", objclsDTOApproval.intCompanyID));
            prmApproval.Add(new SqlParameter("@EmployeeID", objclsDTOApproval.intEmployeeID));
            prmApproval.Add(new SqlParameter("@FromDate", objclsDTOApproval.strFromDate));
            prmApproval.Add(new SqlParameter("@ToDate", objclsDTOApproval.strToDate));
            prmApproval.Add(new SqlParameter("@SearchCondition", strCondition));
            prmApproval.Add(new SqlParameter("@StatusID", intStatusID));

            if (intFormType == (int)ApprovalType.PurchaseQuotation)
            {
                prmApproval.Add(new SqlParameter("@OperationTypeID", Convert.ToString((int)OperationType.PurchaseQuotation)));
                prmApproval.Add(new SqlParameter("@RejectedStatusID", Convert.ToString((int)OperationStatusType.PQuotationRejected)));
            }
            else if (intFormType == (int)ApprovalType.PurchaseOrder)
            {
                prmApproval.Add(new SqlParameter("@OperationTypeID", Convert.ToString((int)OperationType.PurchaseOrder)));
                prmApproval.Add(new SqlParameter("@RejectedStatusID", Convert.ToString((int)OperationStatusType.POrderRejected)));
            }
            else if (intFormType == (int)ApprovalType.SalesQuotation)
            {
                prmApproval.Add(new SqlParameter("@OperationTypeID", Convert.ToString((int)OperationType.SalesQuotation)));
                prmApproval.Add(new SqlParameter("@RejectedStatusID", Convert.ToString((int)OperationStatusType.SQuotationRejected)));
            }
            else if (intFormType == (int)ApprovalType.SalesOrder)
            {
                prmApproval.Add(new SqlParameter("@OperationTypeID", Convert.ToString((int)OperationType.SalesOrder)));
                prmApproval.Add(new SqlParameter("@RejectedStatusID", Convert.ToString((int)OperationStatusType.SOrderRejected)));
            }
            else if (intFormType == (int)ApprovalType.StockTransfer)
            {
                prmApproval.Add(new SqlParameter("@OperationTypeID", Convert.ToString((int)OperationType.StockTransfer)));
                prmApproval.Add(new SqlParameter("@RejectedStatusID", Convert.ToString((int)OperationStatusType.STRejected)));
            }

            return objclsConnection.ExecuteDataTable("spInvApprovalDetails", prmApproval);
        }

        public DataTable GetCompanyByPermission(int RoleID, int MenuID, int ControlID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 4));
            prmCommon.Add(new SqlParameter("@RoleID", RoleID));
            prmCommon.Add(new SqlParameter("@MenuID", MenuID));
            prmCommon.Add(new SqlParameter("@ControlID", ControlID));
            return objclsConnection.ExecuteDataTable("STPermissionSettings", prmCommon);
        }

        public DataTable GetEmployeeByPermission(int RoleID, int MenuID, int ControlID, int CompanyID)
        {
            ArrayList prmCommon = new ArrayList();
            if (CompanyID == 0)
                prmCommon.Add(new SqlParameter("@Mode", 5));
            else
                prmCommon.Add(new SqlParameter("@Mode", 6));
            prmCommon.Add(new SqlParameter("@RoleID", RoleID));
            prmCommon.Add(new SqlParameter("@MenuID", MenuID));
            prmCommon.Add(new SqlParameter("@ControlID", ControlID));
            prmCommon.Add(new SqlParameter("@CompanyID", CompanyID));
            return objclsConnection.ExecuteDataTable("STPermissionSettings", prmCommon);
        }

        public DataTable GetSuggestions(string strCondtition)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 9));
            alParameters.Add(new SqlParameter("@SearchCondition", strCondtition));
            return objclsConnection.ExecuteDataTable("spInvApprovalDetails", alParameters);
        }
    }
}
