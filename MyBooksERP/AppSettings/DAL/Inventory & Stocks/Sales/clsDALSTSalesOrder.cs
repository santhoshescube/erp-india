﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;

using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALSTSalesOrder : IDisposable
    {
        ArrayList prmSales;   
        DataTable dat;
        public clsDTOSTSalesOrder objclsDTOSTSalesMaster { get; set; }
        public DataLayer objclsConnection { get; set; }
        ClsCommonUtility MobjClsCommonUtility; // object of clsCommonUtility
        string strProcedureSalesQuotation = "spInvSalesQuotation";
        string strProcedureSalesOrder = "spInvSalesOrder";
        string strProcedureSalesInvoice = "spInvSalesInvoice";
        string strProcedureStockMaster = "spInvStockMaster";
        string strProcedurePOS = "spInvPOS";
        string strProcedureReceiptAndPayment = "spAccReceiptsAndPayments";
        string strProcedureAlert = "spAlert";
        string strProcedureDocumentMaster = "spDocuments";
        string strProcedurePurchaseQuotation = "spInvPurchaseQuotation";
        string strProcedurePermissionSettings = "spInvPermissionSettings";
        string strProcedurePurchaseModuleFunctions = "spInvPurchaseModuleFunctions";
        string strProcedureAlertRoleSetting = "spAlertRoleSetting";
        string strProcedureAlertUserSetting = "spAlertUserSetting";
        string strProcedureAlertSetting = "spAlertSetting";

        string strTableSalesQuotation = "InvSalesQuotationMaster";
        string strTableSalesOrder = "InvSalesOrderMaster";
        string strTableSalesInvoice = "InvSalesInvoiceMaster";
        //string strTableReceiptAndPayment = "InvReceiptAndPayment";
        string strTableReceiptAndPayment = "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID";
        string strTableVendorAddress = "InvVendorAddress";
        string strTableDocumentMaster = "DocDocumentMaster";
        string strTableMandatoryDocumentTypes = "MandatoryDocumentTypes";
        string strTableDocumentTypeReference = "DocumentTypeReference";
        string strTableItemGroupDetails = "InvItemGroupDetails";

        public clsDALSTSalesOrder(DataLayer objDataLayer)
        {
            MobjClsCommonUtility = new ClsCommonUtility();
            objclsConnection = objDataLayer;
            MobjClsCommonUtility.PobjDataLayer = objclsConnection;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public Int64 GetLastQuotationNo(ref Int64 intSalesQuotationCount, int intCompanyID)
        {
            Int64 intLastQtnNo = 0;

            dat = new DataTable();
            prmSales = new ArrayList();

            prmSales.Add(new SqlParameter("@Mode", 8));
            prmSales.Add(new SqlParameter("@CompanyID", intCompanyID));
            dat = objclsConnection.ExecuteDataTable(strProcedureSalesQuotation, prmSales);

            if (dat.Rows.Count > 0)
            {
                intLastQtnNo = Convert.ToInt32(dat.Rows[0]["LastQtnNo"]);//LastQuotationNo
                intSalesQuotationCount = Convert.ToInt32(dat.Rows[0]["SalesQuotationCount"]);//SalesQuotationCount
            }
            return intLastQtnNo + 1;
        }

        public Int64 GetLastOrderNo(ref Int64 intSalesOrderCount, int intCompanyID)
        {
            Int64 intLastOrderNo = 0;

            dat = new DataTable();
            prmSales = new ArrayList();

            prmSales.Add(new SqlParameter("@Mode", 8));
            prmSales.Add(new SqlParameter("@CompanyID", intCompanyID));

            dat = objclsConnection.ExecuteDataTable(strProcedureSalesOrder, prmSales);

            if (dat.Rows.Count > 0)
            {
                intLastOrderNo = Convert.ToInt32(dat.Rows[0]["LastOrderNo"]);//LastOrderNo
                intSalesOrderCount = Convert.ToInt32(dat.Rows[0]["SalesOrderCount"]);//SalesOrderCount
            }
            return intLastOrderNo + 1;
        }

        public Int64 GetLastInvoiceNo(ref Int64 intSalesInvoiceCount, int intCompanyID)
        {
            Int64 intLastInvoiceNo = 0;

            dat = new DataTable();
            prmSales = new ArrayList();

            prmSales.Add(new SqlParameter("@Mode", 10));
            prmSales.Add(new SqlParameter("@CompanyID", intCompanyID));

            dat = objclsConnection.ExecuteDataTable(strProcedureSalesInvoice, prmSales);

            if (dat.Rows.Count > 0)
            {
                intLastInvoiceNo = Convert.ToInt32(dat.Rows[0]["LastInvoiceNo"]);//LastInvoiceNo
                intSalesInvoiceCount = Convert.ToInt32(dat.Rows[0]["SalesInvoiceCount"]);//SalesInvoiceCount
            }
            return intLastInvoiceNo + 1;
        }

        public DataTable GetQuotationNumbers()
        {   // Get QuotationNumbers For searching
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 5));
            return objclsConnection.ExecuteDataTable(strProcedureSalesQuotation, prmCommon);
        }

        public DataTable GetOrderNumbers()
        {   // Get Order Number For searching
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 5));
            return objclsConnection.ExecuteDataTable(strProcedureSalesOrder, prmCommon);
        }

        public DataTable GetInvoiceNumbers()
        {   // Get Invoice Number For searching
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 5));
            return objclsConnection.ExecuteDataTable(strProcedureSalesInvoice, prmCommon);
        }

        public bool GetQuotationMasterDetails(Int64 intQuotationID)
        {   //Quotation
            ArrayList prmQtn = new ArrayList();

            prmQtn.Add(new SqlParameter("@Mode", 1));
            prmQtn.Add(new SqlParameter("@SalesQuotationID", intQuotationID));
            prmQtn.Add(new SqlParameter("@OperationTypeID", (int)OperationType.SalesQuotation));
            DataTable datQuotation = objclsConnection.ExecuteDataTable(strProcedureSalesQuotation, prmQtn);

            if (datQuotation.Rows.Count > 0)
            {
                objclsDTOSTSalesMaster.intSalesQuotationID = intQuotationID;

                objclsDTOSTSalesMaster.strSalesQuotationNo = Convert.ToString(datQuotation.Rows[0]["SalesQuotationNo"]);
                objclsDTOSTSalesMaster.strQuotationDate = Convert.ToString(datQuotation.Rows[0]["QuotationDate"]);

                if (datQuotation.Rows[0]["VendorID"] != DBNull.Value) objclsDTOSTSalesMaster.intVendorID = Convert.ToInt32(datQuotation.Rows[0]["VendorID"]); else objclsDTOSTSalesMaster.intVendorID = 0;
                if (datQuotation.Rows[0]["VendorName"] != DBNull.Value) objclsDTOSTSalesMaster.strVendorName = Convert.ToString(datQuotation.Rows[0]["VendorName"]); else objclsDTOSTSalesMaster.strVendorName = "";
                objclsDTOSTSalesMaster.strDueDate = Convert.ToString(datQuotation.Rows[0]["DueDate"]);
                if (datQuotation.Rows[0]["VendorAddID"] != DBNull.Value) objclsDTOSTSalesMaster.intVendorAddID = Convert.ToInt32(datQuotation.Rows[0]["VendorAddID"]); else objclsDTOSTSalesMaster.intVendorAddID = 0;
                if (datQuotation.Rows[0]["AddressName"] != DBNull.Value) objclsDTOSTSalesMaster.strAddressName = Convert.ToString(datQuotation.Rows[0]["AddressName"]); else objclsDTOSTSalesMaster.strAddressName = "";
                if (datQuotation.Rows[0]["PaymentTermsID"] != DBNull.Value) objclsDTOSTSalesMaster.intPaymentTermsID = Convert.ToInt32(datQuotation.Rows[0]["PaymentTermsID"]); else objclsDTOSTSalesMaster.intPaymentTermsID = 0;
                if (datQuotation.Rows[0]["Remarks"] != DBNull.Value) objclsDTOSTSalesMaster.strRemarks = Convert.ToString(datQuotation.Rows[0]["Remarks"]); else objclsDTOSTSalesMaster.strRemarks = "";

                if (datQuotation.Rows[0]["IsDiscountPercentage"] != DBNull.Value)
                {
                    objclsDTOSTSalesMaster.blnIsDiscountPercentage = datQuotation.Rows[0]["IsDiscountPercentage"].ToBoolean();

                    if (datQuotation.Rows[0]["GrandDiscountPercentage"] != DBNull.Value)
                        objclsDTOSTSalesMaster.decGrandDiscountPercentage = datQuotation.Rows[0]["GrandDiscountPercentage"].ToDecimal();
                    else
                        objclsDTOSTSalesMaster.decGrandDiscountPercentage = 0;

                    if (datQuotation.Rows[0]["GrandDiscountAmount"] != DBNull.Value)
                        objclsDTOSTSalesMaster.decGrandDiscountAmount = datQuotation.Rows[0]["GrandDiscountAmount"].ToDecimal();
                    else
                        objclsDTOSTSalesMaster.decGrandDiscountAmount = 0;
                }

                if (datQuotation.Rows[0]["CurrencyID"] != DBNull.Value) objclsDTOSTSalesMaster.intCurrencyID = Convert.ToInt32(datQuotation.Rows[0]["CurrencyID"]); else objclsDTOSTSalesMaster.intCurrencyID = 0;
                if (datQuotation.Rows[0]["GrandAmount"] != DBNull.Value) objclsDTOSTSalesMaster.decGrandAmount = Convert.ToDecimal(datQuotation.Rows[0]["GrandAmount"]); else objclsDTOSTSalesMaster.decGrandAmount = 0;
                if (datQuotation.Rows[0]["NetAmount"] != DBNull.Value) objclsDTOSTSalesMaster.decNetAmount = Convert.ToDecimal(datQuotation.Rows[0]["NetAmount"]); else objclsDTOSTSalesMaster.decNetAmount = 0;
                if (datQuotation.Rows[0]["ApprovedDate"] != DBNull.Value) objclsDTOSTSalesMaster.strApprovedDate = Convert.ToString(datQuotation.Rows[0]["ApprovedDate"]); else objclsDTOSTSalesMaster.strApprovedDate = "";
                if (datQuotation.Rows[0]["VerificationDescription"] != DBNull.Value) objclsDTOSTSalesMaster.strVerificationDescription = Convert.ToString(datQuotation.Rows[0]["VerificationDescription"]); else objclsDTOSTSalesMaster.strVerificationDescription = "";
                if (datQuotation.Rows[0]["ApprovedByName"] != DBNull.Value) objclsDTOSTSalesMaster.strApprovedBy = Convert.ToString(datQuotation.Rows[0]["ApprovedByName"]); else objclsDTOSTSalesMaster.strApprovedBy = "";

                if (datQuotation.Rows[0]["CreatedBy"] != DBNull.Value) objclsDTOSTSalesMaster.intCreatedBy = Convert.ToInt32(datQuotation.Rows[0]["CreatedBy"]); else objclsDTOSTSalesMaster.intCreatedBy = 0;
                if (datQuotation.Rows[0]["CreatedDate"] != DBNull.Value) objclsDTOSTSalesMaster.strCreatedDate = Convert.ToString(datQuotation.Rows[0]["CreatedDate"]); else objclsDTOSTSalesMaster.strCreatedDate = "";

                if (datQuotation.Rows[0]["CompanyID"] != DBNull.Value) objclsDTOSTSalesMaster.intCompanyID = Convert.ToInt32(datQuotation.Rows[0]["CompanyID"]); else objclsDTOSTSalesMaster.intCompanyID = 0;
                if (datQuotation.Rows[0]["StatusID"] != DBNull.Value) objclsDTOSTSalesMaster.intStatusID = Convert.ToInt32(datQuotation.Rows[0]["StatusID"]); else objclsDTOSTSalesMaster.intStatusID = 9;
                if (datQuotation.Rows[0]["UserName"] != DBNull.Value) objclsDTOSTSalesMaster.strCreatedBy = Convert.ToString(datQuotation.Rows[0]["UserName"]); else objclsDTOSTSalesMaster.strCreatedBy = "";
                if (datQuotation.Rows[0]["ExpenseAmount"] != DBNull.Value) objclsDTOSTSalesMaster.decExpenseAmount = Convert.ToDecimal(datQuotation.Rows[0]["ExpenseAmount"]); else objclsDTOSTSalesMaster.decExpenseAmount = 0;
                //if (datQuotation.Rows[0]["GrandDiscountAmt"] != DBNull.Value) objclsDTOSTSalesMaster.decGrandDiscountAmt = Convert.ToDecimal(datQuotation.Rows[0]["GrandDiscountAmt"]); else objclsDTOSTSalesMaster.decGrandDiscountAmt = 0;

                objclsDTOSTSalesMaster.decTaxAmount=datQuotation.Rows[0]["TaxAmount"].ToDecimal();
                objclsDTOSTSalesMaster.intTaxSchemeID = datQuotation.Rows[0]["TaxSchemeID"].ToInt32();
                objclsDTOSTSalesMaster.intApprovedBy = datQuotation.Rows[0]["ApprovedBy"].ToInt32();

                DataTable datSalesQuotationCancellationDetails = DisplaySalesQuotationCancellationDetails();

                if (datSalesQuotationCancellationDetails != null && datSalesQuotationCancellationDetails.Rows.Count > 0)
                {
                    objclsDTOSTSalesMaster.strDescription = Convert.ToString(datSalesQuotationCancellationDetails.Rows[0]["Remarks"]);
                    objclsDTOSTSalesMaster.strCancellationDate = Convert.ToString(datSalesQuotationCancellationDetails.Rows[0]["Date"]);
                    objclsDTOSTSalesMaster.strCancelledBy = Convert.ToString(datSalesQuotationCancellationDetails.Rows[0]["CancelledByName"]);
                    objclsDTOSTSalesMaster.intCancelledBy = Convert.ToInt32(datSalesQuotationCancellationDetails.Rows[0]["CancelledBy"]);
                    objclsDTOSTSalesMaster.blnCancelled = true;
                }
                else
                {
                    objclsDTOSTSalesMaster.strDescription = "";
                    objclsDTOSTSalesMaster.blnCancelled = false;
                }
                return true;
            }
            return false;
        }

        public bool GetSalesProQuotationMasterDetails(Int64 intQuotationID)
        {   //Quotation
            ArrayList prmQtn = new ArrayList();

            prmQtn.Add(new SqlParameter("@Mode", 35));
            prmQtn.Add(new SqlParameter("@SalesQuotationID", intQuotationID));         
            DataTable datQuotation = objclsConnection.ExecuteDataTable(strProcedureSalesQuotation, prmQtn);

            if (datQuotation.Rows.Count > 0)
            {
                objclsDTOSTSalesMaster.intSalesQuotationID = intQuotationID;

                objclsDTOSTSalesMaster.strSalesQuotationNo = Convert.ToString(datQuotation.Rows[0]["SalesQuotationNo"]);
                objclsDTOSTSalesMaster.strQuotationDate = Convert.ToString(datQuotation.Rows[0]["QuotationDate"]);

                if (datQuotation.Rows[0]["VendorID"] != DBNull.Value) objclsDTOSTSalesMaster.intVendorID = Convert.ToInt32(datQuotation.Rows[0]["VendorID"]); else objclsDTOSTSalesMaster.intVendorID = 0;
                if (datQuotation.Rows[0]["VendorName"] != DBNull.Value) objclsDTOSTSalesMaster.strVendorName = Convert.ToString(datQuotation.Rows[0]["VendorName"]); else objclsDTOSTSalesMaster.strVendorName = "";
                objclsDTOSTSalesMaster.strDueDate = Convert.ToString(datQuotation.Rows[0]["DueDate"]);
                if (datQuotation.Rows[0]["VendorAddID"] != DBNull.Value) objclsDTOSTSalesMaster.intVendorAddID = Convert.ToInt32(datQuotation.Rows[0]["VendorAddID"]); else objclsDTOSTSalesMaster.intVendorAddID = 0;
                if (datQuotation.Rows[0]["AddressName"] != DBNull.Value) objclsDTOSTSalesMaster.strAddressName = Convert.ToString(datQuotation.Rows[0]["AddressName"]); else objclsDTOSTSalesMaster.strAddressName = "";
                if (datQuotation.Rows[0]["PaymentTermsID"] != DBNull.Value) objclsDTOSTSalesMaster.intPaymentTermsID = Convert.ToInt32(datQuotation.Rows[0]["PaymentTermsID"]); else objclsDTOSTSalesMaster.intPaymentTermsID = 0;
                if (datQuotation.Rows[0]["Remarks"] != DBNull.Value) objclsDTOSTSalesMaster.strRemarks = Convert.ToString(datQuotation.Rows[0]["Remarks"]); else objclsDTOSTSalesMaster.strRemarks = "";

                if (datQuotation.Rows[0]["IsDiscountPercentage"] != DBNull.Value)
                {
                    objclsDTOSTSalesMaster.blnIsDiscountPercentage = datQuotation.Rows[0]["IsDiscountPercentage"].ToBoolean();

                    if (datQuotation.Rows[0]["GrandDiscountPercentage"] != DBNull.Value)
                        objclsDTOSTSalesMaster.decGrandDiscountPercentage = datQuotation.Rows[0]["GrandDiscountPercentage"].ToDecimal();
                    else
                        objclsDTOSTSalesMaster.decGrandDiscountPercentage = 0;

                    if (datQuotation.Rows[0]["GrandDiscountAmount"] != DBNull.Value)
                        objclsDTOSTSalesMaster.decGrandDiscountAmount = datQuotation.Rows[0]["GrandDiscountAmount"].ToDecimal();
                    else
                        objclsDTOSTSalesMaster.decGrandDiscountAmount = 0;
                }

                if (datQuotation.Rows[0]["CurrencyID"] != DBNull.Value) objclsDTOSTSalesMaster.intCurrencyID = Convert.ToInt32(datQuotation.Rows[0]["CurrencyID"]); else objclsDTOSTSalesMaster.intCurrencyID = 0;
                if (datQuotation.Rows[0]["GrandAmount"] != DBNull.Value) objclsDTOSTSalesMaster.decGrandAmount = Convert.ToDecimal(datQuotation.Rows[0]["GrandAmount"]); else objclsDTOSTSalesMaster.decGrandAmount = 0;
                if (datQuotation.Rows[0]["NetAmount"] != DBNull.Value) objclsDTOSTSalesMaster.decNetAmount = Convert.ToDecimal(datQuotation.Rows[0]["NetAmount"]); else objclsDTOSTSalesMaster.decNetAmount = 0;
                if (datQuotation.Rows[0]["ApprovedDate"] != DBNull.Value) objclsDTOSTSalesMaster.strApprovedDate = Convert.ToString(datQuotation.Rows[0]["ApprovedDate"]); else objclsDTOSTSalesMaster.strApprovedDate = "";
                if (datQuotation.Rows[0]["VerificationDescription"] != DBNull.Value) objclsDTOSTSalesMaster.strVerificationDescription = Convert.ToString(datQuotation.Rows[0]["VerificationDescription"]); else objclsDTOSTSalesMaster.strVerificationDescription = "";
                if (datQuotation.Rows[0]["ApprovedByName"] != DBNull.Value) objclsDTOSTSalesMaster.strApprovedBy = Convert.ToString(datQuotation.Rows[0]["ApprovedByName"]); else objclsDTOSTSalesMaster.strApprovedBy = "";

                if (datQuotation.Rows[0]["CreatedBy"] != DBNull.Value) objclsDTOSTSalesMaster.intCreatedBy = Convert.ToInt32(datQuotation.Rows[0]["CreatedBy"]); else objclsDTOSTSalesMaster.intCreatedBy = 0;
                if (datQuotation.Rows[0]["CreatedDate"] != DBNull.Value) objclsDTOSTSalesMaster.strCreatedDate = Convert.ToString(datQuotation.Rows[0]["CreatedDate"]); else objclsDTOSTSalesMaster.strCreatedDate = "";

                if (datQuotation.Rows[0]["CompanyID"] != DBNull.Value) objclsDTOSTSalesMaster.intCompanyID = Convert.ToInt32(datQuotation.Rows[0]["CompanyID"]); else objclsDTOSTSalesMaster.intCompanyID = 0;
                if (datQuotation.Rows[0]["StatusID"] != DBNull.Value) objclsDTOSTSalesMaster.intStatusID = Convert.ToInt32(datQuotation.Rows[0]["StatusID"]); else objclsDTOSTSalesMaster.intStatusID = 9;
                if (datQuotation.Rows[0]["UserName"] != DBNull.Value) objclsDTOSTSalesMaster.strCreatedBy = Convert.ToString(datQuotation.Rows[0]["UserName"]); else objclsDTOSTSalesMaster.strCreatedBy = "";
                if (datQuotation.Rows[0]["ExpenseAmount"] != DBNull.Value) objclsDTOSTSalesMaster.decExpenseAmount = Convert.ToDecimal(datQuotation.Rows[0]["ExpenseAmount"]); else objclsDTOSTSalesMaster.decExpenseAmount = 0;
                //if (datQuotation.Rows[0]["GrandDiscountAmt"] != DBNull.Value) objclsDTOSTSalesMaster.decGrandDiscountAmt = Convert.ToDecimal(datQuotation.Rows[0]["GrandDiscountAmt"]); else objclsDTOSTSalesMaster.decGrandDiscountAmt = 0;

                objclsDTOSTSalesMaster.intApprovedBy = datQuotation.Rows[0]["ApprovedBy"].ToInt32();

                DataTable datSalesQuotationCancellationDetails = DisplaySalesQuotationCancellationDetails();

                if (datSalesQuotationCancellationDetails != null && datSalesQuotationCancellationDetails.Rows.Count > 0)
                {
                    objclsDTOSTSalesMaster.strDescription = Convert.ToString(datSalesQuotationCancellationDetails.Rows[0]["Remarks"]);
                    objclsDTOSTSalesMaster.strCancellationDate = Convert.ToString(datSalesQuotationCancellationDetails.Rows[0]["Date"]);
                    objclsDTOSTSalesMaster.strCancelledBy = Convert.ToString(datSalesQuotationCancellationDetails.Rows[0]["CancelledByName"]);
                    objclsDTOSTSalesMaster.intCancelledBy = Convert.ToInt32(datSalesQuotationCancellationDetails.Rows[0]["CancelledBy"]);
                    objclsDTOSTSalesMaster.blnCancelled = true;
                }
                else
                {
                    objclsDTOSTSalesMaster.strDescription = "";
                    objclsDTOSTSalesMaster.blnCancelled = false;
                }
                return true;
            }
            return false;
        }
        public DataTable GetJobOrderItemDetails(Int64 intJobOrderID)
        {   //Quotation against Job Order
            ArrayList prmJobOrderQtn = new ArrayList();

            prmJobOrderQtn.Add(new SqlParameter("@Mode", 22));
            prmJobOrderQtn.Add(new SqlParameter("@JobOrderID", intJobOrderID));

            return objclsConnection.ExecuteDataTable(strProcedureSalesQuotation, prmJobOrderQtn);
        }

        public bool GetOrderMasterDetails(Int64 intOrderID)
        {   //Order
            ArrayList prmOrder = new ArrayList();
            prmOrder.Add(new SqlParameter("@Mode", 1));
            prmOrder.Add(new SqlParameter("@SalesOrderID", intOrderID));
            prmOrder.Add(new SqlParameter("@OperationTypeID", (int)OperationType.SalesOrder));
            DataTable datOrder = objclsConnection.ExecuteDataTable(strProcedureSalesOrder, prmOrder);

            if (datOrder.Rows.Count > 0)
            {
                objclsDTOSTSalesMaster.intSalesOrderID = intOrderID;

                objclsDTOSTSalesMaster.strSalesOrderNo = Convert.ToString(datOrder.Rows[0]["SalesOrderNo"]);
                objclsDTOSTSalesMaster.strOrderDate = Convert.ToString(datOrder.Rows[0]["OrderDate"]);
                objclsDTOSTSalesMaster.strDueDate = Convert.ToString(datOrder.Rows[0]["DueDate"]);
                objclsDTOSTSalesMaster.intSalesQuotationID = Convert.ToInt64(datOrder.Rows[0]["ReferenceID"]);

                if (datOrder.Rows[0]["VendorID"] != DBNull.Value) objclsDTOSTSalesMaster.intVendorID = Convert.ToInt32(datOrder.Rows[0]["VendorID"]); else objclsDTOSTSalesMaster.intVendorID = 0;
                if (datOrder.Rows[0]["VendorName"] != DBNull.Value) objclsDTOSTSalesMaster.strVendorName = Convert.ToString(datOrder.Rows[0]["VendorName"]); else objclsDTOSTSalesMaster.strVendorName = "";
                if (datOrder.Rows[0]["VendorAddID"] != DBNull.Value) objclsDTOSTSalesMaster.intVendorAddID = Convert.ToInt32(datOrder.Rows[0]["VendorAddID"]); else objclsDTOSTSalesMaster.intVendorAddID = 0;
                if (datOrder.Rows[0]["AddressName"] != DBNull.Value) objclsDTOSTSalesMaster.strAddressName = Convert.ToString(datOrder.Rows[0]["AddressName"]); else objclsDTOSTSalesMaster.strAddressName = "";
                if (datOrder.Rows[0]["PaymentTermsID"] != DBNull.Value) objclsDTOSTSalesMaster.intPaymentTermsID = Convert.ToInt32(datOrder.Rows[0]["PaymentTermsID"]); else objclsDTOSTSalesMaster.intPaymentTermsID = 0;
                if (datOrder.Rows[0]["Remarks"] != DBNull.Value) objclsDTOSTSalesMaster.strRemarks = Convert.ToString(datOrder.Rows[0]["Remarks"]); else objclsDTOSTSalesMaster.strRemarks = "";


                if (datOrder.Rows[0]["TaxSchemeID"] != DBNull.Value) objclsDTOSTSalesMaster.intTaxSchemeID = Convert.ToInt32(datOrder.Rows[0]["TaxSchemeID"]); else objclsDTOSTSalesMaster.intTaxSchemeID = 0;
                if (datOrder.Rows[0]["TaxAmount"] != DBNull.Value) objclsDTOSTSalesMaster.decTaxAmount = Convert.ToDecimal(datOrder.Rows[0]["TaxAmount"]); else objclsDTOSTSalesMaster.decTaxAmount = 0;


                objclsDTOSTSalesMaster.blnIsDiscountAllowed = false;

                if (datOrder.Rows[0]["IsDiscountPercentage"] != DBNull.Value)
                {
                    objclsDTOSTSalesMaster.blnIsDiscountAllowed = true;

                    if (datOrder.Rows[0]["IsDiscountPercentage"].ToBoolean())
                    {
                        if (datOrder.Rows[0]["GrandDiscountPercentage"].ToDecimal() > 0)
                        {
                            objclsDTOSTSalesMaster.blnIsDiscountPercentage = datOrder.Rows[0]["IsDiscountPercentage"].ToBoolean();
                            objclsDTOSTSalesMaster.decGrandDiscountPercentage = datOrder.Rows[0]["GrandDiscountPercentage"].ToDecimal();
                            objclsDTOSTSalesMaster.decGrandDiscountAmount = datOrder.Rows[0]["GrandDiscountAmount"].ToDecimal();
                        }
                    }
                    else
                    {
                        if (datOrder.Rows[0]["GrandDiscountAmount"].ToDecimal() > 0)
                        {
                            objclsDTOSTSalesMaster.blnIsDiscountPercentage = datOrder.Rows[0]["IsDiscountPercentage"].ToBoolean();
                            objclsDTOSTSalesMaster.decGrandDiscountPercentage = 0;
                            objclsDTOSTSalesMaster.decGrandDiscountAmount = datOrder.Rows[0]["GrandDiscountAmount"].ToDecimal();
                        }
                    }
                }
                if (datOrder.Rows[0]["CurrencyID"] != DBNull.Value) objclsDTOSTSalesMaster.intCurrencyID = Convert.ToInt32(datOrder.Rows[0]["CurrencyID"]); else objclsDTOSTSalesMaster.intCurrencyID = 0;
                if (datOrder.Rows[0]["GrandAmount"] != DBNull.Value) objclsDTOSTSalesMaster.decGrandAmount = Convert.ToDecimal(datOrder.Rows[0]["GrandAmount"]); else objclsDTOSTSalesMaster.decGrandAmount = 0;
                if (datOrder.Rows[0]["NetAmount"] != DBNull.Value) objclsDTOSTSalesMaster.decNetAmount = Convert.ToDecimal(datOrder.Rows[0]["NetAmount"]); else objclsDTOSTSalesMaster.decNetAmount = 0;
                if (datOrder.Rows[0]["NetAmountRounded"] != DBNull.Value) objclsDTOSTSalesMaster.decNetAmountRounded = Convert.ToDecimal(datOrder.Rows[0]["NetAmountRounded"]); else objclsDTOSTSalesMaster.decNetAmountRounded = 0;

                if (datOrder.Rows[0]["CreatedBy"] != DBNull.Value) objclsDTOSTSalesMaster.intCreatedBy = Convert.ToInt32(datOrder.Rows[0]["CreatedBy"]); else objclsDTOSTSalesMaster.intCreatedBy = 0;
                if (datOrder.Rows[0]["CreatedDate"] != DBNull.Value) objclsDTOSTSalesMaster.strCreatedDate = Convert.ToString(datOrder.Rows[0]["CreatedDate"]); else objclsDTOSTSalesMaster.strCreatedDate = "";

                if (datOrder.Rows[0]["CompanyID"] != DBNull.Value) objclsDTOSTSalesMaster.intCompanyID = Convert.ToInt32(datOrder.Rows[0]["CompanyID"]); else objclsDTOSTSalesMaster.intCompanyID = 0;
                if (datOrder.Rows[0]["StatusID"] != DBNull.Value) objclsDTOSTSalesMaster.intStatusID = Convert.ToInt32(datOrder.Rows[0]["StatusID"]); else objclsDTOSTSalesMaster.intStatusID = 9;
                if (datOrder.Rows[0]["OrderTypeID"] != DBNull.Value) objclsDTOSTSalesMaster.intOrderTypeID = Convert.ToInt32(datOrder.Rows[0]["OrderTypeID"]); else objclsDTOSTSalesMaster.intOrderTypeID = 1;
                if (datOrder.Rows[0]["UserName"] != DBNull.Value) objclsDTOSTSalesMaster.strCreatedBy = Convert.ToString(datOrder.Rows[0]["UserName"]); else objclsDTOSTSalesMaster.strCreatedBy = "";
                if (datOrder.Rows[0]["ExpenseAmount"] != DBNull.Value) objclsDTOSTSalesMaster.decExpenseAmount = Convert.ToDecimal(datOrder.Rows[0]["ExpenseAmount"]); else objclsDTOSTSalesMaster.decExpenseAmount = 0;
                
                if (datOrder.Rows[0]["ApprovedDate"] != DBNull.Value) objclsDTOSTSalesMaster.strApprovedDate = Convert.ToString(datOrder.Rows[0]["ApprovedDate"]); else objclsDTOSTSalesMaster.strApprovedDate = "";
                if (datOrder.Rows[0]["VerificationDescription"] != DBNull.Value) objclsDTOSTSalesMaster.strVerificationDescription = Convert.ToString(datOrder.Rows[0]["VerificationDescription"]); else objclsDTOSTSalesMaster.strVerificationDescription = "";
                if (datOrder.Rows[0]["ApprovedByName"] != DBNull.Value) objclsDTOSTSalesMaster.strApprovedBy = Convert.ToString(datOrder.Rows[0]["ApprovedByName"]); else objclsDTOSTSalesMaster.strApprovedBy = "";
                if (datOrder.Rows[0]["LPONumber"] != DBNull.Value) objclsDTOSTSalesMaster.lopno = Convert.ToString(datOrder.Rows[0]["LPONumber"]); else objclsDTOSTSalesMaster.lopno = "";
                objclsDTOSTSalesMaster.intApprovedBy = datOrder.Rows[0]["ApprovedBy"].ToInt32();


                objclsDTOSTSalesMaster.strDescription = "";
                objclsDTOSTSalesMaster.blnCancelled = false;

                if (objclsDTOSTSalesMaster.intStatusID == (int)OperationStatusType.SOrderCancelled) // cancelled entries have status id - 15
                {
                    DataTable datSalesOrderCancellationDetails = DisplaySalesOrderCancellationDetails();
                    objclsDTOSTSalesMaster.blnCancelled = true;

                    if (datSalesOrderCancellationDetails != null && datSalesOrderCancellationDetails.Rows.Count > 0)
                    {
                        objclsDTOSTSalesMaster.strDescription = Convert.ToString(datSalesOrderCancellationDetails.Rows[0]["Remarks"]);
                        objclsDTOSTSalesMaster.strCancellationDate = Convert.ToString(datSalesOrderCancellationDetails.Rows[0]["Date"]);
                        objclsDTOSTSalesMaster.intCancelledBy = datSalesOrderCancellationDetails.Rows[0]["CancelledBy"].ToInt32();
                        objclsDTOSTSalesMaster.strCancelledBy = datSalesOrderCancellationDetails.Rows[0]["CancelledByName"].ToString();
                    }
                }

                return true;
            }
            return false;
        }

        public bool GetInvoiceMasterDetails(Int64 intSalesInvoiceID)
        {   //Invoice
            ArrayList prmInvoice = new ArrayList();

            prmInvoice.Add(new SqlParameter("@Mode", 1));
            prmInvoice.Add(new SqlParameter("@SalesInvoiceID", intSalesInvoiceID));
            DataTable datSalesInvoice = objclsConnection.ExecuteDataTable(strProcedureSalesInvoice, prmInvoice);

            if (datSalesInvoice.Rows.Count > 0)
            {
                objclsDTOSTSalesMaster.intSalesInvoiceID = intSalesInvoiceID;
                objclsDTOSTSalesMaster.strSalesInvoiceNo = Convert.ToString(datSalesInvoice.Rows[0]["SalesInvoiceNo"]);
                objclsDTOSTSalesMaster.strInvoiceDate = Convert.ToString(datSalesInvoice.Rows[0]["InvoiceDate"]);
                objclsDTOSTSalesMaster.strDueDate = Convert.ToString(datSalesInvoice.Rows[0]["DueDate"]);
                objclsDTOSTSalesMaster.intSalesOrderID = Convert.ToInt64(datSalesInvoice.Rows[0]["SalesOrderID"]);


                if (datSalesInvoice.Rows[0]["VendorID"] != DBNull.Value) objclsDTOSTSalesMaster.intVendorID = Convert.ToInt32(datSalesInvoice.Rows[0]["VendorID"]); else objclsDTOSTSalesMaster.intVendorID = 0;
                if (datSalesInvoice.Rows[0]["VendorName"] != DBNull.Value) objclsDTOSTSalesMaster.strVendorName = Convert.ToString(datSalesInvoice.Rows[0]["VendorName"]); else objclsDTOSTSalesMaster.strVendorName = "";
                if (datSalesInvoice.Rows[0]["VendorAddID"] != DBNull.Value) objclsDTOSTSalesMaster.intVendorAddID = Convert.ToInt32(datSalesInvoice.Rows[0]["VendorAddID"]); else objclsDTOSTSalesMaster.intVendorAddID = 0;
                if (datSalesInvoice.Rows[0]["AddressName"] != DBNull.Value) objclsDTOSTSalesMaster.strAddressName = Convert.ToString(datSalesInvoice.Rows[0]["AddressName"]); else objclsDTOSTSalesMaster.strAddressName = "";
                if (datSalesInvoice.Rows[0]["PaymentTermsID"] != DBNull.Value) objclsDTOSTSalesMaster.intPaymentTermsID = Convert.ToInt32(datSalesInvoice.Rows[0]["PaymentTermsID"]); else objclsDTOSTSalesMaster.intPaymentTermsID = 0;
                if (datSalesInvoice.Rows[0]["Remarks"] != DBNull.Value) objclsDTOSTSalesMaster.strRemarks = Convert.ToString(datSalesInvoice.Rows[0]["Remarks"]); else objclsDTOSTSalesMaster.strRemarks = "";
                if (datSalesInvoice.Rows[0]["GrandDiscount"] != DBNull.Value) objclsDTOSTSalesMaster.blnGrandDiscount  = Convert.ToBoolean (datSalesInvoice.Rows[0]["GrandDiscount"]); else objclsDTOSTSalesMaster.blnGrandDiscount  = false;
                if (datSalesInvoice.Rows[0]["GrandDiscountAmt"] != DBNull.Value) objclsDTOSTSalesMaster.decGrandDiscountAmt = Convert.ToDecimal(datSalesInvoice.Rows[0]["GrandDiscountAmt"]); else objclsDTOSTSalesMaster.decGrandDiscountAmt = 0;
                if (datSalesInvoice.Rows[0]["CurrencyID"] != DBNull.Value) objclsDTOSTSalesMaster.intCurrencyID = Convert.ToInt32(datSalesInvoice.Rows[0]["CurrencyID"]); else objclsDTOSTSalesMaster.intCurrencyID = 0;
                if (datSalesInvoice.Rows[0]["GrandAmount"] != DBNull.Value) objclsDTOSTSalesMaster.decGrandAmount = Convert.ToDecimal(datSalesInvoice.Rows[0]["GrandAmount"]); else objclsDTOSTSalesMaster.decGrandAmount = 0;
                if (datSalesInvoice.Rows[0]["NetAmount"] != DBNull.Value) objclsDTOSTSalesMaster.decNetAmount = Convert.ToDecimal(datSalesInvoice.Rows[0]["NetAmount"]); else objclsDTOSTSalesMaster.decNetAmount = 0;
                if (datSalesInvoice.Rows[0]["NetAmountRounded"] != DBNull.Value) objclsDTOSTSalesMaster.decNetAmountRounded = Convert.ToDecimal(datSalesInvoice.Rows[0]["NetAmountRounded"]); else objclsDTOSTSalesMaster.decNetAmountRounded = 0;
                if (datSalesInvoice.Rows[0]["CurrencyID"] != DBNull.Value) objclsDTOSTSalesMaster.intCurrencyID = Convert.ToInt32(datSalesInvoice.Rows[0]["CurrencyID"]); else objclsDTOSTSalesMaster.intCurrencyID = 0;
                if (datSalesInvoice.Rows[0]["GrandDiscountAmt"] != DBNull.Value) objclsDTOSTSalesMaster.decGrandDiscountAmt = Convert.ToDecimal(datSalesInvoice.Rows[0]["GrandDiscountAmt"]); else objclsDTOSTSalesMaster.decGrandDiscountAmt = 0;
                //added by hima
                if (datSalesInvoice.Rows[0]["OrderTypeID"] != DBNull.Value) objclsDTOSTSalesMaster.intOrderTypeID = Convert.ToInt32(datSalesInvoice.Rows[0]["OrderTypeID"]); else objclsDTOSTSalesMaster.intOrderTypeID = 0;
                if (datSalesInvoice.Rows[0]["AccountID"] != DBNull.Value) objclsDTOSTSalesMaster.intAccountID = Convert.ToInt32(datSalesInvoice.Rows[0]["AccountID"]); else objclsDTOSTSalesMaster.intAccountID = 0;

                if (datSalesInvoice.Rows[0]["CreatedBy"] != DBNull.Value) objclsDTOSTSalesMaster.intCreatedBy = Convert.ToInt32(datSalesInvoice.Rows[0]["CreatedBy"]); else objclsDTOSTSalesMaster.intCreatedBy = 0;
                if (datSalesInvoice.Rows[0]["CreatedDate"] != DBNull.Value) objclsDTOSTSalesMaster.strCreatedDate = Convert.ToString(datSalesInvoice.Rows[0]["CreatedDate"]); else objclsDTOSTSalesMaster.strCreatedDate = "";

                if (datSalesInvoice.Rows[0]["CompanyID"] != DBNull.Value) objclsDTOSTSalesMaster.intCompanyID = Convert.ToInt32(datSalesInvoice.Rows[0]["CompanyID"]); else objclsDTOSTSalesMaster.intCompanyID = 0;
                if (datSalesInvoice.Rows[0]["StatusID"] != DBNull.Value) objclsDTOSTSalesMaster.intStatusID = Convert.ToInt32(datSalesInvoice.Rows[0]["StatusID"]); else objclsDTOSTSalesMaster.intStatusID = 9;
                if (datSalesInvoice.Rows[0]["UserName"] != DBNull.Value) objclsDTOSTSalesMaster.strCreatedBy = Convert.ToString(datSalesInvoice.Rows[0]["UserName"]); else objclsDTOSTSalesMaster.strCreatedBy = "";
                if (datSalesInvoice.Rows[0]["VoucherNo"] != DBNull.Value) objclsDTOSTSalesMaster.strVoucherNo = Convert.ToString(datSalesInvoice.Rows[0]["VoucherNo"]); else objclsDTOSTSalesMaster.strVoucherNo = "";

                objclsDTOSTSalesMaster.strDescription = "";
                objclsDTOSTSalesMaster.blnCancelled = false;

                if (objclsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SInvoiceCancelled) // cancelled entries have status id - 29
                {
                    DataTable datSalesInvoiceCancellationDetails = DisplaySalesInvoiceCancellationDetails();
                    objclsDTOSTSalesMaster.blnCancelled = true;

                    if (datSalesInvoiceCancellationDetails != null && datSalesInvoiceCancellationDetails.Rows.Count > 0)
                    {
                        objclsDTOSTSalesMaster.strDescription = Convert.ToString(datSalesInvoiceCancellationDetails.Rows[0]["Remarks"]);
                        objclsDTOSTSalesMaster.strCancellationDate = Convert.ToString(datSalesInvoiceCancellationDetails.Rows[0]["Date"]);
                        objclsDTOSTSalesMaster.strCancelledBy = Convert.ToString(datSalesInvoiceCancellationDetails.Rows[0]["CancelledByName"]);
                        objclsDTOSTSalesMaster.intCancelledBy = Convert.ToInt32(datSalesInvoiceCancellationDetails.Rows[0]["CancelledBy"]);
                    }
                }

                return true;
            }
            return false;
        }

        public string GetVendorInformation(int intVendorID)
        {
            string strAddress = "";
            ArrayList prmVendor = new ArrayList();

            prmVendor.Add(new SqlParameter("@Mode", 6));
            prmVendor.Add(new SqlParameter("@VendorID", intVendorID));
            DataTable datVendor = objclsConnection.ExecuteDataTable(strProcedureSalesQuotation, prmVendor);

            if (datVendor.Rows.Count > 0)
            {
                strAddress = Convert.ToString(datVendor.Rows[0]["Contact"]) + " \n" + Convert.ToString(datVendor.Rows[0]["MailingAddress"]) +
                           " \n" + Convert.ToString(datVendor.Rows[0]["Country"]) + " \n" + Convert.ToString(datVendor.Rows[0]["Email"]) +
                           " \n" + Convert.ToString(datVendor.Rows[0]["Telephone"]) + " \n" + Convert.ToString(datVendor.Rows[0]["Fax"]) +
                           " \n" + Convert.ToString(datVendor.Rows[0]["Website"]);

                return strAddress;
            }
            return strAddress;
        }

        public string GetVendorAddressInformation(int intVendorAdd)
        {   //To Get Vendor Address Information
            string strAddress = "";
            ArrayList prmVendor = new ArrayList();

            prmVendor.Add(new SqlParameter("@Mode", 7));
            prmVendor.Add(new SqlParameter("@VendorID", intVendorAdd));
            SqlDataReader drVendor = objclsConnection.ExecuteReader(strProcedureSalesQuotation, prmVendor);

            if (drVendor.Read())
            {
                strAddress = Environment.NewLine;
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["ContactPerson"])))
                {
                    strAddress += Convert.ToString(drVendor["ContactPerson"]);
                    strAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Address"])))
                {
                    strAddress += Convert.ToString(drVendor["Address"]);
                    strAddress += Environment.NewLine;
                }

                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["State"])))
                {
                    strAddress += Convert.ToString(drVendor["State"]);
                    strAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Country"])))
                {
                    strAddress += Convert.ToString(drVendor["Country"]);
                    strAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["ZipCode"])))
                {
                    strAddress += "ZipCode : " + Convert.ToString(drVendor["ZipCode"]);
                    strAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Telephone"])))
                {
                    strAddress += "Telephone : " + Convert.ToString(drVendor["Telephone"]);
                    strAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["MobileNo"])))
                {
                    strAddress += "MobileNo : " + Convert.ToString(drVendor["MobileNo"]);
                    strAddress += Environment.NewLine;
                }

                return strAddress;
            }
            return strAddress;
        }

        public DataTable GetQuotationDetDetails(Int64 intSalesQuotationID)
        {   // Sales Quotation details
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 11));
            prmCommon.Add(new SqlParameter("@SalesQuotationID", intSalesQuotationID));
            return objclsConnection.ExecuteDataTable(strProcedureSalesQuotation, prmCommon);
        }
        public DataTable GetSalesProQuotationDetDetails(Int64 intSalesQuotationID)
        {   // Sales Quotation details
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 36));
            prmCommon.Add(new SqlParameter("@SalesQuotationID", intSalesQuotationID));
            return objclsConnection.ExecuteDataTable(strProcedureSalesQuotation, prmCommon);
        }
        public DataTable GetSalesInvoiceDetDetails(Int64 intSalesInvoiceID, int IngOrderTypeID)
        {   // Display Invoice detail

            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 13));
            prmCommon.Add(new SqlParameter("@SalesInvoiceID", intSalesInvoiceID));
            prmCommon.Add(new SqlParameter("@OrderTypeID", IngOrderTypeID));
            return objclsConnection.ExecuteDataTable(strProcedureSalesInvoice, prmCommon);
        }

        public DataTable GetOrderDetDetails(Int64 intSalesOrderID,int intFormType)
        {   // Display Order detail
            //if (intFormType == 3)
            //{
            //    ArrayList prmCommon = new ArrayList();
            //    prmCommon.Add(new SqlParameter("@Mode", 38));
            //    prmCommon.Add(new SqlParameter("@SalesOrderID", intSalesOrderID));
            //    return objclsConnection.ExecuteDataTable(strProcedureSalesInvoice, prmCommon);
            //}
            //else
            //{
                ArrayList prmCommon = new ArrayList();
                prmCommon.Add(new SqlParameter("@Mode", 11));
                prmCommon.Add(new SqlParameter("@SalesOrderID", intSalesOrderID));
                return objclsConnection.ExecuteDataTable(strProcedureSalesOrder, prmCommon);
            //}
        }

        public int GetVendorRecordID(int intVendorID)
        {   // Retreiving current vendorid for Focussing on the current vendor
            int intVendorRecordID = 0;

            dat = new DataTable();
            prmSales = new ArrayList();

            prmSales.Add(new SqlParameter("@Mode", 10));
            prmSales.Add(new SqlParameter("@VendorID", intVendorID));

            dat = objclsConnection.ExecuteDataTable("STVendorTransaction", prmSales);

            if (dat.Rows.Count > 0)
                intVendorRecordID = Convert.ToInt32(dat.Rows[0]["RowNumber"]);

            return intVendorRecordID;
        }

        public DataTable DtGetAddressName()
        {
            DataTable datAddressName = new DataTable();

            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = objclsConnection;
                objCommonUtility.GetRecordValue(new string[] { "VendorAddID,AddressName", strTableVendorAddress, "VendorID=" + objclsDTOSTSalesMaster.intVendorID + "" }, out datAddressName);
                return datAddressName;
            }
        }

        public void GetItemDiscountAmount(int intMode, int intItemID, bool blnDiscount, int intVendorID)
        {
            ArrayList prmItemDiscount = new ArrayList();
            prmItemDiscount.Add(new SqlParameter("@Mode", intMode));
            prmItemDiscount.Add(new SqlParameter("@ItemID", intItemID));
            prmItemDiscount.Add(new SqlParameter("@Discount", blnDiscount));
            prmItemDiscount.Add(new SqlParameter("@VendorID", intVendorID));

            DataTable datDiscountAmount = objclsConnection.ExecuteDataTable(strProcedurePurchaseModuleFunctions, prmItemDiscount);

            if (datDiscountAmount.Rows.Count > 0)
            {
                objclsDTOSTSalesMaster.blnDiscountForAmount = Convert.ToBoolean(datDiscountAmount.Rows[0]["IsDiscountForAmount"]);
                objclsDTOSTSalesMaster.blnPercentCalculation = Convert.ToBoolean(datDiscountAmount.Rows[0]["PercentOrAmount"]);
                objclsDTOSTSalesMaster.decDiscountValue = Convert.ToDecimal(datDiscountAmount.Rows[0]["PercOrAmtValue"]);
            }
        }

        private bool DeleteSalesQuotationDetailInformation(Int64 intSalesQuotationID)
        {   // Quotation Detail Delete
            ArrayList prmDeleteQuotationDetail;
            prmDeleteQuotationDetail = new ArrayList();
            prmDeleteQuotationDetail.Add(new SqlParameter("@Mode", 10));
            prmDeleteQuotationDetail.Add(new SqlParameter("@SalesQuotationID", intSalesQuotationID));
            objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesQuotation, prmDeleteQuotationDetail);
            return true;
        }

        private void DeleteSalesQuotationCancellationDetails()
        {
            //Function for deleting SalesQuotation cancellation details
            ArrayList prmSalesQuotation;
            prmSalesQuotation = new ArrayList();
            prmSalesQuotation.Add(new SqlParameter("Mode", 13));
            prmSalesQuotation.Add(new SqlParameter("@SalesQuotationID", objclsDTOSTSalesMaster.intSalesQuotationID));
            objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesQuotation, prmSalesQuotation);
        }

        public void SaveSalesQuotationCancellationDetails()
        {
            //Function for saving SalesQuotation cancellation details
            prmSales = new ArrayList();
            prmSales.Add(new SqlParameter("Mode", 12));
            prmSales.Add(new SqlParameter("SalesQuotationID", objclsDTOSTSalesMaster.intSalesQuotationID));
            prmSales.Add(new SqlParameter("CancelledBy", objclsDTOSTSalesMaster.intCancelledBy));
            prmSales.Add(new SqlParameter("CancellationDate", objclsDTOSTSalesMaster.strCancellationDate));
            prmSales.Add(new SqlParameter("Description", objclsDTOSTSalesMaster.strDescription));
            prmSales.Add(new SqlParameter("CompanyID", objclsDTOSTSalesMaster.intCompanyID));
            objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesQuotation, prmSales);
        }

        private DataTable DisplaySalesQuotationCancellationDetails()
        {
            //function for displaying SalesQuotation cancellation details
            ArrayList prmSalesQuotation;
            prmSalesQuotation = new ArrayList();
            prmSalesQuotation.Add(new SqlParameter("Mode", 14));
            prmSalesQuotation.Add(new SqlParameter("SalesQuotationID", objclsDTOSTSalesMaster.intSalesQuotationID));
            return objclsConnection.ExecuteDataTable(strProcedureSalesQuotation, prmSalesQuotation);
        }

        public bool SaveSalesOrder(bool blnAddStatus)
        {   // Sales Order Save
            object objSalesOrderID = 0;
            int intOldStatusID = 0;

            ArrayList prmSalesOrder = new ArrayList();

            if (blnAddStatus)
                prmSalesOrder.Add(new SqlParameter("@Mode", 2));
            else
            {
                DataTable datSales = FillCombos(new string[] { "StatusID", strTableSalesOrder, "SalesOrderID = " + objclsDTOSTSalesMaster.intSalesOrderID });
                if (datSales.Rows.Count > 0)
                {
                    intOldStatusID = datSales.Rows[0]["StatusID"].ToInt32();
                }
                prmSalesOrder.Add(new SqlParameter("@Mode", 3));
            }
            prmSalesOrder.Add(new SqlParameter("@SalesOrderID", objclsDTOSTSalesMaster.intSalesOrderID));
            prmSalesOrder.Add(new SqlParameter("@SalesOrderNo", objclsDTOSTSalesMaster.strSalesOrderNo));
            prmSalesOrder.Add(new SqlParameter("@OrderDate", objclsDTOSTSalesMaster.strOrderDate));
            prmSalesOrder.Add(new SqlParameter("@OrderTypeID", objclsDTOSTSalesMaster.intOrderTypeID));
            prmSalesOrder.Add(new SqlParameter("@VendorID", objclsDTOSTSalesMaster.intVendorID));
            prmSalesOrder.Add(new SqlParameter("@DueDate", objclsDTOSTSalesMaster.strDueDate));
            prmSalesOrder.Add(new SqlParameter("@VendorAddID", objclsDTOSTSalesMaster.intVendorAddID));
            prmSalesOrder.Add(new SqlParameter("@PaymentTermsID", objclsDTOSTSalesMaster.intPaymentTermsID));
            prmSalesOrder.Add(new SqlParameter("@Remarks", objclsDTOSTSalesMaster.strRemarks));
            prmSalesOrder.Add(new SqlParameter("@LPONumber", objclsDTOSTSalesMaster.lopno));

            prmSalesOrder.Add(new SqlParameter("@TaxSchemeID", objclsDTOSTSalesMaster.intTaxSchemeID));
            prmSalesOrder.Add(new SqlParameter("@TaxAmount", objclsDTOSTSalesMaster.decTaxAmount));

            if (objclsDTOSTSalesMaster.blnIsDiscountPercentage)
            {
                if (objclsDTOSTSalesMaster.decGrandDiscountPercentage > 0)
                {
                    prmSalesOrder.Add(new SqlParameter("@IsDiscountPercentage", objclsDTOSTSalesMaster.blnIsDiscountPercentage));
                    prmSalesOrder.Add(new SqlParameter("@GrandDiscountPercentage", objclsDTOSTSalesMaster.decGrandDiscountPercentage));
                    prmSalesOrder.Add(new SqlParameter("@GrandDiscountAmount", objclsDTOSTSalesMaster.decGrandDiscountAmount));
                }
            }
            else
            {
                if (objclsDTOSTSalesMaster.decGrandDiscountAmount > 0)
                {
                    prmSalesOrder.Add(new SqlParameter("@IsDiscountPercentage", objclsDTOSTSalesMaster.blnIsDiscountPercentage));
                    prmSalesOrder.Add(new SqlParameter("@GrandDiscountPercentage", objclsDTOSTSalesMaster.decGrandDiscountPercentage));
                    prmSalesOrder.Add(new SqlParameter("@GrandDiscountAmount", objclsDTOSTSalesMaster.decGrandDiscountAmount));
                }
            }

            prmSalesOrder.Add(new SqlParameter("@CurrencyID", objclsDTOSTSalesMaster.intCurrencyID));
            prmSalesOrder.Add(new SqlParameter("@GrandAmount", objclsDTOSTSalesMaster.decGrandAmount));
            prmSalesOrder.Add(new SqlParameter("@NetAmount", objclsDTOSTSalesMaster.decNetAmount));
            prmSalesOrder.Add(new SqlParameter("@NetAmountRounded", objclsDTOSTSalesMaster.decNetAmountRounded));
            prmSalesOrder.Add(new SqlParameter("@ExpenseAmount", objclsDTOSTSalesMaster.decExpenseAmount));
            prmSalesOrder.Add(new SqlParameter("@CreatedBy", objclsDTOSTSalesMaster.intCreatedBy));
            prmSalesOrder.Add(new SqlParameter("@CreatedDate", objclsDTOSTSalesMaster.strCreatedDate));

            if (objclsDTOSTSalesMaster.intApprovedBy != 0)
            {
                prmSalesOrder.Add(new SqlParameter("@ApprovedBy", objclsDTOSTSalesMaster.intApprovedBy));
                prmSalesOrder.Add(new SqlParameter("@ApprovedDate", objclsDTOSTSalesMaster.strApprovedDate));
            }
            prmSalesOrder.Add(new SqlParameter("@VerificationDescription", objclsDTOSTSalesMaster.strVerificationDescription));

            prmSalesOrder.Add(new SqlParameter("@CompanyID", objclsDTOSTSalesMaster.intCompanyID));
            prmSalesOrder.Add(new SqlParameter("@StatusID", objclsDTOSTSalesMaster.intStatusID));

            if (objclsDTOSTSalesMaster.blnCancelled)
            {
                prmSalesOrder.Add(new SqlParameter("@CancellationDate", objclsDTOSTSalesMaster.strCancellationDate));
                prmSalesOrder.Add(new SqlParameter("@Description", objclsDTOSTSalesMaster.strDescription));
                prmSalesOrder.Add(new SqlParameter("@CancelledBy", objclsDTOSTSalesMaster.intCancelledBy));
            }

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmSalesOrder.Add(objParam);

            if (objclsConnection.ExecuteNonQuery(strProcedureSalesOrder, prmSalesOrder, out objSalesOrderID) != 0)
            {
                if (Convert.ToInt64(objSalesOrderID) > 0)
                {
                    objclsDTOSTSalesMaster.intSalesOrderID = Convert.ToInt64(objSalesOrderID);
                    if (blnAddStatus)
                        SaveExpenses(1);

                    if (objclsDTOSTSalesMaster.IlstClsDTOSTSalesDetails.Count > 0)
                    {
                        if (intOldStatusID != (int)OperationStatusType.SOrderCancelled)
                            DeleteSalesOrderDetailInformation(Convert.ToInt64(objSalesOrderID));
                            SaveSalesOrderDetailInformation(Convert.ToInt64(objSalesOrderID), intOldStatusID);
                        if (objclsDTOSTSalesMaster.intOrderTypeID == (int)OperationOrderType.SOTFromQuotation)
                        {
                            DeleteSalesOrderReferenceDetails(Convert.ToInt64(objSalesOrderID));
                            SaveSalesOrderReferenceDetails(Convert.ToInt64(objSalesOrderID));
                        }
                        if (objclsDTOSTSalesMaster.intOrderTypeID == (int)OperationOrderType.SOTFromDeliveryNote)
                        {   
                            DeleteSalesOrderReferenceDetails(Convert.ToInt64(objSalesOrderID));
                            SaveSalesOrderDetFrmDelivertNote(objclsDTOSTSalesMaster.strCondition,Convert.ToInt64(objSalesOrderID )); 
                        }
                    }
                    DeleteSalesOrderCancellationDetails();

                    if (objclsDTOSTSalesMaster.blnCancelled)
                    {
                        SaveSalesOrderCancellationDetails();

                        if (objclsDTOSTSalesMaster.intOrderTypeID == (int)OperationOrderType.SOTFromQuotation)//if the order type is "From Quotation"
                            ResetQuatationStatus();
                    }

                    long lngSalesQuotationID = 0;
                    DataTable datQuotationDetails = MobjClsCommonUtility.FillCombos(new string[] { "DISTINCT SORD.ReferenceID", "InvSalesOrderMaster SOM INNER JOIN InvSalesOrderReferenceDetails SORD ON SORD.SalesOrderID = SOM.SalesOrderID  ", "SOM.SalesOrderID = " + objclsDTOSTSalesMaster.intSalesOrderID + " And SOM.OrderTypeID = " + (int)OperationOrderType.SOTFromQuotation });
                    if (datQuotationDetails.Rows.Count > 0)
                        lngSalesQuotationID = datQuotationDetails.Rows[0]["ReferenceID"].ToInt64();

                    if ((objclsDTOSTSalesMaster.intStatusID == (int)OperationStatusType.SOrderSubmitted && intOldStatusID != objclsDTOSTSalesMaster.intStatusID) ||
                         (objclsDTOSTSalesMaster.intStatusID == (int)OperationStatusType.SOrderSubmittedForApproval && intOldStatusID != objclsDTOSTSalesMaster.intStatusID))
                        VerificationHistoryTableUpdations(2, objclsDTOSTSalesMaster.intSalesOrderID, false);

                    return true;
                }
            }
            return false;
        }

        private bool DeleteSalesOrderDetailInformation(Int64 intSalesOrderID)
        {   // Order Detail Delete
            ArrayList prmDeleteOrderDetail;

            prmDeleteOrderDetail = new ArrayList();
            prmDeleteOrderDetail.Add(new SqlParameter("@Mode", 10));
            prmDeleteOrderDetail.Add(new SqlParameter("@SalesOrderID", intSalesOrderID));

            objclsConnection.ExecuteNonQuery(strProcedureSalesOrder, prmDeleteOrderDetail);
            return true;
        }

        private bool SaveSalesOrderDetailInformation(Int64 intSalesOrderID,int intOldStatusID)
        {   // Order Detail Save
            int intICounter = 0;
            ArrayList prmSalesOrderDetails;

            foreach (clsDTOSTSalesOrderDetails objclsDTOSTSalesDetails in objclsDTOSTSalesMaster.IlstClsDTOSTSalesDetails)
            {
                prmSalesOrderDetails = new ArrayList();

                prmSalesOrderDetails.Add(new SqlParameter("@Mode", 9));
                prmSalesOrderDetails.Add(new SqlParameter("@SerialNo", ++intICounter));
                prmSalesOrderDetails.Add(new SqlParameter("@SalesOrderID", intSalesOrderID));
                prmSalesOrderDetails.Add(new SqlParameter("@ItemID", objclsDTOSTSalesDetails.intItemID));
                if (objclsDTOSTSalesDetails.intBatchID != 0)
                    prmSalesOrderDetails.Add(new SqlParameter("@BatchID", objclsDTOSTSalesDetails.intBatchID));
                prmSalesOrderDetails.Add(new SqlParameter("@IsGroup", objclsDTOSTSalesDetails.blnIsGroup));
                prmSalesOrderDetails.Add(new SqlParameter("@Quantity", objclsDTOSTSalesDetails.decQuantity));
                prmSalesOrderDetails.Add(new SqlParameter("@UOMID", objclsDTOSTSalesDetails.intUOMID));
                prmSalesOrderDetails.Add(new SqlParameter("@Rate", objclsDTOSTSalesDetails.decRate));

                if (objclsDTOSTSalesDetails.blnIsDiscountPercentage)
                {
                    if (objclsDTOSTSalesDetails.decDiscountPercentage > 0)
                    {
                        prmSalesOrderDetails.Add(new SqlParameter("@IsDiscountPercentage", objclsDTOSTSalesDetails.blnIsDiscountPercentage));
                        prmSalesOrderDetails.Add(new SqlParameter("@DiscountPercentage", objclsDTOSTSalesDetails.decDiscountPercentage));
                        prmSalesOrderDetails.Add(new SqlParameter("@DiscountAmount", objclsDTOSTSalesDetails.decDiscountAmount));
                    }
                }
                else
                {
                    if (objclsDTOSTSalesDetails.decDiscountAmount > 0)
                    {
                        prmSalesOrderDetails.Add(new SqlParameter("@IsDiscountPercentage", objclsDTOSTSalesDetails.blnIsDiscountPercentage));
                        prmSalesOrderDetails.Add(new SqlParameter("@DiscountPercentage", objclsDTOSTSalesDetails.decDiscountPercentage));
                        prmSalesOrderDetails.Add(new SqlParameter("@DiscountAmount", objclsDTOSTSalesDetails.decDiscountAmount));
                    }
                }
                
                prmSalesOrderDetails.Add(new SqlParameter("@GrandAmount", objclsDTOSTSalesDetails.decGrandAmount));
                prmSalesOrderDetails.Add(new SqlParameter("@NetAmount", objclsDTOSTSalesDetails.decNetAmount));
                prmSalesOrderDetails.Add(new SqlParameter("@OldStatusID", intOldStatusID));
                objclsConnection.ExecuteNonQuery(strProcedureSalesOrder, prmSalesOrderDetails);
            }
            return true;
        }

        private void DeleteSalesOrderCancellationDetails()
        {
            //Function for deleting SalesOrder cancellation details
            ArrayList prmSalesOrder;
            prmSalesOrder = new ArrayList();
            prmSalesOrder.Add(new SqlParameter("Mode", 13));
            prmSalesOrder.Add(new SqlParameter("@SalesOrderID", objclsDTOSTSalesMaster.intSalesOrderID));
            objclsConnection.ExecuteNonQuery(strProcedureSalesOrder, prmSalesOrder);
        }

        public void SaveSalesOrderCancellationDetails()
        {
            //Function for saving SalesOrder cancellation details
            ArrayList prmSalesOrder;
            prmSalesOrder = new ArrayList();
            prmSalesOrder.Add(new SqlParameter("Mode", 12));
            prmSalesOrder.Add(new SqlParameter("SalesOrderID", objclsDTOSTSalesMaster.intSalesOrderID));
            prmSalesOrder.Add(new SqlParameter("CreatedBy", objclsDTOSTSalesMaster.intCreatedBy));
            prmSalesOrder.Add(new SqlParameter("CancellationDate", objclsDTOSTSalesMaster.strCancellationDate));
            prmSalesOrder.Add(new SqlParameter("Description", objclsDTOSTSalesMaster.strDescription));
            prmSalesOrder.Add(new SqlParameter("CompanyID", objclsDTOSTSalesMaster.intCompanyID));
            objclsConnection.ExecuteNonQuery(strProcedureSalesOrder, prmSalesOrder);
        }
        public DataTable SaveVendorFromSalesPro()
        {
            //Function for saving SalesOrder cancellation details
            ArrayList prmSalesOrder;
            prmSalesOrder = new ArrayList();
            prmSalesOrder.Add(new SqlParameter("Mode", 37));
            prmSalesOrder.Add(new SqlParameter("@SalesQuotationID", objclsDTOSTSalesMaster.intSalesQuotationID));
            prmSalesOrder.Add(new SqlParameter("@CompanyID",ClsCommonSettings.LoginCompanyID));
            return objclsConnection.ExecuteDataTable(strProcedureSalesQuotation, prmSalesOrder);
        }

        private DataTable DisplaySalesOrderCancellationDetails()
        {
            //function for displaying SalesOrder cancellation details
            ArrayList prmSalesOrder;
            prmSalesOrder = new ArrayList();
            prmSalesOrder.Add(new SqlParameter("Mode", 14));
            prmSalesOrder.Add(new SqlParameter("SalesOrderID", objclsDTOSTSalesMaster.intSalesOrderID));
            return objclsConnection.ExecuteDataTable(strProcedureSalesOrder, prmSalesOrder);
        }

        private void SetQuatationStatus()
        {
            //function for setting Sales Quatation status
            ArrayList prmSalesOrder;
            prmSalesOrder = new ArrayList();
            prmSalesOrder.Add(new SqlParameter("@Mode", 15));
            prmSalesOrder.Add(new SqlParameter("@ReferenceID", objclsDTOSTSalesMaster.intSalesQuotationID));
            objclsConnection.ExecuteNonQuery(strProcedureSalesOrder, prmSalesOrder);
        }

        private void ResetQuatationStatus()
        {
            //function for setting Sales Quatation status
            ArrayList prmSalesOrder;
            prmSalesOrder = new ArrayList();
            prmSalesOrder.Add(new SqlParameter("@Mode", 16));
            prmSalesOrder.Add(new SqlParameter("@ReferenceID", objclsDTOSTSalesMaster.intSalesQuotationID));
            if (ClsCommonSettings.SQApproval)
                prmSalesOrder.Add(new SqlParameter("@StatusID", (int)OperationStatusType.SQuotationApproved));
            else
                prmSalesOrder.Add(new SqlParameter("@StatusID", (int)OperationStatusType.SQuotationSubmitted));

            objclsConnection.ExecuteNonQuery(strProcedureSalesOrder, prmSalesOrder);
        }

        private bool UpdateStockValue()
        {
            //This is to Update Stock Value

            ArrayList prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation.Add(new SqlParameter("@Mode", 32));
            prmItemStockValueUpdation.Add(new SqlParameter("@SalesInvoiceID", objclsDTOSTSalesMaster.intSalesInvoiceID));
            prmItemStockValueUpdation.Add(new SqlParameter("@ItemStatusID", objclsDTOSTSalesMaster.intStatusID));
            prmItemStockValueUpdation.Add(new SqlParameter("@OperationTypeID", (int)OperationType.SalesInvoice));

            if (objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesInvoice, prmItemStockValueUpdation) != 0)
                return true;
            return false;
        }

        private bool DeleteSalesInvoiceDetailInformation(Int64 intSalesInvoiceID)
        {
            // Invoice Detail Delete
            if (objclsDTOSTSalesMaster.blnCancelled)
                DeleteItemSummarywhenCancelled();

            ArrayList prmDeleteInvoiceDetail;
            prmDeleteInvoiceDetail = new ArrayList();
            prmDeleteInvoiceDetail.Add(new SqlParameter("@Mode", 12));
            prmDeleteInvoiceDetail.Add(new SqlParameter("@SalesInvoiceID", intSalesInvoiceID));
            prmDeleteInvoiceDetail.Add(new SqlParameter("@ItemStatusID", objclsDTOSTSalesMaster.intStatusID));
            prmDeleteInvoiceDetail.Add(new SqlParameter("@OperationTypeID", (int)OperationType.SalesInvoice));

            objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesInvoice, prmDeleteInvoiceDetail);
            return true;
        }

        private void DeleteSalesInvoiceCancellationDetails()
        {
            //Function for deleting SalesInvoice cancellation details
            ArrayList prmSalesInvoice;
            prmSalesInvoice = new ArrayList();
            prmSalesInvoice.Add(new SqlParameter("Mode", 15));
            prmSalesInvoice.Add(new SqlParameter("@SalesInvoiceID", objclsDTOSTSalesMaster.intSalesInvoiceID));
            objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesInvoice, prmSalesInvoice);
        }

        public void SaveSalesInvoiceCancellationDetails()
        {
            //Function for saving SalesInvoice cancellation details
            ArrayList prmSalesInvoice;
            prmSalesInvoice = new ArrayList();
            prmSalesInvoice.Add(new SqlParameter("Mode", 14));
            prmSalesInvoice.Add(new SqlParameter("SalesInvoiceID", objclsDTOSTSalesMaster.intSalesInvoiceID));
            prmSalesInvoice.Add(new SqlParameter("CancelledBy", objclsDTOSTSalesMaster.intCreatedBy));
            prmSalesInvoice.Add(new SqlParameter("CancellationDate", objclsDTOSTSalesMaster.strCancellationDate));
            prmSalesInvoice.Add(new SqlParameter("Description", objclsDTOSTSalesMaster.strDescription));
            prmSalesInvoice.Add(new SqlParameter("CompanyID", objclsDTOSTSalesMaster.intCompanyID));
            objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesInvoice, prmSalesInvoice);
        }

        private DataTable DisplaySalesInvoiceCancellationDetails()
        {
            //function for displaying SalesInvoice cancellation details
            ArrayList prmSalesInvoice;
            prmSalesInvoice = new ArrayList();
            prmSalesInvoice.Add(new SqlParameter("Mode", 16));
            prmSalesInvoice.Add(new SqlParameter("SalesInvoiceID", objclsDTOSTSalesMaster.intSalesInvoiceID));
            return objclsConnection.ExecuteDataTable(strProcedureSalesInvoice, prmSalesInvoice);
        }

        private void SetOrderStatus()
        {
            //function for setting Sales Order status
            ArrayList prmSalesInvoice;
            prmSalesInvoice = new ArrayList();
            prmSalesInvoice.Add(new SqlParameter("@Mode", 17));
            prmSalesInvoice.Add(new SqlParameter("@SalesOrderID", objclsDTOSTSalesMaster.intSalesOrderID));
            objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesInvoice, prmSalesInvoice);
        }

        private void SetJobOrderStatus(int intStatusID, long intJobOrderID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 23));
            alParameters.Add(new SqlParameter("@JobOrderID", intJobOrderID));
            alParameters.Add(new SqlParameter("@StatusID", intStatusID));
            objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesQuotation, alParameters);
        }



        public bool IsDocumentsExists(Int64 intSalesOrderID)
        {
            //function for checking Documents Exists or not
            DataTable dtDocumentsCount = new DataTable();
            ArrayList prmSalesOrder;
            prmSalesOrder = new ArrayList();
            prmSalesOrder.Add(new SqlParameter("@Mode", 28));
            prmSalesOrder.Add(new SqlParameter("@SalesOrderID", objclsDTOSTSalesMaster.intSalesOrderID));
            dtDocumentsCount = objclsConnection.ExecuteDataTable(strProcedureSalesOrder, prmSalesOrder);

            if (dtDocumentsCount.Rows.Count > 0)
                return true;
            else
                return false;
        }

        private void ResetOrderStatus()
        {
            //function for setting Sales Order status
            ArrayList prmSalesInvoice;
            prmSalesInvoice = new ArrayList();
            prmSalesInvoice.Add(new SqlParameter("@Mode", 18));
            prmSalesInvoice.Add(new SqlParameter("@SalesOrderID", objclsDTOSTSalesMaster.intSalesOrderID));
            if (ClsCommonSettings.SOApproval)
                prmSalesInvoice.Add(new SqlParameter("@StatusID", (int)OperationStatusType.SOrderApproved));
            else
                prmSalesInvoice.Add(new SqlParameter("@StatusID", (int)OperationStatusType.SOrderSubmitted));
            objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesInvoice, prmSalesInvoice);
        }

        public DataTable GetCustomerWiseDiscounts(int intVendorID, decimal decRate, string strCurrentDate, int intCurrencyID)
        {
            prmSales = new ArrayList();
            if (string.IsNullOrEmpty(strCurrentDate))
            {
                prmSales.Add(new SqlParameter("@Mode", 24));

            }
            else
            {
                prmSales.Add(new SqlParameter("@Mode", 23));
                prmSales.Add(new SqlParameter("@CurrentDate", strCurrentDate));
            }
            prmSales.Add(new SqlParameter("@CurrencyID", intCurrencyID));
            prmSales.Add(new SqlParameter("@VendorID", intVendorID));
            prmSales.Add(new SqlParameter("@Rate", decRate));
            return objclsConnection.ExecuteDataTable(strProcedurePurchaseModuleFunctions, prmSales);
        }

        public DataTable GetItemWiseUOMs(int intItemID)
        {   //To Get ItemWiseUOMs
            ArrayList prmSalesParameters = new ArrayList();

            prmSalesParameters.Add(new SqlParameter("@Mode", 16));
            prmSalesParameters.Add(new SqlParameter("@ItemID", intItemID)); // Selects ItemID,ItemName,Type,ConvertionFactor,ConvertionValue
            return objclsConnection.ExecuteDataTable(strProcedurePurchaseModuleFunctions, prmSalesParameters);
        }

        public DataTable GetItemAndCustomerWiseDiscounts(int intItemID, int intVendorID, decimal decQty, decimal decRate, string strCurrentDate, int intCurrencyID)
        {   //To Get ItemAndCustomerWiseDiscounts
            ArrayList prmSalesParameters = new ArrayList();

            if (!string.IsNullOrEmpty(strCurrentDate))
                prmSalesParameters.Add(new SqlParameter("@Mode", 17));
            else
                prmSalesParameters.Add(new SqlParameter("@Mode", 22));

            prmSalesParameters.Add(new SqlParameter("@ItemID", intItemID)); // Selects DiscountID,DiscountName,Type 
            prmSalesParameters.Add(new SqlParameter("@VendorID", intVendorID));
            prmSalesParameters.Add(new SqlParameter("@Quantity", decQty));
            prmSalesParameters.Add(new SqlParameter("@Rate", decRate));
            prmSalesParameters.Add(new SqlParameter("@CurrentDate", strCurrentDate));
            prmSalesParameters.Add(new SqlParameter("@CurrencyID", intCurrencyID));
            return objclsConnection.ExecuteDataTable(strProcedurePurchaseModuleFunctions, prmSalesParameters);
        }

        public bool IsSalesNoExists(string strSalesNo, int intType, int intCompanyID)
        {
            bool blnIsExists = false;
            DataTable dtSales = new DataTable();
            switch (intType)
            {
                case 1:
                    prmSales = new ArrayList();
                    prmSales.Add(new SqlParameter("@Mode", 17));
                    prmSales.Add(new SqlParameter("@SalesQuotationNo", strSalesNo));
                    prmSales.Add(new SqlParameter("@CompanyID", intCompanyID));
                    dtSales = objclsConnection.ExecuteDataTable(strProcedureSalesQuotation, prmSales);
                    if (dtSales.Rows.Count > 0 && Convert.ToInt64(dtSales.Rows[0]["SalesQuotationID"]) != objclsDTOSTSalesMaster.intSalesQuotationID)
                        blnIsExists = true;
                    break;
                case 2:
                    prmSales = new ArrayList();
                    prmSales.Add(new SqlParameter("@Mode", 19));
                    prmSales.Add(new SqlParameter("@SalesOrderNo", strSalesNo));
                    prmSales.Add(new SqlParameter("@CompanyID", intCompanyID));
                    dtSales = objclsConnection.ExecuteDataTable(strProcedureSalesOrder, prmSales);
                    if (dtSales.Rows.Count > 0 && Convert.ToInt64(dtSales.Rows[0]["SalesOrderID"]) != objclsDTOSTSalesMaster.intSalesOrderID)
                        blnIsExists = true;
                    break;
                case 3:
                    prmSales = new ArrayList();
                    prmSales.Add(new SqlParameter("@Mode", 20));
                    prmSales.Add(new SqlParameter("@SalesInvoiceNo", strSalesNo));
                    prmSales.Add(new SqlParameter("@CompanyID", intCompanyID));
                    dtSales = objclsConnection.ExecuteDataTable(strProcedureSalesInvoice, prmSales);
                    if (dtSales.Rows.Count > 0 && Convert.ToInt64(dtSales.Rows[0]["SalesInvoiceID"]) != objclsDTOSTSalesMaster.intSalesInvoiceID)
                        blnIsExists = true;
                    break;
            }
            return blnIsExists;
        }

        public bool CompareAdvancepaymentAndTotAmt(long intSalesOrderID, decimal decNetAmount)
        {
            //function for checking Advance PAyment Amt and Sales order amt are same
            bool retvalue = false;
            ArrayList prmSalesOrder;
            prmSalesOrder = new ArrayList();
            prmSalesOrder.Add(new SqlParameter("@Mode", 27));
            prmSalesOrder.Add(new SqlParameter("@SalesOrderID", objclsDTOSTSalesMaster.intSalesOrderID));
            prmSalesOrder.Add(new SqlParameter("@NetAmount", decNetAmount));
            SqlDataReader sdr = objclsConnection.ExecuteReader(strProcedureSalesOrder, prmSalesOrder);

            if (sdr.Read())
            {
                retvalue = Convert.ToBoolean(sdr["ApproveStatus"]);
            }
            sdr.Close();
            return retvalue;

        }

        public bool DeleteSalesQuotation()
        {
            // function for deleting Sales Quotation
            object objOutSalesQuotationID = 0;
            prmSales = new ArrayList();
            prmSales.Add(new SqlParameter("@Mode", 4));
            prmSales.Add(new SqlParameter("@SalesQuotationID", objclsDTOSTSalesMaster.intSalesQuotationID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmSales.Add(objParam);
            //if (objclsDTOSTSalesMaster.intStatusID == (Int16)OperationStatusType.SQuotationRejected)
            VerificationHistoryTableUpdations(1, objclsDTOSTSalesMaster.intSalesQuotationID, true);
            if (objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesQuotation, prmSales) != 0)
                return true;
            else
                return false;
        }

        public bool DeleteSalesOrder()
        {
            // function for deleting Sales Order
            object objOutSalesOrderID = 0;
            prmSales = new ArrayList();
            prmSales.Add(new SqlParameter("@Mode", 4));
            prmSales.Add(new SqlParameter("@SalesOrderID", objclsDTOSTSalesMaster.intSalesOrderID));
            prmSales.Add(new SqlParameter("@ReferenceID", objclsDTOSTSalesMaster.intSalesQuotationID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmSales.Add(objParam);
            // if(objclsDTOSTSalesMaster.intStatusID == (Int16)OperationStatusType.SOrderRejected)
            VerificationHistoryTableUpdations(2, objclsDTOSTSalesMaster.intSalesOrderID, true);
            if (objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesOrder, prmSales) != 0)
            {
                if (objclsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SOrderCancelled)
                {
                    if (objclsDTOSTSalesMaster.intOrderTypeID == (int)OperationOrderType.SOTFromQuotation)
                    {
                        ResetQuatationStatus();
                    }
                }
                return true;
            }
            else
                return false;
        }

        public bool DeleteSalesInvoice()
        {
            // function for deleting Sales Invoice
            DeleteItemSummary();
            object objOutSalesInvoiceID = 0;
            prmSales = new ArrayList();
            prmSales.Add(new SqlParameter("@Mode", 4));
            prmSales.Add(new SqlParameter("@SalesOrderID", objclsDTOSTSalesMaster.intSalesOrderID));
            prmSales.Add(new SqlParameter("@SalesInvoiceID", objclsDTOSTSalesMaster.intSalesInvoiceID));
            prmSales.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            prmSales.Add(new SqlParameter("@StatusID", objclsDTOSTSalesMaster.intStatusID));
            prmSales.Add(new SqlParameter("CompanyID", objclsDTOSTSalesMaster.intCompanyID));
            prmSales.Add(new SqlParameter("@OperationTypeID", (int)OperationType.SalesInvoice));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmSales.Add(objParam);
            if (objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesInvoice, prmSales) != 0)
            {
                DeletePayments(objclsDTOSTSalesMaster.intSalesInvoiceID.ToInt32());
                if (objclsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SInvoiceCancelled)
                {
                    ResetOrderStatus();
                }
                return true;
            }
            else
                return false;
        }

        public double GetExpenseAmount()
        {
            double decExpenseAmount = 0;

            ArrayList prmExpenseAmount = new ArrayList();
            prmExpenseAmount.Add(new SqlParameter("@Mode", 21));
            prmExpenseAmount.Add(new SqlParameter("@SalesOrderID", objclsDTOSTSalesMaster.intSalesOrderID));

            DataTable datExpenseAmount = objclsConnection.ExecuteDataTable(strProcedureSalesOrder, prmExpenseAmount);

            if (datExpenseAmount.Rows.Count > 0)
            {
                if (datExpenseAmount.Rows[0]["ExpenseAmount"] != DBNull.Value)
                {
                    decExpenseAmount = Convert.ToDouble(datExpenseAmount.Rows[0]["ExpenseAmount"]);
                }
                if (decExpenseAmount >= 0)
                    return decExpenseAmount;
            }

            return decExpenseAmount;
        }

        public void RejectSalesOrder()
        {
            ArrayList alParameters = new ArrayList();

            alParameters.Add(new SqlParameter("@Mode", 21));
            alParameters.Add(new SqlParameter("@SalesOrderID", objclsDTOSTSalesMaster.intSalesOrderID));
            alParameters.Add(new SqlParameter("@ApprovedBy", objclsDTOSTSalesMaster.intApprovedBy));
            alParameters.Add(new SqlParameter("@ApprovedDate", objclsDTOSTSalesMaster.strApprovedDate));
            alParameters.Add(new SqlParameter("@VerificationDescription", objclsDTOSTSalesMaster.strVerificationDescription));
            alParameters.Add(new SqlParameter("@StatusID", objclsDTOSTSalesMaster.intStatusID));
            objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesInvoice, alParameters);

            long lngSalesQuotationID = 0;
            DataTable datQuotationDetails = MobjClsCommonUtility.FillCombos(new string[] { "DISTINCT SORD.ReferenceID", "InvSalesOrderMaster SOM INNER JOIN InvSalesOrderReferenceDetails SORD ON SORD.SalesOrderID = SOM.SalesOrderID  ", "SOM.SalesOrderID = " + objclsDTOSTSalesMaster.intSalesOrderID + " And SOM.OrderTypeID = " + (int)OperationOrderType.SOTFromQuotation });
            if (datQuotationDetails.Rows.Count > 0)
                lngSalesQuotationID = datQuotationDetails.Rows[0]["ReferenceID"].ToInt64();

            VerificationHistoryTableUpdations(2, objclsDTOSTSalesMaster.intSalesOrderID, false);
        }

        public object IsSalesOrderNoExists()
        {
            ArrayList alParameters = new ArrayList();

            alParameters.Add(new SqlParameter("@Mode", 17));
            alParameters.Add(new SqlParameter("@SalesOrderNo", objclsDTOSTSalesMaster.strSalesOrderNo));

            return objclsConnection.ExecuteScalar(strProcedureSalesOrder, alParameters);
        }

        public bool GetCompanyAccount(int intTransactionType)
        {
            // function for Geting Company Account
            object objOutAccountID = 0;
            prmSales = new ArrayList();
            prmSales.Add(new SqlParameter("@Mode", 18));
            prmSales.Add(new SqlParameter("@CompanyID", objclsDTOSTSalesMaster.intCompanyID));
            prmSales.Add(new SqlParameter("@TransactionType", intTransactionType));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmSales.Add(objParam);

            if (objclsConnection.ExecuteNonQuery(strProcedurePurchaseModuleFunctions, prmSales, out objOutAccountID) != 0)
            {
                ClsCommonSettings.PintSICreditHeadID = (int)objOutAccountID;
                if ((int)objOutAccountID > 0) return true;
            }
            return false;
        }
        public Int32 GetCompanyAccountID(int intTransactionType, int intCmpnyAcc)
        {
            // function for Geting Company Account
            object objOutAccountID = 0;
            prmSales = new ArrayList();
            prmSales.Add(new SqlParameter("@Mode", 18));
            prmSales.Add(new SqlParameter("@CompanyID", intCmpnyAcc));
            prmSales.Add(new SqlParameter("@TransactionType", intTransactionType));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmSales.Add(objParam);

            if (objclsConnection.ExecuteNonQuery(strProcedurePurchaseModuleFunctions, prmSales, out objOutAccountID) != 0)
            {
                ClsCommonSettings.PintSICreditHeadID = (int)objOutAccountID;
                if ((int)objOutAccountID > 0) return (int)objOutAccountID;
            }
            return 0;
        }
        public bool GetExchangeCurrencyRate(int intCompanyID, int intCurrencyID)
        {
            ArrayList prmExchangeCurrencyRate = new ArrayList();
            prmExchangeCurrencyRate.Add(new SqlParameter("@Mode", 19));
            prmExchangeCurrencyRate.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmExchangeCurrencyRate.Add(new SqlParameter("@CurrencyID", intCurrencyID));

            DataTable datExchangeCurrencyRate = objclsConnection.ExecuteDataTable(strProcedurePurchaseModuleFunctions, prmExchangeCurrencyRate);

            if (datExchangeCurrencyRate.Rows.Count > 0)
            {
                if (datExchangeCurrencyRate.Rows[0]["Exchangerate"] != DBNull.Value)
                    objclsDTOSTSalesMaster.decExchangeCurrencyRate = Convert.ToDecimal(datExchangeCurrencyRate.Rows[0]["Exchangerate"]);

                if (objclsDTOSTSalesMaster.decExchangeCurrencyRate >= 0)
                    return true;
            }

            return false;

        }

        // for email
        public DataSet GetSalesQuotationReport()
        {
            ArrayList alParameters = new ArrayList();
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 16));
            alParameters.Add(new SqlParameter("@SalesQuotationID ", objclsDTOSTSalesMaster.intSalesQuotationID));

            return objclsConnection.ExecuteDataSet(strProcedureSalesQuotation, alParameters);
        }


        // for email
        public DataSet GetSalesOrderReport()
        {
            ArrayList alParameters = new ArrayList();
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 18));
            alParameters.Add(new SqlParameter("@SalesOrderID", objclsDTOSTSalesMaster.intSalesOrderID));

            return objclsConnection.ExecuteDataSet(strProcedureSalesOrder, alParameters);
        }

        // for email
        public DataSet GetSalesInvoiceReport()
        {
            ArrayList alParameters = new ArrayList();
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 19));
            alParameters.Add(new SqlParameter("@SalesInvoiceID", objclsDTOSTSalesMaster.intSalesInvoiceID));

            return objclsConnection.ExecuteDataSet(strProcedureSalesInvoice, alParameters);
        }

        private bool VerificationHistoryTableUpdations(int intType, Int64 intReferenceID, bool blnIsDeletion)
        {
            // 1 - Sales Quotation  2 - Sales Order
            ArrayList prmVerHistory = new ArrayList();
            if (intType == 1)
            {
                if (!blnIsDeletion)
                    prmVerHistory.Add(new SqlParameter("@Mode", 18));
                else
                    prmVerHistory.Add(new SqlParameter("@Mode", 19));
                prmVerHistory.Add(new SqlParameter("@OperationTypeID", (Int32)OperationType.SalesQuotation));
                prmVerHistory.Add(new SqlParameter("@SalesQuotationID", intReferenceID));

                prmVerHistory.Add(new SqlParameter("@ApprovedBy", objclsDTOSTSalesMaster.intApprovedBy));
                prmVerHistory.Add(new SqlParameter("@ApprovedDate", objclsDTOSTSalesMaster.strApprovedDate));

                prmVerHistory.Add(new SqlParameter("@VerificationDescription", objclsDTOSTSalesMaster.strVerificationDescription));
                prmVerHistory.Add(new SqlParameter("@CompanyID", objclsDTOSTSalesMaster.intCompanyID));
                prmVerHistory.Add(new SqlParameter("@StatusID", objclsDTOSTSalesMaster.intStatusID));

                if (objclsDTOSTSalesMaster.intVerificationCriteriaID > 0)
                    prmVerHistory.Add(new SqlParameter("@VerificationCriteriaID", objclsDTOSTSalesMaster.intVerificationCriteriaID));

                objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesQuotation, prmVerHistory);
                return true;
            }
            else if (intType == 2)
            {
                if (!blnIsDeletion)
                    prmVerHistory.Add(new SqlParameter("@Mode", 22));
                else
                    prmVerHistory.Add(new SqlParameter("@Mode", 24));
                prmVerHistory.Add(new SqlParameter("@OperationTypeID", (Int32)OperationType.SalesOrder));
                prmVerHistory.Add(new SqlParameter("@SalesOrderID", intReferenceID));

                prmVerHistory.Add(new SqlParameter("@ApprovedBy", objclsDTOSTSalesMaster.intApprovedBy));
                prmVerHistory.Add(new SqlParameter("@ApprovedDate", objclsDTOSTSalesMaster.strApprovedDate));

                prmVerHistory.Add(new SqlParameter("@VerificationDescription", objclsDTOSTSalesMaster.strVerificationDescription));
                prmVerHistory.Add(new SqlParameter("@CompanyID", objclsDTOSTSalesMaster.intCompanyID));
                prmVerHistory.Add(new SqlParameter("@StatusID", objclsDTOSTSalesMaster.intStatusID));

                if (objclsDTOSTSalesMaster.intVerificationCriteriaID > 0)
                    prmVerHistory.Add(new SqlParameter("@VerificationCriteriaID", objclsDTOSTSalesMaster.intVerificationCriteriaID));

                objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesOrder, prmVerHistory);
                return true;
            }
            return false;
        }

        public string GetSaleOrderNo(long intOrderID)
        {   //Order
            ArrayList prmOrder = new ArrayList();
            prmOrder.Add(new SqlParameter("@Mode", 23));
            prmOrder.Add(new SqlParameter("@SalesOrderID", intOrderID));

            return objclsConnection.ExecuteScalar(strProcedureSalesOrder, prmOrder).ToString();
        }

        public DataTable GetAlertSetting(long lngAlertSettingID)
        {
            ArrayList parameters = null;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 5));
                parameters.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));

                return this.objclsConnection.ExecuteDataTable(strProcedureAlertSetting, parameters);
            }
            catch (Exception ex) { throw ex; }
            finally { if (parameters != null) { parameters.Clear(); parameters = null; } }
        }

        public DataTable GetAlertUserSetting(long lngAlertSettingID)
        {
            ArrayList parameters = null;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 9));
                parameters.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));

                return objclsConnection.ExecuteDataTable(strProcedureAlertUserSetting, parameters);
            }
            catch (Exception ex) { throw ex; }
            finally { if (parameters != null) { parameters.Clear(); parameters = null; } }
        }

        public DataTable GetAlertRoleSetting(long lngAlertSettingID)
        {
            ArrayList parameters = null;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 6));
                parameters.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));

                return objclsConnection.ExecuteDataTable(strProcedureAlertRoleSetting, parameters);
            }
            catch (Exception ex) { throw ex; }
            finally { if (parameters != null) { parameters.Clear(); parameters = null; } }
        }

        public string GetCompanyCurrency(int intCompanyID, out int intScale, out int intCurrencyID)
        {
            intScale = 2;
            intCurrencyID = 0;
            string strCurrency = "";
            prmSales = new ArrayList();
            prmSales.Add(new SqlParameter("@Mode", 20));
            prmSales.Add(new SqlParameter("@CompanyID", intCompanyID));
            SqlDataReader sdr = objclsConnection.ExecuteReader(strProcedurePurchaseModuleFunctions, prmSales);
            if (sdr.Read())
            {
                if (sdr["Currency"] != DBNull.Value)
                {
                    strCurrency = Convert.ToString(sdr["Currency"]);
                }
                if (sdr["Scale"] != DBNull.Value)
                {
                    intScale = Convert.ToInt32(sdr["Scale"]);
                }
                if (sdr["CurrencyID"] != DBNull.Value)
                {
                    intCurrencyID = Convert.ToInt32(sdr["CurrencyID"]);
                }
            }
            return strCurrency;
        }

        private bool SaveExpenses(int intType)
        {
            // 1 - SalesOrder , 2 - SalesQuotation
            if (intType == 1)
            {
                if (objclsDTOSTSalesMaster.intOrderTypeID == (Int32)OperationOrderType.SOTFromQuotation)
                {
                    clsDALExpenseMaster objClsDALExpenseMaster = new clsDALExpenseMaster();
                    objClsDALExpenseMaster.objConnection = objclsConnection;
                    objClsDALExpenseMaster.objDTOExpenseMaster = new clsDTOExpenseMaster();
                    objClsDALExpenseMaster.objDTOExpenseMaster.intRowNumber = 1;
                    objClsDALExpenseMaster.objDTOExpenseMaster.blnPostToAccount = true;
                    if (objClsDALExpenseMaster.GetExpenseInfo((int)OperationType.SalesQuotation, objclsDTOSTSalesMaster.intSalesQuotationID, 2))
                    {
                        objClsDALExpenseMaster.objDTOExpenseMaster.intOperationType = (int)OperationType.SalesOrder;
                        objClsDALExpenseMaster.objDTOExpenseMaster.lngReferenceID = objclsDTOSTSalesMaster.intSalesOrderID;
                        objClsDALExpenseMaster.objDTOExpenseMaster.lngExpenseID = 0;
                        objClsDALExpenseMaster.objDTOExpenseMaster.blnPostToAccount = true;

                        foreach (clsDTOExpenseDetails objExpenseDetails in objClsDALExpenseMaster.objDTOExpenseMaster.objDTOExpenseDetails)
                            objExpenseDetails.intSerialNo = 0;

                        objClsDALExpenseMaster.SaveExpenseInfo();
                    }
                }
            }
            else
            {
                clsDALExpenseMaster objClsDALExpenseMaster = new clsDALExpenseMaster();
                objClsDALExpenseMaster.objConnection = objclsConnection;
                objClsDALExpenseMaster.objDTOExpenseMaster = new clsDTOExpenseMaster();
                objClsDALExpenseMaster.objDTOExpenseMaster.intRowNumber = 1;
                objClsDALExpenseMaster.objDTOExpenseMaster.blnPostToAccount = true;
                if (objClsDALExpenseMaster.GetExpenseInfo((int)OperationType.SalesOrder, objclsDTOSTSalesMaster.intSalesOrderID, 2))
                {
                    objClsDALExpenseMaster.objDTOExpenseMaster.intOperationType = (int)OperationType.SalesInvoice;
                    objClsDALExpenseMaster.objDTOExpenseMaster.lngReferenceID = objclsDTOSTSalesMaster.intSalesInvoiceID;
                    objClsDALExpenseMaster.objDTOExpenseMaster.lngExpenseID = 0;
                    objClsDALExpenseMaster.objDTOExpenseMaster.blnPostToAccount = true;

                    foreach (clsDTOExpenseDetails objExpenseDetails in objClsDALExpenseMaster.objDTOExpenseMaster.objDTOExpenseDetails)
                        objExpenseDetails.intSerialNo = 0;

                    objClsDALExpenseMaster.SaveExpenseInfo();
                }
            }
            return true;
        }

        public bool UpdateNetAmount(int intFormType)
        {
            // 1- SalesQuotation 2 - Sales Order 3 - Sales Invoice
            prmSales = new ArrayList();


            prmSales.Add(new SqlParameter("@ExpenseAmount", objclsDTOSTSalesMaster.decExpenseAmount));
            prmSales.Add(new SqlParameter("@GrandDiscountAmt", objclsDTOSTSalesMaster.decGrandDiscountAmt));
            prmSales.Add(new SqlParameter("@NetAmount", objclsDTOSTSalesMaster.decNetAmount));
            switch (intFormType)
            {
                case 1:
                    prmSales.Add(new SqlParameter("@Mode", 20));
                    prmSales.Add(new SqlParameter("@SalesQuotationID", objclsDTOSTSalesMaster.intSalesQuotationID));
                    objclsConnection.ExecuteNonQuery(strProcedureSalesQuotation, prmSales);
                    break;
                case 2:
                    prmSales.Add(new SqlParameter("@Mode", 25));
                    prmSales.Add(new SqlParameter("@SalesOrderID", objclsDTOSTSalesMaster.intSalesOrderID));
                    prmSales.Add(new SqlParameter("@NetAmountRounded", objclsDTOSTSalesMaster.decNetAmountRounded));
                    objclsConnection.ExecuteNonQuery(strProcedureSalesOrder, prmSales);
                    break;
                case 3:
                    prmSales.Add(new SqlParameter("@Mode", 26));
                    prmSales.Add(new SqlParameter("@SalesInvoiceID", objclsDTOSTSalesMaster.intSalesInvoiceID));
                    prmSales.Add(new SqlParameter("@NetAmountRounded", objclsDTOSTSalesMaster.decNetAmountRounded));
                    prmSales.Add(new SqlParameter("@OperationTypeID", (int)OperationType.SalesInvoice));
                    objclsConnection.ExecuteNonQuery(strProcedureSalesInvoice, prmSales);
                    break;
            }
            return true;
        }

        public DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {
            ArrayList prmUomDetails = new ArrayList();
            prmUomDetails.Add(new SqlParameter("@Mode", 11));
            prmUomDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmUomDetails.Add(new SqlParameter("@UnitID", intUOMID));
            //   prmUomDetails.Add(new SqlParameter("@UomTypeID", 1));
            return objclsConnection.ExecuteDataTable(strProcedurePurchaseModuleFunctions, prmUomDetails);
        }

        public DataTable GetDataForItemSelection(int intCompanyID, int intCurrencyID, int intFormType)
       {
            prmSales = new ArrayList();
            // if (intFormType == 1)  
            //     prmSales.Add(new SqlParameter("@Mode", 29));
            //  else
            //prmSales.Add(new SqlParameter("@Mode", 33));
            prmSales.Add(new SqlParameter("@InvoiceDate", ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")));
            prmSales.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmSales.Add(new SqlParameter("@CurrencyID", intCurrencyID));
            DataSet ds = objclsConnection.ExecuteDataSet("spInvItemSelectionSalesOrder", prmSales);
            if (ds.Tables.Count > 0)
            {

                if (ds.Tables.Count > 1)
                {
                   DataTable dt = ds.Tables[0].Copy();
                   
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        DataRow dr1 = dt.NewRow();
                        dr1["ItemCode"] = dr["ItemCode"];
                        dr1["Description"] = dr["Description"];
                        dr1["ItemID"] = dr["ItemGroupID"];
                        dr1["Sale Rate"] = dr["Sale Rate"];
                        //if(intFormType !=1)
                        dr1["QtyAvailable"] = dr["AvaliableQty"];
                        dr1["IsGroup"] = true;
                        dr1["IsGroupItem"] = "Yes";
                        dt.Rows.Add(dr1);
                    }
                    return dt;
                }
                else
                    return ds.Tables[0];
            }
            return null;
        }

        public decimal GetAdvancePayment(Int64 intSalesOrderID)
        {
            ArrayList prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", 28));
            prmPayments.Add(new SqlParameter("@OperationTypeID", (Int32)OperationType.SalesOrder));
            prmPayments.Add(new SqlParameter("@SalesOrderID", intSalesOrderID));
            SqlDataReader sdr = objclsConnection.ExecuteReader(strProcedureSalesInvoice, prmPayments);
            if (sdr.Read())
            {
                if (sdr["TotalAmount"] != DBNull.Value)
                    return Convert.ToDecimal(sdr["TotalAmount"]);
            }
            return 0;
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 4));
            prmCommon.Add(new SqlParameter("@RoleID", intRoleID));
            prmCommon.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmCommon.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable(strProcedurePermissionSettings, prmCommon);
        }

        public DataTable GetOperationTypeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable(strProcedurePermissionSettings, parameters);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 12));
            prmCommon.Add(new SqlParameter("@RoleID", intRoleID));
            prmCommon.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmCommon.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable(strProcedurePermissionSettings, prmCommon);
        }

        public bool Suggest(int intOperationTypeID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 26));
            alParameters.Add(new SqlParameter("@OperationTypeID", intOperationTypeID));
            if (intOperationTypeID == (int)OperationType.SalesQuotation)
                alParameters.Add(new SqlParameter("@ReferenceID", objclsDTOSTSalesMaster.intSalesQuotationID));
            else if (intOperationTypeID == (int)OperationType.SalesOrder)
                alParameters.Add(new SqlParameter("@ReferenceID", objclsDTOSTSalesMaster.intSalesOrderID));

            alParameters.Add(new SqlParameter("@Description", objclsDTOSTSalesMaster.strDescription));
            alParameters.Add(new SqlParameter("@StatusID", objclsDTOSTSalesMaster.intStatusID));
            alParameters.Add(new SqlParameter("@CancelledBy", objclsDTOSTSalesMaster.intCancelledBy));
            alParameters.Add(new SqlParameter("@CancellationDate", objclsDTOSTSalesMaster.strCancellationDate));
            if (objclsConnection.ExecuteNonQuery(strProcedurePurchaseQuotation, alParameters) != 0)
            {
                return true;
            }
            return false;
        }

        private bool SaveItemSummary(int intItemID, int intBatchID, decimal decQty, int intUomID, decimal decNetAmt, decimal decDisAmt, bool IsGroup)
        {
            bool blnStatus = false;
            if (!(objclsDTOSTSalesMaster.blnCancelled))
            {
                try
                {
                    ArrayList prmItemSummary = new ArrayList();
                    if (IsGroup == false)
                    {
                        prmItemSummary = new ArrayList();
                        prmItemSummary.Add(new SqlParameter("@Mode", 30));
                        prmItemSummary.Add(new SqlParameter("@StatusID", objclsDTOSTSalesMaster.intStatusID));
                        prmItemSummary.Add(new SqlParameter("@ItemID", intItemID));
                        prmItemSummary.Add(new SqlParameter("@BatchID", intBatchID));
                        prmItemSummary.Add(new SqlParameter("@Quantity", decQty));
                        prmItemSummary.Add(new SqlParameter("@UOMID", intUomID));
                        prmItemSummary.Add(new SqlParameter("@CompanyID", objclsDTOSTSalesMaster.intCompanyID));
                        prmItemSummary.Add(new SqlParameter("@InvoiceDate", objclsDTOSTSalesMaster.strInvoiceDate));
                        prmItemSummary.Add(new SqlParameter("@BaseUnitQty", MobjClsCommonUtility.ConvertQtyToBaseUnitQty(intUomID, intItemID, decQty, 1)));
                        prmItemSummary.Add(new SqlParameter("@NetAmount", decNetAmt));
                        prmItemSummary.Add(new SqlParameter("@DiscountAmt", decDisAmt));
                        prmItemSummary.Add(new SqlParameter("@CurrencyID", objclsDTOSTSalesMaster.intCurrencyID));
                        objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesInvoice, prmItemSummary);
                        blnStatus = true;
                    }
                    else
                    {
                        DataTable datTemp = FillCombos(new string[] { "*", strTableItemGroupDetails, "ItemGroupID=" + intItemID });
                        if (datTemp != null)
                        {
                            if (datTemp.Rows.Count > 0)
                            {
                                for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                                {
                                    prmItemSummary = new ArrayList();
                                    prmItemSummary.Add(new SqlParameter("@Mode", 30));
                                    prmItemSummary.Add(new SqlParameter("@StatusID", objclsDTOSTSalesMaster.intStatusID));
                                    prmItemSummary.Add(new SqlParameter("@ItemID", Convert.ToInt32(datTemp.Rows[iCounter]["ItemID"].ToString())));
                                    try { prmItemSummary.Add(new SqlParameter("@BatchID", Convert.ToInt32(datTemp.Rows[iCounter]["BatchID"].ToString()))); }
                                    catch { prmItemSummary.Add(new SqlParameter("@BatchID", 0)); }
                                    prmItemSummary.Add(new SqlParameter("@Quantity", decQty * Convert.ToDecimal(datTemp.Rows[iCounter]["Quantity"].ToString())));
                                    prmItemSummary.Add(new SqlParameter("@UOMID", Convert.ToInt32(datTemp.Rows[iCounter]["UOMID"].ToString())));
                                    prmItemSummary.Add(new SqlParameter("@CompanyID", objclsDTOSTSalesMaster.intCompanyID));
                                    prmItemSummary.Add(new SqlParameter("@InvoiceDate", objclsDTOSTSalesMaster.strInvoiceDate));
                                    prmItemSummary.Add(new SqlParameter("@BaseUnitQty", MobjClsCommonUtility.ConvertQtyToBaseUnitQty
                                        (Convert.ToInt32(datTemp.Rows[iCounter]["UOMID"].ToString()),
                                        Convert.ToInt32(datTemp.Rows[iCounter]["ItemID"].ToString()),
                                        decQty * Convert.ToDecimal(datTemp.Rows[iCounter]["Quantity"].ToString()), 1)));
                                    prmItemSummary.Add(new SqlParameter("@NetAmount", (decQty * Convert.ToDecimal(datTemp.Rows[iCounter]["Quantity"].ToString())) *
                                        Convert.ToDecimal(datTemp.Rows[iCounter]["ItemTotal"].ToString())));
                                    prmItemSummary.Add(new SqlParameter("@DiscountAmt", 0));
                                    prmItemSummary.Add(new SqlParameter("@CurrencyID", objclsDTOSTSalesMaster.intCurrencyID));
                                    objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesInvoice, prmItemSummary);
                                    blnStatus = true;
                                }
                            }
                        }
                    }
                }
                catch { blnStatus = false; }
            }
            return blnStatus;
        }

        private bool DeleteItemSummary()
        {
            bool blnStatus = false;
            if (!(objclsDTOSTSalesMaster.blnCancelled))
            {
                try
                {
                    ArrayList prmItemSummary = new ArrayList();
                    prmItemSummary.Add(new SqlParameter("@Mode", 31));
                    prmItemSummary.Add(new SqlParameter("@SalesInvoiceID", objclsDTOSTSalesMaster.intSalesInvoiceID));
                    objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesInvoice, prmItemSummary);
                    blnStatus = true;
                }
                catch { blnStatus = false; }
            }
            return blnStatus;
        }

        private bool DeleteItemSummarywhenCancelled()
        {
            bool blnStatus = false;
            if (objclsDTOSTSalesMaster.blnCancelled)
            {
                try
                {
                    ArrayList prmItemSummary = new ArrayList();
                    prmItemSummary.Add(new SqlParameter("@Mode", 31));
                    prmItemSummary.Add(new SqlParameter("@SalesInvoiceID", objclsDTOSTSalesMaster.intSalesInvoiceID));
                    objclsConnection.ExecuteNonQueryWithTran(strProcedureSalesInvoice, prmItemSummary);
                    blnStatus = true;
                }
                catch { blnStatus = false; }
            }
            return blnStatus;
        }

        public bool SavePayments(int intSalesID)
        {
            //Saving Payments
            if (objclsDTOSTSalesMaster.decNetAmount - objclsDTOSTSalesMaster.decAdvanceAmount > 0)
            {
                int intTempPaymentID = 0;
                long lngSerialNo = 0;
                DataTable datTempPay = FillCombos(new string[] { "*", strTableReceiptAndPayment, "RPD.ReferenceID=" + intSalesID + " AND Payments.OperationTypeID=" + (int)OperationType.SalesInvoice });
                if (datTempPay != null)
                {
                    if (datTempPay.Rows.Count > 0)
                    {
                        intTempPaymentID = Convert.ToInt32(datTempPay.Rows[0]["ReceiptAndPaymentID"].ToString());
                        lngSerialNo = datTempPay.Rows[0]["SerialNo"].ToInt64();
                    }
                }
                //DataTable datTemp = FillCombos(new string[] { "*", "CompanySettings", "CompanyID=" + objclsDTOSTSalesMaster.intCompanyID });
                //string ReceiptsPrefix = GetCompanySettingsValue("Receipts", "Prefix", datTemp);

                ArrayList prmPayments = new ArrayList();
                prmPayments.Add(new SqlParameter("@Mode", Convert.ToInt32(intTempPaymentID) == 0 ? 20 : 21));
                prmPayments.Add(new SqlParameter("@ReceiptAndPaymentID", Convert.ToInt32(intTempPaymentID)));
                prmPayments.Add(new SqlParameter("@PaymentDate", Convert.ToDateTime(objclsDTOSTSalesMaster.strInvoiceDate).ToString("dd-MMM-yyyy")));
                prmPayments.Add(new SqlParameter("@OperationTypeID", (int)OperationType.SalesInvoice));
                prmPayments.Add(new SqlParameter("@PaymentTypeID", (int)PaymentTypes.InvoicePayment));
                prmPayments.Add(new SqlParameter("@PaymentModeID", (int)PaymentModes.Cash));
                prmPayments.Add(new SqlParameter("@ReferenceID", intSalesID));
                prmPayments.Add(new SqlParameter("@CreditHeadID1", 0));
                prmPayments.Add(new SqlParameter("@DebitHeadID", (int)Accounts.CashAccount));
                prmPayments.Add(new SqlParameter("@Amount", objclsDTOSTSalesMaster.decNetAmount - objclsDTOSTSalesMaster.decAdvanceAmount));
                prmPayments.Add(new SqlParameter("@VoucherNo", ""));
                prmPayments.Add(new SqlParameter("VoucherIds", ""));
                prmPayments.Add(new SqlParameter("@PostToAccount", 1));
                prmPayments.Add(new SqlParameter("@CurrencyID", objclsDTOSTSalesMaster.intCurrencyID));
                prmPayments.Add(new SqlParameter("@CompanyID", objclsDTOSTSalesMaster.intCompanyID));
                prmPayments.Add(new SqlParameter("@VendorID", objclsDTOSTSalesMaster.intVendorID));

                prmPayments.Add(new SqlParameter("@CreatedBy", objclsDTOSTSalesMaster.intCreatedBy));
                prmPayments.Add(new SqlParameter("@CreatedDate", Convert.ToDateTime(objclsDTOSTSalesMaster.strCreatedDate).ToString("dd-MMM-yyyy")));
                prmPayments.Add(new SqlParameter("@Description", objclsDTOSTSalesMaster.strRemarks));
                prmPayments.Add(new SqlParameter("@StatusID", objclsDTOSTSalesMaster.intStatusID));
                prmPayments.Add(new SqlParameter("@OperationModID", (Int32)TransactionTypes.CashSale));
                prmPayments.Add(new SqlParameter("@IsReceipt", 1));
                prmPayments.Add(new SqlParameter("@FormType", "SI"));
                SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
                objParam.Direction = ParameterDirection.ReturnValue;
                prmPayments.Add(objParam);
                if (intTempPaymentID == 0)
                    lngSerialNo = GetMaxSerialNo(objclsDTOSTSalesMaster.intCompanyID);

                string strPaymentNumber = ClsCommonSettings.ReceiptsPrefix + lngSerialNo.ToString();
                prmPayments.Add(new SqlParameter("@PaymentNumber", strPaymentNumber));
                prmPayments.Add(new SqlParameter("@SerialNo", lngSerialNo));

                object objOutPaymentID = 0;
                if (objclsConnection.ExecuteNonQueryWithTran(strProcedurePOS, prmPayments, out objOutPaymentID) != 0)
                {
                    if (Convert.ToInt64(objOutPaymentID) > 0)
                    {
                        //objDTOPaymentMaster.intPaymentID = Convert.ToInt64(objOutPaymentID);
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        public bool DeletePayments(int intSalesID)
        {
            //Delete Payments
            int intTempPaymentID = 0;
            DataTable datTempPay = FillCombos(new string[] { "*", strTableReceiptAndPayment, "RPD.ReferenceID=" + intSalesID + " AND Payments.OperationTypeID=" + (int)OperationType.SalesInvoice });
            if (datTempPay != null)
            {
                if (datTempPay.Rows.Count > 0)
                    intTempPaymentID = Convert.ToInt32(datTempPay.Rows[0]["ReceiptAndPaymentID"].ToString());
            }
            ArrayList prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", 7));
            prmPayments.Add(new SqlParameter("@ReceiptAndPaymentID", intTempPaymentID));
            objclsConnection.ExecuteNonQueryWithTran("spAccReceiptsAndPayments", prmPayments);

            ArrayList prmPayments2 = new ArrayList();
            prmPayments2.Add(new SqlParameter("@Mode", 6));
            prmPayments2.Add(new SqlParameter("@ReceiptAndPaymentID", intTempPaymentID));

            if (objclsConnection.ExecuteNonQueryWithTran("spAccReceiptsAndPayments", prmPayments2) != 0)
            {
                return true;
            }
            return false;
        }

        public Int64 GetMaxSerialNo(int intCompanyID)
        {
            // function for getting max of paymentsID
            ArrayList prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", 2));
            prmPayments.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPayments.Add(new SqlParameter("@IsReceipt", 1));
            DataTable datTemp = objclsConnection.ExecuteDataTable(strProcedureReceiptAndPayment, prmPayments);
            Int64 intRetValue = 0;
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    try { intRetValue = Convert.ToInt32(datTemp.Rows[0][0].ToString()); }
                    catch { }
                }
            }
            return intRetValue + 1;
        }

        public string GetCompanySettingsValue(string strConfigurationItem, string strSubItem, DataTable dtCompanySettings)
        {
            dtCompanySettings.DefaultView.RowFilter = "ConfigurationItem = '" + strConfigurationItem + "' And SubItem = '" + strSubItem + "'";
            if (dtCompanySettings.DefaultView.ToTable().Rows.Count > 0)
                return dtCompanySettings.DefaultView.ToTable().Rows[0]["ConfigurationValue"].ToString();
            return "";
        }

        public decimal GetSalesInvoiceReservedQty(long lngItemID, long lngBatchID, long lngSalesInvoiceID, int intCompanyID)
        {
            decimal decInvoiceQty = 0;


            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 15));
            alParameters.Add(new SqlParameter("@ItemID", lngItemID));
            alParameters.Add(new SqlParameter("@BatchID", lngBatchID));
            alParameters.Add(new SqlParameter("@SalesInvoiceID", lngSalesInvoiceID));
            alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            DataTable datSalesReservedQty = objclsConnection.ExecuteDataTable(strProcedureStockMaster, alParameters);
            if (datSalesReservedQty.Rows.Count > 0)
            {
                decInvoiceQty = datSalesReservedQty.Rows[0]["ReservedQty"].ToDecimal();
            }

            return decInvoiceQty;
        }

        public decimal GetItemRateForQuotation(long lngItemID, long lngBatchID, long lngQuotationID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 22));
            alParameters.Add(new SqlParameter("@ItemID", lngItemID));
            alParameters.Add(new SqlParameter("@BatchID", lngBatchID));
            alParameters.Add(new SqlParameter("@SalesQuotationID", lngQuotationID));
            decimal decSaleRate = objclsConnection.ExecuteScalar(strProcedureSalesQuotation, alParameters).ToDecimal();
            return decSaleRate;
        }

        public decimal GetItemRateForSalesOrder(long lngItemID, long lngBatchID, long lngSalesOrderID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 31));
            alParameters.Add(new SqlParameter("@ItemID", lngItemID));
            alParameters.Add(new SqlParameter("@BatchID", lngBatchID));
            alParameters.Add(new SqlParameter("@SalesOrderID", lngSalesOrderID));
            decimal decSaleRate = objclsConnection.ExecuteScalar(strProcedureSalesOrder, alParameters).ToDecimal();
            return decSaleRate;
        }

        public decimal GetItemRateForInvoice(long lngItemID, long lngBatchID, long lngSalesInvoiceID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 36));
            alParameters.Add(new SqlParameter("@ItemID", lngItemID));
            alParameters.Add(new SqlParameter("@BatchID", lngBatchID));
            alParameters.Add(new SqlParameter("@SalesInvoiceID", lngSalesInvoiceID));
            decimal decSaleRate = objclsConnection.ExecuteScalar(strProcedureSalesInvoice, alParameters).ToDecimal();
            return decSaleRate;
        }


        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (dat != null)
                dat.Dispose();

            if (prmSales != null)
                prmSales = null;
        }

        public bool SaveAlert(DataTable datAlert)// insert
        {
            ArrayList parameters;
            object iOutAletID = 0;

            try
            {
                parameters = new ArrayList();
                foreach (DataRow dtAlertRow in datAlert.Rows)
                {
                    if (Convert.ToInt64(dtAlertRow["AlertID"]) > 0)
                    {
                        parameters.Add(new SqlParameter("@Mode", 2));//update
                        parameters.Add(new SqlParameter("@AlertID", dtAlertRow["AlertID"]));
                    }
                    else
                    {
                        parameters.Add(new SqlParameter("@Mode", 1));//insert
                    }
                    foreach (DataColumn datCol in datAlert.Columns)
                    {
                        if (datCol.ColumnName.Equals("AlertID")) continue;
                        parameters.Add(new SqlParameter("@" + datCol.ColumnName, dtAlertRow[datCol.ColumnName]));
                    }

                    //parameters.Add(new SqlParameter("@AlertUserSettingID", dtAlertRow["AlertUserSettingID"]));
                    //parameters.Add(new SqlParameter("@ReferenceID", dtAlertRow["ReferenceID"]));
                    //if(dtAlertRow["ReferenceID1"]
                    //parameters.Add(new SqlParameter("@ReferenceID1", dtAlertRow["ReferenceID1"]));
                    //parameters.Add(new SqlParameter("@StartDate", dtAlertRow["StartDate"]));
                    //parameters.Add(new SqlParameter("@AlertMessage", dtAlertRow["AlertMessage"]));
                    //parameters.Add(new SqlParameter("@Status", dtAlertRow["Status"]));

                    SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
                    objParam.Direction = ParameterDirection.ReturnValue;
                    parameters.Add(objParam);

                    if (this.objclsConnection.ExecuteNonQuery(strProcedureAlert, parameters, out iOutAletID) != 0)
                    {
                        if (Convert.ToInt32(iOutAletID) > 0)
                        {
                            dtAlertRow["AlertID"] = iOutAletID;
                        }
                    }
                    else
                        return false;
                    parameters.Clear();
                }
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public string IsMandatoryDocumentsEntered(int intOperationTypeID, int intVendorID, int intCompanyID, int intOpertionTypeIDSales)
        {
            DataTable datMandatoryDocumentTypes = FillCombos(new string[] { "MDT.DocumentTypeID,DT.DocumentType", strTableMandatoryDocumentTypes + " MDT Inner Join " + strTableDocumentTypeReference + " DT On DT.DocumentTypeID = MDT.DocumentTypeID ", "MDT.OperationTypeID = " + intOpertionTypeIDSales + " And MDT.CompanyID = " + intCompanyID + "" });
            foreach (DataRow dr in datMandatoryDocumentTypes.Rows)
            {
                DataTable datDocuments = FillCombos(new string[] { "DocumentTypeID", strTableDocumentMaster, "DocumentTypeID = " + dr["DocumentTypeID"].ToInt32() + " And OperationTypeID = " + intOperationTypeID + " And ReferenceID = " + intVendorID + "" });
                if (datDocuments.Rows.Count == 0)
                    return dr["DocumentType"].ToString();
            }
            return "";
        }

        public string IsMandatoryDocumentExpired(int intOperationTypeID, int intVendorID, int intOpertionTypeIDSales, int intCompany)
        {
            string strRetValue = "";
            prmSales = new ArrayList();
            prmSales.Add(new SqlParameter("@Mode", 13));
            prmSales.Add(new SqlParameter("@OperationTypeID", intOperationTypeID));
            prmSales.Add(new SqlParameter("@VendorID", intVendorID));
            prmSales.Add(new SqlParameter("@OperationTypeID1", intOpertionTypeIDSales));
            prmSales.Add(new SqlParameter("@CompanyID", intCompany));
            prmSales.Add(new SqlParameter("@CurrentDate", ClsCommonSettings.GetServerDate()));
            DataTable datDocuments = objclsConnection.ExecuteDataTable(strProcedureDocumentMaster, prmSales);

            if (datDocuments.Rows.Count > 0)
            {
                strRetValue = Convert.ToString(datDocuments.Rows[0]["Description"]);

                return strRetValue;
            }
            return strRetValue;

        }

        public bool UpdationVerificationTableRemarks(int intType, Int64 intReferenceID, string strRemarks)
        {
            ArrayList alParameters = new ArrayList();
            if (intType == 1)
            {
                alParameters.Add(new SqlParameter("@Mode", 21));
                alParameters.Add(new SqlParameter("@OperationTypeID", (Int32)OperationType.SalesQuotation));
                alParameters.Add(new SqlParameter("@SalesQuotationID", intReferenceID));
                alParameters.Add(new SqlParameter("@CancelledBy", ClsCommonSettings.UserID));
                alParameters.Add(new SqlParameter("@Description", strRemarks));
                objclsConnection.ExecuteNonQuery(strProcedureSalesQuotation, alParameters);
            }
            else
            {
                alParameters.Add(new SqlParameter("@Mode", 29));
                alParameters.Add(new SqlParameter("@OperationTypeID", (Int32)OperationType.SalesOrder));
                alParameters.Add(new SqlParameter("@ReferenceID", intReferenceID));
                alParameters.Add(new SqlParameter("@CancelledBy", ClsCommonSettings.UserID));
                alParameters.Add(new SqlParameter("@Description", strRemarks));
                objclsConnection.ExecuteNonQuery(strProcedureSalesOrder, alParameters);
            }
            return true;
        }
        #endregion

        /// <summary>
        /// Gets Suppliers purchase history of an item 
        /// </summary>
        /// <param name="CompanyID"></param>
        /// <param name="SupplierID"></param>
        /// <param name="ItemID"></param>
        /// <returns>datatable</returns>
        public DataTable GetCustomerSaleHistory(int CompanyID, int CustomerID, int ItemID)
        {
            return objclsConnection.ExecuteDataTable("spInvSalesQuotation", new List<SqlParameter>{
                new SqlParameter("@Mode",30),
                new SqlParameter("@CompanyID",CompanyID),
                new SqlParameter("@VendorID",CustomerID),
                new SqlParameter("@ItemID",ItemID)
            });
        }
        public DataTable GetSaleRates(int ItemID, int CompanyID)
        {
            return objclsConnection.ExecuteDataTable("spInvSalesQuotation", new List<SqlParameter>{
                new SqlParameter("@Mode",31),
                new SqlParameter("@ItemID",ItemID),
                new SqlParameter("@CompanyID",CompanyID)
            });
        }
        public DataTable GetMinMaxSaleRate(int ItemID, int CompanyID)
        {
            return objclsConnection.ExecuteDataTable("spInvSalesQuotation", new List<SqlParameter>{
                new SqlParameter("@Mode",32),
                new SqlParameter("@ItemID",ItemID),
                new SqlParameter("@CompanyID",CompanyID)
            });
        }

        public DataTable GetDataForSalesOrder(int intCompanyID, long IngSalesOrderID)
        {
            prmSales = new ArrayList();

            prmSales.Add(new SqlParameter("@Mode", 37));
            prmSales.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmSales.Add(new SqlParameter("@SalesOrderID", IngSalesOrderID));
          return objclsConnection.ExecuteDataTable(strProcedureSalesInvoice, prmSales);
        }
        //public DataTable GetSalesOrderDetDetails(Int64 intSalesOrderID)
        //{   // Display Order detail
        //    ArrayList prmCommon = new ArrayList();
        //    prmCommon.Add(new SqlParameter("@Mode", 38));
        //    prmCommon.Add(new SqlParameter("@SalesOrderID", intSalesOrderID));
        //    return objclsConnection.ExecuteDataTable(strProcedureSalesInvoice, prmCommon);
        //}
        public DataSet FillGridAganistDelivertNote(string strCondition, int intCompanyID)
        {
            prmSales = new ArrayList();

            prmSales.Add(new SqlParameter("@Mode", 32));
            prmSales.Add(new SqlParameter("@SearchCondition", strCondition));
            prmSales.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objclsConnection.ExecuteDataSet(strProcedureSalesOrder, prmSales);
        }
        private bool DeleteSalesOrderReferenceDetails(Int64 intSalesOrderID)
        {   // Order Detail Delete
            ArrayList prmDeleteOrderDetail;

            prmDeleteOrderDetail = new ArrayList();
            prmDeleteOrderDetail.Add(new SqlParameter("@Mode", 35));
            prmDeleteOrderDetail.Add(new SqlParameter("@SalesOrderID", intSalesOrderID));

            objclsConnection.ExecuteNonQuery(strProcedureSalesOrder, prmDeleteOrderDetail);
            return true;
        }
        private bool SaveSalesOrderReferenceDetails(Int64 intSalesOrderID)
        {   // Order Detail Save
            int intICounter = 0;
            ArrayList prmSalesOrderDetails;

            foreach (clsDTOSTSalesOrderDetails objclsDTOSTSalesDetails in objclsDTOSTSalesMaster.IlstClsDTOSTSalesDetails)
            {
                prmSalesOrderDetails = new ArrayList();

                prmSalesOrderDetails.Add(new SqlParameter("@Mode", 34));
                prmSalesOrderDetails.Add(new SqlParameter("@SalesOrderID", intSalesOrderID)); 
                prmSalesOrderDetails.Add(new SqlParameter("@ReferenceID",objclsDTOSTSalesDetails.intSalesQuotationID));
                prmSalesOrderDetails.Add(new SqlParameter("@ItemID", objclsDTOSTSalesDetails.intItemID));
                //if (objclsDTOSTSalesDetails.intBatchID != 0)
                    prmSalesOrderDetails.Add(new SqlParameter("@BatchID", objclsDTOSTSalesDetails.intBatchID));
                    prmSalesOrderDetails.Add(new SqlParameter("@IsGroup", objclsDTOSTSalesDetails.blnIsGroup));
                prmSalesOrderDetails.Add(new SqlParameter("@Quantity", objclsDTOSTSalesDetails.decQuantity));
                objclsConnection.ExecuteNonQuery(strProcedureSalesOrder, prmSalesOrderDetails);
            }
            return true;  
        }
        private DataTable SaveSalesOrderDetFrmDelivertNote(string strCondition, Int64 intSalesOrderID) 
        {
            prmSales = new ArrayList();

            prmSales.Add(new SqlParameter("@Mode", 36));
            prmSales.Add(new SqlParameter("@SearchCondition", strCondition));   
            prmSales.Add(new SqlParameter("@SalesOrderID",intSalesOrderID));
            return objclsConnection.ExecuteDataTable(strProcedureSalesOrder, prmSales);
        }

       public DataTable GetItemIssueIDFromSalesOrder(Int64 intSalesOrderID)
        {
            prmSales = new ArrayList();

            prmSales.Add(new SqlParameter("@Mode", 37));
            prmSales.Add(new SqlParameter("@SalesOrderID", intSalesOrderID));
            return objclsConnection.ExecuteDataTable(strProcedureSalesOrder, prmSales);
        }

       public DataTable FillDeliveryNote(Int64 IntCompanyID, Int64 IntVendorID, Int64 OrderTypeID)
       {
           prmSales = new ArrayList();

           prmSales.Add(new SqlParameter("@Mode", 38));
           prmSales.Add(new SqlParameter("@CompanyID", IntCompanyID));
           prmSales.Add(new SqlParameter("@VendorID", IntVendorID));
           prmSales.Add(new SqlParameter("@OrderTypeID", OrderTypeID));
           return objclsConnection.ExecuteDataTable(strProcedureSalesOrder, prmSales);
       }
       public DataTable FillDeliveryNoteDetails (Int64 IntCompanyID,Int64 intSalesOrderID, Int64 IntVendorID, Int64 OrderTypeID)
       {
           prmSales = new ArrayList();

           prmSales.Add(new SqlParameter("@Mode", 40));
           prmSales.Add(new SqlParameter("@CompanyID", IntCompanyID));
           prmSales.Add(new SqlParameter("@SalesOrderID", intSalesOrderID));
           prmSales.Add(new SqlParameter("@VendorID", IntVendorID));
           prmSales.Add(new SqlParameter("@OrderTypeID", OrderTypeID));
           return objclsConnection.ExecuteDataTable(strProcedureSalesOrder, prmSales);
       }

       public bool CheckSalesOrderExists(Int64 intSalesOrderID)
       {

           prmSales = new ArrayList();
           prmSales.Add(new SqlParameter("@Mode", 41));
           prmSales.Add(new SqlParameter("@SalesOrderID", intSalesOrderID));
           int value = Convert.ToInt32(objclsConnection.ExecuteScalar(strProcedureSalesOrder, prmSales));
           if (value > 0)
               return true;
           else
               return false;
       }
       public bool IsSalesInvoiceExists(long lngItemIssueID)
       {
           ArrayList prmSalesInvoice;
           prmSalesInvoice = new ArrayList();

           prmSalesInvoice.Add(new SqlParameter("@Mode", 75));
           prmSalesInvoice.Add(new SqlParameter("@ItemIssueID", lngItemIssueID));
           int value = Convert.ToInt32(objclsConnection.ExecuteScalar(strProcedureSalesInvoice, prmSalesInvoice));
           if (value > 0)
               return true;
           else
               return false;
       }
       

       public decimal GetSalesRateFormDeliveryNote(string strCondition, Int32 IntCompanyID, int ItemID, int BatchID)
        {
            decimal decSaleRate = 0;
            prmSales = new ArrayList();
            prmSales.Add(new SqlParameter("@Mode", 43));
            prmSales.Add(new SqlParameter("@SearchCondition", strCondition));
            prmSales.Add(new SqlParameter("@CompanyID", IntCompanyID));
            prmSales.Add(new SqlParameter("@ItemID", ItemID));
            prmSales.Add(new SqlParameter("@BatchID", BatchID));
            DataTable dtSales =  objclsConnection.ExecuteDataTable(strProcedureSalesOrder, prmSales);
            if (dtSales != null && dtSales.Rows.Count > 0)
                decSaleRate = dtSales.Rows[0]["Rate"].ToDecimal();
            return decSaleRate;
        }

       public bool CheckWheatherPurchaseInvoiceExists(int IntItemID, int lntBatchID)
       {

           prmSales = new ArrayList();

           prmSales.Add(new SqlParameter("@Mode", 76));
           prmSales.Add(new SqlParameter("@ItemID", IntItemID));
           prmSales.Add(new SqlParameter("@BatchID", lntBatchID));
           int value = Convert.ToInt32(objclsConnection.ExecuteScalar(strProcedureSalesInvoice, prmSales));
           if (value > 0)
               return true;
           else
               return false;
       }
       public bool CheckWhetherSalesInvoiceExists(string strCondition)
       {

           prmSales = new ArrayList();

           prmSales.Add(new SqlParameter("@Mode",44));
           prmSales.Add(new SqlParameter("@SearchCondition", strCondition));
           int value = Convert.ToInt32(objclsConnection.ExecuteScalar(strProcedureSalesOrder, prmSales));
           if (value > 0)
               return true;
           else
               return false;
       }

       public bool CheckWhetherSOExists(string strCondition)
       {

           prmSales = new ArrayList();

           prmSales.Add(new SqlParameter("@Mode", 45));
           prmSales.Add(new SqlParameter("@SearchCondition", strCondition));
           int value = Convert.ToInt32(objclsConnection.ExecuteScalar(strProcedureSalesOrder, prmSales));
           if (value > 0)
               return true;
           else
               return false;
       }
       public bool CheckWhetherSOExistsAganistDeliveryNote(Int64 intSalesOrderID)
       {

           prmSales = new ArrayList();
           prmSales.Add(new SqlParameter("@Mode", 46));
           prmSales.Add(new SqlParameter("@SalesOrderID", intSalesOrderID));
           int value = Convert.ToInt32(objclsConnection.ExecuteScalar(strProcedureSalesOrder, prmSales));
           if (value > 0)
               return true;
           else
               return false;
       }
       public decimal getTaxValue(int TaxScheme)
       {
           return this.objclsConnection.ExecuteScalar("Select AccValue From AccAccountMaster where AccountID='" + TaxScheme + "'").ToDecimal();
       }
    }
}