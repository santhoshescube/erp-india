﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
namespace MyBooksERP
{
    public class clsDALPackingList
    {
        public clsDTOPackingList objclsDTOPackingList { get; set; }
        public DataLayer objclsConnection { get; set; }
        ArrayList prmCI;
        public clsDALPackingList(DataLayer objDataLayer)
        {
            objclsConnection = objDataLayer;
        }
        public bool InsertMaster(bool IsInsert)
        {
            prmCI = new ArrayList();

            if (IsInsert)
                prmCI.Add(new SqlParameter("@Mode", 1));
            else
                prmCI.Add(new SqlParameter("@Mode", 3));

            prmCI.Add(new SqlParameter("@InvoiceID", objclsDTOPackingList.lngInvoiceID));
            prmCI.Add(new SqlParameter("@InvoiceDate", objclsDTOPackingList.dtInvoiceDate));
            prmCI.Add(new SqlParameter("@HSCode", objclsDTOPackingList.strHSCode));
            prmCI.Add(new SqlParameter("@InvoiceNo", objclsDTOPackingList.strInvoiceNo));
            prmCI.Add(new SqlParameter("@GrossWeight", objclsDTOPackingList.strGrossWeight));
            prmCI.Add(new SqlParameter("@NetWeight", objclsDTOPackingList.strNetWeight));
            prmCI.Add(new SqlParameter("@AccountOf1", objclsDTOPackingList.strAccountOf1));
            prmCI.Add(new SqlParameter("@AccountOf2", objclsDTOPackingList.strAccountOf2));
            prmCI.Add(new SqlParameter("@AccountOf3", objclsDTOPackingList.strAccountOf3));
            prmCI.Add(new SqlParameter("@DrawnUnder1", objclsDTOPackingList.strDrawnUnder1));
            prmCI.Add(new SqlParameter("@DrawnUnder2", objclsDTOPackingList.strDrawnUnder2));
            prmCI.Add(new SqlParameter("@DrawnUnder3", objclsDTOPackingList.strDrawnUnder3));
            prmCI.Add(new SqlParameter("@Description", objclsDTOPackingList.strDescription));
            prmCI.Add(new SqlParameter("@ShippingMarks", objclsDTOPackingList.strShippingMarks));
            prmCI.Add(new SqlParameter("@TradeMark", objclsDTOPackingList.strTradeMark));
            prmCI.Add(new SqlParameter("@ManufactureName", objclsDTOPackingList.strManufactureName));
            prmCI.Add(new SqlParameter("@Nationality", objclsDTOPackingList.strNationality));
            prmCI.Add(new SqlParameter("@Address", objclsDTOPackingList.strAddress));
            prmCI.Add(new SqlParameter("@Telephone", objclsDTOPackingList.strTelephone));
            prmCI.Add(new SqlParameter("@UOM", objclsDTOPackingList.strUOM));
            prmCI.Add(new SqlParameter("@TotalUOM", objclsDTOPackingList.decTotalUOM));
            prmCI.Add(new SqlParameter("@FOBValue", objclsDTOPackingList.decFOBValue));
            prmCI.Add(new SqlParameter("@FreightCharge", objclsDTOPackingList.decFreightCharge));
            prmCI.Add(new SqlParameter("@CFR", objclsDTOPackingList.strCFR));
            prmCI.Add(new SqlParameter("@TotalCFR", objclsDTOPackingList.decTotalCFR));
            prmCI.Add(new SqlParameter("@Declaration", objclsDTOPackingList.strDeclaration));
            prmCI.Add(new SqlParameter("@GrossTotal", objclsDTOPackingList.decGrossTotal));
            prmCI.Add(new SqlParameter("@LCPercentage", objclsDTOPackingList.decLCPercentage));
            prmCI.Add(new SqlParameter("@NetTotal", objclsDTOPackingList.decNetTotal));
            if (objclsConnection.ExecuteNonQueryWithTran("spInvCommercialInvoice", prmCI) != 0)
            {
                if (objclsDTOPackingList.lstclsDTOPackingListDetails.Count > 0)
                {
                    DeleteDetails();
                    InsertDetails();
                }

                return true;
            }
            return false;
        }

        public void DeleteDetails()
        {
            prmCI = new ArrayList();
            prmCI.Add(new SqlParameter("@Mode", 6));
            prmCI.Add(new SqlParameter("@InvoiceID", objclsDTOPackingList.lngInvoiceID));
            objclsConnection.ExecuteNonQueryWithTran("spInvCommercialInvoice", prmCI);
        }
        public void InsertDetails()
        {
            ArrayList prmCTDetails;

            foreach (clsDTOPackingListDetails objclsDTOPackingListDetails in objclsDTOPackingList.lstclsDTOPackingListDetails)
            {
                prmCTDetails = new ArrayList();
                prmCTDetails.Add(new SqlParameter("@Mode", 2));
                prmCTDetails.Add(new SqlParameter("@InvoiceID", objclsDTOPackingList.lngInvoiceID));
                prmCTDetails.Add(new SqlParameter("@NoOfRolls", objclsDTOPackingListDetails.strNoofRolls));
                prmCTDetails.Add(new SqlParameter("@Roll", objclsDTOPackingListDetails.decRoll));
                prmCTDetails.Add(new SqlParameter("@Item", objclsDTOPackingListDetails.strItem));
                prmCTDetails.Add(new SqlParameter("@Quantity", objclsDTOPackingListDetails.decQuantity));
                prmCTDetails.Add(new SqlParameter("@UOM", objclsDTOPackingListDetails.strUOM));
                prmCTDetails.Add(new SqlParameter("@UnitPrice", objclsDTOPackingListDetails.decUnitPrice));
                prmCTDetails.Add(new SqlParameter("@EachCartoon", objclsDTOPackingListDetails.strEachCartoon));
                prmCTDetails.Add(new SqlParameter("@Total", objclsDTOPackingListDetails.decTotal));
                objclsConnection.ExecuteNonQueryWithTran("spInvCommercialInvoice", prmCTDetails);

            }


        }
        public bool DeleteCI()
        {
            DeleteDetails();
            prmCI = new ArrayList();
            prmCI.Add(new SqlParameter("@Mode", 5));
            prmCI.Add(new SqlParameter("@InvoiceID", objclsDTOPackingList.lngInvoiceID));            
            int intTemp = objclsConnection.ExecuteNonQueryWithTran("spInvCommercialInvoice", prmCI);

            if (intTemp > 0)
                return true;
            else
                return false;
        }

        public DataSet GetDetails()
        {
            prmCI = new ArrayList();
            prmCI.Add(new SqlParameter("@Mode", 7));
            prmCI.Add(new SqlParameter("@InvoiceID", objclsDTOPackingList.lngInvoiceID));
            return objclsConnection.ExecuteDataSet("spInvCommercialInvoice", prmCI);

        }
    }
}
