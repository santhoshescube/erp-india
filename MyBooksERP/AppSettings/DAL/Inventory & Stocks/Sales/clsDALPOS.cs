﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;

using System.Data.SqlClient;
/* 
=================================================
   Author:		< Sanju >
   Create date: < 21 Nov 2011 >
   Description:	< POS DAL>
================================================
*/
namespace MyBooksERP
{
    public class clsDALPOS
    {
        ArrayList prmPOS;
        DataTable dat;
        public clsDTOPOS objclsDTOPOS { get; set; }
        public DataLayer objclsConnection { get; set; }
        ClsCommonUtility MobjClsCommonUtility; // object of clsCommonUtility
        public clsDALPaymentMaster objDALPaymentMaster { get; set; } // obj of clsDALPaymentMaster
        public clsDTOPaymentMaster objDTOPaymentMaster { get; set; }
        public clsDALLogin objclsDALLogin { get; set; }

        public clsDALPOS(DataLayer objDataLayer)
        {
            MobjClsCommonUtility = new ClsCommonUtility();
            objclsConnection = objDataLayer;
            MobjClsCommonUtility.PobjDataLayer = objclsConnection;
        }

        public clsDALPOS()
        {
            MobjClsCommonUtility = new ClsCommonUtility();
            objclsConnection = new DataLayer();
            MobjClsCommonUtility.PobjDataLayer = objclsConnection;
        }

        public DataTable GetDataForItemSelection(int intCompanyID, int intCurrencyID, int intWarehouseID)
        {
            prmPOS = new ArrayList();
            //if (intFormType == 1)
            //    prmPOS.Add(new SqlParameter("@Mode", 29));
            //else
            prmPOS.Add(new SqlParameter("@Mode", 25));
            prmPOS.Add(new SqlParameter("@dtePOSDate", ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")));
            prmPOS.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPOS.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            prmPOS.Add(new SqlParameter("@intCurrencyID", intCurrencyID));
            DataSet ds = objclsConnection.ExecuteDataSet("spInvPOS", prmPOS);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables.Count > 1)
                {
                    DataTable dt = ds.Tables[0].Copy();

                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        DataRow dr1 = dt.NewRow();
                        dr1["ItemCode"] = dr["ItemGroupCode"];
                        dr1["Description"] = dr["Description"];
                        dr1["ItemID"] = dr["ItemGroupID"];
                        dr1["Rate"] = dr["Rate"];
                        //if (intFormType != 1)
                        dr1["QtyAvailable"] = dr["AvaliableQty"];
                        dr1["UOM"] = dr["UOM"];
                        dr1["IsGroup"] = true;
                        dr1["IsGroupItem"] = "Yes";
                        dt.Rows.Add(dr1);
                    }
                    return dt;
                }
                else
                    return ds.Tables[0];
            }
            return null;
        }
        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 4));
            prmCommon.Add(new SqlParameter("@RoleID", intRoleID));
            prmCommon.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmCommon.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable("STPermissionSettings", prmCommon);
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }
        public DataTable GetItemWiseUOMs(int intItemID)
        {   //To Get ItemWiseUOMs
            ArrayList prmSalesParameters = new ArrayList();

            prmSalesParameters.Add(new SqlParameter("@Mode", 16));
            prmSalesParameters.Add(new SqlParameter("@ItemID", intItemID)); // Selects ItemID,ItemName,Type,ConvertionFactor,ConvertionValue
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmSalesParameters);
        }
        public DataTable GetItemAndCustomerWiseDiscounts(int intItemID, int intVendorID, decimal decQty, decimal decRate, string strCurrentDate, int intCurrencyID)
        {   //To Get ItemAndCustomerWiseDiscounts
            ArrayList prmSalesParameters = new ArrayList();

            if (!string.IsNullOrEmpty(strCurrentDate))
                prmSalesParameters.Add(new SqlParameter("@Mode", 17));
            else
                prmSalesParameters.Add(new SqlParameter("@Mode", 22));

            prmSalesParameters.Add(new SqlParameter("@ItemID", intItemID)); // Selects DiscountID,DiscountName,Type 
            prmSalesParameters.Add(new SqlParameter("@VendorID", intVendorID));
            prmSalesParameters.Add(new SqlParameter("@Quantity", decQty));
            prmSalesParameters.Add(new SqlParameter("@Rate", decRate));
            prmSalesParameters.Add(new SqlParameter("@CurrentDate", strCurrentDate));
            prmSalesParameters.Add(new SqlParameter("@CurrencyID", intCurrencyID));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmSalesParameters);
        }
        public DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {
            ArrayList prmUomDetails = new ArrayList();
            prmUomDetails.Add(new SqlParameter("@Mode", 11));
            prmUomDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmUomDetails.Add(new SqlParameter("@UnitID", intUOMID));
            //  prmUomDetails.Add(new SqlParameter("@UomTypeID", 1));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmUomDetails);
        }
        public void GetItemDiscountAmount(int intMode, int intItemID, int intDiscountID, int intVendorID)
        {
            ArrayList prmItemDiscount = new ArrayList();
            prmItemDiscount.Add(new SqlParameter("@Mode", intMode));
            prmItemDiscount.Add(new SqlParameter("@ItemID", intItemID));
            prmItemDiscount.Add(new SqlParameter("@DiscountID", intDiscountID));
            prmItemDiscount.Add(new SqlParameter("@VendorID", intVendorID));

            DataTable datDiscountAmount = objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmItemDiscount);

            if (datDiscountAmount.Rows.Count > 0)
            {
                objclsDTOPOS.blnDiscountForAmount = Convert.ToBoolean(datDiscountAmount.Rows[0]["IsDiscountForAmount"]);
                objclsDTOPOS.blnPercentCalculation = Convert.ToBoolean(datDiscountAmount.Rows[0]["PercentOrAmount"]);
                objclsDTOPOS.decDiscountValue = Convert.ToDecimal(datDiscountAmount.Rows[0]["PercOrAmtValue"]);
            }
        }
        public double GetRate(int intUOMID, int intItemID, int intCompanyID, int intBatchID)
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 3));
            prmPOS.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPOS.Add(new SqlParameter("@ItemID", intItemID));
            prmPOS.Add(new SqlParameter("@UOMID", intUOMID));
            prmPOS.Add(new SqlParameter("@BatchID", intBatchID));
            return objclsConnection.ExecuteScalar("spInvPOS", prmPOS).ToDouble();
        }
        public Int32 GetPOSNo(int intCompanyID)
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 4));
            prmPOS.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objclsConnection.ExecuteScalar("spInvPOS", prmPOS).ToInt32();
        }
        public Int32 GetPOSID()
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 2));
            return objclsConnection.ExecuteScalar("spInvPOS", prmPOS).ToInt32();
        }
        public Int32 GetCurrencyID(int intCompanyID)
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 5));
            prmPOS.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objclsConnection.ExecuteScalar("spInvPOS", prmPOS).ToInt32();
        }
        public Int32 insertPOSMaster()
        {
            ArrayList prmPOS = new ArrayList();
            if (objclsDTOPOS.blnAddMode)
            {
                prmPOS.Add(new SqlParameter("@Mode", 6));
            }
            else
            {
                prmPOS.Add(new SqlParameter("@Mode", 10));
                prmPOS.Add(new SqlParameter("@intPOSID", objclsDTOPOS.intPOSID));
            }
            prmPOS.Add(new SqlParameter("@intSLNO", objclsDTOPOS.intSLNO));
            prmPOS.Add(new SqlParameter("@strPOSNo", objclsDTOPOS.strPOSNo));
            prmPOS.Add(new SqlParameter("@dtePOSDate", objclsDTOPOS.dtePOSDate));
            prmPOS.Add(new SqlParameter("@intVendorID", objclsDTOPOS.intVendorID));
            prmPOS.Add(new SqlParameter("@dteDueDate", objclsDTOPOS.dteDueDate));
            if (objclsDTOPOS.intVendorAddID > 0)
            {
                prmPOS.Add(new SqlParameter("@intVendorAddID", objclsDTOPOS.intVendorAddID));
            }
            prmPOS.Add(new SqlParameter("@intPaymentTermID", objclsDTOPOS.intPaymentTermID));
            prmPOS.Add(new SqlParameter("@strRemarks", objclsDTOPOS.strRemarks));
            prmPOS.Add(new SqlParameter("@intGrandDiscountID", objclsDTOPOS.intGrandDiscountID));
            prmPOS.Add(new SqlParameter("@intCurrencyID", objclsDTOPOS.intCurrencyID));
            prmPOS.Add(new SqlParameter("@decExpenseAmount", objclsDTOPOS.decExpenseAmount));
            prmPOS.Add(new SqlParameter("@decGrandDiscountAmount", objclsDTOPOS.decGrandDiscountAmount));
            prmPOS.Add(new SqlParameter("@decGrandAmount", objclsDTOPOS.decGrandAmount));
            prmPOS.Add(new SqlParameter("@decNetAmount", objclsDTOPOS.decNetAmount));
            prmPOS.Add(new SqlParameter("@decNetRounded", objclsDTOPOS.decNetRounded));
            prmPOS.Add(new SqlParameter("@intCreatedBy", objclsDTOPOS.intCreatedBy));
            prmPOS.Add(new SqlParameter("@dteCreatedDate", objclsDTOPOS.dteCreatedDate));
            prmPOS.Add(new SqlParameter("@intCompanyID", objclsDTOPOS.intCompanyID));
            prmPOS.Add(new SqlParameter("@intStatusID", objclsDTOPOS.intStatusID));
            prmPOS.Add(new SqlParameter("@intSalesCounterID", objclsDTOPOS.intSalesCounterID));
            prmPOS.Add(new SqlParameter("@decTendered", objclsDTOPOS.decTendered));
            prmPOS.Add(new SqlParameter("@decChange", objclsDTOPOS.decChange));
            prmPOS.Add(new SqlParameter("@DebitHeadID", objclsDTOPOS.intDebitHeadID));
            prmPOS.Add(new SqlParameter("@OperationModID", objclsDTOPOS.intOperationModID));
            prmPOS.Add(new SqlParameter("@WarehouseID", objclsDTOPOS.intWareHouseID));
            prmPOS.Add(new SqlParameter("@OperationTypeID", (int)OperationType.POS));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPOS.Add(objParam);
            object objPOSID = 0;

            objclsConnection.ExecuteNonQuery("spInvPOS", prmPOS, out objPOSID);
            SavePayments((Int32)objPOSID);
            return (Int32)objPOSID;
        }

        public bool SavePayments(int intPosID)
        {
            //Saving Payments
            int intTempPaymentID = 0;
            long lngSerialNo = 0;
            DataTable datTempPay = FillCombos(new string[] { "*", "AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", "RPD.ReferenceID=" + intPosID + " AND RPM.OperationTypeID=" + (int)OperationType.POS });
            if (datTempPay != null)
            {
                if (datTempPay.Rows.Count > 0)
                {
                    intTempPaymentID = Convert.ToInt32(datTempPay.Rows[0]["ReceiptAndPaymentID"].ToString());
                    lngSerialNo = datTempPay.Rows[0]["SerialNo"].ToInt64();
                }
            }
            //DataTable datTemp = FillCombos(new string[] { "*", "CompanySettings", "CompanyID=" + objclsDTOPOS.intCompanyID });
            //string ReceiptsPrefix = GetCompanySettingsValue("Receipts", "Prefix", datTemp);

            ArrayList prmPayments = new ArrayList();

            prmPayments.Add(new SqlParameter("@Mode", Convert.ToInt32(intTempPaymentID) == 0 ? 20 : 21));
            prmPayments.Add(new SqlParameter("@ReceiptAndPaymentID", Convert.ToInt32(intTempPaymentID)));
            prmPayments.Add(new SqlParameter("@PaymentDate", objclsDTOPOS.dtePOSDate.ToString("dd-MMM-yyyy")));
            prmPayments.Add(new SqlParameter("@OperationTypeID", (int)OperationType.POS));
            prmPayments.Add(new SqlParameter("@PaymentTypeID", (int)PaymentTypes.InvoicePayment));
            prmPayments.Add(new SqlParameter("@PaymentModeID", (int)PaymentModes.Cash));
            prmPayments.Add(new SqlParameter("@ReferenceID", intPosID));
            //prmPayments.Add(new SqlParameter("@CreditHeadID1", 0));
            //prmPayments.Add(new SqlParameter("@DebitHeadID", (int)Accounts.CashInHand));
            prmPayments.Add(new SqlParameter("@Amount", objclsDTOPOS.decNetRounded));
            //prmPayments.Add(new SqlParameter("@VoucherNo", ""));
            //prmPayments.Add(new SqlParameter("VoucherIds", ""));
            prmPayments.Add(new SqlParameter("@PostToAccount", 1));
            prmPayments.Add(new SqlParameter("@CurrencyID", objclsDTOPOS.intCurrencyID));
            prmPayments.Add(new SqlParameter("@CompanyID", objclsDTOPOS.intCompanyID));
            prmPayments.Add(new SqlParameter("@CreatedBy", objclsDTOPOS.intCreatedBy));
            prmPayments.Add(new SqlParameter("@VendorID", objclsDTOPOS.intVendorID));
            prmPayments.Add(new SqlParameter("@CreatedDate", objclsDTOPOS.dteCreatedDate.ToString("dd-MMM-yyyy")));
            prmPayments.Add(new SqlParameter("@Description", objclsDTOPOS.strRemarks));
            prmPayments.Add(new SqlParameter("@StatusID", objclsDTOPOS.intStatusID));
            prmPayments.Add(new SqlParameter("@OperationModID", (Int32)TransactionTypes.CashSale));
            prmPayments.Add(new SqlParameter("@IsReceipt", 1));
            prmPayments.Add(new SqlParameter("@FormType", "POS"));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPayments.Add(objParam);

            //long lngSerialNo = GetMaxSerialNo(objclsDTOPOS.intCompanyID);
            //string strPaymentNumber = ReceiptsPrefix + lngSerialNo.ToString();
            if (intTempPaymentID == 0)
                lngSerialNo = GetMaxSerialNo(objclsDTOPOS.intCompanyID);

            string strPaymentNumber = ClsCommonSettings.ReceiptsPrefix + lngSerialNo.ToString();

            prmPayments.Add(new SqlParameter("@PaymentNumber", strPaymentNumber));
            prmPayments.Add(new SqlParameter("@SerialNo", lngSerialNo));

            object objOutPaymentID = 0;
            if (objclsConnection.ExecuteNonQueryWithTran("spInvPOS", prmPayments, out objOutPaymentID) != 0)
            {
                if (Convert.ToInt64(objOutPaymentID) > 0)
                {
                    //objDTOPaymentMaster.intPaymentID = Convert.ToInt64(objOutPaymentID);
                    return true;
                }
            }
            return false;
        }

        public bool DeletePayments(int intPosID)
        {
            //Delete Payments
            int intTempPaymentID = 0;
            DataTable datTempPay = FillCombos(new string[] { "*", "AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID", "RPD.ReferenceID=" + intPosID + " AND RPM.OperationTypeID=" + (int)OperationType.POS });
            if (datTempPay != null)
            {
                if (datTempPay.Rows.Count > 0)
                    intTempPaymentID = Convert.ToInt32(datTempPay.Rows[0]["ReceiptAndPaymentID"].ToString());
            }
            ArrayList prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", 22));
            prmPayments.Add(new SqlParameter("@ReceiptAndPaymentID", intTempPaymentID));

            if (objclsConnection.ExecuteNonQueryWithTran("spInvPOS", prmPayments) != 0)
            {
                return true;
            }
            return false;
        }

        public Int64 GetMaxSerialNo(int intCompanyID)
        {
            // function for getting max of paymentsID
            ArrayList prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", 2));
            prmPayments.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPayments.Add(new SqlParameter("@IsReceipt", 1));
            DataTable datTemp = objclsConnection.ExecuteDataTable("spAccReceiptsAndPayments", prmPayments);
            Int64 intRetValue = 0;
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    try { intRetValue = Convert.ToInt32(datTemp.Rows[0][0].ToString()); }
                    catch { }
                }
            }
            return intRetValue + 1;
        }

        public string GetCompanySettingsValue(string strConfigurationItem, string strSubItem, DataTable dtCompanySettings)
        {
            dtCompanySettings.DefaultView.RowFilter = "ConfigurationItem = '" + strConfigurationItem + "' And SubItem = '" + strSubItem + "'";
            if (dtCompanySettings.DefaultView.ToTable().Rows.Count > 0)
                return dtCompanySettings.DefaultView.ToTable().Rows[0]["ConfigurationValue"].ToString();
            return "";
        }

        //public void insertPOSdetails()
        //{
        //foreach (clsDTOPOSDetails objclsDTOPOSDetails in objclsDTOPOS.lstclsDTOPOSDetails)
        //{
        //    ArrayList prmPOS = new ArrayList();
        //    prmPOS.Add(new SqlParameter("@Mode", 7));
        //    prmPOS.Add(new SqlParameter("@intPOSID",objclsDTOPOS.intPOSID));
        //    prmPOS.Add(new SqlParameter("@intCreatedBy", objclsDTOPOS.intCreatedBy));
        //    prmPOS.Add(new SqlParameter("@intItemID",objclsDTOPOSDetails.intItemID ));
        //    prmPOS.Add(new SqlParameter("@blnIsGroup",objclsDTOPOSDetails.blnIsGroup ));
        //    prmPOS.Add(new SqlParameter("@decQuantity",objclsDTOPOSDetails.decQuantity ));
        //    prmPOS.Add(new SqlParameter("@intUOMID",objclsDTOPOSDetails.intUOMID ));
        //    prmPOS.Add(new SqlParameter("@intBatchID",objclsDTOPOSDetails.intBatchID ));
        //    prmPOS.Add(new SqlParameter("@decRate",objclsDTOPOSDetails.decRate ));
        //    prmPOS.Add(new SqlParameter("@intDiscountID",objclsDTOPOSDetails.intDiscountID ));
        //    prmPOS.Add(new SqlParameter("@decDiscountAmount",objclsDTOPOSDetails.decDiscountAmount ));
        //    prmPOS.Add(new SqlParameter("@decGrandAmount",objclsDTOPOSDetails.decGrandAmount ));
        //    prmPOS.Add(new SqlParameter("@decNetAmount",objclsDTOPOSDetails.decNetAmount));
        //    prmPOS.Add(new SqlParameter("@blnDeliveryRequired",objclsDTOPOSDetails.blnDeliveryRequired ));
        //    prmPOS.Add(new SqlParameter("@intStatusID",objclsDTOPOSDetails.intStatusID ));
        //    prmPOS.Add(new SqlParameter("@WarehouseID", objclsDTOPOS.intWareHouseID));
        //    prmPOS.Add(new SqlParameter("@intCompanyID", objclsDTOPOS.intCompanyID));
        //    prmPOS.Add(new SqlParameter("@dtePOSDate", objclsDTOPOS.dtePOSDate));

        //    objclsConnection.ExecuteNonQuery("spInvPOS", prmPOS);
        //}
        //}
        public void insertPOSdetails()
        {//using bulk insertion

            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 16));
            prmPOS.Add(new SqlParameter("@intPOSID", objclsDTOPOS.intPOSID));
            prmPOS.Add(new SqlParameter("@intCreatedBy", objclsDTOPOS.intCreatedBy));
            prmPOS.Add(new SqlParameter("@WarehouseID", objclsDTOPOS.intWareHouseID));
            prmPOS.Add(new SqlParameter("@intCompanyID", objclsDTOPOS.intCompanyID));
            prmPOS.Add(new SqlParameter("@dtePOSDate", objclsDTOPOS.dtePOSDate));
            prmPOS.Add(new SqlParameter("@OperationTypeID", (int)OperationType.POS));

            prmPOS.Add(new SqlParameter("@XmlPOSDetails", objclsDTOPOS.lstclsDTOPOSDetails.ToXml()));
            if (objclsDTOPOS.lstclsDTOPOSItemGroupDetails != null && objclsDTOPOS.lstclsDTOPOSItemGroupDetails.Count != 0)
            prmPOS.Add(new SqlParameter("@XmlPOSGroupDetails", objclsDTOPOS.lstclsDTOPOSItemGroupDetails.ToXml()));
            objclsConnection.ExecuteNonQuery("spInvPOS", prmPOS);


            //foreach (clsDTOPOSDetails objclsDTOPOSDetails in objclsDTOPOS.lstclsDTOPOSDetails)
            //{
            //    int intSerialNo = 0;
            //    if (objclsDTOPOSDetails.blnIsGroup)
            //    {
            //        objclsDTOPOSDetails.intSerialNo= intSerialNo;
            //        SavePOSItemGroupIssueDetails(objclsDTOPOSDetails);
            //    }
            //}
            //DataTable dat=objclsConnection.ExecuteDataTable("spInvPOS", prmPOS);            
        }
        public DataTable GetPOS()
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 29));
            prmPOS.Add(new SqlParameter("@intPOSID", objclsDTOPOS.intPOSID));
            return objclsConnection.ExecuteDataTable("spInvPOS", prmPOS);
        }
        public DataTable SearchPOSNos(string strFilterCondition)
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 9));
            prmPOS.Add(new SqlParameter("@SearchCondition", strFilterCondition));
            //prmPOS.Add(new SqlParameter("@intCompanyID", objclsDTOPOS.intCompanyID));
            //prmPOS.Add(new SqlParameter("@intSalesCounterID", objclsDTOPOS.intSalesCounterID));
            //prmPOS.Add(new SqlParameter("@intCreatedBy", objclsDTOPOS.intCreatedBy));
            //prmPOS.Add(new SqlParameter("@intVendorID", objclsDTOPOS.intVendorID));
            //prmPOS.Add(new SqlParameter("@intStatusID", objclsDTOPOS.intSearchStatusID));
            //prmPOS.Add(new SqlParameter("@strPOSNo", objclsDTOPOS.strPOSNo));
            //prmPOS.Add(new SqlParameter("@POSStartDate", objclsDTOPOS.dtePOSFromDate));
            //prmPOS.Add(new SqlParameter("@POSEndDDate", objclsDTOPOS.dtePOSToDate));
            return objclsConnection.ExecuteDataTable("spInvPOS", prmPOS);
        }
        public void DeletePOSDetails()
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 11));
            prmPOS.Add(new SqlParameter("@intPOSID", objclsDTOPOS.intPOSID));
            prmPOS.Add(new SqlParameter("@intCreatedBy", objclsDTOPOS.intCreatedBy));
            prmPOS.Add(new SqlParameter("@OperationModID", objclsDTOPOS.intOperationModID));
            prmPOS.Add(new SqlParameter("@OperationTypeID", (int)OperationType.POS));
            objclsConnection.ExecuteNonQuery("spInvPOS", prmPOS);
        }
        public void DeletePOSMaster()
        {
            DataTable datTempMaster = FillCombos(new string[] { "*", "InvPOSMaster", "POSID=" + objclsDTOPOS.intPOSID });
            if (Convert.ToInt32(datTempMaster.Rows[0]["StatusID"].ToString()) != (int)OperationStatusType.SPOSCancelled)
                DeleteItemSummary(objclsDTOPOS.intPOSID);

            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 12));
            prmPOS.Add(new SqlParameter("@intPOSID", objclsDTOPOS.intPOSID));
            prmPOS.Add(new SqlParameter("@OperationTypeID", (int)OperationType.POS));

            objclsConnection.ExecuteNonQuery("spInvPOS", prmPOS);
            DeletePayments(objclsDTOPOS.intPOSID);
        }
        public void ChangePOSStatus()
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 13));
            prmPOS.Add(new SqlParameter("@intPOSID", objclsDTOPOS.intPOSID));
            prmPOS.Add(new SqlParameter("@intStatusID", objclsDTOPOS.intStatusID));
            prmPOS.Add(new SqlParameter("@intCreatedBy", objclsDTOPOS.intCreatedBy));
            prmPOS.Add(new SqlParameter("@WarehouseID", objclsDTOPOS.intWareHouseID));
            prmPOS.Add(new SqlParameter("@intCompanyID", objclsDTOPOS.intCompanyID));
            prmPOS.Add(new SqlParameter("@dtePOSDate", objclsDTOPOS.dtePOSDate));
            prmPOS.Add(new SqlParameter("@OperationModID", objclsDTOPOS.intOperationModID));
            prmPOS.Add(new SqlParameter("@strRemarks", objclsDTOPOS.strRemarks));
            prmPOS.Add(new SqlParameter("@OperationTypeID", (int)OperationType.POS));

            objclsConnection.ExecuteNonQuery("spInvPOS", prmPOS);

            DataTable datTempMaster = FillCombos(new string[] { "*", "InvPOSMaster", "POSID=" + objclsDTOPOS.intPOSID });
            if (Convert.ToInt32(datTempMaster.Rows[0]["StatusID"].ToString()) == (int)OperationStatusType.SPOSCancelled)
            {
                DeletePayments(objclsDTOPOS.intPOSID);
                DeleteItemSummarywhenCancelled(objclsDTOPOS.intPOSID);
            }
            else
            {
                SavePayments(objclsDTOPOS.intPOSID);
                SaveItemSummary(objclsDTOPOS.intPOSID);
            }
        }
        public DataTable GetQuantityAvailable(int intItemID, int intWareHouseID, int intBatchID, int intPOSID)
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 14));
            prmPOS.Add(new SqlParameter("@WarehouseID", intWareHouseID));
            prmPOS.Add(new SqlParameter("@ItemID", intItemID));
            prmPOS.Add(new SqlParameter("@BatchID", intBatchID));
            prmPOS.Add(new SqlParameter("@intPOSID", intPOSID));
            return objclsConnection.ExecuteDataTable("spInvPOS", prmPOS);
        }
        public DataTable GetDiscount(int intCurrencyID, decimal decNetAmount, DateTime dtePosDate, int intCustomerID)
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 15));
            prmPOS.Add(new SqlParameter("@intCurrencyID", intCurrencyID));
            prmPOS.Add(new SqlParameter("@decNetAmount", decNetAmount));
            prmPOS.Add(new SqlParameter("@dteCreatedDate", dtePosDate));
            prmPOS.Add(new SqlParameter("@intVendorID", intCustomerID));
            return objclsConnection.ExecuteDataTable("spInvPOS", prmPOS);
        }

        public DataSet GetEmailPOSinfo() // POS 
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 17));
            prmPOS.Add(new SqlParameter("@POSID", objclsDTOPOS.intPOSID));
            return objclsConnection.ExecuteDataSet("spInvPOS", prmPOS);

        }

        public bool SaveItemSummary(int intPOSID)
        {
            bool blnStatus = false;
            DataTable datTempMaster = FillCombos(new string[] { "*", "InvPOSMaster", "POSID=" + intPOSID });
            DataTable datTemp = FillCombos(new string[] { "*", "InvPOSDetails", "POSID=" + intPOSID });
            if (datTemp != null)
            {
                for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                {
                    try
                    {
                        ArrayList prmItemSummary = new ArrayList();
                        prmItemSummary.Add(new SqlParameter("@Mode", 18));
                        prmItemSummary.Add(new SqlParameter("@StatusID", Convert.ToInt32(datTempMaster.Rows[0]["StatusID"].ToString())));
                        prmItemSummary.Add(new SqlParameter("@ItemID", Convert.ToInt32(datTemp.Rows[iCounter]["ItemID"].ToString())));
                        prmItemSummary.Add(new SqlParameter("@BatchID", Convert.ToInt32(datTemp.Rows[iCounter]["BatchID"].ToString())));
                        prmItemSummary.Add(new SqlParameter("@ActualQuantity", Convert.ToDecimal(datTemp.Rows[iCounter]["Quantity"].ToString())));
                        prmItemSummary.Add(new SqlParameter("@UOMID", Convert.ToInt32(datTemp.Rows[iCounter]["UOMID"].ToString())));
                        prmItemSummary.Add(new SqlParameter("@CompanyID", Convert.ToInt32(datTempMaster.Rows[0]["CompanyID"].ToString())));
                        prmItemSummary.Add(new SqlParameter("@WarehouseID", Convert.ToInt32(datTempMaster.Rows[0]["WarehouseID"].ToString())));
                        prmItemSummary.Add(new SqlParameter("@POSDate", datTempMaster.Rows[0]["POSDate"].ToString()));
                        prmItemSummary.Add(new SqlParameter("@BaseUnitQty", MobjClsCommonUtility.ConvertQtyToBaseUnitQty(Convert.ToInt32(datTemp.Rows[iCounter]["UOMID"].ToString()),
                            Convert.ToInt32(datTemp.Rows[iCounter]["ItemID"].ToString()), Convert.ToDecimal(datTemp.Rows[iCounter]["Quantity"].ToString()), 1)));
                        prmItemSummary.Add(new SqlParameter("@NetAmount", Convert.ToDecimal(datTemp.Rows[iCounter]["NetAmount"].ToString())));
                        prmItemSummary.Add(new SqlParameter("@DiscountAmount", Convert.ToDecimal(datTemp.Rows[iCounter]["DiscountAmount"].ToString())));
                        prmItemSummary.Add(new SqlParameter("@CurrencyID", Convert.ToInt32(datTempMaster.Rows[0]["CurrencyID"].ToString())));
                        prmItemSummary.Add(new SqlParameter("@blnIsGroup", Convert.ToBoolean(datTemp.Rows[0]["IsGroup"])));
                        prmItemSummary.Add(new SqlParameter("@POSID", intPOSID));
                        objclsConnection.ExecuteNonQueryWithTran("spInvPOS", prmItemSummary);
                        blnStatus = true;
                    }
                    catch { blnStatus = false; }
                }
            }
            return blnStatus;
        }

        public bool DeleteItemSummary(int intPOSID)
        {
            bool blnStatus = false;
            DataTable datTempMaster = FillCombos(new string[] { "*", "InvPOSMaster", "POSID=" + intPOSID });
            if (datTempMaster != null)
            {
                if (datTempMaster.Rows.Count > 0)
                {
                    if (Convert.ToInt32(datTempMaster.Rows[0]["StatusID"].ToString()) != (int)OperationStatusType.SPOSCancelled)
                    {
                        try
                        {
                            ArrayList prmItemSummary = new ArrayList();
                            prmItemSummary.Add(new SqlParameter("@Mode", 19));
                            prmItemSummary.Add(new SqlParameter("@POSID", intPOSID));
                            objclsConnection.ExecuteNonQueryWithTran("spInvPOS", prmItemSummary);
                            blnStatus = true;
                        }
                        catch { blnStatus = false; }
                    }
                }
            }
            return blnStatus;
        }

        public bool DeleteItemSummarywhenCancelled(int intPOSID)
        {
            bool blnStatus = false;
            DataTable datTempMaster = FillCombos(new string[] { "*", "InvPOSMaster", "POSID=" + intPOSID });
            if (datTempMaster != null)
            {
                if (datTempMaster.Rows.Count > 0)
                {
                    try
                    {
                        ArrayList prmItemSummary = new ArrayList();
                        prmItemSummary.Add(new SqlParameter("@Mode", 19));
                        prmItemSummary.Add(new SqlParameter("@POSID", intPOSID));
                        objclsConnection.ExecuteNonQueryWithTran("spInvPOS", prmItemSummary);
                        blnStatus = true;
                    }
                    catch { blnStatus = false; }
                }
            }
            return blnStatus;
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 12));
            prmCommon.Add(new SqlParameter("@RoleID", intRoleID));
            prmCommon.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmCommon.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable("STPermissionSettings", prmCommon);
        }
        public DataTable GetItemForBarCode(int intCompanyID, int intItemID, int intBatchID)
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 23));
            prmPOS.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPOS.Add(new SqlParameter("@ItemID", intItemID));
            prmPOS.Add(new SqlParameter("@BatchID", intBatchID));
            return objclsConnection.ExecuteDataTable("spInvPOS", prmPOS);
        }

        public bool GetCompanyAccount(int intTransactionType, int intCompanyID)
        {
            // function for Geting Company Account
            ArrayList prmPurchase = new ArrayList();
            object objOutAccountID = 0;
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 18));
            prmPurchase.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPurchase.Add(new SqlParameter("@TransactionType", intTransactionType));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPurchase.Add(objParam);

            if (objclsConnection.ExecuteNonQuery("spInvPurchaseModuleFunctions", prmPurchase, out objOutAccountID) != 0)
            {
                ClsCommonSettings.PintSIDebitHeadID = (int)objOutAccountID;
                if ((int)objOutAccountID > 0) return true;
            }
            return false;
        }

        public decimal GetItemRateForPOS(long lngItemID, long lngBatchID, long lngPOSID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 24));
            alParameters.Add(new SqlParameter("@ItemID", lngItemID));
            alParameters.Add(new SqlParameter("@BatchID", lngBatchID));
            alParameters.Add(new SqlParameter("@intPOSID", lngPOSID));
            decimal decSaleRate = objclsConnection.ExecuteScalar("spInvPOS", alParameters).ToDecimal();
            return decSaleRate;
        }

        public DataTable BatchLessItems(int intItemID, int intWareHouseID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 26));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWareHouseID));
            return objclsConnection.ExecuteDataTable("spInvPOS", alParameters);
        }

        public DataTable GetGroupDetails(int intItemID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 28));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            return objclsConnection.ExecuteDataTable("spInvPOS", alParameters);
        }

        private bool SavePOSItemGroupIssueDetails(clsDTOPOSDetails objclsDTOPOSDetails)
        {
            //Save Item Group Issue Details
            //int intSerialNo = 0;
            //foreach (clsDTOPOSItemGroupDetails objDTOItemGroupIssueDetails in objclsDTOPOSDetails.lstclsDTOPOSItemGroupDetails)
            //{
            //    ArrayList prmItemGroupIssueDetails = new ArrayList();
            //    prmItemGroupIssueDetails.Add(new SqlParameter("@Mode", 27));
            //    prmItemGroupIssueDetails.Add(new SqlParameter("@SerialNo", ++intSerialNo));
            //    prmItemGroupIssueDetails.Add(new SqlParameter("@POSID", objclsDTOPOSDetails.intPOSID));
            //    prmItemGroupIssueDetails.Add(new SqlParameter("@POSDetailsSerialNo", objclsDTOPOSDetails.intSerialNo));
            //    prmItemGroupIssueDetails.Add(new SqlParameter("@ItemID", objDTOItemGroupIssueDetails.intItemID));
            //    prmItemGroupIssueDetails.Add(new SqlParameter("@BatchID", objDTOItemGroupIssueDetails.intBatchID));
            //    prmItemGroupIssueDetails.Add(new SqlParameter("@UOMID", objDTOItemGroupIssueDetails.intUOMID));
            //    prmItemGroupIssueDetails.Add(new SqlParameter("@Quantity", objDTOItemGroupIssueDetails.decQuantity));
            //    objclsConnection.ExecuteNonQueryWithTran("spInvPOS", prmItemGroupIssueDetails);

            //    clsDTOPOSDetails objItemIssueDetails = new clsDTOPOSDetails();
            //    objItemIssueDetails.decQuantity = objDTOItemGroupIssueDetails.decQuantity;
            //    objItemIssueDetails.intBatchID = objDTOItemGroupIssueDetails.intBatchID;
            //    objItemIssueDetails.intItemID = objDTOItemGroupIssueDetails.intItemID;
            //    objItemIssueDetails.intUOMID = objDTOItemGroupIssueDetails.intUOMID;
            //    //UpdateStockDetails(objItemIssueDetails, false);
            //    //UpdateStockMaster(objItemIssueDetails, false);
            //}
            return true;
        }

        public void DeleteAllPOSDetails()
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 11));
            prmPOS.Add(new SqlParameter("@intPOSID", objclsDTOPOS.intPOSID));
            prmPOS.Add(new SqlParameter("@intCreatedBy", objclsDTOPOS.intCreatedBy));
            prmPOS.Add(new SqlParameter("@OperationModID", objclsDTOPOS.intOperationModID));
            prmPOS.Add(new SqlParameter("@OperationTypeID", (int)OperationType.POS));
            objclsConnection.ExecuteNonQuery("spInvPOS", prmPOS);
        }
        public DataTable GetGroupItemDetails(int intItemID)
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 31));
            prmPOS.Add(new SqlParameter("@ItemID", intItemID));
            return objclsConnection.ExecuteDataTable("spInvPOS", prmPOS);
        }

        public DataSet GetAllQuantityAvailable(int intItemID, int intWareHouseID, int intBatchID, int intPOSID)
        {
            ArrayList prmPOS = new ArrayList();
            prmPOS.Add(new SqlParameter("@Mode", 14));
            prmPOS.Add(new SqlParameter("@WarehouseID", intWareHouseID));
            prmPOS.Add(new SqlParameter("@ItemID", intItemID));
            prmPOS.Add(new SqlParameter("@BatchID", intBatchID));
            prmPOS.Add(new SqlParameter("@intPOSID", intPOSID));
            return objclsConnection.ExecuteDataSet("spInvPOS", prmPOS);
        }

        public decimal GetUsedQuantity(long lngItemID, int WarehouseID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 30));
            alParameters.Add(new SqlParameter("@ItemID", lngItemID));
            alParameters.Add(new SqlParameter("@WarehouseID", WarehouseID));
            alParameters.Add(new SqlParameter("@intPOSID", objclsDTOPOS.intPOSID));
            decimal decUsedQty = objclsConnection.ExecuteScalar("spInvPOS", alParameters).ToDecimal();
            return decUsedQty;
        }
        //public bool GetClosingStockValidation(int CompanyID, int CurrencyID, int ItemID, int BatchID, decimal Quantity, bool IsGroup)
        //{
        //    ArrayList Parameters = new ArrayList();
        //    Parameters.Add(new SqlParameter("@Mode", 31));
        //    Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        //    Parameters.Add(new SqlParameter("@CurrencyID", CurrencyID));
        //    Parameters.Add(new SqlParameter("@ItemID", ItemID));
        //    Parameters.Add(new SqlParameter("@BatchID", BatchID));
        //    Parameters.Add(new SqlParameter("@Quantity", Quantity));
        //    Parameters.Add(new SqlParameter("@IsGroup", IsGroup));
        //    object obj = objclsConnection.ExecuteScalar("spInvPOS", Parameters);
        //    return obj.ToInt32() > 0;
        //}


        //public decimal GetClosingRate(int CompanyID, DateTime ToDate)
        //{
        //    ArrayList alParameters = new ArrayList();
        //    alParameters.Add(new SqlParameter("@Mode", 17));
        //    alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        //    alParameters.Add(new SqlParameter("@ToDate", ToDate));
        //    decimal cRate = objclsConnection.ExecuteScalar("spAccRptAccounts", alParameters).ToDecimal();
        //    return cRate;
        //}
        //public decimal GetGroupItemPurchaseRate(int ItemID, int BatchID)
        //{
        //    ArrayList alParameters = new ArrayList();
        //    alParameters.Add(new SqlParameter("@Mode", 32));
        //    alParameters.Add(new SqlParameter("@ItemID", ItemID));
        //    alParameters.Add(new SqlParameter("@BatchID", BatchID));
        //    decimal PurchaseRate = objclsConnection.ExecuteScalar("spInvPOS", alParameters).ToDecimal();
        //    return PurchaseRate;
        //}

    
    }
}
