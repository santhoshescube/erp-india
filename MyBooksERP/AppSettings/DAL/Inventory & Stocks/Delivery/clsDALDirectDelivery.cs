﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using System.Data;
using System.Data.SqlClient;
/* 
=================================================
   Author:		<Author,,Hima>
   Create date: <Create Date,,22 Mar 2011>
   Description:	<Description,,Delivery Scheduling DAL>
================================================
*/
namespace MyBooksERP
{
    public class clsDALDirectDelivery
    {
        ArrayList prmDirectDelivery, prmDirectDeliveryDetails;               // array list for storing parameters
        public clsDTODirectDeliveryMaster PobjDTODeliveryMaster { get; set; }  // property of clsDTOItemIssueMaster
        public DataLayer MobjDataLayer;                                  // obj of datalayer 
        bool blnAddStatus = false, blnUpdateStatus = false, blnDeleteStatus = false;
        DataSet dtsTemp11 = new DataSet();
        DataSet dtsTemp22 = new DataSet();
        bool blnSalesOrderMasterStatus = true;
        public clsDALDirectDelivery(DataLayer objDataLayer)
        {
            //constructor
            MobjDataLayer = objDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = MobjDataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool SaveItemIssueMaster()
        {
            // Save Function 
            int Mode = 0;
            prmDirectDelivery = new ArrayList();

            if (PobjDTODeliveryMaster.intDirectDeliveryID == 0)
            {
                prmDirectDelivery.Add(new SqlParameter("@Mode", 2));
                Mode = 35;
                blnAddStatus = true;
                blnUpdateStatus = false;
                blnDeleteStatus = false;
            }
            else
            {
                prmDirectDelivery.Add(new SqlParameter("@Mode", 3));
                blnAddStatus = false;
                blnUpdateStatus = false;
                blnDeleteStatus = true;
            //    DeleteItemGroupIssueDetails();

                blnAddStatus = false;
                blnUpdateStatus = true;
                blnDeleteStatus = false;
                Mode = 36;
             //   updateSaleOpStockAccountDetailsEdit(PobjDTODeliveryMaster.intReferenceID);
            }
            prmDirectDelivery.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            prmDirectDelivery.Add(new SqlParameter("@ItemIssueNo", PobjDTODeliveryMaster.strDirectDeliveryNo));
            prmDirectDelivery.Add(new SqlParameter("@OrderTypeID", PobjDTODeliveryMaster.intOrderTypeID));
            prmDirectDelivery.Add(new SqlParameter("@WarehouseID", PobjDTODeliveryMaster.intWarehouseID));
            prmDirectDelivery.Add(new SqlParameter("@DeliveryDate", PobjDTODeliveryMaster.strDeliveryDate));
            prmDirectDelivery.Add(new SqlParameter("@IssuedBy", PobjDTODeliveryMaster.intIssuedBy));
            prmDirectDelivery.Add(new SqlParameter("@IssuedDate", PobjDTODeliveryMaster.strIssuedDate));
            prmDirectDelivery.Add(new SqlParameter("@CompanyID", PobjDTODeliveryMaster.intCompanyID));
            prmDirectDelivery.Add(new SqlParameter("@VendorID", PobjDTODeliveryMaster.intVendorID));
            prmDirectDelivery.Add(new SqlParameter("@VendorAddID", PobjDTODeliveryMaster.intVendorAddID));
            prmDirectDelivery.Add(new SqlParameter("@LPONumber", PobjDTODeliveryMaster.Lpono));
            prmDirectDelivery.Add(new SqlParameter("@TotalAmount",PobjDTODeliveryMaster.decNetAmount));

            prmDirectDelivery.Add(new SqlParameter("@Remarks", PobjDTODeliveryMaster.strRemarks));
            prmDirectDelivery.Add(new SqlParameter("@OperationTypeID", (int)OperationType.DeliveryNote));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmDirectDelivery.Add(objParam);
            object objItemIssueID = 0;

            if (MobjDataLayer.ExecuteNonQueryWithTran("spDirectDelivery", prmDirectDelivery, out objItemIssueID) != 0)
            {
                if (Convert.ToInt64(objItemIssueID) > 0)
                {
                    if (PobjDTODeliveryMaster.intDirectDeliveryID != 0)
                    {
                        //DeleteItemIssueDetails();
                      //DeleteItemGroupIssueDetails();

                        //if (PobjDTODeliveryMaster.intOperationTypeID != (int)OperationOrderType.DNOTSalesInvoice)
                        //DeleteItemIssueLocationDetails();
                    }
                    PobjDTODeliveryMaster.intDirectDeliveryID = Convert.ToInt64(objItemIssueID);
                    if (SaveItemIssueDetails())
                    {
                        //if (PobjDTODeliveryMaster.intOperationTypeID != (int)OperationOrderType.DNOTSalesInvoice)
                        //SaveItemIssueLocationDetails();
                        //if (PobjDTODeliveryMaster.intOperationTypeID == (int)OperationOrderType.DNOTSalesOrder)
                        //    PostAccounts(Mode);

                     //   UpdateSaleOpStockAccountDetails(PobjDTODeliveryMaster.intReferenceID);
                        return true;
                    }
                }
            }
            return false;
        }

        public bool DeleteItemIssue()
        {
            //Delete Function            
            
            if (PobjDTODeliveryMaster.intOperationTypeID == (int)OperationOrderType.DNOTSalesOrder)
                PostAccounts(37); // post account for 
            DeleteItemGroupIssueDetails();
            DeleteItemIssueDetails();
           
            if (PobjDTODeliveryMaster.intOperationTypeID != (int)OperationOrderType.DNOTSalesInvoice)
           // DeleteItemIssueLocationDetails();
            blnAddStatus = false;
            blnUpdateStatus = false;
            blnDeleteStatus = true;
          //  UpdateSaleOpStockAccountDetails(PobjDTODeliveryMaster.intReferenceID);
            
            prmDirectDelivery = new ArrayList();
            prmDirectDelivery.Add(new SqlParameter("@Mode", 4));
            prmDirectDelivery.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            prmDirectDelivery.Add(new SqlParameter("@OrderTypeID", PobjDTODeliveryMaster.intOperationTypeID));
            prmDirectDelivery.Add(new SqlParameter("@SIOperationTypeID", (int)OperationType.SalesInvoice));

            if (PobjDTODeliveryMaster.intOperationTypeID == (int)OperationOrderType.DNOTTransfer)
            {
                if (ClsCommonSettings.StockTransferApproval)
                    prmDirectDelivery.Add(new SqlParameter("@StatusID", (int)OperationStatusType.STApproved));
                else
                    prmDirectDelivery.Add(new SqlParameter("@StatusID", (int)OperationStatusType.STSubmitted));
            }

            if (MobjDataLayer.ExecuteNonQueryWithTran("spDirectDelivery", prmDirectDelivery) != 0)
            {                
                return true;
            }
            return false;
        }

        private bool SaveItemIssueDetails()
        {
            //Save Item Issue Details
            int intSerialNo = 0;
            //int   intBatchless = 0;
            foreach (clsDTODirectDeliveryDetails objDTOItemIssueDetails in PobjDTODeliveryMaster.lstDirectDeliveryDetails)
            {



                
                       prmDirectDeliveryDetails = new ArrayList();
                       prmDirectDeliveryDetails.Add(new SqlParameter("@Mode", 8));
                       prmDirectDeliveryDetails.Add(new SqlParameter("@SerialNo", ++intSerialNo));
                       prmDirectDeliveryDetails.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
                       prmDirectDeliveryDetails.Add(new SqlParameter("@ReferenceSerialNo", objDTOItemIssueDetails.intReferenceSerialNo));
                       prmDirectDeliveryDetails.Add(new SqlParameter("@ItemID", objDTOItemIssueDetails.intItemID));
                       prmDirectDeliveryDetails.Add(new SqlParameter("@IsGroup", objDTOItemIssueDetails.blnIsGroup));
                       prmDirectDeliveryDetails.Add(new SqlParameter("@BatchID", objDTOItemIssueDetails.intBatchID));
                       prmDirectDeliveryDetails.Add(new SqlParameter("@UOMID", objDTOItemIssueDetails.intUOMID));
                       prmDirectDeliveryDetails.Add(new SqlParameter("@Quantity", objDTOItemIssueDetails.decQuantity));
                       prmDirectDeliveryDetails.Add(new SqlParameter("@CompanyID", PobjDTODeliveryMaster.intCompanyID));
                       prmDirectDeliveryDetails.Add(new SqlParameter("@WarehouseID", PobjDTODeliveryMaster.intWarehouseID));
                       prmDirectDeliveryDetails.Add(new SqlParameter("@IssuedDate", PobjDTODeliveryMaster.strIssuedDate));
                       prmDirectDeliveryDetails.Add(new SqlParameter("@Rate", objDTOItemIssueDetails.decRate));
                       prmDirectDeliveryDetails.Add(new SqlParameter("@NetAmount", objDTOItemIssueDetails.decNetAmount));
                   
               
                if (objDTOItemIssueDetails.blnIsGroup)
                {
                    objDTOItemIssueDetails.intSerialNo = intSerialNo;
                    SaveItemGroupIssueDetails(objDTOItemIssueDetails);
                }
        
                MobjDataLayer.ExecuteNonQueryWithTran("spDirectDelivery", prmDirectDeliveryDetails);
                if (!objDTOItemIssueDetails.blnIsGroup)
                {
                    UpdateStockDetails(objDTOItemIssueDetails, false);
                    UpdateStockMaster(objDTOItemIssueDetails, false);
                }
                //UpdateStockValue();

        }
      
            

            return true;
}

    
        public bool DeleteItemIssueDetails()
        {
            //Delete ItemIssueDetails by ItemIssueID
            DataTable dtItemIssueDetails = GetItemIssueDetailsForDeletion();
            foreach (DataRow dr in dtItemIssueDetails.Rows)
            {
                clsDTODirectDeliveryDetails objItemIssueDetails = new clsDTODirectDeliveryDetails();
                objItemIssueDetails.decQuantity = Convert.ToDecimal(dr["Quantity"]);
                objItemIssueDetails.intBatchID = Convert.ToInt64(dr["BatchID"]);
                objItemIssueDetails.intItemID = Convert.ToInt32(dr["ItemID"]);
                objItemIssueDetails.intUOMID = Convert.ToInt32(dr["UomID"]);


                if (Convert.ToBoolean(dr["IsGroup"]) == false)
                {
                    UpdateStockDetails(objItemIssueDetails, true);
                    UpdateStockMaster(objItemIssueDetails, true);
                }

                
           }
            //prmDirectDeliveryDetails = new ArrayList();
            prmDirectDeliveryDetails = new ArrayList();
            prmDirectDeliveryDetails.Add(new SqlParameter("@Mode", 9));
            prmDirectDeliveryDetails.Add(new SqlParameter("@OperationTypeID", (int)OperationType.DeliveryNote));
            prmDirectDeliveryDetails.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));

        

            if (MobjDataLayer.ExecuteNonQueryWithTran("spDirectDelivery", prmDirectDeliveryDetails) != 0)
                return true;
            return false;
        }

    

        public bool DisplayItemIssueMasterInfo()
        {
            //Display PurchaseIndent Master Information
            prmDirectDelivery = new ArrayList();
            prmDirectDelivery.Add(new SqlParameter("@Mode", 1));
            prmDirectDelivery.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            DataTable datItemIssue = MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmDirectDelivery);
            if (datItemIssue.Rows.Count >0)
            {
                PobjDTODeliveryMaster.intDirectDeliveryID = Convert.ToInt64(datItemIssue.Rows[0]["ItemIssueID"]);
                PobjDTODeliveryMaster.strDirectDeliveryNo = Convert.ToString(datItemIssue.Rows[0]["ItemIssueNo"]);

                PobjDTODeliveryMaster.intOperationTypeID = Convert.ToInt32(datItemIssue.Rows[0]["OrderTypeID"]);
                
                PobjDTODeliveryMaster.intWarehouseID = Convert.ToInt32(datItemIssue.Rows[0]["WarehouseID"]);
                PobjDTODeliveryMaster.intIssuedBy = Convert.ToInt32(datItemIssue.Rows[0]["IssuedBy"]);
                PobjDTODeliveryMaster.strIssuedDate = Convert.ToString(datItemIssue.Rows[0]["IssuedDate"]);
                PobjDTODeliveryMaster.intCompanyID = Convert.ToInt32(datItemIssue.Rows[0]["CompanyID"]);
                if (datItemIssue.Rows[0]["LPONumber"] != DBNull.Value)
                    PobjDTODeliveryMaster.Lpono = Convert.ToString(datItemIssue.Rows[0]["LPONumber"]);
                else PobjDTODeliveryMaster.Lpono = "";

                if (datItemIssue.Rows[0]["VendorID"] != DBNull.Value)
                    PobjDTODeliveryMaster.intVendorID = Convert.ToInt32(datItemIssue.Rows[0]["VendorID"]);
                else PobjDTODeliveryMaster.intVendorID = 0;
                if (datItemIssue.Rows[0]["VendorAddID"] != DBNull.Value)
                    PobjDTODeliveryMaster.intVendorAddID = Convert.ToInt32(datItemIssue.Rows[0]["VendorAddID"]);
                else PobjDTODeliveryMaster.intVendorAddID = 0;
                PobjDTODeliveryMaster.strIssuedBy = datItemIssue.Rows[0]["Employee"].ToString();
                if (datItemIssue.Rows[0]["Remarks"] != DBNull.Value)
                    PobjDTODeliveryMaster.strRemarks = Convert.ToString(datItemIssue.Rows[0]["Remarks"]);
                if (datItemIssue.Rows[0]["StatusID"] != DBNull.Value)
                    PobjDTODeliveryMaster.intStatusID = Convert.ToInt32(datItemIssue.Rows[0]["StatusID"]);
                return true;
            }
            return false;
        }

        public DataTable GetItemIssueDetails()
        {
            //function for getting delivery schedule details find by scheduleID
            prmDirectDeliveryDetails = new ArrayList();
            prmDirectDeliveryDetails.Add(new SqlParameter("@Mode", 7));
            prmDirectDeliveryDetails.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            return MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmDirectDeliveryDetails);
        }

        public int GetNextIssueNo(int intCompanyID,int intFormType)
        {
            //Function for getting Next Issue No
            prmDirectDelivery = new ArrayList();
            prmDirectDelivery.Add(new SqlParameter("@Mode", 5));
            prmDirectDelivery.Add(new SqlParameter("@CompanyID", intCompanyID));
            
       
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spDirectDelivery", prmDirectDelivery);
            int intNextSerialNo = 0;

            if (sdr.Read())
            {
                if (sdr["MaxNo"] != DBNull.Value)
                    intNextSerialNo = Convert.ToInt32(sdr["MaxNo"]);
            }
            sdr.Close();
            return ++intNextSerialNo;
        }

        public DataTable GetIssueNos(string strCondition)
        {
            // Get All Issue Nos By Filter Condition
            prmDirectDeliveryDetails = new ArrayList();
            prmDirectDeliveryDetails.Add(new SqlParameter("@Mode", 6));
            prmDirectDeliveryDetails.Add(new SqlParameter("@Condition", strCondition));
            return MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmDirectDeliveryDetails);
        }

        public string FindEmployeeNameByUserID(int intUserID)
        {
            string strEmployeeName = string.Empty;
            prmDirectDelivery = new ArrayList();
            prmDirectDelivery.Add(new SqlParameter("@Mode", 10));
            prmDirectDelivery.Add(new SqlParameter("@UserID", intUserID));
            SqlDataReader sdr;
            sdr = MobjDataLayer.ExecuteReader("STUserInformation", prmDirectDelivery);
            if (sdr.Read())
            {
                if (sdr["FirstName"] != DBNull.Value)
                {
                    strEmployeeName = Convert.ToString(sdr["FirstName"]);
                }
            }
            sdr.Close();
            return strEmployeeName;
        }

        public DataTable GetDataForItemSelection(  int intCompanyID,int intWareHouseID)
        {
            prmDirectDeliveryDetails = new ArrayList();

            //prmDirectDeliveryDetails.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            prmDirectDeliveryDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmDirectDeliveryDetails.Add(new SqlParameter("@WarehouseID", intWareHouseID));

            //if (intOperationType != 0)
            //{
                prmDirectDeliveryDetails.Add(new SqlParameter("@Mode", 10));
                //prmDirectDeliveryDetails.Add(new SqlParameter("@OrderTypeID", intOperationType));
                //prmDirectDeliveryDetails.Add(new SqlParameter("@ReferenceID", intReferenceID));
                DataSet ds = MobjDataLayer.ExecuteDataSet("spInvItemSelectionDirectDelivery", prmDirectDeliveryDetails);
                if (ds.Tables.Count > 0)
                {

                    if (ds.Tables.Count > 1)
                    {
                        DataTable dt = ds.Tables[0].Copy();

                        foreach (DataRow dr in ds.Tables[1].Rows)
                        {
                            DataRow dr1 = dt.NewRow();
                            dr1["ItemCode"] = dr["ItemCode"];
                            dr1["ItemName"] = dr["ItemName"];
                            dr1["ItemID"] = dr["ItemGroupID"];
                            dr1["Rate"] = dr["Rate"];
                            //if(intFormType !=1)
                            dr1["QtyAvailable"] = dr["AvailableQty"];
                            dr1["IsGroup"] = true;
                            dr1["BatchID"] ="0";
                            dr1["IsGroupItem"] = "Yes";
                            dt.Rows.Add(dr1);
                        }
                        return dt;
                    }
                    else
                        return ds.Tables[0];
                }
                return null;



            //}
            //else
            //{
            //    prmDirectDeliveryDetails.Add(new SqlParameter("@Mode", 11));
            //    return MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmDirectDeliveryDetails);
            //}
        }

        public DataTable GetItemDetails(int intOperationTypeID, Int64 intReferenceID)
        {
            prmDirectDeliveryDetails = new ArrayList();
            prmDirectDeliveryDetails.Add(new SqlParameter("@Mode", 16));
            prmDirectDeliveryDetails.Add(new SqlParameter("@OrderTypeID", intOperationTypeID));
            //prmDirectDeliveryDetails.Add(new SqlParameter("@ReferenceID", intReferenceID));
            return MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmDirectDeliveryDetails);
        }
        public decimal GetAvailableQty(int intItemID, int WareHouseID)
        {
            prmDirectDelivery = new ArrayList();
            prmDirectDelivery.Add(new SqlParameter("@Mode", 55));
            prmDirectDelivery.Add(new SqlParameter("@ItemID", intItemID));
            prmDirectDelivery.Add(new SqlParameter("@WarehouseID", WareHouseID));
            return MobjDataLayer.ExecuteScalar("spDirectDelivery", prmDirectDeliveryDetails).ToDecimal();
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            prmDirectDelivery = new ArrayList();
            prmDirectDelivery.Add(new SqlParameter("@Mode", 10));
            prmDirectDelivery.Add(new SqlParameter("@ItemID", intItemID));
          //  prmDirectDelivery.Add(new SqlParameter("@UomTypeID", 1));
            DataTable dtItemUoms = new DataTable();
            dtItemUoms = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmDirectDelivery);

            prmDirectDelivery = new ArrayList();
            prmDirectDelivery.Add(new SqlParameter("@Mode", 12));
            prmDirectDelivery.Add(new SqlParameter("@ItemID", intItemID));
            DataTable dtItemBaseUnit = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmDirectDelivery);
            DataRow dr = dtItemUoms.NewRow();
            dr["UOMID"] = dtItemBaseUnit.Rows[0]["UOMID"];
            dr["Description"] = dtItemBaseUnit.Rows[0]["Description"];
            dr["ShortName"] = dtItemBaseUnit.Rows[0]["ShortName"];
            dr["ItemID"] = intItemID;
            dtItemUoms.Rows.InsertAt(dr, 0);
            return dtItemUoms;
        }

        public decimal GetItemLocationQty(clsDTOItemLocationDetails objItemLocationDetails,int intWarehouseID,bool blnIsSum)
        {
            ArrayList prmItemLocation = new ArrayList();
            prmItemLocation.Add(new SqlParameter("@Mode", 15));
            prmItemLocation.Add(new SqlParameter("@ItemID", objItemLocationDetails.intItemID));
            prmItemLocation.Add(new SqlParameter("@BatchID", objItemLocationDetails.intBatchID));
            prmItemLocation.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            prmItemLocation.Add(new SqlParameter("@LocationID", objItemLocationDetails.intLocationID));
            prmItemLocation.Add(new SqlParameter("@RowID", objItemLocationDetails.intRowID));
            prmItemLocation.Add(new SqlParameter("@BlockID", objItemLocationDetails.intBlockID));
            prmItemLocation.Add(new SqlParameter("@LotID", objItemLocationDetails.intLotID));
            DataTable  datItemLocationDetails = MobjDataLayer.ExecuteDataTable("STItemLocationDetail", prmItemLocation);
            decimal decQuantity = 0;
            if (datItemLocationDetails.Rows.Count >0)
            {
                if (datItemLocationDetails.Rows[0]["Quantity"] != DBNull.Value)
                {
                    decQuantity = Convert.ToDecimal(datItemLocationDetails.Rows[0]["Quantity"]);
                }
            }
            return decQuantity;
        }

        public decimal GetStockQuantity(int intItemID, Int64 intBatchID,int intCompanyID,int intWarehouseID)
        {
            ArrayList prmStockDetails = new ArrayList();
            prmStockDetails.Add(new SqlParameter("@Mode", 53));
            prmStockDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmStockDetails.Add(new SqlParameter("@BatchID", intBatchID));
            prmStockDetails.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            prmStockDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            //prmStockDetails.Add(new SqlParameter("@IDate", strDate));
            DataTable datStockDetails = MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmStockDetails);
            decimal decQuantity = 0;
            if (datStockDetails.Rows.Count >0)
            {
                if (datStockDetails.Rows[0]["AvailableQty"] != DBNull.Value)
                {
                    decQuantity = Convert.ToDecimal(datStockDetails.Rows[0]["AvailableQty"]);
                }
            }
            return decQuantity;
        }

        public decimal GetAvailableQtyAgainstJobOrder(long lngJobOrderID, long lngJobOrderProductID, int intWarehouseID)
        {

            ArrayList prmStockDetails = new ArrayList();
            prmStockDetails.Add(new SqlParameter("@Mode", 11));
            prmStockDetails.Add(new SqlParameter("@JobOrderProductID", lngJobOrderProductID));
            prmStockDetails.Add(new SqlParameter("@JobOrderID", lngJobOrderID));
            prmStockDetails.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            DataTable datStockDetails = MobjDataLayer.ExecuteDataTable("spInvStockMaster", prmStockDetails);
            decimal decQuantity = 0;
            if (datStockDetails.Rows.Count > 0)
            {
                if (datStockDetails.Rows[0]["AvailableQty"] != DBNull.Value)
                {
                    decQuantity = Convert.ToDecimal(datStockDetails.Rows[0]["AvailableQty"]);
                }
            }
            return decQuantity;
        }


        private void UpdateStockDetails(clsDTODirectDeliveryDetails objItemIssueDetails,bool blnIsDelete)
        {
            decimal decProducedQty=0,decReceivedFromTransfer = 0, decDamagedQty = 0, decGRNQty = 0, decDemoQty = 0, decInvoicedQty = 0, decOpeningStock = 0,decSoldQuantity=0,decTransferedQty=0;
            DataTable datStockdetails = new DataTable();
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 4));
            alParameters.Add(new SqlParameter("@ItemID", objItemIssueDetails.intItemID));
            alParameters.Add(new SqlParameter("@BatchID", objItemIssueDetails.intBatchID));
            alParameters.Add(new SqlParameter("@WarehouseID", PobjDTODeliveryMaster.intWarehouseID));
            alParameters.Add(new SqlParameter("@CompanyID", PobjDTODeliveryMaster.intCompanyID));
            datStockdetails = MobjDataLayer.ExecuteDataTable("spInvStockMaster", alParameters);
            if (datStockdetails.Rows.Count > 0)
            {
                decReceivedFromTransfer = datStockdetails.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                decOpeningStock = datStockdetails.Rows[0]["OpeningStock"].ToDecimal();
                decGRNQty = datStockdetails.Rows[0]["GRNQuantity"].ToDecimal();
                decDamagedQty = datStockdetails.Rows[0]["DamagedQuantity"].ToDecimal();
                decInvoicedQty = datStockdetails.Rows[0]["InvoicedQuantity"].ToDecimal();
                decDemoQty = datStockdetails.Rows[0]["DemoQuantity"].ToDecimal();
                decSoldQuantity = datStockdetails.Rows[0]["SoldQuantity"].ToDecimal();
                decTransferedQty = datStockdetails.Rows[0]["TransferedQuantity"].ToDecimal();
                decProducedQty = datStockdetails.Rows[0]["ProducedQuantity"].ToDecimal();
            }
         
            ArrayList prmStockDetails = new ArrayList();
            prmStockDetails.Add(new SqlParameter("@Mode", 6));
            prmStockDetails.Add(new SqlParameter("@ItemID", objItemIssueDetails.intItemID));
            prmStockDetails.Add(new SqlParameter("@BatchID", objItemIssueDetails.intBatchID));
            prmStockDetails.Add(new SqlParameter("@WarehouseID", PobjDTODeliveryMaster.intWarehouseID));
            prmStockDetails.Add(new SqlParameter("@CompanyID", PobjDTODeliveryMaster.intCompanyID));

            DataTable dtUomDetails = new DataTable();
            decimal decIssueQuantity = 0;
                decIssueQuantity = objItemIssueDetails.decQuantity;
                dtUomDetails = GetUomConversionValues(objItemIssueDetails.intUOMID, objItemIssueDetails.intItemID);
            if (dtUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                    decIssueQuantity = decIssueQuantity / decConversionValue;
                else if (intConversionFactor == 2)
                    decIssueQuantity = decIssueQuantity * decConversionValue;
            }
            if (PobjDTODeliveryMaster.intOperationTypeID == (int)OperationOrderType.DNOTTransfer )
            {
                if (!blnIsDelete)
                    prmStockDetails.Add(new SqlParameter("@TransferedQuantity", decTransferedQty + decIssueQuantity));
                else
                    prmStockDetails.Add(new SqlParameter("@TransferedQuantity", decTransferedQty - decIssueQuantity));
                prmStockDetails.Add(new SqlParameter("@SoldQuantity", decSoldQuantity));
               
            }
            else
            {
                if (!blnIsDelete)
                    prmStockDetails.Add(new SqlParameter("@SoldQuantity", decSoldQuantity + decIssueQuantity));
                else
                    prmStockDetails.Add(new SqlParameter("@SoldQuantity", decSoldQuantity - decIssueQuantity));
                prmStockDetails.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));
            }

            prmStockDetails.Add(new SqlParameter("@DamagedQuantity", decDamagedQty));
            prmStockDetails.Add(new SqlParameter("@GRNQuantity", decGRNQty));
            prmStockDetails.Add(new SqlParameter("@InvoicedQuantity", decInvoicedQty));
            prmStockDetails.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
            prmStockDetails.Add(new SqlParameter("@OpeningStock", decOpeningStock));
            prmStockDetails.Add(new SqlParameter("@DemoQuantity", decDemoQty));
            prmStockDetails.Add(new SqlParameter("@ProducedQuantity", decProducedQty));

            MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmStockDetails);
        }

        private void UpdateStockMaster(clsDTODirectDeliveryDetails objItemIssueDetails, bool blnIsDelete)
        {
            decimal decProducedQty =0,decReceivedFromTransfer = 0, decDamagedQty = 0, decGRNQty = 0, decDemoQty = 0, decInvoicedQty = 0, decOpeningStock = 0, decSoldQuantity = 0, decTransferedQty = 0;
            DataTable datStockdetails = new DataTable();
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@ItemID", objItemIssueDetails.intItemID));
            datStockdetails = MobjDataLayer.ExecuteDataTable("spInvStockMaster", alParameters);
            if (datStockdetails.Rows.Count > 0)
            {
                decReceivedFromTransfer = datStockdetails.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                decOpeningStock = datStockdetails.Rows[0]["OpeningStock"].ToDecimal();
                decGRNQty = datStockdetails.Rows[0]["GRNQuantity"].ToDecimal();
                decDamagedQty = datStockdetails.Rows[0]["DamagedQuantity"].ToDecimal();
                decInvoicedQty = datStockdetails.Rows[0]["InvoicedQuantity"].ToDecimal();
                decDemoQty = datStockdetails.Rows[0]["DemoQuantity"].ToDecimal();
                decSoldQuantity = datStockdetails.Rows[0]["SoldQuantity"].ToDecimal();
                decTransferedQty = datStockdetails.Rows[0]["TransferedQuantity"].ToDecimal();
                decProducedQty = datStockdetails.Rows[0]["ProducedQuantity"].ToDecimal();
            }

            ArrayList prmStockDetails = new ArrayList();
            prmStockDetails.Add(new SqlParameter("@Mode", 3));
            prmStockDetails.Add(new SqlParameter("@ItemID", objItemIssueDetails.intItemID));
            DataTable dtUomDetails = new DataTable();
            decimal decIssueQuantity = 0;
                decIssueQuantity = objItemIssueDetails.decQuantity;
                dtUomDetails = GetUomConversionValues(objItemIssueDetails.intUOMID, objItemIssueDetails.intItemID);
            if (dtUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                    decIssueQuantity = decIssueQuantity / decConversionValue;
                else if (intConversionFactor == 2)
                    decIssueQuantity = decIssueQuantity * decConversionValue;
            }
            if (PobjDTODeliveryMaster.intOperationTypeID == (int)OperationOrderType.DNOTTransfer )
            {
                if (!blnIsDelete)
                    prmStockDetails.Add(new SqlParameter("@TransferedQuantity", decTransferedQty + decIssueQuantity));
                else
                    prmStockDetails.Add(new SqlParameter("@TransferedQuantity", decTransferedQty - decIssueQuantity));

                prmStockDetails.Add(new SqlParameter("@SoldQuantity", decSoldQuantity));
            }
            else
            {
                if (!blnIsDelete)
                    prmStockDetails.Add(new SqlParameter("@SoldQuantity", decSoldQuantity + decIssueQuantity));
                else
                    prmStockDetails.Add(new SqlParameter("@SoldQuantity", decSoldQuantity - decIssueQuantity));
                prmStockDetails.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));
              
            }

            prmStockDetails.Add(new SqlParameter("@DamagedQuantity", decDamagedQty));
            prmStockDetails.Add(new SqlParameter("@GRNQuantity", decGRNQty));
            prmStockDetails.Add(new SqlParameter("@InvoicedQuantity", decInvoicedQty));
            prmStockDetails.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
            prmStockDetails.Add(new SqlParameter("@OpeningStock", decOpeningStock));
            prmStockDetails.Add(new SqlParameter("@DemoQuantity", decDemoQty));
            prmStockDetails.Add(new SqlParameter("@ProducedQuantity", decProducedQty));
            prmStockDetails.Add(new SqlParameter("@LastModifiedBy", PobjDTODeliveryMaster.intIssuedBy));
            prmStockDetails.Add(new SqlParameter("@LastModifiedDate",ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")));
            MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmStockDetails);            
        }

        public DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {
            ArrayList prmUomDetails = new ArrayList();
            prmUomDetails.Add(new SqlParameter("@Mode", 11));
            prmUomDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmUomDetails.Add(new SqlParameter("@UnitID", intUOMID));
        //    prmUomDetails.Add(new SqlParameter("@UomTypeID", 1));
            return MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmUomDetails);
        }

        public DataSet GetDeliveryNoteReport()
        {
            ArrayList prmUomDetails = new ArrayList();
            prmUomDetails.Add(new SqlParameter("@Mode", 15));
            prmUomDetails.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            return MobjDataLayer.ExecuteDataSet("spInvItemIssue", prmUomDetails);
        }


        private bool SaveItemGroupIssueDetails(clsDTODirectDeliveryDetails objClsItemIssueDetails)
        {
            //Save Item Group Issue Details
            int intSerialNo = 0;
            foreach (clsDTOItemGroupIssueDetailsDN objDTOItemGroupIssueDetails in objClsItemIssueDetails.lstItemGroupIssueDetailsDN)
            {
                ArrayList prmItemGroupIssueDetails = new ArrayList();
                prmItemGroupIssueDetails.Add(new SqlParameter("@Mode", 17));
                prmItemGroupIssueDetails.Add(new SqlParameter("@SerialNo", ++intSerialNo));
                prmItemGroupIssueDetails.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@ItemIssueDetailsSerialNo", objClsItemIssueDetails.intItemID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@ItemID", objDTOItemGroupIssueDetails.intItemID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@BatchID", objDTOItemGroupIssueDetails.intBatchID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@UOMID", objDTOItemGroupIssueDetails.intUOMID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@Quantity", objDTOItemGroupIssueDetails.decQuantity));
                MobjDataLayer.ExecuteNonQueryWithTran("spDirectDelivery", prmItemGroupIssueDetails);

                clsDTODirectDeliveryDetails objItemIssueDetails = new clsDTODirectDeliveryDetails();
                objItemIssueDetails.decQuantity = objDTOItemGroupIssueDetails.decQuantity;
                objItemIssueDetails.intBatchID = objDTOItemGroupIssueDetails.intBatchID;
                objItemIssueDetails.intItemID = objDTOItemGroupIssueDetails.intItemID;
                objItemIssueDetails.intUOMID = objDTOItemGroupIssueDetails.intUOMID;
                UpdateStockDetails(objItemIssueDetails, false);
                UpdateStockMaster(objItemIssueDetails,false);
            }
            SaveItemSummary(objClsItemIssueDetails);

            return true;
        }

        private void SaveItemSummary(clsDTODirectDeliveryDetails objClsItemIssueDetails)
        {
            foreach (clsDTOItemGroupIssueDetailsDN objDTOItemGroupIssueDetails in objClsItemIssueDetails.lstItemGroupIssueDetailsDN)
            {
                ArrayList prmItemGroupIssueDetails = new ArrayList();
                prmItemGroupIssueDetails.Add(new SqlParameter("@Mode", 54));
                prmItemGroupIssueDetails.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@ItemID", objDTOItemGroupIssueDetails.intItemID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@BatchID", objDTOItemGroupIssueDetails.intBatchID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@UOMID", objDTOItemGroupIssueDetails.intUOMID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@Quantity", objDTOItemGroupIssueDetails.decQuantity));
                MobjDataLayer.ExecuteNonQueryWithTran("spDirectDelivery", prmItemGroupIssueDetails);
            }

        }

        public bool DeleteItemGroupIssueDetails()
        {
            //Delete ItemIssueDetails by ItemIssueID
            DataTable dtItemGroupIssueDetails = GetItemGroupIssueDetails();
            foreach (DataRow dr in dtItemGroupIssueDetails.Rows)
            {
                clsDTODirectDeliveryDetails objItemIssueDetails = new clsDTODirectDeliveryDetails();
                objItemIssueDetails.decQuantity = Convert.ToDecimal(dr["Quantity"]);
                objItemIssueDetails.intBatchID = Convert.ToInt64(dr["BatchID"]);
                objItemIssueDetails.intItemID = Convert.ToInt32(dr["ItemID"]);
                objItemIssueDetails.intUOMID = Convert.ToInt32(dr["UomID"]);
                UpdateStockDetails(objItemIssueDetails, true);
                UpdateStockMaster(objItemIssueDetails, true);
            }
            ArrayList prmItemGroupIssueDetails = new ArrayList();
            prmItemGroupIssueDetails = new ArrayList();
            prmItemGroupIssueDetails.Add(new SqlParameter("@Mode", 19));
            prmItemGroupIssueDetails.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));

            if (MobjDataLayer.ExecuteNonQueryWithTran("spDirectDelivery", prmItemGroupIssueDetails) != 0)
                return true;
            return false;
        }

        private bool UpdateStockValue()
        {
            //This is to Update Stock Value

            ArrayList prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation.Add(new SqlParameter("@Mode", 30));
            prmItemStockValueUpdation.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
           // prmItemStockValueUpdation.Add(new SqlParameter("@ReferenceID", PobjDTODeliveryMaster.intReferenceID));
            prmItemStockValueUpdation.Add(new SqlParameter("@SIOperationTypeID", (int)OperationType.SalesInvoice));
            prmItemStockValueUpdation.Add(new SqlParameter("@OperationTypeID", (int)OperationType.DeliveryNote));

            if (MobjDataLayer.ExecuteNonQueryWithTran("spDirectDelivery", prmItemStockValueUpdation) != 0)
                return true;
            return false;
        }

        public DataTable GetItemGroupIssueDetails()
        {
            //function for getting ItemGroup Issue Details
            ArrayList prmItemGroupIssueDetails = new ArrayList();
            prmItemGroupIssueDetails.Add(new SqlParameter("@Mode", 20));
            prmItemGroupIssueDetails.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            return MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmItemGroupIssueDetails);
        }

        public DataTable GetItemGroupDetails(int intItemGroupID)
        {
            ArrayList prmItemGroupIssueDetails = new ArrayList();
            prmItemGroupIssueDetails.Add(new SqlParameter("@Mode", 21));
            prmItemGroupIssueDetails.Add(new SqlParameter("@ItemID", intItemGroupID));
            return MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmItemGroupIssueDetails);
        }

        public void updateSaleOpStockAccountDetailsEdit(Int64 ReferenceID)
        {
            dtsTemp11 = SalesInvoiceDetailForStockAccount(ReferenceID);
            if (dtsTemp11.Tables[0].Rows.Count > 0)
            {
                int AccountId = Convert.ToInt32(dtsTemp11.Tables[1].Rows[0]["AccountId"].ToString());
                int CompanyID = 0;
                DateTime InvoiceDate = DateTime.Now;
                double Amount = 0;

                for (int i = 0; i < dtsTemp11.Tables[0].Rows.Count; i++)
                {
                    CompanyID = Convert.ToInt32(dtsTemp11.Tables[0].Rows[i]["CompanyID"].ToString());
                    InvoiceDate = Convert.ToDateTime(dtsTemp11.Tables[0].Rows[i]["IssuedDate"].ToString());
                    Amount += Convert.ToDouble(dtsTemp11.Tables[0].Rows[i]["Amount"].ToString());
                }
                string strAmt = "-" + Amount.ToString();
                Amount = Convert.ToDouble(strAmt);
                UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
            }            
        }

        public bool UpdateSaleOpStockAccountDetails(Int64 ReferenceID)
        {
            DataSet dtsTemp1 = SalesInvoiceDetailForStockAccount(ReferenceID);
            if (dtsTemp1.Tables[0].Rows.Count > 0)
            {
                int AccountId = Convert.ToInt32(dtsTemp1.Tables[1].Rows[0]["AccountId"].ToString());
                int CompanyID = 0;
                DateTime InvoiceDate = DateTime.Now;
                double Amount = 0;

                for (int i = 0; i < dtsTemp1.Tables[0].Rows.Count; i++)
                {
                    CompanyID = Convert.ToInt32(dtsTemp1.Tables[0].Rows[i]["CompanyID"].ToString());
                    InvoiceDate = Convert.ToDateTime(dtsTemp1.Tables[0].Rows[i]["IssuedDate"].ToString());
                    Amount += Convert.ToDouble(dtsTemp1.Tables[0].Rows[i]["Amount"].ToString());
                }
                if (blnAddStatus == true && blnUpdateStatus == false && blnDeleteStatus == false)
                {
                    UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
                }
                else if (blnAddStatus == false && blnUpdateStatus == true && blnDeleteStatus == false)
                {
                    UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
                }
                else if (blnAddStatus == false && blnUpdateStatus == false && blnDeleteStatus == true)
                {
                    string strAmt = "-" + Amount.ToString();
                    Amount = Convert.ToDouble(strAmt);
                    UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
                }
            }            
            blnAddStatus = true;
            blnUpdateStatus = false;
            blnDeleteStatus = false;
            return true;
        }
        
        public DataSet SalesInvoiceDetailForStockAccount(Int64 Rownum)// Getting Invoice detail
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 22));
            //prmCommon.Add(new SqlParameter("@ReferenceID", Rownum));
            return MobjDataLayer.ExecuteDataSet("spDirectDelivery", prmCommon);
        }

        public DataSet SalesInvoiceDetailForStockAccountScheduled(Int64 Rownum)// Getting Invoice detail
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 23));
            //prmCommon.Add(new SqlParameter("@ReferenceID", Rownum));
            return MobjDataLayer.ExecuteDataSet("spDirectDelivery", prmCommon);
        }
        
        public bool UpdateOpStockAccount(int AccountId, int CompanyId, DateTime CurDate, double Amount)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 24));
            prmCommon.Add(new SqlParameter("@OperationTypeID", AccountId));
            prmCommon.Add(new SqlParameter("@CompanyID", CompanyId));
            prmCommon.Add(new SqlParameter("@DeliveryDate", CurDate));
            prmCommon.Add(new SqlParameter("@Quantity", Amount));
            MobjDataLayer.ExecuteNonQuery("spDirectDelivery", prmCommon);
            return true;
        }
        public bool PostAccounts(int mode)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", mode));
            prmCommon.Add(new SqlParameter("@SalesOrderID", PobjDTODeliveryMaster.intReferenceID));
            prmCommon.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            MobjDataLayer.ExecuteNonQuery("spDirectDelivery", prmCommon);
            return true;
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", parameters);
        }

        public DataTable GetOperationTypeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", parameters);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", parameters);
        }

        public DataTable GetEmployeeByPermissionWithDept(int RoleID, int MenuID, int ControlID, int CompanyID, int DeptID)
        {
            ArrayList prmDetails = new ArrayList();
            prmDetails.Add(new SqlParameter("@Mode", 7));
            prmDetails.Add(new SqlParameter("@RoleID", RoleID));
            prmDetails.Add(new SqlParameter("@MenuID", MenuID));
            prmDetails.Add(new SqlParameter("@ControlID", ControlID));
            prmDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmDetails.Add(new SqlParameter("@DeptID", DeptID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", prmDetails);
        }

        public DataTable GetEmployeeByPermissionWithDeptWithoutCompany(int RoleID, int MenuID, int ControlID, int DeptID)
        {
            ArrayList prmDetails = new ArrayList();
            prmDetails.Add(new SqlParameter("@Mode", 8));
            prmDetails.Add(new SqlParameter("@RoleID", RoleID));
            prmDetails.Add(new SqlParameter("@MenuID", MenuID));
            prmDetails.Add(new SqlParameter("@ControlID", ControlID));
            prmDetails.Add(new SqlParameter("@DeptID", DeptID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", prmDetails);
        }

        private bool SaveItemIssueLocationDetails()
        {

            int intSerialNo = 0;
            foreach (clsDTODirectDeliveryLocationDetails objLocationDetails in PobjDTODeliveryMaster.lstDirectDeliveryLocationDetails)
            {
                ArrayList alParameters = new ArrayList();
                 
                alParameters.Add(new SqlParameter("@Mode", 26));
                alParameters.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));

                alParameters.Add(new SqlParameter("@SerialNo", ++intSerialNo));
                alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.intItemID));
                alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.lngBatchID));
                alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.intLocationID));
                alParameters.Add(new SqlParameter("@RowID", objLocationDetails.intRowID));
                alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.intBlockID));
                alParameters.Add(new SqlParameter("@LotID", objLocationDetails.intLotID));
                alParameters.Add(new SqlParameter("@UOMID", objLocationDetails.intUOMID));
                alParameters.Add(new SqlParameter("@Quantity", objLocationDetails.decQuantity));
                    DataTable dtGetUomDetails = GetUomConversionValues(objLocationDetails.intUOMID, objLocationDetails.intItemID);
                    decimal decQuantity = objLocationDetails.decQuantity;
                    if (dtGetUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                            objLocationDetails.decQuantity = objLocationDetails.decQuantity / decConversionValue;
                        else if (intConversionFactor == 2)
                            objLocationDetails.decQuantity = objLocationDetails.decQuantity * decConversionValue;
                    }
                    UpdateItemLocationDetails(objLocationDetails, false);
                    objLocationDetails.decQuantity = decQuantity;

                MobjDataLayer.ExecuteNonQueryWithTran("spDirectDelivery", alParameters);
            }
            return true;
        }

        private bool UpdateItemLocationDetails(clsDTODirectDeliveryLocationDetails objLocationDetails, bool blnIsDeletion)
        {
            ArrayList alParameters = new ArrayList();
            if (!blnIsDeletion)
                alParameters.Add(new SqlParameter("@Mode", 2));
            else
                alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@Type", 1));
            alParameters.Add(new SqlParameter("@CompanyID", PobjDTODeliveryMaster.intCompanyID));
            alParameters.Add(new SqlParameter("@WarehouseID", PobjDTODeliveryMaster.intWarehouseID));
            alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.intItemID));
            alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.lngBatchID));
            alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.intLocationID));
            alParameters.Add(new SqlParameter("@RowID", objLocationDetails.intRowID));
            alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.intBlockID));
            alParameters.Add(new SqlParameter("@LotID", objLocationDetails.intLotID));
            alParameters.Add(new SqlParameter("@Quantity", objLocationDetails.decQuantity));
            MobjDataLayer.ExecuteNonQueryWithTran("spInvItemLocationTransfer", alParameters);
            return true;
        }

        public decimal GetItemLocationQuantity(clsDTODirectDeliveryLocationDetails objLocationDetails)
        {
            ArrayList alParameters = new ArrayList();
            decimal decQuantity = 0;
            alParameters.Add(new SqlParameter("@Mode", 13));
            alParameters.Add(new SqlParameter("@CompanyID", PobjDTODeliveryMaster.intCompanyID));
            alParameters.Add(new SqlParameter("@WarehouseID", PobjDTODeliveryMaster.intWarehouseID));
            alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.intItemID));
            alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.lngBatchID));
            alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.intLocationID));
            alParameters.Add(new SqlParameter("@RowID", objLocationDetails.intRowID));
            alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.intBlockID));
            alParameters.Add(new SqlParameter("@LotID", objLocationDetails.intLotID));
            DataTable datLocationDetails = MobjDataLayer.ExecuteDataTable("spInvItemLocationTransfer", alParameters);
            if (datLocationDetails.Rows.Count > 0)
            {
                if (datLocationDetails.Rows[0]["Quantity"] != DBNull.Value)
                    decQuantity = datLocationDetails.Rows[0]["Quantity"].ToDecimal();
            }
            return decQuantity;
        }

        public DataTable GetItemIssueLocationDetails()
        {
            ArrayList alParameters = new ArrayList();
                alParameters.Add(new SqlParameter("@Mode", 28));
                alParameters.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            return MobjDataLayer.ExecuteDataTable("spDirectDelivery", alParameters);
        }

        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 14));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@LocationWise", ClsCommonSettings.PblnIsLocationWise));
            return MobjDataLayer.ExecuteDataTable("spInvItemLocationTransfer", alParameters).Rows[0];
        }

        private bool DeleteItemIssueLocationDetails()
        {
                DataTable datItemIssueLocationDetails = GetItemIssueLocationDetails();
                foreach (DataRow dr in datItemIssueLocationDetails.Rows)
                {
                    clsDTODirectDeliveryLocationDetails objLocationDetails = new clsDTODirectDeliveryLocationDetails();
                    objLocationDetails.intItemID = dr["ItemID"].ToInt32();
                    objLocationDetails.lngBatchID = dr["BatchID"].ToInt64();
                    objLocationDetails.intLocationID = dr["LocationID"].ToInt32();
                    objLocationDetails.intRowID = dr["RowID"].ToInt32();
                    objLocationDetails.intBlockID = dr["BlockID"].ToInt32();
                    objLocationDetails.intLotID = dr["LotID"].ToInt32();
                    objLocationDetails.decQuantity = dr["Quantity"].ToDecimal();
                    objLocationDetails.intUOMID = dr["UomID"].ToInt32();
                    DataTable dtGetUomDetails = GetUomConversionValues(objLocationDetails.intUOMID, objLocationDetails.intItemID);
                    decimal decQuantity = objLocationDetails.decQuantity;
                    if (dtGetUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                            objLocationDetails.decQuantity = objLocationDetails.decQuantity / decConversionValue;
                        else if (intConversionFactor == 2)
                            objLocationDetails.decQuantity = objLocationDetails.decQuantity * decConversionValue;
                    }

                    UpdateItemLocationDetails(objLocationDetails, true);
                }
            ArrayList alParameters = new ArrayList();
                alParameters.Add(new SqlParameter("@Mode", 27));
                alParameters.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            MobjDataLayer.ExecuteNonQueryWithTran("spDirectDelivery", alParameters);
            return true;
        }
        public DataSet DisplayItemIssueLocationReport()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 29));
            prmReportDetails.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            return MobjDataLayer.ExecuteDataSet("spDirectDelivery", prmReportDetails);
        }


        public decimal GetBalanceAmount(long lngSalesInvoiceID)
        {
            prmDirectDelivery = new ArrayList();
            prmDirectDelivery.Add(new SqlParameter("@Mode", 31));
           // prmDirectDelivery.Add(new SqlParameter("@ReferenceID", lngSalesInvoiceID));
            return MobjDataLayer.ExecuteScalar("spDirectDelivery", prmDirectDelivery).ToDecimal();
        }
        private bool UpdateSalesOrderStatus(int StatusID, long SalesOrderID, int ItemID)
        {
            //This is to Update sales Order Detail status

            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 33));
            prmSalesStatus.Add(new SqlParameter("@DeliveredStatus", StatusID));
            prmSalesStatus.Add(new SqlParameter("@SalesOrderIDD",SalesOrderID));
            prmSalesStatus.Add(new SqlParameter("@ItemID",ItemID));
            int value = MobjDataLayer.ExecuteNonQuery("spDirectDelivery", prmSalesStatus);
            if (value > 0)
                return true;
            else
                return false;
        }

        private bool UpdateSalesOrderMasterStatus(int StatusID, long SalesOrderID)  // Update satus in SalesOrdermaster
        {
            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 34));
            prmSalesStatus.Add(new SqlParameter("@DeliveredStatus", StatusID));
            prmSalesStatus.Add(new SqlParameter("@SalesOrderIDD", SalesOrderID));
            int value = MobjDataLayer.ExecuteNonQuery("spDirectDelivery", prmSalesStatus);
            if (value > 0)
                return true;
            else
                return false;
        }
        public decimal SalesOrderQuantity(long SalesOrderID, int ItemID)
        {
            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 32));
            //prmSalesStatus.Add(new SqlParameter("@SalesOrderIDD",SalesOrderID));
            prmSalesStatus.Add(new SqlParameter("@ItemID", ItemID));
            decimal value = MobjDataLayer.ExecuteScalar("spDirectDelivery", prmSalesStatus).ToDecimal();
            return value;
        }


        private bool UpdateSalesInvoiceStatus(int StatusID, long SalesInvoiceID, int ItemID)
        {
            //This is to Update InvoiceOrder Detail status

            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 39));
            prmSalesStatus.Add(new SqlParameter("@DeliveredStatus", StatusID));
            prmSalesStatus.Add(new SqlParameter("@SalesInvoiceID", SalesInvoiceID));
            prmSalesStatus.Add(new SqlParameter("@ItemID", ItemID));
            int value = MobjDataLayer.ExecuteNonQuery("spDirectDelivery", prmSalesStatus);
            if (value > 0)
                return true;
            else
                return false;
        }
        private bool UpdateSalesInvoiceMasterStatus(int StatusID, long SalesInvoiceID)  // Update satus in SalesInvoicemaster
        {
            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 40));
            prmSalesStatus.Add(new SqlParameter("@DeliveredStatus", StatusID));
            prmSalesStatus.Add(new SqlParameter("@SalesInvoiceID", SalesInvoiceID));
            int value = MobjDataLayer.ExecuteNonQuery("spDirectDelivery", prmSalesStatus);
            if (value > 0)
                return true;
            else
                return false;
        }
        public decimal SalesInvoiceQuantity(long SalesInvoiceID, int ItemID)
        {
            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 38));
            prmSalesStatus.Add(new SqlParameter("@SalesInvoiceID", SalesInvoiceID));
            prmSalesStatus.Add(new SqlParameter("@ItemID", ItemID));
            decimal value = MobjDataLayer.ExecuteScalar("spDirectDelivery", prmSalesStatus).ToDecimal();
            return value;
        }
        public string GetVendorAddressInformation(int intVendorAdd)
        {   //To Get Vendor Address Information
            string strAddress = "";
            ArrayList prmVendor = new ArrayList();

            prmVendor.Add(new SqlParameter("@Mode", 7));
            prmVendor.Add(new SqlParameter("@VendorID", intVendorAdd));
            SqlDataReader drVendor = MobjDataLayer.ExecuteReader("spInvSalesQuotation", prmVendor);

            if (drVendor.Read())
            {
                strAddress = Environment.NewLine;
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["ContactPerson"])))
                {
                    strAddress += Convert.ToString(drVendor["ContactPerson"]);
                    strAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Address"])))
                {
                    strAddress += Convert.ToString(drVendor["Address"]);
                    strAddress += Environment.NewLine;
                }

                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["State"])))
                {
                    strAddress += Convert.ToString(drVendor["State"]);
                    strAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Country"])))
                {
                    strAddress += Convert.ToString(drVendor["Country"]);
                    strAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["ZipCode"])))
                {
                    strAddress += "ZipCode : " + Convert.ToString(drVendor["ZipCode"]);
                    strAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Telephone"])))
                {
                    strAddress += "Telephone : " + Convert.ToString(drVendor["Telephone"]);
                    strAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["MobileNo"])))
                {
                    strAddress += "MobileNo : " + Convert.ToString(drVendor["MobileNo"]);
                    strAddress += Environment.NewLine;
                }

                return strAddress;
            }
            return strAddress;
        }
        public DataTable DtGetAddressName()
        {
            DataTable datAddressName = new DataTable();

            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                objCommonUtility.GetRecordValue(new string[] { "VendorAddID,AddressName", "InvVendorAddress", "VendorID=" + PobjDTODeliveryMaster.intVendorID + "" }, out datAddressName);
                return datAddressName;
            }
        }
        public bool CheckSalesInvoiceExists()
        {
            bool blnRetValue = false;
            prmDirectDelivery = new ArrayList();
            prmDirectDelivery.Add(new SqlParameter("@Mode", "42"));
            prmDirectDelivery.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spDirectDelivery", prmDirectDelivery);
            if (sdr.Read())
            {
                if (Convert.ToInt32(sdr[0]) != 0)
                {
                    blnRetValue = true;
                }
                sdr.Close();
            }
            return blnRetValue;
        }
        public bool CheckSalesOrderExists()
        {
            bool blnRetValue = false;
            prmDirectDelivery = new ArrayList();
            prmDirectDelivery.Add(new SqlParameter("@Mode", "43"));
            prmDirectDelivery.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spDirectDelivery", prmDirectDelivery);
            if (sdr.Read())
            {
                if (Convert.ToInt32(sdr[0]) != 0)
                {
                    blnRetValue = true;
                }
                sdr.Close();
            }
            return blnRetValue;
        }
        public bool IsSalesInvoiceExists()
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 45));
            Parameters.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            object objReturnValue = MobjDataLayer.ExecuteScalar("spDirectDelivery", Parameters);
            return objReturnValue.ToInt32() > 0;
        }

        public bool IsSalesOrderExists()
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 44));
            Parameters.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            object objReturnValue = MobjDataLayer.ExecuteScalar("spDirectDelivery", Parameters);
            return objReturnValue.ToInt32() > 0;
        }
        public DataTable DisplayItemIssueMasterInfoForSales()
        {
            //Display PurchaseIndent Master Information
            prmDirectDelivery = new ArrayList();
            prmDirectDelivery.Add(new SqlParameter("@Mode", 1));
            prmDirectDelivery.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            //DataTable datItemIssue = MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmDirectDelivery);
            return MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmDirectDelivery);
        }
        public bool GetExchangeCurrencyRate(int intCompanyID, int intCurrencyID)
        {
            ArrayList prmExchangeCurrencyRate = new ArrayList();
            prmExchangeCurrencyRate.Add(new SqlParameter("@Mode", 19));
            prmExchangeCurrencyRate.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmExchangeCurrencyRate.Add(new SqlParameter("@CurrencyID", intCurrencyID));

            DataTable datExchangeCurrencyRate = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmExchangeCurrencyRate);

            if (datExchangeCurrencyRate.Rows.Count > 0)
            {
                if (datExchangeCurrencyRate.Rows[0]["Exchangerate"] != DBNull.Value)
                 PobjDTODeliveryMaster.decExchangeCurrencyRate = Convert.ToDecimal(datExchangeCurrencyRate.Rows[0]["Exchangerate"]);

                if (PobjDTODeliveryMaster.decExchangeCurrencyRate >= 0)
                    return true;
            }

            return false;

        }
        public DataTable BatchLessItems(int intItemID, int intWareHouseID)
        {
            prmDirectDelivery = new ArrayList();
            prmDirectDelivery.Add(new SqlParameter("@Mode", 46));
            prmDirectDelivery.Add(new SqlParameter("@ItemID", intItemID));

            prmDirectDelivery.Add(new SqlParameter("@WarehouseID", intWareHouseID));

            return MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmDirectDelivery);
        }
        public DataTable GetGroupDetails(int intItemID)
        {
            prmDirectDelivery = new ArrayList();
            prmDirectDelivery.Add(new SqlParameter("@Mode", 47));
            prmDirectDelivery.Add(new SqlParameter("@ItemID", intItemID));

      

            return MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmDirectDelivery);
        }
        public decimal GetStockQuantityForBatchlessItems(int intItemID, int intWarehouseID)
        {
            ArrayList prmStockDetailsBatchless = new ArrayList();
            prmStockDetailsBatchless.Add(new SqlParameter("@Mode",48));
            prmStockDetailsBatchless.Add(new SqlParameter("@ItemID", intItemID));
         
            prmStockDetailsBatchless.Add(new SqlParameter("@WarehouseID", intWarehouseID));

            DataTable datStockDetailsB = MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmStockDetailsBatchless);
            decimal decQuantity = 0;
            if (datStockDetailsB.Rows.Count > 0)
            {
                if (datStockDetailsB.Rows[0]["AvailableQty"] != DBNull.Value)
                {
                    decQuantity = Convert.ToDecimal(datStockDetailsB.Rows[0]["AvailableQty"]);
                }
            }
            return decQuantity;
        }
        public DataTable GetItemIssueDetailsForDeletion()
        {
            
            prmDirectDeliveryDetails = new ArrayList();
            prmDirectDeliveryDetails.Add(new SqlParameter("@Mode", 49));
            prmDirectDeliveryDetails.Add(new SqlParameter("@ItemIssueID", PobjDTODeliveryMaster.intDirectDeliveryID));
            return MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmDirectDeliveryDetails);
        }
        public decimal GetStockQuantityForGroup(int intItemGroupID,int intWareHouseID)
        {
            ArrayList prmStockDetailsGrp = new ArrayList();
            prmStockDetailsGrp.Add(new SqlParameter("@Mode", 50));
            prmStockDetailsGrp.Add(new SqlParameter("@ItemGroupID", intItemGroupID));

            prmStockDetailsGrp.Add(new SqlParameter("@WarehouseID", intWareHouseID));

            DataTable datStockDetailsB = MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmStockDetailsGrp);
            decimal decQuantity = 0;
            if (datStockDetailsB.Rows.Count > 0)
            {
                if (datStockDetailsB.Rows[0]["AvailableQtyGrp"] != DBNull.Value)
                {
                    decQuantity = Convert.ToDecimal(datStockDetailsB.Rows[0]["AvailableQtyGrp"]);
                }
            }
            return decQuantity;
        }
        public DataTable BatchLessItemsEditMode(int intItemID, int intItemIssueID)
        {
            prmDirectDelivery = new ArrayList();
            prmDirectDelivery.Add(new SqlParameter("@Mode", 51));
            prmDirectDelivery.Add(new SqlParameter("@ItemID", intItemID));

            prmDirectDelivery.Add(new SqlParameter("@ItemIssueID", intItemIssueID));

            return MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmDirectDelivery);
        }
        public DataTable GetQtyInGroup(Int64 IntItemID)
        {
            ArrayList prmDelivery;
            prmDelivery = new ArrayList();

            prmDelivery.Add(new SqlParameter("@Mode", 52));
            prmDelivery.Add(new SqlParameter("@ItemID", IntItemID));
            return MobjDataLayer.ExecuteDataTable("spDirectDelivery", prmDelivery);
        }
    }

}
