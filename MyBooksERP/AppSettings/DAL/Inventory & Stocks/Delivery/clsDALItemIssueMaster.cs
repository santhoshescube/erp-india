﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using System.Data;
using System.Data.SqlClient;
/* 
=================================================
   Author:		<Author,,Midhun>--
   Create date: <Create Date,,22 Mar 2011>
   Description:	<Description,,Delivery Scheduling DAL>
================================================
*/
namespace MyBooksERP
{
    public class clsDALItemIssueMaster
    {
        ArrayList prmItemIssueMaster, prmItemIssueDetails;               // array list for storing parameters
        public clsDTOItemIssueMaster PobjDTOItemIssueMaster { get; set; }  // property of clsDTOItemIssueMaster
        public DataLayer MobjDataLayer;                                  // obj of datalayer 
        bool blnAddStatus = false, blnUpdateStatus = false, blnDeleteStatus = false;
        bool blnUpdateMode = false;
        DataSet dtsTemp11 = new DataSet();
        DataSet dtsTemp22 = new DataSet();
        bool blnSalesOrderMasterStatus = true;
        public clsDALItemIssueMaster(DataLayer objDataLayer)
        {
            //constructor
            MobjDataLayer = objDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = MobjDataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool SaveItemIssueMaster()
        {
            // Save Function 

            prmItemIssueMaster = new ArrayList();

            if (PobjDTOItemIssueMaster.intItemIssueID == 0)
            {
                prmItemIssueMaster.Add(new SqlParameter("@Mode", 2));

                blnAddStatus = true;
                blnUpdateStatus = false;
                blnDeleteStatus = false;
            }
            else
            {
                prmItemIssueMaster.Add(new SqlParameter("@Mode", 3));
                blnAddStatus = false;
                blnUpdateStatus = false;
                blnDeleteStatus = true;
                blnUpdateMode = true;
                //    DeleteItemGroupIssueDetails();

                blnAddStatus = false;
                blnUpdateStatus = true;
                blnDeleteStatus = false;
                //   updateSaleOpStockAccountDetailsEdit(PobjDTOItemIssueMaster.intReferenceID);
            }
            prmItemIssueMaster.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
            prmItemIssueMaster.Add(new SqlParameter("@ItemIssueNo", PobjDTOItemIssueMaster.strItemIssueNo));
            prmItemIssueMaster.Add(new SqlParameter("@OrderTypeID", PobjDTOItemIssueMaster.intOperationTypeID));
            prmItemIssueMaster.Add(new SqlParameter("@ReferenceID", PobjDTOItemIssueMaster.intReferenceID));

            prmItemIssueMaster.Add(new SqlParameter("@WarehouseID", PobjDTOItemIssueMaster.intWarehouseID));
            prmItemIssueMaster.Add(new SqlParameter("@DeliveryDate", PobjDTOItemIssueMaster.strDeliveryDate));
            prmItemIssueMaster.Add(new SqlParameter("@IssuedBy", PobjDTOItemIssueMaster.intIssuedBy));
            prmItemIssueMaster.Add(new SqlParameter("@IssuedDate", PobjDTOItemIssueMaster.strIssuedDate));
            prmItemIssueMaster.Add(new SqlParameter("@CompanyID", PobjDTOItemIssueMaster.intCompanyID));
            prmItemIssueMaster.Add(new SqlParameter("@Remarks", PobjDTOItemIssueMaster.strRemarks));
            prmItemIssueMaster.Add(new SqlParameter("@OperationTypeID", (int)OperationType.DeliveryNote));
            prmItemIssueMaster.Add(new SqlParameter("@LPONumber", PobjDTOItemIssueMaster.Lpono));
            prmItemIssueMaster.Add(new SqlParameter("@VendorID", PobjDTOItemIssueMaster.intVendorID));
            prmItemIssueMaster.Add(new SqlParameter("@VendorAddID", PobjDTOItemIssueMaster.intVendorAddID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmItemIssueMaster.Add(objParam);
            object objItemIssueID = 0;

            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvItemIssue", prmItemIssueMaster, out objItemIssueID) != 0)
            {
                if (Convert.ToInt64(objItemIssueID) > 0)
                {
                    if (PobjDTOItemIssueMaster.intItemIssueID != 0)
                    {
                        DeleteItemIssueDetails();
                        DeleteItemIssueReferenceDetails();
                        //if (PobjDTOItemIssueMaster.intOperationTypeID != (int)OperationOrderType.DNOTSalesInvoice)
                        //DeleteItemIssueLocationDetails();
                    }
                    SaveItemIssueReferenceDetails(Convert.ToInt64(objItemIssueID), PobjDTOItemIssueMaster.intReferenceID.ToInt64());

                    PobjDTOItemIssueMaster.intItemIssueID = Convert.ToInt64(objItemIssueID);
                    if (SaveItemIssueDetails())
                    {
                        //if (PobjDTOItemIssueMaster.intOperationTypeID != (int)OperationOrderType.DNOTSalesInvoice)
                        //SaveItemIssueLocationDetails();
                        //if (PobjDTOItemIssueMaster.intOperationTypeID == (int)OperationOrderType.DNOTSalesOrder)
                        //    PostAccounts(Mode);

                        //   UpdateSaleOpStockAccountDetails(PobjDTOItemIssueMaster.intReferenceID);
                        if (PobjDTOItemIssueMaster.intOperationTypeID == (int)OperationOrderType.DNOTSalesInvoice)
                        {
                            UpdateSalesInvoiceMastersStatus(PobjDTOItemIssueMaster.intReferenceID);
                        }
                        else if (PobjDTOItemIssueMaster.intOperationTypeID == (int)OperationOrderType.DNOTSalesOrder)
                        {
                            UpdateSalesOrderMastersStatus(PobjDTOItemIssueMaster.intReferenceID);
                        }
                        return true;
                    }
                }
            }
            return false;
        }

        public bool DeleteItemIssue()
        {
            //Delete Function     
         
           DeleteItemGroupIssueDetails();
           DeleteItemIssueDetails();
            //if (PobjDTOItemIssueMaster.intOperationTypeID == (int)OperationOrderType.DNOTSalesOrder)
            //    PostAccounts(37); // post account for 

           
            DeleteItemIssueReferenceDetails();
            if (PobjDTOItemIssueMaster.intOperationTypeID == (Int32)OperationOrderType.DNOTSalesOrder)
            {
                UpdateSalesOrderMastersStatus(PobjDTOItemIssueMaster.intReferenceID);
            }
            else if (PobjDTOItemIssueMaster.intOperationTypeID == (Int32)OperationOrderType.DNOTSalesInvoice)
            {
                UpdateSalesInvoiceMastersStatus(PobjDTOItemIssueMaster.intReferenceID);
            }
            //no location 
            //if (PobjDTOItemIssueMaster.intOperationTypeID != (int)OperationOrderType.DNOTSalesInvoice)
            //DeleteItemIssueLocationDetails();
            blnAddStatus = false;
            blnUpdateStatus = false;
            blnDeleteStatus = true;
            //  UpdateSaleOpStockAccountDetails(PobjDTOItemIssueMaster.intReferenceID);

            prmItemIssueMaster = new ArrayList();
            prmItemIssueMaster.Add(new SqlParameter("@Mode", 4));
            prmItemIssueMaster.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
            prmItemIssueMaster.Add(new SqlParameter("@OrderTypeID", PobjDTOItemIssueMaster.intOperationTypeID));
            prmItemIssueMaster.Add(new SqlParameter("@ReferenceID", PobjDTOItemIssueMaster.intReferenceID));
            prmItemIssueMaster.Add(new SqlParameter("@SIOperationTypeID", (int)OperationType.SalesInvoice));

            if (PobjDTOItemIssueMaster.intOperationTypeID == (int)OperationOrderType.DNOTTransfer)
            {
                if (ClsCommonSettings.StockTransferApproval)
                    prmItemIssueMaster.Add(new SqlParameter("@StatusID", (int)OperationStatusType.STApproved));
                else
                    prmItemIssueMaster.Add(new SqlParameter("@StatusID", (int)OperationStatusType.STSubmitted));
            }

            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvItemIssue", prmItemIssueMaster) != 0)
            {
                return true;
            }
            return false;
        }

        private bool SaveItemIssueDetails()
        {
            //Save Item Issue Details
            int intSerialNo = 0;
            bool blnsalesOrderStatus = true;
            bool salesInvoiceStatus = true;
            foreach (clsDTOItemIssueDetails objDTOItemIssueDetails in PobjDTOItemIssueMaster.lstItemIssueDetails)
            {
                prmItemIssueDetails = new ArrayList();
                prmItemIssueDetails.Add(new SqlParameter("@Mode", 8));
                prmItemIssueDetails.Add(new SqlParameter("@SerialNo", ++intSerialNo));
                prmItemIssueDetails.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
                prmItemIssueDetails.Add(new SqlParameter("@ReferenceSerialNo", objDTOItemIssueDetails.intReferenceSerialNo));
                prmItemIssueDetails.Add(new SqlParameter("@ItemID", objDTOItemIssueDetails.intItemID));
                prmItemIssueDetails.Add(new SqlParameter("@IsGroup", objDTOItemIssueDetails.blnIsGroup));
                prmItemIssueDetails.Add(new SqlParameter("@BatchID", objDTOItemIssueDetails.intBatchID));
                prmItemIssueDetails.Add(new SqlParameter("@UOMID", objDTOItemIssueDetails.intUOMID));
                prmItemIssueDetails.Add(new SqlParameter("@Quantity", objDTOItemIssueDetails.decQuantity));
                prmItemIssueDetails.Add(new SqlParameter("@CompanyID", PobjDTOItemIssueMaster.intCompanyID));
                prmItemIssueDetails.Add(new SqlParameter("@WarehouseID", PobjDTOItemIssueMaster.intWarehouseID));
                prmItemIssueDetails.Add(new SqlParameter("@IssuedDate", PobjDTOItemIssueMaster.strIssuedDate));
                prmItemIssueDetails.Add(new SqlParameter("@PackingUnit", objDTOItemIssueDetails.strPackingUnit));

                if (objDTOItemIssueDetails.blnIsGroup)
                {
                    objDTOItemIssueDetails.intSerialNo = intSerialNo;
                    SaveItemGroupIssueDetails(objDTOItemIssueDetails);
                }
                MobjDataLayer.ExecuteNonQueryWithTran("spInvItemIssue", prmItemIssueDetails);
                //if (PobjDTOItemIssueMaster.intOperationTypeID == (int)OperationOrderType.DNOTSalesInvoice && GetJobOrderIDAgainstInvoice(PobjDTOItemIssueMaster.intReferenceID) != 0)
                if (!objDTOItemIssueDetails.blnIsGroup)
                {
                    UpdateStockDetails(objDTOItemIssueDetails, false);
                    UpdateStockMaster(objDTOItemIssueDetails, false);
                }
                if (PobjDTOItemIssueMaster.intOperationTypeID == (Int32)OperationOrderType.DNOTTransfer)//for stock transfer
                {
                    if (IsStockValueUpdationAllowed(Convert.ToInt64(PobjDTOItemIssueMaster.intItemIssueID)))
                    {
                        UpdateStockValue();
                    }
                }
               // UpdateStockValue();//exception
               if (PobjDTOItemIssueMaster.intOperationTypeID == (Int32)OperationOrderType.DNOTSalesInvoice)
                {
                    UpdateSalesInvoiceDetailsStatus(objDTOItemIssueDetails);
                }
               else if (PobjDTOItemIssueMaster.intOperationTypeID == (Int32)OperationOrderType.DNOTSalesOrder)
               {
                   UpdateSalesOrderDetailsStatus(objDTOItemIssueDetails);
               }
                

            }


            return true;
        }

        private void DeleteItemIssueReferenceDetails()
        {
            ArrayList prmItemIssueRefDetails = new ArrayList();
            prmItemIssueRefDetails.Add(new SqlParameter("@Mode", 44));
            prmItemIssueRefDetails.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
            MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmItemIssueRefDetails);
        }

        public bool DeleteItemIssueDetails()
        {
            //Delete ItemIssueDetails by ItemIssueID
            DataTable dtItemIssueDetails = GetItemIssueDetailsForDeletion();
            foreach (DataRow dr in dtItemIssueDetails.Rows)
            {
                clsDTOItemIssueDetails objItemIssueDetails = new clsDTOItemIssueDetails();
                objItemIssueDetails.decQuantity = Convert.ToDecimal(dr["Quantity"]);
                objItemIssueDetails.intBatchID = Convert.ToInt64(dr["BatchID"]);
                objItemIssueDetails.intItemID = Convert.ToInt32(dr["ItemID"]);
                objItemIssueDetails.intUOMID = Convert.ToInt32(dr["UomID"]);
                objItemIssueDetails.blnIsGroup = Convert.ToBoolean(dr["IsGroup"]);
                //objItemIssueDetails.decQty = Convert.ToInt32(dr["DeliveredQty"]);

                if (!objItemIssueDetails.blnIsGroup)
                {
                    UpdateStockDetails(objItemIssueDetails, true);
                    UpdateStockMaster(objItemIssueDetails, true);
                }
                if (PobjDTOItemIssueMaster.intOperationTypeID == (Int32)OperationOrderType.DNOTSalesInvoice)
                {
                    UpdateSalesInvoiceDetailsStatusOnDelete(objItemIssueDetails);
                }
                else if (PobjDTOItemIssueMaster.intOperationTypeID == (Int32)OperationOrderType.DNOTSalesOrder)
                {
                    UpdateSalesOrderDetailsStatusOnDelete(objItemIssueDetails);
                   // UpdateSIMastersStatusOnDNdelete(objItemIssueDetails);
                }
               

            }
            prmItemIssueDetails = new ArrayList();
            prmItemIssueDetails = new ArrayList();
            prmItemIssueDetails.Add(new SqlParameter("@Mode", 9));
            prmItemIssueDetails.Add(new SqlParameter("@OperationTypeID", (int)OperationType.DeliveryNote));
            prmItemIssueDetails.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));



            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvItemIssue", prmItemIssueDetails) != 0)
                
                return true;
            return false;
        }



        public bool DisplayItemIssueMasterInfo()
        {
            //Display PurchaseIndent Master Information
            prmItemIssueMaster = new ArrayList();
            prmItemIssueMaster.Add(new SqlParameter("@Mode", 1));
            prmItemIssueMaster.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
            DataTable datItemIssue = MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmItemIssueMaster);
            if (datItemIssue.Rows.Count > 0)
            {
                PobjDTOItemIssueMaster.intItemIssueID = Convert.ToInt64(datItemIssue.Rows[0]["ItemIssueID"]);
                PobjDTOItemIssueMaster.strItemIssueNo = Convert.ToString(datItemIssue.Rows[0]["ItemIssueNo"]);
                PobjDTOItemIssueMaster.intOperationTypeID = Convert.ToInt32(datItemIssue.Rows[0]["OrderTypeID"]);
                if (datItemIssue.Rows[0]["ReferenceID"] != DBNull.Value)
                    PobjDTOItemIssueMaster.intReferenceID = Convert.ToInt64(datItemIssue.Rows[0]["ReferenceID"]);
                PobjDTOItemIssueMaster.intWarehouseID = Convert.ToInt32(datItemIssue.Rows[0]["WarehouseID"]);
                PobjDTOItemIssueMaster.intIssuedBy = Convert.ToInt32(datItemIssue.Rows[0]["IssuedBy"]);
                PobjDTOItemIssueMaster.strIssuedDate = Convert.ToString(datItemIssue.Rows[0]["IssuedDate"]);
                PobjDTOItemIssueMaster.intCompanyID = Convert.ToInt32(datItemIssue.Rows[0]["CompanyID"]);
                PobjDTOItemIssueMaster.strIssuedBy = datItemIssue.Rows[0]["Employee"].ToString();
                if (datItemIssue.Rows[0]["Remarks"] != DBNull.Value)
                    PobjDTOItemIssueMaster.strRemarks = Convert.ToString(datItemIssue.Rows[0]["Remarks"]);
                if (datItemIssue.Rows[0]["LPONumber"] != DBNull.Value)
                    PobjDTOItemIssueMaster.Lpono = Convert.ToString(datItemIssue.Rows[0]["LPONumber"]);
                else PobjDTOItemIssueMaster.Lpono = "";
                return true;
            }
            return false;
        }

        public DataTable GetItemIssueDetails()
        {
            //function for getting delivery schedule details find by scheduleID
            prmItemIssueDetails = new ArrayList();
            prmItemIssueDetails.Add(new SqlParameter("@Mode", 7));
            prmItemIssueDetails.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
            return MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmItemIssueDetails);
        }

        public int GetNextIssueNo(int intCompanyID, int intFormType)
        {
            //Function for getting Next Issue No
            prmItemIssueMaster = new ArrayList();
            prmItemIssueMaster.Add(new SqlParameter("@Mode", 5));
            prmItemIssueMaster.Add(new SqlParameter("@CompanyID", intCompanyID));


            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spInvItemIssue", prmItemIssueMaster);
            int intNextSerialNo = 0;

            if (sdr.Read())
            {
                if (sdr["MaxNo"] != DBNull.Value)
                    intNextSerialNo = Convert.ToInt32(sdr["MaxNo"]);
            }
            sdr.Close();
            return ++intNextSerialNo;
        }

        public DataTable GetIssueNos(string strCondition)
        {
            // Get All Issue Nos By Filter Condition
            prmItemIssueDetails = new ArrayList();
            prmItemIssueDetails.Add(new SqlParameter("@Mode", 6));
            prmItemIssueDetails.Add(new SqlParameter("@Condition", strCondition));
            return MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmItemIssueDetails);
        }

        public string FindEmployeeNameByUserID(int intUserID)
        {
            string strEmployeeName = string.Empty;
            prmItemIssueMaster = new ArrayList();
            prmItemIssueMaster.Add(new SqlParameter("@Mode", 10));
            prmItemIssueMaster.Add(new SqlParameter("@UserID", intUserID));
            SqlDataReader sdr;
            sdr = MobjDataLayer.ExecuteReader("STUserInformation", prmItemIssueMaster);
            if (sdr.Read())
            {
                if (sdr["FirstName"] != DBNull.Value)
                {
                    strEmployeeName = Convert.ToString(sdr["FirstName"]);
                }
            }
            sdr.Close();
            return strEmployeeName;
        }

        public DataTable GetDataForItemSelection(int intOperationType, Int64 intReferenceID, int intWarehouseID, int intCompanyID)
        {
            prmItemIssueDetails = new ArrayList();

            prmItemIssueDetails.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            prmItemIssueDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            if (intOperationType != 0)
            {
                prmItemIssueDetails.Add(new SqlParameter("@Mode", 46));
                prmItemIssueDetails.Add(new SqlParameter("@OrderTypeID", intOperationType));
                prmItemIssueDetails.Add(new SqlParameter("@ReferenceID", intReferenceID));
            }
            return MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmItemIssueDetails);

            //else
            //{
            //    prmItemIssueDetails.Add(new SqlParameter("@Mode", 11));
            //    return MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmItemIssueDetails);
            //}
        }

        public DataTable GetItemDetails(int intOperationTypeID, Int64 intReferenceID)
        {
            prmItemIssueDetails = new ArrayList();
            prmItemIssueDetails.Add(new SqlParameter("@Mode", 16));
            prmItemIssueDetails.Add(new SqlParameter("@OrderTypeID", intOperationTypeID));
            prmItemIssueDetails.Add(new SqlParameter("@ReferenceID", intReferenceID));
            return MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmItemIssueDetails);
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            prmItemIssueMaster = new ArrayList();
            prmItemIssueMaster.Add(new SqlParameter("@Mode", 10));
            prmItemIssueMaster.Add(new SqlParameter("@ItemID", intItemID));
            //  prmItemIssueMaster.Add(new SqlParameter("@UomTypeID", 1));
            DataTable dtItemUoms = new DataTable();
            dtItemUoms = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmItemIssueMaster);

            prmItemIssueMaster = new ArrayList();
            prmItemIssueMaster.Add(new SqlParameter("@Mode", 12));
            prmItemIssueMaster.Add(new SqlParameter("@ItemID", intItemID));
            DataTable dtItemBaseUnit = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmItemIssueMaster);
            DataRow dr = dtItemUoms.NewRow();
            dr["UOMID"] = dtItemBaseUnit.Rows[0]["UOMID"];
            dr["Description"] = dtItemBaseUnit.Rows[0]["Description"];
            dr["ShortName"] = dtItemBaseUnit.Rows[0]["ShortName"];
            dr["ItemID"] = intItemID;
            dtItemUoms.Rows.InsertAt(dr, 0);
            return dtItemUoms;
        }

        public decimal GetItemLocationQty(clsDTOItemLocationDetails objItemLocationDetails, int intWarehouseID, bool blnIsSum)
        {
            ArrayList prmItemLocation = new ArrayList();
            prmItemLocation.Add(new SqlParameter("@Mode", 15));
            prmItemLocation.Add(new SqlParameter("@ItemID", objItemLocationDetails.intItemID));
            prmItemLocation.Add(new SqlParameter("@BatchID", objItemLocationDetails.intBatchID));
            prmItemLocation.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            prmItemLocation.Add(new SqlParameter("@LocationID", objItemLocationDetails.intLocationID));
            prmItemLocation.Add(new SqlParameter("@RowID", objItemLocationDetails.intRowID));
            prmItemLocation.Add(new SqlParameter("@BlockID", objItemLocationDetails.intBlockID));
            prmItemLocation.Add(new SqlParameter("@LotID", objItemLocationDetails.intLotID));
            DataTable datItemLocationDetails = MobjDataLayer.ExecuteDataTable("STItemLocationDetail", prmItemLocation);
            decimal decQuantity = 0;
            if (datItemLocationDetails.Rows.Count > 0)
            {
                if (datItemLocationDetails.Rows[0]["Quantity"] != DBNull.Value)
                {
                    decQuantity = Convert.ToDecimal(datItemLocationDetails.Rows[0]["Quantity"]);
                }
            }
            return decQuantity;
        }

        public decimal GetStockQuantity(int intItemID, Int64 intBatchID, int intCompanyID, int intWarehouseID)
        {
            ArrayList prmStockDetails = new ArrayList();
            prmStockDetails.Add(new SqlParameter("@Mode", 63));
            prmStockDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmStockDetails.Add(new SqlParameter("@BatchID", intBatchID));
            prmStockDetails.Add(new SqlParameter("@WarehouseID", intWarehouseID));
         
            prmStockDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            DataTable datStockDetails = MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmStockDetails);
            decimal decQuantity = 0;
            if (datStockDetails.Rows.Count > 0)
            {
                if (datStockDetails.Rows[0]["AvailableQty"] != DBNull.Value)
                {
                    decQuantity = Convert.ToDecimal(datStockDetails.Rows[0]["AvailableQty"]);
                }
            }
            return decQuantity;
        }

        public decimal GetAvailableQtyAgainstJobOrder(long lngJobOrderID, long lngJobOrderProductID, int intWarehouseID)
        {

            ArrayList prmStockDetails = new ArrayList();
            prmStockDetails.Add(new SqlParameter("@Mode", 11));
            prmStockDetails.Add(new SqlParameter("@JobOrderProductID", lngJobOrderProductID));
            prmStockDetails.Add(new SqlParameter("@JobOrderID", lngJobOrderID));
            prmStockDetails.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            DataTable datStockDetails = MobjDataLayer.ExecuteDataTable("spInvStockMaster", prmStockDetails);
            decimal decQuantity = 0;
            if (datStockDetails.Rows.Count > 0)
            {
                if (datStockDetails.Rows[0]["AvailableQty"] != DBNull.Value)
                {
                    decQuantity = Convert.ToDecimal(datStockDetails.Rows[0]["AvailableQty"]);
                }
            }
            return decQuantity;
        }


        private void UpdateStockDetails(clsDTOItemIssueDetails objItemIssueDetails, bool blnIsDelete)
        {
            decimal decProducedQty = 0, decReceivedFromTransfer = 0, decDamagedQty = 0, decGRNQty = 0, decDemoQty = 0, decInvoicedQty = 0, decOpeningStock = 0, decSoldQuantity = 0, decTransferedQty = 0;
            DataTable datStockdetails = new DataTable();
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 4));
            alParameters.Add(new SqlParameter("@ItemID", objItemIssueDetails.intItemID));
            alParameters.Add(new SqlParameter("@BatchID", objItemIssueDetails.intBatchID));
            alParameters.Add(new SqlParameter("@WarehouseID", PobjDTOItemIssueMaster.intWarehouseID));
            alParameters.Add(new SqlParameter("@CompanyID", PobjDTOItemIssueMaster.intCompanyID));
            datStockdetails = MobjDataLayer.ExecuteDataTable("spInvStockMaster", alParameters);
            if (datStockdetails.Rows.Count > 0)
            {
                decReceivedFromTransfer = datStockdetails.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                decOpeningStock = datStockdetails.Rows[0]["OpeningStock"].ToDecimal();
                decGRNQty = datStockdetails.Rows[0]["GRNQuantity"].ToDecimal();
                decDamagedQty = datStockdetails.Rows[0]["DamagedQuantity"].ToDecimal();
                decInvoicedQty = datStockdetails.Rows[0]["InvoicedQuantity"].ToDecimal();
                decDemoQty = datStockdetails.Rows[0]["DemoQuantity"].ToDecimal();
                decSoldQuantity = datStockdetails.Rows[0]["SoldQuantity"].ToDecimal();
                decTransferedQty = datStockdetails.Rows[0]["TransferedQuantity"].ToDecimal();
                decProducedQty = datStockdetails.Rows[0]["ProducedQuantity"].ToDecimal();
            }

            ArrayList prmStockDetails = new ArrayList();
            prmStockDetails.Add(new SqlParameter("@Mode", 6));
            prmStockDetails.Add(new SqlParameter("@ItemID", objItemIssueDetails.intItemID));
            prmStockDetails.Add(new SqlParameter("@BatchID", objItemIssueDetails.intBatchID));
            prmStockDetails.Add(new SqlParameter("@WarehouseID", PobjDTOItemIssueMaster.intWarehouseID));
            prmStockDetails.Add(new SqlParameter("@CompanyID", PobjDTOItemIssueMaster.intCompanyID));

            DataTable dtUomDetails = new DataTable();
            decimal decIssueQuantity = 0;
            decIssueQuantity = objItemIssueDetails.decQuantity;
            dtUomDetails = GetUomConversionValues(objItemIssueDetails.intUOMID, objItemIssueDetails.intItemID);
            if (dtUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                    decIssueQuantity = decIssueQuantity / decConversionValue;
                else if (intConversionFactor == 2)
                    decIssueQuantity = decIssueQuantity * decConversionValue;
            }
            if (PobjDTOItemIssueMaster.intOperationTypeID == (int)OperationOrderType.DNOTTransfer)
            {
                if (!blnIsDelete)
                    prmStockDetails.Add(new SqlParameter("@TransferedQuantity", decTransferedQty + decIssueQuantity));
                else
                    prmStockDetails.Add(new SqlParameter("@TransferedQuantity", decTransferedQty - decIssueQuantity));
                prmStockDetails.Add(new SqlParameter("@SoldQuantity", decSoldQuantity));

            }
            else
            {
                if (!blnIsDelete)
                    prmStockDetails.Add(new SqlParameter("@SoldQuantity", decSoldQuantity + decIssueQuantity));
                else
                    prmStockDetails.Add(new SqlParameter("@SoldQuantity", decSoldQuantity - decIssueQuantity));
                prmStockDetails.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));
            }

            prmStockDetails.Add(new SqlParameter("@DamagedQuantity", decDamagedQty));
            prmStockDetails.Add(new SqlParameter("@GRNQuantity", decGRNQty));
            prmStockDetails.Add(new SqlParameter("@InvoicedQuantity", decInvoicedQty));
            prmStockDetails.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
            prmStockDetails.Add(new SqlParameter("@OpeningStock", decOpeningStock));
            prmStockDetails.Add(new SqlParameter("@DemoQuantity", decDemoQty));
            prmStockDetails.Add(new SqlParameter("@ProducedQuantity", decProducedQty));

            MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmStockDetails);
        }

        private void UpdateStockMaster(clsDTOItemIssueDetails objItemIssueDetails, bool blnIsDelete)
        {
            decimal decProducedQty = 0, decReceivedFromTransfer = 0, decDamagedQty = 0, decGRNQty = 0, decDemoQty = 0, decInvoicedQty = 0, decOpeningStock = 0, decSoldQuantity = 0, decTransferedQty = 0;
            DataTable datStockdetails = new DataTable();
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@ItemID", objItemIssueDetails.intItemID));
            datStockdetails = MobjDataLayer.ExecuteDataTable("spInvStockMaster", alParameters);
            if (datStockdetails.Rows.Count > 0)
            {
                decReceivedFromTransfer = datStockdetails.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                decOpeningStock = datStockdetails.Rows[0]["OpeningStock"].ToDecimal();
                decGRNQty = datStockdetails.Rows[0]["GRNQuantity"].ToDecimal();
                decDamagedQty = datStockdetails.Rows[0]["DamagedQuantity"].ToDecimal();
                decInvoicedQty = datStockdetails.Rows[0]["InvoicedQuantity"].ToDecimal();
                decDemoQty = datStockdetails.Rows[0]["DemoQuantity"].ToDecimal();
                decSoldQuantity = datStockdetails.Rows[0]["SoldQuantity"].ToDecimal();
                decTransferedQty = datStockdetails.Rows[0]["TransferedQuantity"].ToDecimal();
                decProducedQty = datStockdetails.Rows[0]["ProducedQuantity"].ToDecimal();
            }

            ArrayList prmStockDetails = new ArrayList();
            prmStockDetails.Add(new SqlParameter("@Mode", 3));
            prmStockDetails.Add(new SqlParameter("@ItemID", objItemIssueDetails.intItemID));
            DataTable dtUomDetails = new DataTable();
            decimal decIssueQuantity = 0;
            decIssueQuantity = objItemIssueDetails.decQuantity;
            dtUomDetails = GetUomConversionValues(objItemIssueDetails.intUOMID, objItemIssueDetails.intItemID);
            if (dtUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                    decIssueQuantity = decIssueQuantity / decConversionValue;
                else if (intConversionFactor == 2)
                    decIssueQuantity = decIssueQuantity * decConversionValue;
            }
            if (PobjDTOItemIssueMaster.intOperationTypeID == (int)OperationOrderType.DNOTTransfer)
            {
                if (!blnIsDelete)
                    prmStockDetails.Add(new SqlParameter("@TransferedQuantity", decTransferedQty + decIssueQuantity));
                else
                    prmStockDetails.Add(new SqlParameter("@TransferedQuantity", decTransferedQty - decIssueQuantity));

                prmStockDetails.Add(new SqlParameter("@SoldQuantity", decSoldQuantity));
            }
            else
            {
                if (!blnIsDelete)
                    prmStockDetails.Add(new SqlParameter("@SoldQuantity", decSoldQuantity + decIssueQuantity));
                else
                    prmStockDetails.Add(new SqlParameter("@SoldQuantity", decSoldQuantity - decIssueQuantity));
                prmStockDetails.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));

            }

            prmStockDetails.Add(new SqlParameter("@DamagedQuantity", decDamagedQty));
            prmStockDetails.Add(new SqlParameter("@GRNQuantity", decGRNQty));
            prmStockDetails.Add(new SqlParameter("@InvoicedQuantity", decInvoicedQty));
            prmStockDetails.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
            prmStockDetails.Add(new SqlParameter("@OpeningStock", decOpeningStock));
            prmStockDetails.Add(new SqlParameter("@DemoQuantity", decDemoQty));
            prmStockDetails.Add(new SqlParameter("@ProducedQuantity", decProducedQty));
            prmStockDetails.Add(new SqlParameter("@LastModifiedBy", PobjDTOItemIssueMaster.intIssuedBy));
            prmStockDetails.Add(new SqlParameter("@LastModifiedDate", ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")));
            MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmStockDetails);
        }

        public DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {
            ArrayList prmUomDetails = new ArrayList();
            prmUomDetails.Add(new SqlParameter("@Mode", 11));
            prmUomDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmUomDetails.Add(new SqlParameter("@UnitID", intUOMID));
            //    prmUomDetails.Add(new SqlParameter("@UomTypeID", 1));
            return MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmUomDetails);
        }

        public DataSet GetDeliveryNoteReport()
        {
            ArrayList prmUomDetails = new ArrayList();
            prmUomDetails.Add(new SqlParameter("@Mode", 15));
            prmUomDetails.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
            return MobjDataLayer.ExecuteDataSet("spInvItemIssue", prmUomDetails);
        }


        private bool SaveItemGroupIssueDetails(clsDTOItemIssueDetails objClsItemIssueDetails)
        {
            //Save Item Group Issue Details
            int intSerialNo = 0;
            foreach (clsDTOItemGroupIssueDetails objDTOItemGroupIssueDetails in objClsItemIssueDetails.lstItemGroupIssueDetails)
            {
                ArrayList prmItemGroupIssueDetails = new ArrayList();
                prmItemGroupIssueDetails.Add(new SqlParameter("@Mode", 17));
                prmItemGroupIssueDetails.Add(new SqlParameter("@SerialNo", ++intSerialNo));
                prmItemGroupIssueDetails.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
                //GroupItemID
                prmItemGroupIssueDetails.Add(new SqlParameter("@ItemIssueDetailsSerialNo",objClsItemIssueDetails.intItemID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@ItemID", objDTOItemGroupIssueDetails.intItemID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@BatchID", objDTOItemGroupIssueDetails.intBatchID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@UOMID", objDTOItemGroupIssueDetails.intUOMID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@Quantity", objDTOItemGroupIssueDetails.decQuantity));
                MobjDataLayer.ExecuteNonQueryWithTran("spInvItemIssue", prmItemGroupIssueDetails);

                clsDTOItemIssueDetails objItemIssueDetails = new clsDTOItemIssueDetails();
                objItemIssueDetails.decQuantity = objDTOItemGroupIssueDetails.decQuantity;
                objItemIssueDetails.intBatchID = objDTOItemGroupIssueDetails.intBatchID;
                objItemIssueDetails.intItemID = objDTOItemGroupIssueDetails.intItemID;
                objItemIssueDetails.intUOMID = objDTOItemGroupIssueDetails.intUOMID;
                UpdateStockDetails(objItemIssueDetails, false);
                UpdateStockMaster(objItemIssueDetails, false);
            }
            SaveItemSummary(objClsItemIssueDetails);
            return true;
        }

        private void SaveItemSummary(clsDTOItemIssueDetails objClsItemIssueDetails)
        {
            foreach (clsDTOItemGroupIssueDetails objDTOItemGroupIssueDetails in objClsItemIssueDetails.lstItemGroupIssueDetails)
            {
                ArrayList prmItemGroupIssueDetails = new ArrayList();
                prmItemGroupIssueDetails.Add(new SqlParameter("@Mode", 64));
                prmItemGroupIssueDetails.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@ItemID", objDTOItemGroupIssueDetails.intItemID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@BatchID", objDTOItemGroupIssueDetails.intBatchID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@UOMID", objDTOItemGroupIssueDetails.intUOMID));
                prmItemGroupIssueDetails.Add(new SqlParameter("@Quantity", objDTOItemGroupIssueDetails.decQuantity));
                MobjDataLayer.ExecuteNonQueryWithTran("spInvItemIssue", prmItemGroupIssueDetails);
            }

        }
        public bool DeleteItemGroupIssueDetails()
        {
            //Delete ItemIssueDetails by ItemIssueID
            DataTable dtItemGroupIssueDetails = GetItemGroupIssueDetails();
            foreach (DataRow dr in dtItemGroupIssueDetails.Rows)
            {
                clsDTOItemIssueDetails objItemIssueDetails = new clsDTOItemIssueDetails();
                objItemIssueDetails.decQuantity = Convert.ToDecimal(dr["Quantity"]);
                objItemIssueDetails.intBatchID = Convert.ToInt64(dr["BatchID"]);
                objItemIssueDetails.intItemID = Convert.ToInt32(dr["ItemID"]);
                objItemIssueDetails.intUOMID = Convert.ToInt32(dr["UomID"]);
                UpdateStockDetails(objItemIssueDetails, true);
                UpdateStockMaster(objItemIssueDetails, true);
            }
            ArrayList prmItemGroupIssueDetails = new ArrayList();
            prmItemGroupIssueDetails = new ArrayList();
            prmItemGroupIssueDetails.Add(new SqlParameter("@Mode", 19));
            prmItemGroupIssueDetails.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));

            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvItemIssue", prmItemGroupIssueDetails) != 0)
                return true;
            return false;
        }

        private bool UpdateStockValue()
        {
            //This is to Update Stock Value

            ArrayList prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation.Add(new SqlParameter("@Mode", 30));
            prmItemStockValueUpdation.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
            prmItemStockValueUpdation.Add(new SqlParameter("@ReferenceID", PobjDTOItemIssueMaster.intReferenceID));
            prmItemStockValueUpdation.Add(new SqlParameter("@SIOperationTypeID", (int)OperationType.SalesInvoice));
            prmItemStockValueUpdation.Add(new SqlParameter("@OperationTypeID", (int)OperationType.DeliveryNote));

            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvItemIssue", prmItemStockValueUpdation) != 0)
                return true;
            return false;
        }

        public DataTable GetItemGroupIssueDetails()
        {
            //function for getting ItemGroup Issue Details
            ArrayList prmItemGroupIssueDetails = new ArrayList();
            prmItemGroupIssueDetails.Add(new SqlParameter("@Mode", 20));
            prmItemGroupIssueDetails.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
            return MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmItemGroupIssueDetails);
        }

        public DataTable GetItemGroupDetails(int intItemGroupID)
        {
            ArrayList prmItemGroupIssueDetails = new ArrayList();
            prmItemGroupIssueDetails.Add(new SqlParameter("@Mode", 21));
            prmItemGroupIssueDetails.Add(new SqlParameter("@ItemID", intItemGroupID));
            return MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmItemGroupIssueDetails);
        }

        public void updateSaleOpStockAccountDetailsEdit(Int64 ReferenceID)
        {
            dtsTemp11 = SalesInvoiceDetailForStockAccount(ReferenceID);
            if (dtsTemp11.Tables[0].Rows.Count > 0)
            {
                int AccountId = Convert.ToInt32(dtsTemp11.Tables[1].Rows[0]["AccountId"].ToString());
                int CompanyID = 0;
                DateTime InvoiceDate = DateTime.Now;
                double Amount = 0;

                for (int i = 0; i < dtsTemp11.Tables[0].Rows.Count; i++)
                {
                    CompanyID = Convert.ToInt32(dtsTemp11.Tables[0].Rows[i]["CompanyID"].ToString());
                    InvoiceDate = Convert.ToDateTime(dtsTemp11.Tables[0].Rows[i]["IssuedDate"].ToString());
                    Amount += Convert.ToDouble(dtsTemp11.Tables[0].Rows[i]["Amount"].ToString());
                }
                string strAmt = "-" + Amount.ToString();
                Amount = Convert.ToDouble(strAmt);
                UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
            }
        }

        public bool UpdateSaleOpStockAccountDetails(Int64 ReferenceID)
        {
            DataSet dtsTemp1 = SalesInvoiceDetailForStockAccount(ReferenceID);
            if (dtsTemp1.Tables[0].Rows.Count > 0)
            {
                int AccountId = Convert.ToInt32(dtsTemp1.Tables[1].Rows[0]["AccountId"].ToString());
                int CompanyID = 0;
                DateTime InvoiceDate = DateTime.Now;
                double Amount = 0;

                for (int i = 0; i < dtsTemp1.Tables[0].Rows.Count; i++)
                {
                    CompanyID = Convert.ToInt32(dtsTemp1.Tables[0].Rows[i]["CompanyID"].ToString());
                    InvoiceDate = Convert.ToDateTime(dtsTemp1.Tables[0].Rows[i]["IssuedDate"].ToString());
                    Amount += Convert.ToDouble(dtsTemp1.Tables[0].Rows[i]["Amount"].ToString());
                }
                if (blnAddStatus == true && blnUpdateStatus == false && blnDeleteStatus == false)
                {
                    UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
                }
                else if (blnAddStatus == false && blnUpdateStatus == true && blnDeleteStatus == false)
                {
                    UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
                }
                else if (blnAddStatus == false && blnUpdateStatus == false && blnDeleteStatus == true)
                {
                    string strAmt = "-" + Amount.ToString();
                    Amount = Convert.ToDouble(strAmt);
                    UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
                }
            }
            blnAddStatus = true;
            blnUpdateStatus = false;
            blnDeleteStatus = false;
            return true;
        }

        public DataSet SalesInvoiceDetailForStockAccount(Int64 Rownum)// Getting Invoice detail
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 22));
            prmCommon.Add(new SqlParameter("@ReferenceID", Rownum));
            return MobjDataLayer.ExecuteDataSet("spInvItemIssue", prmCommon);
        }

        public DataSet SalesInvoiceDetailForStockAccountScheduled(Int64 Rownum)// Getting Invoice detail
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 23));
            prmCommon.Add(new SqlParameter("@ReferenceID", Rownum));
            return MobjDataLayer.ExecuteDataSet("spInvItemIssue", prmCommon);
        }

        public bool UpdateOpStockAccount(int AccountId, int CompanyId, DateTime CurDate, double Amount)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 24));
            prmCommon.Add(new SqlParameter("@OperationTypeID", AccountId));
            prmCommon.Add(new SqlParameter("@CompanyID", CompanyId));
            prmCommon.Add(new SqlParameter("@DeliveryDate", CurDate));
            prmCommon.Add(new SqlParameter("@Quantity", Amount));
            MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmCommon);
            return true;
        }
        //public bool PostAccounts(int mode)
        //{
        //    ArrayList prmCommon = new ArrayList();
        //    prmCommon.Add(new SqlParameter("@Mode", mode));
        //    prmCommon.Add(new SqlParameter("@SalesOrderID", PobjDTOItemIssueMaster.intReferenceID));
        //    prmCommon.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
        //    MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmCommon);
        //    return true;
        //}

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", parameters);
        }

        public DataTable GetOperationTypeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", parameters);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", parameters);
        }

        public DataTable GetEmployeeByPermissionWithDept(int RoleID, int MenuID, int ControlID, int CompanyID, int DeptID)
        {
            ArrayList prmDetails = new ArrayList();
            prmDetails.Add(new SqlParameter("@Mode", 7));
            prmDetails.Add(new SqlParameter("@RoleID", RoleID));
            prmDetails.Add(new SqlParameter("@MenuID", MenuID));
            prmDetails.Add(new SqlParameter("@ControlID", ControlID));
            prmDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmDetails.Add(new SqlParameter("@DeptID", DeptID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", prmDetails);
        }

        public DataTable GetEmployeeByPermissionWithDeptWithoutCompany(int RoleID, int MenuID, int ControlID, int DeptID)
        {
            ArrayList prmDetails = new ArrayList();
            prmDetails.Add(new SqlParameter("@Mode", 8));
            prmDetails.Add(new SqlParameter("@RoleID", RoleID));
            prmDetails.Add(new SqlParameter("@MenuID", MenuID));
            prmDetails.Add(new SqlParameter("@ControlID", ControlID));
            prmDetails.Add(new SqlParameter("@DeptID", DeptID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", prmDetails);
        }

        private bool SaveItemIssueLocationDetails()
        {

            int intSerialNo = 0;
            foreach (clsDTOItemIssueLocationDetails objLocationDetails in PobjDTOItemIssueMaster.lstItemIssueLocationDetails)
            {
                ArrayList alParameters = new ArrayList();

                alParameters.Add(new SqlParameter("@Mode", 26));
                alParameters.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));

                alParameters.Add(new SqlParameter("@SerialNo", ++intSerialNo));
                alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.intItemID));
                alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.lngBatchID));
                alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.intLocationID));
                alParameters.Add(new SqlParameter("@RowID", objLocationDetails.intRowID));
                alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.intBlockID));
                alParameters.Add(new SqlParameter("@LotID", objLocationDetails.intLotID));
                alParameters.Add(new SqlParameter("@UOMID", objLocationDetails.intUOMID));
                alParameters.Add(new SqlParameter("@Quantity", objLocationDetails.decQuantity));
                DataTable dtGetUomDetails = GetUomConversionValues(objLocationDetails.intUOMID, objLocationDetails.intItemID);
                decimal decQuantity = objLocationDetails.decQuantity;
                if (dtGetUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                        objLocationDetails.decQuantity = objLocationDetails.decQuantity / decConversionValue;
                    else if (intConversionFactor == 2)
                        objLocationDetails.decQuantity = objLocationDetails.decQuantity * decConversionValue;
                }
                UpdateItemLocationDetails(objLocationDetails, false);
                objLocationDetails.decQuantity = decQuantity;

                MobjDataLayer.ExecuteNonQueryWithTran("spInvItemIssue", alParameters);
            }
            return true;
        }

        private bool UpdateItemLocationDetails(clsDTOItemIssueLocationDetails objLocationDetails, bool blnIsDeletion)
        {
            ArrayList alParameters = new ArrayList();
            if (!blnIsDeletion)
                alParameters.Add(new SqlParameter("@Mode", 2));
            else
                alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@Type", 1));
            alParameters.Add(new SqlParameter("@CompanyID", PobjDTOItemIssueMaster.intCompanyID));
            alParameters.Add(new SqlParameter("@WarehouseID", PobjDTOItemIssueMaster.intWarehouseID));
            alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.intItemID));
            alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.lngBatchID));
            alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.intLocationID));
            alParameters.Add(new SqlParameter("@RowID", objLocationDetails.intRowID));
            alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.intBlockID));
            alParameters.Add(new SqlParameter("@LotID", objLocationDetails.intLotID));
            alParameters.Add(new SqlParameter("@Quantity", objLocationDetails.decQuantity));
            MobjDataLayer.ExecuteNonQueryWithTran("spInvItemLocationTransfer", alParameters);
            return true;
        }

        public decimal GetItemLocationQuantity(clsDTOItemIssueLocationDetails objLocationDetails)
        {
            ArrayList alParameters = new ArrayList();
            decimal decQuantity = 0;
            alParameters.Add(new SqlParameter("@Mode", 13));
            alParameters.Add(new SqlParameter("@CompanyID", PobjDTOItemIssueMaster.intCompanyID));
            alParameters.Add(new SqlParameter("@WarehouseID", PobjDTOItemIssueMaster.intWarehouseID));
            alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.intItemID));
            alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.lngBatchID));
            alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.intLocationID));
            alParameters.Add(new SqlParameter("@RowID", objLocationDetails.intRowID));
            alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.intBlockID));
            alParameters.Add(new SqlParameter("@LotID", objLocationDetails.intLotID));
            DataTable datLocationDetails = MobjDataLayer.ExecuteDataTable("spInvItemLocationTransfer", alParameters);
            if (datLocationDetails.Rows.Count > 0)
            {
                if (datLocationDetails.Rows[0]["Quantity"] != DBNull.Value)
                    decQuantity = datLocationDetails.Rows[0]["Quantity"].ToDecimal();
            }
            return decQuantity;
        }

        public DataTable GetItemIssueLocationDetails()
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 28));
            alParameters.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
            return MobjDataLayer.ExecuteDataTable("spInvItemIssue", alParameters);
        }

        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 14));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@LocationWise", ClsCommonSettings.PblnIsLocationWise));
            return MobjDataLayer.ExecuteDataTable("spInvItemLocationTransfer", alParameters).Rows[0];
        }

        private bool DeleteItemIssueLocationDetails()
        {
            DataTable datItemIssueLocationDetails = GetItemIssueLocationDetails();
            foreach (DataRow dr in datItemIssueLocationDetails.Rows)
            {
                clsDTOItemIssueLocationDetails objLocationDetails = new clsDTOItemIssueLocationDetails();
                objLocationDetails.intItemID = dr["ItemID"].ToInt32();
                objLocationDetails.lngBatchID = dr["BatchID"].ToInt64();
                objLocationDetails.intLocationID = dr["LocationID"].ToInt32();
                objLocationDetails.intRowID = dr["RowID"].ToInt32();
                objLocationDetails.intBlockID = dr["BlockID"].ToInt32();
                objLocationDetails.intLotID = dr["LotID"].ToInt32();
                objLocationDetails.decQuantity = dr["Quantity"].ToDecimal();
                objLocationDetails.intUOMID = dr["UomID"].ToInt32();
                DataTable dtGetUomDetails = GetUomConversionValues(objLocationDetails.intUOMID, objLocationDetails.intItemID);
                decimal decQuantity = objLocationDetails.decQuantity;
                if (dtGetUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                        objLocationDetails.decQuantity = objLocationDetails.decQuantity / decConversionValue;
                    else if (intConversionFactor == 2)
                        objLocationDetails.decQuantity = objLocationDetails.decQuantity * decConversionValue;
                }

                UpdateItemLocationDetails(objLocationDetails, true);
            }
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 27));
            alParameters.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
            MobjDataLayer.ExecuteNonQueryWithTran("spInvItemIssue", alParameters);
            return true;
        }
        public DataSet DisplayItemIssueLocationReport()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 29));
            prmReportDetails.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
            return MobjDataLayer.ExecuteDataSet("spInvItemIssue", prmReportDetails);
        }


        public decimal GetBalanceAmount(long lngSalesInvoiceID)
        {
            prmItemIssueMaster = new ArrayList();
            prmItemIssueMaster.Add(new SqlParameter("@Mode", 31));
            prmItemIssueMaster.Add(new SqlParameter("@ReferenceID", lngSalesInvoiceID));
            return MobjDataLayer.ExecuteScalar("spInvItemIssue", prmItemIssueMaster).ToDecimal();
        }
        private bool UpdateSalesOrderStatus(int StatusID, long SalesOrderID, int ItemID)
        {
            //This is to Update sales Order Detail status

            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 33));
            prmSalesStatus.Add(new SqlParameter("@DeliveredStatus", StatusID));
            prmSalesStatus.Add(new SqlParameter("@SalesOrderIDD", SalesOrderID));
            prmSalesStatus.Add(new SqlParameter("@ItemID", ItemID));
            int value = MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmSalesStatus);
            if (value > 0)
                return true;
            else
                return false;
        }

        private bool UpdateSalesOrderMasterStatus(int StatusID, long SalesOrderID)  // Update satus in SalesOrdermaster
        {
            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 34));
            prmSalesStatus.Add(new SqlParameter("@DeliveredStatus", StatusID));
            prmSalesStatus.Add(new SqlParameter("@SalesOrderIDD", SalesOrderID));
            int value = MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmSalesStatus);
            if (value > 0)
                return true;
            else
                return false;
        }
        public decimal SalesOrderQuantity(long SalesOrderID, int ItemID)
        {
            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 32));
            prmSalesStatus.Add(new SqlParameter("@SalesOrderIDD", SalesOrderID));
            prmSalesStatus.Add(new SqlParameter("@ItemID", ItemID));
            decimal value = MobjDataLayer.ExecuteScalar("spInvItemIssue", prmSalesStatus).ToDecimal();
            return value;
        }


        private bool UpdateSalesInvoiceStatus(int StatusID, long SalesInvoiceID, int ItemID)
        {
            //This is to Update InvoiceOrder Detail status

            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 39));
            prmSalesStatus.Add(new SqlParameter("@DeliveredStatus", StatusID));
            prmSalesStatus.Add(new SqlParameter("@SalesInvoiceID", SalesInvoiceID));
            prmSalesStatus.Add(new SqlParameter("@ItemID", ItemID));
            int value = MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmSalesStatus);
            if (value > 0)
                return true;
            else
                return false;
        }
        //private bool UpdateSalesInvoiceMasterStatus(int StatusID, long SalesInvoiceID)  // Update satus in SalesInvoicemaster
        //{
        //    ArrayList prmSalesStatus = new ArrayList();
        //    prmSalesStatus = new ArrayList();
        //    prmSalesStatus.Add(new SqlParameter("@Mode", 40));
        //    prmSalesStatus.Add(new SqlParameter("@DeliveredStatus", StatusID));
        //    prmSalesStatus.Add(new SqlParameter("@SalesInvoiceID", SalesInvoiceID));
        //    int value = MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmSalesStatus);
        //    if (value > 0)
        //        return true;
        //    else
        //        return false;
        //}
        public decimal SalesInvoiceQuantity(long SalesInvoiceID, int ItemID)
        {
            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 38));
            prmSalesStatus.Add(new SqlParameter("@SalesInvoiceID", SalesInvoiceID));
            prmSalesStatus.Add(new SqlParameter("@ItemID", ItemID));
            decimal value = MobjDataLayer.ExecuteScalar("spInvItemIssue", prmSalesStatus).ToDecimal();
            return value;
        }
        public DataTable GetSalesInvoiceNos(int CompanyID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 41));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            return MobjDataLayer.ExecuteDataTable("spInvItemIssue", alParameters);
        }
        public DataTable GetSalesOrderNos(int CompanyID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 42));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            return MobjDataLayer.ExecuteDataTable("spInvItemIssue", alParameters);
        }
        private void SaveItemIssueReferenceDetails(long intItemIssueID, long intReferenceID)
        {
            //Save Item Issue reference Details
            ArrayList prmItemIssueRefDetails = new ArrayList();
            prmItemIssueRefDetails.Add(new SqlParameter("@Mode", 44));
            prmItemIssueRefDetails.Add(new SqlParameter("@ItemIssueID", intItemIssueID));
            MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmItemIssueRefDetails);

            foreach (clsDTOItemIssueDetails objDTOItemIssueDetails in PobjDTOItemIssueMaster.lstItemIssueDetails)
            {
                prmItemIssueDetails = new ArrayList();
                prmItemIssueDetails.Add(new SqlParameter("@Mode", 43));

                prmItemIssueDetails.Add(new SqlParameter("@ItemIssueID", intItemIssueID));
                prmItemIssueDetails.Add(new SqlParameter("@ReferenceID", intReferenceID));

                prmItemIssueDetails.Add(new SqlParameter("@ItemID", objDTOItemIssueDetails.intItemID));
                prmItemIssueDetails.Add(new SqlParameter("@BatchID", objDTOItemIssueDetails.intBatchID));
                prmItemIssueDetails.Add(new SqlParameter("@Quantity", objDTOItemIssueDetails.decQuantity));
                prmItemIssueDetails.Add(new SqlParameter("@IsGroup", objDTOItemIssueDetails.blnIsGroup));
               

                MobjDataLayer.ExecuteNonQueryWithTran("spInvItemIssue", prmItemIssueDetails);
            }

        }
        public decimal GetDeliveredQty(int intItemID, Int64 intReferenceID, int OrderTypeID)
        {
            decimal decDeliveredQty = 0;
            DataTable datDetails;
            prmItemIssueMaster = new ArrayList();
            prmItemIssueMaster.Add(new SqlParameter("@Mode", 45));
            prmItemIssueMaster.Add(new SqlParameter("@ItemID", intItemID));

            prmItemIssueMaster.Add(new SqlParameter("@ReferenceID", intReferenceID));
            prmItemIssueMaster.Add(new SqlParameter("@OrderTypeID", OrderTypeID));
            datDetails = MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmItemIssueMaster);

            if (datDetails.Rows.Count > 0)
            {
                if (datDetails.Rows[0]["deliveredQty"] != DBNull.Value)
                    decDeliveredQty = Convert.ToDecimal(datDetails.Rows[0]["deliveredQty"]);
                else
                    decDeliveredQty = 0;

            }
            return decDeliveredQty;
        }
        public decimal GetDeliveredQtyNonBatchItems(int intItemID,int intItemIssueID)
        {
            decimal decOldTotalNonBatchQty = 0;
            DataTable datDetails;
            prmItemIssueMaster = new ArrayList();
            prmItemIssueMaster.Add(new SqlParameter("@Mode", 54));
            prmItemIssueMaster.Add(new SqlParameter("@ItemID", intItemID));

            prmItemIssueMaster.Add(new SqlParameter("@ItemIssueID", intItemIssueID));
            datDetails = MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmItemIssueMaster);

            if (datDetails.Rows.Count > 0)
            {
                if (datDetails.Rows[0]["TotalOldQtyForMinItems"] != DBNull.Value)
                    decOldTotalNonBatchQty = Convert.ToInt32(datDetails.Rows[0]["TotalOldQtyForMinItems"]);
                else
                    decOldTotalNonBatchQty = 0;

            }
            return decOldTotalNonBatchQty;
        }
        //STATUS UPDATIONS
        private void UpdateSalesInvoiceDetailsStatus(clsDTOItemIssueDetails objItemIssueDetails)
        {
            //This is to Update InvoiceOrder Detail status

            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 48));
            prmSalesStatus.Add(new SqlParameter("@ReferenceID",PobjDTOItemIssueMaster.intReferenceID));
            prmSalesStatus.Add(new SqlParameter("@ItemID",objItemIssueDetails.intItemID));
            prmSalesStatus.Add(new SqlParameter("@BatchID", objItemIssueDetails.intBatchID));
            prmSalesStatus.Add(new SqlParameter("@IsGroup", objItemIssueDetails.blnIsGroup));
            //int value =
                MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmSalesStatus);
            //if (value > 0)
            //    return true;
            //else
            //    return false;
        }
        private void UpdateSalesOrderDetailsStatus(clsDTOItemIssueDetails objItemIssueDetails)
        {
            

            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 52));
            prmSalesStatus.Add(new SqlParameter("@ReferenceID", PobjDTOItemIssueMaster.intReferenceID));
            //prmSalesStatus.Add(new SqlParameter("@OrderTypeID", PobjDTOItemIssueMaster.intOperationTypeID));
            prmSalesStatus.Add(new SqlParameter("@ItemID", objItemIssueDetails.intItemID));
            prmSalesStatus.Add(new SqlParameter("@BatchID", objItemIssueDetails.intBatchID));
            prmSalesStatus.Add(new SqlParameter("@IsGroup", objItemIssueDetails.blnIsGroup));
            
            MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmSalesStatus);
            
        }
        private bool UpdateSalesOrderMastersStatus(long SalesOrderID)
        {
            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 51));

            prmSalesStatus.Add(new SqlParameter("@SalesOrderID", SalesOrderID));
            int value = MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmSalesStatus);
            if (value > 0)
                return true;
            else
                return false;
        }
        private bool UpdateSalesInvoiceMastersStatus( long SalesInvoiceID)  
        {
            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 47));
           
            prmSalesStatus.Add(new SqlParameter("@SalesInvoiceID", SalesInvoiceID));
            int value = MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmSalesStatus);
            if (value > 0)
                return true;
            else
                return false;
        }
        private void UpdateSalesInvoiceDetailsStatusOnDelete(clsDTOItemIssueDetails objItemIssueDetails)
        {
            //This is to Update InvoiceOrder Detail status on delete

            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 49));
            prmSalesStatus.Add(new SqlParameter("@ReferenceID", PobjDTOItemIssueMaster.intReferenceID));
            prmSalesStatus.Add(new SqlParameter("@ItemID", objItemIssueDetails.intItemID));
            prmSalesStatus.Add(new SqlParameter("@BatchID", objItemIssueDetails.intBatchID));
            prmSalesStatus.Add(new SqlParameter("@IsGroup", objItemIssueDetails.blnIsGroup));

            
            MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmSalesStatus);
            
        }
        private void UpdateSalesOrderDetailsStatusOnDelete(clsDTOItemIssueDetails objItemIssueDetails)
        {
            

            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 53));
            prmSalesStatus.Add(new SqlParameter("@ReferenceID", PobjDTOItemIssueMaster.intReferenceID));
            prmSalesStatus.Add(new SqlParameter("@ItemID", objItemIssueDetails.intItemID));
            prmSalesStatus.Add(new SqlParameter("@BatchID", objItemIssueDetails.intBatchID));
            prmSalesStatus.Add(new SqlParameter("@IsGroup", objItemIssueDetails.blnIsGroup));



            MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmSalesStatus);

        }
        public bool IsStockValueUpdationAllowed(long lngItemIssueID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 55));
            prmCommon.Add(new SqlParameter("@ItemIssueID", lngItemIssueID));
            object objResult = MobjDataLayer.ExecuteScalar("spInvItemIssue", prmCommon);
            return Convert.ToInt32(objResult) > 0;
        }
        public DataTable BatchLessItems(int intItemID, int intWareHouseID)
        {
            ArrayList prmDelivery = new ArrayList();
            prmDelivery.Add(new SqlParameter("@Mode", 61));
            prmDelivery.Add(new SqlParameter("@ItemID", intItemID));

            prmDelivery.Add(new SqlParameter("@WarehouseID", intWareHouseID));

            return MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmDelivery);
        }
        public DataTable GetGroupDetails(int intItemID)
        {
            ArrayList prmDelivery = new ArrayList();
            prmDelivery.Add(new SqlParameter("@Mode", 56));
            prmDelivery.Add(new SqlParameter("@ItemID", intItemID));



            return MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmDelivery);
        }
        public decimal GetStockQuantityForBatchlessItems(int intItemID, int intWarehouseID)
        {
            ArrayList prmStockDetailsBatchless = new ArrayList();
            prmStockDetailsBatchless.Add(new SqlParameter("@Mode", 57));
            prmStockDetailsBatchless.Add(new SqlParameter("@ItemID", intItemID));

            prmStockDetailsBatchless.Add(new SqlParameter("@WarehouseID", intWarehouseID));

            DataTable datStockDetailsB = MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmStockDetailsBatchless);
            decimal decQuantity = 0;
            if (datStockDetailsB.Rows.Count > 0)
            {
                if (datStockDetailsB.Rows[0]["AvailableQty"] != DBNull.Value)
                {
                    decQuantity = Convert.ToDecimal(datStockDetailsB.Rows[0]["AvailableQty"]);
                }
            }
            return decQuantity;
        }
        public DataTable GetItemIssueDetailsForDeletion()
        {

            ArrayList prmDeliveryDetails = new ArrayList();
            prmDeliveryDetails.Add(new SqlParameter("@Mode", 58));
            prmDeliveryDetails.Add(new SqlParameter("@ItemIssueID", PobjDTOItemIssueMaster.intItemIssueID));
            return MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmDeliveryDetails);
        }
        public decimal GetStockQuantityForGroup(int intItemGroupID, int intWarehouseID)
        {
            ArrayList prmStockDetailsGrp = new ArrayList();
            prmStockDetailsGrp.Add(new SqlParameter("@Mode", 59));
            prmStockDetailsGrp.Add(new SqlParameter("@ItemGroupID", intItemGroupID));

             prmStockDetailsGrp.Add(new SqlParameter("@WarehouseID", intWarehouseID));

            DataTable datStockDetailsB = MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmStockDetailsGrp);
            decimal decQuantity = 0;
            if (datStockDetailsB.Rows.Count > 0)
            {
                if (datStockDetailsB.Rows[0]["AvailableQtyGrp"] != DBNull.Value)
                {
                    decQuantity = Convert.ToDecimal(datStockDetailsB.Rows[0]["AvailableQtyGrp"]);
                }
            }
            return decQuantity;
        }
        public DataTable BatchLessItemsEditMode(int intItemID, int intItemIssueID)
        {
            ArrayList prmDelivery = new ArrayList();
            prmDelivery.Add(new SqlParameter("@Mode", 60));
            prmDelivery.Add(new SqlParameter("@ItemID", intItemID));

            prmDelivery.Add(new SqlParameter("@ItemIssueID", intItemIssueID));

            return MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmDelivery);
        }
        public DataTable GetQtyInGroup(Int64 IntItemID)
        {
            ArrayList prmDelivery;
            prmDelivery = new ArrayList();

            prmDelivery.Add(new SqlParameter("@Mode", 62));
            prmDelivery.Add(new SqlParameter("@ItemID", IntItemID));
            return MobjDataLayer.ExecuteDataTable("spInvItemIssue", prmDelivery);
        }
        private bool UpdateSIMastersStatusOnDNdelete(clsDTOItemIssueDetails objItemIssueDetails)
        {
            ArrayList prmSalesStatus = new ArrayList();
            prmSalesStatus = new ArrayList();
            prmSalesStatus.Add(new SqlParameter("@Mode", 66));
            prmSalesStatus.Add(new SqlParameter("@ReferenceID", PobjDTOItemIssueMaster.intReferenceID));

           
            int value = MobjDataLayer.ExecuteNonQuery("spInvItemIssue", prmSalesStatus);
            if (value > 0)
                return true;
            else
                return false;
        }
    }

}
