﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
namespace MyBooksERP
{
   public  class clsDALWarehouseTransfer
    {
        ClsCommonUtility MobjClsCommonUtility; // object of clsCommonUtility
        DataLayer MobjDataLayer;
        ArrayList prmStockTransfer;

        public clsDTOWarehouseTransfer MobjclsDTOWarehouseTransfer { get; set; }
        public clsDALWarehouseTransfer(DataLayer objDataLayer)
        {
            MobjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MobjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MobjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }
        public bool DisplayStockTransferInfo(Int64 iOrderID) 
        {

            //Function For Displaying Stock transfer Info
            prmStockTransfer = new ArrayList();

            prmStockTransfer.Add(new SqlParameter("@Mode", 1));

            prmStockTransfer.Add(new SqlParameter("@StockTransferID", iOrderID));

            SqlDataReader sdrStockTransferOrder = MobjDataLayer.ExecuteReader("spInvStockTransfer", prmStockTransfer, CommandBehavior.SingleRow);

            if (sdrStockTransferOrder.Read())
            {
               MobjclsDTOWarehouseTransfer.intStockTransferID= iOrderID;
                MobjclsDTOWarehouseTransfer.intSlNo = sdrStockTransferOrder["SlNo"].ToInt32();
                MobjclsDTOWarehouseTransfer.intOrderTypeID = sdrStockTransferOrder["OrderTypeID"].ToInt32();
                MobjclsDTOWarehouseTransfer.strStockTransferNo = Convert.ToString(sdrStockTransferOrder["StockTransferNo"]);

                MobjclsDTOWarehouseTransfer.strOrderDate= Convert.ToString(sdrStockTransferOrder["OrderDate"]);
                MobjclsDTOWarehouseTransfer.intVendorID = sdrStockTransferOrder["VendorID"].ToInt32();
                MobjclsDTOWarehouseTransfer.intWarehouseID = sdrStockTransferOrder["WarehouseID"].ToInt32();
                MobjclsDTOWarehouseTransfer.intReferenceID= sdrStockTransferOrder["ReferenceID"].ToInt32();
                MobjclsDTOWarehouseTransfer.intReferenceInchargeID = sdrStockTransferOrder["ReferenceInchargeID"].ToInt32();
                MobjclsDTOWarehouseTransfer.intCompanyID = sdrStockTransferOrder["CompanyID"].ToInt32();
                MobjclsDTOWarehouseTransfer.intVendorAddID = sdrStockTransferOrder["VendorAddID"].ToInt32();

                if (sdrStockTransferOrder["VendorName"] != DBNull.Value) MobjclsDTOWarehouseTransfer.strVendorName = Convert.ToString(sdrStockTransferOrder["VendorName"]); else MobjclsDTOWarehouseTransfer.strVendorName = "";


                if (sdrStockTransferOrder["AddressName"] != DBNull.Value) MobjclsDTOWarehouseTransfer.strAddressName = Convert.ToString(sdrStockTransferOrder["AddressName"]); else MobjclsDTOWarehouseTransfer.strAddressName = "";
                if (sdrStockTransferOrder["PaymentTermsID"] != DBNull.Value) MobjclsDTOWarehouseTransfer.intPaymentTermsID = Convert.ToInt32(sdrStockTransferOrder["PaymentTermsID"]); else MobjclsDTOWarehouseTransfer.intPaymentTermsID = 0;
                if (sdrStockTransferOrder["CurrencyID"] != DBNull.Value) MobjclsDTOWarehouseTransfer.intCurrencyID = Convert.ToInt32(sdrStockTransferOrder["CurrencyID"]); else MobjclsDTOWarehouseTransfer.intCurrencyID = 0;
                MobjclsDTOWarehouseTransfer.strOrderDate = Convert.ToString(sdrStockTransferOrder["OrderDate"]);
                MobjclsDTOWarehouseTransfer.strDueDate = Convert.ToString(sdrStockTransferOrder["DueDate"]);

                if (sdrStockTransferOrder["StatusID"] != DBNull.Value) MobjclsDTOWarehouseTransfer.intStatusID = Convert.ToInt32(sdrStockTransferOrder["StatusID"]); else MobjclsDTOWarehouseTransfer.intStatusID = 0;
                if (sdrStockTransferOrder["Status"]!=DBNull.Value) MobjclsDTOWarehouseTransfer.strStatus = Convert.ToString(sdrStockTransferOrder["Status"]); else MobjclsDTOWarehouseTransfer.strStatus = "";
                if (sdrStockTransferOrder["Remarks"] != DBNull.Value) MobjclsDTOWarehouseTransfer.strRemarks = Convert.ToString(sdrStockTransferOrder["Remarks"]); else MobjclsDTOWarehouseTransfer.strRemarks = "";
                MobjclsDTOWarehouseTransfer.strEmployeeName = Convert.ToString(sdrStockTransferOrder["EmployeeName"]);
                //if (sdrStockTransferOrder["GrandDiscountID"] != DBNull.Value) MobjclsDTOWarehouseTransfer.intGrandDiscountID = Convert.ToInt32(sdrStockTransferOrder["GrandDiscountID"]); else MobjclsDTOWarehouseTransfer.intGrandDiscountID = 0;
                //if (sdrStockTransferOrder["GrandDiscountAmount"] != DBNull.Value) MobjclsDTOWarehouseTransfer.decGrandDiscountAmount = Convert.ToDecimal(sdrStockTransferOrder["GrandDiscountAmount"]); else MobjclsDTOWarehouseTransfer.decGrandDiscountAmount = 0;
                if (sdrStockTransferOrder["ExpenseAmount"] != DBNull.Value) MobjclsDTOWarehouseTransfer.decExpenseAmount = Convert.ToDecimal(sdrStockTransferOrder["ExpenseAmount"]); else MobjclsDTOWarehouseTransfer.decExpenseAmount = 0;
                if (sdrStockTransferOrder["GrandAmount"] != DBNull.Value) MobjclsDTOWarehouseTransfer.decGrandAmount = Convert.ToDecimal(sdrStockTransferOrder["GrandAmount"]); else MobjclsDTOWarehouseTransfer.decGrandAmount = 0;
                if (sdrStockTransferOrder["NetAmount"] != DBNull.Value) MobjclsDTOWarehouseTransfer.decNetAmount = Convert.ToDecimal(sdrStockTransferOrder["NetAmount"]); else MobjclsDTOWarehouseTransfer.decNetAmount = 0;
                if (sdrStockTransferOrder["CreatedDate"] != DBNull.Value) MobjclsDTOWarehouseTransfer.strCreatedDate = Convert.ToString(sdrStockTransferOrder["CreatedDate"]); else MobjclsDTOWarehouseTransfer.strCreatedDate = "";
               
                //if (sdrStockTransferOrder["ApprovedDate"] != DBNull.Value) MobjclsDTOWarehouseTransfer.strApprovedDate = Convert.ToString(sdrStockTransferOrder["ApprovedDate"]); else MobjclsDTOWarehouseTransfer.strApprovedDate = "";
                //if (sdrStockTransferOrder["ApprovedBy"] != DBNull.Value) MobjclsDTOWarehouseTransfer.intApprovedBy = Convert.ToInt32(sdrStockTransferOrder["ApprovedBy"]); else MobjclsDTOWarehouseTransfer.intApprovedBy = 0;
                //if (sdrStockTransferOrder["ApprovedByName"] != DBNull.Value) MobjclsDTOWarehouseTransfer.strApprovedBy = Convert.ToString(sdrStockTransferOrder["ApprovedByName"]); else MobjclsDTOWarehouseTransfer.strApprovedBy = "";

             //   if (sdrStockTransferOrder["VerificationDescription"] != DBNull.Value) MobjclsDTOWarehouseTransfer.strVerificationDescription = Convert.ToString(sdrStockTransferOrder["VerificationDescription"]); else MobjclsDTOWarehouseTransfer.strVerificationDescription = "";


                //if (sdrStockTransferOrder["IncoTermsID"] != DBNull.Value)
                //    MobjclsDTOWarehouseTransfer.intIncoTermsID = Convert.ToInt32(sdrStockTransferOrder["IncoTermsID"]);
                //else
                //    MobjclsDTOWarehouseTransfer.intIncoTermsID = 0;
                
               
                //if (sdrStockTransferOrder["LeadTimeDetsID"] != DBNull.Value)
                //    MobjclsDTOWarehouseTransfer.intLeadTimeDetsID = Convert.ToInt32(sdrStockTransferOrder["LeadTimeDetsID"]);
                //if (sdrStockTransferOrder["LeadTime"] != DBNull.Value)
                //    MobjclsDTOWarehouseTransfer.decLeadTime = Convert.ToDecimal(sdrStockTransferOrder["LeadTime"]);
                //if (sdrStockTransferOrder["ProductionDate"] != DBNull.Value)
                //    MobjclsDTOWarehouseTransfer.strProductionDate = Convert.ToString(sdrStockTransferOrder["ProductionDate"]);
                //else
                //    MobjclsDTOWarehouseTransfer.strProductionDate = "";
                //if (sdrStockTransferOrder["TranshipmentDate"] != DBNull.Value)
                //    MobjclsDTOWarehouseTransfer.strTranshipmentDate = Convert.ToString(sdrStockTransferOrder["TranshipmentDate"]);
                //else
                //    MobjclsDTOWarehouseTransfer.strTranshipmentDate = "";
                //if (sdrStockTransferOrder["ClearenceDate"] != DBNull.Value)
                //    MobjclsDTOWarehouseTransfer.strClearenceDate = Convert.ToString(sdrStockTransferOrder["ClearenceDate"]);
                //else
                //    MobjclsDTOWarehouseTransfer.strClearenceDate = "";

                DataTable datCancellationDetails = DisplayCancellationDetails();


                if (datCancellationDetails != null && datCancellationDetails.Rows.Count > 0)
                {
                    MobjclsDTOWarehouseTransfer.strVerificationDescription = Convert.ToString(datCancellationDetails.Rows[0]["Remarks"]);
                    MobjclsDTOWarehouseTransfer.strApprovedDate = Convert.ToString(datCancellationDetails.Rows[0]["Date"]);
                    MobjclsDTOWarehouseTransfer.intCancelledBy = Convert.ToInt32(datCancellationDetails.Rows[0]["CancelledBy"]);
                    MobjclsDTOWarehouseTransfer.strApprovedBy = Convert.ToString(datCancellationDetails.Rows[0]["CancelledByName"]);
                }

                sdrStockTransferOrder.Close();
                return true;
            }
            else
            {
                sdrStockTransferOrder.Close();
                return false;
            }
           
        }

        public DataTable GetStockTransferItemDetails()
        {
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 12));
            prmStockTransfer.Add(new SqlParameter("@StockTransferID",MobjclsDTOWarehouseTransfer.intStockTransferID));
            return MobjDataLayer.ExecuteDataTable("spInvStockTransfer",prmStockTransfer);

        }
        public bool SaveStockTransfer(bool blnAddStatus)
        {
            object objOutStockTransferID = 0;
            prmStockTransfer = new ArrayList();



            if (blnAddStatus)
            {
                //InsertLeadTimeDetails();//insert lead time details
                prmStockTransfer = new ArrayList();
                prmStockTransfer.Add(new SqlParameter("@Mode", 2));

            }else
            {
               
                DeleteStockTransferDetails();
              //  UpdateLeadTimeDetails();//update lead time details

                prmStockTransfer = new ArrayList();
                prmStockTransfer.Add(new SqlParameter("@Mode", 3));
                prmStockTransfer.Add(new SqlParameter("@StockTransferID", MobjclsDTOWarehouseTransfer.intStockTransferID));
            }
            //prmStockTransfer.Add(new SqlParameter("@SlNo", MobjclsDTOWarehouseTransfer.intSlNo));
            prmStockTransfer.Add(new SqlParameter("@OrderTypeID", MobjclsDTOWarehouseTransfer.intOrderTypeID));
            prmStockTransfer.Add(new SqlParameter("@StockTransferNo", MobjclsDTOWarehouseTransfer.strStockTransferNo));
            prmStockTransfer.Add(new SqlParameter("@OrderDate", MobjclsDTOWarehouseTransfer.strOrderDate));

            
            prmStockTransfer.Add(new SqlParameter("@WarehouseID", MobjclsDTOWarehouseTransfer.intWarehouseID));
            prmStockTransfer.Add(new SqlParameter("@ReferenceID", MobjclsDTOWarehouseTransfer.intReferenceID));
            prmStockTransfer.Add(new SqlParameter("@ReferenceInchargeID", MobjclsDTOWarehouseTransfer.intReferenceInchargeID)); 
            prmStockTransfer.Add(new SqlParameter("@DueDate", MobjclsDTOWarehouseTransfer.strDueDate));
            if (MobjclsDTOWarehouseTransfer.intVendorID != 0)
            {
                prmStockTransfer.Add(new SqlParameter("@VendorID", MobjclsDTOWarehouseTransfer.intVendorID));
                prmStockTransfer.Add(new SqlParameter("@VendorAddID", MobjclsDTOWarehouseTransfer.intVendorAddID));
            }

            if(MobjclsDTOWarehouseTransfer.intPaymentTermsID !=0)
            prmStockTransfer.Add(new SqlParameter("@PaymentTermsID", MobjclsDTOWarehouseTransfer.intPaymentTermsID));
            //if (MobjclsDTOWarehouseTransfer.intIncoTermsID > 0)
            //    prmStockTransfer.Add(new SqlParameter("@IncoTermsID", MobjclsDTOWarehouseTransfer.intIncoTermsID));
            //if (MobjclsDTOWarehouseTransfer.intLeadTimeDetsID > 0)
            //    prmStockTransfer.Add(new SqlParameter("@LeadTimeDetsID", MobjclsDTOWarehouseTransfer.intLeadTimeDetsID));

            prmStockTransfer.Add(new SqlParameter("@Remarks", MobjclsDTOWarehouseTransfer.strRemarks));

            if (MobjclsDTOWarehouseTransfer.intGrandDiscountID != 0)
            {
                prmStockTransfer.Add(new SqlParameter("@GrandDiscountID", MobjclsDTOWarehouseTransfer.intGrandDiscountID));
                prmStockTransfer.Add(new SqlParameter("@GrandDiscountAmount", MobjclsDTOWarehouseTransfer.decGrandDiscountAmount));
            }
            else
                prmStockTransfer.Add(new SqlParameter("@GrandDiscountAmount", 0.ToDecimal()));

            prmStockTransfer.Add(new SqlParameter("@CurrencyID", MobjclsDTOWarehouseTransfer.intCurrencyID));

            if (MobjclsDTOWarehouseTransfer.decExpenseAmount > 0)
                prmStockTransfer.Add(new SqlParameter("@ExpenseAmount", MobjclsDTOWarehouseTransfer.decExpenseAmount));

            prmStockTransfer.Add(new SqlParameter("@GrandAmount", MobjclsDTOWarehouseTransfer.decGrandAmount));
            prmStockTransfer.Add(new SqlParameter("@NetAmount", MobjclsDTOWarehouseTransfer.decNetAmount));
            prmStockTransfer.Add(new SqlParameter("@NetAmountRounded", MobjclsDTOWarehouseTransfer.decNetAmountRounded));
            prmStockTransfer.Add(new SqlParameter("@CreatedBy", MobjclsDTOWarehouseTransfer.intCreatedBy));
            prmStockTransfer.Add(new SqlParameter("@CreatedDate", MobjclsDTOWarehouseTransfer.strCreatedDate));
          
            if (MobjclsDTOWarehouseTransfer.intApprovedBy > 0)
            {
                prmStockTransfer.Add(new SqlParameter("@ApprovedBy", MobjclsDTOWarehouseTransfer.intApprovedBy));
                prmStockTransfer.Add(new SqlParameter("@ApprovedDate", MobjclsDTOWarehouseTransfer.strApprovedDate));
                prmStockTransfer.Add(new SqlParameter("@VerificationDescription", MobjclsDTOWarehouseTransfer.strVerificationDescription));
            }

            prmStockTransfer.Add(new SqlParameter("@CompanyID", MobjclsDTOWarehouseTransfer.intCompanyID));
            prmStockTransfer.Add(new SqlParameter("@StatusID", MobjclsDTOWarehouseTransfer.intStatusID));
            

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmStockTransfer.Add(objParam);

            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvStockTransfer", prmStockTransfer, out objOutStockTransferID) != 0)
            {
                MobjclsDTOWarehouseTransfer.intStockTransferID=(int)objOutStockTransferID;
                SaveStockTransferDetails();
            }
            return true;


        }
        public bool SaveStockTransferDetails()
        {
            object objoutRowsAffected = 0;
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 5));
            prmStockTransfer.Add(new SqlParameter("@StockTransferID", MobjclsDTOWarehouseTransfer.intStockTransferID));
            prmStockTransfer.Add(new SqlParameter("@XmlTransferDetails", MobjclsDTOWarehouseTransfer.lstObjClsDTOWarehouseTransferDet.ToXml()));//Saving Det
            prmStockTransfer.Add(new SqlParameter("@CompanyID", MobjclsDTOWarehouseTransfer.intCompanyID));
            prmStockTransfer.Add(new SqlParameter("@OrderDate", MobjclsDTOWarehouseTransfer.strOrderDate));
            prmStockTransfer.Add(new SqlParameter("@CurrencyID", MobjclsDTOWarehouseTransfer.intCurrencyID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmStockTransfer.Add(objParam);

            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvStockTransfer", prmStockTransfer, out objoutRowsAffected) != 0)
            {

            }
            return ((int)objoutRowsAffected>0);
            
        }
        public bool DeleteStockTransferInfo()
        {
            //function for deleting StockTransfer Info 

            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 4));
            prmStockTransfer.Add(new SqlParameter("@StockTransferID", MobjclsDTOWarehouseTransfer.intStockTransferID));
            prmStockTransfer.Add(new SqlParameter("@StatusID", MobjclsDTOWarehouseTransfer.intStatusID));
            MobjDataLayer.ExecuteNonQuery("spInvStockTransfer", prmStockTransfer);
            return true;
        }
        public bool DeleteStockTransferDetails()
        {//Function For Deleting Stock Transfer Details Info
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 11));
            prmStockTransfer.Add(new SqlParameter("@StockTransferID", MobjclsDTOWarehouseTransfer.intStockTransferID));
            MobjDataLayer.ExecuteNonQuery("spInvStockTransfer", prmStockTransfer);
            return true;
        }
        private bool DeleteLeadTimeDetails()
        {
            // DELETE Lead Time Details
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 10));
            prmStockTransfer.Add(new SqlParameter("@LeadTimeDetsID", MobjclsDTOWarehouseTransfer.intLeadTimeDetsID));
            if (MobjDataLayer.ExecuteNonQuery("spInvStockTransfer", prmStockTransfer) != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            // END
        }

        public Int64 GetLastStockTransferNo(int intCompanyID)
        {
            Int64 intRetValue = 0;
            string strProcedureName = string.Empty;
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 9));
            prmStockTransfer.Add(new SqlParameter("@CompanyID", intCompanyID));

            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spInvStockTransfer", prmStockTransfer);
            if (sdr.Read())
            {
                if (sdr["ReturnValue"] != DBNull.Value)
                 intRetValue = Convert.ToInt64(sdr["ReturnValue"]);
                else   
                    intRetValue = 0;
            }
            sdr.Close();
            return intRetValue;
        }

        public string DisplayAddressInformation(int iVendorAdd)
        {
            string sAddress = "";
            ArrayList prmVendor = new ArrayList();

            prmVendor.Add(new SqlParameter("@Mode", 8));
            prmVendor.Add(new SqlParameter("@VendorAddID", iVendorAdd));
            SqlDataReader drVendor = MobjDataLayer.ExecuteReader("spInvStockTransfer", prmVendor, CommandBehavior.SingleRow);

            if (drVendor.Read())
            {
                sAddress = Environment.NewLine;
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["ContactPerson"])))
                {
                    sAddress += Convert.ToString(drVendor["ContactPerson"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Address"])))
                {
                    sAddress += Convert.ToString(drVendor["Address"]);
                    sAddress += Environment.NewLine;
                }

                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["State"])))
                {
                    sAddress += Convert.ToString(drVendor["State"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Country"])))
                {
                    sAddress += Convert.ToString(drVendor["Country"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["ZipCode"])))
                {
                    sAddress += "ZipCode : " + Convert.ToString(drVendor["ZipCode"]);
                    sAddress += Environment.NewLine;
                }
                
                drVendor.Close();
                return sAddress;

            }
            else
            {
                drVendor.Close();
                return sAddress;
            }

        }

        private int InsertLeadTimeDetails()
        {
            // Insert Lead Time Details
            object objOutLeadTimeID = 0;
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 6));
            prmStockTransfer.Add(new SqlParameter("@LeadTime", MobjclsDTOWarehouseTransfer.decLeadTime));
            prmStockTransfer.Add(new SqlParameter("@ProductionDate", MobjclsDTOWarehouseTransfer.strProductionDate));
            prmStockTransfer.Add(new SqlParameter("@TranshipmentDate", MobjclsDTOWarehouseTransfer.strTranshipmentDate));
            prmStockTransfer.Add(new SqlParameter("@ClearenceDate", MobjclsDTOWarehouseTransfer.strClearenceDate));
            SqlParameter objParam1 = new SqlParameter("ReturnLeadTimeID", SqlDbType.Int);
            objParam1.Direction = ParameterDirection.ReturnValue;
            prmStockTransfer.Add(objParam1);
            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvStockTransfer", prmStockTransfer, out objOutLeadTimeID) != 0)
            {
                MobjclsDTOWarehouseTransfer.intLeadTimeDetsID = (int)objOutLeadTimeID;
            }
            // END
            return (int)objOutLeadTimeID;
        }
        private int UpdateLeadTimeDetails()
        {
            object objOutLeadTimeID = 0;
            prmStockTransfer = new ArrayList();
            // Update Lead Time Details
            prmStockTransfer.Add(new SqlParameter("@Mode", 7));
            prmStockTransfer.Add(new SqlParameter("@LeadTimeDetsID", MobjclsDTOWarehouseTransfer.intLeadTimeDetsID));
            prmStockTransfer.Add(new SqlParameter("@LeadTime", MobjclsDTOWarehouseTransfer.decLeadTime));
            prmStockTransfer.Add(new SqlParameter("@ProductionDate", MobjclsDTOWarehouseTransfer.strProductionDate));
            prmStockTransfer.Add(new SqlParameter("@TranshipmentDate", MobjclsDTOWarehouseTransfer.strTranshipmentDate));
            prmStockTransfer.Add(new SqlParameter("@ClearenceDate", MobjclsDTOWarehouseTransfer.strClearenceDate));
            SqlParameter objParam1 = new SqlParameter("ReturnLeadTimeID", SqlDbType.Int);
            objParam1.Direction = ParameterDirection.ReturnValue;
            prmStockTransfer.Add(objParam1);
            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvStockTransfer", prmStockTransfer, out objOutLeadTimeID) != 0)
            {
                MobjclsDTOWarehouseTransfer.intLeadTimeDetsID = (int)objOutLeadTimeID;
            }
            // END
            return (int)objOutLeadTimeID;
        }

        public DataTable SearchStockTransfers(string strSearchCondition)
        {
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 13));
            prmStockTransfer.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return MobjDataLayer.ExecuteDataTable("spInvStockTransfer", prmStockTransfer);

        }

        public bool UpdateStockTransferStatus()
        {//updating stockTarnsfer master
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 14));
            prmStockTransfer.Add(new SqlParameter("@StockTransferID",MobjclsDTOWarehouseTransfer.intStockTransferID));
            prmStockTransfer.Add(new SqlParameter("@StatusID", MobjclsDTOWarehouseTransfer.intStatusID));
            if (MobjclsDTOWarehouseTransfer.strVerificationDescription != "")
            prmStockTransfer.Add(new SqlParameter("@VerificationDescription", MobjclsDTOWarehouseTransfer.strVerificationDescription));
             if(MobjclsDTOWarehouseTransfer.intApprovedBy>0)
            prmStockTransfer.Add(new SqlParameter("@ApprovedBy", MobjclsDTOWarehouseTransfer.intApprovedBy));
             if (!string.IsNullOrEmpty(MobjclsDTOWarehouseTransfer.strApprovedDate))
             prmStockTransfer.Add(new SqlParameter("@ApprovedDate", MobjclsDTOWarehouseTransfer.strApprovedDate.ToDateTime().ToString("dd-MMM-yyyy")));
            

            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvStockTransfer", prmStockTransfer) != 0)
            {
                if (MobjclsDTOWarehouseTransfer.intStatusID == (int)OperationStatusType.STCancelled || MobjclsDTOWarehouseTransfer.intStatusID == (int)OperationStatusType.STOpened)
                {
                    prmStockTransfer = new ArrayList();
                    prmStockTransfer.Add(new SqlParameter("@Mode", 23));
                    prmStockTransfer.Add(new SqlParameter("@StockTransferID", MobjclsDTOWarehouseTransfer.intStockTransferID));
                    prmStockTransfer.Add(new SqlParameter("@StatusID", MobjclsDTOWarehouseTransfer.intStatusID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spInvStockTransfer",prmStockTransfer);
                }
                return true;
            }
            return false;
        }

        public bool Suggest(int intOperationTypeID)
        {// Verification History insertion when suggest or deny
           // intOperationTypeID = 32;//Stock Transfer
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 15));
            prmStockTransfer.Add(new SqlParameter("@ReferenceID", MobjclsDTOWarehouseTransfer.intStockTransferID));
            prmStockTransfer.Add(new SqlParameter("@Description", MobjclsDTOWarehouseTransfer.strDescription));
            prmStockTransfer.Add(new SqlParameter("@StatusID", MobjclsDTOWarehouseTransfer.intStatusID));
            prmStockTransfer.Add(new SqlParameter("@CancelledBy", MobjclsDTOWarehouseTransfer.intCancelledBy));
            prmStockTransfer.Add(new SqlParameter("@CancellationDate", MobjclsDTOWarehouseTransfer.strCancellationDate));
            if (MobjDataLayer.ExecuteNonQuery("spInvStockTransfer", prmStockTransfer) != 0)
            {
                return true;
            }
            return false;
        }
        public bool SaveCancellationDetails()
        {//Insert Cancellation details
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 17));
            prmStockTransfer.Add(new SqlParameter("@StockTransferID", MobjclsDTOWarehouseTransfer.intStockTransferID));
            prmStockTransfer.Add(new SqlParameter("@CancellationDate", MobjclsDTOWarehouseTransfer.strCancellationDate));
            prmStockTransfer.Add(new SqlParameter("@Description", MobjclsDTOWarehouseTransfer.strDescription));
            prmStockTransfer.Add(new SqlParameter("@CancelledBy", MobjclsDTOWarehouseTransfer.intCancelledBy));
            prmStockTransfer.Add(new SqlParameter("@CompanyID", MobjclsDTOWarehouseTransfer.intCompanyID));
            if (MobjDataLayer.ExecuteNonQuery("spInvStockTransfer", prmStockTransfer) != 0)
            {
                return true;
            }
            return false;
        }
        public bool DeleteCancellationDetails()
        {//Delete Stock Transfer Cancellation Details

            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 18));
            prmStockTransfer.Add(new SqlParameter("@StockTransferID", MobjclsDTOWarehouseTransfer.intStockTransferID));
            if (MobjDataLayer.ExecuteNonQuery("spInvStockTransfer", prmStockTransfer) != 0)
            {
                return true;
            }
            return false;
        }
       //---------------------------------------------------------------------------------------------


       private DataTable DisplayCancellationDetails()
        {
            //function for displaying cancellation details

            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 21));
                    prmStockTransfer.Add(new SqlParameter("@StockTransferID",MobjclsDTOWarehouseTransfer.intStockTransferID));
                    return MobjDataLayer.ExecuteDataTable("spInvStockTransfer", prmStockTransfer);
            }

        public DataTable GetItemUOMs(int intItemID)
        {
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 10));
            prmStockTransfer.Add(new SqlParameter("@ItemID", intItemID));
       //     prmStockTransfer.Add(new SqlParameter("@UomTypeID", 2));
            DataTable dtItemUoms = new DataTable();
            dtItemUoms = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmStockTransfer);

            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 12));
            prmStockTransfer.Add(new SqlParameter("@ItemID", intItemID));
            DataTable dtItemBaseUnit = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmStockTransfer);
            DataRow dr = dtItemUoms.NewRow();
            dr["UOMID"] = dtItemBaseUnit.Rows[0]["UOMID"];
            dr["ShortName"] = dtItemBaseUnit.Rows[0]["ShortName"];
            dr["ItemID"] = intItemID;
            dtItemUoms.Rows.InsertAt(dr, 0);
            return dtItemUoms;
        }
        public DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 11));
            prmStockTransfer.Add(new SqlParameter("@ItemID", intItemID));
            prmStockTransfer.Add(new SqlParameter("@UnitID", intUOMID));
           // prmStockTransfer.Add(new SqlParameter("@UomTypeID", 1));
            return MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmStockTransfer);
        }

        public DataTable GetItemWiseDiscountDetails(int intItemID, decimal decQty, decimal decRate, string strCurrentDate,int intCurrencyID)
        {
            DataTable dtDiscounts = new DataTable();
            prmStockTransfer = new ArrayList();
            if (!string.IsNullOrEmpty(strCurrentDate))
            {
                prmStockTransfer.Add(new SqlParameter("@Mode", 26));
            }
            else
            {
              
                prmStockTransfer.Add(new SqlParameter("@Mode", 27));
            }

            prmStockTransfer.Add(new SqlParameter("@ItemID", intItemID));
            prmStockTransfer.Add(new SqlParameter("@Quantity", decQty));
            prmStockTransfer.Add(new SqlParameter("@Rate", decRate));
            prmStockTransfer.Add(new SqlParameter("@CurrentDate", strCurrentDate));
            prmStockTransfer.Add(new SqlParameter("@CurrencyID", intCurrencyID));
            return MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmStockTransfer);
            
        }
        public bool IsDiscountForAmount(int intDiscountID)
        {
            bool blnIsDiscountForAmount = false;
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 15));
            prmStockTransfer.Add(new SqlParameter("@DiscountID", intDiscountID));
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spInvPurchaseModuleFunctions", prmStockTransfer);
            if (sdr.Read())
            {
                if (sdr["DiscountForAmount"] != DBNull.Value)
                    blnIsDiscountForAmount = Convert.ToBoolean(sdr["DiscountForAmount"]);
            }
            sdr.Close();
            return blnIsDiscountForAmount;
        }
        public double GetItemDiscountAmount(int iMode, int iItemID, int DiscountID, double Rate)
        {

            double dOutItemNetAmount = 0;
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 3));
            prmStockTransfer.Add(new SqlParameter("@ItemID", iItemID));
            prmStockTransfer.Add(new SqlParameter("@DiscountID", DiscountID));
            prmStockTransfer.Add(new SqlParameter("@Rate", Rate));

            SqlDataReader sReader = MobjDataLayer.ExecuteReader("spInvPurchaseModuleFunctions", prmStockTransfer, CommandBehavior.SingleRow);
            if (sReader.Read())
            {
                if (sReader[0] != DBNull.Value)
                {
                    dOutItemNetAmount = Convert.ToDouble(sReader[0]);
                }
                else
                {
                    dOutItemNetAmount = 0;
                }


                if (dOutItemNetAmount >= 0)
                {
                    sReader.Close();
                    return dOutItemNetAmount;
                }
            }
            sReader.Close();
            return dOutItemNetAmount;

        }
        public bool GetExchangeCurrencyRate(int intCompanyID, int intCurrencyID)
        {
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 19));
            prmStockTransfer.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmStockTransfer.Add(new SqlParameter("@CurrencyID", intCurrencyID));

            DataTable datExchangeCurrencyRate = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmStockTransfer);

            if (datExchangeCurrencyRate.Rows.Count > 0)
            {
                if (datExchangeCurrencyRate.Rows[0]["Exchangerate"] != DBNull.Value)
                    MobjclsDTOWarehouseTransfer.decExchangeCurrencyRate = Convert.ToDecimal(datExchangeCurrencyRate.Rows[0]["Exchangerate"]);
            }

            return (MobjclsDTOWarehouseTransfer.decExchangeCurrencyRate >= 0);

        }

        public string GetCompanyCurrency(int intCompanyID, out int intScale,out int intCurrencyID)
        {
            intScale = 2;
            intCurrencyID = 0;
            string strCurrency = "";
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@Mode", 20));
            prmStockTransfer.Add(new SqlParameter("@CompanyID", intCompanyID));
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spInvPurchaseModuleFunctions", prmStockTransfer);
            if (sdr.Read())
            {
                if (sdr["Currency"] != DBNull.Value)
                {
                    strCurrency = Convert.ToString(sdr["Currency"]);
                }
                if (sdr["Scale"] != DBNull.Value)
                {
                    intScale = Convert.ToInt32(sdr["Scale"]);
                }
                if (sdr["CurrencyID"] != DBNull.Value)
                {
                    intCurrencyID = sdr["CurrencyID"].ToInt32();
                }
            }
            return strCurrency;
        }
        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 4));
            prmCommon.Add(new SqlParameter("@RoleID", intRoleID));
            prmCommon.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmCommon.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", prmCommon);
        }

        public DataSet GetStockTransferReport()
        {
            ArrayList prmStock = new ArrayList();

            prmStock = new ArrayList();
            prmStock.Add(new SqlParameter("@Mode", 16));
            prmStock.Add(new SqlParameter("@StockTransferID", MobjclsDTOWarehouseTransfer.intStockTransferID));
            return MobjDataLayer.ExecuteDataSet("spInvStockTransfer", prmStock);

        }

        public DataTable GetOperationTypeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", parameters);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", parameters);
        }

        public bool UpdateGrandTotalAmount()
        {
            prmStockTransfer = new ArrayList();
            prmStockTransfer.Add(new SqlParameter("@GrandDiscountAmount", MobjclsDTOWarehouseTransfer.decGrandDiscountAmount));
            prmStockTransfer.Add(new SqlParameter("@GrandAmount", MobjclsDTOWarehouseTransfer.decGrandAmount));
            prmStockTransfer.Add(new SqlParameter("@NetAmount", MobjclsDTOWarehouseTransfer.decNetAmount));
            prmStockTransfer.Add(new SqlParameter("@NetAmountRounded", MobjclsDTOWarehouseTransfer.decNetAmountRounded));
            prmStockTransfer.Add(new SqlParameter("@ExpenseAmount", MobjclsDTOWarehouseTransfer.decExpenseAmount));
            prmStockTransfer.Add(new SqlParameter("@Mode", 19));
            prmStockTransfer.Add(new SqlParameter("@StockTransferID", MobjclsDTOWarehouseTransfer.intStockTransferID));
            MobjDataLayer.ExecuteNonQuery("spInvStockTransfer", prmStockTransfer);
            return true;
        }

        public DataTable GetDataForItemSelection(int intCompanyID, int intCurrencyID,int intWarehouseId,bool blnWarehouseToWarehouse)
        {
            prmStockTransfer = new ArrayList();
            if (blnWarehouseToWarehouse)
                prmStockTransfer.Add(new SqlParameter("@Mode", 20));
            else
                prmStockTransfer.Add(new SqlParameter("@Mode", 24));

            prmStockTransfer.Add(new SqlParameter("@OrderDate", ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")));
            prmStockTransfer.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmStockTransfer.Add(new SqlParameter("@CurrencyID", intCurrencyID));
            prmStockTransfer.Add(new SqlParameter("@WarehouseID", intWarehouseId));
            return MobjDataLayer.ExecuteDataTable("spInvStockTransfer", prmStockTransfer);
        }

        public bool UpdationVerificationTableRemarks(Int64 intReferenceID, string strRemarks)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 22));
            alParameters.Add(new SqlParameter("@StockTransferID", intReferenceID));
            alParameters.Add(new SqlParameter("@CancelledBy", ClsCommonSettings.UserID));
            alParameters.Add(new SqlParameter("@Remarks", strRemarks));
            MobjDataLayer.ExecuteNonQuery("spInvStockTransfer", alParameters);
            return true;
        }

        public bool IsReferenceExistsInReceipt(Int64 intReferenceID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 25));
            alParameters.Add(new SqlParameter("@StockTransferID", intReferenceID));
            object objRetValue = MobjDataLayer.ExecuteScalar("spInvStockTransfer", alParameters);
            return objRetValue.ToInt64() > 0;
        }
        public bool IsDuplicateStockTranferNo(Int64 intCompanyID, string strStockTransferNo)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 26));
            alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            alParameters.Add(new SqlParameter("@StockTransferNo", strStockTransferNo));
            object objRetValue = MobjDataLayer.ExecuteScalar("spInvStockTransfer", alParameters);
            return objRetValue.ToInt64() > 0;
        }
        public bool IsValidStockTransferID(Int64 intCompanyID, Int64 intStockTransferID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 27));
            alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            alParameters.Add(new SqlParameter("@StockTransferID", intStockTransferID));
            object objRetValue = MobjDataLayer.ExecuteScalar("spInvStockTransfer", alParameters);
            return objRetValue.ToInt64() > 0;
        }
        public bool ExistCurrencyReference(int FromWarehouse, int ToWarehouse)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 28));
            alParameters.Add(new SqlParameter("@WarehouseID", FromWarehouse));
            alParameters.Add(new SqlParameter("@ReferenceID", ToWarehouse));
            object objRetValue = MobjDataLayer.ExecuteScalar("spInvStockTransfer", alParameters);
            return objRetValue.ToInt64() > 0;
        }
   }//Class
}//namespace
