﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;


namespace MyBooksERP
{
    public class clsDALSummary
    {
        public DataLayer MobjDataLayer = null;
        ArrayList prmSqlParameters = null;
        public clsDTOSummary objclsDTOSummary { get; set; }

        public clsDALSummary(DataLayer objDataLayer)
        {
            //constructor
            MobjDataLayer = objDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = MobjDataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataTable GetCompanyByPermission(int RoleID, int MenuID, int ControlID)
        {
            prmSqlParameters = new ArrayList();
            prmSqlParameters.Add(new SqlParameter("@Mode", 4));
            prmSqlParameters.Add(new SqlParameter("@RoleID", RoleID));
            prmSqlParameters.Add(new SqlParameter("@MenuID", MenuID));
            prmSqlParameters.Add(new SqlParameter("@ControlID", ControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", prmSqlParameters);
        }

        public DataTable GetStockDetails()   
        {
            prmSqlParameters = new ArrayList();
            prmSqlParameters.Add(new SqlParameter("@SummaryReportType", objclsDTOSummary.SummaryReportType));
            prmSqlParameters.Add(new SqlParameter("@CompanyID", objclsDTOSummary.CompanyID));
            prmSqlParameters.Add(new SqlParameter("@WarehouseID", objclsDTOSummary.WarehouseID));
            prmSqlParameters.Add(new SqlParameter("@SummaryType", objclsDTOSummary.SummaryType));
            prmSqlParameters.Add(new SqlParameter("@ParameterFromDate", objclsDTOSummary.ParameterFromDate.Date.ToString("dd MMM yyyy")));
            prmSqlParameters.Add(new SqlParameter("@ParameterToDate", objclsDTOSummary.ParameterToDate.Date.ToString("dd MMM yyyy")));
            prmSqlParameters.Add(new SqlParameter("@SelectFields", objclsDTOSummary.SelectFields));
            prmSqlParameters.Add(new SqlParameter("@SearchCondition", objclsDTOSummary.SearchCondition));
            prmSqlParameters.Add(new SqlParameter("@GroupByCondition", objclsDTOSummary.GroupByCondition));
            return MobjDataLayer.ExecuteDataTable("spInvSummary", prmSqlParameters);
        }
    }
}
