﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
   public class ClsDALLocationTransfer
    {


        ArrayList arrlstPrmItemLocation;

        public clsDTOLLocationTransfer clsDTOLLocationTransfer { get; set; }
        public DataLayer MobjclsConnection { get; set; }
        private string MstrProcName = "STItemLocationTransfer"; //ProcedureName
       
       
       //---------Save and Update---------------------------------------

        public bool SaveItemMovements(bool blnStaus, bool blnDamagedQty)
        {
           //intType=1-Actaul Qty,inttype=2-Dameged Qty
            int intType = 1;
            if (blnDamagedQty)
            {
                intType = 2;
            }
                arrlstPrmItemLocation = new ArrayList();
                if (blnStaus)//insert
                {
                    arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 1));//2 - Insert
                }
                else//update
                {
                    arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 2));//2 - updating
                }

                arrlstPrmItemLocation.Add(new SqlParameter("@CompanyID", clsDTOLLocationTransfer.intCompanyID));
                arrlstPrmItemLocation.Add(new SqlParameter("@WarehouseID", clsDTOLLocationTransfer.intWarehouseID));
                arrlstPrmItemLocation.Add(new SqlParameter("@LocationID", clsDTOLLocationTransfer.intLocationID));
                arrlstPrmItemLocation.Add(new SqlParameter("@RowID", clsDTOLLocationTransfer.intRowID));
                arrlstPrmItemLocation.Add(new SqlParameter("@BlockID", clsDTOLLocationTransfer.intBlockID));
                arrlstPrmItemLocation.Add(new SqlParameter("@LotID", clsDTOLLocationTransfer.intLotID));
                arrlstPrmItemLocation.Add(new SqlParameter("@ItemID", clsDTOLLocationTransfer.intItemID));
                arrlstPrmItemLocation.Add(new SqlParameter("@BatchID", clsDTOLLocationTransfer.intBatchID));
                if (intType == 1)
                {
                    arrlstPrmItemLocation.Add(new SqlParameter("@Quantity", clsDTOLLocationTransfer.decQuantity));//Actaul Qty
                }
                else if (intType == 2)
                {
                    arrlstPrmItemLocation.Add(new SqlParameter("@DamagedQuantity", clsDTOLLocationTransfer.decQuantity));//Dameged Qty
                }
                arrlstPrmItemLocation.Add(new SqlParameter("@Type", intType));
                MobjclsConnection.ExecuteNonQuery(MstrProcName, arrlstPrmItemLocation);
            
            return true;
        }
       //-- Filtertion Item-------
               
        public DataTable GetItemLocationDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID,bool blnDamagedQty)
        {
            string strCondition = "";

           if (intWarehouseID>0)
            {
                strCondition = " [STItemLocation].WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LocationID=" + intLocID.ToString();
            }
            if(intRowID>0)
            {
                strCondition = strCondition + " and [STItemLocation].RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LotID=" + intLotID.ToString();
            }
            if (intItemID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].ItemID=" + intItemID.ToString();
            }
            if (intBatchID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BatchID=" + intBatchID.ToString();
            }
            if (blnDamagedQty == true)
            {
                strCondition = strCondition + " and isnull([STItemLocation].DamagedQuantity,0) > 0 ";
            }
            else
            {
                strCondition = strCondition + " and isnull([STItemLocation].Quantity,0) > 0 "; 
            }

            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 3));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetItemRowDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID, bool blnDamagedQty)
        {
            string strCondition = "";

            if (intWarehouseID > 0)
            {
                strCondition = " [STItemLocation].WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LotID=" + intLotID.ToString();
            }
            if (intItemID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].ItemID=" + intItemID.ToString();
            }
            if (intBatchID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BatchID=" + intBatchID.ToString();
            }
            if (blnDamagedQty == true)
            {
                strCondition = strCondition + " and isnull([STItemLocation].DamagedQuantity,0) > 0 ";
            }
            else
            {
                strCondition = strCondition + " and isnull([STItemLocation].Quantity,0) > 0 ";
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 4));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetItemBlockDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID, bool blnDamagedQty)
        {
            string strCondition = "";

            if (intWarehouseID > 0)
            {
                strCondition = " [STItemLocation].WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LotID=" + intLotID.ToString();
            }
            if (intItemID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].ItemID=" + intItemID.ToString();
            }
            if (intBatchID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BatchID=" + intBatchID.ToString();
            }
            if (blnDamagedQty == true)
            {
                strCondition = strCondition + " and isnull([STItemLocation].DamagedQuantity,0) > 0 ";
            }
            else
            {
                strCondition = strCondition + " and isnull([STItemLocation].Quantity,0) > 0 ";
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 5));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetItemLotDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID, bool blnDamagedQty)
        {
            string strCondition = "";

            if (intWarehouseID > 0)
            {
                strCondition = " [STItemLocation].WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LotID=" + intLotID.ToString();
            }
            if (intItemID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].ItemID=" + intItemID.ToString();
            }
            if (intBatchID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BatchID=" + intBatchID.ToString();
            }
            if (blnDamagedQty == true)
            {
                strCondition = strCondition + " and isnull([STItemLocation].DamagedQuantity,0) > 0 ";
            }
            else
            {
                strCondition = strCondition + " and isnull([STItemLocation].Quantity,0) > 0 ";
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 6));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetItemItemDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID, bool blnDamagedQty)
        {
            string strCondition = "";

            if (intWarehouseID > 0)
            {
                strCondition = " [STItemLocation].WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LotID=" + intLotID.ToString();
            }
            if (intItemID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].ItemID=" + intItemID.ToString();
            }
            if (intBatchID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BatchID=" + intBatchID.ToString();
            }
            if (blnDamagedQty == true)
            {
                strCondition = strCondition + " and isnull([STItemLocation].DamagedQuantity,0) > 0 ";
            }
            else
            {
                strCondition = strCondition + " and isnull([STItemLocation].Quantity,0) > 0 ";
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 7));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetItemBatchDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID, bool blnDamagedQty)
        {
            string strCondition = "";
            int intType = 1;
            if (intWarehouseID > 0)
            {
                strCondition = " [STItemLocation].WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LotID=" + intLotID.ToString();
            }
            if (intItemID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].ItemID=" + intItemID.ToString();
            }
            if (intBatchID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BatchID=" + intBatchID.ToString();
            }
            if (blnDamagedQty == true)
            {
                strCondition = strCondition + " and isnull([STItemLocation].DamagedQuantity,0) > 0 ";
                intType = 2;
            }
            else
            {
                strCondition = strCondition + " and isnull([STItemLocation].Quantity,0) > 0 ";
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 8));
            arrlstPrmItemLocation.Add(new SqlParameter("@Type", intType));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
     //-- Filter WareHouse

        public DataTable GetWarehouseLocInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID,int intLotID)
        {
            
			string strCondition="";
            if (intWarehouseID > 0)
            {
                strCondition = " WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and  LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and LotID=" + intLotID.ToString();
            }

            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 9));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
            
        }
        public DataTable GetWarehouseRowInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)
        {
            string strCondition = "";
            if (intWarehouseID > 0)
            {
                strCondition = " WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and  LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and LotID=" + intLotID.ToString();
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 10));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetWarehouseBlockInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)
        {
            string strCondition = "";
            if (intWarehouseID > 0)
            {
                strCondition = " WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and  LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and LotID=" + intLotID.ToString();
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 11));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetWarehouseLotInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)
        {
            string strCondition = "";
            if (intWarehouseID > 0)
            {
                strCondition = " WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and  LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and LotID=" + intLotID.ToString();
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 12));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
       //------fill combo-----------
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = MobjclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 15));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, parameters);
        }
    }
}
/* old code
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
   public class ClsDALLocationTransfer
    {


        ArrayList arrlstPrmItemLocation;

        public clsDTOLLocationTransfer clsDTOLLocationTransfer { get; set; }
        public DataLayer MobjclsConnection { get; set; }
        private string MstrProcName = "STItemLocationTransfer"; //ProcedureName

        public DataTable DisplayLocationInfo(int intWarehouseID)//Selecting Locations
        {
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 1));
            arrlstPrmItemLocation.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
            
        }
        public DataTable DisplayRowinfo(int intWarehouseID,  int intLocID)//Getting Row Details
        {

            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 2));
            arrlstPrmItemLocation.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            arrlstPrmItemLocation.Add(new SqlParameter("@LocationID", intLocID));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
            
        }
        public DataTable DisplayBlockInfo(int intWarehouseID,  int intLocID,  int intRowID)//Blocks
        {

            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 3));
            arrlstPrmItemLocation.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            arrlstPrmItemLocation.Add(new SqlParameter("@LocationID", intLocID));
            arrlstPrmItemLocation.Add(new SqlParameter("@RowID", intRowID));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
          }
        public DataTable DisplayLotInfo(int intWarehouseID,  int intLocID, int intRowID, int intBlockID)//Selecting Lots
        {
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 4));
            arrlstPrmItemLocation.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            arrlstPrmItemLocation.Add(new SqlParameter("@LocationID", intLocID));
            arrlstPrmItemLocation.Add(new SqlParameter("@RowID", intRowID));
            arrlstPrmItemLocation.Add(new SqlParameter("@BlockID", intBlockID));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
            
        }
        public DataTable DisplayItemInfo(int intWarehouseID, int intLocID, int intRowID, int intBlockID,int intLotID)//Selecting Lots
        {
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 5));
            arrlstPrmItemLocation.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            arrlstPrmItemLocation.Add(new SqlParameter("@LocationID", intLocID));
            arrlstPrmItemLocation.Add(new SqlParameter("@RowID", intRowID));
            arrlstPrmItemLocation.Add(new SqlParameter("@BlockID", intBlockID));
            arrlstPrmItemLocation.Add(new SqlParameter("@LotID", intLotID));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);

        }
        public DataTable DisplayBatchInfo(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID,int intItemID)//Selecting Lots
        {
            // Selecting Batch Details
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 6));
            arrlstPrmItemLocation.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            arrlstPrmItemLocation.Add(new SqlParameter("@LocationID", intLocID));
            arrlstPrmItemLocation.Add(new SqlParameter("@RowID", intRowID));
            arrlstPrmItemLocation.Add(new SqlParameter("@BlockID", intBlockID));
            arrlstPrmItemLocation.Add(new SqlParameter("@LotID", intLotID));
            arrlstPrmItemLocation.Add(new SqlParameter("@ItemID", intItemID));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);

        }
       // Warehouse Details........................................
        public DataTable DisplayWarehouseInformationDetail(int pWarehouseID)
        {
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 7));
            arrlstPrmItemLocation.Add(new SqlParameter("@WarehouseID", pWarehouseID));
            return MobjclsConnection.ExecuteDataTable("STWarehouseMasterTransactions", arrlstPrmItemLocation);
            //return true;
        }
        public DataTable DisplayWarehouseInformationDetailRow(int pWarehouseID, string psLocation, int LocID)
        {

            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 11));
            arrlstPrmItemLocation.Add(new SqlParameter("@WarehouseID", pWarehouseID));
            arrlstPrmItemLocation.Add(new SqlParameter("@Location", psLocation));
            arrlstPrmItemLocation.Add(new SqlParameter("@LocationID", LocID));
            return MobjclsConnection.ExecuteDataTable("STWarehouseMasterTransactions", arrlstPrmItemLocation);
            //return true;
        }
        public DataTable DisplayWarehouseInformationDetailBLOCK(int pWarehouseID, string psLocation, int LocID, string psRows, int RowID)
        {

            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 12));
            arrlstPrmItemLocation.Add(new SqlParameter("@WarehouseID", pWarehouseID));
            arrlstPrmItemLocation.Add(new SqlParameter("@Location", psLocation));
            arrlstPrmItemLocation.Add(new SqlParameter("@LocationID", LocID));
            arrlstPrmItemLocation.Add(new SqlParameter("@RowNumbers", psRows));
            arrlstPrmItemLocation.Add(new SqlParameter("@RowID", RowID));
            return MobjclsConnection.ExecuteDataTable("STWarehouseMasterTransactions", arrlstPrmItemLocation);
            //return true;
        }
        public DataTable DisplayWarehouseInformationDetailLOTS(int pWarehouseID, string psLocation, int LocID, string psRows, int RowID, string psBlocks, int BlockID)
        {
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 13));
            arrlstPrmItemLocation.Add(new SqlParameter("@WarehouseID", pWarehouseID));
            arrlstPrmItemLocation.Add(new SqlParameter("@Location", psLocation));
            arrlstPrmItemLocation.Add(new SqlParameter("@LocationID", LocID));
            arrlstPrmItemLocation.Add(new SqlParameter("@RowNumbers", psRows));
            arrlstPrmItemLocation.Add(new SqlParameter("@RowID", RowID));
            arrlstPrmItemLocation.Add(new SqlParameter("@BlockNumber", psBlocks));
            arrlstPrmItemLocation.Add(new SqlParameter("@BlockID", BlockID));
            return MobjclsConnection.ExecuteDataTable("STWarehouseMasterTransactions", arrlstPrmItemLocation);
            //return true;
        }
       //---------Save and Update---------------------------------------
        public bool Save(bool blnStaus)
        {
            foreach (ClsDTOItemLocation objClsClsDTOItemLocation in clsDTOLLocationTransfer.lstClsDTOItemLocation)
            {
                arrlstPrmItemLocation = new ArrayList();
                if (blnStaus)//insert
                {
                    arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 7));//2 - Insert
                }
                else//update
                {
                    arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 8));//2 - updating
                }

                arrlstPrmItemLocation.Add(new SqlParameter("@CompanyID", clsDTOLLocationTransfer.intCompanyID));
                arrlstPrmItemLocation.Add(new SqlParameter("@WarehouseID", clsDTOLLocationTransfer.intWarehouseID));
                arrlstPrmItemLocation.Add(new SqlParameter("@LocationID", objClsClsDTOItemLocation.intLocationID));
                arrlstPrmItemLocation.Add(new SqlParameter("@RowID", objClsClsDTOItemLocation.intRowID));
                arrlstPrmItemLocation.Add(new SqlParameter("@BlockID", objClsClsDTOItemLocation.intBlockID));
                arrlstPrmItemLocation.Add(new SqlParameter("@LotID", objClsClsDTOItemLocation.intLotID));
                arrlstPrmItemLocation.Add(new SqlParameter("@ItemID", objClsClsDTOItemLocation.intItemID));
                arrlstPrmItemLocation.Add(new SqlParameter("@BatchID", objClsClsDTOItemLocation.intBatchID));
                arrlstPrmItemLocation.Add(new SqlParameter("@Quantity", objClsClsDTOItemLocation.intQuantity));
                MobjclsConnection.ExecuteNonQuery(MstrProcName, arrlstPrmItemLocation);
            }
            return true;
        }
       //-- Filtertion Item-------

        public DataTable GetItemLocationDetailByFilter(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID,int intBatchID)
        {
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 9));
            arrlstPrmItemLocation.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            arrlstPrmItemLocation.Add(new SqlParameter("@LocationID", intLocID));
            arrlstPrmItemLocation.Add(new SqlParameter("@RowID", intRowID));
            arrlstPrmItemLocation.Add(new SqlParameter("@BlockID", intBlockID));
            arrlstPrmItemLocation.Add(new SqlParameter("@LotID", intLotID));
            arrlstPrmItemLocation.Add(new SqlParameter("@ItemID", intItemID));
            arrlstPrmItemLocation.Add(new SqlParameter("@BatchID", intBatchID));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetItemLocationDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID)
        {
            string strCondition = "";

           if (intWarehouseID>0)
            {
                strCondition = " [STItemLocation].WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LocationID=" + intLocID.ToString();
            }
            if(intRowID>0)
            {
                strCondition = strCondition + " and [STItemLocation].RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LotID=" + intLotID.ToString();
            }
            if (intItemID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].ItemID=" + intItemID.ToString();
            }
            if (intBatchID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BatchID=" + intBatchID.ToString();
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 10));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetItemRowDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID)
        {
            string strCondition = "";

            if (intWarehouseID > 0)
            {
                strCondition = " [STItemLocation].WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LotID=" + intLotID.ToString();
            }
            if (intItemID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].ItemID=" + intItemID.ToString();
            }
            if (intBatchID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BatchID=" + intBatchID.ToString();
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 11));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetItemBlockDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID)
        {
            string strCondition = "";

            if (intWarehouseID > 0)
            {
                strCondition = " [STItemLocation].WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LotID=" + intLotID.ToString();
            }
            if (intItemID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].ItemID=" + intItemID.ToString();
            }
            if (intBatchID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BatchID=" + intBatchID.ToString();
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 12));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetItemLotDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID)
        {
            string strCondition = "";

            if (intWarehouseID > 0)
            {
                strCondition = " [STItemLocation].WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LotID=" + intLotID.ToString();
            }
            if (intItemID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].ItemID=" + intItemID.ToString();
            }
            if (intBatchID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BatchID=" + intBatchID.ToString();
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 13));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetItemItemDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID)
        {
            string strCondition = "";

            if (intWarehouseID > 0)
            {
                strCondition = " [STItemLocation].WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LotID=" + intLotID.ToString();
            }
            if (intItemID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].ItemID=" + intItemID.ToString();
            }
            if (intBatchID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BatchID=" + intBatchID.ToString();
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 14));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetItemBatchDetailByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID, int intItemID, int intBatchID)
        {
            string strCondition = "";

            if (intWarehouseID > 0)
            {
                strCondition = " [STItemLocation].WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].LotID=" + intLotID.ToString();
            }
            if (intItemID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].ItemID=" + intItemID.ToString();
            }
            if (intBatchID > 0)
            {
                strCondition = strCondition + " and [STItemLocation].BatchID=" + intBatchID.ToString();
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 15));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
     //-- Filter WareHouse

        public DataTable GetWarehouseLocInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID,int intLotID)
        {
            
			string strCondition="";
            if (intWarehouseID > 0)
            {
                strCondition = " WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and  LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and LotID=" + intLotID.ToString();
            }

            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 16));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
            
        }
        public DataTable GetWarehouseRowInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)
        {
            string strCondition = "";
            if (intWarehouseID > 0)
            {
                strCondition = " WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and  LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and LotID=" + intLotID.ToString();
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 17));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetWarehouseBlockInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)
        {
            string strCondition = "";
            if (intWarehouseID > 0)
            {
                strCondition = " WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and  LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and LotID=" + intLotID.ToString();
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 18));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
        public DataTable GetWarehouseLotInfoByFilterCondition(int intWarehouseID, int intLocID, int intRowID, int intBlockID, int intLotID)
        {
            string strCondition = "";
            if (intWarehouseID > 0)
            {
                strCondition = " WarehouseID=" + intWarehouseID.ToString();
            }
            if (intLocID > 0)
            {
                strCondition = strCondition + " and  LocationID=" + intLocID.ToString();
            }
            if (intRowID > 0)
            {
                strCondition = strCondition + " and RowID=" + intRowID.ToString();
            }
            if (intBlockID > 0)
            {
                strCondition = strCondition + " and BlockID=" + intBlockID.ToString();
            }
            if (intLotID > 0)
            {
                strCondition = strCondition + " and LotID=" + intLotID.ToString();
            }
            arrlstPrmItemLocation = new ArrayList();
            arrlstPrmItemLocation.Add(new SqlParameter("@Mode", 19));
            arrlstPrmItemLocation.Add(new SqlParameter("@Condition", strCondition));
            return MobjclsConnection.ExecuteDataTable(MstrProcName, arrlstPrmItemLocation);
        }
       //------fill combo-----------
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = MobjclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }


    }
}
*/