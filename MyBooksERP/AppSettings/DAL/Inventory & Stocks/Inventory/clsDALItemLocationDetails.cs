﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using System.Data.SqlClient;
using System.Data;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,8 Mar 2011>
   Description:	<Description,,Item Location Details DAL>
================================================
*/
namespace MyBooksERP
{
    public class clsDALItemLocationDetails : IDisposable
    {
        ClsCommonUtility MobjClsCommonUtility; // obj of clsCommonUtility
        DataLayer MobjDataLayer;//obj of datalayer
        ArrayList prmItemLocationDetails;//array list for storing parameters

        public clsDTOItemLocationMaster PobjClsDTOItemLocationMaster { get; set; } //property for clsDTOItemLocationMaster

        public clsDALItemLocationDetails(DataLayer objDataLayer)
        {
            //Constructor
            MobjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MobjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public bool SaveItemLocationDetails()
        {
            //Saving ItemLocationDetails
            foreach (clsDTOItemLocationDetails objClsDTOItemLocationDetails in PobjClsDTOItemLocationMaster.lstItemLocationDetails)
            {
                prmItemLocationDetails = new ArrayList();
                DataTable dtItemLocationDetails = new DataTable();
                prmItemLocationDetails.Add(new SqlParameter("@Mode", 2));
                prmItemLocationDetails.Add(new SqlParameter("@ItemID", objClsDTOItemLocationDetails.intItemID));
                prmItemLocationDetails.Add(new SqlParameter("@BatchID", objClsDTOItemLocationDetails.intBatchID));
                prmItemLocationDetails.Add(new SqlParameter("@WarehouseID", PobjClsDTOItemLocationMaster.intWarehouseID));
                prmItemLocationDetails.Add(new SqlParameter("@LocationID", objClsDTOItemLocationDetails.intLocationID));
                prmItemLocationDetails.Add(new SqlParameter("@RowID", objClsDTOItemLocationDetails.intRowID));
                prmItemLocationDetails.Add(new SqlParameter("@BlockID", objClsDTOItemLocationDetails.intBlockID));
                prmItemLocationDetails.Add(new SqlParameter("@LotID", objClsDTOItemLocationDetails.intLotID));
                prmItemLocationDetails.Add(new SqlParameter("@ReferenceID", PobjClsDTOItemLocationMaster.intReferenceID));
                prmItemLocationDetails.Add(new SqlParameter("@IsGRN", PobjClsDTOItemLocationMaster.blnIsGRN));
                dtItemLocationDetails = MobjDataLayer.ExecuteDataTable("STItemLocationDetail", prmItemLocationDetails);

                if (dtItemLocationDetails.Rows.Count > 0)
                {
                    prmItemLocationDetails.RemoveAt(0);
                    prmItemLocationDetails.Add(new SqlParameter("@Mode", 4));
                    prmItemLocationDetails.Add(new SqlParameter("Quantity", Convert.ToDecimal(dtItemLocationDetails.Rows[0]["Quantity"]) + objClsDTOItemLocationDetails.decQuantity));
                    MobjDataLayer.ExecuteNonQueryWithTran("STItemLocationDetail", prmItemLocationDetails);
                }
                else
                {
                    prmItemLocationDetails.RemoveAt(0);
                    prmItemLocationDetails.Add(new SqlParameter("@Mode", 3));
                    prmItemLocationDetails.Add(new SqlParameter("Quantity", objClsDTOItemLocationDetails.decQuantity));
                    MobjDataLayer.ExecuteNonQueryWithTran("STItemLocationDetail", prmItemLocationDetails);
                }
            }
            return true;
        }

        public decimal GetItemBatchQuantity(int intItemID, int intBatchID, int intWarehouseID)
        {
            //Function for getting item batch quantity
            SqlDataReader sdr;
            decimal decQuantity=0;
            prmItemLocationDetails = new ArrayList();
            prmItemLocationDetails.Add(new SqlParameter("@Mode", 1));
            prmItemLocationDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmItemLocationDetails.Add(new SqlParameter("@BatchID", intBatchID));
            prmItemLocationDetails.Add(new SqlParameter("WarehouseID", intWarehouseID));
            sdr = MobjDataLayer.ExecuteReader("STItemLocationDetail", prmItemLocationDetails);
            
            if (sdr.Read())
            {
                if (sdr["Quantity"] != DBNull.Value)
                    decQuantity = Convert.ToDecimal(sdr["Quantity"]);
            }
            sdr.Close();
            return decQuantity;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MobjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }

        public DataTable GetGRNsAndInvoices(int intReferenceID, bool blnIsGRN)
        {
            //Function for getting GRNs And Invoices With Pending Location Setting
            //prmItemLocationDetails = new ArrayList();
            //if (blnIsGRN)
             //   prmItemLocationDetails.Add(new SqlParameter("@Mode", 5));
            //else
            //    prmItemLocationDetails.Add(new SqlParameter("@Mode", 6));
            //return MobjDataLayer.ExecuteDataTable("STItemLocationDetail",prmItemLocationDetails);
            DataTable dtDetDetails;
            dtDetDetails = GetGRNsAndInvoicesTotalReceivedQty(blnIsGRN);
          //  dtLotDetails = GetLotTotalQuantity(blnIsGRN);
            for(int i=0;i<dtDetDetails.Rows.Count;i++)
            {
            ///    string strFilterString = "ReferenceID = ";
             //   if (blnIsGRN)
               // {
                //    strFilterString += dtDetDetails.Rows[i]["GRNID"].ToString();
                //}
                //else
                //{
                  //  strFilterString += dtDetDetails.Rows[i]["PurchaseInvoiceID"].ToString();
                //}
                //dtLotDetails.DefaultView.RowFilter = strFilterString;
                DataTable dtItemDetails = new DataTable();
                if (blnIsGRN)
                    dtItemDetails = FindAllItemsWithoutLocation(blnIsGRN, Convert.ToInt32(dtDetDetails.Rows[i]["GRNID"]));
                else
                    dtItemDetails = FindAllItemsWithoutLocation(blnIsGRN, Convert.ToInt32(dtDetDetails.Rows[i]["PurchaseInvoiceID"]));
                if (dtItemDetails.Rows.Count ==0)//(dtLotDetails.DefaultView.ToTable().Rows.Count > 0)
                {
                    //if (Convert.ToDecimal(dtDetDetails.Rows[i]["GRNQuantity"]) == Convert.ToDecimal(dtLotDetails.DefaultView.ToTable().Rows[0]["LotQuantity"]))
                   // {
                        dtDetDetails.Rows[i].Delete();
                        i--;
                        dtDetDetails.AcceptChanges();
                    //}
                }
            }
            return dtDetDetails;
        }

        private DataTable GetGRNsAndInvoicesTotalReceivedQty(bool blnIsGRN)
        {
            prmItemLocationDetails = new ArrayList();
            if (blnIsGRN)
                prmItemLocationDetails.Add(new SqlParameter("@Mode", 7));
            else
                prmItemLocationDetails.Add(new SqlParameter("@Mode", 8));
            return MobjDataLayer.ExecuteDataTable("STItemLocationDetail", prmItemLocationDetails);
        }

        private DataTable GetLotTotalQuantity(bool blnIsGRN)
        {
            prmItemLocationDetails = new ArrayList();
            if (blnIsGRN)
                prmItemLocationDetails.Add(new SqlParameter("@Mode", 9));
            else
                prmItemLocationDetails.Add(new SqlParameter("@Mode", 10));
            return MobjDataLayer.ExecuteDataTable("STItemLocationDetail", prmItemLocationDetails);
        }

        public DataTable FindAllItemsWithoutLocation(bool blnIsGRN, int intReferenceID)
        {
            DataTable dtRetTable = new DataTable();
            dtRetTable.Columns.Add("ItemCode");
            dtRetTable.Columns.Add("ItemName");
            dtRetTable.Columns.Add("ItemID");
            dtRetTable.Columns.Add("BatchID");
            dtRetTable.Columns.Add("BatchNo");
            dtRetTable.Columns.Add("RemainingQty");
            DataTable dtGRNDetails = new DataTable();
            prmItemLocationDetails = new ArrayList();
            if (blnIsGRN)
                prmItemLocationDetails.Add(new SqlParameter("@Mode", 12));
            else
                prmItemLocationDetails.Add(new SqlParameter("@Mode", 13));
            prmItemLocationDetails.Add(new SqlParameter("@ReferenceID", intReferenceID));
            dtGRNDetails = MobjDataLayer.ExecuteDataTable("STItemLocationDetail", prmItemLocationDetails);
            foreach (DataRow dr in dtGRNDetails.Rows)
            {
                decimal decLotQuantity=0,decGRNQuantity = 0;
                SqlDataReader sdr;
                prmItemLocationDetails = new ArrayList();
                prmItemLocationDetails.Add(new SqlParameter("@Mode", 14));
                prmItemLocationDetails.Add(new SqlParameter("@ItemID", Convert.ToInt32(dr["ItemID"])));
                prmItemLocationDetails.Add(new SqlParameter("@BatchID",Convert.ToInt32(dr["BatchID"])));
                prmItemLocationDetails.Add(new SqlParameter("@ReferenceID",intReferenceID));
                prmItemLocationDetails.Add(new SqlParameter("@IsGRN",blnIsGRN));
                sdr = MobjDataLayer.ExecuteReader("STItemLocationDetail", prmItemLocationDetails);
                if (sdr.Read())
                {
                    if (sdr["LotQuantity"] != DBNull.Value)
                        decLotQuantity = Convert.ToDecimal(sdr["LotQuantity"]);
                }
                sdr.Close();
                if (Convert.ToInt32(dr["UOMID"]) == GetItemBaseUOMID(Convert.ToInt32(dr["ItemID"])))
                {
                    decGRNQuantity = Convert.ToDecimal(dr["GRNQuantity"]);
                }
                else
                {
                    DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dr["UOMID"]), Convert.ToInt32(dr["ItemID"]));
                    if (dtGetUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                            decGRNQuantity = Convert.ToDecimal(dr["GRNQuantity"]) / decConversionValue;
                        else if (intConversionFactor == 2)
                            decGRNQuantity = Convert.ToDecimal(dr["GRNQuantity"]) * decConversionValue;
                    }
                }
                if (decGRNQuantity > decLotQuantity)
                {
                    DataRow drNewRow = dtRetTable.NewRow();
                    drNewRow["ItemCode"] = dr["ItemCode"];
                    drNewRow["ItemName"] = dr["ItemName"];
                    drNewRow["ItemID"] = dr["ItemID"];
                    drNewRow["BatchID"] = dr["BatchID"];
                    drNewRow["BatchNo"] = dr["BatchNo"];
                    drNewRow["RemainingQty"] = decGRNQuantity - decLotQuantity;
                    dtRetTable.Rows.Add(drNewRow);
                }
            }
            return dtRetTable;
        }

        private int GetItemBaseUOMID(int intItemID)
        {
            prmItemLocationDetails = new ArrayList();
            prmItemLocationDetails.Add(new SqlParameter("@Mode", 12));
            prmItemLocationDetails.Add(new SqlParameter("@ItemID", intItemID));
            SqlDataReader sdr;
            sdr = MobjDataLayer.ExecuteReader("STPurchaseModule", prmItemLocationDetails);
            int intBaseUOMID = 0;
            if (sdr.Read())
            {
                if (sdr["UOMID"] != DBNull.Value)
                {
                    intBaseUOMID = Convert.ToInt32(sdr["UOMID"]);
                }
            }
            sdr.Close();
            return intBaseUOMID;
        }

        private DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {
            prmItemLocationDetails = new ArrayList();
            prmItemLocationDetails.Add(new SqlParameter("@Mode", 11));
            prmItemLocationDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmItemLocationDetails.Add(new SqlParameter("@UnitID", intUOMID));
            return MobjDataLayer.ExecuteDataTable("STPurchaseModule", prmItemLocationDetails);
        }


        #region IDisposable Members

        public void Dispose()
        {
            if (prmItemLocationDetails != null)
                prmItemLocationDetails = null;
            if (MobjDataLayer != null)
                MobjDataLayer = null;
            if (MobjClsCommonUtility != null)
                MobjClsCommonUtility = null;
        }

        #endregion
    }
}
