﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;

namespace MyBooksERP
{
    public class clsDALMaterialIssueMaster
    {
        DataLayer MobjDataLayer;
        ArrayList alParameters;
        string strMaterialIssueProcedureName = "dbo.spInvMaterialIssue";

        public clsDTOMaterialIssueMaster PobjclsDTOMaterialIssueMaster { get; set; }

        public clsDALMaterialIssueMaster(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
        }

        public bool SaveMaterialIssueMaster()
        {
            alParameters = new ArrayList();
            if (PobjclsDTOMaterialIssueMaster.MaterialIssueID == 0)
                alParameters.Add(new SqlParameter("@Mode", 2));
            else
                alParameters.Add(new SqlParameter("@Mode", 3));
            alParameters.Add(new SqlParameter("@MaterialIssueID", PobjclsDTOMaterialIssueMaster.MaterialIssueID));
            alParameters.Add(new SqlParameter("@SlNo", PobjclsDTOMaterialIssueMaster.SlNo));
            alParameters.Add(new SqlParameter("@MaterialIssueNo", PobjclsDTOMaterialIssueMaster.MaterialIssueNo));
            alParameters.Add(new SqlParameter("@MaterialIssueDate", PobjclsDTOMaterialIssueMaster.MaterialIssueDate));
            alParameters.Add(new SqlParameter("@WareHouseID", PobjclsDTOMaterialIssueMaster.WarehouseID));
            alParameters.Add(new SqlParameter("@TemplateID", PobjclsDTOMaterialIssueMaster.TemplateID));
            alParameters.Add(new SqlParameter("@Quantity", PobjclsDTOMaterialIssueMaster.Quantity));
            alParameters.Add(new SqlParameter("@CompanyID", PobjclsDTOMaterialIssueMaster.CompanyID));
            alParameters.Add(new SqlParameter("@NetAmount", PobjclsDTOMaterialIssueMaster.NetAmount));
            alParameters.Add(new SqlParameter("@NetAmountRounded", PobjclsDTOMaterialIssueMaster.NetAmountRounded));
            alParameters.Add(new SqlParameter("@Remarks", PobjclsDTOMaterialIssueMaster.Remarks));
            alParameters.Add(new SqlParameter("@CreatedBy", PobjclsDTOMaterialIssueMaster.CreatedBy));
            alParameters.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")));
            alParameters.Add(new SqlParameter("@XmlMaterialIssueDetails",PobjclsDTOMaterialIssueMaster.MaterialIssueDetails.ToXml()));
            //alParameters.Add(new SqlParameter("@XmlMaterialIssueLocationDetails", PobjclsDTOMaterialIssueMaster.MaterialIssueLocationDetails.ToXml()));
            alParameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.MaterialIssue));
            alParameters.Add(new SqlParameter("@AutoMaterialIssue", PobjclsDTOMaterialIssueMaster.blnAutoMaterialIssue));
            alParameters.Add(new SqlParameter("@ProductionID", PobjclsDTOMaterialIssueMaster.lngProductionID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            alParameters.Add(objParam);

            if (MobjDataLayer.ExecuteNonQueryWithTran(strMaterialIssueProcedureName, alParameters) !=0)
            {
                return true;
            }
            return false;
        }

        public bool DeleteMaterialIssue()
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 7));

            alParameters.Add(new SqlParameter("@MaterialIssueID", PobjclsDTOMaterialIssueMaster.MaterialIssueID));
            alParameters.Add(new SqlParameter("@WareHouseID", PobjclsDTOMaterialIssueMaster.WarehouseID));
            alParameters.Add(new SqlParameter("@CompanyID", PobjclsDTOMaterialIssueMaster.CompanyID));
            alParameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
            alParameters.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")));
            alParameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.MaterialIssue));

            if (MobjDataLayer.ExecuteNonQueryWithTran(strMaterialIssueProcedureName, alParameters) !=0)
            {
                return true;
            }
            return false;
        }

        public bool FillMaterialIssueMasterInfo()
        {
            DataTable datMaterialIssue = new DataTable();
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@MaterialIssueID", PobjclsDTOMaterialIssueMaster.MaterialIssueID));
            datMaterialIssue = MobjDataLayer.ExecuteDataTable(strMaterialIssueProcedureName, alParameters);

            if (datMaterialIssue.Rows.Count > 0)
            {
                PobjclsDTOMaterialIssueMaster.MaterialIssueID = datMaterialIssue.Rows[0]["MaterialIssueID"].ToInt64();
                PobjclsDTOMaterialIssueMaster.SlNo = datMaterialIssue.Rows[0]["SlNo"].ToInt64();
                PobjclsDTOMaterialIssueMaster.MaterialIssueNo = datMaterialIssue.Rows[0]["MaterialIssueNo"].ToStringCustom();
                PobjclsDTOMaterialIssueMaster.MaterialIssueDate = datMaterialIssue.Rows[0]["MaterialIssueDate"].ToStringCustom();
                PobjclsDTOMaterialIssueMaster.WarehouseID = datMaterialIssue.Rows[0]["WarehouseID"].ToInt32();
                PobjclsDTOMaterialIssueMaster.TemplateID = datMaterialIssue.Rows[0]["TemplateID"].ToInt64();
                PobjclsDTOMaterialIssueMaster.Quantity = datMaterialIssue.Rows[0]["Quantity"].ToDecimal();
                PobjclsDTOMaterialIssueMaster.CompanyID = datMaterialIssue.Rows[0]["CompanyID"].ToInt32();
                PobjclsDTOMaterialIssueMaster.CreatedBy = datMaterialIssue.Rows[0]["CreatedBy"].ToInt32();
                PobjclsDTOMaterialIssueMaster.CreatedByText = datMaterialIssue.Rows[0]["CreatedByText"].ToStringCustom();
                PobjclsDTOMaterialIssueMaster.CreatedDate = datMaterialIssue.Rows[0]["CreatedDate"].ToStringCustom();
                PobjclsDTOMaterialIssueMaster.Remarks = datMaterialIssue.Rows[0]["Remarks"].ToStringCustom();
                PobjclsDTOMaterialIssueMaster.NetAmount = datMaterialIssue.Rows[0]["NetAmount"].ToDecimal();
                PobjclsDTOMaterialIssueMaster.NetAmountRounded = datMaterialIssue.Rows[0]["NetAmountRounded"].ToDecimal();

                return true;
            }
            return false;
        }

        public DataTable GetMaterialIssueDetails()
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 8));
            alParameters.Add(new SqlParameter("@MaterialIssueID", PobjclsDTOMaterialIssueMaster.MaterialIssueID));
            return MobjDataLayer.ExecuteDataTable(strMaterialIssueProcedureName, alParameters);
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 10));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@UomTypeID", 1));
            DataTable dtItemUoms = new DataTable();
            dtItemUoms = MobjDataLayer.ExecuteDataTable("dbo.spInvPurchaseModuleFunctions", alParameters);

            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 12));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            DataTable dtItemBaseUnit = MobjDataLayer.ExecuteDataTable("dbo.spInvPurchaseModuleFunctions", alParameters);
            DataRow dr = dtItemUoms.NewRow();
            dr["UOMID"] = dtItemBaseUnit.Rows[0]["UOMID"];
            dr["Description"] = dtItemBaseUnit.Rows[0]["Description"];
            dr["ShortName"] = dtItemBaseUnit.Rows[0]["ShortName"];
            dr["ItemID"] = intItemID;
            dtItemUoms.Rows.InsertAt(dr, 0);
            return dtItemUoms;
        }

        public DataTable GetItems(int intCompanyID, int intWarehouseID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 9));
            alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            alParameters.Add(new SqlParameter("@WareHouseID", intWarehouseID));
            return MobjDataLayer.ExecuteDataTable(strMaterialIssueProcedureName, alParameters);
        }

        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 14));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@LocationWise", ClsCommonSettings.PblnIsLocationWise));
            return MobjDataLayer.ExecuteDataTable("dbo.spInvItemLocationTransfer", alParameters).Rows[0];
        }

        public DataTable GetAllMaterialIssues(string strCondition)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 10));
            alParameters.Add(new SqlParameter("@SearchCondition", strCondition));
            return MobjDataLayer.ExecuteDataTable(strMaterialIssueProcedureName, alParameters);
        }

        public DataSet GetMaterialIssueReport() //Material Issue Report
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 11));
            alParameters.Add(new SqlParameter("@MaterialIssueID", PobjclsDTOMaterialIssueMaster.MaterialIssueID));
            return MobjDataLayer.ExecuteDataSet(strMaterialIssueProcedureName , alParameters);
        }

        public DataTable GetProductionTemplateDetails(long lngTemplateID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 12));
            alParameters.Add(new SqlParameter("@TemplateID", lngTemplateID));
            return MobjDataLayer.ExecuteDataTable(strMaterialIssueProcedureName, alParameters);
        }
    }
}
