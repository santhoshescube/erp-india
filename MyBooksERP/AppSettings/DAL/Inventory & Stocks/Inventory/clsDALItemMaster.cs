﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.SqlClient;
using System.Data;


namespace MyBooksERP
{
    public class clsDALItemMaster
    {
        ArrayList prmItem;

        public clsDTOItemMaster objClsDTOItemMaster { get; set; }
        public DataLayer objClsConnection { get; set; }

        private readonly string ProcedureName = "spInvItemMaster";

        public bool SaveItemInfo()
        {
            prmItem = new ArrayList();

            prmItem.Add(new SqlParameter("@Mode", (objClsDTOItemMaster.intItemID == 0 ? 2 : 3)));

            // ItemMaster
            prmItem.Add(new SqlParameter("@ItemID", objClsDTOItemMaster.intItemID));
            prmItem.Add(new SqlParameter("@ItemCode", objClsDTOItemMaster.strItemCode));
            prmItem.Add(new SqlParameter("@ShortName", objClsDTOItemMaster.strShortName));
            prmItem.Add(new SqlParameter("@Description", objClsDTOItemMaster.strDescription));
            prmItem.Add(new SqlParameter("@DescriptionArabic", objClsDTOItemMaster.strDescriptionArabic));            
            prmItem.Add(new SqlParameter("@CutOffRate", objClsDTOItemMaster.decCutOffPrice));

            if (objClsDTOItemMaster.intCategoryID > 0)
                prmItem.Add(new SqlParameter("@CategoryID", objClsDTOItemMaster.intCategoryID));

            if (objClsDTOItemMaster.intSubCategoryID > 0)
                prmItem.Add(new SqlParameter("@SubCategoryID", objClsDTOItemMaster.intSubCategoryID));

            prmItem.Add(new SqlParameter("@BaseUomID", objClsDTOItemMaster.intBaseUomID));
            prmItem.Add(new SqlParameter("@DefaultPurchaseUomID", objClsDTOItemMaster.intDefaultPurchaseUomID));
            prmItem.Add(new SqlParameter("@DefaultSalesUomID", objClsDTOItemMaster.intDefaultSalesUomID));
            prmItem.Add(new SqlParameter("@CreatedBy", objClsDTOItemMaster.intCreatedBy));
            prmItem.Add(new SqlParameter("@ItemStatusID", objClsDTOItemMaster.intItemStatusID));
            prmItem.Add(new SqlParameter("@CompanyID", objClsDTOItemMaster.intCompanyID));

            // Item Details
            prmItem.Add(new SqlParameter("@WarrantyPeriod", objClsDTOItemMaster.strWarrantyPeriod));
            prmItem.Add(new SqlParameter("@GuaranteePeriod", objClsDTOItemMaster.strGuaranteePeriod));
            prmItem.Add(new SqlParameter("@Model", objClsDTOItemMaster.strModel));
            //prmItem.Add(new SqlParameter("@Colour", objClsDTOItemMaster.strColour));
           
            prmItem.Add(new SqlParameter("@ReorderLevel", objClsDTOItemMaster.decReorderLevel));
            prmItem.Add(new SqlParameter("@ReorderQuantity", objClsDTOItemMaster.decReorderQuantity));
            if(this.objClsDTOItemMaster.Images.Count > 0)
                prmItem.Add(new SqlParameter("@ImageFile", this.objClsDTOItemMaster.Images[0].FileName));

            prmItem.Add(new SqlParameter("@Notes", objClsDTOItemMaster.strNotes));

            if (objClsDTOItemMaster.intManufacturerID > 0)
                prmItem.Add(new SqlParameter("@ManufacturerID", objClsDTOItemMaster.intManufacturerID));

            if (objClsDTOItemMaster.intMadeInID > 0)
                prmItem.Add(new SqlParameter("@MadeInID", objClsDTOItemMaster.intMadeInID));

            if (objClsDTOItemMaster.intTaxID > 0)
                prmItem.Add(new SqlParameter("@TaxID", objClsDTOItemMaster.intTaxID));

            prmItem.Add(new SqlParameter("@Width", objClsDTOItemMaster.dblWidth));
            prmItem.Add(new SqlParameter("@Height", objClsDTOItemMaster.dblHeight));
            prmItem.Add(new SqlParameter("@Length", objClsDTOItemMaster.dblLength));

            if (objClsDTOItemMaster.intSizeUomID > 0)
                prmItem.Add(new SqlParameter("@SizeUOMID", objClsDTOItemMaster.intSizeUomID));

            prmItem.Add(new SqlParameter("@Weight", objClsDTOItemMaster.dblWeight));

            if (objClsDTOItemMaster.intWeightUomID > 0)
                prmItem.Add(new SqlParameter("@WeightUOMID", objClsDTOItemMaster.intWeightUomID));
                        
            prmItem.Add(new SqlParameter("@BarcodeValue", objClsDTOItemMaster.strBarcodeValue));
            if (objClsDTOItemMaster.imgBarcodeImage != null)
                prmItem.Add(new SqlParameter("@BarcodeImage", objClsDTOItemMaster.imgBarcodeImage));

            if (objClsDTOItemMaster.intCostingMethodID > 0)
                prmItem.Add(new SqlParameter("@CostingMethodID", objClsDTOItemMaster.intCostingMethodID));
            if (objClsDTOItemMaster.intProductTypeID > 0)
                prmItem.Add(new SqlParameter("@ProductTypeID", objClsDTOItemMaster.intProductTypeID));
            if (objClsDTOItemMaster.intPricingSchemeID > 0)
                prmItem.Add(new SqlParameter("@PricingSchemeID", objClsDTOItemMaster.intPricingSchemeID));
            prmItem.Add(new SqlParameter("@PurchaseRate", objClsDTOItemMaster.decPurchaseRate));
            prmItem.Add(new SqlParameter("@SaleRate", objClsDTOItemMaster.decSaleRate));
          
            prmItem.Add(new SqlParameter("@ExpiryDateMandatory", objClsDTOItemMaster.blnExpiryDateMandatory));
            prmItem.Add(new SqlParameter("@IsLandingCostEffected", objClsDTOItemMaster.blnIsLandingCostEffected));
            if(objClsDTOItemMaster.blnIsLandingCostEffected)
                prmItem.Add(new SqlParameter("@CalculationBasedOnRate", false));
            else
                prmItem.Add(new SqlParameter("@CalculationBasedOnRate", objClsDTOItemMaster.blnCalculationBasedOnRate));

            if(this.objClsDTOItemMaster.Dimensions.Count > 0)
                prmItem.Add(new SqlParameter("@xmlDimensions", this.objClsDTOItemMaster.Dimensions.ToXml()));

            if (this.objClsDTOItemMaster.Features.Count > 0)
                prmItem.Add(new SqlParameter("@xmlFeatures", this.objClsDTOItemMaster.Features.ToXml()));

            if(this.objClsDTOItemMaster.Images.Count > 0)
                prmItem.Add(new SqlParameter("@xmlImages", this.objClsDTOItemMaster.Images.ToXml()));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmItem.Add(objParam);

            object intItemID = 0;

            objClsConnection.ExecuteNonQuery( this.ProcedureName , prmItem, out intItemID);
            objClsDTOItemMaster.intItemID = Convert.ToInt32(intItemID);

            bool blnStatus = true;

            if (SaveItemCostingAndPricingDetails(objClsDTOItemMaster.datBatchDetails) == false)
                blnStatus = false;

            if (objClsDTOItemMaster.intItemID > 0 && blnStatus == true)
                return true;
            else
                return false;
        }

        public void SaveItemLocations()
        {
            prmItem = new ArrayList();
            prmItem.Add(new SqlParameter("@Mode", 3));   // Mode for delete
            prmItem.Add(new SqlParameter("@ItemID", objClsDTOItemMaster.intItemID));
            objClsConnection.ExecuteNonQuery("spInvItemReorder", prmItem);

            foreach (clsDTOItemReorder objclsDTOItemReorder in objClsDTOItemMaster.objclsDTOItemReorder)
            {
                prmItem = new ArrayList();

                prmItem.Add(new SqlParameter("@Mode", 2));
                prmItem.Add(new SqlParameter("@ItemID", objClsDTOItemMaster.intItemID));
                if (objclsDTOItemReorder.intWarehouseID > 0)
                    prmItem.Add(new SqlParameter("@WarehouseID", objclsDTOItemReorder.intWarehouseID));
                prmItem.Add(new SqlParameter("@ReorderLevel", objclsDTOItemReorder.decReorderLevel));
                prmItem.Add(new SqlParameter("@ReorderQuantity", objclsDTOItemReorder.decReorderQuantity));

                if (objclsDTOItemReorder.intLocationID > 0 && objclsDTOItemReorder.intRowID > 0 && objclsDTOItemReorder.intBlockID > 0 && objclsDTOItemReorder.intLotID > 0)
                {
                    prmItem.Add(new SqlParameter("@LocationID", objclsDTOItemReorder.intLocationID));
                    prmItem.Add(new SqlParameter("@RowID", objclsDTOItemReorder.intRowID));
                    prmItem.Add(new SqlParameter("@BlockID", objclsDTOItemReorder.intBlockID));
                    prmItem.Add(new SqlParameter("@LotID", objclsDTOItemReorder.intRowID));
                }

                objClsConnection.ExecuteNonQuery("spInvItemReorder", prmItem);
            }
        }

        public void SaveItemUom()
        {
            prmItem = new ArrayList();
            prmItem.Add(new SqlParameter("@Mode", 3));   // Mode for delete
            prmItem.Add(new SqlParameter("@ItemID", objClsDTOItemMaster.intItemID));

            objClsConnection.ExecuteNonQuery("spInvInvItemUOMs", prmItem);

            foreach (clsDTOItemUOM objclsDTOItemUOM in objClsDTOItemMaster.objclsDTOItemUOM)
            {
                prmItem = new ArrayList();

                prmItem.Add(new SqlParameter("@Mode", 2));
                prmItem.Add(new SqlParameter("@ItemID", objClsDTOItemMaster.intItemID));
                prmItem.Add(new SqlParameter("@UOMID", objclsDTOItemUOM.intUOMID));
                prmItem.Add(new SqlParameter("@ItemUOMType", objclsDTOItemUOM.intItemUOMType));

                objClsConnection.ExecuteNonQuery("spInvInvItemUOMs", prmItem);
            }
        }

        public bool SaveItemCostingAndPricingDetails(DataTable datTemp)
        {
            bool blnStatus = true;
            try
            {
                DataTable datTempCurrency = FillCombos(new string[] { "CurrencyId AS CurrencyID", "CompanyMaster", "CompanyID = " +
                            "(SELECT (CASE WHEN CompanyID = 0 THEN 1 ELSE ISNULL(CompanyID,1) END) FROM InvItemMaster WHERE " +
                            "ItemID= " + objClsDTOItemMaster.intItemID + ")" });
                if (datTemp != null && datTemp.Rows.Count > 0)
                {
                    for (int i = 0; i <= datTemp.Rows.Count - 1; i++)
                    {
                        prmItem = new ArrayList();
                        prmItem.Add(new SqlParameter("@Mode", 9));
                        prmItem.Add(new SqlParameter("@CostingMethodID", objClsDTOItemMaster.intCostingMethodID));
                        prmItem.Add(new SqlParameter("@PricingSchemeID", objClsDTOItemMaster.intPricingSchemeID));
                        prmItem.Add(new SqlParameter("@ItemID", objClsDTOItemMaster.intItemID));
                        prmItem.Add(new SqlParameter("@CurrencyID", datTempCurrency.Rows[0]["CurrencyID"].ToString().ToInt32()));
                        prmItem.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
                        prmItem.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDateTime()));
                        prmItem.Add(new SqlParameter("@BatchID", datTemp.Rows[i]["ColBatchID"].ToString().ToInt32()));
                        prmItem.Add(new SqlParameter("@PurchaseRate", datTemp.Rows[i]["ColPurchaseRate"].ToString().ToDouble()));
                        prmItem.Add(new SqlParameter("@ActualRate", datTemp.Rows[i]["ColActualRate"].ToString().ToDouble()));
                        if (objClsDTOItemMaster.intCostingMethodID == (int)CostingMethodReference.Batch)
                            prmItem.Add(new SqlParameter("@SaleRate", datTemp.Rows[i]["ColSaleRate"].ToString().ToDouble()));
                        else
                            prmItem.Add(new SqlParameter("@SaleRate", objClsDTOItemMaster.decSaleRate));
                        DeleteCostingAndPricingDetails(objClsDTOItemMaster.intItemID,
                                    datTemp.Rows[i]["ColBatchID"].ToString().ToInt32());
                        objClsConnection.ExecuteNonQuery("spInvPricingScheme", prmItem);
                    }
                }
                else
                {
                    prmItem = new ArrayList();
                    prmItem.Add(new SqlParameter("@Mode", 9));
                    prmItem.Add(new SqlParameter("@CostingMethodID", objClsDTOItemMaster.intCostingMethodID));
                    prmItem.Add(new SqlParameter("@PricingSchemeID", objClsDTOItemMaster.intPricingSchemeID));
                    prmItem.Add(new SqlParameter("@ItemID", objClsDTOItemMaster.intItemID));
                    prmItem.Add(new SqlParameter("@CurrencyID", datTempCurrency.Rows[0]["CurrencyID"].ToString().ToInt32()));
                    prmItem.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
                    prmItem.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDateTime()));
                    prmItem.Add(new SqlParameter("@BatchID", 1));
                    prmItem.Add(new SqlParameter("@PurchaseRate", objClsDTOItemMaster.decPurchaseRate));
                    prmItem.Add(new SqlParameter("@ActualRate", objClsDTOItemMaster.decActualRate));
                    prmItem.Add(new SqlParameter("@SaleRate", objClsDTOItemMaster.decSaleRate));
                    DeleteCostingAndPricingDetails(objClsDTOItemMaster.intItemID, -1);
                    objClsConnection.ExecuteNonQuery("spInvPricingScheme", prmItem);
                }
            }
            catch { blnStatus = false; }
            return blnStatus;
        }

        private void DeleteCostingAndPricingDetails(int intItemID, int intBatchID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 16));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", intBatchID));
            objClsConnection.ExecuteNonQuery("spInvPricingScheme", alParameters);
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objClsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool DuplicateItemCodeExists(string strItemCode,int CompanyID)
        {
            prmItem = new ArrayList();

            prmItem.Add(new SqlParameter("@Mode", 6));
            prmItem.Add(new SqlParameter("@CompanyID", CompanyID));
            prmItem.Add(new SqlParameter("@ItemID", objClsDTOItemMaster.intItemID));
            prmItem.Add(new SqlParameter("@ItemCode", strItemCode));

            if (Convert.ToInt32(objClsConnection.ExecuteScalar(this.ProcedureName, prmItem)) > 0)
                return true;
            else
                return false;
        }

        public DataTable SearchItem(int intCategory, int intSubCategory, string strDescription, string strCode,int CompanyID, out int intTotalRows)
        {
            intTotalRows = 0;
            DataTable datSearchItems = new DataTable();

            prmItem = new ArrayList();

            prmItem.Add(new SqlParameter("@Mode", 5));
            prmItem.Add(new SqlParameter("@SearchCategory", intCategory));
            prmItem.Add(new SqlParameter("@SearchSubCategory", intSubCategory));
            prmItem.Add(new SqlParameter("@SearchItemName", strDescription));
            prmItem.Add(new SqlParameter("@SearchCode", strCode));
            prmItem.Add(new SqlParameter("@CompanyID", CompanyID));


            // Calling method for reading item details according to the given RowNumber
            DataSet dtsItems = objClsConnection.ExecuteDataSet(this.ProcedureName, prmItem);
            datSearchItems = dtsItems.Tables[0];

            intTotalRows = Convert.ToInt32(dtsItems.Tables[1].Rows[0].ItemArray[0]);

            return datSearchItems;
        }

        /// <summary>
        /// Displaying item details according to the given RowNumber
        /// </summary>
        public bool DisplayItemInfo()
        {
            ArrayList prmItem = new ArrayList();

            prmItem.Add(new SqlParameter("@Mode", 1));
            prmItem.Add(new SqlParameter("@ItemID", objClsDTOItemMaster.intItemID));

            // Calling method for reading item details according to the given ItemID
            SqlDataReader sdrItem = objClsConnection.ExecuteReader(this.ProcedureName, prmItem, CommandBehavior.CloseConnection);
            bool bReturnValue = ReadItemDetails(sdrItem);

            DisplayItemReorder();   // Displaying item storage locations
            DisplayItemUOMs();  // Displaying item UOMs

            this.LoadFeatures();

            return bReturnValue;
        }

        private void LoadFeatures()
        {
            this.objClsDTOItemMaster.Dimensions = new List<clsDTOItemDimension>();
            this.objClsDTOItemMaster.Features = new List<clsFeatureTemp>();
            this.objClsDTOItemMaster.Images = new List<clsDTOItemImages>();

            using(DataSet ds = this.objClsConnection.ExecuteDataSet(this.ProcedureName, new List<SqlParameter>() {
                new SqlParameter("@Mode", 11),
                new SqlParameter("@ItemID", this.objClsDTOItemMaster.intItemID)
            }))
            {
                if (ds.Tables.Count > 0)
                {
                    // load dimensions
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            this.objClsDTOItemMaster.Dimensions.Add(new clsDTOItemDimension { 
                                 DimensionID = row["DimensionID"].ToInt32(),
                                 DimensionValue = row["DimensionValue"].ToDecimal(),
                                 UOMID = row["UOMID"].ToInt32(),
                            });
                        }
                    }

                    // load features
                    if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                    {
                        clsFeatureTemp feature;
                        
                        int FeatureID = 0;
                        
                        foreach (DataRow row in ds.Tables[1].Rows)
                        {
                            FeatureID = row["FeatureID"].ToInt32();

                            var fea = this.objClsDTOItemMaster.Features.Find(f => f.FeatureID == FeatureID);

                            if (fea == null)
                            {
                                feature = new clsFeatureTemp
                                {
                                    FeatureID = row["FeatureID"].ToInt32(),
                                    FeatureName = row["FeatureName"].ToStringCustom()
                                };

                                DataRow[] rows = ds.Tables[1].Select("FeatureID=" + FeatureID.ToString());

                                if (rows.Length > 0)
                                {
                                    foreach (DataRow r in rows)
                                    {
                                        feature.Features.Add(new clsFeatureValue { 
                                             FeatureValue = r["FeatureValue"].ToStringCustom(),
                                             FeatureValueID = r["FeatureValueID"].ToInt64(),
                                             IsDefault = r["IsDefault"].ToBoolean()
                                        });
                                    }

                                    this.objClsDTOItemMaster.Features.Add(feature);
                                }
                            }
                            else
                                continue;
                        }
                    }


                    // load ItemImages
                    if (ds.Tables.Count > 2 && ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[2].Rows)
                        {
                            this.objClsDTOItemMaster.Images.Add(new clsDTOItemImages
                            {
                                FileName = row["ImageName"].ToStringCustom(),
                                ViewName = row["ViewName"].ToStringCustom()
                            });
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Reading Item information into the object
        /// </summary>
        private bool ReadItemDetails(SqlDataReader sdrItem)
        {
            bool blnReturnValue;

            if (sdrItem.Read())
            {
                objClsDTOItemMaster.intItemID = sdrItem["ItemID"].ToInt32();
                objClsDTOItemMaster.strItemCode = Convert.ToString(sdrItem["Code"]);
                objClsDTOItemMaster.strDescription = Convert.ToString(sdrItem["ItemName"]);
                objClsDTOItemMaster.strDescriptionArabic = Convert.ToString(sdrItem["DescriptionArabic"]);
                objClsDTOItemMaster.strShortName = Convert.ToString(sdrItem["ShortName"]);
                objClsDTOItemMaster.intCategoryID = sdrItem["CategoryID"].ToInt32();
                objClsDTOItemMaster.intSubCategoryID = sdrItem["SubCategoryID"].ToInt32();
                objClsDTOItemMaster.decCutOffPrice = (sdrItem["CutOffPrice"] == DBNull.Value) ? 0 : sdrItem["CutOffPrice"].ToDecimal();
                objClsDTOItemMaster.intItemStatusID = sdrItem["StatusID"].ToInt32();

                if (sdrItem["CompanyID"] != DBNull.Value)
                    objClsDTOItemMaster.intCompanyID = sdrItem["CompanyID"].ToInt32();
                else
                    objClsDTOItemMaster.intCompanyID = 0;

                objClsDTOItemMaster.intBaseUomID = sdrItem["BaseUomID"].ToInt32();
                objClsDTOItemMaster.intDefaultPurchaseUomID = sdrItem["DefaultPurchaseUomID"].ToInt32();
                objClsDTOItemMaster.intDefaultSalesUomID = sdrItem["DefaultSalesUomID"].ToInt32();
                objClsDTOItemMaster.intManufacturerID = sdrItem["ManufacturerID"].ToInt32();
                objClsDTOItemMaster.intMadeInID = sdrItem["MadeInID"].ToInt32();
                objClsDTOItemMaster.strImageFile = sdrItem["ImageFileName"].ToStringCustom();
                objClsDTOItemMaster.strGuaranteePeriod = sdrItem["GuaranteePeriod"].ToStringCustom();
                objClsDTOItemMaster.strWarrantyPeriod = sdrItem["WarrantyPeriod"].ToStringCustom();
                objClsDTOItemMaster.strModel = sdrItem["Model"].ToStringCustom();
                objClsDTOItemMaster.strNotes = sdrItem["Remarks"].ToStringCustom();                
                objClsDTOItemMaster.intCostingMethodID = sdrItem["CostingMethodID"].ToInt32();
                objClsDTOItemMaster.intProductTypeID = sdrItem["ProductTypeID"].ToInt32();
                objClsDTOItemMaster.intPricingSchemeID = sdrItem["PricingSchemeID"].ToInt32();
                objClsDTOItemMaster.decPurchaseRate = sdrItem["PurchaseRate"].ToDecimal();
                try { objClsDTOItemMaster.blnCalculationBasedOnRate = Convert.ToBoolean(sdrItem["CalculationBasedOnRate"]); }
                catch { objClsDTOItemMaster.blnCalculationBasedOnRate = true; }

                objClsDTOItemMaster.strBarcodeValue = sdrItem["BarcodeValue"].ToStringCustom();
                if (Convert.ToString(sdrItem["BarcodeImage"]).Trim() != string.Empty)
                    objClsDTOItemMaster.imgBarcodeImage = (byte[])sdrItem["BarcodeImage"];
                else
                    objClsDTOItemMaster.imgBarcodeImage = null;

                objClsDTOItemMaster.decReorderLevel = sdrItem["ReorderLevel"].ToDecimal();
                objClsDTOItemMaster.decReorderQuantity = sdrItem["ReorderQuantity"].ToDecimal();

                try { objClsDTOItemMaster.blnExpiryDateMandatory = Convert.ToBoolean(sdrItem["ExpiryDateMandatory"]); }
                catch { objClsDTOItemMaster.blnExpiryDateMandatory = true; }
                if (sdrItem["IsLandingCostEffected"] == DBNull.Value)
                    objClsDTOItemMaster.blnIsLandingCostEffected = false;
                else
                    objClsDTOItemMaster.blnIsLandingCostEffected = Convert.ToBoolean(sdrItem["IsLandingCostEffected"]);
                blnReturnValue = true;
            }
            else
            {
                blnReturnValue = false;
            }
            sdrItem.Close();
            return blnReturnValue;
        }

        /// <summary>
        /// Display Item storage locations
        /// </summary>
        private void DisplayItemReorder()
        {
            prmItem = new ArrayList();

            prmItem.Add(new SqlParameter("@Mode", 1));  // Mode for displaying item Storage details
            prmItem.Add(new SqlParameter("@ItemID", objClsDTOItemMaster.intItemID));

            objClsDTOItemMaster.objclsDTOItemReorder = new List<clsDTOItemReorder>();

            // Calling method for reading item Storage details
            foreach (DataRow drowLocation in objClsConnection.ExecuteDataSet("spInvItemReorder", prmItem).Tables[0].Rows)
            {
                clsDTOItemReorder objclsDTOItemReorder = new clsDTOItemReorder();

                objclsDTOItemReorder.intItemID = drowLocation["ItemID"].ToInt32();
                objclsDTOItemReorder.decReorderLevel = drowLocation["ReorderLevel"].ToDecimal();
                objclsDTOItemReorder.decReorderQuantity = drowLocation["ReorderQuantity"].ToDecimal();
                objclsDTOItemReorder.intSerialNo = drowLocation["SerialNo"].ToInt32();
                objclsDTOItemReorder.intWarehouseID = drowLocation["WarehouseID"].ToInt32();
                objclsDTOItemReorder.intLocationID = drowLocation["LocationID"].ToInt32();
                objclsDTOItemReorder.intRowID = drowLocation["RowID"].ToInt32();
                objclsDTOItemReorder.intBlockID = drowLocation["BlockID"].ToInt32();
                objclsDTOItemReorder.intLotID = drowLocation["LotID"].ToInt32();
                objClsDTOItemMaster.objclsDTOItemReorder.Add(objclsDTOItemReorder);
            }
        }

        /// <summary>
        /// Display item UOMs
        /// </summary>
        private void DisplayItemUOMs()
        {
            prmItem = new ArrayList();

            prmItem.Add(new SqlParameter("@Mode", 1));  // Mode for displaying item UOM details
            prmItem.Add(new SqlParameter("@ItemID", objClsDTOItemMaster.intItemID));

            objClsDTOItemMaster.objclsDTOItemUOM = new List<clsDTOItemUOM>();

            using (DataSet ds = objClsConnection.ExecuteDataSet("spInvInvItemUOMs", prmItem))
            {
                // Calling method for reading item UOMs details
                foreach (DataRow drowUom in ds.Tables[0].Rows)
                {
                    clsDTOItemUOM objclsDTOItemUOM = new clsDTOItemUOM();

                    //objclsDTOItemUOM.intOrderID = Convert.ToInt32(drowUom["OrderID"]);
                    //objclsDTOItemUOM.intItemUOMType = Convert.ToInt32(drowUom["TradingTypeID"]);
                    objclsDTOItemUOM.intUOMID = Convert.ToInt32(drowUom["UOMID"]);

                    objClsDTOItemMaster.objclsDTOItemUOM.Add(objclsDTOItemUOM);
                }
            }
        }

        public bool CheckItemExists()
        {
            prmItem = new ArrayList();
            bool blnIsExists = false;
            prmItem.Add(new SqlParameter("@ID", objClsDTOItemMaster.intItemID));
            prmItem.Add(new SqlParameter("@Table", "InvItemMaster"));
            prmItem.Add(new SqlParameter("@DetailsTable", "InvItemDetails','InvItemFeatures','InvItemDimensions','InvBarcode','InvItemReorder','InvItemUOMs','InvItemStockDetails','InvItemStockMaster','InvBatchDetails"));
           // prmItem.Add(new SqlParameter("@DetailsTable", "InvBarcode','InvItemProductionStages', 'InvItemRawMaterialDetails','InvItemDetails','InvItemReorder','InvItemUOMs','InvItemStockDetails','InvItemStockMaster','InvBatchDetails"));
            //prmItem.Add(new SqlParameter("@DetailsTable", "InvItemStockDetails','InvItemStockMaster','InvBatchDetails','InvRawMaterialCostEstimationDetails','PrdJobOrderProducts','PrdJobOrderCustomizedProductRawMaterials', 'PrdJobOrderCustomizedProductComponents', 'PrdJobOrderCustomizedProductDimensions', 'PrdJobOrderCustomizedProductFeatures','PrdJobOrderCustomizedProductionStages', 'PrdRawMaterialUsedDetails"));
            
            if (Convert.ToString(objClsConnection.ExecuteScalar("spCheckIDExistsNew", prmItem)).Trim().Length > 0)
                blnIsExists = true;
            if (!blnIsExists)
            {
                DataTable datSalesQuotationDetails = FillCombos(new string[] { "SalesQuotationID", "InvSalesQuotationDetail", "IsGroup = 0 And ItemID = " + objClsDTOItemMaster.intItemID });
                DataTable datSalesOrderDetails = FillCombos(new string[] { "SalesOrderID", "InvSalesOrderDetail", "IsGroup = 0 And ItemID = " + objClsDTOItemMaster.intItemID });
                DataTable datSalesInvoiceDetails = FillCombos(new string[] { "SalesInvoiceID", "InvSalesInvoiceDetails", "IsGroup = 0 And ItemID = " + objClsDTOItemMaster.intItemID });
                //DataTable datItemRawMaterialDetails = FillCombos(new string[] {"1","InvItemRawMaterialDetails","RawMaterialItemID = "+objClsDTOItemMaster.intItemID});
                //if (datSalesQuotationDetails.Rows.Count > 0 || datSalesOrderDetails.Rows.Count > 0 || datSalesInvoiceDetails.Rows.Count >0 || datItemRawMaterialDetails.Rows.Count>0)
                if (datSalesQuotationDetails.Rows.Count > 0 || datSalesOrderDetails.Rows.Count > 0 || datSalesInvoiceDetails.Rows.Count > 0 )
                {
                    blnIsExists = true;
                }
            }
            return blnIsExists;
        }

        public bool CheckItemAndUOMExists(int lngItemID,int intUomID,int intUomTypeID)
        {
            prmItem = new ArrayList();
            prmItem.Add(new SqlParameter("@Mode", 2));
            prmItem.Add(new SqlParameter("@ColumnsInCondition", "c.name= 'ItemID' or c.name='UOMID'"));
            if (intUomTypeID == 1)
            prmItem.Add(new SqlParameter("@TablesNotIn", "'InvItemMaster','InvItemDetails','InvItemUOMs','InvRFQDetail','InvPurchaseIndentDetail','InvPurchaseQuotationDetail','InvPurchaseOrderDetail','InvPurchaseInvoiceDetail','InvOpeningstockDetails'"));
            else
                prmItem.Add(new SqlParameter("@TablesNotIn", "'InvItemMaster','InvItemDetails','InvItemUOMs','InvSalesQuotationDetail','InvSalesOrderDetail','InvSalesInvoiceDetails'"));
            prmItem.Add(new SqlParameter("@ColumnsValueCondition", "ItemID = '" + lngItemID + "' And UOMID = '" + intUomID + "'"));
            return Convert.ToBoolean(objClsConnection.ExecuteScalar("spCheckValueExists", prmItem));
        }

        /// <summary>
        /// Deleting Item master details
        /// </summary>
        /// <returns></returns>
        public bool DeleteItemInfo()
        {
            bool blnReturnValue = false;

            prmItem = new ArrayList();

            prmItem.Add(new SqlParameter("@Mode", 4));
            prmItem.Add(new SqlParameter("@ItemID", objClsDTOItemMaster.intItemID));

            if (objClsConnection.ExecuteNonQuery(this.ProcedureName, prmItem) != 0)
                blnReturnValue = true;

            return blnReturnValue;
        }

        public DataTable GetItemUOM(int intItemID)
        {
            prmItem = new ArrayList();

            prmItem.Add(new SqlParameter("@Mode", 4));
            prmItem.Add(new SqlParameter("@ItemID", intItemID));

            return objClsConnection.ExecuteDataSet("spInvInvItemUOMs", prmItem).Tables[0];
        }

        public DataSet GetProductReport()
        {
            prmItem = new ArrayList();
            prmItem.Add(new SqlParameter("@Mode", 7));
            prmItem.Add(new SqlParameter("@ItemID", objClsDTOItemMaster.intItemID));

            return objClsConnection.ExecuteDataSet(this.ProcedureName, prmItem);
        }

        public DataSet GetProductBatchDetails()
        {
            prmItem = new ArrayList();
            prmItem.Add(new SqlParameter("@Mode", 8));
            prmItem.Add(new SqlParameter("@ItemID", objClsDTOItemMaster.intItemID));

            return objClsConnection.ExecuteDataSet(this.ProcedureName, prmItem);
        }

        public int GenerateItemCode()
        {
            int intRetValue = 0;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            DataTable datTemp = objClsConnection.ExecuteDataTable(this.ProcedureName, parameters);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    intRetValue = Convert.ToInt32(datTemp.Rows[0]["ReturnValue"].ToString());
                }
                else
                    intRetValue = 0;
            }
            else
                intRetValue = 0;
            return intRetValue;
        }

        public string GetSaleRate(int intMethodID, int intItemId, int intCompanyID)
        {
            string strRetValue = "0";
            ArrayList parameters = new ArrayList();
            if(intMethodID == (int)CostingMethodReference.LIFO)
                parameters.Add(new SqlParameter("@Mode", 3));
            else if (intMethodID == (int)CostingMethodReference.FIFO)
                parameters.Add(new SqlParameter("@Mode", 4));
            else if (intMethodID == (int)CostingMethodReference.AVG)
                parameters.Add(new SqlParameter("@Mode", 5));
            else if (intMethodID == (int)CostingMethodReference.MIN)
                parameters.Add(new SqlParameter("@Mode", 6));
            else if (intMethodID == (int)CostingMethodReference.MAX)
                parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@ItemID", intItemId));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            DataTable datTemp = objClsConnection.ExecuteDataTable("spInvPricingScheme", parameters);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    strRetValue = datTemp.Rows[0][0].ToString();
                }
                else
                    strRetValue = "0";
            }
            else
                strRetValue = "0";

            if (strRetValue == "0")
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 13));
                parameters.Add(new SqlParameter("@ItemID", intItemId));
                DataTable datTemp1 = objClsConnection.ExecuteDataTable("spInvPricingScheme", parameters);
                if (datTemp1 != null)
                {
                    if (datTemp1.Rows.Count > 0)
                    {
                        strRetValue = datTemp1.Rows[0][0].ToString();
                    }
                    else
                        strRetValue = "0";
                }
                else
                    strRetValue = "0";
            }
            return strRetValue;
        }

        public DataTable GetSaleRateForBatch(int intItemId, int intCompanyID, int intPricingSchemeID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@ItemID", intItemId));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@PricingSchemeID", intPricingSchemeID));
            return objClsConnection.ExecuteDataTable("spInvPricingScheme", parameters);
        }

        public DataTable GetBatchDetailsFromCostingPricing(int intItemId, int intCompanyID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@ItemID", intItemId));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objClsConnection.ExecuteDataTable("spInvPricingScheme", parameters);
        }

        public static DataTable GetUOMsByItemID(long ItemID)
        {
            return new DataLayer().ExecuteDataTable("spInvItemMaster", new List<SqlParameter> { 
                new SqlParameter("@Mode", 12),
                new SqlParameter("@ItemID", ItemID)
            });
        }

        public static int GetBaseUomByItem(long ItemID)
        {
            return  new DataLayer().ExecuteScalar("spInvItemMaster", new List<SqlParameter> { 
                new SqlParameter("@Mode", 14),
                new SqlParameter("@ItemID", ItemID)
            }).ToInt32();
        }

        public DataTable ItemSaleHistory(long ItemID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 16));
            parameters.Add(new SqlParameter("@ItemID", ItemID));
            return objClsConnection.ExecuteDataTable("spInvItemMaster", parameters);
        }
    }
}
