﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace MyBooksERP
{
    public class clsDALDimensionReference
    {
        DataLayer objDataLayer = null;
        private readonly string ProcedureName = "spInvDimensionReference";

        private DataLayer DataLayer
        {
            get
            {
                if (this.objDataLayer == null)
                    this.objDataLayer = new DataLayer();

                return this.objDataLayer;
            }
        }

        public DataTable GetDimensions()
        {
            return this.DataLayer.ExecuteDataTable(this.ProcedureName, new List<SqlParameter>() { 
                new SqlParameter("@Mode", "SEL")
            });
        }
    }
}
