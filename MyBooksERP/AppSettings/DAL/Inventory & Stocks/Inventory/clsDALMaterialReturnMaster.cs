﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;

namespace MyBooksERP
{
    public class clsDALMaterialReturnMaster
    {
        DataLayer MobjDataLayer;
        ArrayList alParameters;
        string strMaterialReturnProcedureName = "dbo.spInvMaterialReturn";

        public clsDTOMaterialReturnMaster PobjclsDTOMaterialReturnMaster { get; set; }

        public clsDALMaterialReturnMaster(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
        }

        public bool SaveMaterialReturnMaster()
        {
            alParameters = new ArrayList();
            if (PobjclsDTOMaterialReturnMaster.MaterialReturnID == 0)
                alParameters.Add(new SqlParameter("@Mode", 2));
            else
                alParameters.Add(new SqlParameter("@Mode", 3));
            alParameters.Add(new SqlParameter("@MaterialReturnID", PobjclsDTOMaterialReturnMaster.MaterialReturnID));
            alParameters.Add(new SqlParameter("@SlNo", PobjclsDTOMaterialReturnMaster.SlNo));
            alParameters.Add(new SqlParameter("@MaterialReturnNo", PobjclsDTOMaterialReturnMaster.MaterialReturnNo));
            alParameters.Add(new SqlParameter("@MaterialReturnDate", PobjclsDTOMaterialReturnMaster.MaterialReturnDate));
            alParameters.Add(new SqlParameter("@WareHouseID", PobjclsDTOMaterialReturnMaster.WarehouseID));
            alParameters.Add(new SqlParameter("@ProjectID", PobjclsDTOMaterialReturnMaster.ProjectID));
            alParameters.Add(new SqlParameter("@CompanyID", PobjclsDTOMaterialReturnMaster.CompanyID));
            alParameters.Add(new SqlParameter("@Remarks", PobjclsDTOMaterialReturnMaster.Remarks));
            alParameters.Add(new SqlParameter("@NetAmount", PobjclsDTOMaterialReturnMaster.NetAmount));
            alParameters.Add(new SqlParameter("@CreatedBy", PobjclsDTOMaterialReturnMaster.CreatedBy));
            alParameters.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")));
            alParameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.MaterialReturn));
            alParameters.Add(new SqlParameter("@XmlMaterialReturnDetails", PobjclsDTOMaterialReturnMaster.MaterialReturnDetails.ToXml()));
            alParameters.Add(new SqlParameter("@XmlMaterialReturnLocationDetails", PobjclsDTOMaterialReturnMaster.MaterialReturnLocationDetails.ToXml()));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            alParameters.Add(objParam);

            if (MobjDataLayer.ExecuteNonQueryWithTran(strMaterialReturnProcedureName, alParameters) !=0)
            {
                return true;
            }
            return false;
        }

        public bool DeleteMaterialReturn()
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 7));

            alParameters.Add(new SqlParameter("@MaterialReturnID", PobjclsDTOMaterialReturnMaster.MaterialReturnID));
            alParameters.Add(new SqlParameter("@WareHouseID", PobjclsDTOMaterialReturnMaster.WarehouseID));
            alParameters.Add(new SqlParameter("@CompanyID", PobjclsDTOMaterialReturnMaster.CompanyID));
            alParameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
            alParameters.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")));
            alParameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.MaterialReturn));

            if (MobjDataLayer.ExecuteNonQueryWithTran(strMaterialReturnProcedureName, alParameters) !=0)
            {
                return true;
            }
            return false;
        }

        public bool FillMaterialReturnMasterInfo()
        {
            DataTable datMaterialReturn = new DataTable();
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@MaterialReturnID", PobjclsDTOMaterialReturnMaster.MaterialReturnID));
            datMaterialReturn = MobjDataLayer.ExecuteDataTable(strMaterialReturnProcedureName, alParameters);

            if (datMaterialReturn.Rows.Count > 0)
            {
                PobjclsDTOMaterialReturnMaster.MaterialReturnID = datMaterialReturn.Rows[0]["MaterialReturnID"].ToInt64();
                PobjclsDTOMaterialReturnMaster.SlNo = datMaterialReturn.Rows[0]["SlNo"].ToInt64();
                PobjclsDTOMaterialReturnMaster.MaterialReturnNo = datMaterialReturn.Rows[0]["MaterialReturnNo"].ToStringCustom();
                PobjclsDTOMaterialReturnMaster.MaterialReturnDate = datMaterialReturn.Rows[0]["MaterialReturnDate"].ToStringCustom();
                PobjclsDTOMaterialReturnMaster.WarehouseID = datMaterialReturn.Rows[0]["WarehouseID"].ToInt32();
                PobjclsDTOMaterialReturnMaster.ProjectID = datMaterialReturn.Rows[0]["ProjectID"].ToInt32();
                PobjclsDTOMaterialReturnMaster.CompanyID = datMaterialReturn.Rows[0]["CompanyID"].ToInt32();
                PobjclsDTOMaterialReturnMaster.CreatedBy = datMaterialReturn.Rows[0]["CreatedBy"].ToInt32();
                PobjclsDTOMaterialReturnMaster.CreatedByText = datMaterialReturn.Rows[0]["CreatedByText"].ToStringCustom();
                PobjclsDTOMaterialReturnMaster.CreatedDate = datMaterialReturn.Rows[0]["CreatedDate"].ToStringCustom();
                PobjclsDTOMaterialReturnMaster.Remarks = datMaterialReturn.Rows[0]["Remarks"].ToStringCustom();
                PobjclsDTOMaterialReturnMaster.NetAmount = datMaterialReturn.Rows[0]["NetAmount"].ToDecimal();

                return true;
            }
            return false;
        }

        public DataTable GetMaterialReturnDetails()
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 8));
            alParameters.Add(new SqlParameter("@MaterialReturnID", PobjclsDTOMaterialReturnMaster.MaterialReturnID));
            return MobjDataLayer.ExecuteDataTable(strMaterialReturnProcedureName, alParameters);
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 10));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@UomTypeID", 1));
            DataTable dtItemUoms = new DataTable();
            dtItemUoms = MobjDataLayer.ExecuteDataTable("dbo.spInvPurchaseModuleFunctions", alParameters);

            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 12));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            DataTable dtItemBaseUnit = MobjDataLayer.ExecuteDataTable("dbo.spInvPurchaseModuleFunctions", alParameters);
            DataRow dr = dtItemUoms.NewRow();
            dr["UOMID"] = dtItemBaseUnit.Rows[0]["UOMID"];
            dr["Description"] = dtItemBaseUnit.Rows[0]["Description"];
            dr["ShortName"] = dtItemBaseUnit.Rows[0]["ShortName"];
            dr["ItemID"] = intItemID;
            dtItemUoms.Rows.InsertAt(dr, 0);
            return dtItemUoms;
        }

        public DataTable GetItems(int intCompanyID,int intWarehouseID,int intProjectID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 9));
            alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            alParameters.Add(new SqlParameter("@WareHouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@ProjectID", intProjectID));
            return MobjDataLayer.ExecuteDataTable(strMaterialReturnProcedureName, alParameters);
        }

        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 14));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@LocationWise", ClsCommonSettings.PblnIsLocationWise));
            return MobjDataLayer.ExecuteDataTable("dbo.spInvItemLocationTransfer", alParameters).Rows[0];
        }

        public DataTable GetAllMaterialReturns(string strCondition)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 10));
            alParameters.Add(new SqlParameter("@SearchCondition", strCondition));
            return MobjDataLayer.ExecuteDataTable(strMaterialReturnProcedureName, alParameters);
        }

        public DataSet GetMaterialReturnReport() //Material Return Report
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 11));
            alParameters.Add(new SqlParameter("@MaterialReturnID", PobjclsDTOMaterialReturnMaster.MaterialReturnID));
            return MobjDataLayer.ExecuteDataSet(strMaterialReturnProcedureName, alParameters);
        }

    }
}
