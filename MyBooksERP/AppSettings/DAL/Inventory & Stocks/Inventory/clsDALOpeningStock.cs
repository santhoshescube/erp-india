﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Data;
using System.Data.SqlClient;


namespace MyBooksERP
{
    public class clsDALOpeningStock
    {
        ArrayList prmOpeningStock;
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOOpeningStock objDTOOpeningStock { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        bool blnAddStatus = false, blnUpdateStatus = false, blnDeleteStatus = false;
        bool blnStatus = false;

        public clsDALOpeningStock(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                    return objClsCommonUtility.FillCombos(saFieldValues);
            }
            return null;
        }

        public bool GetOpeningStockInfo()
        {
            bool blnRead = false;
            prmOpeningStock = new ArrayList();
            prmOpeningStock.Add(new SqlParameter("@Mode", 1));
            prmOpeningStock.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));


            prmOpeningStock.Add(new SqlParameter("@RowNumber", objDTOOpeningStock.intRowNumber));

            using (DataTable datOpeningStockDetails = MobjDataLayer.ExecuteDataTable("spInvOpeningStock", prmOpeningStock))
            {
                if (datOpeningStockDetails.Rows.Count>0)
                {
                    objDTOOpeningStock.intCompanyID = Convert.ToInt32(datOpeningStockDetails.Rows[0]["CompanyID"]);
                    objDTOOpeningStock.intWarehouseID = Convert.ToInt32(datOpeningStockDetails.Rows[0]["WarehouseID"]);
                    objDTOOpeningStock.strOpeningDate = Convert.ToString(datOpeningStockDetails.Rows[0]["OpeningDate"]);
                    objDTOOpeningStock.intOpeningStockID = Convert.ToInt64(datOpeningStockDetails.Rows[0]["OpeningStockID"]);
                    objDTOOpeningStock.strCreatedDate = Convert.ToString(datOpeningStockDetails.Rows[0]["CreatedDate"]);
                    blnRead = true;
                }
                
            }
            return blnRead;
        }

        public DataTable GetOpeningStockDetails()
        {
            ArrayList prmOpeningStockDetails = new ArrayList();
            prmOpeningStockDetails.Add(new SqlParameter("@Mode", 6));
            prmOpeningStockDetails.Add(new SqlParameter("@OpeningStockID", objDTOOpeningStock.intOpeningStockID));
            return MobjDataLayer.ExecuteDataTable("spInvOpeningStock", prmOpeningStockDetails);
        }

        public bool SaveOpeningStockInfo()
        {
            prmOpeningStock = new ArrayList();
            if (objDTOOpeningStock.intOpeningStockID == 0)
            {
                prmOpeningStock.Add(new SqlParameter("@Mode", 2));
                blnAddStatus = true;
                blnUpdateStatus = false;
                blnDeleteStatus = false;
            }
            else
            {
                //prmOpeningStock.Add(new SqlParameter("@Mode", 3));
                //DeleteOpeningStockDetails();
                prmOpeningStock.Add(new SqlParameter("@Mode", 3));
                //updateOpStockAccountDetailsEdit(objDTOOpeningStock.intOpeningStockID);
                blnAddStatus = false;
                blnUpdateStatus = true;
                blnDeleteStatus = false;                
            }
         
            prmOpeningStock.Add(new SqlParameter("@OpeningStockID", objDTOOpeningStock.intOpeningStockID));
            prmOpeningStock.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            prmOpeningStock.Add(new SqlParameter("@WarehouseID", objDTOOpeningStock.intWarehouseID));
            prmOpeningStock.Add(new SqlParameter("@OpeningDate", objDTOOpeningStock.strOpeningDate));
            prmOpeningStock.Add(new SqlParameter("@CreatedBy", objDTOOpeningStock.intCreatedBy));
            prmOpeningStock.Add(new SqlParameter("@CreatedDate", objDTOOpeningStock.strCreatedDate));
            prmOpeningStock.Add(new SqlParameter("@ModifiedBy", objDTOOpeningStock.intModifiedBy));
            prmOpeningStock.Add(new SqlParameter("@ModifiedDate", objDTOOpeningStock.strModifiedDate));
            prmOpeningStock.Add(new SqlParameter("@OperationTypeID", objDTOOpeningStock.intOperationTypeID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmOpeningStock.Add(objParam);

            object objOutOpeningStockID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvOpeningStock", prmOpeningStock, out objOutOpeningStockID) != 0)
            {
                if (Convert.ToInt64(objOutOpeningStockID) > 0)
                {
                    objDTOOpeningStock.intOpeningStockID = Convert.ToInt64(objOutOpeningStockID);

                    DeleteOpeningStockDetails();
                    SaveOpeningStockDetails();

                    //UpdateOpStockAccountDetails(objDTOOpeningStock.intOpeningStockID);
                    return true;
                }
            }
            return false;
        }

        private void SaveOpeningStockDetails()
        {
            int intSerialNo = 0;
            foreach (clsDTOOpeningStockDetails objOpeningStockDetails in objDTOOpeningStock.lstOpeningStockDetails)
            
            {
                prmOpeningStock = new ArrayList();
                prmOpeningStock.Add(new SqlParameter("@Mode", 7));
                prmOpeningStock.Add(new SqlParameter("@SerialNo", ++intSerialNo));
                prmOpeningStock.Add(new SqlParameter("@OpeningStockID", objDTOOpeningStock.intOpeningStockID));

                objOpeningStockDetails.intBatchID = BatchUpdation(objOpeningStockDetails);
                prmOpeningStock.Add(new SqlParameter("@BatchID", objOpeningStockDetails.intBatchID));
                prmOpeningStock.Add(new SqlParameter("@ItemID", objOpeningStockDetails.intItemID));
                prmOpeningStock.Add(new SqlParameter("@UOMID", objOpeningStockDetails.intUOMID));
                prmOpeningStock.Add(new SqlParameter("@Quantity", objOpeningStockDetails.decQuantity));
                prmOpeningStock.Add(new SqlParameter("@Rate", objOpeningStockDetails.decRate));
                prmOpeningStock.Add(new SqlParameter("@ExpiryDate", objOpeningStockDetails.strExpiryDate));
                prmOpeningStock.Add(new SqlParameter("@OperationTypeID", objDTOOpeningStock.intOperationTypeID));

                StockMasterUpdation(objOpeningStockDetails, false);
                StockDetailsUpdation(objOpeningStockDetails, false);
                //UpdateLastSaleRateOfItem(objOpeningStockDetails.decSaleRate, objOpeningStockDetails.intItemID, objOpeningStockDetails.intUOMID);

                //clsDTOLocationDetails objLocationDetails = new clsDTOLocationDetails();
                //objLocationDetails.decQuantity = ConvertQtyToBaseUnitQty(objOpeningStockDetails.intUOMID, objOpeningStockDetails.intItemID, objOpeningStockDetails.decQuantity);
                //objLocationDetails.intItemID = objOpeningStockDetails.intItemID;
                //objLocationDetails.lngBatchID = objOpeningStockDetails.intBatchID;
                //DataRow drLocation = GetItemDefaultLocation(objOpeningStockDetails.intItemID, objDTOOpeningStock.intWarehouseID);
                //objLocationDetails.intLocationID = drLocation["LocationID"].ToInt32();
                //objLocationDetails.intRowID = drLocation["RowID"].ToInt32();
                //objLocationDetails.intBlockID = drLocation["BlockID"].ToInt32();
                //objLocationDetails.intLotID = drLocation["LotID"].ToInt32();
                //UpdateItemLocationDetails(objLocationDetails, false);

                MobjDataLayer.ExecuteNonQueryWithTran("spInvOpeningStock", prmOpeningStock);
                
                SaveCostingMethodDetails(objDTOOpeningStock.intOpeningStockID.ToInt32(), objOpeningStockDetails.intItemID,
                    objOpeningStockDetails.intBatchID.ToInt32(), objOpeningStockDetails.intUOMID);
            }
            if (blnStatus == true)
                UpdateStockValue();
        }

        private bool UpdateStockValue()
        {
            //This is to Update Stock Value

            ArrayList prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation.Add(new SqlParameter("@Mode", 12));
            prmItemStockValueUpdation.Add(new SqlParameter("@OpeningStockID", objDTOOpeningStock.intOpeningStockID));
            prmItemStockValueUpdation.Add(new SqlParameter("@OperationTypeID", (int)OperationType.OpeningStock));

            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvOpeningStock", prmItemStockValueUpdation) != 0)
                return true;
            return false;
        }

        public bool DeleteOpeningStockInfo()
        {
            blnAddStatus = false;
            blnUpdateStatus = false;
            blnDeleteStatus = true;
            //UpdateOpStockAccountDetails(objDTOOpeningStock.intOpeningStockID);

            prmOpeningStock = new ArrayList();
            prmOpeningStock.Add(new SqlParameter("@Mode", 4));
            prmOpeningStock.Add(new SqlParameter("@OpeningStockID", objDTOOpeningStock.intOpeningStockID));
            prmOpeningStock.Add(new SqlParameter("@OperationTypeID", objDTOOpeningStock.intOperationTypeID));

            DeleteOpeningStockDetails();
            if (MobjDataLayer.ExecuteNonQuery("spInvOpeningStock", prmOpeningStock) != 0)
            {
                return true;
            }
            return false;
        }

        public bool DeleteOpeningStockDetails()
        {
            DataTable dtOpeningStockDetails = GetOpeningStockDetails();
            foreach (DataRow dr in dtOpeningStockDetails.Rows)
            {
                clsDTOOpeningStockDetails objDTOOpeningStockDetails = new clsDTOOpeningStockDetails();
                objDTOOpeningStockDetails.intItemID = Convert.ToInt32(dr["ItemID"]);
                objDTOOpeningStockDetails.intBatchID = Convert.ToInt64(dr["BatchID"]);
                objDTOOpeningStockDetails.intUOMID = Convert.ToInt32(dr["UOMID"]);
                objDTOOpeningStockDetails.decQuantity = Convert.ToDecimal(dr["Quantity"]);

                StockMasterUpdation(objDTOOpeningStockDetails, true);
                StockDetailsUpdation(objDTOOpeningStockDetails, true);

                //clsDTOLocationDetails objLocationDetails = new clsDTOLocationDetails();
                //objLocationDetails.decQuantity = ConvertQtyToBaseUnitQty(Convert.ToInt32(dr["UOMID"]), Convert.ToInt32(dr["ItemID"]), Convert.ToDecimal(dr["Quantity"]));
                //objLocationDetails.intItemID = Convert.ToInt32(dr["ItemID"]);
                //objLocationDetails.lngBatchID = Convert.ToInt64(dr["BatchID"]);
                //DataRow drLocation = GetItemDefaultLocation(Convert.ToInt32(dr["ItemID"]), objDTOOpeningStock.intWarehouseID);
                //objLocationDetails.intLocationID = drLocation["LocationID"].ToInt32();
                //objLocationDetails.intRowID = drLocation["RowID"].ToInt32();
                //objLocationDetails.intBlockID = drLocation["BlockID"].ToInt32();
                //objLocationDetails.intLotID = drLocation["LotID"].ToInt32();
                //UpdateItemLocationDetails(objLocationDetails, true);

                //DeleteCostingAndPricingDetails(objDTOOpeningStockDetails.intItemID.ToInt32(), objDTOOpeningStockDetails.intBatchID.ToInt32());
                // MobjDataLayer.ExecuteNonQueryWithTran("spInvOpeningStock", prmOpeningStock);
                //DeleteCostingAndPricingDetailsFromOpeningStock(objLocationDetails.intItemID, objLocationDetails.lngBatchID.ToInt32());
            }

            ArrayList alParameters = new ArrayList();

            alParameters.Add(new SqlParameter("@Mode", 8));
            alParameters.Add(new SqlParameter("@OpeningStockID", objDTOOpeningStock.intOpeningStockID));
            alParameters.Add(new SqlParameter("@OperationTypeID", objDTOOpeningStock.intOperationTypeID));

            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvOpeningStock", alParameters) != 0)
                return true;
            else return false;
        }

        public int GetRecordCount()
        {
            prmOpeningStock = new ArrayList();
            prmOpeningStock.Add(new SqlParameter("@Mode", 5));
            prmOpeningStock.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));

            return Convert.ToInt32(MobjDataLayer.ExecuteScalar("spInvOpeningStock", prmOpeningStock));
        }
        public DataTable GetItemUOMs(int intItemID)
        {
            ArrayList prmUomDetails = new ArrayList();
            prmUomDetails.Add(new SqlParameter("@Mode", 10));
            prmUomDetails.Add(new SqlParameter("@ItemID", intItemID));
            //prmUomDetails.Add(new SqlParameter("@UomTypeID", 2));
            DataTable dtItemUoms = new DataTable();
            dtItemUoms = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmUomDetails);

            prmUomDetails = new ArrayList();
            prmUomDetails.Add(new SqlParameter("@Mode", 12));
            prmUomDetails.Add(new SqlParameter("@ItemID", intItemID));
            DataTable dtItemBaseUnit = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmUomDetails);
            DataRow dr = dtItemUoms.NewRow();
            dr["UOMID"] = dtItemBaseUnit.Rows[0]["UOMID"];
            dr["Description"] = dtItemBaseUnit.Rows[0]["Description"];
            dr["ShortName"] = dtItemBaseUnit.Rows[0]["ShortName"];
            dr["ItemID"] = intItemID;
            dtItemUoms.Rows.InsertAt(dr, 0);
            return dtItemUoms;
        }
        public DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {
            ArrayList prmUomDetails = new ArrayList();
            prmUomDetails.Add(new SqlParameter("@Mode", 11));
            prmUomDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmUomDetails.Add(new SqlParameter("@UnitID", intUOMID));
       //     prmUomDetails.Add(new SqlParameter("@UomTypeID", 2));
            return MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmUomDetails);
        }
        private bool StockMasterUpdation(clsDTOOpeningStockDetails objOpeningStockDetails,bool blnIsDeleted)
        {
            ArrayList prmStock = new ArrayList();
            prmStock.Add(new SqlParameter("@Mode", 1));
            prmStock.Add(new SqlParameter("@ItemID", objOpeningStockDetails.intItemID));
            DataTable dtStockMaster = MobjDataLayer.ExecuteDataTable("spInvStockMaster", prmStock);
            DataTable dtGetUomDetails = GetUomConversionValues(objOpeningStockDetails.intUOMID, objOpeningStockDetails.intItemID);
            decimal decQuantity = objOpeningStockDetails.decQuantity;
            if (dtGetUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                {
                    decQuantity = decQuantity / decConversionValue;
                }
                else if (intConversionFactor == 2)
                {
                    decQuantity = decQuantity * decConversionValue;
                }
            }
            if (dtStockMaster.Rows.Count > 0)
            {
                decimal decGRNQty, decInvoicedQty, decOpeningStock = 0, decSoldQuantity = 0, decDamagedQuantity = 0, decDemoQuantity = 0, decTransferedQty = 0, decReceivedFromTransfer = 0;
                decGRNQty = dtStockMaster.Rows[0]["GRNQuantity"].ToDecimal();
                decInvoicedQty = dtStockMaster.Rows[0]["InvoicedQuantity"].ToDecimal();
                decSoldQuantity = dtStockMaster.Rows[0]["SoldQuantity"].ToDecimal();
                decDamagedQuantity = dtStockMaster.Rows[0]["DamagedQuantity"].ToDecimal();
                decTransferedQty = dtStockMaster.Rows[0]["TransferedQuantity"].ToDecimal();
                decReceivedFromTransfer = dtStockMaster.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                decDemoQuantity = dtStockMaster.Rows[0]["DemoQuantity"].ToDecimal();
                    if (!blnIsDeleted)
                    {
                        decOpeningStock = dtStockMaster.Rows[0]["OpeningStock"].ToDecimal() + decQuantity;
                        decGRNQty = decGRNQty + decQuantity;
                    }
                    else
                    {
                        decOpeningStock = dtStockMaster.Rows[0]["OpeningStock"].ToDecimal() - decQuantity;
                        decGRNQty = decGRNQty - decQuantity;
                    }
                prmStock = new ArrayList();
                prmStock.Add(new SqlParameter("@Mode", 3));
                prmStock.Add(new SqlParameter("@ItemID", objOpeningStockDetails.intItemID));
                prmStock.Add(new SqlParameter("@GRNQuantity", decGRNQty));
                prmStock.Add(new SqlParameter("@InvoicedQuantity", decInvoicedQty));
                prmStock.Add(new SqlParameter("@OpeningStock", decOpeningStock));
                prmStock.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
                prmStock.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));
                prmStock.Add(new SqlParameter("@SoldQuantity", decSoldQuantity));
                prmStock.Add(new SqlParameter("@DamagedQuantity", decDamagedQuantity));
                prmStock.Add(new SqlParameter("@DemoQuantity", decDemoQuantity));
                prmStock.Add(new SqlParameter("@LastModifiedBy", objDTOOpeningStock.intModifiedBy));
                prmStock.Add(new SqlParameter("@LastModifiedDate", objDTOOpeningStock.strModifiedDate));
                MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmStock);
            }
            else
            {
                prmStock = new ArrayList();
                prmStock.Add(new SqlParameter("@Mode", 2));
                prmStock.Add(new SqlParameter("@ItemID", objOpeningStockDetails.intItemID));
                prmStock.Add(new SqlParameter("@OpeningStock", decQuantity));
                prmStock.Add(new SqlParameter("@GRNQuantity", decQuantity));
                prmStock.Add(new SqlParameter("@LastModifiedBy", objDTOOpeningStock.intCreatedBy));
                prmStock.Add(new SqlParameter("@LastModifiedDate", objDTOOpeningStock.strCreatedDate));
                MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmStock);
            }
            return true;
        }

        private bool StockDetailsUpdation(clsDTOOpeningStockDetails objClsDtoOpeningStockDetails,bool blnIsDeleted)
        {
            ArrayList prmStockDetails = new ArrayList();
            prmStockDetails.Add(new SqlParameter("@Mode", 4));
            prmStockDetails.Add(new SqlParameter("@ItemID", objClsDtoOpeningStockDetails.intItemID));
            prmStockDetails.Add(new SqlParameter("@BatchID", objClsDtoOpeningStockDetails.intBatchID));
            prmStockDetails.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            prmStockDetails.Add(new SqlParameter("@WarehouseID", objDTOOpeningStock.intWarehouseID));
            DataTable dtStockDetails = MobjDataLayer.ExecuteDataTable("spInvStockMaster", prmStockDetails);
            DataTable dtGetUomDetails = GetUomConversionValues(objClsDtoOpeningStockDetails.intUOMID, objClsDtoOpeningStockDetails.intItemID);
            decimal decQuantity = objClsDtoOpeningStockDetails.decQuantity;
            if (dtGetUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                    decQuantity = decQuantity / decConversionValue;
                else if (intConversionFactor == 2)
                    decQuantity = decQuantity * decConversionValue;
            }
            prmStockDetails = new ArrayList();
            prmStockDetails.Add(new SqlParameter("@ItemID", objClsDtoOpeningStockDetails.intItemID));
            prmStockDetails.Add(new SqlParameter("@BatchID", objClsDtoOpeningStockDetails.intBatchID));
            prmStockDetails.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            prmStockDetails.Add(new SqlParameter("@WarehouseID", objDTOOpeningStock.intWarehouseID));
            if (dtStockDetails.Rows.Count > 0)
            {
                decimal decGRNQty,decInvoicedQty,decOpeningStock = 0,decSoldQuantity=0,decDamagedQuantity=0,decDemoQuantity = 0,decTransferedQty=0,decReceivedFromTransfer=0;
                decGRNQty = dtStockDetails.Rows[0]["GRNQuantity"].ToDecimal();
                decInvoicedQty = dtStockDetails.Rows[0]["InvoicedQuantity"].ToDecimal();
                decSoldQuantity = dtStockDetails.Rows[0]["SoldQuantity"].ToDecimal();
                decDamagedQuantity = dtStockDetails.Rows[0]["DamagedQuantity"].ToDecimal();
                decTransferedQty = dtStockDetails.Rows[0]["TransferedQuantity"].ToDecimal();
                decReceivedFromTransfer = dtStockDetails.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                decDemoQuantity = dtStockDetails.Rows[0]["DemoQuantity"].ToDecimal();
                    if (!blnIsDeleted)
                    {
                        decOpeningStock = dtStockDetails.Rows[0]["OpeningStock"].ToDecimal() + decQuantity;
                        decGRNQty = decGRNQty + decQuantity;
                    }
                    else
                    {
                        decOpeningStock = dtStockDetails.Rows[0]["OpeningStock"].ToDecimal() - decQuantity;
                        decGRNQty = decGRNQty - decQuantity;
                    }
                prmStockDetails.Add(new SqlParameter("@Mode", 6));
                prmStockDetails.Add(new SqlParameter("@GRNQuantity", decGRNQty));
                prmStockDetails.Add(new SqlParameter("@InvoicedQuantity", decInvoicedQty));
                prmStockDetails.Add(new SqlParameter("@OpeningStock", decOpeningStock));
                prmStockDetails.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
                prmStockDetails.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));
                prmStockDetails.Add(new SqlParameter("@SoldQuantity", decSoldQuantity));
                prmStockDetails.Add(new SqlParameter("@DamagedQuantity", decDamagedQuantity));
                prmStockDetails.Add(new SqlParameter("@DemoQuantity", decDemoQuantity));

                MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmStockDetails);
            }
            else
            {
                    prmStockDetails.Add(new SqlParameter("@Mode", 5));
                    prmStockDetails.Add(new SqlParameter("@OpeningStock", decQuantity));
                    prmStockDetails.Add(new SqlParameter("@GRNQuantity", decQuantity));
                MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmStockDetails);
            }
            return true;
        }

        private Int64 FindBatchID(string strBatchNo)
        {
            ArrayList prmBatchDetails = new ArrayList();
            prmBatchDetails.Add(new SqlParameter("@Mode", 2));
            prmBatchDetails.Add(new SqlParameter("@BatchNo", strBatchNo));
            DataTable dtBatchDetails = MobjDataLayer.ExecuteDataTable("spInvBatchDetailsTransaction", prmBatchDetails);
            if (dtBatchDetails.Rows.Count > 0)
                return Convert.ToInt64(dtBatchDetails.Rows[0]["BatchID"]);
            else
                return -1;
        }
        public Int32 GetCostingMethod(Int64 intItemID)
        {
            ArrayList prmBatchDetails = new ArrayList();
            prmBatchDetails.Add(new SqlParameter("@Mode", 18));
            prmBatchDetails.Add(new SqlParameter("@ItemID", intItemID));
            DataTable dtBatchDetails = MobjDataLayer.ExecuteDataTable("spInvItemMaster", prmBatchDetails);
            if (dtBatchDetails.Rows.Count > 0)
                return dtBatchDetails.Rows[0]["CostingMethodID"].ToInt32();
            else
                return 0;
        }
        private Int64 BatchUpdation(clsDTOOpeningStockDetails objClsDtoOpeningStockDetails)
        {
            //if (!string.IsNullOrEmpty(objClsDtoOpeningStockDetails.strBatchNumber))
            //{
                Int64 intBatchID = -1;
                Int32 intCostingMethod = GetCostingMethod(objClsDtoOpeningStockDetails.intItemID);
                if (intCostingMethod == (int)CostingMethodReference.Batch)
                {
                    objClsDtoOpeningStockDetails.intBatchID = FindBatchID(objClsDtoOpeningStockDetails.strBatchNumber);
                }
                else
                {
                    objClsDtoOpeningStockDetails.intBatchID = -1;
                }

                Int64 intMaxbatchID = 0;
                ArrayList prmBatchDetails = new ArrayList();
                prmBatchDetails.Add(new SqlParameter("@Mode", 6));
                prmBatchDetails.Add(new SqlParameter("@ItemID", objClsDtoOpeningStockDetails.intItemID));
                intMaxbatchID = MobjDataLayer.ExecuteDataTable("spInvBatchDetailsTransaction", prmBatchDetails).Rows[0]["MaxBatchID"].ToInt64();
                prmBatchDetails = new ArrayList();
                //prmPurchase.Add(new SqlParameter("@BatchID", intBatchID));
                prmBatchDetails.Add(new SqlParameter("@BatchNo", objClsDtoOpeningStockDetails.strBatchNumber));
                prmBatchDetails.Add(new SqlParameter("@ItemID", objClsDtoOpeningStockDetails.intItemID));

                //decimal decPurchaseRate = objClsDtoOpeningStockDetails.decRate;

                //DataTable dtGetUomDetails = GetUomConversionValues(objClsDtoOpeningStockDetails.intUOMID, objClsDtoOpeningStockDetails.intItemID);
                //if (dtGetUomDetails.Rows.Count > 0)
                //{
                //    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                //    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                //    if (intConversionFactor == 1)
                //    {
                //        decPurchaseRate = decPurchaseRate * decConversionValue;
                //    }
                //    else if (intConversionFactor == 2)
                //    {
                //        decPurchaseRate = decPurchaseRate / decConversionValue;
                //    }
                //}

                //prmBatchDetails.Add(new SqlParameter("@PurchaseRate", decPurchaseRate));
                //prmBatchDetails.Add(new SqlParameter("@ActualRate", decPurchaseRate));
                prmBatchDetails.Add(new SqlParameter("@ExpiryDate", objClsDtoOpeningStockDetails.strExpiryDate));
                if (objClsDtoOpeningStockDetails.intBatchID == -1)
                {
                    if (intCostingMethod == (int)CostingMethodReference.Batch)
                    {
                        intBatchID = intMaxbatchID + 1;
                    }
                    else
                    {
                        intBatchID = 0;
                    }
                    prmBatchDetails.Add(new SqlParameter("@Mode", 3));
                    prmBatchDetails.Add(new SqlParameter("@BatchID", intBatchID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spInvBatchDetailsTransaction", prmBatchDetails);
                }
                else
                {
                    intBatchID = objClsDtoOpeningStockDetails.intBatchID;
                    prmBatchDetails.Add(new SqlParameter("@Mode", 5));
                    prmBatchDetails.Add(new SqlParameter("@BatchID", objClsDtoOpeningStockDetails.intBatchID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spInvBatchDetailsTransaction", prmBatchDetails);
                }
                return intBatchID;
            //}
            return 0;
        }
        public string StockValidation(bool isDeletion,out bool blnIsLocation,out bool blnIsSalesDone)
        {
            blnIsLocation = false;
            blnIsSalesDone = false;
            DataTable dtOpeningStockDetails = new DataTable();
                dtOpeningStockDetails = GetOpeningStockDetails();
            
            List<int> lstupdateIndices = new List<int>();
            if (isDeletion)
            {
                for (int i = 0; i < dtOpeningStockDetails.Rows.Count; i++)
                {
                    clsDTOOpeningStockDetails objDTOOpeningStockDetails = new clsDTOOpeningStockDetails();
                    objDTOOpeningStockDetails.intBatchID = Convert.ToInt64(dtOpeningStockDetails.Rows[i]["BatchID"]);
                    objDTOOpeningStockDetails.intItemID = Convert.ToInt32(dtOpeningStockDetails.Rows[i]["ItemID"]);
                    DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtOpeningStockDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtOpeningStockDetails.Rows[i]["ItemID"]));
                    decimal decOldQuantity = Convert.ToDecimal(dtOpeningStockDetails.Rows[i]["Quantity"]);
                    if (dtGetUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                            decOldQuantity = decOldQuantity / decConversionValue;
                        else if (intConversionFactor == 2)
                            decOldQuantity = decOldQuantity * decConversionValue;
                    }
                    if (IsSold(dtOpeningStockDetails.Rows[i], decOldQuantity, 0))
                        return Convert.ToString(dtOpeningStockDetails.Rows[i]["ItemName"]);
                    //if (GetDefaultLocationQuantity(objDTOOpeningStockDetails.intItemID, objDTOOpeningStockDetails.intBatchID, objDTOOpeningStock.intWarehouseID) - decOldQuantity < 0)
                    //{
                    //    blnIsLocation = true;
                    //    return Convert.ToString(dtOpeningStockDetails.Rows[i]["ItemName"]);
                    //}
                    if (objClsCommonUtility.FillCombos(new string[]{"CostingMethodID","InvItemDetails","ItemID = "+objDTOOpeningStockDetails.intItemID}).Rows[0]["CostingMethodID"].ToInt32() == (int)CostingMethodReference.Batch && objClsCommonUtility.GetStockQuantity(objDTOOpeningStockDetails.intItemID,objDTOOpeningStockDetails.intBatchID,ClsCommonSettings.LoginCompanyID,objDTOOpeningStock.intWarehouseID) - decOldQuantity < GetUsedQuantity(objDTOOpeningStockDetails.intItemID, objDTOOpeningStockDetails.intBatchID))
                    {
                        blnIsSalesDone = true;
                        return Convert.ToString(dtOpeningStockDetails.Rows[i]["ItemName"]);
                    }

                }
            }
            else
            {
                for (int i = 0; i < dtOpeningStockDetails.Rows.Count; i++)
                {
                    bool blnExists = false;
                    for (int j = 0; j < objDTOOpeningStock.lstOpeningStockDetails.Count; j++)
                    {
                        //if (objDTOOpeningStock.lstOpeningStockDetails[j].strBatchNumber == dtOpeningStockDetails.Rows[i]["BatchNo"].ToString())
                        if (objDTOOpeningStock.lstOpeningStockDetails[j].intItemID == dtOpeningStockDetails.Rows[i]["ItemID"].ToInt32())
                        {
                            objDTOOpeningStock.lstOpeningStockDetails[j].intBatchID = FindBatchID(objDTOOpeningStock.lstOpeningStockDetails[j].strBatchNumber);

                            DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtOpeningStockDetails.Rows[j]["UOMID"]), Convert.ToInt32(dtOpeningStockDetails.Rows[j]["ItemID"]));
                            decimal decOldQuantity = Convert.ToDecimal(dtOpeningStockDetails.Rows[j]["Quantity"]);
                            if (dtGetUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                    decOldQuantity = decOldQuantity / decConversionValue;
                                else if (intConversionFactor == 2)
                                    decOldQuantity = decOldQuantity * decConversionValue;
                            }
                            blnExists = true;
                            if (IsSold(dtOpeningStockDetails.Rows[i], decOldQuantity, objDTOOpeningStock.lstOpeningStockDetails[j].decQuantity))
                                return Convert.ToString(dtOpeningStockDetails.Rows[i]["ItemName"]);
                            //if ((GetDefaultLocationQuantity(objDTOOpeningStock.lstOpeningStockDetails[j].intItemID, objDTOOpeningStock.lstOpeningStockDetails[j].intBatchID, objDTOOpeningStock.intWarehouseID) - decOldQuantity) < 0)
                            //{
                            //    blnIsLocation = true;
                            //    return Convert.ToString(dtOpeningStockDetails.Rows[i]["ItemName"]);
                            //}
                            if (objClsCommonUtility.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + objDTOOpeningStock.lstOpeningStockDetails[j].intItemID }).Rows[0]["CostingMethodID"].ToInt32() == (int)CostingMethodReference.Batch && objClsCommonUtility.GetStockQuantity(objDTOOpeningStock.lstOpeningStockDetails[j].intItemID, objDTOOpeningStock.lstOpeningStockDetails[j].intBatchID, ClsCommonSettings.LoginCompanyID, objDTOOpeningStock.intWarehouseID) - decOldQuantity + objClsCommonUtility.ConvertQtyToBaseUnitQty(objDTOOpeningStock.lstOpeningStockDetails[j].intUOMID,objDTOOpeningStock.lstOpeningStockDetails[j].intItemID,objDTOOpeningStock.lstOpeningStockDetails[j].decQuantity,2) < GetUsedQuantity(objDTOOpeningStock.lstOpeningStockDetails[j].intItemID, objDTOOpeningStock.lstOpeningStockDetails[j].intBatchID))
                            {
                                blnIsSalesDone = true;
                                return Convert.ToString(dtOpeningStockDetails.Rows[i]["ItemName"]);
                            }
                        }
                    }
                    if (!blnExists)
                    {
                        clsDTOPurchaseDetails objDTOPurchaseDetails = new clsDTOPurchaseDetails();
                        objDTOPurchaseDetails.intBatchID = Convert.ToInt64(dtOpeningStockDetails.Rows[i]["BatchID"]);
                        objDTOPurchaseDetails.intItemID = Convert.ToInt32(dtOpeningStockDetails.Rows[i]["ItemID"]);
                        DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtOpeningStockDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtOpeningStockDetails.Rows[i]["ItemID"]));
                        decimal decOldQuantity = Convert.ToDecimal(dtOpeningStockDetails.Rows[i]["Quantity"]);
                        if (dtGetUomDetails.Rows.Count > 0)
                        {
                            int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                            decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                            if (intConversionFactor == 1)
                                decOldQuantity = decOldQuantity / decConversionValue;
                            else if (intConversionFactor == 2)
                                decOldQuantity = decOldQuantity * decConversionValue;
                        }
                        if (IsSold(dtOpeningStockDetails.Rows[i], decOldQuantity, 0))
                            return Convert.ToString(dtOpeningStockDetails.Rows[i]["ItemName"]);

                        //if (GetDefaultLocationQuantity(dtOpeningStockDetails.Rows[i]["ItemID"].ToInt32(), dtOpeningStockDetails.Rows[i]["BatchID"].ToInt64(), objDTOOpeningStock.intWarehouseID) - decOldQuantity < 0)
                        //{
                        //    blnIsLocation = true;
                        //    return Convert.ToString(dtOpeningStockDetails.Rows[i]["ItemName"]);
                        //}

                        if (objClsCommonUtility.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + dtOpeningStockDetails.Rows[i]["ItemID"].ToInt32() }).Rows[0]["CostingMethodID"].ToInt32() == (int)CostingMethodReference.Batch && objClsCommonUtility.GetStockQuantity(dtOpeningStockDetails.Rows[i]["ItemID"].ToInt32(), dtOpeningStockDetails.Rows[i]["BatchID"].ToInt64(), ClsCommonSettings.LoginCompanyID, objDTOOpeningStock.intWarehouseID) - decOldQuantity < GetUsedQuantity(dtOpeningStockDetails.Rows[i]["ItemID"].ToInt32(), dtOpeningStockDetails.Rows[i]["BatchID"].ToInt64()))
                        {
                            blnIsSalesDone = true;
                            return Convert.ToString(dtOpeningStockDetails.Rows[i]["ItemName"]);
                        }
                    }
                }
            }
            return null;
        }
        private bool IsSold(DataRow drOpeningStockDetails, decimal decOldQuantity, decimal decQuantity)
        {
            prmOpeningStock = new ArrayList();
            prmOpeningStock.Add(new SqlParameter("@Mode", 4));
            prmOpeningStock.Add(new SqlParameter("@ItemID", drOpeningStockDetails["ItemID"]));
            prmOpeningStock.Add(new SqlParameter("@BatchID", drOpeningStockDetails["BatchID"]));
            prmOpeningStock.Add(new SqlParameter("@CompanyID",  ClsCommonSettings.LoginCompanyID));
            prmOpeningStock.Add(new SqlParameter("@WarehouseID", objDTOOpeningStock.intWarehouseID));
            DataTable dtStockDetails = MobjDataLayer.ExecuteDataTable("spInvStockMaster", prmOpeningStock);
            DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(drOpeningStockDetails["UOMID"]), Convert.ToInt32(drOpeningStockDetails["ItemID"]));
            if (dtGetUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                    decQuantity = decQuantity / decConversionValue;
                else if (intConversionFactor == 2)
                    decQuantity = decQuantity * decConversionValue;
            }
            if (dtStockDetails.Rows.Count > 0)
            {
                decimal decGRNQty = 0;
               // //decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - decOldQuantity + decQuantity;
                decGRNQty = (dtStockDetails.Rows[0]["GRNQuantity"].ToDecimal() + dtStockDetails.Rows[0]["ReceivedFromTransfer"].ToDecimal()) - (dtStockDetails.Rows[0]["SoldQuantity"].ToDecimal() + dtStockDetails.Rows[0]["DamagedQuantity"].ToDecimal() + dtStockDetails.Rows[0]["TransferedQuantity"].ToDecimal() + dtStockDetails.Rows[0]["DemoQuantity"].ToDecimal());
                decGRNQty = decGRNQty - decOldQuantity + decQuantity;
               // decGRNQty = decQuantity - Convert.ToDecimal(dtStockDetails.Rows[0]["SoldQuantity"]);
                if (decGRNQty < 0)
                    return true;
            }
            return false;
        }
     
        //private void UpdateLastSaleRateOfItem(decimal decSaleRate, int intItemID, int intUOMID)
        //{
        //    if (decSaleRate != 0)
        //    {
        //        ArrayList prmItemDetails = new ArrayList();
        //        prmItemDetails.Add(new SqlParameter("@Mode", 13));
        //        prmItemDetails.Add(new SqlParameter("@ItemID", intItemID));
        //        DataTable dtGetUomDetails = GetUomConversionValues(intUOMID, intItemID);
        //        if (dtGetUomDetails.Rows.Count > 0)
        //        {
        //            int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
        //            decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
        //            if (intConversionFactor == 1)
        //                decSaleRate = decSaleRate * decConversionValue;
        //            else if (intConversionFactor == 2)
        //                decSaleRate = decSaleRate / decConversionValue;
        //        }
        //        prmItemDetails.Add(new SqlParameter("@Rate", decSaleRate));
        //        MobjDataLayer.ExecuteNonQueryWithTran("STPurchaseModule", prmItemDetails);
        //    }
        //}

        //Email
        public DataSet GetOpeningStockReport()
        {
            prmOpeningStock = new ArrayList();
            prmOpeningStock.Add(new SqlParameter("@Mode", 9));
            prmOpeningStock.Add(new SqlParameter("@OpeningStockID", objDTOOpeningStock.intOpeningStockID));
            return MobjDataLayer.ExecuteDataSet("spInvOpeningStock", prmOpeningStock);
        }

        public bool IsStockAdjusmentDone(int intItemID, Int64 intBatchID,int intWarehouseID)
        {
            ArrayList prmStockAdjustmentDetails = new ArrayList();
            prmStockAdjustmentDetails.Add(new SqlParameter("@Mode", "ChkSAD"));
            prmStockAdjustmentDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmStockAdjustmentDetails.Add(new SqlParameter("@BatchID", intBatchID));
            prmStockAdjustmentDetails.Add(new SqlParameter("@CreatedDate", objDTOOpeningStock.strCreatedDate));
            prmStockAdjustmentDetails.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            DataTable  datStockAdjustmentDetails = MobjDataLayer.ExecuteDataTable("spInvStockAdjustment", prmStockAdjustmentDetails);
            if (datStockAdjustmentDetails.Rows.Count >0)
            {
                if (datStockAdjustmentDetails.Rows[0]["DetailsCount"] != DBNull.Value)
                {
                    if (datStockAdjustmentDetails.Rows[0]["DetailsCount"].ToInt32() > 0)
                        return true;
                }
            }
            return false;
        }

        private bool UpdateItemLocationDetails(clsDTOLocationDetails objLocationDetails, bool blnIsDeletion)
        {
            ArrayList alParameters = new ArrayList();
            if (!blnIsDeletion)
                alParameters.Add(new SqlParameter("@Mode", 1));
            else
                alParameters.Add(new SqlParameter("@Mode", 2));
                
            alParameters.Add(new SqlParameter("@Type", 1));
            alParameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            alParameters.Add(new SqlParameter("@WarehouseID", objDTOOpeningStock.intWarehouseID));
            alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.intItemID));
            alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.lngBatchID));
            alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.intLocationID));
            alParameters.Add(new SqlParameter("@RowID", objLocationDetails.intRowID));
            alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.intBlockID));
            alParameters.Add(new SqlParameter("@LotID", objLocationDetails.intLotID));
            alParameters.Add(new SqlParameter("@Quantity", objLocationDetails.decQuantity));
            MobjDataLayer.ExecuteNonQueryWithTran("spInvItemLocationTransfer", alParameters);
            return true;
        }

        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 14));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@LocationWise", ClsCommonSettings.PblnIsLocationWise));
            return MobjDataLayer.ExecuteDataTable("spInvItemLocationTransfer", alParameters).Rows[0];
        }

        public decimal GetDefaultLocationQuantity(int intItemID, long lngBatchID, int intWarehouseID)
        {
            ArrayList alParameters = new ArrayList();
            DataRow dr = GetItemDefaultLocation(intItemID, intWarehouseID);
            decimal decQuantity = 0;
            alParameters.Add(new SqlParameter("@Mode", 13));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", lngBatchID));
            alParameters.Add(new SqlParameter("@LocationID", dr["LocationID"].ToInt32()));
            alParameters.Add(new SqlParameter("@RowID", dr["RowID"].ToInt32()));
            alParameters.Add(new SqlParameter("@BlockID", dr["BlockID"].ToInt32()));
            alParameters.Add(new SqlParameter("@LotID", dr["LotID"].ToInt32()));
            DataTable  datLocationDetails = MobjDataLayer.ExecuteDataTable("spInvItemLocationTransfer", alParameters);
            if (datLocationDetails.Rows.Count>0)
            {
                if (datLocationDetails.Rows[0]["Quantity"] != DBNull.Value)
                    decQuantity = datLocationDetails.Rows[0]["Quantity"].ToDecimal();
            }
            return decQuantity;
        }
         
        public decimal ConvertQtyToBaseUnitQty(int intUomID, int intItemId, decimal decQuantity)
        {
            DataTable dtUomDetails = GetUomConversionValues(intUomID, intItemId);
            if (dtUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                    decQuantity = decQuantity / decConversionValue;
                else if (intConversionFactor == 2)
                    decQuantity = decQuantity * decConversionValue;
            }
            return decQuantity;
        }

        private void DeleteCostingAndPricingDetails(int intItemID, int intBatchID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 16));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", intBatchID));
            MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", alParameters);
        }

        private void DeleteCostingAndPricingDetailsFromOpeningStock(int intItemID, int intBatchID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 16));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", intBatchID));
            MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", alParameters);

            CalculateCostingAndPricingAfterDelete(intItemID, intBatchID);
        }

        private void CalculateCostingAndPricingAfterDelete(int intTempItemID, int intBatchID)
        {
            DataTable datCosting = FillCombos(new string[] { "*", "InvCostingAndPricingDetails", "ItemID=" + intTempItemID +" AND BatchID > 0"});
            DataTable datTempItem = FillCombos(new string[] { "*", "InvItemMaster", "ItemID=" + intTempItemID });
            DataTable datTempBatch = new DataTable();
            if (datCosting != null)
            {
                if (datCosting.Rows.Count > 0)
                {
                    if (datCosting.Rows[0]["CostingMethodID"].ToInt32() != (int)CostingMethodReference.Batch && datCosting.Rows[0]["BatchID"].ToInt32() == intBatchID)
                    {
                        switch (datCosting.Rows[0]["CostingMethodID"].ToInt32())
                        {
                            case (int)CostingMethodReference.FIFO:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.FIFO, intTempItemID);
                                break;
                            case (int)CostingMethodReference.LIFO:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.LIFO, intTempItemID);
                                break;
                            case (int)CostingMethodReference.MAX:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.MAX, intTempItemID);
                                break;
                            case (int)CostingMethodReference.MIN:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.MIN, intTempItemID);
                                break;
                            case (int)CostingMethodReference.AVG:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.AVG, intTempItemID);
                                break;
                            case (int)CostingMethodReference.NONE:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.NONE, intTempItemID);
                                break;
                        }
                        if (datTempBatch != null && datTempBatch.Rows.Count > 0)
                        {
                            string strRate = "0", strActualRate = "0", strSaleRate = "0", strLandingCost = "0";
                            strRate = datTempBatch.Rows[0]["PurchaseRate"].ToString();
                            strActualRate = datTempBatch.Rows[0]["ActualRate"].ToString();
                            strLandingCost = datTempBatch.Rows[0]["LandingCost"].ToString();

                            ArrayList prms = new ArrayList();
                            prms.Add(new SqlParameter("@Mode", 9));
                            prms.Add(new SqlParameter("@CostingMethodID", datCosting.Rows[0]["CostingMethodID"].ToString().ToInt32()));
                            prms.Add(new SqlParameter("@PricingSchemeID", datCosting.Rows[0]["PricingSchemeID"].ToString().ToInt32()));
                            prms.Add(new SqlParameter("@ItemID", intTempItemID));
                            prms.Add(new SqlParameter("@CurrencyID", datCosting.Rows[0]["CurrencyID"].ToString().ToInt32()));
                            prms.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
                            prms.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDateTime()));
                            prms.Add(new SqlParameter("@BatchID", intBatchID));
                            prms.Add(new SqlParameter("@PurchaseRate", strRate.ToDouble()));
                            prms.Add(new SqlParameter("@ActualRate", strActualRate.ToDouble()));
                            prms.Add(new SqlParameter("@LandingCost", strLandingCost.ToDouble()));

                            DataTable datTempPricing = FillCombos(new string[] { "*", "InvPricingSchemeReference", "PricingSchemeID=" + datCosting.Rows[0]["PricingSchemeID"].ToString().ToInt32() });
                            double dblExRate = 1;

                            if (datTempItem.Rows[0]["IsLandingCostEffected"].ToStringCustom() == "")
                                datTempItem.Rows[0]["IsLandingCostEffected"] = "False";

                            if (Convert.ToBoolean(datTempItem.Rows[0]["IsLandingCostEffected"].ToString()) == true)
                            {
                                if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                    strSaleRate = (strLandingCost.ToDouble() +
                                        (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                else
                                    strSaleRate = (strLandingCost.ToDouble() +
                                                (strLandingCost.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                            }
                            else
                            {
                                if (Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()) == true)
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate = (strRate.ToDouble() +
                                            (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                    else
                                        strSaleRate = (strRate.ToDouble() +
                                                    (strRate.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                                }
                                else
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate = (strActualRate.ToDouble() +
                                            (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                    else
                                        strSaleRate = (strActualRate.ToDouble() +
                                                    (strActualRate.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                                }
                            }
                            prms.Add(new SqlParameter("@SaleRate", strSaleRate.ToDouble()));
                            DeleteCostingAndPricingDetails(intTempItemID, intBatchID);
                            MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", prms);
                        }
                    }
                }
            }
        }

        public DataTable GetPurchaseRateAndActualRate(int intMethodID, int intItemID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 18));
            parameters.Add(new SqlParameter("@CostingMethodID", intMethodID));
            parameters.Add(new SqlParameter("@ItemID", intItemID));
            DataTable datRate = MobjDataLayer.ExecuteDataTable("spInvPricingScheme", parameters);
            return datRate;
        }

        public void SaveCostingMethodDetails(int OpStID, int intItemID, int intBatchID, int intUomID)
        {
            blnStatus = false;
            try
            {
                DataTable datTempOpStMaster = FillCombos(new string[] { "*", "InvOpeningStock", "OpeningStockID=" + OpStID });
                if (datTempOpStMaster != null)
                {
                    if (datTempOpStMaster.Rows.Count > 0)
                    {                        
                        DataTable datTempOpSt = FillCombos(new string[] { "*", "InvOpeningStockDetails", "OpeningStockID=" + OpStID + " AND ItemID=" + intItemID + " AND BatchID=" + intBatchID });
                        DataTable datTempItem = FillCombos(new string[] { "*", "InvItemMaster", "ItemID=" + intItemID });
                        DataTable datTemp = FillCombos(new string[] { "*", "InvCostingAndPricingDetails", "ItemID=" + intItemID });
                        DataTable datTempUom = FillCombos(new string[] { "*", "InvUOMConversions", "UOMID = " + datTempItem.Rows[0]["BaseUomID"].ToString() + 
                            " AND OtherUOMID=" + intUomID });
                        DataTable datTempCurrency = FillCombos(new string[] { "CurrencyId AS CurrencyID", "CompanyMaster", "CompanyID = " +
                            "(SELECT (CASE WHEN CompanyID = 0 THEN 1 ELSE ISNULL(CompanyID,1) END) FROM InvItemMaster WHERE " +
                            "ItemID= " + intItemID + ")" });

                        string strRate = "0", strActualRate = "0", strSaleRate = "0", strLandingCost = "0";
                        //DataTable datTempRate = GetSaleRateForBatch(intItemID, datTemp.Rows[0]["PricingSchemeID"].ToString().ToInt32(),
                        //    datTempItem.Rows[0]["CalculationBasedOnRate"].ToBoolean());

                        DataTable datTempRate = GetSaleRate(datTemp.Rows[0]["CostingMethodID"].ToInt32(),
                            intItemID, Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()));

                        if (datTempRate != null && datTempRate.Rows.Count > 0)
                        {
                            //try
                            //{
                            //    datTempOpSt.DefaultView.RowFilter = "BatchID=" + intBatchID;
                            //    strRate = datTempOpSt.DefaultView.ToTable().Rows[0]["Rate"].ToString();
                            //    strActualRate = datTempOpSt.DefaultView.ToTable().Rows[0]["Rate"].ToString();
                            //}
                            //catch { }

                            if (strRate == "0")
                                strRate = datTempRate.Rows[0]["Rate"].ToString();
                            if (strActualRate == "0")
                                strActualRate = datTempRate.Rows[0]["ActualRate"].ToString();
                            if (strLandingCost == "0")
                                strLandingCost = datTempRate.Rows[0]["LandingCost"].ToString();

                            if (intUomID != datTempItem.Rows[0]["BaseUomID"].ToInt32())
                            {
                                if (datTempUom.Rows[0]["ConversionFactorID"].ToString() == "1")
                                {
                                    strRate = (strRate.ToDouble() * datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strActualRate = (strActualRate.ToDouble() * datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strLandingCost = (strLandingCost.ToDouble() * datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                }
                                else
                                {
                                    strRate = (strRate.ToDouble() / datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strActualRate = (strActualRate.ToDouble() / datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strLandingCost = (strLandingCost.ToDouble() / datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                }
                            }
                            if (datTempOpStMaster.Rows[0]["CompanyID"].ToInt32() != datTempItem.Rows[0]["CompanyID"].ToInt32())
                            {
                                int intComID = 0;
                                if (datTempItem.Rows[0]["CompanyID"].ToInt32() == 0)
                                    intComID = 1;
                                else
                                    intComID = datTempItem.Rows[0]["CompanyID"].ToInt32();
                                //DataTable datTempCur = FillCombos(new string[] { "*", "CompanyMaster", "CompanyID=" + intComID });
                                DataTable datTempCur = FillCombos(new string[] { "*", "CompanyMaster", "CompanyID=" + datTempOpStMaster.Rows[0]["CompanyID"].ToInt32() });
                                double dblExRate11 = getExchangeRate(datTempCur.Rows[0]["CurrencyID"].ToInt32(), intComID);
                                strRate = (strRate.ToDouble() * dblExRate11).ToString();
                                strActualRate = (strActualRate.ToDouble() * dblExRate11).ToString();
                                strLandingCost = (strLandingCost.ToDouble() * dblExRate11).ToString();
                            }

                            prmOpeningStock = new ArrayList();
                            prmOpeningStock.Add(new SqlParameter("@Mode", 9));
                            prmOpeningStock.Add(new SqlParameter("@CostingMethodID", datTemp.Rows[0]["CostingMethodID"].ToString().ToInt32()));
                            prmOpeningStock.Add(new SqlParameter("@PricingSchemeID", datTemp.Rows[0]["PricingSchemeID"].ToString().ToInt32()));
                            prmOpeningStock.Add(new SqlParameter("@ItemID", intItemID));
                            prmOpeningStock.Add(new SqlParameter("@CurrencyID", datTempCurrency.Rows[0]["CurrencyID"].ToString().ToInt32()));
                            prmOpeningStock.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
                            prmOpeningStock.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDateTime()));
                            prmOpeningStock.Add(new SqlParameter("@BatchID", intBatchID));
                            prmOpeningStock.Add(new SqlParameter("@PurchaseRate", strRate.ToDouble()));
                            prmOpeningStock.Add(new SqlParameter("@ActualRate", strActualRate.ToDouble()));
                            prmOpeningStock.Add(new SqlParameter("@LandingCost", strLandingCost.ToDouble()));

                            DataTable datTempPricing = FillCombos(new string[] { "*", "InvPricingSchemeReference", "PricingSchemeID=" + datTemp.Rows[0]["PricingSchemeID"].ToString().ToInt32() });
                            //double dblExRate = getExchangeRate(datTempPricing.Rows[0]["CurrencyID"].ToInt32(), datTempOpStMaster.Rows[0]["CompanyID"].ToInt32());
                            double dblExRate = 1;
                            //if (datTempPricing.Rows[0]["CurrencyID"].ToInt32() == datTempPricing.Rows[0]["CurrencyID"].ToInt32())
                            //    dblExRate = getExchangeRate(datTempPricing.Rows[0]["CurrencyID"].ToInt32(), datTempOpStMaster.Rows[0]["CompanyID"].ToInt32());
                            //else
                            //    dblExRate = getExchangeRate(datTempCurrency.Rows[0]["CurrencyID"].ToString().ToInt32(), datTempOpStMaster.Rows[0]["CompanyID"].ToInt32());

                            if (datTempItem.Rows[0]["IsLandingCostEffected"].ToStringCustom() == "")
                                datTempItem.Rows[0]["IsLandingCostEffected"] = "False";

                            if (Convert.ToBoolean(datTempItem.Rows[0]["IsLandingCostEffected"].ToString()) == true)
                            {
                                if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                    strSaleRate = (strLandingCost.ToDouble() +
                                        (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                else
                                    strSaleRate = (strLandingCost.ToDouble() +
                                                (strLandingCost.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                            }
                            else
                            {
                                if (Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()) == true)
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate = (strRate.ToDouble() +
                                            (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                    else
                                        strSaleRate = (strRate.ToDouble() +
                                                    (strRate.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                                }
                                else
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate = (strActualRate.ToDouble() +
                                            (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                    else
                                        strSaleRate = (strActualRate.ToDouble() +
                                                    (strActualRate.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                                }
                            }

                            prmOpeningStock.Add(new SqlParameter("@SaleRate", strSaleRate.ToDouble()));
                            DeleteCostingAndPricingDetails(intItemID, intBatchID);
                            MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", prmOpeningStock);
                        }
                        else
                        {
                            string strRate1 = "0", strActualRate1 = "0", strSaleRate1 = "0", strLandingCost1 = "0";
                            DataTable datTempRate1 = GetSaleRate(datTemp.Rows[0]["CostingMethodID"].ToString().ToInt32(), intItemID,
                                datTempItem.Rows[0]["CalculationBasedOnRate"].ToBoolean());
                            //try
                            //{
                            //    datTempOpSt.DefaultView.RowFilter = "BatchID=" + intBatchID;
                            //    strRate1 = datTempOpSt.DefaultView.ToTable().Rows[0]["Rate"].ToString();
                            //    strActualRate1 = datTempOpSt.DefaultView.ToTable().Rows[0]["Rate"].ToString();
                            //}
                            //catch { }

                            if (datTempRate1.Rows.Count == 0)
                            {
                                strRate1 = datTempOpSt.Rows[0]["Rate"].ToStringCustom();
                                strActualRate1 = datTempOpSt.Rows[0]["Rate"].ToStringCustom();
                                strLandingCost1 = datTempOpSt.Rows[0]["Rate"].ToStringCustom();
                            }
                            else
                            {
                                if (strRate1 == "0")
                                    strRate1 = datTempRate1.Rows[0]["Rate"].ToString();
                                if (strActualRate1 == "0")
                                    strActualRate1 = datTempRate1.Rows[0]["ActualRate"].ToString();
                                if (strLandingCost1 == "0")
                                    strLandingCost1 = datTempRate1.Rows[0]["LandingCost"].ToString();
                            }

                            if (intUomID != datTempItem.Rows[0]["BaseUomID"].ToInt32())
                            {
                                if (datTempUom.Rows[0]["ConversionFactorID"].ToString() == "1")
                                {
                                    strRate1 = (strRate1.ToDouble() * datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strActualRate1 = (strActualRate1.ToDouble() * datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strLandingCost1 = (strLandingCost1.ToDouble() * datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                }
                                else
                                {
                                    strRate1 = (strRate1.ToDouble() / datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strActualRate1 = (strActualRate1.ToDouble() / datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strLandingCost1 = (strLandingCost1.ToDouble() / datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                }
                            }
                            if (datTempOpStMaster.Rows[0]["CompanyID"].ToInt32() != datTempItem.Rows[0]["CompanyID"].ToInt32())
                            {
                                int intComID = 0;
                                if (datTempItem.Rows[0]["CompanyID"].ToInt32() == 0)
                                    intComID = 1;
                                else
                                    intComID = datTempItem.Rows[0]["CompanyID"].ToInt32();
                                //DataTable datTempCur1 = FillCombos(new string[] { "*", "CompanyMaster", "CompanyID=" + intComID });
                                DataTable datTempCur1 = FillCombos(new string[] { "*", "CompanyMaster", "CompanyID=" + datTempOpStMaster.Rows[0]["CompanyID"].ToInt32() });
                                double dblExRate12 = getExchangeRate(datTempCur1.Rows[0]["CurrencyID"].ToInt32(), intComID);
                                strRate1 = (strRate1.ToDouble() * dblExRate12).ToString();
                                strActualRate1 = (strActualRate1.ToDouble() * dblExRate12).ToString();
                                strLandingCost1 = (strLandingCost1.ToDouble() * dblExRate12).ToString();
                            }

                            prmOpeningStock = new ArrayList();
                            prmOpeningStock.Add(new SqlParameter("@Mode", 9));
                            prmOpeningStock.Add(new SqlParameter("@CostingMethodID", datTemp.Rows[0]["CostingMethodID"].ToString().ToInt32()));
                            prmOpeningStock.Add(new SqlParameter("@PricingSchemeID", datTemp.Rows[0]["PricingSchemeID"].ToString().ToInt32()));
                            prmOpeningStock.Add(new SqlParameter("@ItemID", intItemID));
                            prmOpeningStock.Add(new SqlParameter("@CurrencyID", datTempCurrency.Rows[0]["CurrencyID"].ToString().ToInt32()));
                            prmOpeningStock.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
                            prmOpeningStock.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDateTime()));
                            prmOpeningStock.Add(new SqlParameter("@BatchID", intBatchID));
                            prmOpeningStock.Add(new SqlParameter("@PurchaseRate", strRate1.ToDouble()));
                            prmOpeningStock.Add(new SqlParameter("@ActualRate", strActualRate1.ToDouble()));
                            prmOpeningStock.Add(new SqlParameter("@LandingCost", strLandingCost1.ToDouble()));

                            DataTable datTempPricing = FillCombos(new string[] { "*", "InvPricingSchemeReference", "PricingSchemeID=" + datTemp.Rows[0]["PricingSchemeID"].ToString().ToInt32() });
                            //double dblExRate = getExchangeRate(datTempPricing.Rows[0]["CurrencyID"].ToInt32(), datTempOpStMaster.Rows[0]["CompanyID"].ToInt32());
                            double dblExRate = 1;
                            //if (datTempPricing.Rows[0]["CurrencyID"].ToInt32() == datTempCurrency.Rows[0]["CurrencyID"].ToString().ToInt32())
                            //    dblExRate = getExchangeRate(datTempPricing.Rows[0]["CurrencyID"].ToInt32(), datTempOpStMaster.Rows[0]["CompanyID"].ToInt32());
                            //else
                            //    dblExRate = getExchangeRate(datTempCurrency.Rows[0]["CurrencyID"].ToString().ToInt32(), datTempOpStMaster.Rows[0]["CompanyID"].ToInt32());

                            if (datTempItem.Rows[0]["IsLandingCostEffected"].ToStringCustom() == "")
                                datTempItem.Rows[0]["IsLandingCostEffected"] = "False";

                            if (Convert.ToBoolean(datTempItem.Rows[0]["IsLandingCostEffected"].ToString()) == true)
                            {
                                if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                    strSaleRate1 = (strLandingCost1.ToDouble() +
                                        (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                else
                                    strSaleRate1 = (strLandingCost1.ToDouble() +
                                                (strLandingCost1.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                            }
                            else
                            {
                                if (Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()) == true)
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate1 = (strRate1.ToDouble() +
                                            (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                    else
                                        strSaleRate1 = (strRate1.ToDouble() +
                                                    (strRate1.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                                }
                                else
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate1 = (strActualRate1.ToDouble() +
                                            (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                    else
                                        strSaleRate1 = (strActualRate1.ToDouble() +
                                                    (strActualRate1.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                                }
                            }
                            prmOpeningStock.Add(new SqlParameter("@SaleRate", strSaleRate1.ToDouble()));
                            DeleteCostingAndPricingDetails(intItemID, intBatchID);
                            MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", prmOpeningStock);
                        }
                        blnStatus = true;
                    }
                }
                
            }
            catch { }
        }

        public DataTable GetSaleRate(int intMethodID, int intItemId, bool blnRate)
        {
            ArrayList parameters = new ArrayList();
            if (intMethodID == (int)CostingMethodReference.LIFO)
                parameters.Add(new SqlParameter("@Mode", 15));
            else if (intMethodID == (int)CostingMethodReference.FIFO)
                parameters.Add(new SqlParameter("@Mode", 4));
            else if (intMethodID == (int)CostingMethodReference.AVG)
                parameters.Add(new SqlParameter("@Mode", 5));
            else if (intMethodID == (int)CostingMethodReference.MIN)
                parameters.Add(new SqlParameter("@Mode", 6));
            else if (intMethodID == (int)CostingMethodReference.MAX)
                parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@ItemID", intItemId));
            parameters.Add(new SqlParameter("@CalculationBasedOnRate", blnRate));
            DataTable datPricingScheme = MobjDataLayer.ExecuteDataTable("spInvPricingScheme", parameters);
            return datPricingScheme;
        }

        public DataTable GetSaleRateForBatch(int intItemId, int intPricingSchemeID, bool blnRate)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@ItemID", intItemId));
            parameters.Add(new SqlParameter("@PricingSchemeID", intPricingSchemeID));
            return MobjDataLayer.ExecuteDataTable("spInvPricingScheme", parameters);
        }

        public decimal GetUsedQuantity(int intItemID, long lngBatchID)
        {
            ArrayList alParamters = new ArrayList();
            alParamters.Add(new SqlParameter("@Mode", 28));
            alParamters.Add(new SqlParameter("@ItemID", intItemID));
            alParamters.Add(new SqlParameter("@BatchID", lngBatchID));
            return  MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", alParamters).Rows[0]["UsedQty"].ToDecimal();
        }

        public bool DeleteUnwantedBatchDetails()
        {
            DataTable datDeletedBatches = new DataTable();

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 10));
            alParameters.Add(new SqlParameter("@WarehouseID", objDTOOpeningStock.intWarehouseID));
            datDeletedBatches = MobjDataLayer.ExecuteDataTable("spInvStockMaster", alParameters);

            foreach (DataRow dr in datDeletedBatches.Rows)
            {
                DeleteCostingAndPricingDetailsFromOpeningStock(dr["ItemID"].ToInt32(), dr["BatchID"].ToInt32());
            }

            return true;
        }

        private double getExchangeRate(int intCurrencyID, int intCompanyID)
        {
            double dblExRate = 0;
            DataTable datTempExRate = FillCombos(new string[] { "*", "CurrencyDetails", "CompanyID=" + intCompanyID + " AND CurrencyID=" + intCurrencyID });
            if (datTempExRate != null)
            {
                if (datTempExRate.Rows.Count > 0)
                    dblExRate = Convert.ToDouble(datTempExRate.Rows[0]["ExchangeRate"].ToString());
            }
            return dblExRate;
        }

        public bool UpdateOpStockAccountDetails(Int64 ReferenceID)
        {
            DataSet dtsTemp1 = OpeningStockDetailForStockAccount(ReferenceID);
            if (dtsTemp1.Tables[0].Rows.Count > 0)
            {
                int AccountId = Convert.ToInt32(dtsTemp1.Tables[1].Rows[0]["AccountId"].ToString());
                int CompanyID = 0;
                DateTime InvoiceDate = DateTime.Now;
                double Amount = 0;

                for (int i = 0; i < dtsTemp1.Tables[0].Rows.Count; i++)
                {
                    CompanyID = Convert.ToInt32(dtsTemp1.Tables[0].Rows[i]["CompanyID"].ToString());
                    InvoiceDate = Convert.ToDateTime(dtsTemp1.Tables[0].Rows[i]["OpeningDate"].ToString());
                    Amount += Convert.ToDouble(dtsTemp1.Tables[0].Rows[i]["Amount"].ToString());
                }
                if (blnAddStatus == true && blnUpdateStatus == false && blnDeleteStatus == false)
                {
                    UpdateOpeningStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
                }
                else if (blnAddStatus == false && blnUpdateStatus == true && blnDeleteStatus == false)
                {
                    UpdateOpeningStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
                }
                else if (blnAddStatus == false && blnUpdateStatus == false && blnDeleteStatus == true)
                {
                    string strAmt = "-" + Amount.ToString();
                    Amount = Convert.ToDouble(strAmt);
                    UpdateOpeningStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
                }
            }
            blnAddStatus = true;
            blnUpdateStatus = false;
            blnDeleteStatus = false;
            return true;
        }

        public void updateOpStockAccountDetailsEdit(Int64 ReferenceID)
        {
            DataSet dtsTemp11 = OpeningStockDetailForStockAccount(ReferenceID);
            if (dtsTemp11.Tables[0].Rows.Count > 0)
            {
                int AccountId = Convert.ToInt32(dtsTemp11.Tables[1].Rows[0]["AccountId"].ToString());
                int CompanyID = 0;
                DateTime InvoiceDate = DateTime.Now;
                double Amount = 0;

                for (int i = 0; i < dtsTemp11.Tables[0].Rows.Count; i++)
                {
                    CompanyID = Convert.ToInt32(dtsTemp11.Tables[0].Rows[i]["CompanyID"].ToString());
                    InvoiceDate = Convert.ToDateTime(dtsTemp11.Tables[0].Rows[i]["OpeningDate"].ToString());
                    Amount += Convert.ToDouble(dtsTemp11.Tables[0].Rows[i]["Amount"].ToString());
                }
                string strAmt = "-" + Amount.ToString();
                Amount = Convert.ToDouble(strAmt);
                UpdateOpeningStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
            }
        }

        public DataSet OpeningStockDetailForStockAccount(Int64 Rownum)// Getting Invoice detail
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 10));
            prmCommon.Add(new SqlParameter("@OpeningStockID", Rownum));
            return MobjDataLayer.ExecuteDataSet("spInvOpeningStock", prmCommon);
        }

        public bool UpdateOpeningStockAccount(int AccountId, int CompanyId, DateTime CurDate, double Amount)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 11));
            prmCommon.Add(new SqlParameter("@AccountId", AccountId));
            prmCommon.Add(new SqlParameter("@CompanyId", CompanyId));
            prmCommon.Add(new SqlParameter("@OpeningDate", CurDate));
            prmCommon.Add(new SqlParameter("@Quantity", Amount));
            MobjDataLayer.ExecuteNonQuery("spInvOpeningStock", prmCommon);
            return true;
        }
    }
}
