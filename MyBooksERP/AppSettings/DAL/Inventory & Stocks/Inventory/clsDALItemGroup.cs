﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


namespace MyBooksERP
{
    public class clsDALItemGroup
    {
        ArrayList prmItemGroup;

        public clsDTOItemGroup objClsDTOItemGroup { get; set; }
        public DataLayer objClsConnection { get; set; }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objClsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool DisplayItemGroup()
        {
            bool blnRead = false;

            prmItemGroup = new ArrayList();

            prmItemGroup.Add(new SqlParameter("@Mode", 1));
            prmItemGroup.Add(new SqlParameter("@RowNumber", objClsDTOItemGroup.intRowNumber));
            prmItemGroup.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));


            using (SqlDataReader sdrItemsGroup = objClsConnection.ExecuteReader("spInvItemGroupMaster", prmItemGroup, 
                CommandBehavior.CloseConnection))
            {
                if (sdrItemsGroup.Read())
                {
                    objClsDTOItemGroup.decActualAmount = sdrItemsGroup["MaterialCost"].ToDecimal();
                    objClsDTOItemGroup.decMachineCost = sdrItemsGroup["MachineCost"].ToDecimal();
                    objClsDTOItemGroup.decLabourCost = sdrItemsGroup["LabourCost"].ToDecimal();
                    objClsDTOItemGroup.decSaleAmount = Convert.ToDecimal(sdrItemsGroup["SaleAmount"]);
                    objClsDTOItemGroup.lngItemGroupID =sdrItemsGroup["ItemGroupID"].ToInt64();
                    objClsDTOItemGroup.intItemGroupStatusID = sdrItemsGroup["StatusID"].ToInt32();
                    objClsDTOItemGroup.strDescription = Convert.ToString(sdrItemsGroup["Remarks"]);
                    objClsDTOItemGroup.intCategoryID = sdrItemsGroup["CategoryID"].ToInt32();
                    objClsDTOItemGroup.intSubCategoryID = sdrItemsGroup["SubCategoryID"].ToInt32();
                    objClsDTOItemGroup.intBaseUomID = sdrItemsGroup["BaseUomID"].ToInt32();
                    objClsDTOItemGroup.intCostingMethodID = sdrItemsGroup["CostingMethodID"].ToInt32();
                    objClsDTOItemGroup.intPricingSchemeID = sdrItemsGroup["PricingSchemeID"].ToInt32();
                    objClsDTOItemGroup.strShortName = Convert.ToString(sdrItemsGroup["ItemGroupName"]);
                    objClsDTOItemGroup.strItemGroupCode = Convert.ToString(sdrItemsGroup["ItemGroupCode"]);
                    objClsDTOItemGroup.strBatch = Convert.ToString(sdrItemsGroup["Batch"]);
                    if(sdrItemsGroup["BatchDate"]!=null)
                        objClsDTOItemGroup.strBatchDate = Convert.ToString(sdrItemsGroup["BatchDate"]);
                    blnRead = true;
                }
                sdrItemsGroup.Close();

                GetItemGroupDetails();
            }
            return blnRead;
        }

        public void GetItemGroupDetails()
        {
            prmItemGroup = new ArrayList();

            prmItemGroup.Add(new SqlParameter("@Mode", 1));
            prmItemGroup.Add(new SqlParameter("@ItemGroupID", objClsDTOItemGroup.lngItemGroupID));

            objClsDTOItemGroup.objclsDTOItemGroupDetails = new List<clsDTOItemGroupDetails>();

            using (SqlDataReader sdrItemsGroup = objClsConnection.ExecuteReader("spInvItemGroupDetails", prmItemGroup,
                CommandBehavior.CloseConnection))
            {
                while (sdrItemsGroup.Read())
                {
                    clsDTOItemGroupDetails objItemGroupDetails = new clsDTOItemGroupDetails();

                    objItemGroupDetails.decItemTotal = sdrItemsGroup["ItemTotal"].ToDecimal();
                    objItemGroupDetails.lngItemID = sdrItemsGroup["ItemID"].ToInt64();
                    objItemGroupDetails.intSerialNo = sdrItemsGroup["SerialNo"].ToInt32();
                    objItemGroupDetails.intUOMID = sdrItemsGroup["UOMID"].ToInt32();
                    objItemGroupDetails.decQuantity = sdrItemsGroup["Quantity"].ToDecimal();
                    objItemGroupDetails.strItemCode = Convert.ToString(sdrItemsGroup["ItemCode"]);
                    objItemGroupDetails.strItemName = Convert.ToString(sdrItemsGroup["ItemName"]);
                    objItemGroupDetails.decRate = sdrItemsGroup["Rate"].ToDecimal();

                    objClsDTOItemGroup.objclsDTOItemGroupDetails.Add(objItemGroupDetails);
                }
                sdrItemsGroup.Close();
            }
        }

        public bool SaveItemGroup()
        {
            prmItemGroup = new ArrayList();

            prmItemGroup.Add(new SqlParameter("@Mode", objClsDTOItemGroup.lngItemGroupID == 0 ? 2 : 3));
            prmItemGroup.Add(new SqlParameter("@ItemGroupID", objClsDTOItemGroup.lngItemGroupID));
            prmItemGroup.Add(new SqlParameter("@ItemGroupCode", objClsDTOItemGroup.strItemGroupCode));
            prmItemGroup.Add(new SqlParameter("@ShortName", objClsDTOItemGroup.strShortName));
            prmItemGroup.Add(new SqlParameter("@Description", objClsDTOItemGroup.strDescription));
            prmItemGroup.Add(new SqlParameter("@MaterialCost", objClsDTOItemGroup.decActualAmount));
            prmItemGroup.Add(new SqlParameter("@MachineCost", objClsDTOItemGroup.decMachineCost));
            prmItemGroup.Add(new SqlParameter("@LabourCost", objClsDTOItemGroup.decLabourCost));
            prmItemGroup.Add(new SqlParameter("@SaleAmount", objClsDTOItemGroup.decSaleAmount));
            prmItemGroup.Add(new SqlParameter("@CreatedBy", objClsDTOItemGroup.intCreatedBy));
            prmItemGroup.Add(new SqlParameter("@ItemGroupStatusID", objClsDTOItemGroup.intItemGroupStatusID));
            prmItemGroup.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
            prmItemGroup.Add(new SqlParameter("@CurrencyID", ClsCommonSettings.CurrencyID));
            if(objClsDTOItemGroup.intCategoryID>0)
                prmItemGroup.Add(new SqlParameter("@CategoryID", objClsDTOItemGroup.intCategoryID));
            if (objClsDTOItemGroup.intSubCategoryID > 0)
                prmItemGroup.Add(new SqlParameter("@SubCategoryID", objClsDTOItemGroup.intSubCategoryID));
            if (objClsDTOItemGroup.intBaseUomID > 0)
            prmItemGroup.Add(new SqlParameter("@BaseUomID", objClsDTOItemGroup.intBaseUomID));
            if (objClsDTOItemGroup.intCostingMethodID > 0)
            prmItemGroup.Add(new SqlParameter("@CostingMethodID", objClsDTOItemGroup.intCostingMethodID));
            if (objClsDTOItemGroup.intPricingSchemeID > 0)
            prmItemGroup.Add(new SqlParameter("@PricingSchemeID", objClsDTOItemGroup.intPricingSchemeID));
            prmItemGroup.Add(new SqlParameter("@Batch", objClsDTOItemGroup.strBatch));
            prmItemGroup.Add(new SqlParameter("@BatchDate", objClsDTOItemGroup.strBatchDate)); 	
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmItemGroup.Add(objParam);

            object intItemGroupID = 0;

            objClsConnection.ExecuteNonQueryWithTran("spInvItemGroupMaster", prmItemGroup, out intItemGroupID);
            objClsDTOItemGroup.lngItemGroupID = Convert.ToInt64(intItemGroupID);

            if (objClsDTOItemGroup.lngItemGroupID > 0)
            {
                SaveItemGroupDetails();
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SaveItemGroupDetails()
        {
            prmItemGroup = new ArrayList();

            prmItemGroup.Add(new SqlParameter("@Mode", 3));
            prmItemGroup.Add(new SqlParameter("@ItemGroupID", objClsDTOItemGroup.lngItemGroupID));

            objClsConnection.ExecuteNonQueryWithTran("spInvItemGroupDetails", prmItemGroup);

            foreach (clsDTOItemGroupDetails objItemGroupDetails in objClsDTOItemGroup.objclsDTOItemGroupDetails)
            {
                prmItemGroup = new ArrayList();

                prmItemGroup.Add(new SqlParameter("@Mode", 2));
                prmItemGroup.Add(new SqlParameter("@ItemGroupID", objClsDTOItemGroup.lngItemGroupID));
                prmItemGroup.Add(new SqlParameter("@SerialNo", objItemGroupDetails.intSerialNo));
                prmItemGroup.Add(new SqlParameter("@ItemID", objItemGroupDetails.lngItemID));
                prmItemGroup.Add(new SqlParameter("@UOMID", objItemGroupDetails.intUOMID));
                prmItemGroup.Add(new SqlParameter("@Quantity", objItemGroupDetails.decQuantity));
                prmItemGroup.Add(new SqlParameter("@ItemTotal", objItemGroupDetails.decItemTotal));
                prmItemGroup.Add(new SqlParameter("@Rate", objItemGroupDetails.decRate));

                objClsConnection.ExecuteNonQueryWithTran("spInvItemGroupDetails", prmItemGroup);
            }
        }

        public bool DeleteItemGroup()
        {
            prmItemGroup = new ArrayList();

            prmItemGroup.Add(new SqlParameter("@Mode", 4));
            prmItemGroup.Add(new SqlParameter("@ItemGroupID", objClsDTOItemGroup.lngItemGroupID));
            prmItemGroup.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));


            if (objClsConnection.ExecuteNonQuery("spInvItemGroupMaster", prmItemGroup) != 0)
                return true;
            else
                return false;
        }

        public int GetItemGroupCount()
        {
            prmItemGroup = new ArrayList();

            prmItemGroup.Add(new SqlParameter("@Mode", 5));
            prmItemGroup.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));


            return Convert.ToInt32(objClsConnection.ExecuteScalar("spInvItemGroupMaster", prmItemGroup));
        }

        public DataSet GetItemGroupingReport()
        {
            prmItemGroup = new ArrayList();
            prmItemGroup.Add(new SqlParameter("@Mode", 6));
            prmItemGroup.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
            prmItemGroup.Add(new SqlParameter("@ItemGroupID", objClsDTOItemGroup.lngItemGroupID));

            return objClsConnection.ExecuteDataSet("spInvItemGroupMaster", prmItemGroup);
        }

        public int GenerateItemGroupCode()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
            return objClsConnection.ExecuteScalar("spInvItemGroupMaster", parameters).ToInt32();
          
        }
        public int CheckDupliateGroupCodeName(string strGroupCode,Int64 intItemGroupId)
        {
            int intRetValue = 0;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@ItemGroupCode", strGroupCode));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
            if (intItemGroupId>0)
            {
                parameters.Add(new SqlParameter("@ItemGroupID", intItemGroupId));
            }

            return objClsConnection.ExecuteScalar("spInvItemGroupMaster", parameters).ToInt32();

        }
        public DataTable GetItemGroupRowNumber(Int64 intItemGroupID)
        {
            prmItemGroup = new ArrayList();
            prmItemGroup.Add(new SqlParameter("@Mode", 9));
            prmItemGroup.Add(new SqlParameter("@ItemGroupID", intItemGroupID));
            prmItemGroup.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
            return objClsConnection.ExecuteDataTable("spInvItemGroupMaster", prmItemGroup);
        }
    }
}
