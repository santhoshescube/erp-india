﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;



/******************************************************************
 * Created By       : Ratheesh
 * Creation Date    : 18 June 2011
 * Description      : Handles stock adjustment database interaction
 * ****************************************************************/

namespace MyBooksERP
{
    public class clsDALStockAdjustments
    {
        public clsDTOStockAdjustment StockAdjustment { get; set; }
        public clsPager Pager { get; set; }
        public DataLayer db;
        private ArrayList alSqlParams;
        private const string GLOBAL_PROCEDURE_NAME = "spInvStockAdjustment";
        bool blnAddStatus = false, blnUpdateStatus = false, blnDeleteStatus = false;

        public clsDALStockAdjustments()
        {
            this.StockAdjustment = new clsDTOStockAdjustment();
            this.Pager = new clsPager();
            this.db = new DataLayer();
        }

        public DataSet GetStockAdjustments()
        {
            alSqlParams = new ArrayList() {
                new SqlParameter("@Mode", "SEL"),
                new SqlParameter("@StockAdjustmentID", this.StockAdjustment.StockAdjustmentID)
            };
            return this.db.ExecuteDataSet("");
        }

        public bool GetStockAdjustmentDetails(long lngStockAdjustmentID)
        {
            bool success = false;

            alSqlParams = new ArrayList() {
                new SqlParameter("@Mode", "SEL_STA"),
                new SqlParameter("@StockAdjustmentID", lngStockAdjustmentID)
            };

            if (ClsCommonSettings.RoleID > 2)
            {
                alSqlParams.Add(new SqlParameter("@RoleID", ClsCommonSettings.RoleID));
                alSqlParams.Add(new SqlParameter("@MenuID", (int)eMenuID.StockAdjustment));
                alSqlParams.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
            }

            using (DataSet ds = this.db.ExecuteDataSet(GLOBAL_PROCEDURE_NAME, alSqlParams))
            {
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        this.StockAdjustment.StockAdjustmentID = ds.Tables[0].Rows[0]["StockAdjustmentID"].ToInt64();
                        intreturn(this.StockAdjustment.StockAdjustmentID);
                        this.StockAdjustment.AdjustmentDate = ds.Tables[0].Rows[0]["AdjustmentDate"].ToDateTime();
                        this.StockAdjustment.AdjustmentNo = ds.Tables[0].Rows[0]["AdjustmentNo"].ToString();
                        this.StockAdjustment.CompanyID = ds.Tables[0].Rows[0]["CompanyID"].ToInt32();
                        this.StockAdjustment.ReasonID = ds.Tables[0].Rows[0]["ReasonID"].ToInt32();
                        this.StockAdjustment.Remarks = ds.Tables[0].Rows[0]["Remarks"].ToString();
                        this.StockAdjustment.StatusID = ds.Tables[0].Rows[0]["StatusID"].ToInt32();
                        this.StockAdjustment.WarehouseID = ds.Tables[0].Rows[0]["WarehouseID"].ToInt32();
                        this.StockAdjustment.CreatedDate = ds.Tables[0].Rows[0]["CreatedDate"].ToDateTime();
                        this.StockAdjustment.CreatedByName = ds.Tables[0].Rows[0]["CreatedByName"].ToString();

                        success = true;
                    }

                    if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                    {
                        // clear items (if any)
                        this.StockAdjustment.AdjustedItems.Clear();

                        foreach (DataRow row in ds.Tables[1].Rows)
                        {
                            this.StockAdjustment.AdjustedItems.Add( new clsDTOAdjustedItem() {
                                BatchID = row["BatchID"].ToInt64(),
                                CurrentQuantity = row["CurrentQuantity"].ToDouble(),
                                ItemID = row["ItemID"].ToInt32(),
                                NewQuantity = row["NewQuantity"].ToDouble(),
                                Rate = row["Rate"].ToDouble(),
                                SerialNo = row["SerialNo"].ToInt32(),
                                ItemName = row["ItemName"].ToString(),
                                ItemCode = row["ItemCode"].ToString(),
                                ActualQty = row["ActualQty"].ToDouble()
                            });
                        }

                        success = true;
                    }

                    //if (ds.Tables.Count > 2 && ds.Tables[2].Rows.Count > 0)
                    //{
                    //    // clear items (if any)
                    //    this.StockAdjustment.AdjustedLocationDetails.Clear();

                    //    foreach (DataRow row in ds.Tables[2].Rows)
                    //    {
                    //        this.StockAdjustment.AdjustedLocationDetails.Add(new clsDTOAdjustedLocationDetails()
                    //        {
                    //            BatchID = row["BatchID"].ToInt64(),
                    //            CurrentQuantity = row["CurrentQuantity"].ToDouble(),
                    //            ItemID = row["ItemID"].ToInt32(),
                    //            NewQuantity = row["NewQuantity"].ToDouble(),
                    //            ItemName = row["ItemName"].ToString(),
                    //            ItemCode = row["ItemCode"].ToString(),
                    //            BatchNo = row["BatchNo"].ToString(),
                    //            LocationID = row["LocationID"].ToInt32(),
                    //            RowID = row["RowID"].ToInt32(),
                    //            BlockID = row["BlockID"].ToInt32(),
                    //            LotID = row["LotID"].ToInt32()
                    //        });
                    //    }

                    //    success = true;
                    //}
            }

            return success;
        }

        public void GetRecordCount()
        {
            alSqlParams = new ArrayList() {
                new SqlParameter("@Mode", "ROW_COUNT")
            };

            if (ClsCommonSettings.RoleID > 2)
            {
                alSqlParams.Add(new SqlParameter("@RoleID", ClsCommonSettings.RoleID));
                alSqlParams.Add(new SqlParameter("@ControlID", (int)ControlOperationType.StAdjCompany));
                alSqlParams.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
            }

            object objRowCount = this.db.ExecuteScalar(GLOBAL_PROCEDURE_NAME, alSqlParams);
            this.Pager.TotalRecords = objRowCount.ToInt64();
        }

        public bool InsertStockAdjustment()
        {
            blnAddStatus = true;
            blnUpdateStatus = false;
            blnDeleteStatus = false;

            alSqlParams = new ArrayList() {
                new SqlParameter("@Mode", "INS_MASTER"),
                new SqlParameter("@AdjustmentDate", this.StockAdjustment.AdjustmentDate),
                new SqlParameter("@AdjustmentNo", this.StockAdjustment.AdjustmentNo),
                new SqlParameter("@CompanyID", this.StockAdjustment.CompanyID),
                new SqlParameter("@ReasonID", this.StockAdjustment.ReasonID),
                new SqlParameter("@Remarks", this.StockAdjustment.Remarks),
                new SqlParameter("@WarehouseID", this.StockAdjustment.WarehouseID),
                new SqlParameter("@CreatedBy", this.StockAdjustment.CreatedBy),
                new SqlParameter("@CreatedDate",ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy"))
            };

            // Get newly generated Identity
            object objIdentity = this.db.ExecuteScalar(GLOBAL_PROCEDURE_NAME, alSqlParams);

            this.StockAdjustment.StockAdjustmentID = Convert.ToInt64(objIdentity == DBNull.Value ? -1 : objIdentity);
            
            // returns true if identity value is greater than 1 otherwise false
            return this.StockAdjustment.StockAdjustmentID > 0;            
        }

        public bool UpdateStockAdjustment()
        {
            blnAddStatus = false;
            blnUpdateStatus = true;
            blnDeleteStatus = false;
            //updateSaleOpStockAccountDetailsEdit(this.StockAdjustment.StockAdjustmentID);
            alSqlParams = new ArrayList() {
                new SqlParameter("@Mode", "UPD_MASTER"),
                new SqlParameter("@AdjustmentDate", this.StockAdjustment.AdjustmentDate),
                new SqlParameter("@AdjustmentNo", this.StockAdjustment.AdjustmentNo),
                new SqlParameter("@CompanyID", this.StockAdjustment.CompanyID),
                new SqlParameter("@ReasonID", this.StockAdjustment.ReasonID),
                new SqlParameter("@Remarks", this.StockAdjustment.Remarks),
                new SqlParameter("@WarehouseID", this.StockAdjustment.WarehouseID),
                new SqlParameter("@CreatedBy", this.StockAdjustment.CreatedBy),
                new SqlParameter("@StockAdjustmentID", this.StockAdjustment.StockAdjustmentID)
            };
           
            // Gets number of rows affected by the command
            object objRowsAffected = this.db.ExecuteScalar(GLOBAL_PROCEDURE_NAME, alSqlParams);

            // returns true if objRowsAffected is greater than 1 otherwise false
            return Convert.ToInt64(objRowsAffected == DBNull.Value ? -1 : objRowsAffected) > 0;

            //UpdateSaleOpStockAccountDetails(this.StockAdjustment.StockAdjustmentID);
        }

        public bool InsertAjustedItems()
        {
            int InsertedCount = 0;

                 alSqlParams = new ArrayList() {
                    new SqlParameter("@Mode", "INS_DET"),
                    new SqlParameter("@StockAdjustmentID", this.StockAdjustment.StockAdjustmentID),
                    new SqlParameter("@CompanyID", this.StockAdjustment.CompanyID),
                    new SqlParameter("@WarehouseID", this.StockAdjustment.WarehouseID),
                    new SqlParameter("@CreatedBy", this.StockAdjustment.CreatedBy),
                    new SqlParameter("@OperationTypeID", (int)OperationType.StockAdjustment),
                    new SqlParameter("@XmlStockAdjustment", this.StockAdjustment.AdjustedItems.ToXml())
                };

                // Get number of rows affected by the command
                object objRowsAffected = this.db.ExecuteScalar(GLOBAL_PROCEDURE_NAME, alSqlParams);

                // increments by 1 (if number of rows affected is greater than 1) otherwise 0
                InsertedCount += Convert.ToInt64(objRowsAffected == DBNull.Value ? -1 : objRowsAffected) > 0 ? 1 : 0;
            //}

            return InsertedCount > 0;
        }

        public bool InsertAdjustedLocationDetails()
        {
            int InsertedCount = 0;

            alSqlParams = new ArrayList() {
                new SqlParameter("@Mode","INS_LOCDET"),
                new SqlParameter("@StockAdjustmentID",this.StockAdjustment.StockAdjustmentID),
                new SqlParameter("@CompanyID",this.StockAdjustment.CompanyID),
                new SqlParameter("@WarehouseID",this.StockAdjustment.WarehouseID),
                new SqlParameter("@CreatedBy",this.StockAdjustment.CreatedBy),
                new SqlParameter("@XmlStockAdjustmentLocation",this.StockAdjustment.AdjustedLocationDetails.ToXml())
            };

            // Get number of rows affected by the command
            object objRowsAffected = this.db.ExecuteScalar(GLOBAL_PROCEDURE_NAME, alSqlParams);

            // increments by 1 (if number of rows affected is greater than 1) otherwise 0
            InsertedCount += Convert.ToInt64(objRowsAffected == DBNull.Value ? -1 : objRowsAffected) > 0 ? 1 : 0;
            //}

            return InsertedCount > 0;
        }

        public bool DeleteStockAdjustment()
        {
            blnAddStatus = false;
            blnUpdateStatus = false;
            blnDeleteStatus = true;
            try
            {
                this.db.BeginTransaction();
                //UpdateSaleOpStockAccountDetails(this.StockAdjustment.StockAdjustmentID); // For Updating Stock Adjustment Account
                bool success = this.DeleteStockAdjustmentDetails();

                if (success)
                {
                    success = false;

                    alSqlParams = new ArrayList() {
                        new SqlParameter("@Mode", "DEL"),
                        new SqlParameter("@StockAdjustmentID", this.StockAdjustment.StockAdjustmentID)
                    };

                    // Gets number of rows affected by the command
                    object objRowsAffected = this.db.ExecuteScalar(GLOBAL_PROCEDURE_NAME, alSqlParams);

                    // returns true if objRowsAffected is greater than 1 otherwise false
                    success = Convert.ToInt64(objRowsAffected == DBNull.Value ? -1 : objRowsAffected) > 0;

                }
                if (!success)
                    this.db.RollbackTransaction();
                else
                    this.db.CommitTransaction();

                return success;
            }
            catch(SqlException)
            {
                this.db.RollbackTransaction();
                throw;
            }
        }

        public bool DeleteStockAdjustmentDetails()
        {
            alSqlParams = new ArrayList() {
                new SqlParameter("@Mode", "DEL_DET"),
                new SqlParameter("@StockAdjustmentID", this.StockAdjustment.StockAdjustmentID),
                new SqlParameter("@ModifiedBy", this.StockAdjustment.ModifiedBY)
            };

            // Get number of rows affected by the command
            object objRowsAffected = this.db.ExecuteScalar(GLOBAL_PROCEDURE_NAME, alSqlParams);

            // returns true if objRowsAffected is greater than 1 otherwise false
            return Convert.ToInt64(objRowsAffected == DBNull.Value ? -1 : objRowsAffected) > 0;
        }

        public DataTable GetStockDetailsByWareHouse()
        {
            this.alSqlParams = new ArrayList() {
                new SqlParameter("@Mode", ""),
                new SqlParameter("@WarehouseID", this.StockAdjustment.WarehouseID)
            };

            return this.db.ExecuteDataTable("", this.alSqlParams);
        }

        public DataTable GetRateAndCurrentStock(long BatchID, int ItemID, int WarehouseID)
        {
            this.alSqlParams = new ArrayList() {
                new SqlParameter("@Mode", "GET_STK_AND_RATE"),
                new SqlParameter("@BatchID", BatchID),
                new SqlParameter("@ItemID", ItemID),
                new SqlParameter("@WarehouseID", WarehouseID)
            };

            return this.db.ExecuteDataTable(GLOBAL_PROCEDURE_NAME, this.alSqlParams);
        }

        public bool IsAdjustmentNumberExists()
        {
            this.alSqlParams = new ArrayList() {
                new SqlParameter("@Mode", "ANO_EXIST"),
                new SqlParameter("@AdjustmentNo", this.StockAdjustment.AdjustmentNo)
            };

            if (this.StockAdjustment.StockAdjustmentID > 0)
                this.alSqlParams.Add(new SqlParameter("@StockAdjustmentID", this.StockAdjustment.StockAdjustmentID));

            object objExistingRecordCount = this.db.ExecuteScalar(GLOBAL_PROCEDURE_NAME, alSqlParams);

            return objExistingRecordCount.ToInt32() > 0;
        }

        public string GenerateAdjustmentNo(int intCompanyID)
        {
            this.alSqlParams = new ArrayList() {
                new SqlParameter("@Mode", "GEN_SADNO"),
                new SqlParameter("@CompanyID",intCompanyID)
            };
            object objNewNumber = this.db.ExecuteScalar(GLOBAL_PROCEDURE_NAME, alSqlParams);

            return string.Format("{0}{1}", ClsCommonSettings.strStockAdjustmentNumberPrefix, objNewNumber.ToInt64());
        }

        public DataSet DispalyStockAdjustmentReport(long intStockAdjustmentID)
        {

            alSqlParams = new ArrayList();
            {
                new SqlParameter("@Mode", "10");
            }
            //   parameters.Add(new SqlParameter("@StockAdjustmentID", intStockAdjustmentID));
            alSqlParams.Add(new SqlParameter("@StockAdjustmentID",StockAdjustment.StockAdjustmentID));

            //  return objclsconnection.ExecuteDataTable("STStockAdjustmentTransaction", parameters);

            return db.ExecuteDataSet("spInvStockAdjustment",alSqlParams);
          

        }
        public void intreturn(long intret)
        {
            StockAdjustment.tempStockAdjustmentID = intret;
        }
        public DataSet DisplayStockAdjustmentEmail(long intStockAdjustmentID)  //Stock Adjustment Email
        {
            alSqlParams = new ArrayList();

            {
                alSqlParams.Add(new SqlParameter("@Mode", "10"));
            }
            alSqlParams.Add(new SqlParameter("@StockAdjustmentID", StockAdjustment.StockAdjustmentID));
            return db.ExecuteDataSet("spInvStockAdjustment", alSqlParams);

        }

        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 14));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@LocationWise", ClsCommonSettings.PblnIsLocationWise));
            return db.ExecuteDataTable("spInvItemLocationTransfer", alParameters).Rows[0];
        }

        public decimal GetItemLocationQuantity(clsDTOAdjustedLocationDetails objLocationDetails,int intWarehouseID)
        {
            ArrayList alParameters = new ArrayList();
            decimal decQuantity = 0;
            alParameters.Add(new SqlParameter("@Mode", 13));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.ItemID));
            alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.BatchID));
            alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.LocationID));
            alParameters.Add(new SqlParameter("@RowID", objLocationDetails.RowID));
            alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.BlockID));
            alParameters.Add(new SqlParameter("@LotID", objLocationDetails.LotID));
            DataTable  datLocationdetails = db.ExecuteDataTable("spInvItemLocationTransfer", alParameters);

            if (datLocationdetails.Rows.Count > 0)
                decQuantity = datLocationdetails.Rows[0]["Quantity"].ToDecimal();
            return decQuantity;
        }

        public DataTable GetAllAdjustments()
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GETALL_Adjustments"));
            return db.ExecuteDataTable(GLOBAL_PROCEDURE_NAME, alParameters);
        }

        public bool UpdateSaleOpStockAccountDetails(Int64 ReferenceID)
        {
            DataSet dtsTemp1 = StockAdjDetailForStockAccount(ReferenceID);
            if (dtsTemp1.Tables[0].Rows.Count > 0)
            {
                int AccountId = Convert.ToInt32(dtsTemp1.Tables[1].Rows[0]["AccountId"].ToString());
                int CompanyID = 0;
                DateTime InvoiceDate = DateTime.Now;
                double Amount = 0;

                for (int i = 0; i < dtsTemp1.Tables[0].Rows.Count; i++)
                {
                    CompanyID = Convert.ToInt32(dtsTemp1.Tables[0].Rows[i]["CompanyID"].ToString());
                    InvoiceDate = Convert.ToDateTime(dtsTemp1.Tables[0].Rows[i]["AdjustmentDate"].ToString());
                    Amount += Convert.ToDouble(dtsTemp1.Tables[0].Rows[i]["Amount"].ToString());
                }
                if (blnAddStatus == true && blnUpdateStatus == false && blnDeleteStatus == false)
                {
                    UpdateStockAdjAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
                }
                else if (blnAddStatus == false && blnUpdateStatus == true && blnDeleteStatus == false)
                {
                    UpdateStockAdjAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
                }
                else if (blnAddStatus == false && blnUpdateStatus == false && blnDeleteStatus == true)
                {
                    string strAmt = "-" + Amount.ToString();
                    Amount = Convert.ToDouble(strAmt);
                    UpdateStockAdjAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
                }
            }
            blnAddStatus = true;
            blnUpdateStatus = false;
            blnDeleteStatus = false;
            return true;
        }

        public void updateSaleOpStockAccountDetailsEdit(Int64 ReferenceID)
        {
            DataSet dtsTemp11 = StockAdjDetailForStockAccount(ReferenceID);
            if (dtsTemp11.Tables[0].Rows.Count > 0)
            {
                int AccountId = Convert.ToInt32(dtsTemp11.Tables[1].Rows[0]["AccountId"].ToString());
                int CompanyID = 0;
                DateTime InvoiceDate = DateTime.Now;
                double Amount = 0;

                for (int i = 0; i < dtsTemp11.Tables[0].Rows.Count; i++)
                {
                    CompanyID = Convert.ToInt32(dtsTemp11.Tables[0].Rows[i]["CompanyID"].ToString());
                    InvoiceDate = Convert.ToDateTime(dtsTemp11.Tables[0].Rows[i]["AdjustmentDate"].ToString());
                    Amount += Convert.ToDouble(dtsTemp11.Tables[0].Rows[i]["Amount"].ToString());
                }
                string strAmt = "-" + Amount.ToString();
                Amount = Convert.ToDouble(strAmt);
                UpdateStockAdjAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Amount Updation
            }
        }

        public DataSet StockAdjDetailForStockAccount(Int64 Rownum)// Getting Invoice detail
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", "11"));
            prmCommon.Add(new SqlParameter("@StockAdjustmentID", Rownum));
            return db.ExecuteDataSet("spInvStockAdjustment", prmCommon);
        }

        public bool UpdateStockAdjAccount(int AccountId, int CompanyId, DateTime CurDate, double Amount)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", "12"));
            prmCommon.Add(new SqlParameter("@AccountId", AccountId));
            prmCommon.Add(new SqlParameter("@CompanyId", CompanyId));
            prmCommon.Add(new SqlParameter("@AdjustmentDate", CurDate));
            prmCommon.Add(new SqlParameter("@NewQuantity", Amount));
            db.ExecuteNonQuery("spInvStockAdjustment", prmCommon);
            return true;
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 12));
            prmCommon.Add(new SqlParameter("@RoleID", intRoleID));
            prmCommon.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmCommon.Add(new SqlParameter("@ControlID", intControlID));
            return db.ExecuteDataTable("STPermissionSettings", prmCommon);
        }
    }
}
