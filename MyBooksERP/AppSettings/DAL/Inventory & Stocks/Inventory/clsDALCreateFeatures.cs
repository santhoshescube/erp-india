﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace MyBooksERP
{
    public class clsDALCreateFeatures
    {
        private DataLayer objDataLayer = null;
        public DataLayer DataLayer 
        {
            get
            {
                if (this.objDataLayer == null)
                    this.objDataLayer = new DataLayer();
                
                return this.objDataLayer;
            }
        }
        
        public DataTable GetFeatures()
        {
            return this.DataLayer.ExecuteDataTable("spInvItemMaster", new List<SqlParameter>() { 
                new SqlParameter("@Mode", 10)
            });
        }

        public DataTable GetFeatureValues(int intFeatureID)
        {
            return this.DataLayer.ExecuteDataTable("spInvItemMaster", new List<SqlParameter>() { 
                new SqlParameter("@Mode", 13),
                new SqlParameter("@FeatureID", intFeatureID)
            });
        }

    }
}
