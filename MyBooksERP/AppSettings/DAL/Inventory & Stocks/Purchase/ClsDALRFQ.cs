﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using System.Data;
using System.Data.SqlClient;
/* 
=================================================
   Author:		<Author,,Thasni>
   Create date: <Create Date,,28 Jun 2011>
   Description:	<Description,,RFQ DAL Class>
================================================
*/

namespace MyBooksERP
{
   public class ClsDALRFQ
    {

        public DataLayer MobjDataLayer { get; set; }
        public ClsDTORFQ PobjClsDTORFQ { get; set; }      
        private ArrayList lstSqlParams;
        private clsDTORFQDetails objRFQDetails;

        ClsCommonUtility MobjClsCommonUtility; // object of clsCommonUtility

        string strProcedureName = "STRFQTransaction";

        public ClsDALRFQ(DataLayer objDataLayer)
        {
            MobjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MobjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MobjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }       

        public DataTable GetCompanySettings(int ComID) // for settings according to company
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 6));
            prmCommon.Add(new SqlParameter("@CompanyID", ComID));
            return MobjDataLayer.ExecuteDataSet("STPurchaseModule", prmCommon).Tables[0];

        }

        public long GetNextRFQNo(int intCompanyID)
        {
            lstSqlParams = new ArrayList();
            lstSqlParams.Add(new SqlParameter("@Mode", 17));
            lstSqlParams.Add(new SqlParameter("@CompanyID", intCompanyID));
            return Convert.ToInt64(MobjDataLayer.ExecuteScalar(strProcedureName, lstSqlParams));
        }

        public int GetDefaultUomID(int iItemID)
        {
            object objOutUomID = 0;
            lstSqlParams = new ArrayList();
            lstSqlParams.Add(new SqlParameter("@Mode", 8));
            lstSqlParams.Add(new SqlParameter("@ItemID", iItemID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            lstSqlParams.Add(objParam);
            if (MobjDataLayer.ExecuteNonQueryWithTran("STPurchaseModule", lstSqlParams, out objOutUomID) != 0)
            {
                if ((int)objOutUomID >= 0)
                {
                    return (int)objOutUomID;
                }
            }
            return (int)objOutUomID;

        }
      
        public bool SaveRFQ(bool blnAddStatus)
        {
            // function for saving Purchase Quotation

            int intOldStatusID = 0;
            object objOutRFQID = 0;
            if (blnAddStatus)
            {
                lstSqlParams = new ArrayList();
                lstSqlParams.Add(new SqlParameter("@Mode", 2));
            }
            else
            {
                DataTable datRFQ = FillCombos(new string[] { "StatusID", "STRFQMaster", "RFQID = " + PobjClsDTORFQ.RFQID });
                if (datRFQ.Rows.Count > 0)
                {
                    intOldStatusID = datRFQ.Rows[0]["StatusID"].ToInt32();
                }
                DeleteRFQDetails();
                lstSqlParams = new ArrayList();
                lstSqlParams.Add(new SqlParameter("@Mode", 3));
            }

            lstSqlParams.Add(new SqlParameter("@RFQID", PobjClsDTORFQ.RFQID));
            lstSqlParams.Add(new SqlParameter("@RFQNo", PobjClsDTORFQ.RFQNo));
            lstSqlParams.Add(new SqlParameter("@QuotationDate", PobjClsDTORFQ.QuatationDate));
            lstSqlParams.Add(new SqlParameter("@DueDate", PobjClsDTORFQ.DueDate));          
            lstSqlParams.Add(new SqlParameter("@Remarks", PobjClsDTORFQ.Remarks));     
   
            // prmPurchase.Add(new SqlParameter("@FreightAmount", PobjClsDTOPurchase.decExpenseAmount));

            lstSqlParams.Add(new SqlParameter("@CreatedBy", PobjClsDTORFQ.CreatedBy)); 
            if(PobjClsDTORFQ.ApprovedBy!=0)
                lstSqlParams.Add(new SqlParameter("@ApprovedBy", PobjClsDTORFQ.ApprovedBy));
            if(PobjClsDTORFQ.ApprovedDate!=DateTime.MinValue)
                lstSqlParams.Add(new SqlParameter("@ApprovedDate", PobjClsDTORFQ.ApprovedDate));
            lstSqlParams.Add(new SqlParameter("@VerificationDescription", PobjClsDTORFQ.VerifcationDescription));

            lstSqlParams.Add(new SqlParameter("@CompanyID", PobjClsDTORFQ.CompanyID));
            lstSqlParams.Add(new SqlParameter("@StatusID", PobjClsDTORFQ.StatusID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            lstSqlParams.Add(objParam);

            if (MobjDataLayer.ExecuteNonQuery(strProcedureName, lstSqlParams, out objOutRFQID) != 0)
            {
                if (Convert.ToInt64(objOutRFQID) > 0)
                {
                    PobjClsDTORFQ.RFQID = Convert.ToInt64(objOutRFQID);
                    DeleteCancellationDetails();
                    if (PobjClsDTORFQ.blnCancelled)
                    {
                        SaveCancellationDetails();
                    }
                    if (PobjClsDTORFQ.lstClsDTORFQDetails.Count > 0)
                    {
                        SaveRFQDetails();
                    }
                    if ((PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Rejected && intOldStatusID != PobjClsDTORFQ.StatusID) || (PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Approved && intOldStatusID != PobjClsDTORFQ.StatusID))
                        VerificationHistoryTableUpdations(PobjClsDTORFQ.RFQID, false);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public void SaveCancellationDetails()
        {
            // function for saving cancellation details

            ArrayList prmCancellation = new ArrayList();
            prmCancellation.Add(new SqlParameter("@Mode", 12));
            prmCancellation.Add(new SqlParameter("@CancelledBy", PobjClsDTORFQ.intCancelledBy));
            prmCancellation.Add(new SqlParameter("@CancellationDate", PobjClsDTORFQ.CancellationDate));
            prmCancellation.Add(new SqlParameter("@CancellationDescription", PobjClsDTORFQ.CancellationDescription));
            prmCancellation.Add(new SqlParameter("@CompanyID", PobjClsDTORFQ.CompanyID));

            prmCancellation.Add(new SqlParameter("@RFQID", PobjClsDTORFQ.RFQID));
            MobjDataLayer.ExecuteNonQuery(strProcedureName, prmCancellation);
        }

        private void DeleteCancellationDetails()
        {
            // function for deleting cancellation details
            ArrayList prmCancel = new ArrayList();

            prmCancel.Add(new SqlParameter("@Mode", 13));
            prmCancel.Add(new SqlParameter("@RFQID", PobjClsDTORFQ.RFQID));
            MobjDataLayer.ExecuteNonQuery(strProcedureName, prmCancel);
         
        }

        private DataTable DisplayCancellationDetails()
        {
            //function for displaying cancellation details

            lstSqlParams = new ArrayList();

            lstSqlParams.Add(new SqlParameter("@Mode", 14));
            lstSqlParams.Add(new SqlParameter("@RFQID", PobjClsDTORFQ.RFQID));
            return MobjDataLayer.ExecuteDataTable(strProcedureName, lstSqlParams);
        }

        public bool DeleteRFQ()
        {
            // function for deleting purchase indent

            object objOutRFQID = 0;
            lstSqlParams = new ArrayList();
            lstSqlParams.Add(new SqlParameter("@Mode", 4));
            lstSqlParams.Add(new SqlParameter("@RFQID", PobjClsDTORFQ.RFQID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            lstSqlParams.Add(objParam);
            if (PobjClsDTORFQ.StatusID == (Int16)RFQStatus.Rejected)
                VerificationHistoryTableUpdations(PobjClsDTORFQ.RFQID, true);
            if (MobjDataLayer.ExecuteNonQuery(strProcedureName, lstSqlParams) != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SaveRFQDetails()
        {
            //function for saving RFQ Details
            int count = 0;

            foreach (clsDTORFQDetails objClsDTORFQDetails in PobjClsDTORFQ.lstClsDTORFQDetails)
            {
                lstSqlParams = new ArrayList();

                //if (objClsDTOPurchaseDetails.intSerialNo == 0)
                lstSqlParams.Add(new SqlParameter("@Mode", 8));
                //else
                //  prmPurchaseDetails.Add(new SqlParameter("@Mode", 9));

                lstSqlParams.Add(new SqlParameter("@SerialNo", ++count));
                lstSqlParams.Add(new SqlParameter("@RFQID", PobjClsDTORFQ.RFQID));
                lstSqlParams.Add(new SqlParameter("@ItemID", objClsDTORFQDetails.ItemID));
                lstSqlParams.Add(new SqlParameter("@Quantity", objClsDTORFQDetails.Quantity));
                lstSqlParams.Add(new SqlParameter("@UOMID", objClsDTORFQDetails.UOMID));
                MobjDataLayer.ExecuteNonQuery(strProcedureName, lstSqlParams);
            }
            return true;
        }

        public bool DeleteRFQDetails()
        {
            //function for deleting purchase indent details

            lstSqlParams = new ArrayList();
            lstSqlParams.Add(new SqlParameter("@Mode", 10));
            lstSqlParams.Add(new SqlParameter("@RFQID", PobjClsDTORFQ.RFQID));
            MobjDataLayer.ExecuteNonQueryWithTran(strProcedureName, lstSqlParams);
            return true;
        }

        public DataTable GetRFQNos()// Get RFQ number For searching
        {
            //function for Finding Purchase 
            lstSqlParams = new ArrayList();
            lstSqlParams.Add(new SqlParameter("@Mode", 5));
            return MobjDataLayer.ExecuteDataTable(strProcedureName, lstSqlParams);

        }

        public bool IsRFQNoExists(string strRFQNo,int intCompanyID)
        {
            lstSqlParams = new ArrayList();

            lstSqlParams.Add(new SqlParameter("@Mode", 15));
            lstSqlParams.Add(new SqlParameter("@RFQNo", strRFQNo));
            lstSqlParams.Add(new SqlParameter("@RFQID", PobjClsDTORFQ.RFQID));
            lstSqlParams.Add(new SqlParameter("@CompanyID", intCompanyID));
            return (Convert.ToInt32(MobjDataLayer.ExecuteScalar(strProcedureName, lstSqlParams)) > 0);
        }

        public string GetTotalRFQCount()
        {
            lstSqlParams = new ArrayList();
            lstSqlParams.Add(new SqlParameter("@Mode", 18));
            return Convert.ToString(MobjDataLayer.ExecuteScalar(strProcedureName, lstSqlParams));
        }

        public DataTable DisplayRFQDetails()// Display Indent detail
        {
            //function for displaying purchase Quotation details
            lstSqlParams = new ArrayList();
            lstSqlParams.Add(new SqlParameter("@Mode", 7));
            lstSqlParams.Add(new SqlParameter("@RFQID", PobjClsDTORFQ.RFQID));
            return MobjDataLayer.ExecuteDataTable(strProcedureName, lstSqlParams);
        }

        public bool DisplayRFQInfo(Int64 iRFQID) // Purchse Indent
        {
            //function for displaying purchase Quotation info
            lstSqlParams = new ArrayList();
            lstSqlParams.Add(new SqlParameter("@Mode", 1));
            lstSqlParams.Add(new SqlParameter("@RFQID", iRFQID));
            DataTable dt = MobjDataLayer.ExecuteDataTable(strProcedureName, lstSqlParams);
            if (dt.Rows.Count > 0)
            {
                DataRow Row = dt.Rows[0];
                this.MapRFQMaster(Row);
                dt = this.DisplayRFQDetails();
                foreach (DataRow dr in dt.Rows)
                {
                    this.MapRFQDetails(dr);
                }

                return true;
            }
            else return false;         
        }

        public void MapRFQMaster(DataRow dr)
        {
            this.PobjClsDTORFQ.lstClsDTORFQDetails = new List<clsDTORFQDetails>();
            PobjClsDTORFQ.RFQID = dr["RFQID"].ToInt64();
            PobjClsDTORFQ.RFQNo = dr["RFQNo"].ToString();
            PobjClsDTORFQ.CompanyID = dr["CompanyID"].ToInt32();
            PobjClsDTORFQ.QuatationDate = dr["QuotationDate"].ToDateTime();
            PobjClsDTORFQ.DueDate = dr["DueDate"].ToDateTime();
            PobjClsDTORFQ.Remarks = dr["Remarks"].ToString();
            PobjClsDTORFQ.StatusID = dr["StatusID"].ToInt32();
            PobjClsDTORFQ.EmployeeName = dr["EmployeeName"].ToString();
            PobjClsDTORFQ.VerifcationDescription = dr["VerificationDescription"].ToString();
            PobjClsDTORFQ.ApprovedDate = dr["ApprovedDate"].ToDateTime();
            PobjClsDTORFQ.ApprovedBy = dr["ApprovedBy"].ToInt32();
            if (dr["ApprovedByName"] != DBNull.Value)
                PobjClsDTORFQ.strApprovedBy = dr["ApprovedByName"].ToString();
            PobjClsDTORFQ.CreatedDate = dr["CreatedDate"].ToDateTime();
            DataTable datCancellationDetails = DisplayCancellationDetails();

            if (datCancellationDetails != null && datCancellationDetails.Rows.Count > 0)
            {
                PobjClsDTORFQ.CancellationDescription = Convert.ToString(datCancellationDetails.Rows[0]["Description"]);
                PobjClsDTORFQ.CancellationDate = Convert.ToDateTime(datCancellationDetails.Rows[0]["Date"]);
                PobjClsDTORFQ.intCancelledBy = Convert.ToInt32(datCancellationDetails.Rows[0]["CancelledBy"]);
                PobjClsDTORFQ.strCancelledBy = Convert.ToString(datCancellationDetails.Rows[0]["CancelledByName"]);
            }
        }

        private void MapRFQDetails(DataRow Row)
        {
            this.objRFQDetails = new clsDTORFQDetails();
            objRFQDetails.ItemID = Row["ItemID"].ToInt32();
            objRFQDetails.Quantity = Row["Quantity"].ToDecimal();
            objRFQDetails.UOMID = Row["UOMID"].ToInt32();
            objRFQDetails.ItemCode = Row["ItemCode"].ToString();
            objRFQDetails.ItemName = Row["ItemName"].ToString();
            this.PobjClsDTORFQ.lstClsDTORFQDetails.Add(objRFQDetails);
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            lstSqlParams = new ArrayList();
            lstSqlParams.Add(new SqlParameter("@Mode", 10));
            lstSqlParams.Add(new SqlParameter("@ItemID", intItemID));
            lstSqlParams.Add(new SqlParameter("@UomTypeID", 2));
            DataTable dtItemUoms = new DataTable();
            dtItemUoms = MobjDataLayer.ExecuteDataTable("STPurchaseModule", lstSqlParams);

            lstSqlParams = new ArrayList();
            lstSqlParams.Add(new SqlParameter("@Mode", 12));
            lstSqlParams.Add(new SqlParameter("@ItemID", intItemID));
            DataTable dtItemBaseUnit = MobjDataLayer.ExecuteDataTable("STPurchaseModule", lstSqlParams);
            DataRow dr = dtItemUoms.NewRow();
            dr["UOMID"] = dtItemBaseUnit.Rows[0]["UOMID"];
            dr["ShortName"] = dtItemBaseUnit.Rows[0]["ShortName"];
            dr["ItemID"] = intItemID;
            dtItemUoms.Rows.InsertAt(dr, 0);
            return dtItemUoms;
        }

        private void UpdateStatus(int intStatusID, Int64 intPurchaseID)
        {

            lstSqlParams = new ArrayList();
            lstSqlParams.Add(new SqlParameter("@Mode", 16));
            lstSqlParams.Add(new SqlParameter("@StatusID", intStatusID));
            lstSqlParams.Add(new SqlParameter("@RFQID", intPurchaseID));
            MobjDataLayer.ExecuteNonQueryWithTran(strProcedureName, lstSqlParams);

        }       

        private bool VerificationHistoryTableUpdations(Int64 intReferenceID, bool blnIsDeletion)
        {
            // 1 - Purchase Quotation  2 - Purchase Order
            ArrayList prmVerHistory = new ArrayList();

            if (!blnIsDeletion)
                prmVerHistory.Add(new SqlParameter("@Mode", 20));
            else
                prmVerHistory.Add(new SqlParameter("@Mode", 21));
            prmVerHistory.Add(new SqlParameter("@OperationTypeID", (Int32)OperationType.RFQ));
            prmVerHistory.Add(new SqlParameter("@ReferenceID", intReferenceID));
            MobjDataLayer.ExecuteNonQuery(strProcedureName, prmVerHistory);
            return true;


        }

        public DataSet GetRFQReport()
        {
            lstSqlParams = new ArrayList();
            lstSqlParams.Add(new SqlParameter("@Mode", 19));
            lstSqlParams.Add(new SqlParameter("@RFQID", PobjClsDTORFQ.RFQID));
            return MobjDataLayer.ExecuteDataSet("STRFQTransaction", lstSqlParams);
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            lstSqlParams = new ArrayList();
            lstSqlParams.Add(new SqlParameter("@Mode", 4));
            lstSqlParams.Add(new SqlParameter("@RoleID", intRoleID));
            lstSqlParams.Add(new SqlParameter("@CompanyID", intCompanyID));
            lstSqlParams.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", lstSqlParams);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 12));
            prmCommon.Add(new SqlParameter("@RoleID", intRoleID));
            prmCommon.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmCommon.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", prmCommon);
        }

        public bool Suggest(int intOperationTypeID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 26));
            alParameters.Add(new SqlParameter("@OperationTypeID", intOperationTypeID));
            alParameters.Add(new SqlParameter("@ReferenceID", PobjClsDTORFQ.RFQID));
            alParameters.Add(new SqlParameter("@Description", PobjClsDTORFQ.VerifcationDescription));
            alParameters.Add(new SqlParameter("@StatusID", PobjClsDTORFQ.StatusID));
            alParameters.Add(new SqlParameter("@CancelledBy", PobjClsDTORFQ.ApprovedBy));
            alParameters.Add(new SqlParameter("@CancellationDate", PobjClsDTORFQ.ApprovedDate));
            if (MobjDataLayer.ExecuteNonQuery("STPurchaseQuotationTransaction", alParameters) != 0)
            {
                return true;
            }
            return false;
        }

        public bool UpdationVerificationTableRemarks(Int64 intReferenceID, string strRemarks)
        {
            ArrayList alParameters = new ArrayList();
                alParameters.Add(new SqlParameter("@Mode", 22));
                alParameters.Add(new SqlParameter("@OperationTypeID", (Int32)OperationType.RFQ));
                alParameters.Add(new SqlParameter("@RFQID", intReferenceID));
                alParameters.Add(new SqlParameter("@CancelledBy", ClsCommonSettings.UserID));
                alParameters.Add(new SqlParameter("@Remarks", strRemarks));
                MobjDataLayer.ExecuteNonQuery("STRFQTransaction", alParameters);
            return true;
        }
    }
}
