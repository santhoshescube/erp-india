﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using System.Data;
using System.Data.SqlClient;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,22 Feb 2011>
   Description:	<Description,,Purchase DAL Class>
================================================
*/
namespace MyBooksERP
{
    public class clsDALDirectGRN : IDisposable
    {
        ClsCommonUtility MobjClsCommonUtility; // object of clsCommonUtility
        public DataLayer MobjDataLayer;

        public clsDTOGRN PobjClsDTOGRN { get; set; }

        string strProcedureName = "spInvDirectGRN";

        bool blnIsOldStatusCancelled = false;

        ArrayList prmGRN;

        public clsDALDirectGRN(DataLayer objDataLayer)
        {
            MobjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MobjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MobjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }

        public int GetVendorRecordID(int iVendorID)     // Retreiving current vendorid for Focussing on the current vendor
        {
            int RecordCnt;
            RecordCnt = 0;
            SqlDataReader DS;

            string TSQL = "select a.CurrentID from (select ROW_NUMBER() over (order by VendorID) as  CurrentID " +
                                             "  ,VendorID from InvVendorInformation  where VendorTypeID not in (" + (int)VendorType.Customer + ")) a where a.VendorID=" + iVendorID + "";

            DS = MobjDataLayer.ExecuteReader(TSQL);
            if (DS.Read())
            {
                RecordCnt = Convert.ToInt32(DS["CurrentID"]);
            }
            else
            {
                RecordCnt = 0;
            }
            DS.Close();
            return RecordCnt;
        }

        public string DisplayAddressInformation(int iVendorAdd)
        {
            string sAddress = "";
            prmGRN = new ArrayList();

            prmGRN.Add(new SqlParameter("@Mode", 3));
            prmGRN.Add(new SqlParameter("@VendorAddID", iVendorAdd));
            SqlDataReader drVendor = MobjDataLayer.ExecuteReader(strProcedureName, prmGRN, CommandBehavior.SingleRow);

            if (drVendor.Read())
            {
                sAddress = Environment.NewLine;
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["ContactPerson"])))
                {
                    sAddress += Convert.ToString(drVendor["ContactPerson"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Address"])))
                {
                    sAddress += Convert.ToString(drVendor["Address"]);
                    sAddress += Environment.NewLine;
                }

                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["State"])))
                {
                    sAddress += Convert.ToString(drVendor["State"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Country"])))
                {
                    sAddress += Convert.ToString(drVendor["Country"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["ZipCode"])))
                {
                    sAddress += "ZipCode : " + Convert.ToString(drVendor["ZipCode"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Telephone"])))
                {
                    sAddress += "Telephone : " + Convert.ToString(drVendor["Telephone"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["MobileNo"])))
                {
                    sAddress += "MobileNo : " + Convert.ToString(drVendor["MobileNo"]);
                    sAddress += Environment.NewLine;
                }
               
                drVendor.Close();
                return sAddress;

            }
            else
            {
                drVendor.Close();
                return sAddress;
            }

        }

        public Int64 GetLastGRNNo(bool blnIsNoGeneration, int intCompanyID)
        {
           
            Int64 intRetValue = 0;
           
            prmGRN = new ArrayList();
            prmGRN.Add(new SqlParameter("@CompanyID", intCompanyID));
          
            if (blnIsNoGeneration)
            {
                prmGRN.Add(new SqlParameter("@Mode", 1));
            }
            else
            {
                prmGRN.Add(new SqlParameter("@Mode", 2));
            }
           
            SqlDataReader sdr = MobjDataLayer.ExecuteReader(strProcedureName, prmGRN);
            if (sdr.Read())
            {
                if (sdr["ReturnValue"] != DBNull.Value)
                    intRetValue = Convert.ToInt64(sdr["ReturnValue"]);
                else if (blnIsNoGeneration)
                    intRetValue = 0;
            }
            sdr.Close();
            return intRetValue;
        }

        public DataTable GetGRNNos()// Get GRN number For searching
        {
            //function for Finding Purchase 
            prmGRN = new ArrayList();
            prmGRN.Add(new SqlParameter("@Mode", 4));
            return MobjDataLayer.ExecuteDataTable(strProcedureName, prmGRN);
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            prmGRN = new ArrayList();
            prmGRN.Add(new SqlParameter("@Mode", 5));
            prmGRN.Add(new SqlParameter("@ItemID", intItemID));

            DataTable dtItemUoms = new DataTable();
            dtItemUoms = MobjDataLayer.ExecuteDataTable(strProcedureName, prmGRN);

            prmGRN = new ArrayList();
            prmGRN.Add(new SqlParameter("@Mode", 6));
            prmGRN.Add(new SqlParameter("@ItemID", intItemID));

            DataTable dtItemBaseUnit = MobjDataLayer.ExecuteDataTable(strProcedureName, prmGRN);
            DataRow dr = dtItemUoms.NewRow();
            dr["UOMID"] = dtItemBaseUnit.Rows[0]["UOMID"];
            dr["ShortName"] = dtItemBaseUnit.Rows[0]["ShortName"];
            dr["ItemID"] = intItemID;
            dtItemUoms.Rows.InsertAt(dr, 0);
            return dtItemUoms;
        }


        public bool SavePurchaseGRN()
        {
            object objOutGRN ;
            
            bool blnAddStatus = Convert.ToBoolean(PobjClsDTOGRN.lngGRNID == 0);

             prmGRN = new ArrayList();
             if (blnAddStatus)
             {
                 blnIsOldStatusCancelled = false;
                 prmGRN.Add(new SqlParameter("@Mode", 8));
             }
             else
             {
                 ArrayList parameters = new ArrayList();
                 parameters.Add(new SqlParameter("@Mode", 14));
                 parameters.Add(new SqlParameter("@GRNID", PobjClsDTOGRN.lngGRNID));
                 DataTable datCompany = MobjDataLayer.ExecuteDataTable(strProcedureName, parameters);

                 if (datCompany.Rows.Count > 0)
                 {
                     int intStatusId = -1;
                     if (datCompany.Rows[0]["StatusID"] != DBNull.Value) intStatusId = Convert.ToInt32(datCompany.Rows[0]["StatusID"]);
                     if (PobjClsDTOGRN.blnCancelled && intStatusId == PobjClsDTOGRN.intStatusID)
                         blnIsOldStatusCancelled = true;
                     else if (!PobjClsDTOGRN.blnCancelled && intStatusId != PobjClsDTOGRN.intStatusID)
                         blnIsOldStatusCancelled = true;
                 }
                 prmGRN.Add(new SqlParameter("@Mode", 10));
             }

                 prmGRN.Add(new SqlParameter("@GRNID", PobjClsDTOGRN.lngGRNID));
                 prmGRN.Add(new SqlParameter("@GRNNo", PobjClsDTOGRN.strGRNNo));
                 prmGRN.Add(new SqlParameter("@GRNDate", PobjClsDTOGRN.strGRNDate));
                 prmGRN.Add(new SqlParameter("@DueDate", PobjClsDTOGRN.strGRNDate));
                 prmGRN.Add(new SqlParameter("@OrderTypeID", (Int32)OperationOrderType.GRNDirect));
                 prmGRN.Add(new SqlParameter("@VendorID", PobjClsDTOGRN.intVendorID));
                 prmGRN.Add(new SqlParameter("@VendorAddID", PobjClsDTOGRN.intVendorAddID));
                 prmGRN.Add(new SqlParameter("@Remarks", PobjClsDTOGRN.strRemarks));
                 prmGRN.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
                 prmGRN.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDate()));
                 prmGRN.Add(new SqlParameter("@CompanyID", PobjClsDTOGRN.intCompanyID));
                 prmGRN.Add(new SqlParameter("@CurrencyID", PobjClsDTOGRN.intCurrencyID));
                 prmGRN.Add(new SqlParameter("@StatusID", PobjClsDTOGRN.intStatusID));
                 prmGRN.Add(new SqlParameter("@WarehouseID", PobjClsDTOGRN.intWarehouseID));
                 prmGRN.Add(new SqlParameter("@TotalAmount", PobjClsDTOGRN.decNetAmount));

                 SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
                 objParam.Direction = ParameterDirection.ReturnValue;
                 prmGRN.Add(objParam);

                 if (MobjDataLayer.ExecuteNonQueryWithTran(strProcedureName, prmGRN, out objOutGRN) != 0)
                 {
                     DeleteCancellationDetails();
                     if (PobjClsDTOGRN.blnCancelled)
                     {
                         SaveCancellationDetails();
                     }

                     PobjClsDTOGRN.lngGRNID = objOutGRN.ToInt64();
                     SaveGRNDetails(blnAddStatus);
                     //SaveCostingMethodDetails(PobjClsDTOGRN.lngGRNID); // Saving Costing Method & Pricing Scheme Details
                     return true;
                 }
             return false;
        }

        private void SaveGRNDetails(bool blnAddStatus)
        {
            ArrayList prmGRN;
            DataTable dtGRNDetails = DisplayGRNDetail(PobjClsDTOGRN.lngGRNID);
            if (!blnAddStatus)
            {
                if (blnIsOldStatusCancelled && PobjClsDTOGRN.intStatusID != (int)OperationStatusType.PInvoiceCancelled)
                    DeleteGRNDetails((int)OperationStatusType.GRNCancelled);
                else
                    DeleteGRNDetails((int)OperationStatusType.GRNOpened);
            }

            List<int> lstupdateIndices = new List<int>();
            if (!blnAddStatus && !blnIsOldStatusCancelled)
            {
                if (PobjClsDTOGRN.blnCancelled)
                {
                    for (int i = 0; i < dtGRNDetails.Rows.Count; i++)
                    {
                        clsDTOGRNDetails objDTOGRNDetails = new clsDTOGRNDetails();
                        objDTOGRNDetails.intBatchID = Convert.ToInt64(dtGRNDetails.Rows[i]["BatchID"]);
                        objDTOGRNDetails.intItemID = Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]);
                        DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtGRNDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]));
                        decimal decOldQuantity = Convert.ToDecimal(dtGRNDetails.Rows[i]["ReceivedQuantity"]);
                        decimal decOldExtraQty = Convert.ToDecimal(dtGRNDetails.Rows[i]["ExtraQuantity"]);
                        if (dtGetUomDetails.Rows.Count > 0)
                        {
                            int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                            decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                            if (intConversionFactor == 1)
                            {
                                decOldQuantity = decOldQuantity / decConversionValue;
                                decOldExtraQty = decOldExtraQty / decConversionValue;
                            }
                            else if (intConversionFactor == 2)
                            {
                                decOldQuantity = decOldQuantity * decConversionValue;
                                decOldExtraQty = decOldExtraQty * decConversionValue;
                            }
                        }
                        int intTempWarehouseID = PobjClsDTOGRN.intWarehouseID;
                        if (PobjClsDTOGRN.intTempWarehouseID != PobjClsDTOGRN.intWarehouseID)
                        {
                            PobjClsDTOGRN.intWarehouseID = PobjClsDTOGRN.intTempWarehouseID;
                            StockDetailsUpdation(objDTOGRNDetails, decOldQuantity, decOldExtraQty, true);
                            PobjClsDTOGRN.intWarehouseID = intTempWarehouseID;
                        }
                        else
                        {
                            StockDetailsUpdation(objDTOGRNDetails, decOldQuantity, decOldExtraQty, true);
                        }
                        StockMasterUpdation(objDTOGRNDetails, decOldQuantity, decOldExtraQty, true);
                    }
                }
                else
                {
                    for (int i = 0; i < dtGRNDetails.Rows.Count; i++)
                    {
                        bool blnExists = false;
                        for (int j = 0; j < PobjClsDTOGRN.lstclsDTOGRNDetailsCollection.Count; j++)
                        {
                            if ((PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intItemID == dtGRNDetails.Rows[i]["ItemID"].ToInt32()) && (PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].strBatchNumber == dtGRNDetails.Rows[i]["BatchNo"].ToString()))
                            {
                                PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intBatchID = FindBatchID(PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].strBatchNumber);

                                if (PobjClsDTOGRN.intOrderType != (int)OperationOrderType.GRNFromTransfer)
                                    PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intBatchID = BatchUpdation(PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j]);

                                DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtGRNDetails.Rows[j]["UOMID"]), Convert.ToInt32(dtGRNDetails.Rows[j]["ItemID"]));

                                decimal decOldQuantity = Convert.ToDecimal(dtGRNDetails.Rows[j]["ReceivedQuantity"]);
                                decimal decOldExtraQuantity = Convert.ToDecimal(dtGRNDetails.Rows[j]["ExtraQuantity"]);
                                if (dtGetUomDetails.Rows.Count > 0)
                                {
                                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                    if (intConversionFactor == 1)
                                    {
                                        decOldQuantity = decOldQuantity / decConversionValue;
                                        decOldExtraQuantity = decOldExtraQuantity / decConversionValue;
                                    }
                                    else if (intConversionFactor == 2)
                                    {
                                        decOldQuantity = decOldQuantity * decConversionValue;
                                        decOldExtraQuantity = decOldExtraQuantity * decConversionValue;
                                    }
                                }

                                StockMasterUpdation(PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j], decOldQuantity, decOldExtraQuantity, false);
                                int intTempWarehouseID = PobjClsDTOGRN.intWarehouseID;
                                if (PobjClsDTOGRN.intTempWarehouseID != PobjClsDTOGRN.intWarehouseID)
                                {
                                    PobjClsDTOGRN.intWarehouseID = PobjClsDTOGRN.intTempWarehouseID;
                                    StockDetailsUpdation(PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j], decOldQuantity, decOldExtraQuantity, true);
                                    PobjClsDTOGRN.intWarehouseID = intTempWarehouseID;
                                    StockDetailsUpdation(PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j], 0, 0, false);
                                }
                                else

                                    StockDetailsUpdation(PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j], decOldQuantity, decOldExtraQuantity, false);

                                prmGRN = new ArrayList();
                                prmGRN.Add(new SqlParameter("@Mode", 11));
                                prmGRN.Add(new SqlParameter("@SerialNo", j + 1));
                                prmGRN.Add(new SqlParameter("@GRNID", PobjClsDTOGRN.lngGRNID));
                                prmGRN.Add(new SqlParameter("@ItemID", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intItemID));
                                prmGRN.Add(new SqlParameter("@InvoicedQuantity", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].decInvoiceQuantity));
                                prmGRN.Add(new SqlParameter("@ReceivedQuantity", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].decQuantity));
                                prmGRN.Add(new SqlParameter("@ExtraQuantity", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].decExtraQuantity));
                                prmGRN.Add(new SqlParameter("@UOMID", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intUOMID));
                                prmGRN.Add(new SqlParameter("@BaseUnitQty", MobjClsCommonUtility.ConvertQtyToBaseUnitQty(PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intUOMID, PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intItemID, PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].decQuantity + PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].decExtraQuantity, 1)));
                                prmGRN.Add(new SqlParameter("@CompanyID", PobjClsDTOGRN.intCompanyID));
                                prmGRN.Add(new SqlParameter("@WarehouseID", PobjClsDTOGRN.intWarehouseID));
                                prmGRN.Add(new SqlParameter("@BatchID", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intBatchID));
                                if(PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].strExpiryDate != string.Empty)
                                prmGRN.Add(new SqlParameter("@ExpiryDate", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].strExpiryDate));
                                prmGRN.Add(new SqlParameter("@StatusID", PobjClsDTOGRN.intStatusID));
                                prmGRN.Add(new SqlParameter("@GRNDate", PobjClsDTOGRN.strGRNDate));
                                prmGRN.Add(new SqlParameter("@OperationTypeID", (Int32)OperationType.GRN));
                                prmGRN.Add(new SqlParameter("@PurchaseRate", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].decPurchaseRate));
                                prmGRN.Add(new SqlParameter("@NetAmount", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].decNetAmount));
                                MobjDataLayer.ExecuteNonQueryWithTran(strProcedureName, prmGRN);
                                lstupdateIndices.Add(j);
                                blnExists = true;
                                break;
                            }
                        }
                        if (!blnExists)
                        {
                            clsDTOGRNDetails objclsDTOGRNDetails = new clsDTOGRNDetails();
                            objclsDTOGRNDetails.intBatchID = Convert.ToInt64(dtGRNDetails.Rows[i]["BatchID"]);
                            objclsDTOGRNDetails.intItemID = Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]);
                            DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtGRNDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]));
                            decimal decOldQuantity = Convert.ToDecimal(dtGRNDetails.Rows[i]["ReceivedQuantity"]);
                            decimal decOldExtraQuantity = Convert.ToDecimal(dtGRNDetails.Rows[i]["ExtraQuantity"]);
                            if (dtGetUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                {
                                    decOldQuantity = decOldQuantity / decConversionValue;
                                    decOldExtraQuantity = decOldExtraQuantity / decConversionValue;
                                }
                                else if (intConversionFactor == 2)
                                {
                                    decOldQuantity = decOldQuantity * decConversionValue;
                                    decOldExtraQuantity = decOldExtraQuantity * decConversionValue;
                                }
                            }
                            int intTempWarehouseID = PobjClsDTOGRN.intWarehouseID;
                            if (PobjClsDTOGRN.intTempWarehouseID != PobjClsDTOGRN.intWarehouseID)
                            {
                                PobjClsDTOGRN.intWarehouseID = PobjClsDTOGRN.intTempWarehouseID;
                                StockDetailsUpdation(objclsDTOGRNDetails, decOldQuantity, decOldExtraQuantity, true);
                                PobjClsDTOGRN.intWarehouseID = intTempWarehouseID;
                            }
                            else
                                StockDetailsUpdation(objclsDTOGRNDetails, decOldQuantity, decOldExtraQuantity, true);
                            StockMasterUpdation(objclsDTOGRNDetails, decOldQuantity, decOldExtraQuantity, true);
                        }
                    }
                }
            }
            for (int j = 0; j < PobjClsDTOGRN.lstclsDTOGRNDetailsCollection.Count; j++)
            {
                if (!lstupdateIndices.Contains(j))
                {
                    PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intBatchID = FindBatchID(PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].strBatchNumber);
                    PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intBatchID = BatchUpdation(PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j]);

                    if (!PobjClsDTOGRN.blnCancelled)
                    {
                        StockMasterUpdation(PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j], 0, 0, false);
                        StockDetailsUpdation(PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j], 0, 0, false);
                    }

                    prmGRN = new ArrayList();
                    prmGRN.Add(new SqlParameter("@Mode", 11));
                    prmGRN.Add(new SqlParameter("@SerialNo", j + 1));
                    prmGRN.Add(new SqlParameter("@GRNID", PobjClsDTOGRN.lngGRNID));
                    prmGRN.Add(new SqlParameter("@ItemID", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intItemID));
                    prmGRN.Add(new SqlParameter("@ReceivedQuantity", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].decQuantity));
                    prmGRN.Add(new SqlParameter("@InvoicedQuantity", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].decInvoiceQuantity));
                    prmGRN.Add(new SqlParameter("@ExtraQuantity", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].decExtraQuantity));
                    prmGRN.Add(new SqlParameter("@UOMID", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intUOMID));
                    prmGRN.Add(new SqlParameter("@BaseUnitQty", MobjClsCommonUtility.ConvertQtyToBaseUnitQty(PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intUOMID, PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intItemID, PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].decQuantity + PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].decExtraQuantity, 1)));
                    prmGRN.Add(new SqlParameter("@CompanyID", PobjClsDTOGRN.intCompanyID));
                    prmGRN.Add(new SqlParameter("@WarehouseID", PobjClsDTOGRN.intWarehouseID));
                    prmGRN.Add(new SqlParameter("@BatchID", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].intBatchID));
                    prmGRN.Add(new SqlParameter("@ExpiryDate", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].strExpiryDate));
                    prmGRN.Add(new SqlParameter("@StatusID", PobjClsDTOGRN.intStatusID));
                    prmGRN.Add(new SqlParameter("@GRNDate", PobjClsDTOGRN.strGRNDate));
                    prmGRN.Add(new SqlParameter("@PurchaseRate", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].decPurchaseRate));
                    prmGRN.Add(new SqlParameter("@NetAmount", PobjClsDTOGRN.lstclsDTOGRNDetailsCollection[j].decNetAmount));
                    prmGRN.Add(new SqlParameter("@OperationTypeID", (Int32)OperationType.GRN));
                    MobjDataLayer.ExecuteNonQueryWithTran(strProcedureName, prmGRN);
                    lstupdateIndices.Add(j);

                }
            }
            blnIsOldStatusCancelled = false;

            
        }

        public void SaveCostingMethodDetails(long lngGRNID)
        {
            try
            {
                ArrayList alParameters = new ArrayList();
                DataTable datTempGRNMaster = new DataTable();
                DataTable datTempGRN = new DataTable();
                DataTable datTempPurchaseInvoiceRateGRN = new DataTable();

                datTempGRNMaster = FillCombos(new string[] { "*", "InvGRNMaster G INNER JOIN InvGRNDetails GRD ON G.GRNID = GRD.GRNID ",
                  "G.GRNID=" + lngGRNID + " AND G.OrderTypeID=" + (int)OperationOrderType.GRNDirect });
                datTempGRN = FillCombos(new string[] { "*", "InvGRNDetails", "GRNID=" + lngGRNID });
                if (datTempGRN != null && datTempGRN.Rows.Count > 0)
                {
                   
                    for (int iCounter = 0; iCounter <= datTempGRN.Rows.Count - 1; iCounter++)
                    {
                        if (datTempGRN.Rows[iCounter]["PurchaseRate"] != null && Convert.ToDecimal(datTempGRN.Rows[iCounter]["PurchaseRate"]) > 0)
                        {
                            DataTable datTemp = FillCombos(new string[] { "*", "InvCostingAndPricingDetails", "ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString() });
                            DataTable datTempItem = FillCombos(new string[] { "*", "InvItemMaster", "ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString() });
                            DataTable datTempCurrency = FillCombos(new string[] { "CurrencyId AS CurrencyID", "CompanyMaster", "CompanyID = " +
                                    "(SELECT (CASE WHEN CompanyID = 0 THEN 1 ELSE ISNULL(CompanyID,1) END) FROM InvItemMaster WHERE " +
                                    "ItemID= " + datTempGRN.Rows[iCounter]["ItemID"].ToString() + ")" });
                            DataTable datTempUom = FillCombos(new string[] { "*", "InvUOMConversions", "UOMID = " + datTempItem.Rows[0]["BaseUomID"].ToString() + 
                                        " AND OtherUOMID=" + datTempGRN.Rows[iCounter]["UOMID"].ToString() });

                            string strRate = "0", strActualRate = "0", strSaleRate = "0", strLandingCost = "0";
                            //DataTable datTempRate = GetSaleRateForBatch(datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32(), datTemp.Rows[0]["PricingSchemeID"].ToString().ToInt32(),
                            //    Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()));

                            DataTable datTempRate = GetSaleRate(datTemp.Rows[0]["CostingMethodID"].ToInt32(),
                            datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32(),
                            Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()));

                            if (datTempRate != null && datTempRate.Rows.Count > 0)
                            {

                                //datTempPurchaseInvoiceRateGRN = FillCombos(new string[] { "GD.PurchaseRate", "InvGRNDetails GD INNER JOIN InvGRNMaster GM ON GD.GRNID=GM.GRNID" ,
                                //    "GD.GRNID=" + lngGRNID + " AND GD.ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32() });
                                //if (datTempPurchaseInvoiceRateGRN != null)
                                //{
                                //    if (datTempPurchaseInvoiceRateGRN.Rows.Count > 0)
                                //        strRate = datTempPurchaseInvoiceRateGRN.Rows[0]["PurchaseRate"].ToString();
                                //}

                                //datTempPurchaseInvoiceRateGRN = FillCombos(new string[] { "GD.PurchaseRate", "InvGRNDetails GD INNER JOIN InvGRNMaster GM ON GD.GRNID=GM.GRNID" ,
                                //    "GD.GRNID=" + lngGRNID + " AND GD.ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32() });
                                //if (datTempPurchaseInvoiceRateGRN != null)
                                //{
                                //    if (datTempPurchaseInvoiceRateGRN.Rows.Count > 0)
                                //        strActualRate = datTempPurchaseInvoiceRateGRN.Rows[0]["PurchaseRate"].ToString();
                                //}

                                if (strRate == "0")
                                    strRate = datTempRate.Rows[0]["Rate"].ToString();
                                if (strActualRate == "0")
                                    strActualRate = datTempRate.Rows[0]["ActualRate"].ToString();
                                if (strLandingCost == "0")
                                    strLandingCost = datTempRate.Rows[0]["LandingCost"].ToString();

                                if (Convert.ToInt32(datTempGRN.Rows[iCounter]["UomID"].ToString()) != Convert.ToInt32(datTempItem.Rows[0]["BaseUomID"].ToString()))
                                {
                                    if (datTempUom.Rows[0]["ConversionFactorID"].ToString() == "1")
                                    {
                                        strRate = (Convert.ToDouble(strRate) * Convert.ToDouble(datTempUom.Rows[0]["ConversionRate"].ToString())).ToString();
                                        strActualRate = (Convert.ToDouble(strActualRate) * Convert.ToDouble(datTempUom.Rows[0]["ConversionRate"].ToString())).ToString();
                                        strLandingCost = (Convert.ToDouble(strLandingCost) * Convert.ToDouble(datTempUom.Rows[0]["ConversionRate"].ToString())).ToString();
                                    }
                                    else
                                    {
                                        strRate = (Convert.ToDouble(strRate) / Convert.ToDouble(datTempUom.Rows[0]["ConversionRate"].ToString())).ToString();
                                        strActualRate = (Convert.ToDouble(strActualRate) / Convert.ToDouble(datTempUom.Rows[0]["ConversionRate"].ToString())).ToString();
                                        strLandingCost = (Convert.ToDouble(strLandingCost) / Convert.ToDouble(datTempUom.Rows[0]["ConversionRate"].ToString())).ToString();
                                    }
                                }

                                //strRate = (getRatewithExchangeRate(strRate.ToDecimal(), datTempGRNMaster.Rows[0]["CompanyID"].ToInt32(),
                                //    datTempGRNMaster.Rows[0]["CurrencyID"].ToInt32(), datTempGRN.Rows[iCounter]["ItemID"].ToInt32())).ToString();
                                //strActualRate = (getRatewithExchangeRate(strActualRate.ToDecimal(), datTempGRNMaster.Rows[0]["CompanyID"].ToInt32(),
                                //    datTempGRNMaster.Rows[0]["CurrencyID"].ToInt32(), datTempGRN.Rows[iCounter]["ItemID"].ToInt32())).ToString();

                                alParameters = new ArrayList();
                                alParameters.Add(new SqlParameter("@Mode", 9));
                                alParameters.Add(new SqlParameter("@CostingMethodID", datTemp.Rows[0]["CostingMethodID"].ToString().ToInt32()));
                                alParameters.Add(new SqlParameter("@PricingSchemeID", datTemp.Rows[0]["PricingSchemeID"].ToString().ToInt32()));
                                alParameters.Add(new SqlParameter("@ItemID", datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32()));
                                alParameters.Add(new SqlParameter("@CurrencyID", datTempCurrency.Rows[0]["CurrencyID"].ToString().ToInt32()));
                                alParameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
                                alParameters.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDateTime()));
                                alParameters.Add(new SqlParameter("@BatchID", datTempGRN.Rows[iCounter]["BatchID"].ToString().ToInt32()));
                                alParameters.Add(new SqlParameter("@PurchaseRate", strRate.ToDouble()));
                                alParameters.Add(new SqlParameter("@ActualRate", strActualRate.ToDouble()));
                                alParameters.Add(new SqlParameter("@LandingCost", strLandingCost.ToDouble()));

                                DataTable datTempPricing = FillCombos(new string[] { "*", "InvPricingSchemeReference", "PricingSchemeID=" + datTemp.Rows[0]["PricingSchemeID"].ToString().ToInt32() });
                                double dblExRate = 1;

                                if (datTempItem.Rows[0]["IsLandingCostEffected"].ToStringCustom() == "")
                                    datTempItem.Rows[0]["IsLandingCostEffected"] = "False";

                                if (Convert.ToBoolean(datTempItem.Rows[0]["IsLandingCostEffected"].ToString()) == true)
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate = (Convert.ToDouble(strLandingCost) +
                                            (Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString()) * dblExRate)).ToString();
                                    else
                                        strSaleRate = (Convert.ToDouble(strLandingCost) +
                                                    (Convert.ToDouble(strLandingCost) * Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString())) / 100).ToString();
                                }
                                else
                                {
                                    if (Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()) == true)
                                    {
                                        if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                            strSaleRate = (Convert.ToDouble(strRate) +
                                                (Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString()) * dblExRate)).ToString();
                                        else
                                            strSaleRate = (Convert.ToDouble(strRate) +
                                                        (Convert.ToDouble(strRate) * Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString())) / 100).ToString();
                                    }
                                    else
                                    {
                                        if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                            strSaleRate = (Convert.ToDouble(strActualRate) +
                                                (Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString()) * dblExRate)).ToString();
                                        else
                                            strSaleRate = (Convert.ToDouble(strActualRate) +
                                                        (Convert.ToDouble(strActualRate) * Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString())) / 100).ToString();
                                    }
                                }

                                alParameters.Add(new SqlParameter("@SaleRate", strSaleRate.ToDouble()));
                                DeleteCostingAndPricingDetails(datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32(),
                                    datTempGRN.Rows[iCounter]["BatchID"].ToString().ToInt32());
                                MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", alParameters);
                            }
                            else
                            {
                                string strRate1 = "0", strActualRate1 = "0", strSaleRate1 = "0", strLandingCost1 = "0";
                                DataTable datTempRate1 = GetSaleRate(datTemp.Rows[datTemp.Rows.Count - 1]["CostingMethodID"].ToString().ToInt32(), datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32(),
                                    Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()));


                            //    datTempPurchaseInvoiceRateGRN = FillCombos(new string[] { "GD.PurchaseRate", "InvGRNDetails GD INNER JOIN InvGRNMaster GM ON GD.GRNID=GM.GRNID" ,
                            //"GD.GRNID=" + lngGRNID + " AND GD.ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32() });
                            //    if (datTempPurchaseInvoiceRateGRN != null)
                            //    {
                            //        if (datTempPurchaseInvoiceRateGRN.Rows.Count > 0)
                            //        {
                            //            strRate1 = datTempPurchaseInvoiceRateGRN.Rows[0]["PurchaseRate"].ToString();
                            //            strActualRate1 = datTempPurchaseInvoiceRateGRN.Rows[0]["PurchaseRate"].ToString();
                            //        }
                            //    }

                                if (strRate1 == "0")
                                    strRate1 = datTempRate1.Rows[0]["Rate"].ToString();
                                if (strActualRate1 == "0")
                                    strActualRate1 = datTempRate1.Rows[0]["ActualRate"].ToString();
                                if (strLandingCost1 == "0")
                                    strLandingCost1 = datTempRate1.Rows[0]["LandingCost"].ToString();

                                if (Convert.ToInt32(datTempGRN.Rows[iCounter]["UomID"].ToString()) != Convert.ToInt32(datTempItem.Rows[0]["BaseUomID"].ToString()))
                                {
                                    if (datTempUom.Rows[0]["ConversionFactorID"].ToString() == "1")
                                    {
                                        strRate1 = (Convert.ToDouble(strRate1) * Convert.ToDouble(datTempUom.Rows[0]["ConversionRate"].ToString())).ToString();
                                        strActualRate1 = (Convert.ToDouble(strActualRate1) * Convert.ToDouble(datTempUom.Rows[0]["ConversionRate"].ToString())).ToString();
                                        strLandingCost1 = (Convert.ToDouble(strLandingCost1) * Convert.ToDouble(datTempUom.Rows[0]["ConversionRate"].ToString())).ToString();
                                    }
                                    else
                                    {
                                        strRate1 = (Convert.ToDouble(strRate1) / Convert.ToDouble(datTempUom.Rows[0]["ConversionRate"].ToString())).ToString();
                                        strActualRate1 = (Convert.ToDouble(strActualRate1) / Convert.ToDouble(datTempUom.Rows[0]["ConversionRate"].ToString())).ToString();
                                        strLandingCost1 = (Convert.ToDouble(strLandingCost1) / Convert.ToDouble(datTempUom.Rows[0]["ConversionRate"].ToString())).ToString();
                                    }
                                }

                                //strRate1 = (getRatewithExchangeRate(strRate1.ToDecimal(), datTempGRNMaster.Rows[0]["CompanyID"].ToInt32(),
                                //    datTempGRNMaster.Rows[0]["CurrencyID"].ToInt32(), datTempGRN.Rows[iCounter]["ItemID"].ToInt32())).ToString();
                                //strActualRate1 = (getRatewithExchangeRate(strActualRate1.ToDecimal(), datTempGRNMaster.Rows[0]["CompanyID"].ToInt32(),
                                //    datTempGRNMaster.Rows[0]["CurrencyID"].ToInt32(), datTempGRN.Rows[iCounter]["ItemID"].ToInt32())).ToString();


                                alParameters = new ArrayList();
                                alParameters.Add(new SqlParameter("@Mode", 9));
                                alParameters.Add(new SqlParameter("@CostingMethodID", datTemp.Rows[0]["CostingMethodID"].ToString().ToInt32()));
                                alParameters.Add(new SqlParameter("@PricingSchemeID", datTemp.Rows[0]["PricingSchemeID"].ToString().ToInt32()));
                                alParameters.Add(new SqlParameter("@ItemID", datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32()));
                                alParameters.Add(new SqlParameter("@CurrencyID", datTempCurrency.Rows[0]["CurrencyID"].ToString().ToInt32()));
                                alParameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
                                alParameters.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDateTime()));
                                alParameters.Add(new SqlParameter("@BatchID", datTempGRN.Rows[iCounter]["BatchID"].ToString().ToInt32()));
                                alParameters.Add(new SqlParameter("@PurchaseRate", strRate1.ToDouble()));
                                alParameters.Add(new SqlParameter("@ActualRate", strActualRate1.ToDouble()));
                                alParameters.Add(new SqlParameter("@LandingCost", strLandingCost1.ToDouble()));

                                DataTable datTempPricing = FillCombos(new string[] { "*", "InvPricingSchemeReference", "PricingSchemeID=" + datTemp.Rows[0]["PricingSchemeID"].ToString().ToInt32() });
                                double dblExRate = 1;

                                if (datTempItem.Rows[0]["IsLandingCostEffected"].ToStringCustom() == "")
                                    datTempItem.Rows[0]["IsLandingCostEffected"] = "False";

                                if (Convert.ToBoolean(datTempItem.Rows[0]["IsLandingCostEffected"].ToString()) == true)
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate1 = (Convert.ToDouble(strLandingCost1) +
                                            (Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString()) * dblExRate)).ToString();
                                    else
                                        strSaleRate1 = (Convert.ToDouble(strLandingCost1) +
                                                    (Convert.ToDouble(strLandingCost1) * Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString())) / 100).ToString();
                                }
                                else
                                {
                                    if (Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()) == true)
                                    {
                                        if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                            strSaleRate1 = (Convert.ToDouble(strRate1) +
                                                (Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString()) * dblExRate)).ToString();
                                        else
                                            strSaleRate1 = (Convert.ToDouble(strRate1) +
                                                        (Convert.ToDouble(strRate1) * Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString())) / 100).ToString();
                                    }
                                    else
                                    {
                                        if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                            strSaleRate1 = (Convert.ToDouble(strActualRate1) +
                                                (Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString()) * dblExRate)).ToString();
                                        else
                                            strSaleRate1 = (Convert.ToDouble(strActualRate1) +
                                                        (Convert.ToDouble(strActualRate1) * Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString())) / 100).ToString();
                                    }
                                }

                                alParameters.Add(new SqlParameter("@SaleRate", strSaleRate1.ToDouble()));
                                DeleteCostingAndPricingDetails(datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32(),
                                    datTempGRN.Rows[iCounter]["BatchID"].ToString().ToInt32());
                                MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", alParameters);
                            }
                        }
                    }
                }
            }
            catch { }
        }
        public DataTable GetSaleRate(int intMethodID, int intItemId, bool blnRate)
        {
            ArrayList parameters = new ArrayList();
            if (intMethodID == (int)CostingMethodReference.LIFO)
                parameters.Add(new SqlParameter("@Mode", 3));
            else if (intMethodID == (int)CostingMethodReference.FIFO)
                parameters.Add(new SqlParameter("@Mode", 4));
            else if (intMethodID == (int)CostingMethodReference.AVG)
                parameters.Add(new SqlParameter("@Mode", 5));
            else if (intMethodID == (int)CostingMethodReference.MIN)
                parameters.Add(new SqlParameter("@Mode", 6));
            else if (intMethodID == (int)CostingMethodReference.MAX)
                parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@ItemID", intItemId));
            parameters.Add(new SqlParameter("@CalculationBasedOnRate", blnRate));
            DataTable datPricingScheme = MobjDataLayer.ExecuteDataTable("spInvPricingScheme", parameters);
            return datPricingScheme;
        }
        public decimal getRatewithExchangeRate(decimal decRate, int intCompanyID, int intCurrencyID, int intItemID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 17));
            parameters.Add(new SqlParameter("@PurchaseRate", decRate));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@CurrencyID", intCurrencyID));
            parameters.Add(new SqlParameter("@ItemID", intItemID));
            return Convert.ToDecimal(MobjDataLayer.ExecuteScalar("spInvPricingScheme", parameters));
        }
        public DataTable GetSaleRateForBatch(int intItemId, int intPricingSchemeID, bool blnRate)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@ItemID", intItemId));
            parameters.Add(new SqlParameter("@CalculationBasedOnRate", blnRate));
            parameters.Add(new SqlParameter("@PricingSchemeID", intPricingSchemeID));
            return MobjDataLayer.ExecuteDataTable("spInvPricingScheme", parameters);
        }
        private void DeleteCancellationDetails()
        {
            ArrayList  prmGRN = new ArrayList();
            prmGRN.Add(new SqlParameter("Mode", 17));
            prmGRN.Add(new SqlParameter("GRNID", PobjClsDTOGRN.lngGRNID));
            prmGRN.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
            MobjDataLayer.ExecuteNonQueryWithTran(strProcedureName, prmGRN);
        }

        public void SaveCancellationDetails()
        {

            ArrayList prmGRN = new ArrayList();
            prmGRN.Add(new SqlParameter("Mode", 18));
            prmGRN.Add(new SqlParameter("@CancelledBy", PobjClsDTOGRN.intCancelledBy));
            prmGRN.Add(new SqlParameter("@CancellationDate", PobjClsDTOGRN.strCancellationDate));
            prmGRN.Add(new SqlParameter("@Description", PobjClsDTOGRN.strDescription));
            prmGRN.Add(new SqlParameter("@CompanyID", PobjClsDTOGRN.intCompanyID));
            prmGRN.Add(new SqlParameter("GRNID", PobjClsDTOGRN.lngGRNID));
            prmGRN.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
            MobjDataLayer.ExecuteNonQueryWithTran(strProcedureName, prmGRN);
        }

        private bool StockMasterUpdation(clsDTOGRNDetails objclsDTOGRNDetails, decimal decOldQuantity, decimal decOldExtraQty, bool blnIsDeleted)
        {
            ArrayList prmStockMaster = new ArrayList();
            prmStockMaster.Add(new SqlParameter("@Mode", 1));
            prmStockMaster.Add(new SqlParameter("@ItemID", objclsDTOGRNDetails.intItemID));
            DataTable dtStockMaster = MobjDataLayer.ExecuteDataTable("spInvStockMaster", prmStockMaster);
            DataTable dtGetUomDetails = GetUomConversionValues(objclsDTOGRNDetails.intUOMID, objclsDTOGRNDetails.intItemID);
            decimal decQuantity = objclsDTOGRNDetails.decQuantity;
            decimal decExtraQuantity = objclsDTOGRNDetails.decExtraQuantity;
            if (dtGetUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                {
                    decQuantity = decQuantity / decConversionValue;
                    decExtraQuantity = decExtraQuantity / decConversionValue;
                }
                else if (intConversionFactor == 2)
                {
                    decQuantity = decQuantity * decConversionValue;
                    decExtraQuantity = decExtraQuantity * decConversionValue;
                }
            }
            if (dtStockMaster.Rows.Count > 0)
            {
                decimal decGRNQty = 0, decInvQty = 0, decOpeningStock = 0, decSoldQuantity = 0, decDamagedQuantity = 0, decDemoQuantity = 0, decTransferedQty = 0, decReceivedFromTransfer = 0;
                decGRNQty = dtStockMaster.Rows[0]["GRNQuantity"].ToDecimal();
                decInvQty = dtStockMaster.Rows[0]["InvoicedQuantity"].ToDecimal();
                decOpeningStock = dtStockMaster.Rows[0]["OpeningStock"].ToDecimal();
                decSoldQuantity = dtStockMaster.Rows[0]["SoldQuantity"].ToDecimal();
                decDamagedQuantity = dtStockMaster.Rows[0]["DamagedQuantity"].ToDecimal();
                decTransferedQty = dtStockMaster.Rows[0]["TransferedQuantity"].ToDecimal();
                decReceivedFromTransfer = dtStockMaster.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                decDemoQuantity = dtStockMaster.Rows[0]["DemoQuantity"].ToDecimal();
                decOpeningStock = dtStockMaster.Rows[0]["OpeningStock"].ToDecimal();
                if (!blnIsDeleted)
                {
                    decGRNQty = Convert.ToDecimal(dtStockMaster.Rows[0]["GRNQuantity"]) - (decOldQuantity + decOldExtraQty) + decQuantity + decExtraQuantity;
                }
                else
                {
                    decGRNQty = Convert.ToDecimal(dtStockMaster.Rows[0]["GRNQuantity"]) - (decOldQuantity + decOldExtraQty);
                }
                prmStockMaster = new ArrayList();
                prmStockMaster.Add(new SqlParameter("@Mode", 3));
                prmStockMaster.Add(new SqlParameter("@ItemID", objclsDTOGRNDetails.intItemID));
                prmStockMaster.Add(new SqlParameter("@GRNQuantity", decGRNQty));
                prmStockMaster.Add(new SqlParameter("@InvoicedQuantity", decInvQty));
                prmStockMaster.Add(new SqlParameter("@OpeningStock", decOpeningStock));
                prmStockMaster.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
                prmStockMaster.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));
                prmStockMaster.Add(new SqlParameter("@SoldQuantity", decSoldQuantity));
                prmStockMaster.Add(new SqlParameter("@DamagedQuantity", decDamagedQuantity));
                prmStockMaster.Add(new SqlParameter("@DemoQuantity", decDemoQuantity));
                prmStockMaster.Add(new SqlParameter("@LastModifiedBy", PobjClsDTOGRN.intCreatedBy));
                prmStockMaster.Add(new SqlParameter("@LastModifiedDate", PobjClsDTOGRN.strCreatedDate));
                prmStockMaster.Add(new SqlParameter("@CompanyID", PobjClsDTOGRN.intCompanyID));
                prmStockMaster.Add(new SqlParameter("@CurrencyID", PobjClsDTOGRN.intCurrencyID));
                prmStockMaster.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
                MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmStockMaster);
            }
            else
            {
                decimal decDefaultValue = 0;
                prmStockMaster = new ArrayList();
                prmStockMaster.Add(new SqlParameter("@Mode", 2));
                prmStockMaster.Add(new SqlParameter("@ItemID", objclsDTOGRNDetails.intItemID));
                prmStockMaster.Add(new SqlParameter("@GRNQuantity", decQuantity + decExtraQuantity));
                prmStockMaster.Add(new SqlParameter("@InvoicedQuantity", decDefaultValue));
                prmStockMaster.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
                prmStockMaster.Add(new SqlParameter("@LastModifiedBy", PobjClsDTOGRN.intCreatedBy));
                prmStockMaster.Add(new SqlParameter("@LastModifiedDate", PobjClsDTOGRN.strCreatedDate));
                prmStockMaster.Add(new SqlParameter("@CompanyID", PobjClsDTOGRN.intCompanyID));
                prmStockMaster.Add(new SqlParameter("@CurrencyID", PobjClsDTOGRN.intCurrencyID));
                MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmStockMaster);

            }
            return true;
        }

        private Int64 FindBatchID(string strBatchNo)
        {
            ArrayList prmBatch = new ArrayList();
            prmBatch.Add(new SqlParameter("@Mode", 2));
            prmBatch.Add(new SqlParameter("@BatchNo", strBatchNo));
            DataTable dtBatchDetails = MobjDataLayer.ExecuteDataTable("spInvBatchDetailsTransaction", prmBatch);
            if (dtBatchDetails.Rows.Count > 0)
                return Convert.ToInt64(dtBatchDetails.Rows[0]["BatchID"]);
            else
                return -1;
        }

        private Int64 BatchUpdation(clsDTOGRNDetails objclsDTOGRNDetails)
        {
            int intCostingMethodID = 0;
            Int64 intBatchID = 0;
            DataTable dtCostingMethod = FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + objclsDTOGRNDetails.intItemID });
            if (dtCostingMethod != null && dtCostingMethod.Rows.Count > 0)
            {
                intCostingMethodID = dtCostingMethod.Rows[0]["CostingMethodID"].ToInt32();
            }

            if (!string.IsNullOrEmpty(objclsDTOGRNDetails.strBatchNumber) && intCostingMethodID == (Int32)CostingMethodReference.Batch)
            {
                Int64 intMaxbatchID = 0;
                intBatchID = -1;
                ArrayList prmBatch = new ArrayList();
                prmBatch.Add(new SqlParameter("@Mode", 6));
                prmBatch.Add(new SqlParameter("@ItemID", objclsDTOGRNDetails.intItemID));
                intMaxbatchID = MobjDataLayer.ExecuteDataTable("spInvBatchDetailsTransaction", prmBatch).Rows[0]["MaxBatchID"].ToInt64();
                prmBatch = new ArrayList();
                prmBatch.Add(new SqlParameter("@BatchNo", objclsDTOGRNDetails.strBatchNumber));
                prmBatch.Add(new SqlParameter("@ItemID", objclsDTOGRNDetails.intItemID));

                if (objclsDTOGRNDetails.strExpiryDate != null)
                    prmBatch.Add(new SqlParameter("@ExpiryDate", objclsDTOGRNDetails.strExpiryDate));
                if (objclsDTOGRNDetails.intBatchID == -1)
                {
                    intBatchID = intMaxbatchID + 1;
                    prmBatch.Add(new SqlParameter("@Mode", 3));
                    prmBatch.Add(new SqlParameter("@BatchID", intBatchID));
                    prmBatch.Add(new SqlParameter("@CompanyID", PobjClsDTOGRN.intCompanyID));
                    prmBatch.Add(new SqlParameter("@CurrencyID", PobjClsDTOGRN.intCurrencyID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spInvBatchDetailsTransaction", prmBatch);
                }
                else
                {
                    intBatchID = objclsDTOGRNDetails.intBatchID;
                    prmBatch.Add(new SqlParameter("@Mode", 5));
                    prmBatch.Add(new SqlParameter("@BatchID", objclsDTOGRNDetails.intBatchID));
                    prmBatch.Add(new SqlParameter("@CompanyID", PobjClsDTOGRN.intCompanyID));
                    prmBatch.Add(new SqlParameter("@CurrencyID", PobjClsDTOGRN.intCurrencyID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spInvBatchDetailsTransaction", prmBatch);
                }
            }
            else
            {
                
                intBatchID = 0;
                objclsDTOGRNDetails.intBatchID = intBatchID;
                ArrayList prmBatch = new ArrayList();
                if(IsBatchExists(objclsDTOGRNDetails.intItemID))
                    prmBatch.Add(new SqlParameter("@Mode", 5));
                else
                    prmBatch.Add(new SqlParameter("@Mode", 3));
                if (objclsDTOGRNDetails.strExpiryDate != null)
                    prmBatch.Add(new SqlParameter("@ExpiryDate", objclsDTOGRNDetails.strExpiryDate));
                prmBatch.Add(new SqlParameter("@ItemID", objclsDTOGRNDetails.intItemID));
                prmBatch.Add(new SqlParameter("@BatchID", objclsDTOGRNDetails.intBatchID));
                prmBatch.Add(new SqlParameter("@BatchNo", objclsDTOGRNDetails.strBatchNumber));
                prmBatch.Add(new SqlParameter("@CompanyID", PobjClsDTOGRN.intCompanyID));
                prmBatch.Add(new SqlParameter("@CurrencyID", PobjClsDTOGRN.intCurrencyID));
                MobjDataLayer.ExecuteNonQueryWithTran("spInvBatchDetailsTransaction", prmBatch);
            }
            return intBatchID;
        }

        private bool IsBatchExists(int intItemID)
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 27));
            Parameters.Add(new SqlParameter("@ItemID", intItemID));
            object obj =  MobjDataLayer.ExecuteScalar(strProcedureName, Parameters);
            return obj.ToInt32() > 0;
        }

        public DataTable DisplayGRNDetail(long lngGRNID)// Display GRN detail
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 15));
            Parameters.Add(new SqlParameter("@GRNID", lngGRNID));
            return MobjDataLayer.ExecuteDataTable(strProcedureName, Parameters);
        }

        public DataTable DisplayPurchaseGRN(long GRNID)// Display GRN detail
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 14));
            Parameters.Add(new SqlParameter("@GRNID", GRNID));
            return MobjDataLayer.ExecuteDataSet(strProcedureName, Parameters).Tables[0];
        }

        public DataTable DisplayCancellationDetails(long lngGRNID)
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("Mode", 21));
            Parameters.Add(new SqlParameter("GRNID", lngGRNID));
            Parameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
            return MobjDataLayer.ExecuteDataTable(strProcedureName, Parameters);
        }

        public bool DeleteGRNDetails(int intOldStatusID)
        {
            //function for deleting purchase indent details
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 13));
            Parameters.Add(new SqlParameter("@GRNID", PobjClsDTOGRN.lngGRNID));
            Parameters.Add(new SqlParameter("@StatusID", intOldStatusID));
            Parameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
            Parameters.Add(new SqlParameter("@GRNDate", PobjClsDTOGRN.strGRNDate));
            MobjDataLayer.ExecuteNonQueryWithTran(strProcedureName, Parameters);
            return true;
        }

        public DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 16));
            Parameters.Add(new SqlParameter("@ItemID", intItemID));
            Parameters.Add(new SqlParameter("@UnitID", intUOMID));
            return MobjDataLayer.ExecuteDataTable(strProcedureName, Parameters);
        }

        public void StockDetailsUpdation(clsDTOGRNDetails objDTOGRNDetails, decimal decOldQty, decimal decOldExtraQty, bool blnIsDeleted)
        {
            ArrayList prmStock = new ArrayList();
            prmStock.Add(new SqlParameter("@Mode", 4));
            prmStock.Add(new SqlParameter("@ItemID", objDTOGRNDetails.intItemID));
            prmStock.Add(new SqlParameter("@BatchID", objDTOGRNDetails.intBatchID));
            prmStock.Add(new SqlParameter("@CompanyID", PobjClsDTOGRN.intCompanyID));
            prmStock.Add(new SqlParameter("@WarehouseID", PobjClsDTOGRN.intWarehouseID));
            DataTable dtStockDetails = MobjDataLayer.ExecuteDataTable("spInvStockMaster", prmStock);
            DataTable dtGetUomDetails = GetUomConversionValues(objDTOGRNDetails.intUOMID, objDTOGRNDetails.intItemID);
            decimal decQuantity = objDTOGRNDetails.decQuantity;
            decimal decExtraQuantity = objDTOGRNDetails.decExtraQuantity;
            if (dtGetUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                {
                    decQuantity = decQuantity / decConversionValue;
                    decExtraQuantity = decExtraQuantity / decConversionValue;
                }
                else if (intConversionFactor == 2)
                {
                    decQuantity = decQuantity * decConversionValue;
                    decExtraQuantity = decExtraQuantity * decConversionValue;
                }
            }
            prmStock = new ArrayList();
            prmStock.Add(new SqlParameter("@ItemID", objDTOGRNDetails.intItemID));
            prmStock.Add(new SqlParameter("@BatchID", objDTOGRNDetails.intBatchID));
            prmStock.Add(new SqlParameter("@CompanyID", PobjClsDTOGRN.intCompanyID));
            prmStock.Add(new SqlParameter("@WarehouseID", PobjClsDTOGRN.intWarehouseID));
            if (dtStockDetails.Rows.Count > 0)
            {
                decimal decGRNQty = 0, decInvQty = 0, decOpeningStock = 0, decSoldQuantity = 0, decDamagedQuantity = 0, decDemoQuantity = 0, decTransferedQty = 0, decReceivedFromTransfer = 0;
                decGRNQty = dtStockDetails.Rows[0]["GRNQuantity"].ToDecimal();
                decInvQty = dtStockDetails.Rows[0]["InvoicedQuantity"].ToDecimal();
                decOpeningStock = dtStockDetails.Rows[0]["OpeningStock"].ToDecimal();
                decSoldQuantity = dtStockDetails.Rows[0]["SoldQuantity"].ToDecimal();
                decDamagedQuantity = dtStockDetails.Rows[0]["DamagedQuantity"].ToDecimal();
                decTransferedQty = dtStockDetails.Rows[0]["TransferedQuantity"].ToDecimal();
                decReceivedFromTransfer = dtStockDetails.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                decDemoQuantity = dtStockDetails.Rows[0]["DemoQuantity"].ToDecimal();

                if (!blnIsDeleted)
                {
                    decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty) + decQuantity + decExtraQuantity;
                }
                else
                {
                    decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty);
                }

                prmStock.Add(new SqlParameter("@Mode", 6));
                prmStock.Add(new SqlParameter("@GRNQuantity", decGRNQty));
                prmStock.Add(new SqlParameter("@InvoicedQuantity", decInvQty));
                prmStock.Add(new SqlParameter("@OpeningStock", decOpeningStock));
                prmStock.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
                prmStock.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));
                prmStock.Add(new SqlParameter("@SoldQuantity", decSoldQuantity));
                prmStock.Add(new SqlParameter("@DamagedQuantity", decDamagedQuantity));
                prmStock.Add(new SqlParameter("@DemoQuantity", decDemoQuantity));
                prmStock.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));

                MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmStock);
            }
            else
            {

                prmStock.Add(new SqlParameter("@Mode", 5));
                prmStock.Add(new SqlParameter("@GRNQuantity", decQuantity + decExtraQuantity));
                prmStock.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
                MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmStock);
            }
        }

        public bool DeleteUnwantedBatchDetails()
        {
            DataTable datDeletedBatches = new DataTable();

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 10));
            alParameters.Add(new SqlParameter("@WarehouseID", PobjClsDTOGRN.intWarehouseID));
            datDeletedBatches = MobjDataLayer.ExecuteDataTable("spInvStockMaster", alParameters);

            //foreach (DataRow dr in datDeletedBatches.Rows)
            //{
            //    DeleteCostingAndPricingDetailsFromOpeningStock(dr["ItemID"].ToInt32(), dr["BatchID"].ToInt32());
            //}
            return true;
        }
        private void DeleteCostingAndPricingDetailsFromOpeningStock(int intItemID, int intBatchID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 16));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", intBatchID));
            MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", alParameters);

            CalculateCostingAndPricingAfterDelete(intItemID, intBatchID);
        }

        private void CalculateCostingAndPricingAfterDelete(int intTempItemID, int intBatchID)
        {
            DataTable datCosting = FillCombos(new string[] { "*", "InvCostingAndPricingDetails", "ItemID=" + intTempItemID +" AND BatchID > 0" });
            DataTable datTempItem = FillCombos(new string[] { "*", "InvItemMaster", "ItemID=" + intTempItemID });
            DataTable datTempBatch = new DataTable();
            if (datCosting != null)
            {
                if (datCosting.Rows.Count > 0)
                {
                    if (datCosting.Rows[0]["CostingMethodID"].ToInt32() != (int)CostingMethodReference.Batch && datCosting.Rows[0]["BatchID"].ToInt32()  == intBatchID)
                    {
                        switch (datCosting.Rows[0]["CostingMethodID"].ToInt32())
                        {
                            case (int)CostingMethodReference.FIFO:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.FIFO, intTempItemID);
                                break;
                            case (int)CostingMethodReference.LIFO:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.LIFO, intTempItemID);
                                break;
                            case (int)CostingMethodReference.MAX:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.MAX, intTempItemID);
                                break;
                            case (int)CostingMethodReference.MIN:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.MIN, intTempItemID);
                                break;
                            case (int)CostingMethodReference.AVG:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.AVG, intTempItemID);
                                break;
                        }
                        if (datTempBatch != null && datTempBatch.Rows.Count > 0 )
                        {
                            string strRate = "0", strActualRate = "0", strSaleRate = "0", strLandingCost = "0";
                            strRate = datTempBatch.Rows[0]["PurchaseRate"].ToString();
                            strActualRate = datTempBatch.Rows[0]["ActualRate"].ToString();
                            strLandingCost = datTempBatch.Rows[0]["LandingCost"].ToString();

                            ArrayList prms = new ArrayList();
                            prms.Add(new SqlParameter("@Mode", 9));
                            prms.Add(new SqlParameter("@CostingMethodID", datCosting.Rows[0]["CostingMethodID"].ToString().ToInt32()));
                            prms.Add(new SqlParameter("@PricingSchemeID", datCosting.Rows[0]["PricingSchemeID"].ToString().ToInt32()));
                            prms.Add(new SqlParameter("@ItemID", intTempItemID));
                            prms.Add(new SqlParameter("@CurrencyID", datCosting.Rows[0]["CurrencyID"].ToString().ToInt32()));
                            prms.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
                            prms.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDateTime()));
                            prms.Add(new SqlParameter("@BatchID", intBatchID));
                            prms.Add(new SqlParameter("@PurchaseRate", strRate.ToDouble()));
                            prms.Add(new SqlParameter("@ActualRate", strActualRate.ToDouble()));
                            prms.Add(new SqlParameter("@LandingCost", strLandingCost.ToDouble()));

                            DataTable datTempPricing = FillCombos(new string[] { "*", "InvPricingSchemeReference", "PricingSchemeID=" + datCosting.Rows[0]["PricingSchemeID"].ToString().ToInt32() });
                            double dblExRate = 1;

                            if (datTempItem.Rows[0]["IsLandingCostEffected"].ToStringCustom() == "")
                                datTempItem.Rows[0]["IsLandingCostEffected"] = "False";

                            if (Convert.ToBoolean(datTempItem.Rows[0]["IsLandingCostEffected"].ToString()) == true)
                            {
                                if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                    strSaleRate = (Convert.ToDouble(strLandingCost) +
                                        (Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString()) * dblExRate)).ToString();
                                else
                                    strSaleRate = (Convert.ToDouble(strLandingCost) +
                                                (Convert.ToDouble(strLandingCost) * Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString())) / 100).ToString();
                            }
                            else
                            {
                                if (Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()) == true)
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate = (Convert.ToDouble(strRate) +
                                            (Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString()) * dblExRate)).ToString();
                                    else
                                        strSaleRate = (Convert.ToDouble(strRate) +
                                                    (Convert.ToDouble(strRate) * Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString())) / 100).ToString();
                                }
                                else
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate = (Convert.ToDouble(strActualRate) +
                                            (Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString()) * dblExRate)).ToString();
                                    else
                                        strSaleRate = (strActualRate.ToDouble() +
                                                    (strActualRate.ToDouble() * Convert.ToDouble(datTempPricing.Rows[0]["PercOrAmtValue"].ToString())) / 100).ToString();
                                }
                            }
                            prms.Add(new SqlParameter("@SaleRate", strSaleRate.ToDouble()));
                            //DeleteCostingAndPricingDetails(intTempItemID, intBatchID);
                            MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", prms);
                        }
                    }
                }
            }
        }
        public DataTable GetPurchaseRateAndActualRate(int intMethodID, int intItemID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 18));
            parameters.Add(new SqlParameter("@CostingMethodID", intMethodID));
            parameters.Add(new SqlParameter("@ItemID", intItemID));
            DataTable datRate = MobjDataLayer.ExecuteDataTable("spInvPricingScheme", parameters);
            return datRate;
        }

        private void DeleteCostingAndPricingDetails(int intItemID, int intBatchID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 16));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", intBatchID));
            MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", alParameters);
        }

        public bool IsGRNNoExists(string strGRNNo, int intCompanyID)
        {
            bool blnIsExists = false;
            DataTable dtGRN = new DataTable();
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 19));
            Parameters.Add(new SqlParameter("@GRNNo", strGRNNo));
            Parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            dtGRN = MobjDataLayer.ExecuteDataTable(strProcedureName, Parameters);
            if (dtGRN.Rows.Count > 0 && Convert.ToInt64(dtGRN.Rows[0]["GRNID"]) != PobjClsDTOGRN.lngGRNID)
                blnIsExists = true;
               
            return blnIsExists;

        }

        public bool DeleteGRN(Int64 iOrdID)// Purchase GRN Save
        {
            object iOutPurchaseOrderID = 0;
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 14));
            Parameters.Add(new SqlParameter("@GRNID", iOrdID));
            Parameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
            DataTable datCompany = MobjDataLayer.ExecuteDataTable(strProcedureName, Parameters);

            if (datCompany.Rows.Count > 0)
            {
                int intStatusId = -1;
                if (datCompany.Rows[0]["StatusID"] != DBNull.Value) intStatusId = Convert.ToInt32(datCompany.Rows[0]["StatusID"]);
                if (PobjClsDTOGRN.blnCancelled && intStatusId == PobjClsDTOGRN.intStatusID)
                    blnIsOldStatusCancelled = true;
                else if (!PobjClsDTOGRN.blnCancelled && intStatusId != PobjClsDTOGRN.intStatusID)
                    blnIsOldStatusCancelled = true;

            }
            DataTable dtGRNDetails = DisplayGRNDetail(PobjClsDTOGRN.lngGRNID);
            if (!blnIsOldStatusCancelled)
            {
                for (int i = 0; i < dtGRNDetails.Rows.Count; i++)
                {
                    clsDTOGRNDetails objDTOGRNDetails = new clsDTOGRNDetails();
                    objDTOGRNDetails.intBatchID = Convert.ToInt64(dtGRNDetails.Rows[i]["BatchID"]);
                    objDTOGRNDetails.intItemID = Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]);
                    DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtGRNDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]));
                    decimal decOldQuantity = Convert.ToDecimal(dtGRNDetails.Rows[i]["ReceivedQuantity"]);
                    decimal decOldExtraQty = Convert.ToDecimal(dtGRNDetails.Rows[i]["ExtraQuantity"]);
                    if (dtGetUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                        {
                            decOldQuantity = decOldQuantity / decConversionValue;
                            decOldExtraQty = decOldExtraQty / decConversionValue;
                        }
                        else if (intConversionFactor == 2)
                        {
                            decOldQuantity = decOldQuantity * decConversionValue;
                            decOldExtraQty = decOldExtraQty * decConversionValue;
                        }
                    }
                    int intTempWarehouseID = PobjClsDTOGRN.intWarehouseID;
                    if (PobjClsDTOGRN.intTempWarehouseID != PobjClsDTOGRN.intWarehouseID)
                    {
                        PobjClsDTOGRN.intWarehouseID = PobjClsDTOGRN.intTempWarehouseID;
                        StockDetailsUpdation(objDTOGRNDetails, decOldQuantity, decOldExtraQty, true);
                        PobjClsDTOGRN.intWarehouseID = intTempWarehouseID;
                    }
                    else
                    {
                        StockDetailsUpdation(objDTOGRNDetails, decOldQuantity, decOldExtraQty, true);
                    }
                    StockMasterUpdation(objDTOGRNDetails, decOldQuantity, decOldExtraQty, true);
                }
            }

            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@GRNID", iOrdID));
            parameters.Add(new SqlParameter("@StatusID", PobjClsDTOGRN.intStatusID));
            parameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            blnIsOldStatusCancelled = false;
            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcedureName, parameters) != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DataSet GetGRNReport()
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 20));
            Parameters.Add(new SqlParameter("@GRNID", PobjClsDTOGRN.lngGRNID));
            return MobjDataLayer.ExecuteDataSet(strProcedureName, Parameters);
        }

        public bool IsPurchaseOrderExists()
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 22));
            Parameters.Add(new SqlParameter("@GRNID", PobjClsDTOGRN.lngGRNID));
            object objReturnValue =  MobjDataLayer.ExecuteScalar(strProcedureName, Parameters);
            return objReturnValue.ToInt32() > 0;
        }
        public bool IsPurchaseInvoiceExists()
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 23));
            Parameters.Add(new SqlParameter("@GRNID", PobjClsDTOGRN.lngGRNID));
            object objReturnValue = MobjDataLayer.ExecuteScalar(strProcedureName, Parameters);
            return objReturnValue.ToInt32() > 0;
        }

        public DataTable GetGRNNos(long lngGRNID, int intType)
        {
            ArrayList Parameters = new ArrayList();
            if (intType == 1)
            Parameters.Add(new SqlParameter("@Mode", 24));
            else
            Parameters.Add(new SqlParameter("@Mode", 25));
            Parameters.Add(new SqlParameter("@GRNID", lngGRNID));

            return MobjDataLayer.ExecuteDataTable(strProcedureName, Parameters);
           
        }

        public bool IsReferenceExists()
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 26));
            Parameters.Add(new SqlParameter("@GRNID", PobjClsDTOGRN.lngGRNID));
            object objReturnValue = MobjDataLayer.ExecuteScalar(strProcedureName, Parameters);
            return objReturnValue.ToInt32() > 0;
        }

        public bool GetExchangeCurrencyRate(int intCompanyID, int intCurrencyID)
        {
            ArrayList prmExchangeCurrencyRate = new ArrayList();
            prmExchangeCurrencyRate.Add(new SqlParameter("@Mode", 19));
            prmExchangeCurrencyRate.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmExchangeCurrencyRate.Add(new SqlParameter("@CurrencyID", intCurrencyID));

            DataTable datExchangeCurrencyRate = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmExchangeCurrencyRate);

            if (datExchangeCurrencyRate.Rows.Count > 0)
            {
                if (datExchangeCurrencyRate.Rows[0]["Exchangerate"] != DBNull.Value)
                    PobjClsDTOGRN.decExchangeCurrencyRate = Convert.ToDecimal(datExchangeCurrencyRate.Rows[0]["Exchangerate"]);

                if (PobjClsDTOGRN.decExchangeCurrencyRate >= 0)
                    return true;
            }

            return false;

        }

        // -------------------------------- For GRN Report ---------------------------

        public DataSet FillCombo(int intCompanyID, int intVendorID, long lngGRNID, int intStatusID)
        {
            ArrayList prmRptGRN = new ArrayList();
            prmRptGRN.Add(new SqlParameter("@Mode", 1));
            prmRptGRN.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmRptGRN.Add(new SqlParameter("@VendorID", intVendorID));
            return MobjDataLayer.ExecuteDataSet("spInvRptGRN", prmRptGRN);
        }

        public DataSet GetReport(int intCompanyID, int intVendorID, long lngGRNID, int intStatusID, DateTime? dtFrmDate, DateTime? dtToDate, int intChkFrmTo)
        {
            ArrayList prmRptGRN = new ArrayList();
            prmRptGRN.Add(new SqlParameter("@Mode",2));
            prmRptGRN.Add(new SqlParameter("@CompanyID",intCompanyID));
            prmRptGRN.Add(new SqlParameter("@VendorID",intVendorID));
            prmRptGRN.Add(new SqlParameter("@GRNID",lngGRNID));
            prmRptGRN.Add(new SqlParameter("@StatusID",intStatusID));
            prmRptGRN.Add(new SqlParameter("@FromDate", dtFrmDate));
            prmRptGRN.Add(new SqlParameter("@ToDate", dtToDate));
            prmRptGRN.Add(new SqlParameter("@chkFrmTo", intChkFrmTo));
            return MobjDataLayer.ExecuteDataSet("spInvRptGRN", prmRptGRN);
        }
        public bool IsInvoiceOrOrderExists()
        {
            ArrayList prmRptGRN = new ArrayList();
            prmRptGRN.Add(new SqlParameter("@Mode", 28));
            prmRptGRN.Add(new SqlParameter("@GRNID", PobjClsDTOGRN.lngGRNID));
            object obj = MobjDataLayer.ExecuteScalar(strProcedureName, prmRptGRN);
            return obj.ToInt32() > 0;
        }
       
        #region IDisposable Members

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        #endregion

       
    }
}
