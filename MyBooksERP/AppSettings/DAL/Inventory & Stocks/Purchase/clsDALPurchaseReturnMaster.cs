﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;

using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALPurchaseReturnMaster : IDisposable
    {
        ArrayList alParameters;
        DataTable dat;
        public clsDTOPurchaseReturnMaster objclsDTOPurchaseReturnMaster { get; set; }
        public DataLayer objclsConnection;

        public clsDALPurchaseReturnMaster(DataLayer objDataLayer)
        {
            objclsConnection = objDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public int GetLastReturnNo(int intCompanyID)
        {
            int intRecordCnt = 0;
            dat = new DataTable();
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 6));
            alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            dat = objclsConnection.ExecuteDataTable("spInvPurchaseReturn", alParameters);
            
            if (dat.Rows.Count > 0)
                intRecordCnt = Convert.ToInt32(dat.Rows[0]["LastReturnNo"]);

                return intRecordCnt + 1;
        }

        public DataTable GetReturnNumbers()
        {   // Get Return Number For searching
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 5));
            prmCommon.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));
            return objclsConnection.ExecuteDataTable("spInvPurchaseReturn", prmCommon);
        }

        public bool GetPurchaseReturnMasterDetails(Int64 lngPurchaseReturnID)
        {   //Purchase Return
            ArrayList prmPurchaseReturn = new ArrayList();
            prmPurchaseReturn.Add(new SqlParameter("@Mode", 1));

            prmPurchaseReturn.Add(new SqlParameter("@PurchaseReturnID", lngPurchaseReturnID));
            DataTable datPurchaseReturn = objclsConnection.ExecuteDataTable("spInvPurchaseReturn", prmPurchaseReturn);

            if (datPurchaseReturn.Rows.Count > 0)
            {
                objclsDTOPurchaseReturnMaster.intPurchaseReturnID = lngPurchaseReturnID;
                objclsDTOPurchaseReturnMaster.strPurchaseReturnNo = Convert.ToString(datPurchaseReturn.Rows[0]["PurchaseReturnNo"]);
                if (datPurchaseReturn.Rows[0]["ReturnDate"] != DBNull.Value) objclsDTOPurchaseReturnMaster.strReturnDate = Convert.ToString(datPurchaseReturn.Rows[0]["ReturnDate"]); else objclsDTOPurchaseReturnMaster.strReturnDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                if (datPurchaseReturn.Rows[0]["CompanyID"] != DBNull.Value) objclsDTOPurchaseReturnMaster.intCompanyID = Convert.ToInt32(datPurchaseReturn.Rows[0]["CompanyID"]);
                if (datPurchaseReturn.Rows[0]["WarehouseID"] != DBNull.Value) objclsDTOPurchaseReturnMaster.intWarehouseID = Convert.ToInt32(datPurchaseReturn.Rows[0]["WarehouseID"]); 
                if (datPurchaseReturn.Rows[0]["VendorID"] != DBNull.Value) objclsDTOPurchaseReturnMaster.intVendorID = Convert.ToInt32(datPurchaseReturn.Rows[0]["VendorID"]); else objclsDTOPurchaseReturnMaster.intVendorID = 0;
                //if (datPurchaseReturn.Rows[0]["GrandDiscountID"] != DBNull.Value) objclsDTOPurchaseReturnMaster.intGrandDiscountID = Convert.ToInt32(datPurchaseReturn.Rows[0]["GrandDiscountID"]); else objclsDTOPurchaseReturnMaster.intGrandDiscountID = 0;
                if (datPurchaseReturn.Rows[0]["GrandDiscountAmount"] != DBNull.Value) objclsDTOPurchaseReturnMaster.decGrandDiscountAmount = Convert.ToDecimal(datPurchaseReturn.Rows[0]["GrandDiscountAmount"]); else objclsDTOPurchaseReturnMaster.decGrandDiscountAmount = 0;
                if (datPurchaseReturn.Rows[0]["ExpenseAmount"] != DBNull.Value) objclsDTOPurchaseReturnMaster.decExpenseAmount = Convert.ToDecimal(datPurchaseReturn.Rows[0]["ExpenseAmount"]); else objclsDTOPurchaseReturnMaster.decExpenseAmount = 0;
                if (datPurchaseReturn.Rows[0]["GrandReturnAmount"] != DBNull.Value) objclsDTOPurchaseReturnMaster.decGrandReturnAmount = Convert.ToDecimal(datPurchaseReturn.Rows[0]["GrandReturnAmount"]); else objclsDTOPurchaseReturnMaster.decGrandReturnAmount = 0;
                if (datPurchaseReturn.Rows[0]["GrandAmount"] != DBNull.Value) objclsDTOPurchaseReturnMaster.decGrandAmount = Convert.ToDecimal(datPurchaseReturn.Rows[0]["GrandAmount"]); else objclsDTOPurchaseReturnMaster.decGrandAmount = 0;
                if (datPurchaseReturn.Rows[0]["NetAmount"] != DBNull.Value)
                    objclsDTOPurchaseReturnMaster.decNetAmount = Convert.ToDecimal(datPurchaseReturn.Rows[0]["NetAmount"]); else objclsDTOPurchaseReturnMaster.decNetAmount = 0;

                if (datPurchaseReturn.Rows[0]["CurrencyID"] != DBNull.Value) objclsDTOPurchaseReturnMaster.intCurrencyID = Convert.ToInt32(datPurchaseReturn.Rows[0]["CurrencyID"]); else objclsDTOPurchaseReturnMaster.intCurrencyID = 0;
                if (datPurchaseReturn.Rows[0]["PurchaseInvoiceID"] != DBNull.Value) objclsDTOPurchaseReturnMaster.intPurchaseInvoiceID = Convert.ToInt32(datPurchaseReturn.Rows[0]["PurchaseInvoiceID"]); else objclsDTOPurchaseReturnMaster.intPurchaseInvoiceID = 0;
                if (datPurchaseReturn.Rows[0]["StatusID"] != DBNull.Value) objclsDTOPurchaseReturnMaster.intStatusID = Convert.ToInt32(datPurchaseReturn.Rows[0]["StatusID"]); else objclsDTOPurchaseReturnMaster.intStatusID = 0;

                objclsDTOPurchaseReturnMaster.strReturnDate = Convert.ToString(datPurchaseReturn.Rows[0]["ReturnDate"]);
                if (datPurchaseReturn.Rows[0]["Remarks"] != DBNull.Value) objclsDTOPurchaseReturnMaster.strRemarks = Convert.ToString(datPurchaseReturn.Rows[0]["Remarks"]); else objclsDTOPurchaseReturnMaster.strRemarks = "";

                objclsDTOPurchaseReturnMaster.intCreatedBy = datPurchaseReturn.Rows[0]["CreatedBy"].ToInt32();
                objclsDTOPurchaseReturnMaster.strCreatedDate = datPurchaseReturn.Rows[0]["CreatedDate"].ToString();
                if(datPurchaseReturn.Rows[0]["AccountID"] != DBNull.Value)
                objclsDTOPurchaseReturnMaster.intAccountID = datPurchaseReturn.Rows[0]["AccountID"].ToInt32();
                if (datPurchaseReturn.Rows[0]["TaxSchemeID"] != DBNull.Value)
                    objclsDTOPurchaseReturnMaster.intTaxSchemeID = datPurchaseReturn.Rows[0]["TaxSchemeID"].ToInt32();
                if (datPurchaseReturn.Rows[0]["TaxAmount"] != DBNull.Value)
                    objclsDTOPurchaseReturnMaster.decTaxAmount = Convert.ToDecimal(datPurchaseReturn.Rows[0]["TaxAmount"]);
                else objclsDTOPurchaseReturnMaster.decTaxAmount = 0;


                DataTable datCancellationDetails = DisplayCancellationDetails();

                if (datCancellationDetails != null && datCancellationDetails.Rows.Count > 0)
                {
                    objclsDTOPurchaseReturnMaster.strCancellationDescription = Convert.ToString(datCancellationDetails.Rows[0]["Remarks"]);
                    objclsDTOPurchaseReturnMaster.strCancellationDate = Convert.ToString(datCancellationDetails.Rows[0]["Date"]);
                    objclsDTOPurchaseReturnMaster.strCancelledBy = Convert.ToString(datCancellationDetails.Rows[0]["CancelledByName"]);
                }

                objclsDTOPurchaseReturnMaster.intCreatedBy = Convert.ToInt32(datPurchaseReturn.Rows[0]["CreatedBy"]);
                objclsDTOPurchaseReturnMaster.strCreatedDate = Convert.ToString(datPurchaseReturn.Rows[0]["CreatedDate"]);
                objclsDTOPurchaseReturnMaster.strCreatedBy = Convert.ToString(datPurchaseReturn.Rows[0]["CreatedByName"]);

                return true;
            }
            return false;
        }

        public DataTable GetReturnDetDetails(long lngPurchaseReturnID)
        {   // Display Return detail
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode",9 ));
            prmCommon.Add(new SqlParameter("@PurchaseReturnID", lngPurchaseReturnID));
            return objclsConnection.ExecuteDataTable("spInvPurchaseReturn", prmCommon);
        }

        public bool SavePurchaseReturnMaster()
        {
            alParameters = new ArrayList();
            if (objclsDTOPurchaseReturnMaster.intPurchaseReturnID != 0)
            {
                DeletePurchaseReturnDetails();
                //DeletePurchaseReturnLocationDetails();
                DeleteCancellationDetails();
            }
            alParameters.Add(new SqlParameter("@Mode", objclsDTOPurchaseReturnMaster.intPurchaseReturnID == 0 ? 2 : 3));
            alParameters.Add(new SqlParameter("@PurchaseReturnID", objclsDTOPurchaseReturnMaster.intPurchaseReturnID));
            alParameters.Add(new SqlParameter("@PurchaseInvoiceID", objclsDTOPurchaseReturnMaster.intPurchaseInvoiceID));
            alParameters.Add(new SqlParameter("@PurchaseReturnNo", objclsDTOPurchaseReturnMaster.strPurchaseReturnNo));
            alParameters.Add(new SqlParameter("@ReturnDate", objclsDTOPurchaseReturnMaster.strReturnDate));
            alParameters.Add(new SqlParameter("@Remarks", objclsDTOPurchaseReturnMaster.strRemarks));
            alParameters.Add(new SqlParameter("@CurrencyID", objclsDTOPurchaseReturnMaster.intCurrencyID));

            //if (IsReturnExists(objclsDTOPurchaseReturnMaster.intPurchaseInvoiceID, objclsDTOPurchaseReturnMaster.intPurchaseReturnID))
            //{
            //    alParameters.Add(new SqlParameter("@GrandDiscountID", 0));
            //    alParameters.Add(new SqlParameter("@GrandDiscountAmount", 0));
            //}
            //else
            //{
            //    if (objclsDTOPurchaseReturnMaster.intGrandDiscountID > 0)
            //    {
            //        alParameters.Add(new SqlParameter("@GrandDiscountID", objclsDTOPurchaseReturnMaster.intGrandDiscountID));
            //    }
            //    alParameters.Add(new SqlParameter("@GrandDiscountAmount", objclsDTOPurchaseReturnMaster.decGrandDiscountAmount));
            //}
            alParameters.Add(new SqlParameter("@GrandReturnAmount", objclsDTOPurchaseReturnMaster.decGrandReturnAmount));
            alParameters.Add(new SqlParameter("@GrandAmount", objclsDTOPurchaseReturnMaster.decGrandAmount));
            alParameters.Add(new SqlParameter("@NetAmount", objclsDTOPurchaseReturnMaster.decNetAmount));
            alParameters.Add(new SqlParameter("@CreatedBy", objclsDTOPurchaseReturnMaster.intCreatedBy));
            alParameters.Add(new SqlParameter("@CreatedDate", objclsDTOPurchaseReturnMaster.strCreatedDate));
            alParameters.Add(new SqlParameter("@StatusID", objclsDTOPurchaseReturnMaster.intStatusID));
            alParameters.Add(new SqlParameter("@CompanyID", objclsDTOPurchaseReturnMaster.intCompanyID));
            alParameters.Add(new SqlParameter("@DebitHeadID", objclsDTOPurchaseReturnMaster.intDebitHeadID));
            alParameters.Add(new SqlParameter("@CreditHeadID", objclsDTOPurchaseReturnMaster.intCreditHeadID));
            alParameters.Add(new SqlParameter("@AccountID", objclsDTOPurchaseReturnMaster.intAccountID));
            alParameters.Add(new SqlParameter("@TaxSchemeID", objclsDTOPurchaseReturnMaster.intTaxSchemeID));
            alParameters.Add(new SqlParameter("@TaxAmount", objclsDTOPurchaseReturnMaster.decTaxAmount));
            SqlParameter objParameter = new SqlParameter("RetValue",SqlDbType.Int);
            objParameter.Direction = ParameterDirection.ReturnValue;
            alParameters.Add(objParameter);

            object objPurchaseReturnID=0;
            if (objclsConnection.ExecuteNonQueryWithTran("spInvPurchaseReturn", alParameters, out objPurchaseReturnID) != 0)
            {
                if((Int32)objPurchaseReturnID >0)
                {
                    objclsDTOPurchaseReturnMaster.intPurchaseReturnID = (Int32)objPurchaseReturnID;
                    if (objclsDTOPurchaseReturnMaster.intStatusID == (Int32)OperationStatusType.PReturnCancelled)
                        SaveCancellationDetails();
                    SavePurchaseReturnDetails();
                    //SavePurchaseReturnLocationDetails();
                }
            }
            return true;
        }
        public bool IsReturnExists(long lngPurchaseInvoiceID, long lngPurchaseReturnID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 32));
            alParameters.Add(new SqlParameter("@PurchaseInvoiceID", lngPurchaseInvoiceID));
            alParameters.Add(new SqlParameter("@PurchaseReturnID", lngPurchaseReturnID));
            object obj = objclsConnection.ExecuteScalar("spInvPurchaseReturn", alParameters);
            return obj.ToInt32() > 0;
        }

        public bool SavePurchaseReturnDetails()
        {
            ArrayList alDetParameters;
            int intSerialNo=0;
            foreach(clsDTOPurchaseReturnDetails objPurchaseReturnDetails in objclsDTOPurchaseReturnMaster.lstPurchaseReturnDetails)
            {
                alDetParameters = new ArrayList();
                alDetParameters.Add(new SqlParameter("@Mode", 7));
                alDetParameters.Add(new SqlParameter("@SerialNo", ++intSerialNo));
                alDetParameters.Add(new SqlParameter("@PurchaseReturnID", objclsDTOPurchaseReturnMaster.intPurchaseReturnID));
                alDetParameters.Add(new SqlParameter("@ReferenceSerialNo", objPurchaseReturnDetails.intReferenceSerialNo));
                alDetParameters.Add(new SqlParameter("@BatchID", objPurchaseReturnDetails.lngBatchID));
                alDetParameters.Add(new SqlParameter("@ReturnQuantity", objPurchaseReturnDetails.decReturnQuantity));
                alDetParameters.Add(new SqlParameter("@Rate", objPurchaseReturnDetails.decRate));
                alDetParameters.Add(new SqlParameter("@ReasonID", objPurchaseReturnDetails.intReasonID));
                alDetParameters.Add(new SqlParameter("@UOMID", objPurchaseReturnDetails.intUOMID));
                if (objPurchaseReturnDetails.intDiscountID > 0)
                {
                    alDetParameters.Add(new SqlParameter("@DiscountID", objPurchaseReturnDetails.intDiscountID));
                }
                alDetParameters.Add(new SqlParameter("@DiscountAmount", objPurchaseReturnDetails.decDiscountAmount));
                alDetParameters.Add(new SqlParameter("@ReturnAmount", objPurchaseReturnDetails.decReturnAmount));
                alDetParameters.Add(new SqlParameter("@GrandAmount", objPurchaseReturnDetails.decGrandAmount));
                alDetParameters.Add(new SqlParameter("@NetAmount", objPurchaseReturnDetails.decNetAmount));
                alDetParameters.Add(new SqlParameter("@StatusID", objclsDTOPurchaseReturnMaster.intStatusID));
                alDetParameters.Add(new SqlParameter("@CompanyID", objclsDTOPurchaseReturnMaster.intCompanyID));
                alDetParameters.Add(new SqlParameter("@WarehouseID", objclsDTOPurchaseReturnMaster.intWarehouseID));
                alDetParameters.Add(new SqlParameter("@CurrencyID", objclsDTOPurchaseReturnMaster.intCurrencyID));
                alDetParameters.Add(new SqlParameter("@ReturnDate", objclsDTOPurchaseReturnMaster.strReturnDate));
                alDetParameters.Add(new SqlParameter("@ItemID", objPurchaseReturnDetails.intItemID));

                objclsConnection.ExecuteNonQueryWithTran("spInvPurchaseReturn", alDetParameters);

                if (objclsDTOPurchaseReturnMaster.intStatusID != (Int32)OperationStatusType.PReturnCancelled)
                {
                        decimal decGRNQty = 0, decInvoicedQty = 0;
                        decimal decAlreadyReturnedQty = 0;
                        decimal decExtraItemQty = 0;
                        decimal decQuantity = 0;

                        decQuantity = ConvertQtyToBaseUnitQty(objPurchaseReturnDetails.intUOMID, objPurchaseReturnDetails.intItemID, objPurchaseReturnDetails.decReturnQuantity);
                        decAlreadyReturnedQty = GetAlreadyReturnedQtyInBaseUOM(objclsDTOPurchaseReturnMaster.intPurchaseInvoiceID, objPurchaseReturnDetails.intReferenceSerialNo);
                        decAlreadyReturnedQty = decAlreadyReturnedQty - decQuantity;
                        decExtraItemQty = GetExtraItemQty(objclsDTOPurchaseReturnMaster.intPurchaseInvoiceID, objPurchaseReturnDetails.intItemID, objPurchaseReturnDetails.lngBatchID);

                        GetStockDetailsQuantity(objPurchaseReturnDetails.intItemID, objPurchaseReturnDetails.lngBatchID, objclsDTOPurchaseReturnMaster.intWarehouseID, out decGRNQty, out decInvoicedQty);

                        // decInvoicedQty = decInvoicedQty - decQuantity;
                        if (decAlreadyReturnedQty > decExtraItemQty)
                            decInvoicedQty = decInvoicedQty - decQuantity;
                        else if (decQuantity + decAlreadyReturnedQty > decExtraItemQty)
                            decInvoicedQty = decInvoicedQty - ((decQuantity + decAlreadyReturnedQty) - decExtraItemQty);

                        decGRNQty = decGRNQty - decQuantity;

                        UpdateStockDetailsQuantity(objPurchaseReturnDetails.intItemID, objPurchaseReturnDetails.lngBatchID, objclsDTOPurchaseReturnMaster.intWarehouseID, decGRNQty, decInvoicedQty);

                        GetStockMasterQuantity(objPurchaseReturnDetails.intItemID, out decGRNQty, out decInvoicedQty);

                        if (decAlreadyReturnedQty > decExtraItemQty)
                            decInvoicedQty = decInvoicedQty - decQuantity;
                        else if (decQuantity + decAlreadyReturnedQty > decExtraItemQty)
                            decInvoicedQty = decInvoicedQty - ((decQuantity + decAlreadyReturnedQty) - decExtraItemQty);

                        //  decInvoicedQty = decInvoicedQty - ConvertQtyToBaseUnitQty(objPurchaseReturnDetails.intUOMID, objPurchaseReturnDetails.intItemID, objPurchaseReturnDetails.decReturnQuantity);

                        decGRNQty = decGRNQty - decQuantity;//ConvertQtyToBaseUnitQty(objPurchaseReturnDetails.intUOMID, objPurchaseReturnDetails.intItemID, objPurchaseReturnDetails.decReturnQuantity);
                        UpdateStockMasterQuantity(objPurchaseReturnDetails.intItemID, decGRNQty, decInvoicedQty);
                }
            }

            UpdateStockValue();

            return true;

        }

        private bool UpdateStockValue()
        {
            //This is to Update Stock Value

            ArrayList prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation.Add(new SqlParameter("@Mode", 26));
            prmItemStockValueUpdation.Add(new SqlParameter("@PurchaseReturnID", objclsDTOPurchaseReturnMaster.intPurchaseReturnID));
            prmItemStockValueUpdation.Add(new SqlParameter("@StatusID", objclsDTOPurchaseReturnMaster.intStatusID));

            if (objclsConnection.ExecuteNonQueryWithTran("spInvPurchaseReturn", prmItemStockValueUpdation) != 0)
                return true;
            return false;
        }

        public bool DeletePurchaseReturnDetails()
        {
            DataTable datPurchaseReturnDetails = GetReturnDetDetails(objclsDTOPurchaseReturnMaster.intPurchaseReturnID);
            DataTable dtPurchaseReturn = FillCombos(new string[] {"StatusID","InvPurchaseReturnMaster","PurchaseReturnID = "+objclsDTOPurchaseReturnMaster.intPurchaseReturnID});
            int intOldStatusID = Convert.ToInt32(dtPurchaseReturn.Rows[0]["StatusID"]);
            if (intOldStatusID != (Int32)OperationStatusType.PReturnCancelled)
            {
                foreach (DataRow dr in datPurchaseReturnDetails.Rows)
                {
                    decimal decGRNQuantity = 0, decInvoicedQuantity = 0;

                    decimal decAlreadyReturnedQty = 0;
                    decimal decExtraItemQty = 0;
                    decimal decQuantity = 0;

                    decExtraItemQty = GetExtraItemQty(objclsDTOPurchaseReturnMaster.intPurchaseInvoiceID, dr["ItemID"].ToInt32(), dr["BatchID"].ToInt64());
                    decAlreadyReturnedQty = GetAlreadyReturnedQtyInBaseUOM(objclsDTOPurchaseReturnMaster.intPurchaseInvoiceID, dr["ReferenceSerialNo"].ToInt32());
                    decQuantity = ConvertQtyToBaseUnitQty(Convert.ToInt32(dr["UOMID"]), Convert.ToInt32(dr["ItemID"]), Convert.ToDecimal(dr["ReturnQuantity"]));

                    GetStockDetailsQuantity(Convert.ToInt32(dr["ItemID"]), Convert.ToInt64(dr["BatchID"]), objclsDTOPurchaseReturnMaster.intWarehouseID, out decGRNQuantity, out decInvoicedQuantity);

                    decGRNQuantity = decGRNQuantity + decQuantity;

                    if ((decAlreadyReturnedQty - decQuantity) > decExtraItemQty)
                        decInvoicedQuantity = decInvoicedQuantity + decQuantity;
                    else if(decAlreadyReturnedQty > decExtraItemQty)
                        decInvoicedQuantity = decInvoicedQuantity + ((decAlreadyReturnedQty - decExtraItemQty));
                    //  decInvoicedQuantity = 100;
                    //decInvoicedQuantity = decInvoicedQuantity + ConvertQtyToBaseUnitQty(Convert.ToInt32(dr["UOMID"]), Convert.ToInt32(dr["ItemID"]), Convert.ToDecimal(dr["ReturnQuantity"]));
                    UpdateStockDetailsQuantity(Convert.ToInt32(dr["ItemID"]), Convert.ToInt64(dr["BatchID"]), objclsDTOPurchaseReturnMaster.intWarehouseID, decGRNQuantity, decInvoicedQuantity);

                    GetStockMasterQuantity(Convert.ToInt32(dr["ItemID"]), out decGRNQuantity, out decInvoicedQuantity);
                    decGRNQuantity = decGRNQuantity + decQuantity;// ConvertQtyToBaseUnitQty(Convert.ToInt32(dr["UOMID"]), Convert.ToInt32(dr["ItemID"]), Convert.ToDecimal(dr["ReturnQuantity"]));

                    if ((decAlreadyReturnedQty - decQuantity) > decExtraItemQty)
                        decInvoicedQuantity = decInvoicedQuantity + decQuantity;
                    else if (decAlreadyReturnedQty > decExtraItemQty)
                        decInvoicedQuantity = decInvoicedQuantity + ((decAlreadyReturnedQty - decExtraItemQty));
                    //decInvoicedQuantity = decInvoicedQuantity + ConvertQtyToBaseUnitQty(Convert.ToInt32(dr["UOMID"]), Convert.ToInt32(dr["ItemID"]), Convert.ToDecimal(dr["ReturnQuantity"]));

                    UpdateStockMasterQuantity(Convert.ToInt32(dr["ItemID"]), decGRNQuantity, decInvoicedQuantity);
                }
            }
            ArrayList alDetParameters = new ArrayList();
            alDetParameters.Add(new SqlParameter("@Mode", 8));
            alDetParameters.Add(new SqlParameter("@PurchaseReturnID", objclsDTOPurchaseReturnMaster.intPurchaseReturnID));
            alDetParameters.Add(new SqlParameter("@StatusID", intOldStatusID));

            objclsConnection.ExecuteNonQueryWithTran("spInvPurchaseReturn", alDetParameters);
            return true;
        }

        public bool DeletePurchaseReturnMaster()
        {
            //DeletePurchaseReturnLocationDetails();
            DeletePurchaseReturnDetails();
            DeleteCancellationDetails();
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 4));
            alParameters.Add(new SqlParameter("@PurchaseReturnID", objclsDTOPurchaseReturnMaster.intPurchaseReturnID));

            objclsConnection.ExecuteNonQueryWithTran("spInvPurchaseReturn", alParameters);
            return true;
        }

        public DataTable GetPurchaseInvoiceItemDetails(int intPurchaseInvoiceID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 10));
            alParameters.Add(new SqlParameter("@PurchaseInvoiceID", intPurchaseInvoiceID));
            return objclsConnection.ExecuteDataTable("spInvPurchaseReturn", alParameters);
        }


        public DataTable GetItemUOMs(int intItemID)
        {
            ArrayList prmPurchaseReturn = new ArrayList();
            prmPurchaseReturn.Add(new SqlParameter("@Mode", 10));
            prmPurchaseReturn.Add(new SqlParameter("@ItemID", intItemID));
          //  prmPurchaseReturn.Add(new SqlParameter("@UomTypeID", 2));
            DataTable dtItemUoms = new DataTable();
            dtItemUoms = objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmPurchaseReturn);

            prmPurchaseReturn = new ArrayList();
            prmPurchaseReturn.Add(new SqlParameter("@Mode", 12));
            prmPurchaseReturn.Add(new SqlParameter("@ItemID", intItemID));
            DataTable dtItemBaseUnit = objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmPurchaseReturn);
            DataRow dr = dtItemUoms.NewRow();
            dr["UOMID"] = dtItemBaseUnit.Rows[0]["UOMID"];
            dr["ShortName"] = dtItemBaseUnit.Rows[0]["ShortName"];
            dr["ItemID"] = intItemID;
            dtItemUoms.Rows.InsertAt(dr, 0);
            return dtItemUoms;
        }
        public DataTable GetUomConversionValues(int intUOMID, Int64 intItemID)
        {
            ArrayList prmUomDetails = new ArrayList();
            prmUomDetails.Add(new SqlParameter("@Mode", 11));
            prmUomDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmUomDetails.Add(new SqlParameter("@UnitID", intUOMID));
            //prmUomDetails.Add(new SqlParameter("@UomTypeID", 2));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmUomDetails);
        }

        //public void GetItemDiscountAmount(int intMode, int intItemID, int intDiscountID, int intVendorID)
        //{
        //    ArrayList prmItemDiscount = new ArrayList();
        //    prmItemDiscount.Add(new SqlParameter("@Mode", intMode));
        //    prmItemDiscount.Add(new SqlParameter("@ItemID", intItemID));
        //    prmItemDiscount.Add(new SqlParameter("@DiscountID", intDiscountID));
        //    prmItemDiscount.Add(new SqlParameter("@VendorID", intVendorID));

        //    DataTable datDiscountAmount = objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmItemDiscount);

        //    if (datDiscountAmount.Rows.Count > 0)
        //    {
        //        objclsDTOPurchaseReturnMaster.blnDiscountForAmount = Convert.ToBoolean(datDiscountAmount.Rows[0]["DiscountForAmount"]);
        //        objclsDTOPurchaseReturnMaster.blnPercentCalculation = Convert.ToBoolean(datDiscountAmount.Rows[0]["PercentOrAmount"]);
        //        objclsDTOPurchaseReturnMaster.decDiscountValue = Convert.ToDecimal(datDiscountAmount.Rows[0]["PercOrAmtValue"]);
        //    }
        //}
        public bool IsDiscountForAmount(int intDiscountID)
        {
            bool blnIsDiscountForAmount = false;
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 15));
            alParameters.Add(new SqlParameter("@DiscountID", intDiscountID));
            SqlDataReader sdr = objclsConnection.ExecuteReader("spInvPurchaseModuleFunctions", alParameters);
            if (sdr.Read())
            {
                if (sdr["IsDiscountForAmount"] != DBNull.Value)
                    blnIsDiscountForAmount = Convert.ToBoolean(sdr["IsDiscountForAmount"]);
            }
            sdr.Close();
            return blnIsDiscountForAmount;
        }

        public double GetItemDiscountAmount(int iMode, int iItemID, int DiscountID, double Rate)
        {

            double dOutItemNetAmount = 0;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 3));
            parameters.Add(new SqlParameter("@ItemID", iItemID));
            parameters.Add(new SqlParameter("@DiscountID", DiscountID));
            parameters.Add(new SqlParameter("@Rate", Rate));

            SqlDataReader sReader = objclsConnection.ExecuteReader("spInvPurchaseModuleFunctions", parameters, CommandBehavior.SingleRow);
            if (sReader.Read())
            {
                if (sReader[0] != DBNull.Value)
                {
                    dOutItemNetAmount = Convert.ToDouble(sReader[0]);
                }
                else
                {
                    dOutItemNetAmount = 0;
                }


                if (dOutItemNetAmount >= 0)
                {
                    sReader.Close();
                    return dOutItemNetAmount;
                }
            }
            sReader.Close();
            return dOutItemNetAmount;

        }
        public decimal GetExchangeCurrencyRate(int intCompanyID, int intCurrencyID)
        {
            ArrayList prmExchangeCurrencyRate = new ArrayList();
            prmExchangeCurrencyRate.Add(new SqlParameter("@Mode", 19));
            prmExchangeCurrencyRate.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmExchangeCurrencyRate.Add(new SqlParameter("@CurrencyID", intCurrencyID));

            DataTable datExchangeCurrencyRate = objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmExchangeCurrencyRate);

            if (datExchangeCurrencyRate.Rows.Count > 0)
            {
                if (datExchangeCurrencyRate.Rows[0]["Exchangerate"] != DBNull.Value)
                    return Convert.ToDecimal(datExchangeCurrencyRate.Rows[0]["Exchangerate"]);
            }

            return 0;
        }

        public string GetCompanyCurrency(int intCompanyID, out int intScale)
        {
            intScale = 2;
            string strCurrency = "";
            ArrayList prmCurrency = new ArrayList();
            prmCurrency.Add(new SqlParameter("@Mode", 20));
            prmCurrency.Add(new SqlParameter("@CompanyID", intCompanyID));
            SqlDataReader sdr = objclsConnection.ExecuteReader("spInvPurchaseModuleFunctions", prmCurrency);
            if (sdr.Read())
            {
                if (sdr["Currency"] != DBNull.Value)
                {
                    strCurrency = Convert.ToString(sdr["Currency"]);
                }
                if (sdr["Scale"] != DBNull.Value)
                {
                    intScale = Convert.ToInt32(sdr["Scale"]);
                }
            }
            return strCurrency;
        }
        public string GetVendorAddress(int intReferenceID,bool blnIsVendorID,out string strAddressName)
        {
            if (blnIsVendorID)
            {
                DataTable datVendor = FillCombos(new string[] { "VendorAddID", "InvVendorAddress", "VendorID = " + intReferenceID });
                if (datVendor.Rows.Count > 0)
                {
                    intReferenceID = Convert.ToInt32(datVendor.Rows[0]["VendorAddID"]);
                }
            }
            string sAddress = "";
            sAddress += Environment.NewLine;
            ArrayList prmVendor = new ArrayList();

            prmVendor.Add(new SqlParameter("@Mode", 11));
            prmVendor.Add(new SqlParameter("@VendorAddID", intReferenceID));
            SqlDataReader drVendor = objclsConnection.ExecuteReader("spInvPurchaseReturn", prmVendor, CommandBehavior.SingleRow);
            strAddressName = "Address";
            if (drVendor.Read())
            {
                sAddress = Environment.NewLine;
                if(!string.IsNullOrEmpty(Convert.ToString(drVendor["AddressName"])))
                    strAddressName = Convert.ToString(drVendor["AddressName"]);

                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["ContactPerson"])))
                {
                    sAddress += Convert.ToString(drVendor["ContactPerson"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Address"])))
                {
                    sAddress += Convert.ToString(drVendor["Address"]);
                    sAddress += Environment.NewLine;
                }

                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["State"])))
                {
                    sAddress += Convert.ToString(drVendor["State"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Country"])))
                {
                    sAddress += Convert.ToString(drVendor["Country"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["ZipCode"])))
                {
                    sAddress += "ZipCode : " + Convert.ToString(drVendor["ZipCode"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Telephone"])))
                {
                    sAddress += "Telephone : " + Convert.ToString(drVendor["Telephone"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["MobileNo"])))
                {
                    sAddress += "MobileNo : " + Convert.ToString(drVendor["MobileNo"]);
                    sAddress += Environment.NewLine;
                }
                drVendor.Close();
                return sAddress;
            }
            else
            {
                drVendor.Close();
                return sAddress;
            }
        }

        public bool GetStockDetailsQuantity(int intItemID, long intBatchID, int intWarehouseID,out decimal decGRNQuantity,out decimal decInvoicedQuantity)
        {
            decGRNQuantity = 0; decInvoicedQuantity = 0;
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 12));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", intBatchID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@CompanyID", objclsDTOPurchaseReturnMaster.intCompanyID));
            DataTable datTemp = objclsConnection.ExecuteDataTable("spInvPurchaseReturn", alParameters);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    decGRNQuantity = Convert.ToDecimal(datTemp.Rows[0]["GRNQuantity"].ToString());
                    decInvoicedQuantity = Convert.ToDecimal(datTemp.Rows[0]["InvoicedQuantity"].ToString());
                }
            }
            //SqlDataReader sdr = objclsConnection.ExecuteReader("spInvPurchaseReturn", alParameters);
            //if (sdr.Read())
            //{
            //    if (sdr["GRNQuantity"] != DBNull.Value)
            //        decGRNQuantity = Convert.ToDecimal(sdr["GRNQuantity"]);
            //    if (sdr["InvoicedQuantity"] != DBNull.Value)
            //        decInvoicedQuantity = Convert.ToDecimal(sdr["InvoicedQuantity"]);
            //}
            //sdr.Close();
            return true;
        }

        public bool GetStockMasterQuantity(int intItemID,out decimal decGRNQuantity, out decimal decInvoicedQuantity)
        {
            decGRNQuantity = 0; decInvoicedQuantity = 0;
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 13));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            DataTable datTemp = objclsConnection.ExecuteDataTable("spInvPurchaseReturn", alParameters);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    decGRNQuantity = Convert.ToDecimal(datTemp.Rows[0]["GRNQuantity"].ToString());
                    decInvoicedQuantity = Convert.ToDecimal(datTemp.Rows[0]["InvoicedQuantity"].ToString());
                }
            }
            //SqlDataReader sdr = objclsConnection.ExecuteReader("spInvPurchaseReturn", alParameters);
            //if (sdr.Read())
            //{
            //    if (sdr["GRNQuantity"] != DBNull.Value)
            //        decGRNQuantity = Convert.ToDecimal(sdr["GRNQuantity"]);
            //    if (sdr["InvoicedQuantity"] != DBNull.Value)
            //        decInvoicedQuantity = Convert.ToDecimal(sdr["InvoicedQuantity"]);
            //}
            //sdr.Close();
            return true;
        }

        public bool UpdateStockDetailsQuantity(int intItemID, long intBatchID, int intWarehouseID, decimal decGRNQuantity, decimal decInvoicedQuantity)
        {
            decimal decReceivedFromTransfer = 0, decTransferedQty = 0, decDamagedQty = 0, decDemoQty = 0, decSoldQty = 0, decOpeningStock = 0;
            DataTable datStockdetails = new DataTable();
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 4));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", intBatchID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@CompanyID", objclsDTOPurchaseReturnMaster.intCompanyID));
            datStockdetails = objclsConnection.ExecuteDataTable("spInvStockMaster", alParameters);
            if(datStockdetails.Rows.Count >0)
            {
                decReceivedFromTransfer = datStockdetails.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                decOpeningStock = datStockdetails.Rows[0]["OpeningStock"].ToDecimal();
                decSoldQty = datStockdetails.Rows[0]["SoldQuantity"].ToDecimal();
                decTransferedQty = datStockdetails.Rows[0]["TransferedQuantity"].ToDecimal();
                decDamagedQty = datStockdetails.Rows[0]["DamagedQuantity"].ToDecimal();
                decDemoQty = datStockdetails.Rows[0]["DemoQuantity"].ToDecimal();
            }
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 6));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", intBatchID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@CompanyID", objclsDTOPurchaseReturnMaster.intCompanyID));
            alParameters.Add(new SqlParameter("@GRNQuantity", decGRNQuantity));
            alParameters.Add(new SqlParameter("@InvoicedQuantity", decInvoicedQuantity));
            alParameters.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
            alParameters.Add(new SqlParameter("@OpeningStock", decOpeningStock));
            alParameters.Add(new SqlParameter("@SoldQuantity", decSoldQty));
            alParameters.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));
            alParameters.Add(new SqlParameter("@DemoQuantity", decDemoQty));
            alParameters.Add(new SqlParameter("@DamagedQuantity", decDamagedQty));
            objclsConnection.ExecuteNonQueryWithTran("spInvStockMaster", alParameters);
            return true;
        }

        public bool UpdateStockMasterQuantity(int intItemID,decimal decGRNQuantity, decimal decInvoicedQuantity)
        {
            decimal decReceivedFromTransfer = 0, decTransferedQty = 0, decDamagedQty = 0, decDemoQty = 0, decSoldQty = 0, decOpeningStock = 0;
            DataTable datStockdetails = new DataTable();
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            datStockdetails = objclsConnection.ExecuteDataTable("spInvStockMaster", alParameters);
            if (datStockdetails.Rows.Count > 0)
            {
                decReceivedFromTransfer = datStockdetails.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                decOpeningStock = datStockdetails.Rows[0]["OpeningStock"].ToDecimal();
                decSoldQty = datStockdetails.Rows[0]["SoldQuantity"].ToDecimal();
                decTransferedQty = datStockdetails.Rows[0]["TransferedQuantity"].ToDecimal();
                decDamagedQty = datStockdetails.Rows[0]["DamagedQuantity"].ToDecimal();
                decDemoQty = datStockdetails.Rows[0]["DemoQuantity"].ToDecimal();
            }
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 3));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@GRNQuantity", decGRNQuantity));
            alParameters.Add(new SqlParameter("@InvoicedQuantity", decInvoicedQuantity));
            alParameters.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
            alParameters.Add(new SqlParameter("@OpeningStock", decOpeningStock));
            alParameters.Add(new SqlParameter("@SoldQuantity", decSoldQty));
            alParameters.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));
            alParameters.Add(new SqlParameter("@DemoQuantity", decDemoQty));
            alParameters.Add(new SqlParameter("@DamagedQuantity", decDamagedQty));
            alParameters.Add(new SqlParameter("@LastModifiedBy", objclsDTOPurchaseReturnMaster.intCreatedBy));
            alParameters.Add(new SqlParameter("@LastModifiedDate", ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")));
            objclsConnection.ExecuteNonQueryWithTran("spInvStockMaster", alParameters);
            return true;
        }

        public decimal ConvertQtyToBaseUnitQty(int intUomID, int intItemId, decimal decQuantity)
        {
            DataTable dtUomDetails = GetUomConversionValues(intUomID, intItemId);
            if (dtUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                    decQuantity = decQuantity / decConversionValue;
                else if (intConversionFactor == 2)
                    decQuantity = decQuantity * decConversionValue;
            }
            return decQuantity;
        }

        private DataTable DisplayCancellationDetails()
        {
            //function for displaying cancellation details

            ArrayList prmPurchaseReturn = new ArrayList();
            prmPurchaseReturn.Add(new SqlParameter("Mode", 18));
            prmPurchaseReturn.Add(new SqlParameter("PurchaseReturnID", objclsDTOPurchaseReturnMaster.intPurchaseReturnID));
            return objclsConnection.ExecuteDataTable("spInvPurchaseReturn", prmPurchaseReturn);
        }

        public void SaveCancellationDetails()
        {
            // function for saving cancellation details

            ArrayList prmPurchaseReturn = new ArrayList();

            prmPurchaseReturn.Add(new SqlParameter("CreatedBy", objclsDTOPurchaseReturnMaster.intCreatedBy));
            prmPurchaseReturn.Add(new SqlParameter("CancellationDate", objclsDTOPurchaseReturnMaster.strCancellationDate));
            prmPurchaseReturn.Add(new SqlParameter("Description", objclsDTOPurchaseReturnMaster.strCancellationDescription));
            prmPurchaseReturn.Add(new SqlParameter("CompanyID", objclsDTOPurchaseReturnMaster.intCompanyID));
            prmPurchaseReturn.Add(new SqlParameter("Mode", 16));
            prmPurchaseReturn.Add(new SqlParameter("PurchaseReturnID", objclsDTOPurchaseReturnMaster.intPurchaseReturnID));
            objclsConnection.ExecuteNonQueryWithTran("spInvPurchaseReturn", prmPurchaseReturn);
        }

        private void DeleteCancellationDetails()
        {
            // function for deleting cancellation details
            ArrayList prmPurchaseReturn = new ArrayList();
            prmPurchaseReturn.Add(new SqlParameter("Mode", 17));
            prmPurchaseReturn.Add(new SqlParameter("PurchaseReturnID", objclsDTOPurchaseReturnMaster.intPurchaseReturnID));
            objclsConnection.ExecuteNonQueryWithTran("spInvPurchaseReturn", prmPurchaseReturn);
        }
               
        public DataSet GetPurchaseReturnReport()//email
        {
            ArrayList prmPurchaseReturn = new ArrayList();
            prmPurchaseReturn.Add(new SqlParameter("@Mode", 19));
            prmPurchaseReturn.Add(new SqlParameter("@PurchaseReturnID", objclsDTOPurchaseReturnMaster.intPurchaseReturnID));
            return objclsConnection.ExecuteDataSet("spInvPurchaseReturn", prmPurchaseReturn);
        }

        private bool SavePurchaseReturnLocationDetails()
        {

            int intSerialNo = 0;
            foreach (clsDTOPurchaseReturnLocationDetails objLocationDetails in objclsDTOPurchaseReturnMaster.lstLocationDetails)
            {
                ArrayList alParameters = new ArrayList();

                alParameters.Add(new SqlParameter("@Mode", 19));
                alParameters.Add(new SqlParameter("@PurchaseReturnID", objclsDTOPurchaseReturnMaster.intPurchaseReturnID));

                alParameters.Add(new SqlParameter("@SerialNo", ++intSerialNo));
                alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.intItemID));
                alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.lngBatchID));
                alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.intLocationID));
                alParameters.Add(new SqlParameter("@RowID", objLocationDetails.intRowID));
                alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.intBlockID));
                alParameters.Add(new SqlParameter("@LotID", objLocationDetails.intLotID));
                alParameters.Add(new SqlParameter("@UOMID", objLocationDetails.intUOMID));
                alParameters.Add(new SqlParameter("@ReturnQuantity", objLocationDetails.decQuantity));
                DataTable dtGetUomDetails = GetUomConversionValues(objLocationDetails.intUOMID, objLocationDetails.intItemID);
                decimal decQuantity = objLocationDetails.decQuantity;
                if (dtGetUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                        objLocationDetails.decQuantity = objLocationDetails.decQuantity / decConversionValue;
                    else if (intConversionFactor == 2)
                        objLocationDetails.decQuantity = objLocationDetails.decQuantity * decConversionValue;
                }
                if(objclsDTOPurchaseReturnMaster.intStatusID != (int)OperationStatusType.PReturnCancelled)
                UpdateItemLocationDetails(objLocationDetails, false);
                objLocationDetails.decQuantity = decQuantity;

                objclsConnection.ExecuteNonQueryWithTran("spInvPurchaseReturn", alParameters);
            }
            return true;
        }

        private bool UpdateItemLocationDetails(clsDTOPurchaseReturnLocationDetails objLocationDetails, bool blnIsDeletion)
        {
            ArrayList alParameters = new ArrayList();
            if (!blnIsDeletion)
                alParameters.Add(new SqlParameter("@Mode", 2));
            else
                alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@Type", 1));
            alParameters.Add(new SqlParameter("@CompanyID", objclsDTOPurchaseReturnMaster.intCompanyID));
            alParameters.Add(new SqlParameter("@WarehouseID", objclsDTOPurchaseReturnMaster.intWarehouseID));
            alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.intItemID));
            alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.lngBatchID));
            alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.intLocationID));
            alParameters.Add(new SqlParameter("@RowID", objLocationDetails.intRowID));
            alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.intBlockID));
            alParameters.Add(new SqlParameter("@LotID", objLocationDetails.intLotID));
            alParameters.Add(new SqlParameter("@Quantity", objLocationDetails.decQuantity));
            objclsConnection.ExecuteNonQueryWithTran("spInvItemLocationTransfer", alParameters);
            return true;
        }

        public decimal GetItemLocationQuantity(clsDTOPurchaseReturnLocationDetails objLocationDetails)
        {
            ArrayList alParameters = new ArrayList();
            decimal decQuantity = 0;
            alParameters.Add(new SqlParameter("@Mode", 13));
            alParameters.Add(new SqlParameter("@CompanyID", objclsDTOPurchaseReturnMaster.intCompanyID));
            alParameters.Add(new SqlParameter("@WarehouseID", objclsDTOPurchaseReturnMaster.intWarehouseID));
            alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.intItemID));
            alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.lngBatchID));
            alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.intLocationID));
            alParameters.Add(new SqlParameter("@RowID", objLocationDetails.intRowID));
            alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.intBlockID));
            alParameters.Add(new SqlParameter("@LotID", objLocationDetails.intLotID));
            SqlDataReader sdrLocationDetails = objclsConnection.ExecuteReader("spInvItemLocationTransfer", alParameters);
            if (sdrLocationDetails.Read())
            {
                if (sdrLocationDetails["Quantity"] != DBNull.Value)
                    decQuantity = sdrLocationDetails["Quantity"].ToDecimal();
                sdrLocationDetails.Close();
            }
            return decQuantity;
        }

        public DataTable GetPurchaseReturnLocationDetails()
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 21));
            alParameters.Add(new SqlParameter("@PurchaseReturnID", objclsDTOPurchaseReturnMaster.intPurchaseReturnID));
            return objclsConnection.ExecuteDataTable("spInvPurchaseReturn", alParameters);
        }

        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 14));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@LocationWise", ClsCommonSettings.PblnIsLocationWise));
            return objclsConnection.ExecuteDataTable("spInvItemLocationTransfer", alParameters).Rows[0];
        }

        private bool DeletePurchaseReturnLocationDetails()
        {
            DataTable datPurchaseReturnLocationDetails = GetPurchaseReturnLocationDetails();
            int intOldStatusID = FillCombos(new string[] {"StatusID","InvPurchaseReturnMaster","PurchaseReturnID = "+objclsDTOPurchaseReturnMaster.intPurchaseReturnID}).Rows[0]["StatusID"].ToInt32();
            foreach (DataRow dr in datPurchaseReturnLocationDetails.Rows)
            {
                clsDTOPurchaseReturnLocationDetails objLocationDetails = new clsDTOPurchaseReturnLocationDetails();
                objLocationDetails.intItemID = dr["ItemID"].ToInt32();
                objLocationDetails.lngBatchID = dr["BatchID"].ToInt64();
                objLocationDetails.intLocationID = dr["LocationID"].ToInt32();
                objLocationDetails.intRowID = dr["RowID"].ToInt32();
                objLocationDetails.intBlockID = dr["BlockID"].ToInt32();
                objLocationDetails.intLotID = dr["LotID"].ToInt32();
                objLocationDetails.decQuantity = dr["Quantity"].ToDecimal();
                objLocationDetails.intUOMID = dr["UomID"].ToInt32();

                DataTable dtGetUomDetails = GetUomConversionValues(objLocationDetails.intUOMID, objLocationDetails.intItemID);
                decimal decQuantity = objLocationDetails.decQuantity;
                if (dtGetUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                        objLocationDetails.decQuantity = objLocationDetails.decQuantity / decConversionValue;
                    else if (intConversionFactor == 2)
                        objLocationDetails.decQuantity = objLocationDetails.decQuantity * decConversionValue;
                }

                if (intOldStatusID  != (int)OperationStatusType.PReturnCancelled)
                UpdateItemLocationDetails(objLocationDetails, true);
            }
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 20));
            alParameters.Add(new SqlParameter("@PurchaseReturnID", objclsDTOPurchaseReturnMaster.intPurchaseReturnID));
            objclsConnection.ExecuteNonQueryWithTran("spInvPurchaseReturn", alParameters);
            return true;
        }

        public decimal GetStockQuantity(int intItemID, Int64 intBatchID, int intCompanyID, int intWarehouseID)
        {
            ArrayList prmStockDetails = new ArrayList();
            prmStockDetails.Add(new SqlParameter("@Mode", 8));
            prmStockDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmStockDetails.Add(new SqlParameter("@BatchID", intBatchID));
            prmStockDetails.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            prmStockDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            SqlDataReader sdr = objclsConnection.ExecuteReader("spInvStockMaster", prmStockDetails);
            decimal decQuantity = 0;
            if (sdr.Read())
            {
                if (sdr["AvailableQty"] != DBNull.Value)
                {
                    decQuantity = Convert.ToDecimal(sdr["AvailableQty"]);
                }
            }
            sdr.Close();
            return decQuantity;
        }

        public decimal GetInvoiceReceivedQty(long lngPurchaseInvoiceID, int intItemId, long lngBatchID)
        {
            decimal decReceivedQty = 0;
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 22));
            alParameters.Add(new SqlParameter("@PurchaseInvoiceID", lngPurchaseInvoiceID));
            alParameters.Add(new SqlParameter("@ItemID", intItemId));
            alParameters.Add(new SqlParameter("@BatchID", lngBatchID));
            SqlDataReader sdr = objclsConnection.ExecuteReader("spInvPurchaseReturn", alParameters);
            if (sdr.Read())
            {
                if (sdr["ReceivedQty"] != DBNull.Value)
                    decReceivedQty = sdr["ReceivedQty"].ToDecimal();
                sdr.Close();
            }
            return decReceivedQty;
        }
        public DataSet GetDebitNoteReport()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 23));
            prmReportDetails.Add(new SqlParameter("@PurchaseReturnID",objclsDTOPurchaseReturnMaster.intPurchaseReturnID));
            return objclsConnection.ExecuteDataSet("spInvPurchaseReturn", prmReportDetails);
        }
        public DataSet DisplayDebitNoteLocationReport()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 24));
            prmReportDetails.Add(new SqlParameter("@PurchaseReturnID", objclsDTOPurchaseReturnMaster.intPurchaseReturnID));
            return objclsConnection.ExecuteDataSet("spInvPurchaseReturn", prmReportDetails);
        }
        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable("STPermissionSettings", parameters);
        }
        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable("STPermissionSettings", parameters);
        }

        public string GetAlreadyReturnedQty(long lngPurchaseInvoiceID, int intReferenceSerialNo)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 25));
            alParameters.Add(new SqlParameter("@PurchaseInvoiceID", lngPurchaseInvoiceID));
            alParameters.Add(new SqlParameter("@ReferenceSerialNo", intReferenceSerialNo));
            DataTable datReturnDetails = objclsConnection.ExecuteDataTable("spInvPurchaseReturn", alParameters);
            if (datReturnDetails.Rows.Count > 0)
            {
                return datReturnDetails.Rows[0]["ReturnedQty"].ToString();
            }
            else
            {
                return "";
            }
        }

        public decimal GetAlreadyReturnedQtyInBaseUOM(long lngPurchaseInvoiceID, int intReferenceSerialNo) // quantity in base uom
        {

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 28));
            alParameters.Add(new SqlParameter("@PurchaseInvoiceID", lngPurchaseInvoiceID));
            alParameters.Add(new SqlParameter("@ReferenceSerialNo", intReferenceSerialNo));
            DataTable datReturnDetails = objclsConnection.ExecuteDataTable("spInvPurchaseReturn", alParameters);
            if (datReturnDetails.Rows.Count > 0)
                return datReturnDetails.Rows[0]["ReturnedQty"].ToDecimal();
            else
                return 0;
        }
        public bool GetCompanyAccount(int intTransactionType, int intCompanyID)
        {
            // function for Geting Company Account
            ArrayList prmPurchase = new ArrayList();
            object objOutAccountID = 0;
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 18));
            prmPurchase.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPurchase.Add(new SqlParameter("@TransactionType", intTransactionType));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPurchase.Add(objParam);

            if (objclsConnection.ExecuteNonQuery("spInvPurchaseModuleFunctions", prmPurchase, out objOutAccountID) != 0)
            {
                ClsCommonSettings.PintSIDebitHeadID = (int)objOutAccountID;
                if ((int)objOutAccountID > 0) return true;
            }
            return false;
        }

        public decimal GetExtraItemQty(long lngPurchaseInvoiceID, int intItemId, long lngBatchID)
        {
            decimal decExtraQty = 0;
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 27));
            alParameters.Add(new SqlParameter("@PurchaseInvoiceID", lngPurchaseInvoiceID));
            alParameters.Add(new SqlParameter("@ItemID", intItemId));
            alParameters.Add(new SqlParameter("@BatchID", lngBatchID));
            DataTable datDetails = objclsConnection.ExecuteDataTable("spInvPurchaseReturn", alParameters);
            if (datDetails.Rows.Count > 0)
            {
                if (datDetails.Rows[0]["ExtraQty"] != DBNull.Value)
                    decExtraQty = datDetails.Rows[0]["ExtraQty"].ToDecimal();
            }
            return decExtraQty;
        }
        public DataTable GetPurchaseInvoiceNo(int CompanyID, int VendorID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 29));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@VendorID", VendorID));
            DataTable datDetails = objclsConnection.ExecuteDataTable("spInvPurchaseReturn", alParameters);
            return datDetails;
        }

        public bool IsPurchaseReturnExists(int intCompanyID, string strPurchaseReturnNo)
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 30));
            Parameters.Add(new SqlParameter("@PurchaseReturnNo", strPurchaseReturnNo));
            Parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            object objReturnNo = objclsConnection.ExecuteScalar("spInvPurchaseReturn", Parameters);
            return objReturnNo.ToInt32() > 0;
        }
        public string GetReturnedQty(long lngPurchaseInvoiceID, long lngBatchID, int intReferenceSerialNo)
        {
            string strQty = "";
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 31));
            Parameters.Add(new SqlParameter("@PurchaseInvoiceID", lngPurchaseInvoiceID));
            Parameters.Add(new SqlParameter("@BatchID", lngBatchID));
            Parameters.Add(new SqlParameter("@ReferenceSerialNo", intReferenceSerialNo));

            DataTable dtReturnedQty = objclsConnection.ExecuteDataTable("spInvPurchaseReturn", Parameters);
            if(dtReturnedQty != null && dtReturnedQty.Rows.Count > 0)
                strQty = dtReturnedQty.Rows[0]["ReturnedQty"].ToString();

            return strQty;
        }
        public decimal GetDiscountAmount(long lngPurchaseInvoiceID, int intItemID, long lngBatchID)
        {
            decimal decDiscountAmount = 0;
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 33));
            Parameters.Add(new SqlParameter("@PurchaseInvoiceID", lngPurchaseInvoiceID));
            Parameters.Add(new SqlParameter("@ItemID", intItemID));
            Parameters.Add(new SqlParameter("@BatchID", lngBatchID));
            DataTable dtDiscount = objclsConnection.ExecuteDataTable("spInvPurchaseReturn", Parameters);
            if (dtDiscount != null && dtDiscount.Rows.Count > 0)
                decDiscountAmount = Convert.ToDecimal(dtDiscount.Rows[0]["DiscountAmount"]);
            return decDiscountAmount;
        }

        public decimal getTaxValue(int TaxScheme)
        {
            return this.objclsConnection.ExecuteScalar("Select AccValue From AccAccountMaster where AccountID='" + TaxScheme + "'").ToDecimal();
        }
       


        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (dat != null)
                dat.Dispose();

            if (alParameters != null)
                alParameters = null;
        }
        #endregion
    }
}