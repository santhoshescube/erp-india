﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using System.Data;
using System.Data.SqlClient;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,22 Feb 2011>
   Description:	<Description,,Purchase DAL Class>
================================================
*/
namespace MyBooksERP
{
    public class clsDALPurchaseInvoice : IDisposable
    {
        ClsCommonUtility MobjClsCommonUtility; // object of clsCommonUtility
        DataLayer MobjDataLayer;
        ArrayList prmPurchase, prmPurchaseDetails, prmLeadTime;
        bool blnIsOldStatusCancelled = false;
        //   bool blnUpdateLIFORate = false;
        bool blnPurchaseOrderMasterStatus = true;
        bool blnIsDelete = false;
     

        public clsDTOPurchaseInvoice PobjClsDTOPurchase { get; set; }

        public clsDALPurchaseInvoice(DataLayer objDataLayer)
        {
            MobjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MobjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MobjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }

        public Int64 GetLastPurchaseNo(int intFormType, bool blnIsNoGeneration, int intCompanyID)
        {
            // formtype 1- Quotation,2- Order ,3-GRN,4- Invoice,5-Material REturn
            Int64 intRetValue = 0;
            string strProcedureName = string.Empty;
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@CompanyID", intCompanyID));
            switch (intFormType)
            {
                case 1:
                    strProcedureName = "spInvPurchaseQuotation";

                    if (blnIsNoGeneration)
                    {
                        prmPurchase.Add(new SqlParameter("@Mode", 17));
                    }
                    else
                    {
                        prmPurchase.Add(new SqlParameter("@Mode", 18));
                    }
                    break;
                case 2:
                    strProcedureName = "spPurchaseOrder";
                    if (blnIsNoGeneration)
                    {
                        prmPurchase.Add(new SqlParameter("@Mode", 18));
                    }
                    else
                    {
                        prmPurchase.Add(new SqlParameter("@Mode", 19));
                    }
                    break;
                case 3:
                    strProcedureName = "spGRN";
                    if (blnIsNoGeneration)
                    {
                        prmPurchase.Add(new SqlParameter("@Mode", 16));
                    }
                    else
                    {
                        prmPurchase.Add(new SqlParameter("@Mode", 17));
                    }
                    break;
                case 4:
                    strProcedureName = "spPurchaseInvoice";
                    if (blnIsNoGeneration)
                    {
                        prmPurchase.Add(new SqlParameter("@Mode", 16));
                    }
                    else
                    {
                        prmPurchase.Add(new SqlParameter("@Mode", 17));
                    }
                    break;
                case 5:
                    //strProcedureName = "spGRN";
                    //if (blnIsNoGeneration)
                    //{
                    //    prmPurchase.Add(new SqlParameter("@Mode", 16));
                    //}
                    //else
                    //{
                    //    prmPurchase.Add(new SqlParameter("@Mode", 17));
                    //}
                    //prmPurchase.Add(new SqlParameter("@OrderTypeID", (int)OperationOrderType.GRNFromMaterialReturn));

                    break;

            }
            SqlDataReader sdr = MobjDataLayer.ExecuteReader(strProcedureName, prmPurchase);
            if (sdr.Read())
            {
                if (sdr["ReturnValue"] != DBNull.Value)
                    intRetValue = Convert.ToInt64(sdr["ReturnValue"]);
                else if (blnIsNoGeneration)
                    intRetValue = 0;
            }
            sdr.Close();
            return intRetValue;
        }

        public bool SavePurchaseOrder(bool AddStatus)// Purchase Order Save
        {
            object objOutPurchase = 0;
            int intOldStatusID = 0;

            prmPurchase = new ArrayList();
            if (AddStatus)
            {
                //InsertLeadTimeDetails();//insert lead time details
                prmPurchase.Add(new SqlParameter("@Mode", 2));
            }
            else
            {
                DeletePurchaseOrderDetails();
                DataTable datPurchase = FillCombos(new string[] { "StatusID", "InvPurchaseOrderMaster", "PurchaseOrderID = " + PobjClsDTOPurchase.intPurchaseID });
                if (datPurchase.Rows.Count > 0)
                {
                    intOldStatusID = datPurchase.Rows[0]["StatusID"].ToInt32();
                }
                // UpdateLeadTimeDetails();//update lead time details
                prmPurchase.Add(new SqlParameter("@Mode", 3));
            }

            prmPurchase.Add(new SqlParameter("@PurchaseOrderID", PobjClsDTOPurchase.intPurchaseID));
            prmPurchase.Add(new SqlParameter("@OrderTypeID", PobjClsDTOPurchase.intOrderType));
            prmPurchase.Add(new SqlParameter("@ReferenceID ", PobjClsDTOPurchase.intReferenceID));
            prmPurchase.Add(new SqlParameter("@PurchaseOrderNo", PobjClsDTOPurchase.strPurchaseNo));
            prmPurchase.Add(new SqlParameter("@OrderDate", PobjClsDTOPurchase.strDate));
            prmPurchase.Add(new SqlParameter("@VendorID", PobjClsDTOPurchase.intVendorID));
            prmPurchase.Add(new SqlParameter("@DueDate", PobjClsDTOPurchase.strDueDate));
            prmPurchase.Add(new SqlParameter("@VendorAddID", PobjClsDTOPurchase.intVendorAddID));
            prmPurchase.Add(new SqlParameter("@PaymentTermsID", PobjClsDTOPurchase.intPaymentTermsID));
            prmPurchase.Add(new SqlParameter("@IsGRNRequired", PobjClsDTOPurchase.blnGRNRequired));
            prmPurchase.Add(new SqlParameter("@Remarks", PobjClsDTOPurchase.strRemarks));
            if (PobjClsDTOPurchase.blnGrandDiscount == true)
            {
                prmPurchase.Add(new SqlParameter("@DiscountPercentage", PobjClsDTOPurchase.decGrandDiscountPercent));
                prmPurchase.Add(new SqlParameter("@DiscountAmount", 0.0));
                prmPurchase.Add(new SqlParameter("@IsDiscountPercentage", PobjClsDTOPurchase.blnGrandDiscount));

            }
            else if (PobjClsDTOPurchase.blnGrandDiscount == false)
            {
                prmPurchase.Add(new SqlParameter("@DiscountPercentage", 0.0));
                prmPurchase.Add(new SqlParameter("@GrandDiscountAmount", PobjClsDTOPurchase.decGrandDiscountAmt));
                prmPurchase.Add(new SqlParameter("@IsDiscountPercentage", PobjClsDTOPurchase.blnGrandDiscount));

            }

            prmPurchase.Add(new SqlParameter("@CurrencyID", PobjClsDTOPurchase.intCurrencyID));
            prmPurchase.Add(new SqlParameter("@ExpenseAmount", PobjClsDTOPurchase.decExpenseAmount));
            prmPurchase.Add(new SqlParameter("@GrandAmount", PobjClsDTOPurchase.decGrandAmount));
            prmPurchase.Add(new SqlParameter("@NetAmount", PobjClsDTOPurchase.decNetAmount));
            prmPurchase.Add(new SqlParameter("@CreatedBy", PobjClsDTOPurchase.intCreatedBy));
            prmPurchase.Add(new SqlParameter("@CreatedDate", PobjClsDTOPurchase.strCreatedDate));
            //if (PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.POrderApproved || PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.POrderRejected)
            //{
            //    prmPurchase.Add(new SqlParameter("@ApprovedBy", PobjClsDTOPurchase.intApprovedBy));
            //    prmPurchase.Add(new SqlParameter("@ApprovedDate", PobjClsDTOPurchase.strApprovedDate));
            //}
            prmPurchase.Add(new SqlParameter("@VerificationDescription", PobjClsDTOPurchase.strVerifcationDescription));
            if (PobjClsDTOPurchase.intVerificationCriteria > 0)
                prmPurchase.Add(new SqlParameter("@VerificationCriteriaID", PobjClsDTOPurchase.intVerificationCriteria));
            prmPurchase.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
            prmPurchase.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
            //if (PobjClsDTOPurchase.intIncoTerms != 0)
            //prmPurchase.Add(new SqlParameter("@IncoTermsID", PobjClsDTOPurchase.intIncoTerms));
            //prmPurchase.Add(new SqlParameter("@LeadTimeDetsID", PobjClsDTOPurchase.intLeadTimeDetsID));
            //if (PobjClsDTOPurchase.intContainerTypeID != 0)
            //prmPurchase.Add(new SqlParameter("@ContainerTypeID", PobjClsDTOPurchase.intContainerTypeID));
            //prmPurchase.Add(new SqlParameter("@NoOfContainers", PobjClsDTOPurchase.intNoOfContainers));


            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPurchase.Add(objParam);
            if (MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseOrder", prmPurchase, out objOutPurchase) != 0)
            {
                if (Convert.ToInt64(objOutPurchase) > 0)
                {
                    PobjClsDTOPurchase.intPurchaseID = Convert.ToInt64(objOutPurchase);

                    if (AddStatus)
                        SaveExpenses(1);

                    if (!PobjClsDTOPurchase.blnCancelled)
                    {
                        //if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.POrderFromIndent)
                        //    UpdateStatus(1, (Int32)OperationStatusType.PIndentClosed, PobjClsDTOPurchase.intReferenceID);
                        if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.POrderFromQuotation)
                        {
                            UpdateStatus(2, (Int32)OperationStatusType.PQuotationClosed, PobjClsDTOPurchase.intReferenceID);
                        }
                    }
                    else
                    {
                        //if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.POrderFromIndent)
                        //    UpdateStatus(1, (Int32)OperationStatusType.PIndentOpened, PobjClsDTOPurchase.intReferenceID);
                        if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.POrderFromQuotation)
                        {
                            if (ClsCommonSettings.PQApproval)
                                UpdateStatus(2, (Int32)OperationStatusType.PQuotationApproved, PobjClsDTOPurchase.intReferenceID);
                            else
                                UpdateStatus(2, (Int32)OperationStatusType.PQuotationSubmitted, PobjClsDTOPurchase.intReferenceID);
                        }
                    }
                    DeleteCancellationDetails(2);
                    if (PobjClsDTOPurchase.blnCancelled)
                    {
                        SaveCancellationDetails(2);
                    }
                    if (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection.Count > 0)
                    {
                        SaveOrderDetailInformation();
                    }
                    //if (PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderRejected && intOldStatusID != (int)OperationStatusType.POrderRejected)
                    if ((PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderRejected && intOldStatusID != PobjClsDTOPurchase.intStatusID) ||
                        (PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderApproved && intOldStatusID != PobjClsDTOPurchase.intStatusID) ||
                         (PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderSubmitted && intOldStatusID != PobjClsDTOPurchase.intStatusID) ||
                          (PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderSubmittedForApproval && intOldStatusID != PobjClsDTOPurchase.intStatusID))
                        VerificationHistoryTableUpdations(2, PobjClsDTOPurchase.intPurchaseID, false);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        private bool SaveOrderDetailInformation()// Purchase Order Detail Save
        {
            int intSerialNo = 0;
            foreach (clsDTOPurchaseDetails objClsDtoPurchaseDetails in PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection)
            {
                prmPurchaseDetails = new ArrayList();
                //  if (objClsDtoPurchaseDetails.intSerialNo == 0)
                prmPurchaseDetails.Add(new SqlParameter("@Mode", 9));
                //  else
                //    prmPurchaseDetails.Add(new SqlParameter("@Mode", 3));
                prmPurchaseDetails.Add(new SqlParameter("@SerialNo", ++intSerialNo));
                prmPurchaseDetails.Add(new SqlParameter("@PurchaseOrderID", PobjClsDTOPurchase.intPurchaseID));
                prmPurchaseDetails.Add(new SqlParameter("@ItemID", objClsDtoPurchaseDetails.intItemID));
                prmPurchaseDetails.Add(new SqlParameter("@Quantity", objClsDtoPurchaseDetails.decQuantity));
                prmPurchaseDetails.Add(new SqlParameter("@UOMID", objClsDtoPurchaseDetails.intUOMID));
                prmPurchaseDetails.Add(new SqlParameter("@Rate", objClsDtoPurchaseDetails.decRate));

                if (objClsDtoPurchaseDetails.blnDiscount == true)
                {
                    prmPurchaseDetails.Add(new SqlParameter("@DiscountPercentage", objClsDtoPurchaseDetails.decDiscountPercent));
                    prmPurchaseDetails.Add(new SqlParameter("@DiscountAmount", 0.0));
                    prmPurchaseDetails.Add(new SqlParameter("@IsDiscountPercentage", objClsDtoPurchaseDetails.blnDiscount));

                }
                else if (objClsDtoPurchaseDetails.blnDiscount == false)
                {
                    prmPurchaseDetails.Add(new SqlParameter("@DiscountPercentage", 0.0));
                    prmPurchaseDetails.Add(new SqlParameter("@GrandDiscountAmount", objClsDtoPurchaseDetails.decDiscountAmt));
                    prmPurchaseDetails.Add(new SqlParameter("@IsDiscountPercentage", objClsDtoPurchaseDetails.blnDiscount));

                }

                //if (objClsDtoPurchaseDetails.intDiscountID != 0)
                //{
                //    prmPurchaseDetails.Add(new SqlParameter("@DiscountID", objClsDtoPurchaseDetails.intDiscountID));
                //    prmPurchaseDetails.Add(new SqlParameter("@DiscountAmount", objClsDtoPurchaseDetails.decDiscountAmount));
                //}
                prmPurchaseDetails.Add(new SqlParameter("@GrandAmount", objClsDtoPurchaseDetails.decGrandAmount));
                prmPurchaseDetails.Add(new SqlParameter("@NetAmount", objClsDtoPurchaseDetails.decNetAmount));


                MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseOrder", prmPurchaseDetails);

            }
            return true;
        }

        public bool SavePurchaseInvoice(bool blnAddStatus, string strCondition, bool MblnIsFromCancellation, bool Reopen, bool blnIsExpense)// Purchase Invoice Save
        {
            // DeleteItemLocationDetails(PobjClsDTOPurchase.intPurchaseID, false);
            object objOutPurchase = 0;

            ArrayList parameters = new ArrayList();
            IsGRNRequired();
            if (blnAddStatus)
            {
                blnIsOldStatusCancelled = false;
                parameters.Add(new SqlParameter("@Mode", 2));
            }
            else
            {

                ArrayList prmPurOrder = new ArrayList();
                prmPurOrder.Add(new SqlParameter("@Mode", 1));
                prmPurOrder.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                DataTable datCompany = MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", prmPurOrder);

                if (datCompany.Rows.Count > 0)
                {
                    int intStatusId = -1;
                    if (datCompany.Rows[0]["StatusID"] != DBNull.Value) intStatusId = Convert.ToInt32(datCompany.Rows[0]["StatusID"]);

                    if (PobjClsDTOPurchase.blnCancelled && intStatusId == PobjClsDTOPurchase.intStatusID)
                        blnIsOldStatusCancelled = true;
                    else if (!PobjClsDTOPurchase.blnCancelled && intStatusId != PobjClsDTOPurchase.intStatusID)
                        blnIsOldStatusCancelled = true;

                }
                parameters.Add(new SqlParameter("@Mode", 3));
            }

            if (!blnAddStatus)
            {
                int intOrderTypeID = 0;
                if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                    intOrderTypeID = (MobjClsCommonUtility.FillCombos(new string[] { "POM.OrderTypeID", "InvPurchaseInvoiceMaster PIM INNER JOIN InvPurchaseInvoiceReferenceDetails PIRD ON PIRD.PurchaseInvoiceID = PIM.PurchaseInvoiceID LEFT JOIN InvPurchaseOrderMaster POM ON POM.PurchaseOrderID = PIRD.ReferenceID", "PIM.PurchaseInvoiceID = " + PobjClsDTOPurchase.intPurchaseID })).Rows[0]["OrderTypeID"].ToInt32();

                if ((PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder && intOrderTypeID == (int)OperationOrderType.POrderFromGRN) || PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
                {
                    DeleteGRNSummary();
                }
            }

            parameters.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
            parameters.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID));
            //parameters.Add(new SqlParameter("@PurchaseOrderID ", PobjClsDTOPurchase.intPurchaseOrderID));
            parameters.Add(new SqlParameter("@PurchaseInvoiceNo", PobjClsDTOPurchase.strPurchaseNo));
            parameters.Add(new SqlParameter("@PurchaseBillNo", PobjClsDTOPurchase.strPurchaseBillNo));
            parameters.Add(new SqlParameter("@InvoiceDate", PobjClsDTOPurchase.strDate));
            parameters.Add(new SqlParameter("@OrderTypeID", PobjClsDTOPurchase.intOrderType));
            parameters.Add(new SqlParameter("@IsGRNRequired", PobjClsDTOPurchase.blnGRNRequired));
            parameters.Add(new SqlParameter("@CurrencyID", PobjClsDTOPurchase.intCurrencyID));
            parameters.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
            parameters.Add(new SqlParameter("@VendorID", PobjClsDTOPurchase.intVendorID));
            parameters.Add(new SqlParameter("@TaxSchemeID", PobjClsDTOPurchase.intTaxSchemeID));
            parameters.Add(new SqlParameter("@DueDate", PobjClsDTOPurchase.strDueDate));
            parameters.Add(new SqlParameter("@VendorAddID", PobjClsDTOPurchase.intVendorAddID));
            parameters.Add(new SqlParameter("@PaymentTermsID", PobjClsDTOPurchase.intPaymentTermsID));
            parameters.Add(new SqlParameter("@Remarks", PobjClsDTOPurchase.strRemarks));
            parameters.Add(new SqlParameter("@AccountID", PobjClsDTOPurchase.intPurchaseAccountID));
            parameters.Add(new SqlParameter("@IsReopen", Reopen));

            if (PobjClsDTOPurchase.blnGrandDiscount == true)
            {
                parameters.Add(new SqlParameter("@DiscountPercentage", PobjClsDTOPurchase.decGrandDiscountPercent));
                parameters.Add(new SqlParameter("@GrandDiscountAmount", 0.0));
                parameters.Add(new SqlParameter("@IsDiscountPercentage", PobjClsDTOPurchase.blnGrandDiscount));

            }
            else if (PobjClsDTOPurchase.blnGrandDiscount == false)
            {
                parameters.Add(new SqlParameter("@DiscountPercentage", 0.0));
                parameters.Add(new SqlParameter("@GrandDiscountAmount", PobjClsDTOPurchase.decGrandDiscountAmt));
                parameters.Add(new SqlParameter("@IsDiscountPercentage", PobjClsDTOPurchase.blnGrandDiscount));

            }
            parameters.Add(new SqlParameter("@ExpenseAmount", PobjClsDTOPurchase.decExpenseAmount));
            parameters.Add(new SqlParameter("@NetAmount", PobjClsDTOPurchase.decNetAmount));
            parameters.Add(new SqlParameter("@GrandAmount", PobjClsDTOPurchase.decGrandAmount));
            parameters.Add(new SqlParameter("@TaxAmount", PobjClsDTOPurchase.decTaxAmount));
            parameters.Add(new SqlParameter("@NetAmountRounded", PobjClsDTOPurchase.decNetAmountRounded));
            parameters.Add(new SqlParameter("@CreatedBy", PobjClsDTOPurchase.intCreatedBy));
            parameters.Add(new SqlParameter("@CreatedDate", PobjClsDTOPurchase.strCreatedDate));
            parameters.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
            parameters.Add(new SqlParameter("@ExpensePosted", ClsMainSettings.ExpensePosted));
            parameters.Add(new SqlParameter("@CancellationDate", PobjClsDTOPurchase.strCancellationDate));
            parameters.Add(new SqlParameter("@Description", PobjClsDTOPurchase.strDescription));
            //parameters.Add(new SqlParameter("@AccountDate", PobjClsDTOPurchase.strAccountDate));
            //parameters.Add(new SqlParameter("@CreditHeadID", PobjClsDTOPurchase.intCreditHeadID));
            //parameters.Add(new SqlParameter("@DebitHeadID", PobjClsDTOPurchase.intDebitHeadID));
            //parameters.Add(new SqlParameter("@VoucherAmount", PobjClsDTOPurchase.decVoucherAmount));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            parameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));

            if (MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseInvoice", parameters, out objOutPurchase) != 0)
            {
                if (Convert.ToInt64(objOutPurchase) > 0)
                {
                    PobjClsDTOPurchase.intPurchaseID = Convert.ToInt64(objOutPurchase);

                    if (blnAddStatus)
                        SaveExpenses(2);
                    else
                    {
                        //if (blnIsOldStatusCancelled)
                        //    DeletePurchaseLocationDetails(false, true);
                        //else
                        //    DeletePurchaseLocationDetails(false, false);
                    }

                    DeleteCancellationDetails(4);
                    if (PobjClsDTOPurchase.blnCancelled)
                    {
                        SaveCancellationDetails(4);

                        //  DeleteItemLocationDetails(PobjClsDTOPurchase.intPurchaseID, false);
                    }
                    if (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection.Count > 0)
                    {

                        SavePurchaseInvoiceDetailInformation(blnAddStatus, PobjClsDTOPurchase.intPurchaseOrderID, strCondition, blnIsExpense);

                        //int value = CheckReferenceForBatchInfo(PobjClsDTOPurchase.intPurchaseID.ToInt32());

                        //if (PobjClsDTOPurchase.blnGRNRequired == false )
                        // No need to consider GRN Required or Not
                        SaveCostingMethodDetails(PobjClsDTOPurchase.intPurchaseID.ToInt32(), "PI", (Int32)OperationOrderType.GRNFromInvoice); // Saving Costing Method & Pricing Scheme Details

                        if (PobjClsDTOPurchase.blnCancelled)
                        {
                            //if (!PobjClsDTOPurchase.blnGRNRequired)
                            //    SavePurchaseLocationDetails(false, true);
                            if (ClsCommonSettings.POApproval)
                                UpdateStatus(3, (Int32)OperationStatusType.POrderApproved, PobjClsDTOPurchase.intPurchaseOrderID);
                            else
                                UpdateStatus(3, (Int32)OperationStatusType.POrderSubmitted, PobjClsDTOPurchase.intPurchaseOrderID);
                        }
                        else
                        {
                            //if (!PobjClsDTOPurchase.blnGRNRequired)
                            //    SavePurchaseLocationDetails(false, false);
                            if (GetPurchaseOrderStatus(PobjClsDTOPurchase.intPurchaseOrderID))
                            {
                                UpdateStatus(3, (Int32)OperationStatusType.POrderClosed, PobjClsDTOPurchase.intPurchaseOrderID);
                            }
                            else
                            {
                                if (ClsCommonSettings.POApproval)
                                    UpdateStatus(3, (Int32)OperationStatusType.POrderApproved, PobjClsDTOPurchase.intPurchaseOrderID);
                                else
                                    UpdateStatus(3, (Int32)OperationStatusType.POrderSubmitted, PobjClsDTOPurchase.intPurchaseOrderID);
                            }
                        }
                    }
                    if (PobjClsDTOPurchase.intPaymentTermsID == (int)PaymentTerms.Cash)//Reference Needed
                    {
                        if (PobjClsDTOPurchase.blnCancelled)
                            DeletePayments(PobjClsDTOPurchase.intPurchaseID);
                        else
                            SavePayments(PobjClsDTOPurchase.intPurchaseID);
                    }
                    GRNSummaryUpdation();
                    //  OpeningStockAccountUpdationForPurchaseInvoice(blnAddStatus);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        private bool SaveExpenses(int intType)
        {
            // 1 - Purchase Order , 2 - Purchase Invoice
            if (intType == 1)
            {
                if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.POrderFromQuotation)
                {
                    clsDALExpenseMaster objClsDALExpenseMaster = new clsDALExpenseMaster();
                    objClsDALExpenseMaster.objConnection = MobjDataLayer;
                    objClsDALExpenseMaster.objDTOExpenseMaster = new clsDTOExpenseMaster();
                    objClsDALExpenseMaster.objDTOExpenseMaster.intRowNumber = 1;
                    objClsDALExpenseMaster.objDTOExpenseMaster.blnPostToAccount = true;
                    if (objClsDALExpenseMaster.GetExpenseInfo((int)OperationType.PurchaseQuotation, PobjClsDTOPurchase.intReferenceID, 1))
                    {
                        objClsDALExpenseMaster.objDTOExpenseMaster.intOperationType = (int)OperationType.PurchaseOrder;
                        objClsDALExpenseMaster.objDTOExpenseMaster.lngReferenceID = PobjClsDTOPurchase.intPurchaseID;
                        objClsDALExpenseMaster.objDTOExpenseMaster.lngExpenseID = 0;
                        objClsDALExpenseMaster.objDTOExpenseMaster.blnPostToAccount = true;

                        foreach (clsDTOExpenseDetails objExpenseDetails in objClsDALExpenseMaster.objDTOExpenseMaster.objDTOExpenseDetails)
                        {
                            objExpenseDetails.intSerialNo = 0;
                            objExpenseDetails.intCreatedBy = ClsCommonSettings.UserID;
                        }

                        objClsDALExpenseMaster.SaveExpenseInfo();
                    }
                }
            }
            else
            {
                if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.PInvoiceFromOrder)
                {
                    DataTable dtInvoice = new clsBLLPurchase().FillCombos(new string[] {"PIM.PurchaseInvoiceID", "InvPurchaseInvoiceMaster PIM INNER JOIN InvPurchaseInvoiceReferenceDetails PIRD ON PIRD.PurchaseInvoiceID = PIM.PurchaseInvoiceID INNER JOIN  InvPurchaseOrderMaster POM ON POM.PurchaseOrderID = PIRD.ReferenceID", "PIM.OrderTypeID = 3 AND PIM.StatusID <> 19 AND POM.PurchaseOrderID =" + PobjClsDTOPurchase.intPurchaseOrderID});
                    if (dtInvoice != null && dtInvoice.Rows.Count == 0)
                    {
                        clsDALExpenseMaster objClsDALExpenseMaster = new clsDALExpenseMaster();
                        objClsDALExpenseMaster.objConnection = MobjDataLayer;
                        objClsDALExpenseMaster.objDTOExpenseMaster = new clsDTOExpenseMaster();
                        objClsDALExpenseMaster.objDTOExpenseMaster.intRowNumber = 1;
                        objClsDALExpenseMaster.objDTOExpenseMaster.blnPostToAccount = true;

                        if (objClsDALExpenseMaster.GetExpenseInfo((int)OperationType.PurchaseOrder, PobjClsDTOPurchase.intPurchaseOrderID, 1))
                        {
                            objClsDALExpenseMaster.objDTOExpenseMaster.intOperationType = (int)OperationType.PurchaseInvoice;
                            objClsDALExpenseMaster.objDTOExpenseMaster.lngReferenceID = PobjClsDTOPurchase.intPurchaseID;
                            objClsDALExpenseMaster.objDTOExpenseMaster.lngExpenseID = 0;
                            objClsDALExpenseMaster.objDTOExpenseMaster.blnPostToAccount = true;
                            foreach (clsDTOExpenseDetails objExpenseDetails in objClsDALExpenseMaster.objDTOExpenseMaster.objDTOExpenseDetails)
                            {
                                objExpenseDetails.intSerialNo = 0;
                                objExpenseDetails.intCreatedBy = ClsCommonSettings.UserID;
                            }

                            objClsDALExpenseMaster.SaveExpenseInfo();
                        }
                    }
                }
            }
            return true;
        }

        private bool SavePurchaseInvoiceDetailInformation(bool blnAddStatus, long ReferenceID, string strCondition, bool blnIsExpense)// Purchase Invoice Detail Save
        {
            ArrayList prmPurOrder;
            DataTable dtPurchaseInvoiceDetails = DisplayPurchaseInvoiceDetail(PobjClsDTOPurchase.intPurchaseID);
            if (!blnAddStatus)
            {
                if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.PInvoiceFromGRN && !PobjClsDTOPurchase.blnCancelled)
                {
                    bool result = DeleteOldDetails(PobjClsDTOPurchase.intPurchaseID, "", blnIsExpense);
                    blnAddStatus = true;
                }
                else
                {
                    if (blnIsOldStatusCancelled && PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PInvoiceCancelled)
                        DeletePurchaseInvoiceDetails((int)OperationStatusType.PInvoiceCancelled);
                    else
                        DeletePurchaseInvoiceDetails((int)OperationStatusType.PInvoiceOpened);

                    if (!PobjClsDTOPurchase.blnCancelled)
                    {
                        DeletePurchaseInvoiceReferenceDetails(PobjClsDTOPurchase.intPurchaseID);
                    }
                }
                
            }

            if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.PInvoiceFromGRN && !PobjClsDTOPurchase.blnCancelled)
            {

                SavePurchaseInvoiceReference(PobjClsDTOPurchase.intPurchaseID, strCondition);

                //DataTable DtReferenceDetails= GetSavedPurchaseInvoiceReference(PobjClsDTOPurchase.intPurchaseID);
                //foreach(DataRow dr in DtReferenceDetails.Rows)
                //{
                //    clsDTOPurchaseDetails objDTOPurchaseReferenceDetails = new clsDTOPurchaseDetails();
                //    objDTOPurchaseReferenceDetails.intItemID = Convert.ToInt32(DtReferenceDetails.Rows[0]["ItemID"]);
                //    objDTOPurchaseReferenceDetails.intBatchID = Convert.ToInt32(DtReferenceDetails.Rows[0]["BatchID"]);

                //    BatchUpdationFromGRN(objDTOPurchaseReferenceDetails);
                //}

            }

            List<int> lstupdateIndices = new List<int>();
            if (!blnAddStatus && !blnIsOldStatusCancelled)
            {
                if (PobjClsDTOPurchase.blnCancelled)
                {
                    for (int i = 0; i < dtPurchaseInvoiceDetails.Rows.Count; i++)
                    {
                        clsDTOPurchaseDetails objDTOPurchaseDetails = new clsDTOPurchaseDetails();
                        if (dtPurchaseInvoiceDetails.Rows[i]["BatchID"] != DBNull.Value)
                            objDTOPurchaseDetails.intBatchID = Convert.ToInt64(dtPurchaseInvoiceDetails.Rows[i]["BatchID"]);
                        objDTOPurchaseDetails.intItemID = Convert.ToInt32(dtPurchaseInvoiceDetails.Rows[i]["ItemID"]);

                        DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtPurchaseInvoiceDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtPurchaseInvoiceDetails.Rows[i]["ItemID"]));
                        decimal decOldQuantity = Convert.ToDecimal(dtPurchaseInvoiceDetails.Rows[i]["ReceivedQuantity"]);
                        decimal decOldExtraQuantity = Convert.ToDecimal(dtPurchaseInvoiceDetails.Rows[i]["ExtraQuantity"]);
                        if (dtGetUomDetails.Rows.Count > 0)
                        {
                            int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                            decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                            if (intConversionFactor == 1)
                            {
                                decOldQuantity = decOldQuantity / decConversionValue;
                                decOldExtraQuantity = decOldExtraQuantity / decConversionValue;
                            }
                            else if (intConversionFactor == 2)
                            {
                                decOldQuantity = decOldQuantity * decConversionValue;
                                decOldExtraQuantity = decOldExtraQuantity * decConversionValue;
                            }
                        }
                        int intTempWarehouseID = PobjClsDTOPurchase.intWarehouseID;

                        if (!PobjClsDTOPurchase.blnGRNRequired || PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN || CheckGRNExistingAgainstOrder())
                        {
                            if (PobjClsDTOPurchase.intTempWarehouseID != PobjClsDTOPurchase.intWarehouseID)
                            {
                                PobjClsDTOPurchase.intWarehouseID = PobjClsDTOPurchase.intTempWarehouseID;

                                if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
                                {
                                    StockDetailsUpdationGRN(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                }
                                else
                                {
                                    if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                                    {
                                        DataTable dtTemp = GetGRNBatchExistingAgainstGRN(objDTOPurchaseDetails.intItemID);
                                        if (dtTemp.Rows.Count > 0)
                                        {
                                            foreach (DataRow dr in dtTemp.Rows)
                                            {
                                                objDTOPurchaseDetails.intBatchID = dr["BatchID"].ToInt64();
                                                StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                            }
                                        }
                                        else
                                            StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                    }
                                    else 
                                    StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                }
                                PobjClsDTOPurchase.intWarehouseID = intTempWarehouseID;
                            }
                            else
                            {
                                if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
                                {
                                    StockDetailsUpdationGRN(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                }
                                else
                                {
                                    if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                                    {
                                        DataTable dtTemp = GetGRNBatchExistingAgainstGRN(objDTOPurchaseDetails.intItemID);
                                        if (dtTemp.Rows.Count > 0)
                                        {
                                            foreach (DataRow dr in dtTemp.Rows)
                                            {
                                                objDTOPurchaseDetails.intBatchID = dr["BatchID"].ToInt64();
                                                StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                            }
                                        }
                                        else
                                            StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);

                                    }
                                    else
                                    StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                }
                            }
                        }
                        StockMasterUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                    }
                }
                else
                {
                    for (int i = 0; i < dtPurchaseInvoiceDetails.Rows.Count; i++)
                    {
                        bool blnExists = false;
                        for (int j = 0; j < PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection.Count; j++)
                        {
                            if (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].strBatchNumber == dtPurchaseInvoiceDetails.Rows[i]["BatchNo"].ToString() && PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID == Convert.ToInt32(dtPurchaseInvoiceDetails.Rows[i]["ItemID"]))
                            {
                                PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intBatchID = FindBatchID(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].strBatchNumber);
                                PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intBatchID = BatchUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j]);
                                //if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.PInvoiceFromGRN && !PobjClsDTOPurchase.blnCancelled)
                                //{                                  
                                //    DataTable DtReferenceDetails = GetSavedPurchaseInvoiceReference(PobjClsDTOPurchase.intPurchaseID);
                                //    foreach (DataRow dr in DtReferenceDetails.Rows)
                                //    {
                                //        clsDTOPurchaseDetails objDTOPurchaseReferenceDetails = new clsDTOPurchaseDetails();
                                //        objDTOPurchaseReferenceDetails.intItemID = Convert.ToInt32(DtReferenceDetails.Rows[0]["ItemID"]);
                                //        objDTOPurchaseReferenceDetails.intBatchID = Convert.ToInt32(DtReferenceDetails.Rows[0]["BatchID"]);

                                //        BatchUpdationFromGRN(objDTOPurchaseReferenceDetails, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j]);
                                //    }

                                //}
                                DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtPurchaseInvoiceDetails.Rows[j]["UOMID"]), Convert.ToInt32(dtPurchaseInvoiceDetails.Rows[j]["ItemID"]));
                                decimal decOldQuantity = Convert.ToDecimal(dtPurchaseInvoiceDetails.Rows[j]["ReceivedQuantity"]);
                                decimal decOldExtraQty = Convert.ToDecimal(dtPurchaseInvoiceDetails.Rows[j]["ExtraQuantity"]);
                                if (dtGetUomDetails.Rows.Count > 0)
                                {
                                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                    if (intConversionFactor == 1)
                                    {
                                        decOldQuantity = decOldQuantity / decConversionValue;
                                        decOldExtraQty = decOldExtraQty / decConversionValue;
                                    }
                                    else if (intConversionFactor == 2)
                                    {
                                        decOldQuantity = decOldQuantity * decConversionValue;
                                        decOldExtraQty = decOldExtraQty * decConversionValue;
                                    }
                                }

                                StockMasterUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j], decOldQuantity, decOldExtraQty, false, false);

                                int intTempWarehouseID = PobjClsDTOPurchase.intWarehouseID;
                                if (!PobjClsDTOPurchase.blnGRNRequired || PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN || CheckGRNExistingAgainstOrder())
                                {
                                    if (PobjClsDTOPurchase.intTempWarehouseID != PobjClsDTOPurchase.intWarehouseID)
                                    {
                                        PobjClsDTOPurchase.intWarehouseID = PobjClsDTOPurchase.intTempWarehouseID;

                                        if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
                                        {
                                            StockDetailsUpdationGRN(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j], decOldQuantity, decOldExtraQty, false, true);
                                        }
                                        else
                                        {
                                            if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                                            {
                                                DataTable dtTemp = GetGRNBatchExistingAgainstGRN(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID);
                                                if (dtTemp.Rows.Count > 0)
                                                {
                                                    foreach (DataRow dr in dtTemp.Rows)
                                                    {
                                                        PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intBatchID = dr["BatchID"].ToInt64();
                                                        StockDetailsUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j], decOldQuantity, decOldExtraQty, false, true);
                                                    }
                                                }
                                                else
                                                    StockDetailsUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j], decOldQuantity, decOldExtraQty, false, true);
                                            }
                                            else
                                            StockDetailsUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j], decOldQuantity, decOldExtraQty, false, true);
                                        }
                                        PobjClsDTOPurchase.intWarehouseID = intTempWarehouseID;
                                        StockDetailsUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j], 0, 0, false, false);
                                    }
                                    else
                                    {
                                        if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
                                        {
                                            StockDetailsUpdationGRN(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j], decOldQuantity, decOldExtraQty, false, false);
                                        }
                                        else
                                        {
                                            if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                                            {
                                                DataTable dtTemp = GetGRNBatchExistingAgainstGRN(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID);
                                                if (dtTemp.Rows.Count > 0)
                                                {
                                                    foreach (DataRow dr in dtTemp.Rows)
                                                    {
                                                        PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intBatchID = dr["BatchID"].ToInt64();
                                                        StockDetailsUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j], decOldQuantity, decOldExtraQty, false, false);
                                                    }
                                                }
                                                else
                                                    StockDetailsUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j], decOldQuantity, decOldExtraQty, false, false);
                                            }
                                            else
                                            StockDetailsUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j], decOldQuantity, decOldExtraQty, false, false);
                                        }
                                    }
                                    // UpdateLastSaleRateOfItem(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decSaleRate, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intUOMID);
                                }
                                prmPurOrder = new ArrayList();

                                prmPurOrder.Add(new SqlParameter("@Mode", 8));
                                prmPurOrder.Add(new SqlParameter("@SerialNo", j + 1));
                                prmPurOrder.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                                prmPurOrder.Add(new SqlParameter("@ItemID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID));
                                prmPurOrder.Add(new SqlParameter("@OrderedQuantity", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decOrderQuantity));
                                prmPurOrder.Add(new SqlParameter("@ReceivedQuantity", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decQuantity));
                                prmPurOrder.Add(new SqlParameter("@ExtraQuantity", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decExtraQuantity));
                                prmPurOrder.Add(new SqlParameter("@UOMID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intUOMID));
                                prmPurOrder.Add(new SqlParameter("@BaseUnitQty", MobjClsCommonUtility.ConvertQtyToBaseUnitQty(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intUOMID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decQuantity + PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decExtraQuantity, 1)));
                                prmPurOrder.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                                prmPurOrder.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID));
                                if (!PobjClsDTOPurchase.blnGRNRequired)
                                    prmPurOrder.Add(new SqlParameter("@BatchID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intBatchID));
                                prmPurOrder.Add(new SqlParameter("@Rate", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decRate));

                                prmPurOrder.Add(new SqlParameter("@PurchaseRate", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decPurchaseRate));

                                //if (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intDiscountID != 0)
                                //{
                                //    prmPurOrder.Add(new SqlParameter("@DiscountID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intDiscountID));
                                //    prmPurOrder.Add(new SqlParameter("@DiscountAmount", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decDiscountAmount));
                                //}
                                if (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].blnDiscount == true)
                                {
                                    prmPurOrder.Add(new SqlParameter("@DiscountPercentage", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decDiscountPercent));
                                    prmPurOrder.Add(new SqlParameter("@DiscountAmount", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decDiscountAmt));
                                    prmPurOrder.Add(new SqlParameter("@IsDiscountPercentage", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].blnDiscount));

                                }
                                else if (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].blnDiscount == false)
                                {
                                    prmPurOrder.Add(new SqlParameter("@DiscountPercentage", 0.0));
                                    prmPurOrder.Add(new SqlParameter("@DiscountAmount", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decDiscountAmt));
                                    prmPurOrder.Add(new SqlParameter("@IsDiscountPercentage", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].blnDiscount));

                                }
                                prmPurOrder.Add(new SqlParameter("@GrandAmount", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decGrandAmount));
                                prmPurOrder.Add(new SqlParameter("@NetAmount", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decNetAmount));
                                prmPurOrder.Add(new SqlParameter("@ExpiryDate", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].strExpiryDate));
                                prmPurOrder.Add(new SqlParameter("@InvoiceDate", PobjClsDTOPurchase.strDate));
                                prmPurOrder.Add(new SqlParameter("@CurrencyID", PobjClsDTOPurchase.intCurrencyID));
                                prmPurOrder.Add(new SqlParameter("@IsGRNRequired", PobjClsDTOPurchase.blnGRNRequired));
                                prmPurOrder.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
                                prmPurOrder.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
                                prmPurOrder.Add(new SqlParameter("@LandingCost", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decLandingCost));
                                MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseInvoice", prmPurOrder);
                                lstupdateIndices.Add(j);
                                blnExists = true;

                                if (PobjClsDTOPurchase.intOrderType != (Int32)OperationOrderType.PInvoiceFromGRN)
                                {
                                    ArrayList prmPuchaseStatus = new ArrayList();
                                    prmPuchaseStatus.Add(new SqlParameter("@Mode", 38));
                                    prmPuchaseStatus.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                                    prmPuchaseStatus.Add(new SqlParameter("@ReferenceID", ReferenceID));
                                    prmPuchaseStatus.Add(new SqlParameter("@ItemID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID));
                                    prmPuchaseStatus.Add(new SqlParameter("@Quantity", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decQuantity));
                                    prmPuchaseStatus.Add(new SqlParameter("@BatchID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intBatchID));//no batchID Exists
                                    MobjDataLayer.ExecuteNonQuery("spPurchaseInvoice", prmPuchaseStatus);

                                }
                                break;
                            }

                        }
                        if (!blnExists)
                        {
                            clsDTOPurchaseDetails objDTOPurchaseDetails = new clsDTOPurchaseDetails();
                            if (dtPurchaseInvoiceDetails.Rows[i]["BatchID"] != DBNull.Value)
                                objDTOPurchaseDetails.intBatchID = Convert.ToInt64(dtPurchaseInvoiceDetails.Rows[i]["BatchID"]);
                            objDTOPurchaseDetails.intItemID = Convert.ToInt32(dtPurchaseInvoiceDetails.Rows[i]["ItemID"]);

                            DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtPurchaseInvoiceDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtPurchaseInvoiceDetails.Rows[i]["ItemID"]));
                            decimal decOldQuantity = Convert.ToDecimal(dtPurchaseInvoiceDetails.Rows[i]["ReceivedQuantity"]);
                            decimal decOldExtraQuantity = Convert.ToDecimal(dtPurchaseInvoiceDetails.Rows[i]["ExtraQuantity"]);
                            if (dtGetUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                {
                                    decOldQuantity = decOldQuantity / decConversionValue;
                                    decOldExtraQuantity = decOldExtraQuantity / decConversionValue;
                                }
                                else if (intConversionFactor == 2)
                                {
                                    decOldQuantity = decOldQuantity * decConversionValue;
                                    decOldExtraQuantity = decOldExtraQuantity * decConversionValue;
                                }
                            }
                            int intTempWarehouseID = PobjClsDTOPurchase.intWarehouseID;
                            if (!PobjClsDTOPurchase.blnGRNRequired || PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN || CheckGRNExistingAgainstOrder())
                            {
                                if (PobjClsDTOPurchase.intTempWarehouseID != PobjClsDTOPurchase.intWarehouseID)
                                {
                                    PobjClsDTOPurchase.intWarehouseID = PobjClsDTOPurchase.intTempWarehouseID;
                                    if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
                                    {
                                        StockDetailsUpdationGRN(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                    }
                                    else
                                    {
                                        if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                                        {
                                            DataTable dtTemp = GetGRNBatchExistingAgainstGRN(objDTOPurchaseDetails.intItemID);
                                            if (dtTemp.Rows.Count > 0)
                                            {
                                                foreach (DataRow dr in dtTemp.Rows)
                                                {
                                                    objDTOPurchaseDetails.intBatchID = dr["BatchID"].ToInt64();
                                                    StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                                }
                                            }
                                            else
                                                StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                        }
                                        else
                                        StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                    }
                                    PobjClsDTOPurchase.intWarehouseID = intTempWarehouseID;
                                }
                                else
                                {
                                    if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
                                    {
                                        StockDetailsUpdationGRN(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                    }
                                    else
                                    {
                                        if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                                        {
                                            DataTable dtTemp = GetGRNBatchExistingAgainstGRN(objDTOPurchaseDetails.intItemID);
                                            if (dtTemp.Rows.Count > 0)
                                            {
                                                foreach (DataRow dr in dtTemp.Rows)
                                                {
                                                    objDTOPurchaseDetails.intBatchID = dr["BatchID"].ToInt64();
                                                    StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                                }
                                            }
                                            else
                                                StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                        }
                                        else
                                        StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                    }
                                }
                            }
                            StockMasterUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                      
                        }
                       
                    }
                }
            }

            for (int i = 0; i < PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection.Count; i++)
            {

                if (!lstupdateIndices.Contains(i))
                {
                    PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intBatchID = FindBatchID(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].strBatchNumber);
                    PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intBatchID = BatchUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i]);
                 
                    if (!PobjClsDTOPurchase.blnCancelled)
                    {
                        //  blnUpdateLIFORate = true;
                        StockMasterUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i], 0, 0, false, false);
                        if (!PobjClsDTOPurchase.blnGRNRequired || CheckGRNExistingAgainstOrder())
                        {

                            if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                            {
                                DataTable dtTemp = GetGRNBatchExistingAgainstGRN(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intItemID);
                                if (dtTemp.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dtTemp.Rows)
                                    {
                                        PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intBatchID = dr["BatchID"].ToInt64();
                                        StockDetailsUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i], 0, 0, false, false);
                                    }
                                }
                                else
                                    StockDetailsUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i], 0, 0, false, false);
                            }
                            else
                            StockDetailsUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i], 0, 0, false, false);
                        }
                        if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
                        {
                            StockDetailsUpdationGRN(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i], 0, 0, false, false);
                        }
                        // blnUpdateLIFORate = false;
                    }
                    prmPurOrder = new ArrayList();

                    prmPurOrder.Add(new SqlParameter("@Mode", 8));
                    prmPurOrder.Add(new SqlParameter("@SerialNo", i + 1));
                    prmPurOrder.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                    prmPurOrder.Add(new SqlParameter("@ItemID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intItemID));

                    if (!PobjClsDTOPurchase.blnGRNRequired)
                        //UpdateLastSaleRateOfItem(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decSaleRate, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intItemID,PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intUOMID);

                        prmPurOrder.Add(new SqlParameter("@OrderedQuantity", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decOrderQuantity));
                    prmPurOrder.Add(new SqlParameter("@ReceivedQuantity", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decQuantity));
                    prmPurOrder.Add(new SqlParameter("@ExtraQuantity", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decExtraQuantity));
                    prmPurOrder.Add(new SqlParameter("@BaseUnitQty", MobjClsCommonUtility.ConvertQtyToBaseUnitQty(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intUOMID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intItemID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decQuantity + PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decExtraQuantity, 1)));
                    prmPurOrder.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                    prmPurOrder.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID));
                    prmPurOrder.Add(new SqlParameter("@UOMID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intUOMID));
                    if (!PobjClsDTOPurchase.blnGRNRequired || PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intBatchID != 0)
                        prmPurOrder.Add(new SqlParameter("@BatchID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intBatchID));
                    prmPurOrder.Add(new SqlParameter("@Rate", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decRate));

                    prmPurOrder.Add(new SqlParameter("@PurchaseRate", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decPurchaseRate));
                    //if (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intDiscountID != 0)
                    //{
                    //    prmPurOrder.Add(new SqlParameter("@DiscountID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intDiscountID));
                    //    prmPurOrder.Add(new SqlParameter("@DiscountAmount", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decDiscountAmount));
                    //}
                    if (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].blnDiscount == true)
                    {
                        prmPurOrder.Add(new SqlParameter("@DiscountPercentage", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decDiscountPercent));
                        prmPurOrder.Add(new SqlParameter("@DiscountAmount", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decDiscountAmt));
                        prmPurOrder.Add(new SqlParameter("@IsDiscountPercentage", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].blnDiscount));

                    }
                    else if (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].blnDiscount == false)
                    {
                        prmPurOrder.Add(new SqlParameter("@DiscountPercentage", 0.0));
                        prmPurOrder.Add(new SqlParameter("@DiscountAmount", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decDiscountAmt));
                        prmPurOrder.Add(new SqlParameter("@IsDiscountPercentage", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].blnDiscount));

                    }
                    prmPurOrder.Add(new SqlParameter("@GrandAmount", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decGrandAmount));
                    prmPurOrder.Add(new SqlParameter("@NetAmount", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decNetAmount));
                    prmPurOrder.Add(new SqlParameter("@ExpiryDate", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].strExpiryDate));
                    prmPurOrder.Add(new SqlParameter("@InvoiceDate", PobjClsDTOPurchase.strDate));
                    prmPurOrder.Add(new SqlParameter("@CurrencyID", PobjClsDTOPurchase.intCurrencyID));
                    prmPurOrder.Add(new SqlParameter("@IsGRNRequired", PobjClsDTOPurchase.blnGRNRequired));
                    prmPurOrder.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
                    prmPurOrder.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
                    prmPurOrder.Add(new SqlParameter("@LandingCost", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decLandingCost));

                    MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseInvoice", prmPurOrder);

                    if (PobjClsDTOPurchase.intOrderType != (Int32)OperationOrderType.PInvoiceFromGRN)
                    {
                            ArrayList prmPuchaseStatus = new ArrayList();
                            prmPuchaseStatus.Add(new SqlParameter("@Mode", 38));
                            prmPuchaseStatus.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                            prmPuchaseStatus.Add(new SqlParameter("@ReferenceID", ReferenceID));
                            prmPuchaseStatus.Add(new SqlParameter("@ItemID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intItemID));
                            prmPuchaseStatus.Add(new SqlParameter("@Quantity", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decQuantity));
                            if (!PobjClsDTOPurchase.blnGRNRequired || PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intBatchID != 0)
                                //prmPuchaseStatus.Add(new SqlParameter("@BatchID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intBatchID));

                                prmPuchaseStatus.Add(new SqlParameter("@BatchID", 1));
                            //if (PobjClsDTOPurchase.blnGRNRequired)
                            //{
                            //    prmPuchaseStatus.Add(new SqlParameter("@BatchID", 1));
                            //}
                            //else
                            //{
                            //    prmPuchaseStatus.Add(new SqlParameter("@BatchID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intBatchID));
                            //}
                            MobjDataLayer.ExecuteNonQuery("spPurchaseInvoice", prmPuchaseStatus);

                    }
             
                }
            }
            if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.PInvoiceFromGRN && !PobjClsDTOPurchase.blnCancelled)
            {
                DataTable DtReferenceDetails = GetSavedPurchaseInvoiceReference(PobjClsDTOPurchase.intPurchaseID);
                foreach (DataRow dr in DtReferenceDetails.Rows)
                {
                    clsDTOPurchaseDetails objDTOPurchaseReferenceDetails = new clsDTOPurchaseDetails();
                    objDTOPurchaseReferenceDetails.intItemID = Convert.ToInt32(dr["ItemID"]);
                    objDTOPurchaseReferenceDetails.intBatchID = Convert.ToInt32(dr["BatchID"]);
                    objDTOPurchaseReferenceDetails.strBatchNumber = Convert.ToString(dr["BatchNo"]);

                    BatchUpdationFromGRN(objDTOPurchaseReferenceDetails, PobjClsDTOPurchase.intPurchaseID);
                }
                int value = CheckReferenceForBatchInfo(PobjClsDTOPurchase.intPurchaseID.ToInt32());
                if (value > 0)
                    SaveCostingMethodDetails(PobjClsDTOPurchase.intPurchaseID.ToInt32(), "PI", (Int32)OperationOrderType.PInvoiceFromGRN); // Saving Costing Method & Pricing Scheme Details

            }
            if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.PInvoiceFromGRN )
            {
                UpdateGRNStatus(PobjClsDTOPurchase.intPurchaseID);
            }
            blnIsOldStatusCancelled = false;

            UpdateStockValue();

            return true;
        }



        public bool DeleteOldDetails(Int64 iOrdID, string strCondition, bool blnIsExpense)// Purchase Invoice Save
        {
            // DeleteOpeningStockAccountForPurchaseInvoice();

            object iOutPurchaseOrderID = 0;
            ArrayList prmPurOrder = new ArrayList();
            prmPurOrder.Add(new SqlParameter("@Mode", 1));
            prmPurOrder.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
            prmPurOrder.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
            prmPurOrder.Add(new SqlParameter("@CreatedBy", PobjClsDTOPurchase.intCreatedBy));
            IsGRNRequired();
            DataTable datCompany = MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", prmPurOrder);
            if (datCompany.Rows.Count > 0)
            {
                int intStatusId = -1;
                if (datCompany.Rows[0]["StatusID"] != DBNull.Value) intStatusId = Convert.ToInt32(datCompany.Rows[0]["StatusID"]);
                if (PobjClsDTOPurchase.blnCancelled && intStatusId == PobjClsDTOPurchase.intStatusID)
                    blnIsOldStatusCancelled = true;
                else if (!PobjClsDTOPurchase.blnCancelled && intStatusId != PobjClsDTOPurchase.intStatusID)
                    blnIsOldStatusCancelled = true;

            }

            if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
            {
                UpdateGRNStatusPIDeleted(iOrdID, strCondition, 1);//to change GRN status
            }
            DataTable dtInvoiceDetails = DisplayPurchaseInvoiceDetail(PobjClsDTOPurchase.intPurchaseID);
            if (!blnIsOldStatusCancelled)
            {
                for (int i = 0; i < dtInvoiceDetails.Rows.Count; i++)
                {
                    //clsDTOPurchaseDetails objDTOPurchaseDetails = new clsDTOPurchaseDetails();
                    //objDTOPurchaseDetails.intBatchID = Convert.ToInt32(dtInvoiceDetails.Rows[i]["BatchID"]);
                    //objDTOPurchaseDetails.intItemID = Convert.ToInt32(dtInvoiceDetails.Rows[i]["ItemID"]);
                    //StockDetailsUpdation(objDTOPurchaseDetails, Convert.ToDecimal(dtInvoiceDetails.Rows[i]["ReceivedQuantity"]) + Convert.ToDecimal(dtInvoiceDetails.Rows[i]["ExtraQuantity"]), false, true);
                    //StockMasterUpdation(objDTOPurchaseDetails, Convert.ToDecimal(dtInvoiceDetails.Rows[i]["ReceivedQuantity"]) + Convert.ToDecimal(dtInvoiceDetails.Rows[i]["ExtraQuantity"]), false, true);
                    clsDTOPurchaseDetails objDTOPurchaseDetails = new clsDTOPurchaseDetails();
                    if (dtInvoiceDetails.Rows[i]["BatchID"] != DBNull.Value)
                        objDTOPurchaseDetails.intBatchID = Convert.ToInt64(dtInvoiceDetails.Rows[i]["BatchID"]);
                    objDTOPurchaseDetails.intItemID = Convert.ToInt32(dtInvoiceDetails.Rows[i]["ItemID"]);

                    DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtInvoiceDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtInvoiceDetails.Rows[i]["ItemID"]));
                    decimal decOldQuantity = Convert.ToDecimal(dtInvoiceDetails.Rows[i]["ReceivedQuantity"]);
                    decimal decOldExtraQuantity = Convert.ToDecimal(dtInvoiceDetails.Rows[i]["ExtraQuantity"]);
                    if (dtGetUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                        {
                            decOldQuantity = decOldQuantity / decConversionValue;
                            decOldExtraQuantity = decOldExtraQuantity / decConversionValue;
                        }
                        else if (intConversionFactor == 2)
                        {
                            decOldQuantity = decOldQuantity * decConversionValue;
                            decOldExtraQuantity = decOldExtraQuantity * decConversionValue;
                        }
                    }
                    int intTempWarehouseID = PobjClsDTOPurchase.intWarehouseID;
                    if (!PobjClsDTOPurchase.blnGRNRequired || PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN || CheckGRNExistingAgainstOrder())
                    {
                        if (PobjClsDTOPurchase.intTempWarehouseID != PobjClsDTOPurchase.intWarehouseID)
                        {
                            PobjClsDTOPurchase.intWarehouseID = PobjClsDTOPurchase.intTempWarehouseID;
                            if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
                            {
                                StockDetailsUpdationGRN(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                            }
                            else
                            {
                                if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                                {
                                    DataTable dtTemp = GetGRNBatchExistingAgainstGRN(objDTOPurchaseDetails.intItemID);
                                    foreach (DataRow dr in dtTemp.Rows)
                                    {
                                        objDTOPurchaseDetails.intBatchID = dr["BatchID"].ToInt64();
                                        StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                    }
                                }
                                else
                                    StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                            }
                            PobjClsDTOPurchase.intWarehouseID = intTempWarehouseID;
                        }
                        else
                        {
                            if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
                            {
                                StockDetailsUpdationGRN(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                            }
                            else
                            {
                                StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                            }
                        }
                    }
                    StockMasterUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);


                    //DeleteCostingAndPricingDetails(objDTOPurchaseDetails.intItemID.ToInt32(), objDTOPurchaseDetails.intBatchID.ToInt32());
                    // DeleteCostingAndPricingDetailsFromOpeningStock(objDTOPurchaseDetails.intItemID.ToInt32(), objDTOPurchaseDetails.intBatchID.ToInt32());
                }
                if (!PobjClsDTOPurchase.blnGRNRequired)
                    DeletePurchaseLocationDetails(false, false);
            }
            else
            {
                if (!PobjClsDTOPurchase.blnGRNRequired)
                    DeletePurchaseLocationDetails(false, true);
            }

            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 82));
            parameters.Add(new SqlParameter("@PurchaseInvoiceID", iOrdID));
            parameters.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
            parameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
            parameters.Add(new SqlParameter("@IsExpense", blnIsExpense));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            blnIsOldStatusCancelled = false;
            if (MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseInvoice", parameters) != 0)
            {
                if (PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PInvoiceCancelled)
                {
                    if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                    {
                        if (ClsCommonSettings.POApproval)
                            UpdateStatus(3, (Int32)OperationStatusType.POrderApproved, PobjClsDTOPurchase.intPurchaseOrderID);
                        else
                            UpdateStatus(3, (Int32)OperationStatusType.POrderSubmitted, PobjClsDTOPurchase.intPurchaseOrderID);
                    }
                }
                DeletePayments(iOrdID.ToInt32());

                return true;
            }
            else
            {
                return false;
            }

        }


        private bool UpdateStockValue()
        {
            //This is to Update Stock Value

            ArrayList prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation.Add(new SqlParameter("@Mode", 30));
            prmItemStockValueUpdation.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
            prmItemStockValueUpdation.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
            prmItemStockValueUpdation.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));

            if (MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseInvoice", prmItemStockValueUpdation) != 0)
                return true;
            return false;
        }

        private bool StockMasterUpdation(clsDTOPurchaseDetails objClsDtoPurchaseDetails, decimal decOldQuantity, decimal decOldExtraQty, bool blnIsGRN, bool blnIsDeleted)
        {
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 1));
            prmPurchase.Add(new SqlParameter("@ItemID", objClsDtoPurchaseDetails.intItemID));
            DataTable dtStockMaster = MobjDataLayer.ExecuteDataTable("spInvStockMaster", prmPurchase);
            DataTable dtGetUomDetails = GetUomConversionValues(objClsDtoPurchaseDetails.intUOMID, objClsDtoPurchaseDetails.intItemID);
            //if (PobjClsDTOPurchase.intOrderType != (int)OperationOrderType.GRNFromInvoice)
            //    dtGetUomDetails = GetUomConversionValues(objClsDtoPurchaseDetails.intUOMID,objClsDtoPurchaseDetails.intItemID);
            decimal decQuantity = objClsDtoPurchaseDetails.decQuantity;
            decimal decExtraQuantity = objClsDtoPurchaseDetails.decExtraQuantity;
            if (dtGetUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                {
                    decQuantity = decQuantity / decConversionValue;
                    decExtraQuantity = decExtraQuantity / decConversionValue;
                }
                else if (intConversionFactor == 2)
                {
                    decQuantity = decQuantity * decConversionValue;
                    decExtraQuantity = decExtraQuantity * decConversionValue;
                }
            }
            if (dtStockMaster.Rows.Count > 0)
            {
                decimal decGRNQty = 0, decInvQty = 0, decOpeningStock = 0, decSoldQuantity = 0, decDamagedQuantity = 0, decDemoQuantity = 0, decTransferedQty = 0, decReceivedFromTransfer = 0;
                decGRNQty = dtStockMaster.Rows[0]["GRNQuantity"].ToDecimal();
                decInvQty = dtStockMaster.Rows[0]["InvoicedQuantity"].ToDecimal();
                decOpeningStock = dtStockMaster.Rows[0]["OpeningStock"].ToDecimal();
                decSoldQuantity = dtStockMaster.Rows[0]["SoldQuantity"].ToDecimal();
                decDamagedQuantity = dtStockMaster.Rows[0]["DamagedQuantity"].ToDecimal();
                decTransferedQty = dtStockMaster.Rows[0]["TransferedQuantity"].ToDecimal();
                decReceivedFromTransfer = dtStockMaster.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                decDemoQuantity = dtStockMaster.Rows[0]["DemoQuantity"].ToDecimal();
                decOpeningStock = dtStockMaster.Rows[0]["OpeningStock"].ToDecimal();
                if (blnIsGRN)
                {
                    if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice)
                    {
                        if (!blnIsDeleted)
                        {
                            decGRNQty = Convert.ToDecimal(dtStockMaster.Rows[0]["GRNQuantity"]) - (decOldQuantity + decOldExtraQty) + decQuantity + decExtraQuantity;
                            decInvQty = Convert.ToDecimal(dtStockMaster.Rows[0]["InvoicedQuantity"]);
                        }
                        else
                        {
                            decGRNQty = Convert.ToDecimal(dtStockMaster.Rows[0]["GRNQuantity"]) - (decOldQuantity + decOldExtraQty);
                            decInvQty = Convert.ToDecimal(dtStockMaster.Rows[0]["InvoicedQuantity"]);
                        }
                    }
                    else if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromOrder)
                    {
                        if (!blnIsDeleted)
                        {
                            decGRNQty = Convert.ToDecimal(dtStockMaster.Rows[0]["GRNQuantity"]) - (decOldQuantity + decOldExtraQty) + decQuantity + decExtraQuantity;
                            decInvQty = Convert.ToDecimal(dtStockMaster.Rows[0]["InvoicedQuantity"]);
                        }
                        else
                        {
                            decGRNQty = Convert.ToDecimal(dtStockMaster.Rows[0]["GRNQuantity"]) - (decOldQuantity + decOldExtraQty);
                            decInvQty = Convert.ToDecimal(dtStockMaster.Rows[0]["InvoicedQuantity"]);
                        }
                    }
                    else if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromTransfer)
                    {
                        int intTransferTypeID = FillCombos(new string[] { "OrderTypeID", "InvStockTransferMaster", "StockTransferID = " + PobjClsDTOPurchase.intReferenceID }).Rows[0]["OrderTypeID"].ToInt32();
                        if (intTransferTypeID == (int)OperationOrderType.STWareHouseToWareHouseTransferDemo)
                        {
                            if (!blnIsDeleted)
                            {
                                decDemoQuantity = Convert.ToDecimal(dtStockMaster.Rows[0]["DemoQuantity"]) - decOldQuantity + decQuantity;
                            }
                            else
                            {
                                decDemoQuantity = Convert.ToDecimal(dtStockMaster.Rows[0]["DemoQuantity"]) - decOldQuantity;
                            }

                        }
                        else
                        {
                            if (!blnIsDeleted)
                            {
                                decReceivedFromTransfer = Convert.ToDecimal(dtStockMaster.Rows[0]["ReceivedFromTransfer"]) - decOldQuantity + decQuantity;
                            }
                            else
                            {
                                decReceivedFromTransfer = Convert.ToDecimal(dtStockMaster.Rows[0]["ReceivedFromTransfer"]) - decOldQuantity;
                            }
                        }
                    }


                }
                else
                {
                    if (PobjClsDTOPurchase.blnGRNRequired)
                    {
                        if (!blnIsDeleted)
                        {
                            decGRNQty = Convert.ToDecimal(dtStockMaster.Rows[0]["GRNQuantity"]);
                            decInvQty = Convert.ToDecimal(dtStockMaster.Rows[0]["InvoicedQuantity"]) - decOldQuantity + decQuantity;
                        }
                        else
                        {
                            decGRNQty = Convert.ToDecimal(dtStockMaster.Rows[0]["GRNQuantity"]);
                            decInvQty = Convert.ToDecimal(dtStockMaster.Rows[0]["InvoicedQuantity"]) - decOldQuantity;
                        }
                    }
                    else
                    {
                        if (!blnIsDeleted)
                        {
                            decGRNQty = Convert.ToDecimal(dtStockMaster.Rows[0]["GRNQuantity"]) - (decOldQuantity + decOldExtraQty) + decQuantity + decExtraQuantity;
                            decInvQty = Convert.ToDecimal(dtStockMaster.Rows[0]["InvoicedQuantity"]) - (decOldQuantity) + decQuantity;
                        }
                        else
                        {
                            decGRNQty = Convert.ToDecimal(dtStockMaster.Rows[0]["GRNQuantity"]) - (decOldQuantity + decOldExtraQty);
                            decInvQty = Convert.ToDecimal(dtStockMaster.Rows[0]["InvoicedQuantity"]) - (decOldQuantity);
                        }
                    }
                }
                prmPurchase = new ArrayList();
                prmPurchase.Add(new SqlParameter("@Mode", 3));
                prmPurchase.Add(new SqlParameter("@ItemID", objClsDtoPurchaseDetails.intItemID));
                //  if (blnUpdateLIFORate)
                //       prmPurchase.Add(new SqlParameter("@LIFORate", objClsDtoPurchaseDetails.decRate));
                //   else
                //      prmPurchase.Add(new SqlParameter("@LIFORate", dtStockMaster.Rows[0]["LIFORate"]));
                prmPurchase.Add(new SqlParameter("@GRNQuantity", decGRNQty));
                prmPurchase.Add(new SqlParameter("@InvoicedQuantity", decInvQty));
                prmPurchase.Add(new SqlParameter("@OpeningStock", decOpeningStock));
                prmPurchase.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
                prmPurchase.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));
                prmPurchase.Add(new SqlParameter("@SoldQuantity", decSoldQuantity));
                prmPurchase.Add(new SqlParameter("@DamagedQuantity", decDamagedQuantity));
                prmPurchase.Add(new SqlParameter("@DemoQuantity", decDemoQuantity));
                prmPurchase.Add(new SqlParameter("@LastModifiedBy", PobjClsDTOPurchase.intCreatedBy));
                prmPurchase.Add(new SqlParameter("@LastModifiedDate", PobjClsDTOPurchase.strCreatedDate));
                prmPurchase.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                prmPurchase.Add(new SqlParameter("@CurrencyID", PobjClsDTOPurchase.intCurrencyID));

                if (blnIsGRN)
                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
                else
                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));

                MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmPurchase);
            }
            else
            {
                decimal decDefaultValue = 0;
                prmPurchase = new ArrayList();
                prmPurchase.Add(new SqlParameter("@Mode", 2));
                prmPurchase.Add(new SqlParameter("@ItemID", objClsDtoPurchaseDetails.intItemID));
                if (blnIsGRN)
                {
                    if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice || PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromOrder)
                    {
                        prmPurchase.Add(new SqlParameter("@GRNQuantity", decQuantity + decExtraQuantity));
                        prmPurchase.Add(new SqlParameter("@InvoicedQuantity", decDefaultValue));
                    }
                    else
                    {
                        int intTransferTypeID = FillCombos(new string[] { "OrderTypeID", "InvStockTransferMaster", "StockTransferID = " + PobjClsDTOPurchase.intReferenceID }).Rows[0]["OrderTypeID"].ToInt32();
                        if (intTransferTypeID == (int)OperationOrderType.STWareHouseToWareHouseTransferDemo)
                        {
                            prmPurchase.Add(new SqlParameter("@DemoQuantity", decQuantity));
                        }
                        else
                        {
                            prmPurchase.Add(new SqlParameter("@ReceivedFromTransfer", decQuantity));
                        }
                    }

                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
                }
                else
                {
                    if (PobjClsDTOPurchase.blnGRNRequired)
                    {
                        prmPurchase.Add(new SqlParameter("@GRNQuantity", decDefaultValue));
                        prmPurchase.Add(new SqlParameter("@InvoicedQuantity", decQuantity));
                    }
                    else
                    {
                        prmPurchase.Add(new SqlParameter("@GRNQuantity", decQuantity + decExtraQuantity));
                        prmPurchase.Add(new SqlParameter("@InvoicedQuantity", decQuantity));
                    }

                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
                }
                prmPurchase.Add(new SqlParameter("@LastModifiedBy", PobjClsDTOPurchase.intCreatedBy));
                prmPurchase.Add(new SqlParameter("@LastModifiedDate", PobjClsDTOPurchase.strCreatedDate));
                prmPurchase.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                prmPurchase.Add(new SqlParameter("@CurrencyID", PobjClsDTOPurchase.intCurrencyID));



                MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmPurchase);

            }
            return true;
        }

        private bool StockDetailsUpdation(clsDTOPurchaseDetails objClsDtoPurchaseDetails, decimal decOldQty, decimal decOldExtraQty, bool blnIsGRN, bool blnIsDeleted)
        {
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 4));
            prmPurchase.Add(new SqlParameter("@ItemID", objClsDtoPurchaseDetails.intItemID));
            prmPurchase.Add(new SqlParameter("@BatchID", objClsDtoPurchaseDetails.intBatchID));
            prmPurchase.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
            prmPurchase.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID));
            DataTable dtStockDetails = MobjDataLayer.ExecuteDataTable("spInvStockMaster", prmPurchase);
            DataTable dtGetUomDetails = GetUomConversionValues(objClsDtoPurchaseDetails.intUOMID, objClsDtoPurchaseDetails.intItemID);
            //if (PobjClsDTOPurchase.intOrderType != (int)OperationOrderType.GRNFromInvoice)
            //    dtGetUomDetails = GetUomConversionValues(objClsDtoPurchaseDetails.intUOMID, objClsDtoPurchaseDetails.intItemID, 1);
            decimal decQuantity = objClsDtoPurchaseDetails.decQuantity;
            decimal decExtraQuantity = objClsDtoPurchaseDetails.decExtraQuantity;
            if (dtGetUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                {
                    decQuantity = decQuantity / decConversionValue;
                    decExtraQuantity = decExtraQuantity / decConversionValue;
                }
                else if (intConversionFactor == 2)
                {
                    decQuantity = decQuantity * decConversionValue;
                    decExtraQuantity = decExtraQuantity * decConversionValue;
                }
            }
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@ItemID", objClsDtoPurchaseDetails.intItemID));
            prmPurchase.Add(new SqlParameter("@BatchID", objClsDtoPurchaseDetails.intBatchID));
            prmPurchase.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
            prmPurchase.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID));
            if (dtStockDetails.Rows.Count > 0)
            {
                decimal decGRNQty = 0, decInvQty = 0, decOpeningStock = 0, decSoldQuantity = 0, decDamagedQuantity = 0, decDemoQuantity = 0, decTransferedQty = 0, decReceivedFromTransfer = 0;
                decGRNQty = dtStockDetails.Rows[0]["GRNQuantity"].ToDecimal();
                decInvQty = dtStockDetails.Rows[0]["InvoicedQuantity"].ToDecimal();
                decOpeningStock = dtStockDetails.Rows[0]["OpeningStock"].ToDecimal();
                decSoldQuantity = dtStockDetails.Rows[0]["SoldQuantity"].ToDecimal();
                decDamagedQuantity = dtStockDetails.Rows[0]["DamagedQuantity"].ToDecimal();
                decTransferedQty = dtStockDetails.Rows[0]["TransferedQuantity"].ToDecimal();
                decReceivedFromTransfer = dtStockDetails.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                decDemoQuantity = dtStockDetails.Rows[0]["DemoQuantity"].ToDecimal();
                if (blnIsGRN || CheckGRNExistingAgainstOrder())
                {
                    if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice)
                    {
                        if (!blnIsDeleted)
                        {
                            decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty) + decQuantity + decExtraQuantity;
                            decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - decOldQty + decQuantity;
                        }
                        else
                        {
                            decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty);
                            decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - decOldQty;
                        }
                    }
                    else if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromOrder)
                    {
                        if (!blnIsDeleted)
                        {
                            decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty) + decQuantity + decExtraQuantity;
                            decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - decOldQty + decQuantity;
                        }
                        else
                        {
                            decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty);
                            decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - decOldQty;
                        }
                    }
                    else if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                    {
                        if (!blnIsDeleted)
                        {
                            DataTable dt = new clsBLLPurchaseInvoice().FillCombos(new string[]{"OrderTypeID","InvPurchaseOrderMaster ","PurchaseOrderID = "+PobjClsDTOPurchase.intPurchaseOrderID});
                            int intCostingMethodID = 0;
                            DataTable datCostingMethod = FillCombos(new string[] { "CostingMethodID", "InvCostingAndPricingDetails", "ItemID = " + objClsDtoPurchaseDetails.intItemID + "" });
                            if (datCostingMethod != null && datCostingMethod.Rows.Count > 0 )
                            {
                                intCostingMethodID = datCostingMethod.Rows[0]["CostingMethodID"].ToInt32();
                            }
                            if (dt != null && dt.Rows.Count > 0 && dt.Rows[0]["OrderTypeID"].ToInt32() == (Int32)OperationOrderType.POrderFromGRN && intCostingMethodID == (int)CostingMethodReference.Batch)
                            {
                                decInvQty = GetQuantiyForBatch(objClsDtoPurchaseDetails.intBatchID, objClsDtoPurchaseDetails.intItemID);
                            }
                            else
                            {
                                decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - decOldQty + decQuantity;
                            }

                            //decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty) + decQuantity + decExtraQuantity;
                            
                        }
                        else
                        {
                           // decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty);
                            decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - decOldQty;
                        }
                    }
                    else if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromTransfer)
                    {
                        int intTransferTypeID = FillCombos(new string[] { "OrderTypeID", "InvStockTransferMaster", "StockTransferID = " + PobjClsDTOPurchase.intReferenceID }).Rows[0]["OrderTypeID"].ToInt32();
                        if (intTransferTypeID == (int)OperationOrderType.STWareHouseToWareHouseTransferDemo)
                        {
                            if (!blnIsDeleted)
                            {
                                decDemoQuantity = Convert.ToDecimal(dtStockDetails.Rows[0]["DemoQuantity"]) - decOldQty + decQuantity;
                            }
                            else
                            {
                                decDemoQuantity = Convert.ToDecimal(dtStockDetails.Rows[0]["DemoQuantity"]) - decOldQty;
                            }

                        }
                        else
                        {
                            if (!blnIsDeleted)
                            {
                                decReceivedFromTransfer = Convert.ToDecimal(dtStockDetails.Rows[0]["ReceivedFromTransfer"]) - decOldQty + decQuantity;
                            }
                            else
                            {
                                decReceivedFromTransfer = Convert.ToDecimal(dtStockDetails.Rows[0]["ReceivedFromTransfer"]) - decOldQty;
                            }
                        }
                    }


                }
                else
                {
                    if (PobjClsDTOPurchase.blnGRNRequired)
                    {

                        if (!blnIsDeleted)
                        {
                            decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]);
                            decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - (decOldQty) + decQuantity;
                        }
                        else
                        {
                            decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]);
                            decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - (decOldQty);
                        }
                    }
                    else
                    {
                        if (!blnIsDeleted)
                        {
                            decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty) + decQuantity + decExtraQuantity;
                            decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - (decOldQty) + decQuantity;
                            //decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"])+ decQuantity;
                        }
                        else
                        {
                            decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty);
                            decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - (decOldQty);
                        }
                    }
                }
                prmPurchase.Add(new SqlParameter("@Mode", 6));
                prmPurchase.Add(new SqlParameter("@GRNQuantity", decGRNQty));
                prmPurchase.Add(new SqlParameter("@InvoicedQuantity", decInvQty));
                prmPurchase.Add(new SqlParameter("@OpeningStock", decOpeningStock));
                prmPurchase.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
                prmPurchase.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));
                prmPurchase.Add(new SqlParameter("@SoldQuantity", decSoldQuantity));
                prmPurchase.Add(new SqlParameter("@DamagedQuantity", decDamagedQuantity));
                prmPurchase.Add(new SqlParameter("@DemoQuantity", decDemoQuantity));

                if (blnIsGRN)
                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
                else
                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));

                MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmPurchase);
            }
            else
            {
                prmPurchase.Add(new SqlParameter("@Mode", 5));
                decimal decDefaultQty = 0;
                if (blnIsGRN)
                {
                    if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice)
                    {
                        prmPurchase.Add(new SqlParameter("@GRNQuantity", decQuantity + decExtraQuantity));
                        prmPurchase.Add(new SqlParameter("@InvoicedQuantity", decQuantity));
                    }

                    else if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromOrder)
                    {
                        ////prmPurchase.Add(new SqlParameter("@GRNQuantity", decQuantity + decExtraQuantity));
                        ////prmPurchase.Add(new SqlParameter("@InvoicedQuantity", decQuantity));
                        prmPurchase.Add(new SqlParameter("@GRNQuantity", decQuantity + decExtraQuantity));
                        prmPurchase.Add(new SqlParameter("@InvoicedQuantity", 0));//For adjusting stock,there is no column ordered quantity in invstockDetails so for test now passing 0 
                    }
                    else
                    {
                        int intTransferTypeID = FillCombos(new string[] { "OrderTypeID", "InvStockTransferMaster", "StockTransferID = " + PobjClsDTOPurchase.intReferenceID }).Rows[0]["OrderTypeID"].ToInt32();
                        if (intTransferTypeID == (int)OperationOrderType.STWareHouseToWareHouseTransferDemo)
                        {
                            prmPurchase.Add(new SqlParameter("@DemoQuantity", decQuantity));
                        }
                        else
                        {
                            prmPurchase.Add(new SqlParameter("@ReceivedFromTransfer", decQuantity));
                        }
                    }
                }
                else
                {
                    if (PobjClsDTOPurchase.blnGRNRequired)
                    {
                        prmPurchase.Add(new SqlParameter("@GRNQuantity", decDefaultQty));
                        prmPurchase.Add(new SqlParameter("@InvoicedQuantity", decQuantity));
                    }
                    else
                    {
                        prmPurchase.Add(new SqlParameter("@GRNQuantity", decQuantity + decExtraQuantity));
                        prmPurchase.Add(new SqlParameter("@InvoicedQuantity", decQuantity));
                    }
                }
                if (blnIsGRN)
                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
                else
                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
                MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmPurchase);
            }
            return true;
        }

        private bool StockDetailsUpdationGRN(clsDTOPurchaseDetails objClsDtoPurchaseDetails, decimal decOldQty, decimal decOldExtraQty, bool blnIsGRN, bool blnIsDeleted)
        {
            clsDTOPurchaseDetails objDTOPurchaseReferenceDetails = new clsDTOPurchaseDetails();
            DataTable DtReferenceDetails = GetSavedPurchaseInvoiceReferenceGRN(PobjClsDTOPurchase.intPurchaseID, objClsDtoPurchaseDetails.intItemID);
            foreach (DataRow dr in DtReferenceDetails.Rows)
            {
                objDTOPurchaseReferenceDetails.intBatchID = Convert.ToInt32(dr["BatchID"]);



                prmPurchase = new ArrayList();
                prmPurchase.Add(new SqlParameter("@Mode", 4));
                prmPurchase.Add(new SqlParameter("@ItemID", objClsDtoPurchaseDetails.intItemID));
                prmPurchase.Add(new SqlParameter("@BatchID", objDTOPurchaseReferenceDetails.intBatchID));
                prmPurchase.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                prmPurchase.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID));
                DataTable dtStockDetails = MobjDataLayer.ExecuteDataTable("spInvStockMaster", prmPurchase);
                DataTable dtGetUomDetails = GetUomConversionValues(objClsDtoPurchaseDetails.intUOMID, objClsDtoPurchaseDetails.intItemID);
                //if (PobjClsDTOPurchase.intOrderType != (int)OperationOrderType.GRNFromInvoice)
                //    dtGetUomDetails = GetUomConversionValues(objClsDtoPurchaseDetails.intUOMID, objClsDtoPurchaseDetails.intItemID, 1);
                decimal decQuantity = objClsDtoPurchaseDetails.decQuantity;
                decimal decExtraQuantity = objClsDtoPurchaseDetails.decExtraQuantity;
                if (dtGetUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                    {
                        decQuantity = decQuantity / decConversionValue;
                        decExtraQuantity = decExtraQuantity / decConversionValue;
                    }
                    else if (intConversionFactor == 2)
                    {
                        decQuantity = decQuantity * decConversionValue;
                        decExtraQuantity = decExtraQuantity * decConversionValue;
                    }
                }
                prmPurchase = new ArrayList();
                prmPurchase.Add(new SqlParameter("@ItemID", objClsDtoPurchaseDetails.intItemID));
                prmPurchase.Add(new SqlParameter("@BatchID", objDTOPurchaseReferenceDetails.intBatchID));
                prmPurchase.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                prmPurchase.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID));
                if (dtStockDetails.Rows.Count > 0)
                {
                    decimal decGRNQty = 0, decInvQty = 0, decOpeningStock = 0, decSoldQuantity = 0, decDamagedQuantity = 0, decDemoQuantity = 0, decTransferedQty = 0, decReceivedFromTransfer = 0;
                    decGRNQty = dtStockDetails.Rows[0]["GRNQuantity"].ToDecimal();
                    decInvQty = dtStockDetails.Rows[0]["InvoicedQuantity"].ToDecimal();
                    decOpeningStock = dtStockDetails.Rows[0]["OpeningStock"].ToDecimal();
                    decSoldQuantity = dtStockDetails.Rows[0]["SoldQuantity"].ToDecimal();
                    decDamagedQuantity = dtStockDetails.Rows[0]["DamagedQuantity"].ToDecimal();
                    decTransferedQty = dtStockDetails.Rows[0]["TransferedQuantity"].ToDecimal();
                    decReceivedFromTransfer = dtStockDetails.Rows[0]["ReceivedFromTransfer"].ToDecimal();
                    decDemoQuantity = dtStockDetails.Rows[0]["DemoQuantity"].ToDecimal();
                    if (blnIsGRN || PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
                    {
                        if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice)
                        {
                            if (!blnIsDeleted)
                            {
                                decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty) + decQuantity + decExtraQuantity;
                                decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - decOldQty + decQuantity;
                            }
                            else
                            {
                                decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty);
                                decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - decOldQty;
                            }
                        }
                        else if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
                        {
                            if (!blnIsDeleted)
                            {
                               // decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty) + decQuantity + decExtraQuantity;
                                //decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - decOldQty + decQuantity;
                                int intCostingMethodID=0;
                                DataTable datCostingMethod = FillCombos(new string[] { "CostingMethodID", "InvCostingAndPricingDetails", "ItemID = " + objClsDtoPurchaseDetails.intItemID + "" });
                                if (datCostingMethod != null && datCostingMethod.Rows.Count > 0)
                                {
                                    intCostingMethodID = datCostingMethod.Rows[0]["CostingMethodID"].ToInt32();
                                }
                                if (DtReferenceDetails.Rows.Count > 1 && intCostingMethodID==(int)CostingMethodReference.Batch)
                                {
                                    decInvQty = GetQuantiyForBatch(objDTOPurchaseReferenceDetails.intBatchID, objClsDtoPurchaseDetails.intItemID);
                                }
                                else
                                {
                                    decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - decOldQty+decQuantity;
                                }
                            }
                            else
                            {
                                //decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty);
                                decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - decOldQty;
                            }
                        }
                        else if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromOrder)
                        {
                            if (!blnIsDeleted)
                            {
                                decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty) + decQuantity + decExtraQuantity;
                                decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - decOldQty + decQuantity;
                            }
                            else
                            {
                                decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty);
                                decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - decOldQty;
                            }
                        }
                        else if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromTransfer)
                        {
                            int intTransferTypeID = FillCombos(new string[] { "OrderTypeID", "InvStockTransferMaster", "StockTransferID = " + PobjClsDTOPurchase.intReferenceID }).Rows[0]["OrderTypeID"].ToInt32();
                            if (intTransferTypeID == (int)OperationOrderType.STWareHouseToWareHouseTransferDemo)
                            {
                                if (!blnIsDeleted)
                                {
                                    decDemoQuantity = Convert.ToDecimal(dtStockDetails.Rows[0]["DemoQuantity"]) - decOldQty + decQuantity;
                                }
                                else
                                {
                                    decDemoQuantity = Convert.ToDecimal(dtStockDetails.Rows[0]["DemoQuantity"]) - decOldQty;
                                }

                            }
                            else
                            {
                                if (!blnIsDeleted)
                                {
                                    decReceivedFromTransfer = Convert.ToDecimal(dtStockDetails.Rows[0]["ReceivedFromTransfer"]) - decOldQty + decQuantity;
                                }
                                else
                                {
                                    decReceivedFromTransfer = Convert.ToDecimal(dtStockDetails.Rows[0]["ReceivedFromTransfer"]) - decOldQty;
                                }
                            }
                        }


                    }
                    else
                    {
                        if (PobjClsDTOPurchase.blnGRNRequired)
                        {
                            if (!blnIsDeleted)
                            {
                                decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]);
                                decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - (decOldQty) + decQuantity;
                            }
                            else
                            {
                                decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]);
                                decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - (decOldQty);
                            }
                        }
                        else
                        {
                            if (!blnIsDeleted)
                            {
                                decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty) + decQuantity + decExtraQuantity;
                                decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - (decOldQty) + decQuantity;
                            }
                            else
                            {
                                decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - (decOldQty + decOldExtraQty);
                                decInvQty = Convert.ToDecimal(dtStockDetails.Rows[0]["InvoicedQuantity"]) - (decOldQty);
                            }
                        }
                    }
                    prmPurchase.Add(new SqlParameter("@Mode", 6));
                    prmPurchase.Add(new SqlParameter("@GRNQuantity", decGRNQty));
                    prmPurchase.Add(new SqlParameter("@InvoicedQuantity", decInvQty));
                    prmPurchase.Add(new SqlParameter("@OpeningStock", decOpeningStock));
                    prmPurchase.Add(new SqlParameter("@ReceivedFromTransfer", decReceivedFromTransfer));
                    prmPurchase.Add(new SqlParameter("@TransferedQuantity", decTransferedQty));
                    prmPurchase.Add(new SqlParameter("@SoldQuantity", decSoldQuantity));
                    prmPurchase.Add(new SqlParameter("@DamagedQuantity", decDamagedQuantity));
                    prmPurchase.Add(new SqlParameter("@DemoQuantity", decDemoQuantity));

                    if (blnIsGRN)
                        prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
                    else
                        prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));

                    MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmPurchase);
                }
                else
                {
                    prmPurchase.Add(new SqlParameter("@Mode", 5));
                    decimal decDefaultQty = 0;
                    if (blnIsGRN)
                    {
                        if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice)
                        {
                            prmPurchase.Add(new SqlParameter("@GRNQuantity", decQuantity + decExtraQuantity));
                            prmPurchase.Add(new SqlParameter("@InvoicedQuantity", decQuantity));
                        }

                        else if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromOrder)
                        {
                            ////prmPurchase.Add(new SqlParameter("@GRNQuantity", decQuantity + decExtraQuantity));
                            ////prmPurchase.Add(new SqlParameter("@InvoicedQuantity", decQuantity));
                            prmPurchase.Add(new SqlParameter("@GRNQuantity", decQuantity + decExtraQuantity));
                            prmPurchase.Add(new SqlParameter("@InvoicedQuantity", 0));//For adjusting stock,there is no column ordered quantity in invstockDetails so for test now passing 0 
                        }
                        else
                        {
                            int intTransferTypeID = FillCombos(new string[] { "OrderTypeID", "InvStockTransferMaster", "StockTransferID = " + PobjClsDTOPurchase.intReferenceID }).Rows[0]["OrderTypeID"].ToInt32();
                            if (intTransferTypeID == (int)OperationOrderType.STWareHouseToWareHouseTransferDemo)
                            {
                                prmPurchase.Add(new SqlParameter("@DemoQuantity", decQuantity));
                            }
                            else
                            {
                                prmPurchase.Add(new SqlParameter("@ReceivedFromTransfer", decQuantity));
                            }
                        }
                    }
                    else
                    {
                        if (PobjClsDTOPurchase.blnGRNRequired)
                        {
                            prmPurchase.Add(new SqlParameter("@GRNQuantity", decDefaultQty));
                            prmPurchase.Add(new SqlParameter("@InvoicedQuantity", decQuantity));
                        }
                        else
                        {
                            prmPurchase.Add(new SqlParameter("@GRNQuantity", decQuantity + decExtraQuantity));
                            prmPurchase.Add(new SqlParameter("@InvoicedQuantity", decQuantity));
                        }
                    }
                    if (blnIsGRN)
                        prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
                    else
                        prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
                    MobjDataLayer.ExecuteNonQueryWithTran("spInvStockMaster", prmPurchase);
                }
            }
            return true;
            
        }

        /// <summary>
        /// Updates stock summary tables
        /// </summary>
        /// <param name="Mode"> 1 : Adds GRN_Qty with existing qty in summary tables. 2: Substracts GRN_Qty from existing quantity </param>
        /// <param name="objClsDtoPurchaseDetails"></param>
        /// <param name="GRNQty"> Quantity (in base UOM) of the item to be updated</param>
        /// <param name="InvoicedQty"> Invoiced Qty </param>
        /// <returns> returns true if success otherwise false </returns>
        private bool UpdateStockSummary(int Mode, clsDTOPurchaseDetails objClsDtoPurchaseDetails, decimal GRNQty, decimal InvoicedQty)
        {
            // not using now
            prmPurchase = new ArrayList() { 
                    new SqlParameter("@Add", Mode),
                    new SqlParameter("@ItemID", objClsDtoPurchaseDetails.intItemID),
                    new SqlParameter("@BatchID", objClsDtoPurchaseDetails.intBatchID),
                    new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID),
                    new SqlParameter("@GRNQuantity", GRNQty),
                    new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID),
                    new SqlParameter("@StockDate", string.Format("{0:mm/dd/yyyy})", ClsCommonSettings.GetServerDate())),
                    new SqlParameter("@InvoicedQuantity", InvoicedQty)
               };

            // not using now
            //this.MobjDataLayer.ExecuteNonQueryWithTran("STStockSummaryUpdations", prmPurchase);

            return true;
        }

        private Int64 FindBatchID(string strBatchNo)
        {
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 2));
            prmPurchase.Add(new SqlParameter("@BatchNo", strBatchNo));
            DataTable dtBatchDetails = MobjDataLayer.ExecuteDataTable("spInvBatchDetailsTransaction", prmPurchase);
            if (dtBatchDetails.Rows.Count > 0)
                return Convert.ToInt64(dtBatchDetails.Rows[0]["BatchID"]);
            else
                return -1;
        }

        private Int64 BatchUpdation(clsDTOPurchaseDetails objClsDtoPurchaseDetails)
        {
            int intCostingMethodID = 0;
            Int64 intBatchID = 0;
            DataTable datCostingMethod = FillCombos(new string[] { "CostingMethodID", "InvCostingAndPricingDetails", "ItemID = " + objClsDtoPurchaseDetails.intItemID.ToInt32() + "" });
             if (datCostingMethod != null && datCostingMethod.Rows.Count > 0)
             {
                  intCostingMethodID = datCostingMethod.Rows[0]["CostingMethodID"].ToInt32();
             }
             if (!string.IsNullOrEmpty(objClsDtoPurchaseDetails.strBatchNumber) && intCostingMethodID==(int)CostingMethodReference.Batch)
            
             {
                intBatchID = -1;
                Int64 intMaxbatchID = 0;
                prmPurchase = new ArrayList();
                prmPurchase.Add(new SqlParameter("@Mode", 6));
                prmPurchase.Add(new SqlParameter("@ItemID", objClsDtoPurchaseDetails.intItemID));
                intMaxbatchID = MobjDataLayer.ExecuteDataTable("spInvBatchDetailsTransaction", prmPurchase).Rows[0]["MaxBatchID"].ToInt64();
                prmPurchase = new ArrayList();
                //prmPurchase.Add(new SqlParameter("@BatchID", intBatchID));
                prmPurchase.Add(new SqlParameter("@BatchNo", objClsDtoPurchaseDetails.strBatchNumber));
                prmPurchase.Add(new SqlParameter("@ItemID", objClsDtoPurchaseDetails.intItemID));

                decimal decActualRate = objClsDtoPurchaseDetails.decRate;
                decimal decPurchaseRate = objClsDtoPurchaseDetails.decPurchaseRate;

                DataTable dtGetUomDetails = GetUomConversionValues(objClsDtoPurchaseDetails.intUOMID, objClsDtoPurchaseDetails.intItemID);
                if (dtGetUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                    {
                        decActualRate = decActualRate * decConversionValue;
                        decPurchaseRate = decPurchaseRate * decConversionValue;
                    }
                    else if (intConversionFactor == 2)
                    {
                        decActualRate = decActualRate / decConversionValue;
                        decPurchaseRate = decPurchaseRate / decConversionValue;
                    }
                }
                //prmPurchase.Add(new SqlParameter("@ActualRate", decActualRate));
                //prmPurchase.Add(new SqlParameter("@PurchaseRate", decPurchaseRate));
                prmPurchase.Add(new SqlParameter("ExpiryDate", objClsDtoPurchaseDetails.strExpiryDate));
                if (objClsDtoPurchaseDetails.intBatchID == -1)
                {
                    intBatchID = intMaxbatchID + 1;
                    prmPurchase.Add(new SqlParameter("@Mode", 3));
                    prmPurchase.Add(new SqlParameter("@BatchID", intBatchID));
                    prmPurchase.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                    prmPurchase.Add(new SqlParameter("@CurrencyID", PobjClsDTOPurchase.intCurrencyID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spInvBatchDetailsTransaction", prmPurchase);
                }
                else
                {
                    intBatchID = objClsDtoPurchaseDetails.intBatchID;
                    prmPurchase.Add(new SqlParameter("@Mode", 5));
                    prmPurchase.Add(new SqlParameter("@BatchID", objClsDtoPurchaseDetails.intBatchID));
                    prmPurchase.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                    prmPurchase.Add(new SqlParameter("@CurrencyID", PobjClsDTOPurchase.intCurrencyID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spInvBatchDetailsTransaction", prmPurchase);
                }
                
            }
             else
             {

                 intBatchID = 0;
                 objClsDtoPurchaseDetails.intBatchID = intBatchID;
                 ArrayList prmBatch = new ArrayList();
                 if (IsBatchExists(objClsDtoPurchaseDetails.intItemID))
                     prmBatch.Add(new SqlParameter("@Mode", 5));
                 else
                     prmBatch.Add(new SqlParameter("@Mode", 3));
                 if (objClsDtoPurchaseDetails.strExpiryDate != null)
                     prmBatch.Add(new SqlParameter("@ExpiryDate", objClsDtoPurchaseDetails.strExpiryDate));
                 prmBatch.Add(new SqlParameter("@ItemID", objClsDtoPurchaseDetails.intItemID));
                 prmBatch.Add(new SqlParameter("@BatchID", objClsDtoPurchaseDetails.intBatchID));
                 prmBatch.Add(new SqlParameter("@BatchNo", objClsDtoPurchaseDetails.strBatchNumber));
                 prmBatch.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                 prmBatch.Add(new SqlParameter("@CurrencyID", PobjClsDTOPurchase.intCurrencyID));
                 MobjDataLayer.ExecuteNonQueryWithTran("spInvBatchDetailsTransaction", prmBatch);
             }
             return intBatchID;
          
        }

        private Int64 BatchUpdationFromGRN(clsDTOPurchaseDetails objDTOPurchaseReferenceDetails,long PurchaseInvoiceID)
        {
            if (!string.IsNullOrEmpty(objDTOPurchaseReferenceDetails.strBatchNumber))
            {
                Int64 intBatchID = -1;
                Int64 intMaxbatchID = 0;
                //prmPurchase = new ArrayList();
                //prmPurchase.Add(new SqlParameter("@Mode", 6));
                //prmPurchase.Add(new SqlParameter("@ItemID", objClsDtoPurchaseDetails.intItemID));
                ////intMaxbatchID = MobjDataLayer.ExecuteDataTable("spInvBatchDetailsTransaction", prmPurchase).Rows[0]["MaxBatchID"].ToInt64();
                prmPurchase = new ArrayList();
                //prmPurchase.Add(new SqlParameter("@BatchID", intBatchID));
                //prmPurchase.Add(new SqlParameter("@BatchNo", objDTOPurchaseReferenceDetails.strBatchNumber));
                prmPurchase.Add(new SqlParameter("@ItemID", objDTOPurchaseReferenceDetails.intItemID));

                DataTable dtRates = GetPurchaseRates(PurchaseInvoiceID, objDTOPurchaseReferenceDetails.intItemID);
                //decimal decActualRate = objClsDtoPurchaseDetails.decRate;
                //decimal decPurchaseRate = objClsDtoPurchaseDetails.decPurchaseRate;
                if (dtRates.Rows.Count > 0)
                {
                    decimal decActualRate = Convert.ToDecimal(dtRates.Rows[0]["Rate"]);
                    decimal decPurchaseRate = Convert.ToDecimal(dtRates.Rows[0]["PurchaseRate"]);

                    DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtRates.Rows[0]["UOMID"]), objDTOPurchaseReferenceDetails.intItemID);
                    if (dtGetUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                        {
                            decActualRate = decActualRate * decConversionValue;
                            decPurchaseRate = decPurchaseRate * decConversionValue;
                        }
                        else if (intConversionFactor == 2)
                        {
                            decActualRate = decActualRate / decConversionValue;
                            decPurchaseRate = decPurchaseRate / decConversionValue;
                        }
                    }
                    ArrayList prmPurchase1 = new ArrayList();
                    prmPurchase1.Add(new SqlParameter("@ItemID", objDTOPurchaseReferenceDetails.intItemID));
                    //prmPurchase1.Add(new SqlParameter("@ActualRate", decActualRate));
                    //prmPurchase1.Add(new SqlParameter("@PurchaseRate", decPurchaseRate));
                    if (Convert.ToString(dtRates.Rows[0]["ExpiryDate"]) == "")
                    {
                        DateTime? ExpiryDate = null;
                        prmPurchase1.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
                    }
                    else
                        prmPurchase1.Add(new SqlParameter("@ExpiryDate", Convert.ToString(dtRates.Rows[0]["ExpiryDate"])));
                    if (objDTOPurchaseReferenceDetails.intBatchID == -1)
                    {
                        //intBatchID = intMaxbatchID + 1;
                        //prmPurchase1.Add(new SqlParameter("@Mode", 3));
                        //prmPurchase1.Add(new SqlParameter("@BatchID", intBatchID));
                        //prmPurchase1.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                        //prmPurchase1.Add(new SqlParameter("@CurrencyID", PobjClsDTOPurchase.intCurrencyID));
                        //MobjDataLayer.ExecuteNonQueryWithTran("spInvBatchDetailsTransaction", prmPurchase1);
                    }
                    else
                    {
                        intBatchID = objDTOPurchaseReferenceDetails.intBatchID;
                        prmPurchase1.Add(new SqlParameter("@Mode", 5));
                        prmPurchase1.Add(new SqlParameter("@BatchID", objDTOPurchaseReferenceDetails.intBatchID));
                        prmPurchase1.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                        prmPurchase1.Add(new SqlParameter("@CurrencyID", PobjClsDTOPurchase.intCurrencyID));
                        int value = new DataLayer().ExecuteNonQueryWithTran("spInvBatchDetailsTransaction", prmPurchase1);
                    }
                    return intBatchID;
                }
            }
            return 0;
        }

        private bool SavePurchaseGRNDetail(bool blnAddStatus,int OpertionTypeID, long intReferenceID,bool blnCanceStatus)// GRN detail Save
        {
            ArrayList prmPurOrder;
            DataTable dtGRNDetails = DisplayPurchaseGRNDetail(PobjClsDTOPurchase.intPurchaseID);
            if (!blnAddStatus)
            {
                if (blnIsOldStatusCancelled && PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PInvoiceCancelled)
                    DeletePurchaseGRNDetails((int)OperationStatusType.GRNCancelled);
                else
                    DeletePurchaseGRNDetails((int)OperationStatusType.GRNOpened);
            }
            List<int> lstupdateIndices = new List<int>();
            if (!blnAddStatus && !blnIsOldStatusCancelled)
            {
                if (PobjClsDTOPurchase.blnCancelled)
                {
                    for (int i = 0; i < dtGRNDetails.Rows.Count; i++)
                    {
                        clsDTOPurchaseDetails objDTOPurchaseDetails = new clsDTOPurchaseDetails();
                        objDTOPurchaseDetails.intBatchID = Convert.ToInt64(dtGRNDetails.Rows[i]["BatchID"]);
                        objDTOPurchaseDetails.intItemID = Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]);
                        DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtGRNDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]));
                        //if (PobjClsDTOPurchase.intOrderType != (int)OperationOrderType.GRNFromInvoice)
                        //    dtGetUomDetails = GetUomConversionValues(dtGRNDetails.Rows[i]["UOMID"].ToInt32(), dtGRNDetails.Rows[i]["ItemID"].ToInt32(), 1);
                        decimal decOldQuantity = Convert.ToDecimal(dtGRNDetails.Rows[i]["ReceivedQuantity"]);
                        decimal decOldExtraQty = Convert.ToDecimal(dtGRNDetails.Rows[i]["ExtraQuantity"]);
                        if (dtGetUomDetails.Rows.Count > 0)
                        {
                            int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                            decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                            if (intConversionFactor == 1)
                            {
                                decOldQuantity = decOldQuantity / decConversionValue;
                                decOldExtraQty = decOldExtraQty / decConversionValue;
                            }
                            else if (intConversionFactor == 2)
                            {
                                decOldQuantity = decOldQuantity * decConversionValue;
                                decOldExtraQty = decOldExtraQty * decConversionValue;
                            }
                        }
                        int intTempWarehouseID = PobjClsDTOPurchase.intWarehouseID;
                        if (PobjClsDTOPurchase.intTempWarehouseID != PobjClsDTOPurchase.intWarehouseID)
                        {
                            PobjClsDTOPurchase.intWarehouseID = PobjClsDTOPurchase.intTempWarehouseID;
                            StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQty, true, true);
                            PobjClsDTOPurchase.intWarehouseID = intTempWarehouseID;
                        }
                        else
                        {
                            StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQty, true, true);
                        }
                        StockMasterUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQty, true, true);
                    }
                }
                else
                {
                    for (int i = 0; i < dtGRNDetails.Rows.Count; i++)
                    {
                        bool blnExists = false;
                        for (int j = 0; j < PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection.Count; j++)
                        {
                            if (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].strBatchNumber == dtGRNDetails.Rows[i]["BatchNo"].ToString())
                            {
                                PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intBatchID = FindBatchID(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].strBatchNumber);

                                if (PobjClsDTOPurchase.intOrderType != (int)OperationOrderType.GRNFromTransfer)
                                    PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intBatchID = BatchUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j]);

                                DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtGRNDetails.Rows[j]["UOMID"]), Convert.ToInt32(dtGRNDetails.Rows[j]["ItemID"]));
                                //if (PobjClsDTOPurchase.intOrderType != (int)OperationOrderType.GRNFromInvoice)
                                //    dtGetUomDetails = GetUomConversionValues(dtGRNDetails.Rows[j]["UOMID"].ToInt32(), dtGRNDetails.Rows[j]["ItemID"].ToInt32(), 1);

                                decimal decOldQuantity = Convert.ToDecimal(dtGRNDetails.Rows[j]["ReceivedQuantity"]);
                                decimal decOldExtraQuantity = Convert.ToDecimal(dtGRNDetails.Rows[j]["ExtraQuantity"]);
                                if (dtGetUomDetails.Rows.Count > 0)
                                {
                                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                    if (intConversionFactor == 1)
                                    {
                                        decOldQuantity = decOldQuantity / decConversionValue;
                                        decOldExtraQuantity = decOldExtraQuantity / decConversionValue;
                                    }
                                    else if (intConversionFactor == 2)
                                    {
                                        decOldQuantity = decOldQuantity * decConversionValue;
                                        decOldExtraQuantity = decOldExtraQuantity * decConversionValue;
                                    }
                                }

                                StockMasterUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j], decOldQuantity, decOldExtraQuantity, true, false);
                                int intTempWarehouseID = PobjClsDTOPurchase.intWarehouseID;
                                if (PobjClsDTOPurchase.intTempWarehouseID != PobjClsDTOPurchase.intWarehouseID)
                                {
                                    PobjClsDTOPurchase.intWarehouseID = PobjClsDTOPurchase.intTempWarehouseID;
                                    StockDetailsUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j], decOldQuantity, decOldExtraQuantity, true, true);
                                    PobjClsDTOPurchase.intWarehouseID = intTempWarehouseID;
                                    StockDetailsUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j], 0, 0, true, false);
                                }
                                else

                                    StockDetailsUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j], decOldQuantity, decOldExtraQuantity, true, false);
                                // UpdateLastSaleRateOfItem(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decSaleRate, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intUOMID);
                                //prmPurOrder = new ArrayList();
                                decimal OrderQuantity = PurchaseOrderQuantity(intReferenceID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID);
                                int StatusID = 0;
                                //decimal decQtyReceived = GetReceivedQty(Convert.ToInt32(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID), PobjClsDTOPurchase.intPurchaseID, true);
                                if (blnCanceStatus && OpertionTypeID == (int)OperationOrderType.GRNFromOrder)
                                {
                                    if (OrderQuantity == (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decQuantity + PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decOldReceivedQuantity))
                                    {
                                        StatusID = (int)OperationStatusType.POReceived;
                                    }
                                    else if (OrderQuantity != PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decOrderQuantity - PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decQuantity)
                                    {
                                        StatusID = (int)OperationStatusType.POPartiallyReceived;
                                        blnPurchaseOrderMasterStatus = false;
                                    }
                                    bool result = UpdatePurchaseOrderStatus(StatusID, intReferenceID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID);
                                  
                                }
                                else if (!blnCanceStatus && OpertionTypeID == (int)OperationOrderType.GRNFromOrder)
                                {

                                    if (OrderQuantity == PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decOldReceivedQuantity + (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decQuantity - decOldQuantity))
                                    {
                                        StatusID = (int)OperationStatusType.POReceived;                                     
                                    }
                                    else if (OrderQuantity > (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decOldReceivedQuantity +(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decQuantity - decOldQuantity)) && OrderQuantity != 0)
                                    {
                                        StatusID = (int)OperationStatusType.POPartiallyReceived;
                                        blnPurchaseOrderMasterStatus = false;
                                    }
                                    bool result = UpdatePurchaseOrderStatus(StatusID, intReferenceID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID);
                                   
                                }
                                if (blnPurchaseOrderMasterStatus)
                                {
                                    StatusID = (int)OperationStatusType.POReceived;
                                }
                                else
                                {
                                    StatusID = (int)OperationStatusType.POPartiallyReceived;
                                }
                                UpdatePurchaseOrderMasterStatus(StatusID, intReferenceID);
                                    
                                prmPurOrder = new ArrayList();

  
                                prmPurOrder.Add(new SqlParameter("@Mode", 8));
                                prmPurOrder.Add(new SqlParameter("@SerialNo", j + 1));
                                prmPurOrder.Add(new SqlParameter("@GRNID", PobjClsDTOPurchase.intPurchaseID));
                                prmPurOrder.Add(new SqlParameter("@ItemID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID));
                                prmPurOrder.Add(new SqlParameter("@InvoicedQuantity", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decOrderQuantity));
                                prmPurOrder.Add(new SqlParameter("@ReceivedQuantity", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decQuantity));
                                prmPurOrder.Add(new SqlParameter("@ExtraQuantity", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decExtraQuantity));
                                prmPurOrder.Add(new SqlParameter("@UOMID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intUOMID));
                                prmPurOrder.Add(new SqlParameter("@BaseUnitQty", MobjClsCommonUtility.ConvertQtyToBaseUnitQty(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intUOMID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decQuantity + PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decExtraQuantity, 1)));
                                prmPurOrder.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                                prmPurOrder.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID));
                                //   prmPurOrder.Add(new SqlParameter("@LocationID", objClsDtoPurchaseDetails.intLocationID));
                                //  prmPurOrder.Add(new SqlParameter("@LotNumber", ""));
                                prmPurOrder.Add(new SqlParameter("@BatchID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intBatchID));
                                prmPurOrder.Add(new SqlParameter("@ExpiryDate", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].strExpiryDate));
                                prmPurOrder.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
                                prmPurOrder.Add(new SqlParameter("@GRNDate", PobjClsDTOPurchase.strDate));
                                prmPurOrder.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
                                MobjDataLayer.ExecuteNonQueryWithTran("spGRN", prmPurOrder);
                                lstupdateIndices.Add(j);
                                blnExists = true;
                                break;
                            }

                        }
                        if (!blnExists)
                        {
                            clsDTOPurchaseDetails objDTOPurchaseDetails = new clsDTOPurchaseDetails();
                            objDTOPurchaseDetails.intBatchID = Convert.ToInt64(dtGRNDetails.Rows[i]["BatchID"]);
                            objDTOPurchaseDetails.intItemID = Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]);
                            DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtGRNDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]));
                            //if (PobjClsDTOPurchase.intOrderType != (int)OperationOrderType.GRNFromInvoice)
                            //    dtGetUomDetails = GetUomConversionValues(dtGRNDetails.Rows[i]["UOMID"].ToInt32(), dtGRNDetails.Rows[i]["ItemID"].ToInt32(), 1);
                            decimal decOldQuantity = Convert.ToDecimal(dtGRNDetails.Rows[i]["ReceivedQuantity"]);
                            decimal decOldExtraQuantity = Convert.ToDecimal(dtGRNDetails.Rows[i]["ExtraQuantity"]);
                            if (dtGetUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                {
                                    decOldQuantity = decOldQuantity / decConversionValue;
                                    decOldExtraQuantity = decOldExtraQuantity / decConversionValue;
                                }
                                else if (intConversionFactor == 2)
                                {
                                    decOldQuantity = decOldQuantity * decConversionValue;
                                    decOldExtraQuantity = decOldExtraQuantity * decConversionValue;
                                }
                            }
                            int intTempWarehouseID = PobjClsDTOPurchase.intWarehouseID;
                            if (PobjClsDTOPurchase.intTempWarehouseID != PobjClsDTOPurchase.intWarehouseID)
                            {
                                PobjClsDTOPurchase.intWarehouseID = PobjClsDTOPurchase.intTempWarehouseID;
                                StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, true, true);
                                PobjClsDTOPurchase.intWarehouseID = intTempWarehouseID;
                            }
                            else
                                StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, true, true);
                            StockMasterUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, true, true);
                        }
                    }
                }
            }

            for (int i = 0; i < PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection.Count; i++)
            {

                //   string sLotNumber = GetLastLotnumber(objClsDtoPurchaseDetails.intLocationID, PobjClsDTOPurchase.intOrderType);

                //   SaveTempBatchLotDetailsFromPurchaseInvoice(2, objClsDtoPurchaseDetails.intItemID, objClsDtoPurchaseDetails.intLocationID, objClsDtoPurchaseDetails.decReceivedQuantity, objClsDtoPurchaseDetails.decRate, sLotNumber);
                if (!lstupdateIndices.Contains(i))
                {
                    PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intBatchID = FindBatchID(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].strBatchNumber);
                    if (PobjClsDTOPurchase.intOrderType != (int)OperationOrderType.GRNFromTransfer)
                        PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intBatchID = BatchUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i]);
                    if (!PobjClsDTOPurchase.blnCancelled)
                    {
                        //  blnUpdateLIFORate = true;
                        StockMasterUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i], 0, 0, true, false);
                        StockDetailsUpdation(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i], 0, 0, true, false);
                        //  blnUpdateLIFORate = false;
                    }
                    decimal OrderQuantity = PurchaseOrderQuantity(intReferenceID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intItemID);
                    int StatusID = 0;
                    if (blnCanceStatus && OpertionTypeID == (int)OperationOrderType.GRNFromOrder)
                    {
                        if (OrderQuantity == (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decQuantity + PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decOldReceivedQuantity))
                        {
                            StatusID = (int)OperationStatusType.POReceived;
                        }
                        else if (OrderQuantity != PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decOrderQuantity-PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decQuantity )
                        {
                            StatusID = (int)OperationStatusType.POPartiallyReceived;
                            blnPurchaseOrderMasterStatus = false;
                        }
                        bool result = UpdatePurchaseOrderStatus(StatusID, intReferenceID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intItemID);
                     
                    }
                    else if (!blnCanceStatus && OpertionTypeID == (int)OperationOrderType.GRNFromOrder)
                    {
                        if (OrderQuantity == PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decQuantity + PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decOldReceivedQuantity)
                        {
                            StatusID = (int)OperationStatusType.POReceived;
                        }
                        else if (OrderQuantity > (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decQuantity + PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decOldReceivedQuantity) && OrderQuantity != 0)
                        {
                                       
                            StatusID = (int)OperationStatusType.POPartiallyReceived;
                            blnPurchaseOrderMasterStatus = false;
                        }
                        bool result = UpdatePurchaseOrderStatus(StatusID,intReferenceID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intItemID);
                     
                    }
                    if (blnPurchaseOrderMasterStatus && OpertionTypeID == (int)OperationOrderType.GRNFromOrder)
                    {
                        StatusID = (int)OperationStatusType.POReceived;
                    }
                    else if(!blnPurchaseOrderMasterStatus && OpertionTypeID == (int)OperationOrderType.GRNFromOrder)
                    {
                        StatusID = (int)OperationStatusType.POPartiallyReceived;
                    }
                    UpdatePurchaseOrderMasterStatus(StatusID, intReferenceID);
                    
                    prmPurOrder = new ArrayList();

                    prmPurOrder.Add(new SqlParameter("@Mode", 8));
                    prmPurOrder.Add(new SqlParameter("@SerialNo", i + 1));
                    prmPurOrder.Add(new SqlParameter("@GRNID", PobjClsDTOPurchase.intPurchaseID));
                    prmPurOrder.Add(new SqlParameter("@ItemID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intItemID));

                    // UpdateLastSaleRateOfItem(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decSaleRate, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intItemID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intUOMID);

                    prmPurOrder.Add(new SqlParameter("@InvoicedQuantity", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decOrderQuantity));
                    prmPurOrder.Add(new SqlParameter("@ReceivedQuantity", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decQuantity));
                    prmPurOrder.Add(new SqlParameter("@ExtraQuantity", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decExtraQuantity));

                    prmPurOrder.Add(new SqlParameter("@BaseUnitQty", MobjClsCommonUtility.ConvertQtyToBaseUnitQty(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intUOMID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intItemID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decQuantity + PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].decExtraQuantity, 1)));
                    prmPurOrder.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                    prmPurOrder.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID));
                    prmPurOrder.Add(new SqlParameter("@UOMID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intUOMID));
                    prmPurOrder.Add(new SqlParameter("@BatchID", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].intBatchID));
                    prmPurOrder.Add(new SqlParameter("@ExpiryDate", PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[i].strExpiryDate));
                    prmPurOrder.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
                    prmPurOrder.Add(new SqlParameter("@GRNDate", PobjClsDTOPurchase.strDate));
                    prmPurOrder.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
                    MobjDataLayer.ExecuteNonQueryWithTran("spGRN", prmPurOrder);
                }
            }
            if (OpertionTypeID == (int)OperationOrderType.GRNFromOrder)
            {
                int StatusID;
                bool result = PurchaseOrderCheck(intReferenceID);
                if (!result)
                    StatusID = (int)OperationStatusType.POReceived;
                else
                    StatusID = (int)OperationStatusType.POPartiallyReceived;

                UpdatePurchaseOrderMasterStatus(StatusID, intReferenceID);
            }
            blnIsOldStatusCancelled = false;

            UpdateGRNStockValue();

            return true;
        }
        public decimal PurchaseOrderQuantity(long PurchaseID,int ItemID)
        {
            //function for Getting purchase Quantity 

            prmPurchaseDetails = new ArrayList();
            prmPurchaseDetails.Add(new SqlParameter("@Mode", 26));
            prmPurchaseDetails.Add(new SqlParameter("@PurchaseOrderID", PurchaseID));
            prmPurchaseDetails.Add(new SqlParameter("@ItemID",ItemID));
            decimal value=MobjDataLayer.ExecuteScalar("spPurchaseOrder", prmPurchaseDetails).ToDecimal();
            return value;
        }

        public bool PurchaseOrderCheck(long PurchaseID)
        {
            //function for Getting purchase Quantity 

            prmPurchaseDetails = new ArrayList();
            prmPurchaseDetails.Add(new SqlParameter("@Mode", 30));
            prmPurchaseDetails.Add(new SqlParameter("@PurchaseOrderID", PurchaseID));
            object objRowCount = MobjDataLayer.ExecuteScalar("spPurchaseOrder", prmPurchaseDetails);
            return objRowCount.ToInt32() > 0;
        }
        private bool UpdatePurchaseOrderStatus(int StatusID, long PurchaseID, int ItemID)
        {
            //This is to Update Purchase Order Detail status

            ArrayList prmPuchaseStatus = new ArrayList();
            prmPuchaseStatus = new ArrayList();
            prmPuchaseStatus.Add(new SqlParameter("@Mode", 27));
            prmPuchaseStatus.Add(new SqlParameter("@ReceivedStatus", StatusID));
            prmPuchaseStatus.Add(new SqlParameter("@PurchaseOrderID", PurchaseID));
            prmPuchaseStatus.Add(new SqlParameter("@ItemID", ItemID));
            int value = MobjDataLayer.ExecuteNonQuery("spPurchaseOrder", prmPuchaseStatus);
            if (value > 0)
                return true;
            else
                return false;
        }

        private bool UpdatePurchaseOrderMasterStatus(int StatusID, long PurchaseID)
        {
            //This is to Update Purchase Order Master status

            ArrayList prmPuchaseStatus = new ArrayList();
            prmPuchaseStatus = new ArrayList();
            prmPuchaseStatus.Add(new SqlParameter("@Mode", 28));
            prmPuchaseStatus.Add(new SqlParameter("@ReceivedStatus", StatusID));
            prmPuchaseStatus.Add(new SqlParameter("@PurchaseOrderID", PurchaseID));
            int value = MobjDataLayer.ExecuteNonQuery("spPurchaseOrder", prmPuchaseStatus);
            if (value > 0)
                return true;
            else
                return false;
        }

        private bool UpdateGRNStockValue()
        {
            //This is to Update Stock Value

            ArrayList prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation = new ArrayList();
            prmItemStockValueUpdation.Add(new SqlParameter("@Mode", 25));
            prmItemStockValueUpdation.Add(new SqlParameter("@GRNID", PobjClsDTOPurchase.intPurchaseID));
            prmItemStockValueUpdation.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
            prmItemStockValueUpdation.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));

            if (MobjDataLayer.ExecuteNonQueryWithTran("spGRN", prmItemStockValueUpdation) != 0)
                return true;
            return false;
        }

        public bool SavePurchaseGRN(bool blnAddStatus,bool blnCancelStatus)// GRN Save
        {
            object objOutPurchase = 0;
            //DeleteItemLocationDetails(PobjClsDTOPurchase.intPurchaseID, true);
            ArrayList parameters = new ArrayList();
            if (blnAddStatus)
            {
                blnIsOldStatusCancelled = false;
                parameters.Add(new SqlParameter("@Mode", 2));
            }
            else
            {
                ArrayList prmPurOrder = new ArrayList();
                prmPurOrder.Add(new SqlParameter("@Mode", 1));
                prmPurOrder.Add(new SqlParameter("@GRNID", PobjClsDTOPurchase.intPurchaseID));
                DataTable datCompany = MobjDataLayer.ExecuteDataTable("spGRN", prmPurOrder);

                if (datCompany.Rows.Count > 0)
                {
                    int intStatusId = -1;
                    if (datCompany.Rows[0]["StatusID"] != DBNull.Value) intStatusId = Convert.ToInt32(datCompany.Rows[0]["StatusID"]);
                    if (PobjClsDTOPurchase.blnCancelled && intStatusId == PobjClsDTOPurchase.intStatusID)
                        blnIsOldStatusCancelled = true;
                    else if (!PobjClsDTOPurchase.blnCancelled && intStatusId != PobjClsDTOPurchase.intStatusID)
                        blnIsOldStatusCancelled = true;

                }
                parameters.Add(new SqlParameter("@Mode", 3));
            }
            parameters.Add(new SqlParameter("@GRNID", PobjClsDTOPurchase.intPurchaseID));
            parameters.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID));
            parameters.Add(new SqlParameter("@ReferenceID ", PobjClsDTOPurchase.intReferenceID));
            parameters.Add(new SqlParameter("@GRNNo", PobjClsDTOPurchase.strPurchaseNo));
            parameters.Add(new SqlParameter("@GRNDate", PobjClsDTOPurchase.strDate));
            parameters.Add(new SqlParameter("@OrderTypeID", PobjClsDTOPurchase.intOrderType));
            parameters.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
            if (PobjClsDTOPurchase.intVendorID != 0)
                parameters.Add(new SqlParameter("@VendorID", PobjClsDTOPurchase.intVendorID));
            parameters.Add(new SqlParameter("@DueDate", PobjClsDTOPurchase.strDueDate));
            if (PobjClsDTOPurchase.intVendorAddID != 0)
                parameters.Add(new SqlParameter("@VendorAddID", PobjClsDTOPurchase.intVendorAddID));
            parameters.Add(new SqlParameter("@Remarks", PobjClsDTOPurchase.strRemarks));
            parameters.Add(new SqlParameter("@CreatedBy", PobjClsDTOPurchase.intCreatedBy));
            parameters.Add(new SqlParameter("@CreatedDate", PobjClsDTOPurchase.strCreatedDate));
            parameters.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));

            parameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            if (MobjDataLayer.ExecuteNonQueryWithTran("spGRN", parameters, out objOutPurchase) != 0)
            {
                if (Convert.ToInt64(objOutPurchase) > 0)
                {
                    PobjClsDTOPurchase.intPurchaseID = Convert.ToInt64(objOutPurchase);
                    DeleteCancellationDetails(3);
                    if (PobjClsDTOPurchase.blnCancelled)
                    {
                        SaveCancellationDetails(3);
                        // DeleteItemLocationDetails(PobjClsDTOPurchase.intPurchaseID, true);
                    }
                    if (!blnAddStatus)
                    {
                        if (blnIsOldStatusCancelled)
                            DeletePurchaseLocationDetails(true, true);
                        else
                            DeletePurchaseLocationDetails(true, false);
                    }
                    if (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection.Count > 0)
                    {
                        SavePurchaseGRNDetail(blnAddStatus, PobjClsDTOPurchase.intOrderType, PobjClsDTOPurchase.intReferenceID, blnCancelStatus);

                        if (PobjClsDTOPurchase.blnCancelled)
                        {
                            if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice)
                                UpdateStatus(4, (Int32)OperationStatusType.PInvoiceOpened, PobjClsDTOPurchase.intReferenceID);
                            else if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromTransfer)
                                UpdateStatus(6, (int)OperationStatusType.STIssued, PobjClsDTOPurchase.intReferenceID);

                            SavePurchaseLocationDetails(true, true);
                        }
                        else
                        {
                            if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice)
                            {
                                if (GetPurchaseInvoiceStatus(PobjClsDTOPurchase.intReferenceID))
                                {
                                    UpdateStatus(4, (Int32)OperationStatusType.PInvoiceClosed, PobjClsDTOPurchase.intReferenceID);
                                }
                                else
                                {
                                    UpdateStatus(4, (Int32)OperationStatusType.PInvoiceOpened, PobjClsDTOPurchase.intReferenceID);
                                }
                            }
                            else if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromTransfer)
                                UpdateStatus(6, (int)OperationStatusType.STClosed, PobjClsDTOPurchase.intReferenceID);
                            SavePurchaseLocationDetails(true, false);
                        }
                    }
                    if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice)
                        SaveCostingMethodDetails(PobjClsDTOPurchase.intPurchaseID.ToInt32(), "GRN", (int)OperationOrderType.GRNFromInvoice); // Saving Costing Method & Pricing Scheme Details
                    else if(PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromOrder)
                        SaveCostingMethodDetails(PobjClsDTOPurchase.intPurchaseID.ToInt32(), "GRN", (int)OperationOrderType.GRNFromOrder);
                    //  OpeningStockAccountUpdationForGRN(blnAddStatus);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public DataTable GetOrderNumber(int iComID, string sNo) // Get Order Number For searching
        {

            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 5));
            prmCommon.Add(new SqlParameter("@CompanyID", iComID));
            prmCommon.Add(new SqlParameter("@VendorID", PobjClsDTOPurchase.intVendorID));
            prmCommon.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
            prmCommon.Add(new SqlParameter("@SearchNo", sNo));
            return MobjDataLayer.ExecuteDataSet("spPurchaseOrder", prmCommon).Tables[0];

        }

        public DataTable GetInvoiceNumber(int iComID, string sNo) // Get Invoice Number For searching
        {

            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 5));
            prmCommon.Add(new SqlParameter("@CompanyID", iComID));
            prmCommon.Add(new SqlParameter("@VendorID", PobjClsDTOPurchase.intVendorID));
            prmCommon.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
            prmCommon.Add(new SqlParameter("@SearchNo", sNo));
            return MobjDataLayer.ExecuteDataSet("spPurchaseInvoice", prmCommon).Tables[0];

        }

        public DataTable GetGRNNumber(int iComID, string sNo) // Get GRN Number For searching
        {

            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 5));
            prmCommon.Add(new SqlParameter("@CompanyID", iComID));
            prmCommon.Add(new SqlParameter("@VendorID", PobjClsDTOPurchase.intVendorID));
            prmCommon.Add(new SqlParameter("@SearchNo", sNo));
            return MobjDataLayer.ExecuteDataSet("spGRN", prmCommon).Tables[0];

        }

        public DataTable DisplayPurchaseOrderDetail(Int64 Rownum) // Purchase Order detail
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 8));
            prmCommon.Add(new SqlParameter("@PurchaseOrderID", Rownum));
            return MobjDataLayer.ExecuteDataSet("spPurchaseOrder", prmCommon).Tables[0];

        }

        public DataTable DisplayPurchaseInvoiceDetail(Int64 Rownum)// Display Invoice detail
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 7));
            prmCommon.Add(new SqlParameter("@PurchaseInvoiceID", Rownum));
            return MobjDataLayer.ExecuteDataSet("spPurchaseInvoice", prmCommon).Tables[0];

        }

        public DataTable DisplayPurchaseGRNDetail(Int64 Rownum)// Display GRN detail
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 7));
            prmCommon.Add(new SqlParameter("@GRNID", Rownum));
            return MobjDataLayer.ExecuteDataSet("spGRN", prmCommon).Tables[0];

        }

        public bool DisplayPurchaseOrderInfo(Int64 iOrderID) // Purchse Order
        {


            ArrayList prmPurOrder = new ArrayList();

            prmPurOrder.Add(new SqlParameter("@Mode", 1));

            prmPurOrder.Add(new SqlParameter("@PurchaseOrderID", iOrderID));
            SqlDataReader sdrPurchaseOrder = MobjDataLayer.ExecuteReader("spPurchaseOrder", prmPurOrder, CommandBehavior.SingleRow);

            if (sdrPurchaseOrder.Read())
            {
                PobjClsDTOPurchase.intPurchaseID = iOrderID;
                PobjClsDTOPurchase.intCompanyID = sdrPurchaseOrder["CompanyID"].ToInt32();
                //PobjClsDTOPurchase.intReferenceID = sdrPurchaseOrder["ReferenceID"].ToInt64();
                PobjClsDTOPurchase.intOrderType = sdrPurchaseOrder["OrderTypeID"].ToInt32();
                PobjClsDTOPurchase.strPurchaseNo = Convert.ToString(sdrPurchaseOrder["PurchaseOrderNo"]);
                PobjClsDTOPurchase.blnGRNRequired = Convert.ToBoolean(sdrPurchaseOrder["IsGRNRequired"]);
                PobjClsDTOPurchase.intVendorID = sdrPurchaseOrder["VendorID"].ToInt32();
                PobjClsDTOPurchase.intVendorAddID = sdrPurchaseOrder["VendorAddID"].ToInt32();
                if (sdrPurchaseOrder["VendorName"] != DBNull.Value) PobjClsDTOPurchase.strVendorName = Convert.ToString(sdrPurchaseOrder["VendorName"]); else PobjClsDTOPurchase.strVendorName = "";

                if (sdrPurchaseOrder["TaxSchemeID"] != DBNull.Value) PobjClsDTOPurchase.intTaxSchemeID = Convert.ToInt32(sdrPurchaseOrder["TaxSchemeID"]); else PobjClsDTOPurchase.intTaxSchemeID = 0;
                if (sdrPurchaseOrder["TaxAmount"] != DBNull.Value) PobjClsDTOPurchase.decTaxAmount = Convert.ToDecimal(sdrPurchaseOrder["TaxAmount"]); else PobjClsDTOPurchase.decTaxAmount = 0;
              
                if (sdrPurchaseOrder["AddressName"] != DBNull.Value) PobjClsDTOPurchase.strAddressName = Convert.ToString(sdrPurchaseOrder["AddressName"]); else PobjClsDTOPurchase.strAddressName = "";
                if (sdrPurchaseOrder["PaymentTermsID"] != DBNull.Value) PobjClsDTOPurchase.intPaymentTermsID = Convert.ToInt32(sdrPurchaseOrder["PaymentTermsID"]); else PobjClsDTOPurchase.intPaymentTermsID = 0;
                if (sdrPurchaseOrder["CurrencyID"] != DBNull.Value) PobjClsDTOPurchase.intCurrencyID = Convert.ToInt32(sdrPurchaseOrder["CurrencyID"]); else PobjClsDTOPurchase.intCurrencyID = 0;
                PobjClsDTOPurchase.strDate = Convert.ToString(sdrPurchaseOrder["OrderDate"]);
                PobjClsDTOPurchase.strDueDate = Convert.ToString(sdrPurchaseOrder["DueDate"]);
                if (sdrPurchaseOrder["StatusID"] != DBNull.Value) PobjClsDTOPurchase.intStatusID = Convert.ToInt32(sdrPurchaseOrder["StatusID"]); else PobjClsDTOPurchase.intStatusID = 0;
                if (sdrPurchaseOrder["Remarks"] != DBNull.Value) PobjClsDTOPurchase.strRemarks = Convert.ToString(sdrPurchaseOrder["Remarks"]); else PobjClsDTOPurchase.strRemarks = "";
                PobjClsDTOPurchase.strEmployeeName = Convert.ToString(sdrPurchaseOrder["EmployeeName"]);
                if (sdrPurchaseOrder["IsDiscountPercentage"] != DBNull.Value)
                {
                    PobjClsDTOPurchase.blnGrandDiscount = Convert.ToBoolean(sdrPurchaseOrder["IsDiscountPercentage"]);
                    PobjClsDTOPurchase.decGrandDiscountAmt = sdrPurchaseOrder["GrandDiscountAmount"].ToDecimal();
                    PobjClsDTOPurchase.decGrandDiscountPercent = sdrPurchaseOrder["GrandDiscountPercentage"].ToDecimal();

                }

                //if (sdrPurchaseOrder["GrandDiscountID"] != DBNull.Value) PobjClsDTOPurchase.intGrandDiscountID = Convert.ToInt32(sdrPurchaseOrder["GrandDiscountID"]); else PobjClsDTOPurchase.intGrandDiscountID = 0;
                //if (sdrPurchaseOrder["GrandDiscountAmount"] != DBNull.Value) PobjClsDTOPurchase.decGrandDiscountAmount = Convert.ToDecimal(sdrPurchaseOrder["GrandDiscountAmount"]); else PobjClsDTOPurchase.decGrandDiscountAmount = 0;

                if (sdrPurchaseOrder["ExpenseAmount"] != DBNull.Value) PobjClsDTOPurchase.decExpenseAmount = Convert.ToDecimal(sdrPurchaseOrder["ExpenseAmount"]); else PobjClsDTOPurchase.decExpenseAmount = 0;
                if (sdrPurchaseOrder["GrandAmount"] != DBNull.Value) PobjClsDTOPurchase.decGrandAmount = Convert.ToDecimal(sdrPurchaseOrder["GrandAmount"]); else PobjClsDTOPurchase.decGrandAmount = 0;
                if (sdrPurchaseOrder["NetAmount"] != DBNull.Value) PobjClsDTOPurchase.decNetAmount = Convert.ToDecimal(sdrPurchaseOrder["NetAmount"]); else PobjClsDTOPurchase.decNetAmount = 0;
                if (sdrPurchaseOrder["CreatedDate"] != DBNull.Value) PobjClsDTOPurchase.strCreatedDate = Convert.ToString(sdrPurchaseOrder["CreatedDate"]); else PobjClsDTOPurchase.strCreatedDate = "";
                DataTable datCancellationDetails = DisplayCancellationDetails(2);

                if (datCancellationDetails != null && datCancellationDetails.Rows.Count > 0)
                {
                    PobjClsDTOPurchase.strDescription = Convert.ToString(datCancellationDetails.Rows[0]["Description"]);
                    PobjClsDTOPurchase.strCancellationDate = Convert.ToString(datCancellationDetails.Rows[0]["Date"]);
                    PobjClsDTOPurchase.strCancelledBy = Convert.ToString(datCancellationDetails.Rows[0]["CancelledByName"]);
                    PobjClsDTOPurchase.intCancelledBy = Convert.ToInt32(datCancellationDetails.Rows[0]["CancelledBy"]);
                }

                if (sdrPurchaseOrder["ApprovedDate"] != DBNull.Value) PobjClsDTOPurchase.strApprovedDate = Convert.ToString(sdrPurchaseOrder["ApprovedDate"]); else PobjClsDTOPurchase.strApprovedDate = "";
                if (sdrPurchaseOrder["ApprovedByName"] != DBNull.Value) PobjClsDTOPurchase.strApprovedBy = Convert.ToString(sdrPurchaseOrder["ApprovedByName"]); else PobjClsDTOPurchase.strApprovedBy = "";
                PobjClsDTOPurchase.intApprovedBy = sdrPurchaseOrder["ApprovedBy"].ToInt32();

                if (sdrPurchaseOrder["VerificationDescription"] != DBNull.Value) PobjClsDTOPurchase.strVerifcationDescription = Convert.ToString(sdrPurchaseOrder["VerificationDescription"]); else PobjClsDTOPurchase.strVerifcationDescription = "";


                //if (sdrPurchaseOrder["IncoTermsID"] != DBNull.Value)
                //    PobjClsDTOPurchase.intIncoTerms = Convert.ToInt32(sdrPurchaseOrder["IncoTermsID"]);
                //else
                //    PobjClsDTOPurchase.intIncoTerms = 0;
                //if (sdrPurchaseOrder["LeadTimeDetsID"] != DBNull.Value)
                //    PobjClsDTOPurchase.intLeadTimeDetsID = Convert.ToInt32(sdrPurchaseOrder["LeadTimeDetsID"]);
                //if (sdrPurchaseOrder["LeadTime"] != DBNull.Value)
                //    PobjClsDTOPurchase.decLeadTime = Convert.ToDecimal(sdrPurchaseOrder["LeadTime"]);
                //if (sdrPurchaseOrder["ProductionDate"] != DBNull.Value)
                //    PobjClsDTOPurchase.strProductionDate = Convert.ToString(sdrPurchaseOrder["ProductionDate"]);
                //if (sdrPurchaseOrder["TranshipmentDate"] != DBNull.Value)
                //    PobjClsDTOPurchase.strTranShipmentDate = Convert.ToString(sdrPurchaseOrder["TranshipmentDate"]);
                //if (sdrPurchaseOrder["ClearenceDate"] != DBNull.Value)
                //    PobjClsDTOPurchase.strClearenceDate = Convert.ToString(sdrPurchaseOrder["ClearenceDate"]);

                //if (sdrPurchaseOrder["ContainerTypeID"] != DBNull.Value)
                //    PobjClsDTOPurchase.intContainerTypeID = sdrPurchaseOrder["ContainerTypeID"].ToInt32();
                //else
                //    PobjClsDTOPurchase.intContainerTypeID = 0;

                //if (sdrPurchaseOrder["NoOfContainers"] != DBNull.Value)
                //    PobjClsDTOPurchase.intNoOfContainers = sdrPurchaseOrder["NoOfContainers"].ToInt32();

                if (sdrPurchaseOrder["VerificationCriteria"] != DBNull.Value)
                    PobjClsDTOPurchase.strVerificationCriteria = Convert.ToString(sdrPurchaseOrder["VerificationCriteria"]);
                sdrPurchaseOrder.Close();
                return true;
            }
            else
            {
                sdrPurchaseOrder.Close();
                return false;
            }
        }

        public void IsGRNRequired() // Purchse Order
        {
            ArrayList prmPurOrder = new ArrayList();

            prmPurOrder.Add(new SqlParameter("@Mode", 1));

            prmPurOrder.Add(new SqlParameter("@PurchaseOrderID", PobjClsDTOPurchase.intPurchaseOrderID));
            DataTable datCompany = MobjDataLayer.ExecuteDataTable("spPurchaseOrder", prmPurOrder);

            if (datCompany.Rows.Count > 0)
            {
                PobjClsDTOPurchase.blnGRNRequired = Convert.ToBoolean(datCompany.Rows[0]["IsGRNRequired"]);
            }
        }

        public bool DisplayPurchaseInvoiceInfo(Int64 iOrderID) // Purchase Invoice
        {

            ArrayList prmPurOrder = new ArrayList();

            prmPurOrder.Add(new SqlParameter("@Mode", 1));
            prmPurOrder.Add(new SqlParameter("@PurchaseInvoiceID", iOrderID));
            prmPurOrder.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));

            SqlDataReader sdrInvoice = MobjDataLayer.ExecuteReader("spPurchaseInvoice", prmPurOrder, CommandBehavior.SingleRow);

            if (sdrInvoice.Read())
            {
                PobjClsDTOPurchase.intPurchaseID = iOrderID;
                PobjClsDTOPurchase.intCompanyID = Convert.ToInt32(sdrInvoice["CompanyID"]);
                PobjClsDTOPurchase.intOrderType = Convert.ToInt32(sdrInvoice["OrderTypeID"]);
                PobjClsDTOPurchase.intWarehouseID = Convert.ToInt32(sdrInvoice["WarehouseID"]);
                PobjClsDTOPurchase.strPurchaseNo = Convert.ToString(sdrInvoice["PurchaseInvoiceNo"]);
                PobjClsDTOPurchase.strPurchaseBillNo = Convert.ToString(sdrInvoice["PurchaseBillNo"]);
                PobjClsDTOPurchase.intTaxSchemeID =sdrInvoice["TaxSchemeID"]==DBNull.Value? 0 : Convert.ToInt32(sdrInvoice["TaxSchemeID"]);
                //PobjClsDTOPurchase.strTRNNo = Convert.ToString(sdrInvoice["TRNNo"]);
                
                if (PobjClsDTOPurchase.intOrderType != 26)
                {
                    PobjClsDTOPurchase.intPurchaseOrderID = Convert.ToInt32(sdrInvoice["PurchaseOrderID"]);
                    if (sdrInvoice["PurchaseOrderID"] != DBNull.Value) PobjClsDTOPurchase.intPurchaseOrderID = Convert.ToInt32(sdrInvoice["PurchaseOrderID"]);
                }
                if (sdrInvoice["CompanyID"] != DBNull.Value) PobjClsDTOPurchase.intCompanyID = Convert.ToInt32(sdrInvoice["CompanyID"]); else PobjClsDTOPurchase.intCompanyID = 0;
                if (sdrInvoice["VendorID"] != DBNull.Value) PobjClsDTOPurchase.intVendorID = Convert.ToInt32(sdrInvoice["VendorID"]); else PobjClsDTOPurchase.intVendorID = 0;
                if (sdrInvoice["VendorName"] != DBNull.Value) PobjClsDTOPurchase.strVendorName = Convert.ToString(sdrInvoice["VendorName"]); else PobjClsDTOPurchase.strVendorName = "";
                if (sdrInvoice["VendorAddID"] != DBNull.Value) PobjClsDTOPurchase.intVendorAddID = Convert.ToInt32(sdrInvoice["VendorAddID"]); else PobjClsDTOPurchase.intVendorAddID = 0;
                if (sdrInvoice["AddressName"] != DBNull.Value) PobjClsDTOPurchase.strAddressName = Convert.ToString(sdrInvoice["AddressName"]); else PobjClsDTOPurchase.strAddressName = "";
                if (sdrInvoice["PaymentTermsID"] != DBNull.Value) PobjClsDTOPurchase.intPaymentTermsID = Convert.ToInt32(sdrInvoice["PaymentTermsID"]); else PobjClsDTOPurchase.intPaymentTermsID = 0;
                if (sdrInvoice["CurrencyID"] != DBNull.Value) PobjClsDTOPurchase.intCurrencyID = Convert.ToInt32(sdrInvoice["CurrencyID"]); else PobjClsDTOPurchase.intCurrencyID = 0;
                PobjClsDTOPurchase.strEmployeeName = Convert.ToString(sdrInvoice["EmployeeName"]);
                if (sdrInvoice["IsGRNRequired"] != DBNull.Value)
                    PobjClsDTOPurchase.blnGRNRequired = Convert.ToBoolean(sdrInvoice["IsGRNRequired"]);
                else
                    PobjClsDTOPurchase.blnGRNRequired = true;

                PobjClsDTOPurchase.strDate = Convert.ToString(sdrInvoice["InvoiceDate"]);
                PobjClsDTOPurchase.strDueDate = Convert.ToString(sdrInvoice["DueDate"]);
                PobjClsDTOPurchase.strRemarks = Convert.ToString(sdrInvoice["Remarks"]);
                if (sdrInvoice["AccountID"] != DBNull.Value) PobjClsDTOPurchase.intPurchaseAccountID = Convert.ToInt32(sdrInvoice["AccountID"]); else PobjClsDTOPurchase.intPurchaseAccountID = 0;
                if (sdrInvoice["IsDiscountPercentage"] != DBNull.Value)
                {
                    PobjClsDTOPurchase.blnGrandDiscount = Convert.ToBoolean(sdrInvoice["IsDiscountPercentage"]);
                    PobjClsDTOPurchase.decGrandDiscountAmt = sdrInvoice["GrandDiscountAmount"].ToDecimal();
                    PobjClsDTOPurchase.decGrandDiscountPercent = sdrInvoice["GrandDiscountPercentage"].ToDecimal();

                }
                else
                {
                    PobjClsDTOPurchase.blnGrandDiscount = null;

                }
                //if (sdrInvoice["GrandDiscountID"] != DBNull.Value) PobjClsDTOPurchase.intGrandDiscountID = Convert.ToInt32(sdrInvoice["GrandDiscountID"]); else PobjClsDTOPurchase.intGrandDiscountID = 0;
                if (sdrInvoice["ExpenseAmount"] != DBNull.Value) PobjClsDTOPurchase.decExpenseAmount = Convert.ToDecimal(sdrInvoice["ExpenseAmount"]); else PobjClsDTOPurchase.decExpenseAmount = 0;
                if (sdrInvoice["TaxAmount"] != DBNull.Value) PobjClsDTOPurchase.decTaxAmount = Convert.ToDecimal(sdrInvoice["TaxAmount"]); else PobjClsDTOPurchase.decTaxAmount = 0;

                //if (sdrInvoice["GrandDiscountAmount"] != DBNull.Value) PobjClsDTOPurchase.decGrandDiscountAmount = Convert.ToDecimal(sdrInvoice["GrandDiscountAmount"]); else PobjClsDTOPurchase.decGrandDiscountAmount = 0;
                if (sdrInvoice["GrandAmount"] != DBNull.Value) PobjClsDTOPurchase.decGrandAmount = Convert.ToDecimal(sdrInvoice["GrandAmount"]); else PobjClsDTOPurchase.decGrandAmount = 0;
                if (sdrInvoice["NetAmount"] != DBNull.Value) PobjClsDTOPurchase.decNetAmount = Convert.ToDecimal(sdrInvoice["NetAmount"]); else PobjClsDTOPurchase.decNetAmount = 0;
                // if (sdrInvoice["VoucherAmount"] != DBNull.Value) PobjClsDTOPurchase.decVoucherAmount = Convert.ToDecimal(sdrInvoice["VoucherAmount"]); else PobjClsDTOPurchase.decVoucherAmount = 0;
                if (sdrInvoice["StatusID"] != DBNull.Value) PobjClsDTOPurchase.intStatusID = Convert.ToInt32(sdrInvoice["StatusID"]); else PobjClsDTOPurchase.intStatusID = 0;
                DataTable datCancellationDetails = DisplayCancellationDetails(4);

                if (datCancellationDetails != null && datCancellationDetails.Rows.Count > 0)
                {
                    PobjClsDTOPurchase.strDescription = Convert.ToString(datCancellationDetails.Rows[0]["Description"]);
                    PobjClsDTOPurchase.strCancellationDate = Convert.ToString(datCancellationDetails.Rows[0]["Date"]);
                    PobjClsDTOPurchase.intCancelledBy = Convert.ToInt32(datCancellationDetails.Rows[0]["CancelledBy"]);
                    PobjClsDTOPurchase.strCancelledBy = Convert.ToString(datCancellationDetails.Rows[0]["CancelledByName"]);
                }

                PobjClsDTOPurchase.intCreatedBy = Convert.ToInt32(sdrInvoice["CreatedBy"]);
                PobjClsDTOPurchase.strCreatedDate = Convert.ToString(sdrInvoice["CreatedDate"]);

                sdrInvoice.Close();

                return true;
            }
            else
            {
                sdrInvoice.Close();
                return false;
            }
        }

        public bool DisplayPurchaseGRNInfo(Int64 iOrderID) // GRN Master
        {

            ArrayList prmPurOrder = new ArrayList();

            prmPurOrder.Add(new SqlParameter("@Mode", 1));
            prmPurOrder.Add(new SqlParameter("@GRNID", iOrderID));
            DataTable datGRN = MobjDataLayer.ExecuteDataTable("spGRN", prmPurOrder);

            if (datGRN.Rows.Count > 0)
            {
                PobjClsDTOPurchase.intPurchaseID = iOrderID;
                PobjClsDTOPurchase.intCompanyID = Convert.ToInt32(datGRN.Rows[0]["CompanyID"]);
                PobjClsDTOPurchase.intOrderType = Convert.ToInt32(datGRN.Rows[0]["OrderTypeID"]);
                PobjClsDTOPurchase.intReferenceID = Convert.ToInt64(datGRN.Rows[0]["ReferenceID"]);
                PobjClsDTOPurchase.intWarehouseID = Convert.ToInt32(datGRN.Rows[0]["WarehouseID"]);
                PobjClsDTOPurchase.strPurchaseNo = Convert.ToString(datGRN.Rows[0]["GRNNo"]);
                if (datGRN.Rows[0]["ReferenceID"] != DBNull.Value) PobjClsDTOPurchase.intReferenceID = Convert.ToInt32(datGRN.Rows[0]["ReferenceID"]);
                if (datGRN.Rows[0]["CompanyID"] != DBNull.Value) PobjClsDTOPurchase.intCompanyID = Convert.ToInt32(datGRN.Rows[0]["CompanyID"]); else PobjClsDTOPurchase.intCompanyID = 0;
                if (datGRN.Rows[0]["VendorID"] != DBNull.Value) PobjClsDTOPurchase.intVendorID = Convert.ToInt32(datGRN.Rows[0]["VendorID"]); else PobjClsDTOPurchase.intVendorID = 0;
                if (datGRN.Rows[0]["VendorName"] != DBNull.Value) PobjClsDTOPurchase.strVendorName = Convert.ToString(datGRN.Rows[0]["VendorName"]); else PobjClsDTOPurchase.strVendorName = "";
                if (datGRN.Rows[0]["VendorAddID"] != DBNull.Value) PobjClsDTOPurchase.intVendorAddID = Convert.ToInt32(datGRN.Rows[0]["VendorAddID"]); else PobjClsDTOPurchase.intVendorAddID = 0;
                if (datGRN.Rows[0]["AddressName"] != DBNull.Value) PobjClsDTOPurchase.strAddressName = Convert.ToString(datGRN.Rows[0]["AddressName"]); else PobjClsDTOPurchase.strAddressName = "";
                PobjClsDTOPurchase.strEmployeeName = Convert.ToString(datGRN.Rows[0]["EmployeeName"]);
                PobjClsDTOPurchase.strDate = Convert.ToString(datGRN.Rows[0]["GRNDate"]);
                PobjClsDTOPurchase.strDueDate = Convert.ToString(datGRN.Rows[0]["DueDate"]);
                PobjClsDTOPurchase.strRemarks = Convert.ToString(datGRN.Rows[0]["Remarks"]);

                if (datGRN.Rows[0]["StatusID"] != DBNull.Value) PobjClsDTOPurchase.intStatusID = Convert.ToInt32(datGRN.Rows[0]["StatusID"]); else PobjClsDTOPurchase.intStatusID = 0;
                DataTable datCancellationDetails = DisplayCancellationDetails(3);

                if (datCancellationDetails != null && datCancellationDetails.Rows.Count > 0)
                {
                    PobjClsDTOPurchase.strDescription = Convert.ToString(datCancellationDetails.Rows[0]["Description"]);
                    PobjClsDTOPurchase.strCancellationDate = Convert.ToString(datCancellationDetails.Rows[0]["Date"]);
                    PobjClsDTOPurchase.intCancelledBy = Convert.ToInt32(datCancellationDetails.Rows[0]["CancelledBy"]);
                    PobjClsDTOPurchase.strCancelledBy = Convert.ToString(datCancellationDetails.Rows[0]["CancelledByName"]);
                }

                PobjClsDTOPurchase.intCreatedBy = Convert.ToInt32(datGRN.Rows[0]["CreatedBy"]);
                PobjClsDTOPurchase.strCreatedDate = Convert.ToString(datGRN.Rows[0]["CreatedDate"]);

                return true;
            }
            else
            {
                return false;
            }
        }

        public string DisplayVendorInformation(int VendorID)
        {
            string sAddress = "";
            ArrayList prmVendor = new ArrayList();

            prmVendor.Add(new SqlParameter("@Mode", 6));
            prmVendor.Add(new SqlParameter("@VendorID", VendorID));
            SqlDataReader drVendor = MobjDataLayer.ExecuteReader("spPurchaseOrder", prmVendor, CommandBehavior.SingleRow);

            if (drVendor.Read())
            {

                sAddress = Convert.ToString(drVendor["Contact"]) + " \n" + Convert.ToString(drVendor["MailingAddress"]) +
                           " \n" + Convert.ToString(drVendor["Country"]) + " \n" + Convert.ToString(drVendor["Email"]) +
                           " \n" + Convert.ToString(drVendor["Telephone"]) + " \n" + Convert.ToString(drVendor["Fax"]) +
                           " \n" + Convert.ToString(drVendor["Website"]);

                drVendor.Close();
                return sAddress;
            }
            else
            {
                drVendor.Close();
                return sAddress;
            }

        }

        public string DisplayAddressInformation(int iVendorAdd)
        {
            string sAddress = "";
            ArrayList prmVendor = new ArrayList();

            prmVendor.Add(new SqlParameter("@Mode", 7));
            prmVendor.Add(new SqlParameter("@VendorID", iVendorAdd));
            SqlDataReader drVendor = MobjDataLayer.ExecuteReader("spPurchaseOrder", prmVendor, CommandBehavior.SingleRow);

            if (drVendor.Read())
            {
                sAddress = Environment.NewLine;
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["ContactPerson"])))
                {
                    sAddress += Convert.ToString(drVendor["ContactPerson"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Address"])))
                {
                    sAddress += Convert.ToString(drVendor["Address"]);
                    sAddress += Environment.NewLine;
                }

                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["State"])))
                {
                    sAddress += Convert.ToString(drVendor["State"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Country"])))
                {
                    sAddress += Convert.ToString(drVendor["Country"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["ZipCode"])))
                {
                    sAddress += "ZipCode : " + Convert.ToString(drVendor["ZipCode"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["Telephone"])))
                {
                    sAddress += "Telephone : " + Convert.ToString(drVendor["Telephone"]);
                    sAddress += Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(drVendor["MobileNo"])))
                {
                    sAddress += "Mobile : " + Convert.ToString(drVendor["MobileNo"]);
                    sAddress += Environment.NewLine;
                }
                //sAddress = Convert.ToString(drVendor["ContactPerson"]) + Environment.NewLine + Convert.ToString(drVendor["Address"]) +
                //           Environment.NewLine  + Convert.ToString(drVendor["ZipCode"]) + Environment.NewLine + Convert.ToString(drVendor["State"]) +
                //           Environment.NewLine + Convert.ToString(drVendor["Country"]) + Environment.NewLine + Convert.ToString(drVendor["Telephone"]) +
                //           Environment.NewLine + Convert.ToString(drVendor["Fax"]);

                drVendor.Close();
                return sAddress;

            }
            else
            {
                drVendor.Close();
                return sAddress;
            }

        }

        public int GetDefaultUomID(int iItemID)
        {
            object objOutUomID = 0;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@ItemID", iItemID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvPurchaseModuleFunctions", parameters, out objOutUomID) != 0)
            {
                if ((int)objOutUomID >= 0)
                {
                    return (int)objOutUomID;
                }
            }
            return (int)objOutUomID;

        }

        public double GetItemDiscountAmount(int iMode, int iItemID, int DiscountID, double Rate)
        {

            double dOutItemNetAmount = 0;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 3));
            parameters.Add(new SqlParameter("@ItemID", iItemID));
            parameters.Add(new SqlParameter("@DiscountID", DiscountID));
            parameters.Add(new SqlParameter("@Rate", Rate));

            SqlDataReader sReader = MobjDataLayer.ExecuteReader("spInvPurchaseModuleFunctions", parameters, CommandBehavior.SingleRow);
            if (sReader.Read())
            {
                if (sReader[0] != DBNull.Value)
                {
                    dOutItemNetAmount = Convert.ToDouble(sReader[0]);
                }
                else
                {
                    dOutItemNetAmount = 0;
                }


                if (dOutItemNetAmount >= 0)
                {
                    sReader.Close();
                    return dOutItemNetAmount;
                }
            }
            sReader.Close();
            return dOutItemNetAmount;

        }

        public int GetVendorRecordID(int iVendorID)     // Retreiving current vendorid for Focussing on the current vendor
        {
            int RecordCnt;
            RecordCnt = 0;
            SqlDataReader DS;

            string TSQL = "select a.CurrentID from (select ROW_NUMBER() over (order by VendorID) as  CurrentID " +
                                             "  ,VendorID from InvVendorInformation  where VendorTypeID not in (" + (int)VendorType.Customer + ")) a where a.VendorID=" + iVendorID + "";

            DS = MobjDataLayer.ExecuteReader(TSQL);
            if (DS.Read())
            {
                RecordCnt = Convert.ToInt32(DS["CurrentID"]);
            }
            else
            {
                RecordCnt = 0;
            }
            DS.Close();
            return RecordCnt;
        }

        public int GetWarehouseRecordID(int iWarehouse)     // Retreiving current warehouseID for Focussing on the current warehouse
        {
            int RecordCnt;
            RecordCnt = 0;
            SqlDataReader DS;

            string TSQL = "select a.CurrentID from (select ROW_NUMBER() over (order by WarehouseID) as  CurrentID " +
                                             "  ,WarehouseID from InvWarehouse ) a where a.WarehouseID=" + iWarehouse + "";

            DS = MobjDataLayer.ExecuteReader(TSQL);
            if (DS.Read())
            {
                RecordCnt = Convert.ToInt32(DS["CurrentID"]);
            }
            else
            {
                RecordCnt = 0;
            }
            DS.Close();
            return RecordCnt;
        }

        public bool DeletePurchaseOrder(Int64 iOrdID)// Purchase Order Delete
        {

            //  DeleteLeadTimeDetails();

            object iOutPurchaseOrderID = 0;
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@PurchaseOrderID", iOrdID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            //if (PobjClsDTOPurchase.intStatusID == (Int16)OperationStatusType.POrderRejected)
            VerificationHistoryTableUpdations(2, PobjClsDTOPurchase.intPurchaseID, true);
            if (MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseOrder", parameters) != 0)
            {
                if (!PobjClsDTOPurchase.blnCancelled)
                {
                    //if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.POrderFromIndent)
                    //    UpdateStatus(1, (Int32)OperationStatusType.PIndentOpened, PobjClsDTOPurchase.intReferenceID);
                    if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.POrderFromQuotation)
                    {
                        if (ClsCommonSettings.PQApproval)
                            UpdateStatus(2, (Int32)OperationStatusType.PQuotationApproved, PobjClsDTOPurchase.intReferenceID);
                        else
                            UpdateStatus(2, (Int32)OperationStatusType.PQuotationSubmitted, PobjClsDTOPurchase.intReferenceID);
                    }
                }
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool DeleteGRN(Int64 iOrdID)// Purchase GRN Save
        {
            // DeleteOpeningStockAccountForGRN();

            object iOutPurchaseOrderID = 0;
            ArrayList prmPurOrder = new ArrayList();
            prmPurOrder.Add(new SqlParameter("@Mode", 1));
            prmPurOrder.Add(new SqlParameter("@GRNID", PobjClsDTOPurchase.intPurchaseID));
            prmPurOrder.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
            DataTable datCompany = MobjDataLayer.ExecuteDataTable("spGRN", prmPurOrder);

            if (datCompany.Rows.Count > 0)
            {
                int intStatusId = -1;
                if (datCompany.Rows[0]["StatusID"] != DBNull.Value) intStatusId = Convert.ToInt32(datCompany.Rows[0]["StatusID"]);
                if (PobjClsDTOPurchase.blnCancelled && intStatusId == PobjClsDTOPurchase.intStatusID)
                    blnIsOldStatusCancelled = true;
                else if (!PobjClsDTOPurchase.blnCancelled && intStatusId != PobjClsDTOPurchase.intStatusID)
                    blnIsOldStatusCancelled = true;

            }
            DataTable dtGRNDetails = DisplayPurchaseGRNDetail(PobjClsDTOPurchase.intPurchaseID);
            if (!blnIsOldStatusCancelled)
            {
                for (int i = 0; i < dtGRNDetails.Rows.Count; i++)
                {
                    clsDTOPurchaseDetails objDTOPurchaseDetails = new clsDTOPurchaseDetails();
                    objDTOPurchaseDetails.intBatchID = Convert.ToInt64(dtGRNDetails.Rows[i]["BatchID"]);
                    objDTOPurchaseDetails.intItemID = Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]);
                    DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtGRNDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]));
                    decimal decOldQuantity = Convert.ToDecimal(dtGRNDetails.Rows[i]["ReceivedQuantity"]);
                    decimal decOldExtraQty = Convert.ToDecimal(dtGRNDetails.Rows[i]["ExtraQuantity"]);
                    if (dtGetUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                        {
                            decOldQuantity = decOldQuantity / decConversionValue;
                            decOldExtraQty = decOldExtraQty / decConversionValue;
                        }
                        else if (intConversionFactor == 2)
                        {
                            decOldQuantity = decOldQuantity * decConversionValue;
                            decOldExtraQty = decOldExtraQty * decConversionValue;
                        }
                    }
                    int intTempWarehouseID = PobjClsDTOPurchase.intWarehouseID;
                    if (PobjClsDTOPurchase.intTempWarehouseID != PobjClsDTOPurchase.intWarehouseID)
                    {
                        PobjClsDTOPurchase.intWarehouseID = PobjClsDTOPurchase.intTempWarehouseID;
                        StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQty, true, true);
                        PobjClsDTOPurchase.intWarehouseID = intTempWarehouseID;
                    }
                    else
                    {
                        StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQty, true, true);
                    }
                    StockMasterUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQty, true, true);
                    //clsDTOPurchaseDetails objDTOPurchaseDetails = new clsDTOPurchaseDetails();
                    //objDTOPurchaseDetails.intBatchID = Convert.ToInt32(dtGRNDetails.Rows[i]["BatchID"]);
                    //objDTOPurchaseDetails.intItemID = Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]);
                    //StockDetailsUpdation(objDTOPurchaseDetails, Convert.ToDecimal(dtGRNDetails.Rows[i]["ReceivedQuantity"]) + Convert.ToDecimal(dtGRNDetails.Rows[i]["ExtraQuantity"]), true, true);
                    //StockMasterUpdation(objDTOPurchaseDetails, Convert.ToDecimal(dtGRNDetails.Rows[i]["ReceivedQuantity"]) + Convert.ToDecimal(dtGRNDetails.Rows[i]["ExtraQuantity"]), true, true);

                    //DeleteCostingAndPricingDetails(objDTOPurchaseDetails.intItemID.ToInt32(), objDTOPurchaseDetails.intBatchID.ToInt32());
                    //  DeleteCostingAndPricingDetailsFromOpeningStock(objDTOPurchaseDetails.intItemID.ToInt32(), objDTOPurchaseDetails.intBatchID.ToInt32());
                }
                DeletePurchaseLocationDetails(true, false);
            }
            else
                DeletePurchaseLocationDetails(true, true);

            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@GRNID", iOrdID));
            parameters.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
            parameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            blnIsOldStatusCancelled = false;
            if (MobjDataLayer.ExecuteNonQueryWithTran("spGRN", parameters) != 0)
            {
                if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice)
                    UpdateStatus(4, Convert.ToInt32(OperationStatusType.PInvoiceOpened), PobjClsDTOPurchase.intReferenceID);
                else
                    UpdateStatus(6, (int)OperationStatusType.STIssued, PobjClsDTOPurchase.intReferenceID);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteInvoice(Int64 iOrdID, string strCondition)// Purchase Invoice Save
        {
            // DeleteOpeningStockAccountForPurchaseInvoice();

            int intOrderTypeID = 0;
            if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                intOrderTypeID = (MobjClsCommonUtility.FillCombos(new string[] { "POM.OrderTypeID", "InvPurchaseInvoiceMaster PIM INNER JOIN InvPurchaseInvoiceReferenceDetails PIRD ON PIRD.PurchaseInvoiceID = PIM.PurchaseInvoiceID INNER JOIN InvPurchaseOrderMaster POM ON POM.PurchaseOrderID = PIRD.ReferenceID", "PIM.PurchaseInvoiceID = " + PobjClsDTOPurchase.intPurchaseID })).Rows[0]["OrderTypeID"].ToInt32();

            if ((PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder && intOrderTypeID == (int)OperationOrderType.POrderFromGRN) || PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
            {
                DeleteGRNSummary();
                SaveGRNSummary(1);
            }

            object iOutPurchaseOrderID = 0;
            ArrayList prmPurOrder = new ArrayList();
            prmPurOrder.Add(new SqlParameter("@Mode", 1));
            prmPurOrder.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
            prmPurOrder.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
            prmPurOrder.Add(new SqlParameter("@CreatedBy", PobjClsDTOPurchase.intCreatedBy));
            IsGRNRequired();
            DataTable datCompany = MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", prmPurOrder);
            if (datCompany.Rows.Count > 0)
            {
                int intStatusId = -1;
                if (datCompany.Rows[0]["StatusID"] != DBNull.Value) intStatusId = Convert.ToInt32(datCompany.Rows[0]["StatusID"]);
                if (PobjClsDTOPurchase.blnCancelled && intStatusId == PobjClsDTOPurchase.intStatusID)
                    blnIsOldStatusCancelled = true;
                else if (!PobjClsDTOPurchase.blnCancelled && intStatusId != PobjClsDTOPurchase.intStatusID)
                    blnIsOldStatusCancelled = true;
                PobjClsDTOPurchase.blnGRNRequired=Convert.ToBoolean(datCompany.Rows[0]["IsGRNRequired"]);

            }

            if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
            {
                UpdateGRNStatusPIDeleted(iOrdID, strCondition, 1);//to change GRN status
            }
            DataTable dtInvoiceDetails = DisplayPurchaseInvoiceDetail(PobjClsDTOPurchase.intPurchaseID);
            if (!blnIsOldStatusCancelled)
            {
                for (int i = 0; i < dtInvoiceDetails.Rows.Count; i++)
                {
                    //clsDTOPurchaseDetails objDTOPurchaseDetails = new clsDTOPurchaseDetails();
                    //objDTOPurchaseDetails.intBatchID = Convert.ToInt32(dtInvoiceDetails.Rows[i]["BatchID"]);
                    //objDTOPurchaseDetails.intItemID = Convert.ToInt32(dtInvoiceDetails.Rows[i]["ItemID"]);
                    //StockDetailsUpdation(objDTOPurchaseDetails, Convert.ToDecimal(dtInvoiceDetails.Rows[i]["ReceivedQuantity"]) + Convert.ToDecimal(dtInvoiceDetails.Rows[i]["ExtraQuantity"]), false, true);
                    //StockMasterUpdation(objDTOPurchaseDetails, Convert.ToDecimal(dtInvoiceDetails.Rows[i]["ReceivedQuantity"]) + Convert.ToDecimal(dtInvoiceDetails.Rows[i]["ExtraQuantity"]), false, true);
                    clsDTOPurchaseDetails objDTOPurchaseDetails = new clsDTOPurchaseDetails();
                    if (dtInvoiceDetails.Rows[i]["BatchID"] != DBNull.Value)
                        objDTOPurchaseDetails.intBatchID = Convert.ToInt64(dtInvoiceDetails.Rows[i]["BatchID"]);
                    objDTOPurchaseDetails.intItemID = Convert.ToInt32(dtInvoiceDetails.Rows[i]["ItemID"]);

                    DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtInvoiceDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtInvoiceDetails.Rows[i]["ItemID"]));
                    decimal decOldQuantity = Convert.ToDecimal(dtInvoiceDetails.Rows[i]["ReceivedQuantity"]);
                    decimal decOldExtraQuantity = Convert.ToDecimal(dtInvoiceDetails.Rows[i]["ExtraQuantity"]);
                    if (dtGetUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                        {
                            decOldQuantity = decOldQuantity / decConversionValue;
                            decOldExtraQuantity = decOldExtraQuantity / decConversionValue;
                        }
                        else if (intConversionFactor == 2)
                        {
                            decOldQuantity = decOldQuantity * decConversionValue;
                            decOldExtraQuantity = decOldExtraQuantity * decConversionValue;
                        }
                    }
                    int intTempWarehouseID = PobjClsDTOPurchase.intWarehouseID;
                    if (!PobjClsDTOPurchase.blnGRNRequired || PobjClsDTOPurchase.intOrderType==(int)OperationOrderType.PInvoiceFromGRN || CheckGRNExistingAgainstOrder())
                    {
                        if (PobjClsDTOPurchase.intTempWarehouseID != PobjClsDTOPurchase.intWarehouseID)
                        {
                            PobjClsDTOPurchase.intWarehouseID = PobjClsDTOPurchase.intTempWarehouseID;
                            if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
                            {
                                StockDetailsUpdationGRN(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                            }
                            else
                            {
                                if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                                {
                                    DataTable dtTemp = GetGRNBatchExistingAgainstGRN(objDTOPurchaseDetails.intItemID);
                                    foreach (DataRow dr in dtTemp.Rows)
                                    {
                                        objDTOPurchaseDetails.intBatchID = dr["BatchID"].ToInt64();
                                        StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                                    }
                                }
                                else
                                StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                            }
                            PobjClsDTOPurchase.intWarehouseID = intTempWarehouseID;
                        }
                        else
                        {
                            if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
                            {
                                StockDetailsUpdationGRN(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                            }
                            else
                            {
                                StockDetailsUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                            }
                        }
                    }
                    StockMasterUpdation(objDTOPurchaseDetails, decOldQuantity, decOldExtraQuantity, false, true);
                   

                    //DeleteCostingAndPricingDetails(objDTOPurchaseDetails.intItemID.ToInt32(), objDTOPurchaseDetails.intBatchID.ToInt32());
                    //DeleteCostingAndPricingDetailsFromOpeningStock(objDTOPurchaseDetails.intItemID.ToInt32(), objDTOPurchaseDetails.intBatchID.ToInt32());
                }
                //if (!PobjClsDTOPurchase.blnGRNRequired)
                //    DeletePurchaseLocationDetails(false, false);
            }
            else
            {
                //if (!PobjClsDTOPurchase.blnGRNRequired)
                //    DeletePurchaseLocationDetails(false, true);
            }    

            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@PurchaseInvoiceID", iOrdID));
            parameters.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
            parameters.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
            parameters.Add(new SqlParameter("@ExpensePosted", ClsMainSettings.ExpensePosted));
           
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);
            blnIsOldStatusCancelled = false;
            if (MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseInvoice", parameters) != 0)
            {
                if (PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PInvoiceCancelled)
                {
                    if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                    {
                        if (ClsCommonSettings.POApproval)
                            UpdateStatus(3, (Int32)OperationStatusType.POrderApproved, PobjClsDTOPurchase.intPurchaseOrderID);
                        else
                            UpdateStatus(3, (Int32)OperationStatusType.POrderSubmitted, PobjClsDTOPurchase.intPurchaseOrderID);
                    }
                }
                DeletePayments(iOrdID.ToInt32());

                for (int i = 0; i < dtInvoiceDetails.Rows.Count; i++)
                    CalculateCostingAndPricingAfterDelete(dtInvoiceDetails.Rows[i]["ItemID"].ToInt32(), dtInvoiceDetails.Rows[i]["BatchID"].ToInt32());

                return true;
            }
            else
            {
                return false;
            }
        }

        private void DeleteCostingAndPricingDetailsFromOpeningStock(int intItemID, int intBatchID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 16));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", intBatchID));
            MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", alParameters);

            CalculateCostingAndPricingAfterDelete(intItemID, intBatchID);
        }

        private void CalculateCostingAndPricingAfterDelete(int intTempItemID, int intBatchID)
        {
            DataTable datCosting = FillCombos(new string[] { "*", "InvCostingAndPricingDetails", "ItemID=" + intTempItemID });
            DataTable datTempItem = FillCombos(new string[] { "*", "InvItemMaster", "ItemID=" + intTempItemID });
            DataTable datTempBatch = new DataTable();
            if (datCosting != null)
            {
                if (datCosting.Rows.Count > 0)
                {
                    if (datCosting.Rows[0]["CostingMethodID"].ToInt32() != (int)CostingMethodReference.Batch)
                    {
                        switch (datCosting.Rows[0]["CostingMethodID"].ToInt32())
                        {
                            case (int)CostingMethodReference.FIFO:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.FIFO, intTempItemID);
                                break;
                            case (int)CostingMethodReference.LIFO:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.LIFO, intTempItemID);
                                break;
                            case (int)CostingMethodReference.MAX:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.MAX, intTempItemID);
                                break;
                            case (int)CostingMethodReference.MIN:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.MIN, intTempItemID);
                                break;
                            case (int)CostingMethodReference.AVG:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.AVG, intTempItemID);
                                break;
                            case (int)CostingMethodReference.NONE:
                                datTempBatch = GetPurchaseRateAndActualRate((int)CostingMethodReference.NONE, intTempItemID);
                                break;
                        }
                        if (datTempBatch != null && datTempBatch.Rows.Count > 0)
                        {
                            string strRate = "0", strActualRate = "0", strSaleRate = "0", strLandingCost = "0";
                            strRate = datTempBatch.Rows[0]["PurchaseRate"].ToString();
                            strActualRate = datTempBatch.Rows[0]["ActualRate"].ToString();
                            strLandingCost = datTempBatch.Rows[0]["LandingCost"].ToString();

                            ArrayList prms = new ArrayList();
                            prms.Add(new SqlParameter("@Mode", 9));
                            prms.Add(new SqlParameter("@CostingMethodID", datCosting.Rows[0]["CostingMethodID"].ToString().ToInt32()));
                            prms.Add(new SqlParameter("@PricingSchemeID", datCosting.Rows[0]["PricingSchemeID"].ToString().ToInt32()));
                            prms.Add(new SqlParameter("@ItemID", intTempItemID));
                            prms.Add(new SqlParameter("@CurrencyID", datCosting.Rows[0]["CurrencyID"].ToString().ToInt32()));
                            prms.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
                            prms.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDateTime()));
                            prms.Add(new SqlParameter("@BatchID", intBatchID));
                            prms.Add(new SqlParameter("@PurchaseRate", strRate.ToDouble()));
                            prms.Add(new SqlParameter("@ActualRate", strActualRate.ToDouble()));
                            prms.Add(new SqlParameter("@LandingCost", strLandingCost.ToDouble()));

                            DataTable datTempPricing = FillCombos(new string[] { "*", "InvPricingSchemeReference", "PricingSchemeID=" + datCosting.Rows[0]["PricingSchemeID"].ToString().ToInt32() });
                            double dblExRate = 1;

                            if (datTempItem.Rows[0]["IsLandingCostEffected"].ToStringCustom() == "")
                                datTempItem.Rows[0]["IsLandingCostEffected"] = "False";

                            if (Convert.ToBoolean(datTempItem.Rows[0]["IsLandingCostEffected"].ToString()) == true)
                            {
                                if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                    strSaleRate = (strLandingCost.ToDouble() +
                                        (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                else
                                    strSaleRate = (strLandingCost.ToDouble() +
                                                (strLandingCost.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                            }
                            else
                            {
                                if (Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()) == true)
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate = (strRate.ToDouble() +
                                            (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                    else
                                        strSaleRate = (strRate.ToDouble() +
                                                    (strRate.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                                }
                                else
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate = (strActualRate.ToDouble() +
                                            (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                    else
                                        strSaleRate = (strActualRate.ToDouble() +
                                                    (strActualRate.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                                }
                            }

                            prms.Add(new SqlParameter("@SaleRate", strSaleRate.ToDouble()));
                            DeleteCostingAndPricingDetails(intTempItemID, 0);
                            MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", prms);
                        }
                    }
                }
            }
        }

        public DataTable GetPurchaseRateAndActualRate(int intMethodID, int intItemID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 18));
            parameters.Add(new SqlParameter("@CostingMethodID", intMethodID));
            parameters.Add(new SqlParameter("@ItemID", intItemID));
            DataTable datRate = MobjDataLayer.ExecuteDataTable("spInvPricingScheme", parameters);
            return datRate;
        }
        // ----------------------------------------Reports------------------------------------------------------------

        public DataTable GetPurIndent()
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 1));
            return MobjDataLayer.ExecuteDataSet("STPurchaseReportModule", prmCommon).Tables[0];

        }

        public DataTable GetPurOrder()
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 2));
            return MobjDataLayer.ExecuteDataSet("STPurchaseReportModule", prmCommon).Tables[0];

        }

        public DataTable GetPurOrderDetail(Int64 iPurOrderID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 3));
            prmCommon.Add(new SqlParameter("@ID", iPurOrderID));
            return MobjDataLayer.ExecuteDataSet("STPurchaseReportModule", prmCommon).Tables[0];

        }

        public bool SavePurchaseQuotation(bool blnAddStatus)
        {
            // function for saving Purchase Quotation
            object objOutPurchaseQuotationID = 0;
            int intOldStatusID = 0;
            prmPurchase = new ArrayList();

            if (blnAddStatus)
            {
                //     InsertLeadTimeDetails();
                prmPurchase.Add(new SqlParameter("@Mode", 2));
            }
            else
            {
                DeletePurchaseQuotationDetails();
                DataTable datPurchase = FillCombos(new string[] { "StatusID", "InvPurchaseQuotationMaster", "PurchaseQuotationID = " + PobjClsDTOPurchase.intPurchaseID });
                if (datPurchase.Rows.Count > 0)
                {
                    intOldStatusID = datPurchase.Rows[0]["StatusID"].ToInt32();
                }
                //    UpdateLeadTimeDetails();                
                prmPurchase.Add(new SqlParameter("@Mode", 3));
            }

            prmPurchase.Add(new SqlParameter("@PurchaseQuotationID", PobjClsDTOPurchase.intPurchaseID));
            prmPurchase.Add(new SqlParameter("@PurchaseQuotationNo", PobjClsDTOPurchase.strPurchaseNo));
            prmPurchase.Add(new SqlParameter("@OrderTypeID", PobjClsDTOPurchase.intOrderType));
            prmPurchase.Add(new SqlParameter("@ReferenceID", PobjClsDTOPurchase.intReferenceID));
            prmPurchase.Add(new SqlParameter("@QuotationDate", PobjClsDTOPurchase.strDate));
            prmPurchase.Add(new SqlParameter("@VendorID", PobjClsDTOPurchase.intVendorID));
            prmPurchase.Add(new SqlParameter("@DueDate", PobjClsDTOPurchase.strDueDate));
            prmPurchase.Add(new SqlParameter("@VendorAddID", PobjClsDTOPurchase.intVendorAddID));
            prmPurchase.Add(new SqlParameter("@PaymentTermsID", PobjClsDTOPurchase.intPaymentTermsID));
            prmPurchase.Add(new SqlParameter("@Remarks", PobjClsDTOPurchase.strRemarks));
            prmPurchase.Add(new SqlParameter("@EmployeeID", PobjClsDTOPurchase.intEmployeeID));

            if (PobjClsDTOPurchase.blnGrandDiscount == true)
            {
                prmPurchase.Add(new SqlParameter("@DiscountPercentage", PobjClsDTOPurchase.decGrandDiscountPercent));
                prmPurchase.Add(new SqlParameter("@DiscountAmount", 0.0));
                prmPurchase.Add(new SqlParameter("@IsDiscountPercentage", PobjClsDTOPurchase.blnGrandDiscount));

            }
            else if (PobjClsDTOPurchase.blnGrandDiscount == false)
            {
                prmPurchase.Add(new SqlParameter("@DiscountPercentage", 0.0));
                prmPurchase.Add(new SqlParameter("@GrandDiscountAmount", PobjClsDTOPurchase.decGrandDiscountAmt));
                prmPurchase.Add(new SqlParameter("@IsDiscountPercentage", PobjClsDTOPurchase.blnGrandDiscount));

            }
            prmPurchase.Add(new SqlParameter("@CurrencyID", PobjClsDTOPurchase.intCurrencyID));
            prmPurchase.Add(new SqlParameter("@GrandAmount", PobjClsDTOPurchase.decGrandAmount));
            prmPurchase.Add(new SqlParameter("@NetAmount", PobjClsDTOPurchase.decNetAmount));
            prmPurchase.Add(new SqlParameter("@ExpenseAmount", PobjClsDTOPurchase.decExpenseAmount));
            prmPurchase.Add(new SqlParameter("@CreatedBy", PobjClsDTOPurchase.intCreatedBy));
            prmPurchase.Add(new SqlParameter("@CreatedDate", PobjClsDTOPurchase.strCreatedDate));
            if (PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PQuotationApproved || PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PQuotationRejected)
            {
                prmPurchase.Add(new SqlParameter("@ApprovedBy", PobjClsDTOPurchase.intApprovedBy));
                prmPurchase.Add(new SqlParameter("@ApprovedDate", PobjClsDTOPurchase.strApprovedDate));
            }
            prmPurchase.Add(new SqlParameter("@VerificationDescription", PobjClsDTOPurchase.strVerifcationDescription));
            prmPurchase.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
            prmPurchase.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
            //if (PobjClsDTOPurchase.intVerificationCriteria > 0)
            //prmPurchase.Add(new SqlParameter("@VerificationCriteriaID", PobjClsDTOPurchase.intVerificationCriteria));
            //if(PobjClsDTOPurchase.intIncoTerms !=0)
            //prmPurchase.Add(new SqlParameter("@IncoTermsID", PobjClsDTOPurchase.intIncoTerms));
            //prmPurchase.Add(new SqlParameter("@LeadTimeDetsID", PobjClsDTOPurchase.intLeadTimeDetsID));
            //if(PobjClsDTOPurchase.intContainerTypeID !=0)
            //prmPurchase.Add(new SqlParameter("@ContainerTypeID", PobjClsDTOPurchase.intContainerTypeID));
            //prmPurchase.Add(new SqlParameter("@NoOfContainers", PobjClsDTOPurchase.intNoOfContainers));


            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPurchase.Add(objParam);

            intOldStatusID = 0;
            DataTable datQuotationDetail = FillCombos(new string[] { "StatusID", "InvPurchaseQuotationMaster", "PurchaseQuotationID = " + PobjClsDTOPurchase.intPurchaseID });
            if (datQuotationDetail.Rows.Count > 0)
                intOldStatusID = datQuotationDetail.Rows[0]["StatusID"].ToInt32();

            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvPurchaseQuotation", prmPurchase, out objOutPurchaseQuotationID) != 0)
            {
                if (Convert.ToInt64(objOutPurchaseQuotationID) > 0)
                {
                    PobjClsDTOPurchase.intPurchaseID = Convert.ToInt64(objOutPurchaseQuotationID);

                    //if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.PQuotationFromRFQ)
                    //{
                    //    if (PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PQuotationApproved || PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PQuotationSubmitted)
                    //    {
                    //        UpdateStatus(5, (Int32)RFQStatus.Closed, PobjClsDTOPurchase.intReferenceID);
                    //        DataTable datQuotations = FillCombos(new string[] { "PurchaseQuotationID", "InvPurchaseQuotationMaster", "QuotationType=" + (int)OperationOrderType.PQuotationFromRFQ + " And ReferenceID = " + PobjClsDTOPurchase.intReferenceID +" And PurchaseQuotationID <> "+PobjClsDTOPurchase.intPurchaseID });
                    //        foreach (DataRow dr in datQuotations.Rows)
                    //        {
                    //            UpdateStatus(2, (int)OperationStatusType.PQuotationInvalid, dr["PurchaseQuotationID"].ToInt64());
                    //        }
                    //    }
                    //    else if ((PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PQuotationRejected && intOldStatusID == (int)OperationStatusType.PQuotationApproved) || (PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PQuotationCancelled && intOldStatusID == (int)OperationStatusType.PQuotationSubmitted))
                    //    {
                    //        if (ClsCommonSettings.RFQApproval)
                    //            UpdateStatus(5, (Int32)RFQStatus.Approved, PobjClsDTOPurchase.intReferenceID);
                    //        else
                    //            UpdateStatus(5, (Int32)RFQStatus.Submitted, PobjClsDTOPurchase.intReferenceID);

                    //        DataTable datQuotations = FillCombos(new string[] { "PurchaseQuotationID", "InvPurchaseQuotationMaster", "QuotationType=" + (int)OperationOrderType.PQuotationFromRFQ + " And ReferenceID = " + PobjClsDTOPurchase.intReferenceID + " And PurchaseQuotationID <> " + PobjClsDTOPurchase.intPurchaseID });
                    //        foreach (DataRow dr in datQuotations.Rows)
                    //        {
                    //            UpdateStatus(2, (int)OperationStatusType.PQuotationOpened, dr["PurchaseQuotationID"].ToInt64());
                    //        }
                    //    }
                    //}


                    DeleteCancellationDetails(1);

                    if (PobjClsDTOPurchase.blnCancelled)
                    {
                        SaveCancellationDetails(1);
                    }
                    if (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection.Count > 0)
                    {
                        SavePurchaseQuotationDetails();
                    }
                    if ((PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationRejected && intOldStatusID != PobjClsDTOPurchase.intStatusID) ||
                            (PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationApproved && intOldStatusID != PobjClsDTOPurchase.intStatusID) ||
                             (PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationSubmitted && intOldStatusID != PobjClsDTOPurchase.intStatusID) ||
                                (PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationSubmittedForApproval && intOldStatusID != PobjClsDTOPurchase.intStatusID))
                        VerificationHistoryTableUpdations(1, PobjClsDTOPurchase.intPurchaseID, false);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public DataTable DisplayPurchaseIndentDetail(Int64 Rownum)// Display Indent detail
        {
            //function for displaying purchase indent details
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 7));
            prmPurchase.Add(new SqlParameter("@PurchaseIndentID", Rownum));
            return MobjDataLayer.ExecuteDataSet("STPurchaseIndentTransaction", prmPurchase).Tables[0];
        }

        public bool DisplayPurchaseIndentInfo(Int64 iOrderID) // Purchse Indent
        {
            //function for displaying purchase indent info
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 1));
            prmPurchase.Add(new SqlParameter("@PurchaseIndentID", iOrderID));
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("STPurchaseIndentTransaction", prmPurchase, CommandBehavior.SingleRow);

            if (sdr.Read())
            {
                PobjClsDTOPurchase.intReferenceID = iOrderID;
                //PobjClsDTOPurchase.strPurchaseIndentNo = Convert.ToString(sdr["PurchaseIndentNo"]);

                if (sdr["CompanyID"] != DBNull.Value) PobjClsDTOPurchase.intCompanyID = Convert.ToInt32(sdr["CompanyID"]); else PobjClsDTOPurchase.intCompanyID = 0;

                //if (sdr["DepartmentID"] != DBNull.Value) PobjClsDTOPurchaseIndent.intDepartmentID = Convert.ToInt32(sdr["DepartmentID"]); else PobjClsDTOPurchaseIndent.intDepartmentID = 0;
                if (sdr["IndentDate"] != DBNull.Value)
                    PobjClsDTOPurchase.strDate = Convert.ToString(sdr["IndentDate"]);
                //PobjClsDTOPurchase.strDueDate = Convert.ToString(sdr["DueDate"]);
                // PobjClsDTOPurchaseIndent.strRemarks = Convert.ToString(sdr["Remarks"]);
                // PobjClsDTOPurchase.intStatusID = Convert.ToInt16(sdr["StatusID"]);

                //  DataTable datCancellationDetails = DisplayCancellationDetails();

                //  if (datCancellationDetails != null && datCancellationDetails.Rows.Count > 0)
                //   {
                //      PobjClsDTOPurchaseIndent.strDescription = Convert.ToString(datCancellationDetails.Rows[0]["Description"]);
                //      PobjClsDTOPurchaseIndent.strCancellationDate = Convert.ToString(datCancellationDetails.Rows[0]["Date"]);
                // }

                sdr.Close();
                return true;
            }
            else
            {
                sdr.Close();
                return false;
            }
        }

        public DataTable DisplayRFQDetail(Int64 Rownum)// Display RFQ detail
        {
            //function for displaying purchase RFQ details
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 7));
            prmPurchase.Add(new SqlParameter("@RFQID", Rownum));
            return MobjDataLayer.ExecuteDataSet("STRFQTransaction", prmPurchase).Tables[0];
        }

        public bool DisplayRFQInfo(Int64 iOrderID) // Purchse Indent
        {
            //function for displaying purchase RFQ info
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 1));
            prmPurchase.Add(new SqlParameter("@RFQID", iOrderID));
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("STRFQTransaction", prmPurchase, CommandBehavior.SingleRow);

            if (sdr.Read())
            {
                PobjClsDTOPurchase.intReferenceID = iOrderID;
                //PobjClsDTOPurchase.strPurchaseIndentNo = Convert.ToString(sdr["PurchaseIndentNo"]);

                if (sdr["CompanyID"] != DBNull.Value) PobjClsDTOPurchase.intCompanyID = Convert.ToInt32(sdr["CompanyID"]); else PobjClsDTOPurchase.intCompanyID = 0;

                //if (sdr["DepartmentID"] != DBNull.Value) PobjClsDTOPurchaseIndent.intDepartmentID = Convert.ToInt32(sdr["DepartmentID"]); else PobjClsDTOPurchaseIndent.intDepartmentID = 0;
                if (sdr["QuotationDate"] != DBNull.Value)
                    PobjClsDTOPurchase.strDate = Convert.ToString(sdr["QuotationDate"]);
                if (sdr["DueDate"] != DBNull.Value)
                    PobjClsDTOPurchase.strDueDate = Convert.ToString(sdr["DueDate"]);

                sdr.Close();
                return true;
            }
            else
            {
                sdr.Close();
                return false;
            }
        }

        public void SaveCancellationDetails(int type)
        {
            // function for saving cancellation details

            prmPurchase = new ArrayList();

            prmPurchase.Add(new SqlParameter("@CancelledBy", PobjClsDTOPurchase.intCancelledBy));
            prmPurchase.Add(new SqlParameter("@CancellationDate", PobjClsDTOPurchase.strCancellationDate));
            prmPurchase.Add(new SqlParameter("@Description", PobjClsDTOPurchase.strDescription));
            prmPurchase.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
            switch (type)
            {
                case 1:
                    prmPurchase.Add(new SqlParameter("Mode", 12));
                    prmPurchase.Add(new SqlParameter("PurchaseQuotationID", PobjClsDTOPurchase.intPurchaseID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spInvPurchaseQuotation", prmPurchase);
                    break;
                case 2:
                    prmPurchase.Add(new SqlParameter("Mode", 13));
                    prmPurchase.Add(new SqlParameter("PurchaseOrderID", PobjClsDTOPurchase.intPurchaseID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseOrder", prmPurchase);
                    break;
                case 3:
                    prmPurchase.Add(new SqlParameter("Mode", 10));
                    prmPurchase.Add(new SqlParameter("GRNID", PobjClsDTOPurchase.intPurchaseID));
                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
                    MobjDataLayer.ExecuteNonQueryWithTran("spGRN", prmPurchase);
                    break;
                case 4:
                    prmPurchase.Add(new SqlParameter("Mode", 10));
                    prmPurchase.Add(new SqlParameter("PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
                    MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseInvoice", prmPurchase);
                    break;

            }
        }

        private void DeleteCancellationDetails(int type)
        {
            // function for deleting cancellation details
            prmPurchase = new ArrayList();
            switch (type)
            {
                case 1:
                    prmPurchase.Add(new SqlParameter("Mode", 13));
                    prmPurchase.Add(new SqlParameter("PurchaseQuotationID", PobjClsDTOPurchase.intPurchaseID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spInvPurchaseQuotation", prmPurchase);
                    break;
                case 2:
                    prmPurchase.Add(new SqlParameter("Mode", 14));
                    prmPurchase.Add(new SqlParameter("PurchaseOrderID", PobjClsDTOPurchase.intPurchaseID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseOrder", prmPurchase);
                    break;
                case 3:
                    prmPurchase.Add(new SqlParameter("Mode", 11));
                    prmPurchase.Add(new SqlParameter("GRNID", PobjClsDTOPurchase.intPurchaseID));
                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
                    MobjDataLayer.ExecuteNonQueryWithTran("spGRN", prmPurchase);
                    break;
                case 4:
                    prmPurchase.Add(new SqlParameter("Mode", 11));
                    prmPurchase.Add(new SqlParameter("PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
                    MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseInvoice", prmPurchase);
                    break;
            }
        }

        private DataTable DisplayCancellationDetails(int type)
        {
            //function for displaying cancellation details

            prmPurchase = new ArrayList();

            switch (type)
            {
                case 1:
                    prmPurchase.Add(new SqlParameter("Mode", 14));
                    prmPurchase.Add(new SqlParameter("PurchaseQuotationID", PobjClsDTOPurchase.intPurchaseID));
                    return MobjDataLayer.ExecuteDataTable("spInvPurchaseQuotation", prmPurchase);
                case 2:
                    prmPurchase.Add(new SqlParameter("Mode", 15));
                    prmPurchase.Add(new SqlParameter("PurchaseOrderID", PobjClsDTOPurchase.intPurchaseID));
                    return MobjDataLayer.ExecuteDataTable("spPurchaseOrder", prmPurchase);
                case 3:
                    prmPurchase.Add(new SqlParameter("Mode", 12));
                    prmPurchase.Add(new SqlParameter("GRNID", PobjClsDTOPurchase.intPurchaseID));
                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
                    return MobjDataLayer.ExecuteDataTable("spGRN", prmPurchase);
                case 4:
                    prmPurchase.Add(new SqlParameter("Mode", 12));
                    prmPurchase.Add(new SqlParameter("PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
                    return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", prmPurchase);
                default:
                    return null;
            }
        }

        public bool DeletePurchaseQuotation()
        {
            //DeleteLeadTimeDetails();
            // function for deleting purchase Quotation
            object objOutPurchaseQuotationID = 0;
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 4));
            prmPurchase.Add(new SqlParameter("@PurchaseQuotationID", PobjClsDTOPurchase.intPurchaseID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPurchase.Add(objParam);
            // if (PobjClsDTOPurchase.intStatusID == (Int16)OperationStatusType.PQuotationRejected)
            VerificationHistoryTableUpdations(1, PobjClsDTOPurchase.intPurchaseID, true);
            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvPurchaseQuotation", prmPurchase) != 0)
            {
                //if (!PobjClsDTOPurchase.blnCancelled)
                //{
                //    if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.PQuotationFromRFQ)
                //        UpdateStatus(5, (Int32)RFQStatus.Approved, PobjClsDTOPurchase.intReferenceID);
                //}

                //if (PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PQuotationCancelled)
                //{
                //    if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.PQuotationFromRFQ)
                //    {
                //        if (ClsCommonSettings.RFQApproval)
                //            UpdateStatus(5, (Int32)RFQStatus.Approved, PobjClsDTOPurchase.intReferenceID);
                //        else
                //            UpdateStatus(5, (Int32)RFQStatus.Submitted, PobjClsDTOPurchase.intReferenceID);
                //    }
                //}
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SavePurchaseQuotationDetails()
        {
            //function for saving purchase indent details
            int count = 0;

            foreach (clsDTOPurchaseDetails objClsDTOPurchaseDetails in PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection)
            {
                prmPurchaseDetails = new ArrayList();

                //if (objClsDTOPurchaseDetails.intSerialNo == 0)
                prmPurchaseDetails.Add(new SqlParameter("@Mode", 8));
                //else
                //  prmPurchaseDetails.Add(new SqlParameter("@Mode", 9));

                prmPurchaseDetails.Add(new SqlParameter("@SerialNo", ++count));
                prmPurchaseDetails.Add(new SqlParameter("@PurchaseQuotationID", PobjClsDTOPurchase.intPurchaseID));
                prmPurchaseDetails.Add(new SqlParameter("@ItemID", objClsDTOPurchaseDetails.intItemID));
                prmPurchaseDetails.Add(new SqlParameter("@Quantity", objClsDTOPurchaseDetails.decQuantity));
                prmPurchaseDetails.Add(new SqlParameter("@UOMID", objClsDTOPurchaseDetails.intUOMID));
                prmPurchaseDetails.Add(new SqlParameter("@Rate", objClsDTOPurchaseDetails.decRate));

               
                if (objClsDTOPurchaseDetails.blnDiscount == true)
                {
                    prmPurchaseDetails.Add(new SqlParameter("@DiscountPercentage", objClsDTOPurchaseDetails.decDiscountPercent));
                    prmPurchaseDetails.Add(new SqlParameter("@DiscountAmount", 0.0));
                    prmPurchaseDetails.Add(new SqlParameter("@IsDiscountPercentage", objClsDTOPurchaseDetails.blnDiscount));

                }
                else if (objClsDTOPurchaseDetails.blnDiscount == false)
                {
                    prmPurchaseDetails.Add(new SqlParameter("@DiscountPercentage", 0.0));
                    prmPurchaseDetails.Add(new SqlParameter("@GrandDiscountAmount", objClsDTOPurchaseDetails.decDiscountAmt));
                    prmPurchaseDetails.Add(new SqlParameter("@IsDiscountPercentage", objClsDTOPurchaseDetails.blnDiscount));

                }

                prmPurchaseDetails.Add(new SqlParameter("@GrandAmount", objClsDTOPurchaseDetails.decGrandAmount));
                prmPurchaseDetails.Add(new SqlParameter("@NetAmount", objClsDTOPurchaseDetails.decNetAmount));
                MobjDataLayer.ExecuteNonQueryWithTran("spInvPurchaseQuotation", prmPurchaseDetails);
            }
            return true;
        }

        public bool DeletePurchaseQuotationDetails()
        {
            //function for deleting purchase indent details

            prmPurchaseDetails = new ArrayList();
            prmPurchaseDetails.Add(new SqlParameter("@Mode", 10));
            prmPurchaseDetails.Add(new SqlParameter("@PurchaseQuotationID", PobjClsDTOPurchase.intPurchaseID));
            MobjDataLayer.ExecuteNonQueryWithTran("spInvPurchaseQuotation", prmPurchaseDetails);
            return true;
        }

        public bool DeletePurchaseOrderDetails()
        {
            //function for deleting purchase indent details

            prmPurchaseDetails = new ArrayList();
            prmPurchaseDetails.Add(new SqlParameter("@Mode", 11));
            prmPurchaseDetails.Add(new SqlParameter("@PurchaseOrderID", PobjClsDTOPurchase.intPurchaseID));
            MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseOrder", prmPurchaseDetails);
            return true;
        }

        public bool DeletePurchaseGRNDetails(int intOldStatusID)
        {
            //function for deleting purchase indent details

            prmPurchaseDetails = new ArrayList();
            prmPurchaseDetails.Add(new SqlParameter("@Mode", 9));
            prmPurchaseDetails.Add(new SqlParameter("@GRNID", PobjClsDTOPurchase.intPurchaseID));
            prmPurchaseDetails.Add(new SqlParameter("@StatusID", intOldStatusID));
            prmPurchaseDetails.Add(new SqlParameter("@OperationTypeID", (int)OperationType.GRN));
            MobjDataLayer.ExecuteNonQueryWithTran("spGRN", prmPurchaseDetails);
            return true;
        }

        public bool DeletePurchaseInvoiceDetails(int intOldStatusID)
        {
            //function for deleting purchase indent details

            prmPurchaseDetails = new ArrayList();
            prmPurchaseDetails.Add(new SqlParameter("@Mode", 9));
            prmPurchaseDetails.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
            prmPurchaseDetails.Add(new SqlParameter("@StatusID", intOldStatusID));
            prmPurchaseDetails.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
            MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseInvoice", prmPurchaseDetails);
            return true;
        }

        public DataTable GetPurchaseNos(int intType)// Get Purchase number For searching
        {
            //function for Finding Purchase 
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 5));
            prmPurchase.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));

            switch (intType)
            {
                case 1:
                    return MobjDataLayer.ExecuteDataTable("spInvPurchaseQuotation", prmPurchase);
                case 2:
                    return MobjDataLayer.ExecuteDataTable("spPurchaseOrder", prmPurchase);
                case 3:
                    return MobjDataLayer.ExecuteDataTable("spGRN", prmPurchase);
                case 4:
                    return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", prmPurchase);
                default:
                    return null;
            }
        }

        public DataTable DisplayPurchaseQuotationDetail(Int64 Rownum)// Display Indent detail
        {
            //function for displaying purchase Quotation details
            prmPurchaseDetails = new ArrayList();
            prmPurchaseDetails.Add(new SqlParameter("@Mode", 7));
            prmPurchaseDetails.Add(new SqlParameter("@PurchaseQuotationID", Rownum));
            return MobjDataLayer.ExecuteDataSet("spInvPurchaseQuotation", prmPurchaseDetails).Tables[0];
        }

        public bool DisplayPurchaseQuotationInfo(Int64 iOrderID) // Purchse Indent
        {
            //function for displaying purchase Quotation info
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 1));
            prmPurchase.Add(new SqlParameter("@PurchaseQuotationID", iOrderID));
            SqlDataReader sdrQuotation = MobjDataLayer.ExecuteReader("spInvPurchaseQuotation", prmPurchase, CommandBehavior.SingleRow);

            if (sdrQuotation.Read())
            {
                PobjClsDTOPurchase.intPurchaseID = iOrderID;
                PobjClsDTOPurchase.strPurchaseNo = Convert.ToString(sdrQuotation["PurchaseQuotationNo"]);
                PobjClsDTOPurchase.intOrderType = Convert.ToInt32(sdrQuotation["OrderTypeID"]);
                PobjClsDTOPurchase.intReferenceID = Convert.ToInt32(sdrQuotation["ReferenceID"]);

                if (sdrQuotation["CompanyID"] != DBNull.Value) PobjClsDTOPurchase.intCompanyID = Convert.ToInt32(sdrQuotation["CompanyID"]); else PobjClsDTOPurchase.intCompanyID = 0;

                if (sdrQuotation["VendorID"] != DBNull.Value) PobjClsDTOPurchase.intVendorID = Convert.ToInt32(sdrQuotation["VendorID"]); else PobjClsDTOPurchase.intVendorID = 0;

                if (sdrQuotation["VendorName"] != DBNull.Value) PobjClsDTOPurchase.strVendorName = Convert.ToString(sdrQuotation["VendorName"]); else PobjClsDTOPurchase.strVendorName = "";

                if (sdrQuotation["VendorAddID"] != DBNull.Value) PobjClsDTOPurchase.intVendorAddID = Convert.ToInt32(sdrQuotation["VendorAddID"]); else PobjClsDTOPurchase.intVendorAddID = 0;


                if (sdrQuotation["PaymentTermsID"] != DBNull.Value) PobjClsDTOPurchase.intPaymentTermsID = Convert.ToInt32(sdrQuotation["PaymentTermsID"]); else PobjClsDTOPurchase.intPaymentTermsID = 0;



               // if (sdrQuotation["GrandDiscountID"] != DBNull.Value) PobjClsDTOPurchase.intGrandDiscountID = Convert.ToInt32(sdrQuotation["GrandDiscountID"]); else PobjClsDTOPurchase.intGrandDiscountID = 0;


                if (sdrQuotation["CurrencyID"] != DBNull.Value) PobjClsDTOPurchase.intCurrencyID = Convert.ToInt32(sdrQuotation["CurrencyID"]); else PobjClsDTOPurchase.intCurrencyID = 0;

               // PobjClsDTOPurchase.decGrandDiscountAmount = Convert.ToDecimal(sdrQuotation["GrandDiscountAmount"]);

                PobjClsDTOPurchase.decGrandAmount = Convert.ToDecimal(sdrQuotation["GrandAmount"]);
                PobjClsDTOPurchase.decNetAmount = Convert.ToDecimal(sdrQuotation["NetAmount"]);
                if (sdrQuotation["ExpenseAmount"] != DBNull.Value) PobjClsDTOPurchase.decExpenseAmount = Convert.ToDecimal(sdrQuotation["ExpenseAmount"]); else PobjClsDTOPurchase.decExpenseAmount = 0;
                PobjClsDTOPurchase.strDate = Convert.ToString(sdrQuotation["QuotationDate"]);
                PobjClsDTOPurchase.strDueDate = Convert.ToString(sdrQuotation["DueDate"]);
                PobjClsDTOPurchase.strRemarks = Convert.ToString(sdrQuotation["Remarks"]);
                PobjClsDTOPurchase.intStatusID = Convert.ToInt16(sdrQuotation["StatusID"]);
                PobjClsDTOPurchase.strAddressName = Convert.ToString(sdrQuotation["AddressName"]);
                PobjClsDTOPurchase.strEmployeeName = Convert.ToString(sdrQuotation["EmployeeName"]);
                if (sdrQuotation["CreatedDate"] != DBNull.Value) PobjClsDTOPurchase.strCreatedDate = Convert.ToString(sdrQuotation["CreatedDate"]); else PobjClsDTOPurchase.strCreatedDate = "";
                DataTable datCancellationDetails = DisplayCancellationDetails(1);


                if (datCancellationDetails != null && datCancellationDetails.Rows.Count > 0)
                {
                    PobjClsDTOPurchase.strDescription = Convert.ToString(datCancellationDetails.Rows[0]["Description"]);
                    PobjClsDTOPurchase.strCancellationDate = Convert.ToString(datCancellationDetails.Rows[0]["Date"]);
                    PobjClsDTOPurchase.intCancelledBy = Convert.ToInt32(datCancellationDetails.Rows[0]["CancelledBy"]);
                    PobjClsDTOPurchase.strCancelledBy = Convert.ToString(datCancellationDetails.Rows[0]["CancelledByName"]);
                }

                if (sdrQuotation["ApprovedDate"] != DBNull.Value) PobjClsDTOPurchase.strApprovedDate = Convert.ToString(sdrQuotation["ApprovedDate"]); else PobjClsDTOPurchase.strApprovedDate = "";
                if (sdrQuotation["ApprovedByName"] != DBNull.Value) PobjClsDTOPurchase.strApprovedBy = Convert.ToString(sdrQuotation["ApprovedByName"]); else PobjClsDTOPurchase.strApprovedBy = "";

                PobjClsDTOPurchase.intApprovedBy = sdrQuotation["ApprovedBy"].ToInt32();

                if (sdrQuotation["VerificationDescription"] != DBNull.Value) PobjClsDTOPurchase.strVerifcationDescription = Convert.ToString(sdrQuotation["VerificationDescription"]); else PobjClsDTOPurchase.strVerifcationDescription = "";

                if (sdrQuotation["VerificationCriteria"] != DBNull.Value)
                    PobjClsDTOPurchase.strVerificationCriteria = Convert.ToString(sdrQuotation["VerificationCriteria"]);
                else
                    PobjClsDTOPurchase.strVerificationCriteria = "";
                //if (sdrQuotation["IncoTermsID"] != DBNull.Value)
                //    PobjClsDTOPurchase.intIncoTerms = Convert.ToInt32(sdrQuotation["IncoTermsID"]);
                //else
                //    PobjClsDTOPurchase.intIncoTerms = 0;
                //if (sdrQuotation["LeadTimeDetsID"] != DBNull.Value)
                //PobjClsDTOPurchase.intLeadTimeDetsID = Convert.ToInt32(sdrQuotation["LeadTimeDetsID"]);
                //if (sdrQuotation["LeadTime"] != DBNull.Value)
                //PobjClsDTOPurchase.decLeadTime = Convert.ToDecimal(sdrQuotation["LeadTime"]);
                //if (sdrQuotation["ProductionDate"] != DBNull.Value)
                //PobjClsDTOPurchase.strProductionDate = Convert.ToString(sdrQuotation["ProductionDate"]);
                //if (sdrQuotation["TranshipmentDate"] != DBNull.Value)
                //PobjClsDTOPurchase.strTranShipmentDate = Convert.ToString(sdrQuotation["TranshipmentDate"]);
                //if (sdrQuotation["ClearenceDate"] != DBNull.Value)
                //PobjClsDTOPurchase.strClearenceDate = Convert.ToString(sdrQuotation["ClearenceDate"]);

                //if (sdrQuotation["ContainerTypeID"] != DBNull.Value)
                //    PobjClsDTOPurchase.intContainerTypeID = sdrQuotation["ContainerTypeID"].ToInt32();
                //else
                //    PobjClsDTOPurchase.intContainerTypeID = 0;

                //if (sdrQuotation["NoOfContainers"] != DBNull.Value)
                //    PobjClsDTOPurchase.intNoOfContainers = sdrQuotation["NoOfContainers"].ToInt32();

                sdrQuotation.Close();
                return true;
            }
            else
            {
                sdrQuotation.Close();
                return false;
            }
        }

        public decimal GetReceivedQty(int intItemID, Int64 intReferenceID, bool blnIsGRN)
        {
            decimal decReceivedQty = 0;
            DataTable datDetails;
            prmPurchase = new ArrayList();

           if (PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.PInvoiceFromGRN)
           {
                prmPurchase.Add(new SqlParameter("@Mode", 61));
                prmPurchase.Add(new SqlParameter("@ItemID", intItemID));
                prmPurchase.Add(new SqlParameter("@PurchaseOrderID", intReferenceID));
                prmPurchase.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                datDetails = MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", prmPurchase);
            }
            else
            {
                prmPurchase.Add(new SqlParameter("@Mode", 13));
                prmPurchase.Add(new SqlParameter("@ItemID", intItemID));
                prmPurchase.Add(new SqlParameter("@PurchaseOrderID", intReferenceID));
                datDetails = MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", prmPurchase);
            }
            if (datDetails.Rows.Count > 0)
            {
                if (datDetails.Rows[0]["ReceivedQty"] != DBNull.Value)
                    decReceivedQty = Convert.ToDecimal(datDetails.Rows[0]["ReceivedQty"]);

            }
            return decReceivedQty;
        }

        //public decimal GetOrderedQty(int intItemID, Int64 intReferenceID)
        //{
        //    decimal OrderedQty = 0;
        //    DataTable datDetails;
        //    prmPurchase = new ArrayList();
        //    prmPurchase.Add(new SqlParameter("@Mode", 26));
        //    prmPurchase.Add(new SqlParameter("@ItemID", intItemID));       
        //        prmPurchase.Add(new SqlParameter("@PurchaseOrderID", intReferenceID));
        //        datDetails = MobjDataLayer.ExecuteDataTable("spPurchaseOrder", prmPurchase);

        //        return OrderedQty;
        //}

        public string GetLastBatchNo(int intItemID, Int64 intReferenceID, bool blnIsGRN)
        {
            string strBatchNo = null; DataTable dtLastBatchDetails = new DataTable();
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 14));
            prmPurchase.Add(new SqlParameter("@ItemID", intItemID));
            if (blnIsGRN)
            {
                prmPurchase.Add(new SqlParameter("@ReferenceID", intReferenceID));
                dtLastBatchDetails = MobjDataLayer.ExecuteDataTable("spGRN", prmPurchase);
            }
            else
            {
                prmPurchase.Add(new SqlParameter("@PurchaseOrderID", intReferenceID));
                dtLastBatchDetails = MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", prmPurchase);
            }
            if (dtLastBatchDetails.Rows.Count > 0)
            {
                if (dtLastBatchDetails.Rows[0]["BatchNo"] != DBNull.Value)
                    strBatchNo = dtLastBatchDetails.Rows[0]["BatchNo"].ToString();
            }
            return strBatchNo;
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 10));
            prmPurchase.Add(new SqlParameter("@ItemID", intItemID));
            //prmPurchase.Add(new SqlParameter("@UomTypeID", intUOMTypeID));
            DataTable dtItemUoms = new DataTable();
            dtItemUoms = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmPurchase);

            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 12));
            prmPurchase.Add(new SqlParameter("@ItemID", intItemID));
            DataTable dtItemBaseUnit = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmPurchase);
            DataRow dr = dtItemUoms.NewRow();
            dr["UOMID"] = dtItemBaseUnit.Rows[0]["UOMID"];
            dr["ShortName"] = dtItemBaseUnit.Rows[0]["ShortName"];
            dr["ItemID"] = intItemID;
            dtItemUoms.Rows.InsertAt(dr, 0);
            return dtItemUoms;
        }

        public DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {
            ArrayList prmUomDetails = new ArrayList();
            prmUomDetails.Add(new SqlParameter("@Mode", 11));
            prmUomDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmUomDetails.Add(new SqlParameter("@UnitID", intUOMID));
            //    prmUomDetails.Add(new SqlParameter("@UomTypeID", intUOMTypeID));
            return MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmUomDetails);
        }

        //private void UpdateLastSaleRateOfItem(decimal decSaleRate, int intItemID,int intUOMID)
        //{
        //    if (decSaleRate != 0)
        //    {
        //        ArrayList prmUomDetails = new ArrayList();
        //        prmUomDetails.Add(new SqlParameter("@Mode", 13));
        //        prmUomDetails.Add(new SqlParameter("@ItemID", intItemID));
        //        DataTable dtGetUomDetails = GetUomConversionValues(intUOMID, intItemID);
        //        if (dtGetUomDetails.Rows.Count > 0)
        //        {
        //            int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
        //            decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
        //            if (intConversionFactor == 1)
        //                decSaleRate = decSaleRate * decConversionValue;
        //            else if (intConversionFactor == 2)
        //                decSaleRate = decSaleRate / decConversionValue;
        //        }
        //        prmUomDetails.Add(new SqlParameter("@Rate", decSaleRate));
        //        prmUomDetails.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
        //        prmUomDetails.Add(new SqlParameter("@CurrencyID", PobjClsDTOPurchase.intCurrencyID));
        //        MobjDataLayer.ExecuteNonQueryWithTran("spInvPurchaseModuleFunctions", prmUomDetails);
        //    }
        //}

        public bool IsPurchaseNoExists(string strPurchaseNo, int intType, int intCompanyID)
        {
            bool blnIsExists = false;
            DataTable dtPurchases = new DataTable();
            switch (intType)
            {
                case 1:
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 15));
                    prmPurchase.Add(new SqlParameter("@PurchaseQuotationNo", strPurchaseNo));
                    prmPurchase.Add(new SqlParameter("@CompanyID", intCompanyID));
                    dtPurchases = MobjDataLayer.ExecuteDataTable("spInvPurchaseQuotation", prmPurchase);
                    if (dtPurchases.Rows.Count > 0 && Convert.ToInt64(dtPurchases.Rows[0]["PurchaseQuotationID"]) != PobjClsDTOPurchase.intPurchaseID)
                        blnIsExists = true;
                    break;
                case 2:
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 16));
                    prmPurchase.Add(new SqlParameter("@PurchaseOrderNo", strPurchaseNo));
                    prmPurchase.Add(new SqlParameter("@CompanyID", intCompanyID));
                    dtPurchases = MobjDataLayer.ExecuteDataTable("spPurchaseOrder", prmPurchase);
                    if (dtPurchases.Rows.Count > 0 && Convert.ToInt64(dtPurchases.Rows[0]["PurchaseOrderID"]) != PobjClsDTOPurchase.intPurchaseID)
                        blnIsExists = true;
                    break;
                case 3:
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 15));
                    prmPurchase.Add(new SqlParameter("@GRNNo", strPurchaseNo));
                    prmPurchase.Add(new SqlParameter("@CompanyID", intCompanyID));
                    dtPurchases = MobjDataLayer.ExecuteDataTable("spGRN", prmPurchase);
                    if (dtPurchases.Rows.Count > 0 && Convert.ToInt64(dtPurchases.Rows[0]["GRNID"]) != PobjClsDTOPurchase.intPurchaseID)
                        blnIsExists = true;
                    break;
                case 4:
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 15));
                    prmPurchase.Add(new SqlParameter("@CompanyID", intCompanyID));
                    prmPurchase.Add(new SqlParameter("@PurchaseInvoiceNo", strPurchaseNo));
                    dtPurchases = MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", prmPurchase);
                    if (dtPurchases.Rows.Count > 0 && Convert.ToInt64(dtPurchases.Rows[0]["PurchaseInvoiceID"]) != PobjClsDTOPurchase.intPurchaseID)
                        blnIsExists = true;
                    break;
            }
            return blnIsExists;
        }

        public bool IsPurchaseBillNoExists(string strPurchaseBillNo, long lngPurchaseInvoiceID)
        {
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 22));
            prmPurchase.Add(new SqlParameter("@PurchaseBillNo", strPurchaseBillNo));
            prmPurchase.Add(new SqlParameter("@PurchaseInvoiceID", lngPurchaseInvoiceID));
            DataTable dtPurchases = MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", prmPurchase);
            if (dtPurchases.Rows.Count > 0)
                return true;
            return false;
        }

        private void UpdateStatus(int intType, int intStatusID, Int64 intPurchaseID)
        {
            switch (intType)
            {
                case 1://Purchase Indent Upadtion
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 16));
                    prmPurchase.Add(new SqlParameter("@StatusID", intStatusID));
                    prmPurchase.Add(new SqlParameter("@PurchaseIndentID", intPurchaseID));
                    MobjDataLayer.ExecuteNonQueryWithTran("STPurchaseIndentTransaction", prmPurchase);
                    break;
                case 2: //Purchase Quotation Updation
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 16));
                    prmPurchase.Add(new SqlParameter("@StatusID", intStatusID));
                    prmPurchase.Add(new SqlParameter("@PurchaseQuotationID", intPurchaseID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spInvPurchaseQuotation", prmPurchase);
                    break;
                case 3://Purchase Order Updation
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 17));
                    prmPurchase.Add(new SqlParameter("@StatusID", intStatusID));
                    prmPurchase.Add(new SqlParameter("@PurchaseOrderID", intPurchaseID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseOrder", prmPurchase);
                    break;
                case 4: // Purchase Invoice Updation
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 24));
                    prmPurchase.Add(new SqlParameter("@StatusID", intStatusID));
                    prmPurchase.Add(new SqlParameter("@PurchaseInvoiceID", intPurchaseID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseInvoice", prmPurchase);
                    break;
                case 5: // RFQ Updation
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 16));
                    prmPurchase.Add(new SqlParameter("@StatusID", intStatusID));
                    prmPurchase.Add(new SqlParameter("@RFQID", intPurchaseID));
                    MobjDataLayer.ExecuteNonQueryWithTran("STRFQTransaction", prmPurchase);
                    break;
                case 6: // Transfer Status Updation
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 14));
                    prmPurchase.Add(new SqlParameter("@StatusID", intStatusID));
                    prmPurchase.Add(new SqlParameter("@StockTransferID", intPurchaseID));
                    MobjDataLayer.ExecuteNonQueryWithTran("spInvStockTransfer", prmPurchase);
                    break;
            }
        }

        private bool GetPurchaseOrderStatus(Int64 intPurchaseOrderID)
        {
            DataTable dtPurchaseOrderDetails = new DataTable();
            dtPurchaseOrderDetails = DisplayPurchaseOrderDetail(intPurchaseOrderID);
            dtPurchaseOrderDetails.Columns.Add("GRNReceivedQty");
            dtPurchaseOrderDetails.Columns.Add("InvoiceReceivedQty");
            int intFullyReceivedCount = 0;
            foreach (DataRow dr in dtPurchaseOrderDetails.Rows)
            {
                //if (PobjClsDTOPurchase.blnGRNRequired)
                //{
                //    dr["GRNReceivedQty"] = GetReceivedQty(Convert.ToInt64(dr["ItemID"]), intPurchaseOrderID, true);
                //    dr["InvoiceReceivedQty"] = GetReceivedQty(Convert.ToInt64(dr["ItemID"]), intPurchaseOrderID, false);
                //}
                //else
                //{
                dr["InvoiceReceivedQty"] = GetReceivedQty(Convert.ToInt32(dr["ItemID"]), intPurchaseOrderID, false);
                dr["GRNReceivedQty"] = dr["InvoiceReceivedQty"];
                //}
                if ((Convert.ToDecimal(dr["GRNReceivedQty"]) == Convert.ToDecimal(dr["Quantity"])) && (Convert.ToDecimal(dr["InvoiceReceivedQty"]) == Convert.ToDecimal(dr["Quantity"])))
                    intFullyReceivedCount++;
            }
            if (intFullyReceivedCount == dtPurchaseOrderDetails.Rows.Count)
                return true;
            else
                return false;
        }

        private bool GetPurchaseInvoiceStatus(Int64 intPurchaseInvoiceID)
        {
            DataTable dtPurchaseInvoiceDetails = new DataTable();
            dtPurchaseInvoiceDetails = DisplayPurchaseInvoiceDetail(intPurchaseInvoiceID);
            dtPurchaseInvoiceDetails.Columns.Add("GRNReceivedQty");
            int intFullyReceivedCount = 0;
            foreach (DataRow dr in dtPurchaseInvoiceDetails.Rows)
            {
                //if (PobjClsDTOPurchase.blnGRNRequired)
                //{
                //    dr["GRNReceivedQty"] = GetReceivedQty(Convert.ToInt64(dr["ItemID"]), intPurchaseOrderID, true);
                //    dr["InvoiceReceivedQty"] = GetReceivedQty(Convert.ToInt64(dr["ItemID"]), intPurchaseOrderID, false);
                //}
                //else
                //{
                dr["GRNReceivedQty"] = GetReceivedQty(Convert.ToInt32(dr["ItemID"]), intPurchaseInvoiceID, true);
                //}
                if ((Convert.ToDecimal(dr["GRNReceivedQty"]) == Convert.ToDecimal(dr["ReceivedQuantity"])))
                    intFullyReceivedCount++;
            }
            if (intFullyReceivedCount == dtPurchaseInvoiceDetails.Rows.Count)
                return true;
            else
                return false;
        }

        //private bool DeleteItemLocationDetails(Int64 intReferenceID, bool blnIsGRN)
        //{
        //    prmPurchase = new ArrayList();
        //    prmPurchase.Add(new SqlParameter("@Mode", 11));
        //    prmPurchase.Add(new SqlParameter("@ReferenceID", intReferenceID));
        //    prmPurchase.Add(new SqlParameter("@IsGRN", blnIsGRN));
        //    MobjDataLayer.ExecuteNonQueryWithTran("STItemLocationDetail", prmPurchase);
        //    return true;
        //}

        public string StockValidation(bool isDeletion, bool blnIsFromGRN, out bool blnIsSalesDone)
        {
            try
            {
                DataTable dtGRNDetails = new DataTable();
                blnIsSalesDone = false;
                bool blnIsDemoQty = false;

                if (blnIsFromGRN)
                {
                    if (MobjClsCommonUtility.FillCombos(new string[] { "GRNID", "InvGRNMaster", "GRNID = " + PobjClsDTOPurchase.intPurchaseID + " And StatusID <> " + (int)OperationStatusType.GRNCancelled }).Rows.Count > 0)
                    {
                        dtGRNDetails = DisplayPurchaseGRNDetail(PobjClsDTOPurchase.intPurchaseID);
                    }
                    //if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GTFromTransfer)
                    //{

                    //    int intTransferTypeID = FillCombos(new string[] { "OrderTypeID", "InvStockTransferMaster", "StockTransferID = " + PobjClsDTOPurchase.intReferenceID }).Rows[0]["OrderTypeID"].ToInt32();
                    //    if (intTransferTypeID == (int)OperationOrderType.STWareHouseToWareHouseTransferDemo)
                    //        blnIsDemoQty = true;
                    //}
                }
                else
                {
                    if (MobjClsCommonUtility.FillCombos(new string[] { "PurchaseInvoiceID", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + PobjClsDTOPurchase.intPurchaseID + " And StatusID <> " + (int)OperationStatusType.PInvoiceCancelled }).Rows.Count > 0)
                    {
                        dtGRNDetails = DisplayPurchaseInvoiceDetail(PobjClsDTOPurchase.intPurchaseID);
                    }
                }

                List<int> lstupdateIndices = new List<int>();
                if (PobjClsDTOPurchase.blnCancelled || isDeletion)
                {
                    for (int i = 0; i < dtGRNDetails.Rows.Count; i++)
                    {
                        clsDTOPurchaseDetails objDTOPurchaseDetails = new clsDTOPurchaseDetails();
                        objDTOPurchaseDetails.intBatchID = Convert.ToInt64(dtGRNDetails.Rows[i]["BatchID"]);
                        objDTOPurchaseDetails.intItemID = Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]);
                        DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtGRNDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]));
                        decimal decOldQuantity = Convert.ToDecimal(dtGRNDetails.Rows[i]["ReceivedQuantity"]) + Convert.ToDecimal(dtGRNDetails.Rows[i]["ExtraQuantity"]);
                        if (dtGetUomDetails.Rows.Count > 0)
                        {
                            int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                            decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                            if (intConversionFactor == 1)
                                decOldQuantity = decOldQuantity / decConversionValue;
                            else if (intConversionFactor == 2)
                                decOldQuantity = decOldQuantity * decConversionValue;
                        }
                        if (blnIsFromGRN)
                        {
                            if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice || PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromOrder)
                            {

                                if (IsSold(dtGRNDetails.Rows[i], decOldQuantity, 0, blnIsDemoQty))
                                    return Convert.ToString(dtGRNDetails.Rows[i]["ItemName"]);
                            }
                            else
                            {
                                if (IsSold(dtGRNDetails.Rows[i], decOldQuantity, 0, false))
                                    return Convert.ToString(dtGRNDetails.Rows[i]["ItemName"]);
                            }
                        }
                        else
                        {
                            if (IsSold(dtGRNDetails.Rows[i], decOldQuantity, 0, false))
                                return Convert.ToString(dtGRNDetails.Rows[i]["ItemName"]);
                        }
                        if (!blnIsDemoQty && MobjClsCommonUtility.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + objDTOPurchaseDetails.intItemID }).Rows[0]["CostingMethodID"].ToInt32() == (int)CostingMethodReference.Batch && MobjClsCommonUtility.GetStockQuantity(objDTOPurchaseDetails.intItemID, objDTOPurchaseDetails.intBatchID, PobjClsDTOPurchase.intCompanyID, PobjClsDTOPurchase.intWarehouseID) - decOldQuantity < GetUsedQuantity(objDTOPurchaseDetails.intItemID, objDTOPurchaseDetails.intBatchID))
                        {
                            blnIsSalesDone = true;
                            return Convert.ToString(dtGRNDetails.Rows[i]["ItemName"]);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < dtGRNDetails.Rows.Count; i++)
                    {
                        bool blnExists = false;
                        for (int j = 0; j < PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection.Count; j++)
                        {
                            if (PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].strBatchNumber == dtGRNDetails.Rows[i]["BatchNo"].ToString())
                            {
                                PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intBatchID = FindBatchID(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].strBatchNumber);

                                DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtGRNDetails.Rows[j]["UOMID"]), Convert.ToInt32(dtGRNDetails.Rows[j]["ItemID"]));
                                decimal decOldQuantity = Convert.ToDecimal(dtGRNDetails.Rows[j]["ReceivedQuantity"]) + Convert.ToDecimal(dtGRNDetails.Rows[j]["ExtraQuantity"]);
                                if (dtGetUomDetails.Rows.Count > 0)
                                {
                                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                    if (intConversionFactor == 1)
                                        decOldQuantity = decOldQuantity / decConversionValue;
                                    else if (intConversionFactor == 2)
                                        decOldQuantity = decOldQuantity * decConversionValue;
                                }
                                blnExists = true;
                                if (blnIsFromGRN)
                                {
                                    if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice || PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromOrder)
                                    {
                                        if (IsSold(dtGRNDetails.Rows[i], decOldQuantity, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decQuantity + PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decExtraQuantity, blnIsDemoQty))
                                            return Convert.ToString(dtGRNDetails.Rows[i]["ItemName"]);
                                    }
                                    else
                                    {
                                        if (IsSold(dtGRNDetails.Rows[i], decOldQuantity, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decQuantity + PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decExtraQuantity, false))
                                            return Convert.ToString(dtGRNDetails.Rows[i]["ItemName"]);
                                    }
                                }
                                else
                                {
                                    if (IsSold(dtGRNDetails.Rows[i], decOldQuantity, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decQuantity + PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decExtraQuantity, false))
                                        return Convert.ToString(dtGRNDetails.Rows[i]["ItemName"]);
                                }

                                if (!blnIsDemoQty && MobjClsCommonUtility.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID }).Rows[0]["CostingMethodID"].ToInt32() == (int)CostingMethodReference.Batch && MobjClsCommonUtility.GetStockQuantity(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intBatchID, PobjClsDTOPurchase.intCompanyID, PobjClsDTOPurchase.intWarehouseID) - decOldQuantity + MobjClsCommonUtility.ConvertQtyToBaseUnitQty(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intUOMID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decQuantity + PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].decExtraQuantity, 2) < GetUsedQuantity(PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intItemID, PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection[j].intBatchID))
                                {
                                    blnIsSalesDone = true;
                                    return Convert.ToString(dtGRNDetails.Rows[i]["ItemName"]);
                                }
                            }
                        }
                        if (!blnExists)
                        {
                            clsDTOPurchaseDetails objDTOPurchaseDetails = new clsDTOPurchaseDetails();
                            objDTOPurchaseDetails.intBatchID = Convert.ToInt64(dtGRNDetails.Rows[i]["BatchID"]);
                            objDTOPurchaseDetails.intItemID = Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]);
                            DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(dtGRNDetails.Rows[i]["UOMID"]), Convert.ToInt32(dtGRNDetails.Rows[i]["ItemID"]));
                            decimal decOldQuantity = Convert.ToDecimal(dtGRNDetails.Rows[i]["ReceivedQuantity"]) + Convert.ToDecimal(dtGRNDetails.Rows[i]["ExtraQuantity"]);
                            if (dtGetUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                    decOldQuantity = decOldQuantity / decConversionValue;
                                else if (intConversionFactor == 2)
                                    decOldQuantity = decOldQuantity * decConversionValue;
                            }
                            if (blnIsFromGRN)
                            {
                                if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice || PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromOrder)
                                {
                                    if (IsSold(dtGRNDetails.Rows[i], decOldQuantity, 0, blnIsDemoQty))
                                        return Convert.ToString(dtGRNDetails.Rows[i]["ItemName"]);
                                }
                                else
                                {
                                    if (IsSold(dtGRNDetails.Rows[i], decOldQuantity, 0, false))
                                        return Convert.ToString(dtGRNDetails.Rows[i]["ItemName"]);
                                }
                            }
                            else
                                if (IsSold(dtGRNDetails.Rows[i], decOldQuantity, 0, false))
                                    return Convert.ToString(dtGRNDetails.Rows[i]["ItemName"]);

                            if (!blnIsDemoQty && MobjClsCommonUtility.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + dtGRNDetails.Rows[i]["ItemID"].ToInt32() }).Rows[0]["CostingMethodID"].ToInt32() == (int)CostingMethodReference.Batch && MobjClsCommonUtility.GetStockQuantity(dtGRNDetails.Rows[i]["ItemID"].ToInt32(), dtGRNDetails.Rows[i]["BatchID"].ToInt64(), PobjClsDTOPurchase.intCompanyID, PobjClsDTOPurchase.intWarehouseID) - decOldQuantity < GetUsedQuantity(dtGRNDetails.Rows[i]["ItemID"].ToInt32(), dtGRNDetails.Rows[i]["BatchID"].ToInt32()))
                            {
                                blnIsSalesDone = true;
                                return Convert.ToString(dtGRNDetails.Rows[i]["ItemName"]);
                            }
                        }
                    }
                }

                return null;
            }
            catch { blnIsSalesDone = false; return ""; }            
        }
        public decimal GetUsedQuantity(int intItemID, long lngBatchID)
        {
            ArrayList alParamters = new ArrayList();
            alParamters.Add(new SqlParameter("@Mode", 28));
            alParamters.Add(new SqlParameter("@ItemID", intItemID));
            alParamters.Add(new SqlParameter("@BatchID", lngBatchID));
            return MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", alParamters).Rows[0]["UsedQty"].ToDecimal();
        }
        private bool IsSold(DataRow drGRNDetails, decimal decOldQuantity, decimal decQuantity, bool blnIsDemoQty)
        {
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 4));
            prmPurchase.Add(new SqlParameter("@ItemID", drGRNDetails["ItemID"]));
            prmPurchase.Add(new SqlParameter("@BatchID", drGRNDetails["BatchID"]));
            prmPurchase.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
            prmPurchase.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intTempWarehouseID));
            DataTable dtStockDetails = MobjDataLayer.ExecuteDataTable("spInvStockMaster", prmPurchase);
            DataTable dtGetUomDetails = GetUomConversionValues(Convert.ToInt32(drGRNDetails["UOMID"]), Convert.ToInt32(drGRNDetails["ItemID"]));
            if (dtGetUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                    decQuantity = decQuantity / decConversionValue;
                else if (intConversionFactor == 2)
                    decQuantity = decQuantity * decConversionValue;
            }
            if (dtStockDetails.Rows.Count > 0)
            {
                decimal decGRNQty = 0;
                if (!blnIsDemoQty)
                {
                    // //decGRNQty = Convert.ToDecimal(dtStockDetails.Rows[0]["GRNQuantity"]) - decOldQuantity + decQuantity;
                    decGRNQty = (dtStockDetails.Rows[0]["GRNQuantity"].ToDecimal() + dtStockDetails.Rows[0]["ReceivedFromTransfer"].ToDecimal()) - (dtStockDetails.Rows[0]["SoldQuantity"].ToDecimal() + dtStockDetails.Rows[0]["DamagedQuantity"].ToDecimal() + dtStockDetails.Rows[0]["TransferedQuantity"].ToDecimal() + dtStockDetails.Rows[0]["DemoQuantity"].ToDecimal());
                    decGRNQty = decGRNQty - decOldQuantity + decQuantity;
                    // decGRNQty = decQuantity - Convert.ToDecimal(dtStockDetails.Rows[0]["SoldQuantity"]);
                }
                else
                {
                    decGRNQty = dtStockDetails.Rows[0]["DemoQuantity"].ToDecimal();
                    decGRNQty = decGRNQty - decOldQuantity + decQuantity;
                }
                if (decGRNQty < 0)
                    return true;
            }
            return false;
        }

        public DataTable GetItemWiseDiscountDetails(int intItemID, decimal decQty, decimal decRate, string strCurrentDate, int intCurrencyID)
        {
            DataTable dtDiscounts = new DataTable();
            prmPurchase = new ArrayList();
            if (!string.IsNullOrEmpty(strCurrentDate))
            {
                decimal decMaxSlab = 0;
                dtDiscounts = MobjClsCommonUtility.FillCombos(new string[] { "IsNull(Max(MinSlab),0) as MinSlab", "InvDiscountReference", "DiscountMode = 'N' And IsSaleType = 'False' And IsActive = 1 And IsDiscountForAmount = 0 And (IsPeriodApplicable = 0 or (IsPeriodApplicable = 1 And '" + strCurrentDate + "' >= convert(datetime,convert(varchar,PeriodFrom,106)) And '" + strCurrentDate + "' <= convert(datetime,convert(varchar,PeriodTo,106)))) And (IsSlabApplicable = 0 Or (IsSlabApplicable = 1 And " + decRate + " >= MinSlab)) And PercentOrAmount = 1 " });
                if (dtDiscounts.Rows.Count > 0)
                    decMaxSlab = dtDiscounts.Rows[0]["MinSlab"].ToDecimal();
                dtDiscounts = new DataTable();
                dtDiscounts = MobjClsCommonUtility.FillCombos(new string[] { "DiscountID,DiscountShortName", "InvDiscountReference", "(DiscountMode = 'N' And IsSaleType = 'False' And IsActive = 1 And IsDiscountForAmount = 0 And (IsPeriodApplicable = 0 or (IsPeriodApplicable = 1 And '" + strCurrentDate + "' >= convert(datetime,convert(varchar,PeriodFrom,106)) And '" + strCurrentDate + "' <= convert(datetime,convert(varchar,PeriodTo,106)))) And (IsSlabApplicable = 0 Or (IsSlabApplicable = 1 And " + decRate + " >= MinSlab)) And PercentOrAmount = 1 And MinSlab >= " + decMaxSlab + ") Or ( IsSaleType = 'False' And IsPredefined = 1)" });
                prmPurchase.Add(new SqlParameter("@Mode", 14));
            }
            else
            {
                // dtDiscounts = MobjClsCommonUtility.FillCombos(new string[] { "DiscountTypeID,DiscountShortName", "STDiscountTypeReference", "Active = 1 And DiscountMode = 'N' And IsSaleType = 'False' And DiscountForAmount = 0" });
                prmPurchase.Add(new SqlParameter("@Mode", 21));
            }

            prmPurchase.Add(new SqlParameter("@ItemID", intItemID));
            prmPurchase.Add(new SqlParameter("@Quantity", decQty));
            prmPurchase.Add(new SqlParameter("@Rate", decRate));
            prmPurchase.Add(new SqlParameter("@CurrentDate", strCurrentDate));
            prmPurchase.Add(new SqlParameter("@CurrencyID", intCurrencyID));
            return MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmPurchase);
            //DataTable dtItemWiseDiscounts = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmPurchase);
            //if (dtItemWiseDiscounts != null)
            //{
            //    foreach (DataRow dr in dtItemWiseDiscounts.Rows)
            //    {
            //        DataRow drDiscount = dtDiscounts.NewRow();
            //        drDiscount["DiscountTypeID"] = dr["DiscountTypeID"];
            //        drDiscount["DiscountShortName"] = dr["DiscountShortName"];
            //        dtDiscounts.Rows.Add(drDiscount);
            //    }
            //}
            // return dtDiscounts;
        }

        public DataSet GetPurchaseReport(int intFormTypeID)
        {
            switch (intFormTypeID)
            {
                case 1:
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 19));
                    prmPurchase.Add(new SqlParameter("@PurchaseQuotationID", PobjClsDTOPurchase.intPurchaseID));
                    return MobjDataLayer.ExecuteDataSet("spInvPurchaseQuotation", prmPurchase);
                case 2:
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 20));
                    prmPurchase.Add(new SqlParameter("@PurchaseOrderID", PobjClsDTOPurchase.intPurchaseID));
                    return MobjDataLayer.ExecuteDataSet("spPurchaseOrder", prmPurchase);
                case 3:
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 18));
                    prmPurchase.Add(new SqlParameter("@GRNID", PobjClsDTOPurchase.intPurchaseID));
                    return MobjDataLayer.ExecuteDataSet("spGRN", prmPurchase);
                case 4:
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 18));
                    prmPurchase.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                    return MobjDataLayer.ExecuteDataSet("spPurchaseInvoice", prmPurchase);
                case 5:
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 23));
                    prmPurchase.Add(new SqlParameter("@GRNID", PobjClsDTOPurchase.intPurchaseID));
                    return MobjDataLayer.ExecuteDataSet("spGRN", prmPurchase);
                case 6:
                    prmPurchase = new ArrayList();
                    prmPurchase.Add(new SqlParameter("@Mode", 29));
                    prmPurchase.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                    return MobjDataLayer.ExecuteDataSet("spPurchaseInvoice", prmPurchase);
                default:
                    return null;
            }
        }

        public bool IsDiscountForAmount(int intDiscountID)
        {
            bool blnIsDiscountForAmount = false;
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 15));
            prmPurchase.Add(new SqlParameter("@DiscountID", intDiscountID));
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spInvPurchaseModuleFunctions", prmPurchase);
            if (sdr.Read())
            {
                if (sdr["IsDiscountForAmount"] != DBNull.Value)
                    blnIsDiscountForAmount = Convert.ToBoolean(sdr["IsDiscountForAmount"]);
            }
            sdr.Close();
            return blnIsDiscountForAmount;
        }

        public string FindEmployeeNameByUserID(int intUserID)
        {
            string strEmployeeName = string.Empty;
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 10));
            prmPurchase.Add(new SqlParameter("@UserID", intUserID));
            SqlDataReader sdr;
            sdr = MobjDataLayer.ExecuteReader("STUserInformation", prmPurchase);
            if (sdr.Read())
            {
                if (sdr["FirstName"] != DBNull.Value)
                {
                    strEmployeeName = Convert.ToString(sdr["FirstName"]);
                }
            }
            sdr.Close();
            return strEmployeeName;
        }

        public DataTable GetPurchaseOrderExpenseDetails(Int64 intPurchaseOrderID)
        {
            DataTable dtDetails = new DataTable();
            ArrayList prmExpenseDetails = new ArrayList();
            prmExpenseDetails.Add(new SqlParameter("@Mode", 19));
            prmExpenseDetails.Add(new SqlParameter("@OperationTypeID", (Int32)OperationType.PurchaseInvoice));
            prmExpenseDetails.Add(new SqlParameter("@PurchaseOrderID", PobjClsDTOPurchase.intPurchaseID));
            dtDetails = MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", prmExpenseDetails);
            if (dtDetails == null || dtDetails.Rows.Count == 0)
            {
                prmExpenseDetails = new ArrayList();
                prmExpenseDetails.Add(new SqlParameter("@Mode", 19));
                prmExpenseDetails.Add(new SqlParameter("@OperationTypeID", (Int32)OperationType.PurchaseOrder));
                prmExpenseDetails.Add(new SqlParameter("@PurchaseOrderID", intPurchaseOrderID));
                return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", prmExpenseDetails);
            }
            return null;
        }

        public decimal GetAdvancePayment(Int64 intPurchaseOrderID)
        {
            ArrayList prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", 20));
            prmPayments.Add(new SqlParameter("@OperationTypeID", (Int32)OperationType.PurchaseOrder));
            prmPayments.Add(new SqlParameter("@PurchaseOrderID", intPurchaseOrderID));
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spPurchaseInvoice", prmPayments);
            if (sdr.Read())
            {
                if (sdr["TotalAmount"] != DBNull.Value)
                    return Convert.ToDecimal(sdr["TotalAmount"]);
            }
            return 0;
        }

        public bool GetCompanyAccount(int intTransactionType)
        {
            // function for Geting Company Account
            ArrayList prmPurchase = new ArrayList();
            object objOutAccountID = 0;
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 18));
            prmPurchase.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
            prmPurchase.Add(new SqlParameter("@TransactionType", intTransactionType));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPurchase.Add(objParam);

            if (MobjDataLayer.ExecuteNonQuery("spInvPurchaseModuleFunctions", prmPurchase, out objOutAccountID) != 0)
            {
                ClsCommonSettings.PintSIDebitHeadID = (int)objOutAccountID;
                if ((int)objOutAccountID > 0) return true;
            }
            return false;
        }
        public Int32 GetCompanyAccountID(int intTransactionType,int CompanyID)
        {
            // function for Geting Company Account
            ArrayList prmPurchase = new ArrayList();
            object objOutAccountID = 0;
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 18));
            prmPurchase.Add(new SqlParameter("@CompanyID", CompanyID));
            prmPurchase.Add(new SqlParameter("@TransactionType", intTransactionType));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPurchase.Add(objParam);

            if (MobjDataLayer.ExecuteNonQuery("spInvPurchaseModuleFunctions", prmPurchase, out objOutAccountID) != 0)
            {
                ClsCommonSettings.PintSIDebitHeadID = (int)objOutAccountID;
                if ((int)objOutAccountID > 0) return (int)objOutAccountID;
            }
            return 0;
        }
        public bool GetExchangeCurrencyRate(int intCompanyID, int intCurrencyID)
        {
            ArrayList prmExchangeCurrencyRate = new ArrayList();
            prmExchangeCurrencyRate.Add(new SqlParameter("@Mode", 19));
            prmExchangeCurrencyRate.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmExchangeCurrencyRate.Add(new SqlParameter("@CurrencyID", intCurrencyID));

            DataTable datExchangeCurrencyRate = MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmExchangeCurrencyRate);

            if (datExchangeCurrencyRate.Rows.Count > 0)
            {
                if (datExchangeCurrencyRate.Rows[0]["Exchangerate"] != DBNull.Value)
                    PobjClsDTOPurchase.decExchangeCurrencyRate = Convert.ToDecimal(datExchangeCurrencyRate.Rows[0]["Exchangerate"]);

                if (PobjClsDTOPurchase.decExchangeCurrencyRate >= 0)
                    return true;
            }

            return false;

        }

        private bool VerificationHistoryTableUpdations(int intType, Int64 intReferenceID, bool blnIsDeletion)
        {
            // 1 - Purchase Quotation  2 - Purchase Order
            ArrayList prmVerHistory = new ArrayList();
            if (intType == 1)
            {
                if (!blnIsDeletion)
                    prmVerHistory.Add(new SqlParameter("@Mode", 20));
                else
                    prmVerHistory.Add(new SqlParameter("@Mode", 21));
                prmVerHistory.Add(new SqlParameter("@ReferenceID", intReferenceID));

                prmVerHistory.Add(new SqlParameter("@ApprovedBy", PobjClsDTOPurchase.intApprovedBy));
                prmVerHistory.Add(new SqlParameter("@ApprovedDate", PobjClsDTOPurchase.strApprovedDate));

                prmVerHistory.Add(new SqlParameter("@VerificationDescription", PobjClsDTOPurchase.strVerifcationDescription));
                prmVerHistory.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                prmVerHistory.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
                if (PobjClsDTOPurchase.intVerificationCriteria > 0)
                    prmVerHistory.Add(new SqlParameter("@VerificationCriteriaID", PobjClsDTOPurchase.intVerificationCriteria));

                MobjDataLayer.ExecuteNonQueryWithTran("spInvPurchaseQuotation", prmVerHistory);
                return true;
            }
            else if (intType == 2)
            {
                if (!blnIsDeletion)
                    prmVerHistory.Add(new SqlParameter("@Mode", 21));
                else
                    prmVerHistory.Add(new SqlParameter("@Mode", 22));
                prmVerHistory.Add(new SqlParameter("@ReferenceID", intReferenceID));
                prmVerHistory.Add(new SqlParameter("@ApprovedBy", PobjClsDTOPurchase.intApprovedBy));
                prmVerHistory.Add(new SqlParameter("@ApprovedDate", PobjClsDTOPurchase.strApprovedDate));

                prmVerHistory.Add(new SqlParameter("@VerificationDescription", PobjClsDTOPurchase.strVerifcationDescription));
                prmVerHistory.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                prmVerHistory.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
                if (PobjClsDTOPurchase.intVerificationCriteria > 0)
                    prmVerHistory.Add(new SqlParameter("@VerificationCriteriaID", PobjClsDTOPurchase.intVerificationCriteria));

                MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseOrder", prmVerHistory);
                return true;
            }
            return false;
        }

        public bool UpdationVerificationTableRemarks(int intType, Int64 intReferenceID, string strRemarks)
        {
            ArrayList alParameters = new ArrayList();
            if (intType == 1)
            {
                alParameters.Add(new SqlParameter("@Mode", 27));
                alParameters.Add(new SqlParameter("@ReferenceID", intReferenceID));
                alParameters.Add(new SqlParameter("@CancelledBy", ClsCommonSettings.UserID));
                alParameters.Add(new SqlParameter("@Description", strRemarks));
                MobjDataLayer.ExecuteNonQuery("spInvPurchaseQuotation", alParameters);
            }
            else
            {
                alParameters.Add(new SqlParameter("@Mode", 24));
                alParameters.Add(new SqlParameter("@ReferenceID", intReferenceID));
                alParameters.Add(new SqlParameter("@CancelledBy", ClsCommonSettings.UserID));
                alParameters.Add(new SqlParameter("@Description", strRemarks));
                MobjDataLayer.ExecuteNonQuery("spPurchaseOrder", alParameters);
            }
            return true;
        }

        public string GetCompanyCurrency(int intCompanyID, out int intScale)
        {
            intScale = 2;
            string strCurrency = "";
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 20));
            prmPurchase.Add(new SqlParameter("@CompanyID", intCompanyID));
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spInvPurchaseModuleFunctions", prmPurchase);
            if (sdr.Read())
            {
                if (sdr["Currency"] != DBNull.Value)
                {
                    strCurrency = Convert.ToString(sdr["Currency"]);
                }
                if (sdr["Scale"] != DBNull.Value)
                {
                    intScale = Convert.ToInt32(sdr["Scale"]);
                }
            }
            return strCurrency;
        }

        public bool UpdateGrandTotalAmount(int intFormType)
        {
            // 2 - Purchase Order 4 - Purchase Invoice
            prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@GrandDiscountAmount", PobjClsDTOPurchase.decGrandDiscountAmt));
            prmPurchase.Add(new SqlParameter("@GrandAmount", PobjClsDTOPurchase.decGrandAmount));
            prmPurchase.Add(new SqlParameter("@NetAmount", PobjClsDTOPurchase.decNetAmount));
            prmPurchase.Add(new SqlParameter("@NetAmountRounded", PobjClsDTOPurchase.decNetAmountRounded));
            prmPurchase.Add(new SqlParameter("@ExpenseAmount", PobjClsDTOPurchase.decExpenseAmount));
            prmPurchase.Add(new SqlParameter("@TaxAmount", PobjClsDTOPurchase.decTaxAmount));

            switch (intFormType)
            {
                case 1:
                    prmPurchase.Add(new SqlParameter("@Mode", 22));
                    prmPurchase.Add(new SqlParameter("@PurchaseQuotationID", PobjClsDTOPurchase.intPurchaseID));
                    MobjDataLayer.ExecuteNonQuery("spInvPurchaseQuotation", prmPurchase);
                    break;
                case 2:
                    prmPurchase.Add(new SqlParameter("@Mode", 23));
                    prmPurchase.Add(new SqlParameter("@PurchaseOrderID", PobjClsDTOPurchase.intPurchaseID));
                    MobjDataLayer.ExecuteNonQuery("spPurchaseOrder", prmPurchase);
                    break;
                case 4:
                    prmPurchase.Add(new SqlParameter("@Mode", 23));
                    prmPurchase.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                    prmPurchase.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
                    prmPurchase.Add(new SqlParameter("@ExpensePosted", ClsMainSettings.ExpensePosted));

                    MobjDataLayer.ExecuteNonQuery("spPurchaseInvoice", prmPurchase);

                    if (PobjClsDTOPurchase.intPaymentTermsID == (int)PaymentTerms.Cash)
                    {
                        SavePayments(PobjClsDTOPurchase.intPurchaseID);
                    }


                    break;
            }
            return true;
        }

        public bool IsStockAdjusmentDone(int intItemID, Int64 intBatchID, int intWarehouseID)
        {
            ArrayList prmStockAdjustmentDetails = new ArrayList();
            prmStockAdjustmentDetails.Add(new SqlParameter("@Mode", "ChkSAD"));
            prmStockAdjustmentDetails.Add(new SqlParameter("@ItemID", intItemID));
            prmStockAdjustmentDetails.Add(new SqlParameter("@BatchID", intBatchID));
            prmStockAdjustmentDetails.Add(new SqlParameter("@CreatedDate", PobjClsDTOPurchase.strCreatedDate));
            prmStockAdjustmentDetails.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID));
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spInvStockAdjustment", prmStockAdjustmentDetails);
            if (sdr.Read())
            {
                if (sdr["DetailsCount"] != DBNull.Value)
                {
                    if (sdr["DetailsCount"].ToInt32() > 0)
                        return true;
                }
            }
            return false;
        }

        #region Pruchase Order Alert Methods

        public DataTable GetAlertSetting(long lngAlertSettingID)
        {
            ArrayList parameters = null;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 5));
                parameters.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));

                return MobjDataLayer.ExecuteDataTable("spAlertSetting", parameters);
            }
            catch (Exception ex) { throw ex; }
            finally { if (parameters != null) { parameters.Clear(); parameters = null; } }
        }

        public DataTable GetAlertRoleSetting(long lngAlertSettingID)
        {
            ArrayList parameters = null;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 6));
                parameters.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));

                return MobjDataLayer.ExecuteDataTable("spAlertRoleSetting", parameters);
            }
            catch (Exception ex) { throw ex; }
            finally { if (parameters != null) { parameters.Clear(); parameters = null; } }
        }

        public DataTable GetAlertUserSetting(long lngAlertSettingID)
        {
            ArrayList parameters = null;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 9));
                parameters.Add(new SqlParameter("@AlertSettingID", lngAlertSettingID));

                return MobjDataLayer.ExecuteDataTable("spAlertUserSetting", parameters);
            }
            catch (Exception ex) { throw ex; }
            finally { if (parameters != null) { parameters.Clear(); parameters = null; } }
        }

        public DataTable GetAlerts(int iOperationID, long lngReferenceID)
        {
            ArrayList parameters = null;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 6));
                parameters.Add(new SqlParameter("@OperationTypeID", iOperationID));
                parameters.Add(new SqlParameter("@ReferenceID", lngReferenceID));

                return MobjDataLayer.ExecuteDataTable("spAlert", parameters);
            }
            catch (Exception ex) { throw ex; }
            finally { if (parameters != null) { parameters.Clear(); parameters = null; } }
        }

        public DataTable GetSTOrganizationHierarchy()
        {
            ArrayList parameters = null;

            try
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 7));

                return MobjDataLayer.ExecuteDataTable("STOrganizationHierarchyTransaction", parameters);
            }
            catch (Exception ex) { throw ex; }
            finally { if (parameters != null) { parameters.Clear(); parameters = null; } }
        }

        public bool SaveAlert(DataTable datAlert)// insert
        {
            ArrayList parameters;
            object iOutAletID = 0;

            try
            {
                parameters = new ArrayList();
                foreach (DataRow dtAlertRow in datAlert.Rows)
                {
                    if (Convert.ToInt64(dtAlertRow["AlertID"]) > 0)
                    {
                        parameters.Add(new SqlParameter("@Mode", 2));//update
                        parameters.Add(new SqlParameter("@AlertID", dtAlertRow["AlertID"]));
                    }
                    else
                    {
                        parameters.Add(new SqlParameter("@Mode", 1));//insert
                    }

                    parameters.Add(new SqlParameter("@AlertUserSettingID", dtAlertRow["AlertUserSettingID"]));
                    parameters.Add(new SqlParameter("@ReferenceID", dtAlertRow["ReferenceID"]));
                    parameters.Add(new SqlParameter("@StartDate", dtAlertRow["StartDate"]));
                    parameters.Add(new SqlParameter("@AlertMessage", dtAlertRow["AlertMessage"]));
                    parameters.Add(new SqlParameter("@Status", dtAlertRow["Status"]));

                    SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
                    objParam.Direction = ParameterDirection.ReturnValue;
                    parameters.Add(objParam);

                    if (this.MobjDataLayer.ExecuteNonQuery("spAlert", parameters, out iOutAletID) != 0)
                    {
                        if (Convert.ToInt32(iOutAletID) > 0)
                        {
                            dtAlertRow["AlertID"] = iOutAletID;
                        }
                    }
                    else
                        return false;
                    parameters.Clear();
                }
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (MobjClsCommonUtility != null)
                MobjClsCommonUtility = null;
            if (MobjDataLayer != null)
                MobjDataLayer = null;
            if (prmPurchase != null)
                prmPurchase = null;
            if (prmPurchaseDetails != null)
                prmPurchaseDetails = null;
        }

        #endregion

        public DataSet PurchaseInvoiceDetailForStockAccount(Int64 Rownum)// Getting Invoice detail
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 25));
            prmCommon.Add(new SqlParameter("@PurchaseInvoiceID", Rownum));
            return MobjDataLayer.ExecuteDataSet("spPurchaseInvoice", prmCommon);
        }

        public bool UpdateOpStockAccount(int AccountId, int CompanyId, DateTime CurDate, double Amount)
        {
            //string sQuery = "EXEC PayAccountsSummary " + AccountId + "," + CompanyId + ",'" + CurDate + "',0," + Amount + ",1";
            //return MobjDataLayer.ExecuteReader(sQuery);
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 30));
            prmCommon.Add(new SqlParameter("@AccountId", AccountId));
            prmCommon.Add(new SqlParameter("@CompanyId", CompanyId));
            prmCommon.Add(new SqlParameter("@CurDate", CurDate));
            prmCommon.Add(new SqlParameter("@Amount", Amount));
            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvPurchaseModuleFunctions", prmCommon) != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DataSet GRNDetailForStockAccount(Int64 Rownum)// Getting Invoice detail
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 19));
            prmCommon.Add(new SqlParameter("@GRNID", Rownum));
            return MobjDataLayer.ExecuteDataSet("spGRN", prmCommon);
        }

        private int InsertLeadTimeDetails()
        {
            // Insert Lead Time Details
            object objOutLeadTimeID = 0;
            prmLeadTime = new ArrayList();
            prmLeadTime.Add(new SqlParameter("@Mode", 23));
            prmLeadTime.Add(new SqlParameter("@LeadTime", PobjClsDTOPurchase.decLeadTime));
            prmLeadTime.Add(new SqlParameter("@ProductionDate", PobjClsDTOPurchase.strProductionDate));
            prmLeadTime.Add(new SqlParameter("@TranshipmentDate", PobjClsDTOPurchase.strTranShipmentDate));
            prmLeadTime.Add(new SqlParameter("@ClearenceDate", PobjClsDTOPurchase.strClearenceDate));
            SqlParameter objParam1 = new SqlParameter("ReturnLeadTimeID", SqlDbType.Int);
            objParam1.Direction = ParameterDirection.ReturnValue;
            prmLeadTime.Add(objParam1);
            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvPurchaseQuotation", prmLeadTime, out objOutLeadTimeID) != 0)
            {
                PobjClsDTOPurchase.intLeadTimeDetsID = (int)objOutLeadTimeID;
            }
            // END
            return PobjClsDTOPurchase.intLeadTimeDetsID;
        }

        private int UpdateLeadTimeDetails()
        {
            object objOutLeadTimeID = 0;
            prmLeadTime = new ArrayList();
            // Update Lead Time Details
            prmLeadTime.Add(new SqlParameter("@Mode", 24));
            prmLeadTime.Add(new SqlParameter("@LeadTimeDetsID", PobjClsDTOPurchase.intLeadTimeDetsID));
            prmLeadTime.Add(new SqlParameter("@LeadTime", PobjClsDTOPurchase.decLeadTime));
            prmLeadTime.Add(new SqlParameter("@ProductionDate", PobjClsDTOPurchase.strProductionDate));
            prmLeadTime.Add(new SqlParameter("@TranshipmentDate", PobjClsDTOPurchase.strTranShipmentDate));
            prmLeadTime.Add(new SqlParameter("@ClearenceDate", PobjClsDTOPurchase.strClearenceDate));
            SqlParameter objParam1 = new SqlParameter("ReturnLeadTimeID", SqlDbType.Int);
            objParam1.Direction = ParameterDirection.ReturnValue;
            prmLeadTime.Add(objParam1);
            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvPurchaseQuotation", prmLeadTime, out objOutLeadTimeID) != 0)
            {
                PobjClsDTOPurchase.intLeadTimeDetsID = (int)objOutLeadTimeID;
            }
            // END
            return PobjClsDTOPurchase.intLeadTimeDetsID;
        }

        public string IsMandatoryDocumentsEntered(int intOperationTypeID, Int64 intReferenceID, int intCompanyID)
        {
            DataTable datMandatoryDocumentTypes = FillCombos(new string[] { "MDT.DocumentTypeID,DT.DocumentType", "MandatoryDocumentTypes MDT Inner Join DocumentTypeReference DT On DT.DocumentTypeID = MDT.DocumentTypeID ", "MDT.OperationTypeID = " + intOperationTypeID + " And MDT.CompanyID = " + intCompanyID + "" });
            foreach (DataRow dr in datMandatoryDocumentTypes.Rows)
            {
                DataTable datDocuments = FillCombos(new string[] { "DocumentTypeID", "DocDocumentMaster", "DocumentTypeID = " + dr["DocumentTypeID"].ToInt32() + " And OperationTypeID = " + intOperationTypeID + " And ReferenceID = " + intReferenceID + "" });
                if (datDocuments.Rows.Count == 0)
                    return dr["DocumentType"].ToString();
            }
            return "";
        }

        public string IsMandatoryDocumentExpired(int intOperationTypeID, int intVendorID, int intCompany)
        {
            string strRetValue = "";
            //prmPurchase = new ArrayList();
            //prmPurchase.Add(new SqlParameter("@Mode", 13));
            //prmPurchase.Add(new SqlParameter("@OperationTypeID", intOperationTypeID));
            //prmPurchase.Add(new SqlParameter("@VendorID", intVendorID));
            //prmPurchase.Add(new SqlParameter("@OperationTypeID1", intOperationTypeID));
            //prmPurchase.Add(new SqlParameter("@CompanyID", intCompany));
            //prmPurchase.Add(new SqlParameter("@CurrentDate", ClsCommonSettings.GetServerDate()));
            //DataTable datDocuments = MobjDataLayer.ExecuteDataTable("STDocumentMasterTransactions", prmPurchase);

            //if (datDocuments.Rows.Count > 0)
            //{
            //    strRetValue = Convert.ToString(datDocuments.Rows[0]["Description"]);

            //    return strRetValue;
            //}
            return strRetValue;
        }

        public string IsSupplierDocumentsValid(int intOperationTypeID, int intVendorID, int intCompany)
        {
            DataTable datMandatoryDocumentTypes = FillCombos(new string[] { "MDT.DocumentTypeID,DT.DocumentType", "MandatoryDocumentTypes MDT Inner Join DocumentTypeReference DT On DT.DocumentTypeID = MDT.DocumentTypeID ", "MDT.OperationTypeID = " + intOperationTypeID + " And MDT.CompanyID = " + intCompany + "" });
            foreach (DataRow dr in datMandatoryDocumentTypes.Rows)
            {
                DataTable datDocuments = FillCombos(new string[] { "DocumentTypeID", "DocDocumentMaster", "DocumentTypeID = " + dr["DocumentTypeID"].ToInt32() + " And OperationTypeID = " + intOperationTypeID + " And ReferenceID = " + intVendorID + "" });
                if (datDocuments.Rows.Count == 0)
                    return dr["DocumentType"].ToString();
            }
            return "";
        }

        private bool DeleteLeadTimeDetails()
        {
            // DELETE Lead Time Details
            prmLeadTime = new ArrayList();
            prmLeadTime.Add(new SqlParameter("@Mode", 25));
            prmLeadTime.Add(new SqlParameter("@LeadTimeDetsID", PobjClsDTOPurchase.intLeadTimeDetsID));
            if (MobjDataLayer.ExecuteNonQueryWithTran("spInvPurchaseQuotation", prmLeadTime) != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            // END
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 4));
            prmCommon.Add(new SqlParameter("@RoleID", intRoleID));
            prmCommon.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmCommon.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", prmCommon);
        }

        public DataTable GetOperationTypeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@RoleID", intRoleID));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", parameters);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 12));
            prmCommon.Add(new SqlParameter("@RoleID", intRoleID));
            prmCommon.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmCommon.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable("STPermissionSettings", prmCommon);
        }

        public bool Suggest(int intOperationTypeID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 26));
            alParameters.Add(new SqlParameter("@ReferenceID", PobjClsDTOPurchase.intPurchaseID));
            alParameters.Add(new SqlParameter("@Description", PobjClsDTOPurchase.strDescription));
            alParameters.Add(new SqlParameter("@StatusID", PobjClsDTOPurchase.intStatusID));
            alParameters.Add(new SqlParameter("@CancelledBy", PobjClsDTOPurchase.intCancelledBy));
            alParameters.Add(new SqlParameter("@CancellationDate", PobjClsDTOPurchase.strCancellationDate));
            if (MobjDataLayer.ExecuteNonQuery("spInvPurchaseQuotation", alParameters) != 0)
            {
                return true;
            }
            return false;
        }

        private bool SavePurchaseLocationDetails(bool blnIsGRN, bool blnIsFromCancellation)
        {

            int intSerialNo = 0;
            foreach (clsDTOPurchaseLocationDetails objLocationDetails in PobjClsDTOPurchase.lstClsDTOPurchaseLocationDetailsCollection)
            {
                ArrayList alParameters = new ArrayList();
                string strProcedureName = "";
                if (blnIsGRN)
                {
                    alParameters.Add(new SqlParameter("@Mode", 20));
                    alParameters.Add(new SqlParameter("@GRNID", PobjClsDTOPurchase.intPurchaseID));
                    strProcedureName = "spGRN";
                }
                else
                {
                    alParameters.Add(new SqlParameter("@Mode", 26));
                    alParameters.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                    strProcedureName = "spPurchaseInvoice";
                }

                objLocationDetails.lngBatchID = FindBatchID(objLocationDetails.strBatchNo);
                alParameters.Add(new SqlParameter("@SerialNo", ++intSerialNo));
                alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.intItemID));
                alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.lngBatchID));
                alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.intLocationID));
                alParameters.Add(new SqlParameter("@RowID", objLocationDetails.intRowID));
                alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.intBlockID));
                alParameters.Add(new SqlParameter("@LotID", objLocationDetails.intLotID));
                alParameters.Add(new SqlParameter("@UOMID", objLocationDetails.intUOMID));
                alParameters.Add(new SqlParameter("@Quantity", objLocationDetails.decQuantity));
                if (!blnIsFromCancellation)
                {
                    DataTable dtGetUomDetails = GetUomConversionValues(objLocationDetails.intUOMID, objLocationDetails.intItemID);
                    decimal decQuantity = objLocationDetails.decQuantity;
                    if (dtGetUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                            objLocationDetails.decQuantity = objLocationDetails.decQuantity / decConversionValue;
                        else if (intConversionFactor == 2)
                            objLocationDetails.decQuantity = objLocationDetails.decQuantity * decConversionValue;
                    }
                    if (blnIsGRN)
                    {
                        if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice)
                        {
                            UpdateItemLocationDetails(objLocationDetails, false, false);
                        }
                        else if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromTransfer)
                        {
                            int intTransferTypeID = FillCombos(new string[] { "OrderTypeID", "InvStockTransferMaster", "StockTransferID = " + PobjClsDTOPurchase.intReferenceID }).Rows[0]["OrderTypeID"].ToInt32();
                            if (intTransferTypeID == (int)OperationOrderType.STWareHouseToWareHouseTransferDemo)
                                UpdateItemLocationDetails(objLocationDetails, false, true);
                            else
                                UpdateItemLocationDetails(objLocationDetails, false, false);
                        }
                        else
                        {
                            UpdateItemLocationDetails(objLocationDetails, false, false);
                        }
                    }
                    else
                        UpdateItemLocationDetails(objLocationDetails, false, false);
                    objLocationDetails.decQuantity = decQuantity;
                }

                MobjDataLayer.ExecuteNonQueryWithTran(strProcedureName, alParameters);
            }
            return true;
        }

        private bool UpdateItemLocationDetails(clsDTOPurchaseLocationDetails objLocationDetails, bool blnIsDeletion, bool blnIsDemoQuantity)
        {
            ArrayList alParameters = new ArrayList();
            if (!blnIsDeletion)
                alParameters.Add(new SqlParameter("@Mode", 1));
            else
                alParameters.Add(new SqlParameter("@Mode", 2));
            if (!blnIsDemoQuantity)
            {
                alParameters.Add(new SqlParameter("@Type", 1));
                alParameters.Add(new SqlParameter("@Quantity", objLocationDetails.decQuantity));
            }
            else
            {
                alParameters.Add(new SqlParameter("@Type", 3));
                alParameters.Add(new SqlParameter("@DemoQuantity", objLocationDetails.decQuantity));
            }
            alParameters.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
            alParameters.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID));
            alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.intItemID));
            alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.lngBatchID));
            alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.intLocationID));
            alParameters.Add(new SqlParameter("@RowID", objLocationDetails.intRowID));
            alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.intBlockID));
            alParameters.Add(new SqlParameter("@LotID", objLocationDetails.intLotID));

            MobjDataLayer.ExecuteNonQueryWithTran("spInvItemLocationTransfer", alParameters);
            return true;
        }

        public decimal GetItemLocationQuantity(clsDTOPurchaseLocationDetails objLocationDetails, bool blnIsDemoQty)
        {
            ArrayList alParameters = new ArrayList();
            decimal decQuantity = 0;
            alParameters.Add(new SqlParameter("@Mode", 13));
            alParameters.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
            alParameters.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID));
            alParameters.Add(new SqlParameter("@ItemID", objLocationDetails.intItemID));
            alParameters.Add(new SqlParameter("@BatchID", objLocationDetails.lngBatchID));
            alParameters.Add(new SqlParameter("@LocationID", objLocationDetails.intLocationID));
            alParameters.Add(new SqlParameter("@RowID", objLocationDetails.intRowID));
            alParameters.Add(new SqlParameter("@BlockID", objLocationDetails.intBlockID));
            alParameters.Add(new SqlParameter("@LotID", objLocationDetails.intLotID));
            DataTable datLocationDetails = MobjDataLayer.ExecuteDataTable("spInvItemLocationTransfer", alParameters);
            if (datLocationDetails.Rows.Count > 0)
            {
                if (!blnIsDemoQty)
                {
                    if (datLocationDetails.Rows[0]["Quantity"] != DBNull.Value)
                        decQuantity = datLocationDetails.Rows[0]["Quantity"].ToDecimal();
                }
                else
                {
                    if (datLocationDetails.Rows[0]["DemoQuantity"] != DBNull.Value)
                        decQuantity = datLocationDetails.Rows[0]["DemoQuantity"].ToDecimal();
                }
            }
            return decQuantity;
        }

        public DataTable GetPurchaseLocationDetails(bool blnIsGRN)
        {
            ArrayList alParameters = new ArrayList();
            string strProcedureName = "";
            if (blnIsGRN)
            {
                alParameters.Add(new SqlParameter("@Mode", 22));
                alParameters.Add(new SqlParameter("@GRNID", PobjClsDTOPurchase.intPurchaseID));
                strProcedureName = "spGRN";
            }
            else
            {
                alParameters.Add(new SqlParameter("@Mode", 28));
                alParameters.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                strProcedureName = "spPurchaseInvoice";
            }
            return MobjDataLayer.ExecuteDataTable(strProcedureName, alParameters);
        }

        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 14));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@WarehouseID", intWarehouseID));
            alParameters.Add(new SqlParameter("@LocationWise", ClsCommonSettings.PblnIsLocationWise));
            return MobjDataLayer.ExecuteDataTable("spInvItemLocationTransfer", alParameters).Rows[0];
        }

        private bool DeletePurchaseLocationDetails(bool blnIsGRN, bool blnIsFromCancellation)
        {
            if (!blnIsFromCancellation)
            {
                DataTable datPurchaseLocationDetails = GetPurchaseLocationDetails(blnIsGRN);
                foreach (DataRow dr in datPurchaseLocationDetails.Rows)
                {
                    clsDTOPurchaseLocationDetails objLocationDetails = new clsDTOPurchaseLocationDetails();
                    objLocationDetails.intItemID = dr["ItemID"].ToInt32();
                    objLocationDetails.lngBatchID = dr["BatchID"].ToInt64();
                    objLocationDetails.intLocationID = dr["LocationID"].ToInt32();
                    objLocationDetails.intRowID = dr["RowID"].ToInt32();
                    objLocationDetails.intBlockID = dr["BlockID"].ToInt32();
                    objLocationDetails.intLotID = dr["LotID"].ToInt32();
                    objLocationDetails.intUOMID = dr["UomID"].ToInt32();
                    objLocationDetails.decQuantity = dr["Quantity"].ToDecimal();

                    DataTable dtGetUomDetails = GetUomConversionValues(objLocationDetails.intUOMID, objLocationDetails.intItemID);
                    decimal decQuantity = objLocationDetails.decQuantity;
                    if (dtGetUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                            objLocationDetails.decQuantity = objLocationDetails.decQuantity / decConversionValue;
                        else if (intConversionFactor == 2)
                            objLocationDetails.decQuantity = objLocationDetails.decQuantity * decConversionValue;
                    }

                    if (!blnIsGRN)
                        UpdateItemLocationDetails(objLocationDetails, true, false);
                    else
                    {
                        if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromInvoice)
                        {
                            UpdateItemLocationDetails(objLocationDetails, true, false);
                        }
                        else if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.GRNFromTransfer)
                        {
                            int intTransferTypeID = FillCombos(new string[] { "OrderTypeID", "InvStockTransferMaster", "StockTransferID = " + PobjClsDTOPurchase.intReferenceID }).Rows[0]["OrderTypeID"].ToInt32();
                            if (intTransferTypeID == (int)OperationOrderType.STWareHouseToWareHouseTransferDemo)
                                UpdateItemLocationDetails(objLocationDetails, true, true);
                            else
                                UpdateItemLocationDetails(objLocationDetails, true, false);
                        }
                        else
                        {
                            UpdateItemLocationDetails(objLocationDetails, true, false);
                        }
                    }
                }
            }
            string strProcedureName = "";
            ArrayList alParameters = new ArrayList();
            if (blnIsGRN)
            {
                strProcedureName = "spGRN";
                alParameters.Add(new SqlParameter("@Mode", 21));
                alParameters.Add(new SqlParameter("@GRNID", PobjClsDTOPurchase.intPurchaseID));
            }
            else
            {
                strProcedureName = "spPurchaseInvoice";
                alParameters.Add(new SqlParameter("@Mode", 27));
                alParameters.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
            }
            MobjDataLayer.ExecuteNonQueryWithTran(strProcedureName, alParameters);
            return true;
        }

        public bool DisplayStockTransferInfo(Int64 intStockTransferID) // StockTransferMasterDetails
        {
            ArrayList alParameters = new ArrayList();

            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@StockTransferID", intStockTransferID));
            SqlDataReader sdrStockTransfer = MobjDataLayer.ExecuteReader("spInvStockTransfer", alParameters, CommandBehavior.SingleRow);

            if (sdrStockTransfer.Read())
            {
                PobjClsDTOPurchase.intReferenceID = intStockTransferID;
                PobjClsDTOPurchase.intWarehouseID = sdrStockTransfer["ReferenceID"].ToInt32();
                PobjClsDTOPurchase.intFromWarehouseID = sdrStockTransfer["WarehouseID"].ToInt32();
                PobjClsDTOPurchase.intVendorID = sdrStockTransfer["VendorID"].ToInt32();
                if (sdrStockTransfer["VendorName"] != DBNull.Value) PobjClsDTOPurchase.strVendorName = Convert.ToString(sdrStockTransfer["VendorName"]); else PobjClsDTOPurchase.strVendorName = "";
                PobjClsDTOPurchase.intVendorAddID = sdrStockTransfer["VendorAddID"].ToInt32();
                if (sdrStockTransfer["AddressName"] != DBNull.Value) PobjClsDTOPurchase.strAddressName = Convert.ToString(sdrStockTransfer["AddressName"]); else PobjClsDTOPurchase.strAddressName = "";
                sdrStockTransfer.Close();
                return true;
            }
            else
            {
                sdrStockTransfer.Close();
                return false;
            }
        }

        public DataTable GetStockTransferDetails(Int64 intStockTransferID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 12));
            alParameters.Add(new SqlParameter("@StockTransferID", intStockTransferID));
            return MobjDataLayer.ExecuteDataTable("spInvStockTransfer", alParameters);
        }

        private void DeleteCostingAndPricingDetails(int intItemID, int intBatchID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 16));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            alParameters.Add(new SqlParameter("@BatchID", intBatchID));
            MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", alParameters);
        }

        public void SaveCostingMethodDetails(int intPurchaseGRNID, string strTempName,int OperationTypeID)
        {
            try
            {
                ArrayList alParameters = new ArrayList();
                DataTable datTempGRNMaster = new DataTable();
                DataTable datTempGRN = new DataTable();
                DataTable datTempPurchaseInvoiceRateGRN = new DataTable();
                if (strTempName == "PI")
                {
                    datTempGRNMaster = FillCombos(new string[] { "*", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID=" + intPurchaseGRNID });
                    if (OperationTypeID != (int)OperationOrderType.PInvoiceFromGRN)
                    {
                        datTempGRN = FillCombos(new string[] { "*", "InvPurchaseInvoiceDetail", "PurchaseInvoiceID=" + intPurchaseGRNID });
                    }
                    else
                        datTempGRN = GetPurchaseBatchDetails(PobjClsDTOPurchase.intPurchaseID);
                }
                else
                {
                    if (OperationTypeID == (int)OperationOrderType.GRNFromInvoice)
                    {
                        datTempGRNMaster = FillCombos(new string[] { "*", "InvGRNMaster G INNER JOIN InvPurchaseInvoiceMaster P " +
                    "ON G.ReferenceID=P.PurchaseInvoiceID", "G.GRNID=" + intPurchaseGRNID + " AND G.OrderTypeID=" + (int)OperationOrderType.GRNFromInvoice });
                        //datTempGRN = FillCombos(new string[] { "*", "InvGRNDetails GD INNER JOIN InvGRNMaster GM ON GD.GRNID=GM.GRNID " +
                        //"INNER JOIN InvPurchaseInvoiceDetail PD ON GM.ReferenceID=PD.PurchaseInvoiceID", "GD.GRNID=" + intPurchaseGRNID }); 
                        datTempGRN = FillCombos(new string[] { "*", "InvGRNDetails", "GRNID=" + intPurchaseGRNID });
                    }
                    else if (OperationTypeID == (int)OperationOrderType.GRNFromOrder)
                    {
                        datTempGRNMaster = FillCombos(new string[] { "*", "InvGRNMaster G INNER JOIN InvPurchaseOrderMaster P " +
                    "ON G.ReferenceID=P.PurchaseOrderID", "G.GRNID=" + intPurchaseGRNID + " AND G.OrderTypeID=" + (int)OperationOrderType.GRNFromOrder });
                        //datTempGRN = FillCombos(new string[] { "*", "InvGRNDetails GD INNER JOIN InvGRNMaster GM ON GD.GRNID=GM.GRNID " +
                        //"INNER JOIN InvPurchaseInvoiceDetail PD ON GM.ReferenceID=PD.PurchaseInvoiceID", "GD.GRNID=" + intPurchaseGRNID }); 
                        datTempGRN = FillCombos(new string[] { "*", "InvGRNDetails", "GRNID=" + intPurchaseGRNID });
                    }
                }
                if (datTempGRN != null && datTempGRN.Rows.Count > 0)
                {
                    for (int iCounter = 0; iCounter <= datTempGRN.Rows.Count - 1; iCounter++)
                    {
                        DataTable datTemp = FillCombos(new string[] { "*", "InvCostingAndPricingDetails", "ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString() });
                        DataTable datTempItem = FillCombos(new string[] { "*", "InvItemMaster", "ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString() });
                        DataTable datTempCurrency = FillCombos(new string[] { "CurrencyId AS CurrencyID", "CompanyMaster", "CompanyID = " +
                            "(SELECT (CASE WHEN CompanyID = 0 THEN 1 ELSE ISNULL(CompanyID,1) END) FROM InvItemMaster WHERE " +
                            "ItemID= " + datTempGRN.Rows[iCounter]["ItemID"].ToString() + ")" });
                        DataTable datTempUom = FillCombos(new string[] { "*", "InvUOMConversions", "UOMID = " + datTempItem.Rows[0]["BaseUomID"].ToString() + 
                                " AND OtherUOMID=" + datTempGRN.Rows[iCounter]["UOMID"].ToString() });

                        string strRate = "0", strActualRate = "0", strSaleRate = "0", strLandingCost = "0";
                        int intPricingSchemeID = 0;
                        if(datTemp!=null && datTemp.Rows.Count>0)
                            intPricingSchemeID = datTemp.Rows[0]["PricingSchemeID"].ToInt32();

                        //DataTable datTempRate = GetSaleRateForBatch(datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32(), intPricingSchemeID,
                        //    Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()));

                        DataTable datTempRate = GetSaleRate(datTemp.Rows[0]["CostingMethodID"].ToInt32(),
                            datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32(),
                            Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()));

                        if (datTempRate != null && datTempRate.Rows.Count > 0)
                        {
                            //if (strTempName == "PI")
                            //{
                            //    strRate = datTempGRN.Rows[iCounter]["PurchaseRate"].ToString();
                            //    strActualRate = datTempGRN.Rows[iCounter]["Rate"].ToString();
                            //}
                            //else
                            //{
                            //    if (OperationTypeID == (int)OperationOrderType.GRNFromInvoice)
                            //    {
                            //        datTempPurchaseInvoiceRateGRN = FillCombos(new string[] { "PD.PurchaseRate", "InvGRNDetails GD INNER JOIN InvGRNMaster GM ON GD.GRNID=GM.GRNID " +
                            //        "INNER JOIN InvPurchaseInvoiceDetail PD ON GM.ReferenceID=PD.PurchaseInvoiceID " +
                            //        "", "GD.GRNID=" + intPurchaseGRNID + "AND GD.ItemID=PD.ItemID AND GD.ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32() });
                            //        if (datTempPurchaseInvoiceRateGRN != null)
                            //        {
                            //            if (datTempPurchaseInvoiceRateGRN.Rows.Count > 0)
                            //                strRate = datTempPurchaseInvoiceRateGRN.Rows[0]["PurchaseRate"].ToString();
                            //        }

                            //        datTempPurchaseInvoiceRateGRN = FillCombos(new string[] { "PD.Rate", "InvGRNDetails GD INNER JOIN InvGRNMaster GM ON GD.GRNID=GM.GRNID " +
                            //        "INNER JOIN InvPurchaseInvoiceDetail PD ON GM.ReferenceID=PD.PurchaseInvoiceID " +
                            //        "", "GD.GRNID=" + intPurchaseGRNID + "AND GD.ItemID=PD.ItemID AND GD.ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32() });
                            //        if (datTempPurchaseInvoiceRateGRN != null)
                            //        {
                            //            if (datTempPurchaseInvoiceRateGRN.Rows.Count > 0)
                            //                strActualRate = datTempPurchaseInvoiceRateGRN.Rows[0]["Rate"].ToString();
                            //        }
                            //    }
                            //    else if (OperationTypeID == (int)OperationOrderType.GRNFromOrder)
                            //    {
                            //        datTempPurchaseInvoiceRateGRN = FillCombos(new string[] { "PD.Rate", "InvGRNDetails GD INNER JOIN InvGRNMaster GM ON GD.GRNID=GM.GRNID " +
                            //        "INNER JOIN InvPurchaseOrderDetail PD ON GM.ReferenceID=PD.PurchaseOrderID " +
                            //        "", "GD.GRNID=" + intPurchaseGRNID + "AND GD.ItemID=PD.ItemID AND GD.ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32() });
                            //        if (datTempPurchaseInvoiceRateGRN != null)
                            //        {
                            //            if (datTempPurchaseInvoiceRateGRN.Rows.Count > 0)
                            //                strRate = datTempPurchaseInvoiceRateGRN.Rows[0]["Rate"].ToString();
                            //        }

                            //        datTempPurchaseInvoiceRateGRN = FillCombos(new string[] { "PD.Rate", "InvGRNDetails GD INNER JOIN InvGRNMaster GM ON GD.GRNID=GM.GRNID " +
                            //        "INNER JOIN InvPurchaseOrderDetail PD ON GM.ReferenceID=PD.PurchaseOrderID " +
                            //        "", "GD.GRNID=" + intPurchaseGRNID + "AND GD.ItemID=PD.ItemID AND GD.ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32() });
                            //        if (datTempPurchaseInvoiceRateGRN != null)
                            //        {
                            //            if (datTempPurchaseInvoiceRateGRN.Rows.Count > 0)
                            //                strActualRate = datTempPurchaseInvoiceRateGRN.Rows[0]["Rate"].ToString();
                            //        }
                            //    }
                            //}

                            if (strRate == "0")
                                strRate = datTempRate.Rows[0]["Rate"].ToString();
                            if (strActualRate == "0")
                                strActualRate = datTempRate.Rows[0]["ActualRate"].ToString();
                            if (strLandingCost == "0")
                                strLandingCost = datTempRate.Rows[0]["LandingCost"].ToString();

                            if (Convert.ToInt32(datTempGRN.Rows[iCounter]["UomID"].ToString()) != Convert.ToInt32(datTempItem.Rows[0]["BaseUomID"].ToString()))
                            {
                                if (datTempUom.Rows[0]["ConversionFactorID"].ToString() == "1")
                                {
                                    strRate = (strRate.ToDouble() * datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strActualRate = (strActualRate.ToDouble() * datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strLandingCost = (strLandingCost.ToDouble() * datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                }
                                else
                                {
                                    strRate = (strRate.ToDouble() / datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strActualRate = (strActualRate.ToDouble() / datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strLandingCost = (strLandingCost.ToDouble() / datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                }
                            }

                            //strRate = (getRatewithExchangeRate(strRate.ToDecimal(), datTempGRNMaster.Rows[0]["CompanyID"].ToInt32(),
                            //    datTempGRNMaster.Rows[0]["CurrencyID"].ToInt32(), datTempGRN.Rows[iCounter]["ItemID"].ToInt32())).ToString();
                            //strActualRate = (getRatewithExchangeRate(strActualRate.ToDecimal(), datTempGRNMaster.Rows[0]["CompanyID"].ToInt32(),
                            //    datTempGRNMaster.Rows[0]["CurrencyID"].ToInt32(), datTempGRN.Rows[iCounter]["ItemID"].ToInt32())).ToString();

                            //if (Convert.ToInt32(datTempGRNMaster.Rows[0]["CompanyID"].ToString()) != Convert.ToInt32(datTempItem.Rows[0]["CompanyID"].ToString()))
                            //{
                            //    int intComID = 0;
                            //    if (Convert.ToInt32(datTempItem.Rows[0]["CompanyID"].ToString()) == 0)
                            //        intComID = 1;
                            //    else
                            //        intComID = Convert.ToInt32(datTempItem.Rows[0]["CompanyID"].ToString());
                            //    double dblExRate11 = getExchangeRate(Convert.ToInt32(datTempGRNMaster.Rows[0]["CurrencyID"].ToString()), intComID);
                            //    strRate = (strRate.ToDouble() * dblExRate11).ToString();
                            //    strActualRate = (strActualRate.ToDouble() * dblExRate11).ToString();
                            //}

                            alParameters = new ArrayList();
                            alParameters.Add(new SqlParameter("@Mode", 9));
                            alParameters.Add(new SqlParameter("@CostingMethodID", datTemp.Rows[0]["CostingMethodID"].ToString().ToInt32()));
                            alParameters.Add(new SqlParameter("@PricingSchemeID", datTemp.Rows[0]["PricingSchemeID"].ToString().ToInt32()));
                            alParameters.Add(new SqlParameter("@ItemID", datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32()));
                            alParameters.Add(new SqlParameter("@CurrencyID", datTempCurrency.Rows[0]["CurrencyID"].ToString().ToInt32()));
                            alParameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
                            alParameters.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDateTime()));
                            alParameters.Add(new SqlParameter("@BatchID", datTempGRN.Rows[iCounter]["BatchID"].ToString().ToInt32()));
                            alParameters.Add(new SqlParameter("@PurchaseRate", strRate.ToDouble()));
                            alParameters.Add(new SqlParameter("@ActualRate", strActualRate.ToDouble()));
                            alParameters.Add(new SqlParameter("@LandingCost", strLandingCost.ToDouble()));

                            DataTable datTempPricing = FillCombos(new string[] { "*", "InvPricingSchemeReference", "PricingSchemeID=" + datTemp.Rows[0]["PricingSchemeID"].ToString().ToInt32() });
                            //double dblExRate = getExchangeRate(Convert.ToInt32(datTempPricing.Rows[0]["CurrencyID"].ToString()), Convert.ToInt32(datTempGRNMaster.Rows[0]["CompanyID"].ToString()));
                            double dblExRate = 1;

                            if (datTempItem.Rows[0]["IsLandingCostEffected"].ToStringCustom() == "")
                                datTempItem.Rows[0]["IsLandingCostEffected"] = "False";

                            if (Convert.ToBoolean(datTempItem.Rows[0]["IsLandingCostEffected"].ToString()) == true)
                            {
                                if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                    strSaleRate = (strLandingCost.ToDouble() +
                                        (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                else
                                    strSaleRate = (strLandingCost.ToDouble() +
                                                (strLandingCost.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                            }
                            else
                            {
                                if (Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()) == true)
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate = (strRate.ToDouble() +
                                            (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                    else
                                        strSaleRate = (strRate.ToDouble() +
                                                    (strRate.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                                }
                                else
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate = (strActualRate.ToDouble() +
                                            (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                    else
                                        strSaleRate = (strActualRate.ToDouble() +
                                                    (strActualRate.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                                }
                            }
                            alParameters.Add(new SqlParameter("@SaleRate", strSaleRate.ToDouble()));
                            DeleteCostingAndPricingDetails(datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32(),
                                datTempGRN.Rows[iCounter]["BatchID"].ToString().ToInt32());
                            MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", alParameters);
                        }
                        else
                        {
                            string strRate1 = "0", strActualRate1 = "0", strSaleRate1 = "0", strLandingCost1 = "0";
                            DataTable datTempRate1 = GetSaleRate(datTemp.Rows[datTemp.Rows.Count - 1]["CostingMethodID"].ToString().ToInt32(), datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32(),
                                Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()));

                            //if (strTempName == "PI")
                            //{
                            //    strRate1 = datTempGRN.Rows[iCounter]["PurchaseRate"].ToString();
                            //    strActualRate1 = datTempGRN.Rows[iCounter]["Rate"].ToString();
                            //}
                            //else
                            //{
                            //    if (OperationTypeID == (int)OperationOrderType.GRNFromInvoice)
                            //    {
                            //        datTempPurchaseInvoiceRateGRN = FillCombos(new string[] { "PD.PurchaseRate", "InvGRNDetails GD INNER JOIN InvGRNMaster GM ON GD.GRNID=GM.GRNID " +
                            //        "INNER JOIN InvPurchaseInvoiceDetail PD ON GM.ReferenceID=PD.PurchaseInvoiceID " +
                            //        "", "GD.GRNID=" + intPurchaseGRNID + "AND GD.ItemID=PD.ItemID AND GD.ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32() });
                            //        if (datTempPurchaseInvoiceRateGRN != null)
                            //        {
                            //            if (datTempPurchaseInvoiceRateGRN.Rows.Count > 0)
                            //                strRate1 = datTempPurchaseInvoiceRateGRN.Rows[0]["PurchaseRate"].ToString();
                            //        }

                            //        datTempPurchaseInvoiceRateGRN = FillCombos(new string[] { "PD.Rate", "InvGRNDetails GD INNER JOIN InvGRNMaster GM ON GD.GRNID=GM.GRNID " +
                            //        "INNER JOIN InvPurchaseInvoiceDetail PD ON GM.ReferenceID=PD.PurchaseInvoiceID " +
                            //        "", "GD.GRNID=" + intPurchaseGRNID + "AND GD.ItemID=PD.ItemID AND GD.ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32() });
                            //        if (datTempPurchaseInvoiceRateGRN != null)
                            //        {
                            //            if (datTempPurchaseInvoiceRateGRN.Rows.Count > 0)
                            //                strActualRate1 = datTempPurchaseInvoiceRateGRN.Rows[0]["Rate"].ToString();
                            //        }
                            //    }
                            //    else if (OperationTypeID == (int)OperationOrderType.GRNFromOrder)
                            //    {
                            //        datTempPurchaseInvoiceRateGRN = FillCombos(new string[] { "PD.Rate", "InvGRNDetails GD INNER JOIN InvGRNMaster GM ON GD.GRNID=GM.GRNID " +
                            //        "INNER JOIN InvPurchaseOrderDetail PD ON GM.ReferenceID=PD.PurchaseOrderID " +
                            //        "", "GD.GRNID=" + intPurchaseGRNID + "AND GD.ItemID=PD.ItemID AND GD.ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32() });
                            //        if (datTempPurchaseInvoiceRateGRN != null)
                            //        {
                            //            if (datTempPurchaseInvoiceRateGRN.Rows.Count > 0)
                            //                strRate1 = datTempPurchaseInvoiceRateGRN.Rows[0]["Rate"].ToString();
                            //        }

                            //        datTempPurchaseInvoiceRateGRN = FillCombos(new string[] { "PD.Rate", "InvGRNDetails GD INNER JOIN InvGRNMaster GM ON GD.GRNID=GM.GRNID " +
                            //        "INNER JOIN InvPurchaseOrderDetail PD ON GM.ReferenceID=PD.PurchaseOrderID " +
                            //        "", "GD.GRNID=" + intPurchaseGRNID + "AND GD.ItemID=PD.ItemID AND GD.ItemID=" + datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32() });
                            //        if (datTempPurchaseInvoiceRateGRN != null)
                            //        {
                            //            if (datTempPurchaseInvoiceRateGRN.Rows.Count > 0)
                            //                strActualRate1 = datTempPurchaseInvoiceRateGRN.Rows[0]["Rate"].ToString();
                            //        }
                            //    }
                            //}
                            if (datTempRate1.Rows.Count == 0)
                            {
                                strRate1 = datTempGRN.Rows[0]["PurchaseRate"].ToStringCustom();
                                strActualRate1 = datTempGRN.Rows[0]["Rate"].ToStringCustom();
                                strLandingCost1 = datTempGRN.Rows[0]["LandingCost"].ToStringCustom();
                            }
                            else
                            {
                                if (strRate1 == "0")
                                    strRate1 = datTempRate1.Rows[0]["Rate"].ToString();
                                if (strActualRate1 == "0")
                                    strActualRate1 = datTempRate1.Rows[0]["ActualRate"].ToString();
                                if (strLandingCost1 == "0")
                                    strLandingCost1 = datTempRate1.Rows[0]["LandingCost"].ToString();
                            }
                            if (Convert.ToInt32(datTempGRN.Rows[iCounter]["UomID"].ToString()) != Convert.ToInt32(datTempItem.Rows[0]["BaseUomID"].ToString()))
                            {
                                if (datTempUom.Rows[0]["ConversionFactorID"].ToString() == "1")
                                {
                                    strRate1 = (strRate1.ToDouble() * datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strActualRate1 = (strActualRate1.ToDouble() * datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strLandingCost1 = (strLandingCost1.ToDouble() * datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                }
                                else
                                {
                                    strRate1 = (strRate1.ToDouble() / datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strActualRate1 = (strActualRate1.ToDouble() / datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                    strLandingCost1 = (strLandingCost1.ToDouble() / datTempUom.Rows[0]["ConversionRate"].ToDouble()).ToString();
                                }
                            }

                            //strRate1 = (getRatewithExchangeRate(strRate1.ToDecimal(), datTempGRNMaster.Rows[0]["CompanyID"].ToInt32(),
                            //    datTempGRNMaster.Rows[0]["CurrencyID"].ToInt32(), datTempGRN.Rows[iCounter]["ItemID"].ToInt32())).ToString();
                            //strActualRate1 = (getRatewithExchangeRate(strActualRate1.ToDecimal(), datTempGRNMaster.Rows[0]["CompanyID"].ToInt32(),
                            //    datTempGRNMaster.Rows[0]["CurrencyID"].ToInt32(), datTempGRN.Rows[iCounter]["ItemID"].ToInt32())).ToString();

                            //if (Convert.ToInt32(datTempGRNMaster.Rows[0]["CompanyID"].ToString()) != datTempItem.Rows[0]["CompanyID"].ToInt32())
                            //{
                            //    int intComID = 0;
                            //    if (datTempItem.Rows[0]["CompanyID"].ToInt32() == 0)
                            //        intComID = 1;
                            //    else
                            //        intComID = Convert.ToInt32(datTempItem.Rows[0]["CompanyID"].ToString());
                            //    double dblExRate12 = getExchangeRate(Convert.ToInt32(datTempGRNMaster.Rows[0]["CurrencyID"].ToString()), intComID);
                            //    strRate1 = (strRate1.ToDouble() * dblExRate12).ToString();
                            //    strActualRate1 = (strActualRate1.ToDouble() * dblExRate12).ToString();
                            //}

                            alParameters = new ArrayList();
                            alParameters.Add(new SqlParameter("@Mode", 9));
                            alParameters.Add(new SqlParameter("@CostingMethodID", datTemp.Rows[0]["CostingMethodID"].ToString().ToInt32()));
                            alParameters.Add(new SqlParameter("@PricingSchemeID", datTemp.Rows[0]["PricingSchemeID"].ToString().ToInt32()));
                            alParameters.Add(new SqlParameter("@ItemID", datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32()));
                            alParameters.Add(new SqlParameter("@CurrencyID", datTempCurrency.Rows[0]["CurrencyID"].ToString().ToInt32()));
                            alParameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
                            alParameters.Add(new SqlParameter("@CreatedDate", ClsCommonSettings.GetServerDateTime()));
                            alParameters.Add(new SqlParameter("@BatchID", datTempGRN.Rows[iCounter]["BatchID"].ToString().ToInt32()));
                            alParameters.Add(new SqlParameter("@PurchaseRate", strRate1.ToDouble()));
                            alParameters.Add(new SqlParameter("@ActualRate", strActualRate1.ToDouble()));
                            alParameters.Add(new SqlParameter("@LandingCost", strLandingCost1.ToDouble()));

                            DataTable datTempPricing = FillCombos(new string[] { "*", "InvPricingSchemeReference", "PricingSchemeID=" + datTemp.Rows[0]["PricingSchemeID"].ToString().ToInt32() });
                            //double dblExRate = getExchangeRate(Convert.ToInt32(datTempPricing.Rows[0]["CurrencyID"].ToString()), Convert.ToInt32(datTempGRNMaster.Rows[0]["CompanyID"].ToString()));
                            double dblExRate = 1;

                            if (datTempItem.Rows[0]["IsLandingCostEffected"].ToStringCustom() == "")
                                datTempItem.Rows[0]["IsLandingCostEffected"] = "False";

                            if (Convert.ToBoolean(datTempItem.Rows[0]["IsLandingCostEffected"].ToString()) == true)
                            {
                                if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                    strSaleRate1 = (strLandingCost1.ToDouble() +
                                        (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                else
                                    strSaleRate1 = (strLandingCost1.ToDouble() +
                                                (strLandingCost1.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                            }
                            else
                            {
                                if (Convert.ToBoolean(datTempItem.Rows[0]["CalculationBasedOnRate"].ToString()) == true)
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate1 = (strRate1.ToDouble() +
                                            (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                    else
                                        strSaleRate1 = (strRate1.ToDouble() +
                                                    (strRate1.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                                }
                                else
                                {
                                    if (datTempPricing.Rows[0]["PercentOrAmount"].ToString() == "1")
                                        strSaleRate1 = (strActualRate1.ToDouble() +
                                            (datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                    else
                                        strSaleRate1 = (strActualRate1.ToDouble() +
                                                    (strActualRate1.ToDouble() * datTempPricing.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                                }
                            }
                            alParameters.Add(new SqlParameter("@SaleRate", strSaleRate1.ToDouble()));
                            DeleteCostingAndPricingDetails(datTempGRN.Rows[iCounter]["ItemID"].ToString().ToInt32(),
                                datTempGRN.Rows[iCounter]["BatchID"].ToString().ToInt32());
                            MobjDataLayer.ExecuteNonQuery("spInvPricingScheme", alParameters);
                        }
                    }
                }
            }
            catch { }
        }

        public decimal getRatewithExchangeRate(decimal decRate, int intCompanyID, int intCurrencyID, int intItemID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 17));
            parameters.Add(new SqlParameter("@PurchaseRate", decRate));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@CurrencyID", intCurrencyID));
            parameters.Add(new SqlParameter("@ItemID", intItemID));
            return Convert.ToDecimal(MobjDataLayer.ExecuteScalar("spInvPricingScheme", parameters));
        }

        public DataTable GetSaleRate(int intMethodID, int intItemId, bool blnRate)
        {
            ArrayList parameters = new ArrayList();
            if (intMethodID == (int)CostingMethodReference.LIFO)
                parameters.Add(new SqlParameter("@Mode", 3));
            else if (intMethodID == (int)CostingMethodReference.FIFO)
                parameters.Add(new SqlParameter("@Mode", 4));
            else if (intMethodID == (int)CostingMethodReference.AVG)
                parameters.Add(new SqlParameter("@Mode", 5));
            else if (intMethodID == (int)CostingMethodReference.MIN)
                parameters.Add(new SqlParameter("@Mode", 6));
            else if (intMethodID == (int)CostingMethodReference.MAX)
                parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@ItemID", intItemId));
            parameters.Add(new SqlParameter("@CalculationBasedOnRate", blnRate));
            DataTable datPricingScheme = MobjDataLayer.ExecuteDataTable("spInvPricingScheme", parameters);
            return datPricingScheme;
        }

        public DataTable GetSaleRateForBatch(int intItemId, int intPricingSchemeID, bool blnRate)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@ItemID", intItemId));
            parameters.Add(new SqlParameter("@CalculationBasedOnRate", blnRate));
            parameters.Add(new SqlParameter("@PricingSchemeID", intPricingSchemeID));
            return MobjDataLayer.ExecuteDataTable("spInvPricingScheme", parameters);
        }

        public bool DeleteUnwantedBatchDetails()
        {
            DataTable datDeletedBatches = new DataTable();

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 10));
            alParameters.Add(new SqlParameter("@WarehouseID", PobjClsDTOPurchase.intWarehouseID));
            datDeletedBatches = MobjDataLayer.ExecuteDataTable("spInvStockMaster", alParameters);

            foreach (DataRow dr in datDeletedBatches.Rows)
            {
                DeleteCostingAndPricingDetailsFromOpeningStock(dr["ItemID"].ToInt32(), dr["BatchID"].ToInt32());
            }
            return true;
        }

        private double getExchangeRate(int intCurrencyID, int intCompanyID)
        {
            double dblExRate = 0;
            DataTable datTempExRate = FillCombos(new string[] { "ExchangeRate", "CurrencyDetails WHERE " +
                "CompanyID = " + intCompanyID + " AND CurrencyID = " + intCurrencyID + " AND ExchangeDate = (SELECT MAX(ExchangeDate) FROM CurrencyDetails ",
                "CompanyID = " + intCompanyID + " AND CurrencyID = " + intCurrencyID + ")" });
            if (datTempExRate != null)
            {
                if (datTempExRate.Rows.Count > 0)
                    dblExRate = Convert.ToDouble(datTempExRate.Rows[0]["ExchangeRate"].ToString());
            }
            return dblExRate;
        }

        //private decimal GetStockQuantity(bool blnIsDemoQty,int intItemID,long lngBatchID,int intCompanyId,int intWarehouseID)
        //{
        //    if (!blnIsDemoQty)
        //        return MobjClsCommonUtility.GetStockQuantity(intItemID,lngBatchID,intCompanyId,intWarehouseID);
        //    else
        //    {
        //        return MobjClsCommonUtility.FillCombos(new string[]{"ISNULL(DemoQty
        //    }
        //}

        public decimal GetAlreadyReturnedQty(long lngStockTransferID, int intItemId, long lngBatchID)
        {
            ArrayList alParamters = new ArrayList();
            alParamters.Add(new SqlParameter("@Mode", 24));
            alParamters.Add(new SqlParameter("@ReferenceID", lngStockTransferID));
            alParamters.Add(new SqlParameter("@ItemID", intItemId));
            alParamters.Add(new SqlParameter("@BatchID", lngBatchID));
            DataTable datRetQty = MobjDataLayer.ExecuteDataTable("spGRN", alParamters);
            if (datRetQty.Rows.Count > 0)
                return datRetQty.Rows[0]["ReturnedQty"].ToDecimal();

            return 0;
        }
        public bool DeleteReceiptAndPaymentDetails(long intTempPaymentID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@ReceiptAndPaymentID", intTempPaymentID));
            if (MobjDataLayer.ExecuteNonQueryWithTran("spAccReceiptsAndPayments", parameters) != 0)
                return true;
            else
                return false;
        }
        public bool SavePayments(long intPurchaseID)
        {
            //Saving Payments
            if (PobjClsDTOPurchase.decNetAmount - PobjClsDTOPurchase.decAdvAmount > 0)
            {
                int intTempPaymentID = 0;
                long lngSerialNo = 0;
                DataTable datTempPay = FillCombos(new string[] { "*", "AccReceiptAndPaymentMaster PM Inner Join AccReceiptAndPaymentDetails PD ON  PM.ReceiptAndPaymentID=PD.ReceiptAndPaymentID", "PD.ReferenceID=" + intPurchaseID + " AND PM.OperationTypeID=" + (int)OperationType.PurchaseInvoice + " AND PM.ReceiptAndPaymentTypeID=" + (int)PaymentTypes.InvoicePayment });
                if (datTempPay != null)
                {
                    if (datTempPay.Rows.Count > 0)
                    {
                        intTempPaymentID = Convert.ToInt32(datTempPay.Rows[0]["ReceiptAndPaymentID"].ToString());
                        lngSerialNo = datTempPay.Rows[0]["SerialNo"].ToInt64();
                        DeleteReceiptAndPaymentDetails(intTempPaymentID);
                    }
                }


                ArrayList prmPayments = new ArrayList();
                prmPayments.Add(new SqlParameter("@Mode", Convert.ToInt32(intTempPaymentID) == 0 ? 3 : 5));
                prmPayments.Add(new SqlParameter("@ReceiptAndPaymentID", Convert.ToInt32(intTempPaymentID)));
                prmPayments.Add(new SqlParameter("@PaymentDate", Convert.ToDateTime(PobjClsDTOPurchase.strDate).ToString("dd-MMM-yyyy")));
                prmPayments.Add(new SqlParameter("@OperationTypeID", (int)OperationType.PurchaseInvoice));
                prmPayments.Add(new SqlParameter("@ReceiptAndPaymentTypeID", (int)PaymentTypes.InvoicePayment));
                prmPayments.Add(new SqlParameter("@TransactionTypeID", (int)PaymentModes.Cash));
                //     prmPayments.Add(new SqlParameter("@CreditHeadID1", (int)Accounts.CashInHand));
                // prmPayments.Add(new SqlParameter("@DebitHeadID", 0));
                prmPayments.Add(new SqlParameter("@TotalAmount", ClsMainSettings.ExpensePosted ? PobjClsDTOPurchase.decGrandAmount : PobjClsDTOPurchase.decNetAmount - PobjClsDTOPurchase.decAdvAmount));
                prmPayments.Add(new SqlParameter("@IsDirect", 1));
                prmPayments.Add(new SqlParameter("@CurrencyID", PobjClsDTOPurchase.intCurrencyID));
                prmPayments.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchase.intCompanyID));
                prmPayments.Add(new SqlParameter("@CreatedBy", PobjClsDTOPurchase.intCreatedBy));
                prmPayments.Add(new SqlParameter("@CreatedDate", Convert.ToDateTime(PobjClsDTOPurchase.strCreatedDate).ToString("dd-MMM-yyyy")));
                prmPayments.Add(new SqlParameter("@Remarks", PobjClsDTOPurchase.strRemarks));
                prmPayments.Add(new SqlParameter("@VendorID", PobjClsDTOPurchase.intVendorID));
                // prmPayments.Add(new SqlParameter("@OperationModID", (Int32)TransactionTypes.CashPurchase));
                prmPayments.Add(new SqlParameter("@IsReceipt", "0"));
                // prmPayments.Add(new SqlParameter("@FormType", "PI"));
                SqlParameter objParam = new SqlParameter("@OutReceiptAndPaymentID", SqlDbType.BigInt);
                objParam.Direction = ParameterDirection.Output;
                prmPayments.Add(objParam);

                if (intTempPaymentID == 0)
                    lngSerialNo = GetMaxSerialNo(PobjClsDTOPurchase.intCompanyID);

                string strPaymentNumber = ClsCommonSettings.PaymentsPrefix + lngSerialNo.ToString();
                prmPayments.Add(new SqlParameter("@PaymentNumber", strPaymentNumber));
                prmPayments.Add(new SqlParameter("@SerialNo", lngSerialNo));

                object objOutPaymentID = 0;
                if (MobjDataLayer.ExecuteNonQueryWithTran("spAccReceiptsAndPayments", prmPayments, out objOutPaymentID) != 0)
                {
                    if (objOutPaymentID.ToInt64() > 0)
                    {
                        prmPayments = new ArrayList();
                        prmPayments.Add(new SqlParameter("@Mode", 4));
                        prmPayments.Add(new SqlParameter("@ReceiptAndPaymentID", objOutPaymentID.ToInt64()));
                        prmPayments.Add(new SqlParameter("@ReferenceID", intPurchaseID));
                        prmPayments.Add(new SqlParameter("@Amount",ClsMainSettings.ExpensePosted?PobjClsDTOPurchase.decGrandAmount:  PobjClsDTOPurchase.decNetAmount - PobjClsDTOPurchase.decAdvAmount));
                        if (MobjDataLayer.ExecuteNonQueryWithTran("spAccReceiptsAndPayments", prmPayments) != 0)
                            return true;
                    }
                    else

                        return false;
                }

                return false;
            }
            return true;
        }


        public bool DeletePayments(long intPurchaseID)
        {
            //Delete Payments
            ArrayList prmPayments = new ArrayList();
            ArrayList prmPayments2 = new ArrayList();

            int intTempPaymentID = 0;
            DataTable datTempPay = FillCombos(new string[] { "*", "AccReceiptAndPaymentMaster PM Inner Join AccReceiptAndPaymentDetails  PD ON PM.ReceiptAndPaymentID=PD.ReceiptAndPaymentID", "PD.ReferenceID=" + intPurchaseID + " AND PM.OperationTypeID=" + (int)OperationType.PurchaseInvoice });
            if (datTempPay != null)
            {
                if (datTempPay.Rows.Count > 0)
                {
                    for (int i = 0; i <= datTempPay.Rows.Count - 1; i++)
                    {
                        intTempPaymentID = Convert.ToInt32(datTempPay.Rows[i]["ReceiptAndPaymentID"].ToString());
                        prmPayments = new ArrayList();
                        prmPayments.Add(new SqlParameter("@Mode", 7));
                        prmPayments.Add(new SqlParameter("@ReceiptAndPaymentID", intTempPaymentID));
                        MobjDataLayer.ExecuteNonQueryWithTran("spAccReceiptsAndPayments", prmPayments);

                        prmPayments2 = new ArrayList();
                        prmPayments2.Add(new SqlParameter("@Mode", 6));
                        prmPayments2.Add(new SqlParameter("@ReceiptAndPaymentID", intTempPaymentID));
                        MobjDataLayer.ExecuteNonQueryWithTran("spAccReceiptsAndPayments", prmPayments2);
                    }
                    return true;
                }
            }   

          
            return false;
        }

        public Int64 GetMaxSerialNo(int intCompanyID)
        {
            // function for getting max of paymentsID
            ArrayList prmPayments = new ArrayList();
            prmPayments.Add(new SqlParameter("@Mode", 2));
            prmPayments.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPayments.Add(new SqlParameter("@IsReceipt", false));
            DataTable datTemp = MobjDataLayer.ExecuteDataTable("spAccReceiptsAndPayments", prmPayments);
            Int64 intRetValue = 0;
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    try { intRetValue = Convert.ToInt32(datTemp.Rows[0][0].ToString()); }
                    catch { }
                }
            }
            return intRetValue + 1;
      
        }

        public string GetCompanySettingsValue(string strConfigurationItem, string strSubItem, DataTable dtCompanySettings)
        {
            dtCompanySettings.DefaultView.RowFilter = "ConfigurationItem = '" + strConfigurationItem + "' And SubItem = '" + strSubItem + "'";
            if (dtCompanySettings.DefaultView.ToTable().Rows.Count > 0)
                return dtCompanySettings.DefaultView.ToTable().Rows[0]["ConfigurationValue"].ToString();
            return "";
        }

        private void OpeningStockAccountUpdationForPurchaseInvoice(bool blnAddStatus)
        {
            DataSet dtsTemp = new DataSet();
            if (blnAddStatus == false) // For Taking Old GRN Details
                dtsTemp = PurchaseInvoiceDetailForStockAccount(PobjClsDTOPurchase.intPurchaseID);
            try
            {
                if (!PobjClsDTOPurchase.blnGRNRequired) // When GRN not Required
                {
                    DataSet dtsTemp1 = PurchaseInvoiceDetailForStockAccount(PobjClsDTOPurchase.intPurchaseID); // For Taking Updated Purchase Invoice Details
                    if (blnAddStatus) // When Purchase Invoice Insertion
                    {
                        if (dtsTemp1.Tables[0].Rows.Count > 0)
                        {
                            int AccountId = Convert.ToInt32(dtsTemp1.Tables[1].Rows[0]["AccountId"].ToString());
                            int CompanyID = 0;
                            DateTime InvoiceDate = ClsCommonSettings.GetServerDate();
                            double Amount = 0;

                            for (int i = 0; i < dtsTemp1.Tables[0].Rows.Count; i++)
                            {
                                CompanyID = Convert.ToInt32(dtsTemp1.Tables[0].Rows[i]["CompanyID"].ToString());
                                InvoiceDate = Convert.ToDateTime(dtsTemp1.Tables[0].Rows[i]["InvoiceDate"].ToString());
                                Amount += Convert.ToDouble(dtsTemp1.Tables[0].Rows[i]["Amount"].ToString());
                            }
                            UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Old Amount Updation
                        }
                    }
                    else // When Purchase Invoice Updation
                    {
                        if (dtsTemp1.Tables[0].Rows.Count > 0)
                        {
                            int AccountId = Convert.ToInt32(dtsTemp1.Tables[1].Rows[0]["AccountId"].ToString());
                            int CompanyID = 0;
                            DateTime InvoiceDate = ClsCommonSettings.GetServerDate();
                            double Amount = 0;

                            if (PobjClsDTOPurchase.blnIsCancel == true)
                            {
                                if (dtsTemp.Tables[0].Rows.Count > 0)
                                {
                                    for (int j = 0; j < dtsTemp.Tables[0].Rows.Count; j++)
                                    {
                                        CompanyID = Convert.ToInt32(dtsTemp.Tables[0].Rows[j]["CompanyID"].ToString());
                                        InvoiceDate = Convert.ToDateTime(dtsTemp.Tables[0].Rows[j]["InvoiceDate"].ToString());
                                        Amount += Convert.ToDouble(dtsTemp.Tables[0].Rows[j]["Amount"].ToString());
                                    }
                                    string TempAmt = "-" + Amount.ToString();
                                    Amount = Convert.ToDouble(TempAmt);
                                    UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Old Amount Updation
                                }
                                CompanyID = 0;
                                InvoiceDate = ClsCommonSettings.GetServerDate();
                                Amount = 0;
                            }
                            else
                            {
                                if (PobjClsDTOPurchase.blnIsUpdate == true)
                                {
                                    if (dtsTemp.Tables[0].Rows.Count > 0)
                                    {
                                        for (int j = 0; j < dtsTemp.Tables[0].Rows.Count; j++)
                                        {
                                            CompanyID = Convert.ToInt32(dtsTemp.Tables[0].Rows[j]["CompanyID"].ToString());
                                            InvoiceDate = Convert.ToDateTime(dtsTemp.Tables[0].Rows[j]["InvoiceDate"].ToString());
                                            Amount += Convert.ToDouble(dtsTemp.Tables[0].Rows[j]["Amount"].ToString());
                                        }
                                        string TempAmt = "-" + Amount.ToString();
                                        Amount = Convert.ToDouble(TempAmt);
                                        UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Old Amount Updation
                                    }
                                }

                                CompanyID = 0;
                                InvoiceDate = ClsCommonSettings.GetServerDate();
                                Amount = 0;
                                for (int i = 0; i < dtsTemp1.Tables[0].Rows.Count; i++)
                                {
                                    CompanyID = Convert.ToInt32(dtsTemp1.Tables[0].Rows[i]["CompanyID"].ToString());
                                    InvoiceDate = Convert.ToDateTime(dtsTemp1.Tables[0].Rows[i]["InvoiceDate"].ToString());
                                    Amount += Convert.ToDouble(dtsTemp1.Tables[0].Rows[i]["Amount"].ToString());
                                }
                                UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For New Amount Updation
                            }
                        }
                        else
                        {
                            if (PobjClsDTOPurchase.blnIsCancel == true)
                            {
                                if (dtsTemp.Tables[0].Rows.Count > 0)
                                {
                                    int AccountId = Convert.ToInt32(dtsTemp.Tables[1].Rows[0]["AccountId"].ToString());
                                    int CompanyID = 0;
                                    DateTime InvoiceDate = ClsCommonSettings.GetServerDate();
                                    double Amount = 0;

                                    for (int j = 0; j < dtsTemp.Tables[0].Rows.Count; j++)
                                    {
                                        CompanyID = Convert.ToInt32(dtsTemp.Tables[0].Rows[j]["CompanyID"].ToString());
                                        InvoiceDate = Convert.ToDateTime(dtsTemp.Tables[0].Rows[j]["InvoiceDate"].ToString());
                                        Amount += Convert.ToDouble(dtsTemp.Tables[0].Rows[j]["Amount"].ToString());
                                    }
                                    string TempAmt = "-" + Amount.ToString();
                                    Amount = Convert.ToDouble(TempAmt);
                                    UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Old Amount Updation
                                }
                            }
                        }
                    }
                }
            }
            catch { }
            PobjClsDTOPurchase.blnIsCancel = false;
            PobjClsDTOPurchase.blnIsUpdate = false;
        }

        private void OpeningStockAccountUpdationForGRN(bool blnAddStatus)
        {
            DataSet dtsTemp = new DataSet();
            if (blnAddStatus == false) // For Taking Old GRN Details
                dtsTemp = GRNDetailForStockAccount(PobjClsDTOPurchase.intPurchaseID);
            try
            {
                DataSet dtsTemp1 = GRNDetailForStockAccount(PobjClsDTOPurchase.intPurchaseID); // For Taking Updated GRN Details
                if (blnAddStatus) // When GRN Insertion
                {
                    if (dtsTemp1.Tables[0].Rows.Count > 0)
                    {
                        int AccountId = Convert.ToInt32(dtsTemp1.Tables[1].Rows[0]["AccountId"].ToString());
                        int CompanyID = 0;
                        DateTime InvoiceDate = ClsCommonSettings.GetServerDate();
                        double Amount = 0;

                        for (int i = 0; i < dtsTemp1.Tables[0].Rows.Count; i++)
                        {
                            CompanyID = Convert.ToInt32(dtsTemp1.Tables[0].Rows[i]["CompanyID"].ToString());
                            InvoiceDate = Convert.ToDateTime(dtsTemp1.Tables[0].Rows[i]["GRNDate"].ToString());
                            Amount += Convert.ToDouble(dtsTemp1.Tables[0].Rows[i]["Amount"].ToString());
                        }
                        UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Old Amount Updation
                    }
                }
                else // When GRN Updation
                {
                    if (dtsTemp1.Tables[0].Rows.Count > 0)
                    {
                        int AccountId = Convert.ToInt32(dtsTemp1.Tables[1].Rows[0]["AccountId"].ToString());
                        int CompanyID = 0;
                        DateTime InvoiceDate = ClsCommonSettings.GetServerDate();
                        double Amount = 0;

                        if (PobjClsDTOPurchase.blnIsCancel == true)
                        {
                            if (dtsTemp.Tables[0].Rows.Count > 0)
                            {
                                for (int j = 0; j < dtsTemp.Tables[0].Rows.Count; j++)
                                {
                                    CompanyID = Convert.ToInt32(dtsTemp.Tables[0].Rows[j]["CompanyID"].ToString());
                                    InvoiceDate = Convert.ToDateTime(dtsTemp.Tables[0].Rows[j]["GRNDate"].ToString());
                                    Amount += Convert.ToDouble(dtsTemp.Tables[0].Rows[j]["Amount"].ToString());
                                }
                                string TempAmt = "-" + Amount.ToString();
                                Amount = Convert.ToDouble(TempAmt);
                                UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Old Amount Updation
                            }
                            CompanyID = 0;
                            InvoiceDate = ClsCommonSettings.GetServerDate();
                            Amount = 0;
                        }
                        else
                        {
                            if (PobjClsDTOPurchase.blnIsUpdate == true)
                            {
                                if (dtsTemp.Tables[0].Rows.Count > 0)
                                {
                                    for (int j = 0; j < dtsTemp.Tables[0].Rows.Count; j++)
                                    {
                                        CompanyID = Convert.ToInt32(dtsTemp.Tables[0].Rows[j]["CompanyID"].ToString());
                                        InvoiceDate = Convert.ToDateTime(dtsTemp.Tables[0].Rows[j]["GRNDate"].ToString());
                                        Amount += Convert.ToDouble(dtsTemp.Tables[0].Rows[j]["Amount"].ToString());
                                    }
                                    string TempAmt = "-" + Amount.ToString();
                                    Amount = Convert.ToDouble(TempAmt);
                                    UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Old Amount Updation
                                }
                            }

                            CompanyID = 0;
                            InvoiceDate = ClsCommonSettings.GetServerDate();
                            Amount = 0;
                            for (int i = 0; i < dtsTemp1.Tables[0].Rows.Count; i++)
                            {
                                CompanyID = Convert.ToInt32(dtsTemp1.Tables[0].Rows[i]["CompanyID"].ToString());
                                InvoiceDate = Convert.ToDateTime(dtsTemp1.Tables[0].Rows[i]["GRNDate"].ToString());
                                Amount += Convert.ToDouble(dtsTemp1.Tables[0].Rows[i]["Amount"].ToString());
                            }
                            UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For New Amount Updation
                        }
                    }
                    else
                    {
                        if (PobjClsDTOPurchase.blnIsCancel == true)
                        {
                            if (dtsTemp.Tables[0].Rows.Count > 0)
                            {
                                int AccountId = Convert.ToInt32(dtsTemp.Tables[1].Rows[0]["AccountId"].ToString());
                                int CompanyID = 0;
                                DateTime InvoiceDate = ClsCommonSettings.GetServerDate();
                                double Amount = 0;

                                for (int j = 0; j < dtsTemp.Tables[0].Rows.Count; j++)
                                {
                                    CompanyID = Convert.ToInt32(dtsTemp.Tables[0].Rows[j]["CompanyID"].ToString());
                                    InvoiceDate = Convert.ToDateTime(dtsTemp.Tables[0].Rows[j]["GRNDate"].ToString());
                                    Amount += Convert.ToDouble(dtsTemp.Tables[0].Rows[j]["Amount"].ToString());
                                }
                                string TempAmt = "-" + Amount.ToString();
                                Amount = Convert.ToDouble(TempAmt);
                                UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Old Amount Updation
                            }
                        }
                    }
                }
            }
            catch { }
            PobjClsDTOPurchase.blnIsCancel = false;
            PobjClsDTOPurchase.blnIsUpdate = false;
        }

        private void DeleteOpeningStockAccountForPurchaseInvoice()
        {
            try
            {
                if (!PobjClsDTOPurchase.blnGRNRequired) // When GRN not Required
                {
                    DataSet dtsTemp2 = PurchaseInvoiceDetailForStockAccount(PobjClsDTOPurchase.intPurchaseID); // For Taking Updated Purchase Invoice Details

                    if (dtsTemp2.Tables[0].Rows.Count > 0)
                    {
                        int AccountId = Convert.ToInt32(dtsTemp2.Tables[1].Rows[0]["AccountId"].ToString());
                        int CompanyID = 0;
                        DateTime InvoiceDate = ClsCommonSettings.GetServerDate();
                        double Amount = 0;

                        for (int j = 0; j < dtsTemp2.Tables[0].Rows.Count; j++)
                        {
                            CompanyID = Convert.ToInt32(dtsTemp2.Tables[0].Rows[j]["CompanyID"].ToString());
                            InvoiceDate = Convert.ToDateTime(dtsTemp2.Tables[0].Rows[j]["InvoiceDate"].ToString());
                            Amount += Convert.ToDouble(dtsTemp2.Tables[0].Rows[j]["Amount"].ToString());
                        }
                        string TempAmt = "-" + Amount.ToString();
                        Amount = Convert.ToDouble(TempAmt);
                        UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Old Amount Updation
                    }
                }
            }
            catch { }
        }
        private void DeleteOpeningStockAccountForGRN()
        {
            try
            {
                DataSet dtsTemp1 = GRNDetailForStockAccount(PobjClsDTOPurchase.intPurchaseID); // For Taking Updated GRN Details
                if (dtsTemp1.Tables[0].Rows.Count > 0)
                {
                    int AccountId = Convert.ToInt32(dtsTemp1.Tables[1].Rows[0]["AccountId"].ToString());
                    int CompanyID = 0;
                    DateTime InvoiceDate = ClsCommonSettings.GetServerDate();
                    double Amount = 0;

                    for (int j = 0; j < dtsTemp1.Tables[0].Rows.Count; j++)
                    {
                        CompanyID = Convert.ToInt32(dtsTemp1.Tables[0].Rows[j]["CompanyID"].ToString());
                        InvoiceDate = Convert.ToDateTime(dtsTemp1.Tables[0].Rows[j]["GRNDate"].ToString());
                        Amount += Convert.ToDouble(dtsTemp1.Tables[0].Rows[j]["Amount"].ToString());
                    }
                    string TempAmt = "-" + Amount.ToString();
                    Amount = Convert.ToDouble(TempAmt);
                    UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount);  // For Taking Old Amount Updation
                }
            }
            catch { }
        }
        public DataTable DisplayPurchaseOrderGRNDetail(Int64 Rownum)// Display Invoice detail
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 25));
            prmCommon.Add(new SqlParameter("@PurchaseOrderID", Rownum));
            return MobjDataLayer.ExecuteDataSet("spPurchaseOrder", prmCommon).Tables[0];

        }
        /// <summary>
        /// Gets Suppliers purchase history of an item 
        /// </summary>
        /// <param name="CompanyID"></param>
        /// <param name="SupplierID"></param>
        /// <param name="ItemID"></param>
        /// <returns>datatable</returns>
        public DataTable GetSupplierpurchaseHistory(int CompanyID, int SupplierID, int ItemID)
        {
            return MobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", new List<SqlParameter>{
                new SqlParameter("@Mode",34),
                new SqlParameter("@CompanyID",CompanyID),
                new SqlParameter("@VendorID",SupplierID),
                new SqlParameter("@ItemID",ItemID)
            });
        }

        /// <summary>
        /// Gets FromOrder Datatable in GRN 
        /// </summary>
        /// <returns>datatable</returns>
        public DataTable GetPurchaseOrder(int CompanyID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 29));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            return MobjDataLayer.ExecuteDataTable("spPurchaseOrder", alParameters);
        }
        public bool PurchaseNoExists(int OrderType, long PrchaseID,int CompanyID)
        {
            ArrayList alParameters = new ArrayList();
            object objRowCount=0;
            if(OrderType==(int)OperationOrderType.GRNFromOrder)
            {
            alParameters.Add(new SqlParameter("@Mode", 31));
            alParameters.Add(new SqlParameter("@PurchaseOrderID", PrchaseID));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            objRowCount = MobjDataLayer.ExecuteScalar("spPurchaseOrder", alParameters);
          
            }
            else if(OrderType==(int)OperationOrderType.GRNFromInvoice)
            {
                alParameters.Add(new SqlParameter("@Mode", 31));
                alParameters.Add(new SqlParameter("@PurchaseInvoiceID", PrchaseID));
                alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
                objRowCount = MobjDataLayer.ExecuteScalar("spPurchaseInvoice", alParameters);
            }
          
            return objRowCount.ToInt32() > 0;

        }

        public DataTable GetDataPInvoiceFromGRN(string strConditin, long GRNID, long value, bool chkValue)
        {
            ArrayList parameters = new ArrayList();
            if (value > 0 && chkValue==true)
            {
                parameters.Add(new SqlParameter("@Mode", 47));
                parameters.Add(new SqlParameter("@ReferenceID", GRNID));

            }
            else
            {
                parameters.Add(new SqlParameter("@Mode", 33));
                parameters.Add(new SqlParameter("@SearchCondition", strConditin));
            }
            return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", parameters);
        }

        public bool DeletePurchaseInvoiceReferenceDetails(long PurchaseInvoiceID)
        {
            //function for deleting purchase indent details

            prmPurchaseDetails = new ArrayList();
            prmPurchaseDetails.Add(new SqlParameter("@Mode", 37));
            prmPurchaseDetails.Add(new SqlParameter("@PurchaseInvoiceID", PurchaseInvoiceID));
            MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseInvoice", prmPurchaseDetails);
            return true;
        }

        public bool SavePurchaseInvoiceReference(long PurchaseInvoiceID, string SearchCondition)
        {
            //This is to Update Purchase Order Detail status

            ArrayList prmPuchaseStatus = new ArrayList();
            prmPuchaseStatus = new ArrayList();
            prmPuchaseStatus.Add(new SqlParameter("@Mode", 34));
            prmPuchaseStatus.Add(new SqlParameter("@SearchCondition", SearchCondition));
            prmPuchaseStatus.Add(new SqlParameter("@PurchaseInvoiceID", PurchaseInvoiceID));
            int value = MobjDataLayer.ExecuteNonQuery("spPurchaseInvoice", prmPuchaseStatus);
            if (value > 0)
                return true;
            else
                return false;
        }

        public DataTable GetSavedPurchaseInvoiceReference(long PurchaseInvoiceID)
        {

            ArrayList parameters = new ArrayList();
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 66));
            parameters.Add(new SqlParameter("@PurchaseInvoiceID", PurchaseInvoiceID));
            return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", parameters); ;
        }
        
        public DataTable GetPurchaseInvoiceReference(long PurchaseInvoiceID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 36));
            parameters.Add(new SqlParameter("@PurchaseInvoiceID", PurchaseInvoiceID));
            return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", parameters);
        }

        public DataTable GetGRNNumbers(int CompanyID, int VendorID, int OrderTypeID, bool MblnAddStatus,int WarehouseID)
        {
            ArrayList parameters = new ArrayList();
            if (MblnAddStatus)
            {
                parameters.Add(new SqlParameter("@Mode", 41));
            }
            else
            {
                parameters.Add(new SqlParameter("@Mode", 43));
            }
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            parameters.Add(new SqlParameter("@VendorID", VendorID));
            parameters.Add(new SqlParameter("@OrderTypeID", OrderTypeID));
            parameters.Add(new SqlParameter("@WarehouseID", WarehouseID));
            parameters.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
            return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", parameters);
        }

        public bool CheckPurchaseInvoiceExists(int intValue)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 44));
            parameters.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
            parameters.Add(new SqlParameter("@Value", intValue));
            int value= Convert.ToInt32(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
            if (value > 0)
                return true;
            else
                return false;
        }

        public int GetwarehouseID(int PurchaseOrderID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 76));
            parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            int value = Convert.ToInt32(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
            return value;
        }

        public bool CheckPurchaseInvoiceExistsAgainstGRN(long GRNID,int ItemID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 48));
            parameters.Add(new SqlParameter("@ReferenceID", GRNID));
            parameters.Add(new SqlParameter("@ItemID", ItemID));
            int value = Convert.ToInt32(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
            if (value > 0)
                return true;
            else
                return false;
        }

        public DataTable DisplayPurchaseDirectGRNDetail(long GRNID)// Display Invoice detail
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 50));
            prmCommon.Add(new SqlParameter("@ReferenceID", GRNID));
            return MobjDataLayer.ExecuteDataSet("spPurchaseInvoice", prmCommon).Tables[0];

        }

        public decimal GetTotalDiscount(int ItemID, long PurchaseOrderID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 51));
            parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            parameters.Add(new SqlParameter("@ItemID", ItemID));
            Decimal value = Convert.ToDecimal(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
            return value;
        }
        public decimal GetOldDiscount(int ItemID, long PurchaseOrderID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 52));
            parameters.Add(new SqlParameter("@PurchaseOrderID",PobjClsDTOPurchase.intPurchaseID));
            parameters.Add(new SqlParameter("@ItemID", ItemID));
            Decimal value = Convert.ToDecimal(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
            return value;
        }

        public decimal GetReceivedDiscount(long PurchaseOrderID, int ItemID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 53));
            parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            parameters.Add(new SqlParameter("@ItemID", ItemID));
            Decimal value = Convert.ToDecimal(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
            return value;
        }

        public bool CheckExpenseExists(long PurchaseOrderID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 54));
            parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            int value = Convert.ToInt32(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
            if (value > 0)
                return true;
            else
                return false;
        }

        public decimal GetGrandTotalDiscount(long PurchaseOrderID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 55));
            parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            Decimal value = Convert.ToDecimal(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
            return value;
        }
        public decimal GetGrandOldDiscount(long PurchaseOrderID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 56));
            parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            Decimal value = Convert.ToDecimal(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
               return value;
        }

        public decimal GetGrandReceivedDiscount(long PurchaseInvoiceID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 57));
            parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseInvoiceID));
            Decimal value = Convert.ToDecimal(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
            return value;
        }


        public DataTable GetAllDataPInvoiceFromGRN(string strConditin)
        {
            ArrayList parameters = new ArrayList();
   
                parameters.Add(new SqlParameter("@Mode", 33));
                parameters.Add(new SqlParameter("@SearchCondition", strConditin));
            
            return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", parameters);
        }

        public bool IsGRNExists(string StrConditon, long PurchaseInvoiceID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 59));
            parameters.Add(new SqlParameter("@SearchCondition", StrConditon));
            parameters.Add(new SqlParameter("@PurchaseInvoiceID", PurchaseInvoiceID));
            int value = Convert.ToInt32(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
            if (value > 0)
                return true;
            else
                return false;
        }

        public bool IsPurchaseOrderExists(string strCondition)
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 60));
            Parameters.Add(new SqlParameter("@SearchCondition", strCondition));
            object objReturnValue = MobjDataLayer.ExecuteScalar("spPurchaseInvoice", Parameters);
            return objReturnValue.ToInt32() > 0;
        }

        public DataTable GetOldPurchaseAmount(long PurchaseInvoiceID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 62));
            parameters.Add(new SqlParameter("@PurchaseInvoiceID", PurchaseInvoiceID));
            return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", parameters);
        }

        public int CheckReferenceForBatchInfo(long PurchaseInvoiceID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 63));
            parameters.Add(new SqlParameter("@PurchaseInvoiceID", PurchaseInvoiceID));
            int value = Convert.ToInt32(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
            return value;
        }

        public bool CheckPurchaseInvoiceExistingAgainstGRN(long GRNID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 65));
            parameters.Add(new SqlParameter("@ReferenceID", GRNID));
            int value = Convert.ToInt32(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
            if (value > 0)
                return true;
            else
                return false;
        }

        public DataTable GetPurchaseRates(long PurchaseInvoiceID,int ItemID)
        {
            ArrayList parameters = new ArrayList();
   
                parameters.Add(new SqlParameter("@Mode", 67));
                parameters.Add(new SqlParameter("@PurchaseInvoiceID", PurchaseInvoiceID));
                parameters.Add(new SqlParameter("@ItemID", ItemID));
            return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", parameters);
        }

        public DataTable GetPurchaseBatchDetails(long PurchaseInvoiceID)
        {
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 68));
            parameters.Add(new SqlParameter("@PurchaseInvoiceID", PurchaseInvoiceID));
            return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", parameters);
        }

        public DataTable GetGRNPurchaseRate(string strCondition, int ItemID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 69));
            parameters.Add(new SqlParameter("@SearchCondition", strCondition));
            parameters.Add(new SqlParameter("@ItemID", ItemID));
            return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", parameters);
        }


        public DataTable GetSingleGRNPurchaseRate(long GRNID, int ItemID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 70));
            parameters.Add(new SqlParameter("@GRNID", GRNID));
            parameters.Add(new SqlParameter("@ItemID", ItemID));
            return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", parameters);
        }

        public decimal CalculateGRNPurchaseRate(decimal PurchaseRate, int UOMID,int ItemID)
        {
            decimal decPurchaseRate = PurchaseRate;

            DataTable dtGetUomDetails = GetUomConversionValues(UOMID, ItemID);
            if (dtGetUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                {
                    decPurchaseRate = decPurchaseRate * decConversionValue;
                }
                else if (intConversionFactor == 2)
                {
                    decPurchaseRate = decPurchaseRate / decConversionValue;
                }
            }

            return decPurchaseRate;
        }

        public DataSet SetCheckedListbox(long GRNID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 71));
            parameters.Add(new SqlParameter("@GRNID", GRNID));
            return MobjDataLayer.ExecuteDataSet("spPurchaseInvoice", parameters);
        }
        public DataTable GetSavedPurchaseInvoiceReferenceGRN(long PurchaseInvoiceID,int ItemID)
        {

            ArrayList parameters = new ArrayList();
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 73));
            parameters.Add(new SqlParameter("@PurchaseInvoiceID", PurchaseInvoiceID));
            parameters.Add(new SqlParameter("@ItemID", ItemID));
            return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", parameters); ;
        }

        public bool CheckGRNExistingAgainstOrder()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 74));
            parameters.Add(new SqlParameter("@PurchaseOrderID", PobjClsDTOPurchase.intPurchaseOrderID));
            int value = Convert.ToInt32(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
            if (value > 0)
                return true;
            else
                return false;
        }

        public DataTable GetGRNBatchExistingAgainstGRN(int ItemID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 75));
            parameters.Add(new SqlParameter("@PurchaseOrderID", PobjClsDTOPurchase.intPurchaseOrderID));
            parameters.Add(new SqlParameter("@ItemID", ItemID));
            return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", parameters);
        }

        public decimal GetQuantiyForBatch(long BatchID, int ItemID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 77));
            parameters.Add(new SqlParameter("@BatchID", BatchID));
            parameters.Add(new SqlParameter("@ItemID", ItemID));
            Decimal value = Convert.ToDecimal(MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters));
            return value;
        }

        public bool IsPurchaseOrderCancelled(long PurchaseOrderID)
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 78));
            Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            object objReturnValue = MobjDataLayer.ExecuteScalar("spPurchaseInvoice", Parameters);
            return objReturnValue.ToInt32() > 0;
        }

        public bool IsPurchaseInvoiceExists(long GRNID)
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 79));
            Parameters.Add(new SqlParameter("@GRNID", GRNID));
            object objReturnValue = MobjDataLayer.ExecuteScalar("spPurchaseInvoice", Parameters);
            return objReturnValue.ToInt32() > 0;
        }
        public bool ISWarehouseActive(int WarehouseID)
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 80));
            Parameters.Add(new SqlParameter("@WarehouseID", WarehouseID));
            object objReturnValue = MobjDataLayer.ExecuteScalar("spPurchaseInvoice", Parameters);
            return objReturnValue.ToInt32() > 0;
        }
        private bool IsBatchExists(int intItemID)
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 27));
            Parameters.Add(new SqlParameter("@ItemID", intItemID));
            object obj = MobjDataLayer.ExecuteScalar("spInvDirectGRN", Parameters);
            return obj.ToInt32() > 0;
        }

        private void UpdateGRNStatus(long PurchaseInvoiceID)
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 81));
            Parameters.Add(new SqlParameter("@PurchaseInvoiceID", PurchaseInvoiceID));
            MobjDataLayer.ExecuteNonQuery("spPurchaseInvoice", Parameters);
        }
        private void UpdateGRNStatusPIDeleted(long PurchaseInvoiceID, string strCondition,int IntDelete)
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 81));
            Parameters.Add(new SqlParameter("@PurchaseInvoiceID", PurchaseInvoiceID));
            Parameters.Add(new SqlParameter("@Del", IntDelete));
            MobjDataLayer.ExecuteNonQuery("spPurchaseInvoice", Parameters);
        }
        public bool GRNReferenceExist(long PurchaseInvoiceID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 83));
            alParameters.Add(new SqlParameter("@PurchaseInvoiceID", PurchaseInvoiceID));
            object result = MobjDataLayer.ExecuteScalar("spPurchaseInvoice", alParameters);
            return Convert.ToInt32(result) > 0;
        }
        public DataTable GetPaymentReferences(long PurchaseInvoiceID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 84));
            parameters.Add(new SqlParameter("@ReferenceID", PurchaseInvoiceID));
            return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", parameters);
        }

        private void GRNSummaryUpdation()
        {
            int intOrderTypeID = 0;
            if (PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder)
                intOrderTypeID = (MobjClsCommonUtility.FillCombos(new string[] { "POM.OrderTypeID", "InvPurchaseInvoiceMaster PIM INNER JOIN InvPurchaseInvoiceReferenceDetails PIRD ON PIRD.PurchaseInvoiceID = PIM.PurchaseInvoiceID INNER JOIN InvPurchaseOrderMaster POM ON POM.PurchaseOrderID = PIRD.ReferenceID", "PIM.PurchaseInvoiceID = " + PobjClsDTOPurchase.intPurchaseID })).Rows[0]["OrderTypeID"].ToInt32();

            if ((PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromOrder && intOrderTypeID == (int)OperationOrderType.POrderFromGRN) || PobjClsDTOPurchase.intOrderType == (int)OperationOrderType.PInvoiceFromGRN)
            {
                DeleteGRNSummary();

                intOrderTypeID = PobjClsDTOPurchase.intOrderType;

                DataTable dtTemp = MobjClsCommonUtility.FillCombos(new string[] { "PIM.PurchaseInvoiceID", "InvPurchaseInvoiceMaster PIM", "PIM.PurchaseInvoiceID IN (SELECT DISTINCT PurchaseInvoiceID FROM InvPurchaseInvoiceReferenceDetails PIRD WHERE ReferenceID IN (SELECT DISTINCT ReferenceID FROM InvPurchaseInvoiceReferenceDetails PIRD INNER JOIN  InvPurchaseInvoiceMaster PIM ON PIM.PurchaseInvoiceID = PIRD.PurchaseInvoiceID WHERE PIM.OrderTypeID = " + intOrderTypeID + " AND PIM.PurchaseInvoiceID = " + PobjClsDTOPurchase.intPurchaseID + ")) AND PIM.StatusID <> 19 AND PIM.PurchaseInvoiceID <>" + PobjClsDTOPurchase.intPurchaseID });

                if (PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PInvoiceCancelled && dtTemp.Rows.Count > 0 || PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PInvoiceCancelled)
                {
                    SaveGRNSummary(0);
                }
            }

        }

        private bool DeleteGRNSummary()
        {
            bool blnStatus = false;

            try
            {
                ArrayList prmItemSummary = new ArrayList();
                prmItemSummary.Add(new SqlParameter("@Mode", 85));
                prmItemSummary.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseInvoice", prmItemSummary);
                blnStatus = true;
            }
            catch { blnStatus = false; }

            return blnStatus;
        }

        private bool SaveGRNSummary(int intDel)
        {
            bool blnStatus = false;
            try
            {
                ArrayList prmItemSummary = new ArrayList();
                prmItemSummary.Add(new SqlParameter("@Mode", 86));
                prmItemSummary.Add(new SqlParameter("@PurchaseInvoiceID", PobjClsDTOPurchase.intPurchaseID));
                prmItemSummary.Add(new SqlParameter("@Del", intDel));

                MobjDataLayer.ExecuteNonQueryWithTran("spPurchaseInvoice", prmItemSummary);
                blnStatus = true;
            }
            catch { blnStatus = false; }
            return blnStatus;
        }
        public DataTable GetSavedRate(long GRNID, int ItemID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 87));
            parameters.Add(new SqlParameter("@GRNID", GRNID));
            parameters.Add(new SqlParameter("@ItemID", ItemID));
            return MobjDataLayer.ExecuteDataTable("spPurchaseInvoice", parameters);
        }
        public decimal GetItemSaleRate(Int64 intItemID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 89));
            parameters.Add(new SqlParameter("@ItemID", intItemID));
            object objSaleRate = this.MobjDataLayer.ExecuteScalar("spPurchaseInvoice", parameters);
            if (objSaleRate != DBNull.Value)
            {
                return Convert.ToDecimal(objSaleRate);
            }
            else
            {
                return 0;
            }
        }
        public decimal GetExtracharge(int intCompanyID, int intOperationTypeID, Int64 intReferenceID, int intCompanyCurrencyID)
        {
            decimal decConversionFactor, decTempAmt, decExtraCharge = 0.ToDecimal();
            ArrayList prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 88));
            prmPurchase.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPurchase.Add(new SqlParameter("@OperationTypeID", intOperationTypeID));
            prmPurchase.Add(new SqlParameter("@ReferenceID", intReferenceID));
            decExtraCharge = this.MobjDataLayer.ExecuteScalar("spPurchaseInvoice", prmPurchase).ToDecimal();            
            if (decExtraCharge > 0)
                return decExtraCharge;
            else
                return Convert.ToDecimal(0);

        }
        public decimal GetTotalAmountPaid(int intCompanyID, int intOperationTypeID, Int64 intReferenceID)
        {
            ArrayList prmPurchase = new ArrayList();
            prmPurchase.Add(new SqlParameter("@Mode", 90));
            prmPurchase.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPurchase.Add(new SqlParameter("@OperationTypeID", intOperationTypeID));
            prmPurchase.Add(new SqlParameter("@ReferenceID", intReferenceID));
            object objExtracharge = this.MobjDataLayer.ExecuteScalar("spPurchaseInvoice", prmPurchase);
            if (objExtracharge != DBNull.Value)
                return Convert.ToDecimal(objExtracharge);
            else
                return Convert.ToDecimal(0);
        }


        public decimal getTaxValue(int TaxScheme)
        {
        return this.MobjDataLayer.ExecuteScalar("Select AccValue From AccAccountMaster where AccountID='"+TaxScheme+"'").ToDecimal();
        }

     
       
    }
}
