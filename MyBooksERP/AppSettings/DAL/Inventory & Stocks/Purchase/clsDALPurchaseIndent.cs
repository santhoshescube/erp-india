﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using System.Collections;
using System.Data;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,22 Feb 2011>
   Description:	<Description,,Purchase Indent DAL Class>
================================================
*/ 
namespace MyBooksERP
{
    public class clsDALPurchaseIndent : IDisposable
    {
        ClsCommonUtility MobjClsCommonUtility;//object for common utility
        DataLayer MobjDataLayer; // object for data layer
        ArrayList prmPurchaseIndent,prmPurchaseIndentDetails;

        public clsDTOPurchaseIndent PobjClsDTOPurchaseIndent { get; set; } // Property for clsDTOPurchaseIndent

        public clsDALPurchaseIndent(DataLayer objDataLayer)
        {
            //Constructore
            MobjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MobjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public Int64 GetLastIndentNo(int iType,int intCompanyID)
        {
            // 1- Get Next Indent No else - Total Count
                    Int64 intRetValue = 0;
                    prmPurchaseIndent = new ArrayList();
                    prmPurchaseIndent.Add(new SqlParameter("@CompanyID", intCompanyID));
                    if (iType == 1)
                    {
                        prmPurchaseIndent.Add(new SqlParameter("@Mode", 17));
                    }
                    else
                    {
                        prmPurchaseIndent.Add(new SqlParameter("@Mode", 18));
                    }
                    SqlDataReader sdr = MobjDataLayer.ExecuteReader("STPurchaseIndentTransaction", prmPurchaseIndent);
                    if (sdr.Read())
                    {
                        if (sdr["ReturnValue"] != DBNull.Value)
                            intRetValue = Convert.ToInt64(sdr["ReturnValue"]);
                        else if (iType == 1)
                            intRetValue = 0;
                    }
                    sdr.Close();
            return intRetValue;
        }

        public bool SavePurchaseIndent(bool blnAddStatus)
        {
            // function for saving Purchase indent
            object  objOutPurchaseIndentID = 0;
            prmPurchaseIndent = new ArrayList();

            if (blnAddStatus)
                prmPurchaseIndent.Add(new SqlParameter("@Mode", 2));
            else
            {
                DeletePurchaseIndentDetails();
                prmPurchaseIndent.Add(new SqlParameter("@Mode", 3));
            }

            prmPurchaseIndent.Add(new SqlParameter("@PurchaseIndentID", PobjClsDTOPurchaseIndent.intPurchaseIndentID));
            prmPurchaseIndent.Add(new SqlParameter("@PurchaseIndentNo", PobjClsDTOPurchaseIndent.strPurchaseIndentNo));
            prmPurchaseIndent.Add(new SqlParameter("@IndentDate", PobjClsDTOPurchaseIndent.strIndentDate));
            prmPurchaseIndent.Add(new SqlParameter("@DepartmentID", PobjClsDTOPurchaseIndent.intDepartmentID));
            prmPurchaseIndent.Add(new SqlParameter("@DueDate", PobjClsDTOPurchaseIndent.strDueDate));
            prmPurchaseIndent.Add(new SqlParameter("@Remarks", PobjClsDTOPurchaseIndent.strRemarks));
            prmPurchaseIndent.Add(new SqlParameter("@CreatedBy", PobjClsDTOPurchaseIndent.intCreatedBy));
            prmPurchaseIndent.Add(new SqlParameter("@CreatedDate", PobjClsDTOPurchaseIndent.strCreatedDate));
            prmPurchaseIndent.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchaseIndent.intCompanyID));
            prmPurchaseIndent.Add(new SqlParameter("@StatusID", PobjClsDTOPurchaseIndent.intStatusID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPurchaseIndent.Add(objParam);
            
            if (MobjDataLayer.ExecuteNonQueryWithTran("STPurchaseIndentTransaction",prmPurchaseIndent, out objOutPurchaseIndentID) != 0)
            {
                if (Convert.ToInt64(objOutPurchaseIndentID) > 0)
                {
                    PobjClsDTOPurchaseIndent.intPurchaseIndentID = Convert.ToInt64(objOutPurchaseIndentID);
                    DeleteCancellationDetails();
                    if (PobjClsDTOPurchaseIndent.blnCancelled)
                    {
                        SaveCancellationDetails();
                    }
                    if (PobjClsDTOPurchaseIndent.lstDTOPurchaseIndentDetailsCollection.Count > 0)
                    {
                        SavePurchaseIndentDetails();
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public void SaveCancellationDetails()
        {
            // function for saving cancellation details

            prmPurchaseIndent = new ArrayList();
            prmPurchaseIndent.Add(new SqlParameter("@Mode", 12));
            prmPurchaseIndent.Add(new SqlParameter("@PurchaseIndentID", PobjClsDTOPurchaseIndent.intPurchaseIndentID));
            prmPurchaseIndent.Add(new SqlParameter("@CancelledBy", PobjClsDTOPurchaseIndent.intCancelledBy));
            prmPurchaseIndent.Add(new SqlParameter("@CancellationDate", PobjClsDTOPurchaseIndent.strCancellationDate));
            prmPurchaseIndent.Add(new SqlParameter("@Description", PobjClsDTOPurchaseIndent.strDescription));
            prmPurchaseIndent.Add(new SqlParameter("@CompanyID", PobjClsDTOPurchaseIndent.intCompanyID));
            MobjDataLayer.ExecuteNonQueryWithTran("STPurchaseIndentTransaction", prmPurchaseIndent);
        }

        private void DeleteCancellationDetails()
        {
            // function for deleting cancellation details
            prmPurchaseIndent = new ArrayList();
            prmPurchaseIndent.Add(new SqlParameter("Mode",13));
            prmPurchaseIndent.Add(new SqlParameter("PurchaseIndentID",PobjClsDTOPurchaseIndent.intPurchaseIndentID));
            MobjDataLayer.ExecuteNonQueryWithTran("STPurchaseIndentTransaction",prmPurchaseIndent);
        }

        private DataTable DisplayCancellationDetails()
        {
            //function for displaying cancellation details

            prmPurchaseIndent = new ArrayList();
            prmPurchaseIndent.Add(new SqlParameter("Mode", 14));
            prmPurchaseIndent.Add(new SqlParameter("PurchaseIndentID", PobjClsDTOPurchaseIndent.intPurchaseIndentID));
            return MobjDataLayer.ExecuteDataTable("STPurchaseIndentTransaction", prmPurchaseIndent);
        }

        public bool DeletePurchaseIndent()
        {
            // function for deleting purchase indent

            object objOutPurchaseIndentID = 0;
            prmPurchaseIndent = new ArrayList();
            prmPurchaseIndent.Add(new SqlParameter("@Mode", 4));
            prmPurchaseIndent.Add(new SqlParameter("@PurchaseIndentID", PobjClsDTOPurchaseIndent.intPurchaseIndentID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            prmPurchaseIndent.Add(objParam);

            if (MobjDataLayer.ExecuteNonQueryWithTran("STPurchaseIndentTransaction", prmPurchaseIndent) != 0)
            {
                    return true;
            }
            else
            {
                return false;
            }
        }

        public bool SavePurchaseIndentDetails()
        {
            //function for saving purchase indent details
            int count = 0;

            foreach (clsDTOPurchaseIndentDetails objClsDTOPurchaseIndentDetails in PobjClsDTOPurchaseIndent.lstDTOPurchaseIndentDetailsCollection)
            {
                prmPurchaseIndentDetails = new ArrayList();
            
                if (objClsDTOPurchaseIndentDetails.intSerialNo == 0)
                    prmPurchaseIndentDetails.Add(new SqlParameter("@Mode", 8));
                else
                    prmPurchaseIndentDetails.Add(new SqlParameter("@Mode", 9));

                prmPurchaseIndentDetails.Add(new SqlParameter("@SerialNo", ++count));
                prmPurchaseIndentDetails.Add(new SqlParameter("@PurchaseIndentID", PobjClsDTOPurchaseIndent.intPurchaseIndentID));
                prmPurchaseIndentDetails.Add(new SqlParameter("@ItemID", objClsDTOPurchaseIndentDetails.intItemID));
                prmPurchaseIndentDetails.Add(new SqlParameter("@Quantity", objClsDTOPurchaseIndentDetails.decQuantity));
                prmPurchaseIndentDetails.Add(new SqlParameter("@UOMID", objClsDTOPurchaseIndentDetails.intUOMID));
                MobjDataLayer.ExecuteNonQueryWithTran("STPurchaseIndentTransaction",prmPurchaseIndentDetails);
            }
            return true;
        }

        public bool DeletePurchaseIndentDetails()
        {
            //function for deleting purchase indent details

                prmPurchaseIndentDetails = new ArrayList();
                prmPurchaseIndentDetails.Add(new SqlParameter("@Mode", 10));
                prmPurchaseIndentDetails.Add(new SqlParameter("@PurchaseIndentID", PobjClsDTOPurchaseIndent.intPurchaseIndentID));
                MobjDataLayer.ExecuteNonQueryWithTran("STPurchaseIndentTransaction", prmPurchaseIndentDetails);
            return true;
        }

        public DataTable GetIndentNumbers()// Get Indent Number For searching
        {
            //function for Finding Purchase Indent By indent number
                prmPurchaseIndent = new ArrayList();
                prmPurchaseIndent.Add(new SqlParameter("@Mode", 5));
                return MobjDataLayer.ExecuteDataTable("STPurchaseIndentTransaction", prmPurchaseIndent);
        }

        public DataTable DisplayPurchaseIndentDetail(Int64 Rownum)// Display Indent detail
        {
            //function for displaying purchase indent details
            prmPurchaseIndent = new ArrayList();
            prmPurchaseIndent.Add(new SqlParameter("@Mode", 7));
            prmPurchaseIndent.Add(new SqlParameter("@PurchaseIndentID", Rownum));
            return MobjDataLayer.ExecuteDataSet("STPurchaseIndentTransaction", prmPurchaseIndent).Tables[0];
        }

        public bool DisplayPurchaseIndentInfo(Int64 iOrderID) // Purchse Indent
        {
            //function for displaying purchase indent info
            prmPurchaseIndent = new ArrayList();
            prmPurchaseIndent.Add(new SqlParameter("@Mode", 1));
            prmPurchaseIndent.Add(new SqlParameter("@PurchaseIndentID", iOrderID));
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("STPurchaseIndentTransaction", prmPurchaseIndent, CommandBehavior.SingleRow);

            if (sdr.Read())
            {
                PobjClsDTOPurchaseIndent.intPurchaseIndentID = iOrderID;
                PobjClsDTOPurchaseIndent.strPurchaseIndentNo = Convert.ToString(sdr["PurchaseIndentNo"]);
                
                if (sdr["CompanyID"] != DBNull.Value) PobjClsDTOPurchaseIndent.intCompanyID = Convert.ToInt32(sdr["CompanyID"]); else PobjClsDTOPurchaseIndent.intCompanyID = 0;
                
                if (sdr["DepartmentID"] != DBNull.Value) PobjClsDTOPurchaseIndent.intDepartmentID = Convert.ToInt32(sdr["DepartmentID"]); else PobjClsDTOPurchaseIndent.intDepartmentID = 0;
                
                PobjClsDTOPurchaseIndent.strIndentDate = Convert.ToString(sdr["IndentDate"]);
                PobjClsDTOPurchaseIndent.strDueDate = Convert.ToString(sdr["DueDate"]);
                PobjClsDTOPurchaseIndent.strRemarks = Convert.ToString(sdr["Remarks"]);
                PobjClsDTOPurchaseIndent.intStatusID = Convert.ToInt16(sdr["StatusID"]);
                PobjClsDTOPurchaseIndent.strEmployeeName = Convert.ToString(sdr["EmployeeName"]);
                PobjClsDTOPurchaseIndent.strCreatedDate = Convert.ToString(sdr["CreatedDate"]);

                DataTable datCancellationDetails = DisplayCancellationDetails();
                
                if (datCancellationDetails != null && datCancellationDetails.Rows.Count > 0)
                {
                    PobjClsDTOPurchaseIndent.strDescription = Convert.ToString(datCancellationDetails.Rows[0]["Description"]);
                    PobjClsDTOPurchaseIndent.strCancellationDate = Convert.ToString(datCancellationDetails.Rows[0]["Date"]);
                    PobjClsDTOPurchaseIndent.strCancelledBy = Convert.ToString(datCancellationDetails.Rows[0]["CancelledByName"]);
                    PobjClsDTOPurchaseIndent.intCancelledBy = Convert.ToInt32(datCancellationDetails.Rows[0]["CancelledBy"]);
                }

                sdr.Close();
                return true;
            }
            else
            {
                sdr.Close();
                return false;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MobjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }

        public DataTable GetCompanySettings(int ComID) // for settings according to company
        {
            // function for getting company settings
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode",11));
            prmCommon.Add(new SqlParameter("@CompanyID", ComID));
            return MobjDataLayer.ExecuteDataSet("STPurchaseIndentTransaction", prmCommon).Tables[0];
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            prmPurchaseIndent = new ArrayList();
            prmPurchaseIndent.Add(new SqlParameter("@Mode", 10));
            prmPurchaseIndent.Add(new SqlParameter("@ItemID", intItemID));
            prmPurchaseIndent.Add(new SqlParameter("@UomTypeID", 2));
            DataTable dtItemUoms = new DataTable();
            dtItemUoms = MobjDataLayer.ExecuteDataTable("STPurchaseModule", prmPurchaseIndent);

            prmPurchaseIndent = new ArrayList();
            prmPurchaseIndent.Add(new SqlParameter("@Mode", 12));
            prmPurchaseIndent.Add(new SqlParameter("@ItemID", intItemID));
            DataTable dtItemBaseUnit = MobjDataLayer.ExecuteDataTable("STPurchaseModule", prmPurchaseIndent);
            DataRow dr = dtItemUoms.NewRow();
            dr["UOMID"] = dtItemBaseUnit.Rows[0]["UOMID"];
            dr["ShortName"] = dtItemBaseUnit.Rows[0]["ShortName"];
            dr["ItemID"] = intItemID;
            dtItemUoms.Rows.InsertAt(dr, 0);
            return dtItemUoms;
        }

        public bool IsPurchaseIndentNoExists(string strPurchaseIndentNo,int intCompanyID)
          {
            bool blnIsExists = false;
            DataTable dtPurchases = new DataTable();
                    prmPurchaseIndent = new ArrayList();
                    prmPurchaseIndent.Add(new SqlParameter("@Mode", 15));
                    prmPurchaseIndent.Add(new SqlParameter("@PurchaseIndentNo", strPurchaseIndentNo));
                    prmPurchaseIndent.Add(new SqlParameter("@CompanyID", intCompanyID));

                    dtPurchases = MobjDataLayer.ExecuteDataTable("STPurchaseIndentTransaction", prmPurchaseIndent);
                    if (dtPurchases.Rows.Count > 0 && Convert.ToInt64(dtPurchases.Rows[0]["PurchaseIndentID"]) != PobjClsDTOPurchaseIndent.intPurchaseIndentID)
                        blnIsExists = true;
                    return blnIsExists;
          }

        public string FindEmployeeNameByUserID(int intUserID)
          {
              string strEmployeeName = string.Empty;
              prmPurchaseIndent = new ArrayList();
              prmPurchaseIndent.Add(new SqlParameter("@Mode", 10));
              prmPurchaseIndent.Add(new SqlParameter("@UserID", intUserID));
              SqlDataReader sdr;
              sdr = MobjDataLayer.ExecuteReader("STUserInformation", prmPurchaseIndent);
              if (sdr.Read())
              {
                  if (sdr["FirstName"] != DBNull.Value)
                  {
                      strEmployeeName = Convert.ToString(sdr["FirstName"]);
                  }
              }
              sdr.Close();
              return strEmployeeName;
          }

        public DataSet GetPurchaseIndentReport()
          {
              prmPurchaseIndent = new ArrayList();
              prmPurchaseIndent.Add(new SqlParameter("@Mode", 19));
              prmPurchaseIndent.Add(new SqlParameter("@PurchaseIndentID", PobjClsDTOPurchaseIndent.intPurchaseIndentID));
              return MobjDataLayer.ExecuteDataSet("STPurchaseIndentTransaction", prmPurchaseIndent);
          }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
          {
              prmPurchaseIndent = new ArrayList();
              prmPurchaseIndent.Add(new SqlParameter("@Mode", 4));
              prmPurchaseIndent.Add(new SqlParameter("@RoleID", intRoleID));
              prmPurchaseIndent.Add(new SqlParameter("@CompanyID", intCompanyID));
              prmPurchaseIndent.Add(new SqlParameter("@ControlID", intControlID));
              return MobjDataLayer.ExecuteDataTable("STPermissionSettings", prmPurchaseIndent);
          }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
          {
              ArrayList prmCommon = new ArrayList();
              prmCommon.Add(new SqlParameter("@Mode", 12));
              prmCommon.Add(new SqlParameter("@RoleID", intRoleID));
              prmCommon.Add(new SqlParameter("@CompanyID", intCompanyID));
              prmCommon.Add(new SqlParameter("@ControlID", intControlID));
              return MobjDataLayer.ExecuteDataTable("STPermissionSettings", prmCommon);
          }

        #region IDisposable Members

        public void Dispose()
        {
            if (MobjClsCommonUtility != null)
                MobjClsCommonUtility = null;
            if (MobjDataLayer != null)
                MobjDataLayer = null;
            if (prmPurchaseIndent != null)
                prmPurchaseIndent = null;
            if (prmPurchaseIndentDetails != null)
                prmPurchaseIndentDetails = null;
        }

        #endregion
    }
}
