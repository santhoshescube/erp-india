﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/* 
=================================================
Author:		    <Author,,Sawmya>
Create date:    <Create Date,,12 May 2011>
Description:	<Description,,DAL for Purchase>
Modified By:    <Author,,Amal>
Modified date:  <Modified Date,,18 Oct 2011>
Description:    <Description,,FINE TUNING OF REPORTS>
================================================
*/

namespace MyBooksERP
{
   public class clsDALMISPurchase
    {
        ArrayList prmReportDetails;

        public DataLayer objclsConnection { get; set; }

        //To Fill Combobox
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }
            }
            return null;
        }
        public DataTable DisplayPurchaseindentReport(int intCompany, int intDepartment, string strFrom, string strTo, int intstatus, int intNo, int intExecutive,int chkFrom,int chkTo,int intDate, string strPermittedCompany)
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strDate = "";
            if (intCompany != 0) strSearchCondition = " PIM.CompanyID=" + intCompany + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UserMaster.EmployeeID=" + intExecutive + " AND";
            if (intDepartment != 0) strSearchCondition = strSearchCondition + " PIM.DepartmentID =" + intDepartment + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " PIM.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " PIM.PurchaseIndentNo =" + intNo + " AND";

            if (intDate == 1)
                strDate = " PIM.IndentDate ";
            else if (intDate == 2)
                strDate = " PIM.CreatedDate ";
            else if (intDate == 3)
                strDate = " PIM.DueDate ";
            else
                strDate = " PIM.IndentDate ";

            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 4);

            prmReportDetails.Add(new SqlParameter("@Mode", 1));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptPurchase", prmReportDetails);
        }
        public DataTable DisplayPurchaseQutationReport(int intCompany, int intVendor, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " PQM.CompanyID=" + intCompany + " AND";
            if (intVendor != 0) strSearchCondition = strSearchCondition + " PQM.VendorID =" + intVendor + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UserMaster.EmployeeID=" + intExecutive + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " PQM.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " PQM.PurchaseQuotationNo =" + intNo + " AND";

            if (intDate == 1)
                strDate = " PQM.QuotationDate ";
            else if (intDate == 2)
                strDate = " PQM.CreatedDate ";
            else if (intDate == 3)
                strDate = " PQM.DueDate ";
            else
                strDate = " PQM.QuotationDate ";

            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 4);
            prmReportDetails.Add(new SqlParameter("@Mode", 2));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptPurchase", prmReportDetails);
        }
        public DataTable DisplayRFQ(int intCompany, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany)
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strDate = "";
            if (intCompany != 0) strSearchCondition = " RFQ.CompanyID=" + intCompany + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition +" UM.EmployeeID=" + intExecutive + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " RFQ.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " RFQ.RFQID =" + intNo + " AND";
            
            if (intDate == 1)
            {
                strDate = " RFQ.QuotationDate ";
            }
            else if (intDate == 2)
            {
                strDate = " RFQ.CreatedDate ";
            }
            else if (intDate == 3)
            {
                strDate = " RFQ.DueDate ";
            }
            else
            {
                strDate = " RFQ.QuotationDate ";
            }
            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 4);
            prmReportDetails.Add(new SqlParameter("@Mode",12));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptPurchase", prmReportDetails);
        }
        public DataTable DisplayPurchaseOrderReport(int intCompany, int intVendor, string strFrom, string strTo, int intOrderType, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strDate = "";
            
            if (intCompany != 0) strSearchCondition = " POM.CompanyID=" + intCompany + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UserMaster.EmployeeID=" + intExecutive + " AND";
            if (intVendor != 0) strSearchCondition = strSearchCondition + " POM.VendorID =" + intVendor + " AND";
            if (intOrderType != 0) strSearchCondition = strSearchCondition + " POM.OrderTypeID =" + intOrderType + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " POM.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " POM.PurchaseOrderNo =" + intNo + " AND";

            if (intDate == 1)
                strDate = " POM.OrderDate ";
            else if (intDate == 2)
                strDate = " POM.CreatedDate ";
            else if (intDate == 3)
                strDate = " POM.DueDate ";
            else
                strDate = " POM.OrderDate " ;

            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }

            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);
            prmReportDetails.Add(new SqlParameter("@Mode", 3));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptPurchase", prmReportDetails);
        }
        public DataTable DisplayGRNReport(int intCompany, int intVendor, string strFrom, string strTo, int intOrderType, int intWarehouse, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " GM.CompanyID=" + intCompany + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UserMaster.EmployeeID=" + intExecutive + " AND";
            if (intVendor != 0) strSearchCondition = strSearchCondition + " GM.VendorID =" + intVendor + " AND";
            if (intOrderType != 0) strSearchCondition = strSearchCondition + " GM.OrderTypeID =" + intOrderType + " AND";
            if (intWarehouse != 0) strSearchCondition = strSearchCondition + " GM.WarehouseID =" + intWarehouse + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " GM.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " GM.GRNNo =" + intNo + " AND";

            if (intDate == 1)
                strDate = " GM.GRNDate ";
            else if (intDate == 2)
                strDate = " GM.CreatedDate ";
            else if (intDate == 3)
                strDate = " GM.DueDate ";
            else
                strDate = " GM.GRNDate ";

            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 4);
            prmReportDetails.Add(new SqlParameter("@Mode", 4));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptPurchase", prmReportDetails);
        }
        public DataTable DisplayPurchaseInvoiceReport(int intCompany, int intVendor, string strFrom, string strTo, int intOrderType, int intWarehouse, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " PIM.CompanyID=" + intCompany + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UserMaster.EmployeeID=" + intExecutive + " AND";
            if (intVendor != 0) strSearchCondition = strSearchCondition + " PIM.VendorID =" + intVendor + " AND";
            if (intOrderType != 0) strSearchCondition = strSearchCondition + " PIM.OrderTypeID =" + intOrderType + " AND";
            if (intWarehouse != 0) strSearchCondition = strSearchCondition + " PIM.WarehouseID =" + intWarehouse + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " PIM.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " PIM.PurchaseInvoiceNo =" + intNo + " AND";

            if (intDate == 1)
                strDate = " PIM.InvoiceDate ";
            else if (intDate == 2)
                strDate = " PIM.CreatedDate ";
            else if (intDate == 3)
                strDate = " PIM.DueDate ";
            else
                strDate = " PIM.InvoiceDate ";

            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);
            prmReportDetails.Add(new SqlParameter("@Mode",5));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptPurchase", prmReportDetails);
        }
        public DataTable DisplayDebitNoteReport(int intCompany, int intVendor, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " PRM.CompanyID=" + intCompany + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UM.EmployeeID=" + intExecutive + " AND";
            if (intVendor != 0) strSearchCondition = strSearchCondition + " PIM.VendorID =" + intVendor + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " PRM.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " PRM.PurchaseInvoiceNo =" + intNo + " AND";

            if (intDate == 1)
                strDate = " PRM.ReturnDate ";
            else if (intDate == 2)
                strDate = " PRM.CreatedDate ";
            else
                strDate = " PRM.ReturnDate ";

            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);
            prmReportDetails.Add(new SqlParameter("@Mode",13));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptPurchase", prmReportDetails);
        }
        public DataSet DisplayPurchaseindent(int intCompany, int intDepartment, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strSearchCondition1 = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " PIM.CompanyID=" + intCompany + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UserMaster.EmployeeID=" + intExecutive + " AND";
            if (intDepartment != 0) strSearchCondition = strSearchCondition + " PIM.DepartmentID=" + intDepartment + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " PIM.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " PIM.PurchaseIndentID =" + intNo + " AND";
            if (intNo != 0) strSearchCondition1 = strSearchCondition1 + " PID.PurchaseIndentID =" + intNo + " AND";

            if (intDate == 1)
                strDate = " PIM.IndentDate ";
            else if (intDate == 2)
                strDate = " PIM.CreatedDate ";
            else if (intDate == 3)
                strDate = " PIM.DueDate ";
            else
                strDate = " PIM.IndentDate ";

            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 4);
            if (strSearchCondition1.Length > 3) strSearchCondition1 = strSearchCondition1.Substring(0, strSearchCondition1.Length - 3);
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            prmReportDetails.Add(new SqlParameter("@SearchCondition1", strSearchCondition1));
            return objclsConnection.ExecuteDataSet("spInvRptPurchase", prmReportDetails);
        }
        public DataSet DisplayPurchaseQutation(int intCompany, int intVendor, string strFrom, string strTo, int intType, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strSearchCondition1 = "";
            string strSearchCondition2 = "";
            string strSearchCondition3 = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " PQM.CompanyID=" + intCompany + " AND";
            if (intExecutive != 0) strSearchCondition = " UserMaster.EmployeeID=" + intExecutive + " AND";
            if (intVendor != 0) strSearchCondition = strSearchCondition + " PQM.VendorID =" + intVendor + " AND";
            //if (intSaleType != 0) strSearchCondition = strSearchCondition + " SO.SalesTypeID =" + intSaleType + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " PQM.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " PQM.PurchaseQuotationID =" + intNo + " AND";
            if (intNo != 0) strSearchCondition1 = strSearchCondition1 + "PQD.PurchaseQuotationID  =" + intNo + " AND";

            if (intType != 0) strSearchCondition2 = strSearchCondition2 + " EM.OperationTypeID=2  " + " AND";
            if (intType != 0) strSearchCondition2 = strSearchCondition2 + " EM.IsDirectExpense=1 " + " AND";
            if (intNo != 0) strSearchCondition2 = strSearchCondition2 + " PQM.PurchaseQuotationID  =" + intNo + " AND";
            if (intNo != 0) strSearchCondition3 = " PQM.PurchaseQuotationID =" + intNo + " AND";
            if (intCompany != 0) strSearchCondition3 = strSearchCondition3 + " Terms.CompanyID =" + intCompany + " AND";


            if (intDate == 1)
                strDate = " PQM.QuotationDate ";
            else if (intDate == 2)
                strDate = " PQM.CreatedDate ";
            else if (intDate == 3)
                strDate = " PQM.DueDate ";
            else
                strDate = " PQM.QuotationDate ";

            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }

            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);
            if (strSearchCondition1.Length > 3) strSearchCondition1 = strSearchCondition1.Substring(0, strSearchCondition1.Length - 3);
            if (strSearchCondition2.Length > 3) strSearchCondition2 = strSearchCondition2.Substring(0, strSearchCondition2.Length - 3);
            if (strSearchCondition3.Length > 3) strSearchCondition3 = strSearchCondition3.Substring(0, strSearchCondition3.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@ReferenceID", intNo));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            prmReportDetails.Add(new SqlParameter("@SearchCondition1", strSearchCondition1));
            prmReportDetails.Add(new SqlParameter("@SearchCondition2", strSearchCondition2));
            prmReportDetails.Add(new SqlParameter("@SearchCondition3", strSearchCondition3));
            return objclsConnection.ExecuteDataSet("spInvRptPurchase", prmReportDetails);
        }
        public DataSet DisplayGRN(int intCompany, int intVendor, string strFrom, string strTo, int intOrderType, int intWarehouse, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strSearchCondition1 = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " GM.CompanyID=" + intCompany + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UserMaster.EmployeeID=" + intExecutive + " AND";
            if (intVendor != 0) strSearchCondition = strSearchCondition + " GM.VendorID =" + intVendor + " AND";
            if (intOrderType != 0) strSearchCondition = strSearchCondition + " GM.OrderTypeID =" + intOrderType + " AND";
            if (intWarehouse != 0) strSearchCondition = strSearchCondition + " GM.WarehouseID =" + intWarehouse + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " GM.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " GM.GRNID =" + intNo + " AND";
            if (intNo != 0) strSearchCondition1 = strSearchCondition1 + "GD.GRNID =" + intNo + " AND";
             
            if (intDate == 1)
                strDate = " GM.GRNDate ";
            else if (intDate == 2)
                strDate = " GM.CreatedDate ";
            else if (intDate == 3)
                strDate = " GM.DueDate ";
            else
                strDate = " GM.GRNDate ";
            
            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate +" between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }

            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 4);
            if (strSearchCondition1.Length > 3) strSearchCondition1 = strSearchCondition1.Substring(0, strSearchCondition1.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 8));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            prmReportDetails.Add(new SqlParameter("@SearchCondition1", strSearchCondition1));
            return objclsConnection.ExecuteDataSet("spInvRptPurchase", prmReportDetails);
        }
        public DataSet DisplayPurchaseOrder(int intCompany, int intVendor, string strFrom, string strTo, int intOrderType, int intType, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany)
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strSearchCondition1 = "";
            string strSearchCondition2 = "";
            string strSearchCondition3 = "";
            string strSearchCondition4 = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " POM.CompanyID=" + intCompany + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UserMaster.EmployeeID=" + intExecutive + " AND";
            if (intVendor != 0) strSearchCondition = strSearchCondition + " POM.VendorID =" + intVendor + " AND";
            if (intOrderType != 0) strSearchCondition = strSearchCondition + " POM.OrderTypeID =" + intOrderType + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " POM.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " POM.PurchaseOrderID =" + intNo + " AND";
            if (intNo != 0) strSearchCondition1 = strSearchCondition1 + "POD.PurchaseOrderID =" + intNo + " AND";
            if (intNo != 0) strSearchCondition4 = " POM.PurchaseOrderID =" + intNo + " AND";
            if (intCompany != 0) strSearchCondition4 = strSearchCondition4 + " Terms.CompanyID =" + intCompany + " AND";



            if (intType != 0) strSearchCondition2 = strSearchCondition2 + " EM.OperationTypeID=3  " + " AND";
            //if (intType != 0) strSearchCondition2 = strSearchCondition2 + " EM.PostToAccount=1 " + " AND";
            if (intNo != 0) strSearchCondition2 = strSearchCondition2 + " POM.PurchaseOrderID  =" + intNo + " AND";
            if (intNo != 0) strSearchCondition3 = "Where  POM.PurchaseOrderID = " + intNo + " AND";
            if (intDate == 1)
                strDate = " POM.CreatedDate ";
            else if (intDate == 2)
                strDate = " POM.CreatedDate ";
            else if (intDate == 3)
                strDate = " POM.DueDate";
            else
                strDate = " POM.CreatedDate ";

            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }
            
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 4);
            if (strSearchCondition1.Length > 3) strSearchCondition1 = strSearchCondition1.Substring(0, strSearchCondition1.Length - 3);
            if (strSearchCondition2.Length > 3) strSearchCondition2 = strSearchCondition2.Substring(0, strSearchCondition2.Length - 3);
            if (strSearchCondition3.Length > 3) strSearchCondition3 = strSearchCondition3.Substring(0, strSearchCondition3.Length - 3);
            if (strSearchCondition4.Length > 3) strSearchCondition4 = strSearchCondition4.Substring(0, strSearchCondition4.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 9));
            prmReportDetails.Add(new SqlParameter("@ReferenceID",intNo));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            prmReportDetails.Add(new SqlParameter("@SearchCondition1", strSearchCondition1));
            prmReportDetails.Add(new SqlParameter("@SearchCondition2", strSearchCondition2));
            prmReportDetails.Add(new SqlParameter("@SearchCondition3", strSearchCondition3));
            prmReportDetails.Add(new SqlParameter("@SearchCondition4", strSearchCondition4));
            return objclsConnection.ExecuteDataSet("spInvRptPurchase", prmReportDetails);
        }
        public DataSet DisplayPurchaseInvoice(int intCompany, int intVendor, string strFrom, string strTo, int intOrderType, int intType, int intWarehouse, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strSearchCondition1 = "";
            string strSearchCondition2 = "";
            string strSearchCondition3 = "";
            string strSearchCondition4 = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " PIM.CompanyID=" + intCompany + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UserMaster.EmployeeID=" + intExecutive + " AND";
            if (intVendor != 0) strSearchCondition = strSearchCondition + " PIM.VendorID =" + intVendor + " AND";
            if (intOrderType != 0) strSearchCondition = strSearchCondition + " PIM.OrderTypeID =" + intOrderType + " AND";
            if (intWarehouse != 0) strSearchCondition = strSearchCondition + " PIM.WarehouseID =" + intWarehouse + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " PIM.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " PIM.PurchaseInvoiceID =" + intNo + " AND";
            if (intNo != 0) strSearchCondition1 = strSearchCondition1 + " PID.PurchaseInvoiceID  =" + intNo + " AND";
            if (intType != 0) strSearchCondition2 = strSearchCondition2 + " EM.OperationTypeID=5  " + " AND";
            //if (intType != 0) strSearchCondition2 = strSearchCondition2 + " EM.PostToAccount=1 " + " AND";

           // if (intNo != 0) strSearchCondition3 = " Where PIM.PurchaseInvoiceID = " + intNo + " AND";
            if (intNo != 0) strSearchCondition3 = " where RPD.ReferenceID = (Select Distinct ReferenceID from InvPurchaseInvoiceReferenceDetails where PurchaseInvoiceID =" + intNo + ") AND";
              

            if (intNo != 0) strSearchCondition4 = " PIM.PurchaseInvoiceID = " + intNo + " AND";
            if (intCompany != 0) strSearchCondition4 = strSearchCondition4 + " Term.CompanyID = " + intCompany + " AND";


            if (intNo != 0) strSearchCondition2 = strSearchCondition2 + " PIM.PurchaseInvoiceID =" + intNo + " AND";
            
            if (intDate == 1)
                strDate = " PIM.InvoiceDate ";
            else if (intDate == 2)
                strDate = " PIM.CreatedDate ";
            else if (intDate == 3)
                strDate = " PIM.DueDate ";
            else
                strDate = " PIM.InvoiceDate ";

            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }

            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);
            if (strSearchCondition1.Length > 3) strSearchCondition1 = strSearchCondition1.Substring(0, strSearchCondition1.Length - 3);
            if (strSearchCondition2.Length > 3) strSearchCondition2 = strSearchCondition2.Substring(0, strSearchCondition2.Length - 3);
            if (strSearchCondition3.Length > 3) strSearchCondition3 = strSearchCondition3.Substring(0, strSearchCondition3.Length - 3);
            if (strSearchCondition4.Length > 3) strSearchCondition4 = strSearchCondition4.Substring(0, strSearchCondition4.Length - 3);
            
            prmReportDetails.Add(new SqlParameter("@Mode", 10));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            prmReportDetails.Add(new SqlParameter("@SearchCondition1", strSearchCondition1));
            prmReportDetails.Add(new SqlParameter("@SearchCondition2", strSearchCondition2));
            prmReportDetails.Add(new SqlParameter("@SearchCondition3", strSearchCondition3));
            prmReportDetails.Add(new SqlParameter("@SearchCondition4", strSearchCondition4));
            return objclsConnection.ExecuteDataSet("spInvRptPurchase", prmReportDetails);
        }

        public DataSet DisplayDebitNote(int intCompany, int intVendor, string strFrom, string strTo , int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strSearchCondition1 = "";
            string strSearchCondition2 = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " PRM.CompanyID=" + intCompany + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UM.EmployeeID=" + intExecutive + " AND";
            if (intVendor != 0) strSearchCondition = strSearchCondition + " PIM.VendorID =" + intVendor + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " PRM.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " PRM.PurchaseReturnID =" + intNo + " AND";

            if (intNo != 0) strSearchCondition1 =  " PR.PurchaseReturnID  =" + intNo + " AND";

            if (intNo != 0) strSearchCondition2 =  " PRD.PurchaseReturnID =" + intNo + " AND";
            
            if (intDate == 1)
                strDate = " PRM.ReturnDate ";
            else if (intDate == 2)
                strDate = " PRM.CreatedDate ";
            else
                strDate = " PRM.ReturnDate ";

            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }

            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);
            if (strSearchCondition1.Length > 3) strSearchCondition1 = strSearchCondition1.Substring(0, strSearchCondition1.Length - 3);
            if (strSearchCondition2.Length > 3) strSearchCondition2 = strSearchCondition2.Substring(0, strSearchCondition2.Length - 3);
            
            prmReportDetails.Add(new SqlParameter("@Mode", 14));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            prmReportDetails.Add(new SqlParameter("@SearchCondition1", strSearchCondition1));
            prmReportDetails.Add(new SqlParameter("@SearchCondition2", strSearchCondition2));
            return objclsConnection.ExecuteDataSet("spInvRptPurchase", prmReportDetails);
        }
        public DataSet DisplayRFQReport(int intCompany, string strFrom, string strTo, int intType, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) //  RFQ Report
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strSearchCondition1 = "";
            string strSearchCondition2 = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " RFQ.CompanyID=" + intCompany + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UM.EmployeeID=" + intExecutive + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " RFQ.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " RFQ.RFQID =" + intNo + " AND";
            if (intNo != 0) strSearchCondition1 = strSearchCondition1 + " RFQ.RFQID  =" + intNo + " AND";
            if (intNo != 0) strSearchCondition2 = " RFQ.RFQID  =" + intNo + " AND";
            if (intCompany != 0) strSearchCondition2 = strSearchCondition2 + " Terms.CompanyID =" + intCompany + " AND";

            if (intDate == 1)
                strDate = " RFQ.QuotationDate ";
            else if (intDate == 2)
                strDate = " RFQ.CreatedDate ";
            else if (intDate == 3)
                strDate = " RFQ.DueDate ";
            else
                strDate = " RFQ.QuotationDate ";

            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 4);
            if (strSearchCondition1.Length > 3) strSearchCondition1 = strSearchCondition1.Substring(0, strSearchCondition1.Length - 3);
            if (strSearchCondition2.Length > 3) strSearchCondition2 = strSearchCondition2.Substring(0, strSearchCondition2.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 11));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            prmReportDetails.Add(new SqlParameter("@SearchCondition1", strSearchCondition1));
            prmReportDetails.Add(new SqlParameter("@SearchCondition2", strSearchCondition2));
            return objclsConnection.ExecuteDataSet("spInvRptPurchase", prmReportDetails);
        }
        public DataTable DisplayCompanyHeaderReport(int CompanyId) // for company report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 31));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyId));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmReportDetails);
        }
        public DataTable DisplayALLCompanyHeaderReport() // for company report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 32));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmReportDetails);
        }
        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 4));
            prmReportDetails.Add(new SqlParameter("@RoleID", intRoleID));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmReportDetails.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable("STPermissionSettings", prmReportDetails);
        }
    }
}
