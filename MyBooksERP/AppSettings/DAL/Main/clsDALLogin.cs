﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALLogin
    {
        ArrayList prmLogin;                                          // array list for storing parameters
        public DataLayer MobjDataLayer;                                  // obj of datalayer    

        public clsDALLogin(DataLayer objDataLayer)
        {
            //constructor
            MobjDataLayer = objDataLayer;
        }

        public DataTable GetUserInformation(string strUserName, string strPassword)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 1));
            prmLogin.Add(new SqlParameter("@UserName", strUserName));
            prmLogin.Add(new SqlParameter("@Password", strPassword));
            return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        }

        public DataTable GetTopMostCompany()
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 2));
            return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        }
        public DataTable GetUserCompany()
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 8));
            prmLogin.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
            return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        }

        public DataTable GetDefaultConfigurationSettings()
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 3));
            return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        }
        public DataTable GetCompanySettingsData(int intCompanyID)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 4));
            prmLogin.Add(new SqlParameter("@CompanyID", intCompanyID));
            return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        }
        public DataTable GetCompanySettings(int intCompanyID)
        
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 4));
            prmLogin.Add(new SqlParameter("@CompanyID", intCompanyID));
            DataTable dtCompanySettingsInfo = MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);

            string strValue = "";

            strValue = GetCompanySettingsValue("Installments", "First payment of installment", dtCompanySettingsInfo);
            if (!string.IsNullOrEmpty(strValue))
                ClsCommonSettings.PintFirstInstallmentInterval = Convert.ToInt32(strValue);
            else
                ClsCommonSettings.PintFirstInstallmentInterval = 0;

            strValue = GetCompanySettingsValue("Installments", "Installment Interval", dtCompanySettingsInfo);
            if (!string.IsNullOrEmpty(strValue))
                ClsCommonSettings.PintInstallmentInterval = Convert.ToInt32(strValue);
            else
                ClsCommonSettings.PintInstallmentInterval = 0;

            strValue = GetCompanySettingsValue("Installments", "Installments starts After", dtCompanySettingsInfo);
            if (!string.IsNullOrEmpty(strValue) && strValue.ToUpper() == "INVOICE")
                ClsCommonSettings.PintInstallmentStartFrom = 1;
            else
                ClsCommonSettings.PintInstallmentStartFrom = 2;
            //---------------------------------------------------Create/Modify
            ClsCommonSettings.strEmployeeNumberPrefix = GetCompanySettingsValue("Employee Number", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnEmployeeNumberAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Employee Number", "Prefix Editable", dtCompanySettingsInfo));
            
            ClsCommonSettings.strCustomerCodePrefix = GetCompanySettingsValue("Customer Code", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnCustomerAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Customer Code", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.strSupplierCodePrefix = GetCompanySettingsValue("Supplier Code", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnSupplierAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Supplier Code", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.strAgentBankCodePrefix = GetCompanySettingsValue("AgentBank Code", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.strContracterCodePrefix = GetCompanySettingsValue("Contractor Code", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.strWareHouseLocationPrefix = GetCompanySettingsValue("WareHouse", "Location Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.strWareHouseRowPrefix = GetCompanySettingsValue("WareHouse", "Row Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.strWareHouseBlockPrefix = GetCompanySettingsValue("WareHouse", "Block Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.strWareHouseLotPrefix = GetCompanySettingsValue("WareHouse", "Lot Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.strProjectCodePrefix = GetCompanySettingsValue("Projects", "Prefix", dtCompanySettingsInfo);
            //---------------------------------------------------Purchase
            ClsCommonSettings.PIPrefix = GetCompanySettingsValue("Purchase Indent", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.PIAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Indent", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.PIDueDateLimit = GetCompanySettingsValue("Purchase Indent", "Due Date Limit", dtCompanySettingsInfo);
            ClsCommonSettings.strRFQNumber = GetCompanySettingsValue("RFQ", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnRFQAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("RFQ", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.strRFQDueDateLimit = GetCompanySettingsValue("RFQ", "Due Date Limit", dtCompanySettingsInfo);
            ClsCommonSettings.PQPrefix = GetCompanySettingsValue("Purchase Quotation", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.PQAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Quotation", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.PQDueDateLimit = GetCompanySettingsValue("Purchase Quotation", "Due Date Limit", dtCompanySettingsInfo);
            ClsCommonSettings.POPrefix = GetCompanySettingsValue("Purchase Order", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.POAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Order", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.PODueDateLimit = GetCompanySettingsValue("Purchase Order", "Due Date Limit", dtCompanySettingsInfo);
            ClsCommonSettings.PINVPrefix = GetCompanySettingsValue("Purchase Invoice", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.PINVAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Invoice", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.PGRNPrefix = GetCompanySettingsValue("Goods Receipt Note", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.PGRNAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Goods Receipt Note", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.PMaterialReturnPrefix = GetCompanySettingsValue("Material Return", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.PMaterialReturnAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Material Return", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.PRPrefix = GetCompanySettingsValue("Debit Note", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.PRAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Debit Note", "Prefix Editable", dtCompanySettingsInfo));
            //---------------------------------------------------Sales
            ClsCommonSettings.strSQPrefix = GetCompanySettingsValue("Sales Quotation", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnSQAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Sales Quotation", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.strSQDueDateLimit = GetCompanySettingsValue("Sales Quotation", "Due Date Limit", dtCompanySettingsInfo);
            ClsCommonSettings.strSOPrefix = GetCompanySettingsValue("Sales Order", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnSOAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Sales Order", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.strSODueDateLimit = GetCompanySettingsValue("Sales Order", "Due Date Limit", dtCompanySettingsInfo);
            ClsCommonSettings.strSIPrefix = GetCompanySettingsValue("Sales Invoice", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnSIAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Sales Invoice", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.strSIDueDateLimit = GetCompanySettingsValue("Sales Invoice", "Due Date Limit", dtCompanySettingsInfo);
            ClsCommonSettings.strPOSPrefix = GetCompanySettingsValue("Point Of Sale", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnPOSAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Point Of Sale", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.strSRPrefix = GetCompanySettingsValue("Credit Note", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnSRAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Credit Note", "Prefix Editable", dtCompanySettingsInfo));
            //---------------------------------------------------Delivery
            ClsCommonSettings.ItemIssuePrefix = GetCompanySettingsValue("Delivery Note", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.ItemIssueAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Delivery Note", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.MaterialIssuePrefix = GetCompanySettingsValue("Material Issue", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.MaterialIssueAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Material Issue", "Prefix Editable", dtCompanySettingsInfo));
            //---------------------------------------------------Task
            ClsCommonSettings.PaymentsPrefix = GetCompanySettingsValue("Payments", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.PaymentsAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Payments", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.ReceiptsPrefix = GetCompanySettingsValue("Receipts", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.ReceiptsAutoGenerate = ConvertStringToBoolean(GetCompanySettingsValue("Receipts", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.PblnAutoGeneratePartyAccount = ConvertStringToBoolean(GetCompanySettingsValue("Advanced", "Auto generate Party accounts", dtCompanySettingsInfo));
            ClsCommonSettings.RFQApproval = ConvertStringToBoolean(GetCompanySettingsValue("RFQ", "Approval", dtCompanySettingsInfo));
            ClsCommonSettings.PQApproval = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Quotation", "Approval", dtCompanySettingsInfo));
            ClsCommonSettings.POApproval = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Order", "Approval", dtCompanySettingsInfo));
            ClsCommonSettings.SQApproval = ConvertStringToBoolean(GetCompanySettingsValue("Sales Quotation", "Approval", dtCompanySettingsInfo));
            ClsCommonSettings.SOApproval = ConvertStringToBoolean(GetCompanySettingsValue("Sales Order", "Approval", dtCompanySettingsInfo));
            ClsCommonSettings.StockTransferApproval = ConvertStringToBoolean(GetCompanySettingsValue("Transfer Order", "Approval", dtCompanySettingsInfo));
            ClsCommonSettings.strStockTransferNo = GetCompanySettingsValue("Transfer Order", "Prefix", dtCompanySettingsInfo).ToString();
            //---------------------------------------------------Inventory
            ClsCommonSettings.strProductCodePrefix = GetCompanySettingsValue("Product Code", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnProductCodeAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Product Code", "Prefix Editable", dtCompanySettingsInfo));
            ClsCommonSettings.strProductGroupCodePrefix = GetCompanySettingsValue("Product Assembly Code", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnProductGroupCodeAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Product Assembly Code", "Prefix Editable", dtCompanySettingsInfo));            
            ClsCommonSettings.strStockAdjustmentNumberPrefix = GetCompanySettingsValue("Stock Adjustment Number", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnStockAdjustmentNumberAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Stock Adjustment Number", "Prefix Editable", dtCompanySettingsInfo));

            return dtCompanySettingsInfo;
        }
        private bool ConvertStringToBoolean(string strValue)
        {
            if (strValue == "Yes")
                return true;
            return false;
        }
        public string GetCompanySettingsValue(string strConfigurationItem, string strSubItem, DataTable dtCompanySettings)
        {
            dtCompanySettings.DefaultView.RowFilter = "ConfigurationItem = '" + strConfigurationItem + "' And SubItem = '" + strSubItem + "'";
            if (dtCompanySettings.DefaultView.ToTable().Rows.Count > 0)
                return dtCompanySettings.DefaultView.ToTable().Rows[0]["ConfigurationValue"].ToString();
            return "";
        }
        public void UpdateRemember(string strUserName,bool blnRemember)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 5));
            prmLogin.Add(new SqlParameter("@UserName", strUserName));
            prmLogin.Add(new SqlParameter("@IsRemember", blnRemember));
            MobjDataLayer.ExecuteNonQuery("spLoginOperations", prmLogin);
        }

        public string GetPassword(string strUserName)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 6));
            prmLogin.Add(new SqlParameter("@UserName", strUserName));
            return Convert.ToString(MobjDataLayer.ExecuteScalar("spLoginOperations", prmLogin));
        }

        public bool GetRememberStatus(string strUserName)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 7));
            prmLogin.Add(new SqlParameter("@UserName", strUserName));
            DataTable dtUser = MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
            if (dtUser.Rows.Count > 0)
                return Convert.ToBoolean(dtUser.Rows[0]["IsRemember"]);
            return false;
        }

        public void InsertOpStockAccount(int intCompanyID)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 9));
            prmLogin.Add(new SqlParameter("@CompanyID", intCompanyID));
            MobjDataLayer.ExecuteNonQuery("spLoginOperations", prmLogin);
        }

        public DataTable getCompanyList(int intUserID)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 10));
            prmLogin.Add(new SqlParameter("@UserID", intUserID));
            return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        }

        public DataTable GetBookKeepingYears(int intCompanyID)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 11));
            prmLogin.Add(new SqlParameter("@CompanyID", intCompanyID));
            return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        }

        public DataTable GetCompanyDetails(int intCompanyID)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 12));
            prmLogin.Add(new SqlParameter("@CompanyID", intCompanyID));
            return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        }
    }
}
