﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOProductionTemplate
    {
        public long lngTemplateID { get; set; }
        public string strCode { get; set; }
        public string strName { get; set; }
        public int intProductID { get; set; }
        public string strRemarks { get; set; }
        public decimal decTotal { get; set; }
    }
}
