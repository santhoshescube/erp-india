﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOProductionProcess
    {
        public long lngProductionID { get; set; }
        public string strProductionNo { get; set; }
        public DateTime dtProductionDate { get; set; }
        public long lngTemplateID { get; set; }
        public string strBatchNo { get; set; }
        public int intProductID { get; set; }        
        public decimal decQuantity { get; set; }
        public int intWarehouseID { get; set; }
        public int intInChargeID { get; set; }
        public string strRemarks { get; set; }
    }
}
