﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOAttendanceManual
    {

        public int intCompanyID{ get; set; }
        public int intWorkLocationID{ get; set; }
        public int intDepartmentID{ get; set; }
        public string strDate { get; set; }
        public string strXmlDoc{ get; set; }

    }
}
