﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace MyBooksERP
{
    /*****************************************************
  * Created By       : Arun
  * Creation Date    : 16 Apr 2012
  * Description      : Handle Attendance
  * ***************************************************/
   public class clsDTOAttendance
    {
      
       public int intAttendanceID{get;set;}
       public int intEmployeeID{get;set;}
       public string strDate{get;set;}
       public int intShiftID{get;set;}
       public string strWorkTime{get;set;}
       public string strBreakTime{get;set;}
       public string strLate { get; set; }
       public string strEarly { get; set; }
       public bool blnIsHalfDay{get;set;}
       public string strOT{get;set;}
       public bool blnIsLOP{get;set;}
       public int intCompanyId{get;set;}
       public int intConsequenceID { get; set; }
       public int intPolicyID{get;set;}
       public decimal decAmountDed{get;set;}
       //public int intProjectID { get; set; }
       //public int intWorkLocationID { get; set; }
       public int intAttendenceStatusID{get;set;}
       public int intAbsentTime{get;set;}
       public int intLeaveID{get;set;}
       public int intProjectID { get; set; }
       public int intShiftOrderID { get; set; }
       public string strRemarks { get; set; }
       public List<clsDTOAttendanceDetails> lstclsDTOAttendanceDetails { get; set; }

    }
    //Details
   public class clsDTOAttendanceDetails
   {
       public string strTime { get; set; }
       public int intOrderNo { get; set; }
   }
   public class clsDTOAttendanceDeviceLogs
   {

       public int intDeviceID { get; set; }
       public string strDate { get; set; }
       public int intEmployeeID { get; set; }
       public string strWorkID { get; set; }
       public string strAtnDate { get; set; }
       public string strAtnTime { get; set; }
       public string strAtnStatus { get; set; }
       public string strAtnResult { get; set; }
       public string strAtnFormat { get; set; }
       public int intDeviceTypeID { get; set; }

   }
}
