﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOAttendanceDeviceSettings
    {
      public int intDeviceID { get; set; }
      public string strDevicename{get;set;}
      public string strIPAddress{get;set;}
      public double dblPort{get;set;}
      public int intCommKey{get;set;}
      public string strModel{get;set;}
      public int intDelay{get;set;}
      public int intTimeOut{get;set;}
      public int intDeviceNo { get; set; }
      public int intDeviceTypeID { get; set; }
                  
    }
}
