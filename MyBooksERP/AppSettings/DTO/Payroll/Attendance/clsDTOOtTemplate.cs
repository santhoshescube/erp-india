﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{ /*********************************************
       * Author       : Arun
       * Created On   : 19 Apr 2012
       * Purpose      : Ot template   Properties 
       * ******************************************/

    public class clsDTOOtTemplate
    {
        public List<clsDTOOtTemplateDetails> DTOOtTemplateDetails { get; set; }

        public clsDTOOtTemplate()
        {
            this.DTOOtTemplateDetails = new List<clsDTOOtTemplateDetails>();

        }
    }
    public class clsDTOOtTemplateDetails
    {
        public int SerialNo { get; set; }
        public int StartMin { get; set; }
        public int EndMin { get; set; }
        public int OTMin { get; set; }
    }
}
