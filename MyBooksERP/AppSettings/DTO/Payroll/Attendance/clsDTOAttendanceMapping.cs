﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP//.DTO.FingerTech
{ /*****************************************************
 * Created By       : Arun
 * Creation Date    : 17 Apr 2012
 * Description      : Handle Employee WorkCode Mapping for Attendance
 * ***************************************************/

   public class clsDTOAttendanceMapping
    {
        public int intEmployeeID{get;set;}
        public int intEquipmentID{get;set;}
        public string strWorkID{get;set;}
        public bool blnActive{get;set;}
        public int intTemplateMasterID {get;set;}
        public int intsettingsID{get;set;}
       //public int intType{get;set;}
        public int intComapnyID{get;set;}

    }
}
