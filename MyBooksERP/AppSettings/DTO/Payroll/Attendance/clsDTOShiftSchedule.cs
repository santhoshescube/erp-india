﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/*
 * Created By       : Tijo
 * Created Date     : 22 Aug 2013
 * Purpose          : For Shift Schedule
*/
namespace MyBooksERP
{
    public class clsDTOShiftSchedule
    {
        public int intShiftScheduleID { get; set; }
        public string strScheduleName { get; set; }
        public int intShiftID { get; set; }
        public DateTime dtFromDate { get; set; }
        public DateTime dtToDate { get; set; }
        public int intCreatedBy { get; set; }        
        public string strSearchKey { get; set; }

        public List<clsDTOShiftScheduleDetails> IlstclsDTOShiftScheduleDetails { get; set; }
    }

    public class clsDTOShiftScheduleDetails
    {
        public long lngEmployeeID { get; set; }
        public int intCompanyID { get; set; }
    }
}
