﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    /* 
================================================
   Author:		<Author, Lince P Thomas>
   Create date: <Create Date,,8 Jan 2010>
   Description:	<Description,,Company Form>
 *
 *  Modified :	   <Saju>
    Modified date: <Modified Date,19 August 2013>
    Description:	<Description,Settlement Process Form>
================================================
*/
    public class clsDTOSettlementProcess
    {

        public int RowNum { get; set; }
        public int AddEditMode { get; set; }

        public int  SettlementID { get; set; }
        public int EmployeeID { get; set; }
        public string Date { get; set; }
        public bool DocumentReturn { get; set; }
        public bool AssetReturn { get; set; }
        public string Remarks { get; set; }
        public decimal NetAmount	{ get; set; }
        public int CurrencyID { get; set; }
        public int SettlementType	{ get; set; }
        public string FromDate	{ get; set; }
        public string ToDate { get; set; }
        public bool Closed	{ get; set; }
        public int CompanyID	{ get; set; }
        public decimal CompanyAmount	{ get; set; }
        public decimal  PaymentID	{ get; set; }
        public decimal ExperienceDays	{ get; set; }
        public decimal EligibleLeavePayDays	{ get; set; }
        public decimal TakenLeavePayDays	{ get; set; }
        public decimal LeavePayDays	{ get; set; }
        public decimal AbsentDays	{ get; set; }
        public bool ConsiderAbsentDays { get; set; }
        public decimal VacationDays { get; set; }
        public decimal PaymentIdTemp { get; set; }
        public decimal PaymentAmount { get; set; }
        public int AccountID { get; set; }
        public int TransactionTypeID { get; set; }
        public string ChequeNumber { get; set; }
        public string ChequeDate { get; set; }
        public int CreatedBy { get; set; }
        public int userid { get; set; }
        public string dateofjoining { get; set; }
        public string strSearchKey { get; set; }
        public bool blnIsExcludeHolidays { get; set; }
        public decimal decExcludeHolidays { get; set; }
        public bool blnIsExcludeLeaveDays { get; set; }
        public decimal decExcludeLeaveDays { get; set; }
        public decimal decEligibleDays { get; set; }

        public List<clsDTOSettlementDetails> lstclsDTOSettlementDetails { get; set; }
        public List<clsDTOGratuityDetails> lstclsDTOGratuityDetails { get; set; }

    }

    public class clsDTOSettlementDetails
    {
      public int   EmployeeSettlementDetailID { get; set; }
      public int SettlementID { get; set; }
      public string  Particulars { get; set; }
      public decimal  Amount { get; set; }
      public bool IsAddition { get; set; }
      public int RepaymentID { get; set; }
      public int LoanID { get; set; }

      public int flag { get; set; }
      public decimal SalaryPaymentID { get; set; }
      public decimal AmountDebit { get; set; }
      public decimal AmountCredit { get; set; }

    }

    public class clsDTOGratuityDetails
    {
        public string ColYears  { get; set; }
        public decimal ColCalDays  { get; set; }
        public decimal ColGRTYDays  { get; set; }
        public decimal ColGratuityAmt  { get; set; }

    }


}
