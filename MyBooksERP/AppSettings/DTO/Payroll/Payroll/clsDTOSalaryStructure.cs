﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOSalaryStructure
    {

        public int intSalaryStructureID { get; set; }
        public int intEmployeeID { get; set; }
        public int intPaymentClassificationID { get; set; }
        public int intPayCalculationTypeID { get; set; }
        public string strRemarks { get; set; }
        public int intIncentiveTypeID { get; set; }
        public int intSalaryDay { get; set; }
        public DateTime dateLastPayment { get; set; }
        public int intCurrencyID { get; set; }
        public bool boolIsCurrencyForBP { get; set; }
        public int intOTPolicyID { get; set; }
        public int intAbsentPolicyID { get; set; }
        public int intEncashPolicyID { get; set; }
        public int intHolidayPolicyID { get; set; }
        //public int intSettlementPolicyID { get; set; }  
        public DateTime dateHistoryDate { get; set; }
        public decimal decNetAmount { get; set; }
        public int intSetPolicyID { get; set; }
        public int intSalaryStructureHistoryID { get; set; }
        public int intNetAmount { get; set; }
        public int intCompanyID { get; set; }
        public int intSearchType { get; set; }
        public string strSearchText { get; set; }
      


        public List<clsDTOSalaryStructureDetail> DTOSalaryStructureDetail { get; set; }
        public List<clsDTORemunerationDetail> DTORemunerationDetail { get; set; }


    }
    public class clsDTOSalaryStructureDetail
    {

        public int intSerialNo { get; set; }
        //public int intSalaryStructureID { get; set; }
        public int intAdditionDeductionID { get; set; }
        public decimal decAmount { get; set; }
        public int? intAdditionDeductionPolicyID { get; set; }
        public int intCategoryID { get; set; }

    }

    public class clsDTORemunerationDetail
    {

        public int intSerialNo { get; set; }

        public int OtherRemunerationDetailID { get; set; }
        public decimal AmountRemuneration { get; set; }


    }

}
