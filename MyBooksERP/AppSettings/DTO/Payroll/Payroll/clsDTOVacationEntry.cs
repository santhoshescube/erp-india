﻿using System ;
using System.Collections.Generic ;
using System.Linq ;
using System.Text;

namespace DTO
{
    public class clsDTOVacationEntry
    {
        public int RowNum { get; set; }
        public int VacationID { get; set; }
        public int EmployeeID { get; set; }
        public bool IsRateOnly { get; set; }
        public int LeavePolicyID { get; set; }
        public int Scale { get; set; }
        public decimal NetAmount { get; set; }
        public int CurrencyID { get; set; }
        public string ShortDescription { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public decimal TakenLeaves { get; set; }


        public decimal OvertakenLeaves { get; set; }
        public DateTime ActualDateTime { get; set; }


        public int IssuedTickets { get; set; }
        public decimal TicketExpense { get; set; }
        public string LeaveID { get; set; }
        public bool IsProcessSalaryForCurrentMonth { get; set; }
        public bool IsConsiderAbsentDays { get; set; }
        public decimal GivenAmount { get; set; }
        public decimal ExperienceDays { get; set; }
        public decimal AbsentDays { get; set; }
        public decimal EligibleLeavePayDays { get; set; }
        public int CompanyID { get; set; }
        public string Remarks { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public bool IsClosed { get; set; }
        public bool PaidLeave { get; set; }
        public bool IsEncashOnly { get; set; }
        public decimal NoOfDays { get; set; }
        public bool IsExtension { get; set; }
        public string RejoinDate { get; set; }
        public DateTime JoiningDate { get; set; }
        public int AccountID { get; set; }
        public int TransactionTypeID { get; set; }
        public string ChequeNumber { get; set; }
        public DateTime ChequeDate { get; set; }
        public bool NeedVoucherEntry { get; set; }
        public decimal TakenLeavePayDays { get; set; }
        public int RePaymentID { get; set; }
        public string strSearchKey { get; set; }
        public decimal EncashComboOffDay { get; set; }

        public List<clsDTOVacationEntryDetail> lstclsDTOVacationEntryDetail { get; set; }
    }
    public class clsDTOVacationEntryDetail
    {
        public int SerialNo { get; set; }
        public int VacationID { get; set; }
        public int AdditionDeductionID { get; set; }
        public decimal Amount { get; set; }
        public decimal NoOfDays { get; set; }
        public bool IsReleaseLater { get; set; }
        public decimal CompanyAmount { get; set; }
    }
}
