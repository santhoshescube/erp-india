﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOEmployeeLeaveStructure
    {
        public int intEmployeeID { get; set; }
        public int intCompanyID { get; set; }
        public string strFinYearStartDate { get; set; }
        public int intLeaveTypeID { get; set; }
        public decimal BalanceLeaves { get; set; }
        public decimal EncashLeaves { get; set; }
        public decimal CarryForwardLeaves { get; set; }
        public decimal MonthLeaves { get; set; }
        public decimal TakenLeaves { get; set; }
        public decimal EncashedDays { get; set; }
        public int intLeavePolicyID { get; set; }
        public decimal CarryForwardDays { get; set; }
        public decimal Exceeded { get; set; }
        public bool IsEdit { get; set; }
    }
}
