﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;

namespace MyBooksERP
{
    public class clsDTOSalaryRelease
    {
       
        public int intTemplateID { get; set; }
        public int intProcessYear { get; set; }
        public int intCompanyID { get; set; }
        public int intBankNameID { get; set; }
        public int intTransactionTypeID { get; set; }
        public int intYearInt { get; set; }
        public int intDepartmentID{ get; set; }
        public int intCurrencyID{ get; set; }
        public int intPaymentID { get; set; }

        public string strProcessDate { get; set; }
        public string strFromDate1 { get; set; }
        public string strToDate1 { get; set; }
        public string strHostName { get; set; }
      


    }
}
