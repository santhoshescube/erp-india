﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOEmployeeListProcess
    {

        public string FromDate1 { get; set; }
        public string ToDate1 { get; set; }
        public int EmployeeID { get; set; }
        public int CompanyID { get; set; }
        public int SettleFlag { get; set; }
        public int IsPartial { get; set; }
        public int UserID { get; set; }
        public int IsFromTransfer { get; set; }
        public int ProcessTypeID { get; set; }

    }
}
