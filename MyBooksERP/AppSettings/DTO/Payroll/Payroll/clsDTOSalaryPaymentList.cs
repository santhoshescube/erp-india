﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOSalaryPaymentList
    {
        public int intCompanyID { get; set; }
        public double intPaymentID { get; set; }
        public int intEmployeeID { get; set; }
        public int intPaymentDetailID{ get; set; }
        public int intAddDedID{ get; set; }
        public double dblAmount{ get; set; }
        public string strRemarks{ get; set; }
        public int intLoanID{ get; set; }
        public int intEmpVenFlag{ get; set; }
        public int intAddFlag{ get; set; }
        public int intIsEditable{ get; set; }
        public int intCurrencyId { get; set; }

    }
}
