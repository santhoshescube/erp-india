﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOSalaryProcess
    {

        public string strFromDate1 { get; set; }
        public string strToDate1 { get; set; }
        public int intEmployeeID { get; set; }
        public int intCompanyID { get; set; }
        public int intSettleFlag { get; set; }
        public int intIsPartial { get; set; }
        public int intUserID { get; set; }
        public int intIsFromTransfer { get; set; }
        public int intProcessTypeID { get; set; }
        public int intSalaryDay { get; set; }
        public string strHostName { get; set; }
        public int intUserId { get; set; }
        public int intBranchIndicator { get; set; }
        public int intPaymentClassificationID { get; set; }
        public int intWorkLocationID { get; set; }
        public List<clsDTOEmployeeListProcess> lstEmployeeListProcess { get; set; }

    }
}
