﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOAdditionDeduction
    {

        public int AdditionDeductionID { get; set; }
        public string AdditionDeduction { get; set; }
        public string AdditionDeductionArb { get; set; }
        public bool IsAddition { get; set; }
        public bool IsPredefined { get; set; }
        
    }
}
