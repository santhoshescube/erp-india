﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
   public  class clsDTOSalaryPayment
    {
    public int intCompanyID { get; set; }
    public double  intPaymentID { get; set; }
    //public int intPaymentDetailID { get; set; }
    public int intEmployeeID { get; set; }
    public string strGetHostName { get; set; }
    public int intUserId { get; set; }
    public int intAddDedID { get; set; }
    public int intTransactionTypeID { get; set; }
    public bool intFormActiveFlag { get; set; }
    public bool intConformActiveFlag { get; set; }
    public string strPProcessDate { get; set; }
    public int intPiCurrencyIDFilter { get; set; }
    public string strPaymentDate { get; set; }
    public string strRowCount { get; set; }
    public int intPaymentDetailID { get; set; }
    public double dblAmount { get; set; }
    public string strRemarks { get; set; }
    public int intLoanID { get; set; }
    public int intEmpVenFlag { get; set; }
    public int intAddFlag { get; set; }
    public int intIsEditable { get; set; }
    public int intCurrencyId { get; set; }
    public double dblNetAmount { get; set; }
    public List<clsDTOEmployeePayList> lstEmployeePayList { get; set; }
    public List<clsDTOSalaryPaymentList> lstSalaryPaymentList { get; set; }

    }
}
