﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOAddParicularsToAll
    {

        public List<clsDTOAddParicularsToAllDetail> DTOAddParicularsToAllDetail { get; set; }
       
    }
    public class clsDTOAddParicularsToAllDetail
    {
        public int PaymentID { get; set; }
        public int CurrencyID { get; set; }
        public decimal Amount { get; set; }
        public int IsAddition { get; set; }
        public int AddDedID { get; set; }

    }
}
