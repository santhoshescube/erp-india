﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
   public class clsDTOLeaveOpening
    {
       public List<clsDTOLeaveOpeningDetail> lstclsDTOLeaveOpeningDetail { get; set; }
    }

   public class clsDTOLeaveOpeningDetail
   {
        public int Mode {get;set;}
        public int EmployeeID  {get;set;}
        public decimal TakenLeaves  {get;set;}
        public decimal PaidDays { get; set; }
        public decimal TakenTickets { get; set; }
        public decimal PaidTicketAmount { get; set; }
   }

}
