﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOLeaveExtension
    {
        public int ExtensionID { get; set; }
        public int EmployeeID { get; set; }
        public int LeaveExtensionTypeID { get; set; }
        public int LeaveTypeID { get; set; }
        public string EntryDate { get; set; }
        public string MonthYear { get; set; }
        public decimal NoOfDays { get; set; }
        public decimal AdditionalMinutes { get; set; }
        public string Reason { get; set; }

    }
}
