﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    /*********************************************
      * Author       : Arun
      * Created On   : 12 Apr 2012
      * Purpose      : Salary Advance Properties 
      * ******************************************/
    public class clsDTOSalaryAdvance
    {
        /// <summary>
        ///  Unique ID of the Absent Policy. Primary Key. 
        /// </summary>
        public int SalaryAdvanceID { get; set; }
        /// <summary>
        /// Coresponding EmployeeID
        /// </summary>
        public int EmployeeID { get; set; }
        /// <summary>
        /// Date of the SalaryAdvance
        /// </summary>
        public string Date { get; set; }
        /// <summary>
        /// Salary advance amount
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// related Remarks
        /// 
        /// </summary>
        public string Remarks { get; set; }
        /// <summary>
        /// Checking Salary advance is setteled(cut from salary)
        /// </summary>
        public bool IsSettled { get; set; }
        /// <summary>
        /// Employee's Company BankID
        /// </summary>
        public int AccountID { get; set; }
        /// <summary>
        /// Transaction type bank or cash
        /// </summary>
        public int TransactionTypeID { get; set; }
        /// <summary>
        /// Transaction type is bank then cheque no.
        /// </summary>
        public string ChequeNumber { get; set; }
        /// <summary>
        /// corresponding cheque date
        /// </summary>
        public string ChequeDate { get; set; }
    }
}
