﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOLeaveEntry
    {
        public int intLeaveID { get; set; }
        public int intEmployeeID { get; set; }
        public int intLeaveTypeID { get; set; }
        public bool blnHalfDay { get; set; }
        public string strLeaveDateFrom { get; set; }
        public string strLeaveDateTo { get; set; }
        public string strRejoinDate { get; set; }
        public string strRemarks { get; set; }
        public int intCompanyID { get; set; }
        public bool blnFromAttendence { get; set; }
        public double dblNoOfDays { get; set; }
        public double dblNoOfHolidays { get; set; }
        public bool blnPaidLeave { get; set; }
        public bool blnIsMorning { get; set; }
    }
}
