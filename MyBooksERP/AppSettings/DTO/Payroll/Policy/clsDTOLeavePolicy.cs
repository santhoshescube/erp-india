﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBooksERP
{
    /*********************************************
       * Author       : Arun
       * Created On   : 5 Apr 2012
       * Purpose      : Leave Policy Properties 
       * ******************************************/
    public class clsDTOLeavePolicy
    {
        /// <summary>
        /// Unique ID of the Leave Policy. Primary Key. 
        /// </summary>
        public int LeavePolicyID{get;set;}
        /// <summary>
        /// CompanyID of the Corresponding Leave Policy
        /// </summary>
        public int CompanyID{get;set;}
        /// <summary>
        /// Policy Name of the Corresponding Leave Policy
        /// </summary>
        public  string LeavePolicyName {get;set;}
        /// <summary>
        ///  leave policy Applicable for the DepartmentID 
        /// </summary>
        public int DepartmentID{get;set;}
        /// <summary>
        /// leave policy Applicable for the corresponding designations
        /// </summary>
        public int DesignationID{get;set;}
        /// <summary>
        /// leave policy Applicable for the corresponding Employment Types
        /// </summary>
        public int EmploymentTypeID{get;set;}
        /// <summary>
        /// Leave Policy Applicable From date
        /// </summary>
        public string ApplicableFrom{get;set;}
         
       
        public List<clsDTOLeavePolicyDetail> DTOLeavePolicyDetail { get; set;}
        public List<clsDTOLeavePolicyConsequenceDetail> DTOLeavePolicyConsequenceDetail { get; set; }
        /// <summary>
        /// Instantiate Items
        /// </summary>
        public clsDTOLeavePolicy()
        {
            this.DTOLeavePolicyDetail = new List<clsDTOLeavePolicyDetail>();
            this.DTOLeavePolicyConsequenceDetail = new List<clsDTOLeavePolicyConsequenceDetail>();
        }
    }
    public class clsDTOLeavePolicyDetail
    {
        /// <summary>
        /// Serial No of Leave Types for a particular leave PolicyID
        /// </summary>
        public int SerialNo { get; set; }
        /// <summary>
        /// Leave Type ID 
        /// </summary>
        public int LeaveTypeID { get; set; }
        /// <summary>
        /// No of leaves allowed for a particular Leave Type
        /// </summary>
        public decimal NoOfLeave { get; set; }
        /// <summary>
        /// leave Type is Encashable or not
        /// </summary>
        public bool IsEncashable { get; set; }
        /// <summary>
        /// Monthly Allowed No of leaves for a particular leave type
        /// </summary>
        public decimal MonthLeave { get; set; }
        /// <summary>
        /// No of days Carryforward 
        /// </summary>
        public decimal CarryForwardLeave { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsCommon { get; set; }
        /// <summary>
        /// No of days for Encash
        /// </summary>
        public decimal EncashDays { get; set; }
    }
    public class clsDTOLeavePolicyConsequenceDetail
    {
        /// <summary>
        /// serial no 
        /// </summary>
        public int SerialNo { get; set; }
        /// <summary>
        /// Specifing the Leave Type
        /// </summary>
        public int LeaveTypeID { get; set; }
        /// <summary>
        /// Specifig the Corresponding Calculationrefernce ID 
        /// </summary>
        public int CalculationID { get; set; }
        /// <summary>
        /// Deduction percentage for the Consequence
        /// </summary>
        public decimal CalculationPercentage { get; set; }
        /// <summary>
        /// Consequence applicable days
        /// </summary>
        public decimal ApplicableDays { get; set; }
        /// <summary>
        /// order No specifing the priority for the Consequence
        /// </summary>
        public int OrderNo { get; set; }
    }

}
