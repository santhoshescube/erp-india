﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOPFPolicy
    {
        public int intDeductionPolicyId { get; set; }
        public string strPolicyName { get; set; }
        public int intPaymentAccountID { get; set; }
        public int intEmployerAccountID { get; set; }
        public string strWithEffect { get; set; }
        public int intAdditionID { get; set; }
        public int intParameterId { get; set; }

        public decimal decAmountLimit { get; set; }
        public decimal dEmployerPart { get; set; }
        public decimal dEmployeePart{ get; set; }
        public decimal decEPSEmployer { get; set; }
        public decimal decEDLI { get; set; }
        public decimal decEPFAdmin { get; set; }
        public decimal decEDLIAdmin { get; set; }
        public decimal decFixedAmount { get; set; }

        public int intRateOnly { get; set; }
        public int intAdditionDeductionID { get; set; }

        public List<clsDTOPFPolicyDetails> lstclsDTOPFPolicyDetails { get; set; }
    


    }
    public class clsDTOPFPolicyDetails
    {
        public int intDetDeductionPolicyId { get; set; }
        public int intDetAddDedID { get; set; }
        
    }

    
}
