﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
   Author:		<Author,,Laxmi>
   Create date: <Create Date,21 Mar 2011>
   Description:	<Shift Policy DTO Class>
================================================
*/
namespace DTO
{

    public class clsDTOShiftPolicy
    {
        public int ShiftID { get; set; }
        public int CompanyID { get; set; }
        public string  Description { get; set; }
        public string  FromTime { get; set; }
        public string  ToTime { get; set; }
        public int ShiftType { get; set; }
        public string Duration { get; set; }
        public int NoOfTimings { get; set; }
        public string MinWorkingHours { get; set; }
        public int ShiftOTPolicyID { get; set; }
        public int IsMinWorkOT { get; set; }
        public int AllowedBreakTime { get; set; }
        public int LateAfter { get; set; }
        public int EarlyBefore { get; set; }
        public int intBuffer { get; set; }
        public int intMinimumOT { get; set; }
        public int intIsBufferForOT { get; set; }
        public string WeekWrkHrs { get ;set;}
        public List<clsDTOShiftPolicyDynamic> lstclsDTOShiftPolicyDynamic { get; set; }
     
  
    }
    public class clsDTOShiftPolicyDynamic
    {
        public int OrderNo { get; set; }
        public int ShiftID { get; set; }
        public string Description { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string Duration { get; set; }
        public string BreakTime { get; set; }
        public int ShiftOTPolicyID { get; set; }
        public int BufferTime { get; set; }
        public string MinWorkingHours { get; set; }
        public int AllowedBreakTime { get; set; }
        public string DynamicDescription { get; set; }
    }

}
