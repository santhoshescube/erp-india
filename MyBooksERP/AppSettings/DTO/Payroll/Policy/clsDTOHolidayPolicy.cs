﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
     /*********************************************
       * Author       : Arun
       * Created On   : 10 Apr 2012
       * Purpose      : Holiday Policy Properties 
       * ******************************************/
    public class clsDTOHolidayPolicy
    {
        /// <summary>
        /// Unique ID of the Holiday Policy. Primary Key.
        /// </summary>
       public int HolidayPolicyID{get;set;}
        /// <summary>
        /// Name of the Holiday Policy
        /// </summary>
         public string HolidayPolicy{get;set;}
        /// <summary>
        /// check Holiday and shortage shown in salary slip separatly or together
        /// </summary>
         public int CalculationID { get; set; }
         /// <summary>
         /// checking Company policy depend
         /// </summary>
         public int CompanyBasedOn { get; set; }
         /// <summary>
         /// checking rate only
         /// </summary>
         public bool IsRateOnly { get; set; }
         /// <summary>
         /// coressponding Holiday rate
         /// </summary>
         public decimal HolidayRate { get; set; }
         /// <summary>
         /// specifing rate is based on Hour
         /// </summary>
         public bool IsRateBasedOnHour { get; set; }
         /// <summary>
         /// 
         /// hours working per day
         /// </summary>
         public decimal HoursPerDay { get; set; }
         /// <summary>
         /// Specifing Holiday or Shortage 1 for Holiday, 2 for Shortage
         /// </summary>
         public int HolidayType { get; set; }
         /// <summary>
         /// 
         /// </summary>
         public bool IsHourBasedShift { get; set; }
         /// <summary>
         /// 
         /// </summary>
         public decimal CalculationPercentage { get; set; }

        public List<clsDTOSalaryPolicyDetailHoliday> DTOSalaryPolicyDetailHoliday { get; set; }
        /// <summary>
        /// Constructor
        /// </summary>
        public clsDTOHolidayPolicy()
        {
            this.DTOSalaryPolicyDetailHoliday = new List<clsDTOSalaryPolicyDetailHoliday>();
        }

    }


    /// <summary>
    /// Salary policy Details for addition deduction
    /// </summary>
    public class clsDTOSalaryPolicyDetailHoliday
    {
        /// <summary>
        /// salpolicy Details ID
        /// </summary>
        public int SerialNo { get; set; }
      //public int SalPolicyID{get;set;}
        /// <summary>
        /// Corresponding Addition deduction ID for Excluding
        /// </summary>
      public int AdditionDeductionID{get;set;}
        /// <summary>
        /// Holiday Or Shortage
        /// </summary>
      public int PolicyType { get; set; }
    }
}
