﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOOvertimePolicy
    {
        public int intOTPolicyID { get; set; }
        public string strOTPolicyName { get; set; }
        public int intCalculationID { get; set; }
        public decimal decTotalWorkingHours { get; set; }
        public bool boolIsRateOnly { get; set; }
        public int intCompanyBasedOn { get; set; }
        public decimal decOTRate { get; set; }
        public decimal CalculationPercentage { get; set; }

        public List<clsDTOSalaryPolicyDetail> DTOSalaryPolicyDetail { get; set; }

        
    }
    public class clsDTOSalaryPolicyDetail
    {
        public int intSerialNo { get; set; }
        public int intSalPolicyID { get; set; }
        public int intAdditionDeductionID { get; set; }
        public int intPolicyType { get; set; }

    }

}
