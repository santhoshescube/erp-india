﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
     /*********************************************
       * Author       : Arun
       * Created On   : 10 Apr 2012
       * Purpose      : Encash Policy Properties 
       * ******************************************/
    public class clsDTOEncashPolicy
    {
        /// <summary>
        /// Unique ID of the Encash Policy. Primary Key.
        /// </summary>
       public int EncashPolicyID{get;set;}
        /// <summary>
        /// Name of the Encash Policy
        /// </summary>
         public string EncashPolicy{get;set;}
        /// <summary>
        /// check Encash and shortage shown in salary slip separatly or together
        /// </summary>
         public int CalculationID { get; set; }
         /// <summary>
         /// checking Company policy depend
         /// </summary>
         public int CompanyBasedOn { get; set; }
         /// <summary>
         /// checking rate only
         /// </summary>
         public bool IsRateOnly { get; set; }
         /// <summary>
         /// coressponding Encash rate
         /// </summary>
         public decimal EncashRate { get; set; }
         /// <summary>
         /// specifing rate is based on Hour
         /// </summary>
         public bool IsRateBasedOnHour { get; set; }
         /// <summary>
         /// 
         /// hours working per day
         /// </summary>
         public decimal HoursPerDay { get; set; }
         /// <summary>
         /// Specifing Encash or Shortage 1 for Encash, 2 for Shortage
         /// </summary>
         public int EncashType { get; set; }
         /// <summary>
         /// 
         /// </summary>
         public bool IsHourBasedShift { get; set; }
         /// <summary>
         /// 
         /// </summary>
         public decimal CalculationPercentage { get; set; }

        public List<clsDTOSalaryPolicyDetailEncash> DTOSalaryPolicyDetailEncash { get; set; }
        /// <summary>
        /// Constructor
        /// </summary>
        public clsDTOEncashPolicy()
        {
            this.DTOSalaryPolicyDetailEncash = new List<clsDTOSalaryPolicyDetailEncash>();
        }

    }


    /// <summary>
    /// Salary policy Details for addition deduction
    /// </summary>
    public class clsDTOSalaryPolicyDetailEncash
    {
        /// <summary>
        /// salpolicy Details ID
        /// </summary>
        public int SerialNo { get; set; }
      //public int SalPolicyID{get;set;}
        /// <summary>
        /// Corresponding Addition deduction ID for Excluding
        /// </summary>
      public int AdditionDeductionID{get;set;}
        /// <summary>
        /// Encash Or Shortage
        /// </summary>
      public int PolicyType { get; set; }
    }
}
