﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{    
     /*********************************************
       * Author       : Ranju Mathew
       * Created On   : 12 Aug 2013
       * Purpose      : Unearned Policy Properties 
       * ******************************************/
 public class clsDTOUnearnedPolicy
    {

/// <summary>
        /// Unique ID of the Absent Policy. Primary Key.
        /// </summary>
       public int UnearnedPolicyID{get;set;}
        /// <summary>
        /// Name of the Absent Policy
        /// </summary>
        public string UnearnedPolicy{get;set;}

        public List<clsDTOUnearnedPolicyDetails> DTOUnearnedPolicyDetails { get; set; }
        public List<clsDTOSalaryPolicyUNDetails> DTOSalaryPolicyDetail { get; set; }
        /// <summary>
        /// Constructor
        /// </summary>
        public clsDTOUnearnedPolicy()
        {
            this.DTOUnearnedPolicyDetails = new List<clsDTOUnearnedPolicyDetails>();
            this.DTOSalaryPolicyDetail = new List<clsDTOSalaryPolicyUNDetails>();
        }

      
    }

 public class clsDTOUnearnedPolicyDetails
    {
         /// <summary>
         /// identifing Calculation based on 
         /// </summary>
        public int CalculationID { get; set; }
        /// <summary>
        /// checking Company policy depend
        /// </summary>
        public int CompanyBasedOn { get; set; }
        /// <summary>
        /// checking rate only
        /// </summary>
      public bool IsRateOnly{get;set;}
        /// <summary>
        /// coressponding absent rate
        /// </summary>
      public decimal UnearnedRate { get; set; }
        /// <summary>
        /// specifing rate is based on Hour
        /// </summary>
      public bool IsRateBasedOnHour{get;set;}
        /// <summary>
        /// 
        /// hours working per day
        /// </summary>
      public decimal HoursPerDay{get;set;}
        
    }
    /// <summary>
    /// Salary policy Details for addition deduction
    /// </summary>
 public class clsDTOSalaryPolicyUNDetails
    {
        /// <summary>
        /// salpolicy Details ID
        /// </summary>
        public int SerialNo { get; set; }
      //public int SalPolicyID{get;set;}
        /// <summary>
        /// Corresponding Addition deduction ID for Excluding
        /// </summary>
      public int AdditionDeductionID{get;set;}
        /// <summary>
        /// Absent Or Shortage
        /// </summary>
      public int PolicyType { get; set; }
    }
}
