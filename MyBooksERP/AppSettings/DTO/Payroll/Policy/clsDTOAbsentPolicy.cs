﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
     /*********************************************
       * Author       : Arun
       * Created On   : 10 Apr 2012
       * Purpose      : Absent Policy Properties 
       * ******************************************/
    public class clsDTOAbsentPolicy
    {
        /// <summary>
        /// Unique ID of the Absent Policy. Primary Key.
        /// </summary>
       public int AbsentPolicyID{get;set;}
        /// <summary>
        /// Name of the Absent Policy
        /// </summary>
         public string AbsentPolicy{get;set;}
        /// <summary>
        /// check absent and shortage shown in salary slip separatly or together
        /// </summary>
        public bool IsMergeBoth{get;set;}

        public List<clsDTOAbsentPolicyDeteails> DTOAbsentPolicyDeteails { get; set; }
        public List<clsDTOSalaryPolicyDetailAbsent> DTOSalaryPolicyDetailAbsent { get; set; }
        /// <summary>
        /// Constructor
        /// </summary>
        public clsDTOAbsentPolicy()
        {
            this.DTOAbsentPolicyDeteails = new List<clsDTOAbsentPolicyDeteails>();
            this.DTOSalaryPolicyDetailAbsent = new List<clsDTOSalaryPolicyDetailAbsent>();
        }

      
    }

    public class clsDTOAbsentPolicyDeteails
    {
         /// <summary>
         /// identifing Calculation based on 
         /// </summary>
        public int CalculationID { get; set; }
        /// <summary>
        /// checking Company policy depend
        /// </summary>
        public int CompanyBasedOn { get; set; }
        /// <summary>
        /// checking rate only
        /// </summary>
      public bool IsRateOnly{get;set;}
        /// <summary>
        /// coressponding absent rate
        /// </summary>
      public decimal AbsentRate{get;set;}
        /// <summary>
        /// specifing rate is based on Hour
        /// </summary>
      public bool IsRateBasedOnHour{get;set;}
        /// <summary>
        /// 
        /// hours working per day
        /// </summary>
      public decimal HoursPerDay{get;set;}
        /// <summary>
        /// Specifing Absent or Shortage 1 for absent, 2 for Shortage
        /// </summary>
      public int AbsentType { get; set; }
    }
    /// <summary>
    /// Salary policy Details for addition deduction
    /// </summary>
    public class clsDTOSalaryPolicyDetailAbsent
    {
        /// <summary>
        /// salpolicy Details ID
        /// </summary>
        public int SerialNo { get; set; }
      //public int SalPolicyID{get;set;}
        /// <summary>
        /// Corresponding Addition deduction ID for Excluding
        /// </summary>
      public int AdditionDeductionID{get;set;}
        /// <summary>
        /// Absent Or Shortage
        /// </summary>
      public int PolicyType { get; set; }
    }
}
