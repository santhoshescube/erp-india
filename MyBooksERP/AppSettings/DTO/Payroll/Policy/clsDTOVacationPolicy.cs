﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class clsDTOVacationPolicy
    {
        //public int ShiftID { get; set; }
        public int VacationPolicyID{ get; set; }
        public string VacationPolicy{ get; set; }
        public bool IsRateOnly{ get; set; }
        public decimal RatePerDay { get; set; }
        public int CalculationID{ get; set; }
        public decimal NoOfTickets { get; set; }
        public decimal TicketAmount { get; set; }
        public decimal dblOnTimeRejoinBenefit { get; set; }
        public int intParticularID { get; set; }
        public bool chkLeaveOnly { get; set; }
        public bool chkFullSalary{ get; set; }
        public List<clsDTOVacationPolicyDetail> lstclsDTOVacationPolicyDetail { get; set; }
     
    }

    public class clsDTOVacationPolicyDetail
    {
        public int  SerialNo  { get; set; }
        public int  VacationPolicyID { get; set; }
        public int ParameterID { get; set; }
        public double  NoOfMonths { get; set; }
        public double NoOfDays { get; set; }
        public double NoOfTickets { get; set; }
        public double TicketAmount { get; set; }
    }

}



