﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class clsDTOWorkPolicy
    {
        public int PolicyID { get; set; }
        public string Description { get; set; }
        public int CompanyID { get; set; }
        public int DepartmentID { get; set; }
        public int DesignationID { get; set; }
        public int EmploymentTypeID { get; set; }
        public int ProjectID { get; set; }

        public bool DefaultPolicy { get; set; }
        public bool Active { get; set; }
        public int OffDayShiftID { get; set; }
        public int RotaionPolicyID { get; set; }

        public List<clsDTOWorkPolicyDetails> lstclsDTOWorkPolicyDetails { get; set; }
        public List<clsDTOWorkPolicyConsequences> lstclsDTOWorkPolicyConsequences { get; set; }


    }
    public class clsDTOWorkPolicyDetails
    {
        public int PolicyID { get; set; }
        public int DayID { get; set; }
        public int AllowedBreakTime { get; set; }
        public int LateAfter { get; set; }
        public int EarlyBefore { get; set; }
        public int ShiftID { get; set; }
    }

    public class clsDTOWorkPolicyConsequences
    {
        public int PolicyConsequenceID { get; set; }
        public int PolicyID { get; set; }
        public int ConsequenceID { get; set; }
        public int LOP { get; set; }
        public int Casual { get; set; }
        public decimal  Amount { get; set; }
        public int AllowedDaysPerMonth { get; set; }
    }

}
