﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class clsDTOSettlementPolicy
    {
        public int ModeAddEdit { get; set; }
        public int SetPolicyID { get; set; }
        public string DescriptionPolicy { get; set; }
        public int IsRateOnly { get; set; }
        public bool blnIsIncludeLeave{ get; set; }
        public double RatePerDay { get; set; }
        public int CalculationID { get; set; }

        public List<clsDTOSettlementPolicyDetail> lstclsDTOSettlementPolicyDetail { get; set; }
        public List<clsDTOSettlementPolicyDetailFive> lstclsDTOSettlementPolicyDetailFive { get; set; }
        public List<clsDTOInExGra> lstclsDTOInExGra { get; set; }
    }

        public class clsDTOSettlementPolicyDetail
        {
            public int DtlsID { get; set; }
            public int ParameterID { get; set; }
            public decimal NoOfMonths { get; set; }
            public decimal NoOfDays { get; set; }
            public decimal decTerminationDays { get; set; }
            public int PolicyType { get; set; }
        }

        public class clsDTOSettlementPolicyDetailFive
        {
            public int DtlsID { get; set; }
            public int ParameterID { get; set; }
            public decimal NoOfMonths { get; set; }
            public decimal NoOfDays { get; set; }
            public decimal decTerminationDays { get; set; }
            public int PolicyType { get; set; }
        }
       public class clsDTOInExGra
       {
            public int AdditionDeductionID { get; set; }
            public string DescriptionStr { get; set; }
            public bool Sel { get; set; }
       }
    
}
