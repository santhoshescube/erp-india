﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    /*********************************************
       * Author       : Arun
       * Created On   : 11 Apr 2012
       * Purpose      : Holiday Calendar Properties 
       * ******************************************/
    public class clsDTOHolidayCalendar
    {
        public int HolidayID { get; set; }
        public int CompanyID { get; set; }
        public int HolidayTypeID { get; set; }
        public string HolidayDate { get; set; }
        public string Remarks { get; set; }
  



    }
}
