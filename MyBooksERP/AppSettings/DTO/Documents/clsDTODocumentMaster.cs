﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBooksERP
{
    
 /*=================================================
Description :	     <Document Transactions>
Modified By :		 <Laxmi>
Modified Date :      <13 Apr 2012>
================================================*/


    public class clsDTODocumentMaster
    {
        public int intDocumentID { get; set; }
        public int intDocumentTypeID { get; set; }
        public int intOperationTypeID { get; set; }
        public long lngReferenceID { get; set; }
        public string strReferenceNumber { get; set; }
        public string strExpiryDate { get; set; }
        public bool blnIsReceipted { get; set; }
        public int intCreatedBy { get; set; }
        public int intStatusID { get; set; }

        public int intRowNumber { get; set; }
        public string strOperationNumber { get; set; }
        public int intVendorID { get; set; }

        public int intOrderNo { get; set; }
        public string strReceiptIssueDate { get; set; }
        public int intCustodian { get; set; }
        public int intDocumentStatusID { get; set; }
        public int intBinID { get; set; }
        public int intIssueReasonID { get; set; }
        public string strExpectedReturnDate { get; set; }
        public string strRemarks { get; set; }


    }
}
