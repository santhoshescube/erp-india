﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,28 Feb 2011>
Description:	<Description,,DTO for ConfigurationSettings>
================================================
*/
namespace MyBooksERP
{
   public  class clsDTOConfigurationSettings
    {

        public  int intConfigurationID {get; set;}
        public int intConfigRefID { get; set; }

        public string strConfigurationValue { get; set; }
    }
}
