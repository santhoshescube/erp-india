﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,28 Feb 2011>
Description:	<Description,,DTO for UserInformation>
================================================
*/

namespace MyBooksERP
{
    public class clsDTOUserInformation
    {
        public int intUserID { get; set; }
        public int intRoleID { get; set; }
        public int intEmployeeID { get; set; }

        public string strUserName { get; set; }
        public string strPassword { get; set; }
        public string strEmailID { get; set; }

        public bool blnSendSms { get; set; }

        // New fields
        public int intUserTypeID { get; set; }
        public string strUserType { get; set; }

        public IList<clsDTOUserCompanyDetails> lstUserInformation { get; set; }
    }

    public class clsDTOUserCompanyDetails
    {
        public int CompanyID { get; set; }
    }  
}
