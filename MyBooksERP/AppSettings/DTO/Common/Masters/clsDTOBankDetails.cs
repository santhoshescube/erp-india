﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,17 Feb 2011>
Description:	<Description,,DTO for Bank>
================================================
*/
namespace MyBooksERP
{
    public class clsDTOBankDetails
    {             
        public int intBankNameID {get; set;}
        public int intCountryID { get; set; }
        public int intBankBranchID { get; set; }
        public int intAccountID { get; set; } 
              
        public string strDescription {get; set;}
        public string strBankCode { get; set; }
        public string strAddress { get; set; }
        public string strTelephone {get; set;}
        public string strFax {get; set;}              
        public string strEmail { get; set; }        
        public string strWebsite { get; set; }
        
        public long lngUAEBankCode { get; set; }

        public bool blnIsAgent { get; set; }

        public string strBankSortCode { get; set; }
       
    }
}
