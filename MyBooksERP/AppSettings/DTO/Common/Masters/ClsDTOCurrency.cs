﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,4 April 2011>
Description:	<Description,DTO for Currency>
================================================
*/

namespace MyBooksERP
{
   public class ClsDTOCurrency
    {
        public int intCurrencyDetailID {get; set;}
        public int intCurrencyID {get; set;}
        public int intCompanyID {get; set;}
        public double dblExchangeRate {get; set;}
        public DateTime dtpExchangeDate { get; set; }
        public string strCompanyCurrency { get; set; }
        public string strCurrency { get; set; }
        public string strShortDescription { get; set; }
      
    }


}
