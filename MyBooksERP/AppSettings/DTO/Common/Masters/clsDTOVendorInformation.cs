using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=============================================================
   Author:          <Author,,Albz>
   Create date:     <Create Date,,16 Feb 2011>
   Description:	    <Description,,DTO for VendorInformation>
   Modified By:     <Author,,Amal>
   Modified date:   <Modified Date,,31 Aug 2011>
=============================================================
*/
namespace MyBooksERP
{
    public class clsDTOVendorInformation
    {
        public int intVendorID { get; set; }
        public int intVendorType { get; set; }
        public int intTransType { get; set; }
        public int intBankID { get; set; }
        public int intBranchID { get; set; }
        public int intAccountHead { get; set; }
        public int intCountry { get; set; }
        public int intVendorAddID { get; set; }
        public int intLocationType { get; set; }
        public int intGLAccountID {get; set;}
        public int intCompanyID {get; set;}
        public int intSerialNo { get; set; }
        //public int intEmploymentTypeID { get; set; }
        //public int intDepartmentID { get; set; }
        //public int intDesignationID{ get; set; }
        public int intStatusID{ get; set; }
        public int intVendorClassificationID{ get; set; }
        public int intAccountTypeID { get; set; }
        public int intOperationTypeID { get; set; }
        public decimal decAdvanceLimit { get; set; }
        public decimal decCreditLimit { get; set; }
        public int intSourceMediaID { get; set; }
        public int intSupplierCurrencyID { get; set; }

        public double dblOpeningBalance{get; set;}
        public double dblCreditLimit{ get; set; }
        public double dblRatting { get; set; }

        public string strVendorCode { get; set; }
        public string strVendorName { get; set; }
        //public string strShortName { get; set; }
        public string strAgentCode { get; set; }
        public string strAccountNumber { get; set; }
        public string strContactPerson { get; set; }
        public string strAddress { get; set; }
        public string strTelephoneNo { get; set; }
        public string strTelephoneNo2 { get; set; }
        public string strMobile1 { get; set; }
        public string strMobile2 { get; set; }
        public string strFax { get; set; }
        public string strEmail { get; set; }
        public string strWebsite { get; set; }
        public string strAddressName { get; set; }
        public string strState { get; set; }
        public string strZipCode { get; set; }
        public string strServiceTaxNo { get; set; }
        public string strCSTNo { get; set; }
        public string strTINNo { get; set; }
        public string strPANNo { get; set; }
        public string strVATNo { get; set; }
        public string strRatingRemarks { get; set; }
        public string strPassword { get; set; }
        public char chrBSOrPL { get; set; }
        public string strTRNNo { get; set; }
        
        //public string strGender { get; set; }
      

        //public decimal decSalaryOrIncome { get; set; }

        //public bool blnDepartmentWise{get; set;}
        public bool blnOrderWise{get; set;}
       public bool blnPredefined{get; set;}
        public int intCreditDaysLimit { get; set; }
        public IList<clsDTOVendorNearestCompetitor> lstNearestCompetitor {get;set;}

        // properties for enquiry page(web)

        public int intPageIndex { get; set; }
        public int intPageSize { get; set; }
        public string strSearchKey { get; set; }
    }

    public class clsDTOVendorNearestCompetitor
    {
       public int intNearestCompetitorID { get; set; }
    }
}