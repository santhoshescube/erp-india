﻿
namespace MyBooksERP
{
   public  class clsDTODocuments
    {
   
       public int intNode {get; set;}
       public int intParentNode { get; set; }
       public long lngCommonId { get; set; }
       public int intNavID { get; set; }
       public int intDocumentTypeID { get; set; }

       public string strDescription { get; set; }
       
       //Details

       public string strFileName{get; set;}
       public string strMetaData{get; set;}
    }
}
