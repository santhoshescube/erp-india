﻿
namespace MyBooksERP
{
    public class clsDTOSubCategory
    {
        public int intSubCategoryID { get; set; }
        public int intCategoryID { get; set; }
        public string strSubCategoryName { get; set; }
        public string strDescription { get; set; }
        public int intRowNumber { get; set; }
    }
}
