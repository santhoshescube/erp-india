﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,25 Feb 2011>
Description:	<Description,,DTO for ReportViewer>
================================================
*/
namespace MyBooksERP
{

   public class clsDTOReportviewer
    {
       //public int intmode { get; set; }
       public Int64 intRecId { get; set; }
       public string strType { get; set; }
       public string strDate { get; set; }
       public int intExecutive { get; set; }
       public int intCompany { get; set; }
       public int intOperation { get; set; }
       public int intStatusID { get; set; } // DocDocumentstatus.Receipt or issue

   }
}


