﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,21 Feb 2011>
Description:	<Description,,DTO for EmployeeInformation>
================================================
*/
namespace MyBooksERP
{
     public class clsDTOEmployeeInformation
    {
        public int intEmployeeID { get; set;}
        public int intCompanyID  { get; set;}
        public int intSalutation { get; set;}
        public int intNationalityID { get; set;}
        public int intReligionID { get; set;}
        public int intDepartmentID { get; set;}
        public int intDesignationID { get; set;}
        public int intEmploymentType { get; set;}
        public int intCountryofOriginID { get; set;}
        public int intMaritalStatusID { get; set;}
        public int intWorkStatusID { get; set;}
      
        public int intPolicyID { get; set;} 
        public int intNoticeperiod { get; set;}
        public int intAccountID { get; set;}
      
        public int intLeavePolicyID { get; set;}
        public int intEthnicOriginId { get; set; }
        public int intMothertongueID { get; set; }
        public int intVisaObtainedFrom { get; set; }
        public int intVisaTakenBy { get; set; }
        public int intRating { get; set; }
        public int intProductionStageID { get; set; }
        public int intReportingToID { get; set; }

       public bool blnIsAgent { get; set;}
       public bool blnAgentCode { get; set;}
       public bool blnPoliceClearenceReq { get; set; }
       public bool blnPoliceClearenceCom { get; set; }
         
      
       public string strEmployeeNumber { get; set;}
       public string strFirstName { get; set;}
       public string strMiddleName { get; set;}
       public string strLastName { get; set;}
       public string strEmployeeFullName { get; set;}
       public string strEmployeeShortName { get; set;}
       public string strGender { get; set;}
      
      
       public string strFathersName { get; set; }
       public string strGrandFatherName { get; set; }
       public string strGrandGrandFathersName { get; set; }
       public string strLanguagesKnown { get; set; }
       public string strIdentificationMarks { get; set; }
       public string strLocalAddress { get; set; }
       public string strLocalPhone { get; set; }
       public string strMobile { get; set; }
       public string strLocalFax { get; set; }
       public string strEmail { get; set; }
       public string strAddress { get; set; }
       public string strOverseasZipCode { get; set; }
       public string strPhone { get; set; }
       public string strOverseasMobile { get; set; }
       public string strOverseasFax { get; set; }
       public string strEmergencyContactPerson { get; set; }
       public string strEmergencyContactPhone { get; set; }
       public string strEmergencyContactMobile { get; set; }
       public string strEmergencyContactAddress { get; set; }
       public string strEmbassyRegistrationNumber { get; set; }
       public string strFathersJob { get; set; }
       public string strMothersName { get; set; }
       public string strMothersJob { get; set; }
       public string strSpouseName { get; set; }
       public string strOfficialEmail { get; set; }
       public string strNotes { get; set; }
       public string strBloodGroup { get; set; }
       //public string strRecentPhotoPath { get; set; }
       //public string strPassportPhotoPath { get; set; }

       public byte[] strRecentPhotoPath { get; set; }
       public byte[] strPassportPhotoPath { get; set; }

       public DateTime dtDateofBirth { get; set; }
       public DateTime dtDateofJoining { get; set; }
       public DateTime dtProbationEndDate { get; set; }
       public DateTime dtPerformanceReviewDate { get; set; }
       public DateTime dtDateOfMarriage { get; set; }



    // Transaction type

       public int intTransactionTypeID { get; set; }
       public int intBankNameID { get; set; }
       public int intBankBranchID { get; set; }
       public string strAccountNumber { get; set; }
       public int intComBankAccId { get; set; }
       public string strPersonID { get; set; }


       public int intDivisionID { get; set; }

        public int intVacationPolicyID { get; set; }
        public int intSettlementPolicyID { get; set; }
    }
}
