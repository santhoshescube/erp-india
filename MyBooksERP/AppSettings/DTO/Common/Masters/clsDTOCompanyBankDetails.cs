﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,16 Feb 2011>
   Description:	<Description,,CompanyBankDetails DTO Class>
================================================
*/
namespace MyBooksERP
{
    public class clsDTOCompanyBankDetails
    {
        public string strAccountNumber { get; set; }

        public int intBankId { get; set; }

        public int intCompanyBankId { get; set; }

        public int intAccountID { get; set; }
    }
}
