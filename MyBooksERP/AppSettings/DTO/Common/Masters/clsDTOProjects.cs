﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP 
{
    public class clsDTOProjects
    {
        public string stringCompanyName { get; set; }
        public string strProjectCode  { get; set; }
        public string strDscription { get; set; }
        public int intProjectInchargeID { get; set; }
        public string strRemarks { get; set; }
        public string strStartDate { get; set; }
        public string strEndDate { get; set; }
        public int intProjectID { get; set; }
        public string decEstimatedAmount { get; set; }
    }
}
