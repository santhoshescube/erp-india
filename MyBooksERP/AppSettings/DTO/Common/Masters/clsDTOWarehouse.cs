﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    /// <summary>
    /// Author:Arun
    /// Date:17/02/2011
    /// Purpose:For FrmWareHouse Transactions
    /// </summary>
    public class clsDTOWarehouse
    {
        //Master
        public int intWarehouseID { get; set; }
        public string strWarehouseName { get; set; }
        public string strShortName { get; set; }
        public int intCompanyID { get; set; }
        public string strAddress1 { get; set; }
        public string strAddress2 { get; set; }
        public string strZipCode { get; set; }
        public string strCityState { get; set; }
        public int intCountryID { get; set; }
        public string strDescription { get; set; }
        public int intInChargeID { get; set; }
        public string strPhone { get; set; }
        public bool blnActive { get; set; }

        public List<ClsDTOWarehouseCompanyDetails> lstWarehouseCompanyDetails { get; set; }
    }

    //Detail
    public class ClsDTOWarehouseCompanyDetails
    {
        public int CompanyID { get; set; }
    }
}