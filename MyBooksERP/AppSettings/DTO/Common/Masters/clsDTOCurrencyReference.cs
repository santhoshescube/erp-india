﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date, 5 April 2011>
Description:	<Description,DTO for CurrencyReference>
================================================
*/
namespace MyBooksERP
{
    public class clsDTOCurrencyReference
    {
        public int intCurrencyID {get; set;}
        public int intCountryID { get; set; }
        public short shtScale { get; set; }

        public string strShortDescription { get; set; }
        public string strDescription { get; set; }

    }
}
