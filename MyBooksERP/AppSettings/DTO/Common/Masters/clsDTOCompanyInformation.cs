﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,16 Feb 2011>
   Description:	<Description,,CompanyInformation DTO Class>
================================================
*/
namespace MyBooksERP
{
    public class clsDTOCompanyInformation
    {
       
        public int intCompanyID { get; set; }

        public int intParentID { get; set; }
        
        public bool blnCompanyBranchIndicator { get; set; }
        
        public string strName { get; set; }
        
        public string strShortName { get; set; }
        
        public string strPOBox { get; set; }
        
        public string strRoad { get; set; }
        
        public string strArea { get; set; }
        
        public string strBlock { get; set; }
        
        public string strCity { get; set; }

        public int intProvinceID { get; set; }

        public int intCountryID { get; set; }
        
        public string strPrimaryEmail { get; set; }
        
        public string strSecondaryEmail { get; set; }
        
        public string strContactPerson { get; set; }

        public int intContactPersonDesignationID { get; set; }
        
        public string strContactPersonPhone { get; set; }
        
        public string strPABXNumber { get; set; }
        
        public string strWebSite { get; set; }

        public int intCompanyIndustryID { get; set; }

        public int intCompanyTypeID { get; set; }
        
        public byte[] byteLogoFile { get; set; }

        public string strOffDayIDs { get; set; }

        public string strOtherInfo { get; set; }
        
        public int intCurrencyId { get; set; }
        
        public string strFinYearStartDate { get; set; }
        
        public int intWorkingDaysInMonth { get; set; }

        public int intUnernedPolicyID { get; set; }
        
        public int intAccountID { get; set; }
        
        public string strBookStartDate { get; set; }
        
        public bool blnIsMonth { get; set; }
        
        public string strEPID { get; set; }

        public string AccountNumber { get; set; }


        //----VAT Registration -------
        public string TaxablePersonName { get; set; }
        public string TaxablePersonNameArb { get; set; }
        public string TRN { get; set; }
        public string TaxAgencyName { get; set; }
        public string TAN { get; set; }
        public string TaxAgentName { get; set; }
        public string TAAN { get; set; }




        //Vat &Other Info

        public string strCompanyServiceTaxNo { get; set; }
        
        public string strCompanyCSTNo { get; set; }
        
        public string strCompanyTINNo { get; set; }
        
        public string strCompanyPANNo { get; set; }
        
        public string strVATRegistrationNo { get; set; }

        public List<clsDTOCompanyBankDetails> lstCompanyBankDetails { get; set; }
        
    }
}
