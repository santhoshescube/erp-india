﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,16 Feb 2011>
   Description:	<Description,,CommonRef DTO Class>
================================================
*/
namespace MyBooksERP
{
    public class clsDTOCommonRef
    {
        public string strCondition { get; set; }

        public string strTableName { get; set; }
        
        public string strFieldValues { get; set; }
        
        public string strFields { get; set; }
        
    }
}
