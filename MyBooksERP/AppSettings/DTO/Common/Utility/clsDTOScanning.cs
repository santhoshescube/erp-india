﻿
/* 
=================================================
Author:		<Author,Arun>
Create date: <Create Date,18 Apr 2012>
Description:	<Description,DTO for Document Attach>
================================================
*/
namespace MyBooksERP
{
    public class clsDTOScanning
    {
        //Master
        public int intDocumentAttachID { get; set; }
        public int intParentNode { get; set; }
        public int intReferenceID { get; set; }
        public int intOperationTypeID { get; set; }
        public int intDocumentTypeID { get; set; }
        public string strNodeName { get; set; }

        //Details

        public string strFileName { get; set; }
        public string strMetaData { get; set; }
    }
}

