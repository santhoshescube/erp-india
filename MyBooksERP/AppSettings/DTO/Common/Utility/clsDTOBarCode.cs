﻿
namespace MyBooksERP
{
    public class clsDTOBarCode
    {
        public int intBarcodeID { get; set; }
        public long lngItemID { get; set; }
        public long lngBatchID { get; set; }
        public string strBarcodeValue { get; set; }
        public byte[] imgBarcodeImage { get; set; }
        public int intRowNumber { get; set; }
        public int intEncodingID	{ get; set; }
        public bool blnIsShowlabel{ get; set; }
        public int intWidth{ get; set; }
        public int intHeight{ get; set; }
        public int intRotationID{ get; set; }
        public int intAlignment { get; set; }
        public int intLabelLocation	{ get; set; }
        public string strRGBHexBackColor{ get; set; }
        public string strRGBHexForeColor{ get; set; }
        public bool blnIsTemplate { get; set; }
    }
}
