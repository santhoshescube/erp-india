﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
   Author:		< Sanju >
   Create date: < 15 Dec 2011 >
   Description:	< Advance Search DTO>
================================================
*/
namespace MyBooksERP
{
    public class clsDTOAdvanceSearch
    {
        public string strSearchCondition { get; set; }
        public Int32 intMode { get; set; }
    }
}
