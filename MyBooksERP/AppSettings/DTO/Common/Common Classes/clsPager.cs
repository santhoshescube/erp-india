﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsPager
    {
        public long RowIndex { get; set; }
        public long TotalRecords { get; set; }

        public clsPager()
        {
            this.RowIndex = 1;
            this.TotalRecords = 0;
        }
    }
}
