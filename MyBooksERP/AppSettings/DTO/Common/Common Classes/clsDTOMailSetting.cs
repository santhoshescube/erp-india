﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOMailSetting
    {
        public int MailId { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string IncomingServer { get; set; }
        public string OutgoingServer { get; set; }
        public int PortNumber { get; set; }
        public string AccountType { get; set; }
        public bool EnableSsl { get; set; }
        public int ProductId { get; set; }
    }
}
