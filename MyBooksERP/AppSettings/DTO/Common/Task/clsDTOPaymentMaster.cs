﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOPaymentMaster
    {
        public Int64 intPaymentID{get;set;}
	    public string strPaymentNumber{get;set;}
        public Int64 intSerialNo { get; set; }
	    public string strPaymentDate{get;set;}
	    public int intOperationTypeID{get;set;}
	    public int intPaymentTypeID {get;set;}
	    public int intPaymentModeID{get;set;}
	    public Int64 intReferenceID{get;set;}
	    public string strChequeNumber{get;set;}
	    public string strChequeDate{get;set;}
	    public int intCreditHeadID{get;set;}
	    public int intDebitHeadID{get;set;}
	    public decimal decAmount{get;set;}
	    public string strVoucherNo{get;set;}
	    public string strVoucherIds{get;set;}
	    public bool blnPostToAccount{get;set;}
	    public int intCurrencyID{get;set;}
	    public int intCompanyID{get;set;}
	    public int intCreatedBy{get;set;}
	    public string strCreatedDate{get;set;}
	    public string strDescription{get;set;}
        public int intStatusID { get; set; }
        public Int64 intRowNumber { get; set; }
        public bool blnIsReceipt { get; set; }
        public int intAccountID { get; set; }
        public int intPaymentHeadID = 31;
    }
}
