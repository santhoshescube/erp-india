﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOExtraChargePayment
    {
        public long lngExtraChargePaymentID { get; set; }
        public string strExtraChargePaymentNo { get; set; }
        public DateTime dtExtraChargePaymentDate { get; set; }
        public int intCompanyID { get; set; }
        public int intCurrencyID { get; set; }
        public int intOperationTypeID { get; set; }
        public long lngReferenceID { get; set; }
        public int intVendorID { get; set; }
        public long lngExtraChargeID { get; set; }
        public decimal decTotalAmount { get; set; }
        public int intPaymentModeID { get; set; }
        public int intAccountID { get; set; }
        public string strChNo { get; set; }
        public DateTime dtChDate { get; set; }        
        public string strDescription { get; set; }
        public long lngRowNumber { get; set; }
    }
}
