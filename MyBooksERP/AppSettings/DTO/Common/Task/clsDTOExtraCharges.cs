﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOExtraCharges
    {
        public long lngExtraChargeID { get; set; }
        public string strExtraChargeNo { get; set; }
        public DateTime dtExtraChargeDate { get; set; }
        public int intOperationTypeID { get; set; }
        public long lngReferenceID { get; set; }
        public decimal decTotalAmount { get; set; }
        public int intCompanyID { get; set; }
        public int intCurrencyID { get; set; }
        public string strDescription { get; set; }
        public long lngRowNumber { get; set; }
    }
}
