﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MyBooksERP
{
    /// <summary>
    /// Created on  : 02.03.2011
    /// Created by  : Devi
    /// Description : Expense object
    /// </summary>
    public class clsDTOExpenseMaster
    {
        public int lngExpenseID { get; set; }
        public int intOperationType { get; set; }
        public long lngReferenceID { get; set; }
        public decimal decTotalAmount { get; set; }
        public int intCompanyID { get; set; }
        public string strRemarks { get; set; }
        public int intRowNumber { get; set; }
        public int intCreditHeadID { get; set; }
        public int intDebitHeadID { get; set; }
        public string strVoucherNo { get; set; }
        public string strVoucherIds { get; set; }
        public bool blnPostToAccount { get; set; }
        public string strAccountDate { get; set; }
        public int intCurrencyID { get; set; }

        public List<clsDTOExpenseDetails> objDTOExpenseDetails { get; set; }

        public int intExpenseHeadID = 4;
    }

    public class clsDTOExpenseDetails
    {
        public int intSerialNo { get; set; }
        public int intExpenseTypeID { get; set; }
        public decimal decQuantity { get; set; }
        public decimal decAmount { get; set; }
        public int intCreatedBy { get; set; }
        public string strDescription { get; set; }
        public decimal intVoucherID { get; set; }
    }
}
