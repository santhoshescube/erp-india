﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
Author:		<Author,Midhun>
Create date: <Create Date,13 Sep 2011>
Description:	<Description,,DTO for MandatoryDocumentTypes>
================================================
*/

namespace MyBooksERP
{
    public class clsDTOMandatoryDocumentTypes
    {
         public List<clsDTOMandatoryDocumentType> lstMandatoryDocumentTypes { get; set; }
         public int intCompanyID { get; set; }
         public int intOperationTypeID { get; set; }
    }

    public class clsDTOMandatoryDocumentType
    {
        public int intOrderNo { get; set; }
        public int intDocumentTypeID { get; set; }

    }
}
