﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
/* ===============================================
  Author:		<Author,,Arun>
  Create date: <Create Date,,23 Feb 2011>
  Description:	<Description,,DTO for CompanySettings>
================================================*/
namespace MyBooksERP
{

    public static class clsDTOCompanySettings
    {

        public static int intSettingsID { get; set; }
        public static int intCompanyID { get; set; }
        public static string strConfigurationItem { get; set; }
        public static string strSubItem { get; set; }
        public static string strDescription { get; set; }
        public static string strConfigurationValue { get; set; }
        public static bool blnPredefined { get; set; }
        public static int intValueLimt { get; set; }
        public static string strDefaultValue { get; set; }
        public static int intProductId { get; set; }

        static PropertyDescriptorCollection _property = new PropertyDescriptorCollection(null);
        public static PropertyDescriptorCollection collection
        {
            get { return _property; }
            set { _property = value; }
        }
    }
}