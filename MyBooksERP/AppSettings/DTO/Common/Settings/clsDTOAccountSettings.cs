﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
   Author:		<Author,,Arun>
   Create date: <Create Date,,24 Feb 2011>
   Description:	<Description,,DTO for AcccountSettings>
================================================
*/
namespace MyBooksERP
{
    public class clsDTOAccountSettings
    {
        public int intSettingsID { get; set; }
        public int intCompanyID { get; set; }
        public int intOperationModId { get; set; }
        public int intAccountID { get; set; }
        public int intGLAccountID { get; set; }
 
    }
}
