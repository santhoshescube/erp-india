﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/*
 ========================================================
 
 Author:<Author,,Amal Raj> 
 Create Date:<Create Date,,06 June 2011> 
 Description:<Description,,Terms And Condition DTO Class>
 
 ========================================================
 */

namespace MyBooksERP
{
    public class clsDTOTermsAndCond
    {
        public int intTermsID { get; set;}                                 //Terms And Condition Table
        public int intCompanyID { get; set; }
        public string strDescriptionEnglish { get; set; }
        public string strDescriptionArabic { get; set; }
        public int intOperationTypeID { get; set; }
                                                                           // Type Reference Table
        public int intTypeID { get; set; }
        public string strDescription {get; set;}
        


    }
}
