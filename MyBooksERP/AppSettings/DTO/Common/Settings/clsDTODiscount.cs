﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,4 Mar 2011>
Description:	<Description,,DTO for Discount Scheme>
================================================
*/

namespace MyBooksERP
{
    public class clsDTODiscount
    {
       
         public int  intDiscountTypeId {get; set;}
         
         public string  strDiscountShortName {get; set;}
         public string  strDescription  {get; set;}       

         public char charDiscountMode {get; set;} // STDiscountTypeReference
         
         public bool blnDiscountForAmount  {get; set;}
         public bool blnPercentOrAmount  {get; set;}
         public bool blnIsSaleType   {get; set;}
         public int intCurrencyID {get; set;}

         public double dblPercOrAmtValue  {get; set;}
        
         public bool blnPeriodApplicable { get; set; }
         public DateTime dteFromPeriod { get; set; }
         public DateTime dteToPeriod { get; set; }
         public bool blnSlabApplicable { get; set; }
         public decimal decMinSlab { get; set; }
         public string strUnit { get; set; }
         public bool blnActive { get; set; }

        
         public List<clsDTODiscountDetails> lstDiscountDetails { get; set; }


    }

    public class clsDTODiscountDetails
    {
        public int intDiscountTypeId { get; set; }
        public int intItemOrderId { get; set; }
        public int intItemID { get; set; }
        public int intCustomerOrderID { get; set; }
        public int intVendorID { get; set; }

        public char charModePercentOrAmt { get; set; } //item and customer

        public decimal decValue { get; set; }

        public bool blnSlabApplicableGrid { get; set; }
        public decimal decMinSlabGrid { get; set; }
        public int intUOMID { get; set; }


    }
}
