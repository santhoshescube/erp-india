﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
   public class clsDTORoleSettings
    {
       public int RoleID { get; set; }
       public int ModuleID { get; set; }
       public int MenuID { get; set; }
       public bool IsCreate { get; set; }
       public bool IsView { get; set; }
       public bool IsUpdate { get; set; }
       public bool IsDelete { get; set; }
       public string FormName { get; set; }
       public bool IsPrintEmail { get; set; }

       public List<clsDTORoleSettings> lstRoleDetails = new List<clsDTORoleSettings>();
    }
}
