﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,9 Mar 2011>
Description:	<Description,,DTO for EmailPopUp>
================================================
*/
namespace MyBooksERP
{
     public class clsDTOEmailPopUp
    {
        public int intMailId { get; set; }
        public int intPortNumber { get; set; }

        public string strUserName { get; set; }
        public string strPassWord { get; set; }
        public string strOutgoingServer { get; set; }
         
    }
}
