﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOChangepassword
    {
        public  int intUserID { get; set; }
        public string strUserName { get; set; }
        public string strCurrentPassword { get; set; }
        public string strNewPassword { get; set; }
    }
}
