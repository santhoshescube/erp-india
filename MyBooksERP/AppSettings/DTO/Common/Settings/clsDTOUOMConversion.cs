﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOUOMConversion
    {
        public int intUomConversionID { get; set; }
        public string strUOMType { get; set; }
        public int intUOMClassID { get; set; }
        public int intUOMBaseID { get; set; }
        public int intUOMID { get; set; }
        public int intConversionFactorID { get; set; }
        public double dblConversionValue { get; set; }
    }
}
