﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
   Author:		<Author,Shajan>
   Create date: <Create Date, 09 May 2011>
   Description:	<Description,,DTO for Organization Hierarchy Setting>
================================================
*/
namespace MyBooksERP
{
    public class clsDTOWorkFlowSettingsSetting
    {
        public int intHierarchyId { get; set; }
        public int intWorkFlowTypeID { get; set; }
        public int intUserID { get; set; }
    }
}