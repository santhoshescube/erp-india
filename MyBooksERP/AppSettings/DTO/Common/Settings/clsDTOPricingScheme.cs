﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOPricingScheme
    {
        public int intPricingSchemeID { get; set; }
        public string strPricingShortName { get; set; }
        public string strDescription { get; set; }
        public int intCurrencyID { get; set; }
        public int intPercentOrAmount { get; set; }
        public double dblPercOrAmtValue { get; set; }
        public int intRowNumber { get; set; }
    }
}
