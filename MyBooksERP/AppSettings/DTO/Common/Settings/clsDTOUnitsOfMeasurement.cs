﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,,1 Mar 2011>
Description:	<Description,,DTO for UOM>
================================================
*/

namespace MyBooksERP
{
    public class clsDTOUnitsOfMeasurement
    {
        public int intUOMID { get; set; }
        public string strDescription { get; set; }
        public string strShortName { get; set; }
        public int intUOMClassID { get; set; }
        public double dblQuantityPerUOM { get; set; }
        public int intScale { get; set; }

        public int intRowNumber { get; set; }
    }
}
