﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOAlertSettings
    {
        public long lngAlertSettingID { get; set; }
        public int intAlertTypeID { get; set; }
        public int intOperationTypeID { get; set; }
        public string strDescription { get; set; }
        public int intModuleID { get; set; }
    }
}