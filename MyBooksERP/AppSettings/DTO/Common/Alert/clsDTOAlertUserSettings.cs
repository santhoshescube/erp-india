﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MyBooksERP
{
    public class clsDTOAlertUserSettings
    {
        public long lngAlertUserSettingID { get; set; }
        public long lngAlertSettingID { get; set; }
        public int intUserID { get; set; }
        public string strUserName { get; set; }
        public int intProcessingInterval { get; set; }
        public int intRepaetInterval { get; set; }
        public int intValidPeriod { get; set; }
        public bool blnIsRepeat { get; set; }
        public bool blnIsAlertOn { get; set; }
        public bool blnIsEmailAlertOn { get; set; }
        public string strAlertMessage { get; set; }
    }
}
