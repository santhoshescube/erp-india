﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOAlertRoleSettings
    {
        public long lngAlertRoleSettingID { get; set; }
        public long lngAlertSettingID { get; set; }
        public int intRoleID { get; set; }
        public string strRoleName { get; set; }
        public int intProcessingInterval{ get; set; }
        public int intRepeatInterval{ get; set; }
        public int intValidPeriod { get; set; }
        public bool blnIsRepeat { get; set; }
        public bool blnIsAlertOn { get; set; }
        public bool blnIsEmailAlertOn { get; set; }
        public string strAlertMessage { get; set; }
    }
}
