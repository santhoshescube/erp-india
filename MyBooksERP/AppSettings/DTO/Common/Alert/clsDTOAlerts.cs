﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOAlerts
    {
        public long lngAlertID { get; set; }
        public long lngAlertUserSettingID { get; set; }
        public long lngReferenceID { get; set; }
        public DateTime dtmStartDate { get; set; }
        public string strAlertMessage { get; set; }
        public short shrStatus { get; set; }
    }
}
