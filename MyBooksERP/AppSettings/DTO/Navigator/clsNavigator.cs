﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    /*********************************************
   * Author       : Sreelekshmi
   * Created On   : 06 Oct 2011
   * Purpose      : Create Navigator 
   * ******************************************/

    public class clsNavigator
    {

        public int parentID { get; set; }
        public long EmployeeID { get; set; }
        public int SearchType { get; set; }
        public bool Current { get; set; }
        /// <summary>
        /// Constructor
        /// </summary>
        public clsNavigator()
        {     
        }

      
    }
    public enum NavSearch
    {
        // these indices are passing to stored procedure.
        // if you change any index, you must update it in the data access layer as well.
        Code = 1,
        Name = 2

    }
}
