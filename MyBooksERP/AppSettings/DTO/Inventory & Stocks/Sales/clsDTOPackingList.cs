﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOPackingList
    {
        public long lngInvoiceID { get; set; }
        public DateTime dtInvoiceDate { get; set; }
        public string strHSCode { get; set; }
        public string strInvoiceNo { get; set; }
        public string strGrossWeight { get; set; }
        public string strNetWeight { get; set; }
        public string strAccountOf1 { get; set; }
        public string strAccountOf2 { get; set; }
        public string strAccountOf3 { get; set; }
        public string strDrawnUnder1 { get; set; }
        public string strDrawnUnder2 { get; set; }
        public string strDrawnUnder3 { get; set; }
        public string strDescription { get; set; }
        public string strShippingMarks { get; set; }
        public string strTradeMark { get; set; }
        public string strManufactureName { get; set; }
        public string strNationality { get; set; }
        public string strAddress { get; set; }
        public string strTelephone { get; set; }
        public string strUOM { get; set; }
        public decimal decTotalUOM { get; set; }
        public decimal decFOBValue { get; set; }
        public decimal decFreightCharge { get; set; }
        public string strCFR { get; set; }
        public decimal decTotalCFR { get; set; }
        public string strDeclaration { get; set; }
        public decimal decGrossTotal { get; set; }
        public decimal decLCPercentage { get; set; }
        public decimal decNetTotal { get; set; }

        public IList<clsDTOPackingListDetails> lstclsDTOPackingListDetails { get; set; }
    }

    public class clsDTOPackingListDetails
    {
        public string strNoofRolls { get; set; }
        public decimal decRoll { get; set; }
        public string strItem { get; set; }
        public decimal decQuantity { get; set; }
        public string strUOM { get; set; }
        public decimal decUnitPrice { get; set; }
        public string strEachCartoon { get; set; }
        public decimal decTotal { get; set; }
    }
}