using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOSTSales : IDisposable
    {
        public int? intApprovedBy { get; set; }
        public string strApprovedDate { get; set; }
        public Int32 intCompanyID { get; set; }
        public int intOrderTypeID { get; set; }
        public int? intCreatedBy { get; set; }
        public string strCreatedDate { get; set; }
        public string strCreditCardNo { get; set; }
        public int intCurrencyID { get; set; }
        public string strDueDate { get; set; }
        public int intEmployeeID { get; set; }
        public decimal decGrandAmount { get; set; }
        public decimal decNetAmount { get; set; }

        public decimal decGrandDiscountAmt { get; set; }
        public bool blnGrandDiscount { get; set; }
        public decimal decGrandDiscountPercent { get; set; }

        
        public decimal decNetAmountRounded { get; set; }
        public string strInvoiceDate { get; set; }
        public int intInvoiceType { get; set; }
        public string strOrderDate { get; set; }
        public int intPaymentMethod { get; set; }
        public int intPaymentTermsID { get; set; }
        public Int64 intPointOfSalesID { get; set; }
        public string strQuotationDate { get; set; }
        public string strRemarks { get; set; }
        public string strReturnDate { get; set; }
        public string strSalesDate { get; set; }
        public Int64 intSalesInvoiceID { get; set; }
        public string strSalesInvoiceNo { get; set; }
        public string strSalesNo { get; set; }
        public Int64 intSalesOrderID { get; set; }
        public string strSalesOrderNo { get; set; }
        public Int64 intSalesQuotationID { get; set; }
        public string strSalesQuotationNo { get; set; }
        public Int64 intSalesReturnID { get; set; }
        public string strSalesReturnNo { get; set; }
        public int intShipAddressID { get; set; }
        public int intStatusID { get; set; }
        public int intVendorAddID { get; set; }
        public int intVendorID { get; set; }
        public int intWarehouseID { get; set; }
        public string strAddressName { get; set; }
        public string strCreatedBy { get; set; }
        public decimal decExpenseAmount { get; set; }
        public string strVendorName { get; set; }
        public decimal decAdvanceAmount { get; set; }
        public string strApprovedBy { get; set; }
        public string strVerificationDescription { get; set; }
        public int intVerificationCriteriaID { get; set; }

        public bool blnDiscountForAmount { get; set; }
        public bool blnPercentCalculation { get; set; }
        public decimal decDiscountValue { get; set; }

        public string strAccountDate { get; set; }
        public int intCreditHeadID { get; set; }
        public int intDebitHeadID { get; set; }
        public string strVoucherNo { get; set; }
        public string strVoucherIds { get; set; }
        public decimal decExchangeCurrencyRate { get; set; }
        public int intAccountID { get; set; }
        public int TaxSchemeID { get; set; }
        public decimal TaxAmount { get; set; }

        //Details

        public List<clsDTOSTSalesDetails> IlstClsDTOSTSalesDetails { get; set; }

        //Cancellation Details
        public string strCancellationDate { get; set; }
        public string strDescription { get; set; }
        public bool blnCancelled { get; set; }
        public int intCancelledBy { get; set; }
        public string strCancelledBy { get; set; }


        #region IDisposable Members

        void IDisposable.Dispose()
        {

        }

        #endregion
    }

    public class clsDTOSTSalesDetails
    {
        public decimal decGrandAmount { get; set; }
        public decimal decNetAmount { get; set; }

        public bool? blnDiscount { get; set; }
        public decimal decDiscountPercent { get; set; }
        public decimal decDiscountAmt { get; set; }

        public decimal decInvoiceQuantity { get; set; }
        public int intItemID { get; set; }
        public bool blnIsGroup { get; set; }
        public decimal decQuantity { get; set; }
        public decimal decRate { get; set; }
        public Int64 intSalesInvoiceID { get; set; }
        public Int64 intSalesOrderID { get; set; }
        public Int64 intSalesQuotationID { get; set; }
        public string strSerialNo { get; set; }
        public int intStatusID { get; set; }
        public int intUOMID { get; set; }
        public Int64 intBatchID { get; set; }

    }

   
}