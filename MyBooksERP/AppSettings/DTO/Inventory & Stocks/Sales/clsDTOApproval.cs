﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOApproval
    {
        public int intCompanyID { get; set; }
        public int intEmployeeID { get; set; }
        public int intApprovalType { get; set; }

        public string strFromDate { get; set; }
        public string strToDate { get; set; }
    }
}
