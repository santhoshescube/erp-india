﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
   public class clsDTOPosProductList
    {
       public int CompanyID { get; set; }
       public int CustomerId { get; set; }
       public List<clsProductListTemp> ProductList { get; set; }
    }


   public class clsProductListTemp
   {
       public int ProductID { get; set; }
       public decimal Quantity{get;set;}
       public int BaseUOMId { get; set; }
    
      
   }

}
