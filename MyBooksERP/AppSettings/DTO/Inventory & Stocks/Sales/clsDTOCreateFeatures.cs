﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOCreateFeatures
    {

    }

    public class clsFeatureTemp
    {
        public int FeatureID { get; set; }
        public string FeatureName { get; set; }
        public List<clsFeatureValue> Features { get; set; }
        public string FeaturesCSV
        {
            get
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                foreach (var s in Features)
                    sb.Append((sb.ToString().Length <= 0 ? string.Empty : ",") + string.Format("{0}{1}", s.FeatureValue, (s.IsDefault) ? "(Default)" : string.Empty));

                return sb.ToString();
            }
        }

        public clsFeatureTemp()
        {
            this.Features = new List<clsFeatureValue>();
        }
        public clsFeatureTemp(int featureID, string featureName)
        {
            this.FeatureID = featureID;
            this.FeatureName = featureName;
            this.Features = new List<clsFeatureValue>();
        }
    }

    public class clsFeatureValue
    {
        public Int64 FeatureValueID { get; set; }
        public string FeatureValue { get; set; }
        public bool IsDefault { get; set; }
    }
}
