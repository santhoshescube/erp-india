﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
   Author:		< Sanju >
   Create date: < 21 Nov 2011 >
   Description:	< POS DTO>
================================================
*/
namespace MyBooksERP
{
    public class clsDTOPOS
    {

        public bool blnDiscountForAmount { get; set; }
        public bool blnPercentCalculation { get; set; }
        public decimal decDiscountValue { get; set; }


        public Int32 intPOSID { get; set; }
        public Int32 intSLNO { get; set; }
        public string strPOSNo { get; set; }
        public DateTime dtePOSDate { get; set; }
        public Int32 intVendorID { get; set; }
        public DateTime dteDueDate { get; set; }
        public Int32 intVendorAddID { get; set; }
        public Int32 intPaymentTermID { get; set; }
        public string strRemarks { get; set; }
        public Int32 intGrandDiscountID { get; set; }
        public Int32 intCurrencyID { get; set; }
        public decimal decExpenseAmount { get; set; }
        public decimal decGrandDiscountAmount { get; set; }
        public decimal decGrandAmount { get; set; }
        public decimal decNetAmount { get; set; }
        public decimal decNetRounded { get; set; }
        public Int32 intCreatedBy { get; set; }
        public DateTime dteCreatedDate { get; set; }
        public Int32 intCompanyID { get; set; }
        public Int32 intStatusID { get; set; }
        public Int32 intSearchStatusID { get; set; }
        public Int32 intSalesCounterID { get; set; }
        public decimal decTendered { get; set; }
        public decimal decChange { get; set; }
        public IList<clsDTOPOSDetails> lstclsDTOPOSDetails { get; set; }
        public IList<clsDTOPOSItemGroupDetails> lstclsDTOPOSItemGroupDetails { get; set; }

        public DateTime dtePOSFromDate { get; set; }
        public DateTime dtePOSToDate { get; set; }
        public Boolean blnAddMode { get; set; }
        public Int32 intWareHouseID { get; set; }
        public Int32 intDebitHeadID { get; set; }
        public Int32 intOperationModID { get; set; }



    }
    public class clsDTOPOSDetails
    {
        public Int32 intItemID { get; set; }
        public Boolean blnIsGroup  { get; set; }
        public decimal decQuantity { get; set; }
        public Int32 intUOMID { get; set; }
        public Int32 intBatchID { get; set; }
        public decimal decRate { get; set; }
        public Int32 intDiscountID { get; set; }
        public decimal decDiscountAmount { get; set; }
        public decimal decGrandAmount { get; set; }
        public decimal decNetAmount { get; set; }
        public Boolean blnDeliveryRequired { get; set; }
        public Int32 intStatusID { get; set; }


        //public int intSerialNo { get; set; }
        //public Int64 intPOSID { get; set; }
        //public int intReferenceSerialNo { get; set; }


    }
    public class clsDTOPOSItemGroupDetails
    {
        public int intSerialNo { get; set; }
        public int intPOSID { get; set; }
        public int intPOSDetailsSerialNo { get; set; }
        public int intItemID { get; set; }
        public int intBatchID { get; set; }
        public int intUOMID { get; set; }
        public decimal decQuantity { get; set; }
    }
}
