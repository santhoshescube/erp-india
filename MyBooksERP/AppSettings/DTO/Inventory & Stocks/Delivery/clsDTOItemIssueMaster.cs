﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOItemIssueMaster
    {//de
        public Int64 intItemIssueID { get; set; }
        public string strItemIssueNo { get; set; }
        public int intOperationTypeID { get; set; }
        public Int64 intReferenceID { get; set; }
        public int intWarehouseID { get; set; }
        public string strDeliveryDate { get; set; }
        public int intIssuedBy { get; set; }
        public string strIssuedDate { get; set; }
        public int intCompanyID { get; set; }
        public string strRemarks { get; set; }
        public string strIssuedBy { get; set; }
        public string Lpono { get; set; }
        public int intVendorID { get; set; }
        public int intVendorAddID { get; set; }
        //list
        public IList<clsDTOItemIssueDetails> lstItemIssueDetails { get; set; }
        public IList<clsDTOItemIssueLocationDetails> lstItemIssueLocationDetails { get; set; }
        
    }

    public class clsDTOItemIssueDetails
    {
        public int intSerialNo { get; set; }
        public Int64 intItemIssueID { get; set; }
        public int intReferenceSerialNo { get; set; }
        public int intItemID { get; set; }
        public Int64 intBatchID { get; set; }
        public int intUOMID { get; set; }
        public string strPackingUnit { get; set; }
        public decimal decQuantity { get; set; }
        public bool blnIsGroup { get; set; }
        public decimal decQty { get; set; }
        public decimal decOldQty { get; set; }
        public decimal decOldDeliveredQty { get; set; }
        public IList<clsDTOItemGroupIssueDetails> lstItemGroupIssueDetails { get; set; }
    }

    public class clsDTOItemGroupIssueDetails
    {
        public int intSerialNo { get; set; }
        public int intItemIssueID { get; set; }
        public int intItemIssueDetailsSerialNo { get; set; }
        public int intItemID { get; set; }
        public Int64 intBatchID { get; set; }
        public int intUOMID { get; set; }
        public decimal decQuantity { get; set; }
    }
    public class clsDTOItemIssueLocationDetails
    {
        public int intSerialNo { get; set; }
        public long lngItemIssueID { get; set; }
        public int intItemID { get; set; }
        public long lngBatchID { get; set; }
        public string strBatchNo { get; set; }
        public decimal decQuantity { get; set; }
        public int intUOMID { get; set; }
        public int intLocationID { get; set; }
        public int intRowID { get; set; }
        public int intBlockID { get; set; }
        public int intLotID { get; set; }
    }

}
