﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTODirectDeliveryMaster
    {
        public Int64 intDirectDeliveryID { get; set; }
        public string strDirectDeliveryNo { get; set; }
        public int intOperationTypeID { get; set; }
        public int intOrderTypeID { get; set; }
        public Int64 intReferenceID { get; set; }
        public int intWarehouseID { get; set; }
        public string strDeliveryDate { get; set; }
        public int intIssuedBy { get; set; }
        public string strIssuedDate { get; set; }
        public int intCompanyID { get; set; }
        public string strRemarks { get; set; }
        public string strIssuedBy { get; set; }
        //newly added for modification
        public int intVendorID { get; set; }
        public int intVendorAddID { get; set; }
        public string strAddressName { get; set; }
        public string Lpono { get; set; }
        public string decNetAmount { get; set; }
        public decimal decExchangeCurrencyRate { get; set; }
        public int intStatusID{get;set;}

        public int intBatchless { get; set; }

        public IList<clsDTODirectDeliveryDetails> lstDirectDeliveryDetails { get; set; }
        public IList<clsDTODirectDeliveryLocationDetails> lstDirectDeliveryLocationDetails { get; set; }
        
    }

    public class clsDTODirectDeliveryDetails
    {
        public int intSerialNo { get; set; }
        public Int64 intDirectDeliveryID { get; set; }
        public int intReferenceSerialNo { get; set; }
        public int intItemID { get; set; }
        public Int64 intBatchID { get; set; }
        public int intUOMID { get; set; }
        public decimal decQuantity { get; set; }
        public bool blnIsGroup { get; set; }
        public decimal decQty { get; set; }
        public decimal decRate { get; set; }
        public int intBatchless { get; set; }
        public int intDiscountID { get; set; }
        public decimal decDiscountAmount { get; set; }
        public decimal decGrandAmount { get; set; }
        public decimal decNetAmount { get; set; }
        public IList<clsDTOItemGroupIssueDetailsDN> lstItemGroupIssueDetailsDN { get; set; }
        public IList<clsDTOBatchlessItemIssueDetails> lstItemBatchlessItemIssueDetails { get; set; }
    }

    public class clsDTOItemGroupIssueDetailsDN
    {
        public int intSerialNo { get; set; }
        public int intDirectDeliveryID { get; set; }
        public int intDirectDeliveryDetailsSerialNo { get; set; }
        public int intItemID { get; set; }
        public Int64 intBatchID { get; set; }
        public int intUOMID { get; set; }
        public decimal decQuantity { get; set; }
    }
    public class clsDTOBatchlessItemIssueDetails
    {
       
        public int intSerialNo { get; set; }
        public Int64 intDirectDeliveryID { get; set; }
        public int intReferenceSerialNo { get; set; }
        public int intItemID { get; set; }
        public Int64 intBatchID { get; set; }
        public int intUOMID { get; set; }
        public decimal decQuantity { get; set; }
        public bool blnIsGroup { get; set; }
        public decimal decQty { get; set; }
        public decimal decRate { get; set; }

       
        public decimal decNetAmount { get; set; }
    }
    public class clsDTODirectDeliveryLocationDetails
    {
        public int intSerialNo { get; set; }
        public long lngDirectDeliveryID { get; set; }
        public int intItemID { get; set; }
        public long lngBatchID { get; set; }
        public string strBatchNo { get; set; }
        public decimal decQuantity { get; set; }
        public int intUOMID { get; set; }
        public int intLocationID { get; set; }
        public int intRowID { get; set; }
        public int intBlockID { get; set; }
        public int intLotID { get; set; }
    }

}
