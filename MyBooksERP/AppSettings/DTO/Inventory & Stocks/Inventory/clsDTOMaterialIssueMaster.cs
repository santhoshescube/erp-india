﻿using System;
using System.Collections.Generic;
using System.Text;

/***********************************************
 * Created By       : Midhun
 * Creation Date    : 15 May 2012
 * Description      : Material Issue DTO
 * *********************************************/

namespace MyBooksERP
{
    public class clsDTOMaterialIssueMaster
    {
        public long MaterialIssueID { get; set; }
        public long SlNo { get; set; }
        public string MaterialIssueNo { get; set; }
        public string MaterialIssueDate { get; set; }
        public int WarehouseID { get; set; }
        public long TemplateID { get; set; }
        public decimal Quantity { get; set; }
        public int CompanyID { get; set; }
        public decimal NetAmount { get; set; }
        public decimal NetAmountRounded { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedByText { get; set; }
        public string CreatedDate { get; set; }
        public string Remarks { get; set; }
        public bool blnAutoMaterialIssue { get; set; }
        public long lngProductionID { get; set; }
        public List<clsDTOMaterialIssueDetails> MaterialIssueDetails { get; set; }
        //public List<clsDTOMaterialIssueLocationDetails> MaterialIssueLocationDetails { get; set; }

        public clsDTOMaterialIssueMaster()
        {
            // Instantiate Items
            this.MaterialIssueDetails = new List<clsDTOMaterialIssueDetails>();
            //this.MaterialIssueLocationDetails = new List<clsDTOMaterialIssueLocationDetails>();
        }
    }

    public class clsDTOMaterialIssueDetails
    {
        public int SerialNo { get; set; }
        public int ItemID { get; set; }
        public long BatchID { get; set; }
        public decimal Quantity { get; set; }
        public decimal ActualQuantity { get; set; }        
        public int UOMID { get; set; }
        public decimal Rate { get; set; }
        public decimal NetAmount { get; set; }
    }

    //public class clsDTOMaterialIssueLocationDetails
    //{
    //    public int SerialNo { get; set; }
    //    public int ItemID { get; set; }
    //    public long BatchID { get; set; }
    //    public decimal Quantity { get; set; }
    //    public int UOMID { get; set; }
    //    public int LocationID { get; set; }
    //    public int RowID { get; set; }
    //    public int BlockID { get; set; }
    //    public int LotID { get; set; }
    //}
}
