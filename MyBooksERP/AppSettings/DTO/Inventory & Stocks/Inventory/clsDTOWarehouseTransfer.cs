﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{/***********************************************
 * Created By       : Arun
 * Creation Date    : 10 Nov 2011
 * Description      : Handle Warehouse to warehouse stock transfer
 * *********************************************/


    public class clsDTOWarehouseTransfer
    {
        public Int64 intStockTransferID { get; set; }

        public Int64 intSlNo { get; set; }
        public int intOrderTypeID { get; set; }
        public string strStockTransferNo { get; set; }
        public string strOrderDate { get; set; }
        public int intVendorID { get; set; }
        public int intWarehouseID { get; set; }
        public int intReferenceID { get; set; }
        public int intReferenceInchargeID { get; set; }
        public string strDueDate { get; set; }
        public int intVendorAddID { get; set; }
        public int intPaymentTermsID { get; set; }
        public int intIncoTermsID { get; set; }
        public int intLeadTimeDetsID { get; set; }
        public string strRemarks { get; set; }
        public int intGrandDiscountID { get; set; }
        public decimal decGrandDiscountAmount { get; set; }
        public int intCurrencyID { get; set; }
        public decimal decExpenseAmount { get; set; }
        public decimal decGrandAmount { get; set; }
        public decimal decNetAmount { get; set; }
        public decimal decNetAmountRounded { get; set; }
        public int intCreatedBy { get; set; }
        public string strCreatedDate { get; set; }
        public int intApprovedBy { get; set; }
        public string strApprovedDate { get; set; }
        public string strVerificationDescription { get; set; }
        public int intCompanyID { get; set; }
        public int intStatusID { get; set; }
        //Lead time Details
        public decimal decLeadTime { get; set; }
        public string strProductionDate { get; set; }
        public string strTranshipmentDate { get; set; }
        public string strClearenceDate { get; set; }

        public decimal decExchangeCurrencyRate { get; set; }
        public List<clsDTOWarehouseTransferDet> lstObjClsDTOWarehouseTransferDet { get; set; }

        public string strVendorName{get;set;}
        public string strAddressName { get; set; }
        public string strEmployeeName { get; set; }
        public string strApprovedBy { get; set; }

        public string strStatus { get; set; }
        //Verification History
        public int intOperationTypeID{get;set;}	
	    public int intCancelledBy {get;set;}
	    public string strDescription	{get;set;}
	    public string strCancellationDate	{get;set;}

    }

   public class clsDTOWarehouseTransferDet
   {
      public int intSerialNo{get;set;}
      //--public Int64 intStockTransferID{get;set;}
      public Int64 intBatchID{get;set;}
      public int intItemID{get;set;}
      public decimal decQuantity{get;set;}
      public int intUOMID{get;set;}
      public decimal decRate{get;set;}
      public int intDiscountID{get;set;}
      public decimal decDiscountAmount{get;set;}
      public decimal decGrandAmount{get;set;}
      public decimal decNetAmount { get; set; }
   }
}
