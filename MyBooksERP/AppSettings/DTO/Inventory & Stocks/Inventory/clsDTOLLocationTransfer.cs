﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOLLocationTransfer
    {

        public int intCompanyID { get; set; }
        public int intWarehouseID { get; set; }
        public int intLocationID { get; set; }
        public int intRowID { get; set; }
        public int intBlockID { get; set; }
        public int intLotID { get; set; }
        public int intItemID { get; set; }
        public int intBatchID { get; set; }
        public decimal decQuantity { get; set; }
        public List<ClsDTOItemLocation> lstClsDTOItemLocation { get; set; }


      #region Dispose

        // Track whether Dispose method has already been called.
        private bool disposed = false;

        /// <summary>
        /// This method will be called by user code.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                // check to see if object is disposed.
                if (!this.disposed)
                {
                    // dispose unmanaged resources here.
                }
            }
            this.disposed = true;
        }

        #endregion

        #region Destructor
        ~clsDTOLLocationTransfer()
        {
            Dispose(false);
        }
        #endregion







    }

    public class ClsDTOItemLocation
    {
        //public int intWarehouseID { get; set; }
        public int intLocationID { get; set; }
        public int intRowID { get; set; }
        public int intBlockID { get; set; }
        public int intLotID { get; set; }
        public int intItemID { get; set; }
        public int intBatchID { get; set; }
        public int intQuantity { get; set; }
      

        #region Dispose

        // Track whether Dispose method has already been called.
        private bool disposed = false;

        /// <summary>
        /// This method will be called by user code.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                // check to see if object is disposed.
                if (!this.disposed)
                {
                    // dispose unmanaged resources here.
                }
            }
            this.disposed = true;
        }

        #endregion

        #region Destructor
        ~ClsDTOItemLocation()
        {
            Dispose(false);
        }
        #endregion




    }

}
