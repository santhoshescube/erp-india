﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,8 Mar 2011>
   Description:	<Description,,ItemLocationDetails DTO>
================================================
*/
namespace MyBooksERP
{
    public class clsDTOItemLocationDetails
    {
        public int intItemID { get; set; }
        public int intBatchID { get; set; }
        public int intLocationID { get; set; }
        public int intRowID { get; set; }
        public int intBlockID { get; set; }
        public int intLotID { get; set; }
        public decimal decQuantity { get; set; }
    }

    public class clsDTOItemLocationMaster
    {
        public int intWarehouseID { get; set; }
        public int intReferenceID { get; set; }
        public bool blnIsGRN { get; set; }
        public IList<clsDTOItemLocationDetails> lstItemLocationDetails { get; set; }
    }
}
