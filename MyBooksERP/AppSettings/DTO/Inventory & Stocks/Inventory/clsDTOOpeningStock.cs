﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOOpeningStock
    {
        public Int64 intOpeningStockID { get; set; }
        public int intCompanyID { get; set; }
        public int intWarehouseID { get; set; }
        public string strOpeningDate { get; set; }
        public int intCreatedBy { get; set; }
        public string strCreatedDate { get; set; }
        public int intModifiedBy { get; set; }
        public string strModifiedDate { get; set; }
        public int intRowNumber { get; set; }
        public int intOperationTypeID { get; set; }
        public IList<clsDTOOpeningStockDetails> lstOpeningStockDetails { get; set; }
    }

    public class clsDTOOpeningStockDetails
    {
        public int intSerialNo { get; set; }
        public Int64 intBatchID { get; set; }
        public string strBatchNumber { get; set; }
        public int intItemID { get; set; }
        public int intUOMID { get; set; }
        public decimal decQuantity { get; set; }
        public decimal decRate { get; set; }
        
        public string strExpiryDate { get; set; }
    }
}
