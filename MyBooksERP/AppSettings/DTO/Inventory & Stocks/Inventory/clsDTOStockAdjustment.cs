﻿using System;
using System.Collections.Generic;
using System.Text;

/***********************************************
 * Created By       : Ratheesh
 * Creation Date    : 18 June 2011
 * Description      : Handle stock adjustments
 * *********************************************/

namespace MyBooksERP
{
    public class clsDTOStockAdjustment
    {
        public long StockAdjustmentID { get ; set ; }
        public string AdjustmentNo { get; set; }
        public DateTime AdjustmentDate { get; set; }
        public int WarehouseID { get; set; }
        public int ReasonID { get; set; }
        public string Remarks { get; set; }
        public int CompanyID { get; set; }
        public int Mode { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBY { get; set; }
        public int StatusID { get; set; }
        public long tempStockAdjustmentID { get; set; }

        /// <summary>
        ///  Gets or sets list of adjusted items in the stock
        /// </summary>
        public List<clsDTOAdjustedItem> AdjustedItems { get; set;}
        public List<clsDTOAdjustedLocationDetails> AdjustedLocationDetails { get; set; }

        public clsDTOStockAdjustment()
        {
            // Instantiate Items
            this.AdjustedItems = new List<clsDTOAdjustedItem>();
            this.AdjustedLocationDetails = new List<clsDTOAdjustedLocationDetails>();
        }
    }

    public class clsDTOAdjustedItem
    {
        public int SerialNo { get; set; }
        public int ItemID { get; set; }
        public long BatchID { get; set; }
        public double CurrentQuantity { get; set; }
        public double NewQuantity { get; set; }
        public double Rate { get; set; }

        // optional fields
        public string ItemName { get; set; }
        public string ItemCode { get; set; }

        public double ActualQty { get; set; }

        /// <summary>
        /// Gets difference (absolute value) between CurrentQuantity and NewQuantity
        /// </summary>
        public double Difference
        {
            get { return NewQuantity - CurrentQuantity; }
        }
    }

    public class clsDTOAdjustedLocationDetails
    {
        public int SerialNo { get; set; }
        public int ItemID { get; set; }
        public long BatchID { get; set; }
        public double CurrentQuantity { get; set; }
        public double NewQuantity { get; set; }
        public int LocationID { get; set; }
        public int RowID { get; set; }
        public int BlockID { get; set; }
        public int LotID { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string BatchNo { get; set; }
    }
}
