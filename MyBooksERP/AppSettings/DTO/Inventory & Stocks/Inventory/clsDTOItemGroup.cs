﻿using System.Collections.Generic;

namespace MyBooksERP
{
    public class clsDTOItemGroup
    {
        public long lngItemGroupID { get; set; }
        public string strItemGroupCode { get; set; }
        public string strShortName { get; set; }
        public string strDescription { get; set; }
        public string strStartDate { get; set; }
        public string strEndDate { get; set; }
        public decimal decActualAmount { get; set; }
        public decimal decSaleAmount { get; set; }
        public decimal decLabourCost { get; set; }
        public int intCreatedBy { get; set; }
        public int intItemGroupStatusID { get; set; }
        public int intCompanyID { get; set; }
        public int intCategoryID { get; set; }
        public int intSubCategoryID { get; set; }       
        public int intBaseUomID { get; set; }
        public int intCostingMethodID { get; set; }
        public int intPricingSchemeID { get; set; }
        public int intRowNumber { get; set; }
        public decimal decMachineCost { get; set; }
        public string strBatch { get; set; }
        public string strBatchDate { get; set; }

        public List<clsDTOItemGroupDetails> objclsDTOItemGroupDetails { get; set; }
    }

    public class clsDTOItemGroupDetails
    {
        public int intSerialNo { get; set; }
        public long lngItemID { get; set; }
        public int intUOMID { get; set; }
        public decimal decQuantity { get; set; }
        public decimal decItemTotal { get; set; }
        public decimal decRate { get; set; }

        public string strItemName { get; set; }
        public string strItemCode { get; set; }
    }
   
}
