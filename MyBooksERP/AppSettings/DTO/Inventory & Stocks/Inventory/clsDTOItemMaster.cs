﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;

namespace MyBooksERP
{
    /// <summary>
    /// Created by  : Devi
    /// Created on  : 18.02.2011
    /// Description : Object for ItemMaster, ItemDetails, ItemReorder and ItemUnits
    /// 
    /// Modified By : Ratheesh
    /// Modified On : 11 Ap 2012
    /// </summary>
    public class clsDTOItemMaster
    {
        // ItemMaster properties
        public int intItemID { get; set; }
        public string strItemCode { get; set; }
        public string strShortName { get; set; }
        public string strDescription { get; set; }
        public string strDescriptionArabic { get; set; }
        public int intCategoryID { get; set; }
        public int intSubCategoryID { get; set; }
        public int intBaseUomID { get; set; }
        public int intDefaultPurchaseUomID { get; set; }
        public int intDefaultSalesUomID { get; set; }
        public DateTime dteCreatedDate { get; set; }
        public int intCreatedBy { get; set; }
        public int intItemStatusID { get; set; }
        public int intCompanyID { get; set; }
        public decimal decCutOffPrice { get; set; }

        // ItemDetails properties
        public int intProductTypeID { get; set; }
        public int intCostingMethodID { get; set; }
        public int intPricingSchemeID { get; set; }
        public int intCurrencyID { get; set; }
        public string strModel { get; set; }
        public int intManufacturerID { get; set; }
        public int intMadeInID { get; set; }
        public string strWarrantyPeriod { get; set; }
        public string strGuaranteePeriod { get; set; }
        public int intTaxID { get; set; }
        public double dblWidth { get; set; }
        public double dblHeight { get; set; }
        public double dblLength { get; set; }
        public int intSizeUomID { get; set; }
        public double dblWeight { get; set; }
        public int intWeightUomID { get; set; }
        public string strColour { get; set; }
        public string strImageFile { get; set; }
        public string strNotes { get; set; }
        public decimal decSaleRate { get; set; }
        public decimal decPurchaseRate { get; set; }
        public decimal decActualRate { get; set; }
        public string strBarcodeValue { get; set; }
        public byte[] imgBarcodeImage { get; set; }
        public decimal decReorderQuantity { get; set; }
        public decimal decReorderLevel { get; set; }
        public bool blnCalculationBasedOnRate { get; set; }
        public bool blnExpiryDateMandatory { get; set; }
        public bool blnIsLandingCostEffected { get; set; }

        public List<clsDTOItemReorder> objclsDTOItemReorder { get; set; }
        public List<clsDTOItemUOM> objclsDTOItemUOM { get; set; }
        public DataTable datBatchDetails { get; set; }

        // Added by Ratheesh
        public List<clsDTOItemDimension> Dimensions { get; set; }
        public List<clsFeatureTemp> Features { get; set; }
        public List<clsDTOItemImages> Images { get; set; }

        public clsDTOItemMaster()
        {
            this.Dimensions = new List<clsDTOItemDimension>();
            this.Features = new List<clsFeatureTemp>();
 
            this.Images =new List<clsDTOItemImages>();
        }
    }

    public class clsDTOItemReorder
    {
        // ItemReorder properties
        public int intSerialNo { get; set; }
        public int intItemID { get; set; }
        public int intWarehouseID { get; set; }
        public decimal decReorderLevel { get; set; }
        public decimal decReorderQuantity { get; set; }
        public int intLocationID { get; set; }
        public int intRowID { get; set; }
        public int intBlockID { get; set; }
        public int intLotID { get; set; }
    }

    public class clsDTOItemUOM
    {
        // ItemUOM properties
        public int intOrderID { get; set; }
        public int intItemID { get; set; }
        public int intUOMID { get; set; }
        public int intItemUOMType { get; set; }
    }

    public class clsDTOItemDimension
    {
        public int DimensionID { get; set; }
        public decimal DimensionValue { get; set; }
        public int UOMID { get; set; }
    }

    public class clsDTOItemImages
    {
        public string ViewName { get; set; }
        public string FileName { get; set; }
        public bool IsDefault { get; set; }
    }
}
