﻿using System;
using System.Collections.Generic;
using System.Text;

/***********************************************
 * Created By       : Midhun
 * Creation Date    : 16 May 2012
 * Description      : Material Return DTO
 * *********************************************/

namespace MyBooksERP
{
    public class clsDTOMaterialReturnMaster
    {
        public long MaterialReturnID {get;set;}
        public long SlNo { get; set; }
        public string MaterialReturnNo { get; set; }
        public string MaterialReturnDate { get; set; }
        public int WarehouseID { get; set; }
        public int ProjectID { get; set; }
        public int CompanyID { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedByText { get; set; }
        public string CreatedDate { get; set; }
        public string Remarks { get; set; }
        public decimal NetAmount { get; set; }
        public List<clsDTOMaterialReturnDetails> MaterialReturnDetails { get; set; }
        public List<clsDTOMaterialReturnLocationDetails> MaterialReturnLocationDetails { get; set; }

        public clsDTOMaterialReturnMaster()
        {
            // Instantiate Items
            this.MaterialReturnDetails = new List<clsDTOMaterialReturnDetails>();
            this.MaterialReturnLocationDetails = new List<clsDTOMaterialReturnLocationDetails>();
        }
    }

    public class clsDTOMaterialReturnDetails
    {
        public int SerialNo { get; set; }
        public int ItemID { get; set; }
        public long BatchID { get; set; }
        public decimal ReturnedQuantity { get; set; }
        public int UOMID { get; set; }
        public decimal Rate { get; set; }
        public decimal NetAmount { get; set; }
    }

    public class clsDTOMaterialReturnLocationDetails
    {
        public int SerialNo { get; set; }
        public int ItemID { get; set; }
        public long BatchID { get; set; }
        public decimal Quantity { get; set; }
        public int UOMID { get; set; }
        public int LocationID { get; set; }
        public int RowID { get; set; }
        public int BlockID { get; set; }
        public int LotID { get; set; }
    }
}
