﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP 
{
    public class clsDTOSummary
    {
        public int SummaryReportType { get; set; }
        public int CompanyID { get; set; }
        public int WarehouseID { get; set; }
        public int SummaryType { get; set; }
        public DateTime ParameterFromDate { get; set; }
        public DateTime ParameterToDate { get; set; }
        public string SelectFields { get; set; }
        public string SearchCondition { get; set; }
        public string GroupByCondition { get; set; }
    }
}
