﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOPurchaseOrder
    {

        //Purchase Quotation

        public Int64 intPurchaseID { get; set; }
        public string strPurchaseNo { get; set; }
        public string strDate { get; set; }
        public int intVendorID { get; set; }
        public string strDueDate { get; set; }
        public int intVendorAddID { get; set; }
        public int intPaymentTermsID { get; set; }
        public string strRemarks { get; set; }
        public int intEmployeeID { get; set; }
        //public int intGrandDiscountID { get; set; }
        public bool? blnGrandDiscount { get; set; }        
        public decimal decGrandDiscountPercent { get; set; }
        public decimal decGrandDiscountAmt { get; set; }
        public int intCurrencyID { get; set; }
        public decimal decExpenseAmount { get; set; }
        public decimal decGrandAmount { get; set; }
        public decimal decNetAmount { get; set; }
        public decimal decAdvAmount { get; set; }
        public int intCreatedBy { get; set; }
        public string strCreatedDate { get; set; }
        public int? intApprovedBy { get; set; }
        public string strApprovedDate { get; set; }
        public string strApprovedBy { get; set; }
        public int intCompanyID { get; set; }
        public int intStatusID { get; set; }
        public string strAddressName { get; set; }
        public string strVerifcationDescription { get; set; }
        public string strEmployeeName { get; set; }
        public int intIncoTerms { get; set; }
        public int intLeadTimeDetsID { get; set; }
        public decimal decLeadTime { get; set; }
        public string strProductionDate { get; set; }
        public string strTranShipmentDate { get; set; }
        public string strClearenceDate { get; set; }
        public int intContainerTypeID { get; set; }
        public int intNoOfContainers { get; set; }
        public string strVerificationCriteria { get; set; }
        public string strVendorName { get; set; }
        public int intTaxSchemeID { get; set; }
        public decimal decTaxAmount { get; set; }
        //PurchaseOrder

        public int intOrderType { get; set; }
        public Int64 intReferenceID { get; set; }

        //GRN 

        public int intWarehouseID { get; set; }
        public bool blnGRNType { get; set; }
        public decimal decNetAmountRounded { get; set; }
        public int intTempWarehouseID { get; set; }
        
        //Purchase Invoice 

        public int intInvoiceType { get; set; }
        public Int64 intPurchaseOrderID { get; set; }
        public string strPurchaseBillNo { get; set; }
        public bool blnGRNRequired { get; set; }
        public int intCreditHeadID { get; set; }
        public int intDebitHeadID { get; set; }
        public string strAccountDate { get; set; }
        public decimal decVoucherAmount { get; set; }
        public decimal decExchangeCurrencyRate { get; set; }
        public int intScale { get; set; }
        public Boolean blnIsCancel { get; set; }
        public Boolean blnIsUpdate { get; set; }
        public int intPurchaseAccountID { get; set; }

        //Purchase Return

        public Int64 intPurchaseInvoiceID { get; set; }

        //Details

        public string strCancellationDate { get; set; }
        public string strDescription { get; set; }
        public bool blnCancelled { get; set; }
        public int intCancelledBy { get; set; }
        public string strCancelledBy { get; set; }
        public int intVerificationCriteria { get; set; }
        public List<clsDTOPurchaseDetails> lstClsDTOPurchaseDetailsCollection { get; set; }
        public IList<clsDTOPurchaseLocationDetails> lstClsDTOPurchaseLocationDetailsCollection { get; set; }
        public int intFromWarehouseID { get; set; }

    }

    public class clsDTOPurchaseOrderDetails
    {
        //Purchase Quotation Details

        public int intSerialNo { get; set; }
        public Int64 intPurchaseID { get; set; }
        public int intItemID { get; set; }
        public decimal decQuantity { get; set; }
        public int intUOMID { get; set; }
        public decimal decRate { get; set; }
        public decimal decPurchaseRate { get; set; }

        public bool? blnDiscount { get; set; }
        public decimal decDiscountPercent { get; set; }
        public decimal decDiscountAmt { get; set; }

        public decimal decGrandAmount { get; set; }
        public decimal decNetAmount { get; set; }

        //Purchase Order Details

        //GRN Details

        public decimal decOrderQuantity { get; set; }
        public decimal decReceivedQuantity { get; set; }
        public decimal decExtraQuantity { get; set; }
        public decimal decOldReceivedQuantity { get; set; }
        public Int64 intBatchID { get; set; }
        public string strBatchNumber { get; set; }
        public string strExpiryDate { get; set; }

        //Purchase Invoice Details

        //Purchase Return Details

        public decimal decInvoiceQuantity { get; set; }
        public decimal decReturnQuantity { get; set; }
        public int intReasonID { get; set; }

        public string strItemName { get; set; }
        public string strItemCode { get; set; }

    }

    public class clsDTOPurchaseOrderLocationDetails
    {
        public int intSerialNo { get; set; }
        public long lngReferenceID { get; set; }
        public int intItemID { get; set; }
        public long lngBatchID { get; set; }
        public string strBatchNo { get; set; }
        public decimal decQuantity { get; set; }
        public int intUOMID { get; set; }
        public int intLocationID { get; set; }
        public int intRowID { get; set; }
        public int intBlockID { get; set; }
        public int intLotID { get; set; }
    }
}
