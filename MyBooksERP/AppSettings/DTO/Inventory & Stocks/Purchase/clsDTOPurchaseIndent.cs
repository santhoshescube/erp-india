﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,22 Feb 2011>
   Description:	<Description,,Purchase Indent DTO Class>
================================================
*/
namespace MyBooksERP
{
    public class clsDTOPurchaseIndent
    {
           public Int64 intPurchaseIndentID {get;set;}
           public string strPurchaseIndentNo {get;set;}
           public string strIndentDate {get;set;}
           public int intDepartmentID {get;set;}
           public string strDueDate {get;set;}
           public string strRemarks {get;set;}
           public int intCreatedBy {get;set;}
           public string strCreatedDate {get;set;}
           public int intCompanyID {get;set;}
           public int intStatusID { get; set; }
           public string strCancellationDate { get; set; }
           public string strDescription { get; set; }
           public bool blnCancelled { get; set; }
           public string strEmployeeName { get; set; }
           public string strCancelledBy { get; set; }
           public int intCancelledBy { get; set; }
           public List<clsDTOPurchaseIndentDetails> lstDTOPurchaseIndentDetailsCollection{get;set;}
    }

    public class clsDTOPurchaseIndentDetails
    {
        public int intSerialNo { get; set; }
        public Int64 intPurchaseIndentID { get; set; }
        public int intItemID { get; set; }
        public decimal decQuantity { get; set; }
        public int intUOMID { get; set; }
    }
}
