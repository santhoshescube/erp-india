﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOPurchaseReturnMaster
    {
        public Int64 intPurchaseReturnID { get; set; }
        public Int64 intPurchaseInvoiceID { get; set; }
        public string strPurchaseReturnNo { get; set; }
        public string strReturnDate { get; set; }
        public int intVendorID { get; set; }
        public string strRemarks { get; set; }
        public int intGrandDiscountID { get; set; }
        public decimal decGrandDiscountAmount { get; set; }
        public int intCurrencyID { get; set; }
        public decimal decNetAmount { get; set; }
        public decimal decGrandAmount { get; set; }
        public int intCreatedBy { get; set; }
        public string strCreatedDate { get; set; }
        public string strCreatedBy { get; set; }
        public int intCompanyID { get; set; }
        public decimal decGrandReturnAmount { get; set; }
        public int intStatusID { get; set; }
        public bool blnIsCancelled { get; set; }
        public int intWarehouseID { get; set; }
        public int intTaxSchemeID { get; set; }
        public string strCancellationDate { get; set; }
        public string strCancellationDescription { get; set; }
        public string strCancelledBy { get; set; }

        public int intCreditHeadID { get; set; }
        public int intDebitHeadID { get; set; }

        public int intAccountID { get; set; }
        public int intOrderTypeID { get; set; }

        public decimal decExpenseAmount { get; set; }
        public decimal decTaxAmount { get; set; }
        public List<clsDTOPurchaseReturnDetails> lstPurchaseReturnDetails { get; set; }
        public List<clsDTOPurchaseReturnLocationDetails> lstLocationDetails { get; set; }

    }

    public class clsDTOPurchaseReturnDetails
    {
        public int intSerialNo { get; set; }
        public Int64 intPurchaseReturnID { get; set; }
        public int intReferenceSerialNo { get; set; }
        public long lngBatchID { get; set; }
        public decimal decReturnQuantity { get; set; }
        public int intReasonID { get; set; }
        public int intUOMID { get; set; }
        public decimal decRate { get; set; }
        public int intDiscountID { get; set; }
        public decimal decDiscountAmount { get; set; }
        public decimal decGrandAmount { get; set; }
        public decimal decNetAmount { get; set; }
        public decimal decReturnAmount { get; set; }
        public int intItemID { get; set; }
    }

    public class clsDTOPurchaseReturnLocationDetails
    {
        public int intSerialNo { get; set; }
        public long lngReferenceID { get; set; }
        public int intItemID { get; set; }
        public long lngBatchID { get; set; }
        public string strBatchNo { get; set; }
        public decimal decQuantity { get; set; }
        public int intUOMID { get; set; }
        public int intLocationID { get; set; }
        public int intRowID { get; set; }
        public int intBlockID { get; set; }
        public int intLotID { get; set; }
    }
}
