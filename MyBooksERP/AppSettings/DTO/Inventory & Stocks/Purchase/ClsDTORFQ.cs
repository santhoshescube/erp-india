﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class ClsDTORFQ
    {
        public Int64 RFQID { get; set; }
        public string RFQNo { get; set; }
        public DateTime QuatationDate { get; set; }
        public DateTime CancellationDate { get; set; }
        public DateTime DueDate { get; set; }
        public string Remarks { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public int CompanyID { get; set; }
        public int StatusID { get; set; }
        public string VerifcationDescription { get; set; }
        public string EmployeeName { get; set; }
        public bool blnCancelled { get; set; }
        public int intCancelledBy { get; set; }
        public string strCancelledBy { get; set; }
        public string strApprovedBy { get; set; }
        public string CancellationDescription { get; set; }
        public List<clsDTORFQDetails> lstClsDTORFQDetails { get; set; }

    }
    public class clsDTORFQDetails
    {
        //Purchase Quotation Details

        public int SerialNo { get; set; }
        public Int64 RFQID { get; set; }
        public int ItemID { get; set; }
        public decimal Quantity { get; set; }
        public int UOMID { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }

    }
}
