﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOChartOfAccounts
    {
        public int CompanyID { get; set; }
        public int AccountID { get; set; }
        public int ParentID { get; set; }
        public int GLAccountID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public char BSOrPL { get; set; }
        public bool DepartmentWise { get; set; }
        public bool OrderWise { get; set; }
        public bool Predefined { get; set; }
        public decimal OpeningBalance { get; set; }
        public CreditOrDebit CreditOrDebit { get; set;}
        public bool IsCostCenterApplicable { get; set; }
        public decimal AccValue { get; set; }
    }
 
    public enum CreditOrDebit
    {
        Debit = 0,
        Credit = 1
    }
}