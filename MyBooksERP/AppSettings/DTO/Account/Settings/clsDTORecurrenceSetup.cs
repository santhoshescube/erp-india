﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTORecurrenceSetup
    {
        public int intRecurrenceSetupID { get; set; }
        public string strRecurrenceType { get; set; }
        public int intRecurringPatternID { get; set; }

        public int intDayInterval { get; set; }
        public bool blnIsDaysDaily { get; set; }

        public int intWeekInterval { get; set; }
        public bool blnIsSunday { get; set; }
        public bool blnIsMonday { get; set; }
        public bool blnIsTuesday { get; set; }
        public bool blnIsWednesday { get; set; }
        public bool blnIsThursday { get; set; }
        public bool blnIsFriday { get; set; }
        public bool blnIsSaturday { get; set; }

        public bool blnIsDaysMonthly { get; set; }
        public int intDayofMonth { get; set; }
        public int intMonthInterval { get; set; }
        public int intWeekDayOrderID { get; set; }
        public int intDayID { get; set; }
    }
}
