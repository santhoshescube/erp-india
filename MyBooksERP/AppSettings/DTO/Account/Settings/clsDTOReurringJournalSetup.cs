﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlTypes;

namespace MyBooksERP
{
    public class clsDTOReurringJournalSetup
       
	
    {
        /// <summary>
        /// gets or sets the primary key of the JournalRecurrencesetup 
        /// </summary>
        public int RecurringJournalSetupID { get; set; }
        /// <summary>
        /// gets or sets the RecurrenceSetupID 
        /// </summary>
        public int RecurrenceSetupID  { get; set; }
        /// <summary>
        /// gets or sets the VoucherID 
        /// </summary>
        public SqlDecimal VoucherID { get; set; }
        /// <summary>
        /// gets or sets the StartDate
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// gets or sets the NoofOccurance
        /// </summary>
        public int NoofOccurance { get; set; }
        /// <summary>
        /// gets or sets the Conpany Id
        /// </summary>
        public int CompanyId { get; set; }
        /// <summary>
        /// gets or sets the VoucherTypeId
        /// </summary>
        public int VoucherTypeId { get; set; }

        /// <summary>
        /// gets or sets the status
        /// </summary>
        public bool Status { get; set; } 
       
        
    }
   
}
