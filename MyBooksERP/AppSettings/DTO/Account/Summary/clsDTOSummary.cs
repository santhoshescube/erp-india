﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOAccountSummary
    {
        public int intTempMenuID { get; set; }
        public int intTempAccountID { get; set; }
        public int intTempCompanyID { get; set; }
        public DateTime dtpTempFromDate { get; set; }
        public DateTime dtpTempToDate { get; set; }
        public bool IsTempGroup { get; set; }

    }
}
