﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOReceiptsAndPayments
    {
        public long intReceiptAndPaymentID { get; set; }
        public string strPaymentNumber { get; set; }
        public long intSerialNo { get; set; }
        public DateTime dtpPaymentDate { get; set; }
        public int intOperationTypeID { get; set; }
        public int intReceiptAndPaymentTypeID { get; set; }
        public int intTransactionTypeID { get; set; }
        public string strChequeNumber { get; set; }
        public DateTime dtpChequeDate { get; set; }
        public decimal decTotalAmount { get; set; }
        public int intCurrencyID { get; set; }
        public int intCompanyID { get; set; }
        public int intCreatedBy { get; set; }
        public DateTime dtpCreatedDate { get; set; }
        public string strRemarks { get; set; }
        public bool blnIsReceipt { get; set; }
        public bool blnIsDirect { get; set; }
        public int intAccountID { get; set; }
        public int intVendorID { get; set; }
        public long intReferenceID { get; set; }
    }
}
