﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
/* 
=================================================
   Author:		< Sanju >
   Create date: < 11 Nov 2011 >
   Description:	< General Receipts And Payments DTO >
================================================
*/
namespace MyBooksERP
{
    public class clsDTOJournalVoucher
    {
        public SqlDecimal dlVoucherID { get; set; }
        public string strVoucherNo { get; set; }
        public int intVoucherTypeID { get; set; }
        public int intCompanyID { get; set; }
        public DateTime dtpVoucherDate { get; set; }
        public string strChNo { get; set; }
        public DateTime dtpChDate { get; set; }
        public string strRemarks { get; set; }
        public int intJournalTypeID { get; set; }
        public int intOperationTypeID { get; set; }
        public int intReferenceID { get; set; }
        public int intCreatedBy { get; set; }
        public DateTime dtpCreatedDate { get; set; }
        public bool blnIsPDC { get; set; }
        public int intBankAccountID { get; set; }

        public int intAccountID { get; set; }
        public int intSerialNo { get; set; }
        public decimal dlDebitAmount { get; set; }
        public decimal dlCreditAmount { get; set; }
        public string strDetailRemarks { get; set; }
        public int intCostCenterID { get; set; }
        public int intCostCenterAccountID { get; set; }
        public decimal dlAmount { get; set; }
    }
}
