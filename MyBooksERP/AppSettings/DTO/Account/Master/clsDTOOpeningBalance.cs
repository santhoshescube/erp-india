﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    /***********************************************************
    * Author       : Junaid
    * Created On   : 13 APRL 2012
    * Purpose      : Create Opening Balance for Accounts
    *                
    * **********************************************************/
   public  class clsDTOOpeningBalance
    {
        public int CompanyID { get; set; }
        public int AccountID { get; set; }
        public decimal OpeningBalance { get; set; }
        public CrOrDr CrOrDr { get; set; }
        public DateTime BookStartDate { get; set; }


    }
    public enum CrOrDr
    {
        Dr = 0,
        Cr = 0
    }
}
