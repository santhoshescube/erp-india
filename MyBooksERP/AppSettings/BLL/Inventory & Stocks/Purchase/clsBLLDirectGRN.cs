﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsBLLDirectGRN
    {
        private DataLayer MobjDataLayer;
        private clsDALDirectGRN MobjClsDALDirectGRN;

        public clsDTOGRN PobjclsDTOGRN { get; set; }
        public clsBLLDirectGRN()
        {
            MobjDataLayer = new DataLayer();
            MobjClsDALDirectGRN = new clsDALDirectGRN(MobjDataLayer);
            PobjclsDTOGRN = new clsDTOGRN();
            MobjClsDALDirectGRN.PobjClsDTOGRN = PobjclsDTOGRN;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            return MobjClsDALDirectGRN.FillCombos(saFieldValues);
        }

        public int GetVendorRecordID(int iVendorID)     // Retreiving current vendorid for Focussing on the current vendor
        {
            return MobjClsDALDirectGRN.GetVendorRecordID(iVendorID);
        }

        public string DisplayAddressInformation(int iVendorAdd)
        {
            return MobjClsDALDirectGRN.DisplayAddressInformation(iVendorAdd);
        }

        public Int64 GetLastGRNNo(bool blnIsNoGeneration, int intCompanyID)
        {
            return MobjClsDALDirectGRN.GetLastGRNNo(blnIsNoGeneration, intCompanyID);
        }

        public DataTable GetGRNNos()
        {
            return MobjClsDALDirectGRN.GetGRNNos();
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            return MobjClsDALDirectGRN.GetItemUOMs(intItemID);
        }


        public bool SavePurchaseGRN()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALDirectGRN.SavePurchaseGRN();
                if (blnRetValue && MobjClsDALDirectGRN.PobjClsDTOGRN.intStatusID != (int)OperationStatusType.GRNCancelled)
                    blnRetValue = MobjClsDALDirectGRN.DeleteUnwantedBatchDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable DisplayPurchaseGRN(long lngGRNID)
        {
           return MobjClsDALDirectGRN.DisplayPurchaseGRN(lngGRNID);
        }

        public DataTable DisplayGRNDetail(long lngGRNID)
        {
           return MobjClsDALDirectGRN.DisplayGRNDetail(lngGRNID);
        }
        public bool IsGRNNoExists(string strGRNNo, int intCompanyID)
        {
            return MobjClsDALDirectGRN.IsGRNNoExists(strGRNNo, intCompanyID);
        }
        public bool DeleteGRN(long GRNID)
        {
            return MobjClsDALDirectGRN.DeleteGRN(GRNID);
        }
        public DataSet GetGRNReport()
        {
            return MobjClsDALDirectGRN.GetGRNReport();
        }

        public DataTable DisplayCancellationDetails(long lngGRNID)
        {
            return MobjClsDALDirectGRN.DisplayCancellationDetails(lngGRNID);
        }

        public bool IsPurchaseOrderExists()
        {
            return MobjClsDALDirectGRN.IsPurchaseOrderExists();
        }
        public bool IsPurchaseInvoiceExists()
        {
            return MobjClsDALDirectGRN.IsPurchaseInvoiceExists();
        }
        public DataTable GetGRNNos(long lngGRNID,int intType)
        {
            return MobjClsDALDirectGRN.GetGRNNos(lngGRNID, intType);
        }

        public bool IsReferenceExists()
        {
            return MobjClsDALDirectGRN.IsReferenceExists();
        }

        public bool GetExchangeCurrencyRate(int intCompanyID, int intCurrencyID)
        {
            return MobjClsDALDirectGRN.GetExchangeCurrencyRate(intCompanyID, intCurrencyID);
        }

        //  ------------------------ For GRN Report ---------------------------------------

        public DataSet FillCombo(int intCompanyID,int intVendorID,long lngGRNID, int intStatusID)
        {
            return MobjClsDALDirectGRN.FillCombo(intCompanyID, intVendorID, lngGRNID, intStatusID);
        }

        public DataSet GetReport(int intCompanyID, int intVendorID, long lngGRNID, int intStatusID,DateTime? dtFrmDate,DateTime? dtToDate,int intChkFrmTo)
        {
            return MobjClsDALDirectGRN.GetReport(intCompanyID, intVendorID, lngGRNID, intStatusID,dtFrmDate,dtToDate,intChkFrmTo);
        }

        public bool IsInvoiceOrOrderExists()
        {
            return MobjClsDALDirectGRN.IsInvoiceOrOrderExists();
        }
    }
}