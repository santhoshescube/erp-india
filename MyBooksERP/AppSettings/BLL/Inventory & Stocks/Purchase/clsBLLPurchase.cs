﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsBLLPurchase
    {
        private DataLayer MobjDataLayer;
        private clsDALPurchase MobjClsDALPurchase;

        public clsDTOPurchase PobjClsDTOPurchase { get; set; }
        public clsBLLPurchase()
        {
            MobjDataLayer = new DataLayer();
            MobjClsDALPurchase = new clsDALPurchase(MobjDataLayer);
            PobjClsDTOPurchase = new clsDTOPurchase();
            MobjClsDALPurchase.PobjClsDTOPurchase = PobjClsDTOPurchase;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            return MobjClsDALPurchase.FillCombos(saFieldValues);
        }
        //public int GetLastOrderNo(int iType)// iType is 1 for generation of no and 2 for total count
        //{
        //    return MobjClsDALPurchase.GetLastOrderNo(iType);
        //}
        //public int GetLastInvoiceNo(int iType)
        //{
        //    return MobjClsDALPurchase.GetLastInvoiceNo(iType);
        //}
        //public int GetLastGRNNo(int iType)
        //{
        //    return MobjClsDALPurchase.GetLastGRNNo(iType);
        //}
        //public int GetLastReturnNo(int iType)
        //{
        //    return MobjClsDALPurchase.GetLastReturnNo(iType);
        //}



        public bool SavePurchaseOrder(bool AddStatus)// Purchase Order Save
        {
            bool blnRetValue = false;
            DataTable datAlert = null;

            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALPurchase.SavePurchaseOrder(AddStatus);
                if (((OperationStatusType)this.PobjClsDTOPurchase.intStatusID) == OperationStatusType.POrderApproved)
                    datAlert = this.SetPurchaseOrderAlert();
                if (datAlert != null)
                    blnRetValue = MobjClsDALPurchase.SaveAlert(datAlert);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }



        public bool SavePurchaseInvoice(bool blnAddStatus)// Purchase Invoice Save
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALPurchase.SavePurchaseInvoice(blnAddStatus);
                if (blnRetValue && MobjClsDALPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PInvoiceCancelled)
                    blnRetValue = MobjClsDALPurchase.DeleteUnwantedBatchDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }




        public bool SavePurchaseGRN(bool blnAddStatus, bool blnCancelStatus)// GRN Save
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALPurchase.SavePurchaseGRN(blnAddStatus, blnCancelStatus);
                if (blnRetValue && MobjClsDALPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.GRNCancelled)
                    blnRetValue = MobjClsDALPurchase.DeleteUnwantedBatchDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }


        public DataTable GetOrderNumber(int iComID, string sNo) // Get Order Number For searching
        {

            return MobjClsDALPurchase.GetOrderNumber(iComID, sNo);
        }

        public DataTable GetInvoiceNumber(int iComID, string sNo) // Get Invoice Number For searching
        {
            return MobjClsDALPurchase.GetInvoiceNumber(iComID, sNo);
        }
        public DataTable GetGRNNumber(int iComID, string sNo) // Get GRN Number For searching
        {
            return MobjClsDALPurchase.GetGRNNumber(iComID, sNo);
        }


        public DataTable DisplayPurchaseOrderDetail(Int64 Rownum) // Purchase Order detail
        {
            return MobjClsDALPurchase.DisplayPurchaseOrderDetail(Rownum);
        }
        public DataTable DisplayPurchaseInvoiceDetail(Int64 Rownum)// Display Invoice detail
        {
            return MobjClsDALPurchase.DisplayPurchaseInvoiceDetail(Rownum);
        }



        public DataTable DisplayPurchaseGRNDetail(Int64 Rownum)// Display GRN detail
        {
            return MobjClsDALPurchase.DisplayPurchaseGRNDetail(Rownum);
        }

        public bool DisplayPurchaseOrderInfo(Int64 iOrderID) // Purchse Order
        {
            return MobjClsDALPurchase.DisplayPurchaseOrderInfo(iOrderID);
        }


        public bool DisplayPurchaseInvoiceInfo(Int64 iOrderID) // Purchase Invoice
        {
            return MobjClsDALPurchase.DisplayPurchaseInvoiceInfo(iOrderID);
        }

        public bool DisplayStockTransferInfo(Int64 intStockTransferID) // StockTransferMasterDetails
        {
            return MobjClsDALPurchase.DisplayStockTransferInfo(intStockTransferID);
        }

        public DataTable GetStockTransferDetails(Int64 intStockTransferID)
        {
            return MobjClsDALPurchase.GetStockTransferDetails(intStockTransferID);
        }

        public bool DisplayPurchaseGRNInfo(Int64 iOrderID) // GRN Master
        {
            return MobjClsDALPurchase.DisplayPurchaseGRNInfo(iOrderID);
        }

        public string DisplayVendorInformation(int VendorID)
        {
            return MobjClsDALPurchase.DisplayVendorInformation(VendorID);
        }
        public string DisplayAddressInformation(int iVendorAdd)
        {
            return MobjClsDALPurchase.DisplayAddressInformation(iVendorAdd);
        }

        public int GetDefaultUomID(int iItemID)
        {
            return MobjClsDALPurchase.GetDefaultUomID(iItemID);
        }

        public double GetItemDiscountAmount(int iMode, int iItemID, int DiscountID, double Rate)
        {
            return MobjClsDALPurchase.GetItemDiscountAmount(iMode, iItemID, DiscountID, Rate);
        }
        public int GetVendorRecordID(int iVendorID)     // Retreiving current vendorid for Focussing on the current vendor
        {
            return MobjClsDALPurchase.GetVendorRecordID(iVendorID);
        }
        public int GetWarehouseRecordID(int iWarehouse)     // Retreiving current warehouseID for Focussing on the current warehouse
        {
            return MobjClsDALPurchase.GetWarehouseRecordID(iWarehouse);
        }

        public bool DeletePurchaseOrder(Int64 iPurOrderID)
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALPurchase.DeletePurchaseOrder(iPurOrderID);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        //public int GetLastQuotationNo(int iType)
        //{
        //    //Function for Getting LastQuotation No
        //    return MobjClsDALPurchase.GetLastQuotationNo(iType);
        //}

        public bool SavePurchaseQuotation(bool blnAddStatus)
        {
            //Function For Saving Purchase Quotation
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALPurchase.SavePurchaseQuotation(blnAddStatus);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public DataTable DisplayPurchaseIndentDetail(Int64 Rownum)// Display Indent detail
        {
            //Function For Displaying Purchase Indent Details
            return MobjClsDALPurchase.DisplayPurchaseIndentDetail(Rownum);
        }

        public bool DisplayPurchaseIndentInfo(Int64 iOrderID) // Purchse Indent
        {
            //Function For Displaying Purchase Indent Information
            return MobjClsDALPurchase.DisplayPurchaseIndentInfo(iOrderID);
        }

        public DataTable DisplayRFQDetail(Int64 Rownum)// Display RFQ detail
        {
            //Function For Displaying Purchase RFQ Details
            return MobjClsDALPurchase.DisplayRFQDetail(Rownum);
        }

        public bool DisplayRFQInfo(Int64 iOrderID) // Purchse RFQ
        {
            //Function For Displaying Purchase RFQ Information
            return MobjClsDALPurchase.DisplayRFQInfo(iOrderID);
        }

        public bool DeletePurchaseQuotation()
        {
            //Function for Deleting Purchase Quotation
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALPurchase.DeletePurchaseQuotation();
                MobjDataLayer.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable GetPurchaseNos(int intType)
        {
            //Function For Finding Purchase Quotation By Quotation No
            return MobjClsDALPurchase.GetPurchaseNos(intType);
        }

        public DataTable DisplayPurchaseQuotationDetail(Int64 Rownum)
        {
            //Function For Displaying Purchase Quotation Details
            return MobjClsDALPurchase.DisplayPurchaseQuotationDetail(Rownum);
        }

        public bool DisplayPurchaseQuotationInfo(Int64 iOrderID)
        {
            //Function For Displaying Purchase Quotation Information
            return MobjClsDALPurchase.DisplayPurchaseQuotationInfo(iOrderID);
        }

        // ----------------------------------------Reports------------------------------------------------------------

        public DataTable GetPurIndent()
        {
            return MobjClsDALPurchase.GetPurIndent();
        }
        public DataTable GetPurOrder()
        {
            return MobjClsDALPurchase.GetPurOrder();
        }
        public DataTable GetPurOrderDetail(Int64 iPurOrderID)
        {
            return MobjClsDALPurchase.GetPurOrderDetail(iPurOrderID);
        }
        public decimal GetReceivedQty(int intItemID, Int64 intReferenceID, bool blnIsGRN)
        {
            return MobjClsDALPurchase.GetReceivedQty(intItemID, intReferenceID, blnIsGRN);
        }
        //public decimal GetOrderedQty(int intItemID, Int64 intReferenceID)
        //{
        //    return MobjClsDALPurchase.GetOrderedQty(intItemID, intReferenceID);
        //}
        public bool DeleteGRN(Int64 intGRNID)
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALPurchase.DeleteGRN(intGRNID);
                if (blnRetValue)
                    blnRetValue = MobjClsDALPurchase.DeleteUnwantedBatchDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public bool DeleteInvoice(Int64 intInvoiceID)
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALPurchase.DeleteInvoice(intInvoiceID);
                if (blnRetValue)
                    blnRetValue = MobjClsDALPurchase.DeleteUnwantedBatchDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public string GetLastBatchNo(int intItemID, Int64 intReferenceID, bool blnIsGRN)
        {
            return MobjClsDALPurchase.GetLastBatchNo(intItemID, intReferenceID, blnIsGRN);
        }
        public DataTable GetItemUOMs(int intItemID)
        {
            return MobjClsDALPurchase.GetItemUOMs(intItemID);
        }
        public DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {
            return MobjClsDALPurchase.GetUomConversionValues(intUOMID, intItemID);
        }
        public bool IsPurchaseNoExists(string strPurchaseNo, int intType, int intCompanyID)
        {
            return MobjClsDALPurchase.IsPurchaseNoExists(strPurchaseNo, intType, intCompanyID);
        }
        public Int64 GetLastPurchaseNo(int intFormType, bool blnIsNoGeneration, int intCompanyID)
        {
            return MobjClsDALPurchase.GetLastPurchaseNo(intFormType, blnIsNoGeneration, intCompanyID);
        }

        public string StockValidation(bool blnIsDeletion, bool blnIsGRN, out bool blnIsSalesDone)
        {
            return MobjClsDALPurchase.StockValidation(blnIsDeletion, blnIsGRN, out blnIsSalesDone);
        }

        public DataTable GetItemDiscountDetails(int intItemID, decimal decQty, decimal decRate, string strCurrentDate, int intCurrencyID)
        {
            return MobjClsDALPurchase.GetItemWiseDiscountDetails(intItemID, decQty, decRate, strCurrentDate, intCurrencyID);
        }

        public bool IsDiscountForAmount(int intDiscountID)
        {
            return MobjClsDALPurchase.IsDiscountForAmount(intDiscountID);
        }

        public string FindEmployeeNameByUserID(int intUserID)
        {
            return MobjClsDALPurchase.FindEmployeeNameByUserID(intUserID);
        }

        public DataSet GetPurchaseReport(int intFormType)
        {
            return MobjClsDALPurchase.GetPurchaseReport(intFormType);
        }

        public DataTable GetPurchaseOrderExpenseDetails(int intPurchaseOrderID)
        {
            return MobjClsDALPurchase.GetPurchaseOrderExpenseDetails(intPurchaseOrderID);
        }

        public bool IsPurchaseBillNoExists(string strPurchaseBillNo, long lngPurchaseInvoiceID)
        {
            return MobjClsDALPurchase.IsPurchaseBillNoExists(strPurchaseBillNo, lngPurchaseInvoiceID);
        }

        public decimal GetAdvancePayment(Int64 intPurchaseOrderID)
        {
            return MobjClsDALPurchase.GetAdvancePayment(intPurchaseOrderID);
        }

        public bool GetCompanyAccount(int intTransactionType)
        {
            return MobjClsDALPurchase.GetCompanyAccount(intTransactionType);
        }
        public Int32 GetCompanyAccountID(int intTransactionType, int CompanyID)
        {
            return MobjClsDALPurchase.GetCompanyAccountID(intTransactionType, CompanyID);
        }

        public bool GetExchangeCurrencyRate(int intCompanyID, int intCurrencyID)
        {
            return MobjClsDALPurchase.GetExchangeCurrencyRate(intCompanyID, intCurrencyID);
        }

        public string GetCompanyCurrency(int intCompanyID, out int intScale)
        {
            return MobjClsDALPurchase.GetCompanyCurrency(intCompanyID, out intScale);
        }

        public bool UpdateTotalAmount(int intFormType)
        {
            return MobjClsDALPurchase.UpdateGrandTotalAmount(intFormType);
        }

        public bool IsStockAdjustmentDone(int intItemID, Int64 intBatchID, int intWarehouseID)
        {
            return MobjClsDALPurchase.IsStockAdjusmentDone(intItemID, intBatchID, intWarehouseID);
        }

        public DataSet PurchaseInvoiceDetailForStockAccount(Int64 Rownum)// Getting Invoice detail
        {
            return MobjClsDALPurchase.PurchaseInvoiceDetailForStockAccount(Rownum);
        }

        public bool UpdateOpStockAccount(int AccountId, int CompanyId, DateTime CurDate, double Amount)
        {
            return MobjClsDALPurchase.UpdateOpStockAccount(AccountId, CompanyId, CurDate, Amount);
        }

        public DataSet GRNDetailForStockAccount(Int64 Rownum)// Getting Invoice detail
        {
            return MobjClsDALPurchase.GRNDetailForStockAccount(Rownum);
        }

        #region Alert Setting Methods

        private DataTable SetPurchaseOrderAlert()
        {
            DataTable datAlert = null, datAlertSetting = null;
            DataTable datAlertRoleSetting = null, datAlertUserSetting = null;

            DataRow[] dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strFilter;

            try
            {
                //3 = Purchase Order
                datAlert = new DataTable();
                datAlert.Columns.Add("AlertID", typeof(long));
                datAlert.Columns.Add("AlertUserSettingID", typeof(long));
                datAlert.Columns.Add("ReferenceID", typeof(long));
                datAlert.Columns.Add("StartDate", typeof(string));
                datAlert.Columns.Add("AlertMessage", typeof(string));
                datAlert.Columns.Add("Status", typeof(int));

                datAlertSetting = this.MobjClsDALPurchase.GetAlertSetting(Convert.ToInt32(AlertSettingsTypes.PurchaseOrderApproved));
                if (datAlertSetting == null || datAlertSetting.Rows.Count == 0)
                    return null;

                DataRow dtSettingRow = datAlertSetting.Rows[0];

                datAlertUserSetting = this.MobjClsDALPurchase.GetAlertUserSetting(Convert.ToInt64(AlertSettingsTypes.PurchaseOrderApproved));
                if (datAlertUserSetting != null && datAlertUserSetting.Rows.Count != 0)
                    dtUserRows = datAlertUserSetting.Select("IsAlertON=true");

                if (dtUserRows == null || dtUserRows.Count() == 0)
                {
                    dtUserRows = null;
                    if (datAlertUserSetting != null)
                    {
                        datAlertUserSetting.Clear();
                        datAlertUserSetting.Dispose();
                        datAlertUserSetting = null;
                    }
                    return null;
                }
                dtUserRows = null;

                datAlertRoleSetting = this.MobjClsDALPurchase.GetAlertRoleSetting(Convert.ToInt64(AlertSettingsTypes.PurchaseOrderApproved));
                if (datAlertRoleSetting == null || datAlertRoleSetting.Rows.Count == 0)
                {
                    if (datAlertRoleSetting != null && datAlertRoleSetting.Rows.Count == 0)
                    {
                        datAlertRoleSetting.Clear();
                        datAlertRoleSetting.Dispose();
                        datAlertRoleSetting = null;
                    }
                    return null;
                }
                //datAlertRoleSetting = GetRoleInHierarchyView(datAlertRoleSetting);
                foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                {
                    strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                    strFilter += " AND IsAlertON = true";
                    dtUserRows = datAlertUserSetting.Select(strFilter);
                    if (dtUserRows.Count() == 0)
                    {
                        dtUserRows = null;
                        continue;
                    }
                    //Purchae order approved. Order no is <PURCHASEORDERNO> and due date is <DUEDATE>.
                    strAlertMsg = dtRoleRow["AlertMessage"].ToString();

                    DataTable datTemp = FillCombos(new string[] { "PO.PurchaseOrderNo,V.VendorName + '-' + V.VendorCode AS VendorName,PO.NetAmount,UM.UserName,PO.DueDate ", "" + 
                                    " InvPurchaseOrderMaster PO INNER JOIN InvVendorInformation V ON PO.VendorID=V.VendorID " +
                                    " Left Join InvVerificationHistory VH On VH.ReferenceID = PO.PurchaseOrderID AND VH.OperationTypeID = " + (int)OperationType.PurchaseOrder + " AND " + 
                                    " VH.StatusID = " + this.PobjClsDTOPurchase.intStatusID + " Left Join UserMaster UM ON UM.UserID = VH.VerifiedBy ", "" +
                                    " PO.PurchaseOrderID=" + this.PobjClsDTOPurchase.intPurchaseID });

                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            strAlertMsg = strAlertMsg.Replace("<PURCHASEORDERNO>", datTemp.Rows[0]["PurchaseOrderNo"].ToString());
                            strAlertMsg = strAlertMsg.Replace("<SUPNAME>", datTemp.Rows[0]["VendorName"].ToString());
                            strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                            strAlertMsg = strAlertMsg.Replace("<DONEBY>", datTemp.Rows[0]["UserName"].ToString());
                            strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                        }
                    }

                    foreach (DataRow dtUserRow in dtUserRows)
                    {
                        dtNewAlertRow = datAlert.NewRow();
                        dtNewAlertRow["AlertID"] = 0;
                        dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                        dtNewAlertRow["ReferenceID"] = this.PobjClsDTOPurchase.intPurchaseID;
                        dtNewAlertRow["StartDate"] = this.PobjClsDTOPurchase.strDueDate;
                        dtNewAlertRow["AlertMessage"] = strAlertMsg;
                        dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                        datAlert.Rows.Add(dtNewAlertRow);

                        dtNewAlertRow = null;
                    }
                    strAlertMsg = "";
                    strFilter = "";
                    dtUserRows = null;
                }
                datAlertRoleSetting.Clear();
                datAlertRoleSetting.Dispose();
                datAlertRoleSetting = null;

                datAlertUserSetting.Clear();
                datAlertUserSetting.Dispose();
                datAlertUserSetting = null;

                datAlert.AcceptChanges();
                return datAlert;
            }
            catch (Exception ex) { throw ex; }
        }

        private DataTable GetRoleInHierarchyView(DataTable datAlertRoleSetting)
        {
            DataRow dtOldRole = null, dtNewRole = null;
            DataTable datNewRole;
            DataRow[] dtRows;
            bool blnChanged = false;

            try
            {
                datNewRole = datAlertRoleSetting.Clone();
                datNewRole.Rows.Clear();

                while (datAlertRoleSetting.Rows.Count > 0)
                {
                    if (dtOldRole != null)
                    {
                        datAlertRoleSetting.Rows.Remove(dtOldRole);
                        datAlertRoleSetting.AcceptChanges();
                        dtOldRole = null;
                    }
                    dtRows = null;
                    dtNewRole = null;
                    datNewRole.AcceptChanges();

                    if (datAlertRoleSetting.Rows.Count == 0)
                        break;

                    dtOldRole = datAlertRoleSetting.Rows[0];
                    dtNewRole = datNewRole.NewRow();
                    foreach (DataColumn datCol in datNewRole.Columns)
                        dtNewRole[datCol.ColumnName] = dtOldRole[datCol.ColumnName];

                    if (datNewRole.Rows.Count == 0)
                    {
                        datNewRole.Rows.Add(dtNewRole);
                        continue;
                    }
                    else if (Convert.ToInt32(dtOldRole["ParentID"]) == 0)
                    {
                        datNewRole.Rows.InsertAt(dtNewRole, 0);
                        blnChanged = true;
                        continue;
                    }
                    dtRows = datNewRole.Select("RoleID = " + dtOldRole["ParentID"].ToString());
                    if (dtRows.Count() > 0)
                    {
                        int iPos = datNewRole.Rows.IndexOf(dtRows[0]) + 1;
                        if (iPos == datNewRole.Rows.Count)
                        {
                            datNewRole.Rows.Add(dtNewRole);
                        }
                        else
                        {
                            datNewRole.Rows.InsertAt(dtNewRole, iPos);
                            blnChanged = true;
                        }
                    }
                    else
                    {
                        dtRows = null;
                        dtRows = datNewRole.Select("ParentID = " + dtOldRole["RoleID"].ToString());
                        if (dtRows.Count() > 0)
                        {
                            blnChanged = true;
                            int iPos = datNewRole.Rows.IndexOf(dtRows[0]);
                            datNewRole.Rows.InsertAt(dtNewRole, iPos);
                        }
                        else
                        {
                            datNewRole.Rows.Add(dtNewRole);
                        }
                    }
                }
                if (blnChanged)
                    datNewRole = GetRoleInHierarchyView(datNewRole);
                return datNewRole;
            }
            catch (Exception ex) { throw ex; }
        }
        //public string IsMandatoryDocumentsEntered(int intOperationTypeID, Int64 intReferenceID, int intCompanyID, int intOpertionTypeIDPurchase)
        //{
        //    return MobjClsDALPurchase.IsMandatoryDocumentsEntered(intOperationTypeID, intReferenceID, intCompanyID,intOpertionTypeIDPurchase);
        //}
        public string IsMandatoryDocumentsEntered(int intOperationTypeID, Int64 intReferenceID, int intCompanyID)
        {
            return MobjClsDALPurchase.IsMandatoryDocumentsEntered(intOperationTypeID, intReferenceID, intCompanyID);
        }
        public string IsMandatoryDocumentExpired(int intOperationTypeID, int intVendorID, int intCompany)
        {
            return MobjClsDALPurchase.IsMandatoryDocumentExpired(intOperationTypeID, intVendorID, intCompany);
        }
        public string IsSupplierDocumentsValid(int intOperationTypeID, int intVendorID, int intCompanyID)
        {
            return MobjClsDALPurchase.IsSupplierDocumentsValid(intOperationTypeID, intVendorID, intCompanyID);
        }
        #endregion

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALPurchase.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetOperationTypeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALPurchase.GetOperationTypeByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALPurchase.GetEmployeeByPermission(intRoleID, intCompanyID, intControlID);
        }

        public bool Suggest(int intOperationTypeID)
        {
            return MobjClsDALPurchase.Suggest(intOperationTypeID);
        }

        public decimal GetItemLocationQuantity(clsDTOPurchaseLocationDetails objLocationDetails, bool blnIsDemoQty)
        {
            return MobjClsDALPurchase.GetItemLocationQuantity(objLocationDetails, blnIsDemoQty);
        }

        public DataTable GetPurchaseLocationDetails(bool blnIsGRN)
        {
            return MobjClsDALPurchase.GetPurchaseLocationDetails(blnIsGRN);
        }
        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            return MobjClsDALPurchase.GetItemDefaultLocation(intItemID, intWarehouseID);
        }

        public bool UpdationVerificationTableRemarks(int intType, Int64 intReferenceID, string strRemarks)
        {
            return MobjClsDALPurchase.UpdationVerificationTableRemarks(intType, intReferenceID, strRemarks);
        }

        public decimal GetAlreadyReturnedQty(long lngStockTransferID, int intItemId, long lngBatchID)
        {
            return MobjClsDALPurchase.GetAlreadyReturnedQty(lngStockTransferID, intItemId, lngBatchID);
        }
        public DataTable DisplayPurchaseOrderGRNDetail(Int64 Rownum)// Display Order detail
        {
            return MobjClsDALPurchase.DisplayPurchaseOrderGRNDetail(Rownum);
        }
        /// <summary>
        /// Gets Suppliers purchase history of an item 
        /// </summary>
        /// <param name="CompanyID"></param>
        /// <param name="SupplierID"></param>
        /// <param name="ItemID"></param>
        /// <returns>datatable</returns>
        public DataTable GetSupplierpurchaseHistory(int CompanyID, int SupplierID, int ItemID)
        {
            return MobjClsDALPurchase.GetSupplierpurchaseHistory(CompanyID, SupplierID, ItemID);
        }
        /// <summary>
        /// Gets FromOrder Datatable in GRN 
        /// </summary>
        /// <returns>datatable</returns>
        public DataTable GetPurchaseOrder(int CompanyID)
        {
            return MobjClsDALPurchase.GetPurchaseOrder(CompanyID);
        }
        /// <summary>
        /// Gets FromOrder Exists in GRN 
        /// </summary>
        /// <returns>datatable</returns>
        public bool PurchaseNoExists(int OrderType, long PurchaseID, int CompanyID)
        {
            return MobjClsDALPurchase.PurchaseNoExists(OrderType, PurchaseID, CompanyID);
        }

        public bool IsValidItem(int ItemID)
        {
            return MobjClsDALPurchase.IsValidItem(ItemID);
        }
    }
}