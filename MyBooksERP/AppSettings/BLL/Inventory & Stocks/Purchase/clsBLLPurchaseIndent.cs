﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;

namespace MyBooksERP
{
    public class clsBLLPurchaseIndent
    {
        private DataLayer MobjDataLayer;//object of datalayer
        private clsDALPurchaseIndent MobjClsDALPurchaseIndent; //object of clsDALPurchaseIndent
        public clsDTOPurchaseIndent PobjClsDTOPurchaseIndent { get; set; }//Property for clsDTOPurchaseIndent

        public clsBLLPurchaseIndent()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjClsDALPurchaseIndent = new clsDALPurchaseIndent(MobjDataLayer);
            PobjClsDTOPurchaseIndent = new clsDTOPurchaseIndent();
            MobjClsDALPurchaseIndent.PobjClsDTOPurchaseIndent = PobjClsDTOPurchaseIndent;
        }

        public Int64 GetLastIndentNo(int iType,int intCompanyID)
        {
            //Function for Getting LastIndent No
            return MobjClsDALPurchaseIndent.GetLastIndentNo(iType,intCompanyID);
        }

        public bool SavePurchaseIndent(bool blnAddStatus)
        {
            //Function For Saving Purchase Indent
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALPurchaseIndent.SavePurchaseIndent(blnAddStatus);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeletePurchaseIndent()
        {
            //Function for Deleting Purchase Indent
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALPurchaseIndent.DeletePurchaseIndent();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable GetIndentNumbers()// Get Indent Number For searching
        {
            //Function For Finding Purchase Indent By Indent No
            return MobjClsDALPurchaseIndent.GetIndentNumbers();
        }

        public DataTable DisplayPurchaseIndentDetail(Int64 Rownum)// Display Indent detail
        {
            //Function For Displaying Purchase Indent Details
            return MobjClsDALPurchaseIndent.DisplayPurchaseIndentDetail(Rownum);
        }

        public bool DisplayPurchaseIndentInfo(Int64 iOrderID) // Purchse Indent
        {
            //Function For Displaying Purchase Indent Information
            return MobjClsDALPurchaseIndent.DisplayPurchaseIndentInfo(iOrderID);
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            //Function For Getting DataTable For Filling Combo
            return MobjClsDALPurchaseIndent.FillCombos(saFieldValues);
        }

        public DataTable GetCompanySettings(int ComID) // for settings according to company
        {
            // Function For Getting Company Settings
            return MobjClsDALPurchaseIndent.GetCompanySettings(ComID);
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            return MobjClsDALPurchaseIndent.GetItemUOMs(intItemID);
        }

        public bool IsPurchaseIndentNoExists(string strPurchaseIndentNo,int intCompanyID)
        {
            return MobjClsDALPurchaseIndent.IsPurchaseIndentNoExists(strPurchaseIndentNo,intCompanyID);
        }

        public string FindEmployeeNameByUserID(int intUserID)
        {
            return MobjClsDALPurchaseIndent.FindEmployeeNameByUserID(intUserID);
        }

        public DataSet GetPurchaseIndentReport()
        {
            return MobjClsDALPurchaseIndent.GetPurchaseIndentReport();
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALPurchaseIndent.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALPurchaseIndent.GetEmployeeByPermission(intRoleID, intCompanyID, intControlID);
        }
    }
}
