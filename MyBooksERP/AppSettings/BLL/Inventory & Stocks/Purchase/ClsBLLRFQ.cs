﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;

namespace MyBooksERP
{
    public class ClsBLLRFQ
    {
        private DataLayer MobjDataLayer;
        private ClsDALRFQ  MobjClsDALRFQ;

        public ClsDTORFQ PobjClsDTORFQ { get; set; }
        public ClsBLLRFQ()
        {
            MobjDataLayer = new DataLayer();
            MobjClsDALRFQ = new ClsDALRFQ(MobjDataLayer);
            PobjClsDTORFQ = new ClsDTORFQ();
            MobjClsDALRFQ.PobjClsDTORFQ = PobjClsDTORFQ;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            return MobjClsDALRFQ.FillCombos(saFieldValues);
        }
        public bool SaveRFQ(bool AddStatus)// Purchase Order Save
        {
            bool blnRetValue = false;
       

            try
            {
                MobjDataLayer.BeginTransaction();
                this.MobjClsDALRFQ.PobjClsDTORFQ = this.PobjClsDTORFQ;
                blnRetValue = MobjClsDALRFQ.SaveRFQ(AddStatus);               
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public DataTable GetCompanySettings(int ComID) // for settings according to company
        {
            return MobjClsDALRFQ.GetCompanySettings(ComID);
        }
        public DataTable DisplayRFQDetails() // Purchase Order detail
        {
            this.MobjClsDALRFQ.PobjClsDTORFQ = this.PobjClsDTORFQ;
            return MobjClsDALRFQ.DisplayRFQDetails();
        }
        public bool DisplayRFQInfo(Int64 iOrderID) // Purchse Order
        {
            this.MobjClsDALRFQ.PobjClsDTORFQ = this.PobjClsDTORFQ;
            return MobjClsDALRFQ.DisplayRFQInfo(iOrderID);
        }
        public int GetDefaultUomID(int iItemID)
        {
            return MobjClsDALRFQ.GetDefaultUomID(iItemID);
        }
        public bool DeleteRFQ()
        {
            //Function for Deleting Purchase Quotation
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                this.MobjClsDALRFQ.PobjClsDTORFQ = this.PobjClsDTORFQ;
                blnRetValue = MobjClsDALRFQ.DeleteRFQ();
                MobjDataLayer.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public DataTable GetRFQNos()
        {
            //Function For Finding Purchase Quotation By Quotation No
            return MobjClsDALRFQ.GetRFQNos();
        }
        public DataTable GetItemUOMs(int intItemID)
        {
            return MobjClsDALRFQ.GetItemUOMs(intItemID);
        }
        public bool IsRFQNoExists(string strRFQNo,int intCompanyID)
        {
            return MobjClsDALRFQ.IsRFQNoExists(strRFQNo,intCompanyID);
        }
        public Int64 GetNextRFQNo(int intCompanyId)
        {
            this.MobjClsDALRFQ.PobjClsDTORFQ = this.PobjClsDTORFQ;
            return MobjClsDALRFQ.GetNextRFQNo(intCompanyId);
        }
        public string GetTotalRFQCount()
        {
            return this.MobjClsDALRFQ.GetTotalRFQCount();
        }

        public DataSet GetRFQReport() // Email for RFQ
        {
            return MobjClsDALRFQ.GetRFQReport();
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALRFQ.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALRFQ.GetEmployeeByPermission(intRoleID, intCompanyID, intControlID);
        }

        public bool Suggest(int intOperationTypeID)
        {
            return MobjClsDALRFQ.Suggest(intOperationTypeID);
        }
        public bool UpdationVerificationTableRemarks(Int64 intReferenceID, string strRemarks)
        {
            return MobjClsDALRFQ.UpdationVerificationTableRemarks(intReferenceID, strRemarks);
        }
    }
}
