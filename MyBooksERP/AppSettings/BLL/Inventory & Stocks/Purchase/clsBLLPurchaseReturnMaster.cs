﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;

namespace MyBooksERP
{
    public class clsBLLPurchaseReturnMaster
    {
        private DataLayer MobjDataLayer;//object of datalayer
        private clsDALPurchaseReturnMaster MobjClsDALPurchaseReturnMaster; //object of clsDALPurchaseReturnMaster

        public clsDTOPurchaseReturnMaster PobjClsDTOPurchaseReturnMaster { get; set; }//Property for clsDTOPurchaseReturnMaster

        public clsBLLPurchaseReturnMaster()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjClsDALPurchaseReturnMaster = new clsDALPurchaseReturnMaster(MobjDataLayer);
            PobjClsDTOPurchaseReturnMaster = new clsDTOPurchaseReturnMaster();
            MobjClsDALPurchaseReturnMaster.objclsDTOPurchaseReturnMaster = PobjClsDTOPurchaseReturnMaster;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return MobjClsDALPurchaseReturnMaster.FillCombos(saFieldValues);
        }

        public int GetLastReturnNo(int intCompanyID)
        {
            return MobjClsDALPurchaseReturnMaster.GetLastReturnNo(intCompanyID);
        }


        public DataTable GetReturnNumbers()
        {   // Get Return Number For searching
            return MobjClsDALPurchaseReturnMaster.GetReturnNumbers();
        }

        public bool GetPurchaseReturnMasterDetails(long lngPurchaseReturnID)
        {   //Purchase Return
            return MobjClsDALPurchaseReturnMaster.GetPurchaseReturnMasterDetails(lngPurchaseReturnID);
        }

        public DataTable GetReturnDetDetails(long lngPurchaseReturnID)
        {   // Display Return detail
            return MobjClsDALPurchaseReturnMaster.GetReturnDetDetails(lngPurchaseReturnID);
        }

        public bool SavePurchaseReturnMaster()
        {
            bool blnRetValue = false;
            MobjClsDALPurchaseReturnMaster.objclsDTOPurchaseReturnMaster = PobjClsDTOPurchaseReturnMaster;
            long intPurchaseReturnID = MobjClsDALPurchaseReturnMaster.objclsDTOPurchaseReturnMaster.intPurchaseReturnID;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALPurchaseReturnMaster.SavePurchaseReturnMaster();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                blnRetValue = false;
                MobjDataLayer.RollbackTransaction();
                MobjClsDALPurchaseReturnMaster.objclsDTOPurchaseReturnMaster.intPurchaseReturnID = intPurchaseReturnID;
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeletePurchaseReturnMaster()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALPurchaseReturnMaster.DeletePurchaseReturnMaster();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                blnRetValue = false;
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable GetPurchaseInvoiceItemDetails(int intPurchaseInvoiceID)
        {
            return MobjClsDALPurchaseReturnMaster.GetPurchaseInvoiceItemDetails(intPurchaseInvoiceID);
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            return MobjClsDALPurchaseReturnMaster.GetItemUOMs(intItemID);
        }
        public DataTable GetUomConversionValues(int intUOMID, Int64 intItemID)
        {
            return MobjClsDALPurchaseReturnMaster.GetUomConversionValues(intUOMID, intItemID);
        }
        public string GetCompanyCurrency(int intCompanyID, out int intScale)
        {
            return MobjClsDALPurchaseReturnMaster.GetCompanyCurrency(intCompanyID, out intScale);
        }
        //public void GetItemDiscountAmount(int intMode, int intItemID, int intDiscountID, int intVendorID)
        //{
        //    MobjClsDALPurchaseReturnMaster.GetItemDiscountAmount(intMode, intItemID, intDiscountID, intVendorID);
        //}
        public decimal GetExchangeCurrencyRate(int intCompanyID, int intCurrencyID)
        {
            return MobjClsDALPurchaseReturnMaster.GetExchangeCurrencyRate(intCompanyID, intCurrencyID);
        }
        public double GetItemDiscountAmount(int iMode, int iItemID, int DiscountID, double Rate)
        {
            return MobjClsDALPurchaseReturnMaster.GetItemDiscountAmount(iMode, iItemID, DiscountID, Rate);
        }

        public string GetVendorAddress(int intReferenceID,bool blnIsVendorID,out string strAddressName)
        {
            return MobjClsDALPurchaseReturnMaster.GetVendorAddress(intReferenceID,blnIsVendorID,out strAddressName);
        }

        public DataSet  GetPurchaseReturnReport()  //email
        {
            return MobjClsDALPurchaseReturnMaster.GetPurchaseReturnReport();
        }

        public bool IsDiscountForAmount(int intDiscountID)
        {
            return MobjClsDALPurchaseReturnMaster.IsDiscountForAmount(intDiscountID);
        }


        public DataTable GetPurchaseReturnLocationDetails()
        {
            return MobjClsDALPurchaseReturnMaster.GetPurchaseReturnLocationDetails();
        }

        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            return MobjClsDALPurchaseReturnMaster.GetItemDefaultLocation(intItemID,intWarehouseID);
        }
        public DataSet DisplayDebitNoteLocationReport()
        {
            return MobjClsDALPurchaseReturnMaster.DisplayDebitNoteLocationReport();
        }
        public decimal GetItemLocationQuantity(clsDTOPurchaseReturnLocationDetails objLocationDetails)
        {
            return MobjClsDALPurchaseReturnMaster.GetItemLocationQuantity(objLocationDetails);
        }

        public decimal GetStockQuantity(int intItemID, Int64 intBatchID, int intCompanyID, int intWarehouseID)
        {
            return MobjClsDALPurchaseReturnMaster.GetStockQuantity(intItemID, intBatchID, intCompanyID, intWarehouseID);
        }

        public decimal GetInvoiceReceivedQty(long lngPurchaseInvoiceID, int intItemId, long lngBatchID)
        {
            return MobjClsDALPurchaseReturnMaster.GetInvoiceReceivedQty(lngPurchaseInvoiceID, intItemId, lngBatchID);
        }
        public DataSet GetDebitNoteReport()
        {
            return MobjClsDALPurchaseReturnMaster.GetDebitNoteReport();
        }
        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALPurchaseReturnMaster.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
        }
        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALPurchaseReturnMaster.GetEmployeeByPermission(intRoleID, intCompanyID, intControlID);
        }

        public string GetAlreadyReturnedQty(long lngPurchaseInvoiceID, int intReferenceSerialNo)
        {
            return MobjClsDALPurchaseReturnMaster.GetAlreadyReturnedQty(lngPurchaseInvoiceID, intReferenceSerialNo);
        }
        public bool GetCompanyAccount(int intTransactionType, int intCompanyID)
        {
            return MobjClsDALPurchaseReturnMaster.GetCompanyAccount(intTransactionType, intCompanyID);
        }
        public DataTable GetPurchaseInvoiceNo(int CompanyID,int VendorID)
        {
            return MobjClsDALPurchaseReturnMaster.GetPurchaseInvoiceNo(CompanyID, VendorID);
        }
        public bool IsPurchaseReturnExists(int intCompanyID, string strPurchaseReturnNo)
        {
            return MobjClsDALPurchaseReturnMaster.IsPurchaseReturnExists(intCompanyID, strPurchaseReturnNo);      
        }

        public string GetReturnedQty(long lngPurchaseInvoiceID, long lngBatchID,int intRefernceSerialNo)
        {
            return MobjClsDALPurchaseReturnMaster.GetReturnedQty(lngPurchaseInvoiceID, lngBatchID, intRefernceSerialNo);
        }

        public bool IsReturnExists(long lngPurchaseInvoiceID,long lngPurchaseReturnID)
        {
            return MobjClsDALPurchaseReturnMaster.IsReturnExists(lngPurchaseInvoiceID, lngPurchaseReturnID);
        }

        public decimal GetDiscountAmount(long lngPurchaseInvoiceID, int intItemID, long lngBatchID)
        {
            return MobjClsDALPurchaseReturnMaster.GetDiscountAmount(lngPurchaseInvoiceID, intItemID, lngBatchID);
        }

        public decimal getTaxValue(int TaxScheme)
        {
            return MobjClsDALPurchaseReturnMaster.getTaxValue(TaxScheme);
        }
    }
}
