﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;

namespace MyBooksERP
{
    public class clsBLLDirectDelivery
    {
        private DataLayer MobjDataLayer;                                  //object of datalayer
        private clsDALDirectDelivery MobjClsDALDirectDelivery;            //object of clsDALItemIssueMaster
        public clsDTODirectDeliveryMaster PobjClsDTODeliveryMaster { get; set; }//Property for clsDTOScheduleMaster

        public clsBLLDirectDelivery()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjClsDALDirectDelivery = new clsDALDirectDelivery(MobjDataLayer);
            PobjClsDTODeliveryMaster = new clsDTODirectDeliveryMaster();
            MobjClsDALDirectDelivery.PobjDTODeliveryMaster = PobjClsDTODeliveryMaster;
        }

        public bool SaveItemIssue()
        {
            // function for saving Item Issue
            bool blnRetValue = false;
            Int64 intItemIssueID = PobjClsDTODeliveryMaster.intDirectDeliveryID;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALDirectDelivery.SaveItemIssueMaster();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                PobjClsDTODeliveryMaster.intDirectDeliveryID = intItemIssueID;
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeleteItemIssue()
        {
            // function for deleting ItemIssue
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALDirectDelivery.DeleteItemIssue();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DisplayItemIssuenfo()
        {
            //function for displaying Item Issue information
            return MobjClsDALDirectDelivery.DisplayItemIssueMasterInfo();
        }

        public int GetNextIssueNo(int intCompanyID,int intFormType)
        {
            //function for getting next issue no
            return MobjClsDALDirectDelivery.GetNextIssueNo(intCompanyID,intFormType);
        }

        public DataTable GetItemIssueDetails()
        {
            //function for getting delivery schedule details
            return MobjClsDALDirectDelivery.GetItemIssueDetails();
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            return MobjClsDALDirectDelivery.FillCombos(saFieldValues);
        }

        public DataTable GetIssueNos(string strCondition)
        {
            // function for getting all schedule nos find by condition
            return MobjClsDALDirectDelivery.GetIssueNos(strCondition);
        }

        public string FindEmployeeNameByUserID(int intUserID)
        {
            // function for finding user name by userId
            return MobjClsDALDirectDelivery.FindEmployeeNameByUserID(intUserID);
        }

        public DataTable GetDataForItemSelection(int intCompanyID,int intWareHouseID)
        {
            // function for getting datatable for innergrid selection
            return MobjClsDALDirectDelivery.GetDataForItemSelection(intCompanyID,intWareHouseID);
        }

        public DataTable GetItemUoms(int intItemID)
        {
            // function for getting item uoms
            return MobjClsDALDirectDelivery.GetItemUOMs(intItemID);
        }

        public decimal GetItemLocationQty(clsDTOItemLocationDetails objItemLocationDetails, int intWarehouseID,bool blnIsSum)
        {
            return MobjClsDALDirectDelivery.GetItemLocationQty(objItemLocationDetails, intWarehouseID,blnIsSum);
        }

        public DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {
            return MobjClsDALDirectDelivery.GetUomConversionValues(intUOMID, intItemID);
        }

        public decimal GetStockQuantity(int intItemID, Int64 intBatchID, int intCompanyID, int intWarehouseID)
        {
            return MobjClsDALDirectDelivery.GetStockQuantity(intItemID, intBatchID, intCompanyID,intWarehouseID);
        }
        public decimal GetAvailableQty(int intItemID, int WareHouseID)
        {
            return MobjClsDALDirectDelivery.GetAvailableQty(intItemID, WareHouseID);
        }

        public DataSet GetDeliveryNoteReport()
        {
            return MobjClsDALDirectDelivery.GetDeliveryNoteReport();
        }

        public DataTable GetItemDetails(int intOperationTypeID, Int64 intReferenceID)
        {
            return MobjClsDALDirectDelivery.GetItemDetails(intOperationTypeID, intReferenceID);
        }

        public DataTable GetItemGroupIssueDetails()
        {
            return MobjClsDALDirectDelivery.GetItemGroupIssueDetails();
        }

        public DataTable GetItemGroupDetails(int intItemGroupID)
        {
            return MobjClsDALDirectDelivery.GetItemGroupDetails(intItemGroupID);
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALDirectDelivery.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetOperationTypeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALDirectDelivery.GetOperationTypeByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALDirectDelivery.GetEmployeeByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetEmployeeByPermissionWithDept(int RoleID, int MenuID, int ControlID, int CompanyID, int Dept)
        {
            return MobjClsDALDirectDelivery.GetEmployeeByPermissionWithDept(RoleID, MenuID, ControlID, CompanyID, Dept);
        }

        public DataTable GetEmployeeByPermissionWithDeptWithoutCompany(int RoleID, int MenuID, int ControlID, int Dept)
        {
            return MobjClsDALDirectDelivery.GetEmployeeByPermissionWithDeptWithoutCompany(RoleID, MenuID, ControlID, Dept);
        }

        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            return MobjClsDALDirectDelivery.GetItemDefaultLocation(intItemID, intWarehouseID);
        }

        public decimal GetItemLocationQuantity(clsDTODirectDeliveryLocationDetails objLocationDetails)
        {
            return MobjClsDALDirectDelivery.GetItemLocationQuantity(objLocationDetails);
        }

        public DataTable GetItemIssueLocationDetails()
        {
            return MobjClsDALDirectDelivery.GetItemIssueLocationDetails();
        }
        public DataSet DisplayItemIssueLocationReport()
        {
            return MobjClsDALDirectDelivery.DisplayItemIssueLocationReport();
        }

        public decimal GetBalanceAmount(long lngSalesInvoiceID)
        {
            return MobjClsDALDirectDelivery.GetBalanceAmount(lngSalesInvoiceID);
        }
        //get address of customer
        public string GetVendorAddressInformation(int intVendorAdd)
        {
            return MobjClsDALDirectDelivery.GetVendorAddressInformation(intVendorAdd);
        }
        public DataTable DtGetAddressName()
        {
            return MobjClsDALDirectDelivery.DtGetAddressName();
        }
        public bool CheckSalesOrderExists()
        {
            return MobjClsDALDirectDelivery.CheckSalesOrderExists();
        }
        public bool CheckSalesInvoiceExists()
        {
            return MobjClsDALDirectDelivery.CheckSalesInvoiceExists();
        }
        public bool IsSalesInvoiceExists()
        {
            return MobjClsDALDirectDelivery.IsSalesInvoiceExists();    
        }

        public bool IsSalesOrderExists()
        {
            return MobjClsDALDirectDelivery.IsSalesOrderExists(); 
        }
        public DataTable DisplayItemIssueMasterInfoForSales()
        {
            return MobjClsDALDirectDelivery.DisplayItemIssueMasterInfoForSales();

        }
        public bool GetExchangeCurrencyRate(int intCompanyID, int intCurrencyID)
        {
            return MobjClsDALDirectDelivery.GetExchangeCurrencyRate(intCompanyID, intCurrencyID);
        }
        public DataTable BatchLessItems(int intItemID, int intWareHouseID)
        {
            return MobjClsDALDirectDelivery.BatchLessItems(intItemID,intWareHouseID);
        }
        public DataTable GetGroupDetails(int intItemID)
        {
            return MobjClsDALDirectDelivery.GetGroupDetails(intItemID);
        }
        public decimal GetStockQuantityForBatchlessItems(int intItemID, int intWarehouseID)
        {
            return MobjClsDALDirectDelivery.GetStockQuantityForBatchlessItems(intItemID, intWarehouseID);
        }
        public decimal GetStockQuantityForGroup(int intItemGroupID,int intWareHouseID)
        {
            return MobjClsDALDirectDelivery.GetStockQuantityForGroup(intItemGroupID,intWareHouseID);
        }
        public DataTable BatchLessItemsEditMode(int intItemID, int intItemIssueID)
        {
            return MobjClsDALDirectDelivery.BatchLessItemsEditMode(intItemID,intItemIssueID);
        }
        public bool DeleteItemIssueDetails()
        {
            return MobjClsDALDirectDelivery.DeleteItemIssueDetails();
        }
          public bool DeleteItemGroupIssueDetails()
        {
            return MobjClsDALDirectDelivery.DeleteItemGroupIssueDetails();
        }
          public DataTable GetQtyInGroup(Int64 IntItemID)
          {
              return MobjClsDALDirectDelivery.GetQtyInGroup(IntItemID);
          }
    }
}
