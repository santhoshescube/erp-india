﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;

namespace MyBooksERP
{
    public class clsBLLItemIssueMaster
    {
        private DataLayer MobjDataLayer;                                  //object of datalayer
        private clsDALItemIssueMaster MobjClsDALItemIssueMaster;            //object of clsDALItemIssueMaster
        public clsDTOItemIssueMaster PobjClsDTOItemIssueMaster { get; set; }//Property for clsDTOScheduleMaster

        public clsBLLItemIssueMaster()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjClsDALItemIssueMaster = new clsDALItemIssueMaster(MobjDataLayer);
            PobjClsDTOItemIssueMaster = new clsDTOItemIssueMaster();
            MobjClsDALItemIssueMaster.PobjDTOItemIssueMaster = PobjClsDTOItemIssueMaster;
        }

        public bool SaveItemIssue()
        {
            // function for saving Item Issue
            bool blnRetValue = false;
            Int64 intItemIssueID = PobjClsDTOItemIssueMaster.intItemIssueID;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALItemIssueMaster.SaveItemIssueMaster();
                //save
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                PobjClsDTOItemIssueMaster.intItemIssueID = intItemIssueID;
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeleteItemIssue()
        {
            // function for deleting ItemIssue i
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALItemIssueMaster.DeleteItemIssue();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DisplayItemIssuenfo()
        {
            //function for displaying Item Issue information
            return MobjClsDALItemIssueMaster.DisplayItemIssueMasterInfo();
        }

        public int GetNextIssueNo(int intCompanyID,int intFormType)
        {
            //function for getting next issue no
            return MobjClsDALItemIssueMaster.GetNextIssueNo(intCompanyID,intFormType);
        }

        public DataTable GetItemIssueDetails()
        {
            //function for getting delivery schedule details
            return MobjClsDALItemIssueMaster.GetItemIssueDetails();
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            return MobjClsDALItemIssueMaster.FillCombos(saFieldValues);
        }

        public DataTable GetIssueNos(string strCondition)
        {
            // function for getting all schedule nos find by condition
            return MobjClsDALItemIssueMaster.GetIssueNos(strCondition);
        }

        public string FindEmployeeNameByUserID(int intUserID)
        {
            // function for finding user name by userId gh
            return MobjClsDALItemIssueMaster.FindEmployeeNameByUserID(intUserID);
        }

        public DataTable GetDataForItemSelection(int intOperationType, Int64 intReferenceID,int intWarehouseID,int intCompanyID)
        {
            // function for getting datatable for innergrid selection
            return MobjClsDALItemIssueMaster.GetDataForItemSelection(intOperationType, intReferenceID,intWarehouseID,intCompanyID);
        }

        public DataTable GetItemUoms(int intItemID)
        {
            // function for getting item uoms
            return MobjClsDALItemIssueMaster.GetItemUOMs(intItemID);
        }

        public decimal GetItemLocationQty(clsDTOItemLocationDetails objItemLocationDetails, int intWarehouseID,bool blnIsSum)
        {
            return MobjClsDALItemIssueMaster.GetItemLocationQty(objItemLocationDetails, intWarehouseID,blnIsSum);
        }

        public DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {
            return MobjClsDALItemIssueMaster.GetUomConversionValues(intUOMID, intItemID);
        }

        public decimal GetStockQuantity(int intItemID, Int64 intBatchID, int intCompanyID, int intWarehouseID)
        {
            return MobjClsDALItemIssueMaster.GetStockQuantity(intItemID, intBatchID, intCompanyID,intWarehouseID);
        }

        public DataSet GetDeliveryNoteReport()
        {
            return MobjClsDALItemIssueMaster.GetDeliveryNoteReport();
        }

        public DataTable GetItemDetails(int intOperationTypeID, Int64 intReferenceID)
        {
            return MobjClsDALItemIssueMaster.GetItemDetails(intOperationTypeID, intReferenceID);
        }

        public DataTable GetItemGroupIssueDetails()
        {
            return MobjClsDALItemIssueMaster.GetItemGroupIssueDetails();
        }

        public DataTable GetItemGroupDetails(int intItemGroupID)
        {
            return MobjClsDALItemIssueMaster.GetItemGroupDetails(intItemGroupID);
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALItemIssueMaster.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetOperationTypeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALItemIssueMaster.GetOperationTypeByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALItemIssueMaster.GetEmployeeByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetEmployeeByPermissionWithDept(int RoleID, int MenuID, int ControlID, int CompanyID, int Dept)
        {
            return MobjClsDALItemIssueMaster.GetEmployeeByPermissionWithDept(RoleID, MenuID, ControlID, CompanyID, Dept);
        }

        public DataTable GetEmployeeByPermissionWithDeptWithoutCompany(int RoleID, int MenuID, int ControlID, int Dept)
        {
            return MobjClsDALItemIssueMaster.GetEmployeeByPermissionWithDeptWithoutCompany(RoleID, MenuID, ControlID, Dept);
        }

        public DataRow GetItemDefaultLocation(int intItemID, int intWarehouseID)
        {
            return MobjClsDALItemIssueMaster.GetItemDefaultLocation(intItemID, intWarehouseID);
        }

        public decimal GetItemLocationQuantity(clsDTOItemIssueLocationDetails objLocationDetails)
        {
            return MobjClsDALItemIssueMaster.GetItemLocationQuantity(objLocationDetails);
        }

        public DataTable GetItemIssueLocationDetails()
        {
            return MobjClsDALItemIssueMaster.GetItemIssueLocationDetails();
        }
        public DataSet DisplayItemIssueLocationReport()
        {
            return MobjClsDALItemIssueMaster.DisplayItemIssueLocationReport();
        }

        public decimal GetBalanceAmount(long lngSalesInvoiceID)
        {
            return MobjClsDALItemIssueMaster.GetBalanceAmount(lngSalesInvoiceID);
        }
        public DataTable GetSalesInvoiceNos(int CompanyID)
        {

            return MobjClsDALItemIssueMaster.GetSalesInvoiceNos(CompanyID);
        }
        public DataTable GetSalesOrderNos(int CompanyID)
        {

            return MobjClsDALItemIssueMaster.GetSalesOrderNos(CompanyID);
        }
        public decimal GetDeliveredQty(int intItemID, Int64 intReferenceID, int OrderTypeID)
        {
            return MobjClsDALItemIssueMaster.GetDeliveredQty(intItemID, intReferenceID, OrderTypeID);
        }
        public decimal GetDeliveredQtyNonBatchItems(int intItemID,int intItemIssueID)
        {
            return MobjClsDALItemIssueMaster.GetDeliveredQtyNonBatchItems(intItemID, intItemIssueID);
        }
        public DataTable BatchLessItems(int intItemID, int intWareHouseID)
        {
            return MobjClsDALItemIssueMaster.BatchLessItems(intItemID, intWareHouseID);
        }
        public DataTable GetGroupDetails(int intItemID)
        {
            return MobjClsDALItemIssueMaster.GetGroupDetails(intItemID);
        }
        public decimal GetStockQuantityForBatchlessItems(int intItemID, int intWarehouseID)
        {
            return MobjClsDALItemIssueMaster.GetStockQuantityForBatchlessItems(intItemID, intWarehouseID);
        }
        public decimal GetStockQuantityForGroup(int intItemGroupID,int intWareHouseID)
        {
            return MobjClsDALItemIssueMaster.GetStockQuantityForGroup(intItemGroupID,intWareHouseID);
        }
        public DataTable BatchLessItemsEditMode(int intItemID, int intItemIssueID)
        {
            return MobjClsDALItemIssueMaster.BatchLessItemsEditMode(intItemID, intItemIssueID);
        }
        public bool DeleteItemIssueDetails()
        {
            return MobjClsDALItemIssueMaster.DeleteItemIssueDetails();
        }
        public bool DeleteItemGroupIssueDetails()
        {
            return MobjClsDALItemIssueMaster.DeleteItemGroupIssueDetails();
        }
        public DataTable GetQtyInGroup(Int64 IntItemID)
        {
            return MobjClsDALItemIssueMaster.GetQtyInGroup(IntItemID);
        }
    }
}
