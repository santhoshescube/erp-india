﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;

namespace MyBooksERP
{
    public class clsBLLSTSalesInvoice
    {
        clsDALSTSalesInvoice objclsDALSTSales; 
       clsDTOSTSalesInvoice objclsDTOSTSalesMaster; 
        private DataLayer objClsConnection;

        clsDALMailSettings objclsDALMailSettings;
        public clsDTOMailSetting objclsDTOMailSetting;

        public clsBLLSTSalesInvoice()
        {
            objClsConnection = new DataLayer();
            objclsDALSTSales = new clsDALSTSalesInvoice(objClsConnection);
            objclsDTOSTSalesMaster = new clsDTOSTSalesInvoice();
            objclsDALSTSales.objclsConnection = objClsConnection;
            objclsDALSTSales.objclsDTOSTSalesMaster = objclsDTOSTSalesMaster;

            objclsDALMailSettings = new clsDALMailSettings();
            objclsDALMailSettings.objClsConnection = objClsConnection;
        }

        public clsDTOSTSalesInvoice clsDTOSTSalesMaster   
        {
            get { return objclsDTOSTSalesMaster; }
            set { objclsDTOSTSalesMaster = value; }
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            return objclsDALSTSales.FillCombos(sarFieldValues);
        }

        public Int64 GetLastQuotationNo(ref Int64 intSalesQuotationCount,int intCompanyID)
        {
            return objclsDALSTSales.GetLastQuotationNo(ref intSalesQuotationCount,intCompanyID);
        }

        public Int64 GetLastOrderNo(ref Int64 intSalesOrderCount,int intCompanyId)
        {
            return objclsDALSTSales.GetLastOrderNo(ref intSalesOrderCount,intCompanyId);
        }

        public Int64 GetLastInvoiceNo(ref Int64 intSalesInvoiceCount,int intCompanyID)
        {
            return objclsDALSTSales.GetLastInvoiceNo(ref intSalesInvoiceCount,intCompanyID);
        }

        public DataTable GetQuotationNumbers()
        {
            return objclsDALSTSales.GetQuotationNumbers();
        }

        public DataTable GetOrderNumbers()
        {
            return objclsDALSTSales.GetOrderNumbers();
        }

        public DataTable GetInvoiceNumbers()
        {
            return objclsDALSTSales.GetInvoiceNumbers();
        }

        public bool GetQuotationMasterDetails(Int64 intQuotationID)
        {
            return objclsDALSTSales.GetQuotationMasterDetails(intQuotationID);
        }

        public DataTable GetJobOrderItemDetails(Int64 intJobOrderID)
        {
            return objclsDALSTSales.GetJobOrderItemDetails(intJobOrderID);
        }

        public bool GetOrderMasterDetails(Int64 intOrderID)
        {
            return objclsDALSTSales.GetOrderMasterDetails(intOrderID);
        }

        public bool GetInvoiceMasterDetails(Int64 intInvoiceID)
        {
            return objclsDALSTSales.GetInvoiceMasterDetails(intInvoiceID);
        }

        public string GetVendorInformation(int intVendorID)
        {
            return objclsDALSTSales.GetVendorInformation(intVendorID);
        }

        public string GetVendorAddressInformation(int intVendorAdd)
        {
            return objclsDALSTSales.GetVendorAddressInformation(intVendorAdd);
        }

        public DataTable GetQuotationDetDetails(Int64 intSalesQuotationID)
        {
            return objclsDALSTSales.GetQuotationDetDetails(intSalesQuotationID);
        }

        public DataTable GetSalesInvoiceDetDetails(Int64 intSalesInvoiceID, int IngOrderTypeID)
        {
            return objclsDALSTSales.GetSalesInvoiceDetDetails(intSalesInvoiceID, IngOrderTypeID);
        }

        public DataTable GetOrderDetDetails(Int64 intSalesOrderID,int IntFormType)
        {
            return objclsDALSTSales.GetOrderDetDetails(intSalesOrderID, IntFormType);
        }

        public int GetVendorRecordID(int intVendorID)
        {
            return objclsDALSTSales.GetVendorRecordID(intVendorID);
        }

        public DataTable DtGetAddressName()
        {
            return objclsDALSTSales.DtGetAddressName();
        }

        public void GetItemDiscountAmount(int intMode, int intItemID, int intDiscountID, int intVendorID)
        {
            objclsDALSTSales.GetItemDiscountAmount(intMode, intItemID, intDiscountID, intVendorID);
        }

        public bool UpdationVerificationTableRemarks(Int64 intReferenceID, string strRemarks)
        {
           return  objclsDALSTSales.UpdationVerificationTableRemarks(intReferenceID, strRemarks);
        }
        public DataTable GetItemIssueIDFromSalesInvoice(Int64 intSalesInvoiceID)
        {
            return objclsDALSTSales.GetItemIssueIDFromSalesInvoice(intSalesInvoiceID);
        }

        public bool SaveSalesInvoice(bool blnAddStatus,bool IsReopen)
        {
            bool blnRetValue = false;
            DataTable datalerts = null;

            try
            {
                objClsConnection.BeginTransaction();
                blnRetValue = objclsDALSTSales.SaveSalesInvoice(blnAddStatus, IsReopen);

                if (blnAddStatus == true)
                {
                    //datalerts = this.SetSaleOrdersAlert(AlertSettingsTypes.SaleOrderApproved);
                    //if (datalerts != null && datalerts.Rows.Count > 0)
                    //    this.objclsDALSTSales.SaveAlert(datalerts);
                }
                objClsConnection.CommitTransaction();
            }
            catch (Exception ex)
            {
                objClsConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable GetItemWiseUOMs(int intItemID)
        {
            return objclsDALSTSales.GetItemWiseUOMs(intItemID);
        }

        public DataTable GetItemAndCustomerWiseDiscounts(int intItemID, int intVendorID, decimal decQuantity, decimal decRate, string strCurrentDate,int intCurrencyID)
        {
            return objclsDALSTSales.GetItemAndCustomerWiseDiscounts(intItemID, intVendorID,decQuantity,decRate,strCurrentDate,intCurrencyID);
        }
        public bool IsDocumentsExists(Int64 intSalesOrderID)
        {
            return objclsDALSTSales.IsDocumentsExists(intSalesOrderID);
        }
        public void RejectSalesOrder()
        {
            DataTable datalerts = null;
            try
            {
                objClsConnection.BeginTransaction();
                objclsDALSTSales.RejectSalesOrder();
                //datalerts = this.SetSaleOrdersAlert(AlertSettingsTypes.SaleOrderRejected);
                //if (datalerts != null && datalerts.Rows.Count > 0)
                //    this.objclsDALSTSales.SaveAlert(datalerts);
                objClsConnection.CommitTransaction();
            }
            catch (Exception ex)
            {
                objClsConnection.RollbackTransaction();
                throw ex;
            }
        }

        public bool IsSalesNoExists(string strSalesNo, int intCompanyID)
        {
            return objclsDALSTSales.IsSalesNoExists(strSalesNo, intCompanyID);
        }

       
        public bool CompareAdvancepaymentAndTotAmt(long intSalesOrderID, decimal decGrandTotalAmount)
        {
            return objclsDALSTSales.CompareAdvancepaymentAndTotAmt(intSalesOrderID, decGrandTotalAmount);
        }

        public bool DeleteSalesInvoice()
        {
            bool blnRetValue = false;
            try
            {
                objClsConnection.BeginTransaction();
                blnRetValue = objclsDALSTSales.DeleteSalesInvoice();
                objClsConnection.CommitTransaction();
            }
            catch (Exception ex)
            {
                objClsConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public double GetExpenseAmount()
        {
            return objclsDALSTSales.GetExpenseAmount();
        }

        public bool GetCompanyAccount(int intTransactionType)
        {
            return objclsDALSTSales.GetCompanyAccount(intTransactionType);
        }
        public Int32 GetCompanyAccountID(int intTransactionType, int intCmpnyAcc)
        {
            return objclsDALSTSales.GetCompanyAccountID(intTransactionType,intCmpnyAcc);
        }
       
        public bool GetExchangeCurrencyRate(int intCompanyID, int intCurrencyID)
        {
            return objclsDALSTSales.GetExchangeCurrencyRate(intCompanyID, intCurrencyID);
        }

        /// <summary>
        /// Returns true if sales order number is already exists in the table else false.
        /// </summary>
        /// <returns></returns>
        public bool IsSalesOrderNoExists()
        {
            object objSalesOrderCount = objclsDALSTSales.IsSalesOrderNoExists();

            return Convert.ToInt16(objSalesOrderCount) > 0 ? true : false;
        }

        // For Email
        public DataSet GetSalesQuotationReport()
        {
            return objclsDALSTSales.GetSalesQuotationReport();
        }

        // For Email
        public DataSet GetSalesOrderReport()
        {
            return objclsDALSTSales.GetSalesOrderReport();
        }

        // For Email
        public DataSet GetSalesInvoiceReport()
        {
            return objclsDALSTSales.GetSalesInvoiceReport();
        }

        public void GetMailSetting()
        {
            try
            {
                this.objclsDALMailSettings.GetMailSetting(2);//SMS
                this.objclsDTOMailSetting = this.objclsDALMailSettings.objclsDTOMailSetting;
            }
            catch (Exception ex) { throw ex; }
        }

        public string GetVendorMobileNo(long lngVendorID)
        {
            clsDALVendorInformation objclsDALVendorInformation;

            try
            {
                objclsDALVendorInformation = new clsDALVendorInformation
                {
                    objclsConnection = this.objClsConnection
                };
                return objclsDALVendorInformation.GetVendorMobileNo(lngVendorID);
            }
            catch (Exception ex) { throw ex; }
        }

        public string GetCompanyCurrency(int intCompanyID,out int intScale,out int intCurrencyID)
        {
            return objclsDALSTSales.GetCompanyCurrency(intCompanyID,out intScale,out intCurrencyID);
        }

        public bool UpdateNetAmount()
        {
            return objclsDALSTSales.UpdateNetAmount();
        }

        public DataTable GetDataForItemSelection(int intCompanyID, int intCurrencyID)
        {
            return objclsDALSTSales.GetDataForItemSelection(intCompanyID, intCurrencyID);
        }
        public DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {
            return objclsDALSTSales.GetUomConversionValues(intUOMID, intItemID);
        }

        public string IsMandatoryDocumentsEntered(int intOperationTypeID, int intVendorID, int intCompanyID,int intOpertionTypeIDSales)
        {
            return objclsDALSTSales.IsMandatoryDocumentsEntered(intOperationTypeID, intVendorID, intCompanyID, intOpertionTypeIDSales);
        }
        public string IsMandatoryDocumentExpired(int intOperationTypeID,int intVendorID,int intOpertionTypeIDSales,int intCompany)
        {
            return objclsDALSTSales.IsMandatoryDocumentExpired(intOperationTypeID, intVendorID, intOpertionTypeIDSales, intCompany);
        }
        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return objclsDALSTSales.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
        }
        public DataTable GetDataForSalesOrder(int intCompanyID, long IngSalesOrderID)
        {
            return objclsDALSTSales.GetDataForSalesOrder(intCompanyID, IngSalesOrderID);
        }
        public DataTable GetQuantityAganistSalesInvoice(Int64 IntSalesInvoiceID)
        {
            return objclsDALSTSales.GetQuantityAganistSalesInvoice(IntSalesInvoiceID);
        }
        public DataTable GetOperationTypeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return objclsDALSTSales.GetOperationTypeByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return objclsDALSTSales.GetEmployeeByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetCustomerWiseDiscounts(int intVendorID, decimal decRate, string strCurrentDate,int intCurrencyID)
        {
            return objclsDALSTSales.GetCustomerWiseDiscounts(intVendorID, decRate, strCurrentDate,intCurrencyID);
        }

        public decimal GetSalesInvoiceReservedQty(long lngItemID, long lngBatchID,long lngSalesInvoiceID,int intCompanyID)
        {
            return objclsDALSTSales.GetSalesInvoiceReservedQty(lngItemID, lngBatchID,lngSalesInvoiceID,intCompanyID);
        }

        public decimal GetItemRateForQuotation(long lngItemID, long lngBatchID, long lngQuotationID)
        {
            return objclsDALSTSales.GetItemRateForQuotation(lngItemID, lngBatchID, lngQuotationID);
        }
        public bool CheckWheaterSalesInvoiceExists(Int64 IntSalesInvoiceID)
        {
            return objclsDALSTSales.CheckWheaterSalesInvoiceExists(IntSalesInvoiceID);
        }
        public bool CheckWheaterItemIssueIDExists(Int64 IntItemIssueID)
        {
            return objclsDALSTSales.CheckWheaterItemIssueIDExists(IntItemIssueID);
        }
        public bool CheckWheaterItemIssueExistsInSalesOrder(Int64 IntSalesOrderID)
        {
            return objclsDALSTSales.CheckWheaterItemIssueExistsInSalesOrder(IntSalesOrderID);
        }
        public decimal GetItemRateForSalesOrder(long lngItemID, long lngBatchID, long lngSalesOrderID)
        {
            return objclsDALSTSales.GetItemRateForSalesOrder(lngItemID, lngBatchID, lngSalesOrderID);
        }
        public DataSet FillGridAganistDelivertNote(string strCondition, int intCompanyID)
        {
            return objclsDALSTSales.FillGridAganistDelivertNote(strCondition, intCompanyID);
        }
        public decimal GetItemRateForInvoice(long lngItemID, long lngBatchID, long lngSalesInvoiceID)
        {
            return objclsDALSTSales.GetItemRateForInvoice(lngItemID, lngBatchID, lngSalesInvoiceID);
        }
        public DataTable FillDeliveryNote(Int64 IntCompanyID, Int64 IntVendorID, Int64 OrderTypeID)
        {
            return objclsDALSTSales.FillDeliveryNote(IntCompanyID, IntVendorID, OrderTypeID);
        }
        public DataTable FillDeliveryNoteDetails(Int64 IntCompanyID, Int64 intSalesInvoiceID, Int64 IntVendorID, Int64 OrderTypeID)
        {
            return objclsDALSTSales.FillDeliveryNoteDetails(IntCompanyID, intSalesInvoiceID, IntVendorID, OrderTypeID);
        }
        public decimal GetTotalDiscount(int ItemID, long IntSalesOrderID,bool IsGroup)
        {
            return objclsDALSTSales.GetTotalDiscount(ItemID, IntSalesOrderID,IsGroup);

        }
        public decimal GetOldDiscount(int ItemID, long IntSalesOrderID, bool IsGroup)
        {
            return objclsDALSTSales.GetOldDiscount(ItemID, IntSalesOrderID, IsGroup);
        }
        public decimal GetReceivedDiscount(long IntSalesInvoiceID, int ItemID,bool IsGroup)
        {
            return objclsDALSTSales.GetReceivedDiscount(IntSalesInvoiceID, ItemID,IsGroup); 
        }
        public decimal GetGrandTotalDiscount(long IntSalesOrderID)
        {
            return objclsDALSTSales.GetGrandTotalDiscount(IntSalesOrderID);
        }
        public decimal GetGrandOldDiscount(long IntSalesOrderID)
        {
            return objclsDALSTSales.GetGrandOldDiscount(IntSalesOrderID);
        }
        public decimal GetGrandReceivedDiscount(long IntSalesInvoiceID)
        {
            return objclsDALSTSales.GetGrandReceivedDiscount(IntSalesInvoiceID);
        }
        public DataTable FillDeliveryNoteDetailsFromDN(Int64 intItemIssueID)
        {
            return objclsDALSTSales.FillDeliveryNoteDetailsFromDN(intItemIssueID);
        }

        public decimal GetStockQuantity(int intItemID, Int64 intBatchID, int intCompanyID, string strInvoiceDate)
        {
            return objclsDALSTSales.GetStockQuantity(intItemID, intBatchID, intCompanyID, strInvoiceDate);
        }
        public bool CheckWheaterSalesInvoiceExistsPayMentAndRecipts(Int64 intSalesInvoiceID)
        {
            return objclsDALSTSales.CheckWheaterSalesInvoiceExistsPayMentAndRecipts(intSalesInvoiceID);
        }
        public DataTable DeleteReceiptsReferences(long IntSalesInvoice)
        {
            return objclsDALSTSales.DeleteReceiptsReferences(IntSalesInvoice);
        }

        public bool CheckExpenseExists(long IntSalesOrderID)
        {
            return objclsDALSTSales.CheckExpenseExists(IntSalesOrderID);
        }
        public bool UpdateSalesInvoiceStatusFromOrder(Int64 intSalesOrderID, Int64 IntSalesInvoiceID)
        {
            return objclsDALSTSales.UpdateSalesInvoiceStatusFromOrder(intSalesOrderID, IntSalesInvoiceID);
        }

        /// <summary>
        /// Gets Suppliers purchase history of an item 
        /// </summary>
        /// <param name="CompanyID"></param>
        /// <param name="SupplierID"></param>
        /// <param name="ItemID"></param>
        /// <returns>datatable</returns>
        public DataTable GetCustomerSaleHistory(int CompanyID, int CustomerID, int ItemID)
        {
            return objclsDALSTSales.GetCustomerSaleHistory(CompanyID, CustomerID, ItemID);
        }
        public DataTable GetSaleRates(int ItemID, int CompanyID)
        {
            return objclsDALSTSales.GetSaleRates(ItemID,CompanyID);
        }
        public DataTable GetMinMaxSaleRate(int ItemID, int CompanyID)
        {
            return objclsDALSTSales.GetMinMaxSaleRate(ItemID, CompanyID);
        }
        public int GetPaymentTermsBySalesInvoiceID(long intSalesInvoiceID)
        {
            return objclsDALSTSales.GetPaymentTermsBySalesInvoiceID(intSalesInvoiceID);
        }
        public bool DeleteReceiptsBySalesInvoiceID(long intSalesInvoiceID)
        {
            return objclsDALSTSales.DeleteReceiptsBySalesInvoiceID(intSalesInvoiceID);
        }

        public bool IsSalesOrderExists(long lngItemIssueID)
        {
            return objclsDALSTSales.IsSalesOrderExists(lngItemIssueID);
        }
        public decimal GetStockQuantityForIsGroupItem(Int64 IntItemID)
        {
            return objclsDALSTSales.GetStockQuantityForIsGroupItem(IntItemID);
        }
        public bool CheckWheaterCreditNoteExists(Int64 IntSalesInvoiceID)
        {
            return objclsDALSTSales.CheckWheaterCreditNoteExists(IntSalesInvoiceID);
        }
        public bool CheckWheatherDNAgainstSalesInvoiceExists(string strCondition)
        {
            return objclsDALSTSales.CheckWheatherDNAgainstSalesInvoiceExists(strCondition);
        }
        public decimal GetSalesRateFormDeliveryNote(string strCondition, Int32 IntCompanyID, int ItemID, int BatchID)
        {
            return objclsDALSTSales.GetSalesRateFormDeliveryNote(strCondition, IntCompanyID, ItemID, BatchID);
        }
        public DataTable GetOrderedQtyFromIsGroup(Int64 IntItemID)
        {
            return objclsDALSTSales.GetOrderedQtyFromIsGroup(IntItemID);
        }
        public bool CheckWhetherPurchaseInvoiceExists(int IntCompanyID, int IntCurrencyID, int IntOrderTypeID, int IntItemID, int IntBatchID, decimal DecQuantity, int IntIsGroup)
        {
            return objclsDALSTSales.CheckWhetherPurchaseInvoiceExists( IntCompanyID,  IntCurrencyID,  IntOrderTypeID,  IntItemID,  IntBatchID,  DecQuantity,  IntIsGroup);
        }
       public bool CheckWheatherSalesOrderIDInDeliveryNote(Int64 IntSalesOrderID)
       {
           return objclsDALSTSales.CheckWheatherSalesOrderIDInDeliveryNote(IntSalesOrderID);
       }
       public bool CheckWhetherSOExists(string strCondition)
       {
           return objclsDALSTSales.CheckWhetherSOExists(strCondition);
       }
       public bool CheckSalesInvoiceExists(string strCondition)
       {
           return objclsDALSTSales.CheckSalesInvoiceExists(strCondition);
       }
       public DataTable GetReceiptsReferences(long IntSalesInvoice)
       {
           return objclsDALSTSales.GetReceiptsReferences(IntSalesInvoice);
       }
       public decimal getTaxValue(int TaxScheme)
       {
           return objclsDALSTSales.getTaxValue(TaxScheme);
       }
        #region Alert Setting Methods

        private DataTable SetSaleOrdersAlert(AlertSettingsTypes enmSaleAlert)
        {
            DataTable datAlert = null, datAlertSetting = null;
            DataTable datAlertRoleSetting = null, datAlertUserSetting = null;

            DataRow[] dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strSaleOrderNo = "", strFilter = "";

            try
            {
                //3 = Purchase Order
                datAlert = new DataTable();
                datAlert.Columns.Add("AlertID", typeof(long));
                datAlert.Columns.Add("AlertUserSettingID", typeof(long));
                datAlert.Columns.Add("ReferenceID", typeof(long));
                datAlert.Columns.Add("ReferenceID1", typeof(long));
                datAlert.Columns.Add("StartDate", typeof(string));
                datAlert.Columns.Add("AlertMessage", typeof(string));
                datAlert.Columns.Add("Status", typeof(int));

                datAlertSetting = this.objclsDALSTSales.GetAlertSetting(Convert.ToInt32(enmSaleAlert));
                if (datAlertSetting == null || datAlertSetting.Rows.Count == 0)
                    return null;

                DataRow dtSettingRow = datAlertSetting.Rows[0];

                datAlertUserSetting = this.objclsDALSTSales.GetAlertUserSetting(Convert.ToInt32(enmSaleAlert));
                if (datAlertUserSetting != null && datAlertUserSetting.Rows.Count != 0)
                    dtUserRows = datAlertUserSetting.Select("IsAlertON=true");

                if (dtUserRows == null || dtUserRows.Count() == 0)
                {
                    dtUserRows = null;
                    if (datAlertUserSetting != null)
                    {
                        datAlertUserSetting.Clear();
                        datAlertUserSetting.Dispose();
                        datAlertUserSetting = null;
                    }
                    return null;
                }
                dtUserRows = null;

                datAlertRoleSetting = this.objclsDALSTSales.GetAlertRoleSetting(Convert.ToInt64(enmSaleAlert));
                if (datAlertRoleSetting == null || datAlertRoleSetting.Rows.Count == 0)
                {
                    if (datAlertRoleSetting != null && datAlertRoleSetting.Rows.Count == 0)
                    {
                        datAlertRoleSetting.Clear();
                        datAlertRoleSetting.Dispose();
                        datAlertRoleSetting = null;
                    }
                    return null;
                }
                //datAlertRoleSetting = GetRoleInHierarchyView(datAlertRoleSetting);
                strSaleOrderNo = this.objclsDALSTSales.GetSaleOrderNo(this.objclsDTOSTSalesMaster.intSalesOrderID);
                foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                {
                    strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                    strFilter += " AND IsAlertON = true";
                    dtUserRows = datAlertUserSetting.Select(strFilter);
                    if (dtUserRows.Count() == 0)
                    {
                        dtUserRows = null;
                        continue;
                    }
                    strAlertMsg = dtRoleRow["AlertMessage"].ToString();

                    DataTable datTemp = FillCombos(new string[] { "SO.SalesOrderNo,V.VendorName + '-' + V.VendorCode AS VendorName,SO.NetAmount,UM.UserName,SO.DueDate ", "" + 
                                    " InvSalesOrderMaster SO LEFT JOIN InvSalesQuotationMaster SQ ON SO.ReferenceID = SQ.SalesQuotationID AND SO.OrderTypeID = " +(int)OperationOrderType.SOTFromQuotation + " LEFT JOIN InvVendorInformation V ON SO.VendorID=V.VendorID " +
                                    " Left Join InvVerificationHistory VH On VH.ReferenceID = SO.SalesOrderID AND VH.OperationTypeID = " + (int)OperationType.SalesOrder + " AND " + 
                                    " VH.StatusID = " + objclsDTOSTSalesMaster.intStatusID + " Left Join UserMaster UM ON UM.UserID = VH.VerifiedBy ", "" +
                                    " SO.SalesOrderID=" + objclsDTOSTSalesMaster.intSalesOrderID });

                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            strAlertMsg = strAlertMsg.Replace("<SALEORDERNO>", datTemp.Rows[0]["SalesOrderNo"].ToString());
                            strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                            //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                            strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                            strAlertMsg = strAlertMsg.Replace("<DONEBY>", datTemp.Rows[0]["UserName"].ToString());
                            strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                        }
                    }

                    foreach (DataRow dtUserRow in dtUserRows)
                    {
                        dtNewAlertRow = datAlert.NewRow();
                        dtNewAlertRow["AlertID"] = 0;
                        dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                        dtNewAlertRow["ReferenceID"] = this.objclsDTOSTSalesMaster.intSalesOrderID;
                        dtNewAlertRow["ReferenceID1"] = "0";
                        dtNewAlertRow["StartDate"] = this.objclsDTOSTSalesMaster.strApprovedDate;
                        dtNewAlertRow["AlertMessage"] = strAlertMsg;
                        dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                        datAlert.Rows.Add(dtNewAlertRow);

                        dtNewAlertRow = null;
                    }
                    strAlertMsg = "";
                    dtUserRows = null;
                }
                datAlertRoleSetting.Clear();
                datAlertRoleSetting.Dispose();
                datAlertRoleSetting = null;

                datAlertUserSetting.Clear();
                datAlertUserSetting.Dispose();
                datAlertUserSetting = null;

                datAlert.AcceptChanges();
                return datAlert;
            }
            catch (Exception ex) { throw ex; }
        }
        public decimal GetAdvancePayment(Int64 intSalesOrderID)
        {
            return objclsDALSTSales.GetAdvancePayment(intSalesOrderID);
        }
        private DataTable GetRoleInHierarchyView(DataTable datAlertRoleSetting)
        {
            DataRow dtOldRole = null, dtNewRole = null;
            DataTable datNewRole;
            DataRow[] dtRows;
            bool blnChanged = false;

            try
            {
                datNewRole = datAlertRoleSetting.Clone();
                datNewRole.Rows.Clear();

                DataRow[] dtParentRows = datAlertRoleSetting.Select("ParentId = 0");
                if (dtParentRows.Count() > 0)
                {
                    foreach (DataRow dtPRow in dtParentRows)
                    {
                        dtNewRole = datNewRole.NewRow();
                        foreach (DataColumn datCol in datNewRole.Columns)
                            dtNewRole[datCol.ColumnName] = dtPRow[datCol.ColumnName];
                        datNewRole.Rows.Add(dtNewRole);
                        datAlertRoleSetting.Rows.Remove(dtPRow);
                    }
                    datAlertRoleSetting.AcceptChanges();
                    datNewRole.AcceptChanges();
                }

                while (datAlertRoleSetting.Rows.Count > 0)
                {
                    if (dtOldRole != null)
                    {
                        datAlertRoleSetting.Rows.Remove(dtOldRole);
                        datAlertRoleSetting.AcceptChanges();
                        dtOldRole = null;
                    }
                    dtRows = null;
                    dtNewRole = null;
                    datNewRole.AcceptChanges();

                    if (datAlertRoleSetting.Rows.Count == 0)
                        break;

                    dtOldRole = datAlertRoleSetting.Rows[0];
                    dtNewRole = datNewRole.NewRow();
                    foreach (DataColumn datCol in datNewRole.Columns)
                        dtNewRole[datCol.ColumnName] = dtOldRole[datCol.ColumnName];

                    if (datNewRole.Rows.Count == 0)
                    {
                        datNewRole.Rows.Add(dtNewRole);
                        continue;
                    }
                    else if (Convert.ToInt32(dtOldRole["ParentID"]) == 0)
                    {

                        datNewRole.Rows.InsertAt(dtNewRole, 0);
                        blnChanged = true;
                        continue;
                    }
                    dtRows = datNewRole.Select("RoleID = " + dtOldRole["ParentID"].ToString());
                    if (dtRows.Count() > 0)
                    {
                        int iPos = datNewRole.Rows.IndexOf(dtRows[0]) + 1;
                        if (iPos == datNewRole.Rows.Count)
                        {
                            datNewRole.Rows.Add(dtNewRole);
                        }
                        else
                        {
                            datNewRole.Rows.InsertAt(dtNewRole, iPos);
                            blnChanged = true;
                        }
                    }
                    else
                    {
                        dtRows = null;
                        dtRows = datNewRole.Select("ParentID = " + dtOldRole["RoleID"].ToString());
                        if (dtRows.Count() > 0)
                        {
                            blnChanged = true;
                            int iPos = datNewRole.Rows.IndexOf(dtRows[0]);
                            datNewRole.Rows.InsertAt(dtNewRole, iPos);
                        }
                        else
                        {
                            datNewRole.Rows.Add(dtNewRole);
                        }
                    }
                }
                if (blnChanged)
                    datNewRole = GetRoleInHierarchyView(datNewRole);
                return datNewRole;
            }
            catch (Exception ex) { throw ex; }
        }


        public bool Suggest(int intOperationTypeID)
        {
            return objclsDALSTSales.Suggest(intOperationTypeID);
        }

        #endregion
    }
}