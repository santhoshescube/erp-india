﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;

namespace MyBooksERP
{
    public class clsBLLApproval
    {
        clsDALApproval objclsDALApproval;
        clsDTOApproval objclsDTOApproval;
        private DataLayer objClsConnection;

        public clsBLLApproval()
        {
            objClsConnection = new DataLayer();
            objclsDALApproval = new clsDALApproval();
            objclsDTOApproval = new clsDTOApproval();
            objclsDALApproval.objclsConnection = objClsConnection;
            objclsDALApproval.objclsDTOApproval = objclsDTOApproval;
        }

        public clsDTOApproval clsDTOSTSalesMaster
        {
            get { return objclsDTOApproval; }
            set { objclsDTOApproval = value; }
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            return objclsDALApproval.FillCombos(sarFieldValues);
        }

        public DataTable GetApprovalDetails(int intFormType,string strCondition,int intStatusID)
        {
            return objclsDALApproval.GetApprovalDetails(intFormType,strCondition,intStatusID);
        }

        public DataTable GetCompanyByPermission(int RoleID, int MenuID, int ControlID)
        {
            return objclsDALApproval.GetCompanyByPermission(RoleID, MenuID, ControlID);
        }

        public DataTable GetEmployeeByPermission(int RoleID, int MenuID, int ControlID, int CompanyID)
        {
            return objclsDALApproval.GetEmployeeByPermission(RoleID, MenuID, ControlID, CompanyID);
        }

        public DataTable GetSuggestions(string strCondition)
        {
            return objclsDALApproval.GetSuggestions(strCondition);
        }
    }
}
