﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;

namespace MyBooksERP
{
    //
    public class clsBLLSalesReturnMaster
    {
        private DataLayer MobjDataLayer;//object of datalayer
        private clsDALSalesReturnMaster MobjClsDALSalesReturnMaster; //object of clsDALSalesReturnMaster

        public clsDTOSalesReturnMaster PobjClsDTOSalesReturnMaster { get; set; }//Property for clsDTOSalesReturnMaster

        public clsBLLSalesReturnMaster()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjClsDALSalesReturnMaster = new clsDALSalesReturnMaster(MobjDataLayer);
            PobjClsDTOSalesReturnMaster = new clsDTOSalesReturnMaster();
            MobjClsDALSalesReturnMaster.objclsDTOSalesReturnMaster = PobjClsDTOSalesReturnMaster;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return MobjClsDALSalesReturnMaster.FillCombos(saFieldValues);
        }

        public int GetLastReturnNo(int intType,int intCompanyID)
        {
            return MobjClsDALSalesReturnMaster.GetLastReturnNo(intType,intCompanyID);
        }


        public DataTable GetReturnNumbers(string strReturnNo)
        {   // Get Return Number For searching
            return MobjClsDALSalesReturnMaster.GetReturnNumbers(strReturnNo);
        }

        public bool GetSalesReturnMasterDetails(long intSalesReturnID)
        {   //Sales Return
            return MobjClsDALSalesReturnMaster.GetSalesReturnMasterDetails(intSalesReturnID);
        }

        public DataTable GetReturnDetDetails(long intSalesReturnID)
        {   // Display Return detail
            return MobjClsDALSalesReturnMaster.GetReturnDetDetails(intSalesReturnID);
        }

        public bool SaveSalesReturnMaster()
        {
            bool blnRetValue = false;
            MobjClsDALSalesReturnMaster.objclsDTOSalesReturnMaster = PobjClsDTOSalesReturnMaster;
            long intSalesReturnID = MobjClsDALSalesReturnMaster.objclsDTOSalesReturnMaster.intSalesReturnID;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALSalesReturnMaster.SaveSalesReturnMaster();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                blnRetValue = false;
                MobjDataLayer.RollbackTransaction();
                MobjClsDALSalesReturnMaster.objclsDTOSalesReturnMaster.intSalesReturnID = intSalesReturnID;
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeleteSalesReturnMaster()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALSalesReturnMaster.DeleteSalesReturnMaster();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                blnRetValue = false;
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable GetSalesInvoiceItemDetails(int intSalesInvoiceID)
        {
            return MobjClsDALSalesReturnMaster.GetSalesInvoiceItemDetails(intSalesInvoiceID);
        }

        public DataTable GetItemUOMs(int intItemID)
        {
            return MobjClsDALSalesReturnMaster.GetItemUOMs(intItemID);
        }
        public DataTable GetUomConversionValues(int intUOMID, Int64 intItemID)
        {
            return MobjClsDALSalesReturnMaster.GetUomConversionValues(intUOMID, intItemID);
        }
        public bool IsReturnNoExists(string strReturnNo,int intCompanyID)
        {
            return MobjClsDALSalesReturnMaster.IsReturnNoExists(strReturnNo,intCompanyID);
        }
        public void GetItemDiscountAmount(int intMode, int intItemID, int intDiscountID, int intVendorID)
        {
            MobjClsDALSalesReturnMaster.GetItemDiscountAmount(intMode, intItemID, intDiscountID, intVendorID);
        }

        public string GetVendorAddress(int intReferenceID,bool blnIsVendorID,out string strAddressName)
        {
            return MobjClsDALSalesReturnMaster.GetVendorAddress(intReferenceID,blnIsVendorID,out strAddressName);
        }

        public DataSet  GetSalesReturnReport()  //email
        {
            return MobjClsDALSalesReturnMaster.GetSalesReturnReport();
        }


        public bool GetCompanyAccount(int intTransactionType)
        {
            return MobjClsDALSalesReturnMaster.GetCompanyAccount(intTransactionType);
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALSalesReturnMaster.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALSalesReturnMaster.GetEmployeeByPermission(intRoleID, intCompanyID, intControlID);
        }

        public decimal GetDefaultLocationQuantity(int intItemID, long lngBatchID, int intWarehouseID, bool blnIsDamaged)
        {
            return MobjClsDALSalesReturnMaster.GetDefaultLocationQuantity(intItemID, lngBatchID, intWarehouseID, blnIsDamaged);
        }
        public string StockValidation(bool isDeletion)
        {
            return MobjClsDALSalesReturnMaster.StockValidation(isDeletion);
        }

        public string GetAlreadyReturnedQty(long lngSalesInvoiceID, int intItemID,long lngBatchID,bool blnIsGroup)
        {
            return MobjClsDALSalesReturnMaster.GetAlreadyReturnedQty(lngSalesInvoiceID, intItemID,lngBatchID,blnIsGroup);
        }

        public DataTable GetBatchWiseIssueDetails(long lngSalesInvoiceID, int intItemId, long lngBatchID, bool blnIsGroup)
        {
            return MobjClsDALSalesReturnMaster.GetBatchWiseIssueDetails(lngSalesInvoiceID, intItemId, lngBatchID, blnIsGroup);
        }
        public bool GetCompanyAccount(int intTransactionType, int intCompanyID)
        {
            return MobjClsDALSalesReturnMaster.GetCompanyAccount(intTransactionType, intCompanyID);
        }
        public Int32 GetCompanyAccountID(int intTransactionType, int intCompanyID)
        {
            return MobjClsDALSalesReturnMaster.GetCompanyAccountID(intTransactionType, intCompanyID);
        }
        public DataTable GetBatch(int intSalesInvoiceID, int intItemID)
        {
            return MobjClsDALSalesReturnMaster.GetBatch(intSalesInvoiceID,intItemID);
        }
        public DataTable GetWareHouse(long lngSaleInvoiceID, int intItemID, long lngBatchID)
        {
            return MobjClsDALSalesReturnMaster.GetWareHouse(lngSaleInvoiceID, intItemID, lngBatchID);
        }
        public DataTable GetGroupDetails(int intItemID)
        {
            return MobjClsDALSalesReturnMaster.GetItemGroupDetails(intItemID);
        }
        public DataTable BatchLessItems(int intItemID, int intWareHouseID)
        {
            return MobjClsDALSalesReturnMaster.BatchLessItems(intItemID, intWareHouseID);
        }
        //GET GROUP ISSUE DETAILS CORRESPONDING TO A SALES INVOICE ID
        public DataTable GetGroupIssueDetails(int intGroupItemID, int intGroupID)
        {
            return MobjClsDALSalesReturnMaster.GetGroupIssueDetails(intGroupItemID,intGroupID);
        }
          //get issue details of  MIN item CORRESPONDING TO A SALES INVOICE ID
        public DataTable GetIssueDetailsBatchlessItems(int intItemID)
        {
            return MobjClsDALSalesReturnMaster.GetIssueDetailsBatchlessItems(intItemID);
        }
        public DataTable GetGroupIssueDetailsEditMode(int intGroupItemID, int intGroupID)
        {
            return MobjClsDALSalesReturnMaster.GetGroupIssueDetailsEditMode(intGroupItemID, intGroupID);
        }
        public bool IsReturnExists(long lngSalesInvoiceID, long lngSalesReturnID)
        {
            return MobjClsDALSalesReturnMaster.IsReturnExists(lngSalesInvoiceID, lngSalesReturnID);
        }
        public decimal getTaxValue(int TaxScheme)
        {
            return MobjClsDALSalesReturnMaster.getTaxValue(TaxScheme);
        }
    }
}
