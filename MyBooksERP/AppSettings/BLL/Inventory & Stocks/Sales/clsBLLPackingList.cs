﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLPackingList
    {
        clsDALPackingList objclsDALPackingList;
        clsDTOPackingList objclsDTOPackingList;
        private DataLayer objClsConnection;

        public clsBLLPackingList()
        {
            objClsConnection = new DataLayer();
            objclsDALPackingList = new clsDALPackingList(objClsConnection);
            objclsDTOPackingList = new clsDTOPackingList();
            objclsDALPackingList.objclsConnection = objClsConnection;
            objclsDALPackingList.objclsDTOPackingList = objclsDTOPackingList;
        }
        public clsDTOPackingList clsDTOPackingList
        {
            get { return objclsDTOPackingList; }
            set { objclsDTOPackingList = value; }
        }
        public bool SaveCommercialInvoice(bool blnAddStatus)
        {
            bool blnRetValue = false;
            try
            {
                objClsConnection.BeginTransaction();
                blnRetValue = objclsDALPackingList.InsertMaster(blnAddStatus);
                objClsConnection.CommitTransaction();
            }
            catch (Exception ex)
            {
                objClsConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public bool DeleteCI()
        {
            bool blnRetValue = false;
            try
            {
                objClsConnection.BeginTransaction();
                blnRetValue = objclsDALPackingList.DeleteCI();
                objClsConnection.CommitTransaction();
            }
            catch (Exception ex)
            {
                objClsConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public DataSet GetDetails()
        {
            return objclsDALPackingList.GetDetails();
        }
    }
}