﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
/* 
=================================================
   Author:		< Sanju >
   Create date: < 21 Nov 2011 >
   Description:	< POS BLL>
================================================
*/

namespace MyBooksERP
{
    public class clsBLLPOS
    {
        clsDALPOS objclsDALPOS;
        clsDTOPOS objclsDTOPOS;
        private DataLayer objClsConnection;
        public clsBLLPOS()
        {
            objClsConnection = new DataLayer();
            objclsDALPOS = new clsDALPOS(objClsConnection);
            objclsDTOPOS = new clsDTOPOS();
            objclsDALPOS.objclsConnection = objClsConnection;
            objclsDALPOS.objclsDTOPOS = objclsDTOPOS;
        }
        public clsDTOPOS clsDTOPOS
        {
            get { return objclsDTOPOS; }
            set { objclsDTOPOS = value; }
        }
        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return objclsDALPOS.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
        }
        public DataTable GetDataForItemSelection(int intCompanyID, int intCurrencyID, int intWarehouseID)
        {
            return objclsDALPOS.GetDataForItemSelection(intCompanyID,intCurrencyID,intWarehouseID);
        }
        public DataTable FillCombos(string[] sarFieldValues)
        {
            return objclsDALPOS.FillCombos(sarFieldValues);
        }
        public DataTable GetItemWiseUOMs(int intItemID)
        {
            return objclsDALPOS.GetItemWiseUOMs(intItemID);
        }
        public DataTable GetItemAndCustomerWiseDiscounts(int intItemID, int intVendorID, decimal decQuantity, decimal decRate, string strCurrentDate, int intCurrencyID)
        {
            return objclsDALPOS.GetItemAndCustomerWiseDiscounts(intItemID, intVendorID, decQuantity, decRate, strCurrentDate,intCurrencyID);
        }
        public DataTable GetUomConversionValues(int intUOMID, int intItemID)
        {
            return objclsDALPOS.GetUomConversionValues(intUOMID, intItemID);
        }
        public void GetItemDiscountAmount(int intMode, int intItemID, int intDiscountID, int intVendorID)
        {
            objclsDALPOS.GetItemDiscountAmount(intMode, intItemID, intDiscountID, intVendorID);
        }
        public double GetRate(int intUOMID, int intItemID, int intCompanyID, int intBatchID)
        {
            double s = objclsDALPOS.GetRate(intUOMID, intItemID, intCompanyID, intBatchID);
            return s;
        }
        public Int32 GetPOSNo(int intCompanyID)
        {
            return objclsDALPOS.GetPOSNo(intCompanyID);
        }
        public Int32 GetPOSID()
        {
            return objclsDALPOS.GetPOSID();
        }
        public Int32 GetCurrencyID(int intCompanyID)
        {
            return objclsDALPOS.GetCurrencyID(intCompanyID);
        }
        public Int32 insertPOS()
        {
            try
            {
                objClsConnection.BeginTransaction();
                objclsDTOPOS.intPOSID= objclsDALPOS.insertPOSMaster();
                if (objclsDTOPOS.intPOSID <= 0)
                {
                    objClsConnection.RollbackTransaction();
                    return 3;
                }
                objclsDALPOS.insertPOSdetails();
                objclsDALPOS.SaveItemSummary(objclsDTOPOS.intPOSID);
                objClsConnection.CommitTransaction();
                return 1;
            }
            catch(Exception e)
            {
                objClsConnection.RollbackTransaction();
                return 2;
            }
        }
        public Int32 UpdatePOS()
        {
            try
            {
                objClsConnection.BeginTransaction();
                objclsDTOPOS.intPOSID = objclsDALPOS.insertPOSMaster();
                if (objclsDTOPOS.intPOSID <= 0)
                {
                    objClsConnection.RollbackTransaction();
                    return 2;
                }
                objclsDALPOS.DeleteItemSummary(objclsDTOPOS.intPOSID);
                objclsDALPOS.DeletePOSDetails();
                objclsDALPOS.insertPOSdetails();
                objclsDALPOS.SaveItemSummary(objclsDTOPOS.intPOSID);
                objClsConnection.CommitTransaction();
                return 1;
            }
            catch (Exception e)
            {
                objClsConnection.RollbackTransaction();
                return 2;
            }
        }
        public DataTable GetPOS()
        {
            return objclsDALPOS.GetPOS();
        }
        public DataTable SearchPOSNos(string strFilterCondition)
        {
            return objclsDALPOS.SearchPOSNos(strFilterCondition);
        }
        public Boolean DeletePOS()
        {
            try
            {
                objClsConnection.BeginTransaction();
                objclsDALPOS.DeleteItemSummary(objclsDTOPOS.intPOSID);
                objclsDALPOS.DeletePOSDetails();
                objclsDALPOS.DeletePOSMaster();                
                objClsConnection.CommitTransaction();
                return true;
            }
            catch (Exception e)
            {
                objClsConnection.RollbackTransaction();
                return false;
            }
        }
        public void ChangePOSStatus()
        {//Cancel or ReOpen
            try
            {
                objClsConnection.BeginTransaction();
                objclsDALPOS.ChangePOSStatus();
                objClsConnection.CommitTransaction();
            }
            catch (Exception e)
            {
                objClsConnection.RollbackTransaction();
            }
        }
        public DataTable GetQuantityAvailable(int intItemID, int intWareHouseID, int intBatchID, int intPOSID)
        {
            return objclsDALPOS.GetQuantityAvailable(intItemID, intWareHouseID, intBatchID, intPOSID);
        }
        public DataTable GetDiscount(int intCurrencyID, decimal decNetAmount,DateTime dtePosDate,int intCustomerID)
        {
            return objclsDALPOS.GetDiscount(intCurrencyID, decNetAmount, dtePosDate, intCustomerID);
        }
        public DataSet GetEmailPOSinfo()
        {
            return objclsDALPOS.GetEmailPOSinfo();
        }

        public DataTable GetEmployeeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return objclsDALPOS.GetEmployeeByPermission(intRoleID, intCompanyID, intControlID);
        }
        public DataTable GetItemForBarCode(int intCompanyID, int intItemID, int intBatchID)
        {
            return objclsDALPOS.GetItemForBarCode(intCompanyID, intItemID, intBatchID);
        }
        public bool GetCompanyAccount(int intTransactionType,int intCompanyID)
        {
            return objclsDALPOS.GetCompanyAccount(intTransactionType, intCompanyID);
        }
        public decimal GetItemRateForPOS(long lngItemID, long lngBatchID, long lngPOSID)
        {
            return objclsDALPOS.GetItemRateForPOS(lngItemID, lngBatchID, lngPOSID);
        }
        public DataTable BatchLessItems(int intItemID, int intWareHouseID)
        {
            return objclsDALPOS.BatchLessItems(intItemID, intWareHouseID);
        }
        public DataTable GetGroupDetails(int intItemID)
        {
            return objclsDALPOS.GetGroupDetails(intItemID);
        }
        public void DeleteAllPOSDetails()
        {
            objclsDALPOS.DeleteAllPOSDetails();
        }
        public DataTable GetGroupItemDetails(int intItemID)
        {
            return objclsDALPOS.GetGroupItemDetails(intItemID);
        }

        public DataSet GetAllQuantityAvailable(int intItemID, int intWareHouseID, int intBatchID, int intPOSID)
        {
            return objclsDALPOS.GetAllQuantityAvailable(intItemID, intWareHouseID, intBatchID, intPOSID);
        }
        public decimal GetUsedQuantity(long lngItemID, int WarehouseID)
        {
            return objclsDALPOS.GetUsedQuantity(lngItemID, WarehouseID);
        }
        //public decimal GetClosingRate(int CompanyID, DateTime ToDate)
        //{
        //    return objclsDALPOS.GetClosingRate(CompanyID, ToDate);
        //}
        //public bool GetClosingStockValidation(int CompanyID, int CurrencyID, int ItemID, int BatchID, decimal Quantity, bool IsGroup)
        //{
        //    return objclsDALPOS.GetClosingStockValidation(CompanyID, CurrencyID, ItemID, BatchID, Quantity, IsGroup);
        //}

    }
}
