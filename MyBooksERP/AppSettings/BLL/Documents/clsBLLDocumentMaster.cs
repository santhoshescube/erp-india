﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;


namespace MyBooksERP
{
    /* 
=================================================
Description :	     <Document Transactions>
Modified By :		 <Laxmi>
Modified Date :      <13 Apr 2012>
================================================
*/

    public class clsBLLDocumentMaster
    {
        public clsDTODocumentMaster objClsDTODocumentMaster { get; set; }
        clsDALDocumentMaster objClsDALDocumentMaster;

        public clsBLLDocumentMaster()
        {
            objClsDALDocumentMaster = new clsDALDocumentMaster();
            objClsDTODocumentMaster = new clsDTODocumentMaster();
            objClsDALDocumentMaster.objClsDTODocumentMaster = objClsDTODocumentMaster;
            objClsDALDocumentMaster.objConnection = new DataLayer();
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objClsDALDocumentMaster.objConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool GetDocumentInfo()
        {
            return objClsDALDocumentMaster.GetDocumentInfo();
        }
        public bool GetExistingDocumentInfo(int PintVendorRefereceID, int intDocumentTypeID)
        {
            return objClsDALDocumentMaster.GetExistingDocumentInfo(PintVendorRefereceID, intDocumentTypeID);
        }
        public int SaveDocumentInfo()
        {
            DataTable dtAlert = null;
            int iDocID=objClsDALDocumentMaster.SaveDocumentInfo();
            if (this.objClsDTODocumentMaster.strExpiryDate != "")
            {
                if (iDocID > 0 & (objClsDTODocumentMaster.intStatusID == (int)OperationStatusType.DocReceipt))
                {
                    dtAlert = this.SetDocumentAlert();
                    if (dtAlert != null)
                        objClsDALDocumentMaster.SaveAlert(dtAlert);
                }
            }

            return iDocID;
        }
      
        public bool DeleteDocumentInfo()
        {
            return objClsDALDocumentMaster.DeleteDocumentInfo();
        }

        public bool CheckDuplicateDocument(int intVendorID)
        {
            return objClsDALDocumentMaster.CheckDuplicateDocument(intVendorID);
        }
        //public bool CheckDuplicateDocumentAtSameOperationType(int Tag)
        //{
        //    return objClsDALDocumentMaster.CheckDuplicateDocumentAtSameOperationType(Tag);
        //}
        public bool CheckDuplicateDocumentDetailsForSameVendor(int intReferenceID,int intDocumentType)
        {
            return objClsDALDocumentMaster.CheckDuplicateDocumentDetailsForSameVendor(intReferenceID, intDocumentType);
        }
    
        public int GetDocumentCount(int PintOperationType, long PlngOperationID)
        {
            return objClsDALDocumentMaster.GetDocumentCount(PintOperationType,PlngOperationID);
        }

        public DataSet GetDocumentReport()
        {
            return objClsDALDocumentMaster.GetDocumentReport();
        }

        public int GetOperationStatus(int PintOperationType, long PlngOperationID)
        {
            return objClsDALDocumentMaster.GetOperationStatus(PintOperationType, PlngOperationID);
        }
        public bool GetStatusAndOrderNo()
        {
            return objClsDALDocumentMaster.GetStatusAndOrderNo();
        }
        public DataTable GetStockDocuments()
        {
            return objClsDALDocumentMaster.GetStockDocuments();
        }
        public DataTable GetStockDocumentDetails()
        {
            return objClsDALDocumentMaster.GetStockDocumentDetails();
        }

        public bool DeleteSingleDocument()
        {
            return objClsDALDocumentMaster.DeleteSingleDocument();
        }
        public long CheckAlertSetting()
        {
            return objClsDALDocumentMaster.CheckAlertSetting();
        }
        private DataTable  SetDocumentAlert()
        {
            string strAlertMsg, strFilter=null;
            DataRow[] dtUserRows = null;
            DataRow dtNewAlertRow;
            DataTable datAlert = null;

            DataTable dtAlertTemplates = objClsDALDocumentMaster.GetAlertSetting();

            DataTable dtAlertDocs = objClsDALDocumentMaster.GetAlertDocs();

            if (dtAlertTemplates == null || dtAlertTemplates.Rows.Count == 0 && dtAlertDocs == null)
            {
                return null;
            }


            datAlert = new DataTable();
            datAlert.Columns.Add("AlertID", typeof(long));
            datAlert.Columns.Add("AlertUserSettingID", typeof(long));
            datAlert.Columns.Add("ReferenceID", typeof(long));
            datAlert.Columns.Add("StartDate", typeof(string));
            datAlert.Columns.Add("AlertMessage", typeof(string));
            datAlert.Columns.Add("Status", typeof(int));

         
            foreach (DataRow dtRow in dtAlertTemplates.Rows)
            {
                //strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                strFilter += "IsAlertON = true";

                dtUserRows = dtAlertTemplates.Select(strFilter);

                if (dtUserRows.Count() == 0)
                {
                    dtUserRows = null;
                    continue;
                }

                strAlertMsg = dtRow["AlertMessage"].ToString();

                strAlertMsg = strAlertMsg.Replace("<OPERATIONTYPE>", dtAlertDocs.Rows[0]["OperationType"].ToString());
                strAlertMsg = strAlertMsg.Replace("<DOCUMENT>",  dtAlertDocs.Rows[0]["DocumentType"].ToString());
                strAlertMsg = strAlertMsg.Replace("<EXPIRYDATE>", dtAlertDocs.Rows[0]["Expydate"].ToString());

                foreach (DataRow dtUserRow in dtUserRows)
                {
                    dtNewAlertRow = datAlert.NewRow();
                    dtNewAlertRow["AlertID"] = 0;
                    dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                    dtNewAlertRow["ReferenceID"] = this.objClsDTODocumentMaster.intDocumentID ;
                    dtNewAlertRow["StartDate"] = objClsDTODocumentMaster.strExpiryDate;
                    dtNewAlertRow["AlertMessage"] = strAlertMsg;
                    dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                    datAlert.Rows.Add(dtNewAlertRow);
                    dtNewAlertRow = null;
                }

                strAlertMsg = "";
                strFilter = "";
                dtUserRows = null;
            }

            datAlert.AcceptChanges();
            return datAlert;

        }

        public int  GetCurStatus() // Opened,Closed,Cancelled
        {
            return objClsDALDocumentMaster.GetCurStatus();
        }
        public bool CheckDocumentExists()
        {
            return objClsDALDocumentMaster.CheckDocumentExists();
        }
        /// <summary>
        /// Delete Alerts on Document deletion or Updation without entering Expiry date
        /// </summary>
        /// <param name="ReferenceID">Document ID</param>
        /// <param name="AlertsettingID"></param>
        /// <returns>bool</returns>
        public void DeleteAlert(int ReferenceID, int AlertsettingID)
        {
            this.objClsDALDocumentMaster.DeleteAlert(ReferenceID, AlertsettingID);
        }
    }
}
