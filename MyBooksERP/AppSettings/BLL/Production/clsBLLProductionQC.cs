﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLProductionQC
    {
        private clsDALProductionQC MobjclsDALProductionQC;
        private DataLayer MobjDataLayer;

        public clsBLLProductionQC()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALProductionQC = new clsDALProductionQC(MobjDataLayer);
        }
        public long SaveQC(DataTable datDetails, long QCID, string QCNo, string QCDate, long ProductionID, string Remarks, int InchargeID, decimal SampleQty, decimal PassedQty)
        {
            long blnRetValue = 0;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALProductionQC.SaveQC(datDetails, QCID, QCNo, QCDate, ProductionID, Remarks, InchargeID, SampleQty, PassedQty);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public bool DeleteQC(long QCID)
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALProductionQC.DeleteQC(QCID);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public DataTable GetAllQC(DateTime dtFromdate, DateTime dtToDate, long ProductionID, long QCID)
        {
            return MobjclsDALProductionQC.GetAllQC(dtFromdate, dtToDate, ProductionID, QCID);
        }
        public DataTable GetQCDetails(long QCID)
        {
            return MobjclsDALProductionQC.GetQCDetails(QCID);
        }
        public string GetQCNo()
        {
            return MobjclsDALProductionQC.GetQCNo();
        }
        public bool DeleteQCDetails(long QCID)
        {
            return MobjclsDALProductionQC.DeleteQCDetails(QCID);
        }
    }
}
