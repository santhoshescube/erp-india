﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLProductionTemplate
    {
        private clsDALProductionTemplate MobjclsDALProductionTemplate;
        private DataLayer MobjDataLayer;
        public clsDTOProductionTemplate PobjClsDTOProductionTemplate { get; set; }

        public clsBLLProductionTemplate()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALProductionTemplate = new clsDALProductionTemplate(MobjDataLayer);
            PobjClsDTOProductionTemplate = new clsDTOProductionTemplate();
            MobjclsDALProductionTemplate.objDTOProductionTemplate = PobjClsDTOProductionTemplate;
        }

        public bool SaveProductionTemplate(DataTable datDetails, DataTable datOther)
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALProductionTemplate.SaveProductionTemplate(datDetails, datOther);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeleteProductionTemplate()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALProductionTemplate.DeleteProductionTemplate();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable getSearchTemplate()
        {
            return MobjclsDALProductionTemplate.getSearchTemplate();
        }

        public DataSet getTemplateDetails()
        {
            return MobjclsDALProductionTemplate.getTemplateDetails();
        }

        public string getTemplateCode()
        {
            return MobjclsDALProductionTemplate.getTemplateCode();
        }

        public bool checkTemplateCodeDuplication(long lngTempID, string strCode)
        {
            return MobjclsDALProductionTemplate.checkTemplateCodeDuplication(lngTempID, strCode);
        }
    }
}
