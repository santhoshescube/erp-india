﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLProductionProcess
    {
        private clsDALProductionProcess MobjclsDALProductionProcess;        
        private DataLayer MobjDataLayer;
        public clsDTOProductionProcess PobjClsDTOProductionProcess { get; set; }

        public clsBLLProductionProcess()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALProductionProcess = new clsDALProductionProcess(MobjDataLayer);
            PobjClsDTOProductionProcess = new clsDTOProductionProcess();
            MobjclsDALProductionProcess.objDTOProductionProcess = PobjClsDTOProductionProcess;
        }

        public bool SaveProductionProcess(DataTable datDetails)
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALProductionProcess.SaveProductionProcess(datDetails);

                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeleteProductionProcess()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALProductionProcess.DeleteProductionProcess();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable getSearchProductionProcess(string strPrdNo, DateTime dtFromDate, DateTime dtToDate, long lngMID)
        {
            return MobjclsDALProductionProcess.getSearchProductionProcess(strPrdNo, dtFromDate, dtToDate, lngMID);
        }

        public DataSet getProductionProcessDetails()
        {
            return MobjclsDALProductionProcess.getProductionProcessDetails();
        }

        public string getProductionProcessNo()
        {
            return MobjclsDALProductionProcess.getProductionProcessNo();
        }

        public bool checkProductionProcessNoDuplication(long lngProductionID, string strProductionNo)
        {
            return MobjclsDALProductionProcess.checkProductionProcessNoDuplication(lngProductionID, strProductionNo);
        }

        public DataTable getBOMDetails(long lngTemplateID)
        {
            return MobjclsDALProductionProcess.getBOMDetails(lngTemplateID);
        }
    }
}
