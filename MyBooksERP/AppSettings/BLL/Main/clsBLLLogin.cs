﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;

namespace MyBooksERP
{
    public class clsBLLLogin
    {
        clsDALLogin MobjClsDALLogin;
        
        public clsBLLLogin()
        {
            DataLayer objDataLayer = new DataLayer();
            MobjClsDALLogin = new clsDALLogin(objDataLayer);
        }

        public DataTable GetUserInfo(string strUserName, string strPassword)
        {
            return MobjClsDALLogin.GetUserInformation(strUserName, strPassword);
        }

        public DataTable GetTopMostCompany()
        {
            return MobjClsDALLogin.GetTopMostCompany();
        }
        public DataTable GetUserCompany()
        {
            return MobjClsDALLogin.GetUserCompany();
        }
        
        public DataTable GetDefaultConfigurationSettings()
        {
            return MobjClsDALLogin.GetDefaultConfigurationSettings();
        }

        public DataTable GetCompanySettingsData(int intCompanyID)
        {
            return MobjClsDALLogin.GetCompanySettingsData(intCompanyID);
        }
        public DataTable GetCompanySettings(int intCompanyID)
        {
            return MobjClsDALLogin.GetCompanySettings(intCompanyID);
        }
        public void UpdateRemember(string strUserName,bool blnRemember)
        {
            MobjClsDALLogin.UpdateRemember(strUserName,blnRemember);
        }

        public string GetPassword(string strUserName)
        {
            return MobjClsDALLogin.GetPassword(strUserName);
        }

        public bool GetRememberStatus(string strUserName)
        {
            return MobjClsDALLogin.GetRememberStatus(strUserName);
        }

        public void InsertOpStockAccount(int intCompanyID)
        {
            MobjClsDALLogin.InsertOpStockAccount(intCompanyID);
        }

        public DataTable getCompanyList(int intUserID)
        {
            return MobjClsDALLogin.getCompanyList(intUserID);
        }

        public DataTable GetBookKeepingYears(int intCompanyID)
        {
            return MobjClsDALLogin.GetBookKeepingYears(intCompanyID);
        }

        public DataTable GetCompanyDetails(int intCompanyID)
        {
            return MobjClsDALLogin.GetCompanyDetails(intCompanyID);
        }
    }
}
