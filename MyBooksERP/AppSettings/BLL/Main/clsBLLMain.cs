﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;

namespace MyBooksERP
{
    public class clsBLLMain
    {
        public clsBLLMain()
        {
        }

        ~clsBLLMain()
        {
        }

        public DataTable GetUserAlerts(int intModuleID)
        {
            clsBLLAlert objclsBLLAlert;

            try
            {
                objclsBLLAlert = new clsBLLAlert();
                return objclsBLLAlert.GetToDayAlerts(intModuleID);
            }
            catch  { return null; }
        }

        public bool SetReadStatus(int intAlertID, int intStatusID)
        {
            clsBLLAlert objclsBLLAlert;

            try
            {
                objclsBLLAlert = new clsBLLAlert();
                return objclsBLLAlert.SetReadStatus(intAlertID, intStatusID);
            }
            catch { return false; }
        }

        public clsDTOMailSetting GetMailSetting()
        {
            clsDALMailSettings objclsDALMailSettings;
            DataLayer clsDataLayer;
            try
            {
                clsDataLayer = new DataLayer();
                objclsDALMailSettings = new clsDALMailSettings();
                objclsDALMailSettings.objClsConnection = clsDataLayer;
                objclsDALMailSettings.GetMailSetting(2);//SMS
                return objclsDALMailSettings.objclsDTOMailSetting;
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            clsBLLAlert objclsBLLAlert = new clsBLLAlert();
            // function for getting datatable for filling combo
            return objclsBLLAlert.FillCombos(saFieldValues);
        }

        public DataTable GetSalesQuotationSubmittedAlert()
        {
            try
            {
                return new clsBLLAlertMoment().GetSalesQuotationSubmittedAlert();
            }
            catch { return null; }
        }

        public DataTable GetSalesQuotationApprovedAlert()
        {
            try
            {
                return new clsBLLAlertMoment().GetSalesQuotationApprovedAlert();
            }
            catch { return null; }
        }

        public DataTable GetSalesQuotationCancelledAlert()
        {
            try
            {
                return new clsBLLAlertMoment().GetSalesQuotationCancelledAlert();
            }
            catch { return null; }
        }

        public DataTable GetSalesOrderAdvancePaidAlert()
        {
            try
            {
                return new clsBLLAlertMoment().GetSalesOrderAdvancePaidAlert();
            }
            catch { return null; }
        }

        public DataTable GetSalesOrderCreatedAlert()
        {
            try
            {
                return new clsBLLAlertMoment().GetSalesOrderCreatedAlert();
            }
            catch { return null; }
        }

        public DataTable GetSaleOrderSubmittedAlert()
        {
            try
            {
                return new clsBLLAlertMoment().GetSaleOrderSubmittedAlert();
            }
            catch { return null; }
        }

        public DataTable GetSaleOrderApprovedAlert()
        {
            try
            {
                return new clsBLLAlertMoment().GetSaleOrderApprovedAlert();
            }
            catch { return null; }
        }

        public DataTable GetSaleOrderRejectedAlert()
        {
            try
            {
                return new clsBLLAlertMoment().GetSaleOrderRejectedAlert();
            }
            catch { return null; }
        }

        public DataTable GetReceiptAlert()
        {
            try
            {
                return new clsBLLAlertMoment().GetReceiptAlert();
            }
            catch { return null; }
        }

        public DataTable GetMaterialIssueAlert()
        {
            try
            {
                return new clsBLLAlertMoment().GetMaterialIssueAlert();
            }
            catch { return null; }
        }

        public DataTable GetDeliveryNotetAlert()
        {
            try
            {
                return new clsBLLAlertMoment().GetDeliveryNotetAlert();
            }
            catch { return null; }
        }

        public DataTable GetPurchaseOrderApprovedAlert()
        {
            try
            {
                return new clsBLLAlertMoment().GetPurchaseOrderApprovedAlert();
            }
            catch { return null; }
        }

    }
}