﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace MyBooksERP
{
    public class clsBLLDeductionPolicy
    {
        clsDALdeductionPolicy MobjclsDALDeductionPolicy;
        clsDTOdeductionPolicy MobjclsDTODeductionPolicy;
        private DataLayer MobjDataLayer;

        public clsBLLDeductionPolicy()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALDeductionPolicy = new clsDALdeductionPolicy(MobjDataLayer);
            MobjclsDTODeductionPolicy = new clsDTOdeductionPolicy();
            MobjclsDALDeductionPolicy.objClsDTOdeductionPolicy = MobjclsDTODeductionPolicy;
        }

        public clsDTOdeductionPolicy clsDTOdeductionPolicy
        {
            get { return MobjclsDTODeductionPolicy; }
            set { MobjclsDTODeductionPolicy = value; }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALDeductionPolicy.FillCombos(saFieldValues);
        }
        public DataTable FillDeductiondetailAddMode()
        {
            return MobjclsDALDeductionPolicy.FillDeductiondetailAddMode();
        }
        public int SavePolicy(bool blnAddStatus)
        {
            //Function For Saving Purchase Quotation
            int blnRetValue = 0;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALDeductionPolicy.SavePolicy(blnAddStatus);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public int RecCountNavigate()
        {
            return MobjclsDALDeductionPolicy.RecCountNavigate();
        }
        public bool Getpolicy(int iRownum)
        {
            return MobjclsDALDeductionPolicy.Getpolicy(iRownum);
        }
        public DataTable FillDeductiondetail(int DeductionID)
        {
            return MobjclsDALDeductionPolicy.FillDeductiondetail(DeductionID);
        }
        public bool IsExists()
        {
            return MobjclsDALDeductionPolicy.IsExists();
        }
        public bool Delete()
        {
            return MobjclsDALDeductionPolicy.Delete();
        }
        public bool CheckDuplicate(int iPolicyID, string strPolicyName, bool blnAddStatus)
        {
            return MobjclsDALDeductionPolicy.CheckDuplicate(iPolicyID, strPolicyName, blnAddStatus);
        }
        public DataSet DisplayDeductionPolicy() 
        {
            return MobjclsDALDeductionPolicy.DisplayDeductionPolicy();
        }
    }
}
