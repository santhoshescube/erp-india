﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLOvertimePolicy
    {

        //clsDALLeavePolicy MobjclsDALLeavePolicy = null;
        clsDALOvertimePolicy MobjclsDALOvertimePolicy = null;
        public clsBLLOvertimePolicy() { }



        //private clsDALLeavePolicy DALLeavePolicy
        private clsDALOvertimePolicy DALOvertimePolicy 
        {
            get
            {
                if (this.MobjclsDALOvertimePolicy == null)
                    this.MobjclsDALOvertimePolicy = new clsDALOvertimePolicy();

                return this.MobjclsDALOvertimePolicy;
            }
        }

        
        public clsDTOOvertimePolicy DTOOvertimePolicy 
        {
            get
            {
                return this.DALOvertimePolicy.DTOOvertimePolicy;
            }
        }

        public DataTable GetAdditionDeductionDetails()
        {
            return DALOvertimePolicy.GetAdditionDeductionDetails();
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALOvertimePolicy.FillCombos(saFieldValues);
        }

        public bool OTPolicyMasterSave(bool blnEditMode)
        {
            bool blnRetValue = false;
            blnRetValue = DALOvertimePolicy.OTPolicyMasterSave(blnEditMode);
            return blnRetValue;
        }

        public bool SalrayPolicyDetailsSave(bool blnEditMode)
        {
            return DALOvertimePolicy.SalrayPolicyDetailsSave(blnEditMode);
        }

        public bool DisplayOTPolicyMaster(int intRowNumber)
        {
            return DALOvertimePolicy.DisplayOTPolicyMaster(intRowNumber);
        }

        public DataTable DispalySalaryPolicyDetail(int intOTPolicyID)
        {
            return DALOvertimePolicy.DispalySalaryPolicyDetail(intOTPolicyID);
        }
        public int GetRecordCount()
        {
            return DALOvertimePolicy.GetRecordCount();
        }

        public bool DeleteOTPolicy()
        {
            return DALOvertimePolicy.DeleteOTPolicy();
        }

        public bool PolicyIDExists()
        {
            return DALOvertimePolicy.PolicyIDExists();
        }
        public int GetRowNumber()
        {
            return DALOvertimePolicy.GetRowNumber();
        }
        public bool CheckDuplicate(int iOTPolicyID, string strPolicyName)
        {
            return DALOvertimePolicy.CheckDuplicate(iOTPolicyID,strPolicyName);
        }


    }
}
