﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace MyBooksERP
{
    public class clsBLLPFPolicy
    {
        clsDALPFPolicy MobjclsDALPFPolicy;
        clsDTOPFPolicy MobjclsDTOPFPolicy;
        private DataLayer MobjDataLayer;

        public clsBLLPFPolicy()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALPFPolicy = new clsDALPFPolicy(MobjDataLayer);
            MobjclsDTOPFPolicy = new clsDTOPFPolicy();
            MobjclsDALPFPolicy.objClsDTOPFPolicy = MobjclsDTOPFPolicy;
        }

        public clsDTOPFPolicy clsDTOPFPolicy
        {
            get { return MobjclsDTOPFPolicy; }
            set { MobjclsDTOPFPolicy = value; }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALPFPolicy.FillCombos(saFieldValues);
        }
        public DataTable FillDeductiondetailAddMode()
        {
            return MobjclsDALPFPolicy.FillDeductiondetailAddMode();
        }
        public int SavePolicy(bool blnAddStatus)
        {
            //Function For Saving Purchase Quotation
            int blnRetValue = 0;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALPFPolicy.SavePolicy(blnAddStatus);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public int RecCountNavigate()
        {
            return MobjclsDALPFPolicy.RecCountNavigate();
        }
        public bool Getpolicy(int iRownum)
        {
            return MobjclsDALPFPolicy.Getpolicy(iRownum);
        }
        public DataTable FillDeductiondetail(int DeductionID)
        {
            return MobjclsDALPFPolicy.FillDeductiondetail(DeductionID);
        }
        public bool IsExists()
        {
            return MobjclsDALPFPolicy.IsExists();
        }
        public bool Delete()
        {
            return MobjclsDALPFPolicy.Delete();
        }
        public bool CheckDuplicate(int iPolicyID, string strPolicyName, bool blnAddStatus)
        {
            return MobjclsDALPFPolicy.CheckDuplicate(iPolicyID, strPolicyName, blnAddStatus);
        }
        public DataSet DisplayDeductionPolicy() 
        {
            return MobjclsDALPFPolicy.DisplayDeductionPolicy();
        }
    }
}
