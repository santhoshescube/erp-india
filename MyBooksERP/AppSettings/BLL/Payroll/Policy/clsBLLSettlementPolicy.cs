﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using DAL;
using DTO;

namespace MyBooksERP
{
    public class clsBLLSettlementPolicy
    {

         private DataLayer MobjDataLayer;

        clsDALSettlementPolicy MobjclsDALSettlementPolicy;
        clsDTOSettlementPolicy MobjclsDTOSettlementPolicy;

        public clsBLLSettlementPolicy()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALSettlementPolicy = new clsDALSettlementPolicy(MobjDataLayer);
            MobjclsDTOSettlementPolicy = new clsDTOSettlementPolicy();
            MobjclsDALSettlementPolicy.DTOSettlementPolicy = MobjclsDTOSettlementPolicy;

        }

        public clsDTOSettlementPolicy clsDTOSettlementPolicy
        {
            get { return MobjclsDTOSettlementPolicy; }
            set { MobjclsDTOSettlementPolicy = value; }
        }

        public System.Data.DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALSettlementPolicy.FillCombos(saFieldValues);
        }

        public int RecCount()
        {
            return MobjclsDALSettlementPolicy.RecCount();
        }
        public int RecCountNavigate()
        {
            return MobjclsDALSettlementPolicy.RecCountNavigate();
        }
        public void DisplayDetails(int RowNum)
        {
             MobjclsDALSettlementPolicy.DisplayDetails(RowNum);
        }
        public bool SaveCompanySettlementPolicy()
        {
            return MobjclsDALSettlementPolicy.SaveCompanySettlementPolicy();
        }
        public Int32 GetRowNum(int RecID)
        {
            return MobjclsDALSettlementPolicy.GetRowNum(RecID);
        }
        public int ExistsCompanySettlementPolicy()
        {
            return MobjclsDALSettlementPolicy.ExistsCompanySettlementPolicy();
        }
        //public int ExistsCompanySettlementPolicy()
        //{
        //    return MobjclsDALSettlementPolicy.ExistsCompanySettlementPolicy();
        //}
        public bool DeleteCompanySettlementPolicy()
        {
            return MobjclsDALSettlementPolicy.DeleteCompanySettlementPolicy();
        }


        public System.Data.DataTable FillAdddetailAddMode()
        {
            return MobjclsDALSettlementPolicy.FillAdddetailAddMode();
        }

        public bool CheckploicyNameDup(int intPolicyID, string strPolicyName)
        {
            return MobjclsDALSettlementPolicy.CheckploicyNameDup(intPolicyID, strPolicyName);
        }
    }

    
}
