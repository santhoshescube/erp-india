﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DAL;
using DTO;

namespace BLL
{
    public class clsBLLWorkPolicy
    {
        clsDALWorkPolicy MobjclsDALWorkPolicy;
        clsDTOWorkPolicy MobjclsDTOWorkPolicy;
        private DataLayer MobjDataLayer;


        public clsBLLWorkPolicy()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALWorkPolicy = new clsDALWorkPolicy(MobjDataLayer);
            MobjclsDTOWorkPolicy = new clsDTOWorkPolicy();
            MobjclsDALWorkPolicy.objclsDTOWorkPolicy = MobjclsDTOWorkPolicy;
        }

        public clsDTOWorkPolicy clsDTOWorkPolicy
        {
            get { return MobjclsDTOWorkPolicy; }
            set { MobjclsDTOWorkPolicy = value; }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALWorkPolicy.FillCombos(saFieldValues);
        }

        #region RecCountNavigate
        /// <summary>
        /// Get the number of records
        /// </summary>
        /// <returns>Number of records as integer</returns>
        public int RecCountNavigate()
        {          
           return MobjclsDALWorkPolicy.RecCountNavigate();
        }
        #endregion RecCountNavigate

        public DataSet GetPolicyDetails()
        {
            return MobjclsDALWorkPolicy.GetPolicyDetails();
        }

        public bool SaveWorkPolicy(bool blnAddStatus)
        {
            //Function For Saving Purchase Quotation
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALWorkPolicy.SaveWorkPolicy(blnAddStatus);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool  GetWorkpolicy(int iRownum)
        {
            return MobjclsDALWorkPolicy.GetWorkpolicy(iRownum);
        }
        public bool CheckDuplication(bool bAddStatus, string[] saValues, int iId, int iType)
        {
            //function for checking duplication
            return MobjclsDALWorkPolicy.CheckDuplication(bAddStatus, saValues, iId, iType);
        }
        public bool IsExists()
        {
            return MobjclsDALWorkPolicy.IsExists();
        }
        public bool Delete()
        {
            return MobjclsDALWorkPolicy.Delete();
        }
        public bool IsExistsAttendanceWithoutProcess(int intPolicyID)
        {
            return MobjclsDALWorkPolicy.IsExistsAttendanceWithoutProcess( intPolicyID);

        }
        public bool IsExistsAttendance(int intPolicyID)
        {
            return MobjclsDALWorkPolicy.IsExistsAttendance(intPolicyID);
        }

    }
}
