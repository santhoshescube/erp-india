﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace MyBooksERP
{
    public class clsBLLHolidayType
    {
        clsDALHolidayType MobjclsDALHolidayType = null;
        public clsBLLHolidayType() { }



       
        private clsDALHolidayType DALHolidayType  
        {
            get
            {
                if (this.MobjclsDALHolidayType == null)
                    this.MobjclsDALHolidayType = new clsDALHolidayType();

                return this.MobjclsDALHolidayType;
            }
        }


        public clsDTOHolidayType DTOHolidayType  
        {
            get
            {
                return this.DALHolidayType.DTOHolidayType;
            }
        }

        public bool DisplayHolidayType(int intRowNumber)
        {
            return DALHolidayType.DisplayHolidayType(intRowNumber);
        }

        public int GetRecordCount()
        {
            return DALHolidayType.GetRecordCount();
        }

        public bool HolidayTypeSave(bool blnEditMode)
        {
            bool blnRetValue = false;
            blnRetValue = DALHolidayType.HolidayTypeSave(blnEditMode);
            return blnRetValue;
        }

        public bool HolidayTypeIDExists()
        {
            return DALHolidayType.HolidayTypeIDExists();
        }

        public bool DeleteHolidayType()
        {
            return DALHolidayType.DeleteHolidayType();
        }
        public bool CheckDuplicate(int iHolidayTypeID, string strHolidayType)
        {
            return DALHolidayType.CheckDuplicate(iHolidayTypeID,strHolidayType);
        }



    }
}
