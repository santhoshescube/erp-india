﻿using System;
using System.Collections.Generic;
using System.Data;

namespace MyBooksERP
{ /*********************************************
       * Author       : Arun
       * Created On   : 10 Apr 2012
       * Purpose      : Holiday Policy BLL 
       * ******************************************/

    public class clsBLLHolidayPolicy
    {

        clsDALHolidayPolicy MobjDALHolidayPolicy = null;

        public clsBLLHolidayPolicy()
        {

        }

        private clsDALHolidayPolicy DALHolidayPolicy
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALHolidayPolicy == null)
                    this.MobjDALHolidayPolicy = new clsDALHolidayPolicy();

                return this.MobjDALHolidayPolicy;
            }
        }

        public clsDTOHolidayPolicy DTOHolidayPolicy
        {
            get { return this.DALHolidayPolicy.DTOHolidayPolicy; }
        }

        public int GetRowNumber(int intHolidayPolicyID)
        {
            return DALHolidayPolicy.GetRowNumber(intHolidayPolicyID);
        }
        public int GetRecordCount()
        {
            return DALHolidayPolicy.GetRecordCount();
        }
        public bool HolidayPolicyMasterSave()
        {
            bool blnRetValue = false;
            try
            {
                DALHolidayPolicy.DataLayer.BeginTransaction();
                blnRetValue = DALHolidayPolicy.HolidayPolicyMasterSave();
                if (blnRetValue)
                {
                    DALHolidayPolicy.DeleteHolidayPolicyDetails();
                    //DALHolidayPolicy.HolidayPolicyDetailSave();
                    DALHolidayPolicy.SalaryPolicyDetailsSave();

                    DALHolidayPolicy.DataLayer.CommitTransaction();
                }
                else
                {
                    DALHolidayPolicy.DataLayer.RollbackTransaction();
                }
            }
            catch 
            {
                DALHolidayPolicy.DataLayer.RollbackTransaction();
                throw;
            }
            return blnRetValue;
            
        }
        public DataTable DisplayHolidayPolicy(int intRowNumber)
        {
            return DALHolidayPolicy.DisplayHolidayPolicy(intRowNumber);
        }
        public DataTable DisplayAddDedDetails(int iPolicyID, int iType)
        {
            return DALHolidayPolicy.DisplayAddDedDetails(iPolicyID, iType);
        }

        public bool DeleteHolidayPolicy()
        {
            return DALHolidayPolicy.DeleteHolidayPolicy();
        }

        public DataTable GetAdditionDeductions()
        {
            return DALHolidayPolicy.GetAdditionDeductions();
        }
        public bool CheckDuplicate(int iPolicyID, string strPolicyName)
        {
            return DALHolidayPolicy.CheckDuplicate(iPolicyID, strPolicyName);
        }
        public bool PolicyIDExists(int iPolicyID)
        {
            return DALHolidayPolicy.PolicyIDExists(iPolicyID);
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALHolidayPolicy.FillCombos(saFieldValues);
        }


    }
}
