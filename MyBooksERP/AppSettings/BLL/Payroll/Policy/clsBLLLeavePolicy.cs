﻿using System;
using System.Collections.Generic;
using System.Data;
namespace MyBooksERP
{
    /*********************************************
      * Author       : Arun
      * Created On   : 5 Apr 2012
      * Purpose      : Leave Policy BLL 
      * ******************************************/
   public  class clsBLLLeavePolicy
    {

       clsDALLeavePolicy MobjclsDALLeavePolicy = null;
       
       /// <summary>
       /// Constructor
       /// </summary>
       public clsBLLLeavePolicy()
       {
       }
       /// <summary>
       /// Data Access Layer of Leave Policy
       /// </summary>
       private clsDALLeavePolicy DALLeavePolicy
       {
           get
           {
               // Create instance of data access layer if null
               if (this.MobjclsDALLeavePolicy == null)
                   this.MobjclsDALLeavePolicy = new clsDALLeavePolicy();

               return this.MobjclsDALLeavePolicy;
           }
       }

       public clsDTOLeavePolicy DTOLeavePolicy
       {
           get
           {
               return this.DALLeavePolicy.DTOLeavePolicy;
           }
       }

       /// <summary>
       /// Get Row Number
       /// </summary>
       /// <param name="intLeavePolicyID">Leave policyID as int</param>
       /// <returns>row number</returns>
       public int GetRowNumber(int intLeavePolicyID)
       {
           return DALLeavePolicy.GetRowNumber(intLeavePolicyID);
       }

       /// <summary>
       /// Get total record count
       /// </summary>
       /// <returns>record count</returns>
       public int GetRecordCount()
       {
           return DALLeavePolicy.GetRecordCount();
       }

       /// <summary>
       /// Display master details
       /// </summary>
       /// <param name="intRowNumber">row number</param>
       /// <returns>whether details exists as bool</returns>
       public bool DisplayLeavePolicyMaster( int intRowNumber)
       {
           return DALLeavePolicy.DisplayLeavePolicyMaster(intRowNumber);
       }

       /// <summary>
       /// Save policy details
       /// </summary>
       /// <returns>success/failure</returns>
       public bool LeavePolicyMasterSave()
       {
           bool blnRetValue = false;
           try
           {
               DALLeavePolicy.DataLayer.BeginTransaction();
               blnRetValue = DALLeavePolicy.LeavePolicyMasterSave();
               if (blnRetValue)
               {
                   DALLeavePolicy.LeavePolicyDetailsSave();
                   DALLeavePolicy.LeavePolicyConsequenceSave();
                   DALLeavePolicy.DataLayer.CommitTransaction();
               }
               else
               {
                   DALLeavePolicy.DataLayer.RollbackTransaction();
               }
           }
           catch
           {
               DALLeavePolicy.DataLayer.RollbackTransaction();
               throw;
           }
           return blnRetValue;
       }

       /// <summary>
       /// Delete policy
       /// </summary>
       /// <returns>success/failure</returns>
       public bool DeleteLeavePolicy()
       {
           return DALLeavePolicy.DeleteLeavePolicy();
       }

       /// <summary>
       /// Get policy details
       /// </summary>
       /// <param name="intLeavePolicyID">policy id</param>
       /// <returns>datatable of details</returns>
       public DataTable DispalyPolicyDetail(int intLeavePolicyID)
       {
           return DALLeavePolicy.DispalyPolicyDetail(intLeavePolicyID);
       }

       /// <summary>
       /// get policy consequences
       /// </summary>
       /// <param name="intLeavePolicyID">policy id</param>
       /// <returns>datatable of consequences</returns>
       public DataTable DispalyPolicyConsequenceDetail(int intLeavePolicyID)
       {
           return DALLeavePolicy.DispalyPolicyConsequenceDetail(intLeavePolicyID);
       }

       /// <summary>
       /// Check if duplicate name exists
       /// </summary>
       /// <param name="iPolicyID">policy id</param>
       /// <param name="intComPID">company id</param>
       /// <param name="strPolicyName">policy name</param>
       /// <returns>duplicate exists/not</returns>
       public bool CheckDuplicatePolicyName(int iPolicyID,  string strPolicyName)
       {
           return DALLeavePolicy.CheckDuplicatePolicyName(iPolicyID, strPolicyName);
       }

       /// <summary>
       /// Check if salary is processed against a policyid
       /// </summary>
       /// <param name="iPolicyID">policy id</param>
       /// <returns>Salary Processed/not</returns>
       public bool CheckSalaryProcessed(int iPolicyID)
       {
           return DALLeavePolicy.CheckSalaryProcessed(iPolicyID);
       }

       /// <summary>
       /// Fill all the combos
       /// </summary>
       /// <param name="saFieldValues">query</param>
       /// <returns>datatable of details</returns>
       public DataTable FillCombos(string[] saFieldValues)
       {
           return DALLeavePolicy.FillCombos(saFieldValues);
       }

       /// <summary>
       /// check if policy updation is permitted /not
       /// </summary>
       /// <param name="iPolicyID">policyid</param>
       /// <param name="iCmpID">companyid</param>
       /// <param name="ileaveTypeID">leavetypeid</param>
       /// <param name="EnCashDays">encashdays</param>
       /// <param name="CarryForwardDays">carryforward days</param>
       /// <param name="balLeaves">balance day</param>
       /// <param name="monthleave">monthly leave</param>
       /// <returns></returns>
       public bool CheckPolicyUpdation(int iPolicyID, int iCmpID, int ileaveTypeID, decimal EnCashDays, decimal CarryForwardDays, decimal balLeaves, decimal monthleave)
       {
           return DALLeavePolicy.CheckPolicyUpdation(iPolicyID, iCmpID, ileaveTypeID, EnCashDays, CarryForwardDays, balLeaves, monthleave);
       }

       /// <summary>
       /// Checks if policy is used by an employee
       /// </summary>
       /// <param name="iPolicyID">policy id</param>
       /// <returns>yes/no</returns>
       public bool PolicyIDExists(int iPolicyID)
       {
           return DALLeavePolicy.PolicyIDExists(iPolicyID);
       }

       /// <summary>
       /// Get days allotted for a leavetype in a policy
       /// </summary>
       /// <param name="iPolicyID">policyid</param>
       /// <param name="iLeaveTypeid">leavetypeid</param>
       /// <returns>number of days</returns>
       public int GetNoOfDays(int iPolicyID, int iLeaveTypeid)
       {
           return DALLeavePolicy.GetNoOfDays(iPolicyID, iLeaveTypeid);
       }


       public int GetMinDiff(int iPolicyID, int iLeaveTypeid)
       {
           return DALLeavePolicy.GetMinDiff(iPolicyID, iLeaveTypeid);
       }
       public int DeletLeavePolicyDetails(int iPolicyID, int iSerialNo)
       {
           return DALLeavePolicy.DeletLeavePolicyDetails(iPolicyID, iSerialNo);
       }
       public int DeletLeaveConseqDetails(int iPolicyID, int iSerialNo)
       {
           return DALLeavePolicy.DeletLeaveConseqDetails(iPolicyID, iSerialNo);
       }
    }
}
