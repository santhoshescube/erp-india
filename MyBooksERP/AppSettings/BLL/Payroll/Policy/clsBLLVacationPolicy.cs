﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using DAL;
using DTO;
using System.Data;
namespace MyBooksERP
{
  

    public class clsBLLVacationPolicy
    {
        #region Declartions

        private DataLayer MobjDataLayer;

        clsDALVacationPolicy MobjclsDALVacationPolicy;
        clsDTOVacationPolicy MobjclsDTOVacationPolicy;

        public clsBLLVacationPolicy()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALVacationPolicy = new clsDALVacationPolicy(MobjDataLayer);
            MobjclsDTOVacationPolicy = new clsDTOVacationPolicy();
            MobjclsDALVacationPolicy.DTOVacationPolicy = MobjclsDTOVacationPolicy;

        }

        public clsDTOVacationPolicy clsDTOVacationPolicy
        {
            get { return MobjclsDTOVacationPolicy; }
            set { MobjclsDTOVacationPolicy = value; }
        }
        #endregion Declartions

        #region FillCombos
        public System.Data.DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALVacationPolicy.FillCombos(saFieldValues);
        }
        #endregion FillCombos

        #region checkDuplication
        public bool checkDuplication(string strName)
        {
            return MobjclsDALVacationPolicy.checkDuplication(strName);
        }
        #endregion checkDuplication

        #region InsertVacationPolicyMaster
        public bool InsertVacationPolicyMaster()
        {
            return MobjclsDALVacationPolicy.InsertVacationPolicyMaster();
        }
        #endregion InsertVacationPolicyMaster

        #region UpdateVacationPolicyMaster
        public bool UpdateVacationPolicyMaster()
        {
            return MobjclsDALVacationPolicy.UpdateVacationPolicyMaster();
        }
        #endregion UpdateVacationPolicyMaster

        #region InsertVacationPolicyDetails
        public bool InsertVacationPolicyDetails()
        {
            return MobjclsDALVacationPolicy.InsertVacationPolicyDetails();
        }
        #endregion InsertVacationPolicyDetails

        #region RecCountNavigate

        public int RecCountNavigate()
        {
            return MobjclsDALVacationPolicy.RecCountNavigate();
        }
        #endregion RecCountNavigate

        #region RecCount
        public int RecCount()
        {
            return MobjclsDALVacationPolicy.RecCount();
        }
        #endregion RecCount

        #region SelectVacationPolicy
        public DataSet SelectVacationPolicy(int intRowNumber)
        {
            return MobjclsDALVacationPolicy.SelectVacationPolicy(intRowNumber);
        }
        #endregion SelectVacationPolicy

        #region getVacationPolicyRowNumber
        public int getVacationPolicyRowNumber(int intPolicyID)
        {
            return MobjclsDALVacationPolicy.getVacationPolicyRowNumber(intPolicyID);
        }
        #endregion getVacationPolicyRowNumber

        #region getParticular
        public DataTable getParticular()
        {
            return MobjclsDALVacationPolicy.getParticular();
        }
        #endregion getParticular

        #region InsertSalaryPolicyDetail
        public bool InsertSalaryPolicyDetail() 
       {
           return MobjclsDALVacationPolicy.InsertSalaryPolicyDetail();
       }
        #endregion InsertSalaryPolicyDetail

        #region checkVacationPolicyUsed

        public bool checkVacationPolicyUsed() 
        {
            return MobjclsDALVacationPolicy.checkVacationPolicyUsed();

        }
        #endregion checkVacationPolicyUsed

        #region DeleteVacationPolicy
        public bool DeleteVacationPolicy()
        {
            return MobjclsDALVacationPolicy.DeleteVacationPolicy();
        }
        #endregion DeleteVacationPolicy

        #region SelectVacationPolicyByVacationPolicyID
        public DataSet SelectVacationPolicyByVacationPolicyID(int intVacationPolicyID)
        {
            return MobjclsDALVacationPolicy.SelectVacationPolicyByVacationPolicyID(intVacationPolicyID);
        }
        #endregion SelectVacationPolicyByVacationPolicyID

        #region DeleteSalaryPolicyDetail
        public bool DeleteSalaryPolicyDetail()
        {
            return MobjclsDALVacationPolicy.DeleteSalaryPolicyDetail();
        }
        #endregion DeleteSalaryPolicyDetail

        #region CheckploicyNameDup
        public bool CheckploicyNameDup(int intPolicyID, string strPolicyName)
        {
            return MobjclsDALVacationPolicy.CheckploicyNameDup(intPolicyID, strPolicyName);
        }
        #endregion CheckploicyNameDup


    }
}
