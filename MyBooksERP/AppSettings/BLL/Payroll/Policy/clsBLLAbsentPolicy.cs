﻿using System;
using System.Collections.Generic;
using System.Data;

namespace MyBooksERP
{ /*********************************************
       * Author       : Arun
       * Created On   : 10 Apr 2012
       * Purpose      : Absent Policy BLL 
       * ******************************************/

    public class clsBLLAbsentPolicy
    {
        #region Declartions
        
        clsDALAbsentPolicy MobjDALAbsentPolicy = null;

        public clsBLLAbsentPolicy()
        {

        }

        private clsDALAbsentPolicy DALAbsentPolicy
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALAbsentPolicy == null)
                    this.MobjDALAbsentPolicy = new clsDALAbsentPolicy();

                return this.MobjDALAbsentPolicy;
            }
        }

        public clsDTOAbsentPolicy DTOAbsentPolicy
        {
            get { return this.DALAbsentPolicy.DTOAbsentPolicy; }
        }
        #endregion Declartions

        #region GetRowNumber
       
        public int GetRowNumber(int intAbsentPolicyID)
        {
            return DALAbsentPolicy.GetRowNumber(intAbsentPolicyID);
        }
        #endregion GetRowNumber

        #region GetRecordCount
        public int GetRecordCount()
        {
            return DALAbsentPolicy.GetRecordCount();
        }
        #endregion GetRecordCount

        #region AbsentPolicyMasterSave
        public bool AbsentPolicyMasterSave()
        {
            bool blnRetValue = false;
            try
            {
                DALAbsentPolicy.DataLayer.BeginTransaction();
                blnRetValue = DALAbsentPolicy.AbsentPolicyMasterSave();
                if (blnRetValue)
                {
                    DALAbsentPolicy.DeleteAbsentPolicyDetails();
                    DALAbsentPolicy.AbsentPolicyDetailSave();
                    DALAbsentPolicy.SalaryPolicyDetailsSave();

                    DALAbsentPolicy.DataLayer.CommitTransaction();
                }
                else
                {
                    DALAbsentPolicy.DataLayer.RollbackTransaction();
                }
            }
            catch
            {
                DALAbsentPolicy.DataLayer.RollbackTransaction();
                throw;
            }
            return blnRetValue;

        }
        #endregion AbsentPolicyMasterSave

        #region DisplayAbsentPolicy
        
        public DataTable DisplayAbsentPolicy(int intRowNumber)
        {
            return DALAbsentPolicy.DisplayAbsentPolicy(intRowNumber);
        }
        #endregion DisplayAbsentPolicy

        #region DisplayAddDedDetails
       
        public DataTable DisplayAddDedDetails(int iPolicyID, int iType)
        {
            return DALAbsentPolicy.DisplayAddDedDetails(iPolicyID, iType);
        }
        #endregion DisplayAddDedDetails

        #region DeleteAbsentPolicy
        public bool DeleteAbsentPolicy()
        {
            return DALAbsentPolicy.DeleteAbsentPolicy();
        }
        #endregion DeleteAbsentPolicy

        #region GetAdditionDeductions
        public DataTable GetAdditionDeductions()
        {
            return DALAbsentPolicy.GetAdditionDeductions();
        }
        #endregion GetAdditionDeductions

        #region CheckDuplicate
       
        public bool CheckDuplicate(int iPolicyID, string strPolicyName)
        {
            return DALAbsentPolicy.CheckDuplicate(iPolicyID, strPolicyName);
        }
        #endregion CheckDuplicate

        #region PolicyIDExists
       
        public bool PolicyIDExists(int iPolicyID)
        {
            return DALAbsentPolicy.PolicyIDExists(iPolicyID);
        }
        #endregion PolicyIDExists

        #region FillCombos
     
        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALAbsentPolicy.FillCombos(saFieldValues);
        }
        #endregion FillCombos

    }
}
