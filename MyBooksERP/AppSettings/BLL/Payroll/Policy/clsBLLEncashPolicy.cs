﻿using System;
using System.Collections.Generic;
using System.Data;

namespace MyBooksERP
{ /*********************************************
       * Author       : Arun
       * Created On   : 10 Apr 2012
       * Purpose      : Encash Policy BLL 
       * ******************************************/

    public class clsBLLEncashPolicy
    {

        clsDALEncashPolicy MobjDALEncashPolicy = null;

        public clsBLLEncashPolicy()
        {

        }

        private clsDALEncashPolicy DALEncashPolicy
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALEncashPolicy == null)
                    this.MobjDALEncashPolicy = new clsDALEncashPolicy();

                return this.MobjDALEncashPolicy;
            }
        }

        public clsDTOEncashPolicy DTOEncashPolicy
        {
            get { return this.DALEncashPolicy.DTOEncashPolicy; }
        }

        public int GetRowNumber(int intEncashPolicyID)
        {
            return DALEncashPolicy.GetRowNumber(intEncashPolicyID);
        }
        public int GetRecordCount()
        {
            return DALEncashPolicy.GetRecordCount();
        }
        public bool EncashPolicyMasterSave()
        {
            bool blnRetValue = false;
            try
            {
                DALEncashPolicy.DataLayer.BeginTransaction();
                blnRetValue = DALEncashPolicy.EncashPolicyMasterSave();
                if (blnRetValue)
                {
                    DALEncashPolicy.DeleteEncashPolicyDetails();
                    //DALEncashPolicy.EncashPolicyDetailSave();
                    DALEncashPolicy.SalaryPolicyDetailsSave();

                    DALEncashPolicy.DataLayer.CommitTransaction();
                }
                else
                {
                    DALEncashPolicy.DataLayer.RollbackTransaction();
                }
            }
            catch 
            {
                DALEncashPolicy.DataLayer.RollbackTransaction();
                throw;
            }
            return blnRetValue;
            
        }
        public DataTable DisplayEncashPolicy(int intRowNumber)
        {
            return DALEncashPolicy.DisplayEncashPolicy(intRowNumber);
        }
        public DataTable DisplayAddDedDetails(int iPolicyID, int iType)
        {
            return DALEncashPolicy.DisplayAddDedDetails(iPolicyID, iType);
        }

        public bool DeleteEncashPolicy()
        {
            return DALEncashPolicy.DeleteEncashPolicy();
        }

        public DataTable GetAdditionDeductions()
        {
            return DALEncashPolicy.GetAdditionDeductions();
        }
        public bool CheckDuplicate(int iPolicyID, string strPolicyName)
        {
            return DALEncashPolicy.CheckDuplicate(iPolicyID, strPolicyName);
        }
        public bool PolicyIDExists(int iPolicyID)
        {
            return DALEncashPolicy.PolicyIDExists(iPolicyID);
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALEncashPolicy.FillCombos(saFieldValues);
        }


    }
}
