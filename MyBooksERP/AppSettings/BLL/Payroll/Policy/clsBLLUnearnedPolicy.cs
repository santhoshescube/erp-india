﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
   public class clsBLLUnearnedPolicy
   {
       #region Declaration
     
       clsDALUnearnedPolicy MobjclsDALUnearnedPolicy = null;

        public clsBLLUnearnedPolicy()
        {

        }

        private clsDALUnearnedPolicy clsDALUnearnedPolicy
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjclsDALUnearnedPolicy == null)
                    this.MobjclsDALUnearnedPolicy = new clsDALUnearnedPolicy();

                return this.MobjclsDALUnearnedPolicy;
            }
        }

        public clsDTOUnearnedPolicy DTOUnearnedPolicy
        {
            get { return this.clsDALUnearnedPolicy.DTOUnearnedPolicy; }
        }
       #endregion Declaration

       #region GetRowNumber
        public int GetRowNumber(int IntUnearnedID)
        {
            return clsDALUnearnedPolicy.GetRowNumber(IntUnearnedID);
        }
        #endregion GetRowNumber

       #region GetRecordCount
        public int GetRecordCount()
        {
            return clsDALUnearnedPolicy.GetRecordCount();
        }
        #endregion GetRecordCount

       #region UnearnedPolicyMasterSave
        public bool UnearnedPolicyMasterSave()
        {
            bool blnRetValue = false;
            try
            {
                clsDALUnearnedPolicy.DataLayer.BeginTransaction();
                blnRetValue = clsDALUnearnedPolicy.UnearnedPolicyMasterSave();
                if (blnRetValue)
                {
                    clsDALUnearnedPolicy.DeleteUnearnedPolicyDetails();
                    clsDALUnearnedPolicy.UnearnedPolicyDetailSave();
                    clsDALUnearnedPolicy.SalaryPolicyDetailsSave();

                    clsDALUnearnedPolicy.DataLayer.CommitTransaction();
                }
                else
                {
                    clsDALUnearnedPolicy.DataLayer.RollbackTransaction();
                }
            }
            catch
            {
                clsDALUnearnedPolicy.DataLayer.RollbackTransaction();
                throw;
            }
            return blnRetValue;

        }
        #endregion UnearnedPolicyMasterSave

       #region DisplayUnearnedPolicy
        public DataTable DisplayUnearnedPolicy(int intRowNumber)
        {
            return clsDALUnearnedPolicy.DisplayUnearnedPolicy(intRowNumber);
        }
        #endregion DisplayUnearnedPolicy

       #region DisplayAddDedDetails
        public DataTable DisplayAddDedDetails(int iPolicyID, int iType)
        {
            return clsDALUnearnedPolicy.DisplayAddDedDetails(iPolicyID, iType);
        }
        #endregion DisplayAddDedDetails

       #region DeleteUnearnedPolicy
        public bool DeleteUnearnedPolicy()
        {
            return clsDALUnearnedPolicy.DeleteUnearnedPolicy();
        }
        #endregion DeleteUnearnedPolicy

       #region GetAdditionDeductions
        public DataTable GetAdditionDeductions()
        {
            return clsDALUnearnedPolicy.GetAdditionDeductions();
        }
        #endregion GetAdditionDeductions

       #region CheckDuplicate
        public bool CheckDuplicate(int iPolicyID, string strPolicyName)
        {
            return clsDALUnearnedPolicy.CheckDuplicate(iPolicyID, strPolicyName);
        }
       #endregion CheckDuplicate

       #region PolicyIDExists
        public bool PolicyIDExists(int iPolicyID)
        {
            return clsDALUnearnedPolicy.PolicyIDExists(iPolicyID);
        }
        #endregion PolicyIDExists

       #region FillCombos
        public DataTable FillCombos(string[] saFieldValues)
        {
            return clsDALUnearnedPolicy.FillCombos(saFieldValues);
        }
        #endregion FillCombos

    
    }
}
