﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;

namespace MyBooksERP
{
    /*********************************************
      * Author         : Arun
      * Created On     : 11 Apr 2012
      * Purpose        : Holiday Calendar BLL 
      * Modified by    : Siny
      * Modified date  : 16 Aug 2013
      * Description    : Tuning performance improvement
      * ******************************************/
    public class clsBLLHolidayCalendar
    {

        clsDALHolidayCalendar MobjDALHolidayCalendar = null;

        public clsBLLHolidayCalendar(){ }

        #region Properties
        private clsDALHolidayCalendar DALHolidayCalendar
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALHolidayCalendar == null)
                    this.MobjDALHolidayCalendar = new clsDALHolidayCalendar();

                return this.MobjDALHolidayCalendar;
            }
        }

        public clsDTOHolidayCalendar DTOHolidayCalendar
        {
            get { return this.DALHolidayCalendar.DTOHolidayCalendar; }
        }
        #endregion properties

        #region InsertHoiday
        /// <summary>
        /// insert holiday details
        /// </summary>
        /// <returns>success/failure</returns>
        public bool InsertHoiday()
        {
            return DALHolidayCalendar.InsertHoiday();
        }
        #endregion InsertHoiday

        #region UpdateHoliday
        /// <summary>
        /// Update holiday details
        /// </summary>
        /// <param name="iCmpID">companyid</param>
        /// <param name="strDate">date</param>
        /// <param name="iHolidayType">holiday type</param>
        /// <param name="strRemarks">remarks</param>
        /// <returns>success/failure</returns>
        public bool UpdateHoliday(int iCmpID, string strDate, int iHolidayType, string strRemarks)
        {
            return DALHolidayCalendar.UpdateHoliday(iCmpID, strDate, iHolidayType, strRemarks);
        }
        #endregion UpdateHoliday

        #region DeleteHoliday
        /// <summary>
        /// Delete holiday details
        /// </summary>
        /// <param name="iCompID">companyID</param>
        /// <param name="strDate">date</param>
        /// <returns></returns>
        public bool DeleteHoliday(int iCompID, string strDate)
        {
            return DALHolidayCalendar.DeleteHoliday(iCompID, strDate);
        }
        #endregion DeleteHoliday

        #region GetMonths
        /// <summary>
        /// Get months
        /// </summary>
        /// <param name="strYear">year</param>
        /// <returns>datatable of months</returns>
        public DataTable GetMonths(string strYear)
        {
            return DALHolidayCalendar.GetMonths(strYear);
        }
        #endregion GetMonths

        #region GetCompanyOffDay
        /// <summary>
        /// Get company off days
        /// </summary>
        /// <param name="intCmpID">companyid</param>
        /// <returns>string array of offdays</returns>
        public string[] GetCompanyOffDay(int intCmpID)
        {
            return DALHolidayCalendar.GetCompanyOffDay(intCmpID);
        }
        #endregion GetCompanyOffDay

        /// <summary>
        /// get ll the details of company holiday of a specific day
        /// </summary>
        /// <param name="intCmpID">companyid</param>
        /// <param name="strDate">date</param>
        /// <param name="strRemarks">returns remarks </param>
        /// <param name="strColor">returns color name</param>
        /// <param name="intHolidayType">returns holiday type</param>
        /// <returns>holiday details exists or not</returns>
        public bool GetCompanyHolidayDetails(int intCmpID, string strDate, ref string strRemarks, ref string strColor, ref int intHolidayType)
        {
            return DALHolidayCalendar.GetCompanyHolidayDetails(intCmpID, strDate, ref strRemarks, ref strColor, ref intHolidayType);
        }

        /// <summary>
        /// Check if holiday exixts
        /// </summary>
        /// <param name="intCmpID">companyid</param>
        /// <param name="strDate">date</param>
        /// <returns>holiday exists/not</returns>
        public bool CheckHolidayExists(int intCmpID, string strDate)
        {
            return DALHolidayCalendar.CheckHolidayExists(intCmpID, strDate);
        }

        /// <summary>
        /// Check if payment for that month exists
        /// </summary>
        /// <param name="intCmpID">companyid</param>
        /// <param name="strDate">date</param>
        /// <returns>payment exists/not</returns>
        public bool CheckPaymentExists(int intCmpID, string strDate)
        {
            return DALHolidayCalendar.CheckPaymentExists(intCmpID, strDate);
        }

        /// <summary>
        /// Get all holiday types
        /// </summary>
        /// <returns>datatable of holidays</returns>
        public DataTable GetHolidayTypes()
        {
            return DALHolidayCalendar.GetHolidayTypes();
        }

        /// <summary>
        /// Get Holiday Type Details
        /// </summary>
        /// <param name="strHolidayType">holiday type</param>
        /// <param name="strColr">returns color name</param>
        /// <param name="intHolidayTypeID">returns typeid</param>
        /// <returns></returns>
        public bool GetHolidayTypeDetails(string strHolidayType, ref string strColr, ref int intHolidayTypeID)
        {
            return DALHolidayCalendar.GetHolidayTypeDetails(strHolidayType, ref strColr, ref intHolidayTypeID);
        }

        /// <summary>
        /// Fill the combos
        /// </summary>
        /// <param name="saFieldValues">string array of fields</param>
        /// <returns>datatable of fields</returns>
        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALHolidayCalendar.FillCombos(saFieldValues);
        }


    }
}
