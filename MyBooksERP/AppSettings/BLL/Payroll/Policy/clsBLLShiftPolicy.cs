﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DAL;
using DTO;
/* 
=================================================
   Author:		<Author,,Laxmi>
   Create date: <Create Date,,21 Feb 2011>
   Description:	<Description,,Shift Policy BLL Class>
================================================
*/

namespace BLL
{
    public class clsBLLShiftPolicy
    {

        clsDALShiftPolicy MobjclsDALShiftPolicy;
        clsDTOShiftPolicy MobjclsDTOShiftPolicy;
        private DataLayer MobjDataLayer;

        public clsBLLShiftPolicy()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALShiftPolicy = new clsDALShiftPolicy(MobjDataLayer);
            MobjclsDTOShiftPolicy = new clsDTOShiftPolicy();
            MobjclsDALShiftPolicy.objClsDTOShiftPolicy = MobjclsDTOShiftPolicy;
        }

        public clsDTOShiftPolicy clsDTOShiftPolicy
        {
            get { return MobjclsDTOShiftPolicy; }
            set { MobjclsDTOShiftPolicy = value; }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALShiftPolicy.FillCombos(saFieldValues);
        }

        public int Insert(bool AddStatus)
        {
            return MobjclsDALShiftPolicy.Insert(AddStatus);
        }

        public bool Delete()
        {
            return MobjclsDALShiftPolicy.Delete();
        }
        public DataSet GetAllShifts()
        {
            return MobjclsDALShiftPolicy.GetAllShifts();
        }

        public SqlDataReader GetShiftDetails()
        {
            return MobjclsDALShiftPolicy.GetShiftDetails();
        }
        //public DataSet GetDynamicShiftDetails() // for Dynamic Shift tab
        //{
        //    return MobjclsDALShiftPolicy.GetDynamicShiftDetails();
        //}
        public DataSet GetDynamicShiftDetails()
        {
            return MobjclsDALShiftPolicy.GetDynamicShiftDetails();
        }

        public bool IsExists(int intMode)
        {
            return MobjclsDALShiftPolicy.IsExists(intMode);
        }
        public bool CheckDuplication(bool bAddStatus, string[] saValues, int iId, int iType)
        {
            //function for checking duplication
            return MobjclsDALShiftPolicy.CheckDuplication(bAddStatus, saValues, iId, iType);
        }

        public DataSet GetMinFromHour(decimal decMint)
        {
            return MobjclsDALShiftPolicy.GetMinFromHour(decMint);
        }
        public DataSet GetHourFromMin(decimal decMint)
        {
            return MobjclsDALShiftPolicy.GetHourFromMin(decMint);
        }


    }
}