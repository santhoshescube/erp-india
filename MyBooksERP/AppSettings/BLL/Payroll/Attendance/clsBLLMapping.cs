﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAL;
using DTO;

namespace BLL
{
    public class clsBLLMapping
    {
        clsDALMapping  MobjclsDALMapping;
        clsDTOMapping  MobjclsDTOMapping;
        private DataLayer MobjDataLayer;

        public clsBLLMapping()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALMapping = new clsDALMapping(MobjDataLayer);
            MobjclsDTOMapping = new clsDTOMapping ();
            MobjclsDALMapping.PobjclsDTOMapping = MobjclsDTOMapping ;
        }

        public clsDTOMapping clsDTOMapping
        {
            get { return MobjclsDTOMapping; }
            set { MobjclsDTOMapping  = value; }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALMapping.FillCombos(saFieldValues);
        }

        public void Execute(string sQuery)
        {
             MobjclsDALMapping.Execute(sQuery);
        }
        public DataTable GetAllData()
        {
            return MobjclsDALMapping.GetAllData();
        }

        public DataTable GetMappingEmployeeList()
        {
            return MobjclsDALMapping.GetMappingEmployeeList();
        }
        public bool IsExists()
        {
            return MobjclsDALMapping.IsExists();
        }
        public int SaveTemplate()
        {
            return  MobjclsDALMapping.SaveTemplate();
        }
    }
}
