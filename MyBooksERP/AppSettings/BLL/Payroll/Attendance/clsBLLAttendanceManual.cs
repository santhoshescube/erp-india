﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLAttendanceManual
    {
        clsDALAttendanceManual MobjclsDALAttendanceManual;
        clsDTOAttendanceManual MobjclsDTOAttendanceManual;
        private DataLayer MobjDataLayer;

        public clsDTOAttendanceManual clsDTOAttendanceManual
        {
            get { return MobjclsDTOAttendanceManual; }
            set { MobjclsDTOAttendanceManual = value; }
        }
        public clsBLLAttendanceManual()
        {

            MobjclsDALAttendanceManual = new clsDALAttendanceManual();
            MobjclsDTOAttendanceManual = new clsDTOAttendanceManual();

            MobjDataLayer = new DataLayer();
            MobjclsDALAttendanceManual.MobjclsDTOAttendanceManual = clsDTOAttendanceManual;
        }

        public DataTable GetAttendanceDetails(int intCurrentPage, int intPageRowCount)
        {
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.GetAttendanceDetails(intCurrentPage, intPageRowCount);
        }

        public DataTable GetAttendanceDetailsPageCount(int intCurrentPage, int intPageRowCount)
        {
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.GetAttendanceDetailsPageCount(intCurrentPage, intPageRowCount);
        }

        public bool SaveAttendanceManual()
        {
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.SaveAttendanceManual();
        }
        public bool SaveManualAttendanceNew(int AttendanceID, int EmployeeID, int CompanyID, string AttDate, int PolicyID, int ShiftID, string WorkTime, string OT, int AbsentTime, int AttendenceStatusID, int LeaveID, int LeaveType, int IsHalfDayLeave, int IsPaidLeave, int ConsequenceID, int WorkLocation)
        {
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.SaveManualAttendanceNew(AttendanceID, EmployeeID, CompanyID, AttDate, PolicyID, ShiftID, WorkTime, OT, AbsentTime, AttendenceStatusID, LeaveID, LeaveType, IsHalfDayLeave, IsPaidLeave, ConsequenceID, WorkLocation);
        }

        public bool SaveManualAttendanceOT(int EmployeeID, string strDate, string sOTHour, decimal dOTDays)
        {
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.SaveManualAttendanceOT(EmployeeID, strDate, sOTHour, dOTDays);
        }

        public bool DeleteAttendanceManual()
        {
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.DeleteAttendanceManual();
        }

        public DataTable GetEmployeeShiftInfo(int iEmpID, string sDate)
        {
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.GetEmployeeShiftInfo(iEmpID, sDate);
        }

        public bool IsSalaryReleased(int iEmpID, string sDate)
        {
            //Checking Salary Is Released or processed
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.IsSalaryReleased(iEmpID, sDate);
        }

        //public bool IsVacationExists(int iEmpID, string sDate)
        //{
        //    //Checking Vaction Is Exists Or not
        //    MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
        //    return MobjclsDALAttendanceManual.IsVacationExists(iEmpID, sDate);
        //}

        public DateTime GetEmployeeJoiningDate(int iEmpID)
        {
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.GetEmployeeJoiningDate(iEmpID);
        }

        public int GetTotalWorkTime(int iEmpID, string strDate, int iAttendanceID)
        {
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.GetTotalWorkTime(iEmpID, strDate, iAttendanceID);
        }

        public int GetTotalTime(int iEmpID, string strDate, int iAttendanceID)
        {
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.GetTotalTime(iEmpID, strDate, iAttendanceID);
        }

        public bool IsOTExists(int iEmpID, string strDate, int iAttendanceID)
        {
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.IsOTExists(iEmpID, strDate, iAttendanceID);
        }

        public bool IsAttendanceManualExists(int iCmpID, string sDate, int iPrjID)
        {
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.IsAttendanceManualExists(iCmpID, sDate, iPrjID);
        }

        public bool IsWorkTimeExists(int iEmpID, string sDate, int iAtnID)
        {
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.IsWorkTimeExists(iEmpID, sDate, iAtnID);
        }

        public DataTable GetShiftOTDetails(int iShiftID)
        {
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.GetShiftOTDetails(iShiftID);
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            MobjclsDALAttendanceManual.objConnection = MobjDataLayer;
            return MobjclsDALAttendanceManual.FillCombos(saFieldValues);
        }
    }
}
