﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace MyBooksERP
{
    /*****************************************************
     * Created By       : Arun
     * Creation Date    : 16 Apr 2012
     * Description      : Handle Attendance
     * ***************************************************/
    /*****************************************************
    * Modified By      : Sanju
    * Creation Date    : 25 Aug 2013
    * Description      : Code optimization
    * FORM ID          : 121
    * ***************************************************/
    public class clsBLLAttendance
    {

        clsDALAttendance MobjclsDALAttendance;
        clsDTOAttendance MobjclsDTOAttendance;
        clsDTOAttendanceDeviceLogs MobjclsDTOAttendanceDeviceLogs;
        private DataLayer MobjDataLayer;

        public clsBLLAttendance()
        {
            MobjclsDALAttendance = new clsDALAttendance();
            MobjclsDTOAttendance = new clsDTOAttendance();
            MobjclsDTOAttendanceDeviceLogs = new clsDTOAttendanceDeviceLogs();

            MobjDataLayer = new DataLayer();
            MobjclsDALAttendance.PobjclsDTOAttendance = clsDTOAttendance;
            MobjclsDALAttendance.PobjclsDTOAttendanceDeviceLogs = clsDTOAttendanceDeviceLogs;

        }
        public clsDTOAttendance clsDTOAttendance
        {
            get { return MobjclsDTOAttendance; }
            set { MobjclsDTOAttendance = value; }
        }

        public clsDTOAttendanceDeviceLogs clsDTOAttendanceDeviceLogs
        {
            get { return MobjclsDTOAttendanceDeviceLogs; }
            set { MobjclsDTOAttendanceDeviceLogs = value; }
        }

        public DataTable GetFingerTechDeviceSettings(int intSlid)//get Finger Tech device settings
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetFingerTechDeviceSettings(intSlid);
        }
        public bool SaveAttendanceInfo()
        {
            bool blnReturn = false;
            try
            {
                //MobjDataLayer.BeginTransaction();
                MobjclsDALAttendance.objConnection = MobjDataLayer;
                blnReturn = MobjclsDALAttendance.SaveAttendanceInfo();
                //MobjDataLayer.CommitTransaction();
            }
            catch (Exception Ex)
            {
                //MobjDataLayer.RollbackTransaction();
                throw Ex;

            }


            return blnReturn;

        }

        public bool UpdateAttendanceInfo()
        {
            bool blnReturn = false;
            try
            {
                // MobjDataLayer.BeginTransaction();
                MobjclsDALAttendance.objConnection = MobjDataLayer;

                blnReturn = MobjclsDALAttendance.UpdateAttendanceInfo();
                // MobjDataLayer.CommitTransaction();
            }
            catch (Exception Ex)
            {
                // MobjDataLayer.RollbackTransaction();
                throw Ex;

            }


            return blnReturn;
        }


        public bool DeleteAttendanceDetails(int iAttendanceID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.DeleteAttendanceDetails(iAttendanceID);
        }

        public int IsAttendanceExists(int iEmpID, int iCompanyID, int iShiftID, string sDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.IsAttendanceExists(iEmpID, iCompanyID, iShiftID, sDate);
        }

        public bool IsEmployeeInService(int iEmpID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.IsEmployeeInService(iEmpID);
        }


        public DataTable GetEmployeeInfo(int intMode, string sWorkID, int intTemplateID, int intDayid, string sFromDate, string sToDate,
                                           int intCmpID, int intEmpID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetEmployeeInfo(intMode, sWorkID, intTemplateID, intDayid, sFromDate, sToDate, intCmpID, intEmpID);

        }

        public DataTable GetShiftInfo(int intMode, int intEmpID, int intShiftID, int intDayID, string sFromDate, string sToDate, int intCmpID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetShiftInfo(intMode, intEmpID, intShiftID, intDayID, sFromDate, sToDate, intCmpID);
        }
        public int GetEmpPolicyID(int intEmpID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetEmpPolicyID(intEmpID);
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.FillCombos(saFieldValues);
        }
        public int GetEmployee(string strWorkID, ref string strEmpName)
        {

            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetEmployee(strWorkID, ref strEmpName);
        }
        public double GetMonthlyLeave(int intEmpID, int intCmpID, string atnDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetMonthlyLeave(intEmpID, intCmpID, atnDate);
        }
        public double GetTakenleaves(int iEId, DateTime dt, int intLeaveTypeID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetTakenleaves(iEId, dt, intLeaveTypeID);
        }
        public bool GetShiftInfo(int intShiftID, ref  int intShiftType, ref  string sduration, ref string sminWorkhours, ref int intPolBreak, ref int intLatemin, ref int intEarlyMin)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetShiftInfo(intShiftID, ref intShiftType, ref sduration, ref sminWorkhours, ref intPolBreak, ref intLatemin, ref intEarlyMin);
        }
        public bool GetEmployeePolicy(int intEmpID, int intDayID, ref int iPolicyID)//, ref string sAllowedBreak, ref int iLate, ref int iEarly)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetEmployeePolicy(intEmpID, intDayID, ref iPolicyID);
        }
        public int GetLeaveExAddMinutes(int intEmpID, DateTime dtAtnDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetLeaveExAddMinutes(intEmpID, dtAtnDate);
        }
        public bool GetEmployeeIsHalfDay(int intEmpID, string strFromDate)//'--------------------------Is employee on half day
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetEmployeeIsHalfDay(intEmpID, strFromDate);

        }
        public bool GetPolicyConsequenceWorktime(int iEmpId, ref int iLop, ref int iCasual, ref double dConseqAmt, ref int iConseqID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetPolicyConsequenceWorktime(iEmpId, ref iLop, ref iCasual, ref dConseqAmt, ref iConseqID);
        }
        public bool GetPolicyConsequenceBreaktime(int iPolicyID, ref int iLop, ref int iCasual, ref double dConseqAmt, ref int iConseqID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetPolicyConsequenceBreaktime(iPolicyID, ref iLop, ref iCasual, ref dConseqAmt, ref iConseqID);
        }

        public bool GetPolicyConsequenceLateComing(int iEmpID, ref int iLop, ref int iCasual, ref double dConseqAmt, ref int iConseqID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetPolicyConsequenceLateComing(iEmpID, ref iLop, ref iCasual, ref  dConseqAmt, ref  iConseqID);
        }

        public bool GetPolicyConsequenceEarlyGoing(int iEmpID, ref int iLop, ref int iCasual, ref double dConseqAmt, ref int iConseqID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetPolicyConsequenceEarlyGoing(iEmpID, ref iLop, ref iCasual, ref dConseqAmt, ref iConseqID);
        }
        public int LeaveFromPolicyConsequence(int iCmpID, int iEmpId, int iHalfday, DateTime DtDate, int Mode)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.LeaveFromPolicyConsequence(iCmpID, iEmpId, iHalfday, DtDate, Mode);
        }
        public string GetIP(int ID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetIP(ID);
        }
        public bool IsHoliday(int empId, int iCmpID, DateTime dtDate, int iDayID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.IsHoliday(empId, iCmpID, dtDate, iDayID);
        }
        public string GetLastSaveDate()
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetLastSaveDate();
        }
        public bool SaveAllLogsFromFingerTek()
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.SaveAllLogsFromFingerTek();
        }
        public DataTable GetDeviceLogs()
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetDeviceLogs();
        }
        public DataTable GetDynamicShiftDetails(int iShiftId)
        {

            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetDynamicShiftDetails(iShiftId);
        }
        public bool isAfterMinWrkHrsAsOT(int iShiftID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.isAfterMinWrkHrsAsOT(iShiftID);
        }
        public string GetDynamicShiftDuration(int iShiftID, int iShiftOrderNo, ref string MinWorkingHours, ref string sAllowedBreakTime)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetDynamicShiftDuration(iShiftID, iShiftOrderNo, ref  MinWorkingHours, ref sAllowedBreakTime);
        }

        public bool DeleteAbsentMarked(int iEmpID, int iShiftID, string sDate, int iCompanyID, int iStatusID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.DeleteAbsentMarked(iEmpID, iShiftID, sDate, iCompanyID, iStatusID);
        }
        public DataTable GetAbsentDays(string sStartDate, string sEnddate, int iEmpID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetAbsentDays(sStartDate, sEnddate, iEmpID);
        }
        public bool GetEmployeeInfoForAbsentMarking(int iEmpID, int iDayID, string sDate, ref int iShiftID, ref int iPolicyId, ref int IcomID,
                                                     ref string sDuration, ref string sMinWorkHrs, ref  int iShiftType, ref string sAllowedBreak)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetEmployeeInfoForAbsentMarking(iEmpID, iDayID, sDate, ref iShiftID, ref iPolicyId, ref IcomID,
                                                                          ref  sDuration, ref sMinWorkHrs, ref iShiftType, ref sAllowedBreak);
        }
        public int AbsentSaving(int iEmployeeId, string sAtndate, int ishiftID, int iShiftOrderID,
                                               int iabsent, int iworkpolicyId, int iCompanyID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.AbsentSaving(iEmployeeId, sAtndate, ishiftID, iShiftOrderID, iabsent, iworkpolicyId, iCompanyID);
        }
        public int GetMaxColumnCount(int intEmpID, int intCMPID, DateTime dtDate, bool blnMonthly, bool blnbreakExceed,
                                        string strSearchConditionEmployee, string strSearchConditionAttendance)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetMaxColumnCount(intEmpID, intCMPID, dtDate, blnMonthly, blnbreakExceed, strSearchConditionEmployee, strSearchConditionAttendance);
        }
        public DataTable GetAttendance(string strInOut1, string strInOut, int EmpID, int CmpID, DateTime dtDate, bool blnMonthly,
                                                string strSearchConditionEmployee, string strSearchConditionAttendance)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetAttendance(strInOut1, strInOut, EmpID, CmpID, dtDate, blnMonthly, strSearchConditionEmployee, strSearchConditionAttendance);
        }
        public bool CheckOffDay(int intEmpID, string dayOffWeek, DateTime dtDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.CheckOffDay(intEmpID, dayOffWeek, dtDate);
        }

        public bool CheckHoliday(int intCmpID, string strDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.CheckHoliday(intCmpID, strDate);

        }
        public bool CheckLeave(int intEmpID, string strDate, ref bool leaveflag)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.CheckLeave(intEmpID, strDate, ref leaveflag);

        }
        public DataTable GetEmployeeShiftInfo(int intEmpID, int intCmpID, string strDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetEmployeeShiftInfo(intEmpID, intCmpID, strDate);
        }
        public bool CheckDay(int IntEmpID, int DayID, DateTime dtDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.CheckDay(IntEmpID, DayID, dtDate);
        }
        public DataTable GetDailyShift(int intEmpID, int intDayId, DateTime dtDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetDailyShift(intEmpID, intDayId, dtDate);
        }
        public DataTable GetOffDayShift(int IntEmpID, int DayID, DateTime dtDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetOffDayShift(IntEmpID, DayID, dtDate);
        }
        public DataTable FillEmployes(string sEmpIds, DateTime dtDate, string strSearchConditionEmployee, string strSearchConditionAttendance)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.FillEmployes(sEmpIds, dtDate, strSearchConditionEmployee, strSearchConditionAttendance);

        }
        public bool IsEmployeeOnLeave(int iEmpId, int iCompID, DateTime dCurrentDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.IsEmployeeOnLeave(iEmpId, iCompID, dCurrentDate);
        }
        public bool CheckOffDayShift(int IntEmpID, int DayID, DateTime dtDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.CheckOffDayShift(IntEmpID, DayID, dtDate);
        }

        public DataTable LoadEmployeeGrid(string strEmpFilterType, string strEmpWorkstatus, int intEmpFilter, int intCmpID, int intEmpFilterTypeValue, int intWorkStatusID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.LoadEmployeeGrid(strEmpFilterType, strEmpWorkstatus, intEmpFilter, intCmpID, intEmpFilterTypeValue, intWorkStatusID);
        }
        public bool IsSalaryReleased(int iEid, int iCmpID, DateTime dtdate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.IsSalaryReleased(iEid, iCmpID, dtdate);
        }
        public bool DeleteAttendanceWithLeave(int iEid, int iCmpID, DateTime dtdate, int iLeaveID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.DeleteAttendanceWithLeave(iEid, iCmpID, dtdate, iLeaveID);
        }
        public string GetJoiningDate(int empid)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetJoiningDate(empid);
        }
        public DataTable GetShift(int iShiftId, int intEmpID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetShift(iShiftId, intEmpID);
        }
        public bool DeleteLeaveEntry(int iEmpId, int Cmpid, DateTime dDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.DeleteLeaveEntry(iEmpId, Cmpid, dDate);
        }
        public bool DeleteLeaveFromAttendance(int iEmpID, int iCmpID, DateTime dtDate, int iLeaveID)//
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.DeleteLeaveFromAttendance(iEmpID, iCmpID, dtDate, iLeaveID);
        }
        public bool DeleteAttendance(int intEmpID, int intCmpID, DateTime dtDates)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.DeleteAttendance(intEmpID, intCmpID, dtDates);

        }
        public bool IsAttendanceExistsManual(int iCmpID, int iEmpID, DateTime DtDates)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.IsAttendanceExistsManual(iCmpID, iEmpID, DtDates);
        }
        public string GetEmployeeJoiningDate(int intEmpID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetEmployeeJoiningDate(intEmpID);

        }
        public int LeaveEntry(int iCmpID, int iEmpId, int intHalf, DateTime dtDate, int intLeavetype, int intPaid)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.LeaveEntry(iCmpID, iEmpId, intHalf, dtDate, intLeavetype, intPaid);
        }
        public bool GetLeaveDetails(int iCmpID, int iEmpID, int ileaveTypeID, int iLeavePolicyId, string strFromDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetLeaveDetails(iCmpID, iEmpID, ileaveTypeID, iLeavePolicyId, strFromDate);
        }
        public bool GetAbsentTime(int iShiftid, int iWorkPolicy, int dayid, ref string sBreakTime, ref string sMinWorkHours)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetAbsentTime(iShiftid, iWorkPolicy, dayid, ref sBreakTime, ref sMinWorkHours);

        }
        public double GetEmployeeLeaveStatus(int iEmpId, int iCmpID, string atnDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetEmployeeLeaveStatus(iEmpId, iCmpID, atnDate);

        }
        public double GetEmployeeLeaveStatus(int iEmpId, int intCompID, string atnDate, int intLeaveTypeID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetEmployeeLeaveStatus(iEmpId, intCompID, atnDate, intLeaveTypeID);
        }

        public DataSet ShowCount(DateTime dtpFromDate, int intEmployeeID, string strSearchConditionEmployee, bool blnIsMonthly)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.ShowCount(dtpFromDate, intEmployeeID, strSearchConditionEmployee, blnIsMonthly);
        }
        public string ShowConsequence(int iEmpID, DateTime dtDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.ShowConsequence(iEmpID, dtDate);
        }
        public bool IsConsiderBufferTimeForOT(int iShiftID, ref int iBufferTime, ref   int iMinOtMinutes)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.IsConsiderBufferTimeForOT(iShiftID, ref iBufferTime, ref iMinOtMinutes);
        }
        public bool CheckHalfdayLeave(int iCompID, int iEmpID, DateTime dCurrentDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.CheckHalfdayLeave(iCompID, iEmpID, dCurrentDate);
        }
        public DataTable GetLeaveRemarks(int iCompID, int iEmpID, DateTime dCurrentDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetLeaveRemarks(iCompID, iEmpID, dCurrentDate);
        }
        public DataSet GetBalanceAndHolidaysdetails(int IntCompany, int IntEmployeeID, int IntLeaveTypeID, string strDate)
        {
            return MobjclsDALAttendance.GetBalanceAndHolidaysdetails(IntCompany, IntEmployeeID, IntLeaveTypeID, strDate);
        }
        public int CheckHalfdayLeaveNew(int iCompID, int iEmpID, DateTime dCurrentDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.CheckHalfdayLeaveNew(iCompID, iEmpID, dCurrentDate);
        }
        public DataTable GetOtDetails()
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetOtDetails();
        }
        public int GetTemplateFileType(int intTemplateID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetTemplateFileType(intTemplateID);
        }
        public bool GetTemplateDetails(int intTemplateID, ref bool IsHeaderExists, ref int PunchingCount, ref string DateFormat, ref bool IsTimeWithDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetTemplateDetails(intTemplateID, ref IsHeaderExists, ref PunchingCount, ref  DateFormat, ref IsTimeWithDate);
        }
        public DataTable GetTemplateFieldDetails(int intTemplateID)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.GetTemplateFieldDetails(intTemplateID);
        }

        public bool AttendanceAllReadyExists(int intTemplateID, string strWorkID, string strDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.AttendanceAllReadyExists(intTemplateID, strWorkID, strDate);
        }
        public bool ClearAllPools()
        {
            MobjclsDALAttendance.ClearAllPools();
            return true;
        }
        public DataTable getAttendanceLive(int iEmpID, int DeviceID, DateTime AttDate)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.getAttendanceLive(iEmpID, DeviceID, AttDate);
        }

        public bool SaveAttendanceSummaryConsequence(int EmployeeID, int Month, int Year)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.SaveAttendanceSummaryConsequence(EmployeeID, Month, Year);
        }
        public DataTable CheckConsequence(int EmployeeID, int Month, int Year)
        {
            MobjclsDALAttendance.objConnection = MobjDataLayer;
            return MobjclsDALAttendance.CheckConsequence(EmployeeID, Month, Year);
        }

    }
}
