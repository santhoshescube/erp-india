﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace MyBooksERP
{
    /*****************************************************
* Created By       : Arun
* Creation Date    : 17 Apr 2012
* Description      : Handle Attendance Device Settings
* ***************************************************/
    public class clsBLLAttendanceDeviceSettings
    {
        clsDALAttendanceDeviceSettings MobjclsDALAttendanceDeviceSettings;
        clsDTOAttendanceDeviceSettings MobjDTOAttendanceDeviceSettings;
        private DataLayer MobjDataLayer;


        public clsBLLAttendanceDeviceSettings()
        {
            MobjclsDALAttendanceDeviceSettings = new clsDALAttendanceDeviceSettings();
            MobjDTOAttendanceDeviceSettings = new clsDTOAttendanceDeviceSettings();
            MobjDataLayer = new DataLayer();
            MobjclsDALAttendanceDeviceSettings.PobjclsDTOAttendanceDeviceSettings = MobjDTOAttendanceDeviceSettings;
           // MobjclsDALAttendanceDeviceSettings.PobjclsDTOFingerTechDeviceSettings = MobjDTOAttendanceDeviceSettings;

        }
        public clsDTOAttendanceDeviceSettings clsDTOAttendanceDeviceSettings
        {
            get { return MobjDTOAttendanceDeviceSettings; }
            set { MobjDTOAttendanceDeviceSettings = value; }
        }
        public bool SaveDeviceSettings()
        {

            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                MobjclsDALAttendanceDeviceSettings.objConnection = MobjDataLayer;
                blnRetValue = MobjclsDALAttendanceDeviceSettings.SaveDeviceSettings();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable DisplayDeviceSettings()
        {
             MobjclsDALAttendanceDeviceSettings.objConnection = MobjDataLayer;
             return MobjclsDALAttendanceDeviceSettings.DisplaySettings();
        }
        public bool DeleteDeviceSettings()
        {
            MobjclsDALAttendanceDeviceSettings.objConnection = MobjDataLayer;
            MobjclsDALAttendanceDeviceSettings.DeleteDeviceSettings();
            return true;
        }
        //------------------------------------------------------
        //public DataTable GetDeviceSettings(int ConectionType)
        //{
        //    MobjclsDALAttendanceDeviceSettings.objConnection = MobjDataLayer;
        //    return MobjclsDALAttendanceDeviceSettings.GetDeviceSettings(ConectionType);
        //}
    }
}
