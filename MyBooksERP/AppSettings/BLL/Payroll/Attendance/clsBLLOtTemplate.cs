﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace MyBooksERP
{

    /*********************************************
       * Author       : Arun
       * Created On   : 19 Apr 2012
       * Purpose      : Ot template   BLL 
       * ******************************************/
    public class clsBLLOtTemplate
    {
        clsDALOtTemplate MobjDALOtTemplate = null;

        public clsBLLOtTemplate()
        {

        }

        private clsDALOtTemplate DALOtTemplate
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALOtTemplate == null)
                    this.MobjDALOtTemplate = new clsDALOtTemplate();

                return this.MobjDALOtTemplate;
            }
        }

        public clsDTOOtTemplate DTOOtTemplate
        {
            get { return this.DALOtTemplate.DTOOtTemplate; }
        }

        public DataTable GetOtTemplateDetails()
        {
            return this.DALOtTemplate.GetOtTemplateDetails();
        }
        public bool SaveOtTemplateDetails()
        {
            return this.SaveOtTemplateDetails();

        }
        public bool DeleteOtTemplate()
        {
            return this.DeleteOtTemplate();
        }
    }
}
