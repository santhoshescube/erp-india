﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP//.BLL.FingerTech
{
    /*****************************************************
 * Created By       : Arun
 * Creation Date    : 17 Apr 2012
 * Description      : Handle Employee WorkCode Mapping for Attendance
 * ***************************************************/

    public class clsBLLAttendanceMapping
    {
        clsDALAttendanceMapping MobjclsDALAtnMapping;
        
        List<clsDTOAttendanceMapping> lstobjclsDTOAtnMapping;
        private DataLayer MobjDataLayer;

        public clsBLLAttendanceMapping()
        {

            MobjclsDALAtnMapping = new clsDALAttendanceMapping();
            lstobjclsDTOAtnMapping = new List<clsDTOAttendanceMapping>();
           
            MobjDataLayer = new DataLayer();
        }

        public List<clsDTOAttendanceMapping> lstclsDTOAtnMapping
        {
            get { return lstobjclsDTOAtnMapping; }
            set { lstobjclsDTOAtnMapping = value; }
        }

        public bool SaveAtnMapping()
        {
            bool blnReturnValue=false;
            try
            {
                MobjDataLayer.BeginTransaction();
                MobjclsDALAtnMapping.objConnection = MobjDataLayer;
                MobjclsDALAtnMapping.lstobjclsDTOAtnMapping = lstclsDTOAtnMapping;
                blnReturnValue = MobjclsDALAtnMapping.SaveMappingInfo();
                MobjDataLayer.CommitTransaction();

               

            }
            catch (Exception )
            {
                MobjDataLayer.RollbackTransaction();
            }

            return blnReturnValue;
        }
        public DataTable GetFingerTechDeviceSettings(int intDeviceID)//get Finger Tech device settings
        {
            MobjclsDALAtnMapping.objConnection = MobjDataLayer;
            return MobjclsDALAtnMapping.GetFingerTechDeviceSettings(intDeviceID);
        }

        public DataTable GetEmployees(int iEquipmentId, int iSettingsId, int intTemplateMasterID)
        {
            MobjclsDALAtnMapping.objConnection = MobjDataLayer;
            return MobjclsDALAtnMapping.GetEmployees(iEquipmentId, iSettingsId, intTemplateMasterID);
        }

        public bool DeleteMappings(int iEquipmentID, int iSettingsID,int intTemplateMasterID)
        {
            MobjclsDALAtnMapping.objConnection = MobjDataLayer;
            return MobjclsDALAtnMapping.DeleteMappings(iEquipmentID, iSettingsID, intTemplateMasterID);
        }
        public bool DeleteEmployeeMapping(int iEmpID, int iSettingsID, int intTemplateMasterID)
        {
            MobjclsDALAtnMapping.objConnection = MobjDataLayer;
            return MobjclsDALAtnMapping.DeleteEmployeeMapping(iEmpID, iSettingsID, intTemplateMasterID);
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            MobjclsDALAtnMapping.objConnection = MobjDataLayer;
            return MobjclsDALAtnMapping.FillCombos(saFieldValues);
        }


    }//class
}//NameSpace
