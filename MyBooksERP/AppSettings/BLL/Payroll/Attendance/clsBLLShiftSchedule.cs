﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
/*
 * Created By       : Tijo
 * Created Date     : 22 Aug 2013
 * Purpose          : For Shift Schedule
*/
namespace MyBooksERP
{
    public class clsBLLShiftSchedule
    {
        private clsDALShiftSchedule MobjclsDALShiftSchedule;
        private DataLayer MobjDataLayer;
        public clsDTOShiftSchedule PobjclsDTOShiftSchedule { get; set; }

        public clsBLLShiftSchedule()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALShiftSchedule = new clsDALShiftSchedule(MobjDataLayer);
            PobjclsDTOShiftSchedule = new clsDTOShiftSchedule();
            MobjclsDALShiftSchedule.objclsDTOShiftSchedule = PobjclsDTOShiftSchedule;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public long GetRecordCount()
        {
            return MobjclsDALShiftSchedule.GetRecordCount();
        }

        public DataTable getEmployeeDetails(int intCompanyID, int intDepartmentID, int intDesignationID)
        {
            return MobjclsDALShiftSchedule.getEmployeeDetails(intCompanyID, intDepartmentID, intDesignationID);
        }

        public int SaveShiftScheduleMaster()
        {
            int intRetValue = 0;
            try
            {
                MobjDataLayer.BeginTransaction();
                intRetValue = MobjclsDALShiftSchedule.SaveShiftScheduleMaster();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return intRetValue;
        }

        public bool DeleteShiftScheduleMaster()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALShiftSchedule.DeleteShiftScheduleMaster();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public int getRecordRowNumber(int intShiftScheduleID)
        {
            return MobjclsDALShiftSchedule.getRecordRowNumber(intShiftScheduleID);
        }

        public DataTable DisplayShiftScheduleMaster(int intRowNum)
        {
            return MobjclsDALShiftSchedule.DisplayShiftScheduleMaster(intRowNum);
        }

        public DataTable DisplayShiftScheduleDetails(int intShiftScheduleID, int intCompanyID, int intDepartmentID, int intDesignationID)
        {
            return MobjclsDALShiftSchedule.DisplayShiftScheduleDetails(intShiftScheduleID, intCompanyID, intDepartmentID, intDesignationID);
        }

        public DataSet GetPrintShiftSchedule(int intShiftScheduleID)
        {
            return MobjclsDALShiftSchedule.GetPrintShiftSchedule(intShiftScheduleID);
        }

        public bool CheckShiftScheduleNameExists(int intShiftScheduleID, string strScheduleName)
        {
            return MobjclsDALShiftSchedule.CheckShiftScheduleNameExists(intShiftScheduleID, strScheduleName);
        }

        public void CheckShiftExists(DateTime dtFromDate, DateTime dtToDate, int intShiftScheduleID, string strScheduleName, long lngEmployeeID,
            ref bool blnShiftExists, ref bool blnPaymentExists, ref bool blnAttendanceExists)
        {
            MobjclsDALShiftSchedule.CheckShiftExists(dtFromDate, dtToDate, intShiftScheduleID, strScheduleName, lngEmployeeID,
                ref blnShiftExists, ref blnPaymentExists, ref blnAttendanceExists);
        }

        public bool CheckIsSalaryExistsForDelete(long lngEmployeeID, DateTime dtFromDate, DateTime dtToDate)
        {
            return MobjclsDALShiftSchedule.CheckIsSalaryExistsForDelete(lngEmployeeID,dtFromDate, dtToDate);
        }

        public bool CheckIsShiftScheduleExistsForDelete(int intShiftScheduleID, long lngEmployeeID, DateTime dtFromDate, DateTime dtToDate)
        {
            return MobjclsDALShiftSchedule.CheckIsShiftScheduleExistsForDelete(intShiftScheduleID, lngEmployeeID, dtFromDate, dtToDate);
        }
    }
}
