﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;


namespace MyBooksERP
{
  public class clsBLLSalaryPayment
{
        clsDALSalaryPayment  MobjclsDALSalaryPayment;
        clsDTOSalaryPayment MobjclsDTOSalaryPayment;

        private DataLayer MobjDataLayer;

        public clsBLLSalaryPayment()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALSalaryPayment = new clsDALSalaryPayment(MobjDataLayer);
            MobjclsDTOSalaryPayment = new clsDTOSalaryPayment();
            MobjclsDALSalaryPayment.PobjclsDTOSalaryPayment = MobjclsDTOSalaryPayment;

        }

        public clsDTOSalaryPayment clsDTOSalaryPayment
        {
            get { return MobjclsDTOSalaryPayment; }
            set { MobjclsDTOSalaryPayment = value; }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALSalaryPayment.FillCombos(saFieldValues);
        }

        public DataTable FillCombos(string sQuery)
        {
            //function for getting datatable for filling combo
            return MobjclsDALSalaryPayment.FillCombos(sQuery);
        }

        public int GetCurrencyScale()
        {
            //function for getting datatable for filling combo
            return MobjclsDALSalaryPayment.GetCurrencyScale();
        }

        public bool FillPaymentID()
        {
            return MobjclsDALSalaryPayment.FillPaymentID();
        }

        public DataTable GetSalaryPaymentDetail()
        {
            return MobjclsDALSalaryPayment.GetSalaryPaymentDetail();
        }

        public DataTable NetAmount()
        {
            return MobjclsDALSalaryPayment.NetAmount();
        }

        public int GetPaymentID()
        {
            return MobjclsDALSalaryPayment.GetPaymentID();
        }

        public int RecCountNavigate()
        {
            return MobjclsDALSalaryPayment.RecCountNavigate(); 
        }
        public bool SalarySlipIsEditable()
        {
            return MobjclsDALSalaryPayment.SalarySlipIsEditable();
        }

        public bool ConfirmAll()
        {

            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALSalaryPayment.ConfirmAll();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception)
            {
                MobjDataLayer.RollbackTransaction();
            }
            return blnRetValue;
        }

       public bool ParticularsUpdate()
       {
           return MobjclsDALSalaryPayment.ParticularsUpdate(); 


       }
       public DataSet ParticularsSelect()
       {
           return MobjclsDALSalaryPayment.ParticularsSelect();


       }
       public DataTable LoadReportBody()
       {
           return MobjclsDALSalaryPayment.LoadReportBody(); 
       }

       public bool DelTempTable()
       {
           return MobjclsDALSalaryPayment.DelTempTable(); 
       }
       public int IsAdditionFun()
       {
           return MobjclsDALSalaryPayment.IsAdditionFun(); 
       }
       public bool SavePayment()
       {
           return MobjclsDALSalaryPayment.SavePayment(); 
       }
       public bool SaveUpdatePayment()
       {
           return MobjclsDALSalaryPayment.SaveUpdatePayment();
       }
       public bool IsAccSet()
       {
           return MobjclsDALSalaryPayment.IsAccSet(); 
       }

       public bool DeleteRow()
       {
           return MobjclsDALSalaryPayment.DeleteRow();
       }
       public string GetBookStartDate()
       {
           return MobjclsDALSalaryPayment.GetBookStartDate();
       }
  }
}
