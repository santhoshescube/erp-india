﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;

namespace MyBooksERP
{


     public class clsBLLSalaryRelease
    {
        //
         clsDALSalaryRelease MobjclsDALSalaryRelease;
         clsDTOSalaryRelease MobjclsDTOSalaryRelease;

        private DataLayer MobjDataLayer;

        public clsBLLSalaryRelease()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALSalaryRelease = new clsDALSalaryRelease(MobjDataLayer);
            MobjclsDTOSalaryRelease = new clsDTOSalaryRelease();
            MobjclsDALSalaryRelease.PobjclsDTOSalaryRelease = MobjclsDTOSalaryRelease;
        }

        public clsDTOSalaryRelease clsDTOSalaryRelease
        {
            get { return MobjclsDTOSalaryRelease; }
            set { MobjclsDTOSalaryRelease = value; }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALSalaryRelease.FillCombos(saFieldValues);
        }

        public DataTable FillCombos(string sQuery)
        {
            //function for getting datatable for filling combo
            return MobjclsDALSalaryRelease.FillCombos(sQuery);
        }

        public DataTable DisplayProcessYear()
        {
            //function for getting datatable for filling combo
            return MobjclsDALSalaryRelease.DisplayProcessYear();
        }

        public DataSet GetPaymentDetail()
        {
            //function for getting datatable for filling combo
            return MobjclsDALSalaryRelease.GetPaymentDetail();
        }
        public int GetCurrencyScale()
        {
            //function for getting datatable for filling combo
            return MobjclsDALSalaryRelease.GetCurrencyScale();
        }
        public DataTable  PayDeletePayment()
        {
            //function for getting datatable for filling combo
            return MobjclsDALSalaryRelease.PayDeletePayment();
        }

        public DataTable GetPaymentClassificationCalculationType()
        {
            return MobjclsDALSalaryRelease.GetPaymentClassificationCalculationType();
        }

        public int GetCompanyID()
        {
            return MobjclsDALSalaryRelease.GetCompanyID();
        }
        public bool CheckReleased()
        {
            return MobjclsDALSalaryRelease.CheckReleased();
        }

        public bool DeleteSingleRow(double PaymentID)
        {
            return MobjclsDALSalaryRelease.DeleteSingleRow(PaymentID);
        }

     }





}
