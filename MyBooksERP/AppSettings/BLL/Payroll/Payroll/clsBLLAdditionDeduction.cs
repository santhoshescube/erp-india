﻿using System;
using System.Collections.Generic;
using System.Data;

namespace MyBooksERP
{
     public class clsBLLAdditionDeduction
    {
        private clsDALAdditionDeduction MobjclsDALAdditionDeduction;
        private DataLayer MobjDataLayer;
        public clsDTOAdditionDeduction PobjclsDTOAdditionDeduction { get; set; }

        public clsBLLAdditionDeduction()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALAdditionDeduction = new clsDALAdditionDeduction(MobjDataLayer);
            PobjclsDTOAdditionDeduction  = new clsDTOAdditionDeduction ();
            MobjclsDALAdditionDeduction.objDTOAdditionDeduction = PobjclsDTOAdditionDeduction;
        }

        public bool CheckDuplication(bool blnAddStatus, string[] sarValues, int intId)
        {
            return MobjclsDALAdditionDeduction.CheckDuplication(blnAddStatus, sarValues, intId);
        }
        public int AddDedIDExists(int iAddDedID)
        {
            return MobjclsDALAdditionDeduction.AddDedIDExists(iAddDedID);
        }
        public bool AddDedSave()
        {
            return MobjclsDALAdditionDeduction.AddDedSave();
        }
        public DataTable DisplayAdditionDeduction()
        {
            return MobjclsDALAdditionDeduction.DisplayAdditionDeduction();
        }
        public bool DeleteAdditionDeduction()
        {
            return MobjclsDALAdditionDeduction.DeleteAdditionDeduction();
        }
        public bool AddDedIDExistInSalary(int iAddDedID)
        {
            return MobjclsDALAdditionDeduction.AddDedIDExistInSalary(iAddDedID);
        }

    }
}
