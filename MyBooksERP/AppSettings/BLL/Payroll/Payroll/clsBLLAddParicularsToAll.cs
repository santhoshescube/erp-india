﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    class clsBLLAddParicularsToAll
    {
         private DataLayer MobjDataLayer;
         private clsDALAddParicularsToAll MobjClsDALAddParicularsToAll;

         public clsDTOAddParicularsToAll PobjClsDTOAddParicularsToAll { get; set; }
         public clsBLLAddParicularsToAll()
        {
            MobjDataLayer = new DataLayer();
            MobjClsDALAddParicularsToAll = new clsDALAddParicularsToAll(MobjDataLayer);
            PobjClsDTOAddParicularsToAll = new clsDTOAddParicularsToAll();
            MobjClsDALAddParicularsToAll.PobjClsDTOAddParicularsToAll = PobjClsDTOAddParicularsToAll;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            return MobjClsDALAddParicularsToAll.FillCombos(saFieldValues);
        }
        /// <summary>
        /// To Get Employee Details
        /// </summary>
        /// <param name="intAdditionDeductionID"></param>
        /// <param name="strProcessDate"></param>
        /// <param name="intDepartmentID"></param>
        /// <returns>DataTable</returns>
        public DataTable GetEmployeeDetails(int intAdditionDeductionID, string strProcessDate, int intCompanyID, int intDepartmentID) // Get Employee Details
        {
            return MobjClsDALAddParicularsToAll.GetEmployeeDetails(intAdditionDeductionID, strProcessDate,intCompanyID, intDepartmentID);
        }
        /// <summary>
        /// Function For Saving Employee Payment Details
        /// </summary>
        /// <returns>bool</returns>
        public bool SaveEmployeeDetails()
        {
            //Function For Saving Employee Payment
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjClsDALAddParicularsToAll.SaveEmployeeDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
    }
}
