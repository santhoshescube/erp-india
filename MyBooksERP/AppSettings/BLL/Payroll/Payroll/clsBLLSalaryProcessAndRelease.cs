﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLSalaryProcessAndRelease
    {
        private clsDALSalaryProcessAndRelease MobjclsDALSalaryProcessAndRelease;
        private DataLayer MobjDataLayer;
        public clsDTOSalaryProcessAndRelease PobjclsDTOSalaryProcessAndRelease { get; set; } 

        public clsBLLSalaryProcessAndRelease()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALSalaryProcessAndRelease = new clsDALSalaryProcessAndRelease(MobjDataLayer);
            PobjclsDTOSalaryProcessAndRelease = new clsDTOSalaryProcessAndRelease();
            MobjclsDALSalaryProcessAndRelease.objDTOSalaryProcessAndRelease = PobjclsDTOSalaryProcessAndRelease;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public DataTable getDetailsInComboBox(int intType, int intCompanyID)
        {
            return MobjclsDALSalaryProcessAndRelease.getDetailsInComboBox(intType, intCompanyID);
        }

        public DataSet GetProcessEmployee(int EmployeeID, int CompanyID, string FromDate, string ToDate, string HostName, int UserId,
            int BranchIndicator, int intTempDept, int intTempDesg, int BranchID)
        {
            return MobjclsDALSalaryProcessAndRelease.GetProcessEmployee(EmployeeID, CompanyID, FromDate, ToDate, HostName, UserId,
                BranchIndicator, intTempDept, intTempDesg,  BranchID);
        }

        public void DeletePayment(string FromDate, string HostName, string ToDate, int CompanyID)
        {
            MobjclsDALSalaryProcessAndRelease.DeletePayment(FromDate, HostName, ToDate, CompanyID);
        }

        public bool SalaryProcess(int ParaEmployeeID, int CompanyID, string FromDate, string ToDate, bool IsPartial, int TypeIndex)
        {
            return MobjclsDALSalaryProcessAndRelease.SalaryProcess(ParaEmployeeID, CompanyID, FromDate, ToDate, IsPartial, TypeIndex);
        }

        public DataTable getTemplate()
        {
            return MobjclsDALSalaryProcessAndRelease.getTemplate();
        }

        public DataTable CheckPaymentIsPartial(long iPaymentID)
        {
            return MobjclsDALSalaryProcessAndRelease.CheckPaymentIsPartial(iPaymentID);
        }

        public DataSet FillSalaryInfogrid(string ProcessDate, int TemplateID, int BankID, int TransactionTypeID, int ProcessYear, int CompanyID, int CurrencyID, int DesignationID, int DepartmentID)
        {
            return MobjclsDALSalaryProcessAndRelease.FillSalaryInfogrid(ProcessDate, TemplateID, BankID, TransactionTypeID, ProcessYear, CompanyID, CurrencyID, DesignationID, DepartmentID);
        }

        public bool DeleteTempEmployeeIDForPaymentRelease(string MachineName)
        {
            return MobjclsDALSalaryProcessAndRelease.DeleteTempEmployeeIDForPaymentRelease(MachineName);
        }

        public bool InsertTempEmployeeIDForPaymentRelease(int EmployeeID, string ProcessDate, string MachineName, long PaymentID)
        {
            return MobjclsDALSalaryProcessAndRelease.InsertTempEmployeeIDForPaymentRelease(EmployeeID, ProcessDate, MachineName, PaymentID);
        }

        public DataTable checkPaymentIDUse(string MachineName)
        {
            return MobjclsDALSalaryProcessAndRelease.checkPaymentIDUse(MachineName);
        }

        public int DeletePayment(string FillDate, string MachineName, int intCompanyID)
        {
            return MobjclsDALSalaryProcessAndRelease.DeletePayment(FillDate, MachineName, intCompanyID);
        }

        public bool InsertTempEmployeeIDForPaymentRelease(string MachineName, long PaymentID)
        {
            return MobjclsDALSalaryProcessAndRelease.InsertTempEmployeeIDForPaymentRelease(MachineName, PaymentID);
        }

        public DataTable getEmployeePaymentDetail(int intTemplateID, int ProcessYear)
        {
            return MobjclsDALSalaryProcessAndRelease.getEmployeePaymentDetail(intTemplateID, ProcessYear);
        }

        public bool DeleteSingleRow(double PaymentID)
        {
            return MobjclsDALSalaryProcessAndRelease.DeleteSingleRow(PaymentID);
        }

        public void InsertIntoFileTables(string strDate, string strpath, int CompanyID, int CompanyBankAccountID, int IsVisa)
        {
            try
            {
                MobjDataLayer.BeginTransaction();
                MobjclsDALSalaryProcessAndRelease.InsertIntoFileTables(strDate, strpath, CompanyID, CompanyBankAccountID, IsVisa);
                MobjDataLayer.CommitTransaction();
                //return true;
            }
            catch (Exception)
            {
                MobjDataLayer.RollbackTransaction();
                //return false;
            }

        }
    }
}
