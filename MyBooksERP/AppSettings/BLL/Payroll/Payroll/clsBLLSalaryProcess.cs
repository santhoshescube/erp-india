﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;

namespace MyBooksERP
{
    public class clsBLLSalaryProcess
    {
        //
        clsDALSalaryProcess  MobjclsDALSalaryProcess;
        clsDTOSalaryProcess MobjclsDTOSalaryProcess;

        private DataLayer MobjDataLayer;

        public clsBLLSalaryProcess()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALSalaryProcess = new clsDALSalaryProcess(MobjDataLayer);
            MobjclsDTOSalaryProcess = new clsDTOSalaryProcess();
            MobjclsDALSalaryProcess.PobjclsDTOSalaryProcess = MobjclsDTOSalaryProcess;
        }

        public clsDTOSalaryProcess clsDTOSalaryProcess
        {
            get { return MobjclsDTOSalaryProcess; }
            set { MobjclsDTOSalaryProcess = value; }
        }

        public bool SaveSalary()
        {
          
            bool blnRetValue = false;

            try
            {
                MobjDataLayer.ClearAllPools();
                MobjDataLayer.BeginTransaction();
                MobjclsDALSalaryProcess.SaveSalary(); 
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataSet GetProcessEmployeeList()
        {
           return  MobjclsDALSalaryProcess.GetProcessEmployeeList();
        }
        
        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALSalaryProcess.FillCombos(saFieldValues);
        }

        public DataTable FillCombos(string sQuery)
        {
            //function for getting datatable for filling combo
            return MobjclsDALSalaryProcess.FillCombos(sQuery);
        }

        public int GetCurrentYear()
        {
            //function for getting Current Year for Salary Process
            return MobjclsDALSalaryProcess.GetCurrentYear();
        }
        public int GetCurrentMonth()
        {
            //function for getting Current Month for Salary Process
            return MobjclsDALSalaryProcess.GetCurrentMonth();
        }

        public string GetCurrentDate()
        {
            //function for getting Current Date for Salary Process
            return MobjclsDALSalaryProcess.GetCurrentDate();
        }

        public int PayCheckForEmployee()
        {
            return MobjclsDALSalaryProcess.PayCheckForEmployee();
        }
        public bool IsEmployeeExists()
        {
            return MobjclsDALSalaryProcess.IsEmployeeExists(); 
        }
    }
}
