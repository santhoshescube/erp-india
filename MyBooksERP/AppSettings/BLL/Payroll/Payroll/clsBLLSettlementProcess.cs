﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient; 
using System.Text;
using System.Data;
namespace MyBooksERP
{
    /* 
================================================
   Author:		<Author, Lince P Thomas>
   Create date: <Create Date,,8 Jan 2010>
   Description:	<Description,,Company Form>
 *
 *  Modified :	   <Saju>
    Modified date: <Modified Date,19 August 2013>
    Description:	<Description,Settlement Process Form>
================================================
*/
    public class clsBLLSettlementProcess
    {

          private DataLayer MobjDataLayer;

        clsDALSettlementProcess MobjclsDALSettlementProcess;
        clsDTOSettlementProcess MobjclsDTOSettlementProcess;

        public clsBLLSettlementProcess()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALSettlementProcess = new clsDALSettlementProcess(MobjDataLayer);
            MobjclsDTOSettlementProcess = new clsDTOSettlementProcess();
            MobjclsDALSettlementProcess.DTOSettlementProcess = MobjclsDTOSettlementProcess;

        }
        
        public clsDTOSettlementProcess clsDTOSettlementProcess
        {
            get { return MobjclsDTOSettlementProcess; }
            set { MobjclsDTOSettlementProcess = value; }
        }

        public System.Data.DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALSettlementProcess.FillCombos(saFieldValues);
        }
        //public int RecCountNavigate()
        //{
        //    return MobjclsDALSettlementProcess.RecCountNavigate();
        //}
        public int RecCount()
        {
            return MobjclsDALSettlementProcess.RecCount();
        }
        public void DisplayInfo()
        {
             MobjclsDALSettlementProcess.DisplayInfo();
        }
        public int IsSalaryProcessed()
        {
            return MobjclsDALSettlementProcess.IsSalaryProcessed();
        }
        public int IsTransferDateGreater()
        {
            return MobjclsDALSettlementProcess.IsTransferDateGreater();
        }
        public int IsDateOfJoinGreater()
        {
            return MobjclsDALSettlementProcess.IsDateOfJoinGreater();
        }
        public int IsEmployeeSettled()
        {
            return MobjclsDALSettlementProcess.IsEmployeeSettled();
        }
        public int RecCountNavigate()
        {
            return MobjclsDALSettlementProcess.RecCountNavigate();
        }
        public bool SaveEmployeeSettlement()
        {
            return MobjclsDALSettlementProcess.SaveEmployeeSettlement();
        }
        public int GetRecordCount()
        {
            return MobjclsDALSettlementProcess.GetRecordCount();
        }
        public int IsPaymentReleaseChecking()
        {
            return MobjclsDALSettlementProcess.IsPaymentReleaseChecking();
        }
        public int IsValidSet(int SetTypeID, int EmpID)
        {
            return MobjclsDALSettlementProcess.IsValidSet(SetTypeID, EmpID);
        }
        public int IsAttendanceExisting()
        {
            return MobjclsDALSettlementProcess.IsAttendanceExisting();
        }
        public void EmployeeSetAttTransaction(int EmpID, string strToDate)
        {
            MobjclsDALSettlementProcess.EmployeeSetAttTransaction(EmpID, strToDate);
        }
        public DataTable SettlementProcess()
        {
            return MobjclsDALSettlementProcess.SettlementProcess();
        }
        public DataSet SettlementProcessDetail()
        {
            return MobjclsDALSettlementProcess.SettlementProcessDetail();
        }
        public DataTable EmployeeHistoryDetail()
        {
            return MobjclsDALSettlementProcess.EmployeeHistoryDetail();
        }
        public bool GetSettlementPolicyAndExperience(int EmpID, string DtpFromDate, string DtpToDate, ref string SetPolicyDesc, ref string lblNoofMonths, ref string txtNofDaysExperience, ref string txtLeavePayDays, ref string lblTakenLeavePayDays, ref string lblEligibleLeavePayDays, ref string txtAbsentDays)
        {
            return MobjclsDALSettlementProcess.GetSettlementPolicyAndExperience(EmpID, DtpFromDate, DtpToDate, ref SetPolicyDesc, ref lblNoofMonths, ref txtNofDaysExperience, ref txtLeavePayDays, ref lblTakenLeavePayDays, ref lblEligibleLeavePayDays, ref txtAbsentDays);
        }
        public bool GetSalaryReleaseDate(int intEmpID, ref DateTime DtpToDate)
        {
            return MobjclsDALSettlementProcess.GetSalaryReleaseDate(intEmpID, ref DtpToDate);
        }
        public bool DisplayCurrencyEmployee(int EmpID, ref int miCurrencyId, ref int MiCompanyID, ref string LblCurrency, ref string DtpFromDate, ref int TransactionTypeID)
        {
            return MobjclsDALSettlementProcess.DisplayCurrencyEmployee(EmpID, ref  miCurrencyId, ref  MiCompanyID, ref  LblCurrency, ref  DtpFromDate,ref  TransactionTypeID);
        }
        public bool DeleteSettlementDetail()
        {
            return MobjclsDALSettlementProcess.DeleteSettlementDetail();
        }
        public bool IsSettlementAccountExists(int intEmpID)
        {
            return MobjclsDALSettlementProcess.IsSettlementAccountExists(intEmpID);
        }
        public int GetEmpCompanyID(int intEmpID)
        {
            return MobjclsDALSettlementProcess.GetEmpCompanyID(intEmpID);
        }
        public void FillDetails(int SettlementID, int EmpID)
        {
             MobjclsDALSettlementProcess.FillDetails(SettlementID, EmpID);
        }

        public DataSet EmailDetail(int SettlementID, int EmpID)
        {
           return  MobjclsDALSettlementProcess.EmailDetail(SettlementID, EmpID);
        }

        public int IsPaymentReleaseCheckingPrevious()
        {
            return MobjclsDALSettlementProcess.IsPaymentReleaseCheckingPrevious();
        }
        public bool DeletePaymentDetails()
        {
            return MobjclsDALSettlementProcess.DeletePaymentDetails();
        }

        public DataTable GetEmployeeLeaveHolidays(int EmpID, string strFromDate, string strToDate) // Employee Leave days & Holidays
        {
            return MobjclsDALSettlementProcess.GetEmployeeLeaveHolidays(EmpID, strFromDate, strToDate);
        }
    }
}
