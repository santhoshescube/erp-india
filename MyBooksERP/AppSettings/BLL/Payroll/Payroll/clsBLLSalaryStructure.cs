﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLSalaryStructure
    {


        clsDALSalaryStructure MobjclsDALSalaryStructure = null;
        public clsBLLSalaryStructure() { }

        private clsDALSalaryStructure DALSalaryStructure//clsDALOvertimePolicy DALOvertimePolicy
        {
            get
            {
                if (this.MobjclsDALSalaryStructure == null)
                    this.MobjclsDALSalaryStructure = new clsDALSalaryStructure();

                return this.MobjclsDALSalaryStructure;
            }
        }

        public clsDTOSalaryStructure DTOSalaryStructure
        {
            get
            {

                return this.DALSalaryStructure.DTOSalaryStructure;
            }
            set
            {
                this.DALSalaryStructure.DTOSalaryStructure = value;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALSalaryStructure.FillCombos(saFieldValues);
        }

        /// <summary>
        /// Get the details of a deduction policy
        /// </summary>
        /// <param name="iDeductionPolicyID">DeductionPolicyID</param>
        /// <returns>datatable of details</returns>
        public DataSet GetDeductionPolicyDetails(int iDeductionPolicyID)
        {
            return DALSalaryStructure.GetDeductionPolicyDetails(iDeductionPolicyID);
        }
        /// <summary>
        /// email source
        /// </summary>
        /// <param name="iSalaryStructureID">salarystructureid</param>
        /// <returns>dataset of salarystructure details</returns>
        public DataSet DisplaySalaryStructure(int iSalaryStructureID)
        {
            return DALSalaryStructure.DisplaySalaryStructure(iSalaryStructureID);
        }


        /// <summary>
        /// Display salary structure by employeeid
        /// </summary>
        /// <param name="iSalaryStructureID"></param>
        /// <returns></returns>
        public static DataSet GetSalaryStructureEmail(int EmployeeID)
        {
            return clsDALSalaryStructure.GetSalaryStructureEmail(EmployeeID);
        }


        public bool SalaryStructureSave(bool blnEditMode)
        {
            bool blnRetValue = false;
            blnRetValue = DALSalaryStructure.SalaryStructureSave(blnEditMode);
            return blnRetValue;
        }
        public bool SalaryStructureDetailSave(bool blnEditMode)
        {
            bool blnRetValue = false;
            blnRetValue = DALSalaryStructure.SalaryStructureDetailSave(blnEditMode);
            return blnRetValue;

        }

        public bool DisplaySalaryStructureMaster(int intRowNumber,string strSearch,int iCompanyID)
        {
            return DALSalaryStructure.DisplaySalaryStructureMaster(intRowNumber, strSearch, iCompanyID);
        }

        public int GetRecordCount(string strSearch, int iCompanyID)
        {
            return DALSalaryStructure.GetRecordCount( strSearch, iCompanyID);
        }

        public DataTable DispalySalaryStructureDetail(int SalaryStructureID)
        {
            return DALSalaryStructure.DispalySalaryStructureDetail(SalaryStructureID);
        }

        public bool CheckSalaryReleased()
        {
            return DALSalaryStructure.CheckSalaryReleased();
        }
        public bool Updatevalidation()
        {
            return DALSalaryStructure.Updatevalidation();
        }

        public bool DeleteSalaryStructure()
        {
            return DALSalaryStructure.DeleteSalaryStructure();
        }

        public bool SalaryStructureHistorySave()
        {
            return DALSalaryStructure.SalaryStructureHistorySave();
        }

        public int GetAdditionOrDeduction(int AdditionDeductionID)
        {
            return DALSalaryStructure.GetAdditionOrDeduction(AdditionDeductionID);
        }

        public bool SearchEmployee(string SearchCriteria, int RowNum, out int TotalRows,int CompanyID)
        {
            return DALSalaryStructure.SearchItem(SearchCriteria, RowNum, out TotalRows, CompanyID);

        }
        public int GetRowNumber(int iCompanyID, string strSearch)
        {
            return DALSalaryStructure.GetRowNumber( iCompanyID, strSearch);
        }

        public bool EmployeeIDExists()
        {
            return DALSalaryStructure.EmployeeIDExists();
        }

        public DataTable DispalyRemunerationDetail(int SalaryStructureID)
        {
            return DALSalaryStructure.DispalyRemunerationDetail(SalaryStructureID);
        }

        public DataTable GetPolicyPermissions(int RoleID)
        {
            return DALSalaryStructure.GetPolicyPermissions(RoleID);
        }

        public DataTable GetAutoCompleteList(int intSearchIndex, int intCompanyID)
        {
            return DALSalaryStructure.GetAutoCompleteList(intSearchIndex, intCompanyID);
        }

        /// <summary>
        /// Check if employee workstatus is active
        /// </summary>
        /// <param name="iEmployeeID">employeeid</param>
        /// <returns>true/false</returns>
        public bool CheckIfEmployeeIsActive(int iEmployeeID)
        {
            return DALSalaryStructure.CheckIfEmployeeIsActive(iEmployeeID);
        }

        /// <summary>
        /// Check existance of salary processed without release
        /// </summary>
        /// <param name="iEmployeeID">employeeid</param>
        /// <returns>true/false</returns>
        public bool CheckIfEmployeeSalaryProcessed(int iEmployeeID)
        {
            return DALSalaryStructure.CheckIfEmployeeSalaryProcessed(iEmployeeID);
        }



        //Created By Rajesh on 29-May-2014

        public static DataSet GetAllEmployeesSalaryStructure(int CompanyID,int DepartmentID,int EmployeeID)
        {
            return clsDALSalaryStructure.GetAllEmployeesSalaryStructure(CompanyID, DepartmentID, EmployeeID); 
        }

    }
}
