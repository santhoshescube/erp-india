﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;

namespace MyBooksERP
{
   
    class clsBLLEmployeeLeaveStructure
    {
        clsDALEmployeeLeaveStructure objDALEmployeeLeaveStructure;
        clsDTOEmployeeLeaveStructure objDTOEmployeeLeaveStructure;

        public clsBLLEmployeeLeaveStructure()
        {
            //Constructor
            objDALEmployeeLeaveStructure = new clsDALEmployeeLeaveStructure();
            objDTOEmployeeLeaveStructure = new clsDTOEmployeeLeaveStructure();
        }

        public DataTable GetEmployeeWorkStatusID(int intEmployeeID)
        {
            return objDALEmployeeLeaveStructure.GetEmployeeWorkStatusID(intEmployeeID);
        }

        //public DataTable GetLeaveTypeIDByEmployeeID(int intEmployeeID)
        //{
        //    return objDALEmployeeLeaveStructure.GetLeaveTypeIDByEmployeeID(intEmployeeID);
        //}
        public bool LeaveSummaryFromLeaveEntry(string[] str, int intEmployeeID,int intCompanyID)
        {
            return objDALEmployeeLeaveStructure.LeaveSummaryFromLeaveEntry(str,intEmployeeID,intCompanyID);
        }
        public bool SaveLeaveStructure(List<clsDTOEmployeeLeaveStructure> lstDTOEmployeeLeaveStructure)
        {
            return objDALEmployeeLeaveStructure.SaveLeaveStructure(lstDTOEmployeeLeaveStructure);
        }
        public DataTable GetDisplayData(string date1, int intEmployeeID, int intCompanyID, out int intLeavePolicyID)
        {
            return objDALEmployeeLeaveStructure.GetDisplayData(date1,intEmployeeID,intCompanyID,out intLeavePolicyID);
        }

        public DataSet GetLeaveStructureEmail(int intEmployeeID, int intCompanyID, string date1, string CurrentFinYear)
        {
            return objDALEmployeeLeaveStructure.GetLeaveStructureEmail(intEmployeeID,  intCompanyID,  date1,  CurrentFinYear);
        }

    }
}
