﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace MyBooksERP
{
    public class clsNavigatorBLL
    {
        clsNavigatorDAL  objNavigatorDAL= null;
        /// <summary>
        /// Constructor 
        /// </summary>
        public clsNavigatorBLL() { }
        /// <summary>
        /// Data Access Layer of Navigator
        /// </summary>
        public clsNavigatorDAL NavigatorDAL
        {
            get 
            {
                // Create instance of data access layer if null
                if (this.objNavigatorDAL == null)
                    this.objNavigatorDAL = new clsNavigatorDAL();
                return this.objNavigatorDAL;
            }
        }
        /// <summary>
        /// Business Object of the Navigator
        /// </summary>
        public clsNavigator  Navigator
        {
            get { return this.NavigatorDAL.Navigator ; }
            set { this.NavigatorDAL.Navigator = value; }
        }

        public DataTable GetNavCompanyTree()
        {
            return this.NavigatorDAL.GetNavCompanyTree();
        }
        public DataTable GetNavCamp()
        {
            return this.NavigatorDAL.GetNavCamp();
        }
        public DataTable GetNavClient()
        {
            return this.NavigatorDAL.GetNavClient();
        }
        public DataTable GetNavEmployee()
        {
            return this.NavigatorDAL.GetNavEmployee();
        }
        public DataTable GetNavBuilding()
        {
            return this.NavigatorDAL.GetNavBuilding();
        }
        public DataTable GetNavMess()
        {
            return this.NavigatorDAL.GetNavMess();
        }
        public DataSet GetNavCompanyEmail()
        {
            return this.NavigatorDAL.GetNavCompanyEmail();
        }
        public DataSet GetNavCampEmail()
        {
            return this.NavigatorDAL.GetNavCampEmail();
        }
        public DataSet GetNavClientEmail()
        {
            return this.NavigatorDAL.GetNavClientEmail();
        }
        public DataSet GetNavEmployeeEmail()
        {
            return this.NavigatorDAL.GetNavEmployeeEmail();
        }

        public void Search(NavSearch  searchBy, string searchKey)
        {
            using (DataSet ds = this.NavigatorDAL.Search(searchBy, searchKey))
            {

                if (ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[1].Rows[0];

                    this.NavigatorDAL.Navigator.EmployeeID = row["EmployeeID"].ToInt64();
                }
            }
        }

      
        public DataSet GetEmployeeReport()
        {
            return NavigatorDAL.GetEmployeeReport();
        }
        public DataSet GetSalaryStructureReport()
        {
            return NavigatorDAL.GetSalaryStructureReport();
        }
        public DataSet GetPreSalaryStructureReport()
        {
            return NavigatorDAL.GetPreSalaryStructureReport();
        }

        public DataSet GetShiftPolicy()
        {
            return NavigatorDAL.GetShiftPolicy();
        }
        public DataTable  LoadShifts()
        {
            return NavigatorDAL.LoadShifts();
        }
        public DataTable LoadCompany()
        {
            return NavigatorDAL.LoadCompany();
        }
        public DataTable LoadWorkPolicies()
        {
            return NavigatorDAL.LoadWorkPolicies();
        }
        public DataTable LoadLeavePolicies()
        {
            return NavigatorDAL.LoadLeavePolicies();
        }
        public DataSet GetWorkPolicy()
        {
            return NavigatorDAL.GetWorkPolicy();
        }
        public DataSet GetLeavePolicy() //Leave Policy
        {
            return NavigatorDAL.GetLeavePolicy();
        }
        public DataTable LoadEmployeeGrid(int intEmpFilterTypeValue, int intEmpFilter, int intWorkStatusID, int intCmpID)
        {
            return NavigatorDAL.LoadEmployeeGrid( intEmpFilterTypeValue,  intEmpFilter,  intWorkStatusID,  intCmpID);
        }
        public DataTable FillData() // navigation double click
        {
            return NavigatorDAL.FillData();
        }

        public DataTable GetAlerts()
        {
            return NavigatorDAL.GetAlerts();
        }
    }
}
