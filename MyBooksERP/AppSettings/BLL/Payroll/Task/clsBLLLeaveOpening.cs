﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace MyBooksERP
{
   public  class clsBLLLeaveOpening
    {
        clsDALLeaveOpening MobjDALLeaveOpening = null;
        public clsBLLLeaveOpening() { }
        private clsDALLeaveOpening DALLeaveOpening
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALLeaveOpening == null)
                    this.MobjDALLeaveOpening = new clsDALLeaveOpening();

                return this.MobjDALLeaveOpening;
            }
        }
        public clsDTOLeaveOpening DTOLeaveOpening
        {
            get { return this.DALLeaveOpening.DTOLeaveOpening; }
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALLeaveOpening.FillCombos(saFieldValues);
        }
        public DataTable getDetailsInComboBox(int intType)
        {
            return DALLeaveOpening.getDetailsInComboBox(intType);
        }

        public DataTable GetEmployeeList(int UserID, int CompanyID)
        {
            return DALLeaveOpening.GetEmployeeList(UserID, CompanyID);

        }
        public void SaveEmployeeLeave()
        {
            DALLeaveOpening.SaveEmployeeLeave(); 
        }


    }
}
