﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace MyBooksERP
{
    public class clsBLLLeaveEntry
    {
        #region DECLARATIONS
        public clsDALLeaveEntry MobjclsDALLeaveEntry;
        public clsDTOLeaveEntry MobjclsDTOLeaveEntry;
        private DataLayer objClsConnection;
        #endregion
        #region PROPERTIES
        public clsDTOLeaveEntry clsDTOLeaveEntry
        {
            get { return MobjclsDTOLeaveEntry; }
            set { MobjclsDTOLeaveEntry = value; }

        }
        #endregion
        #region CONSTRUCTOR
        public clsBLLLeaveEntry()
        {
            objClsConnection = new DataLayer();
            MobjclsDTOLeaveEntry = new clsDTOLeaveEntry();
            MobjclsDALLeaveEntry = new clsDALLeaveEntry();

            MobjclsDALLeaveEntry.objclsConnection = objClsConnection;
            MobjclsDALLeaveEntry.objclsDTOLeaveEntry = MobjclsDTOLeaveEntry;
        }
        #endregion
        #region METHODS
        public DataTable FillCombos(string[] sarFieldValues)
        {
            return MobjclsDALLeaveEntry.FillCombos(sarFieldValues);
        }

        public int GetRowNumber(int iEmpID)
        {
            return MobjclsDALLeaveEntry.GetRowNumber(iEmpID);
        }

        public int GetRecordCount()
        {
            return MobjclsDALLeaveEntry.GetRecordCount();
        }

        public DateTime GetFinYearStartDate(int intCompanyID, string Currentdate)
        {
            return MobjclsDALLeaveEntry.GetFinYearStartDate(intCompanyID, Currentdate);
        }

        public decimal GetLeaveTaken(int intCompanyID, int intLeavePolicyID, int IntEmployeeID, int intLeaveTypeID, string TestDate, out decimal DecBalanceleave)
        {
            return MobjclsDALLeaveEntry.GetLeaveTaken(intCompanyID, intLeavePolicyID, IntEmployeeID, intLeaveTypeID, TestDate, out DecBalanceleave);
        }
        public decimal GetCurrentLeaveTaken(int intLeaveID)
        {
            return MobjclsDALLeaveEntry.GetCurrentLeaveTaken(intLeaveID);
        }
        public bool SaveLeaveEntry(bool bStatus, object iLeaveID1)
        {
            return MobjclsDALLeaveEntry.SaveLeaveEntry(bStatus, iLeaveID1);
        }

        public bool DeleteAbsentEntryFromAttendance()
        {
            return MobjclsDALLeaveEntry.DeleteAbsentEntryFromAttendance();
        }

        public bool UpdateAttendance()
        {
            return MobjclsDALLeaveEntry.UpdateAttendance();
        }

        public bool DeleteEmployeeLeaveSummaryDetails(int intCompanyID, DateTime dFinYearDate, int IntLeavePolicyID, int IntEmployeeID, int IntLeaveTypeID)
        {
            return MobjclsDALLeaveEntry.DeleteEmployeeLeaveSummaryDetails(intCompanyID, dFinYearDate, IntLeavePolicyID, IntEmployeeID, IntLeaveTypeID);
        }

        public DataSet GetBalanceAndHolidaysdetails(int IntCompany, int IntEmployeeID, int IntLeaveTypeID, int MiLeavePolicyID, string s1, string s2, bool MbAddStatus)
        {
            return MobjclsDALLeaveEntry.GetBalanceAndHolidaysdetails(IntCompany, IntEmployeeID, IntLeaveTypeID, MiLeavePolicyID, s1, s2, MbAddStatus);
        }

        public decimal GetMonthLeaveTaken(int IntEmployeeID, string IntLeaveDateFrom, string IntLeaveDateTo, int IntCompany, int IntLeaveTypeID)
        {
            return MobjclsDALLeaveEntry.GetMonthLeaveTaken(IntEmployeeID, IntLeaveDateFrom, IntLeaveDateTo, IntCompany, IntLeaveTypeID);
        }

        public bool SearchLeave(int IntRowNum, string strSearchText, out  int IntTotalCount)
        {
            return MobjclsDALLeaveEntry.SearchLeave(IntRowNum, strSearchText, out IntTotalCount);
        }

        public bool DeleteLeave(int IntLeaveID)
        {
            return MobjclsDALLeaveEntry.DeleteLeave(IntLeaveID);
        }

        public bool DisplayLeave(int IntRecordID, int IntEmpID)
        {
            return MobjclsDALLeaveEntry.DisplayLeave(IntRecordID, IntEmpID);
        }

        public int GetHoildaysWithOffDays(string strLeaveDateFrom, string strLeaveDateTo, int IntEmployeeID)
        {
            return MobjclsDALLeaveEntry.GetHoildaysWithOffDays(strLeaveDateFrom, strLeaveDateTo, IntEmployeeID);
        }

        public void CheckLeavestructures(int IntCompanyID, int IntEmployeeID, int IntLeaveTypeID, int MiLeavepolicyID, string s1)
        {
            MobjclsDALLeaveEntry.CheckLeavestructures(IntCompanyID, IntEmployeeID, IntLeaveTypeID, MiLeavepolicyID, s1);
        }

        public DataSet DisplayEmployeeLeaveEntry()
        {
            return MobjclsDALLeaveEntry.DisplayEmployeeLeaveEntry();
        }
        #endregion
    }
}
