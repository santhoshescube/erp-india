﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace MyBooksERP
{
    /*********************************************
      * Author       : Arun
      * Created On   : 12 Apr 2012
      * Purpose      : Salary Advance BLL 
      * ******************************************/
    public class clsBLLSalaryAdvance
    {

        clsDALSalaryAdvance MobjDALSalaryAdvance = null;

        public clsBLLSalaryAdvance() { }


        private clsDALSalaryAdvance DALSalaryAdvance
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALSalaryAdvance == null)
                    this.MobjDALSalaryAdvance = new clsDALSalaryAdvance();

                return this.MobjDALSalaryAdvance;
            }
        }

        public clsDTOSalaryAdvance DTOSalaryAdvance
        {
            get { return this.DALSalaryAdvance.DTOSalaryAdvance; }
        }


        public int GetRowNumber(int intSalaryAdvanceID)
        {
            
            return this.DALSalaryAdvance.GetRowNumber(intSalaryAdvanceID);
        }
        /// <summary>
        /// Get the total record Count
        /// </summary>
        /// <returns></returns>
        public int GetRecordCount()
        {
            return this.DALSalaryAdvance.GetRecordCount();
        }

        public bool SalaryAdvanceSave()
        {
            bool blnRetValue = false;
            try
            {
                DALSalaryAdvance.DataLayer.BeginTransaction();
               
                blnRetValue = DALSalaryAdvance.SalaryAdvanceSave();
                if (blnRetValue)
                    DALSalaryAdvance.DataLayer.CommitTransaction();
                else
                    DALSalaryAdvance.DataLayer.RollbackTransaction();


            }
            catch
            {
                DALSalaryAdvance.DataLayer.RollbackTransaction();
                throw;
            }
            return blnRetValue;
        }
        public bool DisplaySalaryAdvanceinfo(int intRowNumber)
        {
            return DALSalaryAdvance.DisplaySalaryAdvanceinfo(intRowNumber);
        }
        public bool DeleteSalaryAdvanceInfo()
        {
            bool blnRetValue = false;
            try
            {
                DALSalaryAdvance.DataLayer.BeginTransaction();
                blnRetValue= this.DALSalaryAdvance.DeleteSalaryAdvanceInfo();
                DALSalaryAdvance.DataLayer.CommitTransaction();
            }
            catch
            {
                DALSalaryAdvance.DataLayer.RollbackTransaction();
                throw;
            }
            return blnRetValue;
        }
        public bool IsSalaryAdavanceIsSettled(int intSalaryAdvanceID)
        {
            return this.DALSalaryAdvance.IsSalaryAdavanceIsSettled(intSalaryAdvanceID);
        }
        public bool IsSalaryProcessed(int iEmpID, string strDate)
        {
            return this.DALSalaryAdvance.IsSalaryProcessed(iEmpID,strDate);
        }
        public bool GetEmployeeJoiningDate(int iEmpID, ref string strDate, ref decimal decBasicPay, ref string strSalAdvPercent)
        {
            return this.DALSalaryAdvance.GetEmployeeJoiningDate(iEmpID, ref strDate, ref decBasicPay, ref strSalAdvPercent);
        }
        public bool GetCompanyFinancialYearStartDate(int iEmpID, ref string strDate)
        {
            return this.DALSalaryAdvance.GetCompanyFinancialYearStartDate(iEmpID, ref strDate);
        }
        public int GetEmployeeWorkStatus(int intEmpID)
        {
            return this.DALSalaryAdvance.GetEmployeeWorkStatus(intEmpID);
        }
        public bool IsSalAdvAccountExists(int intEmpID)
        {
            return this.DALSalaryAdvance.IsSalAdvAccountExists(intEmpID);
        }
        public bool GetEmployeeSalaryStructreDetails(int intEmpID, ref int intPayCalculationTypeID, ref int intPaymentClassificationID, ref string strDate)
        {
            return this.DALSalaryAdvance.GetEmployeeSalaryStructreDetails(intEmpID, ref intPayCalculationTypeID, ref intPaymentClassificationID, ref strDate);
        }

        public decimal GetEmployeeBasicPay(int intEmpID)
        {
            return this.DALSalaryAdvance.GetEmployeeBasicPay(intEmpID);
        }

        public bool GetSalaryAdvancePercentage(ref decimal decDefaultValue, ref decimal decConfigurationValue)
        {
            return this.DALSalaryAdvance.GetSalaryAdvancePercentage(ref decDefaultValue, ref decConfigurationValue);
        }
        public decimal GetMonthlyTotalAdvanceAmount(int intAdvanceID, int intEmpID, string strDate)
        {
            return this.DALSalaryAdvance.GetMonthlyTotalAdvanceAmount(intAdvanceID,intEmpID, strDate);
        }
        public decimal GetExchangeRate(int intEmpID, ref string strCmpCurrency)
        {
            return this.DALSalaryAdvance.GetExchangeRate(intEmpID, ref strCmpCurrency);
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALSalaryAdvance.FillCombos(saFieldValues);
        }


    }
}
