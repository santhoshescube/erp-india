﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLLeaveExtension
    {
        #region DECLARATIONS
        private clsDALLeaveExtension MobjclsDALLeaveExtension;
        public clsDTOLeaveExtension PobjDTOLeaveExtension { get; set; }
        #endregion
        #region CONSTRUCTOR
        public clsBLLLeaveExtension()
        {
            MobjclsDALLeaveExtension = new clsDALLeaveExtension();
            PobjDTOLeaveExtension = new clsDTOLeaveExtension();
            MobjclsDALLeaveExtension.PobjDTOLeaveExtension = PobjDTOLeaveExtension;
        }
        #endregion
        #region METHODS
        #region SaveLeaveExtension
        public bool SaveLeaveExtension(bool MAddStatus)
        {
            return MobjclsDALLeaveExtension.SaveLeaveExtension(MAddStatus);
        }
        #endregion SaveLeaveExtension

        #region GetJoiningDate
        public DateTime GetJoiningDate(int intEmployeeID)
        {
            return MobjclsDALLeaveExtension.GetJoiningDate(intEmployeeID);
        }
        #endregion GetJoiningDate

        #region GetShiftDuration
        public string GetShiftDuration(int intEmployeeID, int intDayID, DateTime dteDate)
        {
            return MobjclsDALLeaveExtension.GetShiftDuration(intEmployeeID, intDayID, dteDate);
        }
        #endregion GetShiftDuration

        #region CheckIfPolicyExists
        public bool CheckIfPolicyExists(int intEmployeeID, int intDayID, DateTime dteDate)
        {
            return MobjclsDALLeaveExtension.CheckIfPolicyExists(intEmployeeID, intDayID, dteDate);
        }
        #endregion CheckIfPolicyExists

        #region GetRecordCount
        public int GetRecordCount(string strSearchString, Int32 PintEmployeeID)
        {
            return MobjclsDALLeaveExtension.GetRecordCount(strSearchString, PintEmployeeID);
        }
        #endregion GetRecordCount

        #region GetLeaveExtensionDetails
        public DataTable GetLeaveExtensionDetails(int intRowNum, int LeaveExtensionTypeID, string strSearchString, Int32 PintEmployeeID)
        {
            return MobjclsDALLeaveExtension.GetLeaveExtensionDetails(intRowNum, LeaveExtensionTypeID, strSearchString, PintEmployeeID);
        }
        #endregion GetLeaveExtensionDetails

        #region CheckLeaveDuplicate
        public bool CheckLeaveDuplicate(string MonthYear, int EmployeeID, int LeaveTypeID, int LeaveExtensionTypeID, int MAddStatus, int ExtensionID)
        {
            return MobjclsDALLeaveExtension.CheckLeaveDuplicate(MonthYear, EmployeeID, LeaveTypeID, LeaveExtensionTypeID, MAddStatus, ExtensionID);
        }
        #endregion CheckLeaveDuplicate

        #region CheckTimeDuplicate
        public bool CheckTimeDuplicate(string MonthYear, int EmployeeID, int LeaveExtensionTypeID, int MAddStatus, int ExtensionID)
        {
            return MobjclsDALLeaveExtension.CheckTimeDuplicate(MonthYear, EmployeeID, LeaveExtensionTypeID, MAddStatus, ExtensionID);
        }
        #endregion CheckTimeDuplicate

        #region CheckPaymentRelease
        public bool CheckPaymentRelease(int EmployeeID, string EndDate, int intMode)
        {
            return MobjclsDALLeaveExtension.CheckPaymentRelease(EmployeeID, EndDate, intMode);
        }
        #endregion CheckPaymentRelease

        #region GetEmployeesWithLeaveExtension
        public DataTable GetEmployeesWithLeaveExtension(int ExtensionID)
        {
            return MobjclsDALLeaveExtension.GetEmployeesWithLeaveExtension(ExtensionID);
        }
        #endregion GetEmployeesWithLeaveExtension

        #region GetEmployeeCompany
        public int GetEmployeeCompany(int EmployeeID)
        {
            return MobjclsDALLeaveExtension.GetEmployeeCompany(EmployeeID);
        }
        #endregion GetEmployeeCompany

        #region GetFinYearStartDate
        public string GetFinYearStartDate(int CompanyID, string MonthYear)
        {
            return MobjclsDALLeaveExtension.GetFinYearStartDate(CompanyID, MonthYear);
        }
        #endregion GetFinYearStartDate

        #region GetLeavePolicy
        public int GetLeavePolicy(int EmployeeID)
        {
            return MobjclsDALLeaveExtension.GetLeavePolicy(EmployeeID);
        }
        #endregion GetLeavePolicy

        #region GetBalanceLeave
        public DataTable GetBalanceLeave(int EmployeeID, int CompanyID, string MonthYear, int LeavePolicyID, int LeaveTypeID)
        {
            return MobjclsDALLeaveExtension.GetBalanceLeave(EmployeeID, CompanyID, MonthYear, LeavePolicyID, LeaveTypeID);
        }
        #endregion GetBalanceLeave

        #region CheckMaleOrFemale
        public bool CheckMaleOrFemale(int EmployeeID)
        {
            return MobjclsDALLeaveExtension.CheckMaleOrFemale(EmployeeID);
        }
        #endregion CheckMaleOrFemale

        #region CheckLeavesTakenForEdit
        public bool CheckLeavesTakenForEdit(string MonthYear, int EmployeeID, int CompanyID, int LeaveTypeID, double ExceededEdit)
        {
            return MobjclsDALLeaveExtension.CheckLeavesTakenForEdit(MonthYear, EmployeeID, CompanyID, LeaveTypeID, ExceededEdit);
        }
        #endregion CheckLeavesTakenForEdit

        #region CheckLeavesTakenForDelete
        public bool CheckLeavesTakenForDelete(string MonthYear, int EmployeeID, int CompanyID, int LeaveTypeID)
        {
            return MobjclsDALLeaveExtension.CheckLeavesTakenForDelete(MonthYear, EmployeeID, CompanyID, LeaveTypeID);
        }
        #endregion CheckLeavesTakenForDelete

        #region CheckIfExtensionIDExists
        public bool CheckIfExtensionIDExists(int ExtensionID)
        {
            return MobjclsDALLeaveExtension.CheckIfExtensionIDExists(ExtensionID);
        }
        #endregion CheckIfExtensionIDExists

        #region DeleteLeaveExtension
        public bool DeleteLeaveExtension(int ExtensionID)
        {
            return MobjclsDALLeaveExtension.DeleteLeaveExtension(ExtensionID);
        }
        #endregion DeleteLeaveExtension

        #region GetEmployeeLeaveSummary
        public DataTable GetEmployeeLeaveSummary(int EmployeeID, string MonthYear, int LeaveTypeID)
        {
            return MobjclsDALLeaveExtension.GetEmployeeLeaveSummary(EmployeeID, MonthYear, LeaveTypeID);
        }
        #endregion GetEmployeeLeaveSummary

        #region ShowReport
        public DataSet ShowReport(int EmployeeID, int CompanyID)
        {
            return MobjclsDALLeaveExtension.ShowReport(EmployeeID, CompanyID);
        }
        #endregion ShowReport
        #endregion
    }
}
