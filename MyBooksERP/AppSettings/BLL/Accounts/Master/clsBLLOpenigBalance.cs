﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;

namespace MyBooksERP
{
    /***********************************************************
    * Author       : Junaid
    * Created On   : 13 APRL 2012
    * Purpose      : Create Opening Balance for Accounts
    *                
    * **********************************************************/
    public class clsBLLOpenigBalance
    {
        private   clsDALOpeningBalance objclsDALOpeningBalance;
        private DataLayer objClsConnection;
        public  clsDTOOpeningBalance objclsDTOOpeningBalance;
        public clsBLLOpenigBalance()
        {
            this.objClsConnection = new DataLayer();
            this.objclsDALOpeningBalance = new clsDALOpeningBalance();
            this.objclsDTOOpeningBalance = new clsDTOOpeningBalance();
            this.objclsDALOpeningBalance.ObjclsDTOOpeningBalance = objclsDTOOpeningBalance;
        }
        public clsDTOOpeningBalance clsDTOOpeningBalance
        {
            get { return objclsDTOOpeningBalance; }
            set { objclsDTOOpeningBalance = value; }
        }
        public DataTable LoadComboboxes(string[] saFields)
        {
            try
            {
                return this.objclsDALOpeningBalance.LoadComboboxes(saFields);
            }
            catch (Exception ex) { throw ex; }
        }
        public DataTable FillcboAccount()
        {
            try
            {
                objclsDALOpeningBalance.MObjConnection = objClsConnection;

                return this.objclsDALOpeningBalance.FillcboAccount();
            }
            catch (Exception ex) { throw ex; }
        }
        public DataTable FillcboAccount(int CompanyID)
        {
            try
            {
                objclsDALOpeningBalance.MObjConnection = objClsConnection;

                return this.objclsDALOpeningBalance.FillcboAccount(CompanyID);
            }
            catch (Exception ex) { throw ex; }
        }
        public DataTable FillcboAccount(int CompanyID,int AccountID)
        {
            try
            {
                objclsDALOpeningBalance.MObjConnection = objClsConnection;

                return this.objclsDALOpeningBalance.FillcboAccount(CompanyID, AccountID);
            }
            catch (Exception ex) { throw ex; }
        }
        public string BookStartDate(int CopmanyID)
        {
            try
            {
                return this.objclsDALOpeningBalance.BookStartDate(CopmanyID);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool SaveOpeningBalance(bool blnAddStatus,bool Credit,int OldAccountId)
        {
            bool blnReturnVal = false;
            try
            {
                
                this.objclsDALOpeningBalance.ObjclsDTOOpeningBalance = this.objclsDTOOpeningBalance;
                blnReturnVal = this.objclsDALOpeningBalance.SaveOpeningBalance(blnAddStatus, Credit, OldAccountId);
                
                return blnReturnVal;
            }
            catch (Exception Ex)
            {
                
                throw Ex;
            }

        }
        public DataTable DisplayGridInformation()
        {
            objclsDALOpeningBalance.MObjConnection = objClsConnection;
            return objclsDALOpeningBalance.DisplayGridInformation();
        }
        public bool DeleteOpeningBalance(int AccountID ,int CompanyID)
        {
            bool blnReturnVal = false;
            try
            {
                this.objClsConnection.BeginTransaction();
                blnReturnVal = this.objclsDALOpeningBalance.DeleteOpeningBalance(AccountID,CompanyID);
                this.objClsConnection.CommitTransaction();
                return blnReturnVal;
            }

            catch (Exception Ex)
            {
                
                this.objClsConnection.RollbackTransaction();
                return blnReturnVal;
                throw Ex;
            }
        }
        public bool Checkduplicate()
        {
            try
            {
                return this.objclsDALOpeningBalance.CheckDuplicate();
            }
            catch (Exception ex) { throw ex; }
        }
        public DataTable fillBalance()
        {
            objclsDALOpeningBalance.MObjConnection = objClsConnection;
            return objclsDALOpeningBalance.fillBalance();
        }
    }
}
