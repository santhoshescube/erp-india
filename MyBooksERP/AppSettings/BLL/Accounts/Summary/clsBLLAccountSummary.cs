﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public  class clsBLLAccountSummary
    {
        private clsDALAccountSummary MobjclsDALAccountSummary;
        private DataLayer MobjDataLayer;
        public clsDTOAccountSummary MobjclsDTOAccountSummary { get; set; }

        public clsBLLAccountSummary()
        {
          this. MobjDataLayer = new DataLayer();
          this. MobjclsDALAccountSummary = new clsDALAccountSummary(MobjDataLayer);
          this. MobjclsDTOAccountSummary = new clsDTOAccountSummary();
          this. MobjclsDALAccountSummary.objDTOAccountSummary = MobjclsDTOAccountSummary;
        }
        //~clsBLLAccountSummary()
        //{
        //    this.MobjclsDALAccountSummary = null;
        //    this.MobjDataLayer = null;
        //}
        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public DataSet DisplayAccountsSummaryReoprt()
        {
            return MobjclsDALAccountSummary.DisplayAccountsSummaryReoprt();
        }

        public DataTable GetChildAccounts(int intGroupID, int intSelectedCriteria)
        {
            return MobjclsDALAccountSummary.GetChildAccounts(intGroupID, intSelectedCriteria);
        }

        public string GetCompanyCurrency(int intCompanyID)
        {
            return MobjclsDALAccountSummary.GetCompanyCurrency(intCompanyID);
        }

        public DateTime GetCompanyBookStartDate(int intCompanyID)
        {
            return MobjclsDALAccountSummary.GetCompanyBookStartDate(intCompanyID);
        }

        public DateTime GetFinancialStartDate(int intCompanyID, DateTime dtpDate)
        {
            return MobjclsDALAccountSummary.GetFinancialStartDate(intCompanyID, dtpDate);
        }
    }
}
