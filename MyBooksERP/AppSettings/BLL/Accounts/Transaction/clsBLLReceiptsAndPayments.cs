﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace MyBooksERP
{
    public class clsBLLReceiptsAndPayments
    {
        private clsDALReceiptsAndPayments MobjclsDALReceiptsAndPayments;
        private DataLayer MobjDataLayer;
        public clsDTOReceiptsAndPayments PobjClsDTOReceiptsAndPayments { get; set; }

        public clsBLLReceiptsAndPayments()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALReceiptsAndPayments = new clsDALReceiptsAndPayments(MobjDataLayer);
            PobjClsDTOReceiptsAndPayments = new clsDTOReceiptsAndPayments();
            MobjclsDALReceiptsAndPayments.objDTOReceiptsAndPayments = PobjClsDTOReceiptsAndPayments;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public DateTime GetServerDate()
        {
            return MobjclsDALReceiptsAndPayments.GetServerDate();
        }

        public DataTable getDetails()
        {
            return MobjclsDALReceiptsAndPayments.getDetails();
        }

        public bool SaveReceiptsAndPayments(DataTable datDetails)
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALReceiptsAndPayments.SaveReceiptAndPaymentMaster(datDetails);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeleteReceiptAndPayment()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALReceiptsAndPayments.DeleteReceiptAndPaymentMaster();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public string GetPaymentNo()
        {
            return MobjclsDALReceiptsAndPayments.GetPaymentNo();
        }

        public int GetCompanyCurrency()
        {
            return MobjclsDALReceiptsAndPayments.GetCompanyCurrency();
        }

        public int GetSupplierCurrency()
        {
            return MobjclsDALReceiptsAndPayments.GetSupplierCurrency();
        }

        public long GetRecordCount(int intModuleID)
        {
            return MobjclsDALReceiptsAndPayments.GetRecordCount(intModuleID);
        }

        public bool DisplayReceiptsAndPayments(long intRowNum, int intModuleID)
        {
            return MobjclsDALReceiptsAndPayments.DisplayReceiptsAndPayments(intRowNum, intModuleID);
        }

        public DataTable DisplayReceiptsAndPaymentsDetails()
        {
            return MobjclsDALReceiptsAndPayments.DisplayReceiptsAndPaymentsDetails();
        }

        public DataTable getReferenceIDDetails()
        {
            return MobjclsDALReceiptsAndPayments.getReferenceIDDetails();
        }

        public string getReferenceNo()
        {
            return MobjclsDALReceiptsAndPayments.getReferenceNo();
        }

        public decimal getBillAmount()
        {
            return MobjclsDALReceiptsAndPayments.getBillAmount();
        }

        public decimal getBalanceAmount()
        {
            return MobjclsDALReceiptsAndPayments.getBalanceAmount();
        }

        public int DuplicatePaymentNo()
        {
            return MobjclsDALReceiptsAndPayments.DuplicatePaymentNo();
        }

        public int DuplicatePaymentNo(long intRPID)
        {
            return MobjclsDALReceiptsAndPayments.DuplicatePaymentNo(intRPID);
        }

        public int CheckReferenceNoStatus()
        {
            return MobjclsDALReceiptsAndPayments.CheckReferenceNoStatus();
        }

        public int DuplicateChequeNo(long intRPID)
        {
            return MobjclsDALReceiptsAndPayments.DuplicateChequeNo(intRPID);
        }

        public DataSet DisplayPaymentDetailsReport(long intRPID) //Payment Details Report
        {
            return MobjclsDALReceiptsAndPayments.DisplayPaymentDetailsReport(intRPID);
        }

        public int GetCurrencyScale(int intCurrencyID)
        {
            return MobjclsDALReceiptsAndPayments.GetCurrencyScale(intCurrencyID);
        }

        public long GetRowNumber(long lngRPID, bool blnReceipt)
        {
            return MobjclsDALReceiptsAndPayments.GetRowNumber(lngRPID, blnReceipt);
        }
        public DataTable GetAllInvoiceIDandAmount()
        {
            return MobjclsDALReceiptsAndPayments.GetAllInvoiceIDandAmount();
        }
        public decimal GetAdvanceAmountByInvoiceID()
        {
            return MobjclsDALReceiptsAndPayments.GetAdvanceAmountByInvoiceID();
        }
        public DataTable getReferenceIDDetailstoDisplay()
        {
            return MobjclsDALReceiptsAndPayments.getReferenceIDDetailstoDisplay();
        }
        public int GetPaymentTerms(int intReferenceNo)
        {
            return MobjclsDALReceiptsAndPayments.GetPaymentTerms(intReferenceNo);
        }

        public DataTable GetPreviousAmountAndType(long ReceiptAndPaymentID)
        {
            return MobjclsDALReceiptsAndPayments.GetPreviousAmountAndType(ReceiptAndPaymentID);
        }
        public bool IsExistAllReferenceIDs(DataTable datDetails)
        {
            return MobjclsDALReceiptsAndPayments.IsExistAllReferenceIDs(datDetails);
        }
        public decimal GetReceiptAmount(int ReferenceID)
        {
            return MobjclsDALReceiptsAndPayments.GetReceiptAmount(ReferenceID);
        }
    }
}
