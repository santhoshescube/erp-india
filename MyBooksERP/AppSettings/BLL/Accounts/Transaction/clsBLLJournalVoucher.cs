﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace MyBooksERP
{      
    public  class clsBLLJournalVoucher
    {
        private clsDALJournalVoucher MobjclsDALJournalVoucher;
        private DataLayer MobjDataLayer;
        public clsDTOJournalVoucher PobjClsDTOJournalVoucher { get; set; }

        public clsBLLJournalVoucher()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALJournalVoucher = new clsDALJournalVoucher(MobjDataLayer);
            PobjClsDTOJournalVoucher = new clsDTOJournalVoucher();
            MobjclsDALJournalVoucher.objDTOJournalVoucher = PobjClsDTOJournalVoucher;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public DateTime GetServerDate()
        {
            return MobjclsDALJournalVoucher.GetServerDate();
        }

        public bool DisplayJournalVoucher(SqlDecimal intVoucherID)
        {
            return MobjclsDALJournalVoucher.DisplayJournalVoucher(intVoucherID);
        }

        public DataTable DisplayJournalVoucherDetails(SqlDecimal dlTempVoucherID)
        {
            return MobjclsDALJournalVoucher.DisplayJournalVoucherDetails(dlTempVoucherID);
        }

        public DataTable DisplayCostCenterDetails(SqlDecimal dlTempVoucherID)
        {
            return MobjclsDALJournalVoucher.DisplayCostCenterDetails(dlTempVoucherID);
        }

        public bool SaveJournalVoucher(DataTable datJVDetails, DataTable datCCDetails)
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALJournalVoucher.SaveJournalVoucher(datJVDetails, datCCDetails);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        
        public bool DeleteJournalVoucher()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALJournalVoucher.DeleteJournalVoucher();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public int GetRecordCount(int intTempCompanyID, int intTempVTypeID)
        {
            return MobjclsDALJournalVoucher.GetRecordCount(intTempCompanyID, intTempVTypeID);
        }

        public string GetVoucherNo(int intTempCompID, int intTempVTypeID, DateTime dtpTempVDate)
        {
            return MobjclsDALJournalVoucher.GetVoucherNo(intTempCompID, intTempVTypeID, dtpTempVDate);
        }

        public bool CheckAccount(int intTempAccountID)
        {
            return MobjclsDALJournalVoucher.CheckAccount(intTempAccountID);
        }

        public bool CheckAccountForCheque(int intTempAccountID)
        {
            return MobjclsDALJournalVoucher.CheckAccountForCheque(intTempAccountID);
        }

        public bool CheckAccountCostCenterApplicable(int intTempAccountID)
        {
            return MobjclsDALJournalVoucher.CheckAccountCostCenterApplicable(intTempAccountID);
        }

        public DataTable getVoucherNos(int intTempVoucherTypeID, int intTempCompanyID, string strTempVNo, string strFromDate, string strToDate)
        {
            return MobjclsDALJournalVoucher.getVoucherNos(intTempVoucherTypeID, intTempCompanyID, strTempVNo, strFromDate, strToDate);
        }

        public decimal getAccountBalance(int intTempAccountID, int intTempCompanyID)
        {
            return MobjclsDALJournalVoucher.getAccountBalance(intTempAccountID, intTempCompanyID);
        }

        public DataTable getRecurringVoucherMasterDetails(int intRecurringJournalSetUpID)
        {
            return MobjclsDALJournalVoucher.getRecurringVoucherMasterDetails(intRecurringJournalSetUpID);
        }

        public DataTable getRecurringVoucherDetails(int intRecurringJournalSetUpID)
        {
            return MobjclsDALJournalVoucher.getRecurringVoucherDetails(intRecurringJournalSetUpID);
        }

        public DataSet DisplayGeneralReceiptsAndPayements(long lngVoucherID)// Display report for Jounrnal Voucher
        {
            return MobjclsDALJournalVoucher.DisplayGeneralReceiptsAndPayements(lngVoucherID);
        }

        public DataTable getInnerGridDetails(int intCompID, string strTemp)
        {
            return MobjclsDALJournalVoucher.getInnerGridDetails(intCompID, strTemp);
        }

        // For Group JV
        public bool SaveVoucherMaster(DataTable datJVDetails)
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALJournalVoucher.SaveVoucherMaster(datJVDetails);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool SaveGroupJVMaster(DataTable datJVDetails, DataTable datGJVDetails)
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALJournalVoucher.SaveGroupJVMaster(datJVDetails, datGJVDetails);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeleteGroupJV()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALJournalVoucher.DeleteGroupJV();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public string GetGroupJVNo()
        {
            return MobjclsDALJournalVoucher.GetGroupJVNo();
        }

        public DataTable getGroupJVNos(string strTempVNo)
        {
            return MobjclsDALJournalVoucher.getGroupJVNos(strTempVNo);
        }

        public DataTable getGroupJVNos(string strTempVNo, string strFromDate, string strToDate)
        {
            return MobjclsDALJournalVoucher.getGroupJVNos(strTempVNo, strFromDate, strToDate);
        }

        public bool DisplayGroupJV(SqlDecimal intVoucherID)
        {
            return MobjclsDALJournalVoucher.DisplayGroupJV(intVoucherID);
        }

        public DataTable DisplayGroupJVDetails(SqlDecimal dlTempVoucherID)
        {
            return MobjclsDALJournalVoucher.DisplayGroupJVDetails(dlTempVoucherID);
        }

        public string getMainVoucherNo()
        {
            return MobjclsDALJournalVoucher.getMainVoucherNo();
        }

        public DataSet DisplayEmailGroupJV(long lngVoucherID)// Display report for Jounrnal Voucher
        {
            return MobjclsDALJournalVoucher.DisplayEmailGroupJV(lngVoucherID);
        }

        public DataTable DisplayGroupJVDetDetails(SqlDecimal dlTempVoucherID)
        {
            return MobjclsDALJournalVoucher.DisplayGroupJVDetDetails(dlTempVoucherID);
        }
    }
}
