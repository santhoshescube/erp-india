﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace MyBooksERP
{
    /*******************************************************
  * Author       : Thasni
  * Created On   : 18 Apr 2012
  * Purpose      : Create business logic layer Recurring Journal
  * *****************************************************/
    public class clsBLLReurringJournalSetup
    {
        clsDALReurringJournalSetup objclsDALRecurringJournalSetup = null;
        public clsPager objPager;

        private clsDALReurringJournalSetup DALRecurringJournalSetup
       {
           get
           {
               if (this.objclsDALRecurringJournalSetup == null)
                   objclsDALRecurringJournalSetup = new clsDALReurringJournalSetup();
               return objclsDALRecurringJournalSetup;
           }
           set
           {
               objclsDALRecurringJournalSetup = new clsDALReurringJournalSetup();
           }
       }

        public clsBLLReurringJournalSetup()
       {
           objPager = new clsPager();
           this.objPager = DALRecurringJournalSetup.objPager;
       }

       public clsDTOReurringJournalSetup DTOReurringJournalSetup
       {
           get { return this.DALRecurringJournalSetup.DTOReurringJournalSetup; }
           set { this.DALRecurringJournalSetup.DTOReurringJournalSetup = value; }
           
       }
       /// <summary>
       /// To Get all Vouchers nos of journal type direct
       /// </summary>
       /// <returns>datatable </returns>
       public DataTable FillComboVoucherNo()
       {
           return this.DALRecurringJournalSetup.FillComboVoucherNo();
       }
       public void BindRecurrenceSetupInfo()
       {
           this.DALRecurringJournalSetup.BindRecurrenceSetupInfo();
       }
       public int SaveRecurrenceFournalSetup()
       {
           return this.DALRecurringJournalSetup.SaveRecurrenceFournalSetup();
       }
       public int GetRecurredCount()
       {
           return this.DALRecurringJournalSetup.GetRecurredCount();
       }
       public bool DeleteRecuuringJournalSetup()
       {
          return this.DALRecurringJournalSetup.DeleteRecuuringJournalSetup();
       }
       public DataTable GetVoucherDetails()
       {
           return this.DALRecurringJournalSetup.GetVoucherDetails();
       }
       public DataTable FillAlerts(int UserId)
       {
           return this.DALRecurringJournalSetup.FillAlerts(UserId);
       }
       public bool IsRecurringExists()
       {
           return this.DALRecurringJournalSetup.IsRecurringExists();
       }
       public bool IsAlertUserSettingsExists()
       {
           return this.DALRecurringJournalSetup.IsAlertUserSettingsExists();
       }

    }
}
