﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsBLLRecurrenceSetup
    {
        private clsDALRecurrenceSetup MobjclsDALRecurrenceSetup;
        private DataLayer MobjDataLayer;
        public clsDTORecurrenceSetup PobjClsDTORecurrenceSetup { get; set; }

        public clsBLLRecurrenceSetup()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALRecurrenceSetup = new clsDALRecurrenceSetup(MobjDataLayer);
            PobjClsDTORecurrenceSetup = new clsDTORecurrenceSetup();
            MobjclsDALRecurrenceSetup.objDTORecurrenceSetup = PobjClsDTORecurrenceSetup;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public void DisplayRecurranceSetup(int intRowNum)
        {
            MobjclsDALRecurrenceSetup.DisplayRecurranceSetup(intRowNum);
        }

        public bool SaveRecurrenceSetup()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALRecurrenceSetup.SaveRecurrenceSetup();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public int GetRecordCount()
        {
            return MobjclsDALRecurrenceSetup.GetRecordCount();
        }

        public bool DeleteRecurranceSetup()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALRecurrenceSetup.DeleteRecurrenceSetup();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
    }
}
