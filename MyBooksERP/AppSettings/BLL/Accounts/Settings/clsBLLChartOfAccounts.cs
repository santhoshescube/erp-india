﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;

namespace MyBooksERP
{
    public class clsBLLChartOfAccounts
    {
        private clsDALChartOfAccounts objclsDALChartOfAccounts;
        private DataLayer objClsConnection;
        public clsDTOChartOfAccounts objMclsDTOChartOfAccounts;

        public clsBLLChartOfAccounts()
        {
            this.objClsConnection = new DataLayer();
            objMclsDTOChartOfAccounts = new clsDTOChartOfAccounts();
            this.objclsDALChartOfAccounts = new clsDALChartOfAccounts();
            this.objclsDALChartOfAccounts.MObjConnection = this.objClsConnection;

        }
     
        ~clsBLLChartOfAccounts()
        {
            this.objClsConnection = null;
            this.objclsDALChartOfAccounts = null;
        }

        /// <summary>
        /// Loading comboboxes with values
        /// </summary>
        /// <param name="Cbo">Combobox name</param>
        /// <param name="saFields"></param>
        /// <returns></returns>
        public DataTable LoadComboboxes(string[] saFields)
        {
            try
            {
                return this.objclsDALChartOfAccounts.LoadComboboxes(saFields);
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable FillAccountsHead()
        {
            try
            {
                return this.objclsDALChartOfAccounts.FillAccountsHead();
            }
            catch (Exception ex) { throw ex; }
        }

        //public DataTable FillAccounts(int iParentID, int iCompanyID)
        //{
        //    try
        //    {
        //        return this.objclsDALChartOfAccounts.FillAccounts(iParentID, iCompanyID);
        //    }
        //    catch (Exception ex) { throw ex; }
        //}
       
        public DataTable GetChildAccountHead(int iParentGroupID)
        {
            try
            {
                return this.objclsDALChartOfAccounts.GetChildGroups(iParentGroupID);
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable GetChildAccounts(int iParentGroupID,int iCompanyID)
        {
            try
            {
                return this.objclsDALChartOfAccounts.GetChildAccounts(iParentGroupID,iCompanyID);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool SaveAccount(bool MbAccountHead, bool MbAddStatus)
        {
            bool blnReturnVal = false;
            try
            {
                //
                this.objClsConnection.BeginTransaction();
                this.objclsDALChartOfAccounts.ObjclsDTOChartOfAccounts = this.objMclsDTOChartOfAccounts;
                blnReturnVal = this.objclsDALChartOfAccounts.SaveAccount(MbAccountHead, MbAddStatus);
              
                this.objClsConnection.CommitTransaction();
                blnReturnVal = true;
            }
            catch (Exception Ex)
            {
                this.objClsConnection.RollbackTransaction();
                throw Ex;
            }
            return blnReturnVal;
        }

        public bool GetAccountInfo(bool MbAccountHead, int iAccountID)
        {
            try
            {
                if (this.objclsDALChartOfAccounts.GetAccountInfo(MbAccountHead, iAccountID))
                {
                    this.objMclsDTOChartOfAccounts = this.objclsDALChartOfAccounts.ObjclsDTOChartOfAccounts;
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool CheckExistReferences(bool MbAccountHead, int iAccountID, bool blnChkSetting)
        {
            try
            {
                return MbAccountHead == true ? this.objclsDALChartOfAccounts.CanDeleteAccountGroup(iAccountID) : this.objclsDALChartOfAccounts.CanDeleteAccount(iAccountID, blnChkSetting);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool DeleteAccount(bool MbAccountHead, int iAccountID)
        {
            bool blnReturnVal = false;
            try
            {
                this.objClsConnection.BeginTransaction();
                if (!MbAccountHead)
                    blnReturnVal = this.objclsDALChartOfAccounts.DeleteAccountHead(iAccountID);
                else
                    blnReturnVal = this.objclsDALChartOfAccounts.DeleteAccountGroup(iAccountID);

                this.objClsConnection.CommitTransaction();
                return blnReturnVal;
            }
            catch (Exception Ex)
            {
                this.objClsConnection.RollbackTransaction();
                return blnReturnVal;
                throw Ex;
            }
            
        }

        public bool CheckDuplication(bool bAddStatus, string[] saValues, long iId, int iCompany, bool bAccountHead)
        {
            try
            {
                return this.objclsDALChartOfAccounts.CheckDuplication(bAddStatus, saValues, iId, iCompany, bAccountHead);
            }
            catch (Exception ex) { throw ex; }
        }

        public void GetReport(System.Windows.Forms.TreeNodeCollection tr, bool bReportMode)
        {
            //bool blnReturnVal = false;
            try
            {
                this.objclsDALChartOfAccounts.ObjclsDTOChartOfAccounts = this.objMclsDTOChartOfAccounts;
                this.objClsConnection.BeginTransaction();
                this.objclsDALChartOfAccounts.ObjclsDTOChartOfAccounts = this.objMclsDTOChartOfAccounts;

                this.objclsDALChartOfAccounts.DeleteAllRowsFromSTTmpAccounts();
                FillReportData(tr, bReportMode);

                this.objClsConnection.CommitTransaction();
                //blnReturnVal = true;
            }
            catch (Exception Ex)
            {
                this.objClsConnection.RollbackTransaction();
                throw Ex;
            }
        }

        private void FillReportData(System.Windows.Forms.TreeNodeCollection tr, bool bReportMode)
        {
            string sQuery = string.Empty;
            string sHead = string.Empty;
            try
            {
                foreach (System.Windows.Forms.TreeNode tr1 in tr)
                {
                    if (tr1.Text.Trim() != string.Empty)
                    {
                        string sSpace = string.Empty;
                        for (int i = 1; i <= tr1.Level; i++)
                        {
                            if (bReportMode)
                                sSpace = sSpace + "             ";
                            else sSpace = sSpace + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        }
                        sHead = sSpace + tr1.Text.Trim();

                        this.objclsDALChartOfAccounts.InsertIntoSTTmpAccount(tr1.ImageIndex, sHead);

                        if (tr1.Nodes.Count > 0)
                        {
                            FillReportData(tr1.Nodes, bReportMode);
                        }
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public bool CheckIsPredefined(long lngRefID, bool IsGroup)
        {
            try
            {
                return this.objclsDALChartOfAccounts.CheckIsPredefined(lngRefID, IsGroup);
            }
            catch { return true; }
        }

        public string GetAccountCode(int intAccountGroupID)
        {
            return objclsDALChartOfAccounts.GetAccountCode(intAccountGroupID);
        }

        public bool CheckAccountCodeAutogenerated(int intAccountGroupID)
        {
            return objclsDALChartOfAccounts.CheckAccountCodeAutogenerated(intAccountGroupID);
        }

        public bool CheckAccountCodeExists(int intAccountID, string strCode)
        {
            return objclsDALChartOfAccounts.CheckAccountCodeExists(intAccountID, strCode);
        }
    }
}