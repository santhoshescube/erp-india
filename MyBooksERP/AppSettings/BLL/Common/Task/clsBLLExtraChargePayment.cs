﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLExtraChargePayment
    {
        private clsDALExtraChargePayment objDALExtraChargePayment { get; set; }
        public clsDTOExtraChargePayment objDTOExtraChargePayment { get; set; }
        private DataLayer objConnection;

        clsDALMailSettings objclsDALMailSettings;
        public clsDTOMailSetting objclsDTOMailSetting;

        public clsBLLExtraChargePayment()
        {
            objDTOExtraChargePayment = new clsDTOExtraChargePayment();
            objDALExtraChargePayment = new clsDALExtraChargePayment();
            objConnection = new DataLayer();
            objDALExtraChargePayment.objDTOExtraChargePayment = objDTOExtraChargePayment;

            this.objclsDALMailSettings = new clsDALMailSettings();
            this.objclsDALMailSettings.objClsConnection = this.objConnection;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objDALExtraChargePayment.objConnection = objConnection;
                    objCommonUtility.PobjDataLayer = objDALExtraChargePayment.objConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataTable GetCompanyByPermission(int RoleID, int MenuID, int ControlID)
        {
            return objDALExtraChargePayment.GetCompanyByPermission(RoleID, MenuID, ControlID);
        }

        public bool SaveExtraChargePayment(DataTable datTemp)
        {
            bool blnRetValue = false;
            try
            {
                objConnection.BeginTransaction();
                this.objDALExtraChargePayment.objDTOExtraChargePayment = this.objDTOExtraChargePayment;
                blnRetValue = objDALExtraChargePayment.SaveExtraChargePayment(datTemp);
                objConnection.CommitTransaction();
            }
            catch (Exception ex)
            {
                objConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeleteExtraChargePayment()
        {
            bool blnRetValue = false;
            try
            {
                objConnection.BeginTransaction();
                this.objDALExtraChargePayment.objDTOExtraChargePayment = this.objDTOExtraChargePayment;
                blnRetValue = objDALExtraChargePayment.DeleteExtraChargePayment();
                objConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable DisplayExtraChargePaymentMaster(long lngRowNum)
        {
            return objDALExtraChargePayment.DisplayExtraChargePaymentMaster(lngRowNum);
        }

        public string getExtraChargePaymentNo(int intCompanyID)
        {
            return objDALExtraChargePayment.getExtraChargePaymentNo(intCompanyID);
        }

        public int getCurrency(long lngExtrChID)
        {
            return objDALExtraChargePayment.getCurrency(lngExtrChID);
        }

        public DataTable getReferenceNo(int intMode, int intCompanyID)
        {
            return objDALExtraChargePayment.getReferenceNo(intMode, intCompanyID);
        }

        public long getRecordCount()
        {
            return objDALExtraChargePayment.getRecordCount();
        }

        public string CheckExtraChargePaymentNoAutoGenerate(int intCompanyID)
        {
            return objDALExtraChargePayment.CheckExtraChargePaymentNoAutoGenerate(intCompanyID);
        }

        public bool CheckExtraChargePaymentNoDuplication(int intCompanyID, long lngExChID, string strExChNo)
        {
            return objDALExtraChargePayment.CheckExtraChargePaymentNoDuplication(intCompanyID, lngExChID, strExChNo);
        }

        public DataTable getExtraChargeNo(int intCompanyID, int intOpTypeID, long lngRefID, int intVendorID)
        {
            return objDALExtraChargePayment.getExtraChargeNo(intCompanyID, intOpTypeID, lngRefID, intVendorID);
        }

        public DataTable getShipmentVendor(int intCompanyID, int intOpTypeID, long lngRefID)
        {
            return objDALExtraChargePayment.getShipmentVendor(intCompanyID, intOpTypeID, lngRefID);
        }

        public DataTable getExtraChargeDetails(int intVendorID, long lngExtraChargeID)
        {
            return objDALExtraChargePayment.getExtraChargeDetails(intVendorID, lngExtraChargeID);
        }

        public decimal getCurrentBalance(long lngExtraChargeID, int intExpenseTypeID, int intVendorID)
        {
            return objDALExtraChargePayment.getCurrentBalance(lngExtraChargeID, intExpenseTypeID, intVendorID);
        }

        public string ConvertToWord(string strAmount, int intCurrencyID)
        {
            return objDALExtraChargePayment.ConvertToWord(strAmount, intCurrencyID);
        }

        public DataSet DisplayExtraChargePayment(long ExtraChargePaymentID)
        {
            return objDALExtraChargePayment.DisplayExtraChargePayment(ExtraChargePaymentID);
        }

        public long getRecordCountForSpecificValue(int intCompID, int OpTypeID, long lngRefID, long lngExChID)
        {
            return objDALExtraChargePayment.getRecordCountForSpecificValue(intCompID, OpTypeID, lngRefID, lngExChID);
        }

        public DataTable DisplayExtraChargePaymentMasterForSpecificValue(long lngRowNum, int intCompID, int OpTypeID, long lngRefID, long lngExChID)
        {
            return objDALExtraChargePayment.DisplayExtraChargePaymentMasterForSpecificValue(lngRowNum, intCompID, OpTypeID, lngRefID, lngExChID);
        }

        public DataTable getShipmentVendorWithExtraChargeNo(int intCompanyID, int intOpTypeID, long lngRefID, long lngExChID)
        {
            return objDALExtraChargePayment.getShipmentVendorWithExtraChargeNo(intCompanyID, intOpTypeID, lngRefID, lngExChID);
        }
    }
}
