﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLExtraCharges
    {
        private clsDALExtraCharges objDALExtraCharges { get; set; }
        public clsDTOExtraCharges objDTOExtraCharges { get; set; }
        private DataLayer objConnection;

        clsDALMailSettings objclsDALMailSettings;
        public clsDTOMailSetting objclsDTOMailSetting;

        public clsBLLExtraCharges()
        {
            objDTOExtraCharges = new clsDTOExtraCharges();
            objDALExtraCharges = new clsDALExtraCharges();
            objConnection = new DataLayer();
            objDALExtraCharges.objDTOExtraCharges = objDTOExtraCharges;

            this.objclsDALMailSettings = new clsDALMailSettings();
            this.objclsDALMailSettings.objClsConnection = this.objConnection;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objDALExtraCharges.objConnection = objConnection;
                    objCommonUtility.PobjDataLayer = objDALExtraCharges.objConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataTable GetCompanyByPermission(int RoleID, int MenuID, int ControlID)
        {
            return objDALExtraCharges.GetCompanyByPermission(RoleID, MenuID, ControlID);
        }

        public bool SaveExtraCharge(DataTable datTemp)
        {
            bool blnRetValue = false;
            try
            {
                objConnection.BeginTransaction();
                this.objDALExtraCharges.objDTOExtraCharges = this.objDTOExtraCharges;
                blnRetValue = objDALExtraCharges.SaveExtraCharge(datTemp);
                objConnection.CommitTransaction();
            }
            catch (Exception ex)
            {
                objConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeleteExtraCharge()
        {
            bool blnRetValue = false;
            try
            {
                objConnection.BeginTransaction();
                this.objDALExtraCharges.objDTOExtraCharges = this.objDTOExtraCharges;
                blnRetValue = objDALExtraCharges.DeleteExtraCharge();
                objConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeleteExtraChargeDetails(bool blnIsRowDelete, int intSerialNo)
        {
            bool blnRetValue = false;
            try
            {
                objConnection.BeginTransaction();
                this.objDALExtraCharges.objDTOExtraCharges = this.objDTOExtraCharges;
                blnRetValue = objDALExtraCharges.DeleteExtraChargeDetails(blnIsRowDelete, intSerialNo);
                objConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable DisplayExtraChargeMaster(long lngRowNum)
        {
            return objDALExtraCharges.DisplayExtraChargeMaster(lngRowNum);
        }

        public string getExtraChargeNo(int intCompanyID)
        {
            return objDALExtraCharges.getExtraChargeNo(intCompanyID);
        }

        public int getCurrency(int intCompanyID)
        {
            return objDALExtraCharges.getCurrency(intCompanyID);
        }

        public DataTable getReferenceNo(int intMode, int intCompanyID)
        {
            return objDALExtraCharges.getReferenceNo(intMode, intCompanyID);
        }

        public long getRecordCount()
        {
            return objDALExtraCharges.getRecordCount();
        }

        public string CheckExtraChargeAutoGenerate(int intCompanyID)
        {
            return objDALExtraCharges.CheckExtraChargeAutoGenerate(intCompanyID);
        }

        public bool CheckExtraChargeDuplication(int intCompanyID, long lngExChID, string strExChNo)
        {
            return objDALExtraCharges.CheckExtraChargeDuplication(intCompanyID, lngExChID, strExChNo);
        }

        public DataSet PrintExtraCharges(long ExtraChargeID)
        {
            return objDALExtraCharges.PrintExtraCharges(ExtraChargeID);
        }

        public bool CheckIDExists(long lngExChID)
        {
            return objDALExtraCharges.CheckIDExists(lngExChID);
        }

        public string ConvertToWord(string strAmount, int intCurrencyID)
        {
            return objDALExtraCharges.ConvertToWord(strAmount, intCurrencyID);
        }

        public long getRecordCountForSpecificValue(int intCompID, int OpTypeID, long lngRefID)
        {
            return objDALExtraCharges.getRecordCountForSpecificValue(intCompID, OpTypeID, lngRefID);
        }

        public DataTable DisplayExtraChargeMasterForSpecificValue(long lngRowNum, int intCompID, int OpTypeID, long lngRefID)
        {
            return objDALExtraCharges.DisplayExtraChargeMasterForSpecificValue(lngRowNum, intCompID, OpTypeID, lngRefID);
        }
        public bool CheckPaymentExist(int intExtraChargeID)
        {
            return objDALExtraCharges.CheckPaymentExist(intExtraChargeID);
        }
        public bool CheckPurchaseInvoiceStatus(int intReferenceID)
        {
            return objDALExtraCharges.CheckPurchaseInvoiceStatus(intReferenceID);
        }
        public bool CheckPurchaseOrderStatus(int intReferenceID)
        {
            return objDALExtraCharges.CheckPurchaseOrderStatus(intReferenceID);
        }
        public bool GetCompanyAccount(int intCompanyID, int intTransactionType)
        {
            return objDALExtraCharges.GetCompanyAccount(intCompanyID, intTransactionType);
        }
        public bool CheckSalesInvoiceStatus(int intReferenceID)
        {
            return objDALExtraCharges.CheckSalesInvoiceStatus(intReferenceID);
        }
        public bool CheckSalesOrderStatus(int intReferenceID)
        {
            return objDALExtraCharges.CheckSalesOrderStatus(intReferenceID);
        }
    }
}