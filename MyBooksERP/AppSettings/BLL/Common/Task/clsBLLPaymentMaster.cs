﻿using System;
using System.Collections.Generic;
using System.Data;


/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,16 Mar 2011>
   Description:	<Description,,Payments BLL>
================================================
*/
namespace MyBooksERP
{
    public class clsBLLPaymentMaster
    {
        private clsDALPaymentMaster objDALPaymentMaster { get; set; } // obj of clsDALPaymentMaster
        public clsDTOPaymentMaster objDTOPaymentMaster { get; set; }  // obj of clsDTOPaymentMaster   
        private DataLayer objDataLayer;                               // obj of DataLayer              

        public clsBLLPaymentMaster()
        {
            objDTOPaymentMaster = new clsDTOPaymentMaster();
            objDALPaymentMaster = new clsDALPaymentMaster();
            objDataLayer = new DataLayer();
            objDALPaymentMaster.objDTOPaymentMaster = objDTOPaymentMaster;
            objDALPaymentMaster.objDataLayer = objDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            return objDALPaymentMaster.FillCombos(saFieldValues);
        }

        public bool GetPaymentsInfo(int intModuleID)
        {
            //function for getting payments information
            return objDALPaymentMaster.GetPaymentsInfo(intModuleID);
        }

        public bool SavePayments()
        {
            //saving payments
            bool blnSaved = false;
            try
            {
                objDataLayer.BeginTransaction();
                blnSaved = objDALPaymentMaster.SavePayments();
                objDataLayer.CommitTransaction();
            }
            catch(Exception ex)
            {
                objDataLayer.RollbackTransaction();
                throw ex;
            }
                
            return blnSaved;
        }

        public bool DeletePayments()
        {
            //deleting payments
            bool blnDeleted = false;
            try
            {
                objDataLayer.BeginTransaction();
                blnDeleted = objDALPaymentMaster.DeletePayments();
                objDataLayer.CommitTransaction();
            }
            catch(Exception ex)
            {
                objDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnDeleted;
        }

        public int GetRecordCount(int intRoleID, int intCompanyID, int intControlID,int intModuleID)
        {
            //function for getting record count
            return objDALPaymentMaster.GetRecordCount(intRoleID, intCompanyID, intControlID,intModuleID);
        }

        public DataTable GetAccounts()
        {
            //function for getting data for filling CreditHead and DebitHead
            return objDALPaymentMaster.GetAccounts();
        }

        public Int64 GetMaxSerialNo(int intCompanyID)
        {
            // function for getting max of PaymentID
            return objDALPaymentMaster.GetMaxSerialNo(intCompanyID);
        }

        public DataSet GetPaymentReport()
        {
            // function for getting report details
            return objDALPaymentMaster.GetPaymentReport();
        }


        public decimal GetBalanceAmount(int intOperationTypeID, int intReferenceID,int intPaymentType)
        {
            //Function for getting balance amount for reference no
            return objDALPaymentMaster.GetBalanceAmount(intOperationTypeID, intReferenceID,intPaymentType);
        }

        public bool GetCompanyAccountID(int intCompany,bool blnIsPayment)
        {
            // Function for getting company account id
            return objDALPaymentMaster.GetCompanyAccount(intCompany, blnIsPayment);
        }

        public DataSet GetFilterDetails(string StrFilter)
        {
            // Function for getting Receipt || Payment Details
            return objDALPaymentMaster.GetFilterDetails(StrFilter);
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return objDALPaymentMaster.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetOperationTypeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return objDALPaymentMaster.GetOperationTypeByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetPendingInvoices(int intCompanyID,int intType)
        {
            return objDALPaymentMaster.GetPendingInvoices(intCompanyID,intType);
        }
    }
}
