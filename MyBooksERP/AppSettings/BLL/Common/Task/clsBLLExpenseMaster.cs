﻿using System;
using System.Collections.Generic;
using System.Data;



namespace MyBooksERP
{
    public class clsBLLExpenseMaster
    {
        private clsDALExpenseMaster objDALExpenseMaster { get; set; }
        public clsDTOExpenseMaster objDTOExpenseMaster { get; set; }
        private DataLayer objConnection;

        clsDALMailSettings objclsDALMailSettings;
        public clsDTOMailSetting objclsDTOMailSetting;

        public clsBLLExpenseMaster()
        {
            objDTOExpenseMaster = new clsDTOExpenseMaster();
            objDALExpenseMaster = new clsDALExpenseMaster();
            objConnection = new DataLayer();
            objDALExpenseMaster.objConnection = objConnection;
            objDALExpenseMaster.objDTOExpenseMaster = objDTOExpenseMaster;

            this.objclsDALMailSettings = new clsDALMailSettings();
            this.objclsDALMailSettings.objClsConnection = this.objConnection;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objDALExpenseMaster.objConnection = objConnection;
                    objCommonUtility.PobjDataLayer = objDALExpenseMaster.objConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool GetExpenseInfo(int PintOperationType, long PintOperationID,int intModuleID)
        {
            objDALExpenseMaster.objConnection = objConnection;
            return objDALExpenseMaster.GetExpenseInfo(PintOperationType,PintOperationID,intModuleID);
        }

        public bool SaveExpenseInfo()
        {
            bool blnSaved = false;
            try
            {
                objConnection.BeginTransaction();
                objDALExpenseMaster.objConnection = objConnection;
                blnSaved = objDALExpenseMaster.SaveExpenseInfo();
                objConnection.CommitTransaction();
            }
            catch(Exception ex)
            {
                objConnection.RollbackTransaction();
                throw ex;
            }
                
            return blnSaved;
        }

        public bool DeleteExpenseInfo()
        {
            objDALExpenseMaster.objConnection = objConnection;
            return objDALExpenseMaster.DeleteExpenseInfo();
        }

        public bool DeleteExpenseDetails(int intSerialNo,decimal decTotalAmount)
        {
            objDALExpenseMaster.objConnection = objConnection;
            return objDALExpenseMaster.DeleteExpenseDetails(intSerialNo,decTotalAmount);
        }

        public int GetRecordCount(int PintOperationType, long PlngOperationID, int intRoleID,int intCompanyID,int intControlID,int intModuleID)
        {
            objDALExpenseMaster.objConnection = objConnection;
            return objDALExpenseMaster.GetRecordCount(PintOperationType, PlngOperationID, intRoleID, intCompanyID, intControlID,intModuleID);
        }

        public DataTable GetAccounts()
        {
            objDALExpenseMaster.objConnection = objConnection;
            return objDALExpenseMaster.GetAccounts();
        }

        public DataSet GetExpenseReport()
        {
            objDALExpenseMaster.objConnection = objConnection;
            return objDALExpenseMaster.GetExpenseReport();
        }

        public bool CheckSalesOrderEditable(int intMode,Int64 intReferenceID, string strSearchCondition)
        {
            objDALExpenseMaster.objConnection = objConnection;
            return objDALExpenseMaster.CheckSalesOrderEditable(intMode, intReferenceID, strSearchCondition);
        }

        public double GetExpenseAmount(int intOperationType, Int64 intReferenceID,long intExpenseID)
        {
            return objDALExpenseMaster.GetExpenseAmount(intOperationType, intReferenceID,intExpenseID);
        }
        public double GetPaidAmount(int intOperationType, Int64 intReferenceID)
        {
            return objDALExpenseMaster.GetPaidAmount(intOperationType, intReferenceID);
        }
        public DataTable AccountDetails()
        {
            return objDALExpenseMaster.GetAccount(4);
        }

        public void GetMailSetting()
        {
            try
            {
                this.objclsDALMailSettings.GetMailSetting(2);//SMS
                this.objclsDTOMailSetting = this.objclsDALMailSettings.objclsDTOMailSetting;
            }
            catch (Exception ex) { throw ex; }
        }

        public string GetVendorMobileNo(int iCustomerID)
        {
            clsDALVendorInformation objclsDALVendorInformation;

            try
            {
                objclsDALVendorInformation = new clsDALVendorInformation
                {
                    objclsConnection = this.objConnection
                };
                return objclsDALVendorInformation.GetVendorMobileNo(iCustomerID);
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            objDALExpenseMaster.objConnection = objConnection;
            return objDALExpenseMaster.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
        }

        public DataTable GetOperationTypeByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            objDALExpenseMaster.objConnection = objConnection;
            return objDALExpenseMaster.GetOperationTypeByPermission(intRoleID, intCompanyID, intControlID);
        }
    }
}
