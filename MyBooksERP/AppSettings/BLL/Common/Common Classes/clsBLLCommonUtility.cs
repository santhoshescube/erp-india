﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Security.Cryptography;
using System.IO;
using System.Windows.Forms;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,16 Feb 2011>
   Description:	<Description,,CompanyInformation BLL Class>
================================================
*/
namespace MyBooksERP
{
    public class clsBLLCommonUtility
    {
        ClsCommonUtility MobjclsCommonUtility;
        private DataLayer MobjDataLayer;

        public clsBLLCommonUtility()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
             MobjclsCommonUtility = new ClsCommonUtility();
             MobjclsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public decimal GetStockQuantity(int intItemID, Int64 intBatchID, int intCompanyID, int intWarehouseID)
        {
            return MobjclsCommonUtility.GetStockQuantity(intItemID, intBatchID, intCompanyID, intWarehouseID);
        }

        public decimal GetStockQuantity(int intItemID, Int64 intBatchID, int intCompanyID, string strInvoiceDate)
        {
            return MobjclsCommonUtility.GetStockQuantity(intItemID, intBatchID, intCompanyID, strInvoiceDate);
        }

        public decimal GetSaleRate(int intItemID, int intUomID, long intBatchID, int intCompanyID)
        {
            return MobjclsCommonUtility.GetSaleRate(intItemID, intUomID, intBatchID, intCompanyID);
        }
        public string GetCompanyCurrency(int intCompanyID, out int intScale, out int intCurrencyID)
        {
            return MobjclsCommonUtility.GetCompanyCurrency(intCompanyID, out intScale, out intCurrencyID);
        }
        public string ConvertToWord(string strAmount, int intCurrencyID)
        {
            return MobjclsCommonUtility.ConvertToWord(strAmount,intCurrencyID);
        }
        public DataTable FillCombos(string[] saFilterValues)
        {
            return MobjclsCommonUtility.FillCombos(saFilterValues);
        }

        public decimal ConvertQtyToBaseUnitQty(int intUomID, int intItemId, decimal decQuantity,int intUOMTypeId)
        {
            return MobjclsCommonUtility.ConvertQtyToBaseUnitQty(intUomID, intItemId, decQuantity,intUOMTypeId);
        }

        public decimal ConvertBaseUnitQtyToOtherQty(int intUomID, int intItemId, decimal decQuantity, int intUOMTypeID)
        {
            return MobjclsCommonUtility.ConvertBaseUnitQtyToOtherQty(intUomID, intItemId, decQuantity, intUOMTypeID);
        }

        public static string Encrypt(string strText, string strEncrKey)
        {
            byte[] byKey = null;
            byte[] IV = { 0X12, 0X34, 0X56, 0X78, 0X90, 0XAB, 0XCD, 0XEF };

            try
            {
                byKey = System.Text.Encoding.UTF8.GetBytes(strEncrKey.Substring(0, 8));

                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.UTF8.GetBytes(strText);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static string Decrypt(string strText, string sDecrKey)
        {
            byte[] byKey = null;
            byte[] IV = { 0X12, 0X34, 0X56, 0X78, 0X90, 0XAB, 0XCD, 0XEF };
            byte[] inputByteArray = new byte[strText.Length + 1];

            try
            {
                byKey = System.Text.Encoding.UTF8.GetBytes(sDecrKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(strText);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);

                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;

                return encoding.GetString(ms.ToArray());

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public void SetSerialNo(DataGridView dgv, int intRowIndex, bool blnIncludeLastRow)
        {
            try
            {
                int intRowCount = dgv.RowCount;
                if (!blnIncludeLastRow)
                    intRowCount = dgv.RowCount - 1;

                for (int i = intRowIndex - 1; i < dgv.Rows.Count; i++)
                {
                    if (i >= 0 && i < intRowCount)
                        dgv.Rows[i].HeaderCell.Value = (i + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SetSerialNo() " + ex.Message);
                // MobjClsLogWriter.WriteLog("Error in SetSerialNo() " + ex.Message, 2);
            }
        }


        public decimal GetPurchaseRate(int intItemID, int intUomID, long intBatchID, int intCompanyID)
        {
            return MobjclsCommonUtility.GetPurchaseRate(intItemID, intUomID, intBatchID, intCompanyID);
        }

        public decimal GetCutOffPrice(int intItemID)
        {
            return MobjclsCommonUtility.GetCutOffPrice(intItemID);
        }

        public int GetCompanyID()
        {
            return MobjclsCommonUtility.GetCompanyID();
        }

        public int GetBaseUomByItemID(int intItemID)
        {
            return MobjclsCommonUtility.GetBaseUomByItemID(intItemID);
        }

        public DataTable ConvertRateFromBaseUOMtoOtherUOM(int intBaseUOMID, int intOtherUOMID)
        {
            return MobjclsCommonUtility.ConvertRateFromBaseUOMtoOtherUOM(intBaseUOMID, intOtherUOMID);
        }

    }
}
