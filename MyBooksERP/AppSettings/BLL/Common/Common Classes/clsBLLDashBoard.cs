﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsBLLDashBoard
    {
        clsDALDashBoard MobjClsDALDashBoard;

        public clsBLLDashBoard()
        {
            DataLayer objDataLayer = new DataLayer();
            MobjClsDALDashBoard = new clsDALDashBoard(objDataLayer);
        }

        public DataTable GetChartDetails(DateTime dtMonthYear, int intMenuID, int intItemID)
        {
            return MobjClsDALDashBoard.GetChartDetails(dtMonthYear, intMenuID, intItemID);
        }

        public DataTable GetDetails(DateTime dtMonthYear, int intMenuID, int intItemID)
        {
            return MobjClsDALDashBoard.GetDetails(dtMonthYear, intMenuID, intItemID);
        }

        public DataTable GetChartSummary(DateTime dtMonthYear, int intMenuID, int intItemID)
        {
            return MobjClsDALDashBoard.GetChartSummary(dtMonthYear, intMenuID, intItemID);
        }

        public DataTable getItems(DateTime dtMonthYear)
        {
            return MobjClsDALDashBoard.getItems(dtMonthYear);
        }
    }
}
