﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;

namespace MyBooksERP
{
    public class clsBLLPermissionSettings
    {
        clsDALPermissionSettings MobjClsDALPermissionSettings;
        
        public clsBLLPermissionSettings()
        {
            DataLayer objDataLayer = new DataLayer();
            MobjClsDALPermissionSettings = new clsDALPermissionSettings(objDataLayer);
        }
        public void GetPermissions(int intRoleID, int intCompanyID, int intModuleID, int intMenuID, out bool blnPrintEmailPermission, out bool blnCreatePermission, out bool blnUpdatePermission, out bool blnDeletePermission)
        {
            MobjClsDALPermissionSettings.GetPermissions(intRoleID, intCompanyID, intModuleID, intMenuID, out blnPrintEmailPermission, out blnCreatePermission, out blnUpdatePermission, out blnDeletePermission);
        }

        public DataTable GetControlsPermissions(int intRoleID, int intMenuID,int intCompanyID)
        {
           return MobjClsDALPermissionSettings.GetControlsPermissions(intRoleID, intMenuID,intCompanyID);
        }
        public DataTable GetMenuPermissions(int intRoleID)
        {
            return MobjClsDALPermissionSettings.GetMenuPermissions(intRoleID);
        }
        public DataTable GetMenuPermissions(int intRoleID, int intCompanyID)
        {
            return MobjClsDALPermissionSettings.GetMenuPermissions(intRoleID, intCompanyID);
        }
        public DataTable GetPermissionsForCompany(int intRoleID)
        {
            return MobjClsDALPermissionSettings.GetPermissionsForCompany(intRoleID);
        }
        public DataTable GetPermittedCompany(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALPermissionSettings.GetPermittedCompany(intRoleID, intCompanyID, intControlID);
        }
        public DataTable GetControlPermissions1(int intRoleID, int intCompanyID, int intControlID) // For Getting Individual Control Permissions
        {
            return MobjClsDALPermissionSettings.GetControlPermissions1(intRoleID,intCompanyID,intControlID);
        }

        public DataTable GetPermittedCompany(int intUserID)
        {
            return MobjClsDALPermissionSettings.GetPermittedCompany(intUserID);
        }
    }
}
