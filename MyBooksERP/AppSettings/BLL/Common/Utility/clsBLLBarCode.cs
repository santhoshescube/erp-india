﻿

using System.Data;

namespace MyBooksERP
{
    public class clsBLLBarCode
    {
        public clsDTOBarCode objDTOBarCode { get; set; }
        clsDALBarCode objDALBarCode { get; set; }

        public clsBLLBarCode()
        {
            objDTOBarCode = new clsDTOBarCode();
            objDALBarCode = new clsDALBarCode();
            objDALBarCode.objDTOBarCode = objDTOBarCode;
            objDALBarCode.objConnection = new DataLayer();
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objDALBarCode.FillCombos(saFieldValues);
        }

        public bool GetBarCode(int MintMode, long PlngItemID, string PstrBarcodeValue)
        {
            return objDALBarCode.GetBarCode(MintMode,PlngItemID, PstrBarcodeValue);
        }

        public bool SaveBarCode()
        {
            return objDALBarCode.SaveBarCode();
        }
        public void SaveBatchBarCode()
        {
             objDALBarCode.SaveBatchBarCode();
        }
        public bool DeleteBarCode()
        {
            return objDALBarCode.DeleteBarCode();
        }

        public int GetBarCodeCount()
        {
            return objDALBarCode.GetBarCodeCount();
        }

        public string GetBarCodeValue()
        {
            return objDALBarCode.GetBarCodeValue();
        }
    }
}
