﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
/* 
=================================================
   Author:		< Sanju >
   Create date: < 15 Dec 2011 >
   Description:	< Advance Search BLL>
================================================
*/
namespace MyBooksERP
{
    public class clsBLLAdvanceSearch
    {
        clsDALAdvanceSearch objclsDALAdvanceSearch;
        public clsDTOAdvanceSearch objclsDTOAdvanceSearch;
        private DataLayer objClsConnection;

        public clsBLLAdvanceSearch()
        {
            objClsConnection = new DataLayer();
            objclsDALAdvanceSearch = new clsDALAdvanceSearch(objClsConnection);
            objclsDTOAdvanceSearch = new clsDTOAdvanceSearch();
            objclsDALAdvanceSearch.objclsConnection = objClsConnection;
            objclsDALAdvanceSearch.objclsDTOAdvanceSearch = objclsDTOAdvanceSearch;
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            return objclsDALAdvanceSearch.FillCombos(sarFieldValues);
        }

        public DataTable GetSearchColumn()
        {
            return objclsDALAdvanceSearch.GetSearchColumn();
        }

        public DataTable GetSearchData()
        {
            return objclsDALAdvanceSearch.GetSearchData();
        }

        public DataTable GetUserByPermission(int intRoleID, int intCompanyID, int intControlID, int intEmpControlID, int intUserID)
        {
            return objclsDALAdvanceSearch.GetUserByPermission(intRoleID, intCompanyID, intControlID, intEmpControlID, intUserID);
        }
    }
}