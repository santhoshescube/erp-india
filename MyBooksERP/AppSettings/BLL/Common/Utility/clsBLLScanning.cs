﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


/* 
=================================================
Author:		<Author,Arun>
Create date: <Create Date,18 Apr 2012>
Description:	<Description,BLL for Document Attach>
================================================
*/
namespace MyBooksERP
{
    public class clsBLLScanning
    {
        clsDTOScanning objclsDTOScanning;
        clsDALScanning objclsDALScanning;
        private DataLayer objconnection;

        public clsBLLScanning()
        {
            objconnection = new DataLayer();
            objclsDTOScanning = new clsDTOScanning();
            objclsDALScanning = new clsDALScanning();
            objclsDALScanning.objclsDTOScanning = objclsDTOScanning;
        }

        public clsDTOScanning clsDTOScanning
        {
            get { return objclsDTOScanning; }
            set { objclsDTOScanning = value; }
        }


        public int Print()
        {
            objclsDALScanning.objclsconnection = objconnection;
            return objclsDALScanning.Print();
        }

        public int AddNew()
        {
            objclsDALScanning.objclsconnection = objconnection;
            return objclsDALScanning.AddNew();
        }

        public void InsertMaster()
        {
            objclsDALScanning.objclsconnection = objconnection;
            objclsDALScanning.InsertMaster();
        }

        public bool Save(bool AddStatus)
        {
            bool blnRetValue = false;
            try
            {

                objconnection.BeginTransaction();
                objclsDALScanning.objclsconnection = objconnection;
                objclsDALScanning.Save(AddStatus);
                objconnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objconnection.RollbackTransaction();
                throw ex;
            }

            return blnRetValue;
        }

        public bool DeleteNodes(int iMode, int iDocAttachID)
        {
            bool blnRetValue = false;
            try
            {
                objconnection.BeginTransaction();
                objclsDALScanning.objclsconnection = objconnection;
                blnRetValue = objclsDALScanning.DeleteNodes(iMode, iDocAttachID);
                objconnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objconnection.RollbackTransaction();
                throw ex;

            }
            return blnRetValue;

        }


        public bool SelectDocumentType(int iDocumentTypeID)
        {
            objclsDALScanning.objclsconnection = objconnection;
            objclsDTOScanning = objclsDALScanning.objclsDTOScanning;
            return objclsDALScanning.SelectDocumentType(iDocumentTypeID);
        }


        public bool InsertDetails(int intDocAttachID, string strFilename, string strMetaData)
        {
            objclsDALScanning.objclsconnection = objconnection;
            objclsDTOScanning = objclsDALScanning.objclsDTOScanning;
            return objclsDALScanning.InsertDetails(intDocAttachID, strFilename, strMetaData);
        }

        public bool SelectNode(int iRefID, string strNodeName, int iOperTypeID)
        {
            objclsDALScanning.objclsconnection = objconnection;
            objclsDTOScanning = objclsDALScanning.objclsDTOScanning;
            return objclsDALScanning.SelectNode(iRefID, strNodeName, iOperTypeID);
        }

        public bool SelectDetails(int iDocAttachID)
        {
            objclsDALScanning.objclsconnection = objconnection;
            objclsDTOScanning = objclsDALScanning.objclsDTOScanning;
            return objclsDALScanning.SelectDetails(iDocAttachID);
        }

        //selectforupdate
        public bool SelectForUpdate(string strNodeName)
        {
            objclsDALScanning.objclsconnection = objconnection;
            objclsDTOScanning = objclsDALScanning.objclsDTOScanning;
            return objclsDALScanning.SelectForUpdate(strNodeName);

        }

        public bool SelectfromTreeMaster(int intDocTypeID)
        {
            objclsDALScanning.objclsconnection = objconnection;
            objclsDTOScanning = objclsDALScanning.objclsDTOScanning;
            return objclsDALScanning.SelectfromTreeMaster(intDocTypeID);

        }

        public DataTable Fill(int iCompanyID, string strNodeName, int iOpTypeID, int iParentNode)
        {
            objclsDALScanning.objclsconnection = objconnection;
            objclsDTOScanning = objclsDALScanning.objclsDTOScanning;
            return objclsDALScanning.Fill(iCompanyID, strNodeName, iOpTypeID, iParentNode);
        }

        public DataTable Display(int iOpTypeID, int iParentnode)
        {
            objclsDALScanning.objclsconnection = objconnection;
            objclsDTOScanning = objclsDALScanning.objclsDTOScanning;
            return objclsDALScanning.Display(iOpTypeID,iParentnode);
        }

        public DataTable GetparentNodes(int intOperationTypeID, int intReferenceID, int intDocumentTypeID,string strNodeName)
        {
            objclsDALScanning.objclsconnection = objconnection;
            objclsDTOScanning = objclsDALScanning.objclsDTOScanning;
            return objclsDALScanning.GetparentNodes(intOperationTypeID, intReferenceID, intDocumentTypeID, strNodeName);
        }
    }
}

