﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Collections;

namespace MyBooksERP
{
    public class clsBLLAlert
    {
        private clsDALAlerts objclsDALAlerts;
        private clsDALAlertSetting objclsDALAlertSetting;
        private DataLayer objClsConnection;
        public clsDTOMailSetting objclsDTOMailSetting; 
        private Queue<long> queAlertID = null;
        private DataTable datAlertMsg;
        public string CurDate;

        public clsBLLAlert()
        {
            this.objClsConnection = new DataLayer();
            this.objclsDALAlertSetting = new clsDALAlertSetting();
            this.objclsDALAlerts = new clsDALAlerts();
            this.objclsDALAlerts.objClsConnection = this.objClsConnection;
            this.objclsDALAlertSetting.objClsConnection = this.objClsConnection;
            this.queAlertID = new Queue<long>();
        }

        ~clsBLLAlert()
        {
            this.objClsConnection = null;
            this.objclsDALAlerts = null;
            this.queAlertID.Clear();
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            return objclsDALAlerts.FillCombos(sarFieldValues);
        }

        public DataTable GetToDayAlerts(int intModuleID)
        {
            DateTime dtmToDay;
            try
            {
                dtmToDay = new DateTime(ClsCommonSettings.GetServerDate().Year, ClsCommonSettings.GetServerDate().Month, ClsCommonSettings.GetServerDate().Day, 23, 59, 59);
                CurDate = dtmToDay.ToString();

                if (intModuleID == (int)eModuleID.Inventory)
                {
                    this.GetReorderEnteredItemsAlert(Convert.ToInt64(AlertSettingsTypes.ReorderLevelEntered));
                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.PurchaseQuotationDueDate));
                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.PurchaseOrderApproved));
                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.PurchaseOrderDueDate));
                    //this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.RFQDueDate));
                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.DocumentExpiry));

                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.SalesQuotationSubmitted));
                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.SalesQuotationDueDate));
                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.SalesQuotationApproved));
                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.SalesQuotationCancelled));
                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.SaleOrderCreated));
                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.SalesOrderAdvancePaid));

                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.SaleOrderSubmitted));
                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.SaleOrderRejected));
                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.SalesOrderDueDate));
                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.SalesInvoiceDueDate));
                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.Receipt));
                    this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.DeliveryNote));
                }

                this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.SaleOrderApproved));
                this.GetUserAlert(Convert.ToInt64(AlertSettingsTypes.MaterialIssue));

                this.GetOpenedAlerts(dtmToDay);
                return this.datAlertMsg;
            }
            catch
            {
                return null;
            }
        }

        #region Alert Closing

        private void GetOpenedAlerts(DateTime dtmDate)
        {
            DataTable datAlert = null;
            
            try
            {
                datAlert = this.objclsDALAlerts.GetUserDayAlerts(ClsCommonSettings.UserID, dtmDate);
                if (datAlert == null || datAlert.Rows.Count == 0) return;

                this.CheckPurchaseOrderStatus(datAlert);
                this.CheckRFQStatus(datAlert);
                this.CheckPurchaseQuotationStatus(datAlert);
                this.CheckPurchaseOrderOpStatus(datAlert);
                this.CheckSalesQuotationStatus(datAlert);
                this.CheckSalesOrderStatus(datAlert);
                this.CheckSalesInvoiceStatus(datAlert);
                this.CheckAllAlerts(datAlert);

                if (queAlertID != null && queAlertID.Count > 0)
                {
                    this.objclsDALAlerts.CloseAlert(this.queAlertID);
                    this.queAlertID.Clear();
                }
                
                this.datAlertMsg = datAlert;
                datAlert = null;
            }
            catch (Exception ex) { throw ex; }
            finally
            {
            }
        }
        

        #endregion

        private void CheckPurchaseOrderStatus(DataTable dtAlerts)
        {
            DataRow[] dtRows;
            int iStatus;
            bool blnIsRepeat=true;

            try
            {
                dtRows = dtAlerts.Select("OperationTypeID = " + Convert.ToInt32(OperationType.PurchaseOrder).ToString());
                if (dtRows == null || dtRows.Count() == 0)
                    return;

                foreach (DataRow dtOrderRow in dtRows)
                {
                    DataTable datTemp = FillCombos(new string[] { "ISNULL(StatusID,0) AS StatusID,DueDate", "InvPurchaseOrderMaster", "PurchaseOrderID=" + Convert.ToInt64(dtOrderRow["ReferenceID"]) });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            iStatus = Convert.ToInt32(datTemp.Rows[0]["StatusID"].ToString());//this.objclsDALAlerts.GetPurchaseOrderStatus(Convert.ToInt64(dtOrderRow["ReferenceID"]));
                            if (iStatus == 0 || ((OperationStatusType)iStatus) == OperationStatusType.POrderClosed)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString()).AddDays(Convert.ToDouble(dtOrderRow["ValidPeriod"])))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else
                            {
                                blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                                if (!blnIsRepeat)
                                {
                                    this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                }
                                blnIsRepeat = true;
                            }
                        }
                        else
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                            dtAlerts.Rows.Remove(dtOrderRow);
                        }
                    }
                    else
                    {
                        this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        dtAlerts.Rows.Remove(dtOrderRow);
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private void CheckRFQStatus(DataTable dtAlerts)
        {
            DataRow[] dtRows;
            int iStatus;
            bool blnIsRepeat = true;

            try
            {
                dtRows = dtAlerts.Select("OperationTypeID = " + Convert.ToInt32(OperationType.RFQ).ToString());
                if (dtRows == null || dtRows.Count() == 0)
                    return;

                foreach (DataRow dtOrderRow in dtRows)
                {
                    DataTable datTemp = FillCombos(new string[] { "ISNULL(StatusID,0) AS StatusID,DueDate", "InvRFQMaster", "RFQID=" + Convert.ToInt64(dtOrderRow["ReferenceID"]) });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            iStatus = Convert.ToInt32(datTemp.Rows[0]["StatusID"].ToString());
                            if (iStatus == 0 || ((RFQStatus)iStatus) == RFQStatus.Closed)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString()).AddDays(Convert.ToDouble(dtOrderRow["ValidPeriod"])))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else
                            {
                                blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                                if (!blnIsRepeat)
                                {
                                    this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                }
                                blnIsRepeat = true;
                            }
                        }
                        else
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                            dtAlerts.Rows.Remove(dtOrderRow);
                        }
                    }
                    else
                    {
                        this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        dtAlerts.Rows.Remove(dtOrderRow);
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private void CheckPurchaseQuotationStatus(DataTable dtAlerts)
        {
            DataRow[] dtRows;
            int iStatus;
            bool blnIsRepeat = true;

            try
            {
                dtRows = dtAlerts.Select("OperationTypeID = " + Convert.ToInt32(OperationType.PurchaseQuotation).ToString());
                if (dtRows == null || dtRows.Count() == 0)
                    return;

                foreach (DataRow dtOrderRow in dtRows)
                {
                    DataTable datTemp = FillCombos(new string[] { "ISNULL(StatusID,0) AS StatusID,DueDate", "InvPurchaseQuotationMaster", "PurchaseQuotationID=" + Convert.ToInt64(dtOrderRow["ReferenceID"]) });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            iStatus = Convert.ToInt32(datTemp.Rows[0]["StatusID"].ToString());
                            if (iStatus == 0 || ((OperationStatusType)iStatus) == OperationStatusType.PQuotationClosed)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString()).AddDays(Convert.ToDouble(dtOrderRow["ValidPeriod"])))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else
                            {
                                blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                                if (!blnIsRepeat)
                                {
                                    this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                }
                                blnIsRepeat = true;
                            }
                        }
                        else
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                            dtAlerts.Rows.Remove(dtOrderRow);
                        }
                    }
                    else
                    {
                        this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        dtAlerts.Rows.Remove(dtOrderRow);
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private void CheckPurchaseOrderOpStatus(DataTable dtAlerts)
        {
            DataRow[] dtRows;
            int iStatus;
            bool blnIsRepeat = true;

            try
            {
                dtRows = dtAlerts.Select("OperationTypeID = " + Convert.ToInt32(OperationType.PurchaseOrder).ToString());
                if (dtRows == null || dtRows.Count() == 0)
                    return;

                foreach (DataRow dtOrderRow in dtRows)
                {
                    DataTable datTemp = FillCombos(new string[] { "ISNULL(StatusID,0) AS StatusID,DueDate", "InvPurchaseOrderMaster", "PurchaseOrderID=" + Convert.ToInt64(dtOrderRow["ReferenceID"]) });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            iStatus = Convert.ToInt32(datTemp.Rows[0]["StatusID"].ToString());
                            if (iStatus == 0 || ((OperationStatusType)iStatus) == OperationStatusType.POrderClosed)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString()).AddDays(Convert.ToDouble(dtOrderRow["ValidPeriod"])))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else
                            {
                                blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                                if (!blnIsRepeat)
                                {
                                    this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                }
                                blnIsRepeat = true;
                            }
                        }
                        else
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                            dtAlerts.Rows.Remove(dtOrderRow);
                        }
                    }
                    else
                    {
                        this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        dtAlerts.Rows.Remove(dtOrderRow);
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private void CheckSalesQuotationStatus(DataTable dtAlerts)
        {
            DataRow[] dtRows;
            int iStatus;
            bool blnIsRepeat = true;

            try
            {
                dtRows = dtAlerts.Select("OperationTypeID = " + Convert.ToInt32(OperationType.SalesQuotation).ToString());
                if (dtRows == null || dtRows.Count() == 0)
                    return;

                foreach (DataRow dtOrderRow in dtRows)
                {
                    DataTable datTemp = FillCombos(new string[] { "ISNULL(StatusID,0) AS StatusID,DueDate", "InvSalesQuotationMaster", "SalesQuotationID=" + Convert.ToInt64(dtOrderRow["ReferenceID"]) });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            iStatus = Convert.ToInt32(datTemp.Rows[0]["StatusID"].ToString());
                            if (iStatus == 0 || ((OperationStatusType)iStatus) == OperationStatusType.SQuotationClosed)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString()).AddDays(Convert.ToDouble(dtOrderRow["ValidPeriod"])))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else
                            {
                                blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                                if (!blnIsRepeat)
                                {
                                    this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                }
                                blnIsRepeat = true;
                            }
                        }
                        else
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                            dtAlerts.Rows.Remove(dtOrderRow);
                        }
                    }
                    else
                    {
                        this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        dtAlerts.Rows.Remove(dtOrderRow);
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private void CheckSalesOrderStatus(DataTable dtAlerts)
        {
            DataRow[] dtRows;
            int iStatus;
            bool blnIsRepeat = true;

            try
            {
                dtRows = dtAlerts.Select("OperationTypeID = " + Convert.ToInt32(OperationType.SalesOrder).ToString());
                if (dtRows == null || dtRows.Count() == 0)
                    return;

                foreach (DataRow dtOrderRow in dtRows)
                {
                    long lngtemp = Convert.ToInt64(dtOrderRow["ReferenceID"]);
                    DataTable datTemp = FillCombos(new string[] { "ISNULL(StatusID,0) AS StatusID,DueDate", "InvSalesOrderMaster", "SalesOrderID=" + Convert.ToInt64(dtOrderRow["ReferenceID"]) });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            iStatus = Convert.ToInt32(datTemp.Rows[0]["StatusID"].ToString());
                            if (iStatus == 0 || ((OperationStatusType)iStatus) == OperationStatusType.SOrderClosed)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString()).AddDays(Convert.ToDouble(dtOrderRow["ValidPeriod"])))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (Convert.ToInt32(dtOrderRow["AlertSettingID"]) == (int)AlertSettingsTypes.SaleOrderApproved &&
                                (Convert.ToInt32(iStatus) == (int)OperationStatusType.SOrderRejected))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (Convert.ToInt32(dtOrderRow["AlertSettingID"]) == (int)AlertSettingsTypes.SaleOrderRejected &&
                                Convert.ToInt32(iStatus) == (int)OperationStatusType.SOrderApproved)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else
                            {
                                blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                                if (!blnIsRepeat)
                                {
                                    this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                }
                                blnIsRepeat = true;
                            }
                        }
                        else
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                            dtAlerts.Rows.Remove(dtOrderRow);
                        }
                    }
                    else
                    {
                        this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        dtAlerts.Rows.Remove(dtOrderRow);
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private void CheckSalesInvoiceStatus(DataTable dtAlerts)
        {
            DataRow[] dtRows;
            int iStatus;
            bool blnIsRepeat = true;

            try
            {
                dtRows = dtAlerts.Select("OperationTypeID = " + Convert.ToInt32(OperationType.SalesInvoice).ToString());
                if (dtRows == null || dtRows.Count() == 0)
                    return;

                foreach (DataRow dtOrderRow in dtRows)
                {
                    DataTable datTemp = FillCombos(new string[] { "ISNULL(StatusID,0) AS StatusID,DueDate", "InvSalesInvoiceMaster", "SalesInvoiceID=" + Convert.ToInt64(dtOrderRow["ReferenceID"]) });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            iStatus = Convert.ToInt32(datTemp.Rows[0]["StatusID"].ToString());
                            if (iStatus == 0 || ((OperationStatusType)iStatus) == OperationStatusType.SInvoiceClosed)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString()).AddDays(Convert.ToDouble(dtOrderRow["ValidPeriod"])))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else
                            {
                                blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                                if (!blnIsRepeat)
                                {
                                    this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                }
                                blnIsRepeat = true;
                            }
                        }
                        else
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                            dtAlerts.Rows.Remove(dtOrderRow);
                        }
                    }
                    else
                    {
                        this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        dtAlerts.Rows.Remove(dtOrderRow);
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }
        
        private void CheckAllAlerts(DataTable dtAlerts)
        {
            bool blnIsRepeat = true;

            try
            {
                foreach (DataRow dtOrderRow in dtAlerts.Rows)
                {
                    if (!this.queAlertID.Contains(Convert.ToInt64(dtOrderRow["AlertID"])))
                    {
                        blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                        if (!blnIsRepeat)
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        }
                        blnIsRepeat = true;
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private bool CloseAlerts(Queue<long> queAlertID, int iOperationID)
        {
            bool blnReturnVal = false;

            try
            {
                this.objClsConnection.BeginTransaction();

                switch ((OperationType)iOperationID)
                {
                    case OperationType.PurchaseOrder:
                        this.objclsDALAlerts.CloseAlert(queAlertID);
                        break;

                    default:
                        break;
                }
                this.objClsConnection.CommitTransaction();
                blnReturnVal = true;
            }
            catch (Exception Ex)
            {
                this.objClsConnection.RollbackTransaction();
                throw Ex;
            }
            finally
            {

            }
            return blnReturnVal;
        }

        private void GetReorderEnteredItemsAlert(long lngAlertSettingID)
        {
            DataTable datAlert = null;
            DataTable datAlertRoleSetting = null, datAlertUserSetting = null;
            DataRow[] dtUserRows = null;
            DataRow dtNewAlertRow;
            string strAlertMsg, strFilter;

            try
            {
                if (!this.objclsDALAlertSetting.GetAlertSetting(lngAlertSettingID))
                    return;// null;

                datAlertUserSetting = this.objclsDALAlerts.GetAlertUserSetting(lngAlertSettingID);
                if (datAlertUserSetting != null && datAlertUserSetting.Rows.Count != 0)
                    dtUserRows = datAlertUserSetting.Select("IsAlertON=true");

                if (dtUserRows == null || dtUserRows.Count() == 0)
                {
                    dtUserRows = null;
                    if (datAlertUserSetting != null)
                    {
                        datAlertUserSetting.Clear();
                        datAlertUserSetting.Dispose();
                        datAlertUserSetting = null;
                    }
                    return;// null;
                }
                else
                {
                    dtUserRows = null;
                    dtUserRows = datAlertUserSetting.Select("IsAlertON=true AND UserID = " + ClsCommonSettings.UserID.ToString());
                    if (dtUserRows.Count() == 0)
                        return;// null;
                }
                dtUserRows = null;

                datAlertRoleSetting = this.objclsDALAlerts.GetAlertRoleSetting(lngAlertSettingID);
                if (datAlertRoleSetting == null || datAlertRoleSetting.Rows.Count == 0)
                {
                    if (datAlertRoleSetting != null && datAlertRoleSetting.Rows.Count == 0)
                    {
                        datAlertRoleSetting.Clear();
                        datAlertRoleSetting.Dispose();
                        datAlertRoleSetting = null;
                    }
                    return;// null;
                }
                DataTable datItems = this.objclsDALAlerts.GetItems();// GetReorderLevelEnterItems();
                if (datItems != null && datItems.Rows.Count > 0)
                {
                    datAlert = new DataTable();
                    datAlert.Columns.Add("AlertID", typeof(long));
                    datAlert.Columns.Add("StartDate", typeof(string));
                    datAlert.Columns.Add("AlertUserSettingID", typeof(string));
                    datAlert.Columns.Add("ReferenceID", typeof(long));
                    datAlert.Columns.Add("AlertMessage", typeof(string));
                    datAlert.Columns.Add("Status", typeof(int));

                    foreach (DataRow dtItemRow in datItems.Rows)
                    {
                        foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                        {
                            strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                            strFilter += " AND IsAlertON = true";
                            dtUserRows = datAlertUserSetting.Select(strFilter);
                            if (dtUserRows.Count() == 0)
                            {
                                dtUserRows = null;
                                continue;
                            }

                            strAlertMsg = dtRoleRow["AlertMessage"].ToString();
                            strAlertMsg = strAlertMsg.Replace("<ITEMNAME>", dtItemRow["Description"].ToString());
                            strAlertMsg = strAlertMsg.Replace("<ITEMCODE>", dtItemRow["Code"].ToString());
                            strAlertMsg = strAlertMsg.Replace("<QTY>", dtItemRow["Qty"].ToString());
                            strAlertMsg = strAlertMsg.Replace("<WAREHOUSE>", dtItemRow["WarehouseName"].ToString());
                            foreach (DataRow dtUserRow in dtUserRows)
                            {
                                if (Convert.ToInt32(dtUserRow["UserID"]) != ClsCommonSettings.UserID)
                                    continue;
                                dtNewAlertRow = datAlert.NewRow();
                                dtNewAlertRow["AlertID"] = 0;
                                dtNewAlertRow["StartDate"] = ClsCommonSettings.GetServerDate();
                                dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                                dtNewAlertRow["ReferenceID"] = dtItemRow["ItemID"];
                                dtNewAlertRow["AlertMessage"] = strAlertMsg;
                                dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                                datAlert.Rows.Add(dtNewAlertRow);

                                dtNewAlertRow = null;
                            }
                            strAlertMsg = "";
                            strFilter = "";
                            dtUserRows = null;
                        }
                    }
                    datAlertRoleSetting.Clear();
                    datAlertRoleSetting.Dispose();
                    datAlertRoleSetting = null;

                    datAlertUserSetting.Clear();
                    datAlertUserSetting.Dispose();
                    datAlertUserSetting = null;

                    datAlert.AcceptChanges();

                    this.SaveReorderEnteredItemAlert(datAlert);

                    if (datAlert != null)
                    {
                        datAlert.Clear();
                        datAlert.Dispose();
                        datAlert = null;
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }

        private bool SaveReorderEnteredItemAlert(DataTable datItemAlert)
        {
            bool blnReturnVal = false;
            if (datItemAlert != null)
            {
                try
                {
                    this.objClsConnection.BeginTransaction();
                    this.objclsDALAlerts.DeleteAlert(Convert.ToInt32(OperationType.Stock),
                                        Convert.ToInt32(AlertSettingsTypes.ReorderLevelEntered), ClsCommonSettings.UserID);

                    if (datItemAlert != null && datItemAlert.Rows.Count > 0)
                        this.objclsDALAlerts.SaveAlert(datItemAlert);
                    this.objClsConnection.CommitTransaction();
                    blnReturnVal = true;
                }
                catch (Exception Ex)
                {
                    this.objClsConnection.RollbackTransaction();
                    throw Ex;
                }
                finally
                {

                }
            }
            return blnReturnVal;
        }

        private DataTable GetRoleInHierarchyView(DataTable datAlertRoleSetting)
        {
            DataRow dtOldRole = null, dtNewRole = null;
            DataTable datNewRole;
            DataRow[] dtRows;
            try
            {
                datNewRole = datAlertRoleSetting.Clone();
                datNewRole.Rows.Clear();

                while (datAlertRoleSetting.Rows.Count > 0)
                {
                    if (dtOldRole != null)
                    {
                        datAlertRoleSetting.Rows.Remove(dtOldRole);
                        datAlertRoleSetting.AcceptChanges();
                        dtOldRole = null;
                    }
                    dtRows = null;
                    dtNewRole = null;
                    datNewRole.AcceptChanges();

                    if (datAlertRoleSetting.Rows.Count == 0)
                        break;

                    dtOldRole = datAlertRoleSetting.Rows[0];
                    dtNewRole = datNewRole.NewRow();
                    foreach (DataColumn datCol in datNewRole.Columns)
                        dtNewRole[datCol.ColumnName] = dtOldRole[datCol.ColumnName];

                    if (datNewRole.Rows.Count == 0)
                    {
                        datNewRole.Rows.Add(dtNewRole);
                        continue;
                    }
                    else if (dtOldRole["ParentID"] == DBNull.Value || Convert.ToInt32(dtOldRole["ParentID"]) == 0)
                    {
                        datNewRole.Rows.InsertAt(dtNewRole, 0);
                        continue;
                    }
                    dtRows = datNewRole.Select("RoleID = " + dtOldRole["ParentID"].ToString());
                    if (dtRows.Count() > 0)
                    {
                        int iPos = datNewRole.Rows.IndexOf(dtRows[0]) + 1;
                        if (iPos == datNewRole.Rows.Count)
                        {
                            datNewRole.Rows.Add(dtNewRole);
                        }
                        else
                        {
                            datNewRole.Rows.InsertAt(dtNewRole, iPos);
                        }
                    }
                    else
                    {
                        dtRows = null;
                        dtRows = datNewRole.Select("ParentID = " + dtOldRole["RoleID"].ToString());
                        if (dtRows.Count() > 0)
                        {
                            int iPos = datNewRole.Rows.IndexOf(dtRows[0]);
                            datNewRole.Rows.InsertAt(dtNewRole, iPos);
                        }
                        else
                        {
                            datNewRole.Rows.Add(dtNewRole);
                        }
                    }
                }
                return datNewRole;
            }
            catch (Exception ex) { throw ex; }
        }

        private void GetUserAlert(long lngAlertSettingID)
        {
            DataTable datAlert = null;
            DataTable datAlertRoleSetting = null, datAlertUserSetting = null;

            DataRow[] dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strFilter;
            try
            {
                #region User & Role Selection

                if (!this.objclsDALAlertSetting.GetAlertSetting(lngAlertSettingID))
                    return;// null;

                datAlertUserSetting = this.objclsDALAlerts.GetAlertUserSetting(lngAlertSettingID);
                if (datAlertUserSetting != null && datAlertUserSetting.Rows.Count != 0)
                    dtUserRows = datAlertUserSetting.Select("IsAlertON=true");

                if (dtUserRows == null || dtUserRows.Count() == 0)
                {
                    dtUserRows = null;
                    if (datAlertUserSetting != null)
                    {
                        datAlertUserSetting.Clear();
                        datAlertUserSetting.Dispose();
                        datAlertUserSetting = null;
                    }
                    return;// null;
                }
                else
                {
                    dtUserRows = null;
                    dtUserRows = datAlertUserSetting.Select("IsAlertON=true AND UserID = " + ClsCommonSettings.UserID.ToString());
                    if (dtUserRows.Count() == 0)
                        return;// null;
                }
                dtUserRows = null;

                datAlertRoleSetting = this.objclsDALAlerts.GetAlertRoleSetting(lngAlertSettingID);
                if (datAlertRoleSetting == null || datAlertRoleSetting.Rows.Count == 0)
                {
                    if (datAlertRoleSetting != null && datAlertRoleSetting.Rows.Count == 0)
                    {
                        datAlertRoleSetting.Clear();
                        datAlertRoleSetting.Dispose();
                        datAlertRoleSetting = null;
                    }
                    return;// null;
                }

                 #endregion

                #region Alert Table & Conditions

                string strTName = "", Condition = "", IDField = "";

                if (lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationSubmitted)
                {
                    strTName = "InvSalesQuotationMaster";
                    Condition = " StatusID = " + (int)OperationStatusType.SQuotationSubmittedForApproval;
                    IDField = "InvSalesQuotationMaster.SalesQuotationID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationApproved)
                {
                    strTName = "InvSalesQuotationMaster";
                    Condition = " StatusID In (" + (int)OperationStatusType.SQuotationApproved + "," + (int)OperationStatusType.SQuotationSubmitted + ")";
                    IDField = "InvSalesQuotationMaster.SalesQuotationID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationCancelled)
                {
                    strTName = "InvSalesQuotationMaster";
                    Condition = " StatusID = " + (int)OperationStatusType.SQuotationCancelled;
                    IDField = "InvSalesQuotationMaster.SalesQuotationID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationDueDate)
                {
                    strTName = "InvSalesQuotationMaster";
                    Condition = " (StatusID = " + (int)OperationStatusType.SQuotationOpen + " OR StatusID = " + (int)OperationStatusType.SQuotationApproved + " OR " +
                        "StatusID = " + (int)OperationStatusType.SQuotationSubmittedForApproval + ")";
                    IDField = "InvSalesQuotationMaster.SalesQuotationID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderCreated)
                {
                    strTName = "InvSalesOrderMaster";
                    Condition = " StatusID = " + (int)OperationStatusType.SOrderOpen;
                    IDField = "InvSalesOrderMaster.SalesOrderID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesOrderAdvancePaid)
                {
                    strTName = "AccReceiptAndPaymentMaster";
                    Condition = " OperationTypeID = " + (int)OperationType.SalesOrder + " AND ReceiptAndPaymentTypeID=" + (int)PaymentTypes.AdvancePayment;
                    IDField = "AccReceiptAndPaymentMaster.ReceiptAndPaymentID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderSubmitted)
                {
                    strTName = "InvSalesOrderMaster";
                    Condition = " StatusID = " + (int)OperationStatusType.SOrderSubmittedForApproval;
                    IDField = "InvSalesOrderMaster.SalesOrderID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderApproved)
                {
                    strTName = "InvSalesOrderMaster";
                    Condition = " StatusID In ( " + (int)OperationStatusType.SOrderApproved + "," + (int)OperationStatusType.SOrderSubmitted + ")"; 
                    IDField = "InvSalesOrderMaster.SalesOrderID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderRejected)
                {
                    strTName = "InvSalesOrderMaster";
                    Condition = " StatusID = " + (int)OperationStatusType.SOrderRejected;
                    IDField = "InvSalesOrderMaster.SalesOrderID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesOrderDueDate)
                {
                    strTName = "InvSalesOrderMaster";
                    Condition = " (StatusID = " + (int)OperationStatusType.SOrderOpen + " OR StatusID = " + (int)OperationStatusType.SOrderSubmittedForApproval + " OR " +
                        "StatusID = " + (int)OperationStatusType.SOrderApproved + ")"; 
                    IDField = "InvSalesOrderMaster.SalesOrderID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesInvoiceDueDate)
                {
                    strTName = "InvSalesInvoiceMaster";
                    Condition = " (StatusID = " + (int)OperationStatusType.SInvoiceOpen + ")";// AND DueDate<='" + CurDate + "'";
                    IDField = "InvSalesInvoiceMaster.SalesInvoiceID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.MaterialIssue)
                {
                    strTName = "InvMaterialIssueMaster";
                    IDField = "InvMaterialIssueMaster.MaterialIssueID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.Receipt)
                {
                    strTName = "AccReceiptAndPaymentMaster";
                    //Condition = "OperationTypeID=" + (int)OperationType.Receipt;
                    Condition = "IsReceipt = 1 And OperationTypeID = "+(int)OperationType.SalesInvoice+"";
                    IDField = "AccReceiptAndPaymentMaster.ReceiptAndPaymentID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.DeliveryNote)
                {
                    strTName = "InvItemIssueMaster";
                    Condition = " OrderTypeID = " + (int)OperationOrderType.DNOTSalesInvoice;
                    IDField = "InvItemIssueMaster.ItemIssueID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.PurchaseOrderApproved)
                {
                    strTName = "InvPurchaseOrderMaster";
                    Condition = " StatusID In ( " + (int)OperationStatusType.POrderApproved + "," + (int)OperationStatusType.POrderSubmitted + ")";
                    IDField = "InvPurchaseOrderMaster.PurchaseOrderID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.RFQDueDate)
                {
                    strTName = "InvRFQMaster";
                    Condition = " (StatusID = " + (int)RFQStatus.Opened + " OR StatusID = " + (int)RFQStatus.SubmittedForApproval + " OR " +
                        "StatusID = " + (int)RFQStatus.Approved + ")";
                    IDField = "InvRFQMaster.RFQID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.PurchaseQuotationDueDate)
                {
                    strTName = "InvPurchaseQuotationMaster";
                    Condition = " (StatusID = " + (int)OperationStatusType.PQuotationOpened + " OR StatusID = " + (int)OperationStatusType.PQuotationApproved + " OR " +
                        "StatusID = " + (int)OperationStatusType.PQuotationSubmittedForApproval + ")";
                    IDField = "InvPurchaseQuotationMaster.PurchaseQuotationID";
                }
                else if (lngAlertSettingID == (int)AlertSettingsTypes.PurchaseOrderDueDate)
                {
                    strTName = "InvPurchaseOrderMaster";
                    Condition = " (StatusID = " + (int)OperationStatusType.POrderOpened + " OR StatusID = " + (int)OperationStatusType.POrderApproved + " OR " +
                        "StatusID = " + (int)OperationStatusType.POrderSubmittedForApproval + ")";
                    IDField = "InvPurchaseOrderMaster.PurchaseOrderID";
                }

                if (Condition.Length > 0)
                    Condition = Condition + " AND " + IDField + " NOT IN (SELECT ReferenceID FROM Alerts A INNER JOIN AlertUserSettings U" +
                                            " ON A.AlertUserSettingID=U.AlertUserSettingID WHERE U.AlertSettingID=" + lngAlertSettingID + " AND U.UserID=" + ClsCommonSettings.UserID.ToString() + ")";
                else if (Condition.Length == 0)
                    Condition = IDField + " NOT IN (SELECT ReferenceID FROM Alerts A INNER JOIN AlertUserSettings U" +
                                            " ON A.AlertUserSettingID=U.AlertUserSettingID WHERE U.AlertSettingID=" + lngAlertSettingID + " AND U.UserID=" + ClsCommonSettings.UserID.ToString() + ")";
                #endregion

                #region Alert Fetching

                DataTable datItems=new DataTable();
                if (strTName != "")
                    datItems = this.objclsDALAlerts.GetUserAlertEntries(strTName, Condition);

                #endregion

                if (datItems != null && datItems.Rows.Count > 0)
                {
                    datAlert = new DataTable();
                    datAlert.Columns.Add("AlertID", typeof(long));
                    datAlert.Columns.Add("StartDate", typeof(string));
                    datAlert.Columns.Add("AlertUserSettingID", typeof(string));
                    datAlert.Columns.Add("ReferenceID", typeof(long));
                    datAlert.Columns.Add("AlertMessage", typeof(string));
                    datAlert.Columns.Add("Status", typeof(int));

                    foreach (DataRow dtItemRow in datItems.Rows)
                    {
                        foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                        {
                            #region Dynamic Alert Message Creation

                            strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                            strFilter += " AND IsAlertON = true";
                            dtUserRows = datAlertUserSetting.Select(strFilter);

                            if (dtUserRows.Count() == 0)
                            {
                                dtUserRows = null;
                                continue;
                            }

                            strAlertMsg = dtRoleRow["AlertMessage"].ToString();

                            if (lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationSubmitted ||
                                lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationApproved ||
                                lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationCancelled)
                            {
                                int intStatusID = 0;
                                if (lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationSubmitted)
                                    intStatusID = (int)OperationStatusType.SQuotationSubmitted;
                                else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationApproved)
                                    intStatusID = (int)OperationStatusType.SQuotationApproved;
                                else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationCancelled)
                                    intStatusID = (int)OperationStatusType.SQuotationCancelled;

                                DataTable datTemp = FillCombos(new string[] { "SQ.SalesQuotationNo,V.VendorName + '-' + V.VendorCode AS VendorName,SQ.NetAmount,ISNULL(EM.EmployeeFullName,UM.UserName) as UserName ", "" + 
                                    "InvSalesQuotationMaster SQ LEFT JOIN InvVendorInformation V ON SQ.VendorID=V.VendorID " +
                                    " Left Join InvVerificationHistory VH On VH.ReferenceID = SQ.SalesQuotationID AND VH.OperationTypeID = " + (int)OperationType.SalesQuotation + " AND " + 
                                    " VH.StatusID = " + intStatusID + " Left Join UserMaster UM ON UM.UserID = SQ.CreatedBy Left Join EmployeeMaster EM ON EM.EmployeeID = UM.EmployeeID ", "" +
                                    "SQ.SalesQuotationID=" + dtItemRow["SalesQuotationID"].ToInt32() });

                                if (datTemp != null)
                                {
                                    if (datTemp.Rows.Count > 0)
                                    {
                                        strAlertMsg = strAlertMsg.Replace("<SALESQUOTATIONNO>", datTemp.Rows[0]["SalesQuotationNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                        //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<DONEBY>", datTemp.Rows[0]["UserName"].ToString());
                                    }
                                }
                            }
                            else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationDueDate)
                            {
                                DataTable datTemp = FillCombos(new string[] { "SQ.SalesQuotationNo,V.VendorName + '-' + V.VendorCode AS VendorName ",   
                                    " InvSalesQuotationMaster SQ INNER JOIN InvVendorInformation V ON SQ.VendorID = V.VendorID " ,
                                    " SQ.SalesQuotationID=" + dtItemRow["SalesQuotationID"].ToInt32() });

                                if (datTemp != null)
                                {
                                    if (datTemp.Rows.Count > 0)
                                    {
                                        strAlertMsg = strAlertMsg.Replace("<SALESQUOTATIONNO>", dtItemRow["SalesQuotationNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", dtItemRow["NetAmount"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(dtItemRow["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                                    }
                                }
                            }
                            else if (lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderCreated ||
                                lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderSubmitted ||
                                lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderApproved ||
                                lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderRejected)
                            {
                                int intStatusID = 0;
                                if (lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderCreated)
                                    intStatusID = (int)OperationStatusType.SOrderOpen;
                                else if (lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderSubmitted)
                                    intStatusID = (int)OperationStatusType.SOrderSubmitted;
                                else if (lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderApproved)
                                    intStatusID = (int)OperationStatusType.SOrderApproved;
                                else if (lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderRejected)
                                    intStatusID = (int)OperationStatusType.SOrderRejected;

                                DataTable datTemp = FillCombos(new string[] { "SO.SalesOrderNo,V.VendorName + '-' + V.VendorCode AS VendorName,SO.NetAmount,ISNULL(EM.EmployeeFullName,UM.UserName) as UserName ", "" + 
                                    " InvSalesOrderMaster SO LEFT JOIN InvSalesOrderReferenceDetails SOR ON SOR.SalesOrderID=SO.SalesOrderID LEFT JOIN InvSalesQuotationMaster SQ ON SOR.ReferenceID = SQ.SalesQuotationID AND SO.OrderTypeID = " +(int)OperationOrderType.SOTFromQuotation + " LEFT JOIN InvVendorInformation V ON SO.VendorID=V.VendorID " +
                                    " Left Join InvVerificationHistory VH On VH.ReferenceID = SO.SalesOrderID AND VH.OperationTypeID = " + (int)OperationType.SalesOrder + " AND " + 
                                    " VH.StatusID = " + intStatusID + " Left Join UserMaster UM ON UM.UserID = SO.CreatedBy Left Join EmployeeMaster EM ON EM.EmployeeID = UM.EmployeeID ", "" +
                                    " SO.SalesOrderID=" + dtItemRow["SalesOrderID"].ToInt32() });

                                if (datTemp != null)
                                {
                                    if (datTemp.Rows.Count > 0)
                                    {
                                        strAlertMsg = strAlertMsg.Replace("<SALEORDERNO>", dtItemRow["SalesOrderNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                        //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<DONEBY>", datTemp.Rows[0]["UserName"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(dtItemRow["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                                    }
                                }
                            }
                            else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesOrderAdvancePaid)
                            {
                                DataTable datTemp = FillCombos(new string[] { "SO.SalesOrderNo,V.VendorName + '-' + V.VendorCode AS VendorName,SO.NetAmount,SUM(D.Amount) AS PaidAmount", "" + 
                                    "AccReceiptAndPaymentMaster M INNER JOIN AccReceiptAndPaymentDetails D ON M.ReceiptAndPaymentID=D.ReceiptAndPaymentID " +
                                    "INNER JOIN InvSalesOrderMaster SO ON D.ReferenceID = SO.SalesOrderID INNER JOIN InvVendorInformation V ON SO.VendorID=V.VendorID ", "" +
                                    "M.ReceiptAndPaymentID=" + dtItemRow["ReceiptAndPaymentID"].ToInt64() + " GROUP BY SO.SalesOrderNo,V.VendorName,V.VendorCode,SO.NetAmount" });
                                if (datTemp != null)
                                {
                                    if (datTemp.Rows.Count > 0)
                                    {
                                        strAlertMsg = strAlertMsg.Replace("<SALESORDERNO>", datTemp.Rows[0]["SalesOrderNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                        //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<ADVAMOUNT>", datTemp.Rows[0]["PaidAmount"].ToString());
                                    }
                                }
                            }
                            else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesOrderDueDate)
                            {
                                DataTable datTemp = FillCombos(new string[] { "SO.SalesOrderNo,V.VendorName + '-' + V.VendorCode AS VendorName ",   
                                    " InvSalesOrderMaster SO INNER JOIN InvVendorInformation V ON SO.VendorID = V.VendorID " ,
                                    " SO.SalesOrderID=" + dtItemRow["SalesOrderID"].ToInt32() });

                                if (datTemp != null)
                                {
                                    if (datTemp.Rows.Count > 0)
                                    {
                                        strAlertMsg = strAlertMsg.Replace("<SALESORDERNO>", dtItemRow["SalesOrderNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", dtItemRow["NetAmount"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(dtItemRow["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                                    }
                                }
                            }
                            else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesInvoiceDueDate)
                            {
                                DataTable datTemp = FillCombos(new string[] { "SI.SalesInvoiceNo,V.VendorName + '-' + V.VendorCode AS VendorName ",   
                                    " InvSalesInvoiceMaster SI INNER JOIN InvVendorInformation V ON SI.VendorID = V.VendorID " ,
                                    " SI.SalesInvoiceID=" + dtItemRow["SalesInvoiceID"].ToInt32() });

                                if (datTemp != null)
                                {
                                    if (datTemp.Rows.Count > 0)
                                    {
                                        strAlertMsg = strAlertMsg.Replace("<SALESINVNO>", dtItemRow["SalesInvoiceNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", dtItemRow["NetAmount"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(dtItemRow["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                                    }
                                }
                            }
                            else if (lngAlertSettingID == (int)AlertSettingsTypes.MaterialIssue)
                            {
                                DataTable datTemp = FillCombos(new string[] { "MI.MaterialIssueNo,Convert(varchar(15),MI.MaterialIssueDate,106) as MaterialIssueDate,UM.UserName,WM.WarehouseName ", "" + 
                                    "InvMaterialIssueMaster MI INNER JOIN UserMaster UM On UM.UserID = MI.CreatedBy " +
                                    "INNER JOIN InvWarehouse WM ON WM.WarehouseID = MI.WareHouseID",""+
                                    "MI.MaterialIssueID=" + dtItemRow["MaterialIssueID"].ToInt64() });
                                if (datTemp != null)
                                {
                                    if (datTemp.Rows.Count > 0)
                                    {
                                        strAlertMsg = strAlertMsg.Replace("<MINO>", datTemp.Rows[0]["MaterialIssueNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<ISSUEDTE>", datTemp.Rows[0]["MaterialIssueDate"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<ISSUEDBY>", datTemp.Rows[0]["UserName"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<WAREHOUSE>", datTemp.Rows[0]["WarehouseName"].ToString());
                                    }
                                }
                            }
                            else if (lngAlertSettingID == (int)AlertSettingsTypes.Receipt)
                            {
                                //DataTable datTemp = FillCombos(new string[] { "JO.JobOrderNo,V.VendorName,A.PaymentDate,SO.SalesOrderNo,A.TotalAmount,A.PaymentNumber,SO.NetAmount-SUM(A.TotalAmount) AS BalanceAmt ", "" + 
                                //    "AccReceiptAndPaymentMaster A INNER JOIN AccReceiptAndPaymentDetails AD ON A.ReceiptAndPaymentID=AD.ReceiptAndPaymentID " +
                                //    "INNER JOIN InvSalesInvoiceMaster SI ON AD.ReferenceID = SI.SalesInvoiceID INNER JOIN InvSalesOrderMaster SO ON SI.SalesOrderID = SO.SalesOrderID " +
                                //    "LEFT JOIN InvSalesQuotationMaster SQ ON SQ.SalesQuotationID=SO.ReferenceID " +
                                //    "LEFT JOIN PrdJobOrder JO ON JO.JobOrderID = SQ.JobOrderID "+
                                //    "INNER JOIN InvVendorInformation V ON A.VendorID=V.VendorID ", "" +
                                //    "A.ReceiptAndPaymentID=" + dtItemRow["ReceiptAndPaymentID"].ToInt64() });
                                DataTable datTemp = FillCombos(new string[] {"V.VendorName,A.PaymentDate,SO.SalesOrderNo,A.TotalAmount,A.PaymentNumber,SI.SalesInvoiceNo,"+
                                                                            "SO.NetAmount-(Select IsNull(Sum(D.Amount),0) From AccReceiptAndPaymentDetails D "+
				                                                            "Inner Join AccReceiptAndPaymentMaster M On M.ReceiptAndPaymentID = D.ReceiptAndPaymentID "+
				                                                            "Where (M.OperationTypeID = "+(int)OperationType.SalesOrder+" And M.ReceiptAndPaymentTypeID = "+(int)PaymentTypes.AdvancePayment+" And D.ReferenceID = SO.SalesOrderID) Or "+
					                                                        "(M.OperationTypeID = "+(int)OperationType.SalesInvoice+" And M.ReceiptAndPaymentTypeID = "+(int)PaymentTypes.InvoicePayment+" And D.ReferenceID = SI.SalesInvoiceID) ) AS BalanceAmt ",
                                                                            "AccReceiptAndPaymentMaster A "+
                                                                            "left JOIN AccReceiptAndPaymentDetails AD ON A.ReceiptAndPaymentID=AD.ReceiptAndPaymentID  "+
                                                                            "left JOIN InvSalesInvoiceMaster SI ON AD.ReferenceID = SI.SalesInvoiceID  "+
                                                                            "LEFT JOIN InvSalesInvoiceReferenceDetails SIR ON SIR.SalesInvoiceID=SI.SalesInvoiceID "+
                                                                            "left JOIN InvSalesOrderMaster SO ON SIR.ReferenceID = SO.SalesOrderID "+
                                                                            "LEFT JOIN InvSalesOrderReferenceDetails SOR ON SOR.SalesOrderID=SO.SalesOrderID "+
                                                                            "LEFT JOIN InvSalesQuotationMaster SQ ON SQ.SalesQuotationID=SOR.ReferenceID  "+
                                                                            "left JOIN InvVendorInformation V ON A.VendorID=V.VendorID ",
                                                                            "A.ReceiptAndPaymentID=" + dtItemRow["ReceiptAndPaymentID"].ToInt64() });
                                if (datTemp != null)
                                {
                                    if (datTemp.Rows.Count > 0)
                                    {
                                        //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<RCPTDTE>", (Convert.ToDateTime(datTemp.Rows[0]["PaymentDate"].ToString())).ToString("dd-MMM-yyyy")); ;
                                        strAlertMsg = strAlertMsg.Replace("<SONO>", datTemp.Rows[0]["SalesOrderNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<AMOUNT>", datTemp.Rows[0]["TotalAmount"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<RNO>", datTemp.Rows[0]["PaymentNumber"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<BALAMOUNT>", datTemp.Rows[0]["BalanceAmt"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<SINO>", datTemp.Rows[0]["SalesInvoiceNo"].ToString());
                                    }
                                }
                            }
                            else if (lngAlertSettingID == (int)AlertSettingsTypes.DeliveryNote)
                            {
                                DataTable datTemp = FillCombos(new string[] { "V.VendorName,IM.IssuedDate,SI.SalesInvoiceNo,UM.UserName AS IssuedBy,IM.ItemIssueNo ", "" + 
                                    " InvItemIssueMaster IM left join InvItemIssueReferenceDetails IIRD ON IIRD.ItemIssueID=IM.ItemIssueID INNER JOIN UserMaster UM ON UM.UserID = IM.IssuedBy LEFT JOIN EmployeeMaster E ON IM.OrderTypeID=" + (int)OperationOrderType.DNOTSalesInvoice + " AND UM.UserID = E.EmployeeID " +
                                    " INNER JOIN InvSalesInvoiceMaster SI ON IIRD.ReferenceID = SI.SalesInvoiceID LEFT JOIN InvSalesInvoiceReferenceDetails SIR ON SIR.SalesInvoiceID=SI.SalesInvoiceID LEFT JOIN InvSalesOrderMaster SO ON SIR.ReferenceID = SO.SalesOrderID " +
                                    " LEFT JOIN InvSalesOrderReferenceDetails SOR ON SOR.SalesOrderID=SO.SalesOrderID LEFT JOIN InvSalesQuotationMaster SQ ON SOR.ReferenceID = SQ.SalesQuotationID " +
                                    " LEFT JOIN InvVendorInformation V ON SI.VendorID=V.VendorID ", "" +
                                    " IM.ItemIssueID=" + dtItemRow["ItemIssueID"].ToInt64() });
                                if (datTemp != null)
                                {
                                    if (datTemp.Rows.Count > 0)
                                    {
                                        //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<ISSUEDTE>", (Convert.ToDateTime(datTemp.Rows[0]["IssuedDate"].ToString())).ToString("dd-MMM-yyyy")); ;
                                        strAlertMsg = strAlertMsg.Replace("<SINO>", datTemp.Rows[0]["SalesInvoiceNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<ISSUEDBY>", datTemp.Rows[0]["IssuedBy"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<ISSUENO>", datTemp.Rows[0]["ItemIssueNo"].ToString());
                                    }
                                }
                            }
                            else if (lngAlertSettingID == (int)AlertSettingsTypes.PurchaseOrderApproved)
                            {
                                int intStatusID = 0;
                                if (lngAlertSettingID == (int)AlertSettingsTypes.PurchaseOrderApproved)
                                    intStatusID = (int)OperationStatusType.POrderApproved;
                                else if (lngAlertSettingID == (int)AlertSettingsTypes.PurchaseOrderApproved)
                                    intStatusID = (int)OperationStatusType.POrderSubmitted;

                                DataTable datTemp = FillCombos(new string[] { "PO.PurchaseOrderNo,V.VendorName + '-' + V.VendorCode AS VendorName,PO.NetAmount,ISNULL(EM.EmployeeFullName,UM.UserName) as UserName ", "" + 
                                    " InvPurchaseOrderMaster PO INNER JOIN InvVendorInformation V ON PO.VendorID=V.VendorID " +
                                    " Left Join InvVerificationHistory VH On VH.ReferenceID = PO.PurchaseOrderID AND VH.OperationTypeID = " + (int)OperationType.PurchaseOrder + " AND " + 
                                    " VH.StatusID = " + intStatusID + " Left Join UserMaster UM ON UM.UserID = PO.CreatedBy Left Join EmployeeMaster EM ON EM.EmployeeID = UM.EmployeeID ", "" +
                                    " PO.PurchaseOrderID=" + dtItemRow["PurchaseOrderID"].ToInt32() });

                                if (datTemp != null)
                                {
                                    if (datTemp.Rows.Count > 0)
                                    {
                                        strAlertMsg = strAlertMsg.Replace("<PURCHASEORDERNO>", dtItemRow["PurchaseOrderNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<SUPNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<DONEBY>", datTemp.Rows[0]["UserName"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(dtItemRow["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                                    }
                                }
                            }
                            else if (lngAlertSettingID == (int)AlertSettingsTypes.RFQDueDate)
                            {
                                strAlertMsg = strAlertMsg.Replace("<RFQNO>", dtItemRow["RFQNo"].ToString());
                                strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(dtItemRow["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                            }
                            else if (lngAlertSettingID == (int)AlertSettingsTypes.PurchaseQuotationDueDate)
                            {
                                DataTable datTemp = FillCombos(new string[] { "PQ.PurchaseQuotationNo,V.VendorName + '-' + V.VendorCode AS VendorName ",   
                                    " InvPurchaseQuotationMaster PQ INNER JOIN InvVendorInformation V ON PQ.VendorID = V.VendorID " ,
                                    " PQ.PurchaseQuotationID=" + dtItemRow["PurchaseQuotationID"].ToInt32() });

                                if (datTemp != null)
                                {
                                    if (datTemp.Rows.Count > 0)
                                    {
                                        strAlertMsg = strAlertMsg.Replace("<PURCHASEQUOTATIONNO>", dtItemRow["PurchaseQuotationNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<SUPNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", dtItemRow["NetAmount"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(dtItemRow["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                                    }
                                }
                            }
                            else if (lngAlertSettingID == (int)AlertSettingsTypes.PurchaseOrderDueDate)
                            {
                                DataTable datTemp = FillCombos(new string[] { "PO.PurchaseOrderNo,V.VendorName + '-' + V.VendorCode AS VendorName ",   
                                    " InvPurchaseOrderMaster PO INNER JOIN InvVendorInformation V ON PO.VendorID = V.VendorID " ,
                                    " PO.PurchaseOrderID=" + dtItemRow["PurchaseOrderID"].ToInt32() });

                                if (datTemp != null)
                                {
                                    if (datTemp.Rows.Count > 0)
                                    {
                                        strAlertMsg = strAlertMsg.Replace("<PURCHASEORDERNO>", dtItemRow["PurchaseOrderNo"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<SUPNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", dtItemRow["NetAmount"].ToString());
                                        strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(dtItemRow["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                                    }
                                }
                            }

                            #endregion

                            #region ReferenceID Setting

                            foreach (DataRow dtUserRow in dtUserRows)
                            {
                                if (Convert.ToInt32(dtUserRow["UserID"]) != ClsCommonSettings.UserID)
                                    continue;

                                if (lngAlertSettingID == (int)AlertSettingsTypes.PurchaseQuotationDueDate || lngAlertSettingID == (int)AlertSettingsTypes.PurchaseOrderDueDate ||
                                    lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationDueDate || lngAlertSettingID == (int)AlertSettingsTypes.SalesOrderDueDate ||
                                    lngAlertSettingID == (int)AlertSettingsTypes.SalesInvoiceDueDate)
                                {
                                    DataTable datTemp = new DataTable();
                                    if (lngAlertSettingID == (int)AlertSettingsTypes.PurchaseQuotationDueDate)
                                    {
                                       datTemp = FillCombos(new string[] {"1","InvPurchaseQuotationMaster","PurchaseQuotationID = "+dtItemRow["PurchaseQuotationID"].ToInt64()+
                                                                          " AND DATEADD(DAY,"+(dtUserRow["ProcessingInterval"].ToInt32() * -1)+",DueDate) <= '"+ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")+"'"});
                                       if (datTemp.Rows.Count == 0)
                                           continue;

                                    }
                                    else if (lngAlertSettingID == (int)AlertSettingsTypes.PurchaseOrderDueDate)
                                    {
                                        datTemp = FillCombos(new string[] {"1","InvPurchaseOrderMaster","PurchaseOrderID = "+dtItemRow["PurchaseOrderID"].ToInt64()+
                                                                          " AND DATEADD(DAY,"+(dtUserRow["ProcessingInterval"].ToInt32() * -1)+",DueDate) <= '"+ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")+"'"});
                                        if (datTemp.Rows.Count == 0)
                                            continue;
                                    }
                                    else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationDueDate)
                                    {
                                        datTemp = FillCombos(new string[] {"1","InvSalesQuotationMaster","SalesQuotationID = "+dtItemRow["SalesQuotationID"].ToInt64()+
                                                                          " AND DATEADD(DAY,"+(dtUserRow["ProcessingInterval"].ToInt32() * -1)+",DueDate) <= '"+ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")+"'"});
                                        if (datTemp.Rows.Count == 0)
                                            continue;
                                    }
                                    else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesOrderDueDate)
                                    {
                                        datTemp = FillCombos(new string[] {"1","InvSalesOrderMaster","SalesOrderID = "+dtItemRow["SalesOrderID"].ToInt64()+
                                                                          " AND DATEADD(DAY,"+(dtUserRow["ProcessingInterval"].ToInt32() * -1)+",DueDate) <= '"+ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")+"'"});
                                        if (datTemp.Rows.Count == 0)
                                            continue;
                                    }
                                    else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesInvoiceDueDate)
                                    {
                                        datTemp = FillCombos(new string[] {"1","InvSalesInvoiceMaster","SalesInvoiceID = "+dtItemRow["SalesInvoiceID"].ToInt64()+
                                                                          " AND DATEADD(DAY,"+(dtUserRow["ProcessingInterval"].ToInt32() * -1)+",DueDate) <= '"+ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")+"'"});
                                        if (datTemp.Rows.Count == 0)
                                            continue;
                                    }
                                }
                              
                                    dtNewAlertRow = datAlert.NewRow();
                                    dtNewAlertRow["AlertID"] = 0;
                                    dtNewAlertRow["StartDate"] = ClsCommonSettings.GetServerDate();
                                    dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];

                                    if (lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationDueDate
                                        || lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationSubmitted
                                        || lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationCancelled
                                        || lngAlertSettingID == (int)AlertSettingsTypes.SalesQuotationApproved)
                                    {
                                        dtNewAlertRow["ReferenceID"] = dtItemRow["SalesQuotationID"];
                                    }
                                    else if (lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderCreated
                                        || lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderSubmitted
                                        || lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderApproved
                                        || lngAlertSettingID == (int)AlertSettingsTypes.SaleOrderRejected
                                        || lngAlertSettingID == (int)AlertSettingsTypes.SalesOrderDueDate)
                                    {
                                        dtNewAlertRow["ReferenceID"] = dtItemRow["SalesOrderID"];
                                    }
                                    else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesOrderAdvancePaid)
                                    {
                                        dtNewAlertRow["ReferenceID"] = dtItemRow["ReceiptAndPaymentID"];
                                    }
                                    else if (lngAlertSettingID == (int)AlertSettingsTypes.SalesInvoiceDueDate)
                                    {
                                        dtNewAlertRow["ReferenceID"] = dtItemRow["SalesInvoiceID"];
                                    }
                                    else if (lngAlertSettingID == (int)AlertSettingsTypes.Receipt)
                                    {
                                        dtNewAlertRow["ReferenceID"] = dtItemRow["ReceiptAndPaymentID"];
                                    }
                                    else if (lngAlertSettingID == (int)AlertSettingsTypes.DeliveryNote)
                                    {
                                        dtNewAlertRow["ReferenceID"] = dtItemRow["ItemIssueID"];
                                    }
                                    else if (lngAlertSettingID == (int)AlertSettingsTypes.MaterialIssue)
                                    {
                                        dtNewAlertRow["ReferenceID"] = dtItemRow["MaterialIssueID"];
                                    }
                                    else if (lngAlertSettingID == (int)AlertSettingsTypes.RFQDueDate)
                                    {
                                        dtNewAlertRow["ReferenceID"] = dtItemRow["RFQID"];
                                    }
                                    else if (lngAlertSettingID == (int)AlertSettingsTypes.PurchaseQuotationDueDate)
                                    {
                                        dtNewAlertRow["ReferenceID"] = dtItemRow["PurchaseQuotationID"];
                                    }
                                    else if (lngAlertSettingID == (int)AlertSettingsTypes.PurchaseOrderApproved
                                        || lngAlertSettingID == (int)AlertSettingsTypes.PurchaseOrderDueDate)
                                    {
                                        dtNewAlertRow["ReferenceID"] = dtItemRow["PurchaseOrderID"];
                                    }

                                    dtNewAlertRow["AlertMessage"] = strAlertMsg;
                                    dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                                    datAlert.Rows.Add(dtNewAlertRow);
                                
                                dtNewAlertRow = null;
                            }  

                            #endregion

                            strAlertMsg = "";
                            strFilter = "";
                            dtUserRows = null;
                        }
                    }
                    datAlertRoleSetting.Clear();
                    datAlertRoleSetting.Dispose();
                    datAlertRoleSetting = null;

                    datAlertUserSetting.Clear();
                    datAlertUserSetting.Dispose();
                    datAlertUserSetting = null;

                    datAlert.AcceptChanges();
                }

                #region Unwanted Alert Deletion

                if (datAlert != null)
                {
                    string strTName1 = "Alerts INNER JOIN AlertUserSettings ON Alerts.AlertUserSettingID = AlertUserSettings.AlertUserSettingID  " +
                        "AND (Alerts.StatusID= " + (int)AlertStatus.Open + " OR Alerts.StatusID= " + (int)AlertStatus.Read + " OR Alerts.StatusID= " + (int)AlertStatus.Blocked + ")";
                    //string strTName1 = "Alerts INNER JOIN AlertUserSettings ON Alerts.AlertUserSettingID = AlertUserSettings.AlertUserSettingID  ";

                    string Condition1 = "AlertUserSettings.UserID=" + ClsCommonSettings.UserID.ToString() + " AND AlertUserSettings.AlertSettingID=" + lngAlertSettingID;
                    DataTable datTemp = this.objclsDALAlerts.GetUserAlertEntries(strTName1, Condition1);
                    if (datTemp != null)
                    {
                        //if (datTemp.Rows.Count > datAlert.Rows.Count)
                        //{
                        //    for (int k = datTemp.Rows.Count - 1; k >= 0; k--)
                        //    {
                        //        for (int l = datAlert.Rows.Count - 1; l >= 0; l--)
                        //        {
                        //            try
                        //            {
                        //                if ((datTemp.Rows[k]["AlertUserSettingID"].ToString() != datAlert.Rows[l]["AlertUserSettingID"].ToString())
                        //                       || (datTemp.Rows[k]["ReferenceID"].ToString() != datAlert.Rows[l]["ReferenceID"].ToString())
                        //                       || (datTemp.Rows[k]["AlertMessage"].ToString() != datAlert.Rows[l]["AlertMessage"].ToString()))
                        //                {
                        //                    objclsDALAlerts.DeleteAlertForUser(Convert.ToInt32(datTemp.Rows[k]["AlertID"].ToString()));
                        //                }
                        //            }
                        //            catch { }
                        //        }
                        //    }
                        //}

                        for (int j = datTemp.Rows.Count - 1; j >= 0; j--)
                        {
                            int i = datAlert.Rows.Count - 1;

                            while (i >= 0)
                            {
                                if ((datTemp.Rows[j]["AlertUserSettingID"].ToString() == datAlert.Rows[i]["AlertUserSettingID"].ToString())
                                       && (datTemp.Rows[j]["ReferenceID"].ToString() == datAlert.Rows[i]["ReferenceID"].ToString())
                                       && (datTemp.Rows[j]["AlertMessage"].ToString() == datAlert.Rows[i]["AlertMessage"].ToString()))
                                {
                                    datAlert.Rows.RemoveAt(i);
                                    i--;
                                    break;
                                }
                                else
                                {
                                //    objclsDALAlerts.DeleteAlertForUser(Convert.ToInt32(datTemp.Rows[j]["AlertID"].ToString()));
                                //    i--;
                                    break;
                                }
                            }
                        }
                    }
                }
                #endregion

                this.SaveUserAlert(datAlert);
                if (datAlert != null)
                {
                    datAlert.Clear();
                    datAlert.Dispose();
                    datAlert = null;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        private bool SaveUserAlert(DataTable datItemAlert)
        {
            bool blnReturnVal = false;
            if (datItemAlert != null)
            {
                try
                {
                    this.objClsConnection.BeginTransaction();
                   
                    if (datItemAlert != null && datItemAlert.Rows.Count > 0)
                        this.objclsDALAlerts.SaveAlert(datItemAlert);
                    this.objClsConnection.CommitTransaction();
                    blnReturnVal = true;
                }
                catch (Exception Ex)
                {
                    this.objClsConnection.RollbackTransaction();
                    throw Ex;
                }
                finally
                {

                }
            }
            return blnReturnVal;
        }

        public bool SetReadStatus(int intAlertID, int intStatusID)
        {
            bool blnReturnVal = false;
            try
            {
                this.objClsConnection.BeginTransaction();
                blnReturnVal = this.objclsDALAlerts.SetReadStatus(intAlertID,intStatusID);
                this.objClsConnection.CommitTransaction();
                return blnReturnVal;
            }
            catch (Exception Ex)
            {
                this.objClsConnection.RollbackTransaction();
                throw Ex;
            }
            finally
            {

            }

            return blnReturnVal;
        }
    }
}