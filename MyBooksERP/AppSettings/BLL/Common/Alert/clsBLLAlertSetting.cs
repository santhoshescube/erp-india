﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Collections;

namespace MyBooksERP
{
    public class clsBLLAlertSetting
    {
        private clsDALAlertSetting objclsDALAlertSetting;
        private clsDTOAlertSettings objclsDTOAlertSettings;
        private DataLayer objClsConnection;

        public clsBLLAlertSetting()
        {
            this.objClsConnection = new DataLayer();
            this.objclsDALAlertSetting = new clsDALAlertSetting();
            this.objclsDTOAlertSettings = new clsDTOAlertSettings();
            this.objclsDALAlertSetting.objclsDTOAlertSettings = this.objclsDTOAlertSettings;
        }

        ~clsBLLAlertSetting()
        {
            this.objClsConnection = null;
            this.objclsDALAlertSetting = null;
        }

        public clsDTOAlertSettings DTOAlertSettings
        {
            get { return this.objclsDTOAlertSettings; }
            set { this.objclsDTOAlertSettings = value; }
        }

        public DataTable FillCombos(string[] sarFieldValues)// filling combo box
        {
            try
            {
                this.objclsDALAlertSetting.objClsConnection = this.objClsConnection;
                return this.objclsDALAlertSetting.FillCombos(sarFieldValues);
            }
            catch (Exception ex) { throw ex; }
        }       

        public bool SaveAlertSettings()// insert
        {
            bool blnReturnVal = false;
            try
            {
                this.objClsConnection.BeginTransaction();
                this.objclsDALAlertSetting.objClsConnection = objClsConnection;
                this.objclsDALAlertSetting.SaveAlertSettings();
                this.objClsConnection.CommitTransaction();
                blnReturnVal = true;
            }
            catch (Exception Ex)
            {
                this.objClsConnection.RollbackTransaction();
                throw Ex;
            }
            return blnReturnVal;
        }

        public bool DeleteAlertSettings(long lngAlertSettingID)
        {
            bool blnReturnVal = false;
            try
            {
                this.objClsConnection.BeginTransaction();
                this.objclsDALAlertSetting.objClsConnection = objClsConnection;
                this.objclsDALAlertSetting.DeleteAlertSetting(lngAlertSettingID);
                this.objClsConnection.CommitTransaction();
                blnReturnVal = true;
            }
            catch (Exception Ex)
            {
                this.objClsConnection.RollbackTransaction();
                throw Ex;
            }
            return blnReturnVal;
        }

        public int RecCountNavigate()
        {
            try
            {
                this.objclsDALAlertSetting.objClsConnection = this.objClsConnection;
                return this.objclsDALAlertSetting.RecCountNavigate();
            }
            catch (Exception ex) { throw ex; }
        }

        public bool DisplayAlertSetting(int rowno)
        {
            try
            {
                this.objclsDALAlertSetting.objClsConnection = this.objClsConnection;
                this.objclsDTOAlertSettings = this.objclsDALAlertSetting.objclsDTOAlertSettings;

                return this.objclsDALAlertSetting.DisplayAlertSetting(rowno);
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable GetAlertRoleSetting(long lngAlertSettingID)
        {
            try
            {
                this.objclsDALAlertSetting.objClsConnection = this.objClsConnection;
                this.objclsDTOAlertSettings = this.objclsDALAlertSetting.objclsDTOAlertSettings;

                return this.objclsDALAlertSetting.GetAlertRoleSetting(lngAlertSettingID);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool IsAlertRoleSettingExists(long lngAlertSettingID)
        {
            int iRoleSettingCount = 0;
            try
            {
                this.objclsDALAlertSetting.objClsConnection = this.objClsConnection;
                iRoleSettingCount = this.objclsDALAlertSetting.GetAlertRoleSettingCount(lngAlertSettingID);
                return iRoleSettingCount != 0;
            }
            catch (Exception ex) { throw ex; }
        }

        public DataSet DisplayAlertSettingsEmail()// Alert Settings Email
        {
            this.objclsDALAlertSetting.objClsConnection = objClsConnection;
            return objclsDALAlertSetting.DisplayAlertSettingsEmail();
        }

        public bool DeleteAlertRolesInGrid(List<long> lnglstAlertRoleSettingID)
        {
            /* Delete alert from two tables*/
            this.objclsDALAlertSetting.objClsConnection = objClsConnection;
            return objclsDALAlertSetting.DeleteAlertRolesInGrid(lnglstAlertRoleSettingID);
        }
    }
}
