﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    class clsBLLAlertMoment
    {
        private clsDALAlerts objclsDALAlerts;
        private clsDALAlertSetting objclsDALAlertSetting;
        private DataLayer objClsConnection;
        public clsDTOMailSetting objclsDTOMailSetting;
        private Queue<long> queAlertID = null;
        private DataTable datAlertMsg;
        private DateTime dtmToDay;
        private DataTable datAlertRoleSetting = null;
        private DataTable datAlertUserSetting = null;
        private DataRow[] dtUserRows = null;

        public clsBLLAlertMoment()
        {
            this.objClsConnection = new DataLayer();
            this.objclsDALAlertSetting = new clsDALAlertSetting();
            this.objclsDALAlerts = new clsDALAlerts();
            this.objclsDALAlerts.objClsConnection = this.objClsConnection;
            this.objclsDALAlertSetting.objClsConnection = this.objClsConnection;
            this.queAlertID = new Queue<long>();
            this.getDate();
        }

        ~clsBLLAlertMoment()
        {
            this.objClsConnection = null;
            this.objclsDALAlerts = null;
            this.queAlertID.Clear();
        }

        private void getDate()
        {
            dtmToDay = new DateTime(ClsCommonSettings.GetServerDate().Year, ClsCommonSettings.GetServerDate().Month, ClsCommonSettings.GetServerDate().Day, 23, 59, 59);
        }
        public DataTable FillCombos(string[] sarFieldValues)
        {
            return objclsDALAlerts.FillCombos(sarFieldValues);
        }

        private void ClearTable(DataTable datTable)
        {
            datTable.Clear();
            datTable.Dispose();
            datTable = null;
        }
        private bool HasUserPermission(long lngAlertSettingID)
        {
            datAlertUserSetting = this.objclsDALAlerts.GetAlertUserSetting(lngAlertSettingID);
            if (datAlertUserSetting != null && datAlertUserSetting.Rows.Count != 0)
                dtUserRows = datAlertUserSetting.Select("IsAlertON=true AND UserID = " + ClsCommonSettings.UserID.ToString());
            if (dtUserRows == null || dtUserRows.Count() == 0)
            {
                if (datAlertUserSetting != null)
                {
                    ClearTable(datAlertUserSetting);
                }
                return false;
            }
            dtUserRows = null;
            return true;
        }
        private bool HasRole(long lngAlertSettingID)
        {
            datAlertRoleSetting = this.objclsDALAlerts.GetAlertRoleSetting(lngAlertSettingID);
            if (datAlertRoleSetting == null || datAlertRoleSetting.Rows.Count == 0)
            {
                if (datAlertRoleSetting != null && datAlertRoleSetting.Rows.Count == 0)
                {
                    ClearTable(datAlertRoleSetting);
                }
                return false;// null;
            }
            return true;
        }
        public DataTable GetPurchaseOrderApprovedAlert()
        {
            long lngAlertSettingID = (long)AlertSettingsTypes.PurchaseOrderApproved;
            DataTable datAlert = null;
            datAlertRoleSetting = null;
            datAlertUserSetting = null;
            dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strFilter;
            try
            {
                #region User & Role Selection

                if (!this.objclsDALAlertSetting.GetAlertSetting(lngAlertSettingID))
                    return (DataTable)null;// null;

                if (!HasUserPermission(lngAlertSettingID))
                    return (DataTable)null;

                if (!HasRole(lngAlertSettingID))
                    return (DataTable)null;

                #endregion

                #region Alert Fetching

                string strTName = "", Condition = "", IDField = "";

                strTName = "InvPurchaseOrderMaster";
                Condition = " StatusID In ( " + (int)OperationStatusType.POrderApproved + "," + (int)OperationStatusType.POrderSubmitted + ")";
                IDField = "InvPurchaseOrderMaster.PurchaseOrderID";

                DataTable datItems = new DataTable();
                if (strTName != "")
                    datItems = GetItems(IDField, Condition, strTName, lngAlertSettingID);

                #endregion

                if (datItems != null && datItems.Rows.Count > 0)
                {
                    datAlert = GetNewAlertTable();

                    foreach (DataRow dtItemRow in datItems.Rows)
                    {
                        foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                        {
                            #region Dynamic Alert Message Creation

                            strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                            strFilter += " AND IsAlertON = true";
                            dtUserRows = datAlertUserSetting.Select(strFilter);

                            if (dtUserRows.Count() == 0)
                            {
                                dtUserRows = null;
                                continue;
                            }

                            strAlertMsg = dtRoleRow["AlertMessage"].ToString();

                            int intStatusID = 0;
                            intStatusID = (int)OperationStatusType.POrderApproved;

                            DataTable datTemp = FillCombos(new string[] { "PO.PurchaseOrderNo,V.VendorName + '-' + V.VendorCode AS VendorName,PO.NetAmount,ISNULL(EM.EmployeeFullName,UM.UserName) as UserName ", "" + 
                                " InvPurchaseOrderMaster PO INNER JOIN InvVendorInformation V ON PO.VendorID=V.VendorID " +
                                " Left Join InvVerificationHistory VH On VH.ReferenceID = PO.PurchaseOrderID AND VH.OperationTypeID = " + (int)OperationType.PurchaseOrder + " AND " + 
                                " VH.StatusID = " + intStatusID + " Left Join UserMaster UM ON UM.UserID = PO.CreatedBy Left Join EmployeeMaster EM ON EM.EmployeeID = UM.EmployeeID ", "" +
                                " PO.PurchaseOrderID=" + dtItemRow["PurchaseOrderID"].ToInt32() });

                            if (datTemp != null)
                            {
                                if (datTemp.Rows.Count > 0)
                                {
                                    strAlertMsg = strAlertMsg.Replace("<PURCHASEORDERNO>", dtItemRow["PurchaseOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<SUPNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<DONEBY>", datTemp.Rows[0]["UserName"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(dtItemRow["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                                }
                            }


                            #endregion

                            #region ReferenceID Setting

                            foreach (DataRow dtUserRow in dtUserRows)
                            {
                                if (Convert.ToInt32(dtUserRow["UserID"]) != ClsCommonSettings.UserID)
                                    continue;

                                dtNewAlertRow = datAlert.NewRow();
                                dtNewAlertRow["AlertID"] = 0;
                                dtNewAlertRow["StartDate"] = ClsCommonSettings.GetServerDate();
                                dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];

                                if (lngAlertSettingID == (int)AlertSettingsTypes.PurchaseOrderApproved)
                                {
                                    dtNewAlertRow["ReferenceID"] = dtItemRow["PurchaseOrderID"];
                                }

                                dtNewAlertRow["AlertMessage"] = strAlertMsg;
                                dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                                datAlert.Rows.Add(dtNewAlertRow);

                                dtNewAlertRow = null;
                            }

                            #endregion

                            strAlertMsg = "";
                            strFilter = "";
                            dtUserRows = null;
                        }
                    }
                    ClearTable(datAlertRoleSetting);
                    ClearTable(datAlertUserSetting);

                    datAlert.AcceptChanges();
                }


                #region Unwanted Alert Deletion

                if (datAlert != null)
                {
                    UnwantedAlertDeletionAndSave(datAlert, lngAlertSettingID);
                }
                #endregion
                GetOpenedAlerts(dtmToDay);
                return datAlertMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetSalesQuotationSubmittedAlert()

        {
            long lngAlertSettingID = (long)AlertSettingsTypes.SalesQuotationSubmitted;
            DataTable datAlert = null;
            datAlertRoleSetting = null;
            datAlertUserSetting = null;
            dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strFilter;
            try
            {
                #region User & Role Selection

                if (!this.objclsDALAlertSetting.GetAlertSetting(lngAlertSettingID))
                    return (DataTable)null;// null;

                if (!HasUserPermission(lngAlertSettingID))
                    return (DataTable)null;

                if (!HasRole(lngAlertSettingID))
                    return (DataTable)null;

                #endregion

                #region Alert Fetching

                string strTName = "", Condition = "", IDField = "";

                strTName = "InvSalesQuotationMaster";
                Condition = " StatusID = " + (int)OperationStatusType.SQuotationSubmitted;
                IDField = "InvSalesQuotationMaster.SalesQuotationID";

                DataTable datItems = new DataTable();
                if (strTName != "")
                    datItems = GetItems(IDField, Condition, strTName, lngAlertSettingID);

                #endregion

                if (datItems != null && datItems.Rows.Count > 0)
                {
                    datAlert = GetNewAlertTable();

                    foreach (DataRow dtItemRow in datItems.Rows)
                    {
                        foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                        {
                            #region Dynamic Alert Message Creation

                            strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                            strFilter += " AND IsAlertON = true";
                            dtUserRows = datAlertUserSetting.Select(strFilter);

                            if (dtUserRows.Count() == 0)
                            {
                                dtUserRows = null;
                                continue;
                            }

                            strAlertMsg = dtRoleRow["AlertMessage"].ToString();

                            int intStatusID = (int)OperationStatusType.SQuotationSubmitted;

                            DataTable datTemp = FillCombos(new string[] { "SQ.SalesQuotationNo,V.VendorName + '-' + V.VendorCode AS VendorName,SQ.NetAmount,ISNULL(EM.EmployeeFullName,UM.UserName) as UserName ", "" + 
                                    "InvSalesQuotationMaster SQ LEFT JOIN InvVendorInformation V ON SQ.VendorID=V.VendorID " +
                                    " Left Join InvVerificationHistory VH On VH.ReferenceID = SQ.SalesQuotationID AND VH.OperationTypeID = " + (int)OperationType.SalesQuotation + " AND " + 
                                    " VH.StatusID = " + intStatusID + " Left Join UserMaster UM ON UM.UserID = SQ.CreatedBy Left Join EmployeeMaster EM ON EM.EmployeeID = UM.EmployeeID ", "" +
                                    "SQ.SalesQuotationID=" + dtItemRow["SalesQuotationID"].ToInt32() });

                            if (datTemp != null)
                            {
                                if (datTemp.Rows.Count > 0)
                                {
                                    strAlertMsg = strAlertMsg.Replace("<SALESQUOTATIONNO>", datTemp.Rows[0]["SalesQuotationNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                    //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<DONEBY>", datTemp.Rows[0]["UserName"].ToString());
                                }
                            }

                            #endregion

                            #region ReferenceID Setting

                            foreach (DataRow dtUserRow in dtUserRows)
                            {
                                if (Convert.ToInt32(dtUserRow["UserID"]) != ClsCommonSettings.UserID)
                                    continue;

                                dtNewAlertRow = datAlert.NewRow();
                                dtNewAlertRow["AlertID"] = 0;
                                dtNewAlertRow["StartDate"] = ClsCommonSettings.GetServerDate();
                                dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                                dtNewAlertRow["ReferenceID"] = dtItemRow["SalesQuotationID"];
                                dtNewAlertRow["AlertMessage"] = strAlertMsg;
                                dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                                datAlert.Rows.Add(dtNewAlertRow);

                                dtNewAlertRow = null;
                            }

                            #endregion

                            strAlertMsg = "";
                            strFilter = "";
                            dtUserRows = null;
                        }
                    }
                    ClearTable(datAlertRoleSetting);
                    ClearTable(datAlertUserSetting);

                    datAlert.AcceptChanges();
                }


                #region Unwanted Alert Deletion

                if (datAlert != null)
                {
                    UnwantedAlertDeletionAndSave(datAlert, lngAlertSettingID);
                }
                #endregion
                GetOpenedAlerts(dtmToDay);
                return datAlertMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetSalesQuotationApprovedAlert()
        {
            long lngAlertSettingID = (long)AlertSettingsTypes.SalesQuotationApproved;
            DataTable datAlert = null;
            datAlertRoleSetting = null;
            datAlertUserSetting = null;
            dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strFilter;
            try
            {
                #region User & Role Selection

                if (!this.objclsDALAlertSetting.GetAlertSetting(lngAlertSettingID))
                    return (DataTable)null;// null;

                if (!HasUserPermission(lngAlertSettingID))
                    return (DataTable)null;

                if (!HasRole(lngAlertSettingID))
                    return (DataTable)null;

                #endregion

                #region Alert Fetching

                string strTName = "", Condition = "", IDField = "";

                strTName = "InvSalesQuotationMaster";
                Condition = " StatusID In (" + (int)OperationStatusType.SQuotationApproved + "," + (int)OperationStatusType.SQuotationSubmitted + ")";
                IDField = "InvSalesQuotationMaster.SalesQuotationID";

                DataTable datItems = new DataTable();
                if (strTName != "")
                    datItems = GetItems(IDField, Condition, strTName, lngAlertSettingID);

                #endregion

                if (datItems != null && datItems.Rows.Count > 0)
                {
                    datAlert = GetNewAlertTable();

                    foreach (DataRow dtItemRow in datItems.Rows)
                    {
                        foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                        {
                            #region Dynamic Alert Message Creation

                            strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                            strFilter += " AND IsAlertON = true";
                            dtUserRows = datAlertUserSetting.Select(strFilter);

                            if (dtUserRows.Count() == 0)
                            {
                                dtUserRows = null;
                                continue;
                            }

                            strAlertMsg = dtRoleRow["AlertMessage"].ToString();

                            int intStatusID = (int)OperationStatusType.SQuotationApproved;

                            DataTable datTemp = FillCombos(new string[] { "SQ.SalesQuotationNo,V.VendorName + '-' + V.VendorCode AS VendorName,SQ.NetAmount,ISNULL(EM.EmployeeFullName,UM.UserName) as UserName ", "" + 
                                    "InvSalesQuotationMaster SQ LEFT JOIN InvVendorInformation V ON SQ.VendorID=V.VendorID " +
                                    " Left Join InvVerificationHistory VH On VH.ReferenceID = SQ.SalesQuotationID AND VH.OperationTypeID = " + (int)OperationType.SalesQuotation + " AND " + 
                                    " VH.StatusID = " + intStatusID + " Left Join UserMaster UM ON UM.UserID = SQ.CreatedBy Left Join EmployeeMaster EM ON EM.EmployeeID = UM.EmployeeID ", "" +
                                    "SQ.SalesQuotationID=" + dtItemRow["SalesQuotationID"].ToInt32() });

                            if (datTemp != null)
                            {
                                if (datTemp.Rows.Count > 0)
                                {
                                    strAlertMsg = strAlertMsg.Replace("<SALESQUOTATIONNO>", datTemp.Rows[0]["SalesQuotationNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                    //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<DONEBY>", datTemp.Rows[0]["UserName"].ToString());
                                }
                            }
                            #endregion

                            #region ReferenceID Setting

                            foreach (DataRow dtUserRow in dtUserRows)
                            {
                                if (Convert.ToInt32(dtUserRow["UserID"]) != ClsCommonSettings.UserID)
                                    continue;

                                dtNewAlertRow = datAlert.NewRow();
                                dtNewAlertRow["AlertID"] = 0;
                                dtNewAlertRow["StartDate"] = ClsCommonSettings.GetServerDate();
                                dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                                dtNewAlertRow["ReferenceID"] = dtItemRow["SalesQuotationID"];
                                dtNewAlertRow["AlertMessage"] = strAlertMsg;
                                dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                                datAlert.Rows.Add(dtNewAlertRow);

                                dtNewAlertRow = null;
                            }

                            #endregion

                            strAlertMsg = "";
                            strFilter = "";
                            dtUserRows = null;
                        }
                    }
                    ClearTable(datAlertRoleSetting);
                    ClearTable(datAlertUserSetting);

                    datAlert.AcceptChanges();
                }


                #region Unwanted Alert Deletion

                if (datAlert != null)
                {
                    UnwantedAlertDeletionAndSave(datAlert, lngAlertSettingID);
                }
                #endregion
                GetOpenedAlerts(dtmToDay);
                return datAlertMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetSalesQuotationCancelledAlert()
        {
            long lngAlertSettingID = (long)AlertSettingsTypes.SalesQuotationCancelled;
            DataTable datAlert = null;
            datAlertRoleSetting = null;
            datAlertUserSetting = null;
            dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strFilter;
            try
            {
                #region User & Role Selection

                if (!this.objclsDALAlertSetting.GetAlertSetting(lngAlertSettingID))
                    return (DataTable)null;// null;

                if (!HasUserPermission(lngAlertSettingID))
                    return (DataTable)null;

                if (!HasRole(lngAlertSettingID))
                    return (DataTable)null;

                #endregion

                #region Alert Fetching

                string strTName = "", Condition = "", IDField = "";

                strTName = "InvSalesQuotationMaster";
                Condition = " StatusID = " + (int)OperationStatusType.SQuotationCancelled;
                IDField = "InvSalesQuotationMaster.SalesQuotationID";

                DataTable datItems = new DataTable();
                if (strTName != "")
                    datItems = GetItems(IDField, Condition, strTName, lngAlertSettingID);

                #endregion

                if (datItems != null && datItems.Rows.Count > 0)
                {
                    datAlert = GetNewAlertTable();

                    foreach (DataRow dtItemRow in datItems.Rows)
                    {
                        foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                        {
                            #region Dynamic Alert Message Creation

                            strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                            strFilter += " AND IsAlertON = true";
                            dtUserRows = datAlertUserSetting.Select(strFilter);

                            if (dtUserRows.Count() == 0)
                            {
                                dtUserRows = null;
                                continue;
                            }

                            strAlertMsg = dtRoleRow["AlertMessage"].ToString();

                            int intStatusID = (int)OperationStatusType.SQuotationCancelled;

                            DataTable datTemp = FillCombos(new string[] { "SQ.SalesQuotationNo,V.VendorName + '-' + V.VendorCode AS VendorName,SQ.NetAmount,ISNULL(EM.EmployeeFullName,UM.UserName) as UserName ", "" + 
                                    "InvSalesQuotationMaster SQ LEFT JOIN InvVendorInformation V ON SQ.VendorID=V.VendorID " +
                                    " Left Join InvVerificationHistory VH On VH.ReferenceID = SQ.SalesQuotationID AND VH.OperationTypeID = " + (int)OperationType.SalesQuotation + " AND " + 
                                    " VH.StatusID = " + intStatusID + " Left Join UserMaster UM ON UM.UserID = SQ.CreatedBy Left Join EmployeeMaster EM ON EM.EmployeeID = UM.EmployeeID ", "" +
                                    "SQ.SalesQuotationID=" + dtItemRow["SalesQuotationID"].ToInt32() });

                            if (datTemp != null)
                            {
                                if (datTemp.Rows.Count > 0)
                                {
                                    strAlertMsg = strAlertMsg.Replace("<SALESQUOTATIONNO>", datTemp.Rows[0]["SalesQuotationNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                    //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<DONEBY>", datTemp.Rows[0]["UserName"].ToString());
                                }
                            }
                            #endregion

                            #region ReferenceID Setting

                            foreach (DataRow dtUserRow in dtUserRows)
                            {
                                if (Convert.ToInt32(dtUserRow["UserID"]) != ClsCommonSettings.UserID)
                                    continue;

                                dtNewAlertRow = datAlert.NewRow();
                                dtNewAlertRow["AlertID"] = 0;
                                dtNewAlertRow["StartDate"] = ClsCommonSettings.GetServerDate();
                                dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                                dtNewAlertRow["ReferenceID"] = dtItemRow["SalesQuotationID"];
                                dtNewAlertRow["AlertMessage"] = strAlertMsg;
                                dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                                datAlert.Rows.Add(dtNewAlertRow);

                                dtNewAlertRow = null;
                            }

                            #endregion

                            strAlertMsg = "";
                            strFilter = "";
                            dtUserRows = null;
                        }
                    }
                    ClearTable(datAlertRoleSetting);
                    ClearTable(datAlertUserSetting);

                    datAlert.AcceptChanges();
                }


                #region Unwanted Alert Deletion

                if (datAlert != null)
                {
                    UnwantedAlertDeletionAndSave(datAlert, lngAlertSettingID);
                }
                #endregion
                GetOpenedAlerts(dtmToDay);
                return datAlertMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetSalesOrderCreatedAlert()
        {
            long lngAlertSettingID = (long)AlertSettingsTypes.SaleOrderCreated;
            DataTable datAlert = null;
            datAlertRoleSetting = null;
            datAlertUserSetting = null;
            dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strFilter;
            try
            {
                #region User & Role Selection

                if (!this.objclsDALAlertSetting.GetAlertSetting(lngAlertSettingID))
                    return (DataTable)null;// null;

                if (!HasUserPermission(lngAlertSettingID))
                    return (DataTable)null;

                if (!HasRole(lngAlertSettingID))
                    return (DataTable)null;

                #endregion

                #region Alert Fetching

                string strTName = "", Condition = "", IDField = "";

                strTName = "InvSalesOrderMaster";
                Condition = " StatusID = " + (int)OperationStatusType.SOrderOpen;
                IDField = "InvSalesOrderMaster.SalesOrderID";

                DataTable datItems = new DataTable();
                if (strTName != "")
                    datItems = GetItems(IDField, Condition, strTName, lngAlertSettingID);

                #endregion

                if (datItems != null && datItems.Rows.Count > 0)
                {
                    datAlert = GetNewAlertTable();

                    foreach (DataRow dtItemRow in datItems.Rows)
                    {
                        foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                        {
                            #region Dynamic Alert Message Creation

                            strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                            strFilter += " AND IsAlertON = true";
                            dtUserRows = datAlertUserSetting.Select(strFilter);

                            if (dtUserRows.Count() == 0)
                            {
                                dtUserRows = null;
                                continue;
                            }

                            strAlertMsg = dtRoleRow["AlertMessage"].ToString();

                            int intStatusID = (int)OperationStatusType.SOrderOpen;


                            DataTable datTemp = FillCombos(new string[] { "SO.SalesOrderNo,V.VendorName + '-' + V.VendorCode AS VendorName,SO.NetAmount,ISNULL(EM.EmployeeFullName,UM.UserName) as UserName ", "" + 
                                    " InvSalesOrderMaster SO LEFT JOIN InvSalesOrderReferenceDetails SOR ON SOR.SalesOrderID=SO.SalesOrderID LEFT JOIN InvSalesQuotationMaster SQ ON SOR.ReferenceID = SQ.SalesQuotationID AND SO.OrderTypeID = " +(int)OperationOrderType.SOTFromQuotation + " LEFT JOIN InvVendorInformation V ON SO.VendorID=V.VendorID " +
                                    " Left Join InvVerificationHistory VH On VH.ReferenceID = SO.SalesOrderID AND VH.OperationTypeID = " + (int)OperationType.SalesOrder + " AND " + 
                                    " VH.StatusID = " + intStatusID + " Left Join UserMaster UM ON UM.UserID = SO.CreatedBy Left Join EmployeeMaster EM ON EM.EmployeeID = UM.EmployeeID ", "" +
                                    " SO.SalesOrderID=" + dtItemRow["SalesOrderID"].ToInt32() });

                            if (datTemp != null)
                            {
                                if (datTemp.Rows.Count > 0)
                                {
                                    strAlertMsg = strAlertMsg.Replace("<SALEORDERNO>", dtItemRow["SalesOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                    //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<DONEBY>", datTemp.Rows[0]["UserName"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(dtItemRow["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                                }
                            }
                            #endregion

                            #region ReferenceID Setting

                            foreach (DataRow dtUserRow in dtUserRows)
                            {
                                if (Convert.ToInt32(dtUserRow["UserID"]) != ClsCommonSettings.UserID)
                                    continue;

                                dtNewAlertRow = datAlert.NewRow();
                                dtNewAlertRow["AlertID"] = 0;
                                dtNewAlertRow["StartDate"] = ClsCommonSettings.GetServerDate();
                                dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                                dtNewAlertRow["ReferenceID"] = dtItemRow["SalesOrderID"];
                                dtNewAlertRow["AlertMessage"] = strAlertMsg;
                                dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                                datAlert.Rows.Add(dtNewAlertRow);

                                dtNewAlertRow = null;
                            }

                            #endregion

                            strAlertMsg = "";
                            strFilter = "";
                            dtUserRows = null;
                        }
                    }
                    ClearTable(datAlertRoleSetting);
                    ClearTable(datAlertUserSetting);

                    datAlert.AcceptChanges();
                }


                #region Unwanted Alert Deletion

                if (datAlert != null)
                {
                    UnwantedAlertDeletionAndSave(datAlert, lngAlertSettingID);
                }
                #endregion
                GetOpenedAlerts(dtmToDay);
                return datAlertMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetSalesOrderAdvancePaidAlert()
        {
            long lngAlertSettingID = (long)AlertSettingsTypes.SalesOrderAdvancePaid;
            DataTable datAlert = null;
            datAlertRoleSetting = null;
            datAlertUserSetting = null;
            dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strFilter;
            try
            {
                #region User & Role Selection

                if (!this.objclsDALAlertSetting.GetAlertSetting(lngAlertSettingID))
                    return (DataTable)null;// null;

                if (!HasUserPermission(lngAlertSettingID))
                    return (DataTable)null;

                if (!HasRole(lngAlertSettingID))
                    return (DataTable)null;

                #endregion

                #region Alert Fetching

                string strTName = "", Condition = "", IDField = "";

                strTName = "AccReceiptAndPaymentMaster";
                Condition = " OperationTypeID = " + (int)OperationType.SalesOrder + " AND ReceiptAndPaymentTypeID=" + (int)PaymentTypes.AdvancePayment;
                IDField = "AccReceiptAndPaymentMaster.ReceiptAndPaymentID";

                DataTable datItems = new DataTable();
                if (strTName != "")
                    datItems = GetItems(IDField, Condition, strTName, lngAlertSettingID);

                #endregion

                if (datItems != null && datItems.Rows.Count > 0)
                {
                    datAlert = GetNewAlertTable();

                    foreach (DataRow dtItemRow in datItems.Rows)
                    {
                        foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                        {
                            #region Dynamic Alert Message Creation

                            strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                            strFilter += " AND IsAlertON = true";
                            dtUserRows = datAlertUserSetting.Select(strFilter);

                            if (dtUserRows.Count() == 0)
                            {
                                dtUserRows = null;
                                continue;
                            }

                            strAlertMsg = dtRoleRow["AlertMessage"].ToString();

                            DataTable datTemp = FillCombos(new string[] { "SO.SalesOrderNo,V.VendorName + '-' + V.VendorCode AS VendorName,SO.NetAmount,SUM(D.Amount) AS PaidAmount", "" + 
                                    "AccReceiptAndPaymentMaster M INNER JOIN AccReceiptAndPaymentDetails D ON M.ReceiptAndPaymentID=D.ReceiptAndPaymentID " +
                                    "INNER JOIN InvSalesOrderMaster SO ON D.ReferenceID = SO.SalesOrderID INNER JOIN InvVendorInformation V ON SO.VendorID=V.VendorID ", "" +
                                    "M.ReceiptAndPaymentID=" + dtItemRow["ReceiptAndPaymentID"].ToInt64() + " GROUP BY SO.SalesOrderNo,V.VendorName,V.VendorCode,SO.NetAmount" });
                            if (datTemp != null)
                            {
                                if (datTemp.Rows.Count > 0)
                                {
                                    strAlertMsg = strAlertMsg.Replace("<SALESORDERNO>", datTemp.Rows[0]["SalesOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                    //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<ADVAMOUNT>", datTemp.Rows[0]["PaidAmount"].ToString());
                                }
                            }
                            #endregion

                            #region ReferenceID Setting

                            foreach (DataRow dtUserRow in dtUserRows)
                            {
                                if (Convert.ToInt32(dtUserRow["UserID"]) != ClsCommonSettings.UserID)
                                    continue;

                                dtNewAlertRow = datAlert.NewRow();
                                dtNewAlertRow["AlertID"] = 0;
                                dtNewAlertRow["StartDate"] = ClsCommonSettings.GetServerDate();
                                dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                                dtNewAlertRow["ReferenceID"] = dtItemRow["ReceiptAndPaymentID"];
                                dtNewAlertRow["AlertMessage"] = strAlertMsg;
                                dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                                datAlert.Rows.Add(dtNewAlertRow);

                                dtNewAlertRow = null;
                            }

                            #endregion

                            strAlertMsg = "";
                            strFilter = "";
                            dtUserRows = null;
                        }
                    }
                    ClearTable(datAlertRoleSetting);
                    ClearTable(datAlertUserSetting);

                    datAlert.AcceptChanges();
                }


                #region Unwanted Alert Deletion

                if (datAlert != null)
                {
                    UnwantedAlertDeletionAndSave(datAlert, lngAlertSettingID);
                }
                #endregion
                GetOpenedAlerts(dtmToDay);
                return datAlertMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetSaleOrderSubmittedAlert()
        {
            long lngAlertSettingID = (long)AlertSettingsTypes.SaleOrderSubmitted;
            DataTable datAlert = null;
            datAlertRoleSetting = null;
            datAlertUserSetting = null;
            dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strFilter;
            try
            {
                #region User & Role Selection

                if (!this.objclsDALAlertSetting.GetAlertSetting(lngAlertSettingID))
                    return (DataTable)null;// null;

                if (!HasUserPermission(lngAlertSettingID))
                    return (DataTable)null;

                if (!HasRole(lngAlertSettingID))
                    return (DataTable)null;

                #endregion

                #region Alert Fetching

                string strTName = "", Condition = "", IDField = "";

                strTName = "InvSalesOrderMaster";
                Condition = " StatusID = " + (int)OperationStatusType.SOrderSubmitted;
                IDField = "InvSalesOrderMaster.SalesOrderID";

                DataTable datItems = new DataTable();
                if (strTName != "")
                    datItems = GetItems(IDField, Condition, strTName, lngAlertSettingID);

                #endregion

                if (datItems != null && datItems.Rows.Count > 0)
                {
                    datAlert = GetNewAlertTable();

                    foreach (DataRow dtItemRow in datItems.Rows)
                    {
                        foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                        {
                            #region Dynamic Alert Message Creation

                            strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                            strFilter += " AND IsAlertON = true";
                            dtUserRows = datAlertUserSetting.Select(strFilter);

                            if (dtUserRows.Count() == 0)
                            {
                                dtUserRows = null;
                                continue;
                            }

                            strAlertMsg = dtRoleRow["AlertMessage"].ToString();
                            int intStatusID = (int)OperationStatusType.SOrderSubmitted;

                            DataTable datTemp = FillCombos(new string[] { "SO.SalesOrderNo,V.VendorName + '-' + V.VendorCode AS VendorName,SO.NetAmount,ISNULL(EM.EmployeeFullName,UM.UserName) as UserName ", "" + 
                                    " InvSalesOrderMaster SO LEFT JOIN InvSalesOrderReferenceDetails SOR ON SOR.SalesOrderID=SO.SalesOrderID LEFT JOIN InvSalesQuotationMaster SQ ON SOR.ReferenceID = SQ.SalesQuotationID AND SO.OrderTypeID = " +(int)OperationOrderType.SOTFromQuotation + " LEFT JOIN InvVendorInformation V ON SO.VendorID=V.VendorID " +
                                    " Left Join InvVerificationHistory VH On VH.ReferenceID = SO.SalesOrderID AND VH.OperationTypeID = " + (int)OperationType.SalesOrder + " AND " + 
                                    " VH.StatusID = " + intStatusID + " Left Join UserMaster UM ON UM.UserID = SO.CreatedBy Left Join EmployeeMaster EM ON EM.EmployeeID = UM.EmployeeID ", "" +
                                    " SO.SalesOrderID=" + dtItemRow["SalesOrderID"].ToInt32() });

                            if (datTemp != null)
                            {
                                if (datTemp.Rows.Count > 0)
                                {
                                    strAlertMsg = strAlertMsg.Replace("<SALEORDERNO>", dtItemRow["SalesOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                    //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<DONEBY>", datTemp.Rows[0]["UserName"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(dtItemRow["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                                }
                            }
                            #endregion

                            #region ReferenceID Setting

                            foreach (DataRow dtUserRow in dtUserRows)
                            {
                                if (Convert.ToInt32(dtUserRow["UserID"]) != ClsCommonSettings.UserID)
                                    continue;

                                dtNewAlertRow = datAlert.NewRow();
                                dtNewAlertRow["AlertID"] = 0;
                                dtNewAlertRow["StartDate"] = ClsCommonSettings.GetServerDate();
                                dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                                dtNewAlertRow["ReferenceID"] = dtItemRow["SalesOrderID"];
                                dtNewAlertRow["AlertMessage"] = strAlertMsg;
                                dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                                datAlert.Rows.Add(dtNewAlertRow);

                                dtNewAlertRow = null;
                            }

                            #endregion

                            strAlertMsg = "";
                            strFilter = "";
                            dtUserRows = null;
                        }
                    }
                    ClearTable(datAlertRoleSetting);
                    ClearTable(datAlertUserSetting);

                    datAlert.AcceptChanges();
                }


                #region Unwanted Alert Deletion

                if (datAlert != null)
                {
                    UnwantedAlertDeletionAndSave(datAlert, lngAlertSettingID);
                }
                #endregion
                GetOpenedAlerts(dtmToDay);
                return datAlertMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetSaleOrderRejectedAlert()
        {
            long lngAlertSettingID = (long)AlertSettingsTypes.SaleOrderRejected;
            DataTable datAlert = null;
            datAlertRoleSetting = null;
            datAlertUserSetting = null;
            dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strFilter;
            try
            {
                #region User & Role Selection

                if (!this.objclsDALAlertSetting.GetAlertSetting(lngAlertSettingID))
                    return (DataTable)null;// null;

                if (!HasUserPermission(lngAlertSettingID))
                    return (DataTable)null;

                if (!HasRole(lngAlertSettingID))
                    return (DataTable)null;

                #endregion

                #region Alert Fetching

                string strTName = "", Condition = "", IDField = "";

                strTName = "InvSalesOrderMaster";
                Condition = " StatusID = " + (int)OperationStatusType.SOrderRejected;
                IDField = "InvSalesOrderMaster.SalesOrderID";

                DataTable datItems = new DataTable();
                if (strTName != "")
                    datItems = GetItems(IDField, Condition, strTName, lngAlertSettingID);

                #endregion

                if (datItems != null && datItems.Rows.Count > 0)
                {
                    datAlert = GetNewAlertTable();

                    foreach (DataRow dtItemRow in datItems.Rows)
                    {
                        foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                        {
                            #region Dynamic Alert Message Creation

                            strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                            strFilter += " AND IsAlertON = true";
                            dtUserRows = datAlertUserSetting.Select(strFilter);

                            if (dtUserRows.Count() == 0)
                            {
                                dtUserRows = null;
                                continue;
                            }

                            strAlertMsg = dtRoleRow["AlertMessage"].ToString();
                            int intStatusID = (int)OperationStatusType.SOrderRejected;

                            DataTable datTemp = FillCombos(new string[] { "SO.SalesOrderNo,V.VendorName + '-' + V.VendorCode AS VendorName,SO.NetAmount,ISNULL(EM.EmployeeFullName,UM.UserName) as UserName ", "" + 
                                    " InvSalesOrderMaster SO LEFT JOIN InvSalesOrderReferenceDetails SOR ON SOR.SalesOrderID=SO.SalesOrderID LEFT JOIN InvSalesQuotationMaster SQ ON SOR.ReferenceID = SQ.SalesQuotationID AND SO.OrderTypeID = " +(int)OperationOrderType.SOTFromQuotation + " LEFT JOIN InvVendorInformation V ON SO.VendorID=V.VendorID " +
                                    " Left Join InvVerificationHistory VH On VH.ReferenceID = SO.SalesOrderID AND VH.OperationTypeID = " + (int)OperationType.SalesOrder + " AND " + 
                                    " VH.StatusID = " + intStatusID + " Left Join UserMaster UM ON UM.UserID = SO.CreatedBy Left Join EmployeeMaster EM ON EM.EmployeeID = UM.EmployeeID ", "" +
                                    " SO.SalesOrderID=" + dtItemRow["SalesOrderID"].ToInt32() });

                            if (datTemp != null)
                            {
                                if (datTemp.Rows.Count > 0)
                                {
                                    strAlertMsg = strAlertMsg.Replace("<SALEORDERNO>", dtItemRow["SalesOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                    //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<DONEBY>", datTemp.Rows[0]["UserName"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(dtItemRow["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                                }
                            }
                            #endregion

                            #region ReferenceID Setting

                            foreach (DataRow dtUserRow in dtUserRows)
                            {
                                if (Convert.ToInt32(dtUserRow["UserID"]) != ClsCommonSettings.UserID)
                                    continue;

                                dtNewAlertRow = datAlert.NewRow();
                                dtNewAlertRow["AlertID"] = 0;
                                dtNewAlertRow["StartDate"] = ClsCommonSettings.GetServerDate();
                                dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                                dtNewAlertRow["ReferenceID"] = dtItemRow["SalesOrderID"];
                                dtNewAlertRow["AlertMessage"] = strAlertMsg;
                                dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                                datAlert.Rows.Add(dtNewAlertRow);

                                dtNewAlertRow = null;
                            }

                            #endregion

                            strAlertMsg = "";
                            strFilter = "";
                            dtUserRows = null;
                        }
                    }
                    ClearTable(datAlertRoleSetting);
                    ClearTable(datAlertUserSetting);

                    datAlert.AcceptChanges();
                }


                #region Unwanted Alert Deletion

                if (datAlert != null)
                {
                    UnwantedAlertDeletionAndSave(datAlert, lngAlertSettingID);
                }
                #endregion
                GetOpenedAlerts(dtmToDay);
                return datAlertMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetSaleOrderApprovedAlert()
        {
            long lngAlertSettingID = (long)AlertSettingsTypes.SaleOrderApproved;
            DataTable datAlert = null;
            datAlertRoleSetting = null;
            datAlertUserSetting = null;
            dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strFilter;
            try
            {
                #region User & Role Selection

                if (!this.objclsDALAlertSetting.GetAlertSetting(lngAlertSettingID))
                    return (DataTable)null;// null;

                if (!HasUserPermission(lngAlertSettingID))
                    return (DataTable)null;

                if (!HasRole(lngAlertSettingID))
                    return (DataTable)null;

                #endregion

                #region Alert Fetching

                string strTName = "", Condition = "", IDField = "";

                strTName = "InvSalesOrderMaster";
                Condition = " StatusID In ( " + (int)OperationStatusType.SOrderApproved + "," + (int)OperationStatusType.SOrderSubmitted + ")";
                IDField = "InvSalesOrderMaster.SalesOrderID";

                DataTable datItems = new DataTable();
                if (strTName != "")
                    datItems = GetItems(IDField, Condition, strTName, lngAlertSettingID);

                #endregion

                if (datItems != null && datItems.Rows.Count > 0)
                {
                    datAlert = GetNewAlertTable();

                    foreach (DataRow dtItemRow in datItems.Rows)
                    {
                        foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                        {
                            #region Dynamic Alert Message Creation

                            strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                            strFilter += " AND IsAlertON = true";
                            dtUserRows = datAlertUserSetting.Select(strFilter);

                            if (dtUserRows.Count() == 0)
                            {
                                dtUserRows = null;
                                continue;
                            }

                            strAlertMsg = dtRoleRow["AlertMessage"].ToString();
                            int intStatusID = (int)OperationStatusType.SOrderApproved;

                            DataTable datTemp = FillCombos(new string[] { "SO.SalesOrderNo,V.VendorName + '-' + V.VendorCode AS VendorName,SO.NetAmount,ISNULL(EM.EmployeeFullName,UM.UserName) as UserName ", "" + 
                                    " InvSalesOrderMaster SO LEFT JOIN InvSalesOrderReferenceDetails SOR ON SOR.SalesOrderID=SO.SalesOrderID LEFT JOIN InvSalesQuotationMaster SQ ON SOR.ReferenceID = SQ.SalesQuotationID AND SO.OrderTypeID = " +(int)OperationOrderType.SOTFromQuotation + " LEFT JOIN InvVendorInformation V ON SO.VendorID=V.VendorID " +
                                    " Left Join InvVerificationHistory VH On VH.ReferenceID = SO.SalesOrderID AND VH.OperationTypeID = " + (int)OperationType.SalesOrder + " AND " + 
                                    " VH.StatusID = " + intStatusID + " Left Join UserMaster UM ON UM.UserID = SO.CreatedBy Left Join EmployeeMaster EM ON EM.EmployeeID = UM.EmployeeID ", "" +
                                    " SO.SalesOrderID=" + dtItemRow["SalesOrderID"].ToInt32() });

                            if (datTemp != null)
                            {
                                if (datTemp.Rows.Count > 0)
                                {
                                    strAlertMsg = strAlertMsg.Replace("<SALEORDERNO>", dtItemRow["SalesOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                    //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<TOTAMOUNT>", datTemp.Rows[0]["NetAmount"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<DONEBY>", datTemp.Rows[0]["UserName"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<DUEDATE>", (Convert.ToDateTime(dtItemRow["DueDate"].ToString())).ToString("dd-MMM-yyyy"));
                                }
                            }
                            #endregion

                            #region ReferenceID Setting

                            foreach (DataRow dtUserRow in dtUserRows)
                            {
                                if (Convert.ToInt32(dtUserRow["UserID"]) != ClsCommonSettings.UserID)
                                    continue;

                                dtNewAlertRow = datAlert.NewRow();
                                dtNewAlertRow["AlertID"] = 0;
                                dtNewAlertRow["StartDate"] = ClsCommonSettings.GetServerDate();
                                dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                                dtNewAlertRow["ReferenceID"] = dtItemRow["SalesOrderID"];
                                dtNewAlertRow["AlertMessage"] = strAlertMsg;
                                dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                                datAlert.Rows.Add(dtNewAlertRow);

                                dtNewAlertRow = null;
                            }

                            #endregion

                            strAlertMsg = "";
                            strFilter = "";
                            dtUserRows = null;
                        }
                    }
                    ClearTable(datAlertRoleSetting);
                    ClearTable(datAlertUserSetting);

                    datAlert.AcceptChanges();
                }


                #region Unwanted Alert Deletion

                if (datAlert != null)
                {
                    UnwantedAlertDeletionAndSave(datAlert, lngAlertSettingID);
                }
                #endregion
                GetOpenedAlerts(dtmToDay);
                return datAlertMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetMaterialIssueAlert()
        {
            long lngAlertSettingID = (long)AlertSettingsTypes.MaterialIssue;
            DataTable datAlert = null;
            datAlertRoleSetting = null;
            datAlertUserSetting = null;
            dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strFilter;
            try
            {
                #region User & Role Selection

                if (!this.objclsDALAlertSetting.GetAlertSetting(lngAlertSettingID))
                    return (DataTable)null;// null;

                if (!HasUserPermission(lngAlertSettingID))
                    return (DataTable)null;

                if (!HasRole(lngAlertSettingID))
                    return (DataTable)null;

                #endregion

                #region Alert Fetching

                string strTName = "", Condition = "", IDField = "";

                strTName = "InvMaterialIssueMaster";
                IDField = "InvMaterialIssueMaster.MaterialIssueID";

                DataTable datItems = new DataTable();
                if (strTName != "")
                    datItems = GetItems(IDField, Condition, strTName, lngAlertSettingID);

                #endregion

                if (datItems != null && datItems.Rows.Count > 0)
                {
                    datAlert = GetNewAlertTable();

                    foreach (DataRow dtItemRow in datItems.Rows)
                    {
                        foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                        {
                            #region Dynamic Alert Message Creation

                            strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                            strFilter += " AND IsAlertON = true";
                            dtUserRows = datAlertUserSetting.Select(strFilter);

                            if (dtUserRows.Count() == 0)
                            {
                                dtUserRows = null;
                                continue;
                            }

                            strAlertMsg = dtRoleRow["AlertMessage"].ToString();

                            DataTable datTemp = FillCombos(new string[] { "MI.MaterialIssueNo,Convert(varchar(15),MI.MaterialIssueDate,106) as MaterialIssueDate,UM.UserName,WM.WarehouseName ", "" + 
                                    "InvMaterialIssueMaster MI INNER JOIN UserMaster UM On UM.UserID = MI.CreatedBy " +
                                    "INNER JOIN InvWarehouse WM ON WM.WarehouseID = MI.WareHouseID",""+
                                    "MI.MaterialIssueID=" + dtItemRow["MaterialIssueID"].ToInt64() });
                            if (datTemp != null)
                            {
                                if (datTemp.Rows.Count > 0)
                                {
                                    strAlertMsg = strAlertMsg.Replace("<MINO>", datTemp.Rows[0]["MaterialIssueNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<ISSUEDTE>", datTemp.Rows[0]["MaterialIssueDate"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<ISSUEDBY>", datTemp.Rows[0]["UserName"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<WAREHOUSE>", datTemp.Rows[0]["WarehouseName"].ToString());
                                }
                            }
                            #endregion

                            #region ReferenceID Setting

                            foreach (DataRow dtUserRow in dtUserRows)
                            {
                                if (Convert.ToInt32(dtUserRow["UserID"]) != ClsCommonSettings.UserID)
                                    continue;

                                dtNewAlertRow = datAlert.NewRow();
                                dtNewAlertRow["AlertID"] = 0;
                                dtNewAlertRow["StartDate"] = ClsCommonSettings.GetServerDate();
                                dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                                dtNewAlertRow["ReferenceID"] = dtItemRow["MaterialIssueID"];
                                dtNewAlertRow["AlertMessage"] = strAlertMsg;
                                dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                                datAlert.Rows.Add(dtNewAlertRow);

                                dtNewAlertRow = null;
                            }

                            #endregion

                            strAlertMsg = "";
                            strFilter = "";
                            dtUserRows = null;
                        }
                    }
                    ClearTable(datAlertRoleSetting);
                    ClearTable(datAlertUserSetting);

                    datAlert.AcceptChanges();
                }


                #region Unwanted Alert Deletion

                if (datAlert != null)
                {
                    UnwantedAlertDeletionAndSave(datAlert, lngAlertSettingID);
                }
                #endregion
                GetOpenedAlerts(dtmToDay);
                return datAlertMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetReceiptAlert()
        {
            long lngAlertSettingID = (long)AlertSettingsTypes.Receipt;
            DataTable datAlert = null;
            datAlertRoleSetting = null;
            datAlertUserSetting = null;
            dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strFilter;
            try
            {
                #region User & Role Selection

                if (!this.objclsDALAlertSetting.GetAlertSetting(lngAlertSettingID))
                    return (DataTable)null;// null;

                if (!HasUserPermission(lngAlertSettingID))
                    return (DataTable)null;

                if (!HasRole(lngAlertSettingID))
                    return (DataTable)null;

                #endregion

                #region Alert Fetching

                string strTName = "", Condition = "", IDField = "";

                strTName = "AccReceiptAndPaymentMaster";
                //Condition = "OperationTypeID=" + (int)OperationType.Receipt;
                Condition = "IsReceipt = 1 And OperationTypeID = " + (int)OperationType.SalesInvoice + "";
                IDField = "AccReceiptAndPaymentMaster.ReceiptAndPaymentID";

                DataTable datItems = new DataTable();
                if (strTName != "")
                    datItems = GetItems(IDField, Condition, strTName, lngAlertSettingID);

                #endregion

                if (datItems != null && datItems.Rows.Count > 0)
                {
                    datAlert = GetNewAlertTable();

                    foreach (DataRow dtItemRow in datItems.Rows)
                    {
                        foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                        {
                            #region Dynamic Alert Message Creation

                            strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                            strFilter += " AND IsAlertON = true";
                            dtUserRows = datAlertUserSetting.Select(strFilter);

                            if (dtUserRows.Count() == 0)
                            {
                                dtUserRows = null;
                                continue;
                            }

                            strAlertMsg = dtRoleRow["AlertMessage"].ToString();

                            DataTable datTemp = FillCombos(new string[] {"V.VendorName,A.PaymentDate,SO.SalesOrderNo,A.TotalAmount,A.PaymentNumber,SI.SalesInvoiceNo,"+
                                                                            "SO.NetAmount-(Select IsNull(Sum(D.Amount),0) From AccReceiptAndPaymentDetails D "+
				                                                            "Inner Join AccReceiptAndPaymentMaster M On M.ReceiptAndPaymentID = D.ReceiptAndPaymentID "+
				                                                            "Where (M.OperationTypeID = "+(int)OperationType.SalesOrder+" And M.ReceiptAndPaymentTypeID = "+(int)PaymentTypes.AdvancePayment+" And D.ReferenceID = SO.SalesOrderID) Or "+
					                                                        "(M.OperationTypeID = "+(int)OperationType.SalesInvoice+" And M.ReceiptAndPaymentTypeID = "+(int)PaymentTypes.InvoicePayment+" And D.ReferenceID = SI.SalesInvoiceID) ) AS BalanceAmt ",
                                                                            "AccReceiptAndPaymentMaster A "+
                                                                            "left JOIN AccReceiptAndPaymentDetails AD ON A.ReceiptAndPaymentID=AD.ReceiptAndPaymentID  "+
                                                                            "left JOIN InvSalesInvoiceMaster SI ON AD.ReferenceID = SI.SalesInvoiceID  "+
                                                                            "LEFT JOIN InvSalesInvoiceReferenceDetails SIR ON SIR.SalesInvoiceID=SI.SalesInvoiceID "+
                                                                            "left JOIN InvSalesOrderMaster SO ON SIR.ReferenceID = SO.SalesOrderID "+
                                                                            "LEFT JOIN InvSalesOrderReferenceDetails SOR ON SOR.SalesOrderID=SO.SalesOrderID "+
                                                                            "LEFT JOIN InvSalesQuotationMaster SQ ON SQ.SalesQuotationID=SOR.ReferenceID  "+
                                                                            "left JOIN InvVendorInformation V ON A.VendorID=V.VendorID ",
                                                                            "A.ReceiptAndPaymentID=" + dtItemRow["ReceiptAndPaymentID"].ToInt64() });
                            if (datTemp != null)
                            {
                                if (datTemp.Rows.Count > 0)
                                {
                                    //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<RCPTDTE>", (Convert.ToDateTime(datTemp.Rows[0]["PaymentDate"].ToString())).ToString("dd-MMM-yyyy")); ;
                                    strAlertMsg = strAlertMsg.Replace("<SONO>", datTemp.Rows[0]["SalesOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<AMOUNT>", datTemp.Rows[0]["TotalAmount"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<RNO>", datTemp.Rows[0]["PaymentNumber"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<BALAMOUNT>", datTemp.Rows[0]["BalanceAmt"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<SINO>", datTemp.Rows[0]["SalesInvoiceNo"].ToString());
                                }
                            }
                            #endregion

                            #region ReferenceID Setting

                            foreach (DataRow dtUserRow in dtUserRows)
                            {
                                if (Convert.ToInt32(dtUserRow["UserID"]) != ClsCommonSettings.UserID)
                                    continue;

                                dtNewAlertRow = datAlert.NewRow();
                                dtNewAlertRow["AlertID"] = 0;
                                dtNewAlertRow["StartDate"] = ClsCommonSettings.GetServerDate();
                                dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                                dtNewAlertRow["ReferenceID"] = dtItemRow["ReceiptAndPaymentID"];
                                dtNewAlertRow["AlertMessage"] = strAlertMsg;
                                dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                                datAlert.Rows.Add(dtNewAlertRow);

                                dtNewAlertRow = null;
                            }

                            #endregion

                            strAlertMsg = "";
                            strFilter = "";
                            dtUserRows = null;
                        }
                    }
                    ClearTable(datAlertRoleSetting);
                    ClearTable(datAlertUserSetting);

                    datAlert.AcceptChanges();
                }


                #region Unwanted Alert Deletion

                if (datAlert != null)
                {
                    UnwantedAlertDeletionAndSave(datAlert, lngAlertSettingID);
                }
                #endregion
                GetOpenedAlerts(dtmToDay);
                return datAlertMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetDeliveryNotetAlert()
        {
            long lngAlertSettingID = (long)AlertSettingsTypes.DeliveryNote;
            DataTable datAlert = null;
            datAlertRoleSetting = null;
            datAlertUserSetting = null;
            dtUserRows = null;
            DataRow dtNewAlertRow;

            string strAlertMsg, strFilter;
            try
            {
                #region User & Role Selection

                if (!this.objclsDALAlertSetting.GetAlertSetting(lngAlertSettingID))
                    return (DataTable)null;// null;

                if (!HasUserPermission(lngAlertSettingID))
                    return (DataTable)null;

                if (!HasRole(lngAlertSettingID))
                    return (DataTable)null;

                #endregion

                #region Alert Fetching

                string strTName = "", Condition = "", IDField = "";

                strTName = "InvItemIssueMaster";
                Condition = " OrderTypeID = " + (int)OperationOrderType.DNOTSalesInvoice;
                IDField = "InvItemIssueMaster.ItemIssueID";

                DataTable datItems = new DataTable();
                if (strTName != "")
                    datItems = GetItems(IDField, Condition, strTName, lngAlertSettingID);

                #endregion

                if (datItems != null && datItems.Rows.Count > 0)
                {
                    datAlert = GetNewAlertTable();

                    foreach (DataRow dtItemRow in datItems.Rows)
                    {
                        foreach (DataRow dtRoleRow in datAlertRoleSetting.Rows)
                        {
                            #region Dynamic Alert Message Creation

                            strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
                            strFilter += " AND IsAlertON = true";
                            dtUserRows = datAlertUserSetting.Select(strFilter);

                            if (dtUserRows.Count() == 0)
                            {
                                dtUserRows = null;
                                continue;
                            }

                            strAlertMsg = dtRoleRow["AlertMessage"].ToString();

                            DataTable datTemp = FillCombos(new string[] { "V.VendorName,IM.IssuedDate,SI.SalesInvoiceNo,UM.UserName AS IssuedBy,IM.ItemIssueNo ", "" + 
                                    " InvItemIssueMaster IM INNER JOIN InvItemIssueReferenceDetails IIRD ON IM.ItemIssueID = IIRD.ItemIssueID INNER JOIN UserMaster UM ON UM.UserID = IM.IssuedBy LEFT JOIN EmployeeMaster E ON IM.OrderTypeID=" + (int)OperationOrderType.DNOTSalesInvoice + " AND UM.UserID = E.EmployeeID " +
                                    " INNER JOIN InvSalesInvoiceMaster SI ON IIRD.ReferenceID = SI.SalesInvoiceID LEFT JOIN InvSalesInvoiceReferenceDetails SIR ON SIR.SalesInvoiceID=SI.SalesInvoiceID LEFT JOIN  InvSalesOrderMaster SO ON SIR.ReferenceID = SO.SalesOrderID " +
                                    " LEFT JOIN InvSalesOrderReferenceDetails SOR ON SOR.SalesOrderID=SO.SalesOrderID LEFT JOIN InvSalesQuotationMaster SQ ON SOR.ReferenceID = SQ.SalesQuotationID " +
                                    " INNER JOIN InvVendorInformation V ON SI.VendorID=V.VendorID ", "" +
                                    " IM.ItemIssueID=" + dtItemRow["ItemIssueID"].ToInt64() });
                            if (datTemp != null)
                            {
                                if (datTemp.Rows.Count > 0)
                                {
                                    //strAlertMsg = strAlertMsg.Replace("<JBNO>", datTemp.Rows[0]["JobOrderNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<CUSNAME>", datTemp.Rows[0]["VendorName"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<ISSUEDTE>", (Convert.ToDateTime(datTemp.Rows[0]["IssuedDate"].ToString())).ToString("dd-MMM-yyyy")); ;
                                    strAlertMsg = strAlertMsg.Replace("<SINO>", datTemp.Rows[0]["SalesInvoiceNo"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<ISSUEDBY>", datTemp.Rows[0]["IssuedBy"].ToString());
                                    strAlertMsg = strAlertMsg.Replace("<ISSUENO>", datTemp.Rows[0]["ItemIssueNo"].ToString());
                                }
                            }
                            #endregion

                            #region ReferenceID Setting

                            foreach (DataRow dtUserRow in dtUserRows)
                            {
                                if (Convert.ToInt32(dtUserRow["UserID"]) != ClsCommonSettings.UserID)
                                    continue;

                                dtNewAlertRow = datAlert.NewRow();
                                dtNewAlertRow["AlertID"] = 0;
                                dtNewAlertRow["StartDate"] = ClsCommonSettings.GetServerDate();
                                dtNewAlertRow["AlertUserSettingID"] = dtUserRow["AlertUserSettingID"];
                                dtNewAlertRow["ReferenceID"] = dtItemRow["ItemIssueID"];
                                dtNewAlertRow["AlertMessage"] = strAlertMsg;
                                dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
                                datAlert.Rows.Add(dtNewAlertRow);

                                dtNewAlertRow = null;
                            }

                            #endregion

                            strAlertMsg = "";
                            strFilter = "";
                            dtUserRows = null;
                        }
                    }
                    ClearTable(datAlertRoleSetting);
                    ClearTable(datAlertUserSetting);

                    datAlert.AcceptChanges();
                }


                #region Unwanted Alert Deletion

                if (datAlert != null)
                {
                    UnwantedAlertDeletionAndSave(datAlert, lngAlertSettingID);
                }
                #endregion
                GetOpenedAlerts(dtmToDay);
                return datAlertMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void GetOpenedAlerts(DateTime dtmDate)
        {
            DataTable datAlert = null;

            try
            {
                datAlert = this.objclsDALAlerts.GetUserDayAlerts(ClsCommonSettings.UserID, dtmDate);
                if (datAlert == null || datAlert.Rows.Count == 0) return;

                this.CheckPurchaseOrderStatus(datAlert);
                this.CheckRFQStatus(datAlert);
                this.CheckPurchaseQuotationStatus(datAlert);
                this.CheckPurchaseOrderOpStatus(datAlert);
                this.CheckSalesQuotationStatus(datAlert);
                this.CheckSalesOrderStatus(datAlert);
                this.CheckSalesInvoiceStatus(datAlert);
                this.CheckAllAlerts(datAlert);

                if (queAlertID != null && queAlertID.Count > 0)
                {
                    this.objclsDALAlerts.CloseAlert(this.queAlertID);
                    this.queAlertID.Clear();
                }

                this.datAlertMsg = datAlert;
                datAlert = null;
            }
            catch (Exception ex) { throw ex; }
            finally
            {
            }
        }
        private DataTable GetItems(string IDField, string Condition, string strTName, long lngAlertSettingID)
        {
            if (Condition.Length > 0)
                Condition = Condition + " AND " + IDField + " NOT IN (SELECT ReferenceID FROM Alerts A INNER JOIN AlertUserSettings U" +
                                        " ON A.AlertUserSettingID=U.AlertUserSettingID WHERE U.AlertSettingID=" + lngAlertSettingID + " AND U.UserID=" + ClsCommonSettings.UserID.ToString() + ")";
            else if (Condition.Length == 0)
                Condition = IDField + " NOT IN (SELECT ReferenceID FROM Alerts A INNER JOIN AlertUserSettings U" +
                                        " ON A.AlertUserSettingID=U.AlertUserSettingID WHERE U.AlertSettingID=" + lngAlertSettingID + " AND U.UserID=" + ClsCommonSettings.UserID.ToString() + ")";

            return this.objclsDALAlerts.GetUserAlertEntries(strTName, Condition);
        }

        private void UnwantedAlertDeletionAndSave(DataTable datAlert, long lngAlertSettingID)
        {
            string strTName1 = "Alerts INNER JOIN AlertUserSettings ON Alerts.AlertUserSettingID = AlertUserSettings.AlertUserSettingID  " +
                        "AND (Alerts.StatusID= " + (int)AlertStatus.Open + " OR Alerts.StatusID= " + (int)AlertStatus.Read + " OR Alerts.StatusID= " + (int)AlertStatus.Blocked + ")";
            //string strTName1 = "Alerts INNER JOIN AlertUserSettings ON Alerts.AlertUserSettingID = AlertUserSettings.AlertUserSettingID  ";

            string Condition1 = "AlertUserSettings.UserID=" + ClsCommonSettings.UserID.ToString() + " AND AlertUserSettings.AlertSettingID=" + lngAlertSettingID;
            DataTable datTemp = this.objclsDALAlerts.GetUserAlertEntries(strTName1, Condition1);
            if (datTemp != null)
            {
                //if (datTemp.Rows.Count > datAlert.Rows.Count)
                //{
                //    for (int k = datTemp.Rows.Count - 1; k >= 0; k--)
                //    {
                //        for (int l = datAlert.Rows.Count - 1; l >= 0; l--)
                //        {
                //            try
                //            {
                //                if ((datTemp.Rows[k]["AlertUserSettingID"].ToString() != datAlert.Rows[l]["AlertUserSettingID"].ToString())
                //                       || (datTemp.Rows[k]["ReferenceID"].ToString() != datAlert.Rows[l]["ReferenceID"].ToString())
                //                       || (datTemp.Rows[k]["AlertMessage"].ToString() != datAlert.Rows[l]["AlertMessage"].ToString()))
                //                {
                //                    objclsDALAlerts.DeleteAlertForUser(Convert.ToInt32(datTemp.Rows[k]["AlertID"].ToString()));
                //                }
                //            }
                //            catch { }
                //        }
                //    }
                //}

                for (int j = datTemp.Rows.Count - 1; j >= 0; j--)
                {
                    int i = datAlert.Rows.Count - 1;

                    while (i >= 0)
                    {
                        if ((datTemp.Rows[j]["AlertUserSettingID"].ToString() == datAlert.Rows[i]["AlertUserSettingID"].ToString())
                               && (datTemp.Rows[j]["ReferenceID"].ToString() == datAlert.Rows[i]["ReferenceID"].ToString())
                               && (datTemp.Rows[j]["AlertMessage"].ToString() == datAlert.Rows[i]["AlertMessage"].ToString()))
                        {
                            datAlert.Rows.RemoveAt(i);
                            i--;
                            break;
                        }
                        else
                        {
                            //    objclsDALAlerts.DeleteAlertForUser(Convert.ToInt32(datTemp.Rows[j]["AlertID"].ToString()));
                            //    i--;
                            break;
                        }
                    }
                }
            }
            this.SaveUserAlert(datAlert);
            //this.getDate();
            //GetOpenedAlerts(dtmToDay);
        }
        private DataTable GetNewAlertTable()
        {
            DataTable datAlert = new DataTable();
            datAlert.Columns.Add("AlertID", typeof(long));
            datAlert.Columns.Add("StartDate", typeof(string));
            datAlert.Columns.Add("AlertUserSettingID", typeof(string));
            datAlert.Columns.Add("ReferenceID", typeof(long));
            datAlert.Columns.Add("AlertMessage", typeof(string));
            datAlert.Columns.Add("Status", typeof(int));

            return datAlert;
        }

        private string GetItemCondition(string Condition, string IDField, long lngAlertSettingID)
        {
            if (Condition.Length > 0)
                Condition = Condition + " AND " + IDField + " NOT IN (SELECT ReferenceID FROM Alerts A INNER JOIN AlertUserSettings U" +
                                        " ON A.AlertUserSettingID=U.AlertUserSettingID WHERE U.AlertSettingID=" + lngAlertSettingID + " AND U.UserID=" + ClsCommonSettings.UserID.ToString() + ")";
            else if (Condition.Length == 0)
                Condition = IDField + " NOT IN (SELECT ReferenceID FROM Alerts A INNER JOIN AlertUserSettings U" +
                                        " ON A.AlertUserSettingID=U.AlertUserSettingID WHERE U.AlertSettingID=" + lngAlertSettingID + " AND U.UserID=" + ClsCommonSettings.UserID.ToString() + ")";
            return Condition;
        }

        private bool SaveUserAlert(DataTable datItemAlert)
        {
            bool blnReturnVal = false;
            if (datItemAlert != null)
            {
                try
                {
                    this.objClsConnection.BeginTransaction();

                    if (datItemAlert != null && datItemAlert.Rows.Count > 0)
                        this.objclsDALAlerts.SaveAlert(datItemAlert);
                    this.objClsConnection.CommitTransaction();
                    blnReturnVal = true;
                }
                catch (Exception Ex)
                {
                    this.objClsConnection.RollbackTransaction();
                    throw Ex;
                }
                finally
                {

                }
            }
            return blnReturnVal;
        }
        private void CheckPurchaseOrderStatus(DataTable dtAlerts)
        {
            DataRow[] dtRows;
            int iStatus;
            bool blnIsRepeat = true;

            try
            {
                dtRows = dtAlerts.Select("OperationTypeID = " + Convert.ToInt32(OperationType.PurchaseOrder).ToString());
                if (dtRows == null || dtRows.Count() == 0)
                    return;

                foreach (DataRow dtOrderRow in dtRows)
                {
                    DataTable datTemp = FillCombos(new string[] { "ISNULL(StatusID,0) AS StatusID,DueDate", "InvPurchaseOrderMaster", "PurchaseOrderID=" + Convert.ToInt64(dtOrderRow["ReferenceID"]) });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            iStatus = Convert.ToInt32(datTemp.Rows[0]["StatusID"].ToString());//this.objclsDALAlerts.GetPurchaseOrderStatus(Convert.ToInt64(dtOrderRow["ReferenceID"]));
                            if (iStatus == 0 || ((OperationStatusType)iStatus) == OperationStatusType.POrderClosed)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString()).AddDays(Convert.ToDouble(dtOrderRow["ValidPeriod"])))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else
                            {
                                blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                                if (!blnIsRepeat)
                                {
                                    this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                }
                                blnIsRepeat = true;
                            }
                        }
                        else
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                            dtAlerts.Rows.Remove(dtOrderRow);
                        }
                    }
                    else
                    {
                        this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        dtAlerts.Rows.Remove(dtOrderRow);
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private void CheckRFQStatus(DataTable dtAlerts)
        {
            DataRow[] dtRows;
            int iStatus;
            bool blnIsRepeat = true;

            try
            {
                dtRows = dtAlerts.Select("OperationTypeID = " + Convert.ToInt32(OperationType.RFQ).ToString());
                if (dtRows == null || dtRows.Count() == 0)
                    return;

                foreach (DataRow dtOrderRow in dtRows)
                {
                    DataTable datTemp = FillCombos(new string[] { "ISNULL(StatusID,0) AS StatusID,DueDate", "InvRFQMaster", "RFQID=" + Convert.ToInt64(dtOrderRow["ReferenceID"]) });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            iStatus = Convert.ToInt32(datTemp.Rows[0]["StatusID"].ToString());
                            if (iStatus == 0 || ((RFQStatus)iStatus) == RFQStatus.Closed)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString()).AddDays(Convert.ToDouble(dtOrderRow["ValidPeriod"])))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else
                            {
                                blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                                if (!blnIsRepeat)
                                {
                                    this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                }
                                blnIsRepeat = true;
                            }
                        }
                        else
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                            dtAlerts.Rows.Remove(dtOrderRow);
                        }
                    }
                    else
                    {
                        this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        dtAlerts.Rows.Remove(dtOrderRow);
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private void CheckPurchaseQuotationStatus(DataTable dtAlerts)
        {
            DataRow[] dtRows;
            int iStatus;
            bool blnIsRepeat = true;

            try
            {
                dtRows = dtAlerts.Select("OperationTypeID = " + Convert.ToInt32(OperationType.PurchaseQuotation).ToString());
                if (dtRows == null || dtRows.Count() == 0)
                    return;

                foreach (DataRow dtOrderRow in dtRows)
                {
                    DataTable datTemp = FillCombos(new string[] { "ISNULL(StatusID,0) AS StatusID,DueDate", "InvPurchaseQuotationMaster", "PurchaseQuotationID=" + Convert.ToInt64(dtOrderRow["ReferenceID"]) });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            iStatus = Convert.ToInt32(datTemp.Rows[0]["StatusID"].ToString());
                            if (iStatus == 0 || ((OperationStatusType)iStatus) == OperationStatusType.PQuotationClosed)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString()).AddDays(Convert.ToDouble(dtOrderRow["ValidPeriod"])))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else
                            {
                                blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                                if (!blnIsRepeat)
                                {
                                    this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                }
                                blnIsRepeat = true;
                            }
                        }
                        else
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                            dtAlerts.Rows.Remove(dtOrderRow);
                        }
                    }
                    else
                    {
                        this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        dtAlerts.Rows.Remove(dtOrderRow);
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private void CheckPurchaseOrderOpStatus(DataTable dtAlerts)
        {
            DataRow[] dtRows;
            int iStatus;
            bool blnIsRepeat = true;

            try
            {
                dtRows = dtAlerts.Select("OperationTypeID = " + Convert.ToInt32(OperationType.PurchaseOrder).ToString());
                if (dtRows == null || dtRows.Count() == 0)
                    return;

                foreach (DataRow dtOrderRow in dtRows)
                {
                    DataTable datTemp = FillCombos(new string[] { "ISNULL(StatusID,0) AS StatusID,DueDate", "InvPurchaseOrderMaster", "PurchaseOrderID=" + Convert.ToInt64(dtOrderRow["ReferenceID"]) });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            iStatus = Convert.ToInt32(datTemp.Rows[0]["StatusID"].ToString());
                            if (iStatus == 0 || ((OperationStatusType)iStatus) == OperationStatusType.POrderClosed)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString()).AddDays(Convert.ToDouble(dtOrderRow["ValidPeriod"])))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else
                            {
                                blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                                if (!blnIsRepeat)
                                {
                                    this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                }
                                blnIsRepeat = true;
                            }
                        }
                        else
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                            dtAlerts.Rows.Remove(dtOrderRow);
                        }
                    }
                    else
                    {
                        this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        dtAlerts.Rows.Remove(dtOrderRow);
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private void CheckSalesQuotationStatus(DataTable dtAlerts)
        {
            DataRow[] dtRows;
            int iStatus;
            bool blnIsRepeat = true;

            try
            {
                dtRows = dtAlerts.Select("OperationTypeID = " + Convert.ToInt32(OperationType.SalesQuotation).ToString());
                if (dtRows == null || dtRows.Count() == 0)
                    return;

                foreach (DataRow dtOrderRow in dtRows)
                {
                    DataTable datTemp = FillCombos(new string[] { "ISNULL(StatusID,0) AS StatusID,DueDate", "InvSalesQuotationMaster", "SalesQuotationID=" + Convert.ToInt64(dtOrderRow["ReferenceID"]) });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            iStatus = Convert.ToInt32(datTemp.Rows[0]["StatusID"].ToString());
                            if (iStatus == 0 || ((OperationStatusType)iStatus) == OperationStatusType.SQuotationClosed)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString()).AddDays(Convert.ToDouble(dtOrderRow["ValidPeriod"])))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else
                            {
                                blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                                if (!blnIsRepeat)
                                {
                                    this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                }
                                blnIsRepeat = true;
                            }
                        }
                        else
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                            dtAlerts.Rows.Remove(dtOrderRow);
                        }
                    }
                    else
                    {
                        this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        dtAlerts.Rows.Remove(dtOrderRow);
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private void CheckSalesOrderStatus(DataTable dtAlerts)
        {
            DataRow[] dtRows;
            int iStatus;
            bool blnIsRepeat = true;

            try
            {
                dtRows = dtAlerts.Select("OperationTypeID = " + Convert.ToInt32(OperationType.SalesOrder).ToString());
                if (dtRows == null || dtRows.Count() == 0)
                    return;

                foreach (DataRow dtOrderRow in dtRows)
                {
                    long lngtemp = Convert.ToInt64(dtOrderRow["ReferenceID"]);
                    DataTable datTemp = FillCombos(new string[] { "ISNULL(StatusID,0) AS StatusID,DueDate", "InvSalesOrderMaster", "SalesOrderID=" + Convert.ToInt64(dtOrderRow["ReferenceID"]) });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            iStatus = Convert.ToInt32(datTemp.Rows[0]["StatusID"].ToString());
                            if (iStatus == 0 || ((OperationStatusType)iStatus) == OperationStatusType.SOrderClosed)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString()).AddDays(Convert.ToDouble(dtOrderRow["ValidPeriod"])))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (Convert.ToInt32(dtOrderRow["AlertSettingID"]) == (int)AlertSettingsTypes.SaleOrderApproved &&
                                (Convert.ToInt32(iStatus) == (int)OperationStatusType.SOrderRejected))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (Convert.ToInt32(dtOrderRow["AlertSettingID"]) == (int)AlertSettingsTypes.SaleOrderRejected &&
                                Convert.ToInt32(iStatus) == (int)OperationStatusType.SOrderApproved)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else
                            {
                                blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                                if (!blnIsRepeat)
                                {
                                    this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                }
                                blnIsRepeat = true;
                            }
                        }
                        else
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                            dtAlerts.Rows.Remove(dtOrderRow);
                        }
                    }
                    else
                    {
                        this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        dtAlerts.Rows.Remove(dtOrderRow);
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private void CheckSalesInvoiceStatus(DataTable dtAlerts)
        {
            DataRow[] dtRows;
            int iStatus;
            bool blnIsRepeat = true;

            try
            {
                dtRows = dtAlerts.Select("OperationTypeID = " + Convert.ToInt32(OperationType.SalesInvoice).ToString());
                if (dtRows == null || dtRows.Count() == 0)
                    return;

                foreach (DataRow dtOrderRow in dtRows)
                {
                    DataTable datTemp = FillCombos(new string[] { "ISNULL(StatusID,0) AS StatusID,DueDate", "InvSalesInvoiceMaster", "SalesInvoiceID=" + Convert.ToInt64(dtOrderRow["ReferenceID"]) });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            iStatus = Convert.ToInt32(datTemp.Rows[0]["StatusID"].ToString());
                            if (iStatus == 0 || ((OperationStatusType)iStatus) == OperationStatusType.SInvoiceClosed)
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(datTemp.Rows[0]["DueDate"].ToString()).AddDays(Convert.ToDouble(dtOrderRow["ValidPeriod"])))
                            {
                                this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                dtAlerts.Rows.Remove(dtOrderRow);
                            }
                            else
                            {
                                blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                                if (!blnIsRepeat)
                                {
                                    this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                                }
                                blnIsRepeat = true;
                            }
                        }
                        else
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                            dtAlerts.Rows.Remove(dtOrderRow);
                        }
                    }
                    else
                    {
                        this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        dtAlerts.Rows.Remove(dtOrderRow);
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private void CheckAllAlerts(DataTable dtAlerts)
        {
            bool blnIsRepeat = true;

            try
            {
                foreach (DataRow dtOrderRow in dtAlerts.Rows)
                {
                    if (!this.queAlertID.Contains(Convert.ToInt64(dtOrderRow["AlertID"])))
                    {
                        blnIsRepeat = this.objclsDALAlerts.GetAlertUserSettingRepeat(Convert.ToInt64(dtOrderRow["AlertUserSettingID"]));
                        if (!blnIsRepeat)
                        {
                            this.queAlertID.Enqueue(Convert.ToInt64(dtOrderRow["AlertID"]));
                        }
                        blnIsRepeat = true;
                    }
                }
                dtAlerts.AcceptChanges();
            }
            catch (Exception ex)
            {
                dtAlerts.RejectChanges();
                throw ex;
            }
        }

        private bool CloseAlerts(Queue<long> queAlertID, int iOperationID)
        {
            bool blnReturnVal = false;

            try
            {
                this.objClsConnection.BeginTransaction();

                switch ((OperationType)iOperationID)
                {
                    case OperationType.PurchaseOrder:
                        this.objclsDALAlerts.CloseAlert(queAlertID);
                        break;

                    default:
                        break;
                }
                this.objClsConnection.CommitTransaction();
                blnReturnVal = true;
            }
            catch (Exception Ex)
            {
                this.objClsConnection.RollbackTransaction();
                throw Ex;
            }
            finally
            {

            }
            return blnReturnVal;
        }

        /// <summary>
        /// Delete Alerts for a particular ReferenceID and AlertID
        /// </summary>
        /// <param name="ReferenceID"></param>
        /// <param name="AlertTypeID">Alert Type Enum in ClsCommonItems</param>
        /// <returns></returns>
        public void DeleteAlert(int ReferenceID, int AlertTypeID)
        {
            bool blnResult = this.objclsDALAlerts.DeleteAlert(ReferenceID, AlertTypeID);
        }

    }
}
