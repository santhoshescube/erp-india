﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Collections;

namespace MyBooksERP
{
    public class clsBLLAlertRoleSettings
    {
        private clsDALAlertRoleSettings objclsDALAlertRoleSettings;
        private clsDTOAlertRoleSettings objclsDTOAlertRoleSettings;

        private clsDALAlertUserSettings objclsDALAlertUserSettings;
        private Queue<clsDTOAlertUserSettings> queclsDTOAlertUserSettings;

        private DataLayer objClsConnection;

        public clsBLLAlertRoleSettings()
        {
            this.objClsConnection = new DataLayer();
            this.objclsDALAlertRoleSettings = new clsDALAlertRoleSettings();
            this.objclsDTOAlertRoleSettings = new clsDTOAlertRoleSettings();
            this.objclsDALAlertRoleSettings.objclsDTOAlertRoleSettings = this.objclsDTOAlertRoleSettings;

            this.objclsDALAlertUserSettings = new clsDALAlertUserSettings();
            this.queclsDTOAlertUserSettings = new Queue<clsDTOAlertUserSettings>();
            this.objclsDALAlertUserSettings.queclsDTOAlertUserSettings = this.queclsDTOAlertUserSettings;
        }

        ~clsBLLAlertRoleSettings()
        {
            this.objClsConnection = null;
            this.objclsDALAlertRoleSettings = null;
        }

        public clsDTOAlertRoleSettings DTOAlertRoleSettings
        {
            get { return this.objclsDTOAlertRoleSettings; }
            set { this.objclsDTOAlertRoleSettings = value; }
        }

        public Queue<clsDTOAlertUserSettings> DTOAlertUserSettings
        {
            get { return this.queclsDTOAlertUserSettings; }
            set { this.queclsDTOAlertUserSettings = value; }
        }

        public DataTable FillCombos(string[] sarFieldValues)// filling combo box
        {
            try
            {
                this.objclsDALAlertRoleSettings.objClsConnection = this.objClsConnection;
                return this.objclsDALAlertRoleSettings.FillCombos(sarFieldValues);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool GetAlertRoleSetting(long lngAlertSettingID, int intRoleID)
        {
            try
            {
                this.objclsDALAlertRoleSettings.objClsConnection = this.objClsConnection;
                this.objclsDTOAlertRoleSettings = this.objclsDALAlertRoleSettings.objclsDTOAlertRoleSettings;
                return this.objclsDALAlertRoleSettings.GetAlertRoleSetting(lngAlertSettingID, intRoleID);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool GetAlertRoleSetting(long lngAlertRoleSettingID)
        {
            try
            {
                this.objclsDALAlertRoleSettings.objClsConnection = this.objClsConnection;
                this.objclsDTOAlertRoleSettings = this.objclsDALAlertRoleSettings.objclsDTOAlertRoleSettings;
                return this.objclsDALAlertRoleSettings.GetAlertRoleSetting(lngAlertRoleSettingID);
            }
            catch (Exception ex) { throw ex; }
        }

        public DataRow[] GetAlertSettingUsers(long lngAlertSettingID, int intRoleID)
        {
            DataTable datAlertUsers = null, datUsers = null;
            DataRow dtNewRow;

            try
            {
                this.objclsDALAlertUserSettings.objClsConnection = this.objClsConnection;
                datAlertUsers = this.objclsDALAlertUserSettings.GetAlertSettingUsers(lngAlertSettingID, intRoleID);
                datUsers = this.objclsDALAlertUserSettings.GetUsers(lngAlertSettingID, intRoleID);

                if (datAlertUsers == null)
                    return datUsers.Select("0=0", "UserName");

                if (datUsers != null && datUsers.Rows.Count > 0)
                {
                    foreach (DataRow dtRow in datUsers.Rows)
                    {
                        dtNewRow = datAlertUsers.NewRow();
                        dtNewRow["UserID"] = dtRow["UserID"];
                        dtNewRow["RoleID"] = dtRow["RoleID"];
                        dtNewRow["UserName"] = dtRow["UserName"];
                        dtNewRow["AlertUserSettingID"] = 0;
                        dtNewRow["AlertSettingID"] = 0;
                        dtNewRow["ProcessingInterval"] = 0;
                        dtNewRow["RepeatInterval"] = 0;                        
                        dtNewRow["IsRepeat"] = false;
                        dtNewRow["ValidPeriod"] = 0;
                        dtNewRow["IsAlertON"] = false;
                        dtNewRow["Checked"] = 0;
                        datAlertUsers.Rows.Add(dtNewRow);
                        dtNewRow = null;
                    }
                }
                return datAlertUsers.Select("0=0", "UserName");
            }
            catch (Exception ex) { throw ex; }
        }

        public bool SaveAlertUserSettings()// insert
        {
            bool blnReturnVal = false;
            try
            {
                this.objClsConnection.BeginTransaction();
                this.objclsDALAlertRoleSettings.objClsConnection = objClsConnection;
                this.objclsDALAlertUserSettings.objClsConnection = objClsConnection;
                this.objclsDALAlertRoleSettings.SaveAlertRoleSettings();
                this.objclsDALAlertUserSettings.SaveAlertUserSettings();
                this.objClsConnection.CommitTransaction();
                blnReturnVal = true;
            }
            catch (Exception Ex)
            {
                this.objClsConnection.RollbackTransaction();
                throw Ex;
            }
            finally
            {
                this.queclsDTOAlertUserSettings.Clear();
            }
            return blnReturnVal;
        }

        public bool CheckDuplicate(long lngAlertSettingID, int iRoleID,out int iCount )
        {
            try
            {
                this.objclsDALAlertRoleSettings.objClsConnection = this.objClsConnection;

                return this.objclsDALAlertRoleSettings.CheckDuplicate(lngAlertSettingID, iRoleID, out iCount);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool DeleteAlertRoleSetting(long lngAlertSetingID, int intRoleID)
        {
            /* For Deleteing alert (delete and save) */

               try
            { this.objclsDALAlertRoleSettings.objClsConnection = this.objClsConnection;

                return this.objclsDALAlertUserSettings.DeleteAlertRoleSetting(lngAlertSetingID, intRoleID);
            }
               catch (Exception ex) { throw ex; }
        }
    }
}