﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


/* 
=================================================
Author:		<Author,Midhun>
Create date: <Create Date,13 Sep 2011>
Description:	<Description,BLL for MandatoryDocumentTypes>
================================================
*/
namespace MyBooksERP
{
   public  class clsBLLMandatoryDocumentTypes : IDisposable
    {
       clsDTOMandatoryDocumentTypes objclsDTOMandatoryDocumentTypes;
       clsDALMandatoryDocumentTypes objclsDALMandatoryDocumentTypes;
       private DataLayer objclsconnection;


       public clsBLLMandatoryDocumentTypes()
       {
           objclsconnection = new DataLayer();
           objclsDTOMandatoryDocumentTypes = new clsDTOMandatoryDocumentTypes();
           objclsDALMandatoryDocumentTypes = new clsDALMandatoryDocumentTypes();
           objclsDALMandatoryDocumentTypes.MobjclsDTOMandatoryDocumentTypes = objclsDTOMandatoryDocumentTypes;
       }

       public clsDTOMandatoryDocumentTypes clsDTOMandatoryDocumentTypes
       {
           get { return objclsDTOMandatoryDocumentTypes; }
           set { objclsDTOMandatoryDocumentTypes = value; }
       }

       public bool InsertMandatoryDocumentTypes()
       {
           bool blnRetValue = false;
           try
           {
               objclsconnection.BeginTransaction();
               objclsDALMandatoryDocumentTypes.MobjDataLayer = objclsconnection;
               blnRetValue = objclsDALMandatoryDocumentTypes.InsertMandatoryDocumentTypes();
               objclsconnection.CommitTransaction();
               blnRetValue = true;
           }
           catch (Exception ex)
           {
               objclsconnection.RollbackTransaction();
               throw ex;

           }
           return blnRetValue;

       }
       public string ValidationInsertMandatoryDocumentTypes()
       {
           string strDoc="";
           try
           {
               objclsconnection.BeginTransaction();
               objclsDALMandatoryDocumentTypes.MobjDataLayer = objclsconnection;
               strDoc= objclsDALMandatoryDocumentTypes.ValidationInsertMandatoryDocumentTypes();
               objclsconnection.CommitTransaction();
           }
           catch (Exception ex)
           {
               objclsconnection.RollbackTransaction();
               throw ex;

           }
           return strDoc;

       }


       public bool DeleteMandatoryDocumentTypes()
       {
           bool blnRetValue = false;
           try
           {
               objclsconnection.BeginTransaction();
               objclsDALMandatoryDocumentTypes.MobjDataLayer = objclsconnection;
               blnRetValue = objclsDALMandatoryDocumentTypes.DeleteMandatoryDocumentTypes();
               objclsconnection.CommitTransaction();
               blnRetValue = true;
           }
           catch (Exception ex)
           {
               objclsconnection.RollbackTransaction();
               throw ex;
           }
           return blnRetValue;
       }

       public DataTable FillCombos(string[] saFieldValues)
       {
           objclsDALMandatoryDocumentTypes.MobjDataLayer = objclsconnection;
           return objclsDALMandatoryDocumentTypes.FillCombos(saFieldValues);
       }

       public DataTable GetMandatoryDocumentTypes(int intOperationTypeID, int intCompanyID)
       {
           objclsDALMandatoryDocumentTypes.MobjDataLayer = objclsconnection;
           return objclsDALMandatoryDocumentTypes.GetMandatoryDocumentTypes(intOperationTypeID,intCompanyID);
       }

       #region IDisposable Members

       void IDisposable.Dispose()
       {
           if (objclsDALMandatoryDocumentTypes != null)
               objclsDALMandatoryDocumentTypes = null;

           if (objclsDTOMandatoryDocumentTypes != null)
               objclsDTOMandatoryDocumentTypes = null;
           
       }

       #endregion



       


    }
}
