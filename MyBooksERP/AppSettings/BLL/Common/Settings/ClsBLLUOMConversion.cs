﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;



namespace MyBooksERP
{
    public class ClsBLLUOMConversion
    {
        clsDALUomConversion objDALUomConversion;
        clsDTOUOMConversion objDTOUOMConversion;
        private DataLayer objConnection;

        public ClsBLLUOMConversion()
        {
            objDALUomConversion = new clsDALUomConversion();
            objDTOUOMConversion = new clsDTOUOMConversion();
            objConnection = new DataLayer();
            objDALUomConversion.objDTOUOMConversion = objDTOUOMConversion;
        }

        public clsDTOUOMConversion clsDTOUOMConversion
        {
            get { return objDTOUOMConversion; }
            set { objDTOUOMConversion = value; }
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            objDALUomConversion.objConnection = objConnection;
            return objDALUomConversion.FillCombos(sarFieldValues);
        }

        public bool CheckDuplication(string[] sarFieldValues)
        {
            objDALUomConversion.objConnection = objConnection;
            return objDALUomConversion.CheckDuplication(sarFieldValues);
        }

        public bool SaveUOM(bool AddStatus)
        {
            bool blnRetValue = false;
            try
            {
                objConnection.BeginTransaction();
                objDALUomConversion.objConnection = objConnection;
                blnRetValue = objDALUomConversion.SaveUOM(AddStatus);
                objConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable GetUOMDetails()
        {
            objDALUomConversion.objConnection = objConnection;
            return objDALUomConversion.GetUOMDetails();
        }

        public DataTable GetUOMDetails(int UomBaseClassID)
        {
            objDALUomConversion.objConnection = objConnection;
            return objDALUomConversion.GetUOMDetails(UomBaseClassID);
        }

        public DataTable GetUOMDetails(int UomBaseClassID, int UomBaseID)
        {
            objDALUomConversion.objConnection = objConnection;
            return objDALUomConversion.GetUOMDetails(UomBaseClassID, UomBaseID);
        }

        public bool DisplayUOM(int UOMConversionID)
        {
            objDALUomConversion.objConnection = objConnection;
            objDTOUOMConversion = objDALUomConversion.objDTOUOMConversion;
            return objDALUomConversion.DisplayUOM(UOMConversionID);
        }

        public bool DeleteUOM(int UOMConversionID)
        {
            bool blnRetValue = false;
            try
            {
                objConnection.BeginTransaction();
                objDALUomConversion.objConnection = objConnection;
                blnRetValue = objDALUomConversion.DeleteUOM(UOMConversionID);
                objConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objConnection.RollbackTransaction();
                throw ex;

            }
            return blnRetValue;
        }

        public DataSet GetUOMReport()
        {
            return objDALUomConversion.GetUOMReport();
        }

        public DataTable GetUOMExistingOrNot(int intUomID)
        {
            objDALUomConversion.objConnection = objConnection;
            return objDALUomConversion.GetUOMExistingOrNot(intUomID);
        }
        public DataSet DisplayUOMConversionReport(int intUomClassID, int intUomBaseID) //UOM  Report
        {
            objDALUomConversion.objConnection = objConnection;
            return objDALUomConversion.DisplayUOMConversionReport(intUomClassID, intUomBaseID);
        }
    }
}
