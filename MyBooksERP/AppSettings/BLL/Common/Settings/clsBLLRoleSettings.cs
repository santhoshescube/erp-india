﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
   public class clsBLLRoleSettings
    {
       clsDALRoleSettings objclsRoleSettingsDAL;
       public clsDTORoleSettings objclsRoleSettings;

       public clsBLLRoleSettings()
       {
           objclsRoleSettingsDAL = new clsDALRoleSettings();
           objclsRoleSettings = new clsDTORoleSettings();
           objclsRoleSettingsDAL.RoleSettings = objclsRoleSettings;         
       }

       public DataTable BindRoles()
       {
           return objclsRoleSettingsDAL.BindRoles();
       }

       public DataTable BindModules()
       {
           return this.objclsRoleSettingsDAL.BindModules();
       }

       public DataTable BindRoleDetails()
       {
           return this.objclsRoleSettingsDAL.BindRoleDetails();
       }

       public bool SaveRoleDetails()
       {
           return this.objclsRoleSettingsDAL.SaveRoleDetails();
       }

       public DataTable GetModuleIDs()
       {
           return this.objclsRoleSettingsDAL.GetModuleIDs();
       }

       public DataTable BindaModulePermissions()
       {
           return this.objclsRoleSettingsDAL.BindaModulePermissions();
       }

       public void DeleteModulePermissions(int iRoleID,int iModuleID)
       {
            this.objclsRoleSettingsDAL.DeleteModulePermissions(iRoleID,iModuleID);
       }
    }
}
