﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;



namespace MyBooksERP
{
    public class clsBLLPricingScheme
    {
        clsDALPricingScheme objDALPricingScheme;
        clsDTOPricingScheme objDTOPricingScheme;
        private DataLayer objConnection;

        public clsBLLPricingScheme()
        {
            objDALPricingScheme = new clsDALPricingScheme();
            objDTOPricingScheme = new clsDTOPricingScheme();
            objConnection = new DataLayer();
            objDALPricingScheme.objDTOPricingScheme = objDTOPricingScheme;
        }

        public clsDTOPricingScheme clsDTOPricingScheme
        {
            get { return objDTOPricingScheme; }
            set { objDTOPricingScheme = value; }
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            objDALPricingScheme.objConnection = objConnection;
            return objDALPricingScheme.FillCombos(sarFieldValues);
        }

        public bool CheckDuplication(string[] sarFieldValues)
        {
            objDALPricingScheme.objConnection = objConnection;
            return objDALPricingScheme.CheckDuplication(sarFieldValues);
        }

        public bool SavePricingSchemeDetails(bool AddStatus)
        {
            bool blnRetValue = false;
            try
            {
                objConnection.BeginTransaction();
                objDALPricingScheme.objConnection = objConnection;
                blnRetValue = objDALPricingScheme.SavePricingSchemeDetails(AddStatus);
                objConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable DisplayPricingScheme()
        {
            return objDALPricingScheme.DisplayPricingScheme();
        }

        public bool DeletePricingScheme()
        {
            bool blnRetValue = false;
            try
            {
                objConnection.BeginTransaction();
                objDALPricingScheme.objConnection = objConnection;
                blnRetValue = objDALPricingScheme.DeletePricingScheme();
                objConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }


        public DataSet GetPricingSchemeReport()
        {
            return objDALPricingScheme.GetPricingSchemeReport();
        }

    }    
}
