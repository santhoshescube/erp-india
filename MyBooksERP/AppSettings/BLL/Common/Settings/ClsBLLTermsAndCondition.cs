﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
 /*
 ========================================================
 
 Author:<Author,,Amal Raj> 
 Create Date:<Create Date,,06 June 2011> 
 Description:<Description,,Terms And Condition BLL Class>
 
 ========================================================
 */
    public class ClsBLLTermsAndCondition : IDisposable
    {
        clsDTOTermsAndCond objclsDTOTermsAndCond;
        clsDALTermsAndCond objclsDALTermsAndCond;
        private DataLayer objBClsConnection;

        public ClsBLLTermsAndCondition()
        {
            objBClsConnection = new DataLayer();
            objclsDALTermsAndCond= new clsDALTermsAndCond();
            objclsDTOTermsAndCond=new clsDTOTermsAndCond();
            objclsDALTermsAndCond.objclsDTOTermsAndCond = objclsDTOTermsAndCond;
        }

        public clsDTOTermsAndCond clsDTOTermsAndCond
        {
            get { return objclsDTOTermsAndCond; }
            set { objclsDTOTermsAndCond = value; }
        }

        public bool SaveTermsAndConditions(bool AddStatus)
        {
            bool blnReturnVal = false;
            try
            {
                objclsDALTermsAndCond.objclsConnection = objBClsConnection;
                blnReturnVal=objclsDALTermsAndCond.SaveTermsAndConditions(AddStatus);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return blnReturnVal;
        }
        
        public DataTable FillCombos(string[] sarFieldValues)       // Filling combo box
        {
            objclsDALTermsAndCond.objclsConnection = objBClsConnection;
            return objclsDALTermsAndCond.FillCombos(sarFieldValues);
        }

        public DataTable GetTermsAndCondn(int intTypeID, int intCompanyID)
        {
            objclsDALTermsAndCond.objclsConnection = objBClsConnection;
            return objclsDALTermsAndCond.GetTermsAndCondn(intTypeID, intCompanyID);
        }

        public DataSet GetTermsAndConditionsReport(int intOprerationType, int intCompanyID)
        {
            return objclsDALTermsAndCond.GetTermsAndConditionsReport(intOprerationType, intCompanyID);
        }

        public bool DeleteTAC(int intTermsID)
        {
            return objclsDALTermsAndCond.DeleteTAC(intTermsID);
        }
        
        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (objclsDALTermsAndCond != null)
                objclsDALTermsAndCond = null;

            if (objclsDTOTermsAndCond != null)
                objclsDTOTermsAndCond = null;


        }

        #endregion


    }
}
