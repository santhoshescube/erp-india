﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,1 Mar 2011>
Description:	<Description,BLL for UOM>
================================================
*/

namespace MyBooksERP
{
    public class clsBLLUnitsOfMeasuremt:IDisposable
    {
        clsDTOUnitsOfMeasurement objDTOUnitsOfMeasurement; 
        clsDALUnitsOfMeasurement objDALUnitsOfMeasurement;
        private DataLayer objconnection;

        public clsBLLUnitsOfMeasuremt()
        {
            objDTOUnitsOfMeasurement = new clsDTOUnitsOfMeasurement();
            objDALUnitsOfMeasurement = new clsDALUnitsOfMeasurement();
            objconnection = new DataLayer();
            objDALUnitsOfMeasurement.objDTOUnitsOfMeasurement = objDTOUnitsOfMeasurement;
        }
         
        public  clsDTOUnitsOfMeasurement clsDTOUnitsOfMeasurement
        {
            get { return objDTOUnitsOfMeasurement; }
            set { objDTOUnitsOfMeasurement = value; }
        }

        public bool SaveUOM(bool AddStatus)
        {
            bool blnRetValue = false;
            try
            {
                objconnection.BeginTransaction();
                objDALUnitsOfMeasurement.objConnection = objconnection;
                blnRetValue = objDALUnitsOfMeasurement.SaveUOM(AddStatus);
                objconnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objconnection.RollbackTransaction();
                throw ex;

            }
            return blnRetValue;

        }

        //  Display 
        public bool DisplayUOM(int UOMID)
        {
            objDALUnitsOfMeasurement.objConnection = objconnection;
            objDTOUnitsOfMeasurement = objDALUnitsOfMeasurement.objDTOUnitsOfMeasurement;
            return objDALUnitsOfMeasurement.DisplayUOM(UOMID);

        }

        //Display UOM details in grid

        public DataTable GetUOMDetails()
        {
            objDALUnitsOfMeasurement.objConnection = objconnection;
            return objDALUnitsOfMeasurement.GetUOMDetails();

        }

        //Delete 

        public bool DeleteUOM(int UOMID)
        {
            bool blnRetValue = false;
            try
            {
                objconnection.BeginTransaction();
                objDALUnitsOfMeasurement.objConnection = objconnection;
                blnRetValue = objDALUnitsOfMeasurement.DeleteUOM(UOMID);
                objconnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objconnection.RollbackTransaction();
                throw ex;

            }
            return blnRetValue;

        }

        //email
        public DataSet GetUOMReport()
        {
            return objDALUnitsOfMeasurement.GetUOMReport();
        }

        public DataTable UOMIDExists(int intUOMID)
        {
            objDALUnitsOfMeasurement.objConnection = objconnection;
            return objDALUnitsOfMeasurement.UOMIDExists(intUOMID);

        }

        public DataTable GetFormNameTest(string strTableName)
        {
            objDALUnitsOfMeasurement.objConnection = objconnection;
            return objDALUnitsOfMeasurement.GetFormNameTest(strTableName);


        }

        public bool CheckDuplication(bool blnAddStatus, string[] sarValues, int intId)
        {
            objDALUnitsOfMeasurement.objConnection = objconnection;
            return objDALUnitsOfMeasurement.CheckDuplication(blnAddStatus, sarValues, intId);

        }    


        public DataTable FillCombos(string[] sarFieldValues)
        {
            objDALUnitsOfMeasurement.objConnection = objconnection;
            return objDALUnitsOfMeasurement.FillCombos(sarFieldValues);
        }


        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (objDALUnitsOfMeasurement != null)
                objDALUnitsOfMeasurement = null;

            if (objDTOUnitsOfMeasurement != null)
                objDTOUnitsOfMeasurement = null;


        }

        #endregion


        
       
    }
}
