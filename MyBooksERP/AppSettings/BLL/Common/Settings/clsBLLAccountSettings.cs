﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;
/* 
=================================================
   Author:		<Author,,Arun>
   Create date: <Create Date,,24 Feb 2011>
   Description:	<Description,,DTO for AcccountSettings>
================================================
*/
namespace MyBooksERP
{
    public class clsBLLAccountSettings
    {
        clsDALAccountSettings objclsDALAccountSettings;
        clsDTOAccountSettings objclsDTOAccountSettings;
        private DataLayer objClsConnection;

        public clsBLLAccountSettings()
        {
            objClsConnection = new DataLayer();
            objclsDALAccountSettings = new clsDALAccountSettings();
            objclsDTOAccountSettings = new clsDTOAccountSettings();
            objclsDALAccountSettings.objclsDTOAccountSettings = objclsDTOAccountSettings;
        }

        public clsDTOAccountSettings clsDTOAccountSettings
        {
            get { return objclsDTOAccountSettings; }
            set { objclsDTOAccountSettings = value; }
        }

        public bool SaveAccountSettings()// insert
        {
            bool blnReturnVal=false;
            try
            {
                objClsConnection.BeginTransaction();
                objclsDALAccountSettings.objclsConnection = objClsConnection;
                objclsDALAccountSettings.SaveAccountSettings();
                objClsConnection.CommitTransaction();
                blnReturnVal = true;
            }
            catch (Exception Ex)
            {
                objClsConnection.RollbackTransaction();
                throw Ex;
            }

            return blnReturnVal;
        }

        public DataTable DisplayGridInformation()//Dispaly Comapny Account Settings to GRid
        {
            objclsDALAccountSettings.objclsConnection = objClsConnection;
            return objclsDALAccountSettings.DisplayGridInformation();

        }
        public bool DeleteInfo()//Delete
        {
            bool blnReturnVal=false;
            try
            {
                objClsConnection.BeginTransaction();
                objclsDALAccountSettings.objclsConnection = objClsConnection;
                objclsDALAccountSettings.DeleteInfo();
                objClsConnection.CommitTransaction();
                blnReturnVal = true;
            }
            catch (Exception Ex)
            {
                objClsConnection.RollbackTransaction();
                throw Ex;
            }

            return blnReturnVal;
        }
        public DataTable FillCombos(string[] sarFieldValues)// filling combo box
        {
            objclsDALAccountSettings.objclsConnection = objClsConnection;
            return objclsDALAccountSettings.FillCombos(sarFieldValues);
        }
        public bool CheckDuplication(bool bAddStatus, string[] saValues, int iId)//Duplicate checkcing
        {
            try
            {
                objclsDALAccountSettings.objclsConnection = objClsConnection;
                return objclsDALAccountSettings.CheckDuplication(bAddStatus, saValues, iId);
            }
            catch (Exception Ex)
            {
                
                throw Ex;
            }
        }


        //email

        public DataSet GetAccountSettingsReport()
        {
            return objclsDALAccountSettings.GetAccountSettingsReport();
        }

        //Checking if Account ID Exists
        public DataTable CheckAccountIDExists(int MiAccountID)
        {
            return objclsDALAccountSettings.CheckAccountIDExists(MiAccountID);
        
        }
    }
}
