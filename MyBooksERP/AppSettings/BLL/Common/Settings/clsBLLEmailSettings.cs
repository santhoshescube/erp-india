﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsBLLEmailSettings
    {
        clsDALEmailSettings objclsDALEmailSettings;
        clsDTOEmailSettings objclsDTOEmailSettings;
        private DataLayer objclsconnection;

    
     public clsBLLEmailSettings()
        {

            objclsconnection = new DataLayer();
            objclsDALEmailSettings = new clsDALEmailSettings();
            objclsDTOEmailSettings = new clsDTOEmailSettings();
            objclsDALEmailSettings.ObjclsDTOEmailSettings = objclsDTOEmailSettings;
        }
       public clsDTOEmailSettings clsDTOEmailSettings
        {
            get { return objclsDTOEmailSettings; }
            set { objclsDTOEmailSettings = value; }
        }
       public bool GetEmailSmsSettings(int intAccountType)
       {
           objclsDALEmailSettings.ObjClsconnection = objclsconnection;
           return objclsDALEmailSettings.GetEmailSmsSettings(intAccountType);
       }
       public bool SaveEmailSettings(int intAccountType)
       {
           objclsDALEmailSettings.ObjClsconnection = objclsconnection;
           return objclsDALEmailSettings.SaveEmailSettings(intAccountType);
       }



}
}
