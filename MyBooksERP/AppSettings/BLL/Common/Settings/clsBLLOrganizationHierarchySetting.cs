﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Collections;

namespace MyBooksERP
{
    public class clsBLLWorkFlowSettingsSetting
    {
        Queue<clsDTOWorkFlowSettingsSetting> queclsDTOWorkFlowSettingsSetting;
        clsDALWorkFlowSettingsSetting objClsDALWorkFlowSettingsSetting;
        private DataLayer objClsConnection;

        public clsBLLWorkFlowSettingsSetting()
        {
            this.objClsConnection = new DataLayer();
            this.queclsDTOWorkFlowSettingsSetting = new Queue<clsDTOWorkFlowSettingsSetting>();
            this.objClsDALWorkFlowSettingsSetting = new clsDALWorkFlowSettingsSetting();
            this.objClsDALWorkFlowSettingsSetting.queclsDTOWorkFlowSettingsSetting = this.queclsDTOWorkFlowSettingsSetting;
        }

        ~clsBLLWorkFlowSettingsSetting()
        {
            this.objClsDALWorkFlowSettingsSetting=null;
            this.objClsConnection = null;
        }

        public Queue<clsDTOWorkFlowSettingsSetting> DTOWorkFlowSettingsSetting
        {
            get { return this.queclsDTOWorkFlowSettingsSetting; }
            set
            {
                this.queclsDTOWorkFlowSettingsSetting = value;
            }
        }

        /// <summary>
        /// Load all roles
        /// </summary>
        /// <returns></returns>
        public DataTable LoadRoles()
        {
            try
            {
                this.objClsDALWorkFlowSettingsSetting.objClsConnection = this.objClsConnection;
                return objClsDALWorkFlowSettingsSetting.LoadRoles();
            }
            catch (Exception ex) { throw ex; }
        }


        /// <summary>
        /// Load all roles
        /// </summary>
        /// <returns></returns>
        public DataTable LoadUsers()
        {
            try
            {
                this.objClsDALWorkFlowSettingsSetting.objClsConnection = this.objClsConnection;
                return objClsDALWorkFlowSettingsSetting.LoadUsers();
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Get all saved Hierarchy Setting
        /// </summary>
        /// <returns></returns>
        public DataTable GetSavedHierarchySetting()
        {
            try
            {
                this.objClsDALWorkFlowSettingsSetting.objClsConnection = this.objClsConnection;
                return objClsDALWorkFlowSettingsSetting.GetHierarchySettings();
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Get Hierarchy Setting by Role ID
        /// </summary>
        /// <param name="iRoleID"></param>
        /// <returns></returns>
        public DataTable GetSavedHierarchySetting(int iRoleID)
        {
            try
            {
                this.objClsDALWorkFlowSettingsSetting.objClsConnection = this.objClsConnection;
                return objClsDALWorkFlowSettingsSetting.GetHierarchySettings(iRoleID);
            }
            catch (Exception ex) { throw ex; }
        }


        public bool SaveHierarchySetting()// insert
        {
            bool blnReturnVal = false;
            try
            {
                objClsConnection.BeginTransaction();
                this.objClsDALWorkFlowSettingsSetting.objClsConnection = objClsConnection;
                this.objClsDALWorkFlowSettingsSetting.SaveWorkFlowSettingsSettings();
                objClsConnection.CommitTransaction();
                blnReturnVal = true;
            }
            catch (Exception Ex)
            {
                objClsConnection.RollbackTransaction();
                throw Ex;
            }

            return blnReturnVal;
        }
    }
}
