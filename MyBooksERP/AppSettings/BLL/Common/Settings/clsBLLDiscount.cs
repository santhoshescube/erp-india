﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,4 Mar 2011>
Description:	<Description,BLL for Discount>
================================================
*/
namespace MyBooksERP
{
   public  class clsBLLDiscount : IDisposable
    {
       clsDTODiscount objclsDTODiscount;
       clsDALDiscount objclsDALDiscount;
       private DataLayer objclsconnection;


       public clsBLLDiscount()
       {
           objclsconnection = new DataLayer();
           objclsDTODiscount = new clsDTODiscount();
           objclsDALDiscount = new clsDALDiscount();
           objclsDALDiscount.objclsDTODiscount = objclsDTODiscount;
       }

       public clsDTODiscount clsDTODiscount
       {
           get { return objclsDTODiscount; }
           set { objclsDTODiscount = value; }
       }

       public bool SaveDiscount(bool AddStatus)
       {
           bool blnRetValue = false;
           try
           {
               objclsconnection.BeginTransaction();
               objclsDALDiscount.objclsconnection = objclsconnection;
               blnRetValue = objclsDALDiscount.SaveDiscount(AddStatus);
               objclsconnection.CommitTransaction();
               blnRetValue = true;
           }
           catch (Exception ex)
           {
               objclsconnection.RollbackTransaction();
               throw ex;

           }
           return blnRetValue;

       }

      

       
       public int RecCountNavigate()
       {
           objclsDALDiscount.objclsconnection = objclsconnection;
           return objclsDALDiscount.RecCountNavigate();
       }

       public DataTable DisplayGridInformation(int intDiscountId,char mode)
       {
           objclsDALDiscount.objclsconnection = objclsconnection;
           return objclsDALDiscount.DisplayGridInformation(intDiscountId, mode);

       }



       public bool DisplayNormalDiscount(int intDiscountTypeId)
       {
           objclsDALDiscount.objclsconnection = objclsconnection;
           objclsDTODiscount = objclsDALDiscount.objclsDTODiscount;
           return objclsDALDiscount.DisplayNormalDiscount(intDiscountTypeId);

       }   
   

       public bool DeleteAll()
       {         
            bool blnRetValue = false;
            try
            {
                objclsconnection.BeginTransaction();
                objclsDALDiscount.objclsconnection = objclsconnection;
                blnRetValue = objclsDALDiscount.DeleteAll();
                objclsconnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objclsconnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
       public DataTable GetItemUOMs(int intItemID,int intUOMTypeID)
       {
           return objclsDALDiscount.GetItemUOMs(intItemID,intUOMTypeID);
       }
       public DataTable FillCombos(string[] sarFieldValues)
       {
           objclsDALDiscount.objclsconnection = objclsconnection;
           return objclsDALDiscount.FillCombos(sarFieldValues);
       }


       public DataTable GetDiscountIDExists(int MiDiscountTypeId)
       {
           return objclsDALDiscount.GetDiscountIDExists(MiDiscountTypeId);
       }

       public DataSet GetNormalReport()
       {
           return objclsDALDiscount.GetNormalReport();
       }

       public DataSet GetItemReport()
       {
           return objclsDALDiscount.GetItemReport();
          
       }

       public DataSet GetCustomerReport()
       {
           return objclsDALDiscount.GetCustomerReport();
          
       }       

       public string GetFormsInUse(string sCaption, int iPrimeryId, string sCondition, string sFileds)
       {
           return objclsDALDiscount.GetFormsInUse(sCaption, iPrimeryId, sCondition, sFileds);
       }

       public bool CheckDuplication(bool blnAddStatus, string[] sarValues, int intId)
       {
           return objclsDALDiscount.CheckDuplication(blnAddStatus, sarValues, intId);
       }
            

       #region IDisposable Members

       void IDisposable.Dispose()
       {
           if (objclsDALDiscount != null)
               objclsDALDiscount = null;

           if (objclsDTODiscount != null)
               objclsDTODiscount = null;
           
       }

       #endregion



       


    }
}
