﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MyBooksERP
{
    public class clsBLLChangePassword
    {
        private clsDALChangepassword MobjclsDALChangepassword;
        private clsDTOChangepassword MobjclsDTOChangepassword;
        private DataLayer MobjDataLayer;

        public clsBLLChangePassword()
        {
            MobjclsDALChangepassword = new clsDALChangepassword();
            MobjclsDTOChangepassword = new clsDTOChangepassword();
            MobjDataLayer = new DataLayer();
            MobjclsDALChangepassword.MobjclsDTOChangepassword = MobjclsDTOChangepassword;

        }

        public clsDTOChangepassword clsDTOChangepassword
        {
            get { return MobjclsDTOChangepassword; }
            set { MobjclsDTOChangepassword = value; }
        }
        public bool GetUserDetails()
        {
            MobjclsDALChangepassword.MobjDataLayer = MobjDataLayer;
            return MobjclsDALChangepassword.GetUserDetails();
        }
        public bool ChangePassword()
        {
            MobjclsDALChangepassword.MobjDataLayer = MobjDataLayer;
            return MobjclsDALChangepassword.ChangePassword();
        }

    }
}
