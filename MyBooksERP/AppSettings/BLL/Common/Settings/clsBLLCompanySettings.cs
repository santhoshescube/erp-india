﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;
/* ===============================================
  Author:		<Author,,Arun>
  Create date: <Create Date,,23 Feb 2011>
  Description:	<Description,,DTO for CompanySettings>
================================================*/
namespace MyBooksERP
{
   public  class ClsBLLCompanySettings
    {

       private clsDALCompanySettings objclsDALCompanySettings;
       private DataLayer ObjClsConnection;
       public ClsBLLCompanySettings()
       {
           ObjClsConnection=new DataLayer();
           objclsDALCompanySettings = new clsDALCompanySettings();
       }

       public DataTable DisplayCompanysettings(int CompanyID, int iMode, string sConfigurationItem)
       {
           objclsDALCompanySettings.objclsConnection = ObjClsConnection;
           return objclsDALCompanySettings.DisplayCompanysettings(CompanyID, iMode, sConfigurationItem);
       }
       public bool Reset(int intType)  //RESET (Advanced,C,P,P,P,R,S,S)
       {
           bool blnRetValue = false;
           try
           {
               ObjClsConnection.BeginTransaction();
               objclsDALCompanySettings.objclsConnection = ObjClsConnection;
               objclsDALCompanySettings.Reset(intType);
               ObjClsConnection.CommitTransaction();
               blnRetValue = true;
           }
           catch (Exception Ex)
           {
               ObjClsConnection.RollbackTransaction();
               throw Ex;
           }
           return blnRetValue;
       }

       public bool Insert(int intID)
       { 
           bool blnRetValue = false;
           try
           {
               ObjClsConnection.BeginTransaction();
               objclsDALCompanySettings.objclsConnection = ObjClsConnection;
               objclsDALCompanySettings.Insert(intID);
               ObjClsConnection.CommitTransaction();
               blnRetValue = true;
            }
           catch (Exception Ex)
           {
               ObjClsConnection.RollbackTransaction();
               throw Ex;
           }
           return blnRetValue;
       }

       //public void save(ArrayList prmCompany)
       //{
       //    objclsDALCompanySettings.objclsConnection = ObjClsConnection;
       //    objclsDALCompanySettings.save(prmCompany);
       //}
       public bool SavePro(int SettingsId, int CompanyID, string ConfigurationValue)
       {
           bool blnRetValue = false;
           try
           {
               objclsDALCompanySettings.objclsConnection = ObjClsConnection;
               objclsDALCompanySettings.SavePro(SettingsId, CompanyID, ConfigurationValue);
               
           }
           catch (Exception Ex)
           {
               
               throw Ex;
           }
           return blnRetValue;
       }
       public DataTable FillCombos(string[] sarFieldValues)// filling combo box
       {
           objclsDALCompanySettings.objclsConnection = ObjClsConnection;
           return objclsDALCompanySettings.FillCombos(sarFieldValues);
       }

       public DataSet GetCompanySettingsReport(int CompanyID)
       {
           return objclsDALCompanySettings.GetCompanySettingsReport(CompanyID);
       }

      
    }
}
