﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;


namespace MyBooksERP
{
    public class clsBLLProjects
    {

        
        private DataLayer MobjDataLayer;
        private clsDALProjects MobjclsDALProjects;
        private clsDTOProjects PobjclsDTOProjects;
        
        public clsBLLProjects()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALProjects = new clsDALProjects(MobjDataLayer);
            PobjclsDTOProjects = new clsDTOProjects();
            MobjclsDALProjects.objDTOProjects = PobjclsDTOProjects;
            
        }

        public clsDTOProjects clsDTOProjects
        {
            get { return PobjclsDTOProjects; }
            set { PobjclsDTOProjects = value; }
        }

        public DataTable FillCombos(string[] saFieldValues)// filling combo box
        {
            return MobjclsDALProjects.FillCombos(saFieldValues);

        }

        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjclsDALProjects.GetCompanyByPermission(intRoleID, intControlID);
        }

        public int GenerateCode(int intComapnyID)
        {
            return MobjclsDALProjects.GenerateCode(intComapnyID);
        }

        public int AddProjects()
        {
            try
            {
               MobjclsDALProjects.AddProjects();
                
            }
            catch (Exception ex)
            {
                //objDataLayer.RollbackTransaction();
                //throw ex;
            }
            return 0;
        }

        public int RecCountNavigate()
        {
            return MobjclsDALProjects.RecCountNavigate();
            
        }
        public bool DisplayProjectsInfo(int intRowNum) //For Dispaly Projects Information
        {
            return MobjclsDALProjects.DisplayProjectsInfo(intRowNum);
        }
        public int UpdateProjectsInfo()
        {
            return MobjclsDALProjects.UpdateProjectsInfo();
        }
        public bool DeleteProjectsInfo()
        {
            return MobjclsDALProjects.DeleteProjectsInfo();
        }

        public bool GetProjectDetailsExists()
        {
            return MobjclsDALProjects.GetProjectDetailsExists();
        }

        public int RecCountNavigate1(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjclsDALProjects.RecCountNavigate1(intRoleID,  intControlID);
        }

        public bool DisplayProjectsInfo1(int intRoleID, int intCompanyID, int intControlID, int intRowNum) //
        {
            return MobjclsDALProjects.DisplayProjectsInfo1(intRoleID,  intControlID, intRowNum);
        }
        public DataSet GetEmailInfo()
        {
            return MobjclsDALProjects.GetEmailInfo();
        }


    }
   
}
