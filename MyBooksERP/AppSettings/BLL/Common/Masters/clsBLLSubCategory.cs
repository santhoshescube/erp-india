﻿

using System.Data;
using System;

namespace MyBooksERP
{
    public class clsBLLSubCategory
    {
        public clsDTOSubCategory objDTOSubCategory { get; set; }
        private clsDALSubCategory objDALSubCategory;

        public clsBLLSubCategory()
        {
            objDTOSubCategory = new clsDTOSubCategory();
            objDALSubCategory = new clsDALSubCategory();
            objDALSubCategory.objDTOSubCategory = objDTOSubCategory;
            objDALSubCategory.objConnection = new DataLayer();
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objDALSubCategory.objConnection = objDALSubCategory.objConnection;
                    objCommonUtility.PobjDataLayer = objDALSubCategory.objConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public bool GetSubCategoryInfo()
        {
            return objDALSubCategory.GetSubCategoryInfo();
        }

        public bool SaveSubCategoryInfo()
        {
            return objDALSubCategory.SaveSubCategoryInfo();
        }

        public bool DeleteSubCategoryInfo()
        {
            return objDALSubCategory.DeleteSubCategoryInfo();
        }
        public bool GetFormsInUse(int iId)
        {
            bool blnRetValue = false;

            DataTable Dt = objDALSubCategory.GetFormsInUse(iId);

            if (Dt.Rows.Count > 0)
            {
                if (Convert.ToString(Dt.Rows[0]["FormName"]) != "0")
                    blnRetValue = true;
            }

            return blnRetValue;
        }
        public int GetSubCategoryCount()
        {
            return objDALSubCategory.GetSubCategoryCount();
        }

        public bool CheckDuplicateSubCategory()
        {
            return objDALSubCategory.CheckDuplicateSubCategory();
        }

        public DataSet GetSubCategoryReport()
        {
            return objDALSubCategory.GetSubCategoryReport();
        }
    }
}
