﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data;
using System.Data.SqlClient;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,22 Feb 2011>
Description:	<Description,,BLL for EmployeeInformation>
================================================
*/
namespace MyBooksERP
{
   public class ClsBLLEmployeeInformation:IDisposable
    {
       clsDTOEmployeeInformation objclsDTOEmployeeInformation;
       clsDALEmployeeInformation objclsDALEmployeeInformation;
       private DataLayer objclsconnection;

        
       public ClsBLLEmployeeInformation()
        {
            objclsDALEmployeeInformation = new clsDALEmployeeInformation();
            objclsDTOEmployeeInformation = new clsDTOEmployeeInformation();
            objclsconnection = new DataLayer();
            objclsDALEmployeeInformation.objclsDTOEmployeeInformation = objclsDTOEmployeeInformation;
        }
       public clsDTOEmployeeInformation clsDTOEmployeeInformation
       {
           get { return objclsDTOEmployeeInformation; }
           set { objclsDTOEmployeeInformation = value; }

       }

       public bool SaveEmployeeInformation(bool AddStatus)
       {
           bool blnRetValue = false;
           try
           {
               objclsconnection.BeginTransaction();
               objclsDALEmployeeInformation.objclsConnection = objclsconnection;
               blnRetValue = objclsDALEmployeeInformation.SaveEmployeeInformation(AddStatus);
               objclsconnection.CommitTransaction();
               blnRetValue = true;
           }
           catch (Exception ex)
           {
               objclsconnection.RollbackTransaction();
               throw ex;

           }
           return blnRetValue;

       }

       public bool SaveLeavePolicySummary()
       {
           
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return  objclsDALEmployeeInformation.SaveLeavePolicySummary();

       }

       public int RecCountNavigate()
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.RecCountNavigate();
       }
       public int RecCountNavigateByCompany(int intRoleID, int intCompanyID, int intControlID)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.RecCountNavigateByCompany(intRoleID, intCompanyID, intControlID);
       }

       public int GetRowNumber()
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.GetRowNumber();
       }

       public int GetCurRowNumber()
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.GetCurRowNumber();
       }

       public bool DisplayExistence(int EmployeeId)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           objclsDTOEmployeeInformation = objclsDALEmployeeInformation.objclsDTOEmployeeInformation;
           return objclsDALEmployeeInformation.CheckExistence(EmployeeId);

       }

       public bool DisplayEmployeeInformation(int rowno, string strPermittedCompany)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           objclsDTOEmployeeInformation = objclsDALEmployeeInformation.objclsDTOEmployeeInformation;
           return objclsDALEmployeeInformation.DisplayEmployeeInformation(rowno, strPermittedCompany);

       }

       public bool SearchEmployee(int SearchOption, string SearchCriteria,int RowNum, out int TotalRows)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           objclsDTOEmployeeInformation = objclsDALEmployeeInformation.objclsDTOEmployeeInformation;
           return objclsDALEmployeeInformation.SearchItem(SearchOption, SearchCriteria, RowNum,out TotalRows);
           
       }


       public bool DeleteEmployeeInformation()
       {
           bool blnRetValue = false;
           try
           {
               objclsconnection.BeginTransaction();
               objclsDALEmployeeInformation.objclsConnection = objclsconnection;
               blnRetValue = objclsDALEmployeeInformation.DeleteEmployeeInformation();
               objclsconnection.CommitTransaction();
               blnRetValue = true;
           }
           catch (Exception ex)
           {
               objclsconnection.RollbackTransaction();
               throw ex;

           }
           return blnRetValue;

       }
       public DataTable CheckExistReferences()
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.CheckExistReferences();

       }

       public DataTable FillCombos(string[] sarFieldValues)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.FillCombos(sarFieldValues);
       }

       public DataTable FillPolicyCombo(string sQuery)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.FillPolicyCombo(sQuery);
       }

       public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
       }

       public bool CheckDuplication(bool blnAddStatus, string[] sarValues, int intId,int intCompanyID)
       {
           return objclsDALEmployeeInformation.CheckDuplication(blnAddStatus, sarValues, intId, intCompanyID);
       }

       public bool CheckValidEmail(string sEmailAddress)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.CheckValidEmail(sEmailAddress);
       }

       public DataSet GetEmployeeReport()
       {
           return objclsDALEmployeeInformation.GetEmployeeReport();
       }

       public int GenerateEmployeeCode()
       {
           return objclsDALEmployeeInformation.GenerateEmployeeCode();
       }

       /// <summary>
       /// Converts DataTable to AutoCompleteStringCollection.
       /// </summary>
       /// <param name="dt">Datatable to be converted</param>
       /// <returns>string array</returns>
       public AutoCompleteStringCollection ConvertToAutoCompleteCollection(DataTable dt)
       {
           string[] array = { };

           if (dt != null && dt.Rows.Count > 0)
               array = Array.ConvertAll(dt.Select(), row => (string)row[0]);

           var collection = new AutoCompleteStringCollection();
           collection.AddRange(array);

           return collection;
       }

       public DataTable GetAutoCompleteList(SearchBy searchBy)
       {
           return this.objclsDALEmployeeInformation.GetAutoCompleteList(searchBy);
       }

       public bool DeleteLeavePolicy()
       {

           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.DeleteLeavePolicy();

       }
       public DataTable CheckCarryForwardLeaveExists()
       {
           return this.objclsDALEmployeeInformation.CheckCarryForwardLeaveExists();
       }


       public DataSet UpdateLeaveSummary(int iLeaveTypeID, int iLeavePolicyID)
       {
           return this.objclsDALEmployeeInformation.UpdateLeaveSummary(iLeaveTypeID, iLeavePolicyID);
       }

       public string GetCompanyFinYearStartDate(int intCompanyID)
       {
           return this.objclsDALEmployeeInformation.GetCompanyFinYearStartDate(intCompanyID);
       }

       #region IDisposable Members

        public void Dispose()
       {
           if (objclsDALEmployeeInformation != null)
               objclsDALEmployeeInformation = null;

           if (objclsDTOEmployeeInformation != null)
               objclsDTOEmployeeInformation=null;


       }

       #endregion

}
}
