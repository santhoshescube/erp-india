﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,11 Mar 2011>
Description:	<Description,BLL for Document>
================================================
*/
namespace MyBooksERP
{
   public class clsBLLDocuments 
    {
       clsDTODocuments objclsDTODocuments;
       clsDALDocuments objclsDALDocuments;
       private DataLayer objconnection;

       public clsBLLDocuments()
       {
           objconnection = new DataLayer();
           objclsDTODocuments = new clsDTODocuments();
           objclsDALDocuments = new clsDALDocuments();
           objclsDALDocuments.objclsDTODocuments = objclsDTODocuments;
       }

       public clsDTODocuments clsDTODocuments
       {
           get { return objclsDTODocuments; }
           set { objclsDTODocuments = value; }
       }

       public int Print()
       {
           objclsDALDocuments.objclsconnection = objconnection;
           return objclsDALDocuments.Print();
       }

       public int AddNew()
       {
           objclsDALDocuments.objclsconnection = objconnection;
           return objclsDALDocuments.AddNew();
       }

       public void Insert()
       {          
          objclsDALDocuments.objclsconnection = objconnection;
          objclsDALDocuments.Insert();          
       }

       public bool Save(bool AddStatus)
       {
           bool blnRetValue = false;
           try
           {

               objconnection.BeginTransaction();
               objclsDALDocuments.objclsconnection = objconnection;
               objclsDALDocuments.Save(AddStatus);
               objconnection.CommitTransaction();
               blnRetValue = true;
           }
           catch (Exception ex)
           {
               objconnection.RollbackTransaction();
               throw ex;
           }

           return blnRetValue;
       }

       public bool Deletenodes(int mode, int nodes)
       {
           bool blnRetValue = false;
           try
           {
               objconnection.BeginTransaction();
               objclsDALDocuments.objclsconnection = objconnection;
               blnRetValue = objclsDALDocuments.Deletenodes(mode,nodes);
               objconnection.CommitTransaction();
               blnRetValue = true;
           }
           catch (Exception ex)
           {
               objconnection.RollbackTransaction();
               throw ex;

           }
           return blnRetValue;

       }


       public bool selectDocumentType(int intDocument)
       {
           objclsDALDocuments.objclsconnection = objconnection;
           objclsDTODocuments = objclsDALDocuments.objclsDTODocuments;
           return objclsDALDocuments.selectDocumentType(intDocument);
       }

     
       public bool insertDetails(int intNode,string StrFilename, string StrMeta)
       {
           objclsDALDocuments.objclsconnection = objconnection;
           objclsDTODocuments = objclsDALDocuments.objclsDTODocuments;
           return objclsDALDocuments.insertDetails(intNode, StrFilename, StrMeta);
       }

       public bool Selectnode(long lngCommonID, string StrDescription, int intNavid)
       {
           objclsDALDocuments.objclsconnection = objconnection;
           objclsDTODocuments = objclsDALDocuments.objclsDTODocuments;
           return objclsDALDocuments.Selectnode(lngCommonID, StrDescription, intNavid);
       }

       public bool SelectDetails(int intNode)
       {
           objclsDALDocuments.objclsconnection = objconnection;
           objclsDTODocuments = objclsDALDocuments.objclsDTODocuments;
           return objclsDALDocuments.SelectDetails(intNode);
       }

       //selectforupdate
       public bool selectforupdate(string description)
       {
           objclsDALDocuments.objclsconnection = objconnection;
           objclsDTODocuments = objclsDALDocuments.objclsDTODocuments;
           return objclsDALDocuments.selectforupdate(description);

       }

       public bool SelectfromTreeMaster(int documentid)
       {
           objclsDALDocuments.objclsconnection = objconnection;
           objclsDTODocuments = objclsDALDocuments.objclsDTODocuments;
           return objclsDALDocuments.SelectfromTreeMaster( documentid);

       }

       public DataTable Fill(int CompanyId, string strDescription, int Navid,int ParentNode)
       {
           objclsDALDocuments.objclsconnection = objconnection;
           objclsDTODocuments = objclsDALDocuments.objclsDTODocuments;
           return objclsDALDocuments.Fill(CompanyId,strDescription,Navid,ParentNode);
       }

       public DataTable Display(int Navid, long CommonId, int Parentnode)
       {
           objclsDALDocuments.objclsconnection = objconnection;
           objclsDTODocuments = objclsDALDocuments.objclsDTODocuments;
           return objclsDALDocuments.Display(Navid, CommonId, Parentnode);
       }

       public DataTable GetparentNodes(int Navid, long CommonId, int Parentnode, int intOperationTypeID, int intVendorID, int intDocumentTypeid)
       {
           objclsDALDocuments.objclsconnection = objconnection;
           objclsDTODocuments = objclsDALDocuments.objclsDTODocuments;
           return objclsDALDocuments.GetparentNodes(Navid, CommonId, Parentnode, intOperationTypeID, intVendorID, intDocumentTypeid);
       }
    }
}
