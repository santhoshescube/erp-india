﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLVendorMapping
    {
        private clsDALVendorMapping MobjclsDALVendorMapping;
        private DataLayer MobjDataLayer;
        public clsDTOVendorInformation PobjclsDTOVendorInformation { get; set; }

        public clsBLLVendorMapping()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALVendorMapping = new clsDALVendorMapping(MobjDataLayer);
            PobjclsDTOVendorInformation = new clsDTOVendorInformation();
            MobjclsDALVendorMapping.objDTOVendorInformation = PobjclsDTOVendorInformation;
        }

        public DataTable getVendorDetails(int intType, string strSearchText, int intVendorTypeID)
        {
            return MobjclsDALVendorMapping.getVendorDetails(intType, strSearchText, intVendorTypeID);
        }

        public bool SaveVendorInfo()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALVendorMapping.SaveVendorInfo();
                MobjDataLayer.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeleteVendorInfo()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALVendorMapping.DeleteVendorInfo();
                MobjDataLayer.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable SetAutoCompleteList(int intType, string strSearchText, int intVendorTypeID)
        {
            return MobjclsDALVendorMapping.SetAutoCompleteList(intType, strSearchText, intVendorTypeID);
        }
    }
}
