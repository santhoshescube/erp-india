using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;
/* 
=================================================
   Author:		<Author,,Albz>
   Create date: <Create Date,,16 Feb 2011>
   Description:	<Description,,BLL for VendorInformation>
   Modified By: <Author,,Amal>
   Modified date: <Modified Date,,31 Aug 2011>
================================================
*/
namespace MyBooksERP
{
    public class clsBLLVendorInformation : IDisposable 
    {
        clsDALVendorInformation objclsDALVendorInformation;
        clsDTOVendorInformation objclsDTOVendorInformation;
        clsDALMailSettings objclsDALMailSettings;
        public clsDTOMailSetting objclsDTOMailSetting; 
        private DataLayer objClsConnection;

        public clsBLLVendorInformation()
        {
            objClsConnection = new DataLayer();
            objclsDALVendorInformation = new clsDALVendorInformation();
            objclsDTOVendorInformation = new clsDTOVendorInformation();
            objclsDALVendorInformation.objclsDTOVendorInformation = objclsDTOVendorInformation;

            this.objclsDALMailSettings = new clsDALMailSettings();
            this.objclsDALMailSettings.objClsConnection = this.objClsConnection;
        }

        public clsDTOVendorInformation clsDTOVendorInformation
        {
            get { return objclsDTOVendorInformation; }
            set { objclsDTOVendorInformation = value; }
        }
        
        public int AddVendorInfo()
        {
            int intRetValue = 0;
            try
            {
                objClsConnection.BeginTransaction();
                objclsDALVendorInformation.objclsConnection = objClsConnection;
                intRetValue=objclsDALVendorInformation.AddVendorInfo();
                objClsConnection.CommitTransaction();
            }
            catch (Exception ex)
            {
                objClsConnection.RollbackTransaction();
                throw ex;
            }
            return intRetValue;
        }

        public int AddVendorInfoRegular()
        {
            int intRetValue = 0;
            try
            {
                objClsConnection.BeginTransaction();
                objclsDALVendorInformation.objclsConnection = objClsConnection;
                intRetValue = objclsDALVendorInformation.AddVendorInfoRegular();
                objClsConnection.CommitTransaction();
            }
            catch (Exception ex)
            {
                objClsConnection.RollbackTransaction();
                throw ex;
            }
            return intRetValue;
        }
        
        public bool UpdateVendorInfo()
        {
            bool blnRetValue = false;
            try
            {
                objClsConnection.BeginTransaction();
                objclsDALVendorInformation.objclsConnection = objClsConnection;
                objclsDALVendorInformation.UpdateVendorInfo();
                objClsConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objClsConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool UpdatePermanantVendorAddress()
        {
            bool blnRetValue = false;
            try
            {
                objClsConnection.BeginTransaction();
                objclsDALVendorInformation.objclsConnection = objClsConnection;
                objclsDALVendorInformation.UpdatePermanantVendorAddress();
                objClsConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objClsConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeleteVendorInfo()
        {
            bool blnRetValue = false;
            try
            {
                objClsConnection.BeginTransaction();
                objclsDALVendorInformation.objclsConnection = objClsConnection;
                blnRetValue = objclsDALVendorInformation.DeleteVendorInfo();
                objClsConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objClsConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool FindVendorInfo(int intRowNum, int MintVendorType)
        {
            objclsDALVendorInformation.objclsConnection = objClsConnection;
            objclsDTOVendorInformation = objclsDALVendorInformation.objclsDTOVendorInformation;
            return objclsDALVendorInformation.FindVendorInfo(intRowNum, MintVendorType);
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            objclsDALVendorInformation.objclsConnection = objClsConnection;
            return objclsDALVendorInformation.FillCombos(sarFieldValues);
        }

        public DataTable GetNearestCompetitor(int intVendorID)
        {
            objclsDALVendorInformation.objclsConnection = objClsConnection;
            objclsDTOVendorInformation = objclsDALVendorInformation.objclsDTOVendorInformation;
            return objclsDALVendorInformation.GetNearestCompetitor(intVendorID);
        }

        public DataTable GetNearestCompetitorRating(int iVendorID)
        {
            objclsDALVendorInformation.objclsConnection = objClsConnection;
            objclsDTOVendorInformation = objclsDALVendorInformation.objclsDTOVendorInformation;
            return objclsDALVendorInformation.GetNearestCompetitorRating(iVendorID);
        }

        public DataTable FillNearestCompetitor()
        {
            return objclsDALVendorInformation.FillNearestCompetitor();
        }

        public DataTable AccountDetails()
        {
            if (objclsDTOVendorInformation.intVendorType == 2)
                return objclsDALVendorInformation.GetAccount((int)AccountGroups.SundryCreditors);
            else
                return objclsDALVendorInformation.GetAccount((int)AccountGroups.SundryDebtors);
        }

        public bool CheckDuplication(bool blnAddStatus, string[] sarValues, int intId,int intVendorTypeID)
        {
            return objclsDALVendorInformation.CheckDuplication(blnAddStatus, sarValues, intId,intVendorTypeID);
        }
        public bool IsPermanentAddressExist(bool blnAddStatus, string[] sarValues, int intId, int vendorAddId)
        {
            return objclsDALVendorInformation.IsPermanentAddressExist(blnAddStatus, sarValues, intId, vendorAddId);
        }

        public bool CheckMobileNoDuplication(bool blnAddStatus, string[] sarValues, int vendorId)
        {
            return objclsDALVendorInformation.CheckMobileNoDuplication(blnAddStatus, sarValues, vendorId);
        }

        public bool CheckLocationTypeDuplication(bool blnAddStatus, string[] sarValues, int vendorId, int vendorAddId)
        {
            return objclsDALVendorInformation.CheckLocationTypeDuplication(blnAddStatus, sarValues, vendorId,vendorAddId);
        }
        
        public bool CheckValidEmail(string sEmailAddress)
        {
            return objclsDALVendorInformation.CheckValidEmail(sEmailAddress);
        }

        public bool CheckExistReferences()
        {
            objclsDALVendorInformation.objclsConnection = objClsConnection;
            return objclsDALVendorInformation.CheckExistReferences();
        }

        public int RecCountNavigate(int MintVendorType)
        {
            objclsDALVendorInformation.objclsConnection = objClsConnection;
            return objclsDALVendorInformation.RecCountNavigate(MintVendorType);
        }

        public DataTable DtGetAddressName()
        {
            return objclsDALVendorInformation.DtGetAddressName();
        }

        public string GetFormsInUse(string strCaption, int intPrimeryId, string strCondition, string strFileds)
        {
            return objclsDALVendorInformation.GetFormsInUse(strCaption, intPrimeryId, strCondition, strFileds);
        }

        public DataSet GetVendorReport()
        {
            return objclsDALVendorInformation.GetVendorReport();
        }

        public bool AddAddress()
        {
            bool blnRetValue = false;

            objclsDALVendorInformation.objclsConnection = objClsConnection;
            objclsDALVendorInformation.AddAddress();
            blnRetValue = true;

            return blnRetValue;
        }

        public bool UpdateAddress()
        {
            bool blnRetValue = false;

            objclsDALVendorInformation.objclsConnection = objClsConnection;
            objclsDALVendorInformation.UpdateAddress();
            blnRetValue = true;

            return blnRetValue;
        }

        public bool DeleteAddress()
        {
            bool blnRetValue = false;

            objclsDALVendorInformation.objclsConnection = objClsConnection;
            blnRetValue = objclsDALVendorInformation.DeleteAddress();
            blnRetValue = true;

            return blnRetValue;
        }

        public bool FindVendorAddress()
        {
            objclsDALVendorInformation.objclsConnection = objClsConnection;
            objclsDTOVendorInformation = objclsDALVendorInformation.objclsDTOVendorInformation;
            return objclsDALVendorInformation.FindVendorAddress();
        }

        public int GetRowNumber()
        {
            objclsDALVendorInformation.objclsConnection = objClsConnection;
            return objclsDALVendorInformation.GetRowNumber();
        }
        /// <summary>
        /// Created By    : Laxmi
        /// Created date  :04-03-2011
        /// Project Type  : Web
        /// Purpose       : Get all associates from associates page control,Formview binding 
        /// </summary>
        //public DataSet GetAllAssociates()
        //{
        //    objclsDALVendorInformation.objclsConnection = objClsConnection;
        //    return objclsDALVendorInformation.GetAllAssociates();
        //}
        //public int GetSingleAssociates()
        //{
        //    objclsDALVendorInformation.objclsConnection = objClsConnection;
        //    return objclsDALVendorInformation.GetSingleAssociates();
        //}

        //public DataSet GetAssociateAddress(int intRowNum)
        //{
        //    objclsDALVendorInformation.objclsConnection = objClsConnection;
        //    return objclsDALVendorInformation.GetAssociateAddress(intRowNum);
        //}

        //public SqlDataReader GetSingleAssociateAddress(int intVendorAddressID,int intVendorID)
        //{
        //    objclsDALVendorInformation.objclsConnection = objClsConnection;
        //    return objclsDALVendorInformation.GetSingleAssociateAddress(intVendorAddressID, intVendorID);
        //}

        public bool IsVendorExists() // check duplication of vendor
        {
            objclsDALVendorInformation.objclsConnection = objClsConnection;
            return objclsDALVendorInformation.IsVendorExists();
        }

        //public bool IsPermanentAddressExist(bool blnAddStatus, string[] sarValues, int intvendorId) // check PermanentAddress Exist
        //{
        //    objclsDALVendorInformation.objclsConnection = objClsConnection;
        //    return objclsDALVendorInformation.IsPermanentAddressExist(blnAddStatus,sarValues, intvendorId);
        //}

         /// <summary>
        /// Created By   : Thasni Latheef
        /// Created date :07-03-2011
        /// Project Type : Web
        /// Purpose      : Get all customers in enquiry web form form 
        /// </summary>
        
        //public DataSet GetAllCustomers()
        //{
        //    objclsDALVendorInformation.objclsConnection = objClsConnection;
        //    return objclsDALVendorInformation.GetAllCustomers();

        //}

        public void GetMailSetting()
        {
            try
            {
                this.objclsDALMailSettings.GetMailSetting(2);//SMS
                this.objclsDTOMailSetting = this.objclsDALMailSettings.objclsDTOMailSetting;
            }
            catch (Exception ex) { throw ex; }
        }

        public int GenerateCode(int intFormType)
        {
            return objclsDALVendorInformation.GenerateCode(intFormType);
        }
        public bool SearchVendor(int SearchOption, string SearchCriteria, int RowNum,int intVendorTypeID, out int TotalRows)
        {
            objclsDALVendorInformation.objclsConnection = objClsConnection;
            objclsDTOVendorInformation = objclsDALVendorInformation.objclsDTOVendorInformation;
            return objclsDALVendorInformation.SearchVendor(SearchOption, SearchCriteria, RowNum,intVendorTypeID, out TotalRows);
        }

        public bool CheckVendorAlreadyUsed() // check duplication of vendor
        {
            objclsDALVendorInformation.objclsConnection = objClsConnection;
            return objclsDALVendorInformation.CheckVendorAlreadyUsed();
        }

        #region IDisposable Members

        public void Dispose()
        {
            //if (objClsConnection != null)
            //    objClsConnection = null;

            if (objclsDALVendorInformation != null)
                objclsDALVendorInformation = null;

            if (objclsDALVendorInformation != null)
                objclsDTOVendorInformation = null;
        }

        #endregion
    }
}