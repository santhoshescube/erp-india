﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date, 5 April 2011>
Description:	<Description,BLL for CurrencyReference>
================================================
*/
namespace MyBooksERP
{
    public  class clsBLLCurrencyReference
    {
        clsDTOCurrencyReference objclsDTOCurrencyReference { get; set; }
        clsDALCurrencyReference objclsDALCurrencyReference { get; set; }
         private DataLayer objconnection { get; set; }


         public clsDTOCurrencyReference clsDTOCurrencyReference
         {
             get { return objclsDTOCurrencyReference; }
             set { objclsDTOCurrencyReference = value; }

         }

         public clsBLLCurrencyReference()
         {
             objclsDTOCurrencyReference = new clsDTOCurrencyReference();
             objclsDALCurrencyReference = new clsDALCurrencyReference();
             objconnection = new DataLayer();
             objclsDALCurrencyReference.objclsDTOCurrencyReference = objclsDTOCurrencyReference;
         }

         public DataTable FillCombos(string[] sarFieldValues)
         {
             objclsDALCurrencyReference.objclsconnection = objconnection;
             return objclsDALCurrencyReference.FillCombos(sarFieldValues);
         }

         public DataTable GetCurrencyDetails()
         {
             objclsDALCurrencyReference.objclsconnection = objconnection;
             return objclsDALCurrencyReference.GetCurrencyDetails();

         }

         public DataTable CurrencyExists(int intCurrencyID)
         {
             objclsDALCurrencyReference.objclsconnection = objconnection;
             return objclsDALCurrencyReference.GetCurrencyExists(intCurrencyID);

         }

         public bool SaveCurrency(bool AddStatus)
         {
             bool blnRetValue = false;
             try
             {
                 objconnection.BeginTransaction();
                 objclsDALCurrencyReference.objclsconnection = objconnection;
                 blnRetValue = objclsDALCurrencyReference.SaveCurrency(AddStatus);
                 objconnection.CommitTransaction();
                 blnRetValue = true;
             }
             catch (Exception ex)
             {
                 objconnection.RollbackTransaction();
                 throw ex;

             }
             return blnRetValue;

         }

         //Delete 

         public bool DeleteCurrency(int intCurrencyID)
         {
             bool blnRetValue = false;
             try
             {
                 objconnection.BeginTransaction();
                 objclsDALCurrencyReference.objclsconnection = objconnection;
                 blnRetValue = objclsDALCurrencyReference.DeleteCurrency(intCurrencyID);
                 objconnection.CommitTransaction();
                 blnRetValue = true;
             }
             catch (Exception ex)
             {
                 objconnection.RollbackTransaction();
                 throw ex;

             }
             return blnRetValue;

         }

         //  Display 
         public bool DisplayCurrency(int intCurrencyID)
         {
             objclsDALCurrencyReference.objclsconnection = objconnection;
             objclsDTOCurrencyReference = objclsDALCurrencyReference.objclsDTOCurrencyReference;
             return objclsDALCurrencyReference.DisplayCurrency(intCurrencyID);

         }
         public int CheckDuplicateCurrency(string strCurrencyName)
         {
             objclsDALCurrencyReference.objclsconnection = objconnection;
             return objclsDALCurrencyReference.CheckDuplicateCurrency(strCurrencyName);
         }
         public int CheckDuplicateCurrency(string strCurrencyName, int intCurrID)
         {
             objclsDALCurrencyReference.objclsconnection = objconnection;
             return objclsDALCurrencyReference.CheckDuplicateCurrency(strCurrencyName, intCurrID);
         }
         public int CheckDuplicateCountry(int intCurrID, int intCountryID)
         {
             objclsDALCurrencyReference.objclsconnection = objconnection;
             return objclsDALCurrencyReference.CheckDuplicateCountry(intCurrID, intCountryID);
         }
    }
}
