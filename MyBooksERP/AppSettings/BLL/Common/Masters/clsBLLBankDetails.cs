﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;


/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,17 Feb 2011>
Description:	<Description,,BLL for Bank>
================================================
*/
namespace MyBooksERP
{
    
    public class clsBLLBankDetails:IDisposable
    {
        clsDTOBankDetails objclsDTOBankDetails;
        clsDALBankDetails objclsDALBankDetails;
        private DataLayer objClsConnection;


        public clsBLLBankDetails()
        {
            objClsConnection = new DataLayer();
            objclsDALBankDetails = new clsDALBankDetails();
            objclsDTOBankDetails = new clsDTOBankDetails();
            objclsDALBankDetails.objclsDTOBankDetails = objclsDTOBankDetails;

        }

        public clsDTOBankDetails clsDTOBankDetails
        {
            get { return objclsDTOBankDetails; }
            set { objclsDTOBankDetails = value; }
        }


        public bool SaveBankDetails(bool AddStatus)
        {
            bool blnRetValue = false;
            try
            {

                objClsConnection.BeginTransaction();
                objclsDALBankDetails.objclsConnection = objClsConnection;
                objclsDALBankDetails.SaveBankDetails(AddStatus);
                objClsConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objClsConnection.RollbackTransaction();
                throw ex;
            }
                     
                return blnRetValue;
        }
         

        public int RecCountNavigate()
        {
            objclsDALBankDetails.objclsConnection = objClsConnection;
            return objclsDALBankDetails.RecCountNavigate();
        }


        public int RecCountBankNavigate(int MintBankID)
        {
            objclsDALBankDetails.objclsConnection = objClsConnection;
            return objclsDALBankDetails.RecCountBankNavigate(MintBankID);
        }
        public bool DisplayBankDetails(int rowno)
        {
            objclsDALBankDetails.objclsConnection = objClsConnection;
            objclsDTOBankDetails = objclsDALBankDetails.objclsDTOBankDetails;
            return objclsDALBankDetails.DisplayBankDetails(rowno);

        }

        public bool DisplayBankIDDetails(int rowno,int bankID)
        {
            objclsDALBankDetails.objclsConnection = objClsConnection;
            objclsDTOBankDetails = objclsDALBankDetails.objclsDTOBankDetails;
            return objclsDALBankDetails.DisplayBankIDDetails(rowno, bankID);

        }
        public bool DeleteBankDetails()
        {
            bool blnRetValue = false;
            try
            {


                objClsConnection.BeginTransaction();
                objclsDALBankDetails.objclsConnection = objClsConnection;
                blnRetValue = objclsDALBankDetails.DeleteBankDetails();
                objClsConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objClsConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            objclsDALBankDetails.objclsConnection = objClsConnection;
            return objclsDALBankDetails.FillCombos(sarFieldValues);
        }

        public DataTable AccountDetails()
        {
            return objclsDALBankDetails.AccountDetails();
        }

      
        public DataTable CheckExistReferences(int intId)
        {
            return objclsDALBankDetails.CheckExistReferences(intId);
        }

        public string GetFormsInUse(string strCaption, int intPrimeryId, string strCondition, string strFileds)
        {
            return objclsDALBankDetails.GetFormsInUse(strCaption, intPrimeryId, strCondition, strFileds);
        }

        public bool CheckValidEmail(string sEmailAddress)
        {
            return objclsDALBankDetails.CheckValidEmail(sEmailAddress);
        }

        public DataSet GetBankReport()
        {
            return objclsDALBankDetails.GetBankReport();
        }

        #region IDisposable Members

        public void Dispose()
        {           

            if (objclsDTOBankDetails != null)
                objclsDTOBankDetails = null;

            if (objclsDALBankDetails != null)
                objclsDALBankDetails = null;
        }

        #endregion
       

}
}
