﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;


/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,28 Feb 2011>
Description:	<Description,,BLL for UserInformation>
================================================
*/

namespace MyBooksERP
{
    public class clsBLLUserInformation: IDisposable
    {

        clsDTOUserInformation objclsDTOUserInformation;
        clsDALUserInfomation objclsDALUserInfomation;
        private DataLayer objclsconnection;


        public clsBLLUserInformation()
        {
            objclsconnection = new DataLayer();
            objclsDALUserInfomation = new clsDALUserInfomation();
            objclsDTOUserInformation = new clsDTOUserInformation();
            objclsDALUserInfomation.objclsDTOUserInformation = objclsDTOUserInformation;

        }

        public clsDTOUserInformation clsDTOUserInformation
        {
            get { return objclsDTOUserInformation; }
            set { objclsDTOUserInformation = value; }

        }

        public bool SaveUser(bool AddStatus)
        {
            bool blnRetValue = false;
            try
            {
                objclsconnection.BeginTransaction();
                objclsDALUserInfomation.objclsconnection = objclsconnection;
                blnRetValue = objclsDALUserInfomation.SaveUser(AddStatus);
                objclsconnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objclsconnection.RollbackTransaction();
                throw ex;

            }
            return blnRetValue;

        }

        public int RecCountNavigate()
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.RecCountNavigate();
        }

        public bool DisplayUser(int rowno)
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            objclsDTOUserInformation = objclsDALUserInfomation.objclsDTOUserInformation;
            return objclsDALUserInfomation.DisplayUser(rowno);

        }

        public bool DeleteUserInformation()
        {
            bool blnRetValue = false;
            try
            {
                objclsconnection.BeginTransaction();
                objclsDALUserInfomation.objclsconnection = objclsconnection;
                blnRetValue = objclsDALUserInfomation.DeleteUserInformation();
                objclsconnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objclsconnection.RollbackTransaction();
                throw ex;

            }
            return blnRetValue;

        }


        public DataTable FillCombos(string[] sarFieldValues)
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.FillCombos(sarFieldValues);
        }


        public bool CheckDuplication(bool blnAddStatus, string[] sarValues, int intId)
        {
            return objclsDALUserInfomation.CheckDuplication(blnAddStatus, sarValues, intId);
        }



        public bool CheckValidEmail(string sEmailAddress)
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.CheckValidEmail(sEmailAddress);
        }

        public DataTable CheckExistReferences(int intId)
        {
            return objclsDALUserInfomation.CheckExistReferences(intId);
        }


        public string GetFormsInUse(string strCaption, int intPrimeryId, string strCondition, string strFileds)
        {
            return objclsDALUserInfomation.GetFormsInUse(strCaption, intPrimeryId, strCondition, strFileds);
        }

        /// <summary>
        /// Created By   : Laxmi
        /// Created date : 09-03-2011
        /// Purpose      : Check user exists from login page
        /// </summary>
        /// 
        public SqlDataReader IsExistUserName()
        {

            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.IsExistUserName();
        }
        //public int GetEmployeeId()
        //{
        //    FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;

        //    if (id == null)
        //        return -1;

        //    FormsAuthenticationTicket ticket = id.Ticket;
        //    string[] userData = ticket.UserData.Split(',');
        //    return Convert.ToInt32(userData[2]);
        //}

        //public int GetUserId()
        //{
        //    FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;

        //    if (id == null)
        //        return -1;

        //    FormsAuthenticationTicket ticket = id.Ticket;
        //    string[] userData = ticket.UserData.Split(',');
        //    return Convert.ToInt32(userData[1]);
        //}

        //public static clsDTOUserInformation GetUserInfo()
        //{
        //    clsDTOUserInformation objUserInfo = null;

        //    FormsIdentity identity = (FormsIdentity)HttpContext.Current.User.Identity;

        //    if (identity == null) return objUserInfo;

        //    FormsAuthenticationTicket ticket = identity.Ticket;
        //    string[] userData = ticket.UserData.Split(',');
            
        //    if (userData.Length >= 4)
        //    {
        //        objUserInfo = new clsDTOUserInformation();
        //        objUserInfo.intRoleID = Convert.ToInt32(userData[0]);
        //        objUserInfo.intUserID = Convert.ToInt32(userData[1]);
        //        objUserInfo.intEmployeeID =Convert.ToInt32(userData[2]);
        //        objUserInfo.strUserType = userData[3];
        //    }

        //    return objUserInfo;
        //}

        //public int GetRoleId()
        //{
        //    FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;

        //    if (id == null)
        //        return -1;

        //    FormsAuthenticationTicket ticket = id.Ticket;
        //    string[] userData = ticket.UserData.Split(',');
        //    return Convert.ToInt32(userData[0]);
        //}

        public string GetWelcomeText() // Home page
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.GetWelcomeText();
        }

        public DataTable getUserCompanyDetails()
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.getUserCompanyDetails();
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (objclsDALUserInfomation != null)
                objclsDALUserInfomation = null;

            if (objclsDTOUserInformation != null)
                objclsDTOUserInformation = null;


        }

        #endregion
    }
}
