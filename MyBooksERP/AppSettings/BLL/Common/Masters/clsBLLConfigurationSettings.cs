﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,1 Mar 2011>
Description:	<Description,BLL for ConfigurationSettings>
================================================
*/
namespace MyBooksERP
{
    public class clsBLLConfigurationSettings : IDisposable
    {

        clsDALConfigurationSettings objclsDALConfigurationSettings;
        clsDTOConfigurationSettings objclsDTOConfigurationSettings;
        private DataLayer objclsconnection;

        public clsBLLConfigurationSettings()
        {

            objclsconnection = new DataLayer();
            objclsDALConfigurationSettings = new clsDALConfigurationSettings();
            objclsDTOConfigurationSettings = new clsDTOConfigurationSettings();
            objclsDALConfigurationSettings.ObjclsDTOConfigurationSettings = objclsDTOConfigurationSettings;
        }

        public clsDTOConfigurationSettings clsDTOConfigurationSettings
        {
            get { return objclsDTOConfigurationSettings; }
            set { objclsDTOConfigurationSettings = value; }
        }

        public bool update()
        {
            bool blnRetValue = false;
            try
            {

                objclsconnection.BeginTransaction();
                objclsDALConfigurationSettings.ObjClsconnection = objclsconnection;
                objclsDALConfigurationSettings.Update();
                objclsconnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objclsconnection.RollbackTransaction();
                throw ex;
            }

            return blnRetValue;
        }

        public DataTable Display(int ModuleID)
        {
            objclsDALConfigurationSettings.ObjClsconnection = objclsconnection;
            objclsDTOConfigurationSettings = objclsDALConfigurationSettings.ObjclsDTOConfigurationSettings;
            return objclsDALConfigurationSettings.DisplayInformation(ModuleID);
        }

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (objclsDALConfigurationSettings != null)
                objclsDALConfigurationSettings = null;


            if (objclsDTOConfigurationSettings != null)
                objclsDTOConfigurationSettings = null;


        }

        #endregion
    }
}