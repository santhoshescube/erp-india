﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;
namespace MyBooksERP
{
    /// <summary>
    /// Author:Arun
    /// Date:17/02/2011
    /// Purpose:For FrmWarehouse Transactions
    /// </summary>
    public class clsBLLWarehouse
    {
        private clsDALWarehouse objclsDALWarehouse;
        clsDTOWarehouse objclsDTOWarehouse;
        private DataLayer ObjClsConnection;

        public clsBLLWarehouse()
        {
            ObjClsConnection = new DataLayer();
            objclsDALWarehouse = new clsDALWarehouse();
            objclsDTOWarehouse = new clsDTOWarehouse();
            objclsDALWarehouse.clsDTOWarehouse = clsDTOWarehouse;
        }

        public clsDTOWarehouse clsDTOWarehouse
        {
            get { return objclsDTOWarehouse; }
            set { objclsDTOWarehouse = value; }
        }

        public int DisplayRecordCount(int intCompanyID)//Displaying Total Record Count 
        {
            objclsDALWarehouse.objclsConnection = ObjClsConnection;
            return objclsDALWarehouse.DisplayRecordCount(intCompanyID);
        }

        public int DisplayCompanyRecordCount(int intRoleID,int intCompanyID, int intControlID)//Displaying Total Record Count With Company 
        {
            objclsDALWarehouse.objclsConnection = ObjClsConnection;
            return objclsDALWarehouse.DisplayCompanyRecordCount(intRoleID, intCompanyID, intControlID);
        }
        public bool DispalyWarehouseInfo(int RowNo, int intCompanyID)//Displaying The Warehouse Master
        {
            bool blnRetValue = false;
            try
            {
                objclsDALWarehouse.objclsConnection = ObjClsConnection;
                objclsDALWarehouse.DispalyWarehouseInfo(RowNo,intCompanyID);
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                ObjClsConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DisplayCompanyWarehouseInfo(int intRoleID,int intCompanyID, int intControlID, int RowNo)//Displaying The Warehouse Master
        {
            bool blnRetValue = false;
            try
            {
                objclsDALWarehouse.objclsConnection = ObjClsConnection;
                objclsDALWarehouse.DisplayCompanyWarehouseInfo(intRoleID, intCompanyID, intControlID, RowNo);
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                ObjClsConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable DisplayWarehouseInformationDetail(int intWarehouseID)
        {
            objclsDALWarehouse.objclsConnection = ObjClsConnection;
            return objclsDALWarehouse.DisplayWarehouseInformationDetail(intWarehouseID);
        }

        public DataTable DisplayWarehouseInformationDetailRow(int warehouseid, string Location, int locID)
        {
            objclsDALWarehouse.objclsConnection = ObjClsConnection;
            return objclsDALWarehouse.DisplayWarehouseInformationDetailRow(warehouseid, Location, locID);
        }

        public DataTable DisplayWarehouseInformationDetailBLOCK(int WarehouseID, string Location, int LocID, string Rows, int RowId)
        {
            objclsDALWarehouse.objclsConnection = ObjClsConnection;
            return objclsDALWarehouse.DisplayWarehouseInformationDetailBLOCK(WarehouseID, Location, LocID, Rows, RowId);
        }

        public DataTable DisplayWarehouseInformationDetailLOTS(int WarehouseID, string Location, int LocID, string Rows, int RowID, string Blocks, int BlockID)
        {
            objclsDALWarehouse.objclsConnection = ObjClsConnection;
            return objclsDALWarehouse.DisplayWarehouseInformationDetailLOTS(WarehouseID, Location, LocID, Rows, RowID, Blocks, BlockID);
        }

        public bool SaveWarehouseGenaralInformation()//Saving WareHouse
        {
            bool blnRetValue = false;
            try
            {
                ObjClsConnection.BeginTransaction();
                objclsDALWarehouse.objclsConnection = ObjClsConnection;

                if (objclsDTOWarehouse.intWarehouseID > 0)
                    objclsDALWarehouse.DeleteWarehouseDetailInformation();

                objclsDALWarehouse.SaveWarehouseGenaralInformation();
                ObjClsConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                ObjClsConnection.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public string GetFormsInUse(string strCaption, int intPrimeryId, string strCondition, string strFileds)
        {
            return objclsDALWarehouse.GetFormsInUse(strCaption, intPrimeryId, strCondition, strFileds);
        }

        public bool GetFormsInUse(int iId)
        {
            bool blnRetValue = false;

            objclsDALWarehouse.objclsConnection = ObjClsConnection;
            DataTable Dt = objclsDALWarehouse.GetFormsInUse(iId);

            if (Dt.Rows.Count > 0)
            {
                if (Convert.ToString(Dt.Rows[0]["FormName"]) != "0")
                    blnRetValue = true;
            }

            return blnRetValue;
        }
        public bool GetNodeInUse(int intWarehouseID, int intLocId, int intRowID, int intBlockID, int intLotID, int intLevel)
        {
            return objclsDALWarehouse.GetNodeInUse(intWarehouseID, intLocId, intRowID, intBlockID, intLotID, intLevel);
        }
        public bool DeleteWarehouseInformation() //Delete Warehouse Info
        {
            bool blnRetValue = false;
            try
            {
                ObjClsConnection.BeginTransaction();
                objclsDALWarehouse.objclsConnection = ObjClsConnection;
                objclsDALWarehouse.clsDTOWarehouse = clsDTOWarehouse;
                objclsDALWarehouse.DeleteWarehouseInformation();
                ObjClsConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception Ex)
            {
                ObjClsConnection.RollbackTransaction();
                throw Ex;
            }
            return blnRetValue;
        }

        public bool DeleteWarehouseDetailInformation() //For Update
        {
            bool blnRetValue = false;
            try
            {
                ObjClsConnection.BeginTransaction();
                objclsDALWarehouse.objclsConnection = ObjClsConnection;
                objclsDALWarehouse.clsDTOWarehouse = clsDTOWarehouse;
                objclsDALWarehouse.DeleteWarehouseDetailInformation();
                ObjClsConnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception Ex)
            {

                ObjClsConnection.RollbackTransaction();
                throw Ex;
            }
            return blnRetValue;
        }

        public bool CheckDuplication(int CompID, string WareHousename, int Wid)
        {
            bool blnRetValue = false;

            objclsDALWarehouse.objclsConnection = ObjClsConnection;

            if (objclsDALWarehouse.CheckDuplication(CompID, WareHousename, Wid))
                blnRetValue = true;

            return blnRetValue;
        }

        public bool GetWarehouseCount(int CompID)
        {
            bool blnRetValue = false;

            objclsDALWarehouse.objclsConnection = ObjClsConnection;

            if (objclsDALWarehouse.GetWarehouseCount(CompID) > 0)
                blnRetValue = true;

            return blnRetValue;
        }

        public DataTable FillCombos(string[] sarFieldValues)// filling combo box
        {
            objclsDALWarehouse.objclsConnection = ObjClsConnection;
            return objclsDALWarehouse.FillCombos(sarFieldValues);
        }
        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            objclsDALWarehouse.objclsConnection = ObjClsConnection;
            return objclsDALWarehouse.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
        }
        public DataSet GetWarehouseReport()
        {
            objclsDALWarehouse.objclsConnection = ObjClsConnection;
            return objclsDALWarehouse.GetWarehouseReport();
        }
        public int GetRowNumber()
        {
            objclsDALWarehouse.objclsConnection = ObjClsConnection;
            return objclsDALWarehouse.GetRowNumber();
        }

        public DataTable getWarehouseCompanyDetails(int intWarehouseID)
        {
            objclsDALWarehouse.objclsConnection = ObjClsConnection;
            return objclsDALWarehouse.getWarehouseCompanyDetails(intWarehouseID);
        }
    }
}