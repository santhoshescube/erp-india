﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,16 Feb 2011>
   Description:	<Description,,CommonRef BLL Class>
================================================
*/
namespace MyBooksERP
{
    public class clsBLLCommonRef
    {
        private clsDALCommonRef MobjclsDALCommonRef;
        private clsDTOCommonRef MobjclsDTOCommonRef;
        private DataLayer MobjDataLayer;

        public clsDTOCommonRef clsDTOCommonRef
        {
            get { return MobjclsDTOCommonRef; }
            set { MobjclsDTOCommonRef = value; }
        }

        public clsBLLCommonRef()
        {
            //constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALCommonRef = new clsDALCommonRef();
            MobjclsDTOCommonRef = new clsDTOCommonRef();
            MobjclsDALCommonRef.PobjclsDTOCommonRef = clsDTOCommonRef;
            MobjclsDALCommonRef.PobjDataLayer = MobjDataLayer;
        }

        public DataTable DisplayInformation()
        {
            //display information
                return MobjclsDALCommonRef.DisplayInformation();
        }

        public bool AddInformation(out int intPmId)
        {
            //Adding information
            intPmId = 0;
            return MobjclsDALCommonRef.AddInformation(out intPmId);
        }

        public bool UpdateInformation()
        {
            //Updating information
                return MobjclsDALCommonRef.UpdateInformation();
        }

        public bool DeleteInformation()
        {
            //Deleting Information
               return MobjclsDALCommonRef.DeleteInformation();
        }
    }
}