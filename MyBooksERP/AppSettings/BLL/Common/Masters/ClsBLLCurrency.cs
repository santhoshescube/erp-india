﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date, 4 April 2011>
Description:	<Description,BLL for Currency>
================================================
*/
namespace MyBooksERP
{
     public class ClsBLLCurrency
    {
         ClsDTOCurrency objClsDTOCurrency { get; set; }
         ClsDALCurrency objClsDALCurrency { get; set; }
         private DataLayer objconnection { get; set; }


         public ClsDTOCurrency ClsDTOCurrency
         {
             get { return objClsDTOCurrency; }
             set { objClsDTOCurrency = value; }

         }

         public ClsBLLCurrency()
         {
             objClsDTOCurrency = new ClsDTOCurrency();
             objClsDALCurrency = new ClsDALCurrency();
             objconnection = new DataLayer();
             objClsDALCurrency.objClsDTOCurrency = objClsDTOCurrency;
         }

         public DataTable FillCombos(string[] sarFieldValues)
         {
             objClsDALCurrency.objclsconnection = objconnection;
             return objClsDALCurrency.FillCombos(sarFieldValues);
         }
         public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
         {
             objClsDALCurrency.objclsconnection = objconnection;
             return objClsDALCurrency.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
         }

         public DataTable GetCurrencyDetails(int intCompanyID)
         {
             objClsDALCurrency.objclsconnection = objconnection;
             return objClsDALCurrency.GetCurrencyDetails(intCompanyID);

         }

         public bool SaveCurrency(bool AddStatus)
         {
             bool blnRetValue = false;
             try
             {
                 objconnection.BeginTransaction();
                 objClsDALCurrency.objclsconnection = objconnection;
                 blnRetValue = objClsDALCurrency.SaveCurrency(AddStatus);
                 objconnection.CommitTransaction();
                 blnRetValue = true;
             }
             catch (Exception ex)
             {
                 objconnection.RollbackTransaction();
                 throw ex;

             }
             return blnRetValue;

         }


              //  Display 
         public bool DisplayCompanyCurrency(int intCompanyID)
         {
             objClsDALCurrency.objclsconnection = objconnection;
             objClsDTOCurrency = objClsDALCurrency.objClsDTOCurrency;
             return objClsDALCurrency.DisplayCompanyCurrency(intCompanyID);

         }



         public bool CheckDuplication(int intCompanyID , int intCurrencyid, string date )
         {
             objClsDALCurrency.objclsconnection = objconnection;
             objClsDTOCurrency = objClsDALCurrency.objClsDTOCurrency;
             return objClsDALCurrency.CheckDuplication(intCompanyID, intCurrencyid,date);

         }

         //  Display 
         public bool DisplayCurrency(int intCurrencyDetailID)
         {
             objClsDALCurrency.objclsconnection = objconnection;
             objClsDTOCurrency = objClsDALCurrency.objClsDTOCurrency;
             return objClsDALCurrency.DisplayCurrency(intCurrencyDetailID);

         }
         //email
         public DataSet GetExchangeCurrencyReport()
         {
             return objClsDALCurrency.GetExchangeCurrencyReport();
         }
        

         //Delete 

         public bool DeleteCurrency(int intCurrencyDetailID)
         {
             bool blnRetValue = false;
             try
             {
                 objconnection.BeginTransaction();
                 objClsDALCurrency.objclsconnection = objconnection;
                 blnRetValue = objClsDALCurrency.DeleteCurrency(intCurrencyDetailID);
                 objconnection.CommitTransaction();
                 blnRetValue = true;
             }
             catch (Exception ex)
             {
                 objconnection.RollbackTransaction();
                 throw ex;

             }
             return blnRetValue;

         }

         public int GetCurrencyExists(int intCurrDetailId )
         {
             objClsDALCurrency.objclsconnection = objconnection;
             return objClsDALCurrency.GetCurrencyExists(intCurrDetailId);

         }

         public int GetCurrencyExists1(int intCurrDetailId)
         {
             objClsDALCurrency.objclsconnection = objconnection;
             return objClsDALCurrency.GetCurrencyExists1(intCurrDetailId);
         }
       /* 
       =================================================
           Author:		<Author,Amal>
           Create date: <Create Date,16 August 2011>
           Description:	<Description,,Modification in Currency label>
       ================================================
      */

       public bool FillERCompanyCurrencyLabel(int intCompanyID)
       {
         objClsDALCurrency.objclsconnection = objconnection;
         objClsDTOCurrency = objClsDALCurrency.objClsDTOCurrency;
         return objClsDALCurrency.FillERCompanyCurrencyLabel(intCompanyID);
       }
       public bool FillERSelectedCurrencyLabel(int intCurrencyID)
       {
         objClsDALCurrency.objclsconnection = objconnection;
         objClsDTOCurrency = objClsDALCurrency.objClsDTOCurrency;
         return objClsDALCurrency.FillERSelectedCurrencyLabel(intCurrencyID);
       }
         
    }
}
