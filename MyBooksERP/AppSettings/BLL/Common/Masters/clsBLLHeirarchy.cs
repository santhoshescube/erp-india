﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
   public  class clsBLLHeirarchy
    {
       clsDALHeirarchy objclsDALHeirarchy;
       clsDTOHeirarchy objclsDTOHeirarchy;
       private DataLayer objClsConnection;

       public clsBLLHeirarchy()
        {
            objClsConnection = new DataLayer();
            objclsDALHeirarchy = new clsDALHeirarchy();
            objclsDTOHeirarchy  = new clsDTOHeirarchy();
            objclsDALHeirarchy.objclsDTOHeirarchy = objclsDTOHeirarchy ;
        }

       public clsDTOHeirarchy clsDTOHeirarchy
       {
           get { return objclsDTOHeirarchy ; }
           set { objclsDTOHeirarchy  = value; }
       }
       public bool Insert()
       {
           bool blnRetValue = false;
           try
           {
               objClsConnection.BeginTransaction();
               objclsDALHeirarchy.objclsConnection = objClsConnection;
               objclsDALHeirarchy.Insert();
               objClsConnection.CommitTransaction();
               blnRetValue = true;
           }
           catch (Exception ex)
           {
               objClsConnection.RollbackTransaction();
               throw ex;
           }
           return blnRetValue;
       }

       public void Delete(int iHeirarchyID)
       {
           try
           {
               objClsConnection.BeginTransaction();
               objclsDALHeirarchy.objclsConnection = objClsConnection;
               objclsDALHeirarchy.Delete(iHeirarchyID);
               objClsConnection.CommitTransaction();
           }
           catch (Exception ex)
           {
               objClsConnection.RollbackTransaction();
               throw ex;
           }
       }

       public DataSet GetAllParents()
       {
           objclsDALHeirarchy.objclsConnection = objClsConnection;
           return objclsDALHeirarchy.GetAllParents();
       }

       public DataSet GetAllEmployees()
       {
           objclsDALHeirarchy.objclsConnection = objClsConnection;
           return objclsDALHeirarchy.GetAllEmployees();
       }

       public DataSet GetAllChildren(int iParentId)
       {
           objclsDALHeirarchy.objclsConnection = objClsConnection;
           return objclsDALHeirarchy.GetAllChildren(iParentId);
       }

       public DataTable GetAllTreeParents()
       {
           objclsDALHeirarchy.objclsConnection = objClsConnection;
           return objclsDALHeirarchy.GetAllTreeParents();
       }

       public DataTable GetAllNodes()
       {
           objclsDALHeirarchy.objclsConnection = objClsConnection;
           return objclsDALHeirarchy.GetAllNodes();
       }
     
    }
}
