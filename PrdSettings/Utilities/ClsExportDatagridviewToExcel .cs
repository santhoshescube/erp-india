﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;



public class ClsExportDatagridviewToExcel
    {

        public string ExcelFilePath ;
        [DllImport("Shell32dll",EntryPoint = "ShellexCuteA")]
        private static extern int ShellEx(int hWnd , string lpOperation , string lpFile , string lpParameters , string lpDirectory, int nShowCmd );

    /// <summary>
    ///  Attendance GridView Exporting
    /// </summary>
    /// <param name="grdView"> Exporting Grid View</param>
    /// <param name="fileName"> File name for excel</param>
    /// <param name="iStartCol"> strating column </param>
    /// <param name="iEndcol"> End Column</param>
        /// <param name="blnAttendanceStatusShow"> Show the status of attendance </param>
        public void ExportToExcel(System.Windows.Forms.DataGridView  grdView , string fileName 
             , int iStartCol , int iEndcol ,bool blnAttendanceStatusShow)
        {
            if (grdView.Rows.Count > 0)
            {
                // ' Choose the path, name, and extension for the Excel file
                string myFile = fileName;
                ExcelFilePath = myFile;
                // ' Open the file and write the headers
                // Dim fs As New IO.StreamWriter(myFile, False)
                StreamWriter fs = new StreamWriter(myFile, false);
                fs.WriteLine("<?xml version=\"1.0\"?>");

                fs.WriteLine("<?mso-application progid=\"Excel.Sheet\"?>");
                fs.WriteLine("<ss:Workbook xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\">");

                //' Create the styles for the worksheet
                fs.WriteLine("  <ss:Styles>");
                //' Style for the column headers
                fs.WriteLine("    <ss:Style ss:ID=\"1\">");
                fs.WriteLine("      <ss:Font ss:Bold=\"1\"/>");
                fs.WriteLine("      <ss:Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\" ss:WrapText=\"1\"/>");

                fs.WriteLine("      <ss:Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
                fs.WriteLine("    </ss:Style>");
                //' Style for the column information
                fs.WriteLine("    <ss:Style ss:ID=\"2\">");
                fs.WriteLine("      <ss:Alignment ss:Vertical=\"Center\" ss:WrapText=\"1\"/>");
                fs.WriteLine("    </ss:Style>");
                fs.WriteLine("  </ss:Styles>");

                //' Write the worksheet contents
                fs.WriteLine("<ss:Worksheet ss:Name=\"Sheet1\">");
                fs.WriteLine("  <ss:Table>");

                for (int i = iStartCol; i <= iEndcol; i++)//' grdView.Columns.Count - 1
                {
                    fs.WriteLine(String.Format("    <ss:Column ss:Width=\"{0}\"/>",
                             grdView.Columns[i].Width));
                }
                fs.WriteLine("    <ss:Row>");
                for (int j = iStartCol; j <= iEndcol; j++)// ' grdView.Columns.Count - 1
                {
                    fs.WriteLine(String.Format("      <ss:Cell ss:StyleID=\"1\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>",
                          grdView.Columns[j].HeaderText));
                }
                fs.WriteLine("    </ss:Row>");

                //' Check for an empty row at the end due to Adding allowed on the DataGridView
                int subtractBy = 0;
                string cellText = "";
                if (grdView.AllowUserToAddRows == true)
                    subtractBy = 2;
                else
                    subtractBy = 1;

                //' Write contents for each cell
                for (int k = 0; k <= grdView.RowCount - subtractBy; k++)
                {
                    fs.WriteLine(String.Format("    <ss:Row ss:Height=\"{0}\">", grdView.Rows[k].Height));
                    for (int intCol = iStartCol; intCol <= iEndcol; intCol++)// ' grdView.Columns.Count - 1
                    {
                        if (blnAttendanceStatusShow)
                        {
                            if (grdView.Columns[intCol].Name == "ColDgvStatus")
                            {
                                if (grdView[intCol, k].Value.ToStringCustom() != string.Empty)
                                {

                                    if (grdView[intCol, k].Value.ToInt32()==(int)AttendanceStatus.Present)
                                    {
                                    cellText = "Present";
                                    }
                                    else if (grdView[intCol, k].Value.ToInt32() == (int)AttendanceStatus.Rest)
                                    {
                                        cellText = "Rest";
                                    }
                                    else if (grdView[intCol, k].Value.ToInt32() == (int)AttendanceStatus.Leave)
                                    {
                                        cellText = "Leave";
                                    }
                                    else if (grdView[intCol, k].Value.ToInt32() == (int)AttendanceStatus.Absent)
                                    {
                                        cellText = "Absent";
                                    }
                                    else
                                    {
                                        cellText = "";
                                    }
                                }
                                else
                                {
                                    cellText = "";
                                }
                            }
                            else
                            { cellText = (Convert.ToString(grdView[intCol, k].Value)) == "" ? "" : Convert.ToString((grdView[intCol, k].Value)); }

                        }
                        else
                        {
                            cellText = (Convert.ToString(grdView[intCol, k].Value)) == "" ? "" : Convert.ToString((grdView[intCol, k].Value));
                        }
                        //' Check for null cell and change it to empty to avoid error
                        if (Convert.ToString(cellText) == "")
                            cellText = "";

                        fs.WriteLine(String.Format("      <ss:Cell ss:StyleID=\"2\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>", cellText.ToString()));
                    }
                    fs.WriteLine("    </ss:Row>");
                }

                //' Close up the document
                fs.WriteLine("  </ss:Table>");
                fs.WriteLine("</ss:Worksheet>");
                fs.WriteLine("</ss:Workbook>");
                fs.Close();
            }// if 
        // ' Open the file in Microsoft Excel
        // ' 10 = SW_SHOWDEFAULT
        // ' ShellEx(FormName.Handle, "Open", myFile, "", "", 10)
        }


    }
        



