﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Data;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Serialization;

/******************************************
 * Created By : Ratheesh
 * Creation Date : 18 June 2011
 * *****************************************/

public static class Extensions
{

    #region Object Extensions

    /// <summary>
    /// Converts to Int32. If conversion is not possible, returns 0
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static Int32 ToInt32(this object source)
    {
        Int32 Result = 0;
        if (source != null && source != DBNull.Value) Int32.TryParse(source.ToString(), out Result);
        return Result;
    }

    public static Int16 ToInt16(this object source)
    {
        Int16 Result = 0;
        if (source != null && source != DBNull.Value) Int16.TryParse(source.ToString(), out Result);
        return Result;
    }

    /// <summary>
    /// Converts to Double. If conversion is not possible, returns 0
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static Double ToDouble(this object source)
    {
        Double Result = 0;
        if (source != null && source != DBNull.Value) Double.TryParse(source.ToString(), out Result);
        return Result;
    }

    public static bool ToBoolean(this object source)
    {
        bool Result = false;
        if (source != null && source != DBNull.Value) bool.TryParse(source.ToString(), out Result);
        return Result;
    }


    /// <summary>
    /// Converts to Int64. If conversion is not possible, returns 0
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static Int64 ToInt64(this object source)
    {
        Int64 Result = 0;
        if (source != null && source != DBNull.Value) Int64.TryParse(source.ToString(), out Result);
        return Result;
    }

    /// <summary>
    /// Converts to decimal. If conversion is not possible, returns 0
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static decimal ToDecimal(this object source)
    {
        decimal Result = 0;
        if (source != null && source != DBNull.Value) decimal.TryParse(source.ToString(), out Result);
        return Result;
    }

    /// <summary>
    /// Returns true if null otherwise false.
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static bool IsNull(this object source)
    {
        return source == null;
    }

    /// <summary>
    /// Converts to DateTime. If conversion is not possible, returns DateTime.MinValue
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static DateTime ToDateTime(this object source)
    {
        DateTime Result = DateTime.MinValue;
        if (source != null && source != DBNull.Value) DateTime.TryParse(source.ToString(), out Result);
        return Result;
    }

    /// <summary>
    /// Returns true if object is decimal otherwise false.
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static bool IsDecimal(this object source)
    {
        decimal Result = 0M;
        return decimal.TryParse(source.ToString().Trim(), out Result);
    }

    /// <summary>
    /// Converts to string, if object is null or DBNull then returns empty string.
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static string ToStringCustom(this object source)
    {
        if (source == null || source == DBNull.Value)
            return string.Empty;

        return Convert.ToString(source).Trim();
    }

    #endregion

    #region String Extensions

    /// <summary>
    /// Capitalizes first letter of the string.
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static string ToSentenceCase(this string source)
    {
        if (string.IsNullOrEmpty(source))
            return source;

        return char.ToUpper(source[0]) + source.Substring(1).ToLower();
    }

    /// <summary>
    /// Converts to title case according to culture
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static string ToTitleCase(this string source)
    {
        if (string.IsNullOrEmpty(source))
            return string.Empty;

        return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(source.ToLower());
    }

    public static Int16 ToInt16(this string source)
    {
        Int16 Result = 0;
        if (source != null && source != string.Empty) Int16.TryParse(source.ToString(), out Result);
        return Result;
    }

    /// <summary>
    /// Converts to int32. if conversion is not possible, returns 0
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static Int32 ToInt32(this string source)
    {
        Int32 Result = 0;
        if (!string.IsNullOrEmpty(source)) Int32.TryParse(source.Trim().ToString(), out Result);
        return Result;
    }

    /// <summary>
    /// Converts to Int64. if conversion is not possible, returns 0
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static Int64 ToInt64(this string source)
    {
        Int64 Result = 0;
        if (!string.IsNullOrEmpty(source)) Int64.TryParse(source.Trim().ToString(), out Result);
        return Result;
    }

    /// <summary>
    /// Converts to DateTime. if string is invalid format, returns DateTime.MinValue
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static DateTime ToDateTime(this string source)
    {
        DateTime Result = DateTime.MinValue;
        if (!string.IsNullOrEmpty(source)) DateTime.TryParse(source.Trim().ToString(), out Result);
        return Result;
    }

    /// <summary>
    /// Converts to Decimal. if string is invalid number, returns 0
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static Decimal ToDecimal(this string source)
    {
        Decimal Result = 0M;
        if (!string.IsNullOrEmpty(source)) Decimal.TryParse(source.Trim().ToString(), out Result);
        return Result;
    }

    /// <summary>
    /// Returns true if string is decimal otherwise false.
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static bool IsDecimal(this string source)
    {
        decimal Result = 0M;
        return decimal.TryParse(source.Trim(), out Result);
    }

    /// <summary>
    /// Returns true if string is integer otherwise false.
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static bool IsInteger(this string source)
    {
        Int64 Result = 0;
        return Int64.TryParse(source.Trim(), out Result);
    }

    /// <summary>
    /// Replaces single quotes with ` and returns new string.
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static string EscapeSingleQuotes(this string source)
    {
        return string.IsNullOrEmpty(source) == false ? source.Replace("'", "`") : string.Empty;
    }

    /// <summary>
    /// returns true if input string is a valid email otherwise returns false.
    /// </summary>
    /// <param name="strEmail"></param>
    /// <returns></returns>
    public static bool IsValidEmail(this string strEmail)
    {
        return new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*").Match(strEmail).Success;
    }

    /// <summary>
    /// returns true if input string is a valid web url otherwise returns false.
    /// </summary>
    /// <param name="strWebURL">string to be checked</param>
    /// <returns></returns>
    public static bool IsValidWebURL(this string strWebURL)
    {
        bool success = new Regex(@"http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?").Match(strWebURL).Success;

        if (strWebURL.StartsWith("h"))
            return success;

        if (!strWebURL.StartsWith("www"))
            return false;

        else if (!success)
            success = new Regex(@"([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?").Match(strWebURL).Success;

        return success;
    }

    /// <summary>
    /// Removes double spaces from input string
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static string RemoveDoubleSpaces(this string source)
    {
        string returnValue = source;

        if (source != null)
        {
            // continue looping until input string does not have any double spaces
            while (returnValue.Contains("  "))
                // replace double spaces with single space.
                returnValue = returnValue.Replace("  ", " ");
        }

        return returnValue == null ? string.Empty : returnValue;
    }

    #endregion

    #region Double Extensions

    /// <summary>
    /// Formats double value to string with specified scaling.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="scale"> Number of digits for scaling </param>
    /// <returns></returns>
    public static string Format(this double source, int scale)
    {
        if (scale < 0) scale = 0;
        return string.Format("{0:F" + scale + "}", source);
    }

    /// <summary>
    /// Converts to currency format with specified scaling including thousand seperator
    /// </summary>
    /// <param name="source"></param>
    /// <param name="scale">precision</param>
    /// <returns>formated string</returns>
    public static string CurrencyFormat(this double source, int scale)
    {
        if (scale < 0) scale = 0;
        return string.Format("{0:N" + scale + "}", source);
    }

    #endregion

    #region Decimal Extensions

    /// <summary>
    /// Formats decimal to string with specified scaling
    /// </summary>
    /// <param name="source"></param>
    /// <param name="scale">number digits after decimal point</param>
    /// <returns></returns>
    public static string Format(this decimal source, int scale)
    {
        if (scale < 0) scale = 0;
        return string.Format("{0:F" + scale + "}", source);
    }

    /// <summary>
    /// Converts to currency format with specified scaling
    /// </summary>
    /// <param name="source"></param>
    /// <param name="scale"></param>
    /// <returns></returns>
    public static string CurrencyFormat(this decimal source, int scale)
    {
        if (scale < 0) scale = 0;
        return string.Format("{0:N" + scale + "}", source);
    }

    #endregion

    /// <summary>
    /// Converst Generic IList<T> to equivalent DataTable
    /// </summary>
    /// <typeparam name="T">Type of individual item in the IList</typeparam>
    /// <param name="list"></param>
    /// <returns></returns>
    public static DataTable ToDataTable<T>(this IList<T> list, string DataTableName)
    {
        PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
        DataTable table = new DataTable(DataTableName);

        foreach (PropertyDescriptor property in properties)
            table.Columns.Add(property.Name
                    , (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    ? Nullable.GetUnderlyingType(property.PropertyType) : property.PropertyType
                   );

        foreach (T item in list)
        {
            DataRow row = table.NewRow();

            foreach (PropertyDescriptor property in properties)
                row[property.Name] = property.GetValue(item) ?? DBNull.Value;

            table.Rows.Add(row);
        }

        return table;
    }

    /// <summary>
    /// Converts Generic IList<T> to xml string
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <returns></returns>
    public static string ToXml<T>(this IList<T> list)
    {
        if (list == null)
            return string.Empty;

        XmlDocument xmlDoc = new XmlDocument();
        XPathNavigator navigator = xmlDoc.CreateNavigator();

        using (XmlWriter writer = navigator.AppendChild())
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>), new XmlRootAttribute("Root"));
            serializer.Serialize(writer, list);
        }

        // remove xml namespace declaration from root element's attributes.
        if (xmlDoc.DocumentElement.HasAttributes)
            xmlDoc.DocumentElement.RemoveAllAttributes();

        return xmlDoc.InnerXml;
    }

    public static string ToXml2<T>(this IList<T> list)
    {
        if (list == null)
            return string.Empty;

        XmlDocument xmlDoc = new XmlDocument();
        XPathNavigator navigator = xmlDoc.CreateNavigator();

        using (XmlWriter writer = navigator.AppendChild())
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>));
            serializer.Serialize(writer, list);
        }

        // remove xml namespace declaration from root element's attributes.
        if (xmlDoc.DocumentElement.HasAttributes)
            xmlDoc.DocumentElement.RemoveAllAttributes();

        return xmlDoc.InnerXml;
    }

}

