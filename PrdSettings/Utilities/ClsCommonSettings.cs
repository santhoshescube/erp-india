﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class ClsCommonSettings
{
    public static string strEncryptionKey = "MAKE-IT-POSSIBLE";
    public static bool AttendanceAutofillForSinglePunch { get { return true; } }
    public static int CompanyID { get; set; }
    public static int LoginCompanyID { get; set; }
    public static string LoginCompany { get; set; }
    public static DateTime FinYearStartDate { get; set; }
    public static DateTime FinYearEndDate { get; set; }
    public static int CurrencyID { get; set; }
    public static string Currency { get; set; }
    public static int intEmployeeID { get; set; }
    public static int UserID { get; set; }    
    public static int RoleID { get; set; }

    public static int ModuleID { get; set; }
    public static int ProductID { get { return 9; } }
    public static int TimerInterval { get; set; }
    public static int Scale { get; set; }
    public static bool blnProductWiseReporting { get; set; }
    public static string ProductImageUrl { get; set; }
    public static bool AttendanceLiveEnable { get; set; }
    public static int intBackupInterval { get; set; }
    public static int DefaultStatusBarMessageTime { get; set; }
    public static int PintFirstInstallmentInterval { get; set; }
    public static int PintInstallmentInterval { get; set; }
    public static int PintInstallmentStartFrom { get; set; }
    public static int PintNoOfInstallments { get; set; }
    public static int PintSICreditHeadID { get; set; }
    public static int PintSIDebitHeadID { get; set; }
    public static int PintReceiptsCreditHeadID { get; set; }
    public static int PintPaymentDebitHeadID { get; set; }
    public static int intDepartmentID { get; set; }

    public static bool SendMailToAll { get; set; }
    public static bool ShowErrorMess { get; set; }
    public static bool PblnIsTaxable { get; set; }
    public static bool PblnIsLocationWise { get; set ; }
    public static bool PblnAutoGeneratePartyAccount { get; set; }
    public static bool PblnIsCustLoginRequired { get; set; }
    public static bool blnBatchEnabled { get { return false; } }

    public static string DbName { get; set; }
    public static string strCompanyName { get; set; }
    public static string strProductSelectionURL { get; set; }
    public static string strUserName { get; set; }
    public static string strEmployeeName { get; set; }
    public static string ServerName { get; set; }
    public static string MessageCaption { get; set; }
    public static string ReportFooter { get; set; }
    public static string strSQPrefix { get; set; }
    public static bool blnSQAutogenerate { get; set; }
    public static string strSQDueDateLimit { get; set; }
    public static bool SQApproval { get; set; }
    public static string strSOPrefix { get; set; }
    public static string strSODueDateLimit { get; set; }
    public static bool SOApproval { get; set; }
    public static bool blnSOAutogenerate { get; set; }
    public static string strSIPrefix { get; set; }
    public static bool blnSIAutogenerate { get; set; }
    public static string strSIDueDateLimit { get; set; }
    public static string strSRPrefix { get; set; }
    public static bool blnSRAutogenerate { get; set; }
    public static string strPOSPrefix { get; set; }
    public static bool blnPOSAutogenerate { get; set; }
    public static string PIPrefix { get; set; }
    public static bool PIAutogenerate { get; set; }
    public static string PIDueDateLimit { get; set; }
    public static string PQPrefix { get; set; }
    public static bool PQAutogenerate { get; set; }
    public static string PQDueDateLimit { get; set; }
    public static bool PQApproval { get; set; }
    public static string POPrefix { get; set; }
    public static bool POAutogenerate { get; set; }
    public static string PODueDateLimit { get; set; }
    public static bool POApproval { get; set; }
    public static string PGRNPrefix { get; set; }
    public static bool PGRNAutogenerate { get; set; }
    public static string PMaterialReturnPrefix { get; set; }
    public static bool PMaterialReturnAutogenerate { get; set; }
    public static string PINVPrefix { get; set; }
    public static bool PINVAutogenerate { get; set; }
    public static string PRPrefix { get; set; }
    public static bool PRAutogenerate { get; set; }
    public static string PaymentsPrefix { get; set; }
    public static bool PaymentsAutogenerate { get; set; }
    public static string ReceiptsPrefix { get; set; }
    public static bool ReceiptsAutoGenerate { get; set; }
    public static string DeliverySchedulePrefix { get; set; }
    public static bool DeliveryScheduleAutogenerate { get; set; }
    public static string CollectionSchedulePrefix { get; set; }
    public static bool CollectionScheduleAutogenerate {get;set;}
    public static string ComplaintSchedulePrefix { get; set; }
    public static bool ComplaintScheduleAutogenerate { get; set; }
    public static string ItemIssuePrefix { get; set; }
    public static bool ItemIssueAutogenerate { get; set; }
    public static string MaterialIssuePrefix { get; set; }
    public static bool MaterialIssueAutogenerate { get; set; }
    public static string ExtraItemIssuePrefix { get; set; }
    public static string InstallmentCollectionPrefix { get; set; }
    public static bool InstallmentCollectionAutogenerate { get; set; }
    public static string ComplaintRegistrationPrefix { get; set; }
    public static bool ComplaintRegistrationAutogenerate { get; set; }
    public static string strCustomerCodePrefix { get; set; }
    public static bool blnCustomerAutogenerate { get; set; }
    public static string strSupplierCodePrefix { get; set; }
    public static bool blnSupplierAutogenerate { get; set; }
    public static string strProductCodePrefix { get; set; }
    public static bool blnProductCodeAutogenerate { get; set; }
    public static string strProductGroupCodePrefix { get; set; }
    public static bool blnProductGroupCodeAutogenerate { get; set; }
    public static string strEmployeeNumberPrefix { get; set; }
    public static bool blnEmployeeNumberAutogenerate { get; set; }
    public static string strAgentBankCodePrefix { get; set; }
    public static string strContracterCodePrefix { get; set; }
    public static string strPlanningPrefix { get; set; }
    public static bool blnPlanningAutogenerate { get; set; }
    public static string strServerPath { get; set; }
    public static string strStockAdjustmentNumberPrefix { get; set; }
    public static bool blnStockAdjustmentNumberAutogenerate { get; set; }
    public static string strRFQNumber { get; set; }
    public static bool blnRFQAutogenerate { get; set; }
    public static string strRFQDueDateLimit { get; set; }
    public static bool RFQApproval { get; set; }
    public static byte InstallmentReminderInerval { get; set; }
    public static System.Drawing.Color MandatoryColor { get { return System.Drawing.SystemColors.Info; } }
    public static string strWareHouseLocationPrefix { get; set; }
    public static string strWareHouseRowPrefix { get; set; }
    public static string strWareHouseBlockPrefix { get; set; }
    public static string strWareHouseLotPrefix { get; set; }
    public static string strCollectionRecoveryPrefix { get; set; }
    public static bool blnSendSms { get; set; }
    public static string strProjectCodePrefix { get; set; }
    public static bool StockTransferApproval { get; set; }
    public static string strStockTransferNo { get; set; }
   // public static bool payExists { get; set; }
    public static bool Glb24HourFormat { get; set; }
    public static bool GlbSalaryDayIsEditable { get; set; }
    public static bool GlSalarySlipIsEditable { get; set; }
    public static bool GlDisplayLeaveSummaryInSalarySlip { get; set; }
    public static bool PblnIncludeProduction { get; set; }

    public static DateTime GetServerDate()
    {
        DataLayer objDataLayer = new DataLayer();
        object objServerDate = objDataLayer.ExecuteScalar("spGetServerDate");
        return Convert.ToDateTime(objServerDate).Date;
    }

    public static DateTime GetServerDateTime()
    {
        DataLayer objDataLayer = new DataLayer();
        object objServerDate = objDataLayer.ExecuteScalar("spGetServerDate");
        return Convert.ToDateTime(objServerDate);
    }
    public class clsDataBaseCredentials
    {
        /// <summary>
        /// Gets or sets database server name or IP address
        /// </summary>
        public string ServerName { get; set; }
        /// <summary>
        /// Gets  password for database connectivity
        /// </summary>
        public string Password { get { return "camMS!234"; } }
        /// <summary>
        /// Gets  user ID for database connectivity
        /// </summary>
        public string UserID { get { return "camadmin"; } }
        /// <summary>
        /// Gets or sets database name to which we need to connect
        /// </summary>
        public string DataBaseName { get; set; }

        /// <summary>
        /// Gets full connection string
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return string.Format("Server={0};Database={1};Uid={2};Pwd={3};Connect Timeout=0",
                        this.ServerName,
                        this.DataBaseName,
                        this.UserID,
                        this.Password
                    );
            }
        }
    }
}
