﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsReference
/// Author Ratheesh
/// Created Date : 04-03-2011
/// </summary>
/// 
public class clsReference : DataLayer
{
    public clsReference()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public int insert(string TableName, string DataTextField, string Value, int iOperationModeID, string PredefinedColumnName, int iAddition, string sFieldValue, string sFieldName)
    {
        try
        {
            ArrayList alparam = new ArrayList();
            alparam.Add(new SqlParameter("@Mode", "insert"));
            alparam.Add(new SqlParameter("@TableName", TableName));
            alparam.Add(new SqlParameter("@DataTextField", DataTextField));
            alparam.Add(new SqlParameter("@Value", Value));
            alparam.Add(new SqlParameter("@OperationModeID", iOperationModeID));
            alparam.Add(new SqlParameter("@PredefinedColumnName", PredefinedColumnName));
            alparam.Add(new SqlParameter("@Addition", iAddition));
            alparam.Add(new SqlParameter("@Id", sFieldValue));
            alparam.Add(new SqlParameter("@DataValueField", sFieldName));

            return Convert.ToInt32(ExecuteScalar("STReference", alparam));
        }
        catch (Exception)
        {
            return -1;
        }
    }
    public int Update(string TableName, string DataTextField, string Value, string DataValueField, int Id, string sFilterColumn, string sFilterId, string PredefinedColumnName)
    {
        try
        {
            ArrayList alparam = new ArrayList();
            alparam.Add(new SqlParameter("@Mode", "update"));
            alparam.Add(new SqlParameter("@TableName", TableName));
            alparam.Add(new SqlParameter("@DataTextField", DataTextField));
            alparam.Add(new SqlParameter("@Value", Value));
            alparam.Add(new SqlParameter("@DataValueField", DataValueField));
            alparam.Add(new SqlParameter("@FilterColumn", sFilterColumn));
            alparam.Add(new SqlParameter("@FilterValue", sFilterId));
            alparam.Add(new SqlParameter("@PredefinedColumnName", PredefinedColumnName));

            alparam.Add(new SqlParameter("@Id", Id));
            return Convert.ToInt32(ExecuteScalar("STReference", alparam));
        }
        catch (Exception)
        {
            return -1;
        }
    }
    public int Delete(string TableName, string DataValueField, string PredefinedColumnName, int Id)
    {
        try
        {
            ArrayList alparam = new ArrayList();
            alparam.Add(new SqlParameter("@Mode", "delete"));
            alparam.Add(new SqlParameter("@TableName", TableName));
            alparam.Add(new SqlParameter("@DataValueField", DataValueField));
            alparam.Add(new SqlParameter("@Id", Id));
            alparam.Add(new SqlParameter("@PredefinedColumnName", PredefinedColumnName));
            return ExecuteNonQuery("STReference", alparam);            
        }
        catch (Exception)
        {
            return -1;
        }
    }
    public DataSet Select(string TableName, string DataTextField, string DataValueField, bool OrderByDataTextField,int iAddition)
    {
        ArrayList alparam = new ArrayList();
        alparam.Add(new SqlParameter("@Mode", "selectall"));
        alparam.Add(new SqlParameter("@TableName", TableName));
        alparam.Add(new SqlParameter("@DataTextField", DataTextField));
        alparam.Add(new SqlParameter("@Addition", iAddition));
        if (OrderByDataTextField)
            alparam.Add(new SqlParameter("@OrderField", DataTextField));
        else
            alparam.Add(new SqlParameter("@OrderField", DataValueField));

        return ExecuteDataSet("STReference", alparam);
    }
    public DataSet Select(string TableName, string sFieldName, string sFieldValue)
    {
        ArrayList alparam = new ArrayList();
        alparam.Add(new SqlParameter("@Mode", "select"));
        alparam.Add(new SqlParameter("@TableName", TableName));
        alparam.Add(new SqlParameter("@DataValueField", sFieldName));
        alparam.Add(new SqlParameter("@Id", sFieldValue));
        return ExecuteDataSet("STReference", alparam);
    }
}
