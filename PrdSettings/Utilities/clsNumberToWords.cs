﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numeric;

public class ClsNumberToWords
{
    const string strNum0 = "Zero";
    const string strNum1 = "One";
    const string strNum2 = "Two";
    const string strNum3 = "Three";
    const string strNum4 = "Four";
    const string strNum5 = "Five";
    const string strNum6 = "Six";
    const string strNum7 = "Seven";
    const string strNum8 = "Eight";
    const string strNum9 = "Nine";
    const string strNum10 = "Ten";

    const string strNum11 = "Eleven";
    const string strNum12 = "Twelve";
    const string strNum13 = "Thirteen";
    const string strNum14 = "Fourteen";
    const string strNum15 = "Fifteen";
    const string strNumteen = "teen";

    const string strNum20 = "Twenty";
    const string strNum30 = "Thirty";
    const string strNum40 = "Fourty";
    const string strNum50 = "Fifty";
    const string strNum60 = "Sixty";
    const string strNum70 = "Seventy";
    const string strNum80 = "Eighty";
    const string strNum90 = "Ninety";

    public enum PaddingEnum
    {
        PaddingCommas,
        PaddingSpaces,
        PaddingDashes
    }





    public enum DelimitEnum
    {
        DelimiterNone,
        Delimiter1Asterisk,
        Delimiter3Asterisks
    }


    public enum CaseEnum
    {
        CaseCaps,
        CaseLower,
        CaseUpper
    }

    //local variable(s) to hold property value(s)
    private CaseEnum varUseCase;//local copy
    private DelimitEnum varUseDelimiter;//local copy
    private PaddingEnum varUsePadding;//local copy
    // --------------------------------------------------------------
    public string Num2String(decimal number, string sCurrencyMain, string sCurrencySub, int iScale, Boolean bWholeword)
    {
        decimal biln;
        decimal miln;
        decimal thou;
        decimal hund;
        string strNumber = "";

        if ((Convert.ToDouble(number) > 999999999999.99))
        {

            return "***";
        }






        biln = Math.Floor(number / 1000000000);

        if (biln > 0) { strNumber = FormatNum(biln) + " Billion" + Pad(); }

        miln = Math.Floor((number - biln * 1000000000) / 1000000);


        if (miln > 0) { strNumber = strNumber + FormatNum(miln) + " Million" + Pad(); }
        thou = Math.Floor((number - biln * 1000000000 - miln * 1000000) / 1000);

        if (thou > 0) { strNumber = strNumber + FormatNum(thou) + " Thousand" + Pad(); }
        hund = Math.Floor(number - biln * 1000000000 - miln * 1000000 - thou * 1000);
        if (hund > 0) { strNumber = strNumber + FormatNum(hund); }
        if (strNumber.Substring(strNumber.Length - 1) == ",") { strNumber = strNumber.Substring(0, (strNumber.Length) - 1); }
        if (strNumber.Substring(0, 1) == ",") { strNumber = strNumber.Substring(strNumber.Length - (strNumber.Length - 1)); }
        if (number != Math.Floor(number))
        {

            if ((iScale == 1) && (bWholeword != true))
            {
                strNumber = strNumber + FormatDecimal((number - Math.Floor(number)) * 10, sCurrencyMain, sCurrencySub);
            }
            else if ((iScale == 2)&&(bWholeword != true))
            {
                strNumber = strNumber + FormatDecimal((number - Math.Floor(number)) * 100, sCurrencyMain, sCurrencySub);
            }
            else if ((iScale == 3) && (bWholeword != true))
            {
                strNumber = strNumber + FormatDecimal3((number - Math.Floor(number)) * 1000, sCurrencyMain, sCurrencySub);
            }
            else if ((iScale == 3) && (bWholeword = true))//returning the whole word ex:100.345=one hundred rupees three four five Paise
            {

                string sNum;
                string sNum1 = Convert.ToString(number);
                sNum = sNum1.Substring(sNum1.IndexOf(".", 0) + 1);
                if ((sNum.Length) < 3) { sNum = sNum + 0; }
                strNumber = strNumber + FormatDecimal3whole(sNum, sCurrencyMain, sCurrencySub);
            }
           } 
        else
            {//   ' the following loop is necessary for the proper conversion
                //' of amount like 32,100,000 or 1,000,000,000.
                while (strNumber.EndsWith(", "))
                {
                    strNumber = strNumber.Substring(0, strNumber.Length - 2) + " ";
                }
                strNumber = strNumber + "  " + sCurrencyMain;//'" DOLLARS"
            }
            
        
        return Delimit(SetCase(strNumber));
    }


    public PaddingEnum Paddings
    {
        get { return varUsePadding; }
        set { varUsePadding = value; }
    }






    public DelimitEnum Delimiters
    {
        get { return varUseDelimiter; }

        set { varUseDelimiter = value; }
    }



    public CaseEnum Cases
    {
        get { return varUseCase; }

        set { varUseCase = value; }
    }
    private string FormatNum(decimal num)
    {
        decimal digit100; decimal digit10;
        string strNum = "";

        digit100 = Math.Floor(num / 100);
        if (digit100 > 0) { strNum = Format100(digit100); }
        digit10 = Math.Truncate((num - digit100 * 100));
        if (digit10 > 0)
        {
            if (strNum != "")
            {
                strNum = strNum + " And " + Format10(digit10);
            }
            else
            {
                strNum = Format10(digit10);
            }

        }
        return strNum;
    }

    private string Format100(decimal num)
    {
        string str100;
        str100 = Format1(num) + " Hundred";
        return str100;
    }

    private string Format10(decimal num)
    {
        string strNum = "";
        if (num < 10)
        {
            //Format10 = Format1(num)
            return Format1(num);
        }
        if (num < 20)
        {
            switch ((int)num)
            {
                case 10:
                    //Format10 = strNum10
                    return strNum10;
                case 11:
                    //Format10 = strNum11
                    return strNum11;
                case 12:
                    return strNum12;
                case 13:
                    //Format10 = strNum13
                    return strNum13;
                case 14:
                    return strNum14;
                case 15:
                    return strNum15;
                default:
                    return (Format1(num % 10) + strNumteen);
            }
        }

        else
        {
            //switch (Math.Floor(num / 10) * 10)
            decimal num1 = num;
            switch(Convert.ToInt32(Math.Floor(num/10)*10))
            {
                case 20: strNum = strNum20;
                    break;
                case 30: strNum = strNum30;
                    break;
                case 40: strNum = strNum40;
                    break;
                case 50: strNum = strNum50;
                    break;
                case 60: strNum = strNum60;
                    break;
                case 70: strNum = strNum70;
                    break;
                case 80: strNum = strNum80;
                    break;
                case 90: strNum = strNum90;
                    break;
            }
            //'Dim s As String
            //'s = strNum & " " & Format1(num Mod 10)
            return (strNum + " " + Format1(num % 10));
        }
    }
   
   private string Format1(decimal num)
   {
       switch ((int)num)
       {
           case 1: return strNum1;
           case 2: return strNum2;
           case 3: return strNum3;
           case 4: return strNum4;
           case 5: return strNum5;
           case 6: return strNum6;
           case 7: return strNum7;
           case 8: return strNum8;
           case 9: return strNum9;
           default: return null;
       }
   }
    private string FormatDecimal(decimal num  , string scur, string subcur ) 
    {
        return (" " + scur + " and " + Format10(num) + "  " + subcur);
    }
    private string FormatDecimal3(decimal num, string scur, string subcur)
    {
        decimal Num100; decimal Num10;
        string strNum = "";

        Num100 = Math.Floor(num / 100);
        if (Num100 > 0) { strNum = Format100(Num100); }
        Num10 = Math.Truncate((num - Num100 * 100));
        if (Num10 > 0)
        {
            if (strNum != "")
            {
                strNum = strNum + " And " + Format10(Num10);
            }
            else
            { strNum = Format10(Num10); }

        }

        return " " + scur + " and " + strNum + "  " + subcur;
    }


    private string FormatDecimal3whole(string num, string scur, string subcur)
    {
        decimal Num100; decimal Num10;
        string sNum, Snum1;
        string strNum = "";
        sNum = num;
        Num100 = Convert.ToDecimal(sNum.Substring(0, 1));
        if (Num100 > 0)
        {
            strNum = Format1(Num100);
        }
        else
        {
            Num100 = 0;
            strNum = strNum0;
        }
        Num10 = Convert.ToDecimal(sNum.Substring(1, 1));
        if (Num10 > 0)
        {

            strNum = strNum + " " + Format1(Num10);
        }
        else
        {
            Num10 = 0;
            strNum = strNum + " " + strNum0;
        }
        if (sNum.Length != 2) { Snum1 = (string)(sNum.Substring(2, 1)); }//newly added
        else { Snum1 = (string)(sNum.Substring(1, 1)); }//modified
        if (Snum1 == "0")
        {
            Snum1 = "0";
            strNum = strNum + " " + strNum0;
        }
        else
        {
            strNum = strNum + " " + Format1(Convert.ToDecimal(Snum1));
        }


        return " " + scur + " and " + strNum + "  " + subcur;
    }

    private string SetCase(string nString)
    {
        switch (varUseCase)
        {
            case CaseEnum.CaseUpper: return (nString.ToLower());
            case CaseEnum.CaseLower: return nString.ToUpper();
            case CaseEnum.CaseCaps: return nString;
            default: return null;
        }
    }
    private string Pad()
    {
        switch (varUsePadding)
        {
            case PaddingEnum.PaddingSpaces: return " ";
            case PaddingEnum.PaddingDashes: return "-";
            case PaddingEnum.PaddingCommas: return ", ";
            default: return null;
        }
    }
    private string Delimit(string nString)
    {
        switch (varUseDelimiter)
        {
            case DelimitEnum.DelimiterNone: return nString;
            case DelimitEnum.Delimiter1Asterisk: return "*" + nString + "*";
            case DelimitEnum.Delimiter3Asterisks: return "***" + nString + "***";
            default: return null;
        }
    }


}

