﻿using System;

    /*****************************************************************************************************
     *  Author          : Ratheesh
     *  Creation Date   : 7 Mar 2011
     *  Description     : Defines a default value that represents null value for the respective datatype.
     *****************************************************************************************************/

    public sealed class clsConstants
    {
        public static string NullString = string.Empty;

        public static int NullInt = 0;

        public static double NullDouble = 0;

        public static float NullFloat = 0;

        public static long NullLong = 0;

        public static DateTime NullDate = DateTime.MinValue;

        public static decimal NullDecimal = 0;
        
    }

