﻿/// <summary>
/// Enumerator variables for Form Identifications
/// </summary>
public enum ConfigurationItem
{
    SalesproEnabled
}
public enum FormID
{
    CommonMessage = 1,
    CompanyInformation = 2,     // 201-250
    VendorInformation = 3,      // 301- 350
    CustomerInformation = 4,
    Employee = 5,               // 401 - 450
    CompanySettings = 6,        // 451 - 500
    FinancialYear = 7,          // 501 - 550
    Currency = 8,               // 551 - 600
    Warehouse = 9,              // 601 - 650
    BankInformation = 10,       // 651 - 700
    BarCodeCreation = 11,       // 700 - 750
    BarCodeReaderIntegration = 12,  // 751 - 800
    CreditCardSwiping = 13,     // 801 - 850
    RfIDTags = 14,              // 851 - 900
    Purchase = 15,              // 901 - 1000
    Installments = 16,          //1001 - 1050  
    Documents = 17,             // 1051 - 1100
    GoodsReceiptNote = 18,
    Kitting = 19,
    StockAdjustment = 20,
    StockTransfer = 21,
    ItemMaster = 22,            //1601 - 1650
    VendorHistory = 23,
    CustomerHistory = 24,
    OpeningStock = 25,
    CurrentStock = 26,
    SalesQuotation = 27,
    SalesInvoice = 28,
    DespatchSales = 29,
    SalesReturn = 30,
    PointOfSales = 31,
    AccountsEntry = 32,
    JournalEntry = 33,
    CreditDebitNote = 34,
    BankReconciliation = 35,
    BankAcounts = 36,
    Vouchers = 37,
    GeneralLedger = 38,
    TrialBalance = 39,
    ProftAndLoss = 40,
    Configuration = 41,
    UnitsOfMeasurement = 42,
    UOMClass = 43,
    UOMSetup = 44,
    PricingScheme = 45,
    DiscountType = 46,
    UnitConversion = 47,
    TaxCodes = 48,
    PaymentTerms = 49,
    User = 50,
    RoleSettings = 51,
    StockVerification = 52,
    ItemRejection = 53,
    ChequeAllotment = 54,
    ChequeIssue = 55,
    ChequeReceipt = 56,
    AccountsPayable = 57,
    AccountsReceivable = 58,
    MainFormRibbon = 59,
    AccountSettings = 60,   // 3951 - 4000
    EmailSettings = 61,     // 4001 - 4050
    EmailPopup = 62,   // 4051 - 4100
    employee = 63,        //4101 - 4150
    Expense = 64,       //7251 - 7300
    AccountSummary = 65,
    ItemAllocation = 67,    //7200-7250
    Scanning = 68, // 7301-7330
    Payments = 69, // 7331 - 7360
    PurchaseIndent = 70,
    PurchaseQuotation = 71,
    PurchaseOrder = 72,
    PurchaseGRN = 73,
    PurchaseInvoice = 74,
    Receipts = 75,
    DeliverySchedule = 76, // 4151 - 4200
    ItemIssue = 77, // 4201-4256
    SalesOrder = 78,
    ExchangeCurrency = 79, // 7361 -7381
    DeliveryStatusUpdation = 82, //4251-4265
    CollectionScheduling = 83, //4266 - 4280
    InstallmentCollection = 84, // 4281-4300
    ItemGroup = 85,
    AlertSetting = 86,
    Approval = 87, //8700-
    ComplaintsRegistration = 88,//4301-4320
    ColectionStatusUpdation = 89,
    ComplaintScheduling = 90,//4321-4340
    ComplaintStatusUpdation = 91, // 4341-4350
    PlanningSubmission = 92,
    RFQ = 93,
    Vehicle = 94,
    ChangePassword = 95,
    MandatoryDocumentTypes = 96, // 9471 - 9480
    TermsAndConditions = 97,//111
    CollectionRecovery = 98,
    LocationTransfer = 99, //item allocation- msg-9550-9600
    GeneralReceiptsAndPayments = 100,
    Projects = 101,
    Summary = 102,
    DebitNote = 103,
    SiteVisit = 104,//701
    LeavePolicy = 105,
    ChartOfAccounts = 106,
    ShiftPolicy = 107, // 201-250
    WorkPolicy = 108, // 201-250
    RecurrentSetup = 109,
    MaterialRequest = 110,
    OvertimePolicy = 111, //8000-8090
    AbsentPolicy = 112,
    HolidayCalendar = 113,
    JobOrder = 114,
    AdditionDeduction = 115, //8100 -8130
    SalaryProcess = 116, //8150-
    StageWiseProductCompletion = 117,//720
    HolidayType = 118,
    SalaryAdvance = 119,
    OpeningBalance = 120,
    Attendance = 121,
    SalaryStructure = 122,  //9600 - 9700
    JournalRecurrenceSetup=123,
    AttendanceDeviceSttings=124,
    AttendanceMapping=125,
    AttendanceOtTemplate=126,
    AttendanceUpdate=127,
    LeaveFromAttendance=128,
    SalaryRelease=129, // 1800
    SalaryPayment=130,  // 1850
    ProductionProcess=131, //1500-1520
    AccountsFormReport = 132,
    AttendanceReport=133, //Attendance Summary report 9700 -9710
    SalaryStructureMISReport =134,
    EmployeeProfileMISReport = 135,
    PaymentsMISReport = 136,
    SalarySlipMISReport = 137,
    LeaveSummaryMISReport = 138,
    SalaryAdvanceSummaryReport=139,
    ExpenseMISReport = 140,
    MaterialIssue = 141,
    MaterialReturn = 142,
    CompanywiseProduction = 143,
    AgeingReport = 144,
    ProductMovementReport = 145,
    ItemwiseProfitReport = 146,
    DirectGRN       = 147,
    VacationPolicy = 148,
    SettlementPolicy = 149,
    VacationEntry=150,
    SettlementProcess=151,
    ProductAssemblyReport = 152,
    PendingDeliveryReport = 153,
    VendorPriceHistoryReport = 154,
    ExtraCharges = 155,
    ExtraChargePayment = 156,
    GroupJV = 157,
    ProductionTemplate = 158,
    EmployeeLeaveStructure =159,
    AddParicularsToAll=160,
    EncashPolicy=161,
    UnearnedPolicy=162,
    DeductionPolicy=163,
    HolidayPolicy=164,
    LeaveOpening=165,
    LeaveEntry=166,
    LeaveExtension=167,
    PackingList = 168,
    CommercialInvoice = 169
}
public enum PaymentTransactionType
{
    Bank = 1,
    Cash = 2
    //Cheque = 3,
    //DD = 4,
    //Installment = 5
}
public enum EmailFormID
{
    Company = 1,
    Bank = 2,
    Employee = 3,
    Vendor = 4,
    Warehouse = 5,
    UnitsOfMeasurement = 6,
    TaxScheme = 7,
    DiscountNormal = 8,
    Discountitemwise = 22,
    Discountcustomerwise = 23,
    PaymentTerms = 9,
    PricingScheme = 10,
    AccountSettings = 11,
    UOMConversion = 12,
    Configuration = 13,
    CompanySettings = 14,
    ChartOfAccounts = 15,
    RoleSettings = 16,
    VendorHistory = 17,
    CustomerHistory = 18,
    AccountsReceivable = 19,
    ItemGroup = 20,
    ItemBatch = 21,
    Expense = 24,
    Payments = 25,
    PurchaseIndent = 26,
    PurchaseQuotation = 27,
    PurchaseOrder = 28,
    GRN = 29,
    PurchaseInvoice = 30,
    Product = 31,
    Receipts = 32,
    Installments = 33,
    Documents = 34,
    SalesQuotation = 35,
    SalesOrder = 36,
    SalesInvoice = 37,
    ExchangeCurrency = 38,
    DeliverySchedule = 39,
    DeliveryStatusUpdation = 40,
    ItemIssue = 41,
    CollectionSchedule = 42,
    InstallmentCollection = 43,
    OpeningStock = 44,
    ColectionStatusUpdation = 45,
    AlertSettings = 46,
    ComplaintRegistration = 47,
    ComplaintScheduling = 48,
    ComplaintStatusUpdation = 49,
    SalesReturn = 50,
    PlanSubmission = 51,
    RFQ = 52,
    StockAdjustment = 53,
    TermsAndConditions = 54,
    Projects = 55,
    StockTransfer = 56,
    GeneralReceiptsAndPayments = 57,
    Summary = 58,
    DebitNote = 59,
    PickList = 60,
    PointofSale = 61,
    SalaryStructure =62,
    ShiftPolicy =63,
    WorkPolicy =64,
    LeavePolicy =65,
    SalaryRelease=66,
    SalaryPayment=67,
    MaterialIssue = 68,
    MaterialReturn = 69,
    DirectGRN = 70,
    ExtraCharges = 71,
    ExtraChargePayment = 72,
    GroupJV = 73,
    SettlementProcess=74,
    VacationProcess=75,
    LeaveStructure=76,
    DeductionPolicy=77,
    LeaveEntry=78,
    LeaveExtension=79
}

public enum eMessageType
{
    Error = 1,
    Warning = 3,
    Information = 2,
    Question = 4
}

public enum JournalTypes
{
    Direct = 1,
    Recurring = 2,
    Automated = 3
}

public enum EmployeeLeaveType
{
    Vacation = 0,
    Sick = 1,
    Casual = 2,
    Maternity = 3,
    RH = 4
}

public enum LeaveExtensionType
{
    LeaveExtension = 1,
    TimeExtensionDay = 2,
    TimeExtensionMonth = 3

}
public enum LeaveRequestStatus
{
    Requested = 1,
    Approved = 2,
    Rejected = 3

}
public enum RFQStatus
{
    Opened = 103,
    Closed = 104,
    SubmittedForApproval = 105,
    Cancelled = 106,
    Rejected = 107,
    Approved = 108,
    Submitted = 128,
    Suggested = 135,
    Denied = 136
}

public enum OperationType
{
    PurchaseIndent = 1,
    PurchaseQuotation = 2,
    PurchaseOrder = 3,
    GRN = 4,
    PurchaseInvoice = 5,
    DebitNote = 6,
    SalesQuotation = 7,
    SalesOrder = 8,
    SalesInvoice = 9,
    POS = 10,
    CreditNote = 11,
    Stock = 12,
    Customers = 13,
    Suppliers = 14,
    Employee = 15,
    RFQ = 16,
    Alerts = 17,
    DeliveryNote = 18,
    ExtraCharges = 19,
    Receipt = 20,
    Payment = 21,
    StockTransfer = 22,
    Products = 23,
    StockAdjustment = 24,
    Approval = 25,
    Documents = 26,
    RecurringJournal = 27,
    SalaryAdvance = 28,
    SalaryRelease = 29,
    OpeningStock = 30,
    Voucher = 31,
    PostDatedCheque = 32,
    MaterialIssue = 33,
    MaterialReturn = 34,
}

public enum PaymentModes
{
    Bank = 1,
    Cash = 2,
    Cheque = 3,
    DD = 4
}

public enum AlertStatus
{
    Open = 60,
    Close = 61,
    Read = 67,
    Blocked = 68
}

public enum ReceiptsAndPaymentsModes
{
    PurchasePayments = 1,
    InventoryPayments = 2,
    SalesReceipts = 3
}

public enum eMenuID
{
    NewCompany = 1,
    Bank = 2,
    ExchangeCurrency = 3,
    Currency = 4,
    Employee = 5,
    Projects = 6,
    Customer = 7,
    Supplier = 8,
    Warehouse = 9,
    ChangePassword = 10,
    CompanySettings = 11,
    ConfigurationSetting = 12,
    EmailPopup = 13,
    Role = 14,
    TermsAndConditions = 15,
    User = 16,
    Backup = 17,
    SearchForm = 18,
    AlertSettings = 19,
    AlertRoleSettings = 20,
    RawMaterials = 21,
    Products = 22,
    ProductGroups = 23,
    OpeningStock = 24,
    StockAdjustment = 25,
    StockTransfer = 26,
    ItemAllocation = 27,
    ItemSummary = 28,
    PurchaseIndent = 29,
    RFQ = 30,
    PurchaseQuotation = 31,
    PurchaseOrder = 32,
    PurchaseInvoice = 33,
    GRN = 34,
    DebitNote = 35,
    Payment = 36,
    POS = 37,
    SalesQuotation = 38,
    SalesOrder = 39,
    SalesInvoice = 40,
    CreditNote = 41,
    Receipt = 42,
    WorkPolicy = 43,
    ShiftPolicy = 44,
    LeavePolicy = 45,
    HolidayCalender = 46,
    DocumentMaster = 47,
    DocumentHandOver = 48,
    StockRegister = 49,
    AttendanceMapping = 50,
    Attendance = 51,
    SalaryStructure = 52,
    SalaryProcess = 53,
    SalaryRelease = 54,
    ChartofAccounts = 55,
    //AccountsVoucher = 56, -- No need to put this value in the database
    AccountSettings = 57,
    OpeningBalance = 58,
    RecurenceSetup = 59,
    RecurringJournalSetup = 60,
    AccountsSummary = 61,
    GeneralLedger = 62,
    DayBook = 63,
    TrialBalance = 64,
    CashBook = 65,
    RecurringJournals = 66,
    ProfitAndLossAccount = 67,
    BalanceSheet = 68,
    EmailSettings = 69,
    Approval = 70,
    ExtraCharges = 71,
    DeliveryNote = 72,
    MaterialIssue = 73,
    MaterialReturn = 74,
    ProductReports = 75,
    EmployeeReports = 76,
    AssociateReports = 77,
    SalesReports = 78,
    PurchaseReports = 79,
    PaymentReceiptsReports = 80,
    StockReports = 81,
    InventorySummary = 82,
    UOM = 83,
    UOMConversions = 84,
    Discounts = 85,
    PricingScheme = 86,
    MandatoryDocuments = 87,
    OvertimePolicy = 88,
    StockSummary = 89,
    AbsentPolicy = 90,
    SalaryAdvance = 91,
    CashFlow = 92,
    FundFlow = 93,
    EmployeeProfileReport = 94,
    SalaryStructureReport = 95,
    AttendanceReport = 96,
    PaymentReport = 97,
    AttendanceDeviceSettings = 98,
    AdditionDeduction = 99,
    SalaryPayment = 100,
    SalarySlip = 101,
    LeaveSummaryReport = 102,
    SalaryAdvanceSummaryReport = 103,
    IncomeStatement = 104,
    ExpenseReport = 105,
    ChartOfAccounts = 106,
    PurchaseSummaryReport = 107,
    AgeingReport = 108,
    ProductMovementReport = 109,
    ItemwiseProfitReport = 110,
    ItemIssueReport = 111,
    GRNReport = 112,
    PaymentVoucher = 113,
    ReceiptVoucher = 114,
    ContraVoucher = 115,
    JournalVoucher = 116,
    PurchaseVoucher = 117,
    DebitNoteVoucher = 118,
    SalesVoucher = 119,
    CreditNoteVoucher = 120,
    VacationPolicy=121,
    VacationProcess=122,
    ProductAssemblyReport = 123,
    VendorPriceComparisonReport = 124,
    DeliveryReport  = 125,
    ExtraChargePayment = 126,
    GroupJV = 127,
    StatementofAccounts = 128,
    StockSummaryItemwise = 129,
    StockSummaryItemQtywise = 130,
    ProductionTemplate = 131,
    ProductionProcess = 132,
    QC=133,
    SettlementProcess=134,
    StockLedger = 135,
    LeaveStructure =136,
    UnearnedPolicy=137,
    DeductionPolicy=138,
    EncashPolicy=139,
    HolidayPolicy=140,
    SettlementPolicy=141,
    LeaveOpening=142,
    LeaveEntry=143,
    LeaveExtension=144
}

public enum eModuleID
{
    General = 1,
    Inventory = 2,
    Document = 3,
    Payroll = 4,
    Accounts = 5,
    Reports = 6,
    Documents = 7
}

public enum TransactionTypes
{
    CashPurchase = 1,
    CreditPurchase = 2,
    PurchaseReturn = 3,
    CashSale = 4,
    CreditSale = 5,
    SaleReturn = 6,
    Receipts = 7,
    Payment = 8,
    Salary=9,
    SalaryAdvance=10,
    JobOrder=11,
    POS=12,
    VacationSalary = 13,
    Settlement = 14,
    PurchaseOrder = 15,
    PurchaseInvoice = 16,
    SalesOrder = 17,
    SalesInvoice = 18
    //---------------------------Misc
    //CashPurchaseReturn = 18,
    //CreditPurchaseReturn = 19,
    //CashSaleReturn = 22,
    //CreditSaleReturn = 23,
    //InstallmentCollection = 24,
}

public enum COAErrorCode
{
    AddNewAccount = 2351,
    DescriptionValidate = 2352,
    SaveGroupConfirm = 2353,
    SaveGroupMessage = 2354,
    UpdateGroupConfirm = 2355,
    UpdateGroupMessage = 2356,
    DeleteGroupConfirm = 2357,
    DeleteGroupMessage = 2358,
    FormClosing = 2359,
    SaveAccountConfirm = 2360,
    SaveAccountMessage = 2361,
    UpdateAccountConfirm = 2362,
    UpdateAccountMessage = 2363,
    DeleteAccountConfirm = 2364,
    DeleteAccountMessage = 2365,
    DuplicateAccountGroup = 2366,
    DuplicateAccount = 2367,
    CouldNotDeleteAccountGroup = 2368,
    CouldNotDeleteAccount = 2369,
    InvalidOpeningBalance = 2370,
    DebitOrCreditNotSpecified = 2371,
    InvalidSupplierAccountHeadSelected = 2372,
    InvalidCustomerAccountHeadSelected = 2373
}

public enum SqlErrorCodes
{
    UniqueKeyErrorNumber = 2627,
    ForeignKeyErrorNumber = 547
}

public enum ReasonReferences
{
    Damaged = 1,
    Returned = 6
}

public enum ControlState
{
    Enable,
    Disable
}

//public enum ProductStatus
//{
//    Active = 43,
//    InActive = 45,
//    NotForSale = 68,
//    NonSKU = 69
//}

public enum PredefinedUOMs
{
    Numbers = 1
}

public enum PredefinedCompany
{
    DefaultCompany = 1
}

public enum PredefinedDiscounts
{
    DefaultSalesDiscount = 1,
    DefaultPurchaseDiscount = 2
}

public enum eMode
{
    /// <summary>
    /// Indicates Insert Operation
    /// </summary>
    Insert = 1,
    /// <summary>
    /// Indicates Update Operation
    /// </summary>
    Update = 2,
    /// <summary>
    /// Indicates Delete Operation
    /// </summary>
    Delete = 3,
    /// <summary>
    /// Indicates Unknown operation (default)
    /// </summary>
    Unknown = 4
}

public enum PaymentTypes
{
    AdvancePayment = 1,
    InvoicePayment = 2,
    DirectExpense = 3,
    InvoiceExpense = 4
}

public enum AccountGroups
{
    Assets = 1,
    Liabilities = 2,
    Income = 3,
    Expense = 4,
    CurrentAssets = 6,
    FixedAssets = 5,
    CaptialAccounts = 7,
    Loans = 8,
    CurrentLiabilities = 9,
    DirectIncome = 10,
    IndirectIncome = 11,
    DirectExpense = 12,
    IndirectExpense = 13,
    Stockinhand = 14,
    Deposits = 15,
    LoansAdvances = 16,
    SundryDebtors = 17,
    Cashinhand = 18,
    BankAccounts = 19,
    BankODAccout = 20,
    SundryCreditors = 21,
    SalesAccounts = 22,
    PurchaseAccounts = 23,
    SalaryAndAllowance=24,

    NotSet = -1
}

public enum VendorType
{
    Customer = 1,
    Supplier = 2
}
public enum DefaultAccount
{
    PAndLAccounts = 1,
    CashAc = 2,
    StockAccount = 3,
    PDCIssued = 4,
    PDCReceived = 5,
    DiscountAllowed = 6,
    DiscountReceived = 7,
    Workinprogress = 8,
    PurchaseAc = 9,
    PurchaseReturnAc = 10,
    SalesAc = 11,
    SalesReturnAc = 12,
    GoodsinTransit = 13,
    OpeningStock = 14
}
public enum ControlOperationType
{
    //------------Accounts
    CACompany = 1,
    AccGroup = 2,
    AccName = 3,
    //------------Create/Modify
    SupStatus = 4,
    CusStatus = 5,
    EmpCompany = 6,
    WHCompany = 7,
    CurrencyCompany = 8,
    ProjectCompany = 59,
    //------------Purchase
    PurIndCompany = 10,
    PurIndEmployee = 11,
    PurIndCancel = 12,
    PurQuoCompany = 13,
    PurQuoEmployee = 14,
    PurQuoCancel = 15,
    PurQuoType = 16,
    PurOrdCompany = 17,
    PurOrdEmployee = 18,
    PurOrdCancel = 19,
    PurOrdType = 20,
    GRNCompany = 21,
    GRNEmployee = 22,
    GRNCancel = 23,
    GRNType = 24,
    PurInvCompany = 25,
    PurInvEmployee = 26,
    PurInvCancel = 27,
    PurInvType = 28,
    RFQCompany = 29,
    RFQEmployee = 30,
    RFQCancel = 31,
    PurRetCompany = 60,
    PurRetEmployee = 61,
    PurRetCancel = 82,
    //------------Sales
    SalQuoCompany = 32,
    SalQuoEmployee = 33,
    SalQuoCancel = 34,
    SalOrdCompany = 35,
    SalOrdEmployee = 36,
    SalOrdCancel = 37,
    SalOrdType = 38,
    SalInvCompany = 40,
    SalInvEmployee = 41,
    SalInvCancel = 42,
    CreNoteCompany = 62,
    CreNoteEmployee = 63,
    CreNoteCancel = 79,
    POSCompany = 64,
    POSEmployee = 65,
    POSCancel = 80,
    //------------Delivery
    DelNoteCompany = 43,
    DelNoteEmployee = 44,
    DelNoteOpType = 45,
    //------------Inventory
    StAdjCompany = 46,
    StAdjEmployee = 83,
    ItemAllocCompany = 66,
    StTranCompany = 67,
    StTranEmployee = 68,
    StTranType = 69,
    stTranCancel = 78,
    CostingTab = 81,
    //------------Task
    ExpCompany = 47,
    ExpType = 48,
    RecCompany = 49,
    RecType = 50,
    PayCompany = 51,
    PayType = 52,
    AppPurchaseQuotationCtrl = 53,
    AppPurchaseOrderCtrl = 54,
    AppSalesQuotationCtrl = 55,
    AppSalesOrderCtrl = 56,
    AppRFQCtrl = 57,
    AppStockTran = 58,
    GRPCompany = 70,
    GRPVType = 71,
    //------------Report
    RptProduct = 72,
    RptEmployee = 73,
    RptSales = 75,
    RptPurchase = 76,
    RptPayments = 77,
    RptStock = 74,

    ECPurchaseOrder = 75,
    ECPurchaseInvoice = 76,

    PQDirect = 1,
    PQFromRFQ = 1,
    PODirect = 1,
    POFromQuotation = 1,
    GRNFromInvoice = 1,
    GRNFromTransfer = 1,
    PIFromDirect = 1,
    PIFromOrder = 1,
    SODirect = 1,
    SOFromQuotation = 1
}

//public enum OperationOrderType
//{
//    PQuotationFromRFQ = -1,  POrderFromIndent =-1,
   
//    PQuotationDirect = 6,

//    POrderFromQuotation = 1,
//    POrderDirect = 2,
  
//    GRNFromInvoice = 5, 
//    GRNFromTransfer = 10,

//    PInvoiceFromOrder = 3,
//    PInvoiceDirect = 4,

//    SOTFromQuotation = 11,
//    SOTDirect = 12,

//    DNOTSalesInvoice = 8,
//    DNOTTransfer = 9,

//    STWareHouseToWareHouseTransfer = 7,
//    //QuotationDirect = 6,
//    //QuotationRFQType = 13,

//    //OrderIndentType = 4,
//    //OrderQuotationType = 1,
//    //OrderDirect = 2,

//    //GRNInvoiceType = 5,
//    //GRNTransferType = 10,

//    //InvoiceOrderType = 3,
//    //InvoiceDirect = 4,

//    //SQuotationFromJobOrder = 14,
//    //SQuotationDirect = 15,
 

//    //PQTDirect = 12,
//    //PQTFromRFQ = 13,
//    //POTDirect = 3,
//    //POTFromQuotation = 6,
//    //PITFromDirect = 11,
//    //PITFromOrder = 10,
//    //GTFromInvoice = 8,
//    //GTFromTransfer = 9,
//    //GTFromItemReturn = 38,
//    //STInstallmentSale = 6,
//    //STWholeSale = 7,

//    //DNOTPOS = -1,
//    //DNOTDMR = -1,
//    //CROTSalesInvoice = 17,
//    //CROTPOS = 18,
//    //ECOTPurchaseInvoice = 19,
//    //ECOTSalesInvoice = 20,
//    //ROTSalesOrder = 21,
//    //ROTSalesInvoice = 22,
//    //ROTPOS = 23,
//    //ROTComplaints = 24,
//    //POPPurchaseOrder = 25,
//    //POPPurchaseInvoice = 26,
    
//    STWareHouseToWareHouseTransferDemo = -1,
//    STWareHouseToEmployee = -1,
//    STWareHouseToEmployeeDemo = -1,
//    //STAApprove = -1,
//    //STASuggest = -1,
//    STWareHouseToProject = -1,
//    STWareHouseToDepartment = -1,
//    //ECOTPurchaseOrder = -1,
//    //ECOTStockTransfer = -1,
//    //POPStockTransfer = -1
//}

//public enum OperationStatusType
//{
//    //Purchase

//    PIndentOpened = -1,
//    PIndentClosed = -1,
//    PIndentCancelled = -1,

//    PQuotationOpened = 11,
//    PQuotationClosed = 12,
//    PQuotationApproved = 13,
//    PQuotationRejected = 14,
//    PQuotationCancelled = 15,
//    PQuotationSubmittedForApproval = 26,
//    PQuotationInvalid = -1,
//    PQuotationSubmitted =93,
//    PQuotationSuggested = -1,
//    PQuotationDenied = -1,

//    POrderOpened = 16,
//    POrderClosed = 17,
//    POrderApproved = 18,
//    POrderRejected = 19,
//    POrderCancelled = 20,
//    POrderSubmittedForApproval = 27,
//    POrderSubmitted = 94,
//    POrderSuggested = -1,
//    POrderDenied = -1,

//    GRNOpened = 21,
//    GRNCancelled = 22,

//    PInvoiceOpened = 23,
//    PInvoiceCancelled = 25,
//    PInvoiceClosed = 24,

//    PReturnOpened =78,// 28,//96,
//    PReturnCancelled =79,// 29,//97

//    //Sales
//    SNew=0,
//    SQuotationOpen = 51,
//    SQuotationClosed = 52,
//    SQuotationCancelled = 53,
//    SQuotationApproved = 54,
//    SQuotationRejected = 55,
//    SQuotationSubmittedForApproval = 56,
//    SQuotationSubmitted = 95,
//    SQuotationSuggested = -1,
//    SQuotationDenied = -1,

//    SOrderOpen = 57,
//    SOrderClosed = 58,
//    SOrderCancelled = 59,
//    SOrderApproved = 60,
//    SOrderRejected = 61,
//    SOrderSubmittedForApproval = 62,
//    SOrderSubmitted = 96,
//    SOrderSuggested = -1,
//    SOrderDenied = -1,

//    SInvoiceOpen = 63,
//    SInvoiceClosed = 64,
//    SInvoiceCancelled = 65,

//    SSalesReturnOpen = 66, 
//    SSalesReturnCancelled = 67 ,

//    SPOSOpen = 34, 
//    SPOSDeliverd = 35, 
//    SPOSCancelled = 36,    
    
//    SScheduledForDelivery = 40,
//    SDelivered = 77,
//    SPartiallyDelivered = 0,


//    JOPending = 30,            //Job Order
//    JOScheduled = 31,
//    JOCompleted = 32,
//    JOCancelled = 33,
//    JONew = 46,
//    JOSiteVisitRequired = 70,
//    JOSiteVisitCompleted = 71,
//    JOCostEstimated = 72,
//    JOQuotationCreated = 73,
//    JOApproved = 74,
//    JOMovedToStock = 75,
//    JOPartiallyMovedToStock = 81,
//    JODelivered = 82,

//    STOpened = 37,
//    STSubmittedForApproval = 39,
//    STApproved = 40,
//    STRejected = 41,
//    STCancelled = 42,
//    STClosed = 38,
//    STSuggested = -1,
//    STDenied = -1,
//    STSubmitted =97,
//    STIssued = 47,

//    DMROpened = 7,          //DMR
//    DMRClosed = 9,
//    DMRCancelled = 10,

//    SiteVisitRequested = 5,      //Site Visit
//    SiteVisitCancelled=6,
//    SiteVisitClosed = 80,

//    CostEstNew = 88,
//    CostEstOpen = 90,
//    CostEstClosed = 92






























//}

public enum OperationOrderType
{
    PQuotationDirect = 6,
    POrderFromQuotation = 1,
    POrderDirect = 2,
    GRNFromInvoice = 5,
    GRNFromTransfer = 10,
    GRNFromOrder=14,
    GRNDirect = 21,
    PInvoiceFromOrder = 3,
    PInvoiceDirect = 4,

    SOTFromQuotation = 11,
    SOTDirect = 12,
    SOTFromDeliveryNote = 23,
    DNOTSalesInvoice = 8,
    DNOTTransfer = 9,
    DNOTSalesOrder = 13,
    DNOTDirect =22,
    STWareHouseToWareHouseTransfer = 7,
    SOTFromSalesPro = -1,

    PQuotationFromRFQ = -1, POrderFromIndent = -1,
    STWareHouseToWareHouseTransferDemo = -1,
    STWareHouseToEmployee = -1,
    STWareHouseToEmployeeDemo = -1,
    STWareHouseToProject = -1,
    STWareHouseToDepartment = -1,
    SalesInvoiceFromOrder=15,
    SalesInvoiceDirect=16,
    SalesInvoiceFromDeliveryNote = 24,
    CreditNoteFromSalesOrder=17,
    CreditNoteFromSalesInvoice = 18,
    DebitNoteFromPurchaseOrder=19,
    DebitNoteFromPurchaseInvoice = 20,
    DirectDeliveryNote=22,
    POrderFromGRN = 25,
    PInvoiceFromGRN=26
}
public enum SalesOrderDeliveryStatus
{
    partiallyDelivered = 71,
    Delivered = 72,
    New =74
}
public enum ProductType
{
 InventoryItemSKU = 1 ,
InventoryItemNONSKU = 2 ,
ServiceItem = 3 ,
AssetItem = 4 
}

public enum OperationStatusType
{
    CustomerActive = 1,
    CustomerBlocked = 2,
    SupplierActive = 3,
    SupplierBlocked = 4,
    PQuotationOpened = 5,
    PQuotationClosed = 6,
    PQuotationApproved = 7,
    PQuotationRejected = 8,
    PQuotationCancelled = 9,
    POrderOpened = 10,
    POrderClosed = 11,
    POrderApproved = 12,
    POrderRejected = 13,
    POrderCancelled = 14,
    GRNOpened = 15,
    GRNCancelled = 16,
    PInvoiceOpened = 17,
    PInvoiceClosed = 18,
    PInvoiceCancelled = 19,
    PQuotationSubmittedForApproval = 20,
    POrderSubmittedForApproval = 21,
    SPOSOpen = 22,
    SPOSDeliverd = 23,
    SPOSCancelled = 24,
    STOpened = 25,
    STClosed = 26,
    STSubmittedForApproval = 27,
    STApproved = 28,
    STRejected = 29,
    STCancelled = 30,
    ProductActive = 31,
    ProductInActive = 32,
    STIssued = 33,
    DocReceipt = 34,
    DocIssue = 35,
    SQuotationOpen = 36,
    SQuotationClosed = 37,
    SQuotationCancelled = 38,
    SQuotationApproved = 39,
    SQuotationRejected = 40,
    SQuotationSubmittedForApproval = 41,
    SOrderOpen = 42,
    SOrderClosed = 43,
    SOrderCancelled = 44,
    SOrderApproved = 45,
    SOrderRejected = 46,
    SOrderSubmittedForApproval = 47,
    SInvoiceOpen = 48,
    SInvoiceClosed = 49,
    SInvoiceCancelled = 50,
    SSalesReturnOpen = 51,
    SSalesReturnCancelled = 52,
    RecurringJournalActive = 53,
    SDelivered = 54,
    PReturnOpened = 55,
    PReturnCancelled = 56,
    RecurringJournalInActive = 57,
    RecurringJournalPosted = 58,
    RecurringJournalClosed = 59,
    AlertOpen = 60,
    AlertClose = 61,
    PQuotationSubmitted = 62,
    POrderSubmitted = 63,
    SQuotationSubmitted = 64,
    SOrderSubmitted = 65,
    STSubmitted = 66,
    AlertRead = 67,
    AlertBlocked = 68,
    SPartiallyDelivered =75,

    SNew = 0,
    PIndentOpened = -1,
    PIndentClosed = -1,
    PIndentCancelled = -1,
    PQuotationInvalid = -1,
    PQuotationSuggested = -1,
    PQuotationDenied = -1,
    POrderSuggested = -1,
    POrderDenied = -1,
    SQuotationSuggested = -1,
    SQuotationDenied = -1,
    SOrderSuggested = -1,
    SOrderDenied = -1,    
    STSuggested = -1,
    STDenied = -1,
    RFQOpened = -1,
    RFQClosed = -1,
    RFQSubmittedForApproval = -1,
    RFQCancelled = -1,
    RFQRejected = -1,
    RFQApproved = -1,
    RFQSubmitted = -1,
    RFQSuggested = -1,
    RFQDenied = -1,

    PONew=74,
    POPartiallyReceived=69,
    POReceived=70,

    SOrderPartiallyDelivered = 71,
    SOrderFullyDelivered=72,


    GRNPartiallyInvoiced = 80,
    GRNFullyInvoiced = 79,
    DDNFullyInvoiced=81,
    DDNPartiallyInvoiced=82,
    DDNopen=83,
    GRNOrdered = 84

}

public enum Accounts
{
    PLAccounts = 1,
    CashAccount = 2,
    StockAccount = 3,
    PDCIssued = 4,
    PDCReceived = 5,
    DiscountAllowed = 6,
    DiscountReceived = 7,
    WorkInProgress = 8,
    PurchaseAccounts = 9,
    PurchaseReturnAccounts = 10,
    SalesAccounts = 11,
    SalesReturnAccounts = 12,
    GoodsInTransit = 13,
    OpeningStock = 14,
    NotSelect=-1
}

public enum VendorStatus
{
    CustomerActive = 1,
    CustomerBlocked = 2,
    SupplierActive = 3,
    SupplierBlocked = 4,
    Active = 5,
    Blocked = 6
}
public enum ItemGroupStatus
{
    Active = 77,
    Blocked = 78
}
public enum PaymentTerms
{
    Cash = 1,
    Credit = 2
}

public enum WarehouseLocationTypeReference
{
    FreeZone = 1,
    ActiveLocations = 2
}

public enum ApprovePermission
{
    Suggest = 1,
    Approve = 2,
    Both = 3
}

public enum CostingMethodReference
{
    LIFO = 1,
    FIFO = 2,
    Batch = 3,
    MIN = 4,
    MAX = 5,
    AVG = 6,   
    NONE=7
}

public enum Periods
{
    Yearly = 0,
    Monthly = 1,
    Daily = 2,
    Summary = 3
}

public enum sChartType
{
    Area = 1,
    Bar = 2,
    Boxsplot = 3,
    Bubble = 4,
    CandileStick = 5,
    Column = 6,
    Doughnut = 7,
    ErrorBar = 8,
    FastLine = 9,
    FastPoint = 10,
    Funnel = 11,
    Line = 12,
    Pie = 13,
    Point = 14,
    Polar = 15,
    Pyramid = 16,
    Radar = 17,
    Range = 18,
    RangeBar = 19,
    RangeColumn = 20,
    Spline = 21,
    SplineArea = 22,
    StackedArea = 23,
    StackedBar = 24,
    StackedBar100 = 25,
    StackedColumn = 26,
    StackedColumn100 = 27,
    StepLine = 28,
    Stock = 29
}

public enum ApprovalType
{
    PurchaseQuotation = 1,
    PurchaseOrder = 2,
    SalesQuotation = 3,
    SalesOrder = 4,
    PlanSubmission = 5,
    RFQ = 6,
    RFA = 7,
    StockTransfer = 8
}

public enum SummaryTypes
{
    Stock = 0,
    PI = 1,
    GRN = 2,
    PR = 3,
    SI = 4,
    POS = 5,
    SR = 6,
    DN = 7,
    TO = 8
}

public enum AttendanceStatus
{
    Present = 1,
    Rest = 2,
    Leave = 3,
    Absent = 4
}

public enum AttendanceMode
{
    AttendanceAuto = 1,
    AttendanceManual = 2
}
public enum CommonMessages
{
    SaveConfirm = 1,
    Saved = 2,
    UpdateConfirm = 3,
    Updated = 21,
    DeleteConfirm = 13,
    Deleted = 4,
    FormClose = 8,
    SaveFailed = 9,
    NextRecord = 11,
    LastRecord = 12,
    PreviousRecord = 10,
    FirstRecord = 9,
    NewRecord = 655,
    DetailExists = 19,
    ConfirmPhotoRemoval = 20,
    PhotoRemoved = 21,
    SearchNotFound = 23,
}
public enum AttendnaceDevices
{

    FingerTech = 1,
    ZKFingerprint = 2,
    HandPunch = 3,
    ActaTEK = 4
}

public enum AttendanceFiletype
{
    Csv=1,
    Excel=2,
    AccessDB=3,
    SqlDataBase=4
}

public enum PolicyType
{
    Absent = 1,
    Shortage = 2,
    Overtime = 3,
    Holiday = 4,
    Encash = 5,
    Vacation = 6,
    Settlement = 7,
    UnEarnedAmt = 8

}

public enum AbsentType
{
    Absent = 1,
    Shortage = 2
}

public enum CalculationType
{
    BasicPay = 1,
    GrossSalary = 2
}

public enum CostCenterGroupReference
{
    Employee = 1,
    General = 2
}

public enum EmployeeWorkStatus
{
    Absconding = 1,
    Expired = 2,
    Retired = 3,
    Resigned = 4,
    Terminated = 5,
    InService = 6,
    Probation = 7
}
public enum VoucherType
{
    Payment = 1,
    Receipt = 2,
    Contra = 3,
    Journal = 4,
    Purchase = 5,
    Sale = 6,
    DebitNote = 7,
    CreditNote = 8
}

//public enum DocDocumentStatus
//{
//    Receipt = 34,
//    Issue = 35
//}

public enum MessageCode
{
    DoYouWishToSave = 1,
    SavedSuccessfully = 2,
    DoYouWishToUpdate = 3,
    UpdatedSuccessfully = 21,
    DoYouWishToDelete = 13,
    DeletedSuccessfully = 4,
    DoYouWishToClose = 4269,
    SelectCompany = 14,
    SelectWarehouse = 9105,
    SelectItemDetails = 26,
    SelectItemCode = 27,
    SelectItemName = 28,
    EnterQuantity = 29,
    EnterUOM = 30,
    CheckDuplication = 31,
    AddNewInformation = 38

}

public enum Locations
{
    BOTTOMCENTER = 1,
    BOTTOMLEFT=2,
    BOTTOMRIGHT=3,
    TOPCENTER=4,
    TOPLEFT=5,
    TOPRIGHT=6
}

public enum Allignment
{
    CENTER=1,
    LEFT = 2,
    RIGHT = 3
}

public enum BarcodeEncodingType
{
    UPCA=1,
    UPCE=2,
    UPC_SUPPLEMENTAL_2DIGIT=3,
    UPC_SUPPLEMENTAL_5DIGIT=4,
    EAN13=5,
    JAN13=6,
    EAN8=7,
    ITF14=8,
    Codabar=9,
    PostNet=10,
    BOOKLAND=11,
    CODE11=12,
    CODE39=13,
    CODE39Extended=14,
    CODE93=15,
    LOGMARS=16,
    MSI_Mod10=17,
    Interleaved2of5=18,
    Standard2of5=19,
    CODE128=20,
    CODE128A=21,
    CODE128B=22,
    CODE128C=23,
    TELEPEN=24
}

public enum SearchBy
{
    // these indices are passing to stored procedure.
    // if you change any index, you must update it in the data access layer as well.
    Department = 1,
    Designation = 2,
    EmployeeNumber = 3,
    FirstName = 4
}

public enum AlertSettingsTypes
{
    SalesQuotationSubmitted = 1,
    SalesQuotationApproved = 2,
    SalesQuotationCancelled = 3,
    SalesQuotationDueDate = 4,
    SaleOrderCreated = 5,
    SalesOrderAdvancePaid = 6,
    SaleOrderSubmitted = 7,
    SaleOrderApproved = 8,
    SaleOrderRejected = 9,
    SalesOrderDueDate = 10,
    SalesInvoiceDueDate = 11,
    MaterialIssue = 12,
    Receipt = 13,
    DeliveryNote = 14,

    ReorderLevelEntered = 15,
    PurchaseOrderApproved = 16,
    RFQDueDate = 17,
    PurchaseQuotationDueDate = 18,
    PurchaseOrderDueDate = 19,
    DocumentExpiry = 20,
    RecurringJournalAlert = 21,
    PostDatedCheque = 22,
    FirstDisplay=-1
}

public enum AlertTypes
{
    Alert = 1,
    SMS = 2,
    EMAIL = 3 
}