﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

    public class NumericTextBox : TextBox
    {
        
        public NumericTextBox()
        {
            this.TextAlign = HorizontalAlignment.Right;
            this.ShortcutsEnabled = false;
            string value = "0";
            if (decimalPlaces > 0)
            {
                value += ".";
                for (int i = 0; i < decimalPlaces; i++)
                    value += "0";
            }
            Text = value;
        }
        private int decimalPlaces;

        public int DecimalPlaces
        {
            get { return decimalPlaces; }
            set { decimalPlaces = value;
            string textBoxvalue = "0";
            if (decimalPlaces > 0)
            {
                textBoxvalue += ".";
                for (int i = 0; i < decimalPlaces; i++)
                    textBoxvalue += "0";
            }
            Text = textBoxvalue;
            }
        }
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;
            }
            else
            {
                if (decimalPlaces == 0)
                {
                    if (e.KeyChar == '.')
                        e.Handled = true;
                }
                else
                {
                    int dotIndex = -1;
                    if (Text.Contains('.'))
                    {
                        dotIndex = Text.IndexOf('.');
                    }
                    if (e.KeyChar == '.')
                    {
                        if (dotIndex != -1 && !base.SelectedText.Contains('.'))
                        {
                            e.Handled = true;
                        }
                    }
                    else
                    {
                        if (char.IsDigit(e.KeyChar))
                        {
                            if (dotIndex !=-1 && base.SelectionStart > dotIndex)
                            {
                                string[] splitText = Text.Split('.');
                                if (splitText.Length == 2)
                                {
                                    if (splitText[1].Length - base.SelectedText.Length >= decimalPlaces)
                                        e.Handled = true;
                                }
                            }
                        }
                    }
                }
            }
            base.OnKeyPress(e);
        }
        private string GetRoundedTextValue()
        {
            string value = string.Empty;
            if (!string.IsNullOrEmpty(Text))
            {
                string[] splitText = Text.Split('.');
                if (!string.IsNullOrEmpty(splitText[0]))
                    value = splitText[0];
                else
                    value = "0";
                if (splitText.Length == 2)
                {
                    string decimalPart = splitText[1];
                    value += "." + decimalPart;
                    for (int i = decimalPart.Length ; i < decimalPlaces; i++)
                        value += "0";
                }
                else
                {
                    if (decimalPlaces > 0)
                    {
                        value += ".";
                        for (int i = 0; i < decimalPlaces; i++)
                            value += "0";
                    }
                }
            }
            else
            {
                value = "0";
                if (decimalPlaces > 0)
                {
                    value += ".";
                    for (int i = 0; i < decimalPlaces; i++)
                        value += "0";
                }
           }
            string trimedValue = string.Empty;
            string[] splitValue = value.Split('.');
            trimedValue = Convert.ToString(Convert.ToDecimal(splitValue[0]));
            if(splitValue.Length > 1)
            trimedValue += "." + splitValue[1];
            return trimedValue;
        }
        protected override void OnLeave(EventArgs e)
        {
            try
            {
                Text = GetRoundedTextValue();
                base.OnLeave(e);
            }
            catch (Exception ex)
            {
                if (ex.Message == "Input string was not in a correct format.")
                {
                    Text = "0";
                    base.OnTextChanged(e);
                }
 
            }
        }
        protected override void OnTextChanged(EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Text.Trim()))
                {
                    string value = "0";
                    if (decimalPlaces > 0)
                    {
                        value += ".";
                        for (int i = 0; i < decimalPlaces; i++)
                            value += "0";
                    }
                    Text = value;
                }
                base.OnTextChanged(e);
            }
            catch (Exception)
            {

            }
        }
        protected override void OnEnter(EventArgs e)
        {
            System.Windows.Forms.Clipboard.Clear();
             base.OnEnter(e);
        }
        protected override void OnMouseEnter(EventArgs e)
        {
            System.Windows.Forms.Clipboard.Clear();
                base.OnMouseEnter(e);
        }
        
    }
