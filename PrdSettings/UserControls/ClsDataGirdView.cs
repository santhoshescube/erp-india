﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar.Controls;

namespace DemoClsDataGridview
{
    /// <summary>
    /// Editting combobox and tab on Enter key
    /// created by : Arun,Renjith, Devi 
    /// </summary>
    public class ClsDataGirdView : DataGridView
    {
        #region Property setting for DataGridView

        private int[] iNumericCols = { };
        private int[] iDecimalCols = { };
        private int[] iAlphaNumericCols = { };
        private int[] iCapsLockCols = { };
        private int[] iNegativeValueCols = { };
        private bool bHasSlNo;
        private bool bAddNewRow;
        private int iLastRowIndex;

        public int[] NumericCols
        {
            get { return iNumericCols; }
            set { iNumericCols = value; }
        }

        public int[] DecimalCols
        {
            get { return iDecimalCols; }
            set { iDecimalCols = value; }
        }

        public int[] AlphaNumericCols
        {
            get { return iAlphaNumericCols; }
            set { iAlphaNumericCols = value; }
        }

        public int[] CapsLockCols
        {
            get { return iCapsLockCols; }
            set { iCapsLockCols = value; }
        }

        public int[] NegativeValueCols
        {
            get { return iNegativeValueCols; }
            set { iNegativeValueCols = value; }
        }

        public bool HasSlNo
        {
            get { return bHasSlNo; }
            set { bHasSlNo = value; }
        }

        public bool AddNewRow
        {
            get { return bAddNewRow; }
            set { bAddNewRow = value; }
        }

        public int LastRowIndex
        {
            get { return iLastRowIndex; }
            set { iLastRowIndex = value; }
        }

        #endregion // Property setting for DataGridView

        ComboBox combo;
        public ClsDataGirdView()// constructor
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ClsDataGirdView
            // 
            
            this.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ClsDataGirdView_EditingControlShowing);
            this.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ClsDataGirdView_DataError);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
        }

        
        protected override void OnCellEnter(DataGridViewCellEventArgs e)
        {
            System.Windows.Forms.Clipboard.Clear();
                    base.OnCellEnter(e);
        }
        protected override void OnCellMouseEnter(DataGridViewCellEventArgs e)
        {
            System.Windows.Forms.Clipboard.Clear();
            base.OnCellMouseEnter(e);
        }
        /// <summary>
        /// Enter Key act as Tab key
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            //Enter Key Will Act as Tab
            if (keyData == Keys.Enter)
            {
                if (!bAddNewRow)
                    SendKeys.Send("{Tab}");
                else
                {
                    if (this.CurrentCell.ColumnIndex == iLastRowIndex)
                    {
                        if (this.CurrentRow.Index == this.RowCount - 1)
                        {
                            //this.Rows.Add();
                            //this.CurrentCell = this[0, this.CurrentRow.Index + 1];
                            this.CommitEdit(DataGridViewDataErrorContexts.Commit);
                            //DataGridViewCellEventArgs e = new DataGridViewCellEventArgs(this.CurrentCell.ColumnIndex, this.CurrentCell.RowIndex);
                            //OnCellValueChanged(e);
                            KeyEventArgs en = new KeyEventArgs(Keys.Enter);
                            OnKeyDown(en);
                        }
                        else
                            SendKeys.Send("{Tab}");
                    }
                    else
                        SendKeys.Send("{Tab}");
                }

                return true;
            }
            else
            {
                return base.ProcessCmdKey(ref msg, keyData);
            }
        }

        /// <summary>
        /// Editing Combobox  adding to the datagridview combobox column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClsDataGirdView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            System.Windows.Forms.Clipboard.Clear();

            
               
           
            if ((this.CurrentCell) is DataGridViewComboBoxCell)// Adding editing combobox to DataGridview
            {
                this.combo = (ComboBox)e.Control;
                if (this.combo != null)
                {
                    this.combo.DropDownHeight = 134;
                    this.combo.DropDownStyle = ComboBoxStyle.DropDown;
                    this.combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    this.combo.AutoCompleteSource = AutoCompleteSource.ListItems;
                    //this.combo.SelectedIndexChanged -= this.ComboBox_SelectedIndexChanged;//Remove an existing event-handler
                    //this.combo.SelectedIndexChanged += this.ComboBox_SelectedIndexChanged;//Adding  event-handler
                    //Remove an existing event-handler, if present, to avoid 
                    //adding multiple handlers when the editing control is reused.
                    this.combo.KeyPress -= this.ComboBox_KeyPress;//remove 
                    // Add the event handler. 
                    this.combo.KeyPress += this.ComboBox_KeyPress;
                }
                this.NotifyCurrentCellDirty(true);
            }
            else if (e.Control is DataGridViewTextBoxEditingControl)
            {
                ((TextBox)e.Control).ShortcutsEnabled = false;//disabling Contextmenu strip for copy paste
                e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
            }
        }

        //Key Press event
        private void ComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.combo.DroppedDown = false;
        }

        private void ClsDataGirdView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                return;
            }
            catch (Exception)
            {
                return;
            }
        }

        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataGridViewTextBoxEditingControl CurrentCol = (DataGridViewTextBoxEditingControl)sender;

            // Setting numeric cell
            if (iNumericCols.Contains(CurrentCol.EditingControlDataGridView.CurrentCell.ColumnIndex))
            {
                if (!(Char.IsDigit(e.KeyChar)) && (!(e.KeyChar == (char)8)))
                {
                    e.Handled = true;
                }
            }
            // Setting decimal cells
            else if (iDecimalCols.Contains(CurrentCol.EditingControlDataGridView.CurrentCell.ColumnIndex))
            {
                if (e.KeyChar == (char)46)
                {
                    if (CurrentCol.Text.Trim().Contains("."))
                    {
                        e.Handled = true;
                    }
                }
                else if (!char.IsDigit(e.KeyChar) && (!(e.KeyChar == (char)8)))
                {
                    e.Handled = true;
                }
            }
            // Alphanumeric characters
            else if (iAlphaNumericCols.Contains(CurrentCol.EditingControlDataGridView.CurrentCell.ColumnIndex))
            {
                if (!char.IsLetterOrDigit(e.KeyChar) && (!(e.KeyChar == (char)8)))
                {
                    e.Handled = true;
                }
            }
            // Setting negative value cells
            else if (iNegativeValueCols.Contains(CurrentCol.EditingControlDataGridView.CurrentCell.ColumnIndex))
            {
                if (e.KeyChar == (char)46)
                {
                    if (CurrentCol.Text.Trim().Contains("."))
                    {
                        e.Handled = true;
                    }
                }
                else if (!char.IsDigit(e.KeyChar) && (!(e.KeyChar == (char)8)))
                {
                    if (e.KeyChar != (char)45)
                        e.Handled = true;
                }
            }
            else if (iCapsLockCols.Contains(CurrentCol.EditingControlDataGridView.CurrentCell.ColumnIndex))
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
            }
        }

        protected override void OnCellValueChanged(DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    // For a decimal column, format the value
                    if (iDecimalCols.Contains(e.ColumnIndex))
                    {
                        //this[e.ColumnIndex, e.RowIndex].Value = Convert.ToDecimal(this[e.ColumnIndex, e.RowIndex].EditedFormattedValue).ToString();
                        base.OnCellValueChanged(e);
                        //return;
                    }
                    // Set the Serial number
                    if (this.RowCount == 1 && bHasSlNo == true)
                    {
                        this[0, 0].Value = 1;
                        this[0, 0].ReadOnly = true;
                    }
                }
                base.OnCellValueChanged(e);
            }
            catch (Exception)
            {
                if (e.RowIndex >= 0)
                    this[e.ColumnIndex, e.RowIndex].Value = "";
            }
        }

        protected override void OnRowsAdded(DataGridViewRowsAddedEventArgs e)
        {
            if (bHasSlNo)
            {
                this.Rows[this.NewRowIndex].Cells[0].Value = this.NewRowIndex + 1;
                this.Rows[this.NewRowIndex].Cells[0].ReadOnly = true;
            }
            base.OnRowsAdded(e);
        }

        protected override void OnRowsRemoved(DataGridViewRowsRemovedEventArgs e)
        {
            if (bHasSlNo)
            {
                foreach (DataGridViewRow Row in this.Rows)
                {
                    Row.Cells[0].Value = Row.Index + 1;
                }
            }
            base.OnRowsRemoved(e);
        }

        protected override void OnColumnAdded(DataGridViewColumnEventArgs e)
        {
            //if (iDecimalCols.Contains(e.Column.Index)||
            //    iNumericCols.Contains(e.Column.Index)||
            //    iNegativeValueCols.Contains(e.Column.Index))
            //{
            //    e.Column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //}
            base.OnColumnAdded(e);
        }
    }

    public class NumericTextBox : TextBox
    {
        public NumericTextBox()
        {
            this.ShortcutsEnabled = false;// For Diasbling copy pasting
        }
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13 || e.KeyChar == (char)8)
            {
                e.Handled = false;
            }
            else if ((!(e.KeyChar >= 48 && e.KeyChar <= 57)))
            {
                e.Handled = true;
            }
            base.OnKeyPress(e);
        }

        protected override void OnEnter(EventArgs e)
        {
            System.Windows.Forms.Clipboard.Clear();
            //this.BackColor = Color.Ivory;
            base.OnEnter(e);
        }
        protected override void OnMouseEnter(EventArgs e)
        {
            System.Windows.Forms.Clipboard.Clear();
            base.OnMouseEnter(e);
        }
        //public override int MaxLength
        //{
        //    get
        //    {
        //        return iMaxLength;
        //    }
        //    set
        //    {
        //        iMaxLength = value;
        //    }
        //}
    }

    public class DecimalTextBox : TextBox
    {
        public DecimalTextBox()
        {
            this.ShortcutsEnabled = false;// For Diasbling copy pasting
        }
        // Handles the characters
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)46)
            {
                if (this.Text.Trim().Contains("."))
                {
                    e.Handled = true;
                }
            }
            else if (e.KeyChar == (char)13 || e.KeyChar == (char)8)
            {
                e.Handled = false;
            }
            else if (!(e.KeyChar >= 48 && e.KeyChar <= 57))
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        // Formatting the value while leaving from the textbox
        protected override void OnLeave(EventArgs e)
        {
            try
            {
                //this.Text = this.Text.Trim().Length > 0 ? Convert.ToDouble(this.Text).ToString() : "";
                base.OnLeave(e);
            }
            catch (Exception)
            {
                this.Text = "";
            }
        }

        protected override void OnEnter(EventArgs e)
        {
            System.Windows.Forms.Clipboard.Clear();
            base.OnEnter(e);
        }
        protected override void OnMouseEnter(EventArgs e)
        {
            System.Windows.Forms.Clipboard.Clear();
            base.OnMouseEnter(e);
        }
        //public override RightToLeft RightToLeft
        //{
        //    get
        //    {
        //        return RightToLeft.Yes;
        //    }
        //}
    }

    public class DecimalDotNetBarTextBox : TextBoxX
    {

        public DecimalDotNetBarTextBox()
        {
            this.ShortcutsEnabled = false;
        }
        // Handles the characters
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)46)
            {
                if (this.Text.Trim().Contains("."))
                {
                    e.Handled = true;
                }
            }
            else if (e.KeyChar == (char)13 || e.KeyChar == (char)8)
            {
                e.Handled = false;
            }
            else if (!(e.KeyChar >= 48 && e.KeyChar <= 57))
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        // Formatting the value while leaving from the textbox
        protected override void OnLeave(EventArgs e)
        {
            try
            {
                //this.Text = this.Text.Trim().Length > 0 ? Convert.ToDouble(this.Text).ToString() : "";
                base.OnLeave(e);
            }
            catch (Exception)
            {
                this.Text = "";
            }
        }
        protected override void OnEnter(EventArgs e)
        {
            System.Windows.Forms.Clipboard.Clear();
                base.OnEnter(e);
        }
        protected override void OnMouseEnter(EventArgs e)
        {
            System.Windows.Forms.Clipboard.Clear();
            base.OnMouseEnter(e);
        }
      
    }

    /// <summary>
    /// Arun ,Ranjith
    /// </summary>

    public class CalendarColumn : DataGridViewColumn
    {
        public CalendarColumn()
            : base(new CalendarCell())
        {
        }

        public override DataGridViewCell CellTemplate
        {
            get
            {
                return base.CellTemplate;
            }
            set
            {
                // Ensure that the cell used for the template is a CalendarCell.
                if (value != null &&
                    !value.GetType().IsAssignableFrom(typeof(CalendarCell)))
                {
                    throw new InvalidCastException("Must be a CalendarCell");
                }
                base.CellTemplate = value;
            }
        }
    }
    //Calendarcell & CalendarEditingControl class Edit for null value 
    public class CalendarCell : DataGridViewTextBoxCell
    {

        public CalendarCell()
            : base()
        {
            // Use the short date format.
            //this.Style.Format = "d";

            this.Style.Format = "dd-MMM-yyyy";//new change
           
           


        }

        public override void InitializeEditingControl(int rowIndex, object
            initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
        {
            // Set the value of the editing control to the current cell value.
            base.InitializeEditingControl(rowIndex, initialFormattedValue,
                dataGridViewCellStyle);
            CalendarEditingControl ctl = DataGridView.EditingControl as CalendarEditingControl;
            if (this.Value != null)
            {
                DateTime dt;
                 if(DateTime.TryParse(Convert.ToString(this.Value),out dt))
                 {
                     ctl.Checked = true;
                    ctl.Value = (DateTime)this.Value;
                 }
            }
            //ctl.Value = (DateTime)this.Value; //Comment by ranjith 9/27/2010 for Null value Display
        }

        public override Type EditType
        {
            get
            {
                // Return the type of the editing contol that CalendarCell uses.
                return typeof(CalendarEditingControl);
            }
        }

        public override Type ValueType
        {
            get
            {
                // Return the type of the value that CalendarCell contains.
                return typeof(DateTime);
            }
        }

        public override object DefaultNewRowValue
        {
            get
            {
                // Use the current date and time as the default value.
                return null;
                //return DateTime.Now; //Comment by ranjith 9/27/2010 for Null value Display
            }
        }
    }

    class CalendarEditingControl : System.Windows.Forms.DateTimePicker, IDataGridViewEditingControl
    {
        DataGridView dataGridView;
        private bool valueChanged = false;
        int rowIndex;

        public CalendarEditingControl()
        {
            //this.Format = DateTimePickerFormat.Short;
            this.ShowCheckBox = true;
            this.CustomFormat = "dd-MMM-yyyy";//new change
            this.Format = DateTimePickerFormat.Custom;//new change
        }

        // Implements the IDataGridViewEditingControl.EditingControlFormattedValue 
        // property.
        public object EditingControlFormattedValue
        {
            get
            {
                if (this.Checked)
                    return this.Value.ToShortDateString();
                else
                    return null;

            }
            set
            {
                if (value is String)
                {
                    this.Value = DateTime.Parse((String)value);
                }
            }
        }

        // Implements the 
        // IDataGridViewEditingControl.GetEditingControlFormattedValue method.
        public object GetEditingControlFormattedValue(
            DataGridViewDataErrorContexts context)
        {
            return EditingControlFormattedValue;
        }

        // Implements the 
        // IDataGridViewEditingControl.ApplyCellStyleToEditingControl method.
        public void ApplyCellStyleToEditingControl(
            DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
            this.CalendarForeColor = dataGridViewCellStyle.ForeColor;
            this.CalendarMonthBackground = dataGridViewCellStyle.BackColor;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex
        {
            get
            {
                return rowIndex;
            }
            set
            {
                rowIndex = value;
            }
        }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(
            Keys key, bool dataGridViewWantsInputKey)
        {
            // Let the DateTimePicker handle the keys listed.
            switch (key & Keys.KeyCode)
            {
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                case Keys.Right:
                case Keys.Home:
                case Keys.End:
                case Keys.PageDown:
                case Keys.PageUp:
                    return true;
                default:
                    return !dataGridViewWantsInputKey;
            }
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
            // No preparation needs to be done.
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get
            {
                return false;
            }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView
        {
            get
            {
                return dataGridView;
            }
            set
            {
                dataGridView = value;
            }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged
        {
            get
            {
                return valueChanged;
            }
            set
            {
                valueChanged = value;
            }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get
            {
                return base.Cursor;
            }
        }

        protected override void OnValueChanged(EventArgs eventargs)
        {
            // Notify the DataGridView that the contents of the cell
            // have changed.
            if (this.Checked)
            {
                valueChanged = true;
                this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
                base.OnValueChanged(eventargs);
            }
            else
            {
                this.valueChanged = true;
                this.EditingControlFormattedValue = null;
                base.OnValueChanged(eventargs);
            }
        }

        protected override void OnLeave(EventArgs e)
        {
            if (!this.Checked)
            {
                this.EditingControlFormattedValue = null;
                dataGridView.EditingControl.Text = null;
                dataGridView.CurrentCell.Value = null;
            }
            base.OnLeave(e);
        }
    }

}