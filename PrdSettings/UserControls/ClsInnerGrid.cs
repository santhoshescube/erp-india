﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.Sql;
using System.Drawing;
using DemoClsDataGridview;

    /// <summary>
    /// Summary description for clsInnerGrid
    /// Summary : Inner DataGridView shows only In First Column
    /// created : Arun,Renjith
    /// Date    : 27-07-2010
    /// </summary>
public class ClsInnerGrid : DataGridView
{
    public Boolean bFlag = true;
    public string TextBoxText, field;//field= where condition field.
    public string PsQuery;//sql query
    public string sCellvalue;
    public string PServerName;// = "VBC-PC10\\SQLEXPRESS";//ServerName
    public string TextParam;

    //public string HideColumn;//For Hide the column
    public string[] ColumnsToHide;
   // public Control ParentControl;//For Showing Innergrid Contain Control

    public String[] aryFirstGridParam;//outer grid column names
    public String[] arySecondGridParam;//inner Grid Column names
    public int PiColumnIndex, PiFocusIndex;
    public DataTable dtDataSource;
    public DataSet ds = new DataSet();
    DataLayer mObjConnection;

    ComboBox combo;
    public TextBox TxtB = new TextBox();
    DataGridView dgv = new DataGridView();

    public event EventHandler<EventArgs> Textbox_TextChanged;//Events
    public EventArgs args;
    public int pnlLeft;
    public int pnlTop;
    public int iGridWidth;
    public int iGridHeight;
    public bool bBothScrollBar;

    private Rectangle RectParentArea;//For adjusting InnerGrid Location
    private int intCellWidth;//For adjusting InnerGrid Location

    #region Property setting for DataGridView

    private int[] iNumericCols = { };
    private int[] iDecimalCols = { };
    private int[] iAlphaNumericCols = { };
    private int[] iCapsLockCols = { };
    private int[] iNegativeValueCols = { };
    private bool bHasSlNo;
    private bool bAddNewRow;
    private int iLastRowIndex;

    public int[] NumericCols
    {
        get { return iNumericCols; }
        set { iNumericCols = value; }
    }

    public int[] DecimalCols
    {
        get { return iDecimalCols; }
        set { iDecimalCols = value; }
    }

    public int[] AlphaNumericCols
    {
        get { return iAlphaNumericCols; }
        set { iAlphaNumericCols = value; }
    }

    public int[] CapsLockCols
    {
        get { return iCapsLockCols; }
        set { iCapsLockCols = value; }
    }

    public int[] NegativeValueCols
    {
        get { return iNegativeValueCols; }
        set { iNegativeValueCols = value; }
    }

    public bool HasSlNo
    {
        get { return bHasSlNo; }
        set { bHasSlNo = value; }
    }

    public bool AddNewRow
    {
        get { return bAddNewRow; }
        set { bAddNewRow = value; }
    }

    public int LastRowIndex
    {
        get { return iLastRowIndex; }
        set { iLastRowIndex = value; }
    }

    #endregion // Property setting for DataGridView

    public ClsInnerGrid()//constructor
    {
        InitializeComponent();
    }

    private void InitializeComponent()
    {
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        this.SuspendLayout();
        // 
        // ClsInnerGrid
        // 
        this.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
        
        this.Leave += new System.EventHandler(this.ClsInnerGrid_Leave);
        this.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ClsInnerGrid_EditingControlShowing);
        this.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ClsInnerGrid_DataError);
        this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgv_KeyDown);
        this.Leave+=new EventHandler(ClsInnerGrid_Leave);
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        this.ResumeLayout(false);

    }

    protected override void OnCellEnter(DataGridViewCellEventArgs e)
    {
         System.Windows.Forms.Clipboard.Clear();
        base.OnCellEnter(e);
    }
       
        //throw new NotImplementedException();

    protected override void OnCellMouseEnter(DataGridViewCellEventArgs e)
    {
        System.Windows.Forms.Clipboard.Clear();
        base.OnCellMouseEnter(e);
    }

    public override bool BeginEdit(bool selectAll)
    {
        System.Windows.Forms.Clipboard.Clear();
        return base.BeginEdit(selectAll);
    }

    /// <summary>
    /// Enter key will act as Tab control
    /// Elseif Part is Used for Focus to InnerGrid
    /// <summary>
    protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
    {
        if (keyData == Keys.Escape)
        {
            if (dgv.Visible)
            {
                dgv.Visible = false;

            }
        }

        //Enter Key Will Act as Tab
        if (keyData == Keys.Enter)
        {
            if (this.CurrentRow != null)
            {
                if (dgv.CurrentRow != null && dgv.Visible)
                {
                    if (!(dgv.CurrentRow.Cells[0].Value == DBNull.Value))
                    {
                        if (aryFirstGridParam.Length == arySecondGridParam.Length)
                        {
                            sCellvalue = Convert.ToString(dgv.CurrentRow.Cells[arySecondGridParam[0]].Value);
                            for (int i = 0; i <= aryFirstGridParam.Length - 1; i++)
                            {
                                string colNamesFirst = aryFirstGridParam[i];
                                string colNamesSecond = arySecondGridParam[i];
                                this.CurrentRow.Cells[colNamesFirst].Value = dgv.CurrentRow.Cells[colNamesSecond].Value;//assigning values
                            }
                            // this.CurrentCell = this.CurrentRow.Cells[aryFirstGridParam.Length];
                            this.CurrentCell = this.CurrentRow.Cells[PiFocusIndex];
                            this.CommitEdit(DataGridViewDataErrorContexts.Commit);
                            bFlag = false;
                            dgv.ClearSelection();
                            dgv.Visible = false;
                            this.Focus();
                        }
                    }
                }
            }

            SendKeys.Send("{Tab}");
            return true;
        }
        else if (keyData == Keys.Down)///focus to InnerGrid
        {
            if ( dgv.Visible == true && this.CurrentCell.ColumnIndex == PiColumnIndex)
            {
                if (dgv.CurrentCell != null)
                {
                    dgv.CurrentCell = dgv[0, 0];
                    dgv.Focus();
                }
            }
            return false;
        }
        else
        {
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
    
    /// <summary>
    /// Adding a Textbox control To The First column
    /// Editing Combobox  adding to the datagridview combobox columns
    /// </summary>
    private void ClsInnerGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
    {
        System.Windows.Forms.Clipboard.Clear();// avoiding Paste option
        if (e.Control is DataGridViewTextBoxEditingControl)
        {
            ((TextBox)e.Control).ShortcutsEnabled = false;//disabling Contextmenu strip for copy paste
            if (this.CurrentCell.ColumnIndex > -1)
            {
                if (this.CurrentCell.ColumnIndex == PiColumnIndex)// adding a TextboxControl to cell for Working TextChanged
                {
                    this.TxtB = (TextBox)e.Control;

                    //Remove an existing event-handler, if present, to avoid 
                    //adding multiple handlers when the editing control is reused.
                    this.TxtB.TextChanged -= this.textBox_TextChanged;
                    this.TxtB.TextChanged += this.textBox_TextChanged;
                    dgv.Visible = false;
                }
            }
        }

        if ((this.CurrentCell) is DataGridViewComboBoxCell)// Adding editing combobox to DataGridview
        {
            this.combo = (ComboBox)e.Control;
            if (this.combo != null)
            {
                this.combo.DropDownHeight = 120;
                this.combo.DropDownStyle = ComboBoxStyle.DropDown;
                this.combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                this.combo.AutoCompleteSource = AutoCompleteSource.ListItems;
                //Remove an existing event-handler, if present, to avoid 
                //adding multiple handlers when the editing control is reused.
                //this.combo.KeyPress -= this.ComboBox_KeyPress;
                //this.combo.KeyPress += this.ComboBox_KeyPress;
                this.combo.KeyDown -= this.ComboBox_KeyDown;
                this.combo.KeyDown += this.ComboBox_KeyDown;
                this.combo.SelectedIndexChanged -= new EventHandler(combo_SelectedIndexChanged);
                this.combo.SelectedIndexChanged += new EventHandler(combo_SelectedIndexChanged);
            }
            this.NotifyCurrentCellDirty(true);
        }
        else if (e.Control is DataGridViewTextBoxEditingControl)
        {
            e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
        }
    }

    void combo_SelectedIndexChanged(object sender, EventArgs e)
    {
        // added to avoid changing background-color of combo colum to black at runtime
        ((ComboBox)sender).DropDown -= new EventHandler(ClsInnerGridBar_DropDown);
        ((ComboBox)sender).DropDown += new EventHandler(ClsInnerGridBar_DropDown);
        ((ComboBox)sender).GotFocus -= new EventHandler(ClsInnerGridBar_DropDown);
        ((ComboBox)sender).GotFocus += new EventHandler(ClsInnerGridBar_DropDown);
    }

    void ClsInnerGridBar_DropDown(object sender, EventArgs e)
    {
        ((ComboBox)sender).BackColor = Color.White;
    }
    
    private void Control_KeyPress(object sender, KeyPressEventArgs e)
    {
        DataGridViewTextBoxEditingControl CurrentCol = (DataGridViewTextBoxEditingControl)sender;

        // Setting numeric cell
        if (iNumericCols.Contains(CurrentCol.EditingControlDataGridView.CurrentCell.ColumnIndex))
        {
            if (!(Char.IsDigit(e.KeyChar)) && (!(e.KeyChar == (char)8)))
            {
                e.Handled = true;
            }
        }
        // Setting decimal cells
        else if (iDecimalCols.Contains(CurrentCol.EditingControlDataGridView.CurrentCell.ColumnIndex))
        {
            if (e.KeyChar == (char)46)
            {
                if (CurrentCol.Text.Trim().Contains("."))
                {
                    e.Handled = true;
                }
            }
            else if (!char.IsDigit(e.KeyChar) && (!(e.KeyChar == (char)8)))
            {
                e.Handled = true;
            }
        }
        // Alphanumeric characters
        else if (iAlphaNumericCols.Contains(CurrentCol.EditingControlDataGridView.CurrentCell.ColumnIndex))
        {
            if (!char.IsLetterOrDigit(e.KeyChar) && (!(e.KeyChar == (char)8)))
            {
                e.Handled = true;
            }
        }
        // Setting negative value cells
        else if (iNegativeValueCols.Contains(CurrentCol.EditingControlDataGridView.CurrentCell.ColumnIndex))
        {
            if (e.KeyChar == (char)46)
            {
                if (CurrentCol.Text.Trim().Contains("."))
                {
                    e.Handled = true;
                }
            }
            else if (!char.IsDigit(e.KeyChar) && (!(e.KeyChar == (char)8)))
            {
                if (e.KeyChar != (char)45)
                    e.Handled = true;
            }
        }
        else if (iCapsLockCols.Contains(CurrentCol.EditingControlDataGridView.CurrentCell.ColumnIndex))
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
        }
    }

    private void ComboBox_KeyDown(object sender, KeyEventArgs e)
    {
        this.combo.DroppedDown = false;
    }

    /// <summary>
    /// Editing Control TextBox-TextChanged Event for filtering InnerGrid data for each Textchange.
    /// Adding InnerGrid To DataGridView
    /// Adding 3 events of InnerGrid-KeyDown,RowEnter,MouseDoubleClick
    /// </summary>
    private void textBox_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBoxText = TxtB.Text.Trim();
            Textbox_TextChanged(this, args);

            if (this.CurrentCell.ColumnIndex == PiColumnIndex)// adding a TextboxControl to cell for Working TextChanged
            {
                if ((this.dgv.Visible != true))// Adding InnerGrid Only One Time
                {
                    if (sCellvalue != TextBoxText)//clear existing cell value in TextChange
                    {
                    }
                    dgv.Visible = true;
                    //-----------------------------------------------------------------------------------------------------------------
                    //-----Adding InnerGrid To Form-------
                    Form frmCurrentform;
                    frmCurrentform = this.FindForm();

                    RectParentArea = frmCurrentform.ClientRectangle;

                    int x, y, iPointL, iPointB;
                    x = this.Location.X + this.Parent.Location.X;
                    y = this.Location.Y + this.Parent.Location.Y;
                    iPointL = this.GetCellDisplayRectangle(this.CurrentCell.ColumnIndex, this.CurrentCell.RowIndex, false).Left ;//+ pnlLeft
                    iPointB = this.GetCellDisplayRectangle(this.CurrentCell.ColumnIndex, this.CurrentCell.RowIndex, false).Bottom ;//+ pnlTop
                    
                    intCellWidth = this.GetCellDisplayRectangle(this.CurrentCell.ColumnIndex, this.CurrentCell.RowIndex, false).Bottom - this.GetCellDisplayRectangle(this.CurrentCell.ColumnIndex, this.CurrentCell.RowIndex, false).Top;//+ pnlTop

                    dgv.BackgroundColor = Color.White;
                    dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    if (iGridHeight <= 0)
                        iGridHeight = 200;
                    if (iGridWidth <= 0)
                        iGridWidth = 350;
                    dgv.Width = iGridWidth;
                    dgv.Height = iGridHeight;
                    if (bBothScrollBar)
                        dgv.ScrollBars = ScrollBars.Both;


                    this.dgv.Location = new System.Drawing.Point(x + iPointL, y + iPointB);//Setting the Location of InnerGrid


                    if (RectParentArea.Top + RectParentArea.Height < dgv.Height + dgv.Location.Y)
                    {

                        dgv.Location = new Point(dgv.Location.X, dgv.Location.Y - (dgv.Height + intCellWidth));//Showing grid CEll top
                    }
                    
                    frmCurrentform.LocationChanged -= new EventHandler(frmCurrentform_LocationChanged);
                    frmCurrentform.LocationChanged += new EventHandler(frmCurrentform_LocationChanged);
                    frmCurrentform.Resize -= new EventHandler(frmCurrentform_Resize);
                    frmCurrentform.Resize += new EventHandler(frmCurrentform_Resize);

                    this.Parent.Parent.Controls.Add(this.dgv);
                    this.dgv.BringToFront();
                    this.dgv.KeyDown -= this.dgv_KeyDown;//removing Remove an existing event-handler
                    this.dgv.KeyDown += this.dgv_KeyDown;// adding dgv keydown event
                    this.dgv.RowEnter -= this.dgv_RowEnter;
                    this.dgv.RowEnter += this.dgv_RowEnter;
                    this.dgv.MouseDoubleClick -= this.dgv_MouseDoubleClick;
                    this.dgv.MouseDoubleClick += this.dgv_MouseDoubleClick;

                    this.dgv.DataError -= new DataGridViewDataErrorEventHandler(dgv_DataError);
                    this.dgv.DataError += new DataGridViewDataErrorEventHandler(dgv_DataError);
                    this.dgv.CurrentCellDirtyStateChanged -= new EventHandler(dgv_CurrentCellDirtyStateChanged);
                    this.dgv.CurrentCellDirtyStateChanged += new EventHandler(dgv_CurrentCellDirtyStateChanged);

                    this.dgv.Leave -= new EventHandler(dgv_Leave);
                    this.dgv.Leave += new EventHandler(dgv_Leave);

                   

                    dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                    dgv.RowHeadersVisible = false;
                    dgv.ReadOnly = true;
                    dgv.AllowUserToDeleteRows = false;
                    bFlag = true;
                    dgv.BringToFront();
                    
                }

                if (PsQuery != null)
                {
                    if (TextBoxText != null)
                    {
                        dgv.DataSource = this.GetData();
                        //if (HideColumn != null)//For column Hide
                        //{
                        //    dgv.Columns[HideColumn].Visible = false;
                        //}

                        //if (dgv.RowCount != 0)//For Selection
                        //{
                        //    dgv.Rows[0].Selected = true;
                        //}
                        foreach (DataGridViewColumn dc in dgv.Columns)
                            dc.Visible = true;
                        if (ColumnsToHide != null)
                        {
                            foreach (string strColumn in ColumnsToHide)
                            {
                                dgv.Columns[strColumn].Visible = false;
                            }
                        }
                    }
                }
                else if (dtDataSource != null)
                {
                    dgv.DataSource = this.dtDataSource;
                    foreach (DataGridViewColumn dc in dgv.Columns)
                        dc.Visible = true;
                    if (ColumnsToHide != null)
                    {
                        foreach (string strColumn in ColumnsToHide)
                        {
                            dgv.Columns[strColumn].Visible = false;
                        }
                    }
                    dgv.ClearSelection();
                }
                else
                {
                    dgv.Visible = false;
                    dgv.DataSource = null;
                }
            }
            else
            {
                dgv.Visible = false;
                dgv.DataSource = null;
            }
        }
        catch (Exception)
        {
            //Example:
            MessageBox.Show("Please Insert PsQuery Field \n" + "For EXAMPLE: " +
                     " private void clsInnerGrid1_Textbox_TextChanged(object sender, EventArgs e)\n " +
                           " \t{clsInnerGrid1.field = Id; \n" +
                           "\tstring[] First = " + "{ " + "column6" + "," + "column7" + "," + "column8" + "}" + ";\n" +
                           "\t string[] second = {" + "ID" + "," + "NAME" + " ," + "MOBILE" + "};\n" +
                           "\tclsInnerGrid1.aryFirstGridParam = First;\n" +
                           "\tclsInnerGrid1.arySecondGridParam = second;\n" +
                           "\tclsInnerGrid1.PsQuery = " + "select [Id] as ID,[names] as NAME,[MobNo] as MOBILE from contact where " +
                           " clsInnerGrid1.field " + " like  '" + "((clsInnerGrid1.TextBoxText))" + "%'}\n", "ADD EVENT", MessageBoxButtons.OK);

        }
    }

    void dgv_Leave(object sender, EventArgs e)
    {
        if (dgv.Visible)
        {
            dgv.Visible = false;
        }
    }

    void frmCurrentform_Resize(object sender, EventArgs e)
    {
        if (dgv.Visible)
        {
            dgv.Visible = false;
        }
    }

    void frmCurrentform_LocationChanged(object sender, EventArgs e)
    {
        if (dgv.Visible)

        {
            dgv.Visible = false;
        }
    }
    private void dgv_CurrentCellDirtyStateChanged(object sender, EventArgs e)
    {
        if (dgv.IsCurrentCellDirty)
        {
            if (dgv.CurrentCell != null)
            {
                dgv.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }
    }
    private void dgv_DataError(object sender, DataGridViewDataErrorEventArgs e)//inner grid data error handling
    {
        try
        {
            return;
        }
        catch (Exception)
        {
            return;
        }
    }
    /// <summary>
    /// Function GetData for FillIng InnerGrid 
    /// </summary>
    private DataTable GetData()
    {
        mObjConnection = new DataLayer();
        return mObjConnection.ExecuteDataSet(PsQuery).Tables[0];
    }

    /// <summary>
    /// Showing InnerGrid Cellvalue In DataGridview Currentcell In Each Rowselection
    /// </summary>
    private void dgv_RowEnter(object sender, DataGridViewCellEventArgs e)//Inner gird rowenter event
    {
        if (this.CurrentRow != null)
        {
            if ((dgv.Rows.Count >= 1) && (dgv.SelectedRows.Count >= 1) && (bFlag != false))
            {
                if (dgv.Visible == true)
                this.CurrentRow.Cells[0].Value = dgv.Rows[e.RowIndex].Cells[0].Value;
            }
        }
    }

    private void eventhandler(EventHandler<EventArgs> eventHandler, EventArgs args)
    {
        throw new NotImplementedException();
    }

    private void ClsInnerGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)//for combobox error handling
    {
        try
        {
            return;
        }
        catch (Exception)
        {
            return;
        }
    }

    /// <summary>
    /// InnerGridCell values assign To DataGridview in the Enter keypress
    /// </summary>
    private void dgv_KeyDown(object sender, KeyEventArgs e)
    {
        dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        if (e.KeyData == Keys.Enter)
        {
            if (this.CurrentRow != null)
            {
                if (!(dgv.CurrentRow.Cells[0].Value == DBNull.Value))
                {
                    if (aryFirstGridParam.Length == arySecondGridParam.Length)
                    {
                        sCellvalue = Convert.ToString(dgv.CurrentRow.Cells[arySecondGridParam[0]].Value);
                        for (int i = 0; i <= aryFirstGridParam.Length - 1; i++)
                        {
                            string colNamesFirst = aryFirstGridParam[i];
                            string colNamesSecond = arySecondGridParam[i];
                            if (this.CurrentRow != null)//Modified
                            this.CurrentRow.Cells[colNamesFirst].Value = dgv.CurrentRow.Cells[colNamesSecond].Value;//assigning values
                        }
                        if (this.CurrentRow != null && this.CurrentCell != null)//Modified
                        this.CurrentCell = this.CurrentRow.Cells[PiFocusIndex];
                        this.CommitEdit(DataGridViewDataErrorContexts.Commit);
                        bFlag = false;
                        dgv.ClearSelection();
                        dgv.Visible = false;
                        this.Focus();
                    }
                }
            }
        }
        else if (e.KeyCode == Keys.Escape)
            dgv.Hide();
    }

    /// <summary>
    /// InnerGridCell values assign To DataGridview in the MouseDoubleClick
    /// </summary>
    private void dgv_MouseDoubleClick(object sender, MouseEventArgs e)
    {
        if (this.CurrentRow != null)
        {

            if (!(dgv.CurrentRow.Cells[0].Value == DBNull.Value))
            {
                if (aryFirstGridParam.Length == arySecondGridParam.Length)
                {
                    sCellvalue = Convert.ToString(dgv.CurrentRow.Cells[arySecondGridParam[0]].Value);
                    for (int i = 0; i <= aryFirstGridParam.Length - 1; i++)
                    {
                        string colNamesFirst = aryFirstGridParam[i];
                        string colNamesSecond = arySecondGridParam[i];
                        if (this.CurrentRow != null)//Modified
                        this.CurrentRow.Cells[colNamesFirst].Value = dgv.CurrentRow.Cells[colNamesSecond].Value;
                    }
                    if (this.CurrentRow != null && this.CurrentCell != null)//Modified
                    this.CurrentCell = this.CurrentRow.Cells[PiFocusIndex];
                    this.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    bFlag = false;
                    dgv.ClearSelection();
                    dgv.Visible = false;
                    this.Focus();
                }
            }
        }
    }

    private void ClsInnerGrid_Leave(object sender, EventArgs e)//
    {
        if (!dgv.Focused)
        {
            dgv.Visible = false;
        }
    }
   

   
}

       

        

