﻿using System;
using System.Windows.Forms;

    /// <summary>
    /// Editting combobox and tab on Enter key
    /// created by : Arun,Renjith, Devi
    /// </summary>
    public class ClsDataGirdViewX : DevComponents.DotNetBar.Controls.DataGridViewX
    {
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ClsDataGirdView
            // 
            //this.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ClsDataGirdView_EditingControlShowing);
            //this.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ClsDataGirdView_DataError);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
        }

        #region Property setting for DataGridView

        private int[] iNumericCols = { };
        private int[] iDecimalCols = { };
        private int[] iAlphaNumericCols = { };
        private int[] iCapsLockCols = { };
        private int[] iNegativeValueCols = { };
        private bool bHasSlNo;
        private bool bAddNewRow;
        private int iLastRowIndex;

        public int[] NumericCols
        {
            get { return iNumericCols; }
            set { iNumericCols = value; }
        }

        public int[] DecimalCols
        {
            get { return iDecimalCols; }
            set { iDecimalCols = value; }
        }

        public int[] AlphaNumericCols
        {
            get { return iAlphaNumericCols; }
            set { iAlphaNumericCols = value; }
        }

        public int[] CapsLockCols
        {
            get { return iCapsLockCols; }
            set { iCapsLockCols = value; }
        }

        public int[] NegativeValueCols
        {
            get { return iNegativeValueCols; }
            set { iNegativeValueCols = value; }
        }

        public bool HasSlNo
        {
            get { return bHasSlNo; }
            set { bHasSlNo = value; }
        }

        public bool AddNewRow
        {
            get { return bAddNewRow; }
            set { bAddNewRow = value; }
        }

        public int LastRowIndex
        {
            get { return iLastRowIndex; }
            set { iLastRowIndex = value; }
        }

        #endregion // Property setting for DataGridView

        ComboBox combo;
        public ClsDataGirdViewX()// constructor
        {
            InitializeComponent();
        }



        /// <summary>
        /// Enter Key act as Tab key
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            //Enter Key Will Act as Tab
            if (keyData == System.Windows.Forms.Keys.Enter)
            {
                if (!bAddNewRow)
                    SendKeys.Send("{Tab}");
                else
                {
                    if (this.CurrentCell.ColumnIndex == iLastRowIndex)
                    {
                        if (this.CurrentRow.Index == this.RowCount - 1)
                        {
                            //this.Rows.Add();
                            //this.CurrentCell = this[0, this.CurrentRow.Index + 1];
                            this.CommitEdit(DataGridViewDataErrorContexts.Commit);
                            //DataGridViewCellEventArgs e = new DataGridViewCellEventArgs(this.CurrentCell.ColumnIndex, this.CurrentCell.RowIndex);
                            //OnCellValueChanged(e);
                            KeyEventArgs en = new KeyEventArgs(System.Windows.Forms.Keys.Enter);
                            OnKeyDown(en);
                        }
                        else
                            SendKeys.Send("{Tab}");
                    }
                    else
                        SendKeys.Send("{Tab}");
                }

                return true;
            }
            else
            {
                return base.ProcessCmdKey(ref msg, keyData);
            }
        }

        /// <summary>
        /// Editing Combobox  adding to the datagridview combobox column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void ClsDataGirdView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        //{
        //    if ((this.CurrentCell) is DataGridViewComboBoxCell)// Adding editing combobox to DataGridview
        //    {
        //        this.combo = (ComboBox)e.Control;
        //        if (this.combo != null)
        //        {
        //            this.combo.DropDownHeight = 134;
        //            this.combo.DropDownStyle = ComboBoxStyle.DropDown;
        //            this.combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
        //            this.combo.AutoCompleteSource = AutoCompleteSource.ListItems;
        //            //this.combo.SelectedIndexChanged -= this.ComboBox_SelectedIndexChanged;//Remove an existing event-handler
        //            //this.combo.SelectedIndexChanged += this.ComboBox_SelectedIndexChanged;//Adding  event-handler
        //            //Remove an existing event-handler, if present, to avoid 
        //            //adding multiple handlers when the editing control is reused.
        //            this.combo.KeyPress -= this.ComboBox_KeyPress;//remove 
        //            // Add the event handler. 
        //            this.combo.KeyPress += this.ComboBox_KeyPress;
        //        }
        //        this.NotifyCurrentCellDirty(true);
        //    }
        //    else if (e.Control is DataGridViewTextBoxEditingControl)
        //    {
        //        //e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
        //    }
        //}

        //Key Press event
        private void ComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.combo.DroppedDown = false;
        }

        private void ClsDataGirdView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                return;
            }
            catch (Exception)
            {
                return;
            }
        }

      
    }

