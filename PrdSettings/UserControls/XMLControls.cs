﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace XMLControls
{
    public class XMLTxtCntl : TextBox
    {
        public string strkey { get; set; }
        public string strvalue { get; set; }
        public string strTxtAlgn { get; set; }
        public string strFontSize { get; set; }
        public string strTextAlign { get; set; }
        public string strBgColor { get; set; }
        public string strBorderStyle { get; set; }
        public string strBorderWidth { get; set; }
        public string strDataSetName { get; set; }
        public string strvisibilty { get; set; }
        public string strGroupingName { get; set; }
        public string strGroupExpressions { get; set; }
        public string strDefaultName { get; set; }
        public int intZindex { get; set; }
        public int intFontWeight { get; set; }
      
    }
    public class XMLLstCntl : GroupBox
    {
        public string strvalue { get; set; }
        public string strDataSetName { get; set; }
        public string strvisibilty { get; set; }
        public string strGroupingName { get; set; }
        public string strGroupExpressions { get; set; }
        public string strDefaultName { get; set; }
        public string strBorderStyle { get; set; }
        public int intZindex { get; set; }
    }
    public class XMLTbCntl : DataGridView
    {
        public string strvalue { get; set; }
        public string strDataSetName { get; set; }
        public string strvisibilty { get; set; }
        public string strGroupingName { get; set; }
        public string strGroupExpressions { get; set; }
        public string strDefaultName { get; set; }
        public string strCellName { get; set ;}
        public string strBorderStyle { get; set; }
        public string strBorderWidth { get; set; }
        public string strFontSize { get; set; }
        public int intFontWeight { get; set; }
        public int intZindex { get; set; }
    }
    public class XMLImgCntl : PictureBox
    {
        public string strvalue { get; set; }
        public string strDataSetName { get; set; }
        public string strGroupingName { get; set; }
        public string strGroupExpressions { get; set; }
        public string strDefaultName { get; set; }
        public string strSizing { get; set; }
        public string strMIMETYPE { get; set; }
        public string strSource { get; set; }
        public int intZindex { get; set; }

    }
}