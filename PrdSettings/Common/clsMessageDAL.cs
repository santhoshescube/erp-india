﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;


    public class clsMessageDAL
    {
        public static DataTable GetMessages(int FormID, int ProductID)
        {
            return new DataLayer().ExecuteDataTable("spMessage", new List<SqlParameter> { 
                new SqlParameter("@Mode", "SEL"),
                new SqlParameter("@FormID", FormID)
            });
        }
    }

