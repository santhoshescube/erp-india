﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsConnection
/// purpose : Handle Database Transactions
/// created : Bijoy George
/// Date    : 01-07-2009
/// </summary>

public class clsConnection
{
    string mConnectionString = "";
    SqlConnection mConnection;
    SqlCommand mCommand;
    SqlTransaction objTrans;

    public clsConnection()
    {
        mConnectionString = MsDb.ConnectionString;
        //"Data Source=" + ClsCommonSettings.ServerName + ";Initial Catalog=" + ClsCommonSettings.DbName + ";uid=msSTadmin;pwd=msST!2345;";
        
        mConnection = new SqlConnection(mConnectionString);
    }

    public SqlConnection SqlConnection()
    {
        return mConnection;
    }

    public void Connect()
    {
        if (mConnection.State == ConnectionState.Broken)
        {
            mConnection.Close();
        }
        if (mConnection.State == ConnectionState.Closed)
        {
            mConnection.Open();
        }
    }

    public void Disconnect()
    {
        mConnection.Close();
    }

    public bool ExecutesCommand(string insertString)
    {
        Connect();

        mCommand = new SqlCommand(insertString, mConnection);
        mCommand.ExecuteNonQuery();

        //Disconnect();
        return true;
    }

    public bool ExecutesScalarCommand(string dbString)
    {
        Connect();
        mCommand = new SqlCommand(dbString, mConnection);
        mCommand.ExecuteNonQuery();

        return true;
    }

    public object ExecuteScalar(string StoredProcedureName, ArrayList parameters)
    {
        Connect();
        mCommand = new SqlCommand(StoredProcedureName, mConnection);
        mCommand.CommandType = CommandType.StoredProcedure;
        if (parameters != null && parameters.Count != 0)
        {
            foreach (SqlParameter Param in parameters)
                mCommand.Parameters.Add(Param);
        }
        return mCommand.ExecuteScalar();
    }

    public bool Chk_Duplicate(string SqlString)
    {
        bool bRet = false;
        Connect();
        mCommand = new SqlCommand(SqlString, mConnection);
        SqlDataReader dr = mCommand.ExecuteReader(CommandBehavior.CloseConnection);
        if (dr.Read() == true)
            bRet = true;
        else
            bRet = false;

        dr.Close();
        //Disconnect();

        return bRet;
    }

    public SqlDataReader ExecutesReader(string cmdText)
    {
        Connect();
        SqlDataReader Reader;
        mCommand = new SqlCommand(cmdText, mConnection);
        mCommand.CommandType = CommandType.Text;
        Reader = mCommand.ExecuteReader(CommandBehavior.CloseConnection);
        return Reader;
    }

    public SqlDataReader ExecutesReader(string commandText, ArrayList parameters, CommandBehavior cmdBehavior)
    {
        SqlDataReader Reader;

        Connect();

        SqlCommand mCommand = new SqlCommand(commandText, mConnection);

        //SqlCmd.CommandTimeout = Settings.CommandTimeOutValue;

        mCommand.CommandType = CommandType.StoredProcedure;

        if (parameters != null && parameters.Count != 0)
        {
            foreach (SqlParameter Param in parameters)
            {
                mCommand.Parameters.Add(Param);
            }
        }

        //Try-catch blocks are not needed here since the exceptions should be handled in the calling code

        Reader = mCommand.ExecuteReader(cmdBehavior);

        return Reader;
    }

    public bool ExecutesQuery(ArrayList parameters, string cmdText)
    {
        Connect();
        mCommand = new SqlCommand(cmdText, mConnection);
        mCommand.CommandType = CommandType.StoredProcedure;
        mCommand.Transaction = objTrans;

        if (parameters.Count != 0)
        {
            foreach (SqlParameter param in parameters)
                mCommand.Parameters.Add(param);
        }

        int rowsaff = mCommand.ExecuteNonQuery();

        ArrayList resultparametes = new ArrayList(mCommand.Parameters);
        //Disconnect();
        return true;
    }

    public bool ExecutesQuery(string cmdText)
    {
        Connect();
        mCommand = new SqlCommand(cmdText, mConnection);
        mCommand.CommandType = CommandType.Text;

        int rowsaff = mCommand.ExecuteNonQuery();

        //Disconnect();
        return true;
    }

    public bool ExecutesQuery(ArrayList parameters, string cmdText, out int iReturnId)
    {
        Connect();
        iReturnId = 0;
        mCommand = new SqlCommand(cmdText, mConnection);
        mCommand.CommandType = CommandType.StoredProcedure;
        mCommand.Transaction = objTrans;

        if (parameters.Count != 0)
        {
            foreach (SqlParameter param in parameters)
                mCommand.Parameters.Add(param);
        }

        int rowsaff = mCommand.ExecuteNonQuery();
        ArrayList resultparametes = new ArrayList(mCommand.Parameters);

        foreach (SqlParameter param in resultparametes)
        {
            if (param.Direction == ParameterDirection.ReturnValue)
                iReturnId = Convert.ToInt32(param.Value);
        }

        //Disconnect();
        return true;

    }

    public bool ExecutesQuery(ArrayList parameters, string cmdText, out long iRetout)
    {
        Connect();
        iRetout = 0;
        mCommand = new SqlCommand(cmdText, mConnection);
        mCommand.CommandType = CommandType.StoredProcedure;

        if (parameters.Count != 0)
        {
            foreach (SqlParameter param in parameters)
                mCommand.Parameters.Add(param);
        }

        int rowsaff = mCommand.ExecuteNonQuery();
        ArrayList resultparametes = new ArrayList(mCommand.Parameters);

        foreach (SqlParameter param in resultparametes)
        {
            if (param.Direction == ParameterDirection.Output)
                iRetout = Convert.ToInt64(param.Value);
        }

        //Disconnect();
        return true;

    }

    public bool ExecutesQuery(ArrayList parameters, string cmdText, out int iReturnId, SqlTransaction NewTransaction)
    {
        Connect();
        iReturnId = 0;

        mCommand = new SqlCommand(cmdText, mConnection, NewTransaction);
        mCommand.CommandType = CommandType.StoredProcedure;

        if (parameters.Count != 0)
        {
            foreach (SqlParameter param in parameters)
                mCommand.Parameters.Add(param);
        }

        int rowsaff = mCommand.ExecuteNonQuery();
        ArrayList resultparametes = new ArrayList(mCommand.Parameters);

        foreach (SqlParameter param in resultparametes)
        {
            if (param.Direction == ParameterDirection.ReturnValue)
                iReturnId = Convert.ToInt32(param.Value);
        }

        //Disconnect();
        return true;
    }

    public DataSet FillDataSet(string commandText)
    {
        DataSet ResultSet;
        Connect();
        mCommand = new SqlCommand(commandText, mConnection);
        SqlDataAdapter oDa = new SqlDataAdapter(mCommand);
        mCommand.CommandType = CommandType.Text;
        ResultSet = new DataSet();

        oDa.Fill(ResultSet);
        oDa.Dispose();

        //Disconnect();

        return ResultSet;
    }

    public DataTable FillDataTable(string commandText)
    {
        DataTable ResultDat;
        Connect();
        mCommand = new SqlCommand(commandText, mConnection);
        SqlDataAdapter oDa = new SqlDataAdapter(mCommand);
        mCommand.CommandType = CommandType.Text;
        ResultDat = new DataTable();

        oDa.Fill(ResultDat);
        oDa.Dispose();

        //Disconnect();

        return ResultDat;
    }

    public DataSet FillDataSet(string commandText, ArrayList parameters)
    {
        DataSet ResultSet;
        Connect();
        SqlDataAdapter SqlDa = new SqlDataAdapter(commandText, mConnection);

        SqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;

        if (parameters != null && parameters.Count != 0)
        {
            foreach (SqlParameter Param in parameters)
            {
                SqlDa.SelectCommand.Parameters.Add(Param);
            }
        }
        ResultSet = new DataSet();
        //Try-catch blocks are not needed here since the exceptions should be handled in the calling code
        SqlDa.Fill(ResultSet);
        SqlDa.Dispose();

        //Disconnect();
        return ResultSet;
    }

    public DataTable FillDataTable(string commandText, ArrayList parameters)
    {
        DataTable ResultDat;
        Connect();
        SqlDataAdapter SqlDa = new SqlDataAdapter(commandText, mConnection);

        SqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;

        if (parameters != null && parameters.Count != 0)
        {
            foreach (SqlParameter Param in parameters)
            {
                SqlDa.SelectCommand.Parameters.Add(Param);
            }
        }
        ResultDat = new DataTable();
        //Try-catch blocks are not needed here since the exceptions should be handled in the calling code
        SqlDa.Fill(ResultDat);
        SqlDa.Dispose();

        //Disconnect();
        return ResultDat;
    }

    public DataSet FillDataSet(string commandText, ArrayList parameters, out ArrayList resultParameters)
    {
        DataSet ResultSet;
        Connect();
        SqlDataAdapter SqlDa = new SqlDataAdapter(commandText, mConnection);

        SqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;

        if (parameters != null && parameters.Count != 0)
        {
            foreach (SqlParameter Param in parameters)
            {
                SqlDa.SelectCommand.Parameters.Add(Param);
            }
        }

        ResultSet = new DataSet();
        //Try-catch blocks are not needed here since the exceptions should be handled in the calling code
        SqlDa.Fill(ResultSet);
        resultParameters = new ArrayList(SqlDa.SelectCommand.Parameters);

        SqlDa.Dispose();
        //Disconnect();
        return ResultSet;
    }

    //get the current date from server by bijoy
    public string GetSysDate()
    {
        string strDate = "";
        Connect();
        SqlCommand cmdDate = new SqlCommand("select convert(nvarchar,getdate(),106)sdate", mConnection);
        SqlDataReader drDate = cmdDate.ExecuteReader();

        if (drDate.Read() == true)
            strDate = Convert.ToString(drDate[0]);
        else
            strDate = ClsCommonSettings.GetServerDate().ToShortDateString();
        drDate.Close();

        //Disconnect();

        return strDate;
    }

    public void Fill_Combo(System.Windows.Forms.ComboBox cmb, string sqlstring, string sVId, string sVName)
    {
        Connect();
        SqlDataAdapter objDa = new SqlDataAdapter(sqlstring, mConnection);
        DataTable dt = new DataTable();
        objDa.Fill(dt);

        cmb.ValueMember = sVId;
        cmb.DisplayMember = sVName;
        cmb.DataSource = dt;

        //Disconnect();
    }

    //public void Fill_Combo(DevComponents.DotNetBar.Controls.ComboBoxEx cmb, string sqlstring, string sVId, string sVName)
    //{
    //    Connect();
    //    SqlDataAdapter objDa = new SqlDataAdapter(sqlstring, mConnection);
    //    DataTable dt = new DataTable();
    //    objDa.Fill(dt);

    //    cmb.ValueMember = sVId;
    //    cmb.DisplayMember = sVName;
    //    cmb.DataSource = dt;

    //    Disconnect();
    //}

    public void Fill_Combo(System.Windows.Forms.DataGridViewComboBoxColumn cmb, string sqlstring, string sVId, string sVName)
    {
        Connect();
        SqlDataAdapter objDa = new SqlDataAdapter(sqlstring, mConnection);
        DataTable dt = new DataTable();
        objDa.Fill(dt);

        cmb.ValueMember = sVId;
        cmb.DisplayMember = sVName;
        cmb.DataSource = dt;

        //Disconnect();
    }

    public void Fill_Combo(System.Windows.Forms.ComboBox cmb, string cmdText, ArrayList parameters, string sVId, string sVName)
    {
        Connect();
        SqlDataAdapter SqlDa = new SqlDataAdapter(cmdText, mConnection);

        SqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;

        if (parameters != null && parameters.Count != 0)
        {
            foreach (SqlParameter Param in parameters)
            {
                SqlDa.SelectCommand.Parameters.Add(Param);
            }
        }
        DataTable dt = new DataTable();
        SqlDa.Fill(dt);

        cmb.ValueMember = sVId;
        cmb.DisplayMember = sVName;
        cmb.DataSource = dt;

        //Disconnect();
    }

    public DataTable Fill_Combo(string cmdText, ArrayList parameters)
    {
        Connect();
        SqlDataAdapter SqlDa = new SqlDataAdapter(cmdText, mConnection);

        SqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;

        if (parameters != null && parameters.Count != 0)
        {
            foreach (SqlParameter Param in parameters)
            {
                SqlDa.SelectCommand.Parameters.Add(Param);
            }
        }
        DataTable dt = new DataTable();
        SqlDa.Fill(dt);

        return dt;
        //Disconnect();
    }

    public void Fill_Combo(System.Windows.Forms.DataGridViewComboBoxColumn cmb, string cmdText, ArrayList parameters, string sVId, string sVName)
    {
        Connect();
        SqlDataAdapter SqlDa = new SqlDataAdapter(cmdText, mConnection);

        SqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;

        if (parameters != null && parameters.Count != 0)
        {
            foreach (SqlParameter Param in parameters)
            {
                SqlDa.SelectCommand.Parameters.Add(Param);
            }
        }
        DataTable dt = new DataTable();
        SqlDa.Fill(dt);

        cmb.ValueMember = sVId;
        cmb.DisplayMember = sVName;
        cmb.DataSource = dt;

        //Disconnect();
    }

    public void Fill_Combo(System.Windows.Forms.DataGridViewComboBoxCell cmb, string cmdText, ArrayList parameters, string sVId, string sVName)
    {
        Connect();
        SqlDataAdapter SqlDa = new SqlDataAdapter(cmdText, mConnection);

        SqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;

        if (parameters != null && parameters.Count != 0)
        {
            foreach (SqlParameter Param in parameters)
            {
                SqlDa.SelectCommand.Parameters.Add(Param);
            }
        }
        DataTable dt = new DataTable();
        SqlDa.Fill(dt);

        cmb.ValueMember = sVId;
        cmb.DisplayMember = sVName;
        cmb.DataSource = dt;

        //Disconnect();
    }

    public DataTable ExecuteDataTable(string commandText, ArrayList parameters)
    {
        DataTable ResultDat;
        Connect();
        SqlDataAdapter SqlDa = new SqlDataAdapter(commandText, mConnection);

        SqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;

        if (parameters != null && parameters.Count != 0)
        {
            foreach (SqlParameter Param in parameters)
            {
                SqlDa.SelectCommand.Parameters.Add(Param);
            }
        }
        ResultDat = new DataTable();
        //Try-catch blocks are not needed here since the exceptions should be handled in the calling code
        SqlDa.Fill(ResultDat);
        SqlDa.Dispose();

        //Disconnect();
        return ResultDat;
    }
    public bool ExecuteNonQuery(string cmdText)
    {
        Connect();
        mCommand = new SqlCommand(cmdText, mConnection);
        mCommand.CommandType = CommandType.Text;

        int rowsaff = mCommand.ExecuteNonQuery();

        //Disconnect();
        return true;
    }

    public void BeginTransaction()
    {
        Connect();
        objTrans = mConnection.BeginTransaction();
    }

    public void CommitTransaction()
    {
        //if (objTrans == null)
        //{
        //    Exception e = new Exception("No active transaction @ CommitTransaction()");
        //    e.Source = "SqlDbManager";
        //    throw e;
        //}

        objTrans.Commit();
        objTrans = null;
    }

    public void RollbackTransaction()
    {
        //if (objTrans == null)
        //{
        //    Exception e = new Exception("No active transaction @ RollbackTransaction()");
        //    e.Source = "SqlDbManager";
        //    throw e;
        //}

        objTrans.Rollback();
        objTrans = null;
    }
}