﻿
public class clsListItem
{
    private object value;
    private string text;

    public object Value
    {
        get { return value; }
        set { value = Value; }
    }
    public string Text
    {
        get { return text; }
        set { text = Text; }
    }
    public clsListItem(object Value, string Text)
    {
        value = Value;
        text = Text;
    }

    public override string ToString()
    {
        return text;
    }
}

