﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Windows.Forms;
 
public class ClsCommonUtility :  IDisposable
{
    private DataLayer objDataLayer = null;


    public DataLayer PobjDataLayer
    {
        get
        {
            if (this.objDataLayer == null)
                this.objDataLayer = new DataLayer();
            return this.objDataLayer;
        }
        set
        { this.objDataLayer = value; }
    }

    public DataTable FillCombos(string[] saFilterValues)
    {
        ArrayList prmCommon = new ArrayList();
        prmCommon.Add(new SqlParameter("@Mode", "0"));
        prmCommon.Add(new SqlParameter("@Fields", saFilterValues[0]));
        prmCommon.Add(new SqlParameter("@TableName", saFilterValues[1]));
        prmCommon.Add(new SqlParameter("@Condition", saFilterValues[2]));

        return PobjDataLayer.ExecuteDataTable("spCommonReferenceTransaction", prmCommon);
    }

    public DataTable FillCombos(string sQuery)
    {
        return  PobjDataLayer.ExecuteDataTable(sQuery); 
    }

    /// <summary>
    /// To check Duplication
    /// </summary>
    /// <param name="saParameters"></param>
    /// <returns></returns>
    public bool CheckDuplication(string[] saParameters)
    {
        ArrayList prmCommon = new ArrayList();
        prmCommon.Add(new SqlParameter("@Mode","0"));
        prmCommon.Add(new SqlParameter("@Fields", saParameters[0]));
        prmCommon.Add(new SqlParameter("@TableName", saParameters[1]));
        prmCommon.Add(new SqlParameter("@Condition", saParameters[2]));
        SqlDataReader sdCheck = PobjDataLayer.ExecuteReader("spCommonReferenceTransaction", prmCommon);
        if (sdCheck.Read())
        {
            sdCheck.Close();
            return true;
        }
        else
        {
            sdCheck.Close();
            return false;
        }
    }

    /// <summary>
    /// To get a field from a table
    /// </summary>
    /// <param name="saParameters"></param>
    /// <param name="iRecordId"></param>
    /// <returns></returns>
    public bool GetRecordValue(string[] saParameters, out int iRecordId)
    {
        iRecordId = 0;
        ArrayList prmCommon = new ArrayList();
        prmCommon.Add(new SqlParameter("@Mode", "0"));
        prmCommon.Add(new SqlParameter("@Fields", saParameters[0]));
        prmCommon.Add(new SqlParameter("@TableName", saParameters[1]));
        prmCommon.Add(new SqlParameter("@Condition", saParameters[2]));
        SqlDataReader sdCheck = PobjDataLayer.ExecuteReader("spCommonReferenceTransaction", prmCommon);
        if (sdCheck.Read())
        {
            iRecordId = Convert.ToInt32(sdCheck[0]);
            sdCheck.Close();
            return true;
        }
        else
        {
            sdCheck.Close();
            return false;
        }
    }

    /// <summary>
    /// To get multiple Fields from  a table
    /// </summary>
    /// <param name="saParameters"></param>
    /// <param name="saRecordValues"></param>
    /// <returns></returns>
    public bool GetRecordValue(string[] saParameters, out string[] saRecordValues)
    {
        int iSpiltCount = saParameters[0].Split(',').Length;
        saRecordValues = new string[iSpiltCount];
        ArrayList prmCommon = new ArrayList();
        prmCommon.Add(new SqlParameter("@Mode", "0"));
        prmCommon.Add(new SqlParameter("@Fields", saParameters[0]));
        prmCommon.Add(new SqlParameter("@TableName", saParameters[1]));
        prmCommon.Add(new SqlParameter("@Condition", saParameters[2]));
        SqlDataReader sdCheck = PobjDataLayer.ExecuteReader("spCommonReferenceTransaction", prmCommon);
        if (sdCheck.Read())
        {
            for (int i = 0; i < iSpiltCount; i++)
            {
                saRecordValues[i] = Convert.ToString(sdCheck[i]);
            }
            sdCheck.Close();
            return true;
        }
        else
        {
            sdCheck.Close();
            return false;
        }
    }

    public bool GetRecordValue(string[] saParameters, out DataTable dtRecordValues)
    {
        int iSpiltCount = saParameters[0].Split(',').Length;
        //saRecordValues = new string[iSpiltCount];
        ArrayList prmCommon = new ArrayList();
        ArrayList rParam = new ArrayList();
        prmCommon.Add(new SqlParameter("@Mode", "0"));
        prmCommon.Add(new SqlParameter("@Fields", saParameters[0]));
        prmCommon.Add(new SqlParameter("@TableName", saParameters[1]));
        prmCommon.Add(new SqlParameter("@Condition", saParameters[2]));
        dtRecordValues = PobjDataLayer.ExecuteDataSet("spCommonReferenceTransaction", prmCommon).Tables[0];
        return true;
    }

    public bool CheckValidEmail(string sEmailAddress)
    {
        bool bValidEmail = false;
        if (sEmailAddress.Length > 3)
        {
            System.Text.RegularExpressions.Regex RegEmailAddress =
                new System.Text.RegularExpressions.Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            if (sEmailAddress.Trim().Length > 0 && RegEmailAddress.IsMatch(sEmailAddress.Trim()))
            {
                bValidEmail = true;
            }

            //    if (sEmailAddress.IndexOf("@") != -1)
            //    {
            //        if (sEmailAddress.IndexOf(".") != -1)
            //        {
            //            string[] sEmailSpilt = sEmailAddress.Split('@');
            //            if (sEmailSpilt.Length > 2) { return false; }
            //            else { return true; }
            //        }
            //        else { return false; }

            //    }
            //    else { return false; }

            //}
            //else { return false; }
        }
        return bValidEmail;
    }

    public DataTable GetFormName(string TableName)
    {
        string sQuery = "Select FormName From STFormNameReference where TableName = '" + TableName + "'";
        return PobjDataLayer.ExecuteDataSet(sQuery).Tables[0];  //returning TableNames
    }

    public decimal GetStockQuantity(int intItemID, Int64 intBatchID, int intCompanyID, int intWarehouseID)
    {
        ArrayList prmStockDetails = new ArrayList();
        prmStockDetails.Add(new SqlParameter("@Mode", 8));
        prmStockDetails.Add(new SqlParameter("@ItemID", intItemID));
        prmStockDetails.Add(new SqlParameter("@BatchID", intBatchID));
        prmStockDetails.Add(new SqlParameter("@WarehouseID", intWarehouseID));
        prmStockDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
        SqlDataReader sdr = PobjDataLayer.ExecuteReader("spInvStockMaster", prmStockDetails);
        decimal decQuantity = 0;
        if (sdr.Read())
        {
            if (sdr["AvailableQty"] != DBNull.Value)
            {
                decQuantity = Convert.ToDecimal(sdr["AvailableQty"]);
            }
        }
        sdr.Close();
        return decQuantity;
    }

    public decimal GetStockQuantity(int intItemID, Int64 intBatchID, int intCompanyID,string strInvoiceDate)
    {
        ArrayList prmStockDetails = new ArrayList();
        prmStockDetails.Add(new SqlParameter("@Mode", 16));
        prmStockDetails.Add(new SqlParameter("@ItemID", intItemID));
        prmStockDetails.Add(new SqlParameter("@BatchID", intBatchID));
        prmStockDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
        prmStockDetails.Add(new SqlParameter("@InvoiceDate", strInvoiceDate));
        SqlDataReader sdr = PobjDataLayer.ExecuteReader("spInvStockMaster", prmStockDetails);
        decimal decQuantity = 0;
        if (sdr.Read())
        {
                if (sdr["AvailableQty"] != DBNull.Value)
                {
                    decQuantity = Convert.ToDecimal(sdr["AvailableQty"]);
                }
        }
        sdr.Close();
        return decQuantity;
    }

    /// <summary>
    /// Formatting the decimal values according to the scale of currency
    /// </summary>
    /// <param name="DecimalValue"></param>
    /// <returns></returns>
    /// 
    public string FormatDecimal(string DecimalValue, int Scale)
    {
        Scale = 3;
        return Convert.ToDecimal(DecimalValue).ToString("F" + Scale);
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    ~ClsCommonUtility()
    {
        Dispose(false);
    }

    protected virtual void Dispose(bool Disposing)
    {
        if (Disposing)
        {

        }
    }

    public decimal GetSaleRate(int intItemID,int intUomID, long intBatchID, int intCompanyID)
    {
        ArrayList prmSaleRate = new ArrayList();
        prmSaleRate.Add(new SqlParameter("@Mode", 25));
        prmSaleRate.Add(new SqlParameter("@ItemID", intItemID));
        prmSaleRate.Add(new SqlParameter("@UnitID", intUomID));
        prmSaleRate.Add(new SqlParameter("@BatchID", intBatchID));
        prmSaleRate.Add(new SqlParameter("@CompanyID", intCompanyID));
        SqlDataReader sdr = PobjDataLayer.ExecuteReader("spInvPurchaseModuleFunctions", prmSaleRate);
        decimal decSaleRate = 0;
        if (sdr.Read())
        {
            if (sdr[0] != DBNull.Value)
            {
                decSaleRate = Convert.ToDecimal(sdr[0]);
            }
        }
        sdr.Close();
        return decSaleRate;
    }

    public string GetCompanyCurrency(int intCompanyID, out int intScale, out int intCurrencyID)
    {
        intScale = 2;
        intCurrencyID = 0;
        string strCurrency = "";
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 20));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        SqlDataReader sdr = PobjDataLayer.ExecuteReader("spInvPurchaseModuleFunctions", alParameters);
        if (sdr.Read())
        {
            if (sdr["Currency"] != DBNull.Value)
            {
                strCurrency = Convert.ToString(sdr["Currency"]);
            }
            if (sdr["Scale"] != DBNull.Value)
            {
                intScale = Convert.ToInt32(sdr["Scale"]);
            }
            if (sdr["CurrencyID"] != DBNull.Value)
            {
                intCurrencyID = Convert.ToInt32(sdr["CurrencyID"]);
            }
        }
        return strCurrency;
    }

    public string ConvertToWord(string strAmount, int intCurrencyID)
    {
        string strAmountInWord = "";
        ArrayList AlParameters = new ArrayList();
        AlParameters.Add(new SqlParameter("@Mode", 29));
        AlParameters.Add(new SqlParameter("@NumWord",strAmount));
        AlParameters.Add(new SqlParameter("@CurrencyID", intCurrencyID));
        SqlDataReader sdr = PobjDataLayer.ExecuteReader("spInvPurchaseModuleFunctions", AlParameters);
        if (sdr.Read())
        {
            if (sdr[0] != DBNull.Value)
            {
                strAmountInWord =Convert.ToString(sdr[0]);
            }
        }
        sdr.Close();
        return strAmountInWord;
    }

    public decimal ConvertQtyToBaseUnitQty(int intUomID, int intItemId, decimal decQuantity,int intUOMTypeID)
    {
        ArrayList prmUomDetails = new ArrayList();
        prmUomDetails.Add(new SqlParameter("@Mode", 11));
        prmUomDetails.Add(new SqlParameter("@ItemID", intItemId));
        prmUomDetails.Add(new SqlParameter("@UnitID", intUomID));
     //   prmUomDetails.Add(new SqlParameter("@UomTypeID", intUOMTypeID));
        DataTable dtUomDetails = PobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmUomDetails);
        if (dtUomDetails.Rows.Count > 0)
        {
            int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
            decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
            if (intConversionFactor == 1)
                decQuantity = decQuantity / decConversionValue;
            else if (intConversionFactor == 2)
                decQuantity = decQuantity * decConversionValue;
        }
        return decQuantity;
    }

    public decimal ConvertBaseUnitQtyToOtherQty(int intUomID, int intItemId, decimal decQuantity,int intUOMTypeID)
    {
        ArrayList prmUomDetails = new ArrayList();
        prmUomDetails.Add(new SqlParameter("@Mode", 11));
        prmUomDetails.Add(new SqlParameter("@ItemID", intItemId));
        prmUomDetails.Add(new SqlParameter("@UnitID", intUomID));
     //   prmUomDetails.Add(new SqlParameter("@UomTypeID", intUOMTypeID));
        DataTable dtUomDetails = PobjDataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", prmUomDetails);
        if (dtUomDetails.Rows.Count > 0)
        {
            int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
            decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
            if (intConversionFactor == 1)
                decQuantity = decQuantity * decConversionValue;
            else if (intConversionFactor == 2)
                decQuantity = decQuantity / decConversionValue;
        }
        return decQuantity;
    }

    public static int GetCurrencyScaleByCompany(int CompanyID)
    {
        return new clsConnection().ExecuteScalar("spCompany", new ArrayList() { new SqlParameter("@Mode", 20), new SqlParameter("@CompanyID", CompanyID) }).ToInt32();
    }

    public decimal GetPurchaseRate(int intItemID, int intUomID, long intBatchID, int intCompanyID)
    {
        ArrayList prmPurchaseRate = new ArrayList();
        prmPurchaseRate.Add(new SqlParameter("@Mode", 33));
        prmPurchaseRate.Add(new SqlParameter("@ItemID", intItemID));
        prmPurchaseRate.Add(new SqlParameter("@UnitID", intUomID));
        prmPurchaseRate.Add(new SqlParameter("@BatchID", intBatchID));
        prmPurchaseRate.Add(new SqlParameter("@CompanyID", intCompanyID));
        SqlDataReader sdr = PobjDataLayer.ExecuteReader("spInvPurchaseModuleFunctions", prmPurchaseRate);
        decimal decPurchaseRate = 0;
        if (sdr.Read())
        {
            if (sdr[0] != DBNull.Value)
            {
                decPurchaseRate = Convert.ToDecimal(sdr[0]);
            }
        }
        sdr.Close();
        return decPurchaseRate;
    }

    public decimal GetCutOffPrice(int intItemID)
    {
        ArrayList prmCutOffRate = new ArrayList();
        prmCutOffRate.Add(new SqlParameter("@Mode", 15));
        prmCutOffRate.Add(new SqlParameter("@ItemID", intItemID));
        decimal decCutOffRate = PobjDataLayer.ExecuteScalar("spInvItemMaster", prmCutOffRate).ToDecimal();
        return decCutOffRate;
    }
    public int GetCompanyID()
    {
        ArrayList prmCompany = new ArrayList();
        prmCompany.Add(new SqlParameter("@Mode", 27));
        int intCompanyID = PobjDataLayer.ExecuteScalar("spCompany", prmCompany).ToInt32();
        return intCompanyID;
    }
    public int GetBaseUomByItemID(int intItemID)
    {
        ArrayList prmBaseUom = new ArrayList();
        prmBaseUom.Add(new SqlParameter("@Mode", 14));
        prmBaseUom.Add(new SqlParameter("@ItemID", intItemID));
        int intBaseUomID = PobjDataLayer.ExecuteScalar("spInvItemMaster", prmBaseUom).ToInt32();
        return intBaseUomID;
    }
    public DataTable ConvertRateFromBaseUOMtoOtherUOM(int intBaseUOMID,int intOtherUOMID)
    {
        ArrayList prmConvertRateUom = new ArrayList();
        prmConvertRateUom.Add(new SqlParameter("@Mode", 17));
        prmConvertRateUom.Add(new SqlParameter("@BaseUomID", intBaseUOMID));
        prmConvertRateUom.Add(new SqlParameter("@OtherUOMID", intOtherUOMID));
        return PobjDataLayer.ExecuteDataTable("spInvItemMaster", prmConvertRateUom);
    }

    public void SetSerialNo(DataGridView dgv, int intRowIndex, bool blnIncludeLastRow)
    {
        try
        {
            int intRowCount = dgv.RowCount;
            if (!blnIncludeLastRow)
                intRowCount = dgv.RowCount - 1;

            for (int i = intRowIndex - 1; i < dgv.Rows.Count; i++)
            {
                if (i >= 0 && i < intRowCount)
                    dgv.Rows[i].HeaderCell.Value = (i + 1).ToString();
            }
        }
        catch (Exception ex)
        {
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error in SetSerialNo() " + ex.Message);
        }
    }
}
