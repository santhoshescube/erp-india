﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using System.Data.SqlTypes;
using System.Windows.Forms;
using System.Runtime.InteropServices;

public class ClsNotification
{
    clsConnection MobjCommon;
    public ClsNotification()
    {
        MobjCommon = new clsConnection();
    }

    //<summary>
    //Summary description for ClsNotification
    //purpose : Handle Error
    //created : Sreelekshmi
    //Date    : 27-08-2010
    //</summary>
    public ArrayList FillMessageArray(int FormID, int ProductID)
    {
        string sQuery;
        SqlDataReader DrGetError;
        ArrayList MessageArr = new ArrayList();
        sQuery = "select Code,MessageEnglish,'Display'=case when MessageType=1 then 'Error: ' when MessageType=2 then 'Warning: ' else 'Information: ' end from  NotificationMaster where  (FormID=" + FormID + " or FormID=0)";
        DrGetError = MobjCommon.ExecutesReader(sQuery);
        while (DrGetError.Read())
        {
            MessageArr.Add(new clsListItem(DrGetError["Code"], Convert.ToString(DrGetError["Display"]) + Convert.ToString(DrGetError["Code"]) + "-" + Convert.ToString(FormID) + '\n' + "#" + Convert.ToString(DrGetError["MessageEnglish"])));
        }

        DrGetError.Close();
        return MessageArr;

    }
    //<summary>
    //Summary description for ClsNotification
    //purpose : Handle Status Message
    //created : Sreelekshmi
    //Date    : 27-08-2010
    //</summary>
    public ArrayList FillStatusMessageArray(int FormID, int ProductID)
    {
        string sQuery;
        SqlDataReader DrGetError;
        ArrayList MessageArr = new ArrayList();
        sQuery = "select Code,MessageEnglish  from  NotificationMaster where (FormID=" + FormID + " or FormID=0)";
        DrGetError = MobjCommon.ExecutesReader(sQuery);
        while (DrGetError.Read())
        {
            MessageArr.Add(new clsListItem(DrGetError["Code"], DrGetError["MessageEnglish"].ToString()));
        }
        DrGetError.Close();
        return MessageArr;
    }


    public string GetErrorMessage(ArrayList MessageArr, object Code, out MessageBoxIcon MsgIcon)
    {
        string ErrorMessage = "";
        MsgIcon = MessageBoxIcon.Information;
        for (int i = 0; i < MessageArr.Count; i++)
        {
            clsListItem objList = (clsListItem)MessageArr[i];
            if ((int)objList.Value == (int)Code)
            {
                ErrorMessage = objList.Text;
                string MiType = ErrorMessage.Substring(0, 1);
                if (MiType == "E")
                    MsgIcon = MessageBoxIcon.Error;
                else if (MiType == "W")
                    MsgIcon = MessageBoxIcon.Warning;
                else
                    MsgIcon = MessageBoxIcon.Information;

                break;
            }
        }
        return ErrorMessage;
    }
    public ArrayList FillMessageArray1(int FormID, int ProductID)
    {
        string sQuery;
        SqlDataReader DrGetError;
        ArrayList MessageArr = new ArrayList();
        sQuery = "select Code,MessageEnglish,'Display'=case when MessageType=1 then 'Error: ' when MessageType=2 then 'Warning: ' else 'Information: ' end from  NotificationMaster where  (FormID=" + FormID + " or FormID=0)";
        DrGetError = MobjCommon.ExecutesReader(sQuery);
        while (DrGetError.Read())
        {
            MessageArr.Add(new clsListItem(DrGetError["Code"], Convert.ToString(DrGetError["Display"]) + Convert.ToString(DrGetError["Code"]) + "-" + Convert.ToString(FormID) + '\n' + "#" + Convert.ToString(DrGetError["MessageEnglish"])));
        }

        DrGetError.Close();
        return MessageArr;

    }
}
