﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;

public class ClsNotificationNew
{
    clsConnection MobjCommon;

    public ClsNotificationNew()
    {
        MobjCommon = new clsConnection();
    }

    //<summary>
    //Summary description for ClsNotification
    //purpose : Handle Error
    //created : Sreelekshmi
    //Date    : 27-08-2010
    //</summary>
    /// <summary>
    /// Modefied by : Devi
    /// Modified on : 11.03.2011
    /// </summary>
    /// <param name="FormID"></param>
    /// <param name="ProductID"></param>
    /// <returns></returns>
    public DataTable FillMessageArray(int FormID, int ProductID)
    {
        string strQuery;
        DataTable MessageArr = new DataTable();

        strQuery = "select Code,(case when MessageType=1 then 'Error: ' when MessageType=2 then 'Warning: ' when MessageType=3 Then 'Information: ' else 'Question: ' end+ " +
            "LTRIM(RTRIM(STR(Code))) +'-'+'" + FormID.ToString() + "'+'\n#'+ MessageEnglish) as MessageEnglish,MessageEnglish as MessageEnglishWithNoCode from NotificationMaster where (FormID=" + FormID + " or FormID=0)";
        MessageArr = MobjCommon.FillDataTable(strQuery);
        return MessageArr;
    }

    public string GetErrorMessage(DataTable datMessages, object Code, out MessageBoxIcon MsgIcon)
    {
        string ErrorMessage = "";
        DataRow[] datRow = datMessages.Select("Code = " + Code);
        MsgIcon = MessageBoxIcon.Information;

        if (datRow.Length > 0)
        {
            ErrorMessage = datRow[0]["MessageEnglish"].ToString();
            if (ErrorMessage.Substring(0, 1) == "E")
                MsgIcon = MessageBoxIcon.Error;
            else if (ErrorMessage.Substring(0, 1) == "W")
                MsgIcon = MessageBoxIcon.Warning;
            else if (ErrorMessage.Substring(0, 1) == "I")
                MsgIcon = MessageBoxIcon.Information;
            else
                MsgIcon = MessageBoxIcon.Question;
        }
        return ErrorMessage;
    }

    public string GetErrorMessage(DataTable datMessages, object Code)
    {
        string ErrorMessage = "";
        DataRow[] datRow = datMessages.Select("Code = " + Code);

        if (datRow.Length > 0)
        {
            ErrorMessage = datRow[0]["MessageEnglishWithNoCode"].ToString();
        }
        return ErrorMessage;
    }


    //<summary>
    //Summary description for ClsNotification
    //purpose : Handle Status Message
    //created : Sreelekshmi
    //Date    : 27-08-2010
    //</summary>
    public ArrayList FillStatusMessageArray(int FormID, int ProductID)
    {
        string sQuery;
        SqlDataReader DrGetError;
        ArrayList MessageArr = new ArrayList();
        sQuery = "select Code,MessageEnglish  from  NotificationMessage where (FormID=" + FormID + " or FormID=0)";
        DrGetError = MobjCommon.ExecutesReader(sQuery);
        while (DrGetError.Read())
        {
            MessageArr.Add(new clsListItem(DrGetError["Code"], DrGetError["MessageEnglish"].ToString()));
        }
        DrGetError.Close();
        return MessageArr;
    }

    public ArrayList FillMessageArray1(int FormID, int ProductID)
    {
        string sQuery;
        SqlDataReader DrGetError;
        ArrayList MessageArr = new ArrayList();
        sQuery = "select Code,MessageEnglish,'Display'=case when MessageType=1 then 'Error: ' when MessageType=2 then 'Warning: ' else 'Information: ' end from  NotificationMaster where (FormID=" + FormID + " or FormID=0)";
        DrGetError = MobjCommon.ExecutesReader(sQuery);
        while (DrGetError.Read())
        {
            MessageArr.Add(new clsListItem(DrGetError["Code"], Convert.ToString(DrGetError["Display"]) + Convert.ToString(DrGetError["Code"]) + "-" + Convert.ToString(FormID) + '\n' + "#" + Convert.ToString(DrGetError["MessageEnglish"])));
        }

        DrGetError.Close();
        return MessageArr;

    }
}
