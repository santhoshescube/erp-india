using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text;
using Log;
using System.Collections.Generic;
using System.Windows.Forms;

/// <summary>
/// Summary description for LogWriter
/// </summary>
/// <summary>
	/// This class is used for writing logs and error logs
	/// </summary>
	/*
			Class:			LogWriter
			Created by:		Bejoy
			Created on:		08-Jul-2009
	*/
public class ClsLogWriter
{
    private string LogFolderPath;//Path of the log folder.

    public ClsLogWriter(string logPath)
    {
        this.LogFolderPath = logPath +"\\Logs\\";
    }

    /// <summary>
    /// Writes the message to the application log file.
    /// </summary>
    /// <param name="message">The message to write.</param>
    /*
        Function:		WriteLog
        Created by:		Bejoy
        Created on:		08-Jul-2009
        iPriority : For all logs(0 exclude,1 low), Error logs(2 medium,3 major,4 High)
    */
    public void WriteLog(string message,short iPriority)
    {
        try
        {
            if (!Directory.Exists(LogFolderPath))
                Directory.CreateDirectory(LogFolderPath);

            if (iPriority >= 0)
            {
                using (StreamWriter Writer = new StreamWriter(LogFolderPath + "ERPlog.txt", true))
                {
                    Writer.WriteLine("Date : " + ClsCommonSettings.GetServerDateTime().ToString());
                    Writer.WriteLine("Log : " + message);
                    Writer.WriteLine("<------------------Log end------------------------>");
                    Writer.WriteLine("");
                    Writer.Flush();
                    Writer.Close();
                }
            }
        }
        catch (Exception)
        {
            return;
        }
    }
    public static void WriteLog(Exception ex, LogSeverity severity)
    {
        try
        {
            new LogToFile(Application.StartupPath + "\\ERPlog.txt").WriteLog(new LogEventArgs(severity, ex));
        }
        catch
        {
            // do nothing
        }
    }
    


   
}