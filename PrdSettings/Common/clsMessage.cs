﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;


    /*********************************************
     * Author       : Ratheesh
     * Created On   : 5 Apr 2012
     * Purpose      : Create message class
     * ******************************************/

    public class clsMessage : IDisposable
    {
        private FormID Form { get; set; }
        private List<clsMessageItem> Messages { get; set; }

        // Track whether Dispose method has already been called.
        private bool disposed = false;

        // variable to track whether messages has already been loaded.
        private bool messageLoaded = false;

        public clsMessage(FormID form)
        {
            this.Form = form;
        }

        public string GetMessageByCode(int MessageCode)
        {
            string message = string.Empty;

            // find messageItem having given message code
            clsMessageItem messageItem = this.GetMessageItemByCode(MessageCode);

            if (messageItem != null)
                message = messageItem.Message;

            return message;
        }

        public void LoadMessages()
        {
            try
            {
                this.Messages = new List<clsMessageItem>();

                using (DataTable dt = clsMessageDAL.GetMessages((short)Form, ClsCommonSettings.ProductID))
                {
                    if (dt == null)
                        return;

                    foreach (DataRow row in dt.Rows)
                    {
                        this.Messages.Add(new clsMessageItem {
                            Message = row["Message"].ToString().Trim(),
                            MessageCode = row["MessageCode"].ToInt32(),
                            MessageType = row["MessageType"].ToInt16()
                        });
                    }
                }

                this.messageLoaded = true;
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, Log.LogSeverity.Error);
            }
        }



        private clsMessageItem GetMessageItemByCode(int MessageCode)
        {
            if (!this.messageLoaded)
                this.LoadMessages();

            if (this.Messages.Count > 0)
                // find messageItem having given message code
                return this.Messages.Find(m => m.MessageCode == MessageCode);
            else
                return null;
        }

        /// <summary>
        /// Returns result of confirmation if message type is question otherwise returns false.
        /// </summary>
        /// <param name="MessageCode">Message Code</param>
        /// <returns></returns>
        public bool ShowMessage(int MessageCode)
        {
            return this.ShowMessage(MessageCode, null, null);
        }

        /// <summary>
        /// Returns result of confirmation if message type is question otherwise returns false.
        /// </summary>
        /// <param name="MessageCode">Message Code</param>
        /// <param name="control">Control to which focus has to be set</param>
        /// <returns></returns>
        public bool ShowMessage(int MessageCode, Control control)
        {
            return this.ShowMessage(MessageCode, control, null);
        }

        /// <summary>
        /// Returns result of confirmation if message type is question otherwise returns false.
        /// </summary>
        /// <param name="MessageCode">Message Code</param>
        /// <param name="control">Control to which focus and error provider has to be set</param>
        /// <param name="errorProvider">Error Provider</param>
        /// <returns></returns>
        public bool ShowMessage(int MessageCode, Control control, ErrorProvider errorProvider)
        {
            try
            {
                if (!this.messageLoaded)
                    this.LoadMessages();

                bool confirmation = false;

                // Initialize with default values
                MessageBoxIcon messageBoxIcon = MessageBoxIcon.Information;
                MessageBoxButtons messageBoxButtons = MessageBoxButtons.OK;

                clsMessageItem messageItem = this.GetMessageItemByCode(MessageCode);

                if (errorProvider != null)
                    errorProvider.Clear();

                if (messageItem != null)
                {
                    switch (messageItem.MessageType)
                    {
                        case (int)eMessageType.Error:
                            messageBoxIcon = MessageBoxIcon.Error;
                            break;
                        case (int)eMessageType.Warning:
                            messageBoxIcon = MessageBoxIcon.Warning;
                            break;
                        case (int)eMessageType.Information:
                            messageBoxIcon = MessageBoxIcon.Information;
                            break;
                        case (int)eMessageType.Question:
                            messageBoxIcon = MessageBoxIcon.Question;
                            messageBoxButtons = MessageBoxButtons.YesNo;
                            break;
                    }

                    if (messageItem.Message != string.Empty)
                    {
                        if (control != null)
                            control.Focus();

                        if (errorProvider != null)
                            errorProvider.SetError(control, messageItem.Message.Replace("#", string.Empty));

                        if (messageBoxButtons == MessageBoxButtons.YesNo)
                            confirmation = MessageBox.Show(messageItem.Message, ClsCommonSettings.MessageCaption, messageBoxButtons, messageBoxIcon) == DialogResult.Yes;
                        else
                            MessageBox.Show(messageItem.Message, ClsCommonSettings.MessageCaption, messageBoxButtons, messageBoxIcon);
                    }
                }
                
                // returns confirmation result, if message type is question, otherwise returns false;
                return confirmation;
            }
            catch(Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                
                return false;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (!this.disposed)
                {
                    this.Messages = null;
                }
            }
            this.disposed = true;
        }

        ~clsMessage()
        {
            this.Dispose(false);
        }
    }

    public class clsMessageItem
    {
        public string Message { get; set; }
        public int MessageCode { get; set; }
        public Int16 MessageType { get; set; }
    }

