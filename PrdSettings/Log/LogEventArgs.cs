﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Log
{
    /// <summary>
    /// Created By  : Ratheesh
    /// Date        : 26 Jan 2010
    /// Description : 
    /// </summary>
    /// 
    public class LogEventArgs
    {
        public LogSeverity Severity { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
        public DateTime Date { get; set; }

        public LogEventArgs(LogSeverity severity, Exception exception)
        {
            this.Severity = severity;
            this.Message = exception.Message;
            this.Exception = exception;
            this.Date = DateTime.Now;
        }

        public LogEventArgs(LogSeverity severity, string customMessage, Exception exception)
        {
            this.Severity = severity;
            this.Message = customMessage + exception.Message;
            this.Exception = exception;
            this.Date = DateTime.Now;
        }

        public LogEventArgs(string customMessage, LogSeverity severity)
        {
            this.Severity = severity;
            this.Message = customMessage;
            this.Date = DateTime.Now;
        }

        public string SeverityString { get { return this.Severity.ToString(); } }

        public override string ToString()
        {
            return Date + 
                   " - " + SeverityString + 
                   " - " + Message + 
                   " - " + Exception.ToString();
        }
    }
}
