﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Log
{
    /// <summary>
    /// Created By  : Ratheesh
    /// Date        : 26 Jan 2010
    /// Description : Writes log request to file specified.
    /// </summary>
    
    public class LogToFile : ILog
    {
        /// <summary>
        /// Gets or sets absolute path of the file to write log request to
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"> Absolute path of the file to write log request to </param>
        public LogToFile(string filePath)
        {
            this.FilePath = filePath;
        }

        /// <summary>
        /// Writes log request to specified file.
        /// </summary>
        /// <param name="arg">Parameter containing exception details </param>
        public void WriteLog(LogEventArgs arg)
        {
            FileStream objfileStream = null;
            StreamWriter objWriter = null;
            try
            {
                // Format log message.
                string Message = "********************************************************************************************************************\n" +
                                "Date           : " + arg.Date + "\n"+
                                "Error Severity : " + arg.SeverityString + "\n" +
                                "Message        : " + arg.Message + "\n" +
                                "Exception      : " + arg.Exception.ToString();

                FileInfo file = new FileInfo(FilePath);

                // create file if does not exist.
                if (!file.Exists) file.Create();

                // Open file in append mode.
                objfileStream = new FileStream(this.FilePath, FileMode.Append);

                // Create an instance of Stream writer.
                objWriter = new StreamWriter(objfileStream);

                // Write message to file. Makes sure you have folder permission for writing to file.
                objWriter.WriteLine(Message);

            }
            catch { /* Do nothing for now */}
            finally
            {
                if (objWriter != null)
                {
                    objWriter.Close();
                    objWriter.Dispose();
                }
                if (objfileStream != null)
                {
                    objfileStream.Close();
                    objfileStream.Dispose();
                }
            }
            
        }
    }
}
