﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Log
{
    public class LogToDatabase : ILog
    {

        public void WriteLog(LogEventArgs e)
        {
            System.Text.StringBuilder sSQL = new System.Text.StringBuilder();

            // Create table if does not exist.

            sSQL.Append("IF (NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Log'))");
            sSQL.Append("BEGIN ");
            sSQL.Append("CREATE TABLE Log ");
            sSQL.Append("( ");
            sSQL.Append("[Date] DATETIME, ");
            sSQL.Append("[Severity] VARCHAR(20), ");
            sSQL.Append("[Message] VARCHAR(500), ");
            sSQL.Append("[Exception] NVARCHAR(MAX) ");
            sSQL.Append(") ");
            sSQL.Append("END ");
            sSQL.Append("INSERT INTO Log ([Date], [Severity], [Message], [Exception]) ");
            sSQL.Append("VALUES (@Date, @Severity, @Message, @Exception)");

            try
            {
                string sConnectionString = MsDb.ConnectionString;

                if (!string.IsNullOrEmpty(sConnectionString))
                {
                    using (SqlConnection con = new SqlConnection(sConnectionString))
                    {
                        con.Open();

                        using (SqlCommand command = new SqlCommand(sSQL.ToString(), con))
                        {
                            command.Parameters.Add("@Date", System.Data.SqlDbType.DateTime).Value = e.Date;
                            command.Parameters.Add("@Severity", System.Data.SqlDbType.VarChar).Value = e.SeverityString;
                            command.Parameters.Add("@Message", System.Data.SqlDbType.VarChar).Value = e.Message;
                            command.Parameters.Add("@Exception", System.Data.SqlDbType.NVarChar).Value = e.Exception.ToString();

                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch
            {
                // re-throw the very same exception that catch statement has caught.
                // comment the following line at the time of deployment 
                throw;
            }
        }

    }
}
