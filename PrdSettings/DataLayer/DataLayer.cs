﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Collections;

/******************************************************************************
* Author       : Ratheesh
* Created On   : 29 Sep 2011
* Purpose      : Implement sql helper class to manage database interactions
* ****************************************************************************/

public class DataLayer : IDisposable
{
    #region Declarations

    private SqlTransaction transaction = null;

    #endregion

    // constructor
    public DataLayer() { }

    #region Data Retreival Handlers

    public DataSet ExecuteDataSet(string CommandText, ArrayList Parameters)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                    cmd.Connection = con;

                // add paramters
                foreach (SqlParameter parameter in Parameters)
                    cmd.Parameters.Add(parameter);

                using (SqlDataAdapter adpater = new SqlDataAdapter(cmd))
                {
                    using (DataSet ds = new DataSet())
                    {
                        adpater.Fill(ds);
                        return ds;
                    }
                }
            }
        }
    }

    public DataSet ExecuteDataSet(string SqlQuery)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(SqlQuery))
            {
                cmd.CommandType = CommandType.Text;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                    cmd.Connection = con;

                using (SqlDataAdapter adpater = new SqlDataAdapter(cmd))
                {
                    using (DataSet ds = new DataSet())
                    {
                        adpater.Fill(ds);
                        return ds;
                    }
                }
            }
        }
    }

    public DataTable ExecuteDataTable(string CommandText, ArrayList Parameters)
    {
        using (DataSet ds = this.ExecuteDataSet(CommandText, Parameters))
        {
            if (ds.Tables.Count > 0)
                return ds.Tables[0];
            else
                return new DataTable();
        }
    }



    public DataTable ExecuteDataTable(string CommandText)
    {
        using (DataSet ds = this.ExecuteDataSet(CommandText))
        {
            if (ds.Tables.Count > 0)
                return ds.Tables[0];
            else
                return new DataTable();
        }
    }

    public SqlDataReader ExecuteReader(string CommandText, ArrayList Parameters,CommandBehavior cmdBehavior)
    {
        SqlConnection con = new SqlConnection(MsDb.ConnectionString);
        
        using (SqlCommand cmd = new SqlCommand(CommandText))
        {
            cmd.CommandType = CommandType.StoredProcedure;

            // use transaction if any active transaction exist.
            if (this.transaction != null)
            {
                cmd.Connection = this.transaction.Connection;
                cmd.Transaction = this.transaction;
            }
            else
            {
                con.Open();
                cmd.Connection = con;
            }

            // add paramters
            if(Parameters !=null)
            foreach (SqlParameter parameter in Parameters)
                cmd.Parameters.Add(parameter);

            return cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }
    }

    public SqlDataReader ExecuteReader(string CommandText, ArrayList Parameters)
    {
        return ExecuteReader(CommandText, Parameters, CommandBehavior.Default);
    }

    public SqlDataReader ExecuteReader(string CommandText)
    {

        SqlConnection con = new SqlConnection(MsDb.ConnectionString);

        using (SqlCommand cmd = new SqlCommand(CommandText))
        {
            cmd.CommandType = CommandType.Text;

            // use transaction if any active transaction exist.
            if (this.transaction != null)
            {
                cmd.Connection = this.transaction.Connection;
                cmd.Transaction = this.transaction;
            }
            else
            {
                con.Open();
                cmd.Connection = con;
            }
            return cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }
    }

    public object ExecuteScalar(string CommandText, ArrayList Parameters)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                {
                    con.Open();
                    cmd.Connection = con;
                }

                // add parameters
                if (Parameters != null)
                    foreach (SqlParameter parameter in Parameters)
                        cmd.Parameters.Add(parameter);

                return cmd.ExecuteScalar();
            }
        }
    }

    public object ExecuteScalar(string CommandText)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.Text;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                {
                    con.Open();
                    cmd.Connection = con;
                }

                return cmd.ExecuteScalar();
            }
        }
    }

    #endregion

    #region Data Updation Handlers

    /// <summary>
    /// Executes procedure and returns number of rows affected
    /// </summary>
    /// <param name="CommandName">Name of the stored procedure</param>
    /// <returns></returns>
    public int ExecuteNonQuery(string CommandText)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                {
                    con.Open();
                    cmd.Connection = con;
                }

                return cmd.ExecuteNonQuery();
            }
        }
    }

    public int ExecuteQuery(string CommandText)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.Text;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                {
                    con.Open();
                    cmd.Connection = con;
                }

                return cmd.ExecuteNonQuery();
            }
        }
    }

    public int ExecuteNonQuery(ArrayList Parameters, string CommandText)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.Text;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                {
                    con.Open();
                    cmd.Connection = con;
                }

                // add parameters
                foreach (SqlParameter parameter in Parameters)
                {
                    cmd.Parameters.Add(parameter);
                }

                return cmd.ExecuteNonQuery();
            }
        }
    }

    /// <summary>
    /// Executes procedure and returns number of rows affected
    /// </summary>
    /// <param name="CommandText">Procedure Name</param>
    /// <param name="Parameters">List of sql parameters</param>
    /// <returns></returns>
    public int ExecuteNonQuery(string CommandText, ArrayList Parameters)
    
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                {
                    con.Open();
                    cmd.Connection = con;
                }

                // add parameters
                foreach (SqlParameter parameter in Parameters)
                {
                    cmd.Parameters.Add(parameter);
                    
                }

                return cmd.ExecuteNonQuery();
            }
        }
    }
    public DataSet ExecuteDataSet(string CommandText, ArrayList Parameters, int CommandTimeout)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = CommandTimeout;
                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                    cmd.Connection = con;

                // add paramters
                foreach (SqlParameter parameter in Parameters)
                    cmd.Parameters.Add(parameter);

                using (SqlDataAdapter adpater = new SqlDataAdapter(cmd))
                {
                    using (DataSet ds = new DataSet())
                    {
                        adpater.Fill(ds);
                        return ds;
                    }
                }
            }
        }
    }
    public DataTable ExecuteDataTable(string CommandText, ArrayList Parameters, int CommandTimeout)
    {
        using (DataSet ds = this.ExecuteDataSet(CommandText, Parameters, CommandTimeout))
        {
            if (ds.Tables.Count > 0)
                return ds.Tables[0];
            else
                return new DataTable();
        }
    }

    /// <summary>
    /// Executes procedure and returns number of rows affected
    /// </summary>
    /// <param name="CommandText">Procedure Name</param>
    /// <param name="Parameters">List of sql parameters</param>
    /// <param name="output">object output to recieve value of parameter having direction Return/Output</param>
    /// <returns></returns>
    public int ExecuteNonQuery(string CommandText, ArrayList Parameters, out object output)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                {
                    con.Open();
                    cmd.Connection = con;
                }

                // add parameters
                cmd.Parameters.Clear();
                
                foreach (SqlParameter parameter in Parameters)
                    cmd.Parameters.Add(parameter);

                output = null;

                int RowsAffected = cmd.ExecuteNonQuery();

                foreach (SqlParameter parameter in cmd.Parameters)
                {
                    if (parameter.Direction == ParameterDirection.Output || parameter.Direction == ParameterDirection.ReturnValue)
                    {
                        output = parameter.Value;
                        break;
                    }
                }

                return RowsAffected;
            }
        }
    }

    public int ExecuteNonQueryWithTran(string command)
    {
        return ExecuteNonQuery(command);
    }

    public int ExecuteNonQueryWithTran(string command, ArrayList parameters)
    {
        return ExecuteNonQuery(command, parameters);
    }

    public int ExecuteNonQueryWithTran(string command, ArrayList parameters, out object output)
    {
        return ExecuteNonQuery(command, parameters, out output);
    }


    #endregion

    #region Data Retreival Handlers Using IList

    public DataSet ExecuteDataSet(string CommandText, List<SqlParameter> Parameters)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                    cmd.Connection = con;

                // add paramters
                foreach (SqlParameter parameter in Parameters)
                    cmd.Parameters.Add(parameter);

                using (SqlDataAdapter adpater = new SqlDataAdapter(cmd))
                {
                    using (DataSet ds = new DataSet())
                    {
                        adpater.Fill(ds);
                        return ds;
                    }
                }
            }
        }
    }

    public DataTable ExecuteDataTable(string CommandText, IList<SqlParameter> Parameters)
    {
        using (DataSet ds = this.ExecuteDataSet(CommandText, Parameters))
        {
            if (ds.Tables.Count > 0)
                return ds.Tables[0];
            else
                return new DataTable();
        }
    }

    public DataSet ExecuteDataSet(string CommandText, IList<SqlParameter> Parameters)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                    cmd.Connection = con;

                // add paramters
                foreach (SqlParameter parameter in Parameters)
                    cmd.Parameters.Add(parameter);

                using (SqlDataAdapter adpater = new SqlDataAdapter(cmd))
                {
                    using (DataSet ds = new DataSet())
                    {
                        adpater.Fill(ds);
                        return ds;
                    }
                }
            }
        }
    }

    public object ExecuteScalar(string CommandText, IList<SqlParameter> Parameters)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                {
                    con.Open();
                    cmd.Connection = con;
                }

                // add parameters
                foreach (SqlParameter parameter in Parameters)
                    cmd.Parameters.Add(parameter);

                return cmd.ExecuteScalar();
            }
        }
    }

    public int ExecuteNonQuery(string CommandText, IList<SqlParameter> Parameters)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                {
                    con.Open();
                    cmd.Connection = con;
                }

                // add parameters
                foreach (SqlParameter parameter in Parameters)
                    cmd.Parameters.Add(parameter);

                return cmd.ExecuteNonQuery();
            }
        }
    }

    public DataTable ExecuteDataTable(string CommandText, List<SqlParameter> Parameters)
    {
        using (DataSet ds = this.ExecuteDataSet(CommandText, Parameters))
        {
            if (ds.Tables.Count > 0)
                return ds.Tables[0];
            else
                return new DataTable();
        }
    }

    public SqlDataReader ExecuteReader(string CommandText, List<SqlParameter> Parameters, CommandBehavior cmdBehavior)
    {
        SqlConnection con = new SqlConnection(MsDb.ConnectionString);

        using (SqlCommand cmd = new SqlCommand(CommandText))
        {
            cmd.CommandType = CommandType.StoredProcedure;

            // use transaction if any active transaction exist.
            if (this.transaction != null)
            {
                cmd.Connection = this.transaction.Connection;
                cmd.Transaction = this.transaction;
            }
            else
            {
                con.Open();
                cmd.Connection = con;
            }

            // add paramters
            if (Parameters != null)
                foreach (SqlParameter parameter in Parameters)
                    cmd.Parameters.Add(parameter);

            return cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }
    }

    public SqlDataReader ExecuteReader(string CommandText, List<SqlParameter> Parameters)
    {
        return ExecuteReader(CommandText, Parameters, CommandBehavior.Default);
    }

    public object ExecuteScalar(string CommandText, List<SqlParameter> Parameters)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                {
                    con.Open();
                    cmd.Connection = con;
                }

                // add parameters
                if (Parameters != null)
                    foreach (SqlParameter parameter in Parameters)
                        cmd.Parameters.Add(parameter);

                return cmd.ExecuteScalar();
            }
        }
    }

    #endregion

    #region Data Updation Handlers  Using IList
    /// <summary>
    /// Executes procedure and returns number of rows affected
    /// </summary>
    /// <param name="CommandText">Procedure Name</param>
    /// <param name="Parameters">List of sql parameters</param>
    /// <returns></returns>
    public int ExecuteNonQuery(string CommandText, List<SqlParameter> Parameters)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                {
                    con.Open();
                    cmd.Connection = con;
                }

                // add parameters
                foreach (SqlParameter parameter in Parameters)
                {
                    cmd.Parameters.Add(parameter);

                }

                return cmd.ExecuteNonQuery();
            }
        }
    }
    /// <summary>
    /// Executes procedure and returns number of rows affected
    /// </summary>
    /// <param name="CommandText">Procedure Name</param>
    /// <param name="Parameters">List of sql parameters</param>
    /// <param name="output">object output to recieve value of parameter having direction Return/Output</param>
    /// <returns></returns>
    public int ExecuteNonQuery(string CommandText,  List<SqlParameter> Parameters, out object output)
    {
        using (SqlConnection con = new SqlConnection(MsDb.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(CommandText))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // use transaction if any active transaction exist.
                if (this.transaction != null)
                {
                    cmd.Connection = this.transaction.Connection;
                    cmd.Transaction = this.transaction;
                }
                else
                {
                    con.Open();
                    cmd.Connection = con;
                }

                // add parameters
                cmd.Parameters.Clear();

                foreach (SqlParameter parameter in Parameters)
                    cmd.Parameters.Add(parameter);

                output = null;

                int RowsAffected = cmd.ExecuteNonQuery();

                foreach (SqlParameter parameter in cmd.Parameters)
                {
                    if (parameter.Direction == ParameterDirection.Output || parameter.Direction == ParameterDirection.ReturnValue)
                    {
                        output = parameter.Value;
                        break;
                    }
                }

                return RowsAffected;
            }
        }
    }

    public int ExecuteNonQueryWithTran(string command, List<SqlParameter> parameters)
    {
        return ExecuteNonQuery(command, parameters);
    }

    public int ExecuteNonQueryWithTran(string command, List<SqlParameter> parameters, out object output)
    {
        return ExecuteNonQuery(command, parameters, out output);
    }


    #endregion

    #region Transaction

    public void BeginTransaction()
    {
        SqlConnection con = new SqlConnection(MsDb.ConnectionString);
        con.Open();
        this.transaction = con.BeginTransaction();
    }

    public void CommitTransaction()
    {
        if (this.transaction != null)
        {
            this.transaction.Commit();

            if (this.transaction.Connection != null && this.transaction.Connection.State != ConnectionState.Closed)
                this.transaction.Connection.Close();
        }

        this.transaction = null;
    }

    public void RollbackTransaction()
    {
        if (this.transaction != null)
        {
            this.transaction.Rollback();

            if (this.transaction.Connection != null && this.transaction.Connection.State != ConnectionState.Closed)
                this.transaction.Connection.Close();
        }

        this.transaction = null;
    }

    #endregion

    #region Dispose Members

    // Variable to track whether Dispose method has already been called.
    private bool disposed = false;

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
        if (disposing)
        {
            if (!this.disposed)
            {
                if(this.transaction != null)
                    this.transaction.Dispose();
            }
        }
        this.disposed = true;
    }

    #endregion

    /// <summary>
    /// Empties the connection pool
    /// </summary>
    public void ClearConnectionPool()
    {
        SqlConnection.ClearAllPools();
    }

    public void ClearAllPools()
    {
        SqlConnection.ClearAllPools();
    }

   public void TransactionStatus()
    {
        if (this.transaction != null)
        {
            if (this.transaction.Connection.State != ConnectionState.Closed)
                this.transaction.Connection.Close();
        }

        this.transaction = null;
    }
    #region Destructor
    ~DataLayer()
    {
        this.Dispose(false);
    }
    #endregion
}
