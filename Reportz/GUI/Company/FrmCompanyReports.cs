﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmCompanyReports : Form
    {
        public FrmCompanyReports()
        {
            InitializeComponent();
        }

        private void FrmCompanyReports_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
            reportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote;
            reportViewer1.ServerReport.ReportServerUrl = new Uri("http://vbc-pc7/ReportServer");
            reportViewer1.ServerReport.ReportPath = "salary";
            reportViewer1.ShowParameterPrompts = false;
            reportViewer1.ShowPrintButton = true;

        }
    }
}
