﻿namespace MyBooksERP
{
    partial class FrmRptAssociates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptAssociates));
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.cboType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ALL = new DevComponents.Editors.ComboItem();
            this.Country = new DevComponents.Editors.ComboItem();
            this.MobileNumber = new DevComponents.Editors.ComboItem();
            this.TransactionType = new DevComponents.Editors.ComboItem();
            this.CboAssociative = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.labelCatogory = new DevComponents.DotNetBar.LabelX();
            this.txtType = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.lblAssociates = new DevComponents.DotNetBar.LabelX();
            this.ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.ErpAssociatesReport = new System.Windows.Forms.ErrorProvider(this.components);
            this.Status = new DevComponents.Editors.ComboItem();
            this.expandablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErpAssociatesReport)).BeginInit();
            this.SuspendLayout();
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.cboType);
            this.expandablePanel1.Controls.Add(this.CboAssociative);
            this.expandablePanel1.Controls.Add(this.labelCatogory);
            this.expandablePanel1.Controls.Add(this.txtType);
            this.expandablePanel1.Controls.Add(this.BtnShow);
            this.expandablePanel1.Controls.Add(this.lblAssociates);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(747, 70);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 0;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By";
            this.expandablePanel1.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandablePanel1_ExpandedChanged);
            // 
            // cboType
            // 
            this.cboType.DisplayMember = "Text";
            this.cboType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboType.FormattingEnabled = true;
            this.cboType.ItemHeight = 14;
            this.cboType.Items.AddRange(new object[] {
            this.ALL,
            this.Country,
            this.Status,
            this.MobileNumber,
            this.TransactionType});
            this.cboType.Location = new System.Drawing.Point(273, 38);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(157, 20);
            this.cboType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboType.TabIndex = 1;
            this.cboType.TabStop = false;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            // 
            // ALL
            // 
            this.ALL.Text = "ALL";
            // 
            // Country
            // 
            this.Country.Text = "Country";
            // 
            // MobileNumber
            // 
            this.MobileNumber.Text = "Mobile Number";
            // 
            // TransactionType
            // 
            this.TransactionType.Text = "Transaction Type";
            // 
            // CboAssociative
            // 
            this.CboAssociative.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboAssociative.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboAssociative.DisplayMember = "Text";
            this.CboAssociative.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboAssociative.DropDownHeight = 75;
            this.CboAssociative.FormattingEnabled = true;
            this.CboAssociative.IntegralHeight = false;
            this.CboAssociative.ItemHeight = 14;
            this.CboAssociative.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem3});
            this.CboAssociative.Location = new System.Drawing.Point(70, 38);
            this.CboAssociative.Name = "CboAssociative";
            this.CboAssociative.Size = new System.Drawing.Size(157, 20);
            this.CboAssociative.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboAssociative.TabIndex = 0;
            this.CboAssociative.TabStop = false;
            this.CboAssociative.SelectedIndexChanged += new System.EventHandler(this.CboAssociative_SelectedIndexChanged);
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "ALL";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "Supplier";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "Customer";
            // 
            // labelCatogory
            // 
            // 
            // 
            // 
            this.labelCatogory.BackgroundStyle.Class = "";
            this.labelCatogory.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelCatogory.Location = new System.Drawing.Point(242, 37);
            this.labelCatogory.Name = "labelCatogory";
            this.labelCatogory.Size = new System.Drawing.Size(75, 23);
            this.labelCatogory.TabIndex = 5;
            this.labelCatogory.Text = "Type";
            // 
            // txtType
            // 
            this.txtType.AutoCompleteCustomSource.AddRange(new string[] {
            "Country",
            "Location",
            "MobileNumber",
            "TransactionType"});
            this.txtType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtType.Border.Class = "TextBoxBorder";
            this.txtType.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtType.Location = new System.Drawing.Point(436, 38);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(137, 20);
            this.txtType.TabIndex = 2;
            this.txtType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtType_KeyPress);
            this.txtType.TextChanged += new System.EventHandler(this.txtType_TextChanged);
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(585, 38);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(75, 20);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 3;
            this.BtnShow.Text = "Show";
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click_1);
            // 
            // lblAssociates
            // 
            // 
            // 
            // 
            this.lblAssociates.BackgroundStyle.Class = "";
            this.lblAssociates.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAssociates.Location = new System.Drawing.Point(11, 35);
            this.lblAssociates.Name = "lblAssociates";
            this.lblAssociates.Size = new System.Drawing.Size(75, 23);
            this.lblAssociates.TabIndex = 0;
            this.lblAssociates.Text = "Associates";
            // 
            // ReportViewer1
            // 
            this.ReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DtSetCompanyFormReport_VendorInformationMISReport";
            reportDataSource1.Value = null;
            reportDataSource2.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource2.Value = null;
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.ReportViewer1.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptMISAssociates.rdlc";
            this.ReportViewer1.Location = new System.Drawing.Point(0, 70);
            this.ReportViewer1.Name = "ReportViewer1";
            this.ReportViewer1.Size = new System.Drawing.Size(747, 411);
            this.ReportViewer1.TabIndex = 1;
            this.ReportViewer1.TabStop = false;
            // 
            // Timer
            // 
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // ErpAssociatesReport
            // 
            this.ErpAssociatesReport.ContainerControl = this;
            this.ErpAssociatesReport.RightToLeft = true;
            // 
            // Status
            // 
            this.Status.Text = "Status";
            // 
            // FrmRptAssociates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 481);
            this.Controls.Add(this.ReportViewer1);
            this.Controls.Add(this.expandablePanel1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptAssociates";
            this.Text = "Associates";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmRptAssociates_Load);
            this.expandablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErpAssociatesReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DevComponents.DotNetBar.LabelX lblAssociates;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboAssociative;
        private Microsoft.Reporting.WinForms.ReportViewer ReportViewer1;
        private System.Windows.Forms.Timer Timer;
        //private DtSetCompanyFormReport dtSetCompanyFormReport;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboType;
        private DevComponents.Editors.ComboItem MobileNumber;
        private DevComponents.Editors.ComboItem Country;
        private DevComponents.Editors.ComboItem TransactionType;
        private DevComponents.DotNetBar.LabelX labelCatogory;
        private DevComponents.Editors.ComboItem ALL;
        public DevComponents.DotNetBar.Controls.TextBoxX txtType;
        private System.Windows.Forms.ErrorProvider ErpAssociatesReport;
        private DevComponents.Editors.ComboItem Status;
    }
}