﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Collections;
using System.Data.SqlClient;

using System.IO;
using System.Drawing.Imaging;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,27 April 2011>
Description:	<Description,, Employee Report Form>
================================================
*/

namespace MyBooksERP
{
    public partial class FrmRptEmployee : DevComponents.DotNetBar.Office2007Form 
    {
        #region  Variable Declaration

        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;

        private string MsReportPath;
        string PsReportFooter;
        private string strCondition = "";
        private string MsMessageCaption = "";
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        private bool MbViewPermission = true;
        private DataTable DTCompany;
        private bool MblnPrintEmailPermission = false;//To Set View Permission
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission

        clsBLLEmployee MobjclsBLLEmployee;
        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;
        #endregion //Variables Declaration

        #region Constructor
        public FrmRptEmployee()
        {
            InitializeComponent();
            PsReportFooter = ClsCommonSettings.ReportFooter;
            MobjclsBLLEmployee = new clsBLLEmployee();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification();
            BtnShow.Click += new EventHandler(BtnShow_Click);
        }
        #endregion  //Constructor

        private void FrmRptEmployee_Load(object sender, EventArgs e)
        {
            
            LoadMessage();
            LoadCombos(0);
          //  SetPermissions();
            BtnShow.Enabled = true;
        }
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.Employee, 4);
            CboCompany.SelectedValue=0;
        //  MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.Employee, 4);
        }
        private void SetPermissions()
        {
            clsBLLPermissionSettingsReports objClsBLLPermissionSettings = new clsBLLPermissionSettingsReports();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Reports, (Int32)eMenuID.EmployeeReports, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            MbViewPermission = (MblnAddPermission || MblnPrintEmailPermission || MblnUpdatePermission || MblnDeletePermission);
            expandablePanel1.Enabled = MbViewPermission;
        }

        private bool LoadCombos(int intType)
        {
            // 0 - For Loading All Combo
            // 1 - For Loading CboCompany
            // 2 - For Loading CboDesignation
            // 3 - For Loading CboDepartment
            // 4 - For Loading CboWorkStatus
            try
            {               
                DataTable datCombos = new DataTable();
                DataTable dtTemp = new DataTable();
                DataRow datrow;
                string strFilterCondition = "";
                if (intType == 0 || intType == 1)
                {
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //{
                        datCombos = MobjclsBLLEmployee.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID ="+ClsCommonSettings.LoginCompanyID });
                        //datrow = datCombos.NewRow();
                        //datrow["CompanyID"] = 0;
                        //datrow["CompanyName"] = "ALL";
                        //datCombos.Rows.InsertAt(datrow, 0);
                        CboCompany.ValueMember = "CompanyID";
                        CboCompany.DisplayMember = "CompanyName";
                        CboCompany.DataSource = datCombos;
                    //}
                    //else
                    //{
                    //    datCombos = MobjclsBLLEmployee.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RptEmployee);
                    //    if (datCombos.Rows.Count <= 0)
                    //        CboCompany.DataSource = null;
                    //    else
                    //    {
                    //        datrow = datCombos.NewRow();
                    //        datrow["CompanyID"] = 0;
                    //        datrow["Name"] = "ALL";
                    //        datCombos.Rows.InsertAt(datrow, 0);
                    //        CboCompany.ValueMember = "CompanyID";
                    //        CboCompany.DisplayMember = "Name";
                    //        CboCompany.DataSource = datCombos;
                    //    }
                    //}
               }
                if (intType == 0 || intType == 2)
                {
                    CboDesignation.DataSource = null;
                    if (Convert.ToInt32(CboCompany.SelectedValue) != 0)
                    {
                        strCondition = "EM.CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue);
                        datCombos = MobjclsBLLEmployee.FillCombos(new string[] { " distinct D.DesignationID,D.Designation", "DesignationReference D inner join EmployeeMaster EM on EM.DesignationID= D.DesignationID left join CompanyMaster CM on CM.CompanyID = EM. CompanyID ", strCondition });
                    }
                    else
                    {
                        dtTemp = null;
                        dtTemp = (DataTable)CboCompany.DataSource;

                        if (dtTemp.Rows.Count > 0)
                        {
                            strFilterCondition = "";
                            strFilterCondition += " (";

                            foreach (DataRow dr in dtTemp.Rows)
                                strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";

                            strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                            strFilterCondition += ")";
                        }
                        strCondition = "EM.CompanyID  in " + strFilterCondition;
                        datCombos = MobjclsBLLEmployee.FillCombos(new string[] { " distinct D.DesignationID,D.Designation", "DesignationReference D inner join EmployeeMaster EM on EM.DesignationID= D.DesignationID left join CompanyMaster CM on CM.CompanyID = EM. CompanyID ", strCondition });
                    }
                    datrow = datCombos.NewRow();
                    datrow["DesignationID"] = 0;
                    datrow["Designation"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);

                    CboDesignation.DataSource = null;
                    CboDesignation.ValueMember = "DesignationID";
                    CboDesignation.DisplayMember = "Designation";
                    CboDesignation.DataSource = datCombos;
                }
                if (intType == 0 || intType == 3)
                {
                    CboDepartment.DataSource = null;
                    if (Convert.ToInt32(CboDesignation.SelectedValue) != 0)
                    {
                       
                    strCondition = ((Convert.ToInt32(CboDesignation.SelectedValue)!=0) ? "EM.DesignationID = " + Convert.ToInt32(CboDesignation.SelectedValue) : "" )+ ((Convert.ToInt32(CboCompany.SelectedValue) != 0) ? " And  CM.CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) : "");
                    datCombos = MobjclsBLLEmployee.FillCombos(new string[] { " distinct D.DepartmentID,D.Department", "DepartmentReference D inner join EmployeeMaster EM on EM.DepartmentID= D.DepartmentID left join CompanyMaster CM on CM.CompanyID = EM. CompanyID ", (strCondition != "") ? strCondition : "" });
                    }
                    else
                    {
                        dtTemp = null;
                        dtTemp = (DataTable)CboDesignation.DataSource;

                        if (dtTemp.Rows.Count > 0)
                        {
                            strFilterCondition = "";
                            strFilterCondition += " (";

                            foreach (DataRow dr in dtTemp.Rows)
                                strFilterCondition += Convert.ToInt32(dr["DesignationID"]) + ",";

                            strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                            strFilterCondition += ")";
                        }
                        strCondition = "EM.DesignationID in " + strFilterCondition;
                        datCombos = MobjclsBLLEmployee.FillCombos(new string[] { " distinct D.DepartmentID,D.Department", "DepartmentReference D inner join EmployeeMaster EM on EM.DepartmentID= D.DepartmentID left join CompanyMaster CM on CM.CompanyID = EM. CompanyID ", strCondition });
                    }
                    datrow = datCombos.NewRow();
                    datrow["DepartmentID"] = 0;
                    datrow["Department"] = "ALL";

                    datCombos.Rows.InsertAt(datrow, 0);
                    CboDepartment.DataSource = null;
                    CboDepartment.ValueMember = "DepartmentID";
                    CboDepartment.DisplayMember = "Department";
                    CboDepartment.DataSource = datCombos;             
                }
                if (intType == 0 || intType == 4)
                {
                    cboWorkStatus.DataSource = null;
                    if (Convert.ToInt32(CboDepartment.SelectedValue) != 0)
                    {
                        strCondition = ((Convert.ToInt32(CboDepartment.SelectedValue) != 0) ? "EM.DepartmentID = " + Convert.ToInt32(CboDepartment.SelectedValue) : "") + ((Convert.ToInt32(CboCompany.SelectedValue) != 0) ? " And  CM.CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) : "");
                        datCombos = MobjclsBLLEmployee.FillCombos(new string[] { " Distinct WSR.WorkStatusID,WSR.WorkStatus", "WorkStatusReference WSR inner join EmployeeMaster EM on EM.WorkStatusID= WSR.WorkStatusID left join DepartmentReference D on EM.DepartmentID=D.DepartmentID left join CompanyMaster CM on CM.CompanyID = EM. CompanyID ", (strCondition != "") ? strCondition : "" });
                    }
                    else
                    {
                        dtTemp = null;
                        dtTemp = (DataTable)CboDepartment.DataSource;

                        if (dtTemp.Rows.Count > 0)
                        {
                            strFilterCondition = "";
                            strFilterCondition += " (";

                            foreach (DataRow dr in dtTemp.Rows)
                                strFilterCondition += Convert.ToInt32(dr["DepartmentID"]) + ",";

                            strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                            strFilterCondition += ")";
                        }
                        strCondition = "EM.DepartmentID in " + strFilterCondition;
                        datCombos = MobjclsBLLEmployee.FillCombos(new string[] { " distinct WSR.WorkStatusID,WSR.WorkStatus", "WorkStatusReference WSR inner join EmployeeMaster EM on EM.WorkStatusID=WSR.WorkStatusID left join CompanyMaster CM on CM.CompanyID = EM. CompanyID ", strCondition });                    
                    }
                    datrow = datCombos.NewRow();
                    datrow["WorkStatusID"] = 0;
                    datrow["WorkStatus"] = "ALL";

                    datCombos.Rows.InsertAt(datrow, 0);
                    cboWorkStatus.DataSource = null;
                    cboWorkStatus.ValueMember = "WorkStatusID";
                    cboWorkStatus.DisplayMember = "WorkStatus";
                    cboWorkStatus.DataSource = datCombos;
                }
                MObjClsLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
                return false;
            }
        }
        private void ShowReport()
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();

            MsReportPath = Application.StartupPath + "\\MainReports\\RptMISEmployee.rdlc";
            ReportViewer1.LocalReport.ReportPath = MsReportPath;
            ReportParameter[] ReportParameter = new ReportParameter[5];
            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
            ReportParameter[1] = new ReportParameter("Company", Convert.ToString(CboCompany.Text.Trim()), false);
            ReportParameter[2] = new ReportParameter("Department", Convert.ToString(CboDepartment.Text.Trim()), false);
            ReportParameter[3] = new ReportParameter("Designation", Convert.ToString(CboDesignation.Text.Trim()), false);
            ReportParameter[4] = new ReportParameter("WorkStatus", Convert.ToString(cboWorkStatus.Text.Trim()), false);
            ReportViewer1.LocalReport.SetParameters(ReportParameter);
            ReportViewer1.LocalReport.DataSources.Clear();
            int Company = Convert.ToInt32(CboCompany.SelectedValue);
            DTCompany = new DataTable();
            if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
            {
                DTCompany = MobjclsBLLEmployee.DisplayALLCompanyHeaderReport();
            }
            else
            {
                DTCompany = MobjclsBLLEmployee.DisplayCompanyHeaderReport(Company);
            }
            ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
            int Department = Convert.ToInt32(CboDepartment.SelectedValue);
            int Designation = Convert.ToInt32(CboDesignation.SelectedValue);
            int WorkStatus = Convert.ToInt32(cboWorkStatus.SelectedValue);

            clsBLLPermissionSettingsReports objClsBLLPermissionSettings = new clsBLLPermissionSettingsReports();
            string strPermittedCompany;
            DataTable DTPermComp = new DataTable();
            //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            //{
                strPermittedCompany = null;
            //}
            //else
            //{
            //    DTPermComp = objClsBLLPermissionSettings.GetPermittedCompany(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RptEmployee);
            //    if (DTPermComp.Rows.Count <= 0)
            //        strPermittedCompany = null;
            //    else
            //        strPermittedCompany = DTPermComp.Rows[0]["IsEnabled"].ToString();
            //}
            DataTable DT = MobjclsBLLEmployee.DisplayALLEmployeeReport(Company, Designation, Department, WorkStatus, strPermittedCompany);
            if (DT.Rows.Count <= 0)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 428, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
            }
            else
            {
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_EmployeeMaster", DT));
                ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                ReportViewer1.ZoomMode = ZoomMode.Percent;
                this.Cursor = Cursors.Default;
            }           
        }
        public void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
        }
        private void BtnShow_Click(object sender, EventArgs e)
        {
            try
            {
                ReportViewer1.Clear();
                ReportViewer1.Reset();
                if (EmployeeReportValidation())
                {
                    BtnShow.Click -= new EventHandler(BtnShow_Click);
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //{
                        BtnShow.Enabled = false;
                        ShowReport();
                    //}
                    //else
                    //{
                    //    if (RptEmployeeValidation())
                    //    {
                    //        BtnShow.Enabled = false;
                    //        ShowReport();
                    //    }
                    //}
                    Timer.Enabled = true;
                }
            }
            catch
            {
                Timer.Enabled = true;
            }
        }
        private bool EmployeeReportValidation()
        {
            if (CboCompany.Enabled == true)
            {
                if (CboCompany.DataSource == null || CboCompany.SelectedValue == null || CboCompany.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9100, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpEmployeeReport.SetError(CboCompany, MsMessageCommon.Replace("#", "").Trim());
                    CboCompany.Focus();
                    return false;
                }
            }
            if (CboDesignation.Enabled == true)
            {
                if (CboDesignation.DataSource == null || CboDesignation.SelectedValue == null || CboDesignation.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9112, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpEmployeeReport.SetError(CboDesignation, MsMessageCommon.Replace("#", "").Trim());
                    CboDesignation.Focus();
                    return false;
                }
            }
            if (CboDepartment.Enabled == true)
            {
                if (CboDepartment.DataSource == null || CboDepartment.SelectedValue == null || CboDepartment.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9109, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpEmployeeReport.SetError(CboDepartment, MsMessageCommon.Replace("#", "").Trim());
                    CboDepartment.Focus();
                    return false;
                }
            }
            if (cboWorkStatus.Enabled == true)
            {
                if (cboWorkStatus.DataSource == null || cboWorkStatus.SelectedValue == null || cboWorkStatus.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9113, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpEmployeeReport.SetError(cboWorkStatus, MsMessageCommon.Replace("#", "").Trim());
                    cboWorkStatus.Focus();
                    return false;
                }
            }
            return true;
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            ErpEmployeeReport.Clear();
            Timer.Enabled = false;
            BtnShow.Enabled = true;
            BtnShow.Click += new EventHandler(BtnShow_Click);
        }
        private void CboCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ErpEmployeeReport.Clear();

            LoadCombos(2);
            LoadCombos(3);
            LoadCombos(4);
        }
        private void CboDesignation_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpEmployeeReport.Clear();

            LoadCombos(3);
        }
        private void CboDepartment_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpEmployeeReport.Clear();

            LoadCombos(4);
        }

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;

            if (cbo.FindForm() != null)
                cbo.DroppedDown = false;
        }

        private void cboWorkStatus_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpEmployeeReport.Clear();
        }
        private bool RptEmployeeValidation()
        {
            if (CboCompany.DataSource==null)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9014, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                CboCompany.Focus();
                return false;
            }
            return true;
        }
    }
}
