﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;


namespace MyBooksERP
{
    /*
        CREATED BY RAJESH
        CREATED ON 20-AUG-2013
        DESCRIPTION EMPLOYEE PROFILE REPORTS
     
     */
    public partial class FrmRptEmployeeProfile : Report
    {
        #region Properties
        private string strMReportPath = "";
        private clsMessage ObjUserMessage = null;
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.EmployeeProfileMISReport);
                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Constuctor
        public FrmRptEmployeeProfile()
        {
            InitializeComponent();
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }
        #endregion

        #region FormEvents
        private void FrmRptEmployeeProfile_Load(object sender, EventArgs e)
        {
            chkIncludeCompany.Checked = true;
            LoadCombos();
            SetDefaultBranch(cboBranch);
            SetDefaultWorkStatus(cboWorkStatus);
            DtpDateOfJoiningFrom.Checked = false;
            if (DtpDateOfJoiningFrom.Checked == true)
            {
                DtpDateOfJoiningTo.Enabled = true;
            }
            else
            {
                DtpDateOfJoiningTo.Enabled = false;
            }
            


        }
        #endregion


        //#region SetArabicControls
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.EmployeeProfileMISReport , this);
        //}
        //#endregion SetArabicControls

        #region Control Events

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Enable or disable include company check box based on the branch value
            EnableDisableIncludeCompany();
            //FillAllWorkLocation();
            FillAllEmployees();

        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.rvEmployeeProfile.Reset();
                if (ValidateForm())
                {
                    ShowReport();
                }
            }
            catch (Exception Ex)
            {
                btnShow.Enabled = true;
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);

            }
            this.Cursor = Cursors.Default;

        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllBranches();
        }


        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            //FillAllWorkLocation();
            FillAllEmployees();
        }


        public void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = false;
        }


        #endregion

        #region Control KeyDown Event
        private void cbo_Keydown(object sender, KeyEventArgs e)
        {
            ComboBox cbo = sender as ComboBox;
            if(sender != null)
                cbo.DroppedDown = false;
        }
        #endregion

        #region Function

        /// <summary>
        /// Load employee function
        /// </summary>
        private void LoadEmployee(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        /// <summary>
        /// Form validat function
        /// </summary>
        public bool ValidateForm()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9708);
                return false;
            }

            if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9709);
                return false;
            }
            if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9710);
                return false;
            }
            if (cboDesignation.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9711);
                return false;
            }
            if (cboEmployeementType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9712);
                return false;
            }
            if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9113);
                return false;
            }
            //if (cboWorkLocation.SelectedIndex == -1)
            //{
            //    UserMessage.ShowMessage(9714);
            //    return false;
            //}
            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9713);
                return false;
            }
            if (DtpDateOfJoiningFrom.Checked == true)
            {
                if (DtpDateOfJoiningFrom.Value > DtpDateOfJoiningTo.Value)
                {
                    UserMessage.ShowMessage(19);
                    return false;
                }
            }

            return true;
        }



        /// <summary>
        /// Enables or disables the include company check box based on the branch value
        /// </summary>
        //private void EnableDisableIncludeCompany()
        //{
        //    //No branch selected
        //    if (cboBranch.SelectedValue.ToInt32() == 0)
        //    {
        //        chkIncludeCompany.Checked = true;
        //        chkIncludeCompany.Enabled = false;
        //    }
        //    else
        //    {
        //        chkIncludeCompany.Enabled = true;
        //    }

        //}
        //private void EnableDisableIncludeCompany()
        //{

        //    //Checking if the user is having access to any branch
        //    if (cboBranch.Items.Count == 2)
        //    {
        //        cboBranch.SelectedValue = 0;
        //        cboBranch.Enabled = false;
        //    }
        //    else
        //    {
        //        cboBranch.Enabled = true;
        //    }

        //    //Checking if the users is having access to the selected company
        //    if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
        //    {
        //        chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        //    }

        //    else
        //    {
        //        //No branch selected
        //        if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
        //        {
        //            chkIncludeCompany.Checked = true;
        //            chkIncludeCompany.Enabled = false;
        //        }
        //        else
        //        {
        //            chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
        //        }
        //    }

        //}
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }
        /// <summary>
        /// Fill all companies
        /// </summary>
        private void FillAllCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        /// <summary>
        /// Fill all companies based on the company id
        /// </summary>
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());

        }

        /// <summary>
        /// Fill all departments
        /// </summary>
        private void FillAllDeparments()
        {
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
        }

        /// <summary>
        /// Fill all Religion
        /// </summary>
        private void FillReligion()
        {
            CboReligion.DataSource = clsBLLReportCommon.GetReligion();
            CboReligion.DisplayMember = "Religion";
            CboReligion.ValueMember = "ReligionID";
        }


        /// <summary>
        /// Fill all Country
        /// </summary>
        private void FillNationality()
        {
            CboNationality.DataSource = clsBLLReportCommon.GetNationality();
            CboNationality.DisplayMember = "Nationality";
            CboNationality.ValueMember = "NationalityID";
        }



        /// <summary>
        /// Fill all designation
        /// </summary>
        private void FillAllDesignation()
        {
            cboDesignation.DataSource = clsBLLReportCommon.GetAllDesignation();
            cboDesignation.DisplayMember = "Designation";
            cboDesignation.ValueMember = "DesignationID";
        }

        /// <summary>
        /// Fill all employment types
        /// </summary>
        private void FillAllEmploymentType()
        {
            cboEmployeementType.DataSource = clsBLLReportCommon.GetAllEmploymentType();
            cboEmployeementType.DisplayMember = "EmploymentType";
            cboEmployeementType.ValueMember = "EmploymentTypeID";
        }

        /// <summary>
        /// Fill all work status
        /// </summary>
        private void FillAllWorkStatus()
        {
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.SelectedValue = (int)EmployeeWorkStatus.InService;
        }

        /// <summary>
        /// Fill all work locations based on the company
        /// </summary>
        //private void FillAllWorkLocation()
        //{
        //    cboWorkLocation.DataSource = clsBLLReportCommon.GetAllWorkLocations(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
        //                                    chkIncludeCompany.Checked);
        //    cboWorkLocation.DisplayMember = "WorkLocation";
        //    cboWorkLocation.ValueMember = "WorkLocationID";
        //}

        /// <summary>
        /// Fill all employees based on the company ,branch,departmnt,desigantion,employment type,work status and worklocation
        /// </summary>
        private void FillAllEmployees()
        {
            cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                            chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(),
                                            cboEmployeementType.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(),-1);
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";
        }

        /// <summary>
        /// Loads all the combos 
        /// </summary>
        private void LoadCombos()
        {
            FillAllCompany();
            FillAllDeparments();
            FillAllDesignation();
            FillAllEmploymentType();
            FillAllWorkStatus();
            //FillAllWorkLocation();
            FillReligion();
            FillNationality();
        }

        /// <summary>
        /// Show reports based on the filteration
        /// </summary>
        public void ShowReport()
        {
            DataTable dtEmployee;
            string sDateOfJoining = "01-Jan-1900";
            string sDateOfJoiningTo = "01-Jan-1900";
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    strMReportPath = cboEmployee.SelectedValue.ToInt32() == -1 ? Application.StartupPath + "\\MainReports\\RptEmployeeProfileArb.rdlc" :
            //          Application.StartupPath + "\\MainReports\\RptEmployeeInformationArb.rdlc";

            //}
            //else
            //{
                strMReportPath = cboEmployee.SelectedValue.ToInt32() == -1 ? Application.StartupPath + "\\MainReports\\RptEmployeeProfile.rdlc" :
                      Application.StartupPath + "\\MainReports\\RptEmployeeInformation.rdlc";

            //}
 
            //Setting the report header
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            //setting the report path
            this.rvEmployeeProfile.LocalReport.ReportPath = strMReportPath;

            //All employees report
            if (cboEmployee.SelectedValue.ToInt32() == -1)
            {
                rvEmployeeProfile.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false),
                    new ReportParameter("Company", cboCompany.Text, false),
                    new ReportParameter("Branch", cboBranch.Text, false),
                    new ReportParameter("Department", cboDepartment.Text, false),
                    new ReportParameter("Designation", cboDesignation.Text, false),
                    new ReportParameter("EmployeementType", cboEmployeementType.Text, false),
                    new ReportParameter("Employee",cboEmployee.Text, false),
                    new ReportParameter("Location", "", false),
                    new ReportParameter("WorkStatus", cboWorkStatus.Text, false)
                
                });


                if (DtpDateOfJoiningFrom.Checked == true)
                {
                  sDateOfJoining= DtpDateOfJoiningFrom.Value.ToString("dd-MMM-yyyy");
                  sDateOfJoiningTo = DtpDateOfJoiningTo.Value.ToString("dd-MMM-yyyy");  
                }

                this.rvEmployeeProfile.LocalReport.DataSources.Clear();
                dtEmployee = clsBLLRptEmployeeProfile.GetEmployeeInformation(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(),
                            cboDesignation.SelectedValue.ToInt32(), cboEmployeementType.SelectedValue.ToInt32(),
                            cboEmployee.SelectedValue.ToInt32(), chkIncludeCompany.Checked, -1, cboWorkStatus.SelectedValue.ToInt32(), CboNationality.SelectedValue.ToInt32(), CboReligion.SelectedValue.ToInt32(), sDateOfJoining, sDateOfJoiningTo);

                if (dtEmployee.Rows.Count > 0)
                {
                    rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_EmployeeProfileSummary", dtEmployee));
                }

            }
            //Single employee report
            else
            {
                rvEmployeeProfile.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false)
                });


                this.rvEmployeeProfile.LocalReport.DataSources.Clear();

                dtEmployee = clsBLLRptEmployeeProfile.DisplaySingleEmployeeReport(cboEmployee.SelectedValue.ToInt32());

                if (dtEmployee.Rows.Count > 0)
                {
                    rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_EmployeeMaster", dtEmployee));
                }
            }
            //No records found message
            if (dtEmployee.Rows.Count == 0)
            {
                UserMessage.ShowMessage(9707);
                return;

            }

            this.rvEmployeeProfile.SetDisplayMode(DisplayMode.PrintLayout);
            this.rvEmployeeProfile.ZoomMode = ZoomMode.Percent;
            this.rvEmployeeProfile.ZoomPercent = 100;


        }

        #endregion

        #region Reportviewer Events
        private void rvEmployeeProfile_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;
        }

        private void rvEmployeeProfile_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;
        }
        #endregion

        private void DtpDateOfJoiningFrom_ValueChanged(object sender, EventArgs e)
        {

        }

        private void DtpDateOfJoiningFrom_MouseDown(object sender, MouseEventArgs e)
        {
            if (DtpDateOfJoiningFrom.Checked == true)
            {
                DtpDateOfJoiningTo.Enabled = true; 
            }
            else
            {
                DtpDateOfJoiningTo.Enabled = false; 
            }
        }

    }
}