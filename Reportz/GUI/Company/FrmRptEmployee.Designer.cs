﻿namespace MyBooksERP
{
    partial class FrmRptEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptEmployee));
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.lblDesignation = new DevComponents.DotNetBar.LabelX();
            this.lblDepartment = new DevComponents.DotNetBar.LabelX();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.CboDepartment = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboDesignation = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.CboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.cboWorkStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblWorkStatus = new DevComponents.DotNetBar.LabelX();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.ErpEmployeeReport = new System.Windows.Forms.ErrorProvider(this.components);
            this.expandablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErpEmployeeReport)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(599, 40);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(75, 20);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 4;
            this.BtnShow.Text = "Show";
            // 
            // lblDesignation
            // 
            // 
            // 
            // 
            this.lblDesignation.BackgroundStyle.Class = "";
            this.lblDesignation.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDesignation.Location = new System.Drawing.Point(12, 62);
            this.lblDesignation.Name = "lblDesignation";
            this.lblDesignation.Size = new System.Drawing.Size(75, 23);
            this.lblDesignation.TabIndex = 1;
            this.lblDesignation.Text = "Designation";
            // 
            // lblDepartment
            // 
            // 
            // 
            // 
            this.lblDepartment.BackgroundStyle.Class = "";
            this.lblDepartment.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDepartment.Location = new System.Drawing.Point(311, 38);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(64, 23);
            this.lblDepartment.TabIndex = 2;
            this.lblDepartment.Text = "Department";
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(12, 37);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(75, 23);
            this.lblCompany.TabIndex = 0;
            this.lblCompany.Text = "Company";
            // 
            // CboDepartment
            // 
            this.CboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDepartment.DisplayMember = "Text";
            this.CboDepartment.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboDepartment.DropDownHeight = 75;
            this.CboDepartment.FormattingEnabled = true;
            this.CboDepartment.IntegralHeight = false;
            this.CboDepartment.ItemHeight = 14;
            this.CboDepartment.Location = new System.Drawing.Point(381, 40);
            this.CboDepartment.Name = "CboDepartment";
            this.CboDepartment.Size = new System.Drawing.Size(200, 20);
            this.CboDepartment.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboDepartment.TabIndex = 2;
            this.CboDepartment.SelectionChangeCommitted += new System.EventHandler(this.CboDepartment_SelectionChangeCommitted);
            this.CboDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // CboDesignation
            // 
            this.CboDesignation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboDesignation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDesignation.DisplayMember = "Text";
            this.CboDesignation.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboDesignation.DropDownHeight = 75;
            this.CboDesignation.FormattingEnabled = true;
            this.CboDesignation.IntegralHeight = false;
            this.CboDesignation.ItemHeight = 14;
            this.CboDesignation.Location = new System.Drawing.Point(93, 65);
            this.CboDesignation.Name = "CboDesignation";
            this.CboDesignation.Size = new System.Drawing.Size(200, 20);
            this.CboDesignation.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboDesignation.TabIndex = 1;
            this.CboDesignation.SelectionChangeCommitted += new System.EventHandler(this.CboDesignation_SelectionChangeCommitted);
            this.CboDesignation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // ReportViewer1
            // 
            this.ReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource1.Value = null;
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.ReportViewer1.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptCompanyHeader.rdlc";
            this.ReportViewer1.Location = new System.Drawing.Point(0, 94);
            this.ReportViewer1.Name = "ReportViewer1";
            this.ReportViewer1.Size = new System.Drawing.Size(848, 385);
            this.ReportViewer1.TabIndex = 3;
            // 
            // CboCompany
            // 
            this.CboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompany.DisplayMember = "Text";
            this.CboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCompany.DropDownHeight = 75;
            this.CboCompany.FormattingEnabled = true;
            this.CboCompany.IntegralHeight = false;
            this.CboCompany.ItemHeight = 14;
            this.CboCompany.Location = new System.Drawing.Point(93, 40);
            this.CboCompany.Name = "CboCompany";
            this.CboCompany.Size = new System.Drawing.Size(200, 20);
            this.CboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCompany.TabIndex = 0;
            this.CboCompany.SelectionChangeCommitted += new System.EventHandler(this.CboCompany_SelectionChangeCommitted);
            this.CboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.cboWorkStatus);
            this.expandablePanel1.Controls.Add(this.lblWorkStatus);
            this.expandablePanel1.Controls.Add(this.BtnShow);
            this.expandablePanel1.Controls.Add(this.lblDesignation);
            this.expandablePanel1.Controls.Add(this.lblDepartment);
            this.expandablePanel1.Controls.Add(this.lblCompany);
            this.expandablePanel1.Controls.Add(this.CboDesignation);
            this.expandablePanel1.Controls.Add(this.CboDepartment);
            this.expandablePanel1.Controls.Add(this.CboCompany);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(848, 94);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 2;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By";
            // 
            // cboWorkStatus
            // 
            this.cboWorkStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkStatus.DisplayMember = "Text";
            this.cboWorkStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWorkStatus.DropDownHeight = 75;
            this.cboWorkStatus.FormattingEnabled = true;
            this.cboWorkStatus.IntegralHeight = false;
            this.cboWorkStatus.ItemHeight = 14;
            this.cboWorkStatus.Location = new System.Drawing.Point(381, 65);
            this.cboWorkStatus.Name = "cboWorkStatus";
            this.cboWorkStatus.Size = new System.Drawing.Size(200, 20);
            this.cboWorkStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboWorkStatus.TabIndex = 3;
            this.cboWorkStatus.SelectionChangeCommitted += new System.EventHandler(this.cboWorkStatus_SelectionChangeCommitted);
            // 
            // lblWorkStatus
            // 
            // 
            // 
            // 
            this.lblWorkStatus.BackgroundStyle.Class = "";
            this.lblWorkStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWorkStatus.Location = new System.Drawing.Point(311, 62);
            this.lblWorkStatus.Name = "lblWorkStatus";
            this.lblWorkStatus.Size = new System.Drawing.Size(64, 23);
            this.lblWorkStatus.TabIndex = 6;
            this.lblWorkStatus.Text = "Work Status";
            // 
            // Timer
            // 
            this.Timer.Interval = 1000;
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // ErpEmployeeReport
            // 
            this.ErpEmployeeReport.ContainerControl = this;
            // 
            // FrmRptEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(848, 479);
            this.Controls.Add(this.ReportViewer1);
            this.Controls.Add(this.expandablePanel1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptEmployee";
            this.Text = "Employee";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmRptEmployee_Load);
            this.expandablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErpEmployeeReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DevComponents.DotNetBar.LabelX lblDesignation;
        private DevComponents.DotNetBar.LabelX lblDepartment;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboDepartment;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboDesignation;
        private Microsoft.Reporting.WinForms.ReportViewer ReportViewer1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboCompany;
        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        //private DtSetCompanyFormReport dtSetCompanyFormReport;
        private System.Windows.Forms.Timer Timer;
        private DevComponents.DotNetBar.LabelX lblWorkStatus;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboWorkStatus;
        private System.Windows.Forms.ErrorProvider ErpEmployeeReport;
    }
}