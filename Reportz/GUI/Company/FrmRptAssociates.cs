﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Collections;
using System.Data.SqlClient;

using System.IO;
using System.Drawing.Imaging;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,17 May 2011>
Description:	<Description, Associative Report Form>
================================================
*/
namespace MyBooksERP
{
    public partial class FrmRptAssociates : DevComponents.DotNetBar.Office2007Form 
    {
        #region  Variable Declaration

        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;

        private string MsReportPath;
        string PsReportFooter;
        private string MsMessageCaption="";
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        private bool MbViewPermission = true;
        private bool MblnPrintEmailPermission = false;//To Set View Permission
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private DataTable DTCompany;
        private string strTitleName;

        clsBLLAssociates MobjclsBLLAssociates;
        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;
        int intType;
        string strType;
        #endregion //Variables Declaration

        #region Constructor
        public FrmRptAssociates()
        {
            InitializeComponent();
            PsReportFooter = ClsCommonSettings.ReportFooter;
            MobjclsBLLAssociates = new clsBLLAssociates();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification();
            BtnShow.Click += new EventHandler(BtnShow_Click);
           
        }
        #endregion //Constructor

        private void FrmRptAssociates_Load(object sender, EventArgs e)
        {
            LoadMessage();
            //LoadCombos(0);
           // SetPermissions();

            CboAssociative.SelectedIndex = 0;
            cboType.SelectedIndex = 0;
            txtType.Focus();
        }
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.VendorInformation, 4);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.VendorInformation, 4);
        }
        private void SetPermissions()
        {
            clsBLLPermissionSettingsReports objClsBLLPermissionSettings = new clsBLLPermissionSettingsReports();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Reports, (Int32)eMenuID.AssociateReports, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            MbViewPermission = (MblnAddPermission || MblnPrintEmailPermission || MblnUpdatePermission || MblnDeletePermission);
            expandablePanel1.Enabled = MbViewPermission;
        }
        private void ShowReport()
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();

            MsReportPath = Application.StartupPath + "\\MainReports\\RptMISAssociates.rdlc";
            ReportViewer1.LocalReport.ReportPath = MsReportPath;
            ReportParameter[] ReportParameter = new ReportParameter[2];
            int intAssociative = Convert.ToInt32(CboAssociative.SelectedIndex);
          int intTypeselected = Convert.ToInt32(cboType.SelectedIndex);
           // int intTypeselected = Convert.ToInt32(cboType.SelectedText);
            if (cboType.Text == "Sales Invoice")
            {
                intTypeselected = 5;
            }
            if (cboType.Text == "Sales Order")
            {
                intTypeselected = 6;
            }
            if (cboType.Text == "Purchase Invoice")
            {
                intTypeselected = 7;
            }
            if (cboType.Text == "Purchase Order")
            {
                intTypeselected = 8;
            }
            
            if (intAssociative == 0)
            {
                strTitleName = "ASSOCIATES DETAILS";
            }
            else if (intAssociative == 1)
            {
                strTitleName = "SUPPLIER DETAILS";
            }
            else
            {
                strTitleName = "CUSTOMER DETAILS";
            }
            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
            ReportParameter[1] = new ReportParameter("Title", strTitleName, false);
            ReportViewer1.LocalReport.SetParameters(ReportParameter);
            ReportViewer1.LocalReport.DataSources.Clear();
            DTCompany = MobjclsBLLAssociates.DisplayALLCompanyHeaderReport();
            ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
            int intType;

            if (intAssociative == 0)
            {
                intType = 0;

            }
            else if (intAssociative == 1)
            {
                intType = (int)VendorType.Supplier;//2;
            }
            else
            {
                intType = (int)VendorType.Customer;//3;
            }
            string strType= (txtType.Text);
            DataTable DT = MobjclsBLLAssociates.DisplayAssociativeReport(intType, strType, intTypeselected);
            
            if (DT.Rows.Count <= 0)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 320, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
            }
            else
            {
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_VendorInformationMISReport", DT));
                ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                ReportViewer1.ZoomMode = ZoomMode.Percent;
                this.Cursor = Cursors.Default;
            }
        }

        public void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
        }

        private void CboAssociative_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErpAssociatesReport.Clear();

            cboType.Items.Remove("Sales Invoice");
            cboType.Items.Remove("Sales Order");
            cboType.Items.Remove("Purchase Invoice");
            cboType.Items.Remove("Purchase Order");

            if (CboAssociative.SelectedIndex == 1)
            {
                cboType.Items.Add("Purchase Order");
                cboType.Items.Add("Purchase Invoice");
                
            }
            else if (CboAssociative.SelectedIndex == 2)
            {
                cboType.Items.Add("Sales Invoice");

                cboType.Items.Add("Sales Order");
               
            }
            else
            {
                cboType.Items.Add("Sales Invoice");
                cboType.Items.Add("Sales Order");
                cboType.Items.Add("Purchase Invoice");
                cboType.Items.Add("Purchase Order");

            }

            if (intType <= 0 && strType == "")
            {
                ReportViewer1.Clear();
                ReportViewer1.Reset();
                txtType.Text = "";
                cboType.Text = "ALL";
            }
          
            cboType.Text = "ALL";
            txtType.Text = "";
            //if (CboAssociative.SelectedIndex != null)
            //    intAssociates = Convert.ToInt32(CboAssociative.SelectedIndex.ToString());
        }
       private void BtnShow_Click(object sender, EventArgs e)
        {
            try 
            {
                ReportViewer1.Clear();
                ReportViewer1.Reset();
                if (AssociatesReportValidation())
                {
                    BtnShow.Click -= new EventHandler(BtnShow_Click);
                    BtnShow.Enabled = false;
                    ShowReport();
                    Timer.Enabled = true;
                    CboAssociative.Focus();
                }
            }
            catch(Exception)
            {
                Timer.Enabled = true;
            }
        }
       private bool AssociatesReportValidation()
       {
           if (CboAssociative.Enabled == true)
           {
               if (CboAssociative.SelectedIndex == -1)
               {
                   MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9114, out MmessageIcon);
                   MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                   ErpAssociatesReport.SetError(CboAssociative, MsMessageCommon.Replace("#", "").Trim());
                   CboAssociative.Focus();
                   return false;
               }
           }
           if (cboType.Enabled == true)
           {
               if (cboType.SelectedIndex == -1)
               {
                   MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9102, out MmessageIcon);
                   MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                   ErpAssociatesReport.SetError(cboType, MsMessageCommon.Replace("#", "").Trim());
                   cboType.Focus();
                   return false;
               }
           }
           return true;
       }
        private void Timer_Tick(object sender, EventArgs e)
        {
            Timer.Enabled = false;
            BtnShow.Enabled = true;
            BtnShow.Click += new EventHandler(BtnShow_Click);
            ErpAssociatesReport.Clear();
        }
        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErpAssociatesReport.Clear();
            if (cboType.SelectedIndex == 0)
            {
                if (cboType.SelectedIndex > 0 && intType > 0)
                {
                    cboType.SelectedIndex = intType;
                }
            }

            strType = "";
            txtType.AutoCompleteCustomSource = null;
            txtType.Text= string.Empty;
           
            AutoCompleteStringCollection strColValues = new AutoCompleteStringCollection();
 
           // if (cboType.SelectedIndex == 1)
            if (cboType.Text == "Country")
            {
                strColValues = MobjclsBLLAssociates.FillStringCollection(new string[] { "CountryName", "CountryReference", "" });
            }
           // else if (cboType.SelectedIndex == 2)
            else if (cboType.Text =="Status")
            {
                strColValues = MobjclsBLLAssociates.FillStringCollection(new string[] { "Status", "CommonStatusReference", " StatusID in (1,2) " });
            }
          //  else if (cboType.SelectedIndex == 3)
            else if (cboType.Text == "Mobile Number")
            {
                strColValues = MobjclsBLLAssociates.FillStringCollection(new string[] { "MobileNo", "InvVendorAddress", "" });
            }
            //else if (cboType.SelectedIndex == 4)
            else if (cboType.Text == "Transaction Type")
            {
                strColValues = MobjclsBLLAssociates.FillStringCollection(new string[] { "TransactionType", "TransactionTypeReference", "" });
            }
            else if (cboType.Text == "Purchase Invoice")
            {
                strColValues = MobjclsBLLAssociates.FillStringCollection(new string[] {"PurchaseInvoiceNo", "InvPurchaseInvoiceMaster", "" });
            }
            else if (cboType.Text == "Purchase Order")
            {
                strColValues = MobjclsBLLAssociates.FillStringCollection(new string[] { "PurchaseOrderNo", "InvpurchaseOrderMaster", "" });
            }
            else if (cboType.Text == "Sales Invoice")
            {
                strColValues = MobjclsBLLAssociates.FillStringCollection(new string[] {"SalesInvoiceNo", "InvSalesInvoiceMaster", ""});
            }
            else if (cboType.Text == "Sales Order")
            {
                strColValues = MobjclsBLLAssociates.FillStringCollection(new string[] { "SalesOrderNo", "InvSalesOrderMaster", "" });
            }

            txtType.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtType.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtType.AutoCompleteCustomSource = strColValues;
            txtType.Clear();
            txtType.Refresh();

            if (cboType.SelectedValue != null)
                intType = Convert.ToInt32(cboType.SelectedIndex.ToString());
        }

        //private void expandablePanel1_Click(object sender, EventArgs e)
        //{

        //}

        private void expandablePanel1_ExpandedChanged(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {
            //CboAssociative.SelectedIndex = intAssociates;
            cboType.SelectedIndex = intType;
            txtType.Text = strType;
            BtnShow.Enabled = true;
        }

        private void txtType_TextChanged(object sender, EventArgs e)
        {
            if (txtType.Text == "")
            {
                if (strType == "")
                {
                    strType = txtType.Text;
                }
                else
                {
                    txtType.Text = strType;
                }
            }
            else
            {
                strType = txtType.Text;
            }
        }

        private void txtType_KeyPress(object sender, KeyPressEventArgs e)
        {
            strType = "";
        }

        private void BtnShow_Click_1(object sender, EventArgs e)
        {

        }

      
    }
}
