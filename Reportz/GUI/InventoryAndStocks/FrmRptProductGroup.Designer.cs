﻿namespace MyBooksERP
{
    partial class FrmRptProductGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptProductGroup));
            this.expnlRptPdtGroup = new DevComponents.DotNetBar.ExpandablePanel();
            this.btnShow = new DevComponents.DotNetBar.ButtonX();
            this.lblToDate = new DevComponents.DotNetBar.LabelX();
            this.lblFromDate = new DevComponents.DotNetBar.LabelX();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.cboProductGroup = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblGroupName = new DevComponents.DotNetBar.LabelX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.rvProductGroup = new Microsoft.Reporting.WinForms.ReportViewer();
            this.ErrRptPdtGroup = new System.Windows.Forms.ErrorProvider(this.components);
            this.TmrPdtGroup = new System.Windows.Forms.Timer(this.components);
            this.expnlRptPdtGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrRptPdtGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // expnlRptPdtGroup
            // 
            this.expnlRptPdtGroup.CanvasColor = System.Drawing.SystemColors.Control;
            this.expnlRptPdtGroup.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expnlRptPdtGroup.Controls.Add(this.btnShow);
            this.expnlRptPdtGroup.Controls.Add(this.lblToDate);
            this.expnlRptPdtGroup.Controls.Add(this.lblFromDate);
            this.expnlRptPdtGroup.Controls.Add(this.dtpFromDate);
            this.expnlRptPdtGroup.Controls.Add(this.dtpToDate);
            this.expnlRptPdtGroup.Controls.Add(this.cboProductGroup);
            this.expnlRptPdtGroup.Controls.Add(this.lblGroupName);
            this.expnlRptPdtGroup.Controls.Add(this.cboCompany);
            this.expnlRptPdtGroup.Controls.Add(this.lblCompany);
            this.expnlRptPdtGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.expnlRptPdtGroup.Location = new System.Drawing.Point(0, 0);
            this.expnlRptPdtGroup.Name = "expnlRptPdtGroup";
            this.expnlRptPdtGroup.Size = new System.Drawing.Size(1026, 94);
            this.expnlRptPdtGroup.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlRptPdtGroup.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlRptPdtGroup.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlRptPdtGroup.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expnlRptPdtGroup.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expnlRptPdtGroup.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expnlRptPdtGroup.Style.GradientAngle = 90;
            this.expnlRptPdtGroup.TabIndex = 0;
            this.expnlRptPdtGroup.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlRptPdtGroup.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlRptPdtGroup.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlRptPdtGroup.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expnlRptPdtGroup.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlRptPdtGroup.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expnlRptPdtGroup.TitleStyle.GradientAngle = 90;
            this.expnlRptPdtGroup.TitleText = "Filter By";
            // 
            // btnShow
            // 
            this.btnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShow.Location = new System.Drawing.Point(487, 58);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 20);
            this.btnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShow.TabIndex = 26;
            this.btnShow.Text = "Show";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // lblToDate
            // 
            // 
            // 
            // 
            this.lblToDate.BackgroundStyle.Class = "";
            this.lblToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblToDate.Location = new System.Drawing.Point(295, 58);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(37, 19);
            this.lblToDate.TabIndex = 25;
            this.lblToDate.Text = "To";
            // 
            // lblFromDate
            // 
            // 
            // 
            // 
            this.lblFromDate.BackgroundStyle.Class = "";
            this.lblFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFromDate.Location = new System.Drawing.Point(295, 32);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(37, 20);
            this.lblFromDate.TabIndex = 24;
            this.lblFromDate.Text = "From";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd MMM yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(337, 32);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.ShowCheckBox = true;
            this.dtpFromDate.Size = new System.Drawing.Size(121, 20);
            this.dtpFromDate.TabIndex = 23;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd MMM  yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(337, 58);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.ShowCheckBox = true;
            this.dtpToDate.Size = new System.Drawing.Size(121, 20);
            this.dtpToDate.TabIndex = 21;
            this.dtpToDate.ValueChanged += new System.EventHandler(this.dtpToDate_ValueChanged);
            // 
            // cboProductGroup
            // 
            this.cboProductGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboProductGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboProductGroup.DisplayMember = "Text";
            this.cboProductGroup.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboProductGroup.DropDownHeight = 134;
            this.cboProductGroup.DropDownWidth = 184;
            this.cboProductGroup.FormattingEnabled = true;
            this.cboProductGroup.IntegralHeight = false;
            this.cboProductGroup.ItemHeight = 14;
            this.cboProductGroup.Location = new System.Drawing.Point(85, 58);
            this.cboProductGroup.Name = "cboProductGroup";
            this.cboProductGroup.Size = new System.Drawing.Size(184, 20);
            this.cboProductGroup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboProductGroup.TabIndex = 4;
            this.cboProductGroup.SelectedIndexChanged += new System.EventHandler(this.cboProductGroup_SelectedIndexChanged);
            this.cboProductGroup.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboProductGroup_KeyPress);
            this.cboProductGroup.SelectedValueChanged += new System.EventHandler(this.cboProductGroup_SelectedValueChanged);
            // 
            // lblGroupName
            // 
            // 
            // 
            // 
            this.lblGroupName.BackgroundStyle.Class = "";
            this.lblGroupName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblGroupName.Location = new System.Drawing.Point(21, 58);
            this.lblGroupName.Name = "lblGroupName";
            this.lblGroupName.Size = new System.Drawing.Size(48, 20);
            this.lblGroupName.TabIndex = 3;
            this.lblGroupName.Text = "Assembly";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.DropDownWidth = 184;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(85, 32);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(184, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 2;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCompany_KeyPress);
            this.cboCompany.SelectedValueChanged += new System.EventHandler(this.cboCompany_SelectedValueChanged);
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(21, 32);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(48, 20);
            this.lblCompany.TabIndex = 1;
            this.lblCompany.Text = "Company";
            // 
            // rvProductGroup
            // 
            this.rvProductGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rvProductGroup.Location = new System.Drawing.Point(0, 94);
            this.rvProductGroup.Name = "rvProductGroup";
            this.rvProductGroup.Size = new System.Drawing.Size(1026, 460);
            this.rvProductGroup.TabIndex = 1;
            // 
            // ErrRptPdtGroup
            // 
            this.ErrRptPdtGroup.ContainerControl = this;
            // 
            // TmrPdtGroup
            // 
            this.TmrPdtGroup.Tick += new System.EventHandler(this.TmrPdtGroup_Tick);
            // 
            // FrmRptProductGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1026, 554);
            this.Controls.Add(this.rvProductGroup);
            this.Controls.Add(this.expnlRptPdtGroup);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptProductGroup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmRptProductGroup";
            this.Load += new System.EventHandler(this.FrmRptProductGroup_Load);
            this.expnlRptPdtGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErrRptPdtGroup)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expnlRptPdtGroup;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboProductGroup;
        private DevComponents.DotNetBar.LabelX lblGroupName;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private DevComponents.DotNetBar.LabelX lblFromDate;
        private DevComponents.DotNetBar.LabelX lblToDate;
        private DevComponents.DotNetBar.ButtonX btnShow;
        private Microsoft.Reporting.WinForms.ReportViewer rvProductGroup;
        private System.Windows.Forms.ErrorProvider ErrRptPdtGroup;
        private System.Windows.Forms.Timer TmrPdtGroup;

    }
}