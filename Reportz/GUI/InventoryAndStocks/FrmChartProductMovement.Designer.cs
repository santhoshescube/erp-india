﻿namespace MyBooksERP
{
    partial class FrmChartProductMovement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.LegendItem legendItem4 = new System.Windows.Forms.DataVisualization.Charting.LegendItem();
            System.Windows.Forms.DataVisualization.Charting.Title title7 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.Title title8 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.ChartSummary = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.ChartSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // ChartSummary
            // 
            this.ChartSummary.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ChartSummary.BackSecondaryColor = System.Drawing.Color.Transparent;
            this.ChartSummary.BorderlineColor = System.Drawing.Color.Transparent;
            this.ChartSummary.BorderlineWidth = 4;
            this.ChartSummary.BorderSkin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ChartSummary.BorderSkin.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.VerticalCenter;
            this.ChartSummary.BorderSkin.BorderColor = System.Drawing.Color.Transparent;
            this.ChartSummary.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.ChartSummary.BorderSkin.SkinStyle = System.Windows.Forms.DataVisualization.Charting.BorderSkinStyle.FrameTitle1;
            chartArea4.AlignmentOrientation = ((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations)((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Vertical | System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal)));
            chartArea4.Area3DStyle.IsClustered = true;
            chartArea4.Area3DStyle.IsRightAngleAxes = false;
            chartArea4.Area3DStyle.LightStyle = System.Windows.Forms.DataVisualization.Charting.LightStyle.Realistic;
            chartArea4.Area3DStyle.Rotation = 0;
            chartArea4.AxisX.Interval = 1;
            chartArea4.AxisX.IsLabelAutoFit = false;
            chartArea4.AxisX.IsStartedFromZero = false;
            chartArea4.AxisX.LabelStyle.Angle = -45;
            chartArea4.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            chartArea4.AxisX.ScaleBreakStyle.Enabled = true;
            chartArea4.AxisX.ScaleBreakStyle.MaxNumberOfBreaks = 5;
            chartArea4.AxisX.ScaleBreakStyle.Spacing = 2;
            chartArea4.AxisX.ScaleBreakStyle.StartFromZero = System.Windows.Forms.DataVisualization.Charting.StartFromZero.No;
            chartArea4.AxisY.IsLabelAutoFit = false;
            chartArea4.AxisY.TitleAlignment = System.Drawing.StringAlignment.Near;
            chartArea4.BackColor = System.Drawing.Color.Silver;
            chartArea4.CursorX.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea4.CursorX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea4.CursorX.IsUserEnabled = true;
            chartArea4.CursorX.IsUserSelectionEnabled = true;
            chartArea4.Name = "ChartArea1";
            chartArea4.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ChartSummary.ChartAreas.Add(chartArea4);
            this.ChartSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            legendItem4.BackImageTransparentColor = System.Drawing.Color.Blue;
            legend4.CustomItems.Add(legendItem4);
            legend4.Name = "Legend1";
            this.ChartSummary.Legends.Add(legend4);
            this.ChartSummary.Location = new System.Drawing.Point(0, 0);
            this.ChartSummary.Name = "ChartSummary";
            this.ChartSummary.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.ChartSummary.Size = new System.Drawing.Size(884, 478);
            this.ChartSummary.TabIndex = 53;
            this.ChartSummary.Text = "Summary Chart";
            title7.Name = "StockChart";
            title7.Text = "Stock Chart";
            title7.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Embed;
            title7.ToolTip = "Stock Chart";
            title8.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title8.Name = "ReportHead";
            title8.Text = "Stock Summary";
            title8.ToolTip = "Stock Summary";
            this.ChartSummary.Titles.Add(title7);
            this.ChartSummary.Titles.Add(title8);
            // 
            // FrmChartProductMovement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 478);
            this.Controls.Add(this.ChartSummary);
            this.Name = "FrmChartProductMovement";
            this.Text = "Product Movement";
            this.Load += new System.EventHandler(this.FrmChartProductMovement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ChartSummary)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart ChartSummary;
    }
}