﻿namespace MyBooksERP
{
    partial class FrmRptAgeing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptAgeing));
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.lblCustomer = new DevComponents.DotNetBar.LabelX();
            this.CboCustomer = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.rvAgeing = new Microsoft.Reporting.WinForms.ReportViewer();
            this.TmrRptAgeing = new System.Windows.Forms.Timer(this.components);
            this.lblDate = new DevComponents.DotNetBar.LabelX();
            this.DtpDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.CboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ErrRptAgeing = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DtpDate)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrRptAgeing)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(437, 65);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(75, 20);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 21;
            this.BtnShow.Text = "Show";
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // lblCustomer
            // 
            // 
            // 
            // 
            this.lblCustomer.BackgroundStyle.Class = "";
            this.lblCustomer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCustomer.Location = new System.Drawing.Point(285, 37);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(52, 23);
            this.lblCustomer.TabIndex = 15;
            this.lblCustomer.Text = "Customer";
            // 
            // CboCustomer
            // 
            this.CboCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCustomer.DisplayMember = "Text";
            this.CboCustomer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCustomer.DropDownHeight = 75;
            this.CboCustomer.FormattingEnabled = true;
            this.CboCustomer.IntegralHeight = false;
            this.CboCustomer.ItemHeight = 14;
            this.CboCustomer.Location = new System.Drawing.Point(354, 37);
            this.CboCustomer.Name = "CboCustomer";
            this.CboCustomer.Size = new System.Drawing.Size(161, 20);
            this.CboCustomer.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCustomer.TabIndex = 16;
            this.CboCustomer.SelectionChangeCommitted += new System.EventHandler(this.CboCustomer_SelectionChangeCommitted);
            // 
            // rvAgeing
            // 
            this.rvAgeing.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rvAgeing.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource1.Value = null;
            reportDataSource2.Name = "DtSetPurchase_STRFQMaster";
            reportDataSource2.Value = null;
            reportDataSource3.Name = "DtSetPurchase_STRFQDetails";
            reportDataSource3.Value = null;
            this.rvAgeing.LocalReport.DataSources.Add(reportDataSource1);
            this.rvAgeing.LocalReport.DataSources.Add(reportDataSource2);
            this.rvAgeing.LocalReport.DataSources.Add(reportDataSource3);
            this.rvAgeing.LocalReport.ReportEmbeddedResource = "SmartTrade.bin.Debug.MainReports.RptRFQForm.rdlc";
            this.rvAgeing.Location = new System.Drawing.Point(0, 96);
            this.rvAgeing.Name = "rvAgeing";
            this.rvAgeing.Size = new System.Drawing.Size(947, 307);
            this.rvAgeing.TabIndex = 7;
            // 
            // lblDate
            // 
            // 
            // 
            // 
            this.lblDate.BackgroundStyle.Class = "";
            this.lblDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDate.Location = new System.Drawing.Point(26, 62);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(38, 23);
            this.lblDate.TabIndex = 13;
            this.lblDate.Text = "Date";
            // 
            // DtpDate
            // 
            // 
            // 
            // 
            this.DtpDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpDate.ButtonDropDown.Visible = true;
            this.DtpDate.CustomFormat = "dd MMM yyyy";
            this.DtpDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpDate.IsPopupCalendarOpen = false;
            this.DtpDate.Location = new System.Drawing.Point(84, 65);
            this.DtpDate.LockUpdateChecked = false;
            // 
            // 
            // 
            this.DtpDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.DtpDate.MonthCalendar.BackgroundStyle.Class = "";
            this.DtpDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.DtpDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpDate.MonthCalendar.DisplayMonth = new System.DateTime(2011, 4, 1, 0, 0, 0, 0);
            this.DtpDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.DtpDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpDate.MonthCalendar.TodayButtonVisible = true;
            this.DtpDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpDate.Name = "DtpDate";
            this.DtpDate.Size = new System.Drawing.Size(184, 20);
            this.DtpDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpDate.TabIndex = 14;
            this.DtpDate.ValueChanged += new System.EventHandler(this.DtpDate_ValueChanged);
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(26, 33);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(52, 23);
            this.lblCompany.TabIndex = 11;
            this.lblCompany.Text = "Company";
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.BtnShow);
            this.expandablePanel1.Controls.Add(this.CboCustomer);
            this.expandablePanel1.Controls.Add(this.lblCustomer);
            this.expandablePanel1.Controls.Add(this.lblDate);
            this.expandablePanel1.Controls.Add(this.DtpDate);
            this.expandablePanel1.Controls.Add(this.lblCompany);
            this.expandablePanel1.Controls.Add(this.CboCompany);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(947, 96);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 6;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By";
            // 
            // CboCompany
            // 
            this.CboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompany.DisplayMember = "Text";
            this.CboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCompany.DropDownHeight = 75;
            this.CboCompany.FormattingEnabled = true;
            this.CboCompany.IntegralHeight = false;
            this.CboCompany.ItemHeight = 14;
            this.CboCompany.Location = new System.Drawing.Point(84, 37);
            this.CboCompany.Name = "CboCompany";
            this.CboCompany.Size = new System.Drawing.Size(184, 20);
            this.CboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCompany.TabIndex = 12;
            this.CboCompany.SelectionChangeCommitted += new System.EventHandler(this.CboCompany_SelectionChangeCommitted);
            // 
            // ErrRptAgeing
            // 
            this.ErrRptAgeing.ContainerControl = this;
            // 
            // FrmRptAgeing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 403);
            this.Controls.Add(this.rvAgeing);
            this.Controls.Add(this.expandablePanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptAgeing";
            this.Text = "FrmRptAgeing";
            this.Load += new System.EventHandler(this.FrmRptAgeing_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DtpDate)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErrRptAgeing)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DevComponents.DotNetBar.LabelX lblCustomer;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboCustomer;
        private Microsoft.Reporting.WinForms.ReportViewer rvAgeing;
        private System.Windows.Forms.Timer TmrRptAgeing;
        private DevComponents.DotNetBar.LabelX lblDate;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpDate;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboCompany;
        private System.Windows.Forms.ErrorProvider ErrRptAgeing;
    }
}