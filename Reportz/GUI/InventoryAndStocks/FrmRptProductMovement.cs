﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;

namespace MyBooksERP
{
    public partial class FrmRptProductMovement : Form
    {
       
        #region Declarations
        private string MsMessageCaption = "MyBooksERP";
        private string MsMessageCommon;
        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;
        private MessageBoxIcon MmessageIcon;
        ClsNotification MObjClsNotification;
        clsBLLRptProductMovement MobjclsBLLRptProductMovement = new clsBLLRptProductMovement();
        SqlDataReader reader;
        # endregion

        #region Constructor

        public FrmRptProductMovement()
        {
            InitializeComponent();
        }

        #endregion

        #region Page_Load
        private void FrmRptProductMovement_Load(object sender, EventArgs e)
        {
            // Load Message
            LoadMessage();

            //Fill Combos
            FillCombos(0);

            if(ClsCommonSettings.CompanyID > 0)
            cboCompany.SelectedValue = ClsCommonSettings.CompanyID;

            cboReportType.SelectedIndex = 0;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                //validate form inputs
                if (ValidateInputs())
                    ShowReport();
                else
                {
                    // Show message to user
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                }
            }
            catch { }
        }

        private void cboWareHouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Fill Item
            FillCombos(3);
            rptViewer.Clear();
        }

        private void cboSubCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Fill Item
            FillCombos(3);
            rptViewer.Clear();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fill cboWareHouse
            FillCombos(1);
            if(cboReportType.SelectedIndex > 0)
            FillCombos(4);
            rptViewer.Clear();
        }

        private void cboCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fill cboSubCategory
            FillCombos(2);
            // Fill Item
            FillCombos(3);
            rptViewer.Clear();
        }


        #endregion

        /// <summary>
        /// Load Message 
        /// </summary>
        private void LoadMessage()
        {
            // Initialize arraylist for storing messages
            MaMessageArr = new ArrayList();

            MaStatusMessage = new ArrayList();
            MObjClsNotification = new ClsNotification();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.ProductMovementReport, 3);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.ProductMovementReport, 3);
            MsMessageCaption = ClsCommonSettings.MessageCaption;
        }
        /// <summary>
        /// Fill Combos
        /// </summary>
        /// <param name="TypeID"> Specify the combos to be filled</param>
        private void FillCombos(int TypeID)
        {
            //Declare & Initialize data table for binding combos
            DataTable dtCombos = null;

            //Declare & Initialize datatrow
            DataRow drRow = null;

            // Fill cboCategory and cboCompany 
            if (TypeID == 0)
            {
                dtCombos = MobjclsBLLRptProductMovement.GetCompany();
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = dtCombos;

                dtCombos = MobjclsBLLRptProductMovement.GetCategory();
                cboCategory.ValueMember = "CategoryID";
                cboCategory.DisplayMember = "CategoryName";
                cboCategory.DataSource = dtCombos;

            }

            //Bind SubCategory according to Category selected
            if (TypeID == 0 || TypeID == 2)
            {
                dtCombos = MobjclsBLLRptProductMovement.GetSubCategoryByCategory(cboCategory.SelectedValue.ToInt32());
                cboSubCategory.ValueMember = "SubCategoryID";
                cboSubCategory.DisplayMember = "SubCategoryName";
                cboSubCategory.DataSource = dtCombos;

            }

            if (TypeID == 0 || TypeID == 3)
            {
                dtCombos = MobjclsBLLRptProductMovement.GetItems(cboCompany.SelectedValue.ToInt32(),
                    cboCategory.SelectedValue.ToInt32(), cboSubCategory.SelectedValue.ToInt32());
                cboItem.ValueMember = "ItemID";
                cboItem.DisplayMember = "ItemName";
                cboItem.DataSource = dtCombos;
            }

            if (TypeID == 4)
            {
                dtCombos = null;
                cboReferenceNo.Text = string.Empty;
                cboReferenceNo.DataSource = null;
                dtCombos = MobjclsBLLRptProductMovement.GetReferenceNo(cboCompany.SelectedValue.ToInt32(),
                    cboOperationType.SelectedValue.ToInt32(), dtpFromDate.Value.ToDateTime(), dtpToDate.Value.ToDateTime());
                if (dtCombos != null && dtCombos.Rows.Count > 0)
                {
                    cboReferenceNo.ValueMember = "ReferenceID";
                    cboReferenceNo.DisplayMember = "ReferenceNo";
                    cboReferenceNo.DataSource = dtCombos;
                }
            }

            if (TypeID == 5)
            {
                if (cboReportType.SelectedIndex == 1)
                {
                    dtCombos = MobjclsBLLRptProductMovement.FillCombos(new string[] { "OperationTypeID,OperationType", "OperationTypeReference", "OperationTypeID IN (3,5,4,6)" });
                }
                else if (cboReportType.SelectedIndex == 2)
                {
                    dtCombos = MobjclsBLLRptProductMovement.FillCombos(new string[] { "OperationTypeID,OperationType", "OperationTypeReference", "OperationTypeID IN (8,9,10,11,18)" });
                }
                if (dtCombos != null)
                {
                    DataRow dr = dtCombos.NewRow();
                    dr["OperationTypeID"] = "0";
                    dr["OperationType"] = "ALL";
                    dtCombos.Rows.InsertAt(dr,0);

                    cboOperationType.DataSource = dtCombos;
                    cboOperationType.DisplayMember = "OperationType";
                    cboOperationType.ValueMember = "OperationTypeID";
                }
            }
        }

        /// <summary>
        /// Bind Data to report
        /// </summary>
        private void ShowReport()
        {
            // string to specify report path
            string strReportPath = string.Empty;

            // string to specify report footer
            string strReportFooter = ClsCommonSettings.ReportFooter;

            // Get selected FromDate 
            string strFromDate = dtpFromDate.Value.Date.ToString("dd MMM yyyy");

            //Get selected To Date
            string strToDate = dtpToDate.Value.Date.ToString("dd MMM yyyy");

            DataSet dsproductMovement;

            rptViewer.Clear();
            rptViewer.Reset();

            //Fill DataSet to Bind Report
            //Get selected value from cboCompany
            int intCompanyID = cboCompany.SelectedValue.ToInt32();
            //Get selected From Date
            DateTime dtFromDate = Convert.ToDateTime(dtpFromDate.Value);
            //Get selected To Date
            DateTime dtToDate = Convert.ToDateTime(dtpToDate.Value);
            //Get selected value from cboCategory
            int intCategoryID = cboCategory.SelectedValue.ToInt32();
            //Get selected value from cboSubCategory
            int intSubCategoryID = cboSubCategory.SelectedValue.ToInt32();
            //Get selected value from cboItem
            int intItemID = cboItem.SelectedValue.ToInt32();
            int intReportType = cboReportType.SelectedIndex.ToInt32();
            int intType = cboOperationType.SelectedValue.ToInt32();
            long lngReferenceID = 0;
            lngReferenceID = cboReferenceNo.SelectedValue.ToInt64();

            if (intReportType == 0)
                //Bind Dataset with Report values
                dsproductMovement = MobjclsBLLRptProductMovement.GetReport(intCompanyID, dtFromDate, dtToDate, intCategoryID, intSubCategoryID, intItemID);
            else
                dsproductMovement = MobjclsBLLRptProductMovement.GetPurchaseSalesReport(intCompanyID, dtFromDate, dtToDate, intType, lngReferenceID, intReportType);

            // Check whether dataset is not null
            if (dsproductMovement != null)
            {
                //Check whether Table is not null
                if (dsproductMovement.Tables[1] != null)
                {
                    // Check whether Row count is Zero or less than Zero
                    if (dsproductMovement.Tables[1].Rows.Count <= 0)
                    {
                        //display message to indicate no data is found
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9763, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                    else
                    {

                        if (intReportType == 0)
                        {
                            strReportPath = Application.StartupPath + "\\MainReports\\RptProductMovement.rdlc";
                        }
                        else if (intReportType == 1)
                        {
                            strReportPath = Application.StartupPath + "\\MainReports\\rptPdtMovementPurchase.rdlc";
                        }
                        else if (intReportType == 2)
                        {
                            strReportPath = Application.StartupPath + "\\MainReports\\rptPdtMovementSales.rdlc";
                        }

                        //Set Reporth path 
                        rptViewer.LocalReport.ReportPath = strReportPath;
                        // Array to store Report Parameter
                        ReportParameter[] ReportParameter = new ReportParameter[9];
                        // Set Report Footer as report Parameter
                        ReportParameter[0] = new ReportParameter("GeneratedBy", strReportFooter, false);
                        //Set From Date - To Date value as report parameter
                        ReportParameter[1] = new ReportParameter("Period", strFromDate + " - " + strToDate, false);
                        //Set company as Report Parameter
                        ReportParameter[2] = new ReportParameter("Company", cboCompany.Text, false);
                        //Set Category as Report Parameter
                        ReportParameter[3] = new ReportParameter("Category", cboCategory.Text, false);
                        //Set SubCategory as Report Parameter
                        ReportParameter[4] = new ReportParameter("SubCategory", cboSubCategory.Text, false);
                        //Set Item as Report Parameter
                        ReportParameter[5] = new ReportParameter("Item", cboItem.Text, false);
                        ReportParameter[6] = new ReportParameter("ReportType", intReportType.ToString(), false);
                        ReportParameter[7] = new ReportParameter("ReferenceNo", cboReferenceNo.Text, false);
                        ReportParameter[8] = new ReportParameter("Type", Convert.ToString(cboOperationType.SelectedValue), false);
                        //Add parameter to report
                        rptViewer.LocalReport.SetParameters(ReportParameter);
                        //Set DataSource For report
                        rptViewer.LocalReport.DataSources.Clear();
                        rptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetProductMovement_dtCompanyHeader", dsproductMovement.Tables[0]));
                        
                        if (intReportType == 0)
                        {
                            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetProductMovement_dtProductMovement", dsproductMovement.Tables[1]));
                        }

                        else if (intReportType == 1)
                        {
                            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetProductMovement_dtPurchase", dsproductMovement.Tables[1]));
                        }
                        else if (intReportType == 2)
                        {
                            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetProductMovement_dtSales", dsproductMovement.Tables[1]));
                        }
                        //Set display Mode
                        rptViewer.SetDisplayMode(DisplayMode.PrintLayout);
                        rptViewer.ZoomMode = ZoomMode.Percent;
                    }
                }
            }
        }
       
        /// <summary>
        /// Validate Inputs
        /// </summary>
        /// <returns></returns>
        private bool ValidateInputs()
        {
            // Check whether company is selected
            if (cboCompany.SelectedValue.ToInt32() <= 0)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9757, out MmessageIcon);
                return false;
            }
            // Check whether Category is selected
            if (cboCategory.SelectedValue.ToInt32() < 0)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9759, out MmessageIcon);
                return false;
            }
            // Check whether SubCategory is selected
            if (cboSubCategory.SelectedValue.ToInt32() < 0)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9761, out MmessageIcon);
                return false;
            }

            // Check whether Item is selected
            if (cboItem.SelectedValue.ToInt32() < 0)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9762, out MmessageIcon);
                return false;
            }
            //Check whether From Date is after To Date
            if (dtpFromDate.Value.Date > dtpToDate.Value.Date)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9764, out MmessageIcon);
                return false;
            }
            return true;
        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void cboCategory_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCategory.DroppedDown = false;
        }

        private void cboSubCategory_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboSubCategory.DroppedDown = false;
        }

        private void cboItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboItem.DroppedDown = false;
        }

        private void cboOperationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillCombos(4);
            rptViewer.Clear();
            if (cboOperationType.SelectedValue.ToInt32() > 0)
            {
                dtpFromDate.Enabled = false;
                dtpToDate.Enabled = false;
                cboReferenceNo.Enabled = true;
            }
            else
            {
                dtpFromDate.Enabled = true;
                dtpToDate.Enabled = true;
                cboReferenceNo.Enabled = false;
                cboReferenceNo.Text = "ALL";
            }
        }

        private void cboReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboReportType.SelectedIndex == 0)
            {
                cboOperationType.SelectedValue = "0";
                cboOperationType.Enabled = false;
                cboReferenceNo.Enabled = false;
                cboCategory.Enabled = true;
                cboSubCategory.Enabled = true;
                cboItem.Enabled = true;
                cboOperationType.Text = "ALL";
                cboReferenceNo.Text = "ALL";
            }
            else
            {
                cboOperationType.Enabled = true;
                cboReferenceNo.Enabled = true;
                cboCategory.Enabled = false;
                cboSubCategory.Enabled = false;
                cboItem.Enabled = false;
            }
            FillCombos(5);
            rptViewer.Clear();
        }

        private void cboOperationType_SelectedValueChanged(object sender, EventArgs e)
        {
            FillCombos(4);
            if (cboOperationType.SelectedValue.ToInt32() > 0)
            {
                dtpFromDate.Enabled = false;
                dtpToDate.Enabled = false;
                cboReferenceNo.Enabled = true;
            }
            else
            {
                dtpFromDate.Enabled = true;
                dtpToDate.Enabled = true;
                cboReferenceNo.Enabled = false;
                cboReferenceNo.Text = "ALL";
               
            }
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            FillCombos(4);
            rptViewer.Clear();
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            FillCombos(4);
            rptViewer.Clear();
        }

        private void cboItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            rptViewer.Clear();
        }

        private void cboReferenceNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            rptViewer.Clear();
        }

        private void cboOperationType_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboOperationType.DroppedDown = false;
        }

        private void cboReferenceNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboReferenceNo.DroppedDown = false;
        }
      
    }
}
