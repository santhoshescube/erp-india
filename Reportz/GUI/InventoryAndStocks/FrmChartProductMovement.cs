﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Windows.Forms.DataVisualization.Charting;

namespace MyBooksERP
{
    public partial class FrmChartProductMovement : Form
    {
        #region Declaration
        public SqlDataReader reader = null;
        public int intMode ;
        #endregion

        #region Constructor
        public FrmChartProductMovement()
        {
            InitializeComponent();
        }
        #endregion

        #region PageLoad
        private void FrmChartProductMovement_Load(object sender, EventArgs e)
        {
            BindChart();
        }
        #endregion

        /// <summary>
        /// Bind Values to chart
        /// </summary>
        private void BindChart()
        {
            //Check whether reader contains values
            if (reader != null)
            {
                if (reader.HasRows)
                {
                    //Bind dataSource to Chart
                    //reader that contains values and the Field to set as XAxis Value is specified
                    if(intMode == 0)
                    ChartSummary.DataBindTable(reader, "OperationType");
                    else
                    ChartSummary.DataBindTable(reader, "ItemName");

                    //Set Series Property
                    foreach (Series sr in ChartSummary.Series)
                    {
                        //Set series Name and YAxis Value as tooltip
                        sr.ToolTip = sr.Name + " - " + "#VALY{G}";
                        //Show YAxis value as Label
                        sr.IsValueShownAsLabel = true;
                        //Set CustomProperties for series
                        sr.CustomProperties = "PointWidth = 0.8,DrawingStyle = Emboss";
                    }
                }
            }
        }
    }
}
