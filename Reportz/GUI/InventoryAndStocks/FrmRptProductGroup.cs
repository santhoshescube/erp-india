﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Collections;

namespace MyBooksERP
{
    public partial class FrmRptProductGroup : Form
    {
        clsBLLRptProductGroup MobjclsBLLRptProductGroup;

        private string MsReportPath;
        
        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;

        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;

        public FrmRptProductGroup()
        {
            InitializeComponent();
            MobjclsBLLRptProductGroup = new clsBLLRptProductGroup();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification(); 
        }

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmRptProductGroup_Load(object sender, EventArgs e)
        {
            LoadCombos(0);
            LoadMessage();
        }

        /// <summary>
        /// Loads all message
        /// </summary>
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.ProductAssemblyReport, ClsCommonSettings.ProductID);
        }

        private void LoadCombos(int intType)
        {
            DataTable datCombos = null;

            if (intType == 0)
            {
                datCombos = MobjclsBLLRptProductGroup.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                BindCombo(cboCompany, "CompanyName", "CompanyID", datCombos, false);

                datCombos = MobjclsBLLRptProductGroup.FillCombos(new string[] { "ItemGroupID,ItemGroupName+'['+ItemGroupCode+']' AS ItemGroupName", "InvItemGroupMaster", "" });
                BindCombo(cboProductGroup, "ItemGroupName", "ItemGroupID", datCombos, true);

            }
            
        }

        private void BindCombo(ComboBox cboCombo, string strTextField, string strValueField, DataTable datCombos, bool blnInsertRow)
        {
            if (blnInsertRow)
            {
                DataRow dr = datCombos.NewRow();
                dr[strValueField] = 0;
                dr[strTextField] = "ALL";
                datCombos.Rows.InsertAt(dr,0);
            }
            cboCombo.DataSource = datCombos;
            cboCombo.DisplayMember = strTextField;
            cboCombo.ValueMember = strValueField;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                btnShow.Enabled = false;
                if (ValidateReport())
                {
                    ShowReport();
                }
                TmrPdtGroup.Enabled = true;
            }
            catch
            {
                TmrPdtGroup.Enabled = true;
            }
        }

        private void ShowReport()
        {

            ClearReport();

            int intCompany = cboCompany.SelectedValue.ToInt32();
            int intItemGroupID = cboProductGroup.SelectedValue.ToInt32();

            int intchkFrmTo = 0;

            DateTime dtFromDate = dtpFromDate.Value;
            DateTime dtToDate = dtpToDate.Value;

            string PsReportFooter = ClsCommonSettings.ReportFooter;


            if (dtpFromDate.Checked && dtpToDate.Checked)
            {
                intchkFrmTo = 1;
            }
            else if (dtpFromDate.Checked && !dtpToDate.Checked)
            {
                intchkFrmTo = 2;
            }
            else if (!dtpFromDate.Checked && dtpToDate.Checked)
            {
                intchkFrmTo = 3;
            }

            DataTable datCompanyHeader = MobjclsBLLRptProductGroup.GetCompanyHeader(intCompany);


            DataTable datReport = MobjclsBLLRptProductGroup.GetReport(intCompany, intItemGroupID, dtFromDate,dtToDate,intchkFrmTo);

            if (datReport.Rows.Count == 0)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9777, out MmessageIcon);//No data found
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            }
            else
            {
                MsReportPath = Application.StartupPath + "\\MainReports\\RptProductGroup.rdlc";
                rvProductGroup.LocalReport.ReportPath = MsReportPath;

                ReportParameter[] ReportParameter = new ReportParameter[5];
                ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                ReportParameter[1] = new ReportParameter("Company", Convert.ToString(cboCompany.Text.Trim()), false);
                ReportParameter[2] = new ReportParameter("FromDate",dtpFromDate.Checked ? dtpFromDate.Value.ToString("dd MMM yyyy") : "" , false);
                ReportParameter[3] = new ReportParameter("ToDate", dtpToDate.Checked? dtpToDate.Value.ToString("dd MMM yyyy") : "", false);
                ReportParameter[4] = new ReportParameter("ItemGroupName",Convert.ToString(cboProductGroup.Text.Trim()), false);

                rvProductGroup.LocalReport.SetParameters(ReportParameter);
                rvProductGroup.LocalReport.DataSources.Clear();
                rvProductGroup.LocalReport.DataSources.Add(new ReportDataSource("DTSetProductGroup_dtCompanyHeader",datCompanyHeader));
                rvProductGroup.LocalReport.DataSources.Add(new ReportDataSource("DTSetProductGroup_dtAssemblyDetails",datReport));
                rvProductGroup.SetDisplayMode(DisplayMode.PrintLayout);
                rvProductGroup.ZoomMode = ZoomMode.Percent;
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Validate Report
        /// </summary>
        /// <returns></returns>
        private bool ValidateReport()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                ShowErrorMessage(9774, cboCompany);
                return false;
            }
            else if (cboProductGroup.SelectedIndex == -1)
            {
                ShowErrorMessage(9775, cboProductGroup);
                return false;
            }
            else if (dtpFromDate.Checked && dtpToDate.Checked && (dtpFromDate.Value > dtpToDate.Value))
            {
                ShowErrorMessage(9776, dtpFromDate);
                return false;
            }
            return true;
        }

        /// <summary>
        /// shows error message
        /// </summary>
        /// <param name="num">error number</param>
        /// <param name="cntrl">control</param>
        private void ShowErrorMessage(int num, Control cntrl)
        {
            MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, num, out MmessageIcon);
            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            ErrRptPdtGroup.SetError(cntrl, MsMessageCommon.Replace("#", "").Trim());
            TmrPdtGroup.Enabled = true;
            cntrl.Focus();
        }

        private void TmrPdtGroup_Tick(object sender, EventArgs e)
        {
            TmrPdtGroup.Enabled = false;
            btnShow.Enabled = true;
        }

        private void ClearReport()
        {
            ErrRptPdtGroup.Clear();
            rvProductGroup.Clear();
            rvProductGroup.Reset();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void cboProductGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void cboProductGroup_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboProductGroup.DroppedDown = false;
        }

        private void cboCompany_SelectedValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void cboProductGroup_SelectedValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }
    }
}
