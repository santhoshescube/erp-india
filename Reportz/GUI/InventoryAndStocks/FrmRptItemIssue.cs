﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
/* 
======================================================================
Author:		    Sanju
Create date:    07 May 2012
Description:	Delivery Note,Material Issue, Material Return Reports
======================================================================
*/
namespace MyBooksERP
{
    public partial class FrmRptItemIssue : DevComponents.DotNetBar.Office2007Form
    {
        #region Declarations
        private string strCondition = "";
        private clsBLLRptItemIssue MobjclsBLLItemIssue;
        private clsMessage ObjUserMessage = null;
        private ClsLogWriter MObjClsLogWriter;
        DataTable datCombos;
        DataRow datrow;
        int intCompany;
        #endregion
        #region Properties
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.ItemIssue);
                return this.ObjUserMessage;
            }
        }
        private ClsLogWriter LogWriter
        {
            get
            {
                if (this.MObjClsLogWriter == null)
                    this.MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
                return this.MObjClsLogWriter;
            }
        }
        private clsBLLRptItemIssue ItemIssue
        {
            get
            {
                if (this.MobjclsBLLItemIssue == null)
                    this.MobjclsBLLItemIssue = new clsBLLRptItemIssue();
                return this.MobjclsBLLItemIssue;
            }
        }
        #endregion 
        #region Enum
        public enum eRptItemIssueMessageCodes
        {
            SelectCompany = 9100,
            SelectWarehouse = 4203,
            SelectType = 4201,
            SelectNo = 4202,
            SelectStatus = 9103,
            SelectVendor = 9125,
            EnterDate = 9126,
            NoDataFound = 9015
        }   
        #endregion
        #region Constructor
        public FrmRptItemIssue()
        {
            InitializeComponent();
        }
        #endregion
        #region Events
        private void FrmRptItemIssue_Load(object sender, EventArgs e)
        {
            DtpFromDate.Value = ClsCommonSettings.GetServerDate();
            DtpToDate.Value = ClsCommonSettings.GetServerDate();

            LoadCombos(0);
            clearControls();

            if (CboCompany.Items.Count > 0) CboCompany.SelectedIndex = 0;
            if (CboWarehouse.Items.Count > 0) CboWarehouse.SelectedIndex = 0;
            if (CboType.Items.Count > 0) CboType.SelectedIndex = 0;
            if (cboVendor.Items.Count > 0) cboVendor.SelectedIndex = 0;
            if (cboNo.Items.Count > 0) cboNo.SelectedIndex = 0;
            //if (cboStatus.Items.Count > 0) cboStatus.SelectedIndex = 0;
        }
        private void CboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCombos(1);
            LoadCombos(2);
            LoadCombos(3);
            //LoadCombos(4);
            //CboWarehouse.SelectedIndex = -1;
        }

        private void CboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCombos(2);
            LoadCombos(3);
            //LoadCombos(4);
            //cboVendor.SelectedIndex = -1;
            //cboNo.SelectedIndex = -1;
        }

        private void cboVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCombos(3);
            //cboNo.SelectedIndex = -1;
        }

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            try
            {

                if (!formValidation())
                {
                    return;
                }
                ReportViewerItemIssue.Clear();
                ReportViewerItemIssue.Reset();
                string MsReportPath;
                strCondition = "";
                string PsReportFooter = ClsCommonSettings.ReportFooter;
                if (cboNo.SelectedValue.ToInt64() == 0)
                {

                    int intDateType = 0;
                    if (DtpFromDate.LockUpdateChecked && !DtpToDate.LockUpdateChecked)
                        intDateType = 1;
                    else if (!DtpFromDate.LockUpdateChecked && DtpToDate.LockUpdateChecked)
                        intDateType = 2;
                    else if (DtpFromDate.LockUpdateChecked && DtpToDate.LockUpdateChecked)
                        intDateType = 3;



                    if (CboType.SelectedValue.ToInt32() != 4)
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptMISItemIssue.rdlc";
                    else
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptMISMaterialReturn.rdlc";
                    ReportViewerItemIssue.LocalReport.ReportPath = MsReportPath;
                    ReportParameter[] ReportParameters = new ReportParameter[9];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportParameters[1] = new ReportParameter("Company", Convert.ToString(CboCompany.Text.Trim()), false);
                    ReportParameters[2] = new ReportParameter("Vendor", Convert.ToString(cboVendor.Text.Trim()), false);
                    ReportParameters[3] = new ReportParameter("FromDate", Convert.ToString(DtpFromDate.Text.Trim()), false);
                    ReportParameters[4] = new ReportParameter("ToDate", Convert.ToString(DtpToDate.Text.Trim()), false);
                    ReportParameters[5] = new ReportParameter("Type", Convert.ToString(CboType.Text.Trim()), false);
                    ReportParameters[6] = new ReportParameter("Warehouse", CboWarehouse.Text, false);
                    ReportParameters[7] = new ReportParameter("Executive", "", false);
                    ReportParameters[8] = new ReportParameter("DateType", Convert.ToString(intDateType), false);

                    ReportViewerItemIssue.LocalReport.SetParameters(ReportParameters);
                    ReportViewerItemIssue.LocalReport.DataSources.Clear();
                    ReportViewerItemIssue.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
                    //ReportViewerItemIssue.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
                    DataTable dtsItemissue;
                    dtsItemissue = ItemIssue.GetReportDetails(CboType.SelectedValue.ToInt32(), GetFilterCondition());

                    if (dtsItemissue.Rows.Count <= 0)
                    {

                        UserMessage.ShowMessage((int)eRptItemIssueMessageCodes.NoDataFound);
                        ReportViewerItemIssue.Clear();
                        ReportViewerItemIssue.Reset();
                        return;
                    }
                    //else
                    //{
                    DataTable datCompany = new DataTable();
                    datCompany = ItemIssue.DisplayCompanyHeaderReport(ClsCommonSettings.CompanyID);

                    ReportViewerItemIssue.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", datCompany));

                    if (CboType.SelectedValue.ToInt32() != 4)
                        ReportViewerItemIssue.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_dtMISItemIssueDetails", dtsItemissue));
                    else
                        ReportViewerItemIssue.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_dtMISMaterialReturnDetails", dtsItemissue));
                }
                else
                {
                    clsBLLReportViewer MobjclsBLLReportViewer = new clsBLLReportViewer();
                    // MobjclsBLLReportViewer.clsDTOReportviewer = new clsDTOReportviewer();

                    if (CboType.SelectedValue.ToInt32() == 1 || (CboType.SelectedValue.ToInt32() == 2))
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptDeliveryNote.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = cboNo.SelectedValue.ToInt64();
                        if (CboType.SelectedValue.ToInt32() == 1)
                            MobjclsBLLReportViewer.clsDTOReportviewer.strType = "Sales Invoice No";
                        else if (CboType.SelectedValue.ToInt32() == 2)
                            MobjclsBLLReportViewer.clsDTOReportviewer.strType = "Transfer Order No";
                        //else
                        //    MobjclsBLLReportViewer.clsDTOReportviewer.strType = "DMR No";

                        ReportViewerItemIssue.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Reference", MobjclsBLLReportViewer.clsDTOReportviewer.strType, false);
                        //  ReportViewerItemIssue.LocalReport.SetParameters(ReportParameter);
                        ReportViewerItemIssue.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayDeliveryNoteReport();
                        ReportViewerItemIssue.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STItemIssueMaster", dtSet.Tables[0]));
                        ReportViewerItemIssue.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STItemIssueDetails", dtSet.Tables[1]));
                        ReportViewerItemIssue.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STTermsAndConditions", dtSet.Tables[2]));
                        ReportViewerItemIssue.LocalReport.SetParameters(ReportParameter);
                    }
                    else if ((CboType.SelectedValue.ToInt32() == 3))
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptMaterialIssue.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = cboNo.SelectedValue.ToInt64();
                        MobjclsBLLReportViewer.clsDTOReportviewer.strType = null;
                        ReportViewerItemIssue.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Reference", MobjclsBLLReportViewer.clsDTOReportviewer.strType, false);
                        ReportViewerItemIssue.LocalReport.SetParameters(ReportParameter);
                        ReportViewerItemIssue.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayMaterialIssueReport();
                        ReportViewerItemIssue.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STMaterialIssueMaster", dtSet.Tables[0]));
                        ReportViewerItemIssue.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STMaterialIssueDetails", dtSet.Tables[1]));
                        ReportViewerItemIssue.LocalReport.SetParameters(ReportParameter);
                    }
                    else if ((CboType.SelectedValue.ToInt32() == 4))
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptMaterialReturn.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = cboNo.SelectedValue.ToInt64();
                        MobjclsBLLReportViewer.clsDTOReportviewer.strType = null;
                        ReportViewerItemIssue.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Reference", MobjclsBLLReportViewer.clsDTOReportviewer.strType, false);
                        ReportViewerItemIssue.LocalReport.SetParameters(ReportParameter);
                        ReportViewerItemIssue.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayMaterialReturnReport();
                        ReportViewerItemIssue.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STMaterialReturnMaster", dtSet.Tables[0]));
                        ReportViewerItemIssue.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STMaterialReturnDetails", dtSet.Tables[1]));
                        ReportViewerItemIssue.LocalReport.SetParameters(ReportParameter);
                    }
                }
                ReportViewerItemIssue.SetDisplayMode(DisplayMode.PrintLayout);
                ReportViewerItemIssue.ZoomMode = ZoomMode.Percent;

                this.Cursor = Cursors.Default;

                //}
            }
            catch (Exception Ex)
            {
                LogWriter.WriteLog("Error on form Load:BtnShow_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnShow_Click() " + Ex.Message.ToString());
            }
        }

        public void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", ItemIssue.DisplayCompanyHeaderReport(ClsCommonSettings.CompanyID)));
        }

        private void CboWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            //CboType.SelectedIndex = -1;
        }

        private void cboNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //cboStatus.SelectedIndex = -1;
            if (cboNo.SelectedValue.ToInt32() == 0)
            {
                DtpFromDate.Enabled = true;
                DtpToDate.Enabled = true;
            }
            else
            {
                DtpFromDate.Enabled = false;
                DtpToDate.Enabled = false;
                DtpFromDate.LockUpdateChecked = false;
                DtpToDate.LockUpdateChecked = false;
            }
        }
         #endregion
        #region Functions
        private void clearControls()
        {
            CboCompany.SelectedIndex = -1;
        }
        private bool LoadCombos(int intType)
        {
            try
            {
                if (intType == 0)
                {

                    datCombos = ItemIssue.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID="+ClsCommonSettings.LoginCompanyID });                   
                    CboCompany.ValueMember = "CompanyID";
                    CboCompany.DisplayMember = "CompanyName";
                    CboCompany.DataSource = datCombos;
                }
                if (intType == 1)
                {
                    if (CboCompany.SelectedIndex != -1)
                    {
                        intCompany = CboCompany.SelectedValue.ToInt32();
                    }
                    else
                    {
                        intCompany = 0;
                    }
                    //strCondition = "CompanyID= " + intCompany + " Or CompanyID = (Select ParentID From CompanyMaster Where CompanyID= " + intCompany + ") Or " +
                    //" CompanyID in(Select CM2.CompanyID From CompanyMaster CM1 Inner Join CompanyMaster CM2 ON CM2.ParentID = CM1.ParentID Where CM1.CompanyID = " + intCompany + " AND CM1.ParentID!=0) Or " +
                    //" CompanyID in(Select CompanyID From CompanyMaster Where ParentID = " + intCompany + ") And IsActive=1";
                    strCondition = "CompanyID= " + intCompany;
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                        strCondition = "";
                    datCombos = ItemIssue.FillCombos(new string[] { "Distinct InvWarehouse.WarehouseID,WarehouseName", "" +
                            "InvWarehouse INNER JOIN InvWarehouseCompanyDetails WC ON InvWarehouse.WarehouseID=WC.WarehouseID", "" +
                            "WC.CompanyID = " + CboCompany.SelectedValue.ToInt32() + " And IsActive = 1" });
                    datrow = datCombos.NewRow();
                    datrow["WarehouseID"] = 0;
                    datrow["WarehouseName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboWarehouse.ValueMember = "WarehouseID";
                    CboWarehouse.DisplayMember = "WarehouseName";
                    CboWarehouse.DataSource = datCombos;
                }
                if (intType == 0)
                {
                    //datCombos = ItemIssue.FillCombos(new string[] { "OperationTypeID,OperationType", "OperationTypeReference", "ModuleID =2 and OperationTypeID in (" + (int)OperationType.PurchaseQuotation + "," + (int)OperationType.PurchaseOrder + "," + (int)OperationType.PurchaseInvoice + "," + (int)OperationType.DebitNote + "," + (int)OperationType.GRN + ")" });
                    //datrow = datCombos.NewRow();
                    //datrow["OperationTypeID"] = 0;
                    //datrow["OperationType"] = "ALL";
                    //datCombos.Rows.InsertAt(datrow, 0);
                    DataTable datOperationType = new DataTable();
                    datOperationType.Columns.Add("OperationTypeID");
                    datOperationType.Columns.Add("OperationType");
                    DataRow dr1 = datOperationType.NewRow();
                    dr1["OperationTypeID"] = 1;//19
                    dr1["OperationType"] = "Delivery Note from Sales Invoice";
                    datOperationType.Rows.Add(dr1);
                    DataRow dr2 = datOperationType.NewRow();
                    dr2["OperationTypeID"] = 2;//19
                    dr2["OperationType"] = "Delivery Note from Stock Transfer";
                    datOperationType.Rows.Add(dr2);
                    //DataRow dr3 = datOperationType.NewRow();
                    //dr3["OperationTypeID"] = 3;//44
                    //dr3["OperationType"] = "Material Issue";
                    //datOperationType.Rows.Add(dr3);
                    //DataRow dr4 = datOperationType.NewRow();
                    //dr4["OperationTypeID"] = 4;//45
                    //dr4["OperationType"] = "Material Return";
                    //datOperationType.Rows.Add(dr4);
                    CboType.ValueMember = "OperationTypeID";
                    CboType.DisplayMember = "OperationType";
                    CboType.DataSource = datOperationType;
                }
                if (intType == 2)
                {
                    if (CboType.SelectedIndex != -1)
                    {
                        int intCompanyID, intWarehouseID;

                        if (CboCompany.SelectedIndex != -1)
                        {
                            intCompanyID = CboCompany.SelectedValue.ToInt32();
                        }
                        else
                        {
                            intCompanyID = 0;
                        }
                        if (CboWarehouse.SelectedIndex != -1)
                        {
                            intWarehouseID = CboWarehouse.SelectedValue.ToInt32();
                        }
                        else
                        {
                            intWarehouseID = 0;
                        }
                        if (CboType.SelectedValue.ToInt32() == 1)
                        {
                            cboVendor.Enabled = true;
                            datCombos = ItemIssue.FillCombos(new string[] { " Distinct VI.VendorID,VI.VendorName", "InvItemIssueMaster IIM inner join InvItemIssueReferenceDetails IIRD on IIRD.ItemIssueID=IIM.ItemIssueID inner join InvSalesInvoiceMaster SIM on SIM.SalesInvoiceID=IIRD.ReferenceID left join InvVendorInformation VI on VI.VendorID=SIM.VendorID", "IIM.OrderTypeID = " + (int)OperationOrderType.DNOTSalesInvoice + "  and (IIM.WarehouseID=" + intWarehouseID + " or " + intWarehouseID + "=0) and (IIM.CompanyID=" + intCompanyID + " or " + intCompanyID + "=0 )" });

                        }
                        else if (CboType.SelectedValue.ToInt32() == 2)
                        {
                            cboVendor.Enabled = true;
                            datCombos = ItemIssue.FillCombos(new string[] { " Distinct VI.VendorID,VI.VendorName", "InvItemIssueMaster IIM inner join InvItemIssueReferenceDetails IIRD on IIRD.ItemIssueID=IIM.ItemIssueID inner join InvStockTransferMaster STM on STM.StockTransferID=IIRD.ReferenceID left join InvVendorInformation VI on VI.VendorID=STM.VendorID", "IIM.OrderTypeID = " + (int)OperationOrderType.DNOTTransfer + "  and (IIM.WarehouseID=" + intWarehouseID + " or " + intWarehouseID + "=0) and (IIM.CompanyID=" + intCompanyID + " or " + intCompanyID + "=0 )" });

                        }
                        else if (CboType.SelectedValue.ToInt32() == 3)
                        {
                            cboVendor.Enabled = false;
                            //datCombos = ItemIssue.FillCombos(new string[] { " Distinct VI.VendorID,VI.VendorName", "InvItemIssueMaster IIM inner join PrdJobOrder JO on JO.JobOrderID=IIM.ReferenceID and JO.JobOrderTypeID=1 left join InvVendorInformation VI on VI.VendorID=JO.VendorID", " IIM.OrderTypeID = " + (int)OperationOrderType.DNOTDMR + "  and (IIM.WarehouseID=" + intWarehouseID + " or " + intWarehouseID + "=0) and (IIM.CompanyID=" + intCompanyID + " or " + intCompanyID + "=0 )" });
                        }
                        else if (CboType.SelectedValue.ToInt32() == 4)
                        {
                            cboVendor.Enabled = false;
                            //datCombos = ItemIssue.FillCombos(new string[] { " Distinct VI.VendorID,VI.VendorName", "InvGRNMaster GRNM inner join PrdJobOrder JO on JO.JobOrderID=GRNM.ReferenceID and JO.JobOrderTypeID=1 left join InvVendorInformation VI on VI.VendorID=JO.VendorID ", "GRNM.OrderTypeID = " + (int)OperationOrderType.GRNMateialReturnType + "  and (GRNM.WarehouseID=" + intWarehouseID + " or " + intWarehouseID + "=0) and (GRNM.CompanyID=" + intCompanyID + " or " + intCompanyID + "=0 )" });
                        }
                        else
                        {
                            datCombos = null; //ItemIssue.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", 1=2});

                        }
                        if (CboType.SelectedValue.ToInt32() == 3 || CboType.SelectedValue.ToInt32() == 4)
                        {
                            datCombos = null;
                        }
                        else
                        { 
                            datrow = datCombos.NewRow();
                            datrow["VendorID"] = 0;
                            datrow["VendorName"] = "ALL";
                            datCombos.Rows.InsertAt(datrow, 0);
                        }

                        cboVendor.ValueMember = "VendorID";
                        cboVendor.DisplayMember = "VendorName";
                        cboVendor.DataSource = datCombos;
                    }
                }
                if (intType == 3)
                {
                    if (CboType.SelectedIndex != -1)
                    {
                        int intVendorId, intCompanyID, intWarehouseID;
                        if (cboVendor.SelectedIndex != -1)
                        {
                            intVendorId = cboVendor.SelectedValue.ToInt32();
                        }
                        else
                        {
                            intVendorId = 0;
                        }
                        if (CboCompany.SelectedIndex != -1)
                        {
                            intCompanyID = CboCompany.SelectedValue.ToInt32();
                        }
                        else
                        {
                            intCompanyID = 0;
                        }
                        if (CboWarehouse.SelectedIndex != -1)
                        {
                            intWarehouseID = CboWarehouse.SelectedValue.ToInt32();
                        }
                        else
                        {
                            intWarehouseID = 0;
                        }
                        if (CboType.SelectedValue.ToInt32() == 1)
                        {
                            datCombos = ItemIssue.FillCombos(new string[] { "distinct IIM.ItemIssueID as ID , IIM.ItemIssueNo as [No]", "InvItemIssueMaster IIM inner join InvItemIssueReferenceDetails IIRD on IIRD.ItemIssueID=IIM.ItemIssueID inner join InvSalesInvoiceMaster SIM on SIM.SalesInvoiceID=IIRD.ReferenceID left join InvVendorInformation VI on VI.VendorID=SIM.VendorID", "IIM.OrderTypeID = " + (int)OperationOrderType.DNOTSalesInvoice + " and (VI.VendorID=" + intVendorId + " or " + intVendorId + "=0) and (IIM.WarehouseID=" + intWarehouseID + " or " + intWarehouseID + "=0) and (IIM.CompanyID=" + intCompanyID + " or " + intCompanyID + "=0 )" });

                        }
                        else if (CboType.SelectedValue.ToInt32() == 2)
                        {
                            datCombos = ItemIssue.FillCombos(new string[] { "distinct IIM.ItemIssueID as ID , IIM.ItemIssueNo as [No]", "InvItemIssueMaster IIM inner join InvItemIssueReferenceDetails IIRD on IIRD.ItemIssueID=IIM.ItemIssueID inner join InvStockTransferMaster STM on STM.StockTransferID=IIRD.ReferenceID left join InvVendorInformation VI on VI.VendorID=STM.VendorID", "IIM.OrderTypeID = " + (int)OperationOrderType.DNOTTransfer + " and (VI.VendorID=" + intVendorId + " or " + intVendorId + "=0) and (IIM.WarehouseID=" + intWarehouseID + " or " + intWarehouseID + "=0) and (IIM.CompanyID=" + intCompanyID + " or " + intCompanyID + "=0 )" });

                        }
                        else if (CboType.SelectedValue.ToInt32() == 3)
                        {
                            datCombos = ItemIssue.FillCombos(new string[] { "MD.MaterialIssueID as ID , MD.MaterialIssueNo as [No]", "InvMaterialIssueMaster MD", "(MD.WareHouseID=" + intWarehouseID + " or " + intWarehouseID + "=0) and (MD.CompanyID=" + intCompanyID + " or " + intCompanyID + "=0 )" });

                        }
                        else if (CboType.SelectedValue.ToInt32() == 4)
                        {
                            datCombos = ItemIssue.FillCombos(new string[] { "MR.MaterialReturnID as ID,MR.MaterialReturnNo as [No]", "InvMaterialReturnMaster MR", "(MR.WareHouseID=" + intWarehouseID + " or " + intWarehouseID + "=0) and (MR.CompanyID=" + intCompanyID + " or " + intCompanyID + "=0)" });

                        }
                        else
                        {
                            datCombos = null; //ItemIssue.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", 1=2});

                        }
                        datrow = datCombos.NewRow();
                        datrow["ID"] = 0;
                        datrow["No"] = "ALL";
                        datCombos.Rows.InsertAt(datrow, 0);
                        cboNo.ValueMember = "ID";
                        cboNo.DisplayMember = "No";
                        cboNo.DataSource = datCombos;
                    }
                }
                //if (intType == 4)
                //{
                //    if (CboType.SelectedIndex != -1)
                //    {

                //        if (CboType.SelectedValue.ToInt32() == 1)
                //        {
                //            cboStatus.Enabled = true;
                //            datCombos = ItemIssue.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID=" + (int)OperationType.DeliveryNote + "" });

                //        }
                //        else if (CboType.SelectedValue.ToInt32() == 2)
                //        {
                //            cboStatus.Enabled = true;
                //            datCombos = ItemIssue.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID=" + (int)OperationType.DeliveryNote + "" });

                //        }
                //        else if (CboType.SelectedValue.ToInt32() == 3)
                //        {
                //            cboStatus.Enabled = false;
                //            //datCombos = ItemIssue.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID=" + (int)OperationType.MaterialIssue + "" });

                //        }
                //        else if (CboType.SelectedValue.ToInt32() == 4)
                //        {
                //            cboStatus.Enabled = false;
                //            //datCombos = ItemIssue.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID=" + (int)OperationType.MaterialReturn + "" });
                //        }
                //        else
                //        {
                //            datCombos = null; //ItemIssue.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", 1=2});

                //        }
                //        if (CboType.SelectedValue.ToInt32() == 4 || CboType.SelectedValue.ToInt32() == 3)
                //        {
                //            datCombos = null; 
                //        }
                //        else
                //        {
                //            datrow = datCombos.NewRow();
                //            datrow["StatusID"] = 0;
                //            datrow["Status"] = "ALL";
                //            datCombos.Rows.InsertAt(datrow, 0);
                //        }
                
                //        cboStatus.ValueMember = "StatusID";
                //        cboStatus.DisplayMember = "Status";
                //        cboStatus.DataSource = datCombos;
                //    }
                //}
                return true;
            }
            catch (Exception Ex)
            {
                LogWriter.WriteLog("Error on form Load:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
                return true;
            }
        }

        

        private string GetFilterCondition()
        {
            try
            {
                string strFilterCondition = "";
                if (CboCompany.SelectedValue.ToInt32() > 0)
                    strFilterCondition += " CM.CompanyID = " + CboCompany.SelectedValue.ToInt32();
                if (CboWarehouse.SelectedValue.ToInt32() > 0)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "WM.WarehouseID = " + CboWarehouse.SelectedValue.ToInt32();
                }
                if (cboVendor.SelectedValue.ToInt32() > 0)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += " VI.VendorID = " + cboVendor.SelectedValue.ToInt32();
                }
                if (CboType.SelectedValue.ToInt32() == 4)
                {
                    //if (cboStatus.SelectedValue.ToInt32() > 0)
                    //{
                    //    if (!string.IsNullOrEmpty(strFilterCondition))
                    //        strFilterCondition += " And ";
                    //    strFilterCondition += " SR.StatusID = " + cboStatus.SelectedValue.ToInt32();
                    //}

                    if (DtpFromDate.LockUpdateChecked || DtpToDate.LockUpdateChecked)
                    {

                        if (!string.IsNullOrEmpty(strFilterCondition))
                            strFilterCondition += " And ";
                        if (DtpFromDate.LockUpdateChecked && DtpToDate.LockUpdateChecked)
                        {
                            strFilterCondition += " GM.GRNDate Between '" + DtpFromDate.Value.ToString("dd-MMM-yyyy") + "' And '" + DtpToDate.Value.ToString("dd-MMM-yyyy") + "'";
                        }
                        else if (DtpFromDate.LockUpdateChecked)
                        {
                            strFilterCondition += " GM.GRNDate >= '" + DtpFromDate.Value.ToString("dd-MMM-yyyy") + "'";
                        }
                        else
                        {
                            strFilterCondition += " GM.GRNDate <= '" + DtpToDate.Value.ToString("dd-MMM-yyyy") + "'";
                        }
                    }
                }
                else
                {
                    if (DtpFromDate.LockUpdateChecked || DtpToDate.LockUpdateChecked)
                    {

                        if (!string.IsNullOrEmpty(strFilterCondition))
                            strFilterCondition += " And ";
                        if (DtpFromDate.LockUpdateChecked && DtpToDate.LockUpdateChecked)
                        {
                            strFilterCondition += " IIM.IssuedDate Between '" + DtpFromDate.Value.ToString("dd-MMM-yyyy") + "' And '" + DtpToDate.Value.ToString("dd-MMM-yyyy") + "'";
                        }
                        else if (DtpFromDate.LockUpdateChecked)
                        {
                            strFilterCondition += " IIM.IssuedDate >= '" + DtpFromDate.Value.ToString("dd-MMM-yyyy") + "'";
                        }
                        else
                        {
                            strFilterCondition += " IIM.IssuedDate <= '" + DtpToDate.Value.ToString("dd-MMM-yyyy") + "'";
                        }
                    }
                }
                return strFilterCondition;
            }
            catch (Exception Ex)
            {
                LogWriter.WriteLog("Error on form Load:GetFilterCondition " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on GetFilterCondition() " + Ex.Message.ToString());
                return "";
            }
        }
        private Boolean formValidation()
        {
            if (CboCompany.SelectedIndex==-1)
            {
                UserMessage.ShowMessage((int)eRptItemIssueMessageCodes.SelectCompany);
                CboCompany.Focus();
                return false;
            }
            if (CboWarehouse.SelectedIndex==-1)
            {
                UserMessage.ShowMessage((int)eRptItemIssueMessageCodes.SelectWarehouse);
                CboWarehouse.Focus();
                return false;
            }
            if (CboType.SelectedIndex==-1)
            {
                UserMessage.ShowMessage((int)eRptItemIssueMessageCodes.SelectType);
                CboType.Focus();
                return false;
            }


            if (!(CboType.SelectedValue.ToInt32() == 3 || CboType.SelectedValue.ToInt32() == 4 ))
                        {
                if (cboVendor.SelectedIndex == -1)
                {
                    UserMessage.ShowMessage((int)eRptItemIssueMessageCodes.SelectVendor);
                    cboVendor.Focus();
                    return false;
                }
                //if (cboStatus.SelectedIndex == -1)
                //{
                //    UserMessage.ShowMessage((int)eRptItemIssueMessageCodes.SelectStatus);
                //    cboStatus.Focus();
                //    return false;
                //}
            }


            if (cboNo.SelectedIndex==-1)
            {
                UserMessage.ShowMessage((int)eRptItemIssueMessageCodes.SelectNo);
                cboNo.Focus();
                return false;
            }

            if (DtpFromDate.Enabled == true && DtpFromDate.LockUpdateChecked == true && DtpFromDate.Text.Trim() == "")
            {
                UserMessage.ShowMessage((int)eRptItemIssueMessageCodes.EnterDate);
                DtpFromDate.Focus();
                return false;
            }
            if (DtpToDate.Enabled == true && DtpToDate.LockUpdateChecked == true && DtpToDate.Text.Trim() == "")
            {
                UserMessage.ShowMessage((int)eRptItemIssueMessageCodes.EnterDate);
                DtpToDate.Focus();
                return false;
            }
            return true;

        }
         #endregion
    }
}
