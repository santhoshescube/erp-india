﻿namespace MyBooksERP
{
    partial class FrmRptVendorPriceHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptVendorPriceHistory));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.expnlRptPdtGroup = new DevComponents.DotNetBar.ExpandablePanel();
            this.lblMaxValue = new DevComponents.DotNetBar.LabelX();
            this.lblMaxRate = new DevComponents.DotNetBar.LabelX();
            this.lblMinValue = new DevComponents.DotNetBar.LabelX();
            this.lblMinRate = new DevComponents.DotNetBar.LabelX();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFind = new DevComponents.DotNetBar.ButtonX();
            this.lblVendor = new DevComponents.DotNetBar.LabelX();
            this.cboVendor = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblType = new DevComponents.DotNetBar.LabelX();
            this.rbSales = new System.Windows.Forms.RadioButton();
            this.rbPurchase = new System.Windows.Forms.RadioButton();
            this.btnShow = new DevComponents.DotNetBar.ButtonX();
            this.lblToDate = new DevComponents.DotNetBar.LabelX();
            this.lblFromDate = new DevComponents.DotNetBar.LabelX();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.cboItem = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblItemName = new DevComponents.DotNetBar.LabelX();
            this.TmrReport = new System.Windows.Forms.Timer(this.components);
            this.ErrReport = new System.Windows.Forms.ErrorProvider(this.components);
            this.dgvReport = new System.Windows.Forms.DataGridView();
            this.expnlRptPdtGroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).BeginInit();
            this.SuspendLayout();
            // 
            // expnlRptPdtGroup
            // 
            this.expnlRptPdtGroup.CanvasColor = System.Drawing.SystemColors.Control;
            this.expnlRptPdtGroup.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expnlRptPdtGroup.Controls.Add(this.lblMaxValue);
            this.expnlRptPdtGroup.Controls.Add(this.lblMaxRate);
            this.expnlRptPdtGroup.Controls.Add(this.lblMinValue);
            this.expnlRptPdtGroup.Controls.Add(this.lblMinRate);
            this.expnlRptPdtGroup.Controls.Add(this.groupBox1);
            this.expnlRptPdtGroup.Controls.Add(this.lblType);
            this.expnlRptPdtGroup.Controls.Add(this.rbSales);
            this.expnlRptPdtGroup.Controls.Add(this.rbPurchase);
            this.expnlRptPdtGroup.Controls.Add(this.btnShow);
            this.expnlRptPdtGroup.Controls.Add(this.lblToDate);
            this.expnlRptPdtGroup.Controls.Add(this.lblFromDate);
            this.expnlRptPdtGroup.Controls.Add(this.dtpFromDate);
            this.expnlRptPdtGroup.Controls.Add(this.dtpToDate);
            this.expnlRptPdtGroup.Controls.Add(this.cboItem);
            this.expnlRptPdtGroup.Controls.Add(this.lblItemName);
            this.expnlRptPdtGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.expnlRptPdtGroup.Location = new System.Drawing.Point(0, 0);
            this.expnlRptPdtGroup.Name = "expnlRptPdtGroup";
            this.expnlRptPdtGroup.Size = new System.Drawing.Size(1232, 100);
            this.expnlRptPdtGroup.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlRptPdtGroup.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlRptPdtGroup.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlRptPdtGroup.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expnlRptPdtGroup.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expnlRptPdtGroup.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expnlRptPdtGroup.Style.GradientAngle = 90;
            this.expnlRptPdtGroup.TabIndex = 1;
            this.expnlRptPdtGroup.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlRptPdtGroup.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlRptPdtGroup.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlRptPdtGroup.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expnlRptPdtGroup.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlRptPdtGroup.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expnlRptPdtGroup.TitleStyle.GradientAngle = 90;
            this.expnlRptPdtGroup.TitleText = "Filter By";
            // 
            // lblMaxValue
            // 
            // 
            // 
            // 
            this.lblMaxValue.BackgroundStyle.Class = "";
            this.lblMaxValue.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMaxValue.ForeColor = System.Drawing.Color.Black;
            this.lblMaxValue.Location = new System.Drawing.Point(1096, 51);
            this.lblMaxValue.Name = "lblMaxValue";
            this.lblMaxValue.Size = new System.Drawing.Size(123, 20);
            this.lblMaxValue.TabIndex = 36;
            this.lblMaxValue.Visible = false;
            // 
            // lblMaxRate
            // 
            // 
            // 
            // 
            this.lblMaxRate.BackgroundStyle.Class = "";
            this.lblMaxRate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMaxRate.Location = new System.Drawing.Point(1036, 50);
            this.lblMaxRate.Name = "lblMaxRate";
            this.lblMaxRate.Size = new System.Drawing.Size(54, 20);
            this.lblMaxRate.TabIndex = 35;
            this.lblMaxRate.Text = "Max Rate";
            this.lblMaxRate.Visible = false;
            // 
            // lblMinValue
            // 
            // 
            // 
            // 
            this.lblMinValue.BackgroundStyle.Class = "";
            this.lblMinValue.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMinValue.ForeColor = System.Drawing.Color.Black;
            this.lblMinValue.Location = new System.Drawing.Point(961, 50);
            this.lblMinValue.Name = "lblMinValue";
            this.lblMinValue.Size = new System.Drawing.Size(69, 20);
            this.lblMinValue.TabIndex = 34;
            this.lblMinValue.Visible = false;
            // 
            // lblMinRate
            // 
            // 
            // 
            // 
            this.lblMinRate.BackgroundStyle.Class = "";
            this.lblMinRate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMinRate.Location = new System.Drawing.Point(904, 50);
            this.lblMinRate.Name = "lblMinRate";
            this.lblMinRate.Size = new System.Drawing.Size(51, 20);
            this.lblMinRate.TabIndex = 33;
            this.lblMinRate.Text = "Min Rate";
            this.lblMinRate.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnFind);
            this.groupBox1.Controls.Add(this.lblVendor);
            this.groupBox1.Controls.Add(this.cboVendor);
            this.groupBox1.Location = new System.Drawing.Point(572, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(317, 55);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Find";
            // 
            // btnFind
            // 
            this.btnFind.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFind.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFind.Enabled = false;
            this.btnFind.Image = ((System.Drawing.Image)(resources.GetObject("btnFind.Image")));
            this.btnFind.Location = new System.Drawing.Point(262, 19);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(35, 20);
            this.btnFind.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFind.TabIndex = 32;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // lblVendor
            // 
            // 
            // 
            // 
            this.lblVendor.BackgroundStyle.Class = "";
            this.lblVendor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblVendor.Location = new System.Drawing.Point(12, 19);
            this.lblVendor.Name = "lblVendor";
            this.lblVendor.Size = new System.Drawing.Size(50, 20);
            this.lblVendor.TabIndex = 30;
            this.lblVendor.Text = "Supplier";
            // 
            // cboVendor
            // 
            this.cboVendor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboVendor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboVendor.DisplayMember = "Text";
            this.cboVendor.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboVendor.DropDownHeight = 134;
            this.cboVendor.DropDownWidth = 184;
            this.cboVendor.Enabled = false;
            this.cboVendor.FormattingEnabled = true;
            this.cboVendor.IntegralHeight = false;
            this.cboVendor.ItemHeight = 14;
            this.cboVendor.Location = new System.Drawing.Point(70, 19);
            this.cboVendor.Name = "cboVendor";
            this.cboVendor.Size = new System.Drawing.Size(184, 20);
            this.cboVendor.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboVendor.TabIndex = 31;
            this.cboVendor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboVendor_KeyPress);
            // 
            // lblType
            // 
            // 
            // 
            // 
            this.lblType.BackgroundStyle.Class = "";
            this.lblType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblType.Location = new System.Drawing.Point(21, 67);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(33, 20);
            this.lblType.TabIndex = 29;
            this.lblType.Text = "Type";
            // 
            // rbSales
            // 
            this.rbSales.AutoSize = true;
            this.rbSales.Location = new System.Drawing.Point(141, 70);
            this.rbSales.Name = "rbSales";
            this.rbSales.Size = new System.Drawing.Size(51, 17);
            this.rbSales.TabIndex = 28;
            this.rbSales.Text = "Sales";
            this.rbSales.UseVisualStyleBackColor = true;
            // 
            // rbPurchase
            // 
            this.rbPurchase.AutoSize = true;
            this.rbPurchase.Checked = true;
            this.rbPurchase.Location = new System.Drawing.Point(65, 70);
            this.rbPurchase.Name = "rbPurchase";
            this.rbPurchase.Size = new System.Drawing.Size(70, 17);
            this.rbPurchase.TabIndex = 27;
            this.rbPurchase.TabStop = true;
            this.rbPurchase.Text = "Purchase";
            this.rbPurchase.UseVisualStyleBackColor = true;
            this.rbPurchase.CheckedChanged += new System.EventHandler(this.rbPurchase_CheckedChanged);
            // 
            // btnShow
            // 
            this.btnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShow.Location = new System.Drawing.Point(467, 67);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 20);
            this.btnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShow.TabIndex = 26;
            this.btnShow.Text = "Show";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // lblToDate
            // 
            // 
            // 
            // 
            this.lblToDate.BackgroundStyle.Class = "";
            this.lblToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblToDate.Location = new System.Drawing.Point(268, 68);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(37, 19);
            this.lblToDate.TabIndex = 25;
            this.lblToDate.Text = "To";
            // 
            // lblFromDate
            // 
            // 
            // 
            // 
            this.lblFromDate.BackgroundStyle.Class = "";
            this.lblFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFromDate.Location = new System.Drawing.Point(268, 32);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(37, 20);
            this.lblFromDate.TabIndex = 24;
            this.lblFromDate.Text = "From";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Checked = false;
            this.dtpFromDate.CustomFormat = "dd MMM yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(310, 33);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.ShowCheckBox = true;
            this.dtpFromDate.Size = new System.Drawing.Size(121, 20);
            this.dtpFromDate.TabIndex = 23;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            // 
            // dtpToDate
            // 
            this.dtpToDate.Checked = false;
            this.dtpToDate.CustomFormat = "dd MMM  yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(310, 67);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.ShowCheckBox = true;
            this.dtpToDate.Size = new System.Drawing.Size(121, 20);
            this.dtpToDate.TabIndex = 21;
            this.dtpToDate.ValueChanged += new System.EventHandler(this.dtpToDate_ValueChanged);
            // 
            // cboItem
            // 
            this.cboItem.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboItem.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboItem.DisplayMember = "Text";
            this.cboItem.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboItem.DropDownHeight = 134;
            this.cboItem.DropDownWidth = 184;
            this.cboItem.FormattingEnabled = true;
            this.cboItem.IntegralHeight = false;
            this.cboItem.ItemHeight = 14;
            this.cboItem.Location = new System.Drawing.Point(65, 33);
            this.cboItem.Name = "cboItem";
            this.cboItem.Size = new System.Drawing.Size(184, 20);
            this.cboItem.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboItem.TabIndex = 4;
            this.cboItem.SelectedIndexChanged += new System.EventHandler(this.cboItem_SelectedIndexChanged);
            this.cboItem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboItem_KeyPress);
            this.cboItem.SelectedValueChanged += new System.EventHandler(this.cboItem_SelectedValueChanged);
            // 
            // lblItemName
            // 
            // 
            // 
            // 
            this.lblItemName.BackgroundStyle.Class = "";
            this.lblItemName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblItemName.Location = new System.Drawing.Point(21, 32);
            this.lblItemName.Name = "lblItemName";
            this.lblItemName.Size = new System.Drawing.Size(33, 20);
            this.lblItemName.TabIndex = 3;
            this.lblItemName.Text = "Item";
            // 
            // TmrReport
            // 
            this.TmrReport.Tick += new System.EventHandler(this.TmrReport_Tick);
            // 
            // ErrReport
            // 
            this.ErrReport.ContainerControl = this;
            // 
            // dgvReport
            // 
            this.dgvReport.AllowUserToAddRows = false;
            this.dgvReport.AllowUserToDeleteRows = false;
            this.dgvReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvReport.BackgroundColor = System.Drawing.Color.White;
            this.dgvReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvReport.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReport.Location = new System.Drawing.Point(0, 100);
            this.dgvReport.Name = "dgvReport";
            this.dgvReport.ReadOnly = true;
            this.dgvReport.Size = new System.Drawing.Size(1232, 454);
            this.dgvReport.TabIndex = 2;
            // 
            // FrmRptVendorPriceHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1232, 554);
            this.Controls.Add(this.dgvReport);
            this.Controls.Add(this.expnlRptPdtGroup);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptVendorPriceHistory";
            this.Text = "FrmRptVendorPriceHistory";
            this.Load += new System.EventHandler(this.FrmRptVendorPriceHistory_Load);
            this.expnlRptPdtGroup.ResumeLayout(false);
            this.expnlRptPdtGroup.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErrReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expnlRptPdtGroup;
        private DevComponents.DotNetBar.ButtonX btnShow;
        private DevComponents.DotNetBar.LabelX lblToDate;
        private DevComponents.DotNetBar.LabelX lblFromDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboItem;
        private DevComponents.DotNetBar.LabelX lblItemName;
        private System.Windows.Forms.Timer TmrReport;
        private System.Windows.Forms.ErrorProvider ErrReport;
        private System.Windows.Forms.DataGridView dgvReport;
        private System.Windows.Forms.RadioButton rbSales;
        private System.Windows.Forms.RadioButton rbPurchase;
        private DevComponents.DotNetBar.LabelX lblType;
        private DevComponents.DotNetBar.LabelX lblMaxValue;
        private DevComponents.DotNetBar.LabelX lblMaxRate;
        private DevComponents.DotNetBar.LabelX lblMinValue;
        private DevComponents.DotNetBar.LabelX lblMinRate;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevComponents.DotNetBar.ButtonX btnFind;
        private DevComponents.DotNetBar.LabelX lblVendor;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboVendor;
    }
}