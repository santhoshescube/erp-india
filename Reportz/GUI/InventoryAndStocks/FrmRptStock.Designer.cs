﻿ 
namespace MyBooksERP
{
    partial class FrmRptStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptStock));
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.cboItemCode = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.lblStockType = new DevComponents.DotNetBar.LabelX();
            this.CboStockType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblBatch = new DevComponents.DotNetBar.LabelX();
            this.CboBatch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblItem = new DevComponents.DotNetBar.LabelX();
            this.CboItem = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboCategory = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCategory = new DevComponents.DotNetBar.LabelX();
            this.lblSubCategory = new DevComponents.DotNetBar.LabelX();
            this.CboSubCategory = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.lblWarehouse = new DevComponents.DotNetBar.LabelX();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.CboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.ErpStockReport = new System.Windows.Forms.ErrorProvider(this.components);
            this.expandablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErpStockReport)).BeginInit();
            this.SuspendLayout();
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.cboItemCode);
            this.expandablePanel1.Controls.Add(this.labelX1);
            this.expandablePanel1.Controls.Add(this.lblStockType);
            this.expandablePanel1.Controls.Add(this.CboStockType);
            this.expandablePanel1.Controls.Add(this.lblBatch);
            this.expandablePanel1.Controls.Add(this.CboBatch);
            this.expandablePanel1.Controls.Add(this.lblItem);
            this.expandablePanel1.Controls.Add(this.CboItem);
            this.expandablePanel1.Controls.Add(this.CboCategory);
            this.expandablePanel1.Controls.Add(this.lblCategory);
            this.expandablePanel1.Controls.Add(this.lblSubCategory);
            this.expandablePanel1.Controls.Add(this.CboSubCategory);
            this.expandablePanel1.Controls.Add(this.BtnShow);
            this.expandablePanel1.Controls.Add(this.lblWarehouse);
            this.expandablePanel1.Controls.Add(this.lblCompany);
            this.expandablePanel1.Controls.Add(this.CboWarehouse);
            this.expandablePanel1.Controls.Add(this.CboCompany);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(1197, 103);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 4;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By";
            // 
            // cboItemCode
            // 
            this.cboItemCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboItemCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboItemCode.DisplayMember = "Text";
            this.cboItemCode.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboItemCode.DropDownHeight = 75;
            this.cboItemCode.FormattingEnabled = true;
            this.cboItemCode.IntegralHeight = false;
            this.cboItemCode.ItemHeight = 14;
            this.cboItemCode.Location = new System.Drawing.Point(661, 38);
            this.cboItemCode.Name = "cboItemCode";
            this.cboItemCode.Size = new System.Drawing.Size(142, 20);
            this.cboItemCode.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboItemCode.TabIndex = 14;
            this.cboItemCode.SelectionChangeCommitted += new System.EventHandler(this.cboItemCode_SelectionChangeCommitted);
            this.cboItemCode.SelectedIndexChanged += new System.EventHandler(this.cboItemCode_SelectedIndexChanged);
            this.cboItemCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);

            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(596, 36);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(66, 23);
            this.labelX1.TabIndex = 13;
            this.labelX1.Text = "Item Code";
            // 
            // lblStockType
            // 
            // 
            // 
            // 
            this.lblStockType.BackgroundStyle.Class = "";
            this.lblStockType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblStockType.Location = new System.Drawing.Point(596, 68);
            this.lblStockType.Name = "lblStockType";
            this.lblStockType.Size = new System.Drawing.Size(59, 15);
            this.lblStockType.TabIndex = 12;
            this.lblStockType.Text = "Stock Type";
            // 
            // CboStockType
            // 
            this.CboStockType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboStockType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboStockType.DisplayMember = "Text";
            this.CboStockType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboStockType.DropDownHeight = 75;
            this.CboStockType.FormattingEnabled = true;
            this.CboStockType.IntegralHeight = false;
            this.CboStockType.ItemHeight = 14;
            this.CboStockType.Location = new System.Drawing.Point(661, 64);
            this.CboStockType.Name = "CboStockType";
            this.CboStockType.Size = new System.Drawing.Size(172, 20);
            this.CboStockType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboStockType.TabIndex = 6;
            this.CboStockType.SelectionChangeCommitted += new System.EventHandler(this.CboStockType_SelectionChangeCommitted);
            this.CboStockType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblBatch
            // 
            // 
            // 
            // 
            this.lblBatch.BackgroundStyle.Class = "";
            this.lblBatch.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblBatch.Location = new System.Drawing.Point(966, 68);
            this.lblBatch.Name = "lblBatch";
            this.lblBatch.Size = new System.Drawing.Size(58, 23);
            this.lblBatch.TabIndex = 5;
            this.lblBatch.Text = "Batch No";
            this.lblBatch.Visible = false;
            // 
            // CboBatch
            // 
            this.CboBatch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboBatch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboBatch.DisplayMember = "Text";
            this.CboBatch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboBatch.DropDownHeight = 75;
            this.CboBatch.FormattingEnabled = true;
            this.CboBatch.IntegralHeight = false;
            this.CboBatch.ItemHeight = 14;
            this.CboBatch.Location = new System.Drawing.Point(1015, 68);
            this.CboBatch.Name = "CboBatch";
            this.CboBatch.Size = new System.Drawing.Size(170, 20);
            this.CboBatch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboBatch.TabIndex = 5;
            this.CboBatch.Visible = false;
            this.CboBatch.SelectionChangeCommitted += new System.EventHandler(this.CboBatch_SelectionChangeCommitted);
            this.CboBatch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblItem
            // 
            // 
            // 
            // 
            this.lblItem.BackgroundStyle.Class = "";
            this.lblItem.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblItem.Location = new System.Drawing.Point(832, 35);
            this.lblItem.Name = "lblItem";
            this.lblItem.Size = new System.Drawing.Size(66, 23);
            this.lblItem.TabIndex = 4;
            this.lblItem.Text = "Item Name";
            // 
            // CboItem
            // 
            this.CboItem.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboItem.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboItem.DisplayMember = "Text";
            this.CboItem.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboItem.DropDownHeight = 75;
            this.CboItem.FormattingEnabled = true;
            this.CboItem.IntegralHeight = false;
            this.CboItem.ItemHeight = 14;
            this.CboItem.Location = new System.Drawing.Point(897, 35);
            this.CboItem.Name = "CboItem";
            this.CboItem.Size = new System.Drawing.Size(288, 20);
            this.CboItem.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboItem.TabIndex = 4;
            this.CboItem.SelectionChangeCommitted += new System.EventHandler(this.CboItem_SelectionChangeCommitted);
            this.CboItem.SelectedIndexChanged += new System.EventHandler(this.CboItem_SelectedIndexChanged);
            this.CboItem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // CboCategory
            // 
            this.CboCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCategory.DisplayMember = "Text";
            this.CboCategory.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCategory.DropDownHeight = 75;
            this.CboCategory.FormattingEnabled = true;
            this.CboCategory.IntegralHeight = false;
            this.CboCategory.ItemHeight = 14;
            this.CboCategory.Location = new System.Drawing.Point(387, 39);
            this.CboCategory.Name = "CboCategory";
            this.CboCategory.Size = new System.Drawing.Size(190, 20);
            this.CboCategory.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCategory.TabIndex = 2;
            this.CboCategory.SelectionChangeCommitted += new System.EventHandler(this.CboCategory_SelectionChangeCommitted);
            this.CboCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblCategory
            // 
            // 
            // 
            // 
            this.lblCategory.BackgroundStyle.Class = "";
            this.lblCategory.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCategory.Location = new System.Drawing.Point(309, 39);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(68, 23);
            this.lblCategory.TabIndex = 3;
            this.lblCategory.Text = "Category";
            // 
            // lblSubCategory
            // 
            this.lblSubCategory.AutoSize = true;
            // 
            // 
            // 
            this.lblSubCategory.BackgroundStyle.Class = "";
            this.lblSubCategory.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSubCategory.Location = new System.Drawing.Point(309, 67);
            this.lblSubCategory.Name = "lblSubCategory";
            this.lblSubCategory.Size = new System.Drawing.Size(70, 15);
            this.lblSubCategory.TabIndex = 6;
            this.lblSubCategory.Text = "Sub Category";
            // 
            // CboSubCategory
            // 
            this.CboSubCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSubCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSubCategory.DisplayMember = "Text";
            this.CboSubCategory.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboSubCategory.DropDownHeight = 75;
            this.CboSubCategory.FormattingEnabled = true;
            this.CboSubCategory.IntegralHeight = false;
            this.CboSubCategory.ItemHeight = 14;
            this.CboSubCategory.Location = new System.Drawing.Point(387, 65);
            this.CboSubCategory.Name = "CboSubCategory";
            this.CboSubCategory.Size = new System.Drawing.Size(190, 20);
            this.CboSubCategory.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboSubCategory.TabIndex = 3;
            this.CboSubCategory.SelectionChangeCommitted += new System.EventHandler(this.CboSubCategory_SelectionChangeCommitted);
            this.CboSubCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(853, 65);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(78, 19);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 7;
            this.BtnShow.Text = "Show";
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // lblWarehouse
            // 
            // 
            // 
            // 
            this.lblWarehouse.BackgroundStyle.Class = "";
            this.lblWarehouse.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWarehouse.Location = new System.Drawing.Point(14, 63);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(68, 23);
            this.lblWarehouse.TabIndex = 2;
            this.lblWarehouse.Text = "Warehouse";
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(14, 43);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(70, 15);
            this.lblCompany.TabIndex = 0;
            this.lblCompany.Text = "Company";
            // 
            // CboWarehouse
            // 
            this.CboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboWarehouse.DisplayMember = "Text";
            this.CboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboWarehouse.DropDownHeight = 75;
            this.CboWarehouse.FormattingEnabled = true;
            this.CboWarehouse.IntegralHeight = false;
            this.CboWarehouse.ItemHeight = 14;
            this.CboWarehouse.Location = new System.Drawing.Point(90, 65);
            this.CboWarehouse.Name = "CboWarehouse";
            this.CboWarehouse.Size = new System.Drawing.Size(200, 20);
            this.CboWarehouse.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboWarehouse.TabIndex = 1;
            this.CboWarehouse.SelectionChangeCommitted += new System.EventHandler(this.CboWarehouse_SelectionChangeCommitted);
            this.CboWarehouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // CboCompany
            // 
            this.CboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompany.DisplayMember = "Text";
            this.CboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCompany.DropDownHeight = 75;
            this.CboCompany.FormattingEnabled = true;
            this.CboCompany.IntegralHeight = false;
            this.CboCompany.ItemHeight = 14;
            this.CboCompany.Location = new System.Drawing.Point(90, 39);
            this.CboCompany.Name = "CboCompany";
            this.CboCompany.Size = new System.Drawing.Size(200, 20);
            this.CboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCompany.TabIndex = 0;
            this.CboCompany.SelectionChangeCommitted += new System.EventHandler(this.CboCompany_SelectionChangeCommitted);
            this.CboCompany.SelectedIndexChanged += new System.EventHandler(this.CboCompany_SelectedIndexChanged_1);
            this.CboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // Timer
            // 
            this.Timer.Interval = 1000;
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // ReportViewer1
            // 
            this.ReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DtSetInventory_STStockDetails";
            reportDataSource1.Value = null;
            reportDataSource2.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource2.Value = null;
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.ReportViewer1.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptMISStock.rdlc";
            this.ReportViewer1.Location = new System.Drawing.Point(0, 103);
            this.ReportViewer1.Name = "ReportViewer1";
            this.ReportViewer1.Size = new System.Drawing.Size(1197, 399);
            this.ReportViewer1.TabIndex = 5;
            // 
            // ErpStockReport
            // 
            this.ErpStockReport.ContainerControl = this;
            // 
            // FrmRptStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1197, 502);
            this.Controls.Add(this.ReportViewer1);
            this.Controls.Add(this.expandablePanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptStock";
            this.Text = "Stock";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmRptStock_Load);
            this.expandablePanel1.ResumeLayout(false);
            this.expandablePanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErpStockReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.LabelX lblBatch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboBatch;
        private DevComponents.DotNetBar.LabelX lblItem;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboItem;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboCategory;
        private DevComponents.DotNetBar.LabelX lblCategory;
        private DevComponents.DotNetBar.LabelX lblSubCategory;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboSubCategory;
        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DevComponents.DotNetBar.LabelX lblWarehouse;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboCompany;
        private System.Windows.Forms.Timer Timer;
        private Microsoft.Reporting.WinForms.ReportViewer ReportViewer1;
        //private DtSetInventory dtSetInventory;
       
        private DevComponents.DotNetBar.LabelX lblStockType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboStockType;
        private System.Windows.Forms.ErrorProvider ErpStockReport;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboItemCode;
        private DevComponents.DotNetBar.LabelX labelX1;
    }
}