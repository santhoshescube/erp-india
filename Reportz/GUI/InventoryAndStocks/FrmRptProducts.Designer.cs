﻿namespace MyBooksERP
{
    partial class FrmRptProducts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptProducts));
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.lblFeatureValue = new DevComponents.DotNetBar.LabelX();
            this.CboFeatureValue = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblFeature = new DevComponents.DotNetBar.LabelX();
            this.CboFeature = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboSubCategory = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSubCategory = new DevComponents.DotNetBar.LabelX();
            this.dtpTo = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.dtpFrom = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.lblTo = new DevComponents.DotNetBar.LabelX();
            this.lblFrom = new DevComponents.DotNetBar.LabelX();
            this.CboType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ProductDetails = new DevComponents.Editors.ComboItem();
            this.PriceHistory = new DevComponents.Editors.ComboItem();
            this.lblType = new DevComponents.DotNetBar.LabelX();
            this.dtpExpiryDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.lblExpiryDate = new DevComponents.DotNetBar.LabelX();
            this.lblWarehouse = new DevComponents.DotNetBar.LabelX();
            this.CboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.lblStatus = new DevComponents.DotNetBar.LabelX();
            this.lblManufacturer = new DevComponents.DotNetBar.LabelX();
            this.lblCategory = new DevComponents.DotNetBar.LabelX();
            this.CboStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboManufacturer = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboCategory = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.CboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.ErpProductReport = new System.Windows.Forms.ErrorProvider(this.components);
            this.expandablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpExpiryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErpProductReport)).BeginInit();
            this.SuspendLayout();
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.lblFeatureValue);
            this.expandablePanel1.Controls.Add(this.CboFeatureValue);
            this.expandablePanel1.Controls.Add(this.lblFeature);
            this.expandablePanel1.Controls.Add(this.CboFeature);
            this.expandablePanel1.Controls.Add(this.CboSubCategory);
            this.expandablePanel1.Controls.Add(this.lblSubCategory);
            this.expandablePanel1.Controls.Add(this.dtpTo);
            this.expandablePanel1.Controls.Add(this.dtpFrom);
            this.expandablePanel1.Controls.Add(this.lblTo);
            this.expandablePanel1.Controls.Add(this.lblFrom);
            this.expandablePanel1.Controls.Add(this.CboType);
            this.expandablePanel1.Controls.Add(this.lblType);
            this.expandablePanel1.Controls.Add(this.dtpExpiryDate);
            this.expandablePanel1.Controls.Add(this.lblExpiryDate);
            this.expandablePanel1.Controls.Add(this.lblWarehouse);
            this.expandablePanel1.Controls.Add(this.CboWarehouse);
            this.expandablePanel1.Controls.Add(this.BtnShow);
            this.expandablePanel1.Controls.Add(this.lblStatus);
            this.expandablePanel1.Controls.Add(this.lblManufacturer);
            this.expandablePanel1.Controls.Add(this.lblCategory);
            this.expandablePanel1.Controls.Add(this.CboStatus);
            this.expandablePanel1.Controls.Add(this.CboManufacturer);
            this.expandablePanel1.Controls.Add(this.CboCategory);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(1362, 92);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 0;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By";
            this.expandablePanel1.Click += new System.EventHandler(this.expandablePanel1_Click);
            this.expandablePanel1.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandablePanel1_ExpandedChanged);
            // 
            // lblFeatureValue
            // 
            // 
            // 
            // 
            this.lblFeatureValue.BackgroundStyle.Class = "";
            this.lblFeatureValue.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFeatureValue.Location = new System.Drawing.Point(1132, 55);
            this.lblFeatureValue.Name = "lblFeatureValue";
            this.lblFeatureValue.Size = new System.Drawing.Size(76, 23);
            this.lblFeatureValue.TabIndex = 28;
            this.lblFeatureValue.Text = "Feature Value";
            this.lblFeatureValue.Visible = false;
            // 
            // CboFeatureValue
            // 
            this.CboFeatureValue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboFeatureValue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboFeatureValue.DisplayMember = "Text";
            this.CboFeatureValue.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboFeatureValue.DropDownHeight = 134;
            this.CboFeatureValue.FormattingEnabled = true;
            this.CboFeatureValue.IntegralHeight = false;
            this.CboFeatureValue.ItemHeight = 14;
            this.CboFeatureValue.Location = new System.Drawing.Point(1215, 56);
            this.CboFeatureValue.Name = "CboFeatureValue";
            this.CboFeatureValue.Size = new System.Drawing.Size(135, 20);
            this.CboFeatureValue.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboFeatureValue.TabIndex = 29;
            this.CboFeatureValue.Visible = false;
            // 
            // lblFeature
            // 
            // 
            // 
            // 
            this.lblFeature.BackgroundStyle.Class = "";
            this.lblFeature.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFeature.Location = new System.Drawing.Point(1097, 28);
            this.lblFeature.Name = "lblFeature";
            this.lblFeature.Size = new System.Drawing.Size(40, 23);
            this.lblFeature.TabIndex = 26;
            this.lblFeature.Text = "Feature";
            this.lblFeature.Visible = false;
            // 
            // CboFeature
            // 
            this.CboFeature.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboFeature.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboFeature.DisplayMember = "Text";
            this.CboFeature.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboFeature.DropDownHeight = 134;
            this.CboFeature.FormattingEnabled = true;
            this.CboFeature.IntegralHeight = false;
            this.CboFeature.ItemHeight = 14;
            this.CboFeature.Location = new System.Drawing.Point(1143, 30);
            this.CboFeature.Name = "CboFeature";
            this.CboFeature.Size = new System.Drawing.Size(135, 20);
            this.CboFeature.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboFeature.TabIndex = 27;
            this.CboFeature.Visible = false;
            // 
            // CboSubCategory
            // 
            this.CboSubCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSubCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSubCategory.DisplayMember = "Text";
            this.CboSubCategory.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboSubCategory.FormattingEnabled = true;
            this.CboSubCategory.ItemHeight = 14;
            this.CboSubCategory.Location = new System.Drawing.Point(291, 60);
            this.CboSubCategory.Name = "CboSubCategory";
            this.CboSubCategory.Size = new System.Drawing.Size(135, 20);
            this.CboSubCategory.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboSubCategory.TabIndex = 21;
            this.CboSubCategory.SelectionChangeCommitted += new System.EventHandler(this.CboSubCategory_SelectionChangeCommitted);
            this.CboSubCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblSubCategory
            // 
            // 
            // 
            // 
            this.lblSubCategory.BackgroundStyle.Class = "";
            this.lblSubCategory.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSubCategory.Location = new System.Drawing.Point(213, 57);
            this.lblSubCategory.Name = "lblSubCategory";
            this.lblSubCategory.Size = new System.Drawing.Size(75, 23);
            this.lblSubCategory.TabIndex = 20;
            this.lblSubCategory.Text = "Sub Category";
            // 
            // dtpTo
            // 
            // 
            // 
            // 
            this.dtpTo.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtpTo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpTo.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtpTo.ButtonDropDown.Visible = true;
            this.dtpTo.CustomFormat = "dd MMM yyyy";
            this.dtpTo.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.dtpTo.IsPopupCalendarOpen = false;
            this.dtpTo.Location = new System.Drawing.Point(898, 60);
            this.dtpTo.LockUpdateChecked = false;
            // 
            // 
            // 
            this.dtpTo.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpTo.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.dtpTo.MonthCalendar.BackgroundStyle.Class = "";
            this.dtpTo.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpTo.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpTo.MonthCalendar.DisplayMonth = new System.DateTime(2011, 11, 1, 0, 0, 0, 0);
            this.dtpTo.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dtpTo.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpTo.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtpTo.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpTo.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtpTo.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.dtpTo.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpTo.MonthCalendar.TodayButtonVisible = true;
            this.dtpTo.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.ShowCheckBox = true;
            this.dtpTo.Size = new System.Drawing.Size(135, 20);
            this.dtpTo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtpTo.TabIndex = 8;
            this.dtpTo.ValueChanged += new System.EventHandler(this.dtpTo_ValueChanged);
            // 
            // dtpFrom
            // 
            // 
            // 
            // 
            this.dtpFrom.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtpFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpFrom.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtpFrom.ButtonDropDown.Visible = true;
            this.dtpFrom.CustomFormat = "dd MMM yyyy";
            this.dtpFrom.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.dtpFrom.IsPopupCalendarOpen = false;
            this.dtpFrom.Location = new System.Drawing.Point(834, 30);
            this.dtpFrom.LockUpdateChecked = false;
            // 
            // 
            // 
            this.dtpFrom.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpFrom.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.dtpFrom.MonthCalendar.BackgroundStyle.Class = "";
            this.dtpFrom.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpFrom.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpFrom.MonthCalendar.DisplayMonth = new System.DateTime(2011, 11, 1, 0, 0, 0, 0);
            this.dtpFrom.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dtpFrom.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpFrom.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtpFrom.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpFrom.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtpFrom.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.dtpFrom.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpFrom.MonthCalendar.TodayButtonVisible = true;
            this.dtpFrom.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.ShowCheckBox = true;
            this.dtpFrom.Size = new System.Drawing.Size(135, 20);
            this.dtpFrom.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtpFrom.TabIndex = 7;
            this.dtpFrom.ValueChanged += new System.EventHandler(this.dtpFrom_ValueChanged);
            // 
            // lblTo
            // 
            // 
            // 
            // 
            this.lblTo.BackgroundStyle.Class = "";
            this.lblTo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTo.Location = new System.Drawing.Point(860, 56);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(32, 23);
            this.lblTo.TabIndex = 19;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            // 
            // 
            // 
            this.lblFrom.BackgroundStyle.Class = "";
            this.lblFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFrom.Location = new System.Drawing.Point(792, 27);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(32, 23);
            this.lblFrom.TabIndex = 18;
            this.lblFrom.Text = "From";
            // 
            // CboType
            // 
            this.CboType.DisplayMember = "Text";
            this.CboType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboType.FormattingEnabled = true;
            this.CboType.ItemHeight = 14;
            this.CboType.Items.AddRange(new object[] {
            this.ProductDetails,
            this.PriceHistory});
            this.CboType.Location = new System.Drawing.Point(72, 34);
            this.CboType.Name = "CboType";
            this.CboType.Size = new System.Drawing.Size(135, 20);
            this.CboType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboType.TabIndex = 0;
            this.CboType.SelectedIndexChanged += new System.EventHandler(this.CboType_SelectedIndexChanged);
            this.CboType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // ProductDetails
            // 
            this.ProductDetails.Text = "Product Details";
            // 
            // PriceHistory
            // 
            this.PriceHistory.Text = "Price History";
            // 
            // lblType
            // 
            // 
            // 
            // 
            this.lblType.BackgroundStyle.Class = "";
            this.lblType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblType.Location = new System.Drawing.Point(7, 33);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(40, 20);
            this.lblType.TabIndex = 16;
            this.lblType.Text = "Type";
            // 
            // dtpExpiryDate
            // 
            // 
            // 
            // 
            this.dtpExpiryDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtpExpiryDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpExpiryDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtpExpiryDate.ButtonDropDown.Visible = true;
            this.dtpExpiryDate.CustomFormat = "dd MMM yyyy";
            this.dtpExpiryDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.dtpExpiryDate.IsPopupCalendarOpen = false;
            this.dtpExpiryDate.Location = new System.Drawing.Point(1002, 31);
            this.dtpExpiryDate.LockUpdateChecked = false;
            // 
            // 
            // 
            this.dtpExpiryDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpExpiryDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.dtpExpiryDate.MonthCalendar.BackgroundStyle.Class = "";
            this.dtpExpiryDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpExpiryDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtpExpiryDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtpExpiryDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpExpiryDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtpExpiryDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtpExpiryDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtpExpiryDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtpExpiryDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.dtpExpiryDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpExpiryDate.MonthCalendar.DisplayMonth = new System.DateTime(2011, 9, 1, 0, 0, 0, 0);
            this.dtpExpiryDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dtpExpiryDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpExpiryDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtpExpiryDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpExpiryDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtpExpiryDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.dtpExpiryDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpExpiryDate.MonthCalendar.TodayButtonVisible = true;
            this.dtpExpiryDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dtpExpiryDate.Name = "dtpExpiryDate";
            this.dtpExpiryDate.ShowCheckBox = true;
            this.dtpExpiryDate.Size = new System.Drawing.Size(89, 20);
            this.dtpExpiryDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtpExpiryDate.TabIndex = 6;
            this.dtpExpiryDate.Visible = false;
            this.dtpExpiryDate.ValueChanged += new System.EventHandler(this.CboExpiryDate_ValueChanged);
            // 
            // lblExpiryDate
            // 
            // 
            // 
            // 
            this.lblExpiryDate.BackgroundStyle.Class = "";
            this.lblExpiryDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblExpiryDate.Location = new System.Drawing.Point(936, 30);
            this.lblExpiryDate.Name = "lblExpiryDate";
            this.lblExpiryDate.Size = new System.Drawing.Size(63, 23);
            this.lblExpiryDate.TabIndex = 10;
            this.lblExpiryDate.Text = "Expiry Date";
            this.lblExpiryDate.Visible = false;
            // 
            // lblWarehouse
            // 
            // 
            // 
            // 
            this.lblWarehouse.BackgroundStyle.Class = "";
            this.lblWarehouse.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWarehouse.Location = new System.Drawing.Point(7, 57);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(63, 23);
            this.lblWarehouse.TabIndex = 8;
            this.lblWarehouse.Text = "Warehouse";
            // 
            // CboWarehouse
            // 
            this.CboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboWarehouse.DisplayMember = "Text";
            this.CboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboWarehouse.DropDownHeight = 134;
            this.CboWarehouse.FormattingEnabled = true;
            this.CboWarehouse.IntegralHeight = false;
            this.CboWarehouse.ItemHeight = 14;
            this.CboWarehouse.Location = new System.Drawing.Point(72, 60);
            this.CboWarehouse.Name = "CboWarehouse";
            this.CboWarehouse.Size = new System.Drawing.Size(135, 20);
            this.CboWarehouse.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboWarehouse.TabIndex = 2;
            this.CboWarehouse.SelectionChangeCommitted += new System.EventHandler(this.CboWarehouse_SelectionChangeCommitted);
            this.CboWarehouse.SelectedValueChanged += new System.EventHandler(this.CboWarehouse_SelectedValueChanged);
            this.CboWarehouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(663, 60);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(75, 20);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 9;
            this.BtnShow.Text = "Show";
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // lblStatus
            // 
            // 
            // 
            // 
            this.lblStatus.BackgroundStyle.Class = "";
            this.lblStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblStatus.Location = new System.Drawing.Point(432, 57);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(34, 23);
            this.lblStatus.TabIndex = 2;
            this.lblStatus.Text = "Status";
            // 
            // lblManufacturer
            // 
            // 
            // 
            // 
            this.lblManufacturer.BackgroundStyle.Class = "";
            this.lblManufacturer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblManufacturer.Location = new System.Drawing.Point(432, 33);
            this.lblManufacturer.Name = "lblManufacturer";
            this.lblManufacturer.Size = new System.Drawing.Size(67, 23);
            this.lblManufacturer.TabIndex = 0;
            this.lblManufacturer.Text = "Manufacturer";
            // 
            // lblCategory
            // 
            // 
            // 
            // 
            this.lblCategory.BackgroundStyle.Class = "";
            this.lblCategory.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCategory.Location = new System.Drawing.Point(213, 33);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(51, 23);
            this.lblCategory.TabIndex = 1;
            this.lblCategory.Text = "Category";
            // 
            // CboStatus
            // 
            this.CboStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboStatus.DisplayMember = "Text";
            this.CboStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboStatus.DropDownHeight = 134;
            this.CboStatus.FormattingEnabled = true;
            this.CboStatus.IntegralHeight = false;
            this.CboStatus.ItemHeight = 14;
            this.CboStatus.Location = new System.Drawing.Point(500, 60);
            this.CboStatus.Name = "CboStatus";
            this.CboStatus.Size = new System.Drawing.Size(135, 20);
            this.CboStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboStatus.TabIndex = 5;
            this.CboStatus.SelectionChangeCommitted += new System.EventHandler(this.CboStatus_SelectionChangeCommitted);
            this.CboStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // CboManufacturer
            // 
            this.CboManufacturer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboManufacturer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboManufacturer.DisplayMember = "Text";
            this.CboManufacturer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboManufacturer.DropDownHeight = 134;
            this.CboManufacturer.FormattingEnabled = true;
            this.CboManufacturer.IntegralHeight = false;
            this.CboManufacturer.ItemHeight = 14;
            this.CboManufacturer.Location = new System.Drawing.Point(500, 34);
            this.CboManufacturer.Name = "CboManufacturer";
            this.CboManufacturer.Size = new System.Drawing.Size(135, 20);
            this.CboManufacturer.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboManufacturer.TabIndex = 4;
            this.CboManufacturer.SelectionChangeCommitted += new System.EventHandler(this.CboManufacturer_SelectionChangeCommitted);
            this.CboManufacturer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // CboCategory
            // 
            this.CboCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCategory.DisplayMember = "Text";
            this.CboCategory.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCategory.DropDownHeight = 134;
            this.CboCategory.FormattingEnabled = true;
            this.CboCategory.IntegralHeight = false;
            this.CboCategory.ItemHeight = 14;
            this.CboCategory.Location = new System.Drawing.Point(291, 34);
            this.CboCategory.Name = "CboCategory";
            this.CboCategory.Size = new System.Drawing.Size(135, 20);
            this.CboCategory.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCategory.TabIndex = 3;
            this.CboCategory.SelectionChangeCommitted += new System.EventHandler(this.CboCategory_SelectionChangeCommitted);
            this.CboCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(34, 223);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(56, 23);
            this.lblCompany.TabIndex = 15;
            this.lblCompany.Text = "Company";
            this.lblCompany.Visible = false;
            // 
            // CboCompany
            // 
            this.CboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompany.DisplayMember = "Text";
            this.CboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCompany.DropDownHeight = 134;
            this.CboCompany.FormattingEnabled = true;
            this.CboCompany.IntegralHeight = false;
            this.CboCompany.ItemHeight = 14;
            this.CboCompany.Location = new System.Drawing.Point(89, 226);
            this.CboCompany.Name = "CboCompany";
            this.CboCompany.Size = new System.Drawing.Size(135, 20);
            this.CboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCompany.TabIndex = 1;
            this.CboCompany.Visible = false;
            this.CboCompany.SelectionChangeCommitted += new System.EventHandler(this.CboCompany_SelectionChangeCommitted);
            this.CboCompany.SelectedIndexChanged += new System.EventHandler(this.CboCompany_SelectedIndexChanged);
            this.CboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // ReportViewer1
            // 
            this.ReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource1.Value = null;
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.ReportViewer1.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptMISProduct.rdlc";
            this.ReportViewer1.Location = new System.Drawing.Point(0, 92);
            this.ReportViewer1.Name = "ReportViewer1";
            this.ReportViewer1.Size = new System.Drawing.Size(1362, 462);
            this.ReportViewer1.TabIndex = 1;
            // 
            // Timer
            // 
            this.Timer.Interval = 1000;
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // ErpProductReport
            // 
            this.ErpProductReport.ContainerControl = this;
            // 
            // FrmRptProducts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 554);
            this.Controls.Add(this.ReportViewer1);
            this.Controls.Add(this.expandablePanel1);
            this.Controls.Add(this.CboCompany);
            this.Controls.Add(this.lblCompany);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptProducts";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Products";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmRptProducts_Load);
            this.expandablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtpTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpExpiryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErpProductReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private Microsoft.Reporting.WinForms.ReportViewer ReportViewer1;
        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DevComponents.DotNetBar.LabelX lblStatus;
        private DevComponents.DotNetBar.LabelX lblManufacturer;
        private DevComponents.DotNetBar.LabelX lblCategory;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboStatus;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboManufacturer;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboCategory;
        //private DtSetInventory dtSetInventory;
        private System.Windows.Forms.Timer Timer;
       
        private DevComponents.DotNetBar.LabelX lblWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboWarehouse;
        private DevComponents.DotNetBar.LabelX lblExpiryDate;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtpExpiryDate;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboCompany;
        private DevComponents.DotNetBar.LabelX lblType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboType;
        private DevComponents.Editors.ComboItem ProductDetails;
        private DevComponents.Editors.ComboItem PriceHistory;
        private DevComponents.DotNetBar.LabelX lblTo;
        private DevComponents.DotNetBar.LabelX lblFrom;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtpTo;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtpFrom;
        private System.Windows.Forms.ErrorProvider ErpProductReport;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboSubCategory;
        private DevComponents.DotNetBar.LabelX lblSubCategory;
        private DevComponents.DotNetBar.LabelX lblFeatureValue;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboFeatureValue;
        private DevComponents.DotNetBar.LabelX lblFeature;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboFeature;
    }
}