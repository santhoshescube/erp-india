﻿namespace MyBooksERP
{
    partial class FrmRptProductMovement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptProductMovement));
            this.expanelTop = new DevComponents.DotNetBar.ExpandablePanel();
            this.cboCategory = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCategory = new DevComponents.DotNetBar.LabelX();
            this.cboReferenceNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblReferenceNo = new DevComponents.DotNetBar.LabelX();
            this.cboOperationType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblOperationType = new DevComponents.DotNetBar.LabelX();
            this.cboReportType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ALL = new DevComponents.Editors.ComboItem();
            this.Purchase = new DevComponents.Editors.ComboItem();
            this.Sales = new DevComponents.Editors.ComboItem();
            this.lblReportType = new DevComponents.DotNetBar.LabelX();
            this.btnShow = new DevComponents.DotNetBar.ButtonX();
            this.cboSubCategory = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboItem = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSubCategory = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblFrom = new DevComponents.DotNetBar.LabelX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new DevComponents.DotNetBar.LabelX();
            this.rptViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.expanelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // expanelTop
            // 
            this.expanelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.expanelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expanelTop.Controls.Add(this.cboCategory);
            this.expanelTop.Controls.Add(this.lblCategory);
            this.expanelTop.Controls.Add(this.cboReferenceNo);
            this.expanelTop.Controls.Add(this.lblReferenceNo);
            this.expanelTop.Controls.Add(this.cboOperationType);
            this.expanelTop.Controls.Add(this.lblOperationType);
            this.expanelTop.Controls.Add(this.cboReportType);
            this.expanelTop.Controls.Add(this.lblReportType);
            this.expanelTop.Controls.Add(this.btnShow);
            this.expanelTop.Controls.Add(this.cboSubCategory);
            this.expanelTop.Controls.Add(this.cboItem);
            this.expanelTop.Controls.Add(this.lblSubCategory);
            this.expanelTop.Controls.Add(this.labelX1);
            this.expanelTop.Controls.Add(this.dtpFromDate);
            this.expanelTop.Controls.Add(this.lblFrom);
            this.expanelTop.Controls.Add(this.cboCompany);
            this.expanelTop.Controls.Add(this.lblCompany);
            this.expanelTop.Controls.Add(this.dtpToDate);
            this.expanelTop.Controls.Add(this.lblTo);
            this.expanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expanelTop.Location = new System.Drawing.Point(0, 0);
            this.expanelTop.Name = "expanelTop";
            this.expanelTop.Size = new System.Drawing.Size(1244, 87);
            this.expanelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expanelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expanelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expanelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expanelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expanelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expanelTop.Style.GradientAngle = 90;
            this.expanelTop.TabIndex = 2;
            this.expanelTop.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expanelTop.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expanelTop.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expanelTop.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expanelTop.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expanelTop.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expanelTop.TitleStyle.GradientAngle = 90;
            this.expanelTop.TitleText = "Filter By";
            // 
            // cboCategory
            // 
            this.cboCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCategory.DisplayMember = "Text";
            this.cboCategory.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCategory.DropDownHeight = 134;
            this.cboCategory.FormattingEnabled = true;
            this.cboCategory.IntegralHeight = false;
            this.cboCategory.ItemHeight = 14;
            this.cboCategory.Location = new System.Drawing.Point(325, 35);
            this.cboCategory.Name = "cboCategory";
            this.cboCategory.Size = new System.Drawing.Size(129, 20);
            this.cboCategory.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCategory.TabIndex = 34;
            this.cboCategory.SelectedIndexChanged += new System.EventHandler(this.cboCategory_SelectedIndexChanged);
            this.cboCategory.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCategory_KeyPress);
            // 
            // lblCategory
            // 
            // 
            // 
            // 
            this.lblCategory.BackgroundStyle.Class = "";
            this.lblCategory.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCategory.Location = new System.Drawing.Point(250, 35);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(75, 23);
            this.lblCategory.TabIndex = 33;
            this.lblCategory.Text = "Category";
            // 
            // cboReferenceNo
            // 
            this.cboReferenceNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboReferenceNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboReferenceNo.DisplayMember = "Text";
            this.cboReferenceNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboReferenceNo.DropDownHeight = 134;
            this.cboReferenceNo.FormattingEnabled = true;
            this.cboReferenceNo.IntegralHeight = false;
            this.cboReferenceNo.ItemHeight = 14;
            this.cboReferenceNo.Location = new System.Drawing.Point(805, 35);
            this.cboReferenceNo.Name = "cboReferenceNo";
            this.cboReferenceNo.Size = new System.Drawing.Size(94, 20);
            this.cboReferenceNo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboReferenceNo.TabIndex = 32;
            this.cboReferenceNo.SelectedIndexChanged += new System.EventHandler(this.cboReferenceNo_SelectedIndexChanged);
            this.cboReferenceNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboReferenceNo_KeyPress);
            // 
            // lblReferenceNo
            // 
            // 
            // 
            // 
            this.lblReferenceNo.BackgroundStyle.Class = "";
            this.lblReferenceNo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblReferenceNo.Location = new System.Drawing.Point(725, 35);
            this.lblReferenceNo.Name = "lblReferenceNo";
            this.lblReferenceNo.Size = new System.Drawing.Size(74, 20);
            this.lblReferenceNo.TabIndex = 31;
            this.lblReferenceNo.Text = "Reference No";
            // 
            // cboOperationType
            // 
            this.cboOperationType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOperationType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOperationType.DisplayMember = "Text";
            this.cboOperationType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboOperationType.DropDownHeight = 134;
            this.cboOperationType.FormattingEnabled = true;
            this.cboOperationType.IntegralHeight = false;
            this.cboOperationType.ItemHeight = 14;
            this.cboOperationType.Location = new System.Drawing.Point(556, 60);
            this.cboOperationType.Name = "cboOperationType";
            this.cboOperationType.Size = new System.Drawing.Size(160, 20);
            this.cboOperationType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboOperationType.TabIndex = 30;
            this.cboOperationType.SelectedIndexChanged += new System.EventHandler(this.cboOperationType_SelectedIndexChanged);
            this.cboOperationType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboOperationType_KeyPress);
            this.cboOperationType.SelectedValueChanged += new System.EventHandler(this.cboOperationType_SelectedValueChanged);
            // 
            // lblOperationType
            // 
            // 
            // 
            // 
            this.lblOperationType.BackgroundStyle.Class = "";
            this.lblOperationType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblOperationType.Location = new System.Drawing.Point(470, 57);
            this.lblOperationType.Name = "lblOperationType";
            this.lblOperationType.Size = new System.Drawing.Size(81, 23);
            this.lblOperationType.TabIndex = 29;
            this.lblOperationType.Text = "Operation Type";
            // 
            // cboReportType
            // 
            this.cboReportType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboReportType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboReportType.DisplayMember = "Text";
            this.cboReportType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboReportType.DropDownHeight = 134;
            this.cboReportType.FormattingEnabled = true;
            this.cboReportType.IntegralHeight = false;
            this.cboReportType.ItemHeight = 14;
            this.cboReportType.Items.AddRange(new object[] {
            this.ALL,
            this.Purchase,
            this.Sales});
            this.cboReportType.Location = new System.Drawing.Point(83, 60);
            this.cboReportType.Name = "cboReportType";
            this.cboReportType.Size = new System.Drawing.Size(154, 20);
            this.cboReportType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboReportType.TabIndex = 28;
            this.cboReportType.SelectedIndexChanged += new System.EventHandler(this.cboReportType_SelectedIndexChanged);
            // 
            // ALL
            // 
            this.ALL.Text = "ALL";
            // 
            // Purchase
            // 
            this.Purchase.Text = "Purchase";
            // 
            // Sales
            // 
            this.Sales.Text = "Sales";
            // 
            // lblReportType
            // 
            // 
            // 
            // 
            this.lblReportType.BackgroundStyle.Class = "";
            this.lblReportType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblReportType.Location = new System.Drawing.Point(12, 60);
            this.lblReportType.Name = "lblReportType";
            this.lblReportType.Size = new System.Drawing.Size(65, 20);
            this.lblReportType.TabIndex = 27;
            this.lblReportType.Text = "Report Type";
            // 
            // btnShow
            // 
            this.btnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShow.Location = new System.Drawing.Point(1077, 52);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(92, 27);
            this.btnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShow.TabIndex = 26;
            this.btnShow.Text = "Show Report";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // cboSubCategory
            // 
            this.cboSubCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSubCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSubCategory.DisplayMember = "Text";
            this.cboSubCategory.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSubCategory.DropDownHeight = 134;
            this.cboSubCategory.FormattingEnabled = true;
            this.cboSubCategory.IntegralHeight = false;
            this.cboSubCategory.ItemHeight = 14;
            this.cboSubCategory.Location = new System.Drawing.Point(325, 60);
            this.cboSubCategory.Name = "cboSubCategory";
            this.cboSubCategory.Size = new System.Drawing.Size(129, 20);
            this.cboSubCategory.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboSubCategory.TabIndex = 24;
            this.cboSubCategory.SelectedIndexChanged += new System.EventHandler(this.cboSubCategory_SelectedIndexChanged);
            this.cboSubCategory.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboSubCategory_KeyPress);
            // 
            // cboItem
            // 
            this.cboItem.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboItem.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboItem.DisplayMember = "Text";
            this.cboItem.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboItem.DropDownHeight = 134;
            this.cboItem.DropDownWidth = 166;
            this.cboItem.FormattingEnabled = true;
            this.cboItem.IntegralHeight = false;
            this.cboItem.ItemHeight = 14;
            this.cboItem.Location = new System.Drawing.Point(556, 35);
            this.cboItem.Name = "cboItem";
            this.cboItem.Size = new System.Drawing.Size(160, 20);
            this.cboItem.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboItem.TabIndex = 23;
            this.cboItem.SelectedIndexChanged += new System.EventHandler(this.cboItem_SelectedIndexChanged);
            this.cboItem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboItem_KeyPress);
            // 
            // lblSubCategory
            // 
            // 
            // 
            // 
            this.lblSubCategory.BackgroundStyle.Class = "";
            this.lblSubCategory.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSubCategory.Location = new System.Drawing.Point(250, 61);
            this.lblSubCategory.Name = "lblSubCategory";
            this.lblSubCategory.Size = new System.Drawing.Size(75, 19);
            this.lblSubCategory.TabIndex = 22;
            this.lblSubCategory.Text = "Sub Category";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(469, 35);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(33, 21);
            this.labelX1.TabIndex = 21;
            this.labelX1.Text = "Item";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd MMM yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(949, 35);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(109, 20);
            this.dtpFromDate.TabIndex = 20;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            // 
            // 
            // 
            this.lblFrom.BackgroundStyle.Class = "";
            this.lblFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFrom.Location = new System.Drawing.Point(910, 35);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(28, 15);
            this.lblFrom.TabIndex = 19;
            this.lblFrom.Text = "From";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(83, 35);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(154, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 0;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCompany_KeyPress);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(12, 35);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(50, 15);
            this.lblCompany.TabIndex = 15;
            this.lblCompany.Text = "Company";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd MMM  yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(949, 60);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(109, 20);
            this.dtpToDate.TabIndex = 7;
            this.dtpToDate.ValueChanged += new System.EventHandler(this.dtpToDate_ValueChanged);
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            // 
            // 
            // 
            this.lblTo.BackgroundStyle.Class = "";
            this.lblTo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTo.Location = new System.Drawing.Point(910, 65);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(15, 15);
            this.lblTo.TabIndex = 3;
            this.lblTo.Text = "To";
            // 
            // rptViewer
            // 
            this.rptViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptViewer.Location = new System.Drawing.Point(0, 87);
            this.rptViewer.Name = "rptViewer";
            this.rptViewer.Size = new System.Drawing.Size(1244, 408);
            this.rptViewer.TabIndex = 3;
            // 
            // FrmRptProductMovement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1244, 495);
            this.Controls.Add(this.rptViewer);
            this.Controls.Add(this.expanelTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptProductMovement";
            this.Text = "FrmRptProductMovement";
            this.Load += new System.EventHandler(this.FrmRptProductMovement_Load);
            this.expanelTop.ResumeLayout(false);
            this.expanelTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expanelTop;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSubCategory;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboItem;
        private DevComponents.DotNetBar.LabelX lblSubCategory;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private DevComponents.DotNetBar.LabelX lblFrom;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private DevComponents.DotNetBar.LabelX lblTo;
        private DevComponents.DotNetBar.ButtonX btnShow;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboReportType;
        private DevComponents.DotNetBar.LabelX lblReportType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboOperationType;
        private DevComponents.DotNetBar.LabelX lblOperationType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboReferenceNo;
        private DevComponents.DotNetBar.LabelX lblReferenceNo;
        private DevComponents.Editors.ComboItem ALL;
        private DevComponents.Editors.ComboItem Purchase;
        private DevComponents.Editors.ComboItem Sales;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCategory;
        private DevComponents.DotNetBar.LabelX lblCategory;
        private Microsoft.Reporting.WinForms.ReportViewer rptViewer;

    }
}