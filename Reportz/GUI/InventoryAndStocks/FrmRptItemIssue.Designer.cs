﻿namespace MyBooksERP
{
    partial class FrmRptItemIssue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptItemIssue));
            this.expandablePanelItemIssue = new DevComponents.DotNetBar.ExpandablePanel();
            this.cboNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.cboVendor = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblOperationType = new DevComponents.DotNetBar.LabelX();
            this.lblWarehouse = new DevComponents.DotNetBar.LabelX();
            this.CboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblTo = new DevComponents.DotNetBar.LabelX();
            this.lblFrom = new DevComponents.DotNetBar.LabelX();
            this.DtpToDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.DtpFromDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.lblType = new DevComponents.DotNetBar.LabelX();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.CboType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ReportViewerItemIssue = new Microsoft.Reporting.WinForms.ReportViewer();
            this.expandablePanelItemIssue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtpToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFromDate)).BeginInit();
            this.SuspendLayout();
            // 
            // expandablePanelItemIssue
            // 
            this.expandablePanelItemIssue.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanelItemIssue.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanelItemIssue.Controls.Add(this.cboNo);
            this.expandablePanelItemIssue.Controls.Add(this.labelX1);
            this.expandablePanelItemIssue.Controls.Add(this.cboVendor);
            this.expandablePanelItemIssue.Controls.Add(this.lblOperationType);
            this.expandablePanelItemIssue.Controls.Add(this.lblWarehouse);
            this.expandablePanelItemIssue.Controls.Add(this.CboWarehouse);
            this.expandablePanelItemIssue.Controls.Add(this.lblTo);
            this.expandablePanelItemIssue.Controls.Add(this.lblFrom);
            this.expandablePanelItemIssue.Controls.Add(this.DtpToDate);
            this.expandablePanelItemIssue.Controls.Add(this.DtpFromDate);
            this.expandablePanelItemIssue.Controls.Add(this.BtnShow);
            this.expandablePanelItemIssue.Controls.Add(this.lblType);
            this.expandablePanelItemIssue.Controls.Add(this.lblCompany);
            this.expandablePanelItemIssue.Controls.Add(this.CboType);
            this.expandablePanelItemIssue.Controls.Add(this.CboCompany);
            this.expandablePanelItemIssue.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanelItemIssue.Location = new System.Drawing.Point(0, 0);
            this.expandablePanelItemIssue.Name = "expandablePanelItemIssue";
            this.expandablePanelItemIssue.Size = new System.Drawing.Size(948, 118);
            this.expandablePanelItemIssue.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanelItemIssue.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanelItemIssue.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanelItemIssue.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanelItemIssue.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanelItemIssue.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanelItemIssue.Style.GradientAngle = 90;
            this.expandablePanelItemIssue.TabIndex = 4;
            this.expandablePanelItemIssue.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanelItemIssue.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanelItemIssue.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanelItemIssue.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanelItemIssue.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanelItemIssue.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanelItemIssue.TitleStyle.GradientAngle = 90;
            this.expandablePanelItemIssue.TitleText = "Filter By";
            // 
            // cboNo
            // 
            this.cboNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboNo.DisplayMember = "Text";
            this.cboNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboNo.FormattingEnabled = true;
            this.cboNo.ItemHeight = 14;
            this.cboNo.Location = new System.Drawing.Point(358, 59);
            this.cboNo.Name = "cboNo";
            this.cboNo.Size = new System.Drawing.Size(200, 20);
            this.cboNo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboNo.TabIndex = 12;
            this.cboNo.SelectedIndexChanged += new System.EventHandler(this.cboNo_SelectedIndexChanged);
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(291, 55);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(56, 23);
            this.labelX1.TabIndex = 13;
            this.labelX1.Text = "No";
            // 
            // cboVendor
            // 
            this.cboVendor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboVendor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboVendor.DisplayMember = "Text";
            this.cboVendor.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboVendor.FormattingEnabled = true;
            this.cboVendor.ItemHeight = 14;
            this.cboVendor.Location = new System.Drawing.Point(358, 35);
            this.cboVendor.Name = "cboVendor";
            this.cboVendor.Size = new System.Drawing.Size(200, 20);
            this.cboVendor.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboVendor.TabIndex = 1;
            this.cboVendor.SelectedIndexChanged += new System.EventHandler(this.cboVendor_SelectedIndexChanged);
            this.cboVendor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblOperationType
            // 
            // 
            // 
            // 
            this.lblOperationType.BackgroundStyle.Class = "";
            this.lblOperationType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblOperationType.Location = new System.Drawing.Point(291, 31);
            this.lblOperationType.Name = "lblOperationType";
            this.lblOperationType.Size = new System.Drawing.Size(56, 23);
            this.lblOperationType.TabIndex = 11;
            this.lblOperationType.Text = "Vendor";
            // 
            // lblWarehouse
            // 
            // 
            // 
            // 
            this.lblWarehouse.BackgroundStyle.Class = "";
            this.lblWarehouse.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWarehouse.Location = new System.Drawing.Point(13, 54);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(62, 23);
            this.lblWarehouse.TabIndex = 5;
            this.lblWarehouse.Text = "Warehouse";
            // 
            // CboWarehouse
            // 
            this.CboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboWarehouse.DisplayMember = "Text";
            this.CboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboWarehouse.DropDownHeight = 75;
            this.CboWarehouse.FormattingEnabled = true;
            this.CboWarehouse.IntegralHeight = false;
            this.CboWarehouse.ItemHeight = 14;
            this.CboWarehouse.Location = new System.Drawing.Point(79, 59);
            this.CboWarehouse.Name = "CboWarehouse";
            this.CboWarehouse.Size = new System.Drawing.Size(200, 20);
            this.CboWarehouse.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboWarehouse.TabIndex = 5;
            this.CboWarehouse.SelectedIndexChanged += new System.EventHandler(this.CboWarehouse_SelectedIndexChanged);
            this.CboWarehouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblTo
            // 
            // 
            // 
            // 
            this.lblTo.BackgroundStyle.Class = "";
            this.lblTo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTo.Location = new System.Drawing.Point(567, 54);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 23);
            this.lblTo.TabIndex = 9;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            // 
            // 
            // 
            this.lblFrom.BackgroundStyle.Class = "";
            this.lblFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFrom.Location = new System.Drawing.Point(567, 31);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(34, 23);
            this.lblFrom.TabIndex = 8;
            this.lblFrom.Text = "From";
            // 
            // DtpToDate
            // 
            this.DtpToDate.AutoSelectDate = true;
            // 
            // 
            // 
            this.DtpToDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpToDate.ButtonDropDown.Visible = true;
            this.DtpToDate.CustomFormat = "dd MMM yyyy";
            this.DtpToDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpToDate.IsPopupCalendarOpen = false;
            this.DtpToDate.Location = new System.Drawing.Point(607, 57);
            this.DtpToDate.LockUpdateChecked = false;
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.DtpToDate.MonthCalendar.BackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.DisplayMonth = new System.DateTime(2011, 4, 1, 0, 0, 0, 0);
            this.DtpToDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpToDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.TodayButtonVisible = true;
            this.DtpToDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpToDate.Name = "DtpToDate";
            this.DtpToDate.ShowCheckBox = true;
            this.DtpToDate.Size = new System.Drawing.Size(118, 20);
            this.DtpToDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpToDate.TabIndex = 10;
            // 
            // DtpFromDate
            // 
            this.DtpFromDate.AutoSelectDate = true;
            // 
            // 
            // 
            this.DtpFromDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpFromDate.ButtonDropDown.Visible = true;
            this.DtpFromDate.CustomFormat = "dd MMM yyyy";
            this.DtpFromDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpFromDate.IsPopupCalendarOpen = false;
            this.DtpFromDate.Location = new System.Drawing.Point(607, 34);
            this.DtpFromDate.LockUpdateChecked = false;
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.DtpFromDate.MonthCalendar.BackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.DisplayMonth = new System.DateTime(2011, 4, 1, 0, 0, 0, 0);
            this.DtpFromDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpFromDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.TodayButtonVisible = true;
            this.DtpFromDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpFromDate.Name = "DtpFromDate";
            this.DtpFromDate.ShowCheckBox = true;
            this.DtpFromDate.Size = new System.Drawing.Size(118, 20);
            this.DtpFromDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpFromDate.TabIndex = 9;
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(650, 83);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(75, 20);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 11;
            this.BtnShow.Text = "Show";
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // lblType
            // 
            // 
            // 
            // 
            this.lblType.BackgroundStyle.Class = "";
            this.lblType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblType.Location = new System.Drawing.Point(13, 78);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(40, 23);
            this.lblType.TabIndex = 2;
            this.lblType.Text = "Type";
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(13, 31);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(52, 23);
            this.lblCompany.TabIndex = 0;
            this.lblCompany.Text = "Company";
            // 
            // CboType
            // 
            this.CboType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboType.DisplayMember = "Text";
            this.CboType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboType.DropDownHeight = 75;
            this.CboType.FormattingEnabled = true;
            this.CboType.IntegralHeight = false;
            this.CboType.ItemHeight = 14;
            this.CboType.Location = new System.Drawing.Point(79, 83);
            this.CboType.Name = "CboType";
            this.CboType.Size = new System.Drawing.Size(200, 20);
            this.CboType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboType.TabIndex = 2;
            this.CboType.SelectedIndexChanged += new System.EventHandler(this.CboType_SelectedIndexChanged);
            // 
            // CboCompany
            // 
            this.CboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompany.DisplayMember = "Text";
            this.CboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCompany.DropDownHeight = 75;
            this.CboCompany.FormattingEnabled = true;
            this.CboCompany.IntegralHeight = false;
            this.CboCompany.ItemHeight = 14;
            this.CboCompany.Location = new System.Drawing.Point(79, 35);
            this.CboCompany.Name = "CboCompany";
            this.CboCompany.Size = new System.Drawing.Size(200, 20);
            this.CboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCompany.TabIndex = 0;
            this.CboCompany.SelectedIndexChanged += new System.EventHandler(this.CboCompany_SelectedIndexChanged);
            this.CboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // ReportViewerItemIssue
            // 
            this.ReportViewerItemIssue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportViewerItemIssue.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Value = null;
            reportDataSource2.Value = null;
            reportDataSource3.Value = null;
            this.ReportViewerItemIssue.LocalReport.DataSources.Add(reportDataSource1);
            this.ReportViewerItemIssue.LocalReport.DataSources.Add(reportDataSource2);
            this.ReportViewerItemIssue.LocalReport.DataSources.Add(reportDataSource3);
            this.ReportViewerItemIssue.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptRFQForm.rdlc";
            this.ReportViewerItemIssue.Location = new System.Drawing.Point(0, 118);
            this.ReportViewerItemIssue.Name = "ReportViewerItemIssue";
            this.ReportViewerItemIssue.Size = new System.Drawing.Size(948, 280);
            this.ReportViewerItemIssue.TabIndex = 5;
            // 
            // FrmRptItemIssue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 398);
            this.Controls.Add(this.ReportViewerItemIssue);
            this.Controls.Add(this.expandablePanelItemIssue);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptItemIssue";
            this.Text = "Item Issue";
            this.Load += new System.EventHandler(this.FrmRptItemIssue_Load);
            this.expandablePanelItemIssue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DtpToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFromDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expandablePanelItemIssue;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboVendor;
        private DevComponents.DotNetBar.LabelX lblOperationType;
        private DevComponents.DotNetBar.LabelX lblWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboWarehouse;
        private DevComponents.DotNetBar.LabelX lblTo;
        private DevComponents.DotNetBar.LabelX lblFrom;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpToDate;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpFromDate;
        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DevComponents.DotNetBar.LabelX lblType;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboCompany;
        private Microsoft.Reporting.WinForms.ReportViewer ReportViewerItemIssue;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboNo;
        private DevComponents.DotNetBar.LabelX labelX1;
    }
}