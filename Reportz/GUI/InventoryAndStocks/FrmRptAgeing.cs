﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Microsoft.Reporting.WinForms;
using System.IO;
using System.Drawing.Imaging;

namespace MyBooksERP
{
    public partial class FrmRptAgeing : Form
    {
        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;
        private DataTable dtCompanyHeader;

        private string MsReportPath;
        string PsReportFooter;
        private string MsMessageCaption = "Warning";
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        private string strCondition = "";
        ClsBLLRptAging MObjClsBLLRptAging;
        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;

        public FrmRptAgeing()
        {
            InitializeComponent();
            PsReportFooter = ClsCommonSettings.ReportFooter;
            MObjClsBLLRptAging = new ClsBLLRptAging();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification(); 
        }

        private void FrmRptAgeing_Load(object sender, EventArgs e)
        {
            this.LoadMessage();
            this.LoadCombos(0);
            DtpDate.Value = ClsCommonSettings.GetServerDate();
        }

        /// <summary>
        /// Loads all message
        /// </summary>
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.AgeingReport, ClsCommonSettings.ProductID);
        }

        /// <summary>
        /// Binds Datasource to combobox
        /// </summary>
        /// <param name="strText">Text Field</param>
        /// <param name="strValue">Value Field</param>
        /// <param name="datCombos">Table</param>
        /// <param name="cntrlCombo">Control</param>
        /// <param name="blnInsertNewField"></param>
        private void BindCombo(string strText, string strValue, DataTable datCombos, Control cntrlCombo, bool blnInsertNewField)
        {
            if (blnInsertNewField)
            {
                if (datCombos.Rows.Count == 0)
                {
                    datCombos = new DataTable();
                    datCombos.Columns.Add(new DataColumn(strValue, typeof(int)));
                    datCombos.Columns.Add(new DataColumn(strText, typeof(string)));
                }
                DataRow datrow = datCombos.NewRow();
                datrow[strValue] = 0;
                datrow[strText] = "ALL";
                datCombos.Rows.InsertAt(datrow, 0);
            }
            ComboBox cboCombo = (ComboBox)cntrlCombo;
            cboCombo.DataSource = null;
            cboCombo.ValueMember = strValue;
            cboCombo.DisplayMember = strText;
            cboCombo.DataSource = datCombos;
        }
        /// <summary>
        /// fills combo box
        /// </summary>
        /// <param name="intType"></param>
        /// <returns>bool</returns>
        private bool LoadCombos(int intType)
        {
            try
            {
                DataTable datCombos = new DataTable();
                if (intType == 0)
                {
                    datCombos = MObjClsBLLRptAging.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID="+ClsCommonSettings.LoginCompanyID });
                   // this.BindCombo("CompanyName", "CompanyID", datCombos, CboCompany, true);
                    CboCompany.ValueMember = "CompanyID";
                    CboCompany.DisplayMember = "CompanyName";
                    CboCompany.DataSource = datCombos;
                    if (datCombos.Rows.Count > 1)
                        CboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;
                }
                if (intType == 0)
                {
                    datCombos = MObjClsBLLRptAging.FillCombos(new string[] { "Distinct V.VendorName, V.VendorID", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID", "VendorTypeID = 1 AND VC.CompanyID="+ClsCommonSettings.LoginCompanyID }); //VendorTypeID = 1 for customer 
                    this.BindCombo("VendorName", "VendorID", datCombos, CboCustomer, true);
                }
                MObjClsLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }
        /// <summary>
        /// shows error message
        /// </summary>
        /// <param name="num">error number</param>
        /// <param name="cntrl">control</param>
        private void ShowErrorMessage(int num, Control cntrl)
        {
            MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, num, out MmessageIcon);
            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            ErrRptAgeing.SetError(cntrl, MsMessageCommon.Replace("#", "").Trim());
            TmrRptAgeing.Enabled = true;
            cntrl.Focus();
        }
        /// <summary>
        /// Validates all form controls
        /// </summary>
        /// <returns>bool</returns>
        private bool ValidateForm()
        {
            if (this.CboCompany.SelectedIndex == -1)
            {
                this.ShowErrorMessage(2401, CboCompany);
                return false;
            }
            if (this.DtpDate.Text == "")
            {
                this.ShowErrorMessage(2402, DtpDate);
                return false;
            }
            if (this.CboCustomer.SelectedIndex == -1)
            {
                this.ShowErrorMessage(2403, CboCustomer);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Clears report data
        /// </summary>
        private void ClearReport()
        {
            this.rvAgeing.Clear();
            this.rvAgeing.Reset();
            this.ErrRptAgeing.Clear();
        }
        /// <summary>
        /// Shows Ageing Report
        /// </summary>
        private void ShowAgeingReport()
        {
            int intCompany = 0;
            int intCustomer = 0;
            DateTime AgeingDate = this.DtpDate.Value;

            intCompany = this.CboCompany.SelectedValue.ToInt32();
            intCustomer = this.CboCustomer.SelectedValue.ToInt32();

            dtCompanyHeader = new DataTable();
            if (CboCompany.SelectedIndex==0)
            {
                dtCompanyHeader = MObjClsBLLRptAging.DisplayALLCompanyHeaderReport();
            }
            else
            {
                dtCompanyHeader = MObjClsBLLRptAging.DisplayCompanyHeaderReport(intCompany);
            }

            DataTable DtAgeingDetails = new DataTable();
            DtAgeingDetails = this.MObjClsBLLRptAging.GetAgeingDetails(intCompany, AgeingDate, intCustomer);//ageing details

            if (DtAgeingDetails.Rows.Count == 0)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2404, out MmessageIcon);//No data found
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            }
            else
            {
                MsReportPath = Application.StartupPath + "\\MainReports\\RptAgeing.rdlc";
                rvAgeing.LocalReport.ReportPath = MsReportPath;

                ReportParameter[] ReportParameter = new ReportParameter[4];
                ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                ReportParameter[1] = new ReportParameter("Company", Convert.ToString(CboCompany.Text.Trim()), false);
                ReportParameter[2] = new ReportParameter("Date", this.DtpDate.Value.ToString("dd MMM yyyy"), false);
                ReportParameter[3] = new ReportParameter("Customer", Convert.ToString(CboCustomer.Text.Trim()), false);

                rvAgeing.LocalReport.SetParameters(ReportParameter);
                rvAgeing.LocalReport.DataSources.Clear();
                rvAgeing.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompanyHeader));
                rvAgeing.LocalReport.DataSources.Add(new ReportDataSource("DtSetAgeing_Ageing", DtAgeingDetails));
                rvAgeing.SetDisplayMode(DisplayMode.PrintLayout);
                rvAgeing.ZoomMode = ZoomMode.Percent;
                this.Cursor = Cursors.Default;
            }
        }
        private void BtnShow_Click(object sender, EventArgs e)
        {
            if (this.ValidateForm())
            {
                this.ShowAgeingReport();
            }
        }

        private void CboCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            this.ClearReport();
        }

        private void CboCustomer_SelectionChangeCommitted(object sender, EventArgs e)
        {
            this.ClearReport();
        }

        private void DtpDate_ValueChanged(object sender, EventArgs e)
        {
            this.ClearReport();
        }
    }
}
