﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace MyBooksERP
{
    public partial class FrmRptVendorPriceHistory : Form
    {
        clsBLLRptVendorPriceHistory MobjclsBLLRptVendorPriceHistory;
        private string MsReportPath;

        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;

        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;

        DataTable datReport = null;

        public FrmRptVendorPriceHistory()
        {
            InitializeComponent();
            MobjclsBLLRptVendorPriceHistory = new clsBLLRptVendorPriceHistory();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification(); 
        }

        private void FrmRptVendorPriceHistory_Load(object sender, EventArgs e)
        {
            LoadMessage();
            LoadCombo(0);
        }

        private void LoadCombo(int intType)
        {
            DataTable datCombos = null;

            int intTypeID = rbPurchase.Checked ? 1 : 2;

            if (intType == 0 || intType == 1)
            {
                datCombos = MobjclsBLLRptVendorPriceHistory.LoadItems(intTypeID);
                BindCombo(cboItem, "ItemName", "ItemID", datCombos);
            }
            else if (intType == 2)
            {
                BindCombo(cboVendor, "VendorName", "VendorID", datReport);
            }
        }

        /// <summary>
        /// Loads all message
        /// </summary>
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.VendorPriceHistoryReport, ClsCommonSettings.ProductID);
        }

        private void BindCombo(ComboBox cboCombo, string strTextField, string strValueField, DataTable datCombos)
        {
            cboCombo.DataSource = datCombos;
            cboCombo.DisplayMember = strTextField;
            cboCombo.ValueMember = strValueField;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                btnShow.Enabled = false;
                if (ValidateReport())
                {
                    BindGrid();
                }
                TmrReport.Enabled = true;
            }
            catch
            {
                TmrReport.Enabled = true;
            }
        }

        /// <summary>
        /// Validate Report
        /// </summary>
        /// <returns></returns>
        private bool ValidateReport()
        {
            if (cboItem.SelectedIndex == -1)
            {
                ShowErrorMessage(9784, cboItem);
                return false;
            }
            else if (dtpFromDate.Checked && dtpToDate.Checked && (dtpFromDate.Value > dtpToDate.Value))
            {
                ShowErrorMessage(9785, dtpFromDate);
                return false;
            }
            return true;
        }

        private void ShowErrorMessage(int num, Control cntrl)
        {
            MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, num, out MmessageIcon);
            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            ErrReport.SetError(cntrl, MsMessageCommon.Replace("#", "").Trim());
            TmrReport.Enabled = true;
            cntrl.Focus();
        }

        private void ClearReport()
        {
            ErrReport.Clear();
            dgvReport.Rows.Clear();
            dgvReport.Columns.Clear();
            cboVendor.Enabled = false;
            btnFind.Enabled = false;
            lblMaxRate.Visible = false;
            lblMinRate.Visible = false;
            lblMaxValue.Visible = false;
            lblMinValue.Visible = false;
        }

        private void BindGrid()
        {

            int intchkFrmTo = 0;

            bool blnIsGroup = false;
            string[] arrItem = Convert.ToString(cboItem.SelectedValue).Split('@');
            int intItemID = Convert.ToInt32(arrItem[0]);
            blnIsGroup = Convert.ToBoolean(Convert.ToInt32(arrItem[1]));

            int intTypeID = rbPurchase.Checked ? 1 : 2;
            DateTime dtFromDate = dtpFromDate.Value;
            DateTime dtToDate = dtpToDate.Value;

            string PsReportFooter = ClsCommonSettings.ReportFooter;


            if (dtpFromDate.Checked && dtpToDate.Checked)
            {
                intchkFrmTo = 1;
            }
            else if (dtpFromDate.Checked && !dtpToDate.Checked)
            {
                intchkFrmTo = 2;
            }
            else if (!dtpFromDate.Checked && dtpToDate.Checked)
            {
                intchkFrmTo = 3;
            }

            datReport = MobjclsBLLRptVendorPriceHistory.GetReport(intTypeID, intItemID, blnIsGroup, dtFromDate, dtToDate, intchkFrmTo);

            if (datReport != null && datReport.Rows.Count > 0)
            {
                dgvReport.Columns.Clear();
                dgvReport.Rows.Clear();
                int iCounter = 0;

                dgvReport.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
                dgvReport.ColumnHeadersHeight = 30;
                

                dgvReport.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
                dgvReport.RowHeadersWidth = 80;

               
                decimal decMinValue = decimal.MaxValue;
                decimal decMaxValue = decimal.MinValue; 

                foreach (DataRow dr in datReport.Rows)
                {
                    datReport.Columns["Rate"].DataType = typeof(decimal);

                    decimal decRate = dr.Field<decimal>("Rate");

                    decMinValue = Math.Min(decRate, decMinValue);
                    decMaxValue = Math.Max(decRate, decMaxValue);

                     
                    dgvReport.Columns.Add(dr["VendorName"].ToString(), dr["VendorName"].ToString());
                    dgvReport.Columns[iCounter].MinimumWidth = 150;

                    if (iCounter == 0)
                        dgvReport.Rows.Add();

                    dgvReport.Rows[0].Cells[dr["VendorName"].ToString()].Value = dr["Rate"];
                    dgvReport.Rows[0].HeaderCell.Value = "Rate";
                    dgvReport.Rows[0].Cells[iCounter].Tag = dr["VendorID"].ToInt32();

                    iCounter = iCounter + 1;
                }

                if (decMinValue != decMaxValue)
                {
                    foreach (DataGridViewColumn dCol in dgvReport.Columns)
                    {
                        if (decMinValue == dgvReport[dCol.Index, 0].Value.ToDecimal())
                        {
                            dgvReport[dCol.Index, 0].Style.ForeColor = Color.Red;
                        }
                        else if (decMaxValue == dgvReport[dCol.Index, 0].Value.ToDecimal())
                        {
                            dgvReport[dCol.Index, 0].Style.ForeColor = Color.Green;
                        }
                    }
                    lblMaxValue.ForeColor = Color.Green;
                    lblMinValue.ForeColor = Color.Red;
                }
                else
                {
                    lblMaxValue.ForeColor = lblMinValue.ForeColor = Color.Black;
                }
                
                lblMaxRate.Visible = true;
                lblMinRate.Visible = true;
                lblMaxValue.Visible = true;
                lblMinValue.Visible = true;

                lblMinValue.Text = Convert.ToDecimal(decMinValue).ToString("F" + 2);
                lblMaxValue.Text = Convert.ToDecimal(decMaxValue).ToString("F" + 2); 

                LoadCombo(2);
                cboVendor.Enabled = true;
                btnFind.Enabled = true;

            }
            else
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9786, out MmessageIcon);//No data found
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            }

        }

        private void TmrReport_Tick(object sender, EventArgs e)
        {
            TmrReport.Enabled = false;
            btnShow.Enabled = true;
        }

        private void cboItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboItem.DroppedDown = false;
        }

        private void cboItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void cboItem_SelectedValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void rbPurchase_CheckedChanged(object sender, EventArgs e)
        {
            LoadCombo(1);

            if (rbPurchase.Checked)
            {
                lblVendor.Text = "Supplier";
            }
            else
                lblVendor.Text = "Customer";
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewColumn dCol in dgvReport.Columns)
            {
                if (cboVendor.SelectedValue.ToInt32() == dgvReport.Rows[0].Cells[dCol.Name].Tag.ToInt32())
                {
                    dgvReport.Focus();
                    dgvReport.CurrentCell = dgvReport[dCol.Name, 0];
                }
            }
        }

        private void cboVendor_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboVendor.DroppedDown = false;
        }

    }
}
