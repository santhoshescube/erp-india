﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Collections;
using System.Data.SqlClient;

using System.IO;
using System.Drawing.Imaging;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,27 April 2011>
Description:	<Description,, Product Report Form>
================================================
*/

namespace MyBooksERP
{
    public partial class FrmRptProducts : DevComponents.DotNetBar.Office2007Form
    {
        #region  Variable Declaration

        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;

        private string MsReportPath;
        string PsReportFooter;
        private string MsMessageCaption = "";
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        private bool MbViewPermission = true;
        private DataTable DTCompany;
        private string strCondition;
        private string strExpiryDate;
        private string strFrom;
        private string strTo;
        
        private bool MblnPrintEmailPermission = false;//To Set View Permission
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission

        private int intchkFrom;
        private int intchkTo;
        clsBLLProductReport MobjclsBLLProduct;
        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;
        DataTable DTProduct;
        #endregion //Variables Declaration

        #region Constructor
        public FrmRptProducts()
        {
            InitializeComponent();
            PsReportFooter = ClsCommonSettings.ReportFooter;
            MobjclsBLLProduct = new clsBLLProductReport();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification();
            BtnShow.Click += new EventHandler(BtnShow_Click);
        }
        #endregion //Constructr

        private void FrmRptProducts_Load(object sender, EventArgs e)
        {
            LoadMessage();
            LoadCombos(0);
            dtpExpiryDate.Value = ClsCommonSettings.GetServerDate();
            dtpFrom.Value = ClsCommonSettings.GetServerDate();
            dtpTo.Value = ClsCommonSettings.GetServerDate();
            CboType.SelectedIndex = 0;
            CboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;
        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            CboCompany.SelectedValue = 0;
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.ItemMaster, 4);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.ItemMaster, 4);
        }

        private void SetPermissions()
        {
            //clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            //if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            //{
            //    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)ModuleID.Reports, (Int32)MenuID.ReportProducts, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            //}
            //else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            MbViewPermission = (MblnAddPermission || MblnPrintEmailPermission || MblnUpdatePermission || MblnDeletePermission);
            expandablePanel1.Enabled = MbViewPermission;
        }

        # region 'LOADING COMBOBOX'

        private bool LoadCombos(int intType)
        {
            // 0 - For Loading All Combo
            // 1 - For Loading CboCompany
            // 2 - For Loading CboWarehouse
            // 3 - For Loading CboCategory
            // 4 - For Loading CboManufacturer
            // 5 - For Loading CboStatus
            // 6 - For Loading CboSubCategory
            // 7 - For Loading CboFeature
            // 8 - For Loading CboFeatureValue
            try
            {
                DataTable datCombos = new DataTable();
                DataTable dtTemp = new DataTable();
                DataRow datrow;

                //int intCompany = Convert.ToInt32(CboCompany.SelectedValue);
                //int intWareHouse = Convert.ToInt32(CboWarehouse.SelectedValue);
                //int intCategory = Convert.ToInt32(CboCategory.SelectedValue);
                //int intSubCategory = Convert.ToInt32(CboSubCategory.SelectedValue);
                //int intManufacturer = Convert.ToInt32(CboManufacturer.SelectedValue);
                //int intFeature = Convert.ToInt32(CboFeature.SelectedValue);
                //int intFeatureValue = Convert.ToInt32(CboFeatureValue.SelectedValue);

                string strFilterCondition = "";

                if (intType == 0 || intType == 1)
                {
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //{
                    datCombos = MobjclsBLLProduct.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID=" + ClsCommonSettings.LoginCompanyID });
                        datrow = datCombos.NewRow();
                        datrow["CompanyID"] = 0;
                        datrow["CompanyName"] = "ALL";
                        datCombos.Rows.InsertAt(datrow, 0);
                        CboCompany.ValueMember = "CompanyID";
                        CboCompany.DisplayMember = "CompanyName";
                        CboCompany.DataSource = datCombos;
                    //}
                    //else
                    //{
                    //    datCombos = MobjclsBLLProduct.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RptProduct);
                    //    if (datCombos.Rows.Count <= 0)
                    //        CboCompany.DataSource = null;
                    //    else
                    //    {
                    //        datrow = datCombos.NewRow();
                    //        datrow["CompanyID"] = 0;
                    //        datrow["Name"] = "ALL";
                    //        datCombos.Rows.InsertAt(datrow, 0);
                    //        CboCompany.ValueMember = "CompanyID";
                    //        CboCompany.DisplayMember = "Name";
                    //        CboCompany.DataSource = datCombos;
                    //    }
                    //}
                }
                if (intType == 0 || intType == 2)
                {
                    CboWarehouse.DataSource = null;

                    //if (Convert.ToInt32(CboCompany.SelectedValue) != 0)
                    //    datCombos = MobjclsBLLProduct.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", " (CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " Or CompanyID = (Select ParentID From CompanyMaster Where CompanyID= " + Convert.ToInt32(CboCompany.SelectedValue) + ") Or " +
                    //    " CompanyID in(Select CM2.CompanyID From CompanyMaster CM1 Inner Join CompanyMaster CM2 ON CM2.ParentID = CM1.ParentID Where CM1.CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " AND CM1.ParentID!=0) Or " +
                    //    " CompanyID in(Select CompanyID From CompanyMaster Where ParentID = " + Convert.ToInt32(CboCompany.SelectedValue) + ")) And IsActive=1" });
                    //else
                    //{
                    //    dtTemp = null;
                    //    dtTemp = (DataTable)CboCompany.DataSource;

                    //    if (dtTemp.Rows.Count > 0)
                    //    {

                    //        strFilterCondition += " (";

                    //        foreach (DataRow dr in dtTemp.Rows)
                    //            strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";

                    //        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                    //        strFilterCondition += ")"; 
                    //    }
                    //    datCombos = MobjclsBLLProduct.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", " (CompanyID in " + strFilterCondition + " Or CompanyID = (Select ParentID From CompanyMaster Where CompanyID in " + strFilterCondition + ") Or " +
                    //    " CompanyID in(Select CM2.CompanyID From CompanyMaster CM1 Inner Join CompanyMaster CM2 ON CM2.ParentID = CM1.ParentID Where CM1.CompanyID in " + strFilterCondition + " AND CM1.ParentID!=0) Or " +
                    //    " CompanyID in(Select CompanyID From CompanyMaster Where ParentID in " + strFilterCondition + ")) And IsActive=1" });
                    //}
                    strCondition = "";
                    if (CboCompany.SelectedValue.ToInt32() != 0)
                    {
                        strCondition = "WC.CompanyID = " + CboCompany.SelectedValue.ToStringCustom();
                    }
                    //datCombos = MobjclsBLLProduct.FillCombos(new string[] { "W.WarehouseID,W.WarehouseName", "InvWarehouse W INNER JOIN InvWarehouseCompanyDetails WC ON W.WarehouseID=WC.WarehouseID", strCondition });
                    datCombos = MobjclsBLLProduct.FillCombos(new string[] { "Distinct InvWarehouse.WarehouseID,WarehouseName", "" +
                                "InvWarehouse INNER JOIN InvWarehouseCompanyDetails WC ON InvWarehouse.WarehouseID=WC.WarehouseID", "" +
                                "WC.CompanyID = " + CboCompany.SelectedValue.ToInt32() + " And IsActive = 1" });

                    datrow = datCombos.NewRow();
                    datrow["WarehouseID"] = 0;
                    datrow["WarehouseName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);

                    CboWarehouse.ValueMember = "WarehouseID";
                    CboWarehouse.DisplayMember = "WarehouseName";
                    CboWarehouse.DataSource = datCombos;
                }
                if (intType == 0 || intType == 3)
                {
                    CboCategory.DataSource = null;

                  
                    datCombos = MobjclsBLLProduct.FillCombos(new string[] { " distinct C.CategoryID,C.CategoryName", " InvCategoryReference C ", "" });
                    datrow = datCombos.NewRow();
                    datrow["CategoryID"] = 0;
                    datrow["CategoryName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);

                    CboCategory.DataSource = null;
                    CboCategory.ValueMember = "CategoryID";
                    CboCategory.DisplayMember = "CategoryName";
                    CboCategory.DataSource = datCombos;
                }
                if (intType == 0 || intType == 4)
                {
                    CboManufacturer.DataSource = null;                    
                    datCombos = MobjclsBLLProduct.FillCombos(new string[] { "M.ManufactureID,M.Manufacture", " InvManufactureReference M ", "" });
                  
                    datrow = datCombos.NewRow();
                    datrow["ManufactureID"] = 0;
                    datrow["Manufacture"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);

                    CboManufacturer.DataSource = null;
                    CboManufacturer.ValueMember = "ManufactureID";
                    CboManufacturer.DisplayMember = "Manufacture";
                    CboManufacturer.DataSource = datCombos;
                }
                if (intType == 0 || intType == 5)
                {
                    CboStatus.DataSource = null;
                    
                   

                    datCombos = MobjclsBLLProduct.FillCombos(new string[] { "S.StatusID,S.Status", " CommonStatusReference S ", "OperationTypeID=" + (int)OperationType.Products });

                    datrow = datCombos.NewRow();
                    datrow["StatusID"] = 0;
                    datrow["Status"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);

                    CboStatus.DataSource = null;
                    CboStatus.ValueMember = "StatusID";
                    CboStatus.DisplayMember = "Status";
                    CboStatus.DataSource = datCombos;
                }
                if (intType == 0 || intType == 6)
                {
                    CboSubCategory.DataSource = null;                   

                    datCombos = MobjclsBLLProduct.FillCombos(new string[] { "Distinct ISC.SubCategoryID,ISC.SubCategoryName", "InvSubCategoryReference ISC ", "(CategoryID=" +CboCategory.SelectedValue.ToInt32()+" OR 0=" + CboCategory.SelectedValue.ToInt32()  + ")" });
                    datrow = datCombos.NewRow();
                    datrow["SubCategoryID"] = 0;
                    datrow["SubCategoryName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboSubCategory.DataSource = null;
                    CboSubCategory.ValueMember = "SubCategoryID";
                    CboSubCategory.DisplayMember = "SubCategoryName";
                    CboSubCategory.DataSource = datCombos;
                }
                if (intType == 0 || intType == 7)
                {
                    CboFeature.DataSource = null;
                    datCombos = MobjclsBLLProduct.FillCombos(new string[] { "FeatureID,FeatureName", "InvFeatureReference", "" });
                    datrow = datCombos.NewRow();
                    datrow["FeatureID"] = -1;
                    datrow["FeatureName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboFeature.DataSource = null;
                    CboFeature.ValueMember = "FeatureID";
                    CboFeature.DisplayMember = "FeatureName";
                    CboFeature.DataSource = datCombos;
                }
                if (intType == 0 || intType == 8)
                {
                    

                    CboFeatureValue.DataSource = null;
                    datCombos = MobjclsBLLProduct.FillCombos(new string[] { "FeatureValueID,FeatureValue", "InvFeatureValueReference", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["FeatureValueID"] = -1;
                    datrow["FeatureValue"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboFeatureValue.DataSource = null;
                    CboFeatureValue.ValueMember = "FeatureValueID";
                    CboFeatureValue.DisplayMember = "FeatureValue";
                    CboFeatureValue.DataSource = datCombos;
                }
                MObjClsLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
                return false;
            }
        }
        #endregion

        private void ShowReport()
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();

            if (CboType.SelectedIndex == 0)
            {
                MsReportPath = Application.StartupPath + "\\MainReports\\RptProducts.rdlc";
                ReportViewer1.LocalReport.ReportPath = MsReportPath;
            }
            else
            {
                MsReportPath = Application.StartupPath + "\\MainReports\\RptMISPricingHistory.rdlc";
                ReportViewer1.LocalReport.ReportPath = MsReportPath;
            }

            ReportParameter[] ReportParameter = new ReportParameter[12];

            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
            ReportParameter[1] = new ReportParameter("Manufacturer", Convert.ToString(CboManufacturer.Text.Trim()), false);
            ReportParameter[2] = new ReportParameter("Category", Convert.ToString(CboCategory.Text.Trim()), false);
            ReportParameter[3] = new ReportParameter("Status", Convert.ToString(CboStatus.Text.Trim()), false);
            ReportParameter[4] = new ReportParameter("Warehouse", Convert.ToString(CboWarehouse.Text.Trim()), false);
            ReportParameter[6] = new ReportParameter("Company", Convert.ToString(CboCompany.Text.Trim()), false);

            if (dtpExpiryDate.LockUpdateChecked == true)
            {
                strExpiryDate = Microsoft.VisualBasic.Strings.Format(dtpExpiryDate.Value.Date, "dd MMM yyyy");
                ReportParameter[5] = new ReportParameter("ExpiryDate", strExpiryDate, false);
            }
            else
            {
                strExpiryDate = "";
                ReportParameter[5] = new ReportParameter("ExpiryDate", "", false);
            }
            if (dtpFrom.LockUpdateChecked == false && dtpTo.LockUpdateChecked == false)
            {
                strFrom = "ALL";
                strTo = "ALL";
                ReportParameter[7] = new ReportParameter("FromDate", strFrom, false);
                ReportParameter[8] = new ReportParameter("ToDate", strTo, false);
            }
            else
            {
                strFrom = Microsoft.VisualBasic.Strings.Format(dtpFrom.Value.Date, "dd MMM yyyy");
                strTo = Microsoft.VisualBasic.Strings.Format(dtpTo.Value.Date, "dd MMM yyyy");

                ReportParameter[7] = new ReportParameter("FromDate", strFrom, false);
                ReportParameter[8] = new ReportParameter("ToDate", strTo, false);
            }
            ReportParameter[9] = new ReportParameter("SubCategory", Convert.ToString(CboSubCategory.Text.Trim()), false);
            ReportParameter[10] = new ReportParameter("Feature", Convert.ToString(CboFeature.Text.Trim()), false);
            ReportParameter[11] = new ReportParameter("FeatureValue", Convert.ToString(CboFeatureValue.Text.Trim()), false);

            ReportViewer1.LocalReport.SetParameters(ReportParameter);
            ReportViewer1.LocalReport.DataSources.Clear();


            DTCompany = new DataTable();

            int Company = Convert.ToInt32(CboCompany.SelectedValue);

            if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                DTCompany = MobjclsBLLProduct.DisplayALLCompanyHeaderReport();
            else
                DTCompany = MobjclsBLLProduct.DisplayCompanyHeaderReport(Company);

            ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
            ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing1);
            int intCompanyID = Convert.ToInt32(CboCompany.SelectedValue);
            int intCategoryID = Convert.ToInt32(CboCategory.SelectedValue);
            int intManufactureID = Convert.ToInt32(CboManufacturer.SelectedValue);
            int intStatusID = Convert.ToInt32(CboStatus.SelectedValue);
            int intWarehouseID = Convert.ToInt32(CboWarehouse.SelectedValue);
            int intSubCategory = Convert.ToInt32(CboSubCategory.SelectedValue);
            int intFeature = Convert.ToInt32(CboFeature.SelectedValue);
            int intFeatureValue = Convert.ToInt32(CboFeatureValue.SelectedValue);

            if (dtpFrom.LockUpdateChecked == true)
                intchkFrom = 1;
            else
                intchkFrom = 0;
            if (dtpTo.LockUpdateChecked == true)
                intchkTo = 1;
            else
                intchkTo = 0;

            string strPermittedCompany;
            DataTable DTPermComp = new DataTable();
            //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            //{
                strPermittedCompany = null;
            //}
            //else
            //{
            //    DTPermComp = MobjclsBLLProduct.GetPermittedCompany(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RptProduct);
            //    if (DTPermComp.Rows.Count <= 0)
            //        strPermittedCompany = null;
            //    else
            //        strPermittedCompany = DTPermComp.Rows[0]["IsEnabled"].ToString();
            //}
            if (Convert.ToInt32(CboType.SelectedIndex) == 0)
            {
                DTProduct = new DataTable();
                DTProduct = MobjclsBLLProduct.DisplayALLProductReport(intCompanyID, intCategoryID, intManufactureID, intStatusID, intWarehouseID, intSubCategory, strExpiryDate, strPermittedCompany,intFeature,intFeatureValue);

                if (DTProduct.Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 1648, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                }
                else
                {
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STItemMaster", DTProduct));
                    ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                    ReportViewer1.ZoomMode = ZoomMode.Percent;
                    this.Cursor = Cursors.Default;
                }
            }
            else
            {
                DataTable DTPriceHistory = new DataTable();
                DTPriceHistory = MobjclsBLLProduct.DisplayALLPriceHistory(intCompanyID, intWarehouseID, intCategoryID, intSubCategory, intManufactureID, intStatusID, intchkFrom, intchkTo, strFrom, strTo, strPermittedCompany);

                if (DTPriceHistory.Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 1648, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                }
                else
                {
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STItemMaster", DTPriceHistory));
                    ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                    ReportViewer1.ZoomMode = ZoomMode.Percent;
                    this.Cursor = Cursors.Default;
                }
            }
        }

        public void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
        }
        public void LocalReport_SubreportProcessing1(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("DtSetInventory_STItemMaster", DTProduct));
        }
        private void BtnShow_Click(object sender, EventArgs e)
        {
            try
            {
            
                ErpProductReport.Clear();
                if (ProductReportValidation())
                {
                    BtnShow.Click -= new EventHandler(BtnShow_Click);
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //{
                        BtnShow.Enabled = false;
                        ShowReport();
                    //}
                    //else
                    //{
                    //    if (ReportValidation())
                    //    {
                    //        BtnShow.Enabled = false;
                    //        ShowReport();
                    //    }
                    //}
                        Timer.Enabled = true;
                }
            }
            catch
            {
                Timer.Enabled = true;
            }
        }
        private bool ReportValidation()
        {
            if (CboCompany.DataSource == null)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9014, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                CboCompany.Focus();
                return false;
            }
            return true;
        }
        private bool ProductReportValidation()
        {
            if (CboCompany.Enabled == true)
            {
                if (CboCompany.DataSource == null || CboCompany.SelectedValue == null || CboCompany.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9100, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpProductReport.SetError(CboCompany, MsMessageCommon.Replace("#", "").Trim());
                    CboCompany.Focus();
                    return false;
                }
            }
            if (CboType.Enabled == true)
            {
                if (CboType.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9102, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpProductReport.SetError(CboType, MsMessageCommon.Replace("#", "").Trim());
                    CboType.Focus();
                    return false;
                }
            }
            if (CboStatus.Enabled == true)
            {
                if (CboStatus.DataSource == null || CboStatus.SelectedValue == null || CboStatus.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9103, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpProductReport.SetError(CboStatus, MsMessageCommon.Replace("#", "").Trim());
                    CboStatus.Focus();
                    return false;
                }
            }
            if (CboWarehouse.Enabled == true)
            {
                if (CboWarehouse.DataSource == null || CboWarehouse.SelectedValue == null || CboWarehouse.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9105, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpProductReport.SetError(CboWarehouse, MsMessageCommon.Replace("#", "").Trim());
                    CboWarehouse.Focus();
                    return false;
                }
            }
            if (CboManufacturer.Enabled == true)
            {
                if (CboManufacturer.DataSource == null || CboManufacturer.SelectedValue == null || CboManufacturer.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9111, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpProductReport.SetError(CboManufacturer, MsMessageCommon.Replace("#", "").Trim());
                    CboManufacturer.Focus();
                    return false;
                }
            }
            if (CboCategory.Enabled == true)
            {
                if (CboCategory.DataSource == null || CboCategory.SelectedValue == null || CboCategory.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9110, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpProductReport.SetError(CboCategory, MsMessageCommon.Replace("#", "").Trim());
                    CboCategory.Focus();
                    return false;
                }
            }
            if (CboSubCategory.Enabled == true)
            {
                if (CboSubCategory.DataSource == null || CboSubCategory.SelectedValue == null || CboSubCategory.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9119, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpProductReport.SetError(CboSubCategory, MsMessageCommon.Replace("#", "").Trim());
                    CboSubCategory.Focus();
                    return false;
                }
            }
            return true;
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            ErpProductReport.Clear();
            Timer.Enabled = false;
            BtnShow.Enabled = true;
            BtnShow.Click += new EventHandler(BtnShow_Click);
        }

        private void CboWarehouse_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpProductReport.Clear();
        }

        private void CboCategory_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpProductReport.Clear();

            //LoadCombos(4);
            //LoadCombos(5);
            LoadCombos(6);
        }

        private void CboManufacturer_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpProductReport.Clear();

            //LoadCombos(5);
            //LoadCombos(6);
        }

        private void CboStatus_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpProductReport.Clear();
        }

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void CboExpiryDate_ValueChanged(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpProductReport.Clear();
        }

        private void CboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadCombos(2);
                //LoadCombos(3);
                //LoadCombos(4);
                //LoadCombos(5);
                //LoadCombos(6);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CboCompany_SelectedIndexChanged() " + ex.Message);

                MObjClsLogWriter.WriteLog("Error in CboCompany_SelectedIndexChanged() " + ex.Message, 2);
            }
        }

        private void CboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErpProductReport.Clear();
            if (CboType.SelectedIndex == 0)
            {
                lblFrom.Visible = false;
                lblTo.Visible = false;
                dtpFrom.Visible = false;
                dtpTo.Visible = false;
                dtpFrom.LockUpdateChecked = false;
                dtpTo.LockUpdateChecked = false;
                //lblExpiryDate.Visible = true;
                //dtpExpiryDate.Visible = true;
                BtnShow.Location = (new Point(670, 60));
                lblWarehouse.Enabled = true;
                CboWarehouse.Enabled = true;
                lblFeature.Visible = false;
                lblFeatureValue.Visible = false;
                CboFeature.Visible = false;
                CboFeatureValue.Visible = false;
            }
            else
            {
                //lblExpiryDate.Visible = false;
                //dtpExpiryDate.Visible = false;
                //dtpExpiryDate.LockUpdateChecked = false;
                lblFrom.Visible = true;
                lblTo.Visible = true;
                dtpFrom.Visible = true;
                dtpTo.Visible = true;
                lblFrom.Location = new Point(643, 33);
                lblTo.Location = new Point(643, 57);
                dtpFrom.Location = new Point(690, 34);
                dtpTo.Location = new Point(690, 60);
                BtnShow.Location = (new Point(850, 60));
                lblWarehouse.Enabled = false;
                LoadCombos(2);
                CboWarehouse.Enabled = false;
                lblFeature.Visible = false;
                lblFeatureValue.Visible = false;
                CboFeature.Visible = false;
                CboFeatureValue.Visible = false;
            }
        }

        private void dtpFrom_ValueChanged(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpProductReport.Clear();
        }

        private void dtpTo_ValueChanged(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpProductReport.Clear();
        }

        private void CboCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset(); 
            ErpProductReport.Clear();
        }

        private void CboSubCategory_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpProductReport.Clear();
            
            //LoadCombos(5);
            //LoadCombos(4);
        }

        private void CboWarehouse_SelectedValueChanged(object sender, EventArgs e)
        {
            //LoadCombos(3);
            //LoadCombos(4);
            //LoadCombos(5);
            //LoadCombos(6);
        }

        private void expandablePanel1_Click(object sender, EventArgs e)
        {

        }

        private void CboFeature_SelectedIndexChanged(object sender, EventArgs e)
        {
           // LoadCombos(8);
        }

        private void CboFeature_KeyDown(object sender, KeyEventArgs e)
        {
            CboFeature.DroppedDown = false;
        }

        private void CboFeatureValue_KeyDown(object sender, KeyEventArgs e)
        {
            CboFeatureValue.DroppedDown = false;
        }

        private void expandablePanel1_ExpandedChanged(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {
            lblFeature.Visible = false;
            lblFeatureValue.Visible = false;
            CboFeature.Visible = false;
            CboFeatureValue.Visible = false;
          
            if (CboType.SelectedIndex == 0)
            {
                lblFrom.Visible = false;
                lblTo.Visible = false;
                dtpFrom.Visible = false;
                dtpTo.Visible = false;
                dtpFrom.LockUpdateChecked = false;
                dtpTo.LockUpdateChecked = false;
                //lblExpiryDate.Visible = true;
                //dtpExpiryDate.Visible = true;
                //BtnShow.Location = (new Point(1102, 35));
                lblWarehouse.Enabled = true;
                CboWarehouse.Enabled = true;
                lblFeature.Visible = false;
                lblFeatureValue.Visible = false;
                CboFeature.Visible = false;
                CboFeatureValue.Visible = false;
            }
            else
            {
                //lblExpiryDate.Visible = false;
                //dtpExpiryDate.Visible = false;
                //dtpExpiryDate.LockUpdateChecked = false;
                lblFrom.Visible = true;
                lblTo.Visible = true;
                dtpFrom.Visible = true;
                dtpTo.Visible = true;
                lblFrom.Location = new Point(643, 33);
                lblTo.Location = new Point(643, 57);
                dtpFrom.Location = new Point(690, 34);
                dtpTo.Location = new Point(690, 60);
                //BtnShow.Location = (new Point(850, 60));
                lblWarehouse.Enabled = false;
                LoadCombos(2);
                CboWarehouse.Enabled = false;
                lblFeature.Visible = false;
                lblFeatureValue.Visible = false;
                CboFeature.Visible = false;
                CboFeatureValue.Visible = false;
            }
        }
    }
}