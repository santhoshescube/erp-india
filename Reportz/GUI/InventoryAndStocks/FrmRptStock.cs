﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Collections;
using System.Data.SqlClient;

using System.IO;
using System.Drawing.Imaging;
/* 
=================================================
Author:		    <Author,,Sawmya>
Create date:    <Create Date,18 May 2011>
Description:	<Description, Stock Report Form>
Modified By:	<Author,,Amal Raj>
Modified date:  <Modified Date,18 May 2011>
Description:	<Description, Added New Searching via Date>
================================================
*/
namespace MyBooksERP
{
    public partial class FrmRptStock : DevComponents.DotNetBar.Office2007Form 
    {
        #region  Variable Declaration
        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;
        private string MsReportPath;
        string  PsReportFooter;
        private string MsMessageCaption = "";
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        private bool MbViewPermission = true;
        private bool MblnPrintEmailPermission = false;//To Set View Permission
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private DataTable DTCompany;
        private DataTable DT;
        private int Company;
        private int Category;
        private int SubCategory;
        private int ItemName;
        private string strBatch;
        private int Warehouse;
        private string strCondition ="";
        //private string strFrom;
        //private string strTo;

        ClsBLLMISStockReport MobjClsBLLMISStock;
        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;

        #endregion //Variables Declaration

        #region Constructor

        public FrmRptStock()
        {
            InitializeComponent();
            PsReportFooter = ClsCommonSettings.ReportFooter;
            MobjClsBLLMISStock = new ClsBLLMISStockReport();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification();
           // BtnShow.Click += new EventHandler(BtnShow_Click);
        }
        #endregion //Constructor

        private void FrmRptStock_Load(object sender, EventArgs e)
        {
          //  SetPermissions();
            LoadMessage();
            LoadCombos(0);
            CboStockType.SelectedIndex = 0;
            CboCompany.SelectedValue = ClsCommonSettings.CompanyID;
            CboBatch.Enabled = false;
        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.StockVerification, ClsCommonSettings.ProductID);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.StockVerification, ClsCommonSettings.ProductID);
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettingsReports objClsBLLPermissionSettings = new clsBLLPermissionSettingsReports();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Reports, (Int32)eMenuID.StockReports, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            MbViewPermission = (MblnAddPermission || MblnPrintEmailPermission || MblnUpdatePermission || MblnDeletePermission);
            expandablePanel1.Enabled = MbViewPermission;
        }

        private bool LoadCombos(int intType)
        {
            // 0 - For Loading All Combo
            // 1 - For Loading CboCategory
            // 2 - For Loading CboManufacturer
            // 3 - For Loading CboStatus

            int intCompany = Convert.ToInt32(CboCompany.SelectedValue);
            int intWareHouse = Convert.ToInt32(CboWarehouse.SelectedValue);
            int intCategory = Convert.ToInt32(CboCategory.SelectedValue);
            int intSubCategory = Convert.ToInt32(CboSubCategory.SelectedValue);
            int intItem = Convert.ToInt32(CboItem.SelectedValue);
            int intBatchNo = Convert.ToInt32(CboBatch.SelectedValue);

            try
            {
                DataTable datCombos = new DataTable();
                DataRow datrow;
                if (intType == 0 || intType == 1)
                {
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //{
                        datCombos = MobjClsBLLMISStock.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID="+ClsCommonSettings.LoginCompanyID });
                    //}
                    //else
                    //{
                    //    datCombos = MobjClsBLLMISStock.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RptStock);
                    //}
                    
                    //datrow["CompanyName"] = "ALL";
                    //datCombos.Rows.InsertAt(datrow, 0);
                    if (datCombos.Rows.Count <= 0)
                    {
                        CboCompany.DataSource = null;
                        CboWarehouse.DataSource = null;
                        CboCategory.DataSource = null;
                        CboSubCategory.DataSource = null;
                        CboItem.DataSource = null;
                        CboBatch.DataSource = null;
                        CboStockType.DataSource = null;
                    }
                    else
                    {
                        CboCompany.ValueMember = "CompanyID";
                        CboCompany.DisplayMember = "CompanyName";
                        CboCompany.DataSource = datCombos;
                    }
                }
                if (CboCompany.DataSource != null)
                {
                    if (CboCompany.SelectedValue.ToInt32() == 0)
                    {
                        if (intType == 0 || intType == 2)
                        {
                            datCombos = MobjClsBLLMISStock.FillCombos(new string[] { "Distinct InvWarehouse.WarehouseID,WarehouseName", "" +
                                "InvWarehouse INNER JOIN InvWarehouseCompanyDetails WC ON InvWarehouse.WarehouseID=WC.WarehouseID", "" +
                                "WC.CompanyID = " + CboCompany.SelectedValue.ToInt32() + " And IsActive = 1" });
                            datrow = datCombos.NewRow();
                            datrow["WarehouseID"] = 0;
                            datrow["WarehouseName"] = "ALL";
                            datCombos.Rows.InsertAt(datrow, 0);
                            CboWarehouse.DataSource = null;
                            CboWarehouse.ValueMember = "WarehouseID";
                            CboWarehouse.DisplayMember = "WarehouseName";
                            CboWarehouse.DataSource = datCombos;
                        }
                    }
                    if (intType == 0 || intType == 3)
                    {
                        datCombos = MobjClsBLLMISStock.FillCombos(new string[] { " Distinct CategoryID,CategoryName", " InvCategoryReference ", "" });
                        datrow = datCombos.NewRow();
                        datrow["CategoryID"] = 0;
                        datrow["CategoryName"] = "ALL";
                        datCombos.Rows.InsertAt(datrow, 0);
                        CboCategory.DataSource = null;
                        CboCategory.ValueMember = "CategoryID";
                        CboCategory.DisplayMember = "CategoryName";
                        CboCategory.DataSource = datCombos;
                    }

                    if (intType == 0 || intType == 4)
                    {
                        datCombos = MobjClsBLLMISStock.FillCombos(new string[] { " Distinct SubCategoryID,SubCategoryName", "InvSubCategoryReference", "" });
                        datrow = datCombos.NewRow();
                        datrow["SubCategoryID"] = 0;
                        datrow["SubCategoryName"] = "ALL";
                        datCombos.Rows.InsertAt(datrow, 0);
                        CboSubCategory.DataSource = null;
                        CboSubCategory.ValueMember = "SubCategoryID";
                        CboSubCategory.DisplayMember = "SubCategoryName";
                        CboSubCategory.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 5)
                    {
                        strCondition = " SD.CompanyID=" + CboCompany.SelectedValue.ToInt32();
                        //datCombos = MobjClsBLLMISStock.FillCombos(new string[] { "ItemID,ItemName+'-'+Code as Description", "InvItemMaster", strCondition });
                        datCombos = MobjClsBLLMISStock.FillCombos(new string[] { " Distinct IM.ItemID,IM.ItemName+'-'+IM.Code as Description", "InvItemMaster IM inner join  InvCategoryReference IMC on IMC.CategoryID = IM.CategoryID inner join  InvSubCategoryReference ISC on ISC.SubCategoryID = IM.SubCategoryID inner join  InvItemStockMaster SM on SM.ItemID = IM.ItemID inner join  InvItemStockDetails SD on SD.ItemID = IM.ItemID", strCondition });
                        datrow = datCombos.NewRow();
                        datrow["ItemID"] = 0;
                        datrow["Description"] = "ALL";
                        datCombos.Rows.InsertAt(datrow, 0);
                        CboItem.DataSource = null;
                        CboItem.ValueMember = "ItemID";
                        CboItem.DisplayMember = "Description";
                        CboItem.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 6)
                    {
                        datCombos = MobjClsBLLMISStock.FillCombos(new string[] { "BatchID,BatchNo", "InvBatchDetails", "" });
                        datrow = datCombos.NewRow();
                        datrow["BatchID"] = 0;
                        datrow["BatchNo"] = "ALL";
                        datCombos.Rows.InsertAt(datrow, 0);
                        CboBatch.DataSource = null;
                        CboBatch.ValueMember = "BatchID";
                        CboBatch.DisplayMember = "BatchNo";
                        CboBatch.DataSource = datCombos;
                    }
                    if (Convert.ToInt32(CboItem.SelectedValue) > 0)
                    {
                        strCondition = "ItemID = " + Convert.ToInt32(CboItem.SelectedValue);
                        if (intType == 7)
                        {
                            datCombos = MobjClsBLLMISStock.FillCombos(new string[] { "BatchID,BatchNo", "InvBatchDetails", strCondition });
                            datrow = datCombos.NewRow();
                            datrow["BatchID"] = 0;
                            datrow["BatchNo"] = "ALL";
                            datCombos.Rows.InsertAt(datrow, 0);
                            CboBatch.DataSource = null;
                            CboBatch.ValueMember = "BatchID";
                            CboBatch.DisplayMember = "BatchNo";
                            CboBatch.DataSource = datCombos;
                        }
                    }
                    if (intType == 8) //Warehouse according to Company
                    {
                        strCondition = "";
                        datCombos = MobjClsBLLMISStock.FillCombos(new string[] { "Distinct InvWarehouse.WarehouseID,WarehouseName", "" +
                            "InvWarehouse INNER JOIN InvWarehouseCompanyDetails WC ON InvWarehouse.WarehouseID=WC.WarehouseID", "" +
                            "WC.CompanyID = " + CboCompany.SelectedValue.ToInt32() + " And IsActive = 1" });
                        datrow = datCombos.NewRow();
                        datrow["WarehouseID"] = 0;
                        datrow["WarehouseName"] = "ALL";
                        datCombos.Rows.InsertAt(datrow, 0);
                        CboWarehouse.DataSource = null;
                        CboWarehouse.ValueMember = "WarehouseID";
                        CboWarehouse.DisplayMember = "WarehouseName";
                        CboWarehouse.DataSource = datCombos;
                    }
                    if (intType == 9)
                    {
                        strCondition = "";
                        if (intCompany != 0) strCondition = " SD.CompanyID=" + intCompany + " AND";
                        if (intWareHouse != 0) strCondition = strCondition + " SD.WarehouseID=" + intWareHouse + " AND";
                        if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                        datCombos = MobjClsBLLMISStock.FillCombos(new string[] { " distinct C.CategoryID,C.CategoryName", " InvCategoryReference C inner  join  InvItemMaster IM on IM.CategoryID = C.CategoryID inner join  InvItemStockMaster SM on SM.ItemID = IM.ItemID inner join InvItemStockDetails SD on SD.ItemID = IM.ItemID ", strCondition });
                        datrow = datCombos.NewRow();
                        datrow["CategoryID"] = 0;
                        datrow["CategoryName"] = "ALL";
                        datCombos.Rows.InsertAt(datrow, 0);
                        CboCategory.DataSource = null;
                        CboCategory.ValueMember = "CategoryID";
                        CboCategory.DisplayMember = "CategoryName";
                        CboCategory.DataSource = datCombos;
                    }
                    if (intType == 10)
                    {
                        strCondition = "";
                        if (intCompany != 0) strCondition = " SD.CompanyID=" + intCompany + " AND";
                        if (intWareHouse != 0) strCondition = strCondition + " SD.WarehouseID=" + intWareHouse + " AND";
                        if (intCategory != 0) strCondition = strCondition + " IM.CategoryID=" + intCategory + " AND";
                        if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                        datCombos = MobjClsBLLMISStock.FillCombos(new string[] { "Distinct ISC.SubCategoryID,ISC.SubCategoryName", "InvSubCategoryReference ISC inner join InvCategoryReference IMC On IMC.CategoryID = ISC.CategoryID inner  join  InvItemMaster IM on IM.CategoryID = IMC.CategoryID inner join  InvItemStockMaster SM on SM.ItemID = IM.ItemID inner join InvItemStockDetails SD on SD.ItemID = IM.ItemID ", strCondition });
                        datrow = datCombos.NewRow();
                        datrow["SubCategoryID"] = 0;
                        datrow["SubCategoryName"] = "ALL";
                        datCombos.Rows.InsertAt(datrow, 0);
                        CboSubCategory.DataSource = null;
                        CboSubCategory.ValueMember = "SubCategoryID";
                        CboSubCategory.DisplayMember = "SubCategoryName";
                        CboSubCategory.DataSource = datCombos;
                    }
                    if (intType == 11)
                    {
                        strCondition = "";
                        if (intCompany != 0) strCondition = " SD.CompanyID=" + intCompany + " AND";
                        if (intWareHouse != 0) strCondition = strCondition + " SD.WarehouseID=" + intWareHouse + " AND";
                        if (intCategory != 0) strCondition = strCondition + " IM.CategoryID=" + intCategory + " AND";
                        if (intSubCategory != 0) strCondition = strCondition + " IM.SubCategoryID=" + intSubCategory + " AND";
                        if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                        datCombos = MobjClsBLLMISStock.FillCombos(new string[] { " Distinct IM.ItemID,IM.ItemName+'-'+IM.Code as Description", "InvItemMaster IM inner join  InvCategoryReference IMC on IMC.CategoryID = IM.CategoryID inner join  InvSubCategoryReference ISC on ISC.SubCategoryID = IM.SubCategoryID inner join  InvItemStockMaster SM on SM.ItemID = IM.ItemID inner join  InvItemStockDetails SD on SD.ItemID = IM.ItemID", strCondition });
                        datrow = datCombos.NewRow();
                        datrow["ItemID"] = 0;
                        datrow["Description"] = "ALL";
                        datCombos.Rows.InsertAt(datrow, 0);
                        CboItem.DataSource = null;
                        CboItem.ValueMember = "ItemID";
                        CboItem.DisplayMember = "Description";
                        CboItem.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 12)
                    {
                        datCombos = new DataTable();
                        datCombos.Columns.Add("StockTypeID");
                        datCombos.Columns.Add("StockType");
                        datrow = datCombos.NewRow();
                        datrow["StockTypeID"] = 1;
                        datrow["StockType"] = "Zero Stock";
                        datCombos.Rows.InsertAt(datrow, 0);                        
                        
                        datrow = datCombos.NewRow();
                        datrow["StockTypeID"] = 2;
                        datrow["StockType"] = "Reorder Level";
                        datCombos.Rows.InsertAt(datrow, 0);                        
                        
                        if (ClsCommonSettings.PblnIsLocationWise)
                        {
                            datrow = datCombos.NewRow();
                            datrow["StockTypeID"] = 3;
                            datrow["StockType"] = "Location Wise Stock";
                            datCombos.Rows.InsertAt(datrow, 0);
                        }                        
                        
                        datrow = datCombos.NewRow();
                        datrow["StockTypeID"] = 0;
                        datrow["StockType"] = "Normal Stock";
                        datCombos.Rows.InsertAt(datrow, 0);

                        CboStockType.DataSource = null;
                        CboStockType.ValueMember = "StockTypeID";
                        CboStockType.DisplayMember = "StockType";
                        CboStockType.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 13)
                    {
                        strCondition = " SD.CompanyID=" + CboCompany.SelectedValue.ToInt32();
                        //datCombos = MobjClsBLLMISStock.FillCombos(new string[] { "ItemID,ItemName+'-'+Code as Description", "InvItemMaster", strCondition });
                        datCombos = MobjClsBLLMISStock.FillCombos(new string[] { " Distinct IM.ItemID,IM.Code", "InvItemMaster IM inner join  InvCategoryReference IMC on IMC.CategoryID = IM.CategoryID inner join  InvSubCategoryReference ISC on ISC.SubCategoryID = IM.SubCategoryID inner join  InvItemStockMaster SM on SM.ItemID = IM.ItemID inner join  InvItemStockDetails SD on SD.ItemID = IM.ItemID", strCondition });
                        datrow = datCombos.NewRow();
                        datrow["ItemID"] = 0;
                        datrow["Code"] = "ALL";
                        datCombos.Rows.InsertAt(datrow, 0);
                        cboItemCode.DataSource = null;
                        cboItemCode.ValueMember = "ItemID";
                        cboItemCode.DisplayMember = "Code";
                        cboItemCode.DataSource = datCombos;
                    }

                }
                MObjClsLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private void ShowReport()
        {
            Company = Convert.ToInt32(CboCompany.SelectedValue);
            SubCategory = Convert.ToInt32(CboSubCategory.SelectedValue);
            ItemName = Convert.ToInt32(CboItem.SelectedValue);
            strBatch = Convert.ToString(CboBatch.Text.Trim());
            Warehouse = Convert.ToInt32(CboWarehouse.SelectedValue);
            Category = Convert.ToInt32(CboCategory.SelectedValue);

            ReportViewer1.Clear();
            ReportViewer1.Reset();
            if (CboStockType.SelectedIndex == 3)
            {
                MsReportPath = Application.StartupPath + "\\MainReports\\RptLocationWiseStock.rdlc";
                ReportViewer1.LocalReport.ReportPath = MsReportPath;
            }
            else
            {
                MsReportPath = Application.StartupPath + "\\MainReports\\RptMISStock.rdlc";
                ReportViewer1.LocalReport.ReportPath = MsReportPath;
            }

            ReportParameter[] ReportParameter = new ReportParameter[7];
            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
            ReportParameter[1] = new ReportParameter("Company", Convert.ToString(CboCompany.Text.Trim()), false);
            ReportParameter[2] = new ReportParameter("Warehouse", Convert.ToString(CboWarehouse.Text.Trim()), false);
            ReportParameter[3] = new ReportParameter("Category", Convert.ToString(CboCategory.Text.Trim()), false);
            ReportParameter[4] = new ReportParameter("SubCategory", Convert.ToString(CboSubCategory.Text.Trim()), false);
            ReportParameter[5] = new ReportParameter("Item", Convert.ToString(CboItem.Text.Trim()), false);
            ReportParameter[6] = new ReportParameter("BatchNo", Convert.ToString(CboBatch.Text.Trim()), false);
            try
            {
                ReportViewer1.LocalReport.SetParameters(ReportParameter);
            }
            catch (Exception)
            {
            }
            ReportViewer1.LocalReport.DataSources.Clear();

            DTCompany = new DataTable();
            DTCompany = MobjClsBLLMISStock.DisplayALLCompanyHeaderReport();
            ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
            //ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);     
            DTCompany = new DataTable();

            if (CboCompany.Text.Trim() == "ALL")
            {
                DTCompany = MobjClsBLLMISStock.DisplayALLCompanyHeaderReport();
            }
            else
            {
                DTCompany = MobjClsBLLMISStock.DisplayCompanyHeaderReport(Company);
            }
            if (CboStockType.SelectedIndex == 2)
            {
                DT = MobjClsBLLMISStock.DisplayZeroStock(Company, Warehouse, Category, SubCategory, ItemName, strBatch);
                if (DT.Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 6058, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                }
                else
                {
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STStockDetails", DT));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STLocationwiseStock", ""));
                    ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                    ReportViewer1.ZoomMode = ZoomMode.Percent;
                    this.Cursor = Cursors.Default;
                }
            }

            if (CboStockType.SelectedIndex == 0)
            {
                //DT = MobjClsBLLMISStock.DisplayStockReport(Company, Warehouse, strFrom, strTo, chk, Category, SubCategory, ItemName, Batch);
                DT = MobjClsBLLMISStock.DisplayNormalStockReport(Company, Warehouse, Category, SubCategory, ItemName, strBatch);
                if (DT.Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 6058, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                }
                else
                {
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STStockDetails", DT));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STLocationwiseStock", ""));
                    ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                    ReportViewer1.ZoomMode = ZoomMode.Percent;
                    this.Cursor = Cursors.Default;
                }
            }

            if (CboStockType.SelectedIndex == 1)
            {
                //DT = MobjClsBLLMISStock.DisplayStockReport(Company, Warehouse, strFrom, strTo, chk, Category, SubCategory, ItemName, Batch);
                if (CboWarehouse.SelectedIndex == 0)
                {
                    DT = MobjClsBLLMISStock.DisplayAllReorderLevelStockReport(Company, Warehouse, Category, SubCategory, ItemName, strBatch);
                }
                else
                {
                    DT = MobjClsBLLMISStock.DisplayReorderLevelStockReport(Company, Warehouse, Category, SubCategory, ItemName, strBatch);
                }
                if (DT.Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 6058, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                }
                else
                {
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STStockDetails", DT));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STLocationwiseStock", ""));
                    ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                    ReportViewer1.ZoomMode = ZoomMode.Percent;
                    this.Cursor = Cursors.Default;
                }
            }
            if (CboStockType.SelectedIndex == 3)
            {
                DT = MobjClsBLLMISStock.DisplayLocationwiseStockReport(Company, Warehouse, Category, SubCategory, ItemName, strBatch);

                if (DT.Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 6058, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                }
                else
                {
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STLocationwiseStock", DT));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STStockDetails", ""));
                    ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                    ReportViewer1.ZoomMode = ZoomMode.Percent;
                    this.Cursor = Cursors.Default;
                }
            }
        }

        public void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            try
            {
                ReportViewer1.Clear();
                ReportViewer1.Reset();
                if (StockReportValidation())
                {
                    BtnShow.Click -= new EventHandler(BtnShow_Click);
                    if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    {
                        BtnShow.Enabled = false;
                        ShowReport();
                    }
                    else
                    {
                        if (ReportValidation())
                        {
                            BtnShow.Enabled = false;
                            ShowReport();
                        }
                    }
                    Timer.Enabled = true;
                }
            }
            catch (Exception)
            {
                Timer.Enabled = true;
            }
        }
        private bool ReportValidation()
        {
            if (CboCompany.DataSource == null)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9014, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                CboCompany.Focus();
                return false;
            }
            return true;
        }
        private bool StockReportValidation()
        {
            if (CboCompany.Enabled == true)
            {
                if (CboCompany.DataSource == null || CboCompany.SelectedValue == null || CboCompany.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9100, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpStockReport.SetError(CboCompany, MsMessageCommon.Replace("#", "").Trim());
                    CboCompany.Focus();
                    return false;
                }
            }
            if (CboWarehouse.Enabled == true)
            {
                if (CboWarehouse.DataSource == null || CboWarehouse.SelectedValue == null || CboWarehouse.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9105, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpStockReport.SetError(CboWarehouse, MsMessageCommon.Replace("#", "").Trim());
                    CboWarehouse.Focus();
                    return false;
                }
            }
            if (CboCategory.Enabled == true)
            {
                if (CboCategory.DataSource == null || CboCategory.SelectedValue == null || CboCategory.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9110, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpStockReport.SetError(CboCategory, MsMessageCommon.Replace("#", "").Trim());
                    CboCategory.Focus();
                    return false;
                }
            }
            if (CboSubCategory.Enabled == true)
            {
                if (CboSubCategory.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9119, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpStockReport.SetError(CboSubCategory, MsMessageCommon.Replace("#", "").Trim());
                    CboSubCategory.Focus();
                    return false;
                }
            }
            if (CboItem.Enabled == true)
            {
                if (CboItem.DataSource == null || CboItem.SelectedValue == null || CboItem.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9120, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpStockReport.SetError(CboItem, MsMessageCommon.Replace("#", "").Trim());
                    CboItem.Focus();
                    return false;
                }
            }
            if (CboBatch.Enabled == true)
            {
                if (CboBatch.DataSource == null || CboBatch.SelectedValue == null || CboBatch.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9121, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpStockReport.SetError(CboBatch, MsMessageCommon.Replace("#", "").Trim());
                    CboBatch.Focus();
                    return false;
                }
            }
            if (CboStockType.Enabled == true)
            {
                if (CboStockType.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9122, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpStockReport.SetError(CboStockType, MsMessageCommon.Replace("#", "").Trim());
                    CboStockType.Focus();
                    return false;
                }
            }

            return true;
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            ErpStockReport.Clear();
            Timer.Enabled = false;
            BtnShow.Enabled = true;
            BtnShow.Click += new EventHandler(BtnShow_Click);
        }

        private void CboCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            LoadCombos(8);
            LoadCombos(6);
            LoadCombos(9);
            LoadCombos(10);
            LoadCombos(11);
            ErpStockReport.Clear();
        }

        private void CboWarehouse_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpStockReport.Clear();
           // if (Convert.ToInt32(CboWarehouse.SelectedValue) != 0)
           // {
                LoadCombos(6);
                LoadCombos(9);
                LoadCombos(10);
                LoadCombos(11);
           // }
            //else
            //{
            //    LoadCombos(0);
            //}
        }
        private void CboCategory_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpStockReport.Clear();
            //if (Convert.ToInt32(CboCategory.SelectedValue) != 0)
           // {
                LoadCombos(6);
                LoadCombos(10);
                LoadCombos(11);
           // }
            //else
            //{
            //    LoadCombos(0);
            //}
        }
        private void CboSubCategory_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpStockReport.Clear();
            //if (Convert.ToInt32(CboSubCategory.SelectedValue) != 0)
            //{
                LoadCombos(6);
                LoadCombos(11);
           // }
            //else
            //{
            //    LoadCombos(0);
            //}
        }
        private void CboItem_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpStockReport.Clear();
           // if (Convert.ToInt32(CboItem.SelectedValue) != 0)
          //  {
                LoadCombos(7);
                CboBatch.Enabled = true;
           // }
            //else
            //{
            //    LoadCombos(0);
            //    CboBatch.Enabled = false;
            //}
        }
        private void CboBatch_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpStockReport.Clear();
            //if (Convert.ToInt32(CboBatch.SelectedValue) == 0)
            //{
            //    LoadCombos(0);
            //}
        }
        private void CboStockType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpStockReport.Clear();
        }

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
            if (e.KeyCode == Keys.Enter)
            {
                string strComboName = cbo.Name;
                switch (strComboName)
                {
                    case "CboCompany":
                        CboCompany_SelectionChangeCommitted(sender, new EventArgs());
                        break;
                    case "CboWarehouse":
                        CboWarehouse_SelectionChangeCommitted(sender, new EventArgs());
                        break;
                    case "CboCategory":
                        CboCategory_SelectionChangeCommitted(sender, new EventArgs());
                        break;
                    case "CboSubCategory":
                        CboSubCategory_SelectionChangeCommitted(sender, new EventArgs());
                        break;
                    case "CboItem":
                        CboItem_SelectionChangeCommitted(sender, new EventArgs());
                        break;
                    case "CboBatch":
                        CboBatch_SelectionChangeCommitted(sender, new EventArgs());
                        break;
                    case "CboStockType":
                        CboStockType_SelectionChangeCommitted(sender, new EventArgs());
                        break;

                    case "cboItemCode":
                        cboItemCode_SelectionChangeCommitted(sender, new EventArgs());
                        break;
                    default:
                        break;
                }
            }
        }

        private void CboWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErpStockReport.Clear();
        }

        private void CboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void CboCompany_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            if (Convert.ToInt32(CboCompany.SelectedValue) != 0)
            {
                LoadCombos(8);
                // LoadCombos(9);
            }
            else
            {
                LoadCombos(0);
            }
        }
        
        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpStockReport.Clear();
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpStockReport.Clear();
        }

        private void CboItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErpStockReport.Clear();
            if (Convert.ToInt32(CboItem.SelectedValue) != 0)
                CboBatch.Enabled = true;
            else
                CboBatch.Enabled = false;
        }

        private void cboItemCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboItemCode.Items.Count > 0)
            {
                if (cboItemCode.SelectedValue.ToInt32() > 0)
                    CboItem.SelectedValue = cboItemCode.SelectedValue;
            }
        }

        private void cboItemCode_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpStockReport.Clear();
            // if (Convert.ToInt32(CboItem.SelectedValue) != 0)
            //  {
            LoadCombos(7);
            CboBatch.Enabled = true;
        }

        

        
     }
}
