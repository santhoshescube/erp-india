﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MyBooksERP
{
    public partial class FrmRptExpense : Form
    {

        private bool MblnPrintEmailPermission = false;          //To set Print Email Permission
        private bool MblnAddPermission = false;                 //To set Add Permission
        private bool MblnUpdatePermission = false;              //To set Update Permission
        private bool MblnDeletePermission = false;              //To set Delete Permission
        private bool MblnAddUpdatePermission = false;           //To set Add Update Permission

        private string strMReportPath = "";

        private clsBLLRptExpense MobjclsBLLRptExpense = null;
        private clsMessage ObjUserMessage = null;


        public FrmRptExpense()
        {
            //InitializeComponent();
            InitializeComponent();
        }

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.ExpenseMISReport);

                return this.ObjUserMessage;
            }
        }

       // private clsBLLRptSalaryStructure BLLRptSalaryStructure  //clsBLLAttendanceReport BLLAttendanceReport
        private clsBLLRptExpense BLLRptExpense  
        {
            get
            {
                if (this.MobjclsBLLRptExpense == null)
                    this.MobjclsBLLRptExpense = new clsBLLRptExpense();

                return this.MobjclsBLLRptExpense;
            }
        }

        private void FrmRptExpense_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadCombos(0);
            cboType.SelectedIndex = 0;
        }

        #region Methods

        private void SetPermissions()
        {
            // Function for setting permissions
            //clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            //if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            // {
            //   objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)ModuleID.Company, (Int32)MenuID.NewCompany, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            //}
            // else
            MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }

        private bool LoadCombos(int intType)
        {
            bool blnRetvalue = false;
            DataTable datCombos = new DataTable();
            try
            {

                if (intType == 0 || intType == 1) //Company Filling
                {
                    datCombos = BLLRptExpense.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID=" + ClsCommonSettings.LoginCompanyID });
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;
                    blnRetvalue = true;
                }

                if (intType == 0 || intType ==2) //Company Filling
                {
                   
                    datCombos = BLLRptExpense.FillCombos(new string[] { "AccountID, AccountName", "AccAccountMaster", "dbo.fnGetGroupAccountId(AccountGroupID,1,0)=4 and AccountID not in (9,10)" });

                    cboExpenseType.ValueMember = "AccountID";
                    cboExpenseType.DisplayMember = "AccountName";
                    DataRow dtRow;
                    dtRow = datCombos.NewRow();
                    dtRow["AccountID"] = 0;
                    dtRow["AccountName"] = "ALL";
                    datCombos.Rows.InsertAt(dtRow, 0);
                    cboExpenseType.DataSource = datCombos;
                    blnRetvalue = true;
          
                }
            
           


            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
                blnRetvalue = false;
            }
            return blnRetvalue;
        }

        private bool ShowReport()
        {
            this.rvExpense.Reset();
            int intType = 0;
            int intCompanyID = 0;
            int intExpenseTypeID = 0;
            DateTime dteFromDate;
            DateTime dteToDate;
            int intDateType;

            
            DataTable dt;

            intCompanyID = cboCompany.SelectedValue.ToInt32();
            intType = cboType.SelectedIndex;
            intExpenseTypeID = cboExpenseType.SelectedValue.ToInt32();

            dteFromDate = dtpFromDate.Value;
            dteToDate = dtpToDate.Value;


            if (dtpFromDate.Checked && dtpToDate.Checked)
            {
                intDateType = 1; //Ref Date Between FromDAte and To Date
            }
            else if (dtpFromDate.Checked && dtpToDate.Checked == false)
            {
                intDateType = 2; //Ref Date >= From Date
            }
            else if (dtpFromDate.Checked == false && dtpToDate.Checked)
            {
                intDateType = 3; //Ref Date <= To Date
            }
            else
            {
                intDateType = 0; //No Need of Date Filteration
            }

         
            if (ChkSummary.Checked ==true)
            {
                strMReportPath = Application.StartupPath + "\\MainReports\\RptMISExpenseSummary.rdlc";
                this.rvExpense.ProcessingMode = ProcessingMode.Local;
                this.rvExpense.LocalReport.ReportPath = strMReportPath;
                ReportParameter[] RepParam = new ReportParameter[1];
                RepParam[0] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);
                this.rvExpense.LocalReport.SetParameters(RepParam);
                this.rvExpense.LocalReport.DataSources.Clear();

                dt = BLLRptExpense.DisplayCompanyReportHeader(intCompanyID);

               
                DataTable dtExpenseSummary = BLLRptExpense.GetExpenseSummary(intType, intCompanyID, intDateType, dteFromDate, dteToDate);
                if (dtExpenseSummary.Rows.Count > 0)
                {
                    if (intType == 0)
                    {
                        rvExpense.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                        rvExpense.LocalReport.DataSources.Add(new ReportDataSource("DtSetExpense_DtExpPurchase", dtExpenseSummary));
                        rvExpense.LocalReport.DataSources.Add(new ReportDataSource("DtSetExpense_DtExpSales", ""));

                    }
                    else if (intType == 1)
                    {
                        rvExpense.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                        rvExpense.LocalReport.DataSources.Add(new ReportDataSource("DtSetExpense_DtExpPurchase", ""));
                        rvExpense.LocalReport.DataSources.Add(new ReportDataSource("DtSetExpense_DtExpSales", dtExpenseSummary));
                    }

                }
                else
                {
                    UserMessage.ShowMessage(9700);
                    return false;

                }

                this.rvExpense.SetDisplayMode(DisplayMode.PrintLayout);
                this.rvExpense.ZoomMode = ZoomMode.Percent;
                this.rvExpense.ZoomPercent = 100;



            }
            else
            {
                strMReportPath = Application.StartupPath + "\\MainReports\\RptMISExpenseDetails.rdlc";
                this.rvExpense.ProcessingMode = ProcessingMode.Local;
                this.rvExpense.LocalReport.ReportPath = strMReportPath;
                ReportParameter[] RepParam = new ReportParameter[1];
                RepParam[0] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);
                this.rvExpense.LocalReport.SetParameters(RepParam);
                this.rvExpense.LocalReport.DataSources.Clear();
               
                dt = BLLRptExpense.DisplayCompanyReportHeader(intCompanyID);


            DataTable dtExpenseDetails = BLLRptExpense.GetExpenseDetails(intType, intCompanyID, intExpenseTypeID, intDateType, dteFromDate, dteToDate);

            if (dtExpenseDetails.Rows.Count > 0)
            {
                if (intType == 0)
                {
                    rvExpense.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                    rvExpense.LocalReport.DataSources.Add(new ReportDataSource("DtSetExpense_DtExpPurchaseDet", dtExpenseDetails));
                    rvExpense.LocalReport.DataSources.Add(new ReportDataSource("DtSetExpense_DtExpSalesDet", ""));

                }
                else if(intType == 1)
                {
                    rvExpense.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                    rvExpense.LocalReport.DataSources.Add(new ReportDataSource("DtSetExpense_DtExpPurchaseDet", ""));
                    rvExpense.LocalReport.DataSources.Add(new ReportDataSource("DtSetExpense_DtExpSalesDet", dtExpenseDetails));

                }
            }
            else
            {
                UserMessage.ShowMessage(9700);
                return false;

            }

            this.rvExpense.SetDisplayMode(DisplayMode.PrintLayout);
            this.rvExpense.ZoomMode = ZoomMode.Percent;
            this.rvExpense.ZoomPercent = 100;


        }

            //if (cboType.SelectedIndex == 0)
            //{

            //    ReportParameter[] RepParam = new ReportParameter[1];
            //    RepParam[0] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);
            //    this.ReportViewerSummary.LocalReport.SetParameters(RepParam);
            //    this.ReportViewerSummary.LocalReport.DataSources.Clear();

            //    DataTable dtSalaryStructureSummary = BLLRptSalaryStructure.GetSalaryStructureSummary(intCompanyID, intBranchID, intDepartmentID, intDesignationID, intEmploymentTypeID, intIncludeComp);
            //    if (dtSalaryStructureSummary.Rows.Count > 0)
            //    {
            //        ReportViewerSummary.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
            //        ReportViewerSummary.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_SalaryStructureSummary", dtSalaryStructureSummary));
            //    }
            //    else
            //    {
            //        UserMessage.ShowMessage(9700);
            //        return false;

            //    }

            //}
            //else
            //{
            //    ReportParameter[] RepParam = new ReportParameter[1];
            //    RepParam[0] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);
            //    this.ReportViewerSummary.LocalReport.SetParameters(RepParam);
            //    this.ReportViewerSummary.LocalReport.DataSources.Clear();

            //    DataTable dtSalaryStructureHistorySummary = BLLRptSalaryStructure.GetSalaryStructureHistorySummary(intCompanyID, intBranchID, intDepartmentID, intDesignationID, intEmploymentTypeID, intIncludeComp);

            //    if (dtSalaryStructureHistorySummary.Rows.Count > 0)
            //    {
            //        ReportViewerSummary.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
            //        ReportViewerSummary.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_SalaryStructureSummary", dtSalaryStructureHistorySummary));
            //    }
            //    else
            //    {
            //        UserMessage.ShowMessage(9700);
            //        return false;
            //    }

            //}


            //this.ReportViewerSummary.SetDisplayMode(DisplayMode.PrintLayout);
            //this.ReportViewerSummary.ZoomMode = ZoomMode.Percent;
            //this.ReportViewerSummary.ZoomPercent = 100;

            return true;
        }

        private bool FormValidations()
        {
            if (cboType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9701);
                return false;
            }
            if (cboCompany.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9702);
                return false;
            }
            if (cboExpenseType.SelectedIndex== -1)
            {
                UserMessage.ShowMessage(9703);
                return false;
            }
            if (Convert.ToDateTime(dtpFromDate.Text) > Convert.ToDateTime(dtpToDate.Text))
            {
                UserMessage.ShowMessage(9704);
                return false;
            }
           

            return true;
        }


        #endregion

        private void btnShow_Click(object sender, EventArgs e)
        {
            //ShowReport();
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (FormValidations())
                {
                    ShowReport();
                }
            }
            catch (Exception Ex)
            {
                btnShow.Enabled = true;
                cboType.Enabled = true;
                cboCompany.Enabled = true;
                cboExpenseType.Enabled = true;
                dtpFromDate.Enabled = true;
                dtpToDate.Enabled = true;
                ChkSummary.Enabled = true;
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);

            }
            this.Cursor = Cursors.Default;
        }

        private void rvExpense_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;
            cboType.Enabled = false;
            cboCompany.Enabled = false;
            cboExpenseType.Enabled = false;
            dtpFromDate.Enabled = false;
            dtpToDate.Enabled = false;
            ChkSummary.Enabled = false;
        }

        private void rvExpense_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;
            cboType.Enabled = true;
            cboCompany.Enabled = true;
            cboExpenseType.Enabled = true;
            dtpFromDate.Enabled = true;
            dtpToDate.Enabled = true;
            ChkSummary.Enabled = true;
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            rvExpense.Reset();
            rvExpense.Clear();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            rvExpense.Reset();
            rvExpense.Clear();
        }

        private void cboExpenseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            rvExpense.Reset();
            rvExpense.Clear();
           // ChkSummary.Checked = false;
        }

        private void ChkSummary_CheckedChanged(object sender, EventArgs e)
        {
            rvExpense.Reset();
            rvExpense.Clear();

            if (ChkSummary.Checked == true)
            {
                cboExpenseType.SelectedIndex = 0;
                cboExpenseType.Enabled = false;

            }
            else if (ChkSummary.Checked == false)
            {
                cboExpenseType.SelectedIndex = 0;
                cboExpenseType.Enabled = true;

            }
        }

       private void ComboBox_KeyPress(object sender,KeyEventArgs e)
       {
           ComboBox Cbo = (ComboBox)sender;
           Cbo.DroppedDown=false;
       }



    }
}
