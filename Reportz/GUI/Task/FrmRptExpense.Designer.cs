﻿namespace MyBooksERP
{
    partial class FrmRptExpense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptExpense));
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.lblToDate = new DevComponents.DotNetBar.LabelX();
            this.lblFromDate = new DevComponents.DotNetBar.LabelX();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.ChkSummary = new System.Windows.Forms.CheckBox();
            this.cboExpenseType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblExpenseType = new DevComponents.DotNetBar.LabelX();
            this.cboType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.lblType = new DevComponents.DotNetBar.LabelX();
            this.btnShow = new System.Windows.Forms.Button();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.rvExpense = new Microsoft.Reporting.WinForms.ReportViewer();
            this.expandablePanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.lblToDate);
            this.expandablePanel1.Controls.Add(this.lblFromDate);
            this.expandablePanel1.Controls.Add(this.dtpToDate);
            this.expandablePanel1.Controls.Add(this.dtpFromDate);
            this.expandablePanel1.Controls.Add(this.ChkSummary);
            this.expandablePanel1.Controls.Add(this.cboExpenseType);
            this.expandablePanel1.Controls.Add(this.lblExpenseType);
            this.expandablePanel1.Controls.Add(this.cboType);
            this.expandablePanel1.Controls.Add(this.lblType);
            this.expandablePanel1.Controls.Add(this.btnShow);
            this.expandablePanel1.Controls.Add(this.cboCompany);
            this.expandablePanel1.Controls.Add(this.lblCompany);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(983, 127);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 2;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By  ";
            // 
            // lblToDate
            // 
            // 
            // 
            // 
            this.lblToDate.BackgroundStyle.Class = "";
            this.lblToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblToDate.Location = new System.Drawing.Point(265, 60);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(41, 25);
            this.lblToDate.TabIndex = 37;
            this.lblToDate.Text = "To Date";
            // 
            // lblFromDate
            // 
            // 
            // 
            // 
            this.lblFromDate.BackgroundStyle.Class = "";
            this.lblFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFromDate.Location = new System.Drawing.Point(265, 37);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(60, 23);
            this.lblFromDate.TabIndex = 36;
            this.lblFromDate.Text = "From Date";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(340, 65);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.ShowCheckBox = true;
            this.dtpToDate.Size = new System.Drawing.Size(120, 20);
            this.dtpToDate.TabIndex = 35;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(340, 36);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.ShowCheckBox = true;
            this.dtpFromDate.Size = new System.Drawing.Size(120, 20);
            this.dtpFromDate.TabIndex = 34;
            // 
            // ChkSummary
            // 
            this.ChkSummary.AutoSize = true;
            this.ChkSummary.Location = new System.Drawing.Point(265, 93);
            this.ChkSummary.Name = "ChkSummary";
            this.ChkSummary.Size = new System.Drawing.Size(69, 17);
            this.ChkSummary.TabIndex = 33;
            this.ChkSummary.Text = "Summary";
            this.ChkSummary.UseVisualStyleBackColor = true;
            this.ChkSummary.CheckedChanged += new System.EventHandler(this.ChkSummary_CheckedChanged);
            // 
            // cboExpenseType
            // 
            this.cboExpenseType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboExpenseType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboExpenseType.DisplayMember = "Text";
            this.cboExpenseType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboExpenseType.FormattingEnabled = true;
            this.cboExpenseType.ItemHeight = 14;
            this.cboExpenseType.Location = new System.Drawing.Point(91, 93);
            this.cboExpenseType.Name = "cboExpenseType";
            this.cboExpenseType.Size = new System.Drawing.Size(149, 20);
            this.cboExpenseType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboExpenseType.TabIndex = 32;
            this.cboExpenseType.SelectedIndexChanged += new System.EventHandler(this.cboExpenseType_SelectedIndexChanged);
            this.cboExpenseType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblExpenseType
            // 
            this.lblExpenseType.AutoSize = true;
            // 
            // 
            // 
            this.lblExpenseType.BackgroundStyle.Class = "";
            this.lblExpenseType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblExpenseType.Location = new System.Drawing.Point(12, 93);
            this.lblExpenseType.Name = "lblExpenseType";
            this.lblExpenseType.Size = new System.Drawing.Size(73, 15);
            this.lblExpenseType.TabIndex = 31;
            this.lblExpenseType.Text = "Expense Type";
            // 
            // cboType
            // 
            this.cboType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboType.DisplayMember = "Text";
            this.cboType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboType.FormattingEnabled = true;
            this.cboType.ItemHeight = 14;
            this.cboType.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2});
            this.cboType.Location = new System.Drawing.Point(91, 65);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(149, 20);
            this.cboType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboType.TabIndex = 30;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            this.cboType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "Purchase";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "Sale";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            // 
            // 
            // 
            this.lblType.BackgroundStyle.Class = "";
            this.lblType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblType.Location = new System.Drawing.Point(12, 65);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(27, 15);
            this.lblType.TabIndex = 26;
            this.lblType.Text = "Type";
            // 
            // btnShow
            // 
            this.btnShow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnShow.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnShow.Image = global::Reportz.Properties.Resources.Go;
            this.btnShow.Location = new System.Drawing.Point(469, 36);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(21, 20);
            this.btnShow.TabIndex = 17;
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(91, 36);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(149, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 4;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(12, 39);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(50, 15);
            this.lblCompany.TabIndex = 2;
            this.lblCompany.Text = "Company";
            // 
            // rvExpense
            // 
            this.rvExpense.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rvExpense.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource1.Value = null;
            this.rvExpense.LocalReport.DataSources.Add(reportDataSource1);
            this.rvExpense.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptCompanyHeader.rdlc";
            this.rvExpense.Location = new System.Drawing.Point(0, 127);
            this.rvExpense.Name = "rvExpense";
            this.rvExpense.Size = new System.Drawing.Size(983, 267);
            this.rvExpense.TabIndex = 5;
            this.rvExpense.RenderingComplete += new Microsoft.Reporting.WinForms.RenderingCompleteEventHandler(this.rvExpense_RenderingComplete);
            this.rvExpense.RenderingBegin += new System.ComponentModel.CancelEventHandler(this.rvExpense_RenderingBegin);
            // 
            // FrmRptExpense
            // 
            this.ClientSize = new System.Drawing.Size(983, 394);
            this.Controls.Add(this.rvExpense);
            this.Controls.Add(this.expandablePanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptExpense";
            this.Text = "Expense";
            this.Load += new System.EventHandler(this.FrmRptExpense_Load);
            this.expandablePanel1.ResumeLayout(false);
            this.expandablePanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboType;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.DotNetBar.LabelX lblType;
        internal System.Windows.Forms.Button btnShow;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboExpenseType;
        private DevComponents.DotNetBar.LabelX lblExpenseType;
        private DevComponents.DotNetBar.LabelX lblToDate;
        private DevComponents.DotNetBar.LabelX lblFromDate;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.CheckBox ChkSummary;
        private Microsoft.Reporting.WinForms.ReportViewer rvExpense;
    }
}