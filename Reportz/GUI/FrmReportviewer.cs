﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Collections;
using System.Data.SqlClient;

using System.IO;
using System.Drawing.Imaging;


namespace MyBooksERP
{
    public partial class FrmReportviewer : Form
    {
        public bool IsBOQEnabled = false;
        public int OperationTypeID { get; set; }
        public int PiFormID;
        public Int64 PiRecId = 0;
        public int PintExecutive;
        public int ITem;
        public int PintoperationType;
        public bool PblnCustomerWiseDeliveryNote;//From Item ISsue Form HAndling 2 Type Print
        public bool PblnPickList;//For Hamdling Location Wise Print Report
        public int PiDiscountType;
        public int No;
        public string PintNo;
        public int PintCompany;
        public string PsPeriod;
        public DataSet dt;
        int PiScale;

        public int PiVendorMode;
        public int PiVendorID;
        public string PsFromDate;
        public string PsToDate;
        public int PiStatus;
        public bool PbFinYearStart;

        public string sItem;
        public string PsReportPath;
        public string PsFormName;
        public string PsExecutive;
        string PsReportFooter;
        public string Type;
        public string sDate;
        public string strAmountInWord;

        public double PTax;
        public double STax;
        public double TTax;
        public double Totals;
        public DateTime dteFromDate;
        public DateTime dteToDate;
        public int intMenuID;
        public Object objTemp;
        public string strAccHeader;
        private string MsReportPath;
        clsBLLReportViewer MobjclsBLLReportViewer;

        // Vendor/customer history related fields
        public int MiVendorMode;
        public DataSet CompanyHeader;
        public int intAccountID;
        public bool blnIsGroup;
        public bool IsExpenseListInSales = false;

        public string strCompany;
        public string strCurrency;
        public bool AlMajdalPrePrintedFormat = false; // AlMajdal
        public bool SampleIssue = false; // AlMajdal

        public FrmReportviewer()
        {
            InitializeComponent();

            PsReportFooter = ClsCommonSettings.ReportFooter;
            PiScale = ClsCommonSettings.Scale;
            MobjclsBLLReportViewer = new clsBLLReportViewer();
        }

        private void FrmReportviewer_Load(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            this.Text = PsFormName;
            reportViewer1.ProcessingMode = ProcessingMode.Local;



            switch (PiFormID)
            {
                case (int)FormID.CompanyInformation:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanyForm.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;

                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_GaReportCompanyForm", MobjclsBLLReportViewer.DisplayCompanyReport()));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyAccountBankDetails", MobjclsBLLReportViewer.DisplayCompanyBankReport()));
                        break;
                    }
                case (int)FormID.DeductionPolicy:
                    {
                        //
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptDeductionPolicy.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataTable dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();

                        DataSet dtSet = MobjclsBLLReportViewer.DisplayDeductionPolicy();
                        //int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                        //dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetDeductionPolicy_DeductionPolicy", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetDeductionPolicy_DeductionParticulars", dtSet.Tables[1]));


                        break;
                    }
                case (int)FormID.BankInformation:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptBankInformationForm.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataTable dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_BankNameReference", MobjclsBLLReportViewer.DisplayBankReport()));
                        break;
                    }
                case (int)FormID.TermsAndConditions:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptTermsandConditions.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intOperation = PintoperationType;
                        MobjclsBLLReportViewer.clsDTOReportviewer.intCompany = PintCompany;
                        DataTable dt = MobjclsBLLReportViewer.DisplayTermsandconditions(PintoperationType, PintCompany);
                        //reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetTermsandConditions_STTermsConditions", dt));
                        break;
                    }
                case (int)FormID.EmployeeLeaveStructure:
                    {
                        string strFromDate = PsPeriod.Split(Convert.ToChar("-"))[0];


                       MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeLeaveStructure.rdlc";

 
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayLeaveStructure(PintExecutive, PintCompany, strFromDate, PsPeriod);

                        ReportParameter[] ReportParameter = new ReportParameter[3];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Employee", PsExecutive, false);
                        ReportParameter[2] = new ReportParameter("Period", PsPeriod, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(PintCompany);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetLeaveSummary_dtLeaveSummary", dtSet.Tables[1]));

                        break;
                    }

                case (int)FormID.Employee:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeInformation.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;

                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataTable dt = MobjclsBLLReportViewer.DisplayEmployeeReport();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_EmployeeMaster", dt));
                        break;
                    }

                case (int)FormID.UnitsOfMeasurement:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptUOMForm.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataTable dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STUOMReference", MobjclsBLLReportViewer.DisplayUOMReport()));
                        break;
                    }

                case (int)FormID.PaymentTerms:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptPaymentTermsForm.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();

                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STPaymentTerms", MobjclsBLLReportViewer.DisplayPaymentTermsReport()));
                        break;
                    }

                case (int)FormID.ExchangeCurrency:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptExchangeCurrencyForm.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayExchangeCurrencyReport();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_STCompanyCurrency", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_STExchangeCurrency", dtSet.Tables[1]));
                        break;
                    }

                case (int)FormID.Expense:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptExpenseform.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Reference", Type, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayExpenseReport();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STExpenseMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STExpenseDetails", dtSet.Tables[1]));
                        break;
                    }
                case (int)FormID.ExtraCharges:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptExtraChargesPrint.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtset = MobjclsBLLReportViewer.PrintExtraCharges(PiRecId);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_CompanyHeader", dtset.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_ExtraChargeMaster", dtset.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_ExtraChargeDetails", dtset.Tables[2]));
                        break;
                        
                    }
                case (int)FormID.ExtraChargePayment:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptExtraChargePayment.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtset = MobjclsBLLReportViewer.DisplayExtraChargePayment(PiRecId);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_CompanyHeader", dtset.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_ExtraChargePaymentMaster", dtset.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_ExtraChargePaymentDetails", dtset.Tables[2]));
                        break;
                    }

                case (int)FormID.ItemMaster:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptProduct.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayProductReport();
                        dtSet.Tables[0].Columns.Add("Image", typeof(byte[]));
                        foreach (DataRow dr in dtSet.Tables[0].Rows)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(dr["ImageFileName"])))
                            {
                                if (File.Exists(ClsCommonSettings.strServerPath + "\\Product Images\\Low Resolution\\"  + dr["ImageFileName"].ToString()))
                                {
                                    Image img = Image.FromFile(ClsCommonSettings.strServerPath + "\\Product Images\\Low Resolution\\" + dr["ImageFileName"].ToString());
                                    MemoryStream ms = new MemoryStream();
                                    img.Save(ms, ImageFormat.Jpeg);
                                    dr["Image"] = ms.ToArray();
                                }

                            }
                        }
                        int intCompanyID = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                        DataTable  dtCompany = new DataTable();
                        DataSet dt= new DataSet();
                        if (intCompanyID == 0)
                            dtCompany = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                        else
                            dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(intCompanyID);

                        if (intCompanyID == 0)
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                        else
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));

                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_dtProductsMain", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_dtProductBatchInfo", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_dtProductComponent", ""));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_dtProductDimensions", dtSet.Tables[2]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_dtProductFeatures", dtSet.Tables[3]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_dtProductReorder", dtSet.Tables[4]));
                        break;
                    }

                //DisplayItemGroupReport()
                case (int)FormID.ItemGroup:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptProductGrouping.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("ReportHeader",IsBOQEnabled?"true":"false" , false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayItemGroupReport();
                        //if (Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]) == 0)
                        //{
                        //    PintCompany = 1;
                        //}
                        //else
                        //{
                        //    PintCompany = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                        //}
                        PintCompany = ClsCommonSettings.CompanyID;
                        DataSet dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(PintCompany);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STItemGroupMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STItemGroupDetails", dtSet.Tables[1]));
                        break;
                    }

                case ((int)FormID.DiscountType):
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptDiscountForm.rdlc";
                        reportViewer1.ProcessingMode = ProcessingMode.Local;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("ItemWise", Convert.ToString(PiDiscountType), false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        if (PiDiscountType == 3)
                        {
                            DataTable dt = MobjclsBLLReportViewer.DisplaySTDiscountTypeReferenceReport();
                            DataTable dtCompany = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STDiscountTypeReference", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STDiscountItemWiseDetails", ""));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STDiscountCustomerWiseDetails", ""));
                        }
                        if (PiDiscountType == 1)
                        {
                            DataTable dt1 = MobjclsBLLReportViewer.DisplaySTDiscountItemWiseDetailsReport();
                            DataTable dtCompany = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STDiscountItemWiseDetails", dt1));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STDiscountTypeReference", ""));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STDiscountCustomerWiseDetails", ""));
                        }
                        if (PiDiscountType == 2)
                        {
                            DataTable dt2 = MobjclsBLLReportViewer.DisplaySTDiscountCustomerWiseDetailsReport();
                            DataTable dtCompany = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STDiscountCustomerWiseDetails", dt2));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STDiscountTypeReference", ""));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STDiscountItemWiseDetails", MobjclsBLLReportViewer.DisplaySTDiscountItemWiseDetailsReport()));
                        }
                        break;
                    }

                case (int)FormID.Warehouse:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptWarehouseForm.rdlc";

                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_STWarehouse", MobjclsBLLReportViewer.DisplayWareHouseReport()));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_STWarehouseDetails", MobjclsBLLReportViewer.DisplayWareHouseDetailsReport()));
                        break;
                    }

                case (int)FormID.AlertSetting:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAlertForm.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayAlertSettingsReport();
                        DataTable dtCompany = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STAlertSettings", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STAlertRoleSettings", dtSet.Tables[1]));
                        break;
                    }
                case (int)FormID.ItemIssue:
                    {
                        if (PblnPickList == false)
                        {
                            if (PblnCustomerWiseDeliveryNote == false)
                            {
                                MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                                MobjclsBLLReportViewer.clsDTOReportviewer.strType = Type;
                                DataSet dtSet = MobjclsBLLReportViewer.DisplayDeliveryNoteReport();
                                decimal decQty = 0;

                                if (AlMajdalPrePrintedFormat)
                                {
                                    if (SampleIssue)
                                        MsReportPath = Application.StartupPath + "\\MainReports\\RptSampleIssueAlMajdal.rdlc";
                                    else
                                    {
                                        MsReportPath = Application.StartupPath + "\\MainReports\\RptDeliveryNoteAlMajdal.rdlc";
                                        for (int i = 0; i <= dtSet.Tables[1].Rows.Count - 1; i++)
                                            decQty += dtSet.Tables[1].Rows[i]["Quantity"].ToDecimal();
                                    }
                                }
                                else
                                {
                                    if (IsBOQEnabled)
                                        MsReportPath = Application.StartupPath + "\\MainReports\\RptDeliveryNoteWisdom.rdlc";
                                    else
                                        MsReportPath = Application.StartupPath + "\\MainReports\\RptDeliveryNote.rdlc";
                                }
                                
                                reportViewer1.LocalReport.ReportPath = MsReportPath;
                                if (AlMajdalPrePrintedFormat && !SampleIssue)
                                {
                                    ReportParameter[] ReportParameter = new ReportParameter[3];
                                    ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                    ReportParameter[1] = new ReportParameter("Reference", Type, false);
                                    ReportParameter[2] = new ReportParameter("TotalQuantity", decQty.ToStringCustom(), false);
                                    reportViewer1.LocalReport.SetParameters(ReportParameter);
                                }
                                else
                                {
                                    ReportParameter[] ReportParameter = new ReportParameter[2];
                                    ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                    ReportParameter[1] = new ReportParameter("Reference", Type, false);
                                    reportViewer1.LocalReport.SetParameters(ReportParameter);
                                }
                                reportViewer1.LocalReport.DataSources.Clear();                                
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STItemIssueMaster", dtSet.Tables[0]));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STItemIssueDetails", dtSet.Tables[1]));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STTermsAndConditions", dtSet.Tables[2]));
                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptCustomerWiseDeliveryNote.rdlc";
                                MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                                MobjclsBLLReportViewer.clsDTOReportviewer.strType = Type;
                                reportViewer1.LocalReport.ReportPath = MsReportPath;
                                ReportParameter[] ReportParameter = new ReportParameter[2];
                                ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                ReportParameter[1] = new ReportParameter("Reference", Type, false);
                                reportViewer1.LocalReport.DataSources.Clear();
                                DataSet dtSet = MobjclsBLLReportViewer.DisplayCustomerWiseDeliveryNoteReport();
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STCustomerwiseDeliveryNoteDetails", dtSet.Tables[0]));
                                reportViewer1.LocalReport.SetParameters(ReportParameter);
                            }
                        }
                        else
                        {
                            MsReportPath = Application.StartupPath + "\\MainReports\\RptLocationDetails.rdlc";

                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[6];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            ReportParameter[1] = new ReportParameter("ReportName", "DELIVERY NOTE", false);
                            ReportParameter[2] = new ReportParameter("MainNo", "Issue No", false);
                            ReportParameter[3] = new ReportParameter("ReferenceNo", "Reference No", false);
                            ReportParameter[4] = new ReportParameter("Date", "Issue Date", false);
                            ReportParameter[5] = new ReportParameter("Type", "Operation Type", false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();

                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            DataSet dSLocation = MobjclsBLLReportViewer.DisplayItemIssueLocationReport();

                            int intCompanyID = Convert.ToInt32(dSLocation.Tables[0].Rows[0]["CompanyID"]);
                            DataSet dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(intCompanyID);

                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_LocationDetails", dSLocation.Tables[0]));
                        }

                        break;
                    }

                case (int)FormID.CompanySettings:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanySettingsForm.rdlc";

                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayCompanySettingsReport();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STCompanyDetails", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STCompanySettings", dtSet.Tables[1]));
                        break;
                    }
                case (int)FormID.RoleSettings:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptRoleSettingsForm.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayRoleSettingsReport();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STRoleSettings", dtSet.Tables[0]));
                        break;
                    }

                case (int)FormID.VendorInformation:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptVendorInformationForm.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataTable DTName = MobjclsBLLReportViewer.DisplayVendorReport();
                        int VendorTypeID = Convert.ToInt32(DTName.Rows[0]["VendorTypeID"]);
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        if (VendorTypeID == 2)
                        {
                            Name = "SUPPLIER INFORMATION";
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            ReportParameter[1] = new ReportParameter("Associates", Name, false);
                        }
                        else
                        {
                            Name = "CUSTOMER INFORMATION";
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            ReportParameter[1] = new ReportParameter("Associates", Name, false);
                        }
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataTable dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_VendorInformation", MobjclsBLLReportViewer.DisplayVendorReport()));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_STVendorAddress", MobjclsBLLReportViewer.DisplayVendorAddressReport()));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_SupplierNearestNeighbour", MobjclsBLLReportViewer.DisplaySupplierNearestNeighbourReport()));
                        break;
                    }

                case (int)FormID.VendorHistory:
                    {
                        reportViewer1.LocalReport.ReportPath = Application.StartupPath + @"\MainReports\RptVendorCustomerHistory.rdlc";
                        reportViewer1.ProcessingMode = ProcessingMode.Local;
                        ReportParameter[] ReportParameters = new ReportParameter[2];
                        ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameters[1] = new ReportParameter("HistoryHeader", (PiVendorMode == 1 ? "Vendor History" : "Customer History"), false);
                        reportViewer1.LocalReport.SetParameters(ReportParameters);
                        reportViewer1.LocalReport.DataSources.Clear();
                        break;
                    }

                case (int)FormID.Payments:
                case (int)FormID.Receipts:
                    {
                        if (AlMajdalPrePrintedFormat)
                        {
                            MsReportPath = Application.StartupPath + "\\MainReports\\RptAccPaymentReceiptVoucher.rdlc";
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[2];

                            if (PiFormID == (int)FormID.Payments)
                            {
                                ReportParameter[0] = new ReportParameter("Header", "PAYMENT VOUCHER", false);
                                ReportParameter[1] = new ReportParameter("AccountOf", "PAID TO MR / MS", false);
                            }
                            else
                            {
                                ReportParameter[0] = new ReportParameter("Header", "RECEIPT VOUCHER", false);
                                ReportParameter[1] = new ReportParameter("AccountOf", "RECEIVED FROM MR / MS", false);
                            }
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            DataSet dtSet = MobjclsBLLReportViewer.DisplayPaymentDetailsReportForAlMajdal();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_ReceiptAndPayment", dtSet.Tables[0]));
                        }
                        else
                        {
                            MsReportPath = Application.StartupPath + "\\MainReports\\RptPayment.rdlc";
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[5];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            if (PiFormID == (int)FormID.Payments)
                            {
                                ReportParameter[1] = new ReportParameter("Title", "PAYMENT", false);
                                ReportParameter[2] = new ReportParameter("PaymentNo", "Payment No", false);
                                ReportParameter[3] = new ReportParameter("PaymentDate", "Payment Date", false);
                                ReportParameter[4] = new ReportParameter("PaymentDetails", "Payment Details", false);
                            }
                            else
                            {
                                ReportParameter[1] = new ReportParameter("Title", "RECEIPT", false);
                                ReportParameter[2] = new ReportParameter("PaymentNo", "Receipt No", false);
                                ReportParameter[3] = new ReportParameter("PaymentDate", "Receipt Date", false);
                                ReportParameter[4] = new ReportParameter("PaymentDetails", "Receipt Details", false);
                            }
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            DataSet dtSet = MobjclsBLLReportViewer.DisplayPaymentDetailsReport();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STPayment", dtSet.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STPaymentDetails", dtSet.Tables[1]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STTermsAndConditions", dtSet.Tables[2]));
                        }
                        break;
                    }
                case (int)FormID.PurchaseIndent:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptPurchaseIndent.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayPurchaseDetailsReport(1);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", dtSet.Tables[1]));
                        break;
                    }
                case (int)FormID.PurchaseQuotation:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptPurchaseQuotation.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayPurchaseDetailsReport(2);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", dtSet.Tables[2]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", dtSet.Tables[3]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", dtSet.Tables[4]));
                        break;
                    }
                case (int)FormID.PurchaseOrder:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptPurchaseOrder.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        MobjclsBLLReportViewer.clsDTOReportviewer.strType = Type;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Reference", Type, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayPurchaseDetailsReport(3);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster", dtSet.Tables[2]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", dtSet.Tables[3]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", dtSet.Tables[4]));
                        break;
                    }
                case (int)FormID.PurchaseGRN:
                    {
                        if (PblnPickList == false)
                        {
                            MsReportPath = Application.StartupPath + "\\MainReports\\RptPurchaseGRN.rdlc";
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.strType = Type;
                            ReportParameter[] ReportParameter = new ReportParameter[2];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            ReportParameter[1] = new ReportParameter("Reference", Type, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            DataSet dtSet = MobjclsBLLReportViewer.DisplayPurchaseDetailsReport(4);
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", dtSet.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", dtSet.Tables[1]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", ""));
                        }
                        else
                        {
                            MsReportPath = Application.StartupPath + "\\MainReports\\RptLocationDetails.rdlc";

                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[6];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            ReportParameter[1] = new ReportParameter("ReportName", "GOODS RECEIPT NOTE", false);
                            ReportParameter[2] = new ReportParameter("MainNo", "GRN No", false);
                            ReportParameter[3] = new ReportParameter("ReferenceNo", "Reference No", false);
                            ReportParameter[4] = new ReportParameter("Date", "GRN Date", false);
                            ReportParameter[5] = new ReportParameter("Type", "GRN Type", false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();

                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            DataSet dSLocation = MobjclsBLLReportViewer.DisplayGRNLocationReport();

                            int intCompanyID = Convert.ToInt32(dSLocation.Tables[0].Rows[0]["CompanyID"]);
                            DataSet dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(intCompanyID);

                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_LocationDetails", dSLocation.Tables[0]));
                        }
                        break;
                    }
                case (int)FormID.PurchaseInvoice:
                    {
                        if (PblnPickList == false)
                        {
                            MsReportPath = Application.StartupPath + "\\MainReports\\RptPurchaseInvoice.rdlc";
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.strType = Type;
                            ReportParameter[] ReportParameter = new ReportParameter[2];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            ReportParameter[1] = new ReportParameter("Reference", Type, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            DataSet dtSet = MobjclsBLLReportViewer.DisplayPurchaseDetailsReport(5);
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", dtSet.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", dtSet.Tables[1]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", dtSet.Tables[2]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", dtSet.Tables[3]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", dtSet.Tables[4]));
                        }
                        else
                        {
                            MsReportPath = Application.StartupPath + "\\MainReports\\RptLocationDetails.rdlc";

                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[6];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            ReportParameter[1] = new ReportParameter("ReportName", "PURCHASE INVOICE", false);
                            ReportParameter[2] = new ReportParameter("MainNo", "Invoice No", false);
                            ReportParameter[3] = new ReportParameter("ReferenceNo", "Supplier Bill No", false);
                            ReportParameter[4] = new ReportParameter("Date", "Invoice Date", false);
                            ReportParameter[5] = new ReportParameter("Type", "Invoice Type", false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();

                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            DataSet dSLocation = MobjclsBLLReportViewer.DisplayPILocationReport();

                            int intCompanyID = Convert.ToInt32(dSLocation.Tables[0].Rows[0]["CompanyID"]);
                            DataSet dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(intCompanyID);

                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_LocationDetails", dSLocation.Tables[0]));
                        }
                        break;
                    }

                case (int)FormID.SalesQuotation:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptSalesQuotation.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplaySalesDetailReport(1);
                        int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                        dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationDetails", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationExpenseDetails", dtSet.Tables[2]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", dtSet.Tables[3]));
                        break;
                    }

                case (int)FormID.SalesOrder:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptSalesOrder.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplaySalesDetailReport(2);
                        int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                        dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderDetails", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderExpenseDetails", dtSet.Tables[2]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", dtSet.Tables[3]));
                        break;
                    }

                case (int)FormID.SalesInvoice:
                    {
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        MobjclsBLLReportViewer.clsDTOReportviewer.IsExpenseListInSales = IsExpenseListInSales;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplaySalesDetailReport(3);
                        if (AlMajdalPrePrintedFormat)
                        {
                            if(dtSet.Tables[0].Rows[0]["TaxScheme"].ToString()=="")
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptSaleInvoiceAlMajdalSales.rdlc";
                            else
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptSaleInvoiceAlMajdal.rdlc";
                           ReportParameter[] ReportParameter1 = new ReportParameter[7];
                           ReportParameter1[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                           ReportParameter1[1] = new ReportParameter("NetAmount", dtSet.Tables[0].Rows[0]["NetAmount"].ToStringCustom(), false);
                           ReportParameter1[2] = new ReportParameter("Remarks", dtSet.Tables[0].Rows[0]["Remarks"].ToStringCustom(), false);
                           ReportParameter1[3] = new ReportParameter("AmountInWords", dtSet.Tables[0].Rows[0]["AmountInWords"].ToStringCustom(), false);
                           ReportParameter1[4] = new ReportParameter("Scale", dtSet.Tables[0].Rows[0]["CurrencyScale"].ToStringCustom(), false);
                           ReportParameter1[5] = new ReportParameter("TaxAmount", dtSet.Tables[0].Rows[0]["TaxAmount"].ToStringCustom(), false);
                           ReportParameter1[6] = new ReportParameter("DiscAmount", dtSet.Tables[0].Rows[0]["GrandDiscountAmount"].ToStringCustom(), false);
                          
                           reportViewer1.LocalReport.ReportPath = MsReportPath;
                           reportViewer1.LocalReport.SetParameters(ReportParameter1);
                        }
                        else
                        {
                            if (IsBOQEnabled)
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptSaleInvoiceWisdome.rdlc";
                            else
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptSaleInvoice.rdlc";

                            ReportParameter[] ReportParameter2 = new ReportParameter[1];
                            ReportParameter2[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            reportViewer1.LocalReport.SetParameters(ReportParameter2);
                        }
                      
                       
                        reportViewer1.LocalReport.DataSources.Clear();

                       
                        int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                        dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);
                        DataTable dt1 = MobjclsBLLReportViewer.DisplaySalesInvoice1Report();

                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoicenDetails", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceExpenseDetails", dtSet.Tables[2]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", dtSet.Tables[3]));
                        break;
                    }
                case (int)FormID.CommercialInvoice:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptCommercialInvoice.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[4];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Company", strCompany, false);
                        ReportParameter[2] = new ReportParameter("Currency", strCurrency, false);
                        ReportParameter[3] = new ReportParameter("AmountInWords", strAmountInWord, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplaySalesDetailReport(5);

                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STCommercialInvoice", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STCommercialInvoiceDetails", dtSet.Tables[1]));
                        break;
                    }
                case (int)FormID.PackingList:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptPackingList.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Company", strCompany, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplaySalesDetailReport(5);

                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STCommercialInvoice", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STCommercialInvoiceDetails", dtSet.Tables[1]));
                        break;
                    }
                case (int)FormID.SalesReturn:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptSalesReturn.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplaySalesDetailReport(4);
                        int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                        DataSet dtsetCompany = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtsetCompany.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnDetails", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", dtSet.Tables[2]));
                        break;
                    }

                case (int)FormID.OpeningStock:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptOpeningStockForm.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayOpeningStockReport();
                        //int companyid=Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);                    
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STOpeningStockMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STOpeningStockGrid", dtSet.Tables[1]));
                        break;
                    }
                case (int)FormID.StockAdjustment:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptStockAdjustment.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayStockAdjustmentReport();
                        //int companyid=Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);    
                        // DataSet dtsetCompany = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtSet.Tables[0]));
                        // reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtsetCompany.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STStockAdjustmentDetails", dtSet.Tables[1]));
                        break;
                    }

                case (int)FormID.AccountSettings:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAccountSettingsForm.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayAccountSettingsReport();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccSettingsGrid", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STCompanyDetails", dtSet.Tables[1]));
                        break;
                    }
                case (int)FormID.RecurrentSetup:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAccRecuurenceSetup.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayRecurrentSetupReport();
                        DataSet dtSetCompany = MobjclsBLLReportViewer.DisplayCompanyReportHeader(PintCompany);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STCompanyDetails", dtSetCompany.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccRecurranceSetup", dtSet.Tables[0]));
                        
                        break;
                    }
                case (int)FormID.AccountSummary :
                    {
                        MobjclsBLLReportViewer.clsDTOReportviewer.intCompany = PintCompany;
                        DataSet dtSetCompany = MobjclsBLLReportViewer.DisplayCompanyReportHeader(PintCompany);
                        clsBLLAccountSummaryReport objclsBLLAccountSummary = new clsBLLAccountSummaryReport();
                        objclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID = intMenuID;
                        objclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempCompanyID= PintCompany;
                        objclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempAccountID = intAccountID;
                        objclsBLLAccountSummary.MobjclsDTOAccountSummary.dtpTempFromDate = dteFromDate;
                        objclsBLLAccountSummary.MobjclsDTOAccountSummary.dtpTempToDate = dteToDate;
                        objclsBLLAccountSummary.MobjclsDTOAccountSummary.IsTempGroup = blnIsGroup;
                        
                        switch (intMenuID)//For geting Accouts Summary Reports
                        {
                            case (int)eMenuID.GeneralLedger:
                                {
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptAccGeneralLedger.rdlc";
                                    reportViewer1.LocalReport.ReportPath = MsReportPath;
                                    ReportParameter[] ReportParameter = new ReportParameter[3];
                                    ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                    ReportParameter[1] = new ReportParameter("Header", strAccHeader, true);
                                    ReportParameter[2] = new ReportParameter("FromDate", dteFromDate.ToString("dd MMM yyyy"), true);
                                    reportViewer1.LocalReport.SetParameters(ReportParameter);
                                    reportViewer1.LocalReport.DataSources.Clear();
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtSetCompany.Tables[0]));
                                    MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                                    DataSet dtSet = objclsBLLAccountSummary.DisplayAccountsSummaryReoprt();
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountSummary_GeneralLedger", dtSet.Tables[0]));//
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountSummary_GeneralLedger_ClosingBalance", dtSet.Tables[1]));

                                    break;
                                }
                            case (int)eMenuID.DayBook:
                                {
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptAccDaybook.rdlc";
                                    reportViewer1.LocalReport.ReportPath = MsReportPath;
                                    ReportParameter[] ReportParameter = new ReportParameter[2];
                                    ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                    ReportParameter[1] = new ReportParameter("Header", strAccHeader, true);
                                    reportViewer1.LocalReport.SetParameters(ReportParameter);
                                    reportViewer1.LocalReport.DataSources.Clear();
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtSetCompany.Tables[0]));
                                    MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                                    DataSet dtSet = objclsBLLAccountSummary.DisplayAccountsSummaryReoprt();
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountSummary_DayBook", dtSet.Tables[0]));//

                                    break;
                                }
                            case (int)eMenuID.CashBook:
                            case (int)eMenuID.AccountsSummary:
                                {
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptAccountsSummary.rdlc";
                                    reportViewer1.LocalReport.ReportPath = MsReportPath;
                                    ReportParameter[] ReportParameter = new ReportParameter[2];
                                    ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                    ReportParameter[1] = new ReportParameter("Header",strAccHeader , true);
                                    reportViewer1.LocalReport.SetParameters(ReportParameter);
                                    reportViewer1.LocalReport.DataSources.Clear();
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtSetCompany.Tables[0]));
                                    DataSet dtSet = objclsBLLAccountSummary.DisplayAccountsSummaryReoprt();
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountSummary", dtSet.Tables[0]));

                                    break;
                                }
                            case (int)eMenuID.TrialBalance:
                                {
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptAccTrialBalance.rdlc";
                                    reportViewer1.LocalReport.ReportPath = MsReportPath;
                                    ReportParameter[] ReportParameter = new ReportParameter[2];
                                    ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                    ReportParameter[1] = new ReportParameter("Header", strAccHeader, true);
                                    reportViewer1.LocalReport.SetParameters(ReportParameter);
                                    reportViewer1.LocalReport.DataSources.Clear();
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtSetCompany.Tables[0]));
                                    DataSet dtSet = objclsBLLAccountSummary.DisplayAccountsSummaryReoprt();
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountSummary", dtSet.Tables[0]));
                                    break;
                                }
                            case (int)eMenuID.BalanceSheet:
                                {
                                    string strPeriod = objclsBLLAccountSummary.MobjclsDTOAccountSummary.dtpTempFromDate.ToString("dd MMM yyyy");
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptAccBalanceSheet.rdlc";
                                    reportViewer1.LocalReport.ReportPath = MsReportPath;
                                    ReportParameter[] ReportParameter = new ReportParameter[2];
                                    ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                    ReportParameter[1] = new ReportParameter("Period", strPeriod, true);
                                    reportViewer1.LocalReport.SetParameters(ReportParameter);
                                    CompanyHeader = objclsBLLAccountSummary.DisplayCompanyHeader();

                                    reportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                                    reportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                                    reportViewer1.LocalReport.DataSources.Clear();
                                    DataSet dtSet = objclsBLLAccountSummary.DisplayAccountsSummaryReoprt();

                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_BalanceSheet_Liabilities", dtSet.Tables[0]));
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_BalanceSheet_Asset", dtSet.Tables[1]));
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_BalanceSheet_GrandTotal", dtSet.Tables[2]));
                                    break;
                                }
                            case (int)eMenuID.ProfitAndLossAccount:
                                {
                                    string strPeriod = "";
                                    if (objclsBLLAccountSummary.MobjclsDTOAccountSummary.dtpTempFromDate !=
                                        objclsBLLAccountSummary.MobjclsDTOAccountSummary.dtpTempToDate)
                                        strPeriod = objclsBLLAccountSummary.MobjclsDTOAccountSummary.dtpTempFromDate.ToString("dd MMM yyyy") + " - " +
                                            objclsBLLAccountSummary.MobjclsDTOAccountSummary.dtpTempToDate.ToString("dd MMM yyyy");
                                    else
                                        strPeriod = objclsBLLAccountSummary.MobjclsDTOAccountSummary.dtpTempFromDate.ToString("dd MMM yyyy");

                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptAccProfitAndLossAccounts.rdlc";
                                    reportViewer1.LocalReport.ReportPath = MsReportPath;
                                    ReportParameter[] ReportParameter = new ReportParameter[2];
                                    ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                    ReportParameter[1] = new ReportParameter("Period", strPeriod, true);
                                    reportViewer1.LocalReport.SetParameters(ReportParameter);
                                    reportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                                    reportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessingEventHandler);

                                    reportViewer1.LocalReport.DataSources.Clear();
                                    CompanyHeader = objclsBLLAccountSummary.DisplayCompanyHeader();
                                    DataSet dtSet = objclsBLLAccountSummary.DisplayAccountsSummaryReoprt();
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));

                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_Trading_Dr", dtSet.Tables[0]));
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_Trading_Cr", dtSet.Tables[1]));
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_ProfitAndLossAccouts_Dr", dtSet.Tables[2]));
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_ProfitAndLossAccouts_Cr", dtSet.Tables[3]));
                                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_ProfitAndLossAccouts_GrandTotal", dtSet.Tables[4]));
                                    break;
                                }
                        }
                        break;
                    }
                case (int)FormID.JournalRecurrenceSetup:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAccRecurringJournalSetup.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSetCompany = MobjclsBLLReportViewer.DisplayCompanyReportHeader(PintCompany);
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayAccRecurringJournalSetup();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STCompanyDetails", dtSetCompany.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccRecurringJournalSetup", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccRecurranceSetup", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccRecurringJournalSetup_Details", dtSet.Tables[2]));

                        break;
                    }
                case (int)FormID.RFQ: // RFQ REport
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptRFQForm.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtset = MobjclsBLLReportViewer.DisplayRFQReport();
                        int companyid = Convert.ToInt32(dtset.Tables[0].Rows[0]["CompanyID"]);
                        DataSet dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(PintCompany );
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", dtset.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", dtset.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", dtset.Tables[2]));
                        break;
                    }

                case (int)FormID.UnitConversion:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptUOMConversion.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataTable dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                        DataTable dt1 = MobjclsBLLReportViewer.DisplayUOMConversionReport(ITem, PintoperationType).Tables[0];
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STUomConversion", dt1));
                        break;
                    }

                case (int)FormID.GeneralReceiptsAndPayments:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptGeneralReceiptsAndPayments.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;//VoucherID
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayGeneralReceiptsAndPayements();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_JournalVoucher", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_VoucherDetails", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtSet.Tables[2]));

                        break;
                    }
                case (int)FormID.GroupJV:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptGeneralReceiptsAndPayments.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;//VoucherID
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayGroupJV();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_JournalVoucher", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_VoucherDetails", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtSet.Tables[2]));

                        break;
                    }
                case (int)FormID.OpeningBalance :
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAccountOpeningBalance.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;//AccountID
                        MobjclsBLLReportViewer.clsDTOReportviewer.intCompany = PintCompany;//CompanyID
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayAccountsOpeningBalance();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STCompanyDetails", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsOpeningBalance", dtSet.Tables[1]));

                        break;
                    }


                case (int)FormID.Projects:
                    {

                        MsReportPath = Application.StartupPath + "\\MainReports\\RptTempProject.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();

                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayProjectReport();
                        int companyid = 0;
                        if (dtSet.Tables[0].Rows.Count > 0)
                            // int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                            companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);

                        dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);
                        // DataSet dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);

                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_STTempProject", dtSet.Tables[0]));

                        break;
                    }

                case (int)FormID.DebitNote:
                    {
                        if (PblnPickList == false)
                        {
                            MsReportPath = Application.StartupPath + "\\MainReports\\RptDebitNote.rdlc";

                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            // ReportParameter[1] = new ReportParameter("strAmountInWords", strAmountInWord, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();

                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            DataSet dtSet = MobjclsBLLReportViewer.DisplayDebitNoteReport();

                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", dtSet.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", dtSet.Tables[1]));
                        }
                        else
                        {
                            MsReportPath = Application.StartupPath + "\\MainReports\\RptLocationDetails.rdlc";

                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[6];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            ReportParameter[1] = new ReportParameter("ReportName", "DEBIT NOTE", false);
                            ReportParameter[2] = new ReportParameter("MainNo", "Return No", false);
                            ReportParameter[3] = new ReportParameter("ReferenceNo", "Invoice No", false);
                            ReportParameter[4] = new ReportParameter("Date", "Return Date", false);
                            ReportParameter[5] = new ReportParameter("Type", "", false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();

                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            DataSet dSLocation = MobjclsBLLReportViewer.DisplayDebitNoteLocationReport();

                            int intCompanyID = Convert.ToInt32(dSLocation.Tables[0].Rows[0]["CompanyID"]);
                            DataSet dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(intCompanyID);

                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_LocationDetails", dSLocation.Tables[0]));
                        }
                        break;
                    }
                case (int)FormID.StockTransfer:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptStockTransfer.rdlc";
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();

                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayStockTransfer();

                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STStockTransferMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STStockTransferDetail", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STStockTransferExpenseMaster", dtSet.Tables[2]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STTermsAndConditions", dtSet.Tables[3]));
                        break;
                    }
                case (int)FormID.PricingScheme:
                    {

                        MsReportPath = Application.StartupPath + "\\MainReports\\RptPricingScheme.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayPricingSchemeReport();
                        DataTable dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STPricingScheme", dtSet.Tables[0]));
                        break;
                    }

                case (int)FormID.PointOfSales:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptPOS.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayPOSReport();
                        int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                        dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);

                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSDetails", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", dtSet.Tables[2]));

                        break;

                    }
                case (int)FormID.LeavePolicy:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\rptCompanyLeaveDetail.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayLeavePolicy();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_LeavePolicyMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_LeavePolicyDetail", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_LeavePolicyConsequences", dtSet.Tables[2]));

                        break;
                    }
                case (int)FormID.ShiftPolicy:
                    {
 
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanyShiftDetails.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayShiftPolicy();
                        //int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                        DataTable dtComp = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();

                        if (ClsCommonSettings.Glb24HourFormat == false && dtSet.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < dtSet.Tables[0].Rows.Count; i++)
                            {
                                if (dtSet.Tables[0].Rows[i]["FromTime"] != DBNull.Value)
                                    dtSet.Tables[0].Rows[i]["FromTime"] = ConvertTo12Hr(Convert.ToString(dtSet.Tables[0].Rows[i]["FromTime"]));
                                if (dtSet.Tables[0].Rows[i]["ToTime"] != DBNull.Value)
                                    dtSet.Tables[0].Rows[i]["ToTime"] = ConvertTo12Hr(Convert.ToString(dtSet.Tables[0].Rows[i]["ToTime"]));
                                if (Convert.ToInt32(dtSet.Tables[0].Rows[i]["ShiftTypeID"]) == 4)
                                {
                                    if (dtSet.Tables[0].Rows[i]["FromTimeD"] != DBNull.Value)
                                        dtSet.Tables[0].Rows[i]["FromTimeD"] = ConvertTo12Hr(Convert.ToString(dtSet.Tables[0].Rows[i]["FromTimeD"]));
                                    if (dtSet.Tables[0].Rows[i]["ToTimeD"] != DBNull.Value)
                                        dtSet.Tables[0].Rows[i]["ToTimeD"] = ConvertTo12Hr(Convert.ToString(dtSet.Tables[0].Rows[i]["ToTimeD"]));
                                }
                            }
                        }
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtComp));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_ShiftPolicy", dtSet.Tables[0]));
                        //reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_ShiftPolicyDetail", dtSet.Tables[1]));
                        break;
                    }
                case (int)FormID.WorkPolicy:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptPolicyMaster.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayWorkPolicy();
                        int companyid = ClsCommonSettings.LoginCompanyID;
                        dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);

                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_WorkPolicy", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_WorkPolicyDetail", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_WorkPolicyConsequence", dtSet.Tables[2]));
                        break;
                    }
                case (int)FormID.OvertimePolicy:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptOTPolicy.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSetOTPolicy = MobjclsBLLReportViewer.DisplayOvertimePolicy();
                        
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtSetOTPolicy.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_OvertimePolicy", dtSetOTPolicy.Tables[1]));
                        break;
                    }

                case (int)FormID.Documents:
                    {
                        if (PiStatus == (int)OperationStatusType.DocReceipt)
                        {
                            MsReportPath = Application.StartupPath + "\\MainReports\\RptDocumentReceipt.rdlc";
                        }
                        else
                        {
                            MsReportPath = Application.StartupPath + "\\MainReports\\RptDocumentIssue.rdlc";
                        }
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        MobjclsBLLReportViewer.clsDTOReportviewer.intStatusID  = PiStatus ;

                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();

                        DataSet stSetDocuments = MobjclsBLLReportViewer.DisplayReceiptIssue();
                        int companyid = 0;
                        if (stSetDocuments.Tables[0].Rows.Count > 0)
                        {
                             companyid = Convert.ToInt32(stSetDocuments.Tables[0].Rows[0]["CompanyID"]);
                        }
                        dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);

                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                        //reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtSetOTPolicy.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtDocument", stSetDocuments.Tables[0]));
                        break;
                    }

                case (int)FormID.SalaryStructure:
                    {

                        decimal decAddAmt = 0, decDedAmt = 0;

                        
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptSalaryStructure.rdlc";

                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        DataSet dtSet = MobjclsBLLReportViewer.DisplaySalaryStructure();
                        if (dtSet.Tables[1].Rows.Count > 0)
                        {
                            decAddAmt = dtSet.Tables[1].Compute("SUM(Amount)", "IsAddtion ='true'").ToDecimal();
                            decDedAmt = dtSet.Tables[1].Compute("SUM(Amount)", "IsAddtion ='false'").ToDecimal();
                        }
                        ReportParameter[] ReportParameter = new ReportParameter[3];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("AdditionAmount", decAddAmt.ToString(), false);
                        ReportParameter[2] = new ReportParameter("DeductionAmount", decDedAmt.ToString(), false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                        dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_SalaryStructure", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_SalaryStructureDetail", dtSet.Tables[1]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_OtherRemuneration", dtSet.Tables[2]));
                        break;
                    }

                case (int)FormID.SalaryRelease:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeePaymentRelease.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intCompany = PintCompany;
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        MobjclsBLLReportViewer.clsDTOReportviewer.strDate = sDate; 
                        reportViewer1.LocalReport.ReportPath = MsReportPath;

                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);

                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplaySalaryRelease();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeepaymentRelease_EmployeePaymentDetailRelease", dtSet.Tables[0]));
                        break;
                    }
                case (int)FormID.MaterialIssue:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptMaterialIssue.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        MobjclsBLLReportViewer.clsDTOReportviewer.strType = Type;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Reference", Type, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayMaterialIssueReport();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STMaterialIssueMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STMaterialIssueDetails", dtSet.Tables[1]));
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        break;
                    }
                case (int)FormID.MaterialReturn:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptMaterialReturn.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        MobjclsBLLReportViewer.clsDTOReportviewer.strType = Type;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Reference", Type, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.DisplayMaterialReturnReport();
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STMaterialReturnMaster", dtSet.Tables[0]));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_STMaterialReturnDetails", dtSet.Tables[1]));
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        break;
                    }
                case (int)FormID.CompanywiseProduction:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanywiseProduction.rdlc";
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        MobjclsBLLReportViewer.clsDTOReportviewer.intCompany = PintCompany;
                        this.reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        this.reportViewer1.LocalReport.SetParameters(ReportParameter);
                        CompanyHeader = MobjclsBLLReportViewer.DisplayCompanyHeader();
                        this.reportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.reportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.reportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = MobjclsBLLReportViewer.GetJobOrderDetails();
                        this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));
                        this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_InvCompanywiseProduction", dtSet.Tables[0]));
                        this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetInventory_InvCompanywiseProductionDetails", dtSet.Tables[1]));
                        break;
                    }
                case (int)FormID.DirectGRN:
                    {
                        if (PblnPickList == false)
                        {
                            MsReportPath = Application.StartupPath + "\\MainReports\\RptPurchaseGRN.rdlc";
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.strType = Type;
                            ReportParameter[] ReportParameter = new ReportParameter[2];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            ReportParameter[1] = new ReportParameter("Reference", Type, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            DataSet dtSet = MobjclsBLLReportViewer.DisplayPurchaseDetailsReport(6);
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", dtSet.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", dtSet.Tables[1]));
                        }
                      
                        break;
                    }

                case (int)FormID.VacationEntry:
                    {
                        DataSet DST;
                        DataTable T1;
                        DataTable T2;

                       MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeVacationProcess.rdlc";

                        reportViewer1.LocalReport.ReportPath = MsReportPath;

                        ReportParameter[] ReportParameter = new ReportParameter[3];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Scale", "3", false);
                        ReportParameter[2] = new ReportParameter("bComDisplay", "true", true);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        DST = MobjclsBLLReportViewer.VacationProcessReport();
                        T1 = DST.Tables[0];
                        T2 = DST.Tables[1];

                        reportViewer1.LocalReport.DataSources.Clear();

                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeVacationProcess_VacationMaster", T1));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeVacationProcess_VacationDetail", T2));
                        break;
                    }


                case (int)FormID.LeaveEntry:
                    {
                        //


                            MsReportPath = Application.StartupPath + "\\MainReports\\RptLeaveDetails.rdlc";


                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[1];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataTable dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();

                        DataSet dtSet = MobjclsBLLReportViewer.DisplayEmployeeLeaveEntry();
                        //int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                        //dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);
                        //reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayEmployeeLeaveEntry_EmployeeLeaveDetails", dtSet.Tables[0]));
                        //  reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayEmployeeLeaveEntry_LeaveOpening", dtSet.Tables[0]));


                        break;
                    }
                case (int)FormID.SettlementProcess:
                    {

                        int HasSalary;
                        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;

                        if (OperationTypeID != 1000)
                        {
                           
                            MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeSettlement.rdlc";


                            reportViewer1.LocalReport.DataSources.Clear();
                            reportViewer1.LocalReport.ReportPath = MsReportPath;

                            DataTable datDetail;

                            datDetail = MobjclsBLLReportViewer.EmpSettlementDetails();
                            HasSalary = 0;

                            DataTable datmaster; int Pscale = 0;
                            datmaster = MobjclsBLLReportViewer.EmpSettlement();
                            if (datmaster.Rows[0]["Scale"] == System.DBNull.Value)
                            {
                                Pscale = 2;
                            }
                            else
                            {
                                Pscale = Convert.ToInt32(datmaster.Rows[0]["Scale"]);
                            }
                            ReportParameter[] RepParam = new ReportParameter[4];
                            RepParam[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            RepParam[1] = new ReportParameter("Scale", Pscale.ToString(), false);
                            RepParam[2] = new ReportParameter("HasSalary", HasSalary.ToString(), false);
                            RepParam[3] = new ReportParameter("bComDisplay", "true", false);
                            reportViewer1.LocalReport.SetParameters(RepParam);

                            DataTable dtCompany = Report.GetReportHeader(datmaster.Rows[0]["CompanyID"].ToInt32(), 0, true);

                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmployeeSettlement", datmaster));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmployeeSettlementDetail", datDetail));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                        }
                        else
                        {
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////
                          
                            MsReportPath = Application.StartupPath + "\\MainReports\\RptGratuityReport.rdlc";
                            reportViewer1.LocalReport.DataSources.Clear();
                            reportViewer1.LocalReport.ReportPath = MsReportPath;

                            DataTable datSettDtls;
                            DataTable datLeaveDtls;
                            DataTable datGLADtls;
                            DataTable datPayEmployeeSettlementGratuity;
                            DataTable datLastDtls;
                            DataSet DTSet;

                            DTSet = MobjclsBLLReportViewer.EmpGratuityReport();

                            HasSalary = 0;

                            datSettDtls = DTSet.Tables[0];
                            datLeaveDtls = DTSet.Tables[1];
                            datGLADtls = DTSet.Tables[2];
                            datPayEmployeeSettlementGratuity = DTSet.Tables[3];
                            datLastDtls = DTSet.Tables[4];



                            ReportParameter[] RepParam = new ReportParameter[1];
                            RepParam[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(RepParam);

                            DataTable dtCompany = Report.GetReportHeader(datSettDtls.Rows[0]["CompanyID"].ToInt32(), 0, true);

                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_SettDtls", datSettDtls));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_LeaveDtls", datLeaveDtls));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_GLADtls", datGLADtls));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_PayEmployeeSettlementGratuity", datPayEmployeeSettlementGratuity));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_LastDtls", datLastDtls));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));

                            //////////////////////////////////////////////////////////////////////////////////////////////////////////

                        }


                        /*******************************************************************************************/
                        break;
                    }

                case (int)FormID.LeaveExtension:
                    {
                        //

                        MsReportPath = Application.StartupPath + "\\MainReports\\RptLeaveExtensionSummary.rdlc";

                        reportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("EmployeeName", PsExecutive, false);
                        reportViewer1.LocalReport.SetParameters(ReportParameter);
                        reportViewer1.LocalReport.DataSources.Clear();
                        DataTable dtCompany = new DataTable();
                        MobjclsBLLReportViewer.clsDTOReportviewer.intCompany = PintCompany;
                        ////dtCompany = MobjclsBLLReportViewer.DisplayCompanyHeader().Tables[0];
                        ////if(dtCompany.Rows.Count==0)
                        //   dtCompany = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();

                        DataTable dtLeave = MobjclsBLLReportViewer.DisplayLeaveExtension(PintExecutive, PintCompany);
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtLeave));
                        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetLeaveExtension_DtSetLeaveExtensionSummary", dtLeave));
                        break;
                    }



            }
            reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
            reportViewer1.ZoomMode = ZoomMode.Percent;
            this.Cursor = Cursors.Default;
        }

        public void SubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));
        }

        private string ConvertTo12Hr(string Time)
        {
            dtpHourFormat12.CustomFormat = "hh:mm tt";
            string msTime = Time;
            string ConvertedTime = "";
            dtpHourFormat12.Text = Time;
            ConvertedTime = dtpHourFormat12.Text;
            return ConvertedTime;
        }
    }
}
