﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MyBooksERP
{
    public partial class FrmRptPayments : Report
    {
        #region Properties
        private string strMReportPath = "";

        private clsMessage ObjUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.PaymentsMISReport);

                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Constructor
        public FrmRptPayments()
        {
            InitializeComponent();
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}



        }
        #endregion
        //#region SetArabicControls
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.PaymentsMISReport, this);
        //}
        //#endregion SetArabicControls

        #region FormLoad
        private void FrmRptPayments_Load(object sender, EventArgs e)
        {
            //Fill all the combos
            FillCombos();

            SetDefaultBranch(cboBranch);
            SetDefaultWorkStatus(cboWorkStatus); 
            //Enable or disable the month combo ,from date and to date combo based on the type value
            EnableDisableDates(); 

            chkIncludeCompany.Checked = true;


            //if (ClsCommonSettings.IsMultiCurrencyEnabled == true)
            //{
            //    lblBranch.Enabled  =false;
            //    cboBranch.Enabled = false;
            //    chkIncludeCompany.Visible  = false;
            //    cboBranch.SelectedValue = 0;
            //}

            dtpFromDate.Value = ("01/" + GetCurrentMonth(dtpFromDate.Value.Month) + "/" + (dtpFromDate.Value.Year) + "").ToDateTime();
            dtpToDate.Value = ("01/" + GetCurrentMonth(dtpToDate.Value.Month) + "/" + (dtpToDate.Value.Year) + "").ToDateTime();

            cboCompany_SelectedIndexChanged(null, null);
        }
        #endregion

        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }

            return Months;
        }


        #region Methods

        private void FillCombos()
        {

            //if (ClsCommonSettings.IsMultiCurrencyEnabled)
            //{
            //    GetAllCompaniesMulti();
            //}
            //else
            //{
                FillAllCompanies();
            //}
            FillAllDepartments(); 
            FillPaymentTypes();
            FillDesignation();
            FillWorkStatus();
            FillTransactionType();
        }

        private void FillTransactionType()
        {
            cboTransactionType.DisplayMember = "TransactionType";
            cboTransactionType.ValueMember = "TransactionTypeID";
            cboTransactionType.DataSource = clsBLLReportCommon.GetTransactionType();
        }

        private void FillAllCompanies()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        private void GetAllCompaniesMulti()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompaniesMulti();
        }

        private void FillAllBranch()
        {
            try
            {
                cboBranch.DisplayMember = "Branch";
                cboBranch.ValueMember = "BranchID";
                cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());

                //if (ClsCommonSettings.IsMultiCurrencyEnabled == true)
                //{
                //    cboBranch.SelectedValue = 0;
                //}
            }
            catch
            {
            }


        }

        private void FillAllDepartments()
        {
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
        }

        private void FillAllEmployees()
        {
            //if (ClsCommonSettings.IsArabicView)
            //{

            //    cboEmployee.DisplayMember = "EmployeeFullName";
            //    cboEmployee.ValueMember = "EmployeeID";
            //    cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployeesArb(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked,
            //                                cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), -1, cboWorkStatus.SelectedValue.ToInt32(), -1);
            //}
            //else
            //{
                cboEmployee.DisplayMember = "EmployeeFullName";
                cboEmployee.ValueMember = "EmployeeID";
                cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked,
                                            cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), -1, cboWorkStatus.SelectedValue.ToInt32(), -1);

            //}

        }

        private void FillPaymentMonthYear()
        {
         
            cboMonthAndYear.ValueMember = "Value";
            cboMonthAndYear.DisplayMember = "Description";
            cboMonthAndYear.DataSource = clsBLLReportCommon.GetPaymentMonthYear(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32());

            if (cboMonthAndYear.Items.Count > 0)
                cboMonthAndYear.SelectedIndex = 0;
            else
                cboMonthAndYear.Text = "";
            
        }

        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }

        private void FillDesignation()
        {
            cboDesignation.DisplayMember = "Designation";
            cboDesignation.ValueMember = "DesignationID";
            cboDesignation.DataSource = clsBLLReportCommon.GetAllDesignation(); 
        }

        private void FillWorkStatus()
        {
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
            
        }

        private void ShowReport()
        {
            int Month = 0;
            int Year = 0;
            DataTable DtPayments = null;

            //setting the report path based on the payment type value

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    strMReportPath = cboType.SelectedIndex == 0 ? Application.StartupPath + "\\MainReports\\RptEmployeeMonthlyPaymentArb.rdlc"
            //        : Application.StartupPath + "\\MainReports\\RptEmployeeYearlyPaymentArb.rdlc";
            //}
            //else
            //{
                strMReportPath = cboType.SelectedIndex == 0 ? Application.StartupPath + "\\MainReports\\RptEmployeeMonthlyPayment.rdlc"
                    : Application.StartupPath + "\\MainReports\\RptEmployeeYearlyPayment.rdlc";
            //}
    

            //Setting the report header 
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            rvPayments.Reset();
            rvPayments.LocalReport.DataSources.Clear();
            rvPayments.LocalReport.ReportPath = strMReportPath;

            //Monthly Report
            if (cboType.SelectedIndex == 0)
            {
                Month = cboMonthAndYear.SelectedValue.ToString().Split('@')[0].ToInt16();
                Year = cboMonthAndYear.SelectedValue.ToString().Split('@')[1].ToInt16();

                rvPayments.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false),
                    new ReportParameter("CompanyName", cboCompany.Text , false),
                    new ReportParameter("BranchName",cboBranch.Text , false),
                    new ReportParameter("Department", cboDepartment.Text , false),
                    new ReportParameter("EmployeeName",cboEmployee.Text, false),
                    new ReportParameter("MonthAndYear",  cboMonthAndYear.Text , false),
                     new ReportParameter("Designation",cboDesignation.Text, false),
                    new ReportParameter("WorkStatus",  cboWorkStatus.Text , false)

                });



                DtPayments = clsBLLRptPayments.GetMonthlyPayments(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(),cboDesignation.SelectedValue.ToInt32(),cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), Month, Year, chkIncludeCompany.Checked,cboTransactionType.SelectedValue.ToInt32(),chkIsPayStructureOnly.Checked);
                if (DtPayments.Rows.Count > 0)
                {
                    rvPayments.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    rvPayments.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayments_EmployeeSalarySummary", DtPayments));
                }
            }

            else if (cboType.SelectedIndex == 1)
            {

                DtPayments = clsBLLRptPayments.GetSummaryPayments(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), dtpFromDate.Value, dtpToDate.Value, chkIncludeCompany.Checked, cboTransactionType.SelectedValue.ToInt32());

                DataTable dtCompanies =  DtPayments.DefaultView.ToTable(true, "CompanyName");  


                rvPayments.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false),
                    new ReportParameter("CompanyName", cboCompany.Text , false),
                    new ReportParameter("BranchName",cboBranch.Text , false),
                    new ReportParameter("Department", cboDepartment.Text , false),
                    new ReportParameter("EmployeeName",cboEmployee.Text, false),
                    new ReportParameter("FromDate",  dtpFromDate.Text, false),
                    new ReportParameter("ToDate", dtpToDate.Text  , false),
                       new ReportParameter("Designation",cboDesignation.Text, false),
                    new ReportParameter("WorkStatus",  cboWorkStatus.Text , false),
                    new ReportParameter("Count",  dtCompanies.Rows.Count.ToString() , false),

                });


                if (DtPayments.Rows.Count > 0)
                {
                    rvPayments.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    rvPayments.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayments_EmployeeYearlyPayment", DtPayments));
                }

            }

            //Show no record found
            if (DtPayments.Rows.Count == 0)
            {
                UserMessage.ShowMessage(9707);
                return;
            }

            //Set the report display mode and zoom percentage
            SetReportDisplay(rvPayments);

        }
       
        public bool FormValidation()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9708);
                return false;
            }
            else if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9709);
                return false;
            }
            else if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9710);
                return false;
            }

            else if (cboDesignation.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9112);
                return false;
            }
            else if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9113);
                return false;
            }

            else if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9711);
                return false;
            }
            else if (cboType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9728);
                return false;
            }

            else if ((cboType.SelectedIndex ==0) && (cboMonthAndYear.SelectedIndex == -1))
            {
                UserMessage.ShowMessage(9712);
                return false;
            }
            else if ((cboType.SelectedIndex == 1) && (dtpFromDate.Value.Date > dtpToDate.Value.Date))
            {
                UserMessage.ShowMessage(1761);
                return false;
            }

            return true;
        }

        private void FillPaymentTypes()
        {
            DataTable dt = new DataTable();
            DataRow dr;
            dt.Columns.Add("TypeID");
            dt.Columns.Add("Type");

            dr = dt.NewRow();
            dr["TypeID"] = 0;
            dr["Type"] = "Monthly";
            dt.Rows.InsertAt(dr, 0);


            dr = dt.NewRow();
            dr["TypeID"] = 1;
            dr["Type"] = "Summary";
            dt.Rows.InsertAt(dr, 1);



            cboType.DataSource = dt;
            cboType.DisplayMember = "Type";
            cboType.ValueMember = "TypeID";


        }

        #endregion

        #region ControlEvents

        private void btnShow_Click(object sender, EventArgs e)
        {
           
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (FormValidation())
                {
                    ShowReport();
                }
            }
            catch (Exception Ex)
            {
                btnShow.Enabled = true;
                this.Cursor = Cursors.WaitCursor;
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);

            }
            this.Cursor = Cursors.Default;

        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllBranch();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany(); 
            FillAllEmployees();
        }

        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableDates(); 
        }

        private void cboWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillPaymentMonthYear();
        }
        private void EnableDisableDates()
        {
            if (cboType.SelectedIndex == 0)
            {
                lblFromDate.Enabled = false;
                lblToDate.Enabled = false;
                dtpFromDate.Enabled = false;
                dtpToDate.Enabled = false;
                lblMonthAndYear.Enabled = true;
                cboMonthAndYear.Enabled = true;
                chkIsPayStructureOnly.Enabled = true; 

            }
            else
            {
                lblFromDate.Enabled = true;
                lblToDate.Enabled = true;
                dtpFromDate.Enabled = true;
                dtpToDate.Enabled = true;
                lblMonthAndYear.Enabled = false;
                cboMonthAndYear.Enabled = false;
                chkIsPayStructureOnly.Enabled = false;
                chkIsPayStructureOnly.Checked = false;

            }
        }

        private void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = false;
        }

        #endregion

        #region ReportViewer Events
        private void rvPayments_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;
        }

        private void rvPayments_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;
        }
        #endregion

        private void labelX3_Click(object sender, EventArgs e)
        {

        }
       
    }
}
