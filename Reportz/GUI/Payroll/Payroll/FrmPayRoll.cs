﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using Microsoft.Reporting.WinForms;
using Microsoft.VisualBasic;

namespace MyBooksERP
{
    public partial class FrmPayRoll : DevComponents.DotNetBar.Office2007Form
    {

    public int CompanyPayFlag;
    public int Division ;
    public  int CompanyID ;
    public String psMonth ="";
    public int piYear;
    public String EmpVenFlag = "";
    public String EmployeeID;
    public String MsReportPath= "";
    public String PstrType = "";
    public int RecordID ;
    public int  FlgEmployeeVendor ;
    public String MachineName  = "";
    public int MIncentiveEntryID ;
    public  String pWorkDays  = "";
    public String pLOP  = "";
    public String pLblLOP  = "";
    public int PEmpVenFlag ;
    public String pLOffDays  = "";
    public String pLLeaveDays  = "";
    public String pLHolidays = "";
    public int PiSelRowCount  = 0;
    public String pOffDays  = "";
    public String pLeaveDays = "";
    public String pHolidays  = "";
    public bool PbSummary  = true;
    public int pMonthlyMonth ;
    public DateAndTime  pPayDate ;
    public  DateAndTime pPayDateTo ;
    public String  pPayID ="";
    public String pSType = "";
    public int CurrencyID=0;
    public double  GrossAmount=0;
    public String pFormName = "";
    public bool PbMonthly = true;
    string PsReportFooter;
    int PiScale;
    public DataTable MdatLeaveDed;
    public DataTable MdatOTDed;
    public DataTable MdatHourDetail;
    public DataTable MdatConsequenceList;
    public DataTable MdatSalarySlipWorkInfo;
    public DataTable MdatMaster;
    public DataTable datDetailDed;
    public DataTable datDetail;
    public DataTable datMaster;
    private bool GlDisplayLeaveSummaryInSalarySlip;
   clsBLLSalarySlip MobjclsBLLSalarySlip;


        public FrmPayRoll()
        {
            InitializeComponent();
            GlDisplayLeaveSummaryInSalarySlip = ClsCommonSettings.GlDisplayLeaveSummaryInSalarySlip;
            PsReportFooter = ClsCommonSettings.ReportFooter;
            PiScale = ClsCommonSettings.Scale;
        }


    public void OneSubReportProcessingEventHandlerOT(Object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_OTPolicyDetailList", MdatOTDed));
    }
    public void OneSubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetLeaveDetailSalarySlip", MdatLeaveDed));
    }
    public void TwoSubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetHourlyDetail", MdatHourDetail));
    }
   public void ThreeSubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
   {
        e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetSalarySlipWorkInfo", MdatSalarySlipWorkInfo));
   }
    public void SubReportCompanyDisplayEventHandler(Object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipMaster", MdatMaster));
    }

    private void FrmPayRoll_Load(object sender, EventArgs e)
    {
        clsBLLSalarySlip MobjclsBLLSalarySlip = new clsBLLSalarySlip();

        this.Text = pFormName;
        MsReportPath = Application.StartupPath + "\\MainReports\\RptSalarySlip.rdlc";
        this.rptMisReportViewer.ProcessingMode = ProcessingMode.Local;
        this.rptMisReportViewer.LocalReport.ReportPath = MsReportPath;

        MobjclsBLLSalarySlip.clsDTOSalarySlip.strPayID = pPayID;
        MobjclsBLLSalarySlip.clsDTOSalarySlip.intCompanyPayFlag = CompanyPayFlag;
        MobjclsBLLSalarySlip.clsDTOSalarySlip.intDivisionID = Division;
        MobjclsBLLSalarySlip.clsDTOSalarySlip.intMonthInt = pMonthlyMonth;
        MobjclsBLLSalarySlip.clsDTOSalarySlip.intYearInt = piYear;
        MobjclsBLLSalarySlip.clsDTOSalarySlip.intCompanyID = CompanyID;

         MdatMaster = MobjclsBLLSalarySlip.datMaster();
         datMaster = MobjclsBLLSalarySlip.datMaster();
         datDetail = MobjclsBLLSalarySlip.datDetail();
         MdatLeaveDed = MobjclsBLLSalarySlip.MdatLeaveDed();
         datDetailDed = MobjclsBLLSalarySlip.datDetailDed();
         MdatOTDed = MobjclsBLLSalarySlip.MdatOTDed();
         MdatHourDetail = MobjclsBLLSalarySlip.MdatHourDetail();
         MdatSalarySlipWorkInfo = MobjclsBLLSalarySlip.MdatSalarySlipWorkInfo();


        Double dNetAmt = 0;
        ClsNumberToWords objConv = new ClsNumberToWords();

        String sQuey = "";
        String sCurMain = "";
        String sCurSub = "";
        String sAmtInWords = "";
        DataTable DTCurrencySubType = MobjclsBLLSalarySlip.CurrencySubType();

        if (DTCurrencySubType.Rows.Count > 0)
        {
            sCurMain = Convert.ToString(DTCurrencySubType.Rows[0]["CurrencySubType"]);
            sCurSub = Convert.ToString(DTCurrencySubType.Rows[0]["CurrencySubType1"]);
        }
        PbSummary = GlDisplayLeaveSummaryInSalarySlip;
        ReportParameter[] RepParam = new ReportParameter[4];
        RepParam[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
        RepParam[1] = new ReportParameter("SelRowCount", PiSelRowCount.ToString(), false);
        RepParam[2] = new ReportParameter("bSummary", Convert.ToBoolean(PbSummary).ToString(), false);
        RepParam[3] = new ReportParameter("bMonthly", PbMonthly.ToString(), false);

        this.rptMisReportViewer.LocalReport.SetParameters(RepParam);
        this.rptMisReportViewer.LocalReport.DataSources.Clear();

        this.rptMisReportViewer.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(OneSubReportProcessingEventHandler);
        this.rptMisReportViewer.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(TwoSubReportProcessingEventHandler);
        this.rptMisReportViewer.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(ThreeSubReportProcessingEventHandler);
        this.rptMisReportViewer.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportCompanyDisplayEventHandler);
        this.rptMisReportViewer.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(OneSubReportProcessingEventHandlerOT);


        this.rptMisReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(OneSubReportProcessingEventHandler);
        this.rptMisReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(TwoSubReportProcessingEventHandler);
        this.rptMisReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(ThreeSubReportProcessingEventHandler);
        this.rptMisReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportCompanyDisplayEventHandler);
        this.rptMisReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(OneSubReportProcessingEventHandlerOT);


        this.rptMisReportViewer.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipMaster", datMaster));
        this.rptMisReportViewer.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipDetail", datDetail));
        this.rptMisReportViewer.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipDetailDed", datDetailDed));
        this.rptMisReportViewer.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetLeaveDetailSalarySlip", MdatLeaveDed));
        this.rptMisReportViewer.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_OTPolicyDetailList", MdatOTDed));
        this.rptMisReportViewer.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetHourlyDetail", MdatHourDetail));
        this.rptMisReportViewer.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetSalarySlipWorkInfo", MdatSalarySlipWorkInfo));
        this.rptMisReportViewer.SetDisplayMode(DisplayMode.PrintLayout);
        this.rptMisReportViewer.ZoomMode = ZoomMode.Percent;
        this.rptMisReportViewer.ZoomPercent = 100;
    }



      
    }
}
