﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmBankPaymentRptSettings : Form
    { 
        
       public  int PintCompanyID;
       public int PintTemplateNo;
       public string PstrSubject;
       public string PstrAnnexure;
       public string PstrCompanyName;
       public string PstrMatter;
       public string PblnHead;

       clsBLLSalarySlip MobjclsBLLSalarySlip;
        public FrmBankPaymentRptSettings()
        {
            InitializeComponent();
            MobjclsBLLSalarySlip = new clsBLLSalarySlip();
        }
      private void ClearControls()
      {
        cboCompany.SelectedIndex = -1;
        txtSubject.Text = "";
        txtMatter.Text = "";
        txtAnnexure.Text = "";
        chkHeader.Checked = false;
        }

        private void LoadCombos()
        {
            DataTable datCombos = new DataTable();

            datCombos = null;
            datCombos = MobjclsBLLSalarySlip.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "", "CompanyID", "CompanyName" });
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DisplayMember = "CompanyName";
            cboCompany.DataSource = datCombos;

        }

        private void SetValues()
        {
            PintCompanyID =Convert.ToInt32(cboCompany.SelectedValue);
            PstrCompanyName = "For " + cboCompany.Text;
            PstrSubject = txtSubject.Text.ToString().Trim();
            PstrAnnexure = txtAnnexure.ToString().Trim();
            PstrMatter = txtMatter.Text.ToString().Trim();
            PblnHead = Convert.ToString(chkHeader.Checked);
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
                 SetValues();
                 this.Close();
        }

        private void FrmBankPaymentRptSettings_Load(object sender, EventArgs e)
        {
            LoadCombos();
            cboCompany.SelectedValue = PintCompanyID;
           
            txtMatter.Text = PstrMatter;
            txtSubject.Text = PstrSubject;
            txtAnnexure.Text = PstrAnnexure;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
