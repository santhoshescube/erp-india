﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Data.SqlClient;
using System.IO;


namespace MyBooksERP
{
    public partial class FrmBankTransfer : Form
    {

        public int PiCompanyID;
        public int PiCurrencyID;
        public int PiCurrencyIDFilter;
        public string PstrType;
        public string PdtProcessedDate;
        public int FlgEmployeeVendor;
        public string MachineName;
        public string pFormName;
        string MsReportPath;
        public int TemplateNo;
        clsBLLSalarySlip MobjclsBLLSalarySlip;
        FrmBankPaymentRptSettings MObjFrmBankPaymentRptSettings = new FrmBankPaymentRptSettings();

        public FrmBankTransfer()
        {
            InitializeComponent();
            MobjclsBLLSalarySlip = new clsBLLSalarySlip();
        }

        private void ReportSettings()
        {
            MObjFrmBankPaymentRptSettings.PintTemplateNo = TemplateNo;
            if (TemplateNo == 1)
            {
                MObjFrmBankPaymentRptSettings.PintCompanyID = PiCompanyID;
                MObjFrmBankPaymentRptSettings.PstrSubject = "Transfer Of Salary Account";
                MObjFrmBankPaymentRptSettings.PstrMatter = "Please find here with the attachment of salary amount to be credited.";
                MObjFrmBankPaymentRptSettings.Text = "Bank Transfer Letter" + " - " + Template1ToolStripMenuItem.Text;
            }
            else if (TemplateNo == 2)
            {
                MObjFrmBankPaymentRptSettings.PintCompanyID = PiCompanyID;
                MObjFrmBankPaymentRptSettings.PstrSubject = "Transfer Of Salary Account";
                MObjFrmBankPaymentRptSettings.PstrAnnexure = "Annexure to Bank Transfer Letter";
                MObjFrmBankPaymentRptSettings.PstrMatter = "Please find here with the attachment of salary amount to be credited.";
                MObjFrmBankPaymentRptSettings.Text = "Bank Transfer Letter" + " - " + Template2ToolStripMenuItem.Text;
            }
            else
            {
                MObjFrmBankPaymentRptSettings.PintCompanyID = PiCompanyID;
                MObjFrmBankPaymentRptSettings.PstrSubject = "Transfer Of Salary Account";
                MObjFrmBankPaymentRptSettings.PstrAnnexure = "Annexure to Bank Transfer Letter";
                MObjFrmBankPaymentRptSettings.PstrMatter = "Please find here with the attachment of salary amount to be credited.";
                MObjFrmBankPaymentRptSettings.Text = "Bank Transfer Letter" + " - " + Template3ToolStripMenuItem.Text;
            }
        }



        private void btnPreview_Click(System.Object sender, System.EventArgs e)
        {

            string StrBankName = ""; string TSQL = ""; string StrBranchName = ""; string StrAddress = ""; string StrTelephone = ""; int BankID = 0; string StrCompanyName = ""; string StrAnnexure = "";
            ReportViewerBank.Reset();
            ReportViewerBank.Clear();

    
            string StrName  = "";
            string StrPOBox  = "";
            string StrRoadArea  = "";
            string StrBlockCity = "";
            string StrPhonePABXNo = "";
            string StrWebSite  = "";
            DataTable DT;
            DataTable datComp;
            DataTable datSalarySlip;
            DataTable datSalarySlipDetails;
            if (TemplateNo == 1)
            {

                this.ReportViewerBank.LocalReport.DataSources.Clear();
                MsReportPath = Application.StartupPath + "\\MainReports\\RptBankPayment.rdlc";

                if (!System.IO.File.Exists(MsReportPath))
                {
                    MessageBox.Show("Report File Not Found");
                    return;
                }


                this.ReportViewerBank.ProcessingMode = ProcessingMode.Local;
                this.ReportViewerBank.LocalReport.ReportPath = MsReportPath;

                ReportParameter[] RepParam = new ReportParameter[8];
                RepParam[0] = new ReportParameter("subject", MObjFrmBankPaymentRptSettings.PstrSubject, false);
                RepParam[1] = new ReportParameter("matterPara", MObjFrmBankPaymentRptSettings.PstrMatter, false);
                RepParam[2] = new ReportParameter("BankName", StrBankName, false);
                RepParam[3] = new ReportParameter("BranchName", StrBranchName, false);
                RepParam[4] = new ReportParameter("Address", StrAddress, false);
                RepParam[5] = new ReportParameter("Telephone", StrTelephone, false);
                RepParam[6] = new ReportParameter("CompanyName", MObjFrmBankPaymentRptSettings.PstrCompanyName, true);
                RepParam[7] = new ReportParameter("chHeader", "true", false);


                //RepParam[7] = new ReportParameter("chHeader", MObjFrmBankPaymentRptSettings.PblnHead.ToString().Trim()  , false);


                this.ReportViewerBank.LocalReport.SetParameters(RepParam);



                MobjclsBLLSalarySlip.clsDTOSalarySlip.intCompanyID = PiCompanyID;
                MobjclsBLLSalarySlip.clsDTOSalarySlip.strProcessDate = PdtProcessedDate;
                datComp = MobjclsBLLSalarySlip.GetCompanyDetail();
                datSalarySlip = MobjclsBLLSalarySlip.GetPayment();

                datSalarySlipDetails = datSalarySlip;

                if (datSalarySlip == null || datSalarySlip.Rows.Count < 1)
                {
                    return;
                }

                this.ReportViewerBank.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetCompanyCommon_GaReportCompanyForm", datComp));
                this.ReportViewerBank.LocalReport.DataSources.Add(new ReportDataSource("DtSetDisplayPayment_DisplayWordReportMaster", datSalarySlip));
                this.ReportViewerBank.LocalReport.DataSources.Add(new ReportDataSource("DtSetDisplayPayment_DisplayWordReport", datSalarySlipDetails));

            }
            else if(TemplateNo == 2)
            {

                this.ReportViewerBank.LocalReport.DataSources.Clear();
                MsReportPath = Application.StartupPath + "\\MainReports\\RptBankPayment1.rdlc";
                  if (!System.IO.File.Exists(MsReportPath))
                  {
                    MessageBox.Show("Report File Not Found");
                   return;
                  }


                this.ReportViewerBank.ProcessingMode = ProcessingMode.Local;
                this.ReportViewerBank.LocalReport.ReportPath = MsReportPath;

                ReportParameter[] RepParam = new ReportParameter[9];
                RepParam[0] = new ReportParameter("subject", MObjFrmBankPaymentRptSettings.PstrSubject, false);
                RepParam[1] = new ReportParameter("matterPara", MObjFrmBankPaymentRptSettings.PstrMatter, false);
                RepParam[2] = new ReportParameter("BankName", StrBankName, false);
                RepParam[3] = new ReportParameter("BranchName", StrBranchName, false);
                RepParam[4] = new ReportParameter("Address", StrAddress, false);
                RepParam[5] = new ReportParameter("Telephone", StrTelephone, false);
                RepParam[6] = new ReportParameter("CompanyName", MObjFrmBankPaymentRptSettings.PstrCompanyName, true);
                RepParam[7] = new ReportParameter("chHeader", MObjFrmBankPaymentRptSettings.PblnHead, false);
                RepParam[8] = new ReportParameter("Annexure", MObjFrmBankPaymentRptSettings.PstrAnnexure, false);


                this.ReportViewerBank.LocalReport.SetParameters(RepParam);

  

                MobjclsBLLSalarySlip.clsDTOSalarySlip.intCompanyID = PiCompanyID;
                MobjclsBLLSalarySlip.clsDTOSalarySlip.strProcessDate = PdtProcessedDate;
                datComp = MobjclsBLLSalarySlip.GetCompanyDetail();
                datSalarySlipDetails = MobjclsBLLSalarySlip.GetPayment();
                datSalarySlip = MobjclsBLLSalarySlip.GetPayment();

                if (datSalarySlip == null || datSalarySlip.Rows.Count < 1)
                {
                    return;
                }
                this.ReportViewerBank.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetCompanyCommon_GaReportCompanyForm", datComp));
                this.ReportViewerBank.LocalReport.DataSources.Add(new ReportDataSource("DtSetDisplayPayment_DisplayWordReportMaster", datSalarySlip));
                this.ReportViewerBank.LocalReport.DataSources.Add(new ReportDataSource("DtSetDisplayPayment_DisplayWordReport", datSalarySlipDetails));

            }
            this.ReportViewerBank.LocalReport.Refresh();
            this.ReportViewerBank.SetDisplayMode(DisplayMode.PrintLayout);
            this.ReportViewerBank.ZoomMode = ZoomMode.Percent;
            this.ReportViewerBank.ZoomPercent = 100;


        }

        private void Template1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TemplateNo = 1;
            ReportSettings();
            btnPreview_Click(sender, e);
        }

        private void Template2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TemplateNo = 2;
            ReportSettings();
            btnPreview_Click(sender, e);
        }

        private void btnReportSettings_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(TemplateNo) < 1)
            {
                TemplateNo = 1;
            }
            MObjFrmBankPaymentRptSettings.PintCompanyID = PiCompanyID;
            MObjFrmBankPaymentRptSettings.ShowDialog();
            ReportViewerBank.Reset();
            ReportViewerBank.Clear();
            btnPreview_Click(sender, e);
        }

    }
}