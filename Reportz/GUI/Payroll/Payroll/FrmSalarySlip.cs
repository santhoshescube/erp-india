﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
namespace MyBooksERP
{
    public partial class FrmSalarySlip : Report
    {
        private clsMessage ObjUserMessage = null;
        private string strMReportPath = "";
    

        private bool GlDisplayLeaveSummaryInSalarySlip;
        int MintCompanyId;
        int MintUserId;
        int PiSelRowCount=0;
        string PsReportFooter;
        int PiScale;
        bool PbSummary = true;
        bool PbMonthly = true;
     
        ClsLogWriter mObjLogs;
        ClsNotification mObjNotification;

        public DataTable MdatLeaveDed;
        public DataTable MdatOTDed;
        public DataTable MdatHourDetail;
        public DataTable MdatConsequenceList;
        public DataTable MdatSalarySlipWorkInfo;
        public DataTable MdatMaster;
        public DataTable datDetailDed;
        public DataTable datDetail;
        public DataTable datMaster;
        String pPayID="";
        clsBLLSalarySlip MobjclsBLLSalarySlip;

        public FrmSalarySlip()
        {
            InitializeComponent();
            
            MintCompanyId = ClsCommonSettings.LoginCompanyID;
            MintUserId = ClsCommonSettings.UserID;
          
           
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
           
            PsReportFooter = ClsCommonSettings.ReportFooter;
            PiScale = ClsCommonSettings.Scale;
            MobjclsBLLSalarySlip = new clsBLLSalarySlip();
            GlDisplayLeaveSummaryInSalarySlip = ClsCommonSettings.GlDisplayLeaveSummaryInSalarySlip;

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}




        }
        #region Message


        //#region SetArabicControls
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.SalarySlipMISReport, this);
        //}
        //#endregion SetArabicControls

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.SalarySlipMISReport);
                return this.ObjUserMessage;
            }
        }
        #endregion

        #region DateTimePicker Value Setting
        private void DtpCurrentMonth_ValueChanged(object sender, EventArgs e)
        {
            int iMonth = DtpCurrentMonth.Value.Date.Month;
            int iYear = DtpCurrentMonth.Value.Date.Year;
            DtpCurrentMonth.Value = Convert.ToDateTime("01-" + Convert.ToString(GetCurrentMonthStr(iMonth)) + "-" + Convert.ToString(iYear) + "");

        }
        private string  GetCurrentMonthStr(int MonthVal) 
      {
        string Months;
        Months = "";

            switch (MonthVal)
            {
                case 1: 
                   Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                 case 3:
                    Months = "Mar";
                    break;
                 case 4:
                    Months = "Apr";
                    break;
                 case 5:
                    Months = "May";
                    break;
                 case 6:
                    Months = "Jun";
                    break;
                 case 7:
                    Months = "Jul";
                    break;
                 case 8:
                    Months = "Aug";
                    break;
                 case 9:
                    Months = "Sep";
                    break;
                 case 10:
                    Months = "Oct";
                    break;
                 case 11:
                    Months = "Nov";
                    break;
                 case 12:
                    Months = "Dec";
                    break;
             
            }
            return Months;
      }
        #endregion

        #region Functions
        private void LoadCombos()
        {
            FillAllCompany();
            FillAllDeparments();
            FillAllWorkStatus();
        }
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }
        private void FillAllCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());

        }
        private void FillAllDeparments()
        {
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
        }
        private void FillAllWorkStatus()
        {
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();

            if (cboWorkStatus.Items.IndexOf((int)EmployeeWorkStatus.InService) >= 0)
                cboWorkStatus.SelectedValue = (int)EmployeeWorkStatus.InService;
        }
        private void FillAllEmployees()
        {
            CboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                            chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), -1, -1, cboWorkStatus.SelectedValue.ToInt32(), -1
                                            );
            CboEmployee.DisplayMember = "EmployeeFullName";
            CboEmployee.ValueMember = "EmployeeID";
        }
        private void LoadEmployee(object sender, EventArgs e)
        {
            FillAllEmployees();
        }
         #endregion

        #region Control KeydownEvents
        private void cbo_Keydown(object sender, KeyEventArgs e)
        {
            ComboBox cbo = sender as ComboBox;
            cbo.DroppedDown = false;
        }
        #endregion

        #region Validations
          private bool FormValidations()
    {
        if (cboCompany.SelectedValue.ToInt32() <= 0)
        {
            UserMessage.ShowMessage(9100);
            return false;
        }
        if (cboBranch.SelectedIndex == -1)
        {
            UserMessage.ShowMessage(9141);
            return false;
        }
        if (cboDepartment.SelectedIndex == -1)
        {
            UserMessage.ShowMessage(9109);
            return false;
        }
        if (cboWorkStatus.SelectedIndex == -1)
        {
            UserMessage.ShowMessage(9113);
            return false;
        }
        if (CboEmployee.SelectedIndex == -1)
        {
            UserMessage.ShowMessage(9124);
            return false;
        }

        return true;
    }
        #endregion

        #region FormLoad
        private void FrmSalarySlip_Load(object sender, EventArgs e)
        {
            chkIncludeCompany.Checked = true;
            LoadCombos();
            SetDefaultBranch(cboBranch);
            SetDefaultWorkStatus(cboWorkStatus);
            DtpCurrentMonth.Value = ("01/" + GetCurrentMonth(DtpCurrentMonth.Value.Month) + "/" + (DtpCurrentMonth.Value.Year) + "").ToDateTime();
            cboCompany_SelectedIndexChanged(null, null);
        }
         #endregion
        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }

            return Months;
        }
        #region Reportload Control Event
         private void bntShow_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            RptView.Reset();
            if (FormValidations())
            {
                if (chkCombined.Checked)
                {
                    ShowReportSum();
                }
                else
                {
                    ShowReport();
                }
            }
            this.Cursor = Cursors.Default;
        }
        #endregion

        #region ControlEvents
         private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllBranches();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllEmployees();
        }
        #endregion

        #region SubReportprocessing Events
        public void OneSubReportProcessingEventHandlerOT(Object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_OTPolicyDetailList", MdatOTDed));
        }
        public void OneSubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetLeaveDetailSalarySlip", MdatLeaveDed));
        }
        public void TwoSubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetHourlyDetail", MdatHourDetail));
        }
        public void ThreeSubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetSalarySlipWorkInfo", MdatSalarySlipWorkInfo));
        }
        public void SubReportCompanyDisplayEventHandler(Object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipMaster", MdatMaster));
        }

        #endregion

        #region ReportViewer Events
        private void RptView_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;
        }

        private void RptView_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;
        }
        #endregion


        private void ShowReport()
        {
            this.RptView.LocalReport.DataSources.Clear();
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intYearInt = 0;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intMonthInt = 0;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intCompanyID = 0;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intDepartmentID = 0;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intDesignationID = 0;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intEmployeeID = 0;

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    strMReportPath = Application.StartupPath + "\\MainReports\\RptSalarySlipArb.rdlc";

            //}
            //else
            //{
                strMReportPath = Application.StartupPath + "\\MainReports\\RptSalarySlip.rdlc";

            //}


            this.RptView.ProcessingMode = ProcessingMode.Local;
            this.RptView.LocalReport.ReportPath = strMReportPath;

            MobjclsBLLSalarySlip.clsDTOSalarySlip.intYearInt = Convert.ToInt32(DtpCurrentMonth.Value.Date.Year);
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intMonthInt = Convert.ToInt32(DtpCurrentMonth.Value.Date.Month);
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);

            if (cboDepartment.SelectedIndex != -1)
            {
                MobjclsBLLSalarySlip.clsDTOSalarySlip.intDepartmentID = Convert.ToInt32(cboDepartment.SelectedValue);
            }
            //if (cboDesignation.SelectedIndex != -1)
            //{
            //    MobjclsBLLSalarySlip.clsDTOSalarySlip.intDesignationID = Convert.ToInt32(cboDesignation.SelectedValue);
            //}
            if (CboEmployee.SelectedIndex != -1)
            {
                MobjclsBLLSalarySlip.clsDTOSalarySlip.intEmployeeID = Convert.ToInt32(CboEmployee.SelectedValue);
            }

            MobjclsBLLSalarySlip.clsDTOSalarySlip.WorkStatusID = cboWorkStatus.SelectedValue.ToInt16();
            MobjclsBLLSalarySlip.clsDTOSalarySlip.BranchID = cboBranch.SelectedValue.ToInt16();
            MobjclsBLLSalarySlip.clsDTOSalarySlip.IncludeCompany = chkIncludeCompany.Checked;

            pPayID = MobjclsBLLSalarySlip.GetPaymentIDList();

            MobjclsBLLSalarySlip.clsDTOSalarySlip.strPayID = pPayID;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intCompanyPayFlag = 1;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intDivisionID = 0;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intMonthInt = DtpCurrentMonth.Value.Date.Month;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intYearInt = DtpCurrentMonth.Value.Date.Year;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
           


            MdatMaster = MobjclsBLLSalarySlip.datMaster();
            if (MdatMaster == null)
            {
                return;
            }
            if (MdatMaster.Rows.Count <= 0)
            {
                UserMessage.ShowMessage(9015);//NO INFORMATION FOUND 
                return;
            }
            
            datMaster = MobjclsBLLSalarySlip.datMaster();
            datDetail = MobjclsBLLSalarySlip.datDetail();
            MdatLeaveDed = MobjclsBLLSalarySlip.MdatLeaveDed();
            datDetailDed = MobjclsBLLSalarySlip.datDetailDed();
            MdatOTDed = MobjclsBLLSalarySlip.MdatOTDed();
            MdatHourDetail = MobjclsBLLSalarySlip.MdatHourDetail();
            MdatSalarySlipWorkInfo = MobjclsBLLSalarySlip.MdatSalarySlipWorkInfo();
            PbSummary = GlDisplayLeaveSummaryInSalarySlip;
            ReportParameter[] RepParam = new ReportParameter[4];
            RepParam[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
            RepParam[1] = new ReportParameter("SelRowCount", PiSelRowCount.ToString(), false);
            RepParam[2] = new ReportParameter("bSummary", Convert.ToBoolean(PbSummary).ToString(), false);
            RepParam[3] = new ReportParameter("bMonthly", Convert.ToBoolean(PbMonthly).ToString(), false);

            this.RptView.LocalReport.SetParameters(RepParam);
            //this.RptView.LocalReport.DataSources.Clear();

            this.RptView.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(OneSubReportProcessingEventHandler);
            this.RptView.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(TwoSubReportProcessingEventHandler);
            this.RptView.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(ThreeSubReportProcessingEventHandler);
            this.RptView.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportCompanyDisplayEventHandler);
            this.RptView.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(OneSubReportProcessingEventHandlerOT);


            this.RptView.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(OneSubReportProcessingEventHandler);
            this.RptView.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(TwoSubReportProcessingEventHandler);
            this.RptView.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(ThreeSubReportProcessingEventHandler);
            this.RptView.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportCompanyDisplayEventHandler);
            this.RptView.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(OneSubReportProcessingEventHandlerOT);


            this.RptView.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipMaster", datMaster));
            this.RptView.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipDetail", datDetail));
            this.RptView.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipDetailDed", datDetailDed));
            this.RptView.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetLeaveDetailSalarySlip", MdatLeaveDed));
            this.RptView.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_OTPolicyDetailList", MdatOTDed));
            this.RptView.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetHourlyDetail", MdatHourDetail));
            this.RptView.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetSalarySlipWorkInfo", MdatSalarySlipWorkInfo));

            SetReportDisplay(RptView);
            Cursor.Current = Cursors.Default;
        }
        private void ShowReportSum()
        {
            this.RptView.LocalReport.DataSources.Clear();
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intYearInt = 0;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intMonthInt = 0;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intCompanyID = 0;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intDepartmentID = 0;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intDesignationID = 0;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intEmployeeID = 0;

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    strMReportPath = Application.StartupPath + "\\MainReports\\RptSalarySlipArb.rdlc";

            //}
            //else
            //{
                strMReportPath = Application.StartupPath + "\\MainReports\\RptSalarySlip.rdlc";

            //}


            this.RptView.ProcessingMode = ProcessingMode.Local;
            this.RptView.LocalReport.ReportPath = strMReportPath;

            MobjclsBLLSalarySlip.clsDTOSalarySlip.intYearInt = Convert.ToInt32(DtpCurrentMonth.Value.Date.Year);
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intMonthInt = Convert.ToInt32(DtpCurrentMonth.Value.Date.Month);
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);

            if (cboDepartment.SelectedIndex != -1)
            {
                MobjclsBLLSalarySlip.clsDTOSalarySlip.intDepartmentID = Convert.ToInt32(cboDepartment.SelectedValue);
            }
            //if (cboDesignation.SelectedIndex != -1)
            //{
            //    MobjclsBLLSalarySlip.clsDTOSalarySlip.intDesignationID = Convert.ToInt32(cboDesignation.SelectedValue);
            //}
            if (CboEmployee.SelectedIndex != -1)
            {
                MobjclsBLLSalarySlip.clsDTOSalarySlip.intEmployeeID = Convert.ToInt32(CboEmployee.SelectedValue);
            }

            MobjclsBLLSalarySlip.clsDTOSalarySlip.WorkStatusID = cboWorkStatus.SelectedValue.ToInt16();
            MobjclsBLLSalarySlip.clsDTOSalarySlip.BranchID = cboBranch.SelectedValue.ToInt16();
            MobjclsBLLSalarySlip.clsDTOSalarySlip.IncludeCompany = chkIncludeCompany.Checked;

            pPayID = MobjclsBLLSalarySlip.GetPaymentIDList();

            MobjclsBLLSalarySlip.clsDTOSalarySlip.strPayID = pPayID;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intCompanyPayFlag = 1;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intDivisionID = 0;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intMonthInt = DtpCurrentMonth.Value.Date.Month;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intYearInt = DtpCurrentMonth.Value.Date.Year;
            MobjclsBLLSalarySlip.clsDTOSalarySlip.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);



            MdatMaster = MobjclsBLLSalarySlip.datMasterSum();
            if (MdatMaster == null)
            {
                return;
            }
            if (MdatMaster.Rows.Count <= 0)
            {
                UserMessage.ShowMessage(9015);//NO INFORMATION FOUND 
                return;
            }

            datMaster = MobjclsBLLSalarySlip.datMasterSum();
            datDetail = MobjclsBLLSalarySlip.datDetail();
            MdatLeaveDed = MobjclsBLLSalarySlip.MdatLeaveDed();
            datDetailDed = MobjclsBLLSalarySlip.datDetailDed();
            MdatOTDed = MobjclsBLLSalarySlip.MdatOTDed();
            MdatHourDetail = MobjclsBLLSalarySlip.MdatHourDetail();
            MdatSalarySlipWorkInfo = MobjclsBLLSalarySlip.MdatSalarySlipWorkInfo();
            PbSummary = GlDisplayLeaveSummaryInSalarySlip;
            ReportParameter[] RepParam = new ReportParameter[4];
            RepParam[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
            RepParam[1] = new ReportParameter("SelRowCount", PiSelRowCount.ToString(), false);
            RepParam[2] = new ReportParameter("bSummary", Convert.ToBoolean(PbSummary).ToString(), false);
            RepParam[3] = new ReportParameter("bMonthly", Convert.ToBoolean(PbMonthly).ToString(), false);

            this.RptView.LocalReport.SetParameters(RepParam);
            //this.RptView.LocalReport.DataSources.Clear();

            this.RptView.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(OneSubReportProcessingEventHandler);
            this.RptView.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(TwoSubReportProcessingEventHandler);
            this.RptView.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(ThreeSubReportProcessingEventHandler);
            this.RptView.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportCompanyDisplayEventHandler);
            this.RptView.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(OneSubReportProcessingEventHandlerOT);


            this.RptView.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(OneSubReportProcessingEventHandler);
            this.RptView.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(TwoSubReportProcessingEventHandler);
            this.RptView.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(ThreeSubReportProcessingEventHandler);
            this.RptView.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportCompanyDisplayEventHandler);
            this.RptView.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(OneSubReportProcessingEventHandlerOT);


            this.RptView.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipMaster", datMaster));
            this.RptView.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipDetail", datDetail));
            this.RptView.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipDetailDed", datDetailDed));
            this.RptView.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetLeaveDetailSalarySlip", MdatLeaveDed));
            this.RptView.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_OTPolicyDetailList", MdatOTDed));
            this.RptView.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetHourlyDetail", MdatHourDetail));
            this.RptView.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetSalarySlipWorkInfo", MdatSalarySlipWorkInfo));

            SetReportDisplay(RptView);
            Cursor.Current = Cursors.Default;
        }
        
    }

 }

