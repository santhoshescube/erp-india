﻿namespace MyBooksERP
{
    partial class FrmRptSalaryStructure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptSalaryStructure));
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.cboEmployee = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblEmployee = new DevComponents.DotNetBar.LabelX();
            this.cboDesignation = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblDesignation = new DevComponents.DotNetBar.LabelX();
            this.btnShow = new DevComponents.DotNetBar.ButtonX();
            this.cboType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.cboEmployeementType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblEmployeementType = new DevComponents.DotNetBar.LabelX();
            this.lblType = new DevComponents.DotNetBar.LabelX();
            this.cboWorkStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblWorkStatus = new DevComponents.DotNetBar.LabelX();
            this.cboDepartment = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblDepartment = new DevComponents.DotNetBar.LabelX();
            this.chkIncludeCompany = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cboBranch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblBranch = new DevComponents.DotNetBar.LabelX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.History = new DevComponents.Editors.ComboItem();
            this.Salary = new DevComponents.Editors.ComboItem();
            this.ReportViewerSummary = new Microsoft.Reporting.WinForms.ReportViewer();
            this.expandablePanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.cboEmployee);
            this.expandablePanel1.Controls.Add(this.lblEmployee);
            this.expandablePanel1.Controls.Add(this.cboDesignation);
            this.expandablePanel1.Controls.Add(this.lblDesignation);
            this.expandablePanel1.Controls.Add(this.btnShow);
            this.expandablePanel1.Controls.Add(this.cboType);
            this.expandablePanel1.Controls.Add(this.cboEmployeementType);
            this.expandablePanel1.Controls.Add(this.lblEmployeementType);
            this.expandablePanel1.Controls.Add(this.lblType);
            this.expandablePanel1.Controls.Add(this.cboWorkStatus);
            this.expandablePanel1.Controls.Add(this.lblWorkStatus);
            this.expandablePanel1.Controls.Add(this.cboDepartment);
            this.expandablePanel1.Controls.Add(this.lblDepartment);
            this.expandablePanel1.Controls.Add(this.chkIncludeCompany);
            this.expandablePanel1.Controls.Add(this.cboBranch);
            this.expandablePanel1.Controls.Add(this.lblBranch);
            this.expandablePanel1.Controls.Add(this.cboCompany);
            this.expandablePanel1.Controls.Add(this.lblCompany);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(1203, 107);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 1;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By  ";
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.DisplayMember = "Text";
            this.cboEmployee.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.ItemHeight = 14;
            this.cboEmployee.Location = new System.Drawing.Point(912, 58);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(175, 20);
            this.cboEmployee.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboEmployee.TabIndex = 160;
            this.cboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            // 
            // 
            // 
            this.lblEmployee.BackgroundStyle.Class = "";
            this.lblEmployee.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblEmployee.Location = new System.Drawing.Point(846, 61);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(51, 15);
            this.lblEmployee.TabIndex = 159;
            this.lblEmployee.Text = "Employee";
            // 
            // cboDesignation
            // 
            this.cboDesignation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDesignation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDesignation.DisplayMember = "Text";
            this.cboDesignation.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDesignation.DropDownHeight = 134;
            this.cboDesignation.FormattingEnabled = true;
            this.cboDesignation.IntegralHeight = false;
            this.cboDesignation.ItemHeight = 14;
            this.cboDesignation.Location = new System.Drawing.Point(350, 58);
            this.cboDesignation.Name = "cboDesignation";
            this.cboDesignation.Size = new System.Drawing.Size(177, 20);
            this.cboDesignation.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboDesignation.TabIndex = 158;
            this.cboDesignation.SelectedIndexChanged += new System.EventHandler(this.LoadEmployee);
            this.cboDesignation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // lblDesignation
            // 
            this.lblDesignation.AutoSize = true;
            // 
            // 
            // 
            this.lblDesignation.BackgroundStyle.Class = "";
            this.lblDesignation.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDesignation.Location = new System.Drawing.Point(281, 58);
            this.lblDesignation.Name = "lblDesignation";
            this.lblDesignation.Size = new System.Drawing.Size(61, 15);
            this.lblDesignation.TabIndex = 157;
            this.lblDesignation.Text = "Designation";
            // 
            // btnShow
            // 
            this.btnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShow.Location = new System.Drawing.Point(1136, 39);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(64, 42);
            this.btnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShow.TabIndex = 144;
            this.btnShow.Text = "Show";
            this.btnShow.Tooltip = "Show Report";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // cboType
            // 
            this.cboType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboType.DisplayMember = "Text";
            this.cboType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboType.DropDownHeight = 134;
            this.cboType.FormattingEnabled = true;
            this.cboType.IntegralHeight = false;
            this.cboType.ItemHeight = 14;
            this.cboType.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2});
            this.cboType.Location = new System.Drawing.Point(912, 36);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(177, 20);
            this.cboType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboType.TabIndex = 30;
            this.cboType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "Salary";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "History";
            // 
            // cboEmployeementType
            // 
            this.cboEmployeementType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployeementType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployeementType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboEmployeementType.DropDownHeight = 134;
            this.cboEmployeementType.FormattingEnabled = true;
            this.cboEmployeementType.IntegralHeight = false;
            this.cboEmployeementType.ItemHeight = 14;
            this.cboEmployeementType.Location = new System.Drawing.Point(647, 58);
            this.cboEmployeementType.Name = "cboEmployeementType";
            this.cboEmployeementType.Size = new System.Drawing.Size(177, 20);
            this.cboEmployeementType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboEmployeementType.TabIndex = 29;
            this.cboEmployeementType.SelectedIndexChanged += new System.EventHandler(this.LoadEmployee);
            this.cboEmployeementType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // lblEmployeementType
            // 
            this.lblEmployeementType.AutoSize = true;
            // 
            // 
            // 
            this.lblEmployeementType.BackgroundStyle.Class = "";
            this.lblEmployeementType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblEmployeementType.Location = new System.Drawing.Point(551, 59);
            this.lblEmployeementType.Name = "lblEmployeementType";
            this.lblEmployeementType.Size = new System.Drawing.Size(88, 15);
            this.lblEmployeementType.TabIndex = 28;
            this.lblEmployeementType.Text = "EmploymentType";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            // 
            // 
            // 
            this.lblType.BackgroundStyle.Class = "";
            this.lblType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblType.Location = new System.Drawing.Point(847, 35);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(27, 15);
            this.lblType.TabIndex = 26;
            this.lblType.Text = "Type";
            // 
            // cboWorkStatus
            // 
            this.cboWorkStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkStatus.DisplayMember = "Text";
            this.cboWorkStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWorkStatus.DropDownHeight = 134;
            this.cboWorkStatus.FormattingEnabled = true;
            this.cboWorkStatus.IntegralHeight = false;
            this.cboWorkStatus.ItemHeight = 14;
            this.cboWorkStatus.Location = new System.Drawing.Point(646, 36);
            this.cboWorkStatus.Name = "cboWorkStatus";
            this.cboWorkStatus.Size = new System.Drawing.Size(177, 20);
            this.cboWorkStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboWorkStatus.TabIndex = 25;
            this.cboWorkStatus.SelectedIndexChanged += new System.EventHandler(this.LoadEmployee);
            this.cboWorkStatus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // lblWorkStatus
            // 
            this.lblWorkStatus.AutoSize = true;
            // 
            // 
            // 
            this.lblWorkStatus.BackgroundStyle.Class = "";
            this.lblWorkStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWorkStatus.Location = new System.Drawing.Point(552, 34);
            this.lblWorkStatus.Name = "lblWorkStatus";
            this.lblWorkStatus.Size = new System.Drawing.Size(62, 15);
            this.lblWorkStatus.TabIndex = 24;
            this.lblWorkStatus.Text = "Work Status";
            // 
            // cboDepartment
            // 
            this.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDepartment.DisplayMember = "Text";
            this.cboDepartment.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDepartment.DropDownHeight = 134;
            this.cboDepartment.FormattingEnabled = true;
            this.cboDepartment.IntegralHeight = false;
            this.cboDepartment.ItemHeight = 14;
            this.cboDepartment.Location = new System.Drawing.Point(350, 36);
            this.cboDepartment.Name = "cboDepartment";
            this.cboDepartment.Size = new System.Drawing.Size(177, 20);
            this.cboDepartment.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboDepartment.TabIndex = 23;
            this.cboDepartment.SelectedIndexChanged += new System.EventHandler(this.LoadEmployee);
            this.cboDepartment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            // 
            // 
            // 
            this.lblDepartment.BackgroundStyle.Class = "";
            this.lblDepartment.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDepartment.Location = new System.Drawing.Point(281, 37);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(60, 15);
            this.lblDepartment.TabIndex = 22;
            this.lblDepartment.Text = "Department";
            // 
            // chkIncludeCompany
            // 
            this.chkIncludeCompany.AutoSize = true;
            // 
            // 
            // 
            this.chkIncludeCompany.BackgroundStyle.Class = "";
            this.chkIncludeCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkIncludeCompany.Location = new System.Drawing.Point(9, 85);
            this.chkIncludeCompany.Name = "chkIncludeCompany";
            this.chkIncludeCompany.Size = new System.Drawing.Size(108, 15);
            this.chkIncludeCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkIncludeCompany.TabIndex = 18;
            this.chkIncludeCompany.Text = "Include Company";
            this.chkIncludeCompany.CheckedChanged += new System.EventHandler(this.LoadEmployee);
            // 
            // cboBranch
            // 
            this.cboBranch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBranch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBranch.DisplayMember = "Text";
            this.cboBranch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboBranch.DropDownHeight = 134;
            this.cboBranch.FormattingEnabled = true;
            this.cboBranch.IntegralHeight = false;
            this.cboBranch.ItemHeight = 14;
            this.cboBranch.Location = new System.Drawing.Point(68, 58);
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.Size = new System.Drawing.Size(177, 20);
            this.cboBranch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboBranch.TabIndex = 7;
            this.cboBranch.SelectedIndexChanged += new System.EventHandler(this.cboBranch_SelectedIndexChanged);
            this.cboBranch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // lblBranch
            // 
            this.lblBranch.AutoSize = true;
            // 
            // 
            // 
            this.lblBranch.BackgroundStyle.Class = "";
            this.lblBranch.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblBranch.Location = new System.Drawing.Point(12, 60);
            this.lblBranch.Name = "lblBranch";
            this.lblBranch.Size = new System.Drawing.Size(37, 15);
            this.lblBranch.TabIndex = 5;
            this.lblBranch.Text = "Branch";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(68, 36);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(177, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 4;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(12, 39);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(50, 15);
            this.lblCompany.TabIndex = 2;
            this.lblCompany.Text = "Company";
            // 
            // History
            // 
            this.History.Text = "History";
            // 
            // Salary
            // 
            this.Salary.Text = "Salary";
            // 
            // ReportViewerSummary
            // 
            this.ReportViewerSummary.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportViewerSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource1.Value = null;
            this.ReportViewerSummary.LocalReport.DataSources.Add(reportDataSource1);
            this.ReportViewerSummary.LocalReport.ReportEmbeddedResource = "MindSoftERP.bin.Debug.MainReports.RptCompanyHeader.rdlc";
            this.ReportViewerSummary.Location = new System.Drawing.Point(0, 107);
            this.ReportViewerSummary.Name = "ReportViewerSummary";
            this.ReportViewerSummary.Size = new System.Drawing.Size(1203, 290);
            this.ReportViewerSummary.TabIndex = 4;
            this.ReportViewerSummary.RenderingComplete += new Microsoft.Reporting.WinForms.RenderingCompleteEventHandler(this.ReportViewerSummary_RenderingComplete);
            this.ReportViewerSummary.RenderingBegin += new System.ComponentModel.CancelEventHandler(this.ReportViewerSummary_RenderingBegin);
            // 
            // FrmRptSalaryStructure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1203, 397);
            this.Controls.Add(this.ReportViewerSummary);
            this.Controls.Add(this.expandablePanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptSalaryStructure";
            this.Text = "SalaryStructure";
            this.Load += new System.EventHandler(this.FrmRptSalaryStructure_Load);
            this.expandablePanel1.ResumeLayout(false);
            this.expandablePanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkIncludeCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboBranch;
        private DevComponents.DotNetBar.LabelX lblBranch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboWorkStatus;
        private DevComponents.DotNetBar.LabelX lblWorkStatus;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDepartment;
        private DevComponents.DotNetBar.LabelX lblDepartment;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboEmployeementType;
        private DevComponents.DotNetBar.LabelX lblEmployeementType;
        private DevComponents.DotNetBar.LabelX lblType;
        private DevComponents.Editors.ComboItem History;
        private DevComponents.Editors.ComboItem Salary;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboType;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private Microsoft.Reporting.WinForms.ReportViewer ReportViewerSummary;
        private DevComponents.DotNetBar.ButtonX btnShow;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDesignation;
        private DevComponents.DotNetBar.LabelX lblDesignation;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboEmployee;
        private DevComponents.DotNetBar.LabelX lblEmployee;
    }
}