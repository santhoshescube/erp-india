﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MyBooksERP
{
    public partial class FrmRptSalaryStructure : Report
    {
        #region Properties
        private string strMReportPath = "";
        private clsMessage ObjUserMessage = null;
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.SalaryStructureMISReport);

                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Constructor
        public FrmRptSalaryStructure()
        {
            InitializeComponent();
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }
        #endregion

        #region FormLoad
        private void FrmRptSalaryStructure_Load(object sender, EventArgs e)
        {
            chkIncludeCompany.Checked = true;
            LoadCombos();
            SetDefaultBranch(cboBranch);
            SetDefaultWorkStatus(cboWorkStatus);
            cboCompany_SelectedIndexChanged(null, null);
            //if (ClsCommonSettings.IsMultiCurrencyEnabled == true)
            //{
            //    lblBranch.Enabled  = false;
            //    cboBranch.Enabled  = false;
            //    chkIncludeCompany.Visible = false;
            //    cboBranch.SelectedValue = 0;
            //}



        }
        #endregion
        //#region SetArabicControls
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.SalaryStructureMISReport, this);
        //}
        //#endregion SetArabicControls


        #region Methods

        private void GetAllCompaniesMulti()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompaniesMulti();

        }

        /// <summary>
        /// Fill All Companies
        /// </summary>
        private void FillCompany()
        {
            //if (ClsCommonSettings.IsMultiCurrencyEnabled == true)
            //{
            //    GetAllCompaniesMulti();
            //}
            //else
            //{
                cboCompany.DisplayMember = "Company";
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
            ////}

        }
        /// <summary>
        /// Fill All Branches based on the selected company
        /// </summary>
        private void FillBraches()
        {
            try
            {
                cboBranch.DisplayMember = "Branch";
                cboBranch.ValueMember = "BranchID";
                cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());
                //if (ClsCommonSettings.IsMultiCurrencyEnabled == true)
                //{
                //    cboBranch.SelectedValue = 0;
                //}
            }
            catch
            {
            }

        }

        /// <summary>
        /// Fill All Departments
        /// </summary>
        private void FillDepartment()
        {
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
        }
        private void FillAllDesignations()
        {
            cboDesignation.DisplayMember = "Designation";
            cboDesignation.ValueMember = "DesignationID";
            cboDesignation.DataSource = clsBLLReportCommon.GetAllDesignation();
        }
        /// <summary>
        /// Fill All WorkStatus
        /// </summary>
        private void FillWorkStatus()
        {
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
        }

        /// <summary>
        /// Fill All EmploymentTypes
        /// </summary>
        private void FillEmploymentTypes()
        {
            cboEmployeementType.DataSource = clsBLLReportCommon.GetAllEmploymentType();
            cboEmployeementType.DisplayMember = "EmploymentType";
            cboEmployeementType.ValueMember = "EmploymentTypeID";
        }
        private void FillAllEmployees()
        {
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployeesInCurrentCompany(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboEmployeementType.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32()

                                );
        }
        /// <summary>
        /// Fill All Combos1
        /// </summary>
        /// <param name="intType"></param>
        private void LoadCombos()
        {
            FillCompany();
            FillDepartment();
            FillAllDesignations();
            FillWorkStatus();
            FillEmploymentTypes();
            cboType.SelectedIndex = 0;
        }

        /// <summary>
        /// Show Report Function
        /// </summary>
        /// <returns></returns>
        private void ShowReport()
        {
            ReportViewerSummary.Reset();
            ReportViewerSummary.LocalReport.DataSources.Clear();
            DataTable dtSalary;

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    strMReportPath = cboType.SelectedIndex == 0 ? Application.StartupPath + "\\MainReports\\RptMISSalaryStructureSummaryArb.rdlc" :
            //            Application.StartupPath + "\\MainReports\\RptMISSalaryStructureHistorySummaryArb.rdlc";
            //}
            //else
            //{
                strMReportPath = cboType.SelectedIndex == 0 ? Application.StartupPath + "\\MainReports\\RptMISSalaryStructureSummary.rdlc" :
                        Application.StartupPath + "\\MainReports\\RptMISSalaryStructureHistorySummary.rdlc";
            //}


            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
            this.ReportViewerSummary.LocalReport.ReportPath = strMReportPath;

            //IF CBOTYPE SELECTED INDEX = 0 THEN SHOW CURRENT SALARY STRUCTURE REPORT
            //ELSE SHOW HISTORY REPORT

            if (cboType.SelectedIndex == 0)
            {
                this.ReportViewerSummary.LocalReport.SetParameters(
                    new ReportParameter[]
                    {
                        new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter),
                        new ReportParameter("Company",cboCompany.Text.Trim()),
                        new ReportParameter("Branch",cboBranch.Text.Trim()),
                        new ReportParameter("Department",cboDepartment.Text.Trim()),
                        new ReportParameter("WorkStatus",cboWorkStatus.Text.Trim()),
                        new ReportParameter("EmploymentType",cboEmployeementType.Text.Trim())
                    });

                dtSalary = clsBLLRptSalaryStructure.GetSalaryStructureSummary(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployeementType.SelectedValue.ToInt32(), chkIncludeCompany.Checked, cboEmployee.SelectedValue.ToInt32());

                ReportViewerSummary.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                ReportViewerSummary.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_SalaryStructureSummary", dtSalary));

            }
            else
            {
                this.ReportViewerSummary.LocalReport.SetParameters(
                    new ReportParameter[]
                    {
                        new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter),
                        new ReportParameter("Company",cboCompany.Text.Trim()),
                        new ReportParameter("Branch",cboBranch.Text.Trim()),
                        new ReportParameter("Department",cboDepartment.Text.Trim()),
                        new ReportParameter("WorkStatus",cboWorkStatus.Text.Trim()),
                        new ReportParameter("EmploymentType",cboEmployeementType.Text.Trim())
                    });

                dtSalary = clsBLLRptSalaryStructure.GetSalaryStructureHistorySummary(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployeementType.SelectedValue.ToInt32(), chkIncludeCompany.Checked, cboEmployee.SelectedValue.ToInt32());


                ReportViewerSummary.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                ReportViewerSummary.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_SalaryStructureSummary", dtSalary));
            }
            //If no records found show error message(No records found)
            if (dtSalary.Rows.Count == 0)
            {
                UserMessage.ShowMessage(9700);

            }
            else
            {
                this.ReportViewerSummary.SetDisplayMode(DisplayMode.PrintLayout);
                this.ReportViewerSummary.ZoomMode = ZoomMode.Percent;
                this.ReportViewerSummary.ZoomPercent = 100;
            }


        }

        /// <summary>
        /// Handles all the validations in the form
        /// </summary>
        /// <returns></returns>
        private bool FormValidations()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9701);
                return false;
            }

            if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9702);
                return false;
            }
            if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9703);
                return false;
            }
            if (cboDesignation.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9112);
                return false;
            }
            if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9704);
                return false;
            }
            if (cboEmployeementType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9705);
                return false;
            }
            if (cboType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9706);
                return false;
            }
            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9124);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Enable or disable the include company check box based on the selected branch value
        /// </summary>
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }
        private void LoadEmployee(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        #endregion

        #region Control Events
        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillBraches();
            FillAllEmployees();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllEmployees();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (FormValidations())
                {
                    ShowReport();
                }
            }
            catch (Exception Ex)
            {
                btnShow.Enabled = true;
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);

            }
            this.Cursor = Cursors.Default;
        }

        public void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = false;
        }
        #endregion

        #region ReportViewer Events
        private void ReportViewerSummary_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;

        }

        private void ReportViewerSummary_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;

        }

        #endregion





    }
}
