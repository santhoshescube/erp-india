﻿namespace MyBooksERP
{
    partial class FrmBankTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBankTransfer));
            this.ReportViewerBank = new Microsoft.Reporting.WinForms.ReportViewer();
            this.Template3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Template2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Template1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSplitButtonTemplate = new System.Windows.Forms.ToolStripSplitButton();
            this.ToolStripBank = new System.Windows.Forms.ToolStrip();
            this.btnReportSettings = new System.Windows.Forms.ToolStripButton();
            this.ToolStripBank.SuspendLayout();
            this.SuspendLayout();
            // 
            // ReportViewerBank
            // 
            this.ReportViewerBank.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportViewerBank.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DtSetDisplayPayment_DisplayWordReportMaster";
            reportDataSource1.Value = null;
            this.ReportViewerBank.LocalReport.DataSources.Add(reportDataSource1);
            this.ReportViewerBank.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptBankPayment1.rdlc";
            this.ReportViewerBank.Location = new System.Drawing.Point(0, 25);
            this.ReportViewerBank.Name = "ReportViewerBank";
            this.ReportViewerBank.Size = new System.Drawing.Size(666, 463);
            this.ReportViewerBank.TabIndex = 69;
            // 
            // Template3ToolStripMenuItem
            // 
            this.Template3ToolStripMenuItem.Name = "Template3ToolStripMenuItem";
            this.Template3ToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.Template3ToolStripMenuItem.Text = "Template 3";
            this.Template3ToolStripMenuItem.Visible = false;
            // 
            // Template2ToolStripMenuItem
            // 
            this.Template2ToolStripMenuItem.Name = "Template2ToolStripMenuItem";
            this.Template2ToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.Template2ToolStripMenuItem.Text = "Template 2";
            this.Template2ToolStripMenuItem.ToolTipText = "Template 2";
            this.Template2ToolStripMenuItem.Click += new System.EventHandler(this.Template2ToolStripMenuItem_Click);
            // 
            // Template1ToolStripMenuItem
            // 
            this.Template1ToolStripMenuItem.Name = "Template1ToolStripMenuItem";
            this.Template1ToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.Template1ToolStripMenuItem.Text = "Template 1";
            this.Template1ToolStripMenuItem.ToolTipText = "Template 1";
            this.Template1ToolStripMenuItem.Click += new System.EventHandler(this.Template1ToolStripMenuItem_Click);
            // 
            // ToolStripSplitButtonTemplate
            // 
            this.ToolStripSplitButtonTemplate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripSplitButtonTemplate.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Template1ToolStripMenuItem,
            this.Template2ToolStripMenuItem,
            this.Template3ToolStripMenuItem});
            this.ToolStripSplitButtonTemplate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripSplitButtonTemplate.Name = "ToolStripSplitButtonTemplate";
            this.ToolStripSplitButtonTemplate.Size = new System.Drawing.Size(16, 22);
            this.ToolStripSplitButtonTemplate.Text = "Template";
            // 
            // ToolStripBank
            // 
            this.ToolStripBank.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripSplitButtonTemplate,
            this.btnReportSettings});
            this.ToolStripBank.Location = new System.Drawing.Point(0, 0);
            this.ToolStripBank.Name = "ToolStripBank";
            this.ToolStripBank.Size = new System.Drawing.Size(666, 25);
            this.ToolStripBank.TabIndex = 68;
            this.ToolStripBank.Text = "ToolStrip1";
            // 
            // btnReportSettings
            // 
            this.btnReportSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnReportSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnReportSettings.Image")));
            this.btnReportSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReportSettings.Name = "btnReportSettings";
            this.btnReportSettings.Size = new System.Drawing.Size(23, 22);
            this.btnReportSettings.ToolTipText = "Bank Transfer - Report Settings";
            this.btnReportSettings.Click += new System.EventHandler(this.btnReportSettings_Click);
            // 
            // FrmBankTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 488);
            this.Controls.Add(this.ReportViewerBank);
            this.Controls.Add(this.ToolStripBank);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmBankTransfer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bank Transfer";
            this.ToolStripBank.ResumeLayout(false);
            this.ToolStripBank.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Microsoft.Reporting.WinForms.ReportViewer ReportViewerBank;
        internal System.Windows.Forms.ToolStripMenuItem Template3ToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem Template2ToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem Template1ToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSplitButton ToolStripSplitButtonTemplate;
        internal System.Windows.Forms.ToolStrip ToolStripBank;
        internal System.Windows.Forms.ToolStripButton btnReportSettings;
        //private global::MyBooksERP.MyBooksERP.Reports.Payroll.Payroll.DtSetDisplayPayment dtSetDisplayPayment;
        //private global::MyBooksERP.MyBooksERP.Reports.Payroll.Payroll.RptDtSetCompanyCommon rptDtSetCompanyCommon;
    }
}