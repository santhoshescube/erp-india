﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
namespace MyBooksERP
{
    /*****************************************************
    * Created By       : Hima
    * Creation Date    : 05 aug 2013
    * Description      : Handle Salary Advance Reports Summary
    * FormID           : 139
    * ******************************************************/
    public partial class FrmRptSalaryAdvance :Report
    {
        
        #region Properties
        private string strMReportPath = "";

        private clsMessage ObjUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.SalaryAdvanceSummaryReport);
                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Constructor
        public FrmRptSalaryAdvance()
        {
            InitializeComponent();

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}

        }
        #endregion

        #region FormLoad
        private void FrmRptSalaryAdvance_Load(object sender, EventArgs e)
        {
            LoadCombos();
            cboType.SelectedIndex = 0;
            SetDefaultBranch(cboBranch);
            SetDefaultWorkStatus(cboWorkStatus);
            cboCompany_SelectedIndexChanged(null, null);

            


        }
        #endregion


        //#region SetArabicControls
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.SalaryAdvanceSummaryReport, this);
        //}
        //#endregion SetArabicControls

      
        #region Functions

        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }

        private void LoadCombos()
        {
            FillAllCompany();
            FillAllDeparments();
            FillAllWorkStatus();
        }
      
        private void FillAllCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());

        }

        private void FillAllDeparments()
        {
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
        }

        private void FillAllWorkStatus()
        {
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
        }
        private void FillAllEmployees()
        {
            cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                            chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(),-1, -1, cboWorkStatus.SelectedValue.ToInt32(), -1
                                            );
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";
        }


        private bool FormValidations()
        {
            if (cboCompany.SelectedValue.ToInt32()<= 0)
            {
                UserMessage.ShowMessage(3950);
                return false;
            }
            if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9141);
                return false;
            }
            if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9109);
                return false;
            }
          
            if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9113);
                return false;
            }
            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9124);
                return false;
            }
            if (cboType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9102);
                return false;
            }
            if (dtpFromDate.Value.Date > dtpToDate.Value.Date)
            {
                UserMessage.ShowMessage(1761);
                return false;
            }
            return true;
        }

        private void LoadEmployee(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void ShowSalaryAdvanceOrLoanReport()
        {
         
            DataTable dtAdvanceOrLoan;
            DataTable dtLoanDetails = new DataTable();
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    strMReportPath = cboType.SelectedIndex == 0 ? Application.StartupPath + "\\MainReports\\RptSalaryAdvanceSummaryArb.rdlc" : Application.StartupPath + "\\MainReports\\RptLoanDetailsArb.rdlc";

            //}
            //else
            //{
                strMReportPath = cboType.SelectedIndex == 0 ? Application.StartupPath + "\\MainReports\\RptSalaryAdvanceSummary.rdlc" : Application.StartupPath + "\\MainReports\\RptLoanDetails.rdlc";

            //}

            //Set ReportPath
            this.rptViewerSalaryAdvance.LocalReport.ReportPath = strMReportPath;
            //Company Header
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
            //Summary main Report for SalaryAdvance and Loan
            dtAdvanceOrLoan = clsBLLSalaryAdvanceReport.GetSalaryAdvanceOrLoanSummary(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(),cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), chkIncludeCompany.Checked, dtpFromDate.Value, dtpToDate.Value, cboType.SelectedIndex.ToInt32());
            //Detailed Report of Loan for a specific employee
            if((cboEmployee.SelectedValue.ToInt32()>0)&& (cboType.SelectedIndex==1))
                dtLoanDetails = clsBLLSalaryAdvanceReport.GetLoanDetails(cboEmployee.SelectedValue.ToInt32(), dtpFromDate.Value, dtpToDate.Value);
            if (dtAdvanceOrLoan != null)
            {
                if (dtAdvanceOrLoan.Rows.Count > 0)
                {
                    rptViewerSalaryAdvance.LocalReport.SetParameters(new ReportParameter[] 
                                {
                                 new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter),
                                 new ReportParameter("Company", cboCompany.Text),
                                 new ReportParameter("Branch", cboBranch.Text),
                                 new ReportParameter("Department", cboDepartment.Text),
                                 new ReportParameter("WorkStatus", cboWorkStatus.Text),
                                 new ReportParameter("Employee",cboEmployee.Text),
                                 new ReportParameter("Period",dtpFromDate.Value.ToString("dd MMM yyyy")+"-"+dtpToDate.Value.ToString("dd MMM yyyy"))
                               });


                    //SALARY ADVANCE REPORT
                    if (cboType.SelectedIndex == 0)
                    {
                        this.rptViewerSalaryAdvance.LocalReport.DataSources.Add(new ReportDataSource("DtSetSalaryAdvanceSummary_SalaryAdvanceReport", dtAdvanceOrLoan));
                    }
                    //LOAN REPORT
                    else
                    {
                       
                            this.rptViewerSalaryAdvance.LocalReport.DataSources.Add(new ReportDataSource("DtSetSalaryAdvanceSummary_LoanDetails", dtAdvanceOrLoan));
                            this.rptViewerSalaryAdvance.LocalReport.DataSources.Add(new ReportDataSource("DtSetSalaryAdvanceSummary_LoanInstallment", dtLoanDetails));
                      
                    }
                    this.rptViewerSalaryAdvance.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                }
                else
                {
                    UserMessage.ShowMessage(3952);//NO INFORMATION FOUND 
                    return;
                }
            }
         
            SetReportDisplay(rptViewerSalaryAdvance);
        }

        #endregion

        #region ControlEvents

        private void BtnShow_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            this.rptViewerSalaryAdvance.Reset();
            try
            {
              
                if (FormValidations())
                {
                    ShowSalaryAdvanceOrLoanReport();
                }
            }
            catch (Exception ex)
            {
                BtnShow.Enabled = true;
                ClsLogWriter.WriteLog(ex, Log.LogSeverity.Error);
               
            }
            this.Cursor = Cursors.Default;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllBranches();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllEmployees();
        }
       
        private void rbSalaryAdvance_CheckedChanged(object sender, EventArgs e)
        {
            FillAllCompany();
        }
        private void rbLoan_CheckedChanged(object sender, EventArgs e)
        {
            FillAllCompany();
        }
        #endregion

        #region Control KeyDown Event
        private void cbo_Keydown(object sender, KeyEventArgs e)
        {
            ComboBox cbo = sender as ComboBox;
            cbo.DroppedDown = false;
        }
        #endregion

        #region ReportViewerEvents
        private void rptViewerSalaryAdvance_RenderingBegin(object sender, CancelEventArgs e)
        {
            BtnShow.Enabled = false;
        }

        private void rptViewerSalaryAdvance_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            BtnShow.Enabled = true;
        }
        #endregion
        
    }
}
