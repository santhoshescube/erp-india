﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

/*****************************************************
   * Created By       : Sruthy K
   * Creation Date    : 19 Aug 2013
   * Description      : Leave Summary Report
   * FormID           : 138
   * Message Code     : 9702,9712
   * ******************************************************/

namespace MyBooksERP
{
    public partial class FrmRptLeaveSummary : Report
    {
        #region Properties
        private clsMessage ObjUserMessage = null;

        /// <summary>
        /// For Error Message
        /// </summary>
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.LeaveSummaryMISReport);

                return this.ObjUserMessage;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public FrmRptLeaveSummary()
        {
            InitializeComponent();
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        #endregion

        #region FormLoad
        private void FrmRptLeaveSummary_Load(object sender, EventArgs e)
        {
            dtpFromDate.Value = dtpToDate.Value =  ClsCommonSettings.GetServerDate();
            LoadCombos();
            SetDefaultBranch(cboBranch);
            SetDefaultWorkStatus(cboWorkStatus);

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}

            cboCompany_SelectedIndexChanged(null, null);

        }
        #endregion

        //#region SetArabicControls
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.LeaveSummaryMISReport, this);
        //}
        //#endregion SetArabicControls

        #region Functions

        /// <summary>
        /// Load All Combos
        /// </summary>
        private void LoadCombos()
        {
            FillCompany();
            FillAllDepartments();
            FillAllWorkStatus();
            FillFinancialYears();
        }

        /// <summary>
        /// Load Company
        /// </summary>
        private void FillCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        /// <summary>
        /// Load Branches by Company
        /// </summary>
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());
        }

        /// <summary>
        /// Load All Departments
        /// </summary>
        private void FillAllDepartments()
        {
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
        }

        /// <summary>
        /// Load All Designations
        /// </summary>
        private void FillAllWorkStatus()
        {
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
        }

        /// <summary>
        /// Load Employees by Company,Department,Designation
        /// </summary>
        private void FillAllEmployees()
        {
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), -1, -1, cboWorkStatus.SelectedValue.ToInt32(), -1
                                );
        }

        /// <summary>
        /// Load Financial Year by Company,Employee
        /// </summary>
        private void FillFinancialYears()
        {
            cboFinYear.DisplayMember = "FinYearPeriod";
            cboFinYear.ValueMember = "FinYearStartDate";
            cboFinYear.DataSource = clsBLLReportCommon.GetAllFinancialYears(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked);
        }

        /// <summary>
        /// Enable or Disable Include Company checkbox
        /// </summary>
        //private void EnableDisableIncludeCompany()
        //{
        //    if (cboBranch.SelectedValue.ToInt32() == 0)
        //    {
        //        chkIncludeCompany.Checked = true;
        //        chkIncludeCompany.Enabled = false;
        //    }
        //    else
        //    {
        //        chkIncludeCompany.Enabled = true;
        //    }
        //}
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }


        /// <summary>
        /// Validate User Inputs
        /// </summary>
        /// <returns>true if success otherwise returns false</returns>
        public bool FormValidation()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9100);
                return false;
            }
            if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9141);
                return false;
            }
            if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9109);
                return false;
            }
            if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9113);
                return false;
            }
            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9124);
                return false;
            }
            if (cboFinYear.Enabled && cboFinYear.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9712);
                return false;
            }
            if((!chkSummary.Checked) &&(dtpFromDate.Value.Date > dtpToDate.Value.Date))
            {
                    UserMessage.ShowMessage(1761);
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Load Report
        /// </summary>
        private void ShowReport()
        {

           //Set Report Path

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.rvLeaveSummary.LocalReport.ReportPath = chkSummary.Checked ? Application.StartupPath + "\\MainReports\\RptLeaveFinancialYearSummaryArb.rdlc" : Application.StartupPath + "\\MainReports\\RpLeaveSummaryArb.rdlc";

            //}
            //else
            //{
                this.rvLeaveSummary.LocalReport.ReportPath = chkSummary.Checked ? Application.StartupPath + "\\MainReports\\RptLeaveFinancialYearSummary.rdlc" : Application.StartupPath + "\\MainReports\\RpLeaveSummary.rdlc";

            //}

            //Clear DataSources
            this.rvLeaveSummary.LocalReport.DataSources.Clear();

            //Set Company Header For Report
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            //Leave Summary Report
            if (chkSummary.Checked == true)
            {
                rvLeaveSummary.LocalReport.SetParameters(new ReportParameter[]
                {
                    new ReportParameter("GeneratedBy",ClsCommonSettings.ReportFooter),
                    new ReportParameter("Company",cboCompany.Text.Trim()),
                    new ReportParameter("Branch",cboBranch.Text.Trim()),
                    new ReportParameter("Department",cboDepartment.Text.Trim()),
                    new ReportParameter("WorkStatus",cboWorkStatus.Text.Trim()),
                    new ReportParameter("Employee",cboEmployee.Text.Trim()),
                    new ReportParameter("Period",cboFinYear.Text.Trim())

                });

                string strFinYearStartDate = string.Empty;


                if (cboFinYear.SelectedIndex != -1 && cboFinYear.Text.ToStringCustom() != string.Empty)
                {
                    strFinYearStartDate = cboFinYear.Text.ToStringCustom().Split('-')[0].Trim();
                }

                DataTable dtLeaveSummary = clsBLLRptLeaveSummary.GetLeaveSummaryReport(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), strFinYearStartDate);

                if (dtLeaveSummary.Rows.Count > 0)
                {
                    rvLeaveSummary.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    rvLeaveSummary.LocalReport.DataSources.Add(new ReportDataSource("DtSetLeaveSummary_dtLeaveSummary", dtLeaveSummary));
                }
                else
                {
                    UserMessage.ShowMessage(9707, null);
                    return;

                }

            }
            else // Get Leave Detail Report
            {

                DateTime dtFromDate = dtpFromDate.Value;
                DateTime dtToDate = dtpToDate.Value;

                rvLeaveSummary.LocalReport.SetParameters(new ReportParameter[]
                {
                    new ReportParameter("GeneratedBy",ClsCommonSettings.ReportFooter),
                    new ReportParameter("Company",cboCompany.Text.Trim()),
                    new ReportParameter("Branch",cboBranch.Text.Trim()),
                    new ReportParameter("Department",cboDepartment.Text.Trim()),
                    new ReportParameter("WorkStatus",cboWorkStatus.Text.Trim()),
                    new ReportParameter("Employee",cboEmployee.Text.Trim()),
                    new ReportParameter("Period",dtFromDate.ToString("dd MMM yyyy")+" - "+dtToDate.ToString("dd MMM yyyy"))

                });

                DataTable dtLeaveReport = clsBLLRptLeaveSummary.GetLeaveReport(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), dtFromDate, dtToDate);
                if (dtLeaveReport.Rows.Count > 0)
                {
                    rvLeaveSummary.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    rvLeaveSummary.LocalReport.DataSources.Add(new ReportDataSource("DtSetLeaveSummary_dtLeave", dtLeaveReport));
                }
                else
                {
                    UserMessage.ShowMessage(9707, null);
                    return;
                }
            }

            SetReportDisplay(rvLeaveSummary);
        }
        #endregion

        #region ControlEvents
        
        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllBranches();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllEmployees();
        }

        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void chkSummary_CheckedChanged(object sender, EventArgs e)
        {
            cboFinYear.Enabled = chkSummary.Checked;
            dtpFromDate.Enabled = dtpToDate.Enabled = !chkSummary.Checked;
        }
        
        private void btnShow_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                this.rvLeaveSummary.Reset();

                if (FormValidation())
                {
                    ShowReport();
                }
            }
            catch (Exception Ex)
            {
                btnShow.Enabled = true;
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
            this.Cursor = Cursors.Default;
        }

        #endregion

        #region Control KeyDown Event
        private void cbo_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox cboSender = (ComboBox)(sender);
            if (cboSender != null)
                cboSender.DroppedDown = false;
        }
        #endregion

        #region ReportViewerEvents
        private void rvLeaveSummary_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;
        }

        private void rvLeaveSummary_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;
        }
        #endregion

    }
}
