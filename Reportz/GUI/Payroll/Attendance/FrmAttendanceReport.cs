﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using Microsoft.VisualBasic;
using System.IO;

/*****************************************************
   * Created By       : Sruthy K
   * Creation Date    : 06 Aug 2013
   * Description      : Attendance Report
   * FormID           : 133
   * Message Code     : 3002 - 3003
   * ******************************************************/

namespace MyBooksERP
{
    public partial class FrmAttendanceReport : Report
    {
       
        #region Properties
        private clsMessage ObjUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.AttendanceReport);
                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Constructor

        public FrmAttendanceReport()
        {
            InitializeComponent();
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }
        #endregion
        //#region SetArabicControls
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.AttendanceReport, this);
        //}
        //#endregion SetArabicControls

        #region FormLoad

        private void FrmRptAttendanceManual_Load(object sender, EventArgs e)
        {
            dtpAttDate.Value = ClsCommonSettings.GetServerDate();
            LoadCombos();
            SetDefaultBranch(cboBranch);
            SetDefaultWorkStatus(cboWorkStatus);
    



        }

        #endregion

        #region Functions
        /// <summary>
        /// Load All Combos
        /// </summary>
        private void LoadCombos()
        {
            FillCompany();
            FillAllDepartments();
            FillAllDesignations();
            FillAllWorkStatus();
        }

        /// <summary>
        /// Load Company
        /// </summary>
        private void FillCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        /// <summary>
        /// Load Branches by Company
        /// </summary>
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());
        }


        /// <summary>
        /// Load All Departments
        /// </summary>
        private void FillAllDepartments()
        {
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
        }



        /// <summary>
        /// Load All Designations
        /// </summary>
        private void FillAllDesignations()
        {
            cboDesignation.DisplayMember = "Designation";
            cboDesignation.ValueMember = "DesignationID";
            cboDesignation.DataSource = clsBLLReportCommon.GetAllDesignation();
        }

        /// <summary>
        /// Load All Departments
        /// </summary>
        private void FillAllWorkStatus()
        {
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
        }


        /// <summary>
        /// Load Employees by Company,Department,Designation
        /// </summary>
        private void FillAllEmployees()
        {
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked,cboDepartment.SelectedValue.ToInt32(),cboDesignation.SelectedValue.ToInt32(),-1,cboWorkStatus.SelectedValue.ToInt32(),-1
                                );
        }
        /// <summary>
        /// Load Month 
        /// </summary>
        private void FillAllMonths()
        {
            cboMonth.DisplayMember = "Month";
            cboMonth.ValueMember = "MDate";
            cboMonth.DataSource = clsBLLReportCommon.GetMonthForAttendance(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked,cboDepartment.SelectedValue.ToInt32(),cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32()
                                );
        }

        private void ShowAttendanceSummaryReport()
        {
            string strMReportPath = "";
            if (FormValidations() == false)
                return;


            this.rvAttendance.Reset();
            decimal decTotalAmt = 0;
            int sChecked = 1;


            string strDate = "";
            string strPerid = "";
            string strEndDate = "";
            string[] MonthYear = new string[2];

            DataTable dtEmployeeAttendance = null; ;
            DataTable dtCompanyDetails = null;
            DataTable dtEntryExitDetails = null;

            int iMonth = 0;
            int iYear = 0;
            int intIncludeComp = chkIncludeCompany.Checked ? 1 : 0;
            int intBranchID = 0;
            int intCompanyID = cboCompany.SelectedValue.ToInt32();
            int intEmployeeID = cboEmployee.SelectedValue.ToInt32();
            int intLocationID = 0;
            string strTotalWorkTime = "00:00:00";
            string strTotalBreakTime = "00:00:00";
            string strTotalOt = "00:00:00";
            string strTotalAbsentTime = "00:00:00";
            string strTotalFoodBreakTime = "00:00:00";

            this.rvAttendance.Reset(); 
            if (cboBranch.SelectedValue.ToInt32() == -2)
                intBranchID = 0;
            else
                intBranchID = cboBranch.SelectedValue.ToInt32();


            if (dtpAttDate.Checked)
            {
                iMonth = dtpAttDate.Value.Month;
                iYear = dtpAttDate.Value.Year;
                strDate = dtpAttDate.Value.ToString("dd-MMM-yyyy");
                strPerid = strDate;

            }
            else
            {
                if (cboMonth.Text.ToStringCustom() != string.Empty && cboMonth.SelectedValue.ToStringCustom() != string.Empty)
                {
                    MonthYear = cboMonth.SelectedValue.ToStringCustom().Split('@');

                    iMonth = MonthYear[0].ToStringCustom().TrimStart().TrimEnd().ToInt32();
                    iYear = MonthYear[1].ToStringCustom().TrimStart().TrimEnd().ToInt32();

                    strDate = "01-" + DateAndTime.MonthName(iMonth, true) + "-" + iYear.ToString();
                    strPerid = strDate;
                }
                else
                {
                    UserMessage.ShowMessage(3002, null);
                    return;
                }

            }
            int intDayID = (int)Convert.ToDateTime(strDate).DayOfWeek + 1;


            if (cboEmployee.Text != "ALL")
            {
                if (dtpAttDate.Checked == false)
                    sChecked = 0;

            }


            this.rvAttendance.LocalReport.DataSources.Clear();

            int iCmpID = cboBranch.SelectedValue.ToInt32() > 0 ? cboBranch.SelectedValue.ToInt32() : cboCompany.SelectedValue.ToInt32();
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            dtCompanyDetails = dtCompany;

            if (chkSummary.Checked)
            {
                strMReportPath = Application.StartupPath + "\\MainReports\\RptAttendancesummaryForaMonth.rdlc";

                if (dtpAttDate.Checked)
                    strEndDate = strDate;
                else
                    strEndDate = Convert.ToDateTime(strDate).AddMonths(1).AddDays(-1).ToString("dd-MMM-yyyy");

                dtEmployeeAttendance = new clsBLLAttendanceReport().GetEmployeeSummary(intCompanyID, intEmployeeID, intBranchID, strDate, strEndDate, intIncludeComp, 0);

                ReportParameter[] RepParam = new ReportParameter[2];

                RepParam[0] = new ReportParameter("Reporton", strPerid, false);
                RepParam[1] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);
                this.rvAttendance.ProcessingMode = ProcessingMode.Local;
                this.rvAttendance.LocalReport.ReportPath = strMReportPath;
                this.rvAttendance.LocalReport.SetParameters(RepParam);
                this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_AttendanceForaMonth", dtEmployeeAttendance));
            }
            else
            {

                if (cboEmployee.SelectedValue.ToInt32() > 0 && dtpAttDate.Checked)
                {
                    strMReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceDailyPunching.rdlc";
                    dtEmployeeAttendance = new clsBLLAttendanceReport().GetEmployeeDetailsAttendance(intEmployeeID, intDayID, strDate, intLocationID);
                    dtEntryExitDetails = new clsBLLAttendanceReport().GetEmployeeDayEntryExitDetails(intEmployeeID, strDate, intLocationID);
                    ReportParameter[] RepParam = new ReportParameter[3];
                    RepParam[0] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);
                    RepParam[1] = new ReportParameter("Date", strDate, false);
                    if (ClsCommonSettings.Glb24HourFormat == true)
                    {
                        RepParam[2] = new ReportParameter("HourFormat", "0");
                    }
                    else
                    {
                        RepParam[2] = new ReportParameter("HourFormat", "1");

                    }
                    this.rvAttendance.ProcessingMode = ProcessingMode.Local;
                    this.rvAttendance.LocalReport.ReportPath = strMReportPath;
                    this.rvAttendance.LocalReport.SetParameters(RepParam);
                    this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_DayDetailAttendance", dtEmployeeAttendance));
                    this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_AttendanceDetails", dtEntryExitDetails));

                }
                else
                {

                    int iMode = dtpAttDate.Checked ? 9 : 5;
                    strMReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceSummary.rdlc";
                    dtEmployeeAttendance = new clsBLLAttendanceReport().GetEmployeeAttendnaceAllEmployee(iMode, intCompanyID, intBranchID, intEmployeeID, strDate, intIncludeComp, intLocationID);

                    string strIsOTShown = "1";

                    if (cboEmployee.Text == "ALL" && dtpAttDate.Checked && !chkSummary.Checked)
                        strIsOTShown = "0";

                    ReportParameter[] RepParam = new ReportParameter[18];
                    RepParam[0] = new ReportParameter("CompanyID", Convert.ToString(intCompanyID), false);
                    RepParam[1] = new ReportParameter("Month", Convert.ToString(iMonth), false);
                    RepParam[2] = new ReportParameter("Year", Convert.ToString(iYear), false);
                    RepParam[3] = new ReportParameter("Companyname", cboCompany.Text.Trim(), false);
                    RepParam[4] = new ReportParameter("EmployeeID", intEmployeeID.ToString(), false);
                    RepParam[5] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);
                    RepParam[6] = new ReportParameter("Totalworktime", strTotalWorkTime, false);
                    RepParam[7] = new ReportParameter("Totalbreaktime", strTotalBreakTime, false);
                    RepParam[8] = new ReportParameter("Date", strDate, false);
                    RepParam[9] = new ReportParameter("totalfbreaktime", strTotalFoodBreakTime, false);
                    RepParam[10] = new ReportParameter("totalot", strTotalOt, false);
                    RepParam[11] = new ReportParameter("checked", Convert.ToString(sChecked), false);
                    RepParam[12] = new ReportParameter("sMonth", Convert.ToString(decTotalAmt), false);
                    RepParam[13] = new ReportParameter("Employee", cboEmployee.Text, false);
                    RepParam[14] = new ReportParameter("Reporton", strPerid, false);
                    RepParam[15] = new ReportParameter("TotalShortage", strTotalAbsentTime, false);
                    RepParam[16] = new ReportParameter("IsOTShown", strIsOTShown, false);
                    RepParam[17] = new ReportParameter("Company", cboCompany.Text.Trim(), false);
                    this.rvAttendance.ProcessingMode = ProcessingMode.Local;
                    this.rvAttendance.LocalReport.ReportPath = strMReportPath;
                    this.rvAttendance.LocalReport.SetParameters(RepParam);
                    this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_EmployeeAttendance", dtEmployeeAttendance));

                }
            }
            this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_CompanyDetails", dtCompanyDetails));
            if (dtEmployeeAttendance.Rows.Count <= 0)
            {

                UserMessage.ShowMessage(9015, null);
                return;
            }

            if (!File.Exists(strMReportPath))
            {
                UserMessage.ShowMessage(9127, null);
                return;
            }


            this.rvAttendance.SetDisplayMode(DisplayMode.PrintLayout);
            this.rvAttendance.ZoomMode = ZoomMode.Percent;
            this.rvAttendance.ZoomPercent = 100;



        }

        /// <summary>
        /// Show Report
        /// </summary>
        private void ShowReport()
        {
            bool blnIsMonth = true;
            if (dtpAttDate.Checked)
                blnIsMonth = false;
            int intMonth = 0;
            int intYear = 0;

            // Check whether Month is Selected
            if (cboMonth.SelectedValue != null)
            {
                intMonth = (cboMonth.SelectedValue.ToString().Split('@')[0]).ToInt32();
                intYear = (cboMonth.SelectedValue.ToString().Split('@')[1]).ToInt32();
            }

            //Set Company Header For Report
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            //Clear Report Datasources
            this.rvAttendance.Reset(); 
            this.rvAttendance.LocalReport.DataSources.Clear();
            this.rvAttendance.Refresh();
     
            if (chkSummary.Checked) // Attendance Summary Report
            {
                DataTable dtReport = clsBLLAttendanceReport.GetSummaryReport(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked,cboDepartment.SelectedValue.ToInt32(),cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(),intMonth, intYear);

                //if (ClsCommonSettings.IsArabicView)
                //{
                //    this.rvAttendance.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceSummaryForaMonthArb.rdlc";

                //}
                //else
                //{
                    this.rvAttendance.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceSummaryForaMonth.rdlc";

                //}
                rvAttendance.LocalReport.SetParameters(new ReportParameter[]
                {
                    new ReportParameter("GeneratedBy",ClsCommonSettings.ReportFooter),
                    new ReportParameter("Company",cboCompany.Text.Trim()),
                    new ReportParameter("Branch",cboBranch.Text.Trim()),
                    new ReportParameter("Department",cboDepartment.Text.Trim()),
                    new ReportParameter("Designation",cboDesignation.Text.Trim()),
                    new ReportParameter("WorkStatus",cboWorkStatus.Text.Trim()),
                    new ReportParameter("Employee",cboEmployee.Text.Trim()),
                    new ReportParameter("Month",cboMonth.Text.Trim())
                });
                if (dtReport.Rows.Count > 0)
                {
                    this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtSummary", dtReport));
                    this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                }
                else
                {
                    UserMessage.ShowMessage(3003);
                    return;
                }
            }
            else
            {
                if (cboEmployee.SelectedValue.ToInt32() > 0 && dtpAttDate.Checked) // Punching Report
                {
                    DataSet dsReport = clsBLLAttendanceReport.GetPunchingReport(cboEmployee.SelectedValue.ToInt32(), dtpAttDate.Value);

                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    this.rvAttendance.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceDailyPunchingArb.rdlc";

                    //}
                    //else
                    //{
                        this.rvAttendance.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceDailyPunching.rdlc";

                    //}
                    rvAttendance.LocalReport.SetParameters(new ReportParameter[]
                    {
                        new ReportParameter("GeneratedBy",ClsCommonSettings.ReportFooter)
                    });
                    if (dsReport.Tables[1].Rows.Count > 0)
                    {
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunchingGeneral", dsReport.Tables[0]));
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunching", dsReport.Tables[1]));
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunchingConsequences", dsReport.Tables[2]));
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    }
                    else
                    {
                        UserMessage.ShowMessage(3003);
                        return;
                    }
                }
                else // Attendance Month Wise Report
                {
                    DataTable dtReport = clsBLLAttendanceReport.GetReport(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), blnIsMonth, dtpAttDate.Value, intMonth, intYear);

                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    this.rvAttendance.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceSummaryArb.rdlc";
                    //}
                    //else
                    //{
                        this.rvAttendance.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceSummary1.rdlc";
                    //}
                    if (dtReport.Rows.Count > 0)
                    {
                    rvAttendance.LocalReport.SetParameters(new ReportParameter[]
                    {
                        new ReportParameter("GeneratedBy",ClsCommonSettings.ReportFooter),
                        new ReportParameter("Company",cboCompany.Text.Trim()),
                        new ReportParameter("Branch",cboBranch.Text.Trim()),
                        new ReportParameter("Department",cboDepartment.Text.Trim()),
                        new ReportParameter("Designation",cboDesignation.Text.Trim()),
                        new ReportParameter("WorkStatus",cboWorkStatus.Text.Trim()),
                        new ReportParameter("Employee",cboEmployee.Text.Trim()),
                        new ReportParameter("IsMonth",blnIsMonth ? "1" : "0"),
                        new ReportParameter("Period",dtpAttDate.Checked ? dtpAttDate.Value.ToString("dd MMM yyyy") : cboMonth.Text.Trim())
                    });


                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtAttendance", dtReport));
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    }
                    else
                    {
                        UserMessage.ShowMessage(3003);
                        return;
                    }
                }
            }
            SetReportDisplay(rvAttendance);
        }

        /// <summary>
        /// Validate User Inputs
        /// </summary>
        /// <returns></returns>
        private bool FormValidations()
        {
            if (cboCompany.SelectedValue.ToInt32() <= 0)
            {
                UserMessage.ShowMessage(9100);
                cboCompany.Focus();
                return false;
            }
            if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9141);
                cboBranch.Focus();
                return false;
            }
            if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9109);
                return false;
            }
            if (cboDesignation.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9112);
                return false;
            }
            if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9113);
                cboWorkStatus.Focus();
                return false;
            }
            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9124);
                cboEmployee.Focus();
                return false;
            }
            if (dtpAttDate.Checked == false && cboMonth.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(3002);
                cboMonth.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Enable or Disable Include Company checkbox
        /// </summary>
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }

        #endregion

        #region ControlEvents

        private void btnShow_Click(object sender, EventArgs e)
        {




            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (chkSummary.Checked == true || dtpAttDate.Checked==true)
                {
                    if (FormValidations())
                    {
                         ShowReport();
                    }
                }
                else 
                {
                    if (FormValidations())
                    {
                        ShowAttendanceSummaryReport();
                    }
                }
            }
            catch (Exception Ex)
            {
                btnShow.Enabled = true;
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
            this.Cursor = Cursors.Default;
        }

        private void dtpAttDate_ValueChanged(object sender, EventArgs e)
        {
            if (dtpAttDate.Checked)
            {
                cboMonth.Enabled = false;
                chkSummary.Checked = false;
                chkSummary.Enabled = false;
            }
            else
            {
                cboMonth.Enabled = true;
                chkSummary.Enabled = true;
            }
        }
       
        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllBranches();
            //FillAllEmployees();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllEmployees();
        }

        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }
      
        private void cboWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllMonths();
        }

        #endregion

        #region Control KeyDown Event
       
        private void cbo_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox cboSender = sender as ComboBox;
            if (cboSender != null)
                cboSender.DroppedDown = false;
        }
        #endregion

        #region ReportViewerEvents
        private void rvAttendance_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;
        }

        private void rvAttendance_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;
        }
        #endregion
        
    }
}
