﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Collections;
using System.Data.SqlClient;

using System.IO;
using System.Drawing.Imaging;
/* 
=================================================
Author:		    <Author,,Sawmya>
Create date:    <Create Date,,12 May 2011>
Description:	<Description,, Purchase Report Form>
Modified By:    <Author,,Amal>
Modified date:  <Modified Date,,18 Oct 2011>
Description:    <Description,,FINE TUNING OF REPORTS>
================================================
*/

namespace MyBooksERP
{
    public partial class FrmRptPurchase : DevComponents.DotNetBar.Office2007Form 
    {
        #region  Variable Declaration

        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;

        private string MsReportPath;
        string PsReportFooter;
        private string MsMessageCaption ="MyBooksERP";
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        private bool MbViewPermission = true;
        private bool MblnPrintEmailPermission = false;//To Set View Permission
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private int intchkFrom;
        private int intchkTo;
        private string strFrom;
        private string strTo;
        private string strCondition = "";
        private int Company;
        private DataTable DTCompany;
        private int intstatus;
        private int intNo;
        private int Vendor;

        clsBLLMISPurchaseReport MobjclsBLLMISPurchase;
        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;
        #endregion //Variables Declaration

        #region Constructor
        public FrmRptPurchase()
        {
            InitializeComponent();
            PsReportFooter = ClsCommonSettings.ReportFooter;
            MobjclsBLLMISPurchase = new clsBLLMISPurchaseReport();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification();
            BtnShow.Click += new EventHandler(BtnShow_Click);
        }
        #endregion //constructor


        private void FrmRptPurchase_Load(object sender, EventArgs e)
        {
            LoadMessage();
            LoadCombos(0);
            //SetPermissions();            
            DtpFromDate.Value = ClsCommonSettings.GetServerDate();
            DtpToDate.Value = ClsCommonSettings.GetServerDate();
            if (Convert.ToInt32(CboCompany.SelectedValue) == 0 && Convert.ToInt32(CboType.SelectedValue) == 0)
            {
                lblStatus.Enabled = false;
                cboStatus.Enabled = false;   
                lblNo.Enabled = false;
                cboOrder.Enabled = false;
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblVendor.Enabled = false;
                CboVendor.Enabled = false;
            }
            cboDate.SelectedIndex = 0;
        }
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.Purchase, (int)ClsCommonSettings.ProductID);
            //MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.Purchase, 4);
        }
        //private void SetPermissions()
        //{
        //    clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
        //    if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
        //    {
        //        objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)ModuleID.Reports, (Int32)MenuID.ReportsPurchase, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
        //    }
        //    else
        //        MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        //    MbViewPermission = (MblnAddPermission || MblnPrintEmailPermission || MblnUpdatePermission || MblnDeletePermission);
        //    expandablePanel1.Enabled = MbViewPermission;
        //}

        #region "Filtering Of Purchase(LOAD COMBO BOX FUNCTION)" 
        private bool LoadCombos(int intType)
        {
            // 0 - For Loading All Combo
            int intCompany = Convert.ToInt32(CboCompany.SelectedValue);
            int intExecutive = Convert.ToInt32(cboExecutive.SelectedValue);
            int intPurchaseType = Convert.ToInt32(CboType.SelectedValue);
            int intStatus = Convert.ToInt32(cboStatus.SelectedValue);
            int intDepartment = Convert.ToInt32(CboDepartment.SelectedValue);
            int intVendor = Convert.ToInt32(CboVendor.SelectedValue);
            int intWareHouse = Convert.ToInt32(CboWarehouse.SelectedValue);
            int intNo = Convert.ToInt32(cboOrder.SelectedValue);
            //clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            string strPermittedCompany;
            DataTable DTPermComp = new DataTable();

            try
            {
                DataTable datCombos = new DataTable();
                DataRow datrow;
                if (intType == 0 || intType == 1)
                {
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //{
                        datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID="+ ClsCommonSettings.LoginCompanyID });
                        
                        CboCompany.ValueMember = "CompanyID";
                        CboCompany.DisplayMember = "CompanyName";
                        CboCompany.DataSource = datCombos;
                        CboCompany_SelectionChangeCommitted(null, null);
                    //}
                    //else
                    //{
                    //    datCombos = MobjclsBLLMISPurchase.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RptPurchase);
                    //    if (datCombos.Rows.Count <= 0)
                    //        CboCompany.DataSource = null;
                    //    else
                    //    {
                    //        datrow = datCombos.NewRow();
                    //        datrow["CompanyID"] = 0;
                    //        datrow["Name"] = "ALL";
                    //        datCombos.Rows.InsertAt(datrow, 0);
                    //        CboCompany.ValueMember = "CompanyID";
                    //        CboCompany.DisplayMember = "Name";
                    //        CboCompany.DataSource = datCombos;
                    //    }
                    //}
                }
                if (intType == 0 || intType == 2)
                {
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", "VendorTypeID="+(int)VendorType.Supplier+" " });
                    datrow = datCombos.NewRow();
                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboVendor.ValueMember = "VendorID";
                    CboVendor.DisplayMember = "VendorName";
                    CboVendor.DataSource = datCombos;
                }
                if (intType == 0 || intType == 3)
                {
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "OperationTypeID,OperationType", "OperationTypeReference", "ModuleID =2 and OperationTypeID in (" + (int)OperationType.PurchaseQuotation + "," + (int)OperationType.PurchaseOrder + "," + (int)OperationType.PurchaseInvoice + "," + (int)OperationType.DebitNote + "," + (int)OperationType.GRN + ")" });
                    datrow = datCombos.NewRow();
                    datrow["OperationTypeID"] = 0;
                    datrow["OperationType"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboType.ValueMember = "OperationTypeID";
                    CboType.DisplayMember = "OperationType";
                    CboType.DataSource = datCombos;
                }
                if (intType == 0 || intType == 4)
                {
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "DepartmentID,Department", "DepartmentReference", "" });
                    datrow = datCombos.NewRow();
                    datrow["DepartmentID"] = 0;
                    datrow["Department"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboDepartment.ValueMember = "DepartmentID";
                    CboDepartment.DisplayMember = "Department";
                    CboDepartment.DataSource = datCombos;
                }
                if (intType == 0 || intType == 5)
                {
                    strCondition = "CompanyID= " + intCompany + " Or CompanyID = (Select ParentID From CompanyMaster Where CompanyID= " + intCompany + ") Or " +
                    " CompanyID in(Select CM2.CompanyID From CompanyMaster CM1 Inner Join CompanyMaster CM2 ON CM2.ParentID = CM1.ParentID Where CM1.CompanyID = " + intCompany + " AND CM1.ParentID!=0) Or " +
                    " CompanyID in(Select CompanyID From CompanyMaster Where ParentID = " + intCompany + ") And IsActive=1";
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                        strCondition = "";
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["WarehouseID"] = 0;
                    datrow["WarehouseName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboWarehouse.ValueMember = "WarehouseID";
                    CboWarehouse.DisplayMember = "WarehouseName";
                    CboWarehouse.DataSource = datCombos;
                }
                if (intType == 0 || intType == 6)
                {
                    strCondition = "CompanyID =" + Convert.ToInt32(CboCompany.SelectedValue);
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                    //    if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //    {
                            strPermittedCompany = null;
                            strCondition = "";
                        //}
                        //else
                        //{
                        //    DTPermComp = objClsBLLPermissionSettings.GetPermittedCompany(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RptPurchase);
                        //    if (DTPermComp.Rows.Count <= 0)
                        //    {
                        //        strPermittedCompany = null;
                        //        strCondition = "";
                        //    }
                        //    else
                        //    {
                        //        strPermittedCompany = DTPermComp.Rows[0]["IsEnabled"].ToString();
                        //        if (strPermittedCompany != null) strCondition = " CompanyID in (" + strPermittedCompany + ")";
                        //    }
                        //}
                    }
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "EmployeeID,FirstName", "EmployeeMaster", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["EmployeeID"] = 0;
                    datrow["FirstName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboExecutive.ValueMember = "EmployeeID";
                    cboExecutive.DisplayMember = "FirstName";
                    cboExecutive.DataSource = datCombos;
                }
                if (intType == 9)
                {
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID=" + (int)OperationType.PurchaseIndent + "" });
                    datrow = datCombos.NewRow();
                    datrow["StatusID"] = 0;
                    datrow["Status"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboStatus.ValueMember = "StatusID";
                    cboStatus.DisplayMember = "Status";
                    cboStatus.DataSource = datCombos;
                }
                if (intType == 10)
                {
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID=" + (int)OperationType.PurchaseQuotation + " And StatusID not in (" + (int)OperationStatusType.PQuotationSuggested + "," + (int)OperationStatusType.PQuotationDenied + " )" });
                    datrow = datCombos.NewRow();
                    datrow["StatusID"] = 0;
                    datrow["Status"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboStatus.ValueMember = "StatusID";
                    cboStatus.DisplayMember = "Status";
                    cboStatus.DataSource = datCombos;
                }
                if (intType == 11)
                {
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID=" + (int)OperationType.PurchaseOrder + " And StatusID not in (" + (int)OperationStatusType.POrderDenied + "," + (int)OperationStatusType.POrderSuggested + " )" });
                    datrow = datCombos.NewRow();
                    datrow["StatusID"] = 0;
                    datrow["Status"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboStatus.ValueMember = "StatusID";
                    cboStatus.DisplayMember = "Status";
                    cboStatus.DataSource = datCombos;
                }
                if (intType == 12)
                {
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID=" + (int)OperationType.PurchaseInvoice + "" });
                    datrow = datCombos.NewRow();
                    datrow["StatusID"] = 0;
                    datrow["Status"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboStatus.ValueMember = "StatusID";
                    cboStatus.DisplayMember = "Status";
                    cboStatus.DataSource = datCombos;
                }
                if (intType == 13)
                {
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID=" + (int)OperationType.GRN + "" });
                    datrow = datCombos.NewRow();
                    datrow["StatusID"] = 0;
                    datrow["Status"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboStatus.ValueMember = "StatusID";
                    cboStatus.DisplayMember = "Status";
                    cboStatus.DataSource = datCombos;
                }
                if (intType == 14)
                {
                    strCondition = "";
                    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                    if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID=" + intExecutive + " AND";
                    if (intStatus != 0) strCondition = strCondition + " PIdM.StatusID=" + intStatus + " AND";
                    if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "PurchaseIndentID,PurchaseIndentNo", "InvPurchaseIndentMaster PIdM Inner Join UserMaster UM on UM.UserID=PIdM.CreatedBy", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["PurchaseIndentID"] = 0;
                    datrow["PurchaseIndentNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "PurchaseIndentID";
                    cboOrder.DisplayMember = "PurchaseIndentNo";
                    cboOrder.DataSource = datCombos;
                }
                if (intType == 15)
                {
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID= 26" + " And StatusID not in (135,136)" });
                    datrow = datCombos.NewRow();
                    datrow["StatusID"] = 0;
                    datrow["Status"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboStatus.ValueMember = "StatusID";
                    cboStatus.DisplayMember = "Status";
                    cboStatus.DataSource = datCombos;
                }
                if (intType ==16)
                {
                    strCondition = "";
                    if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder)
                    {
                        strCondition = "OperationTypeID=" + (int)OperationType.PurchaseOrder + "";
                    }
                    if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.GRN )
                    {
                        strCondition = "OperationTypeID=" + (int)OperationType.GRN + " AND OrderTypeID <> 13";
                    }
                    if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice)
                    {
                        strCondition = "OperationTypeID=" + (int)OperationType.PurchaseInvoice + "";
                    }
                    //datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", strCondition });
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "OrderTypeID,OrderType", "CommonOrderTypeReference", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["OrderTypeID"] = 0;
                    datrow["OrderType"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboOrderType.ValueMember = "OrderTypeID";
                    CboOrderType.DisplayMember = "OrderType";
                    CboOrderType.DataSource = datCombos;
                }
                if (intType == 17)
                {
                    strCondition = "";
                    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                    if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID=" + intExecutive + " AND";
                    if (intStatus != 0) strCondition = strCondition + " PQM.StatusID=" + intStatus + " AND";
                    if (intVendor != 0) strCondition = strCondition + " PQM.VendorID=" + intVendor + " AND";
                    if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);
                                                  
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "PurchaseQuotationID,PurchaseQuotationNo", "InvPurchaseQuotationMaster PQM Inner Join UserMaster UM on UM.UserID=PQM.CreatedBy", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["PurchaseQuotationID"] = 0;
                    datrow["PurchaseQuotationNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "PurchaseQuotationID";
                    cboOrder.DisplayMember = "PurchaseQuotationNo";
                    cboOrder.DataSource = datCombos;
                }
                if (intType == 18)
                {
                    strCondition = "";
                    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                    if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID=" + intExecutive + " AND";
                    if (intStatus != 0) strCondition = strCondition + " POM.StatusID=" + intStatus + " AND";
                    if (intVendor != 0) strCondition = strCondition + " POM.VendorID=" + intVendor + " AND";
                    if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);
                                
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "PurchaseOrderID,PurchaseOrderNo", "InvPurchaseOrderMaster POM Inner Join UserMaster UM on UM.UserID=POM.CreatedBy", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["PurchaseOrderID"] = 0;
                    datrow["PurchaseOrderNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "PurchaseOrderID";
                    cboOrder.DisplayMember = "PurchaseOrderNo";
                    cboOrder.DataSource = datCombos;
                }
                if (intType == 19)
                {
                    strCondition = "";
                    strCondition = strCondition + " GM.OrderTypeID <> 13  AND";
                    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                    if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID=" + intExecutive + " AND";
                    if (intStatus != 0) strCondition = strCondition + " GM.StatusID=" + intStatus + " AND";
                    if (intVendor != 0) strCondition = strCondition + " GM.VendorID=" + intVendor + " AND";
                    if (intWareHouse != 0) strCondition = strCondition + " GM.WarehouseID=" + intWareHouse + " AND";
                    if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);
                                                            
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "GRNID,GRNNo", "InvGRNMaster GM Inner Join UserMaster UM on UM.UserID=GM.CreatedBy", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["GRNID"] = 0;
                    datrow["GRNNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "GRNID";
                    cboOrder.DisplayMember = "GRNNo";
                    cboOrder.DataSource = datCombos;
                }
                if (intType == 20)
                {
                    strCondition = "";
                    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                    if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID=" + intExecutive + " AND";
                    if (intStatus != 0) strCondition = strCondition + " PIM.StatusID=" + intStatus + " AND";
                    if (intVendor != 0) strCondition = strCondition + " PIM.VendorID=" + intVendor + " AND";
                    if (intWareHouse != 0) strCondition = strCondition + " PIM.WarehouseID=" + intWareHouse + " AND";
                    if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);
                                     
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "PurchaseInvoiceID,PurchaseInvoiceNo", "InvPurchaseInvoiceMaster PIM Inner Join UserMaster UM on UM.UserID=PIM.CreatedBy", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["PurchaseInvoiceID"] = 0;
                    datrow["PurchaseInvoiceNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "PurchaseInvoiceID";
                    cboOrder.DisplayMember = "PurchaseInvoiceNo";
                    cboOrder.DataSource = datCombos;
                }
                if (intType == 21)
                {
                    strCondition = "";
                    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                    if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID=" + intExecutive + " AND";
                    if (intStatus != 0) strCondition = strCondition + " RFQM.StatusID=" + intStatus + " AND";
                    if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);
                                       
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "RFQID,RFQNo", "InvRFQMaster RFQM Inner Join UserMaster UM on UM.UserID=RFQM.CreatedBy", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["RFQID"] = 0;
                    datrow["RFQNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "RFQID";
                    cboOrder.DisplayMember = "RFQNo";
                    cboOrder.DataSource = datCombos;
                }
                if (intType == 22)
                {
                    strCondition = " E.CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue);
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        strCondition = "";
                    }
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { " distinct DR.DepartmentID,DR.Department", " InvPurchaseIndentMaster E inner join DepartmentReference DR on DR.DepartmentID = E.DepartmentID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["DepartmentID"] = 0;
                    datrow["Department"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboDepartment.ValueMember = "DepartmentID";
                    CboDepartment.DisplayMember = "Department";
                    CboDepartment.DataSource = datCombos;
                }
                if (intType == 23)
                {
                    strCondition = "";
                    if (intCompany != 0) strCondition = " PRM.CompanyID=" + intCompany + " AND";
                    if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID=" + intExecutive + " AND";
                    if (intStatus != 0) strCondition = strCondition + " PRM.StatusID=" + intStatus + " AND";
                    if (intVendor != 0) strCondition = strCondition + " PIM.VendorID=" + intVendor + " AND";
                    if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "PurchaseReturnID,PurchaseReturnNo", "InvPurchaseReturnMaster PRM Inner Join UserMaster UM on UM.UserID=PRM.CreatedBy Inner Join InvPurchaseInvoiceMaster PIM On PIM.PurchaseInvoiceID = PRM.PurchaseInvoiceID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["PurchaseReturnID"] = 0;
                    datrow["PurchaseReturnNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "PurchaseReturnID";
                    cboOrder.DisplayMember = "PurchaseReturnNo";
                    cboOrder.DataSource = datCombos;
                }
                if (intType == 24)
                {
                    datCombos = MobjclsBLLMISPurchase.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID =" + (int)OperationType.DebitNote + "" });
                    datrow = datCombos.NewRow();
                    datrow["StatusID"] = 0;
                    datrow["Status"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboStatus.ValueMember = "StatusID";
                    cboStatus.DisplayMember = "Status";
                    cboStatus.DataSource = datCombos;
                }
                if (intType == 0 || intType == 25)
                {
                    cboDate.Items.Clear();
                    cboDate.Items.Add("ALL");
                    cboDate.Items.Add("Date");
                    cboDate.Items.Add("Created Date");
                    cboDate.Items.Add("Due Date");
                    cboDate.SelectedIndex = 0;
                }
                if (intType == 26)
                {
                    cboDate.Items.Clear();
                    cboDate.Items.Add("ALL");
                    cboDate.Items.Add("Date");
                    cboDate.Items.Add("Created Date");
                    cboDate.SelectedIndex = 0;
                }
                MObjClsLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private void CboCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPurchaseReport.Clear();
            if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
            {
                LoadCombos(2); //FoR Verndor 
                LoadCombos(3); //FoR Type
                LoadCombos(4); //FoR Dept
                LoadCombos(5); //FoR Warehouse
                LoadCombos(14); //FoR order No
                LoadCombos(6); //FoR executive
                LoadCombos(9); //FoR status
                LoadCombos(16); // For ORDER TYPE
            }
            else
            {
                LoadCombos(5);
                LoadCombos(14);
                LoadCombos(6);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == 0)
            {
                lblStatus.Enabled = false;
                cboStatus.Enabled = false;
                lblNo.Enabled = false;
                cboOrder.Enabled = false;
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
                lblVendor.Enabled = false;
                CboVendor.Enabled = false;
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblDepartment.Enabled = false;
                CboDepartment.Enabled = false;
                LoadCombos(6);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseIndent)
            {
                lblVendor.Visible = false;
                CboVendor.Visible = false;
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblDepartment.Enabled = true;
                CboDepartment.Enabled = true;
                lblDepartment.Visible = true;
                CboDepartment.Visible = true;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
                LoadCombos(9);
                LoadCombos(14);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseQuotation )
            {
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblVendor.Enabled = true;
                CboVendor.Enabled = true;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(10);
                LoadCombos(17);
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder)
            {
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(6);
                LoadCombos(11);
                lblorder.Enabled = true;
                CboOrderType.Enabled = true;
                LoadCombos(18);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.GRN )
            {
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblWarehouse.Enabled = true;
                CboWarehouse.Enabled = true;
                lblWarehouse.Visible = true;
                CboWarehouse.Visible = true;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(13);
                lblorder.Enabled = true;
                CboOrderType.Enabled = true;
                LoadCombos(19);
                if (Convert.ToInt32(CboCompany.SelectedValue) > 0)
                {
                    LoadCombos(5);
                }
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice)
            {
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblWarehouse.Enabled = true;
                CboWarehouse.Enabled = true;
                lblWarehouse.Visible = true;
                CboWarehouse.Visible = true;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(8);
                LoadCombos(12);
                LoadCombos(20);
                lblorder.Enabled = true;
                CboOrderType.Enabled = true;
                if (Convert.ToInt32(CboCompany.SelectedValue) > 0)
                {
                    LoadCombos(5);
                    LoadCombos(6);
                }
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.RFQ )
            {
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblVendor.Enabled = true;
                CboVendor.Enabled = true;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
                LoadCombos(15);
                LoadCombos(21);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.DebitNote)
            {
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblWarehouse.Visible = true;
                CboWarehouse.Visible = true;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(24);
                LoadCombos(23);
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
                if (Convert.ToInt32(CboCompany.SelectedValue) > 0)
                {
                    LoadCombos(6);
                }
            }
        }

        private void cboExecutive_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ErpPurchaseReport.Clear();
            if (Convert.ToInt32(cboExecutive.SelectedValue) == 0)
            {
                cboOrder.Enabled = false;
                LoadCombos(2); //FoR Vendor 
                LoadCombos(3); //FoR Type
                LoadCombos(4); //FoR Dept
                LoadCombos(5); //FoR Warehouse
                LoadCombos(14); //FoR order No
                LoadCombos(9); //FoR status
                LoadCombos(16); // For ORDER TYPE
            }
            if (Convert.ToInt32(CboType.SelectedValue) == 0)
            {
                lblStatus.Enabled = false;
                cboStatus.Enabled = false;
                lblNo.Enabled = false;
                cboOrder.Enabled = false;
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
                lblVendor.Enabled = false;
                CboVendor.Enabled = false;
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblDepartment.Enabled = false;
                CboDepartment.Enabled = false;

                LoadCombos(2); //FoR Verndor 
                LoadCombos(3); //FoR Type
                LoadCombos(4); //FoR Dept
                LoadCombos(5); //FoR Warehouse
                LoadCombos(14); //FoR order No
                //LoadCombos(9); //FoR status
                LoadCombos(16); // For ORDER TYPE
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseIndent)
            {
                lblVendor.Visible = false;
                CboVendor.Visible = false;
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblDepartment.Enabled = true;
                CboDepartment.Enabled = true;
                lblDepartment.Visible = true;
                CboDepartment.Visible = true;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
                
                LoadCombos(9);
                LoadCombos(14);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseQuotation )
            {
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblVendor.Enabled = true;
                CboVendor.Enabled = true;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
                
                LoadCombos(10);
                LoadCombos(17);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder)
            {
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblVendor.Enabled = true;
                CboVendor.Enabled = true;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(11);
                lblorder.Enabled = true;
                CboOrderType.Enabled = true;
                LoadCombos(18);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.GRN )
            {
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblVendor.Enabled = true;
                CboVendor.Enabled = true;
                lblWarehouse.Enabled = true;
                CboWarehouse.Enabled = true;
                lblWarehouse.Visible = true;
                CboWarehouse.Visible = true;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(13);
                lblorder.Enabled = true;
                CboOrderType.Enabled = true;
                LoadCombos(19);
                if (Convert.ToInt32(CboCompany.SelectedValue) > 0)
                {
                    LoadCombos(5);
                }
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice)
            {
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblVendor.Enabled = true;
                CboVendor.Enabled = true;
                lblWarehouse.Enabled = true;
                CboWarehouse.Enabled = true;
                lblWarehouse.Visible = true;
                CboWarehouse.Visible = true;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(8);
                LoadCombos(12);
                LoadCombos(20);
                lblorder.Enabled = true;
                CboOrderType.Enabled = true;
                if (Convert.ToInt32(CboCompany.SelectedValue) > 0)
                {
                    LoadCombos(5);
                }
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.RFQ )
            {
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblVendor.Enabled = true;
                CboVendor.Enabled = true;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
                LoadCombos(15);
                LoadCombos(21);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.DebitNote)
            {
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblVendor.Enabled = true;
                CboVendor.Enabled = true;
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblWarehouse.Visible = true;
                CboWarehouse.Visible = true;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(24);
                LoadCombos(23);
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
            }
        }

        private void CboType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPurchaseReport.Clear();
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.DebitNote)
            {
                LoadCombos(26);
            }
            else
            {
                LoadCombos(25);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == 0)
            {
                lblStatus.Enabled = false;
                cboStatus.Enabled = false;
                lblNo.Enabled = false;
                cboOrder.Enabled = false;
                lblorder.Enabled = false;
                
                lblVendor.Enabled = false;
                CboVendor.Enabled = false;
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblDepartment.Enabled = false;
                CboDepartment.Enabled = false;
                LoadCombos(2); //FoR Verndor 
                LoadCombos(22); //FoR Dept
                LoadCombos(5); //FoR Warehouse
                LoadCombos(14); //FoR order
                LoadCombos(9); //FoR status
                LoadCombos(16); // For ORDER TYPE
                CboOrderType.Enabled = false;
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseIndent)
            {
                lblVendor.Visible = false;
                CboVendor.Visible = false;
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblDepartment.Enabled = true;
                CboDepartment.Enabled = true;
                lblDepartment.Visible = true;
                CboDepartment.Visible = true;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
                LoadCombos(9);
                LoadCombos(14);
                LoadCombos(22);
            }

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseQuotation )
            {
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblVendor.Enabled = true;
                CboVendor.Enabled = true;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                CboOrderType.Enabled = false;
                LoadCombos(10);
                LoadCombos(16);
                LoadCombos(17);
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder)
            {
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblVendor.Enabled = true;
                CboVendor.Enabled = true;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(16);
                LoadCombos(11);
                lblorder.Enabled = true;
                CboOrderType.Enabled = true;
                LoadCombos(18);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.GRN )
            {
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblVendor.Enabled = true;
                CboVendor.Enabled = true;
                lblWarehouse.Enabled = true;
                CboWarehouse.Enabled = true;
                lblWarehouse.Visible = true;
                CboWarehouse.Visible = true;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(16);
                LoadCombos(13);
                LoadCombos(19);
                lblorder.Enabled = true;
                CboOrderType.Enabled = true;
                if (Convert.ToInt32(CboCompany.SelectedValue) > 0)
                {
                   LoadCombos(5);
                }
            }

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice)
            {
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblVendor.Enabled = true;
                CboVendor.Enabled = true;
                lblWarehouse.Enabled = true;
                CboWarehouse.Enabled = true;
                lblWarehouse.Visible = true;
                CboWarehouse.Visible = true;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(16);
                LoadCombos(12);
                LoadCombos(20);
                lblorder.Enabled = true;
                CboOrderType.Enabled = true;
                if (Convert.ToInt32(CboCompany.SelectedValue) > 0)
                {
                    LoadCombos(5);
                }
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.RFQ )
            {
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblDepartment.Enabled = false;
                CboDepartment.Enabled = false;
                lblVendor.Enabled = false;
                CboVendor.Enabled = false;
                lblVendor.Enabled = false;
                CboVendor.Enabled = false;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
                LoadCombos(15);
                LoadCombos(21);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.DebitNote)
            {
                lblVendor.Visible = true;
                CboVendor.Visible = true;
                lblWarehouse.Enabled = false;
                CboWarehouse.Enabled = false;
                lblWarehouse.Visible = true;
                CboWarehouse.Visible = true;
                lblDepartment.Visible = false;
                CboDepartment.Visible = false;
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(24);
                LoadCombos(23);
                LoadCombos(16);
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
            }
        }
        private void cboStatus_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPurchaseReport.Clear();
            if (Convert.ToInt32(cboStatus.SelectedValue) == 0)
            {
                LoadCombos(2); //FoR Verndor 
                LoadCombos(22); //FoR Dept
                LoadCombos(5); //FoR Warehouse
                LoadCombos(16); // For ORDER TYPE
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseIndent)
            {
                LoadCombos(14);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseQuotation )
            {
                LoadCombos(17);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder)
            {
                LoadCombos(18);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.GRN )
            {
                LoadCombos(19);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice)
            {
                LoadCombos(20);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.RFQ )
            {
                LoadCombos(21);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.RFQ )
            {
                LoadCombos(23);
            }
       }

        private void CboDepartment_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPurchaseReport.Clear();
            LoadCombos(5);
        }

        private void CboWarehouse_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPurchaseReport.Clear();
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.GRN )
            {
                LoadCombos(19);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice)
            {
                LoadCombos(20);
            }
        }

        private void cboOrder_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPurchaseReport.Clear();
        }

        private void CboOrderType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPurchaseReport.Clear();
        }

        private void CboVendor_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPurchaseReport.Clear();
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseIndent)
            {
                LoadCombos(14);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseQuotation )
            {
                LoadCombos(17);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder)
            {
                LoadCombos(18);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.GRN )
            {
                LoadCombos(19);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice)
            {
                LoadCombos(20);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.RFQ )
            {
                LoadCombos(21);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == 0)
            {
               LoadCombos(5); //FoR Warehouse
               LoadCombos(16); // For ORDER TYPE
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.DebitNote)
            {
                LoadCombos(23);
            }
        }

        #endregion
        
        private void AllReport()
        {
            MsReportPath = Application.StartupPath + "\\MainReports\\RptMISPurchase.rdlc";
            ReportViewer1.LocalReport.ReportPath = MsReportPath;
            ReportParameter[] ReportParameter = new ReportParameter[14];
            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
            ReportParameter[1] = new ReportParameter("Company", Convert.ToString(CboCompany.Text.Trim()), false);
            ReportParameter[2] = new ReportParameter("Vendor", Convert.ToString(CboVendor.Text.Trim()), false);
            ReportParameter[3] = new ReportParameter("Type", Convert.ToString(CboType.Text.Trim()), false);
            ReportParameter[4] = new ReportParameter("Department", Convert.ToString(CboDepartment.Text.Trim()), false);
            if (DtpFromDate.LockUpdateChecked == false && DtpToDate.LockUpdateChecked==false)
            {
                strFrom = "ALL";
                strTo = "ALL";
                ReportParameter[5] = new ReportParameter("FromDate", strFrom, false);
                ReportParameter[6] = new ReportParameter("ToDate", strTo, false);
            }
            else if (DtpFromDate.LockUpdateChecked == true && DtpToDate.LockUpdateChecked == false)
            {
                strTo = "ALL";
                ReportParameter[5] = new ReportParameter("FromDate", strFrom, false);
                ReportParameter[6] = new ReportParameter("ToDate", strTo, false);
            }
            else if (DtpToDate.LockUpdateChecked == true && DtpFromDate.LockUpdateChecked == false)
            {
                strFrom = "ALL";
                ReportParameter[5] = new ReportParameter("FromDate", strFrom, false);
                ReportParameter[6] = new ReportParameter("ToDate", strTo, false);
            }
            else
            {
                ReportParameter[5] = new ReportParameter("FromDate", strFrom, false);
                ReportParameter[6] = new ReportParameter("ToDate", strTo, false);
            }
            ReportParameter[7] = new ReportParameter("Checkbox", Convert.ToString(intchkFrom), false);
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder)
            {
                string Warehouse = string.Empty;
                lblorder.Enabled = true;
                CboOrderType.Enabled = true;
                ReportParameter[10] = new ReportParameter("PurchaseType", Convert.ToString(CboOrderType.Text.Trim()), false);
                ReportParameter[11] = new ReportParameter("Warehouse", Warehouse, false);

            }
            else if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.GRN )
            {
                lblorder.Enabled = true;
                CboOrderType.Enabled = true;
                 ReportParameter[10] = new ReportParameter("PurchaseType", Convert.ToString(CboOrderType.Text.Trim()), false);
                 ReportParameter[11] = new ReportParameter("Warehouse", Convert.ToString(CboWarehouse.Text.Trim()), false);
            }
            else if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice)
            {
                lblorder.Enabled = true;
                CboOrderType.Enabled = true;
                ReportParameter[10] = new ReportParameter("PurchaseType", Convert.ToString(CboOrderType.Text.Trim()), false);
                ReportParameter[11] = new ReportParameter("Warehouse", Convert.ToString(CboWarehouse.Text.Trim()), false);
            }
            else
            {
                string Purchase = string.Empty;
                string Warehouse = string.Empty;
                lblorder.Enabled = false;
                CboOrderType.Enabled = false;
                ReportParameter[10] = new ReportParameter("PurchaseType", Purchase, false);
                ReportParameter[11] = new ReportParameter("Warehouse", Warehouse, false);
            }
            ReportParameter[8] = new ReportParameter("Status", Convert.ToString(cboStatus.Text.Trim()), false);
            ReportParameter[9] = new ReportParameter("No", Convert.ToString(cboOrder.Text.Trim()), false);
            ReportParameter[12] = new ReportParameter("Executive", Convert.ToString(cboExecutive.Text.Trim()),false);
            if (DtpFromDate.LockUpdateChecked == true || DtpToDate.LockUpdateChecked == true)
            {
                ReportParameter[13] = new ReportParameter("DateType", Convert.ToString(cboDate.Text.Trim()), false);
            }
            else
            {
                string strDateTyp = "ALL";
                ReportParameter[13] = new ReportParameter("DateType", strDateTyp, false);
            }
            ReportViewer1.LocalReport.SetParameters(ReportParameter);
            ReportViewer1.LocalReport.DataSources.Clear();
            DTCompany = new DataTable();
            if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
            {
                DTCompany = MobjclsBLLMISPurchase.DisplayALLCompanyHeaderReport();
            }
            else
            {
                DTCompany = MobjclsBLLMISPurchase.DisplayCompanyHeaderReport(Company);
            }
            ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
            //ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
        }
        public void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
        }
        private void ShowReport()
        {
            Company = Convert.ToInt32(CboCompany.SelectedValue);
            Vendor = Convert.ToInt32(CboVendor.SelectedValue);
            int Department = Convert.ToInt32(CboDepartment.SelectedValue);
            int Type = Convert.ToInt32(CboType.SelectedValue);
            strFrom = Microsoft.VisualBasic.Strings.Format(DtpFromDate.Value.Date, "dd MMM yyyy");
            strTo = Microsoft.VisualBasic.Strings.Format(DtpToDate.Value.Date, "dd MMM yyyy");
            int intOrderType = Convert.ToInt32(CboOrderType.SelectedValue);
            int intExecutive = Convert.ToInt32(cboExecutive.SelectedValue);
            int intDate = 0;
            if (cboDate.SelectedIndex == 0)
            {
                intDate = 0;
            }
            if (cboDate.SelectedIndex == 1)
            {
                intDate = 1;
            }
            if (cboDate.SelectedIndex == 2)
            {
                intDate = 2;
            }
            if (cboDate.SelectedIndex == 3)
            {
                intDate = 3;
            }
            if (Convert.ToInt32(Type) == 0)
            {
                intstatus = 0;
                intNo = 0;
            }
            else
            {
                intstatus = Convert.ToInt32(cboStatus.SelectedValue);
                intNo = Convert.ToInt32(cboOrder.SelectedValue);
            }
            int intWarehouse = Convert.ToInt32(CboWarehouse.SelectedValue);
            if (DtpFromDate.LockUpdateChecked == true)
            {
                intchkFrom = 1;
            }
            else
            {
                intchkFrom = 0;
            }
            if (DtpToDate.LockUpdateChecked == true)
            {
                intchkTo = 1;
            }
            else
            {
                intchkTo = 0;
            }
            //clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            string strPermittedCompany;
            DataTable DTPermComp = new DataTable();
            //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            //{
            strPermittedCompany = null;
            //}
            //else
            //{
            //    DTPermComp = objClsBLLPermissionSettings.GetPermittedCompany(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RptPurchase);
            //    if (DTPermComp.Rows.Count <= 0)
            //        strPermittedCompany = null;
            //    else
            //        strPermittedCompany = DTPermComp.Rows[0]["IsEnabled"].ToString();
            //}
            if (Type == 1)
            {
                if (intNo > 0)
                {
                    MsReportPath = Application.StartupPath + "\\MainReports\\RptPurchaseIndent.rdlc";
                    ReportViewer1.LocalReport.ReportPath = MsReportPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportViewer1.LocalReport.SetParameters(ReportParameters);
                    ReportViewer1.LocalReport.DataSources.Clear();
                }
            }
            if (Type == 2)
            {
                if (intNo > 0)
                {
                    MsReportPath = Application.StartupPath + "\\MainReports\\RptPurchaseQuotation.rdlc";
                    ReportViewer1.LocalReport.ReportPath = MsReportPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportViewer1.LocalReport.SetParameters(ReportParameters);
                    ReportViewer1.LocalReport.DataSources.Clear();
                }
            }
            if (Type == 3)
            {
                if (intNo > 0)
                {
                    MsReportPath = Application.StartupPath + "\\MainReports\\RptPurchaseOrder.rdlc";
                    ReportViewer1.LocalReport.ReportPath = MsReportPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportViewer1.LocalReport.SetParameters(ReportParameters);
                    ReportViewer1.LocalReport.DataSources.Clear();
                }
            }
            if (Type == 4)
            {
                if (intNo > 0)
                {
                    MsReportPath = Application.StartupPath + "\\MainReports\\RptPurchaseGRN.rdlc";
                    ReportViewer1.LocalReport.ReportPath = MsReportPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportViewer1.LocalReport.SetParameters(ReportParameters);
                    ReportViewer1.LocalReport.DataSources.Clear();
                }
            }
            if (Type == 5)
            {
                if (intNo > 0)
                {
                    MsReportPath = Application.StartupPath + "\\MainReports\\RptPurchaseInvoice.rdlc";
                    ReportViewer1.LocalReport.ReportPath = MsReportPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportViewer1.LocalReport.SetParameters(ReportParameters);
                    ReportViewer1.LocalReport.DataSources.Clear();
                }
            }
            if (Type == 23)
            {
                if (intNo > 0)
                {
                    MsReportPath = Application.StartupPath + "\\MainReports\\RptRFQForm.rdlc";
                    ReportViewer1.LocalReport.ReportPath = MsReportPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportViewer1.LocalReport.SetParameters(ReportParameters);
                    ReportViewer1.LocalReport.DataSources.Clear();
                }
            }
            if (Type == 6)
            {
                if (intNo > 0)
                {
                    MsReportPath = Application.StartupPath + "\\MainReports\\RptDebitNote.rdlc";
                    ReportViewer1.LocalReport.ReportPath = MsReportPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportViewer1.LocalReport.SetParameters(ReportParameters);
                    ReportViewer1.LocalReport.DataSources.Clear();
                }
            }
            if (Type == 0)
            {
                DTCompany = new DataTable();
                if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                {
                    DTCompany = MobjclsBLLMISPurchase.DisplayALLCompanyHeaderReport();
                }
                else
                {
                    DTCompany = MobjclsBLLMISPurchase.DisplayCompanyHeaderReport(Company);
                }
                AllReport();
                DataTable DTPurchaseIndent = MobjclsBLLMISPurchase.DisplayPurchaseindentReport(Company, Department, strFrom, strTo, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                DataTable DTRFQ = MobjclsBLLMISPurchase.DisplayRFQ(Company, strFrom, strTo, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                DataTable DTPurchaseQuotation = MobjclsBLLMISPurchase.DisplayPurchaseQutationReport(Company, Vendor, strFrom, strTo, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                DataTable DTPurchaseOrder = MobjclsBLLMISPurchase.DisplayPurchaseOrderReport(Company, Vendor, strFrom, strTo, intOrderType, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                DataTable DTGRN = MobjclsBLLMISPurchase.DisplayGRNReport(Company, Vendor, strFrom, strTo, intOrderType, intWarehouse, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                DataTable DTPurchaseInvoice = MobjclsBLLMISPurchase.DisplayPurchaseInvoiceReport(Company, Vendor, strFrom, strTo, intOrderType, intWarehouse, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                DataTable DTDebitNote= MobjclsBLLMISPurchase.DisplayDebitNoteReport(Company, Vendor, strFrom, strTo, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
             
                if (DTPurchaseIndent.Rows.Count <= 0 && DTPurchaseQuotation.Rows.Count <= 0 && DTPurchaseOrder.Rows.Count <= 0 && DTGRN.Rows.Count <= 0 && DTPurchaseInvoice.Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", DTPurchaseIndent));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", DTRFQ));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", DTPurchaseQuotation));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", DTPurchaseOrder));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", DTGRN));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", DTPurchaseInvoice));
                if (intDate != 3)  // Due Date Checking
                {
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", DTDebitNote));
                }
                else
                {
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", ""));
                }
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
            }
            if (Type == 1)
            {
                DataSet dtsetPurchaseIndent = MobjclsBLLMISPurchase.DisplayPurchaseindent(Company, Department, strFrom, strTo, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                if (dtsetPurchaseIndent.Tables[0].Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                if (intNo > 0)
                {
                    DataSet dtSet = MobjclsBLLMISPurchase.DisplayPurchaseindent(Company, Department, strFrom, strTo, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", dtSet.Tables[0]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", dtSet.Tables[1]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
                }
                else
                {
                    DTCompany = new DataTable();
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        DTCompany = MobjclsBLLMISPurchase.DisplayALLCompanyHeaderReport();
                    }
                    else
                    {
                        DTCompany = MobjclsBLLMISPurchase.DisplayCompanyHeaderReport(Company);
                    }
                    AllReport();
                    DataTable DTPurchaseIndent = MobjclsBLLMISPurchase.DisplayPurchaseindentReport(Company, Department, strFrom, strTo, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                    if (DTPurchaseIndent.Rows.Count <= 0)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", DTPurchaseIndent));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
                }
            }

            if (Type == 2)
            {
                DataSet dtsetPurchaseQuotation = MobjclsBLLMISPurchase.DisplayPurchaseQutation(Company, Vendor, strFrom, strTo, Type, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                if (dtsetPurchaseQuotation.Tables[0].Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr,957,out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                if (intNo > 0)
                {
                    DataSet dtSet = MobjclsBLLMISPurchase.DisplayPurchaseQutation(Company, Vendor, strFrom, strTo, Type, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster",dtSet.Tables[0]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails",dtSet.Tables[1]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster",dtSet.Tables[2]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", dtSet.Tables[3]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", dtSet.Tables[4]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
                }
                else
                {
                    DTCompany = new DataTable();
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        DTCompany = MobjclsBLLMISPurchase.DisplayALLCompanyHeaderReport();
                    }
                    else
                    {
                        DTCompany = MobjclsBLLMISPurchase.DisplayCompanyHeaderReport(Company);
                    }
                    AllReport();
                    DataTable DTPurchaseQuotation = MobjclsBLLMISPurchase.DisplayPurchaseQutationReport(Company, Vendor, strFrom, strTo, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                    if (DTPurchaseQuotation.Rows.Count <= 0)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", DTPurchaseQuotation));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", ""));
                    //ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
                }
            }

            if (Type == 3)
            {
                DataSet dtset = MobjclsBLLMISPurchase.DisplayPurchaseOrder(Company, Vendor, strFrom, strTo, intOrderType, Type, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                if (dtset.Tables[0].Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                if (intNo > 0)
                {
                    DataSet dtsetPurchaseOrder = MobjclsBLLMISPurchase.DisplayPurchaseOrder(Company, Vendor, strFrom, strTo, intOrderType, Type, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster",dtsetPurchaseOrder.Tables[0]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails",dtsetPurchaseOrder.Tables[1] ));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster",dtsetPurchaseOrder.Tables[2] ));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", dtsetPurchaseOrder.Tables[3]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", dtsetPurchaseOrder.Tables[4]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", ""));
                    //ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
                }
                else
                {
                    DTCompany = new DataTable();
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        DTCompany = MobjclsBLLMISPurchase.DisplayALLCompanyHeaderReport();
                    }
                    else
                    {
                        DTCompany = MobjclsBLLMISPurchase.DisplayCompanyHeaderReport(Company);
                    }
                    AllReport();
                    DataTable DTPurchaseOrder = MobjclsBLLMISPurchase.DisplayPurchaseOrderReport(Company, Vendor, strFrom, strTo, intOrderType, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                    if (DTPurchaseOrder.Rows.Count <= 0)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", DTPurchaseOrder));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", ""));
                    //ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
                }
            }
            if (Type == 4)
            {
                DataSet dtsetgrn = MobjclsBLLMISPurchase.DisplayGRN(Company, Vendor, strFrom, strTo, intOrderType, intWarehouse, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                if (dtsetgrn.Tables[0].Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                if (intNo > 0)
                {
                    DataSet dtSet = MobjclsBLLMISPurchase.DisplayGRN(Company, Vendor, strFrom, strTo, intOrderType, intWarehouse, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", dtSet.Tables[0]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", dtSet.Tables[1]));
                    //ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", dtSet.Tables[2]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster",""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", ""));                   
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
                }
                else
                {
                    DTCompany = new DataTable();
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        DTCompany = MobjclsBLLMISPurchase.DisplayALLCompanyHeaderReport();
                    }
                    else
                    {
                        DTCompany = MobjclsBLLMISPurchase.DisplayCompanyHeaderReport(Company);
                    }
                    AllReport();
                    DataTable DTGRN = MobjclsBLLMISPurchase.DisplayGRNReport(Company, Vendor, strFrom, strTo, intOrderType, intWarehouse, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                    if (DTGRN.Rows.Count <= 0)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", DTGRN));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
                }
            }
            if (Type == 5)
            {
                DataSet dtsetPurchaseInvoice = MobjclsBLLMISPurchase.DisplayPurchaseInvoice(Company, Vendor, strFrom, strTo, intOrderType, Type, intWarehouse, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                if (dtsetPurchaseInvoice.Tables[0].Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                if (intNo > 0)
                {
                    DataSet dtSet = MobjclsBLLMISPurchase.DisplayPurchaseInvoice(Company, Vendor, strFrom, strTo, intOrderType, Type, intWarehouse, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", dtSet.Tables[0]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", dtSet.Tables[1]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", dtSet.Tables[2]));
                    //ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", dtSet.Tables[3]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", dtSet.Tables[3]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails",""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster",""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
                }
                else
                {
                    DTCompany = new DataTable();
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        DTCompany = MobjclsBLLMISPurchase.DisplayALLCompanyHeaderReport();
                    }
                    else
                    {
                        DTCompany = MobjclsBLLMISPurchase.DisplayCompanyHeaderReport(Company);
                    }
                    AllReport();
                    DataTable DTPurchaseInvoice = MobjclsBLLMISPurchase.DisplayPurchaseInvoiceReport(Company, Vendor, strFrom, strTo, intOrderType, intWarehouse, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                    if (DTPurchaseInvoice.Rows.Count <= 0)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", DTPurchaseInvoice));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
                }
            }
            if (Type == 23)
            {
                DataSet dtsetRFQ = MobjclsBLLMISPurchase.DisplayRFQReport(Company, strFrom, strTo, Type, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                if (dtsetRFQ.Tables[0].Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                if (intNo > 0)
                {
                    MsReportPath = Application.StartupPath + "\\MainReports\\RptRFQForm.rdlc";
                    ReportViewer1.LocalReport.ReportPath = MsReportPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportViewer1.LocalReport.SetParameters(ReportParameters);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    DataSet dtSet = MobjclsBLLMISPurchase.DisplayRFQReport(Company, strFrom, strTo, Type, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", dtSet.Tables[0]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", dtSet.Tables[1]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", dtSet.Tables[2]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
                }
                else
                {
                    DTCompany = new DataTable();
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        DTCompany = MobjclsBLLMISPurchase.DisplayALLCompanyHeaderReport();
                    }
                    else
                    {
                        DTCompany = MobjclsBLLMISPurchase.DisplayCompanyHeaderReport(Company);
                    }
                    AllReport();
                    DataTable DTRFQ = MobjclsBLLMISPurchase.DisplayRFQ(Company, strFrom, strTo, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                    if (DTRFQ.Rows.Count <= 0)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", DTRFQ));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
                }
            }
            if (Type == 6)
            {
                DataSet DTSetDebitNote = MobjclsBLLMISPurchase.DisplayDebitNote(Company, Vendor, strFrom, strTo, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                if (DTSetDebitNote.Tables[0].Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                if (intNo > 0)
                {
                    DataSet DTSetDebitNotes = MobjclsBLLMISPurchase.DisplayDebitNote(Company, Vendor, strFrom, strTo, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", DTSetDebitNotes.Tables[0]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", DTSetDebitNotes.Tables[1]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", ""));

                }
                else
                {
                    DTCompany = new DataTable();
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        DTCompany = MobjclsBLLMISPurchase.DisplayALLCompanyHeaderReport();
                    }
                    else
                    {
                        DTCompany = MobjclsBLLMISPurchase.DisplayCompanyHeaderReport(Company);
                    }
                    AllReport();
                    DataTable DTDebitNote = MobjclsBLLMISPurchase.DisplayDebitNoteReport(Company, Vendor, strFrom, strTo, intstatus, intNo, intExecutive, intchkFrom, intchkTo, intDate, strPermittedCompany);
                    if (DTDebitNote.Rows.Count <= 0)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteMaster", DTDebitNote));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseInvoiceDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseInvoiceExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseOrderExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STPurchaseQuotationExpenseMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_PurchaseIndentDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STRFQDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_GRNExtraItems", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STDocumentMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_STTermsAndConditions", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteItemDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchase_DebitNoteLocationDetails", ""));
                }
            }
            ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
            ReportViewer1.ZoomMode = ZoomMode.Percent;
            this.Cursor = Cursors.Default;
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            try
            {
                ReportViewer1.Clear();
                ReportViewer1.Reset();
                ErpPurchaseReport.Clear();
                if (PurchaseReportValidation())
                {
                    BtnShow.Click -= new EventHandler(BtnShow_Click);
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //{
                    //    BtnShow.Enabled = false;
                    //    ShowReport();
                    //}
                    //else
                    //{
                        if (ReportValidation())
                        {
                            BtnShow.Enabled = false;
                            ShowReport();
                        }
                    //}
                    TmrPurchaseReport.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                TmrPurchaseReport.Enabled = true;
            }
        }

        #region "Validation For Purchase Report"
        private bool ReportValidation()
        {
            if (CboCompany.DataSource == null)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9014, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                CboCompany.Focus();
                return false;
            }
            return true;
        }
        private bool PurchaseReportValidation()
        {
            if (CboCompany.Enabled == true)
            {
                if (CboCompany.DataSource == null || CboCompany.SelectedValue == null || CboCompany.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9100, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPurchaseReport.SetError(CboCompany, MsMessageCommon.Replace("#", "").Trim());
                    CboCompany.Focus();
                    return false;
                }
            }
            if (cboExecutive.Enabled == true)
            {
                if (cboExecutive.DataSource == null || cboExecutive.SelectedValue == null || cboExecutive.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9101, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPurchaseReport.SetError(cboExecutive, MsMessageCommon.Replace("#", "").Trim());
                    cboExecutive.Focus();
                    return false;
                }
            }
            if (CboType.Enabled == true)
            {
                if (CboType.DataSource == null || CboType.SelectedValue == null || CboType.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9102, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPurchaseReport.SetError(CboType, MsMessageCommon.Replace("#", "").Trim());
                    CboType.Focus();
                    return false;
                }
            }
            if (cboStatus.Enabled == true)
            {
                if (cboStatus.DataSource == null || cboStatus.SelectedValue == null || cboStatus.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9103, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPurchaseReport.SetError(cboStatus, MsMessageCommon.Replace("#", "").Trim());
                    cboStatus.Focus();
                    return false;
                }
            }
            if (CboVendor.Enabled == true)
            {
                if (CboVendor.DataSource == null || CboVendor.SelectedValue == null || CboVendor.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9104, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPurchaseReport.SetError(CboVendor, MsMessageCommon.Replace("#", "").Trim());
                    CboVendor.Focus();
                    return false;
                }
            }
            if (CboWarehouse.Enabled == true)
            {
                if (CboWarehouse.DataSource == null || CboWarehouse.SelectedValue == null || CboWarehouse.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9105, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPurchaseReport.SetError(CboWarehouse, MsMessageCommon.Replace("#", "").Trim());
                    CboWarehouse.Focus();
                    return false;
                }
            }
            if (cboOrder.Enabled == true)
            {
                if (cboOrder.DataSource == null || cboOrder.SelectedValue == null || cboOrder.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9106, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPurchaseReport.SetError(cboOrder, MsMessageCommon.Replace("#", "").Trim());
                    cboOrder.Focus();
                    return false;
                }
            }
            if (CboOrderType.Enabled == true)
            {
                if (CboOrderType.DataSource == null || CboOrderType.SelectedValue == null || CboOrderType.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9107, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPurchaseReport.SetError(CboOrderType, MsMessageCommon.Replace("#", "").Trim());
                    CboOrderType.Focus();
                    return false;
                }
            }
            if (cboDate.Enabled == true)
            {
                if (cboDate.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9108, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPurchaseReport.SetError(cboDate, MsMessageCommon.Replace("#", "").Trim());
                    cboDate.Focus();
                    return false;
                }
            }
            if (CboDepartment.Enabled == true)
            {
                if (CboDepartment.DataSource == null || CboDepartment.SelectedValue == null || CboDepartment.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9109, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPurchaseReport.SetError(CboDepartment, MsMessageCommon.Replace("#", "").Trim());
                    CboDepartment.Focus();
                    return false;
                }
            }
            //Check whether From Date is after To Date
            if (DtpFromDate.Value.Date > DtpToDate.Value.Date)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2281, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErpPurchaseReport.SetError(CboDepartment, MsMessageCommon.Replace("#", "").Trim());
                DtpFromDate.Focus();
                return false;
            }
            return true;
        }
        #endregion

        private void Timer_Tick(object sender, EventArgs e)
        {
            TmrPurchaseReport.Enabled = false;
            BtnShow.Click +=new EventHandler (BtnShow_Click);
            BtnShow.Enabled = true;
            ErpPurchaseReport.Clear();
        }    

        private void DtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
        }

        private void DtpToDate_ValueChanged(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
        }     

        //private void chkSummary_CheckedChanged(object sender, EventArgs e)
        //{
        //    ReportViewer1.Clear();
        //    ReportViewer1.Reset();

        //    if (chkSummary.Checked == true)
        //    {
        //        DtpFromDate.Enabled = false;
        //        lblFrom.Enabled = false;
        //        lblTo.Enabled = false;
        //        DtpToDate.Enabled = false;
        //    }
        //    else
        //    {
        //        DtpFromDate.Enabled = true;
        //        lblFrom.Enabled = true;
        //        lblTo.Enabled = true;
        //        DtpToDate.Enabled = true;
        //    }
        //}       
        private void Cbo_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        private void cboDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErpPurchaseReport.Clear();
            if (cboDate.SelectedIndex == 0)
            {
                lblFrom.Enabled = false;
                lblTo.Enabled = false;
                DtpFromDate.Enabled = false;
                DtpToDate.Enabled = false;
                DtpFromDate.LockUpdateChecked = false;
                DtpToDate.LockUpdateChecked = false;
            }
            else
            {
                lblFrom.Enabled = true;
                lblTo.Enabled = true;
                DtpFromDate.Enabled = true;
                DtpToDate.Enabled = true;
                DtpFromDate.LockUpdateChecked = true;
            }
        }

        private void cboOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboOrder.SelectedIndex != -1)
            {
                if (cboOrder.SelectedIndex == 0)
                {
                    CboOrderType.Enabled = true;
                    cboDate.Enabled = true;
                    DtpFromDate.Enabled = true;
                    DtpToDate.Enabled = true;
                    if ((Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseQuotation)
                    || (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.DebitNote))
                        
                    {
                        CboOrderType.Enabled = false;

                    }
                    
                }
                else
                {

                    CboOrderType.Enabled = false;
                    cboDate.Enabled = false;
                    DtpFromDate.Enabled = false;
                    DtpToDate.Enabled = false;
                    DtpFromDate.LockUpdateChecked = false;
                    DtpToDate.LockUpdateChecked = false;
                    if ((Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.GRN) || (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice) || (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder))
                    
                        {
                            CboOrderType.SelectedIndex = 0;
                            cboDate.SelectedIndex = 0;
                            
                        }
                    //if (Convert.ToInt32(CboType.SelectedValue) != (int)OperationType.DebitNote)
                    //{
                    //    CboOrderType.SelectedIndex = 0;
                    //    cboDate.SelectedIndex = 0;

                    //}
                    
                }
            }
        }
    }
}
