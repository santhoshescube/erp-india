﻿namespace MyBooksERP
{
    partial class FrmRptGRN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptGRN));
            this.exPnlRptGRN = new DevComponents.DotNetBar.ExpandablePanel();
            this.lblTo = new DevComponents.DotNetBar.LabelX();
            this.lblFrom = new DevComponents.DotNetBar.LabelX();
            this.DtpToDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.DtpFromDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.btnShow = new DevComponents.DotNetBar.ButtonX();
            this.cboStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ALL = new DevComponents.Editors.ComboItem();
            this.Pending = new DevComponents.Editors.ComboItem();
            this.Completed = new DevComponents.Editors.ComboItem();
            this.lblStatus = new DevComponents.DotNetBar.LabelX();
            this.cboGRNNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblGRNNo = new DevComponents.DotNetBar.LabelX();
            this.cboSupplier = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSupplier = new DevComponents.DotNetBar.LabelX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.rptGRN = new Microsoft.Reporting.WinForms.ReportViewer();
            this.ErrRptGRN = new System.Windows.Forms.ErrorProvider(this.components);
            this.exPnlRptGRN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtpToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrRptGRN)).BeginInit();
            this.SuspendLayout();
            // 
            // exPnlRptGRN
            // 
            this.exPnlRptGRN.CanvasColor = System.Drawing.SystemColors.Control;
            this.exPnlRptGRN.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.exPnlRptGRN.Controls.Add(this.lblTo);
            this.exPnlRptGRN.Controls.Add(this.lblFrom);
            this.exPnlRptGRN.Controls.Add(this.DtpToDate);
            this.exPnlRptGRN.Controls.Add(this.DtpFromDate);
            this.exPnlRptGRN.Controls.Add(this.btnShow);
            this.exPnlRptGRN.Controls.Add(this.cboStatus);
            this.exPnlRptGRN.Controls.Add(this.lblStatus);
            this.exPnlRptGRN.Controls.Add(this.cboGRNNo);
            this.exPnlRptGRN.Controls.Add(this.lblGRNNo);
            this.exPnlRptGRN.Controls.Add(this.cboSupplier);
            this.exPnlRptGRN.Controls.Add(this.lblSupplier);
            this.exPnlRptGRN.Controls.Add(this.cboCompany);
            this.exPnlRptGRN.Controls.Add(this.lblCompany);
            this.exPnlRptGRN.Dock = System.Windows.Forms.DockStyle.Top;
            this.exPnlRptGRN.Location = new System.Drawing.Point(0, 0);
            this.exPnlRptGRN.Name = "exPnlRptGRN";
            this.exPnlRptGRN.Size = new System.Drawing.Size(1023, 97);
            this.exPnlRptGRN.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.exPnlRptGRN.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.exPnlRptGRN.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.exPnlRptGRN.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.exPnlRptGRN.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.exPnlRptGRN.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.exPnlRptGRN.Style.GradientAngle = 90;
            this.exPnlRptGRN.TabIndex = 0;
            this.exPnlRptGRN.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.exPnlRptGRN.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.exPnlRptGRN.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.exPnlRptGRN.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.exPnlRptGRN.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.exPnlRptGRN.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.exPnlRptGRN.TitleStyle.GradientAngle = 90;
            this.exPnlRptGRN.TitleText = "Filter By";
            // 
            // lblTo
            // 
            // 
            // 
            // 
            this.lblTo.BackgroundStyle.Class = "";
            this.lblTo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTo.Location = new System.Drawing.Point(471, 64);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 19);
            this.lblTo.TabIndex = 13;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            // 
            // 
            // 
            this.lblFrom.BackgroundStyle.Class = "";
            this.lblFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFrom.Location = new System.Drawing.Point(471, 34);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(34, 22);
            this.lblFrom.TabIndex = 11;
            this.lblFrom.Text = "From";
            // 
            // DtpToDate
            // 
            // 
            // 
            // 
            this.DtpToDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpToDate.ButtonDropDown.Visible = true;
            this.DtpToDate.CustomFormat = "dd MMM yyyy";
            this.DtpToDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpToDate.IsPopupCalendarOpen = false;
            this.DtpToDate.Location = new System.Drawing.Point(513, 64);
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.DtpToDate.MonthCalendar.BackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.DisplayMonth = new System.DateTime(2011, 4, 1, 0, 0, 0, 0);
            this.DtpToDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpToDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.TodayButtonVisible = true;
            this.DtpToDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpToDate.Name = "DtpToDate";
            this.DtpToDate.ShowCheckBox = true;
            this.DtpToDate.Size = new System.Drawing.Size(115, 20);
            this.DtpToDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpToDate.TabIndex = 14;
            this.DtpToDate.ValueChanged += new System.EventHandler(this.DtpToDate_ValueChanged);
            // 
            // DtpFromDate
            // 
            // 
            // 
            // 
            this.DtpFromDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpFromDate.ButtonDropDown.Visible = true;
            this.DtpFromDate.CustomFormat = "dd MMM yyyy";
            this.DtpFromDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpFromDate.IsPopupCalendarOpen = false;
            this.DtpFromDate.Location = new System.Drawing.Point(513, 33);
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.DtpFromDate.MonthCalendar.BackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.DisplayMonth = new System.DateTime(2011, 4, 1, 0, 0, 0, 0);
            this.DtpFromDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpFromDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.TodayButtonVisible = true;
            this.DtpFromDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpFromDate.Name = "DtpFromDate";
            this.DtpFromDate.ShowCheckBox = true;
            this.DtpFromDate.Size = new System.Drawing.Size(115, 20);
            this.DtpFromDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpFromDate.TabIndex = 12;
            this.DtpFromDate.ValueChanged += new System.EventHandler(this.DtpFromDate_ValueChanged);
            // 
            // btnShow
            // 
            this.btnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShow.Location = new System.Drawing.Point(652, 61);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 23);
            this.btnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShow.TabIndex = 9;
            this.btnShow.Text = "Show";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // cboStatus
            // 
            this.cboStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboStatus.DisplayMember = "Text";
            this.cboStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.ItemHeight = 14;
            this.cboStatus.Items.AddRange(new object[] {
            this.ALL,
            this.Pending,
            this.Completed});
            this.cboStatus.Location = new System.Drawing.Point(327, 64);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Size = new System.Drawing.Size(121, 20);
            this.cboStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboStatus.TabIndex = 8;
            this.cboStatus.SelectedIndexChanged += new System.EventHandler(this.cboStatus_SelectedIndexChanged);
            this.cboStatus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboStatus_KeyPress);
            // 
            // ALL
            // 
            this.ALL.Text = "ALL";
            // 
            // Pending
            // 
            this.Pending.Text = "Pending";
            // 
            // Completed
            // 
            this.Completed.Text = "Completed";
            // 
            // lblStatus
            // 
            // 
            // 
            // 
            this.lblStatus.BackgroundStyle.Class = "";
            this.lblStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblStatus.Location = new System.Drawing.Point(270, 64);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(59, 19);
            this.lblStatus.TabIndex = 7;
            this.lblStatus.Text = "Status";
            // 
            // cboGRNNo
            // 
            this.cboGRNNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboGRNNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboGRNNo.DisplayMember = "Text";
            this.cboGRNNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboGRNNo.DropDownHeight = 134;
            this.cboGRNNo.FormattingEnabled = true;
            this.cboGRNNo.IntegralHeight = false;
            this.cboGRNNo.ItemHeight = 14;
            this.cboGRNNo.Location = new System.Drawing.Point(327, 34);
            this.cboGRNNo.Name = "cboGRNNo";
            this.cboGRNNo.Size = new System.Drawing.Size(121, 20);
            this.cboGRNNo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboGRNNo.TabIndex = 6;
            this.cboGRNNo.SelectedIndexChanged += new System.EventHandler(this.cboGRNNo_SelectedIndexChanged);
            this.cboGRNNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboGRNNo_KeyPress);
            // 
            // lblGRNNo
            // 
            // 
            // 
            // 
            this.lblGRNNo.BackgroundStyle.Class = "";
            this.lblGRNNo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblGRNNo.Location = new System.Drawing.Point(270, 34);
            this.lblGRNNo.Name = "lblGRNNo";
            this.lblGRNNo.Size = new System.Drawing.Size(59, 19);
            this.lblGRNNo.TabIndex = 5;
            this.lblGRNNo.Text = "GRN No";
            // 
            // cboSupplier
            // 
            this.cboSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSupplier.DisplayMember = "Text";
            this.cboSupplier.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSupplier.DropDownHeight = 75;
            this.cboSupplier.DropDownWidth = 158;
            this.cboSupplier.FormattingEnabled = true;
            this.cboSupplier.IntegralHeight = false;
            this.cboSupplier.ItemHeight = 14;
            this.cboSupplier.Location = new System.Drawing.Point(73, 64);
            this.cboSupplier.Name = "cboSupplier";
            this.cboSupplier.Size = new System.Drawing.Size(158, 20);
            this.cboSupplier.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboSupplier.TabIndex = 4;
            this.cboSupplier.SelectedIndexChanged += new System.EventHandler(this.cboSupplier_SelectedIndexChanged);
            this.cboSupplier.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboSupplier_KeyPress);
            // 
            // lblSupplier
            // 
            // 
            // 
            // 
            this.lblSupplier.BackgroundStyle.Class = "";
            this.lblSupplier.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSupplier.Location = new System.Drawing.Point(15, 64);
            this.lblSupplier.Name = "lblSupplier";
            this.lblSupplier.Size = new System.Drawing.Size(52, 23);
            this.lblSupplier.TabIndex = 3;
            this.lblSupplier.Text = "Supplier";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 75;
            this.cboCompany.DropDownWidth = 158;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(73, 34);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(158, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 2;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCompany_KeyPress);
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(15, 34);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(52, 23);
            this.lblCompany.TabIndex = 1;
            this.lblCompany.Text = "Company";
            // 
            // rptGRN
            // 
            this.rptGRN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptGRN.Location = new System.Drawing.Point(0, 97);
            this.rptGRN.Name = "rptGRN";
            this.rptGRN.Size = new System.Drawing.Size(1023, 301);
            this.rptGRN.TabIndex = 1;
            // 
            // ErrRptGRN
            // 
            this.ErrRptGRN.ContainerControl = this;
            // 
            // FrmRptGRN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 398);
            this.Controls.Add(this.rptGRN);
            this.Controls.Add(this.exPnlRptGRN);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptGRN";
            this.Text = "GRN";
            this.Load += new System.EventHandler(this.FrmRptGRN_Load);
            this.exPnlRptGRN.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DtpToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrRptGRN)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel exPnlRptGRN;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSupplier;
        private DevComponents.DotNetBar.LabelX lblSupplier;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboGRNNo;
        private DevComponents.DotNetBar.LabelX lblGRNNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboStatus;
        private DevComponents.DotNetBar.LabelX lblStatus;
        private DevComponents.DotNetBar.ButtonX btnShow;
        private DevComponents.Editors.ComboItem ALL;
        private DevComponents.Editors.ComboItem Pending;
        private DevComponents.Editors.ComboItem Completed;
        private Microsoft.Reporting.WinForms.ReportViewer rptGRN;
        private System.Windows.Forms.ErrorProvider ErrRptGRN;
        private DevComponents.DotNetBar.LabelX lblTo;
        private DevComponents.DotNetBar.LabelX lblFrom;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpToDate;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpFromDate;

    }
}