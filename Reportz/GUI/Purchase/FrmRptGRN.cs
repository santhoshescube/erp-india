﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Microsoft.Reporting.WinForms;

namespace MyBooksERP
{
    public partial class FrmRptGRN : Form
    {
        clsBLLDirectGRNReport MobjClsBLLDirectGRN = null;
        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;
        ClsNotification MObjClsNotification;
        private string MsMessageCaption = "MyBooksERP";
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        int MintChkFrmTo = 0;
        public FrmRptGRN()
        {
            InitializeComponent();
            MobjClsBLLDirectGRN = new clsBLLDirectGRNReport();
        }

        private void FrmRptGRN_Load(object sender, EventArgs e)
        {
            FillCombo(0);
            cboStatus.SelectedIndex = 1;
            LoadMessage();
            DtpFromDate.Value = ClsCommonSettings.GetServerDate();
            DtpToDate.Value = ClsCommonSettings.GetServerDate();
            if (ClsCommonSettings.CompanyID > 0)
                cboCompany.SelectedValue = ClsCommonSettings.CompanyID;
        }


        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void cboSupplier_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboSupplier.DroppedDown = false;
        }

        private void cboGRNNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboGRNNo.DroppedDown = false;
        }

        private void cboStatus_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboStatus.DroppedDown = false;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ShowReport();
        }

        private void ShowReport()
        {
            ClearReport();

            int intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            int intStatusID = Convert.ToInt32(cboStatus.SelectedIndex );
            int intVendorID = Convert.ToInt32(cboSupplier.SelectedValue);
            long lngGRNID = Convert.ToInt64(cboGRNNo.SelectedValue);
            MintChkFrmTo = 0;
            DateTime? dtFromDate = null;
            DateTime? dtToDate = null;

            if (DtpFromDate.LockUpdateChecked)
            {
                dtFromDate = Convert.ToDateTime(DtpFromDate.Value);

            }
            if (DtpToDate.LockUpdateChecked)
            {
                dtToDate = Convert.ToDateTime(DtpToDate.Value);
            }
            if (DtpFromDate.LockUpdateChecked && DtpToDate.LockUpdateChecked)
            {
                MintChkFrmTo = 1;
            }
            else if (DtpFromDate.LockUpdateChecked && !DtpToDate.LockUpdateChecked)
            {
                MintChkFrmTo = 2;
            }
            else if (!DtpFromDate.LockUpdateChecked && DtpToDate.LockUpdateChecked)
            {
                MintChkFrmTo = 3;
            }

            rptGRN.Clear();
            rptGRN.Reset();

            if (ValidateReport())
            {
                string PsReportFooter = ClsCommonSettings.ReportFooter;
                string MsReportPath = Application.StartupPath + "\\MainReports\\RptGRN.rdlc";

              
                rptGRN.LocalReport.ReportPath = MsReportPath;

                DataSet dsReport = MobjClsBLLDirectGRN.GetReport(intCompanyID, intVendorID, lngGRNID, intStatusID, dtFromDate, dtToDate, MintChkFrmTo);
                if (dsReport != null && dsReport.Tables.Count > 0)
                {
                    if (dsReport.Tables[1].Rows.Count > 0)
                    {
                        rptGRN.LocalReport.DataSources.Clear();
                        ReportParameter[] ReportParameters = new ReportParameter[6];
                        ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameters[1] = new ReportParameter("Company", cboCompany.Text.Trim(), false);
                        ReportParameters[2] = new ReportParameter("Vendor", cboSupplier.Text.Trim(), false);
                        if(MintChkFrmTo == 0 || MintChkFrmTo == 1)
                        ReportParameters[3] = new ReportParameter("Period", DtpFromDate.Text.Trim() + " - " + DtpToDate.Text.Trim(), false);
                        else if (MintChkFrmTo == 2)
                        ReportParameters[3] = new ReportParameter("Period", DtpFromDate.Text.Trim(), false);
                        else
                        ReportParameters[3] = new ReportParameter("Period", DtpToDate.Text.Trim(), false);

                        ReportParameters[4] = new ReportParameter("GRNNo", cboGRNNo.Text.Trim(), false);
                        ReportParameters[5] = new ReportParameter("chkFrmTo", MintChkFrmTo.ToString(), false);

                        rptGRN.LocalReport.SetParameters(ReportParameters);
                        rptGRN.LocalReport.DataSources.Clear();
                        rptGRN.LocalReport.DataSources.Add(new ReportDataSource("DtSetGRN_dtCompany", dsReport.Tables[0]));
                        rptGRN.LocalReport.DataSources.Add(new ReportDataSource("DtSetGRN_dtGRN", dsReport.Tables[1]));
                        rptGRN.LocalReport.DataSources.Add(new ReportDataSource("DtSetGRN_dtGRNDetails", dsReport.Tables[2]));

                        rptGRN.SetDisplayMode(DisplayMode.PrintLayout);
                        rptGRN.ZoomMode = ZoomMode.Percent;
                        this.Cursor = Cursors.Default;
                    }
                    else
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                }
           }
        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MObjClsNotification = new ClsNotification();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.Purchase, (int)ClsCommonSettings.ProductID);
        }

        private void FillCombo(int intType)
        {
            int intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            int intStatusID = Convert.ToInt32(cboStatus.SelectedValue);
            int intVendorID = Convert.ToInt32(cboSupplier.SelectedValue);
            long lngGRNID = Convert.ToInt64(cboGRNNo.SelectedValue);
            
            DataSet dsCombo = MobjClsBLLDirectGRN.FillCombo(ClsCommonSettings.LoginCompanyID, intVendorID, lngGRNID,intStatusID);

            if (dsCombo != null && dsCombo.Tables.Count > 0)
            {
                if (intType == 0)
                {
                    if (dsCombo.Tables[0] != null && dsCombo.Tables[0].Rows.Count > 0)
                    {
                        cboCompany.DisplayMember = "CompanyName";
                        cboCompany.ValueMember = "CompanyID";
                        cboCompany.DataSource = dsCombo.Tables[0];
                    }
                    if (dsCombo.Tables[1] != null && dsCombo.Tables[1].Rows.Count > 0)
                    {
                        cboSupplier.DisplayMember = "VendorName";
                        cboSupplier.ValueMember = "VendorID";
                        cboSupplier.DataSource = dsCombo.Tables[1];
                    }
                }
                if (intType == 1)
                {
                    if (dsCombo.Tables[2] != null && dsCombo.Tables[2].Rows.Count > 0)
                    {
                        cboGRNNo.DisplayMember = "GRNNo";
                        cboGRNNo.ValueMember = "GRNID";
                        cboGRNNo.DataSource = dsCombo.Tables[2];
                    }
                }
            }
        }
        private bool ValidateReport()
        {

            if (cboCompany.DataSource == null || cboCompany.SelectedValue == null || cboCompany.SelectedIndex == -1)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9100, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrRptGRN.SetError(cboCompany, MsMessageCommon.Replace("#", "").Trim());
                cboCompany.Focus();
                return false;
            }
            if (cboSupplier.DataSource == null || cboSupplier.SelectedValue == null || cboSupplier.SelectedIndex == -1)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9104, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrRptGRN.SetError(cboSupplier, MsMessageCommon.Replace("#", "").Trim());
                cboSupplier.Focus();
                return false;
            }
            if (cboGRNNo.DataSource == null || cboGRNNo.SelectedValue == null || cboGRNNo.SelectedIndex == -1)
            {
                MsMessageCommon = (MObjClsNotification.GetErrorMessage(MaMessageArr, 9104, out MmessageIcon)).Replace("Supplier", "GRN No");
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrRptGRN.SetError(cboGRNNo, MsMessageCommon.Replace("#", "").Trim());
                cboGRNNo.Focus();
                return false;
            }
            if (cboStatus.SelectedIndex == -1)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2285, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrRptGRN.SetError(cboStatus, MsMessageCommon.Replace("#", "").Trim());
                cboStatus.Focus();
                return false;
            }
            if ((MintChkFrmTo == 1 && DtpFromDate.Value.Date > DtpToDate.Value.Date) || (DtpFromDate.LockUpdateChecked && DtpFromDate.Value <= DateTime.MinValue) || (DtpToDate.LockUpdateChecked && DtpToDate.Value <= DateTime.MinValue))
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9766, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrRptGRN.SetError(DtpFromDate, MsMessageCommon.Replace("#", "").Trim());
                DtpFromDate.Focus();
                return false;
            }
            
            return true;
        }

        private void ClearReport()
        {
            rptGRN.Clear();
            rptGRN.Refresh();
            ErrRptGRN.Clear();

        }
        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
            FillCombo(1);
        }

        private void cboSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
            FillCombo(1);
        }

        private void cboGRNNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
            if (cboGRNNo.SelectedValue.ToInt32() > 0)
            {
                DtpFromDate.Enabled = false;
                DtpToDate.Enabled = false;
                DtpFromDate.LockUpdateChecked = false;
                DtpToDate.LockUpdateChecked = false;
                cboStatus.Enabled = false;
                cboStatus.SelectedIndex = 0;

            }
            else
            {
                DtpFromDate.Enabled = true;
                DtpToDate.Enabled = true;
                cboStatus.Enabled = true;
            }
        }

        private void cboStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void DtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void DtpToDate_ValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

       
    }
}
