﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Microsoft.Reporting.WinForms;

namespace MyBooksERP
{
    public partial class FrmRptPurchaseSummary : Form
    {
        clsBLLRptPurchaseSummary MobjclsBLLRptPurchaseSummary;

        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;
        ClsNotification MObjClsNotification;
        private string MsMessageCaption = "MyBooksERP";
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;


        public FrmRptPurchaseSummary()
        {
            InitializeComponent();
            MobjclsBLLRptPurchaseSummary = new clsBLLRptPurchaseSummary();
            MObjClsNotification = new ClsNotification();
        }

        private void FrmRptPurchaseSummary_Load(object sender, EventArgs e)
        {
            DtpFromDate.Text = ClsCommonSettings.GetServerDate().ToString();
            DtpToDate.Text = ClsCommonSettings.GetServerDate().ToString();
            LoadMessage();
            LoadCombos(0);
            if (ClsCommonSettings.CompanyID > 0)
                CboCompany.SelectedValue = ClsCommonSettings.CompanyID;
        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.Purchase, (int)ClsCommonSettings.ProductID);
        }

        private void LoadCombos(int intType)
        {
            int intCompanyID = CboCompany.SelectedValue.ToInt32();

            DataSet dsCombo = MobjclsBLLRptPurchaseSummary.LoadCombo(ClsCommonSettings.LoginCompanyID);

            if (dsCombo != null && dsCombo.Tables.Count > 0)
            {
                if (intType == 0)
                {
                    if (dsCombo.Tables[0] != null && dsCombo.Tables[0].Rows.Count > 0)
                    {
                        CboCompany.DataSource = dsCombo.Tables[0];
                        CboCompany.DisplayMember = "CompanyName";
                        CboCompany.ValueMember = "CompanyID";
                    }
                   
                    if (dsCombo.Tables[1] != null && dsCombo.Tables[1].Rows.Count > 0)
                    {
                        CboVendor.DataSource = dsCombo.Tables[1];
                        CboVendor.DisplayMember = "VendorName";
                        CboVendor.ValueMember = "VendorID";
                    }

                    LoadStatus();
                }

                if (intType == 1)
                {
                    if (dsCombo.Tables[2] != null && dsCombo.Tables[2].Rows.Count > 0)
                    {
                        cboItem.DataSource = dsCombo.Tables[2];
                        cboItem.DisplayMember = "ItemName";
                        cboItem.ValueMember = "ItemID";
                    }
                }
            }
        }

        private void CboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErpPurchaseReport.Clear();
            LoadCombos(1);
        }

        private void LoadStatus()
        {
            DataTable dtStatus = new DataTable();
            dtStatus.Columns.Add(new DataColumn("StatusID", typeof(int)));
            dtStatus.Columns.Add(new DataColumn("Status", typeof(string)));

            DataRow dr = dtStatus.NewRow();
            dr["StatusID"] = 0;
            dr["Status"] = "ALL";
            dtStatus.Rows.Add(dr);

            dr = dtStatus.NewRow();
            dr["StatusID"] = 1;
            dr["Status"] = "Pending";
            dtStatus.Rows.Add(dr);


            dr = dtStatus.NewRow();
            dr["StatusID"] = 2;
            dr["Status"] = "Completed";
            dtStatus.Rows.Add(dr);

            cboStatus.DisplayMember = "Status";
            cboStatus.ValueMember = "StatusID";
            cboStatus.DataSource = dtStatus;
            cboStatus.SelectedValue = "1";


        }
        private void ShowReport()
        {
            if (ValidateReport())
            {
                int ItemID = cboItem.SelectedValue.ToInt32();
                int CompanyID = CboCompany.SelectedValue.ToInt32();
                int VendorID = CboVendor.SelectedValue.ToInt32();
                string PsReportFooter = ClsCommonSettings.ReportFooter;
                DateTime dtFromDate = Convert.ToDateTime(DtpFromDate.Value);
                DateTime dtToDate = Convert.ToDateTime(DtpToDate.Value);
                int intFilterType = Convert.ToInt32(cboStatus.SelectedValue);

                ReportViewer1.Clear();
                ReportViewer1.Reset();
                string MsReportPath = Application.StartupPath + "\\MainReports\\RptPurchaseSummary.rdlc";
                ReportViewer1.LocalReport.ReportPath = MsReportPath;
                DataSet dsReport = this.MobjclsBLLRptPurchaseSummary.GetReport(ItemID, CompanyID, VendorID, dtFromDate, dtToDate, intFilterType);
                if (dsReport != null && dsReport.Tables != null)
                {
                    if (dsReport.Tables[1] != null && dsReport.Tables[1].Rows.Count > 0)
                    {
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportParameter[] ReportParameters = new ReportParameter[7];
                        ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameters[1] = new ReportParameter("Item", cboItem.Text.Trim(), false);
                        ReportParameters[2] = new ReportParameter("Company", CboCompany.Text.Trim(), false);
                        ReportParameters[3] = new ReportParameter("Vendor", CboVendor.Text.Trim(), false);
                        ReportParameters[4] = new ReportParameter("FromDate", DtpFromDate.Text.Trim(), false);
                        ReportParameters[5] = new ReportParameter("ToDate", DtpToDate.Text.Trim(), false);
                        ReportParameters[6] = new ReportParameter("Status",cboStatus.Text.Trim(), false);

                        ReportViewer1.LocalReport.SetParameters(ReportParameters);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchaseSummary_dtCompany", dsReport.Tables[0]));
                        ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPurchaseSummary_dtPurchase", dsReport.Tables[1]));
                        ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                        ReportViewer1.ZoomMode = ZoomMode.Percent;
                        this.Cursor = Cursors.Default;
                        
                    }
                    else
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 957, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                  

                }
            }
            
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ShowReport();
        }

        private bool ValidateReport()
        {
            
                if (CboCompany.DataSource == null || CboCompany.SelectedValue == null || CboCompany.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9100, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPurchaseReport.SetError(CboCompany, MsMessageCommon.Replace("#", "").Trim());
                    CboCompany.Focus();
                    return false;
                }
                if (CboVendor.DataSource == null || CboVendor.SelectedValue == null || CboVendor.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9104, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPurchaseReport.SetError(CboVendor, MsMessageCommon.Replace("#", "").Trim());
                    CboVendor.Focus();
                    return false;
                }
                if (cboItem.DataSource == null || cboItem.SelectedValue == null || cboItem.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9765, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPurchaseReport.SetError(cboItem, MsMessageCommon.Replace("#", "").Trim());
                    cboItem.Focus();
                    return false;
                }
                if (cboStatus.DataSource == null || cboStatus.SelectedValue == null || cboStatus.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2285, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPurchaseReport.SetError(cboStatus, MsMessageCommon.Replace("#", "").Trim());
                    cboItem.Focus();
                    return false;
                }
                if (DtpFromDate.Value.Date > DtpToDate.Value.Date || DtpFromDate.Value <= DateTime.MinValue || DtpToDate.Value <= DateTime.MinValue)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9766, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    if (DtpToDate.Value <= DateTime.MinValue)
                    {
                        ErpPurchaseReport.SetError(DtpToDate, MsMessageCommon.Replace("#", "").Trim());
                        DtpToDate.Focus();
                    }
                    else
                    {
                        ErpPurchaseReport.SetError(DtpFromDate, MsMessageCommon.Replace("#", "").Trim());
                        DtpFromDate.Focus();
                    }
                    return false;
                }
            return true;
        }

        private void CboVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErpPurchaseReport.Clear();
        }

        private void cboItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErpPurchaseReport.Clear();
        }
      
        private void DtpFromDate_TextChanged(object sender, EventArgs e)
        {
            ErpPurchaseReport.Clear();
        }

        private void DtpToDate_TextChanged(object sender, EventArgs e)
        {
            ErpPurchaseReport.Clear();
        }

        private void CboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboCompany.DroppedDown = false;
        }

        private void CboVendor_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboVendor.DroppedDown = false;
        }

        private void cboItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboItem.DroppedDown = false;
        }
    }
}
