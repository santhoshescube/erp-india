﻿namespace MyBooksERP
{
    partial class FrmRptPurchase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptPurchase));
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.lblDates = new DevComponents.DotNetBar.LabelX();
            this.cboDate = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.Date = new DevComponents.Editors.ComboItem();
            this.CreatedDate = new DevComponents.Editors.ComboItem();
            this.DueDate = new DevComponents.Editors.ComboItem();
            this.cboExecutive = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblExecutive = new DevComponents.DotNetBar.LabelX();
            this.lblWarehouse = new DevComponents.DotNetBar.LabelX();
            this.CboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblDepartment = new DevComponents.DotNetBar.LabelX();
            this.CboDepartment = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblStatus = new DevComponents.DotNetBar.LabelX();
            this.lblNo = new DevComponents.DotNetBar.LabelX();
            this.cboOrder = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblorder = new DevComponents.DotNetBar.LabelX();
            this.CboOrderType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblTo = new DevComponents.DotNetBar.LabelX();
            this.lblFrom = new DevComponents.DotNetBar.LabelX();
            this.DtpToDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.DtpFromDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.lblType = new DevComponents.DotNetBar.LabelX();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.lblVendor = new DevComponents.DotNetBar.LabelX();
            this.CboType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboVendor = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.TmrPurchaseReport = new System.Windows.Forms.Timer(this.components);
            this.ErpPurchaseReport = new System.Windows.Forms.ErrorProvider(this.components);
            this.expandablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtpToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErpPurchaseReport)).BeginInit();
            this.SuspendLayout();
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.lblDates);
            this.expandablePanel1.Controls.Add(this.cboDate);
            this.expandablePanel1.Controls.Add(this.cboExecutive);
            this.expandablePanel1.Controls.Add(this.lblExecutive);
            this.expandablePanel1.Controls.Add(this.lblWarehouse);
            this.expandablePanel1.Controls.Add(this.CboWarehouse);
            this.expandablePanel1.Controls.Add(this.lblDepartment);
            this.expandablePanel1.Controls.Add(this.CboDepartment);
            this.expandablePanel1.Controls.Add(this.cboStatus);
            this.expandablePanel1.Controls.Add(this.lblStatus);
            this.expandablePanel1.Controls.Add(this.lblNo);
            this.expandablePanel1.Controls.Add(this.cboOrder);
            this.expandablePanel1.Controls.Add(this.lblorder);
            this.expandablePanel1.Controls.Add(this.CboOrderType);
            this.expandablePanel1.Controls.Add(this.lblTo);
            this.expandablePanel1.Controls.Add(this.lblFrom);
            this.expandablePanel1.Controls.Add(this.DtpToDate);
            this.expandablePanel1.Controls.Add(this.DtpFromDate);
            this.expandablePanel1.Controls.Add(this.BtnShow);
            this.expandablePanel1.Controls.Add(this.lblType);
            this.expandablePanel1.Controls.Add(this.lblCompany);
            this.expandablePanel1.Controls.Add(this.lblVendor);
            this.expandablePanel1.Controls.Add(this.CboType);
            this.expandablePanel1.Controls.Add(this.CboCompany);
            this.expandablePanel1.Controls.Add(this.CboVendor);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(948, 118);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 3;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By";
            // 
            // lblDates
            // 
            // 
            // 
            // 
            this.lblDates.BackgroundStyle.Class = "";
            this.lblDates.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDates.Location = new System.Drawing.Point(545, 78);
            this.lblDates.Name = "lblDates";
            this.lblDates.Size = new System.Drawing.Size(63, 23);
            this.lblDates.TabIndex = 14;
            this.lblDates.Text = "Date Type";
            // 
            // cboDate
            // 
            this.cboDate.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDate.DisplayMember = "Text";
            this.cboDate.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDate.DropDownHeight = 75;
            this.cboDate.FormattingEnabled = true;
            this.cboDate.IntegralHeight = false;
            this.cboDate.ItemHeight = 14;
            this.cboDate.Items.AddRange(new object[] {
            this.Date,
            this.CreatedDate,
            this.DueDate});
            this.cboDate.Location = new System.Drawing.Point(613, 83);
            this.cboDate.Name = "cboDate";
            this.cboDate.Size = new System.Drawing.Size(140, 20);
            this.cboDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboDate.TabIndex = 8;
            this.cboDate.SelectedIndexChanged += new System.EventHandler(this.cboDate_SelectedIndexChanged);
            this.cboDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cbo_KeyDown);
            // 
            // Date
            // 
            this.Date.Text = "Date";
            // 
            // CreatedDate
            // 
            this.CreatedDate.Text = "Created Date";
            // 
            // DueDate
            // 
            this.DueDate.Text = "Due Date";
            // 
            // cboExecutive
            // 
            this.cboExecutive.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboExecutive.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboExecutive.DisplayMember = "Text";
            this.cboExecutive.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboExecutive.FormattingEnabled = true;
            this.cboExecutive.ItemHeight = 14;
            this.cboExecutive.Location = new System.Drawing.Point(72, 59);
            this.cboExecutive.Name = "cboExecutive";
            this.cboExecutive.Size = new System.Drawing.Size(200, 20);
            this.cboExecutive.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboExecutive.TabIndex = 1;
            this.cboExecutive.SelectionChangeCommitted += new System.EventHandler(this.cboExecutive_SelectionChangeCommitted);
            this.cboExecutive.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cbo_KeyDown);
            // 
            // lblExecutive
            // 
            // 
            // 
            // 
            this.lblExecutive.BackgroundStyle.Class = "";
            this.lblExecutive.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblExecutive.Location = new System.Drawing.Point(14, 57);
            this.lblExecutive.Name = "lblExecutive";
            this.lblExecutive.Size = new System.Drawing.Size(56, 23);
            this.lblExecutive.TabIndex = 11;
            this.lblExecutive.Text = "Employee";
            // 
            // lblWarehouse
            // 
            // 
            // 
            // 
            this.lblWarehouse.BackgroundStyle.Class = "";
            this.lblWarehouse.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWarehouse.Location = new System.Drawing.Point(290, 78);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(62, 23);
            this.lblWarehouse.TabIndex = 5;
            this.lblWarehouse.Text = "Warehouse";
            // 
            // CboWarehouse
            // 
            this.CboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboWarehouse.DisplayMember = "Text";
            this.CboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboWarehouse.DropDownHeight = 75;
            this.CboWarehouse.FormattingEnabled = true;
            this.CboWarehouse.IntegralHeight = false;
            this.CboWarehouse.ItemHeight = 14;
            this.CboWarehouse.Location = new System.Drawing.Point(357, 83);
            this.CboWarehouse.Name = "CboWarehouse";
            this.CboWarehouse.Size = new System.Drawing.Size(168, 20);
            this.CboWarehouse.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboWarehouse.TabIndex = 5;
            this.CboWarehouse.SelectionChangeCommitted += new System.EventHandler(this.CboWarehouse_SelectionChangeCommitted);
            this.CboWarehouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cbo_KeyDown);
            // 
            // lblDepartment
            // 
            // 
            // 
            // 
            this.lblDepartment.BackgroundStyle.Class = "";
            this.lblDepartment.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDepartment.Location = new System.Drawing.Point(290, 54);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(61, 23);
            this.lblDepartment.TabIndex = 4;
            this.lblDepartment.Text = "Department";
            // 
            // CboDepartment
            // 
            this.CboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDepartment.DisplayMember = "Text";
            this.CboDepartment.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboDepartment.DropDownHeight = 75;
            this.CboDepartment.FormattingEnabled = true;
            this.CboDepartment.IntegralHeight = false;
            this.CboDepartment.ItemHeight = 14;
            this.CboDepartment.Location = new System.Drawing.Point(357, 59);
            this.CboDepartment.Name = "CboDepartment";
            this.CboDepartment.Size = new System.Drawing.Size(168, 20);
            this.CboDepartment.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboDepartment.TabIndex = 4;
            this.CboDepartment.SelectionChangeCommitted += new System.EventHandler(this.CboDepartment_SelectionChangeCommitted);
            this.CboDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cbo_KeyDown);
            // 
            // cboStatus
            // 
            this.cboStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboStatus.DisplayMember = "Text";
            this.cboStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboStatus.DropDownHeight = 75;
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.IntegralHeight = false;
            this.cboStatus.ItemHeight = 14;
            this.cboStatus.Location = new System.Drawing.Point(357, 35);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Size = new System.Drawing.Size(168, 20);
            this.cboStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboStatus.TabIndex = 3;
            this.cboStatus.SelectionChangeCommitted += new System.EventHandler(this.cboStatus_SelectionChangeCommitted);
            this.cboStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cbo_KeyDown);
            // 
            // lblStatus
            // 
            // 
            // 
            // 
            this.lblStatus.BackgroundStyle.Class = "";
            this.lblStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblStatus.Location = new System.Drawing.Point(290, 31);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(35, 23);
            this.lblStatus.TabIndex = 3;
            this.lblStatus.Text = "Status";
            // 
            // lblNo
            // 
            // 
            // 
            // 
            this.lblNo.BackgroundStyle.Class = "";
            this.lblNo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblNo.Location = new System.Drawing.Point(545, 34);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(16, 23);
            this.lblNo.TabIndex = 6;
            this.lblNo.Text = "No";
            // 
            // cboOrder
            // 
            this.cboOrder.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOrder.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOrder.DisplayMember = "Text";
            this.cboOrder.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboOrder.DropDownHeight = 75;
            this.cboOrder.FormattingEnabled = true;
            this.cboOrder.IntegralHeight = false;
            this.cboOrder.ItemHeight = 14;
            this.cboOrder.Location = new System.Drawing.Point(613, 34);
            this.cboOrder.Name = "cboOrder";
            this.cboOrder.Size = new System.Drawing.Size(140, 20);
            this.cboOrder.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboOrder.TabIndex = 6;
            this.cboOrder.SelectionChangeCommitted += new System.EventHandler(this.cboOrder_SelectionChangeCommitted);
            this.cboOrder.SelectedIndexChanged += new System.EventHandler(this.cboOrder_SelectedIndexChanged);
            this.cboOrder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cbo_KeyDown);
            // 
            // lblorder
            // 
            // 
            // 
            // 
            this.lblorder.BackgroundStyle.Class = "";
            this.lblorder.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblorder.Location = new System.Drawing.Point(545, 56);
            this.lblorder.Name = "lblorder";
            this.lblorder.Size = new System.Drawing.Size(63, 23);
            this.lblorder.TabIndex = 7;
            this.lblorder.Text = "Order Type";
            // 
            // CboOrderType
            // 
            this.CboOrderType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboOrderType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboOrderType.DisplayMember = "Text";
            this.CboOrderType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboOrderType.DropDownHeight = 75;
            this.CboOrderType.FormattingEnabled = true;
            this.CboOrderType.IntegralHeight = false;
            this.CboOrderType.ItemHeight = 14;
            this.CboOrderType.Location = new System.Drawing.Point(613, 59);
            this.CboOrderType.Name = "CboOrderType";
            this.CboOrderType.Size = new System.Drawing.Size(140, 20);
            this.CboOrderType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboOrderType.TabIndex = 7;
            this.CboOrderType.SelectionChangeCommitted += new System.EventHandler(this.CboOrderType_SelectionChangeCommitted);
            this.CboOrderType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cbo_KeyDown);
            // 
            // lblTo
            // 
            // 
            // 
            // 
            this.lblTo.BackgroundStyle.Class = "";
            this.lblTo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTo.Location = new System.Drawing.Point(773, 54);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 23);
            this.lblTo.TabIndex = 9;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            // 
            // 
            // 
            this.lblFrom.BackgroundStyle.Class = "";
            this.lblFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFrom.Location = new System.Drawing.Point(773, 31);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(34, 23);
            this.lblFrom.TabIndex = 8;
            this.lblFrom.Text = "From";
            // 
            // DtpToDate
            // 
            // 
            // 
            // 
            this.DtpToDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpToDate.ButtonDropDown.Visible = true;
            this.DtpToDate.CustomFormat = "dd MMM yyyy";
            this.DtpToDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpToDate.IsPopupCalendarOpen = false;
            this.DtpToDate.Location = new System.Drawing.Point(813, 57);
            this.DtpToDate.LockUpdateChecked = false;
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.DtpToDate.MonthCalendar.BackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.DisplayMonth = new System.DateTime(2011, 4, 1, 0, 0, 0, 0);
            this.DtpToDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpToDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.TodayButtonVisible = true;
            this.DtpToDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpToDate.Name = "DtpToDate";
            this.DtpToDate.ShowCheckBox = true;
            this.DtpToDate.Size = new System.Drawing.Size(118, 20);
            this.DtpToDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpToDate.TabIndex = 10;
            this.DtpToDate.ValueChanged += new System.EventHandler(this.DtpToDate_ValueChanged);
            // 
            // DtpFromDate
            // 
            // 
            // 
            // 
            this.DtpFromDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpFromDate.ButtonDropDown.Visible = true;
            this.DtpFromDate.CustomFormat = "dd MMM yyyy";
            this.DtpFromDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpFromDate.IsPopupCalendarOpen = false;
            this.DtpFromDate.Location = new System.Drawing.Point(813, 34);
            this.DtpFromDate.LockUpdateChecked = false;
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.DtpFromDate.MonthCalendar.BackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.DisplayMonth = new System.DateTime(2011, 4, 1, 0, 0, 0, 0);
            this.DtpFromDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpFromDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.TodayButtonVisible = true;
            this.DtpFromDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpFromDate.Name = "DtpFromDate";
            this.DtpFromDate.ShowCheckBox = true;
            this.DtpFromDate.Size = new System.Drawing.Size(118, 20);
            this.DtpFromDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpFromDate.TabIndex = 9;
            this.DtpFromDate.ValueChanged += new System.EventHandler(this.DtpFromDate_ValueChanged);
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(813, 83);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(75, 20);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 11;
            this.BtnShow.Text = "Show";
            // 
            // lblType
            // 
            // 
            // 
            // 
            this.lblType.BackgroundStyle.Class = "";
            this.lblType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblType.Location = new System.Drawing.Point(14, 78);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(40, 23);
            this.lblType.TabIndex = 2;
            this.lblType.Text = "Type";
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(14, 31);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(52, 23);
            this.lblCompany.TabIndex = 0;
            this.lblCompany.Text = "Company";
            // 
            // lblVendor
            // 
            // 
            // 
            // 
            this.lblVendor.BackgroundStyle.Class = "";
            this.lblVendor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblVendor.Location = new System.Drawing.Point(290, 54);
            this.lblVendor.Name = "lblVendor";
            this.lblVendor.Size = new System.Drawing.Size(61, 23);
            this.lblVendor.TabIndex = 0;
            this.lblVendor.Text = "Vendor";
            // 
            // CboType
            // 
            this.CboType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboType.DisplayMember = "Text";
            this.CboType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboType.DropDownHeight = 75;
            this.CboType.FormattingEnabled = true;
            this.CboType.IntegralHeight = false;
            this.CboType.ItemHeight = 14;
            this.CboType.Location = new System.Drawing.Point(72, 83);
            this.CboType.Name = "CboType";
            this.CboType.Size = new System.Drawing.Size(200, 20);
            this.CboType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboType.TabIndex = 2;
            this.CboType.SelectionChangeCommitted += new System.EventHandler(this.CboType_SelectionChangeCommitted);
            this.CboType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cbo_KeyDown);
            // 
            // CboCompany
            // 
            this.CboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompany.DisplayMember = "Text";
            this.CboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCompany.DropDownHeight = 75;
            this.CboCompany.FormattingEnabled = true;
            this.CboCompany.IntegralHeight = false;
            this.CboCompany.ItemHeight = 14;
            this.CboCompany.Location = new System.Drawing.Point(72, 35);
            this.CboCompany.Name = "CboCompany";
            this.CboCompany.Size = new System.Drawing.Size(200, 20);
            this.CboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCompany.TabIndex = 0;
            this.CboCompany.SelectionChangeCommitted += new System.EventHandler(this.CboCompany_SelectionChangeCommitted);
            this.CboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cbo_KeyDown);
            // 
            // CboVendor
            // 
            this.CboVendor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboVendor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboVendor.DisplayMember = "Text";
            this.CboVendor.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboVendor.DropDownHeight = 134;
            this.CboVendor.FormattingEnabled = true;
            this.CboVendor.IntegralHeight = false;
            this.CboVendor.ItemHeight = 14;
            this.CboVendor.Location = new System.Drawing.Point(357, 59);
            this.CboVendor.Name = "CboVendor";
            this.CboVendor.Size = new System.Drawing.Size(168, 20);
            this.CboVendor.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboVendor.TabIndex = 1;
            this.CboVendor.SelectionChangeCommitted += new System.EventHandler(this.CboVendor_SelectionChangeCommitted);
            this.CboVendor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cbo_KeyDown);
            // 
            // ReportViewer1
            // 
            this.ReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Value = null;
            reportDataSource2.Value = null;
            reportDataSource3.Value = null;
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource3);
            this.ReportViewer1.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptRFQForm.rdlc";
            this.ReportViewer1.Location = new System.Drawing.Point(0, 118);
            this.ReportViewer1.Name = "ReportViewer1";
            this.ReportViewer1.Size = new System.Drawing.Size(948, 280);
            this.ReportViewer1.TabIndex = 4;
            // 
            // TmrPurchaseReport
            // 
            this.TmrPurchaseReport.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // ErpPurchaseReport
            // 
            this.ErpPurchaseReport.ContainerControl = this;
            // 
            // FrmRptPurchase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 398);
            this.Controls.Add(this.ReportViewer1);
            this.Controls.Add(this.expandablePanel1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptPurchase";
            this.Text = "Purchase";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmRptPurchase_Load);
            this.expandablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DtpToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErpPurchaseReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboStatus;
        private DevComponents.DotNetBar.LabelX lblStatus;
        private DevComponents.DotNetBar.LabelX lblNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboOrder;
        private DevComponents.DotNetBar.LabelX lblorder;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboOrderType;
        private DevComponents.DotNetBar.LabelX lblTo;
        private DevComponents.DotNetBar.LabelX lblFrom;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpToDate;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpFromDate;
        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DevComponents.DotNetBar.LabelX lblType;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.LabelX lblVendor;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboVendor;
        private Microsoft.Reporting.WinForms.ReportViewer ReportViewer1;
        private DevComponents.DotNetBar.LabelX lblDepartment;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboDepartment;
        private DevComponents.DotNetBar.LabelX lblWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboWarehouse;
        private System.Windows.Forms.BindingSource companyHeaderBindingSource;
       
       
        private System.Windows.Forms.BindingSource purchaseInvoiceMasterBindingSource;
        private System.Windows.Forms.BindingSource purchaseInvoiceDetailsBindingSource;
        private System.Windows.Forms.BindingSource sTPurchaseInvoiceExpenseMasterBindingSource;
        private System.Windows.Forms.BindingSource gRNMasterBindingSource;
        private System.Windows.Forms.BindingSource gRNDetailsBindingSource;
        private System.Windows.Forms.BindingSource purchaseOrderMasterBindingSource;
        private System.Windows.Forms.BindingSource purchaseOrderDetailsBindingSource;
        private System.Windows.Forms.BindingSource sTPurchaseOrderExpenseMasterBindingSource;
        private System.Windows.Forms.BindingSource purchaseQuotationMasterBindingSource;
        private System.Windows.Forms.BindingSource purchaseQuotationDetailsBindingSource;
        private System.Windows.Forms.BindingSource purchaseIndentDetailsBindingSource;
        private System.Windows.Forms.BindingSource purchaseIndentMasterBindingSource;
        private System.Windows.Forms.Timer TmrPurchaseReport;
        private System.Windows.Forms.BindingSource STRFQMasterBindingSource;
        private System.Windows.Forms.BindingSource STRFQDetailsBindingSource;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboExecutive;
        private DevComponents.DotNetBar.LabelX lblExecutive;
        private DevComponents.DotNetBar.LabelX lblDates;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDate;
        private DevComponents.Editors.ComboItem Date;
        private DevComponents.Editors.ComboItem CreatedDate;
        private DevComponents.Editors.ComboItem DueDate;
        private System.Windows.Forms.ErrorProvider ErpPurchaseReport;
    }
}